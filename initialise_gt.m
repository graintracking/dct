function initialise_gt(ignore_id19, skip_functions_check)
% INITIALISE.M for Matlab installations on Windows, Macs, and Linux
%
% ADD DESIRED PATH ENTRIES TO THIS LIST FOLLOWING THE EXISTING FORMAT!!
%
% PT, big bug with image display due to openGL. crashed completely
% ganymedes. disabled the 03/12/07
% '/graintracking',...  % Grain tracking project (WL,MHG,AK) * symlink *
% 24/05/2010  PT correction to ensure that personnal startup is read at the
% end of the initialisation if present for non DCT people
%
% 2011-2013, Modified and improved by Nicola Vigano', vigano@esrf.fr
% 2012, Modified and improved by Yoann Guilhem, guilhem@esrf.fr
% 2005, Create by Greg & Wolfgang

    pathstruct={...                     % Tools from ID19's legacy code
            {'/' },...
            {'/art' },...               % Algebraic reconstruction attempts
            {'/util' },...
            {'/stitching' },...         % Image stitching code
            {'/stitching/heprom' },...
            {'/sliceomatic' },...       % Viewing tool for volumes
            {'/IBF' },...               % IBF tools (Olivier Hignette & Greg Johnson)
            {'/moretomo' },...          % tomography tools in development
            {'/tracking' },...          % particle tracking (Weitzlab, www.harvard.edu)
            {'/ringcorrection' },...    % ring correction code
            {'/imagej' },...            % imagej support
            {'/lsge-matlab' },...       % fitting routines from the NPL in the UK
            {'/lamino' }...             % laminography specific (LH)
            {'/tomotools', '-begin'}... % Tools for tomography
            };
%             {'/parallel' },...          % embarrasingly parallel matlab (ML and GJ)

    current_dir = pwd;
    disp('Graintracking settings in progress...');

    % No multi platform support, at the moment.
    disp(['Machine is: ', computer]);
    if (exist('ignore_id19', 'var') && ~ignore_id19)
        deprecated_id19_paths = fullfile('/data','id19','archive','matlab');

        % Linux machines
        if (~exist(deprecated_id19_paths, 'dir'))
            error(['ERROR: No folder called: "' deprecated_id19_paths '"']);
        end

        % Adding paths
        for n = 1:length(pathstruct)
            switch size(pathstruct{n}, 2)
            case 1
                addpath(fullfile(deprecated_id19_paths, pathstruct{n}{1}));
            case 2
                addpath(fullfile(deprecated_id19_paths, pathstruct{n}{1}), pathstruct{n}{2});
            otherwise
                disp(['Wrong parameters number for entry "', n, '"(', pathstruct{n}{1} ,') of structure "pathstruct"']);
                addpath(fullfile(deprecated_id19_paths, pathstruct{n}{1}));
            end
        end
    end

    % suppress extra linefeeds in command window
    format compact;

    %%%%%%%%%%%%%%%%%%%%%%
    % other special settings

    % changing default settings at root level
    set(0,'DefaultFigurePaperType','A4');
    set(0,'DefaultFigurePaperUnits','centimeters');

    % beamline (for alignment)
    global FT_BL;
    FT_BL = 'id19';

    % volume selection in fasttomo; values are 'total', 'manual', 'graphics'
    global FT_VOLSELECT;
    FT_VOLSELECT = 'graphics';

    % initialise path for graintracking project
    disp('Adding graintracking specific folders to the path');
    global GT_MATLAB_HOME;
    if (isempty(GT_MATLAB_HOME))
        GT_MATLAB_HOME = pwd;
    end
    disp(['Using the DCT code in directory ' GT_MATLAB_HOME]);

    addpath(GT_MATLAB_HOME);

    addpath(fullfile(GT_MATLAB_HOME, '1_preprocessing'));
    addpath(fullfile(GT_MATLAB_HOME, '1_preprocessing', 'gtSetup_Utils'));
    addpath(fullfile(GT_MATLAB_HOME, '2_difspot'));
    addpath(fullfile(GT_MATLAB_HOME, '3_pairmatching'));
    addpath(fullfile(GT_MATLAB_HOME, '3_pairmatchingGUI'));
    addpath(fullfile(GT_MATLAB_HOME, '4_grains'));
    addpath(fullfile(GT_MATLAB_HOME, '5_reconstruction'));
    addpath(fullfile(GT_MATLAB_HOME, '6_rendering'));
    addpath(fullfile(GT_MATLAB_HOME, '7_fed'));
    addpath(fullfile(GT_MATLAB_HOME, '7_fed', 'AndyFunctions'));
    addpath(fullfile(GT_MATLAB_HOME, '7_fed', 'geometry'));
    addpath(fullfile(GT_MATLAB_HOME, '7_fed', 'Ccode_SpreadInt'));
    addpath(fullfile(GT_MATLAB_HOME, '7_fed2'));
    addpath(fullfile(GT_MATLAB_HOME, '8_optimization'));

    addpath(fullfile(GT_MATLAB_HOME, 'zUtil_Analysis'));
    addpath(fullfile(GT_MATLAB_HOME, 'zUtil_Boundaries'));
    addpath(fullfile(GT_MATLAB_HOME, 'zUtil_Crack'));
    addpath(fullfile(GT_MATLAB_HOME, 'zUtil_Cryst'));
    addpath(fullfile(GT_MATLAB_HOME, 'zUtil_Cryst', 'file_cif'));
    addpath(fullfile(GT_MATLAB_HOME, 'zUtil_Conf'));
    addpath(fullfile(GT_MATLAB_HOME, 'zUtil_DataStructures'));
    addpath(fullfile(GT_MATLAB_HOME, 'zUtil_Deformation'));
    addpath(fullfile(GT_MATLAB_HOME, 'zUtil_Deformation', 'plotting'));
    addpath(fullfile(GT_MATLAB_HOME, 'zUtil_Deformation', 'test_generation'));
    addpath(fullfile(GT_MATLAB_HOME, 'zUtil_DB'));
    addpath(fullfile(GT_MATLAB_HOME, 'zUtil_Distortion'));
    addpath(fullfile(GT_MATLAB_HOME, 'zUtil_Drawing'));
    addpath(fullfile(GT_MATLAB_HOME, 'zUtil_Drawing', 'cm_and_cb_utilities'));
    addpath(fullfile(GT_MATLAB_HOME, 'zUtil_Drawing', 'freezeColors'));
    addpath(fullfile(GT_MATLAB_HOME, 'zUtil_EDF'));
    addpath(fullfile(GT_MATLAB_HOME, 'zUtil_ErrorHandling'));
    addpath(fullfile(GT_MATLAB_HOME, 'zUtil_Fit'));
    addpath(fullfile(GT_MATLAB_HOME, 'zUtil_ForwardSim'));
    addpath(fullfile(GT_MATLAB_HOME, 'zUtil_Geo'));
    addpath(fullfile(GT_MATLAB_HOME, 'zUtil_GUI'));
    if (usejava('jvm') && verLessThan('matlab', '8.4.0'))
        addpath(fullfile(GT_MATLAB_HOME, 'zUtil_GUI', 'GUILayout-v1p17'));
        addpath(fullfile(GT_MATLAB_HOME, 'zUtil_GUI', 'GUILayout-v1p17', 'Patch'));
    else
        addpath(fullfile(GT_MATLAB_HOME, 'zUtil_GUI', 'GUILayout-v2.3.2', 'layout'));
    end
    addpath(fullfile(GT_MATLAB_HOME, 'zUtil_GUI', 'FindJObj'));
    addpath(fullfile(GT_MATLAB_HOME, 'zUtil_GVF'));
    addpath(fullfile(GT_MATLAB_HOME, 'zUtil_Help'));
    addpath(fullfile(GT_MATLAB_HOME, 'zUtil_Help', 'ExportFig'));
    addpath(fullfile(GT_MATLAB_HOME, 'zUtil_Help', 'cprintf'));
    addpath(fullfile(GT_MATLAB_HOME, 'zUtil_ICP'));
    addpath(fullfile(GT_MATLAB_HOME, 'zUtil_ID19'));
    addpath(fullfile(GT_MATLAB_HOME, 'zUtil_Imaging'));
    addpath(fullfile(GT_MATLAB_HOME, 'zUtil_Indexter'));
    addpath(fullfile(GT_MATLAB_HOME, 'zUtil_Maths'));
    addpath(fullfile(GT_MATLAB_HOME, 'zUtil_NoiseAnalysis'));
    addpath(fullfile(GT_MATLAB_HOME, 'zUtil_OAR'));
    addpath(fullfile(GT_MATLAB_HOME, 'zUtil_Parameters'));
    addpath(fullfile(GT_MATLAB_HOME, 'zUtil_Python'));
    addpath(fullfile(GT_MATLAB_HOME, 'zUtil_Strain'));
    addpath(fullfile(GT_MATLAB_HOME, 'zUtil_Strain2'));
    addpath(fullfile(GT_MATLAB_HOME, 'zUtil_Taper'));
    addpath(fullfile(GT_MATLAB_HOME, 'zUtil_Tests'));
    addpath(fullfile(GT_MATLAB_HOME, 'zUtil_TIFF'));
    addpath(fullfile(GT_MATLAB_HOME, 'zUtil_TomoUtils'));
    addpath(fullfile(GT_MATLAB_HOME, 'zUtil_Twins'));
    addpath(fullfile(GT_MATLAB_HOME, 'zUtil_VTK'));
    addpath(fullfile(GT_MATLAB_HOME, 'zUtil_XML'));

    addpath(fullfile(GT_MATLAB_HOME, 'FigureManagement'));
    addpath(fullfile(GT_MATLAB_HOME, 'm2html'));

    % matGeom toolbox
    addpath(fullfile(GT_MATLAB_HOME, 'matGeom'));

    % C Compiled code
    addpath(fullfile(GT_MATLAB_HOME, 'bin', 'mex'));

    % Loading configuration from conf file
    load_conf()
	
	% Warning when launched on a Windows PC
	if (ispc())
		% Rendering: the default renderer may have to be changed
		disp('If you are running the code on a Windows PC and experiencing')
		disp('problems with colormaps or graphics resolution, you may have to')
		disp('change the default graphics rendering mode to one of these:')
		disp(' set(0, ''DefaultFigureRenderer'', ''opengl'')')
		disp(' set(0, ''DefaultFigureRenderer'', ''zbuffer'')')
		disp(' set(0, ''DefaultFigureRenderer'', ''painters'')')
	end

    disp(' ')
    cd(fullfile(GT_MATLAB_HOME, 'matGeom'));
    setupMatGeom();
    disp(' ')

    cd(current_dir);

    if (exist('skip_functions_check', 'var') && skip_functions_check)
        check_compiled_funcs = false;
    elseif (verLessThan('MATLAB', '8.6.0'))
        check_compiled_funcs = true;
    else
        check = inputwdefault('Do you want to check compiled functions? (might be very slow) [y/n]', 'n');
        check_compiled_funcs = strcmpi(check, 'y');
    end
    if (check_compiled_funcs)
        gtCheckCompiledFunctions()
    end

    defaultColor = sprintf(gtGetANSIColour());
    boldColor = sprintf(gtGetANSIColour('bold'));
    disp([boldColor 'To report a bug, create a ticket at https://sourceforge.net/p/dct/tickets' defaultColor]);
end

function load_conf()
    global GT_MATLAB_HOME
    global GT_DB;

    default_dbconf = struct( ...
        'name', 'graintracking', ...
        'host', 'graindb.esrf.fr', ...
        'user', 'gtadmin', ...
        'password', 'gtadmin');

    default_astra_paths = struct( ...
        'tools', [GT_MATLAB_HOME '/astra/tools'], ...
        'mex', [GT_MATLAB_HOME '/astra/mex'] );

    try
        xml_conf = gtConfLoadXML(true);

        % File and dirs patterns
        try
            dbconf = gtConfGetField(xml_conf, 'database');
        catch mexc
            gtPrintException(mexc, ...
                '"database" is not a valid field of the xml.conf file. Using defaults.')
            dbconf = default_dbconf;
        end

        GT_DB = struct( ...
            'name', dbconf.name, ...
            'host', dbconf.host, ...
            'user', dbconf.user, ...
            'password', dbconf.password);

        [~, hostname] = system('hostname');
        if (verLessThan('matlab', '8.4.0') ...
                && any(~cellfun(@isempty, regexpi(hostname, {'rnice', 'crunch', 'gpu'}))))
            % This fixes a few problems with rendering on the ESRF cluster
            opengl('software');
        end
        % ASTRA toolkit
        try
            astra_mex_paths = gtConfGetField(xml_conf, 'astra.mex.path');

            inCellForm = iscell(astra_mex_paths);
            for n = 1:length(astra_mex_paths)
                if (inCellForm)
                    tmp_path = astra_mex_paths{n};
                else
                    tmp_path = astra_mex_paths(n);
                end

                tmp_path = gtConfFilterAttribute(tmp_path, 'hostname');
                if (~isempty(tmp_path))
                    addpath(tmp_path);
                end
            end
        catch mexc
            gtPrintException(mexc, ...
                'No Astra mex files configuration. Using hardcoded paths for Astra mex files');
            addpath(default_astra_paths.tools);
            addpath(default_astra_paths.tools);
        end

        % Check which version of matlab compiled the oar executables
        try
            matlabVersion = gtConfGetField(xml_conf, 'matlab.version');

            currentMatlabRelease = version('-release');
            if (~strcmpi(currentMatlabRelease, matlabVersion))
                warning('MATLAB_COMPILED:version_mismatch', ...
                        ['You are now running matlab "%s", but the matlab ' ...
                        'command specified in conf.xml is "%s".\nThere is no ' ...
                        'guarantee that OAR jobs will work'], ...
                        currentMatlabRelease, matlabVersion);
            end
        catch mexc
            gtPrintException(mexc, ...
                'Couldn''t find matlab version in conf.xml');
        end
    catch mexc
        gtPrintException(mexc, ...
            'Couldn''t load configuration file, setting default conf.')

        GT_DB = struct( ...
            'name', default_dbconf.name, ...
            'host', default_dbconf.host, ...
            'user', default_dbconf.user, ...
            'password', default_dbconf.password);

        addpath(default_astra_paths.tools);
        addpath(default_astra_paths.tools);
    end

    disp('Finished adding.');
end

