function vol = gtVolumeMaskBorder(vol, mask_val, lines)
    if (~exist('mask_val', 'var') || isempty(mask_val))
        mask_val = 0;
    end
    if (~exist('lines', 'var') || isempty(lines))
        lines = 1;
    end
    if (iscell(vol))
        for ii = 1:numel(vol)
            vol{ii} = gtVolumeMaskBorder(vol{ii}, mask_val, lines);
        end
    else
        dims_vol = size(vol);
        num_dims = numel(dims_vol);
        if (numel(lines) == 1)
            lines = lines(ones(1, num_dims));
        end
        lines(dims_vol == 1) = 0;
        for ii = 1:num_dims
            lines(ii) = min(round(dims_vol(ii) * 0.25), lines(ii));
            switch(ii)
                case 1
                    vol([1:lines(ii), end-lines(ii)+1:end], :, :) = mask_val;
                case 2
                    vol(:, [1:lines(ii), end-lines(ii)+1:end], :) = mask_val;
                case 3
                    vol(:, :, [1:lines(ii), end-lines(ii)+1:end]) = mask_val;
            end
        end
    end
end
