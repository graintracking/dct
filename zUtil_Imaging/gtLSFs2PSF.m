function psf = gtLSFs2PSF(lsfs, varargin)
    conf = struct('method', 'fourier', 'downsampling', 1);
    conf = parse_pv_pairs(conf, varargin);

    num_lsfs = numel(lsfs);

    data = cat(1, lsfs.data);
    num_data_pixels = size(data, 2);

    if (num_lsfs == 1)
        % We're assuming here that the only LSF given is the radial
        % component of a radially symmetric PSF
        data = reshape(data, [], 1);
        data = (data + flipud(data)) / 2;

        switch (lower(conf.method))
            case 'astra'
            case 'iradon'
                data = data(:, ones(180, 1));
                psf_inner = iradon(data, 1:180,'linear', 'Shepp-Logan');
                psf_upsize = [num_data_pixels, num_data_pixels];
                psf = zeros(psf_upsize, lsfs.data_type);
                shifts = (psf_upsize - 1) / 2 - size(psf_inner) / 2 + 1;
                psf(1+shifts(1):size(psf_inner, 1)+shifts(1), 1+shifts(2):size(psf_inner, 2)+shifts(2)) = psf_inner;
            case 'fourier'
                data_f = ifftshift(data);
                data_f = fft(data_f);
                data_f = fftshift(data_f);

                psf_edge = (num_data_pixels - 1) / 2;
                [xx, yy] = ndgrid(-psf_edge:psf_edge, -psf_edge:psf_edge);
                rr = sqrt(xx .^ 2 + yy .^ 2);
                psf_f = interp1(-psf_edge:psf_edge, data_f, rr, 'spline', 0);

                psf_f = ifftshift(psf_f);
                psf = ifft2(psf_f);
                psf = real(fftshift(psf));
            case 'hankel'
        end
    else
        % Checking compatibility
        if (any(lsfs(1).size ~= [lsfs(2:end).size]))
            error('gtPSFEstimateFromRef:wrong_argument', ...
                'All LSFs should have the same size');
        end
        if (any([lsfs.keep_oversampling]))
            if (any(lsfs(1).keep_oversampling ~= [lsfs(2:end).keep_oversampling]))
                error('gtPSFEstimateFromRef:wrong_argument', ...
                    'All LSFs should behave the same regarding oversampling');
            end
            if (any(lsfs(1).oversampling ~= [lsfs(2:end).oversampling]))
                error('gtPSFEstimateFromRef:wrong_argument', ...
                    'All LSFs should have the same oversampling');
            end
        end

        % The LSFs given here, are Fourier samplings of the 2D PSF
        data_f = ifftshift(data, 2);
        data_f = fft(data_f, [], 2);
        data_f = fftshift(data_f, 2);

        psf_edge = (num_data_pixels - 1) / 2;

        dirs = cat(1, lsfs.direction);
        angles = mod(atan2d(dirs(:, 2), dirs(:, 1)), 360);

        % Let's bring everything in a polar re-arrangement
        data_f = [data_f(:, 1+psf_edge:end); flip(data_f(:, 1:1+psf_edge), 2)];
        angles = [angles; mod(angles - 180, 360)];

        % let's re-order them
        [angles, inds] = sort(angles);
        data_f = data_f(inds, :);

        % Let's wrap around: we need one going to more than 180, and one
        % going to less than -180, and left and right sides have to be
        % treated as radial
        angles = [angles(end) - 360; angles; angles(1) + 360];
        data_f = [data_f(end, :); data_f; data_f(1, :)];

        [xx, yy] = ndgrid(-psf_edge:psf_edge, -psf_edge:psf_edge);
        rr = sqrt(xx .^ 2 + yy .^ 2);
        aa = mod(atan2d(yy, xx), 360);

        [aa_base, rr_base] = ndgrid(angles, 0:psf_edge);
        psf_f = interp2(rr_base, aa_base, data_f, rr, aa, 'spline', 0);

        psf_f = ifftshift(psf_f);
        psf = ifft2(psf_f);
        psf = real(fftshift(psf));
    end

    if (lsfs(1).keep_oversampling) % It was kept before, so now we get rid of it
        to_be_summed = conf.downsampling * lsfs(1).oversampling;
    else
        to_be_summed = conf.downsampling;
    end
    extra_pixels = mod(to_be_summed - mod(num_data_pixels, to_be_summed), to_be_summed);
    num_data_pixels = num_data_pixels + extra_pixels;

    if (mod(conf.downsampling, 2))
        if (mod(lsfs(1).size, conf.downsampling))
            padsize = (extra_pixels - 1) / 2;
            psf = padarray(psf, [padsize, padsize], 'both');
        end
    else
        half_in_size = (size(psf) - 1) / 2;
        half_out_size = (num_data_pixels - 1) / 2;
        [xx, yy] = ndgrid(-half_out_size:half_out_size, -half_out_size:half_out_size);
        psf = interp2(-half_in_size(1):half_in_size(1), -half_in_size(2):half_in_size(2), psf, xx, yy, 'spline', 0);
        size(psf)
    end

    final_psf_size = ceil(lsfs(1).size / conf.downsampling);

    psf = reshape(psf, to_be_summed, final_psf_size, to_be_summed, final_psf_size);
    psf = squeeze(sum(sum(psf, 1), 3));
    psf = psf / sum(psf(:));
end
