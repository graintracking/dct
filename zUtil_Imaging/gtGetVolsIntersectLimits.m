function [lims1, lims2] = gtGetVolsIntersectLimits(size1, size2, shift)
% GTGETVOLSINTERSECTLIMITS Returns the limits in the two volumes, that overlap,
% according to the shift.
%   [lims1, lims2] = gtGetVolsIntersectLimits(size1, size2, shift)

    num_dims_1 = size(size1, 2);
    num_dims_2 = size(size2, 2);
    num_dims_s = size(shift, 2);

    size1(:, (num_dims_1+1):3) = 1;
    size2(:, (num_dims_2+1):3) = 1;
    shift(:, (num_dims_s+1):3) = 0;
    if (num_dims_1 > 3)
        warning('gtGetVolsIntersectLimits:wrong_argument', ...
            'first volume size refers to a %d-dimensional volume', num_dims_1)
    end
    if (num_dims_2 > 3)
        warning('gtGetVolsIntersectLimits:wrong_argument', ...
            'second volume size refers to a %d-dimensional volume', num_dims_2)
    end

    % volume1 limits
    lims1_min = max(1, shift +1);
    lims1_max = min(size1, shift + size2);

    % volume2 limits
    lims2_min = max(1, -shift +1);
    lims2_max = min(size2, size1 - shift);

    lims1 = [ ...
        reshape(lims1_min', 1, num_dims_1, []); ...
        reshape(lims1_max', 1, num_dims_1, []) ];
    lims2 = [ ...
        reshape(lims2_min', 1, num_dims_2, []); ...
        reshape(lims2_max', 1, num_dims_2, []) ];
end
