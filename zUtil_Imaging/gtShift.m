function im = gtShift(imorig, x, y, pad)
% Shifts an image with x (horizontal right) and y (vertical downwards), and zero pads on edges. In case of 
% non-integer x or y, 'interpolatef' is used.

    if (x==0) && (y==0)
        im = imorig;
        return
    end

    xr = ceil(abs(x));
    yr = ceil(abs(y));

    if exist('pad', 'var')
        if strcmpi(pad, 'av')
            padleft   = mean(imorig(:, 1));
            padright  = mean(imorig(:, end));
            padtop    = mean(imorig(1, :));
            padbottom = mean(imorig(end, :));
        else
            padleft   = pad;
            padright  = pad;
            padtop    = pad;
            padbottom = pad;
        end
    else
        padleft   = 0;
        padright  = 0;
        padtop    = 0;
        padbottom = 0;
    end


    if ((round(x) == x) && (round(y) == y))
        xpad = zeros(size(imorig,1), abs(xr));
        ypad = zeros(abs(yr), size(imorig,2));

        if (x > 0)
            xpad(:, :) = padleft;
            tmp = [xpad imorig];
            im = tmp(:, 1:size(imorig,2));
        else
            xpad(:, :) = padright;
            tmp = [imorig xpad];
            im = tmp(:, abs(xr)+1:end);
        end

        if (y > 0)
            ypad(:, :) = padtop;
            tmp = [ypad; im];
            im = tmp(1:size(imorig,1), :);
        else
            ypad(:, :) = padbottom;
            tmp = [im; ypad];
            im = tmp(abs(yr)+1:end, :);
        end
    else
        im = interpolatef(imorig, y, x); % !! interpolatef(im,vertical_down,horizontal_right)

        if (x > 0)
            im(:, 1:xr) = padleft;
        else
            im(:, end-xr+1:end) = padright;
        end

        if (y > 0)
            im(1:yr, :) = padtop;
        else
            im(end-yr+1:end, :) = padbottom;
        end
    end
end % of function
