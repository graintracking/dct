function im=gtShift_subp(imorig,x,y,varargin)
% Shifts an image with x and y, and zero pads on edges. In case of 
% non-integer x or y, 'interpolatef' is used.

xr=ceil(abs(x));
yr=ceil(abs(y));

if round(x)==x && round(y)==y
  xpad=zeros(size(imorig,1),abs(xr));
  ypad=zeros(abs(yr),size(imorig,2));

  if x>0
    tmp=[xpad imorig];
    im=tmp(:,1:size(imorig,2));
  else
    tmp=[imorig xpad];
    im=tmp(:,abs(xr)+1:end);
  end

  if y>0
    tmp=[ypad;im];
    im=tmp(1:size(imorig,1),:);
  else
    tmp=[im;ypad];
    im=tmp(abs(yr)+1:end,:);
  end

else
  im=interpolatef(imorig,x,y);

  if x>0
    im(:,1:xr)=0;
  else
    im(:,end-xr+1:end)=0;
  end

  if y>0
    im(1:yr,:)=0;
  else
    im(end-yr+1:end,:)=0;
  end

end

end % of function
