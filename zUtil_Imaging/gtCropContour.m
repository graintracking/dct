function [x,y]=gtCropContour(x,y,gtbbox)
% GTCROPCONTOUR.M Crops a contour given a bounding box
% Usage:
%       [x,y]=gtCropContour(x,y,bbox)
%       The input contour, contained as a series of points in x and y, is 
%       cropped by bbox, and the result is returned
%       bbox has a format [x1 y1 xsize ysize] where x1 and y1 are the top
%       left pixel to be INCLUDED and x/y sizes are the dimensions of the
%       resulting image.
% 
%
% Example:
%   [x,y]=circlepoints(10,10,15);
%   bbox=[1 1 40 40];
%   [x2,y2]=gtCropContour(x,y,bbox);
%   figure
%   plot(x,y,'r.-');
%   hold on 
%   plot(x2,y2,'g.-')

% Greg Johnson, September 2006
     x1=gtbbox(1);
     y1=gtbbox(2);
     x2=x1+gtbbox(3)-1;
     y2=y1+gtbbox(4)-1;
     
     % if bbox is too large, crop to image extent
     minx=(x<gtbbox(1));
     maxx=(x>gtbbox(1)+gtbbox(3)-1);
     miny=(y<gtbbox(2));
     maxy=(y>gtbbox(2)+gtbbox(4)-1);

     x(minx|maxx|miny|maxy)=[];
     y(minx|maxx|miny|maxy)=[];
     
     