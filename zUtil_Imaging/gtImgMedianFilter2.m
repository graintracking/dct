function filteredImg = gtImgMedianFilter2(img, kernel_size)
% GTIMGMEDIANFILTER2 applies median filter to the given image.
% filteredImg = gtImgMedianFilter2(img, kernel_size)
% Input:
%   - img: is the image that needs to be filtered
%   - kernel_size: is a vector with two entries which specifies the size of the
%                  kernel in the two dimensions (odd numbers only)
% if kernel_size is not given, it will assume a kernel size of [3 3]
% Output:
%   - filteredImg: The filtered image.

    if (~exist('kernel_size','var'))
        kernel_size = [3 3];
    end

    if (size(kernel_size, 1) ~= 1 || size(kernel_size, 2) ~= 2)
        if (size(kernel_size, 2) == 1 && size(kernel_size, 1) == 2)
            kernel_size = kernel_size';
        else
            Mexc = MException('FILTER:wrong_kernel', ...
                              'The kernel size should be a 1x2 vector');
            throw(Mexc);
        end
    end

    if (find((kernel_size & [1 1]) == 0, 1))
        Mexc = MException('FILTER:wrong_kernel', ...
                          'The kernel size should be given by two odd numbers');
        throw(Mexc);
    end

    filteredImg = gtImgMedianFilter(img, kernel_size);
end
