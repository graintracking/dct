function im_big = gtPlaceSubImage2(im_small, im_big, topleftx, toplefty, assign_op)
% GTPLACESUBIMAGE2  Places a small image within a larger one
%     imbig = gtPlaceSubImage(imsmall, imbig, topleftx, toplefty, [assign_op])
%     ------------------------------------------------------------------------
%     imsmall is the image to place
%     imbig is the image to place it into
%     topleftx/y is the location of the top left corner of the placement
%     assign_op can be 'assign','zero','summed','adaptive','conflict'
%
%     The final image is the same size as imbig, no matter where the imsmall
%     is placed

% Greg Johnson, September 2006

if (~exist('assign_op', 'var'))
    assign_op = 'assign';
end

shift = [ (toplefty -1), (topleftx -1), 0 ];

im_big = gtPlaceSubVolume(im_big, im_small, shift, 0, assign_op);

end % end of function
