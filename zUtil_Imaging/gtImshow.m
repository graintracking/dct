function hndl = gtImshow(image, varargin)
    hndl = imshow(permute(image, [2,1]), varargin{:});
end
