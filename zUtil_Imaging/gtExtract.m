function im=gtExtract(imorig,gtbbox)
% GTEXTRACT.M Extracts an subimage, possibly padded with zeros,given a bounding box
% Usage:
%       im=gtExtract(imorig,bbox)
%       The input image, imorig, is cropped by bbox, and the result is im
%       bbox has a format [x1 y1 xsize ysize] where x1 and y1 are the top
%       left pixel to be INCLUDED and x/y sizes are the dimensions of the
%       resulting image.
%
%       The bounding box CAN be larger than the supplied image - see
%       examples.
%
% Examples:
% TO BE WRITTEN!!

% Greg Johnson, September 2006
x1=gtbbox(1);
y1=gtbbox(2);
x2=x1+gtbbox(3)-1;
y2=y1+gtbbox(4)-1;

% get necessary portion of image
subim=gtCrop(imorig,gtbbox);
if numel(subim)==0  % nothing left!
  im=zeros(gtbbox(4),gtbbox(3));
  return
end
if x1>1
  x1=1;
end
if y1>1
  y1=1;
end

im=gtPlaceSubImage(subim,zeros(gtbbox(4),gtbbox(3)),(-x1)+2,(-y1)+2);
