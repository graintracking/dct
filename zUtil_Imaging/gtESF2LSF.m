function lsf_line = gtESF2LSF(img, varargin)
    conf = struct( ...
        'bb', [], 'direction', [], ...
        'size', 21, 'oversampling', 5, 'data_type', 'single', ...
        'side', 'both', 'use_astra', false, 'exclude_border', 1, ...
        'keep_oversampling', true, 'verbose', true);
    conf = parse_pv_pairs(conf, varargin);

    if (ischar(img))
        img = GtVolView.loadVolume(img);
    elseif (numel(img) == 1)
        p = gtLoadParameters();

        filename = sprintf('refHST%04d.edf', img);
        filename = fullfile(p.acq.dir, '0_rawdata', p.acq.name, filename);
        img = edf_read(filename);
    end

    if (isempty(conf.bb))
        try
            if (~exist('p', 'var') || isempty(p))
                p = gtLoadParameters();
            end

            bb_half_size = ceil(p.acq.bbdir(3:4) / 2);
            bb = [p.acq.bbdir(1:2), bb_half_size];
            if (isempty(conf.direction) || strcmpi(conf.direction, 'top-bottom'))
                bb(1) = bb(1) + ceil(bb_half_size(1) / 2);
                bb(2) = bb(2) - ceil(bb_half_size(2) / 2);
            elseif (strcmpi(conf.direction, 'left-right'))
                bb(1) = bb(1) - ceil(bb_half_size(1) / 2);
                bb(2) = bb(2) + ceil(bb_half_size(2) / 2);
            elseif (strcmpi(conf.direction, 'right-left'))
                bb(1) = bb(1) + 3 * ceil(bb_half_size(1) / 2);
                bb(2) = bb(2) + ceil(bb_half_size(2) / 2);
            else
                bb(1) = bb(1) + ceil(bb_half_size(1) / 2);
                bb(2) = bb(2) + 3 * ceil(bb_half_size(2) / 2);
            end
            bb(3:4) = bb(3:4) + 1 - mod(bb(3:4), 2);
        catch
            bb = [0, 0, size(img, 2), size(img, 1)];
        end

        conf.bb = get_roi_bb(img, 'ESF Bounding Box', bb);
    end

    if (isempty(conf.direction))
        directions = {'left-right', 'right-left', 'top-bottom', 'bottom-top'};
        format_dirs = {1, 2, 3, 4; directions{:}};
        questionMsg = ['What is the rough direction of the edge (dark -> bright)?' sprintf('\n%d) %s', format_dirs{:}) '\n'];
        dir_num = inputwdefault(questionMsg, '');
        while (~ismember(dir_num, {'1', '2', '3', '4'}))
            dir_num = inputwdefault(questionMsg, '');
        end
        conf.direction = directions{str2double(dir_num)};
    end

    if (conf.oversampling == 1)
        edge_img = img(conf.bb(2):(conf.bb(2)+conf.bb(4)-1), conf.bb(1):(conf.bb(1)+conf.bb(3)-1));
    else
        shift = (1 - 1/conf.oversampling) / 2;
        vv = linspace(conf.bb(2) - shift, conf.bb(2) + conf.bb(4) - 1 + shift, conf.bb(4) * conf.oversampling);
        uu = linspace(conf.bb(1) - shift, conf.bb(1) + conf.bb(3) - 1 + shift, conf.bb(3) * conf.oversampling);
        [uu, vv] = ndgrid(uu, vv);
        edge_img = interp2(img, uu, vv, 'spline', 0);
    end

    edge_img = cast(edge_img, conf.data_type);

    if (ischar(conf.direction))
        conf.direction = find_edge_angle(edge_img, conf.direction, true);
    end

    if (conf.use_astra)
    else
        angle = atan2d(conf.direction(2), conf.direction(1));
        rot_edge_img = gtRotateVolume(edge_img, -angle);
    end

    sum_esf = sum(rot_edge_img, 2)';
    lsf = diff(sum_esf);
    border = conf.exclude_border * conf.oversampling;
    lsf([1:border, end-border:end]) = 0;
    grad_pos = (1:numel(lsf)) + 0.5;
    [max_lsf_val, max_lsf_pos] = max(lsf);
    rel_max_lsf_pos = max_lsf_pos + 0.5;

    if (conf.verbose)
        f = figure();
        ax = axes('parent', f);
        plot(ax, 1:numel(sum_esf), sum_esf)
        hold(ax, 'on')
        plot(ax, grad_pos, lsf)
        plot(ax, rel_max_lsf_pos, max_lsf_val, 'o')
        hold(ax, 'off')
    end

    % Finding avg point
    lsf_edge = (conf.size * conf.oversampling - 1) / 2;
    inds = max_lsf_pos-5:max_lsf_pos+5;
    center_pos = sum(lsf(inds) .* inds) / sum(lsf(inds));

    lsf = interp1(lsf, linspace(center_pos-lsf_edge, center_pos+lsf_edge, conf.size * conf.oversampling), 'spline');
    switch (conf.side)
        case 'both'
        case 'dark'
            if (verLessThan('matlab', '8.2.0'))
                lsf(lsf_edge+2:end) = fliplr(lsf(1:lsf_edge));
            else
                lsf(lsf_edge+2:end) = flip(lsf(1:lsf_edge));
            end
        case 'bright'
            if (verLessThan('matlab', '8.2.0'))
                lsf(1:lsf_edge) = fliplr(lsf(lsf_edge+2:end));
            else
                lsf(1:lsf_edge) = flip(lsf(lsf_edge+2:end));
            end
    end
%     lsf(lsf < 0) = 0;
    if (~conf.keep_oversampling)
        lsf = reshape(lsf, conf.oversampling, conf.size);
        lsf = sum(lsf, 1);
    end
    lsf = lsf / sum(abs(lsf));

    if (conf.verbose)
        f = figure();
        ax = axes('parent', f);
        plot(ax, lsf)
    end

    c = reshape(fieldnames(conf), 1, []);
    c(2, :) = struct2cell(conf);
    lsf_line = struct('data', lsf, c{:});
end

function bb = get_roi_bb(img, title, bb)
    hfig = figure('name', title);

    colormap(hfig, gray);
    ax = axes('parent', hfig);
    imagesc(img, 'parent', ax);
    drawnow();
    hold(ax, 'on');

    plot([bb(1), bb(1)+bb(3)-1, bb(1)+bb(3)-1, bb(1), bb(1)], [bb(2), bb(2), bb(2)+bb(4)-1, bb(2)+bb(4)-1, bb(2)], 'r-');
    bb_center = bb(1:2) + (bb(3:4) - 1) / 2;
    plot([bb_center(1) bb_center(1)], [bb(2) bb(2)+bb(4)-1], 'c-.');
    plot([bb(1), bb(1)+bb(3)-1], [bb_center(2) bb_center(2)], 'c-.');

    % interactive check
    questionMsg = 'Are you satisfied with this bounding box for the region where to compute the ESF sampling? [y/n]';
    while (strcmpi(inputwdefault(questionMsg, 'y'), 'n'))
        disp('Please select (zoom onto) the region where to compute the ESF and disable zoom afterwards');

        figure(hfig), clf;
        ax = axes('parent', hfig);
        imagesc(img, 'parent', ax);
        h = zoom(hfig);
        set(h, 'Enable', 'on');
        waitfor(h, 'Enable', 'off')
        hold(ax, 'on');
        disp('-> Now click top-left and bottom-right corners for final selection');
        bb = ginput(2);
        bb = round([bb(1, 1), bb(1, 2), bb(2, 1)-bb(1, 1)+1, bb(2, 2)-bb(1, 2)+1]);

        % Make sure the bbox width and height are odd numbers (needed for correlation)
        bb(3:4) = bb(3:4) + 1 - mod(bb(3:4), 2);

        % Plot the new bounding box
        plot([bb(1), bb(1)+bb(3)-1, bb(1)+bb(3)-1, bb(1), bb(1)], [bb(2), bb(2), bb(2)+bb(4)-1, bb(2)+bb(4)-1, bb(2)], 'r-');
        bb_center = bb(1:2) + (bb(3:4) - 1) / 2;
        plot([bb_center(1) bb_center(1)], [bb(2) bb(2)+bb(4)-1], 'c-.');
        plot([bb(1), bb(1)+bb(3)-1], [bb_center(2) bb_center(2)], 'c-.');
    end

    close(hfig);

    fprintf('Chosen BB: [%s]\n', sprintf(' %d', bb))
end

function [direction, angle] = find_edge_angle(edge_img, rough_dir, do_plot)
    % We rotate the roi, to facilitate the computation
    switch (rough_dir)
        case 'left-right'
            base_k_90_rot = 0;
        case 'right-left'
            base_k_90_rot = 2;
        case 'top-bottom'
            base_k_90_rot = 3;
        case 'bottom-top'
            base_k_90_rot = 1;
        otherwise
            error('gtESF2LSF:wrong_argument', ...
                'Direction "%s" unknown', rough_dir);
    end
    edge_img = rot90(edge_img, base_k_90_rot);

    % We will now use the derivative of all the lines, to determine the
    % angle
    grads = diff(edge_img, 1, 1);
    [~, max_grads_pos] = max(grads, [], 1);

    % least square fitting of the tilt angle
    X = [ones(numel(max_grads_pos), 1), (1:numel(max_grads_pos))'];
    vals = X \ reshape(max_grads_pos, [], 1);

    if (exist('do_plot', 'var') && do_plot)
        f = figure();
        ax = axes('parent', f);
        plot(ax, max_grads_pos)
        hold(ax, 'on')
        plot(ax, (1:numel(max_grads_pos))', X*vals);
        hold(ax, 'off')
    end

    angle = base_k_90_rot * 90 + atand(vals(2));

    direction = [cosd(angle), sind(angle)];
end
