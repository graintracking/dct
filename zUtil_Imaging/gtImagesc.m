function hndl = gtImagesc(image, varargin)
% GTIMAGESC permutes the image to visualise. Be aware that some constructs don't
% work:
%   imagesc(x,y,C) % is NOT handled correctly
    hndl = imagesc(permute(image, [2,1]), varargin{:});
end
