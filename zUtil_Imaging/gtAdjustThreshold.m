function [thresh_val,mask]=gtAdjustThreshold(thresh_val,img,h)


   %if ~exist('h','var')
   %  h=figure; 
   %	 state=uisuspend(h,false);  %create a new figure
   %end
  
  quit=0;
  
  subplot(1,2,1);
  imshow(img,[])
  subplot(1,2,2);
  colormap(gray)
 
  set(h,'KeyPressFcn',@sfKeyPress)
  
  while 1
      sfUpdateFigure
      if quit==1
		
        break
      end
  end
   
function sfKeyPress(varargin)
  c=varargin{2};
  switch c.Key
	case 'rightarrow'
      thresh_val=thresh_val+thresh_val/10;
	  sfUpdateFigure;
    case 'leftarrow' 
      thresh_val=thresh_val-thresh_val/10;
	  sfUpdateFigure;
    case 'return'
      %exit interactive mode and return current shift
      set(h,'KeyPressFcn','');
	  
	  mask=double(img>thresh_val);
	  quit=1;
  end      
end

function sfUpdateFigure
  %thresh_val
  imshow(img>thresh_val,[]);
  drawnow;
end


end