function gtCheckSegmentation(imnum,parameters,clims)
% gtCheckSegmentation(imnum,parameters,clims)

gtDBConnect();

if ( ~isfield(parameters.seg, 'writeblobs') || (~parameters.seg.writeblobs) || ...
   isempty(mym(['SELECT XIndex FROM ' parameters.acq.name 'difblobdetails'])) )

    disp('No data available for difblobs...Quitting')
    return
end

query = sprintf('SELECT XIndex,YIndex FROM %sdifblobdetails WHERE ZIndex=%d',parameters.acq.name,imnum);
full = gtGetFullImage(imnum,parameters);
spots = zeros(parameters.acq.ydet,parameters.acq.xdet);
[x,y] = mym(query);
for ii=1:length(x)
    spots(y(ii),x(ii))=1;
end	 


cont=bwperim(spots);
cont=bwmorph(cont,'dilate');
cont=255*uint8(cont);
cont(:,:,2:3)=0;

full = mat2gray(full,clims);

imshow(full);
set(gcf,'Renderer','OpenGL');
hold on;
h = imshow(cont);
hold off;
set(h,'Alphadata',0.3);

end % end of function
