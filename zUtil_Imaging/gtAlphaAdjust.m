function gtAlphaAdjust(h1,h2)
% GTALPHAADJUST.M Adds a GUI slider to adjust opacity of two overlaid images
% Usage:
%      gtAlphaAdjust(h1,h2)
%      h1 and h2 are the handles to the two images
%
% Example:
% im1=rand(100);
% im2=rand(100);
% clf
% h1=imagesc(im1)
% hold on
% h2=imagesc(im2)
% gtAlphaAdjust(h1,h2)

% GJ September 2006
h_fig=get(get(h1,'parent'),'parent');  % find the figure that has the objects in it

h_gui=figure;  % create a new figure for the sliders
tmp=get(h_gui,'position');
ss=get(0,'screensize');
set(h_gui,'position',[tmp(1) tmp(2) 200 200])
set(h_gui,'toolbar','none','menubar','none','name','AlphaControl','numbertitle','off')



  h_slider1=uicontrol('style','slider');
  set(h_slider1,'callback',@sfUpdateDisplay)
  set(h_slider1,'units','normalized');
  set(h_slider1,'position',[0.1 0.5 0.9 0.3])
  set(h_slider1,'min',0,'max',1,'sliderstep',[0.01 0.01]);
  set(h_slider1,'value',1);
  set(h_slider1,'tag','slider');
  

  h_slider2=uicontrol('style','slider');
  set(h_slider2,'callback',@sfUpdateDisplay)
  set(h_slider2,'units','normalized');
  set(h_slider2,'position',[0.1 0.1 0.9 0.3])
  set(h_slider2,'min',0,'max',1,'sliderstep',[0.01 0.01]);
  set(h_slider2,'value',1);
  set(h_slider2,'tag','slider');
  set(h_gui,'handlevisibility','callback')
  
  function sfUpdateDisplay(varargin)

    switch varargin{1}
      case h_slider1
        set(h1,'alphadata',get(gcbo,'value'));
      case h_slider2
        set(h2,'alphadata',get(gcbo,'value'));
      otherwise
        
        disp('Empty')
    end
    drawnow
  end
end
