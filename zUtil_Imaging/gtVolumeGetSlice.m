function slice = gtVolumeGetSlice(vol, plane, slice_idx, do_squeeze)
    switch (lower(plane))
        case {'xy', 'XY'}
            slice = vol(:, :, slice_idx, :);
        case {'yx', 'YX'}
            slice = permute(vol(:, :, slice_idx, :), [2 1 3 4]);
        case {'yz', 'YZ'}
            slice = vol(slice_idx, :, :, :);
        case {'zy', 'ZY'}
            slice = permute(vol(slice_idx, :, :, :), [1 3 2 4]);
        case {'xz', 'XZ'}
            slice = vol(:, slice_idx, :, :);
        case {'zx', 'ZX'}
            slice = permute(vol(:, slice_idx, :, :), [3 2 1 4]);
    end
    if (~exist('do_squeeze', 'var') || isempty(do_squeeze) || do_squeeze)
        slice = squeeze(slice);
    end
end