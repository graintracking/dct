classdef GtPSF < handle
    properties
        psf_direct = [];
        psf_adjoint = [];

        otf_direct = [];
        otf_adjoint = [];

        is_symmetric = true;
        is_projection_invariant = true;
        image_size = [];

        use_otf = true;
        otf_padding = 0;

        data_type = 'single';
    end

    methods (Access = public)
        function self = GtPSF(varargin)
            self = parse_pv_pairs(self, varargin);
        end

        function set_psf_direct(self, psf_d, image_size)
            self.reset();

            [psf_d, psf_a, self.is_symmetric, self.is_projection_invariant] ...
                = self.check_single_psf(psf_d);

            self.psf_direct = psf_d;
            if (~self.is_symmetric)
                self.psf_adjoint = psf_a;
            end

            if (exist('image_size', 'var') && ~isempty(image_size))
                self.image_size = image_size;

                % if we know the images sizes, we can already compute the OTF
                self.init_otfs();
            end
        end

        function imgs = apply(self, imgs, is_direct)
            self.check_incoming_images(imgs);

            if (self.use_otf)
                imgs = self.apply_otf(imgs, is_direct);
            else
                imgs = self.apply_psf(imgs, is_direct);
            end
        end

        function imgs = apply_psf_direct(self, imgs)
            self.check_incoming_images(imgs);

            if (self.use_otf)
                imgs = self.apply_otf(imgs, true);
            else
                imgs = self.apply_psf(imgs, true);
            end
        end

        function imgs = apply_psf_adjoint(self, imgs)
            self.check_incoming_images(imgs);

            if (self.use_otf)
                imgs = self.apply_otf(imgs, false);
            else
                imgs = self.apply_psf(imgs, false);
            end
        end
    end

    methods (Access = protected)
        function reset(self)
            self.psf_direct = [];
            self.psf_adjoint = [];

            self.otf_direct = [];
            self.otf_adjoint = [];

            self.image_size = [];
        end

        function [psf_d, psf_a, is_sym, is_p_inv] = check_single_psf(self, psf_d)
            % let's be sure that it will be in ASTRA's projection
            % convention
            if (size(psf_d, 1) == size(psf_d, 2))
                psf_d = permute(psf_d, [1 3 2]);
            elseif (size(psf_d, 1) ~= size(psf_d, 3))
                error([mfilename ':wrong_argument'], ...
                    'PSFs should be in the form of _square images_, with odd edges')
            elseif (mod(size(psf_d, 1), 2) ~= 1)
                error([mfilename ':wrong_argument'], ...
                    'PSFs should be in the form of square images, with _odd edges_')
            end

            % let's renormalize the PSFs
            psfs_norm = 1 ./ sum(sum(abs(psf_d), 1), 3);
            psf_d = bsxfun(@times, psf_d, psfs_norm);

            psf_d = cast(psf_d, self.data_type);

            % Let's find out whether they are symmetric or not
            if (verLessThan('matlab', '8.2.0'))
                psf_a = flipdim(flipdim(psf_d, 1), 3); %#ok<DFLIPDIM>
            else
                psf_a = flip(flip(psf_d, 1), 3);
            end

            is_sym = all(abs(psf_d(:) - psf_a(:)) < eps('single'));

            is_p_inv = size(psf_d, 2) == 1;
        end

        function otf = init_single_otf(self, psf)
            num_psfs = size(psf, 2);
            psf_edge = (size(psf, 1) - 1) / 2;

            self.otf_padding = psf_edge;

            comp_img_size = [self.image_size(1) + 2 * psf_edge, ...
                num_psfs, self.image_size(2) + 2 * psf_edge];
            otf = zeros(comp_img_size, self.data_type);

            center_otf = floor(self.image_size / 2) + 1;
            lims_otf_lower = center_otf;
            lims_otf_upper = center_otf + 2 * psf_edge;

            otf(lims_otf_lower(1):lims_otf_upper(1), :, lims_otf_lower(2):lims_otf_upper(2)) = psf;

            otf = ifftshift(otf, 1);
            otf = ifftshift(otf, 3);

            otf = fft(otf, [], 1);
            otf = fft(otf, [], 3);

%             otf = fftshift(otf, 1);
%             otf = fftshift(otf, 3);
        end

        function init_otfs(self)
            self.otf_direct = self.init_single_otf(self.psf_direct);

            if (~self.is_symmetric)
                self.otf_adjoint = self.init_single_otf(self.psf_adjoint);
            end
        end

        function check_incoming_images(self, imgs)
            num_imgs = size(imgs, 2);
            if (~self.is_projection_invariant && (num_imgs ~= size(self.psf_d, 2)))
                error([mfilename ':wrong_argument'], ...
                    'The number of images (%d) differs from the number of projection dependent PSFs (%d)', ...
                    num_imgs, size(self.psf_d, 2))
            end

            imgs_size = [size(imgs, 1), size(imgs, 3)];
            if (isempty(self.image_size))
                self.otf_direct = [];
                self.otf_adjoint = [];

                self.image_size = imgs_size;
            elseif (any(self.image_size ~= imgs_size))
                warning([mfilename ':wrong_argument'], ...
                    'OTFs computed for the wrong image size ([%d, %d] instead of [%d, %d]). Recomputing them...', ...
                    self.image_size, imgs_size)

                self.otf_direct = [];
                self.otf_adjoint = [];

                self.image_size = imgs_size;
            end

            if (isempty(self.otf_direct) && self.use_otf)
                self.init_otfs();
            end
        end

        function imgs = apply_otf(self, imgs, is_direct)
            imgs = padarray(imgs, [self.otf_padding, 0, self.otf_padding]);

            imgs = ifftshift(imgs, 1);
            imgs = ifftshift(imgs, 3);

            imgs = fft(imgs, [], 1);
            imgs = fft(imgs, [], 3);

%             imgs = fftshift(imgs, 1);
%             imgs = fftshift(imgs, 3);

            if (is_direct || self.is_symmetric)
                imgs = bsxfun(@times, imgs, self.otf_direct);
            else
                imgs = bsxfun(@times, imgs, self.otf_adjoint);
            end

%             imgs = ifftshift(imgs, 1);
%             imgs = ifftshift(imgs, 3);

            imgs = ifft(imgs, [], 1);
            imgs = ifft(imgs, [], 3);

            imgs = real(imgs);

            imgs = fftshift(imgs, 1);
            imgs = fftshift(imgs, 3);

            imgs = imgs((self.otf_padding+1):(end-self.otf_padding), :, ...
                (self.otf_padding+1):(end-self.otf_padding));
        end

        function imgs = apply_psf(self, imgs, is_direct)
            if (is_direct || self.is_symmetric)
                psf = self.psf_direct;
            else
                psf = self.psf_adjoint;
            end

            if (self.is_projection_invariant)
                imgs = convn(imgs, psf, 'same');
            else
                for ii_i = 1:size(imgs, 2)
                    imgs(:, ii_i, :) = convn(imgs(:, ii_i, :), psf(:, ii_i, :), 'same');
                end
            end
        end
    end
end