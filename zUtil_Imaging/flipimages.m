function flipimages(prefix)
% FUNCTION FLIPIMAGES  Flip up-down images 
% FLIPIMAGES(PREFIX)
%       prefix = image prefix (can contain also the directory path)
%     Overwrite the images in the same folder with the same name
%

if strcmpi(prefix,'ref')
    disp('treating reference images...')
end

listFile = dir([prefix '*.edf'])

for k = 1:length(listFile)
    im = edf_read( listFile(k).name );
    im2 = flipud(im);
    edf_write( im2, listFile(k).name );
    disp(listFile(k).name)
end

end % end function
