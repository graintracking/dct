function average = gtImgMeanValue(content)
%GTIMGMEANVALUE Average or mean of matrix elements.
%   B = gtImgMeanValue(A) computes the mean of the values in A.
%
%   A can be any n-dimensional matrix
%
%   Example
%   -------
%       img = edf_read('blabla.edf');
%       average = gtImgMeanValue(img)
%  
%   See also MEAN, MEAN2, STD, STD2.

    average = gtMathsSumNDVol(content) / numel(content);
end

