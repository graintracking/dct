function img = gtSoftThreshold2(img, thr)
% GTSOFTTHRESHOLD Applies soft thresholding to the image
%   img = gtSoftThreshold(img, thr)

    if (numel(thr) > 1)
        warning('THRESHOLDING:wrong_argument', ...
                'Expected one threshold, but got: %d', length(thr))
        thr = min(thr(:));
    elseif (isempty(thr))
        warning('THRESHOLDING:wrong_argument', ...
                'Expected one threshold, an EMPTY array was passed!')
        thr = 0;
    end

    tempImg = abs(img) - thr;
    tempImg = (tempImg + abs(tempImg)) / 2;
    img = sign(img) .* tempImg;
end
