function dim=gtBboxSizeMat(bbox)
% GTBBOXSIZEMAT returns size of gtbbox in matrix convention (rows, columns)
dim=[bbox(4) bbox(3)];