function gtOarMonitor(array_id)
    global GT_MATLAB_HOME
    cmd = ['python ' fullfile(GT_MATLAB_HOME, 'dct_launch.py') ...
        ' batch monitor array' sprintf(' %d', array_id) ' &'];

    system(cmd, '-echo');
end
