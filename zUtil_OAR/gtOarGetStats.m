function stats_table = gtOarGetStats(IDs, vars)
% GTOARGETSTATS  Gets Oar stats for the given job IDs
%     -------------------------------------------------------------------------
%     statsTable = gtOarGetStats(IDs, vars)
%
%     INPUT:
%       IDs        = <int>    OAR job ID numbers
%       vars       = <string> OAR Variables to extract
%
%     OUTPUT:
%       statsTable = <struct> Asked variables stored in a structure
%
%     Version 002 02-07-2012 by LNervo
%       Format subfunction header
%       Improve regexp matching
%       Check var through possible OAR variables
%
%     Version 001 04-04-2012 by Nvigano

    %%%%%%%%%%%%%%
    % check vars
    %%%%%%%%%%%%%%
    possible_vars = {...
        'assigned_hostnames', ...
        'assigned_resources', ...
        'command', ...
        'cpuset_name', ...
        'dependencies', ...
        'events', ...
        'exit_code', ...
        'initial_request', ...
        'job_array_id', ...
        'job_array_index', ...
        'jobType', ...
        'launchingDirectory', ...
        'message', ...
        'name', ...
        'owner', ...
        'project', ...
        'properties', ...
        'queue', ...
        'reservation', ...
        'resubmit_job_id', ...
        'scheduledStart', ...
        'startTime', ...
        'state', ...
        'stopTime', ...
        'submissionTime', ...
        'types', ...
        'walltime', ...
        'wanted_resources' };

    if (strcmpi(vars, 'all'))
        vars = possible_vars;
    else
        % Removing not allowed variables
        [newVars, acceptedIDs] = intersect(vars, possible_vars);

        if (length(newVars) ~= length(vars))
            fprintf('Removing not existing variables for OAR:\n   ');
            rejectedIDs = setdiff(1:length(vars), acceptedIDs);
            fprintf(' %s', vars{rejectedIDs});
            fprintf('\n');
        end

        vars = newVars;
    end

    if (isempty(IDs))
        stats_table(1, :) = vars(:);
        stats_table(2, :) = cellfun(@(x){[]}, vars(:));
        stats_table = struct(stats_table{:});
        return;
    end

    % Creating the job query string
    jobsStr = ['oarstat -f' sprintf(' -j %d', IDs)];
    % Getting the output
    [~, output] = system(jobsStr);

    [stats_table, incomplete_logs] = parse_oar_output(vars, output);

    if (incomplete_logs)
        fprintf('\nIncomplete logs for jobs: %s\n\n', sprintf(' %d', IDs));
    end
end

function [stats_table, incomplete_logs] = parse_oar_output(vars, output)
    num_vars = numel(vars);

    % Now we chop the input:
    jobsBlocks = regexp(output(1:end-1), '\n\n', 'split');
    stats_table = cell(2, num_vars+1);

    incomplete_logs = false;
    % Now extract info from the output of 'oarstat'
    if (~isempty(stats_table))
        stat_id = regexp(jobsBlocks, 'Job_Id: (?<Job_Id>.*?)\n', 'names', 'once');
        stat_id = [stat_id{:}];
        stats_table{1, 1} = 'Job_Id';
        stats_table{2, 1} = arrayfun(@(x){str2double(x.('Job_Id'))}, stat_id);

        for ii_v = 1:num_vars
            stat = regexp(jobsBlocks, [vars{ii_v} ' = (?<' vars{ii_v} '>.*?)\n'], 'names', 'once');
            for ii_c = numel(stat):-1:1
                if (~isempty(stat{ii_c}))
                    stat_struct(ii_c).(vars{ii_v}) = stat{ii_c}.(vars{ii_v});
                else
                    stat_struct(ii_c).(vars{ii_v}) = '';
                end
            end
            stat = stat_struct;
            stats_table{1, ii_v+1} = vars{ii_v};
            stats_table{2, ii_v+1} = {stat.(vars{ii_v})};

            if (isempty(stats_table{2, ii_v+1}))
                warning('gtOarGetStats:no_such_stat', ...
                    [ 'The stat "%s" is not reported in the output from ' ...
                      'oarstat. This might create further errors.\n'], ...
                    vars{ii_v});
                incomplete_logs = true;

                stats_table{2, ii_v+1} = cell(1, numel(stat_id));
            end
        end
        stats_table = struct(stats_table{:});
    end
end

