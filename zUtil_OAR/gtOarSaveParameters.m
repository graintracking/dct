function gtOarSaveParameters(filename, oarparameters)
    writeParamsOARtoXML([filename '.xml'], oarparameters);
    save([filename '.mat'], 'oarparameters', '-v7.3');
end

function writeParamsOARtoXML(filename, oarparams)
    docNode = com.mathworks.xml.XMLUtils.createDocument('oarparameters');
    docRootNode = docNode.getDocumentElement();
    funcNames = fieldnames(oarparams);
    for funcName = funcNames'
        funcElement = docNode.createElement('function');
        funcElement.setAttribute('name', funcName{1});
        func = oarparams.(funcName{1});
        arrayNames = fieldnames(func);
        for arrayName = arrayNames'
            arrayElement = docNode.createElement('array');
            arrayElement.setAttribute('id', arrayName{1}(6:end));
            array = func.(arrayName{1});
            for n = 1:numel(array.job)
                jobElement = docNode.createElement('job');
                jobElement.setAttribute('id', num2str(array.job(n).jobId));

                % Using reflection, but avoiding duplication of Job ID info
                fields = fieldnames(array.job(n));
                fields = setdiff(fields, {'jobId'});
                for field = fields'
                    fieldElement = docNode.createElement(field{1});
                    fieldElement.appendChild( ...
                        docNode.createTextNode(array.job(n).(field{1})));
                    jobElement.appendChild(fieldElement);
                end
                arrayElement.appendChild(jobElement);
            end
            funcElement.appendChild(arrayElement);
        end
        docRootNode.appendChild(funcElement);
    end
    xmlwrite(filename, docNode);
end
