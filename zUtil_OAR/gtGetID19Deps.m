function result = gtGetID19Deps(functionNames)
% GTGETID19DEPS Function used to identify dependencies on ID19's functions
    result = gtGetFunctionDeps(functionNames, {}, 'matlab_');
    matches = regexp(result, 'id19');
    result = result(cellfun(@(x)isempty(x), matches) == 0);
end
