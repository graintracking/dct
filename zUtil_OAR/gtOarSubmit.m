function gtOarSubmit(verbose)
% FUNCTION gtOarSubmit  Submit jobs to OAR
%     gtOarSubmit(verbose)
%       verbose : enable verbose output

%   Version 003 08-11-2011 LNervo
%       Dealing better with logname
%
%   Version 002 20-09-2011 LNervo

if (~exist('verbose', 'var') || isempty(verbose))
    verbose = false;
end

if (~exist('oarset.mat', 'file'))
    error('OAR:no_oarset', ...
        'The file oarset.mat does not exist. Please go in the directory where it is.')
end

out = GtConditionalOutput(verbose);

% XXX - This is actually a terrible way of passing information around!
% Please fix me in the future.
fprintf('\nLoading oarset.mat...\n')
set = [];
load('oarset.mat');

command = sprintf('oarsub -S %s', set.oardirectives);

% going to the directory where to run the executable
disp(' ')
disp('Submitting the job(s)...')
[ret_val, output_sub] = unix(command);
disp('...done!')

if (ret_val)
    error(sprintf('%s:submission_error', mfilename), ...
        '\n%s\nCommand: "%s"', output_sub, command)
end

% Getting the job id array. The first value is the job_array_id.
inx = strfind(output_sub, 'OAR_JOB');
output_sub = output_sub(inx:end);

% cell object array
array = regexp(output_sub, '[0-9]*', 'match');
array = sort(array);
array = unique(array);

fname = set.fname;

% Save info of each job
vars = {'state', 'exit_code', 'submissionTime', 'startTime', 'stopTime', 'owner'};
IDs = cellfun(@str2double, array);

fprintf('Quering OAR to get fresh information...')
oarStats = gtOarGetStats(IDs, vars);
fprintf(' Done.\n')

oarParamsFile = fullfile(set.launchdir, set.matparfile);
try
    fprintf('Loading oarparameters.[xml|mat]...')
    oarparameters = gtOarLoadParameters(oarParamsFile);
    fprintf(' Done.\n')
catch mexc
    gtPrintException(mexc)
    oarparameters = struct();
    fprintf(' it doesn''t exist! NO PROBLEM, creating a new one.\n')
end

for ii = 1:length(array)
    job.jobId           = oarStats(ii).Job_Id;
    job.state           = oarStats(ii).state;
    job.exit_code       = oarStats(ii).exit_code;
    job.submissionTime  = oarStats(ii).submissionTime;
    job.startTime       = oarStats(ii).startTime;
    job.stopTime        = oarStats(ii).stopTime;
    oarparameters.(fname).(['array' array{1}]).job(ii) = job;
end

out.fprintf('\n\nSaving the oarparameters.mat file...');
gtOarSaveParameters(oarParamsFile, oarparameters)
out.fprintf('done!\n');

log = set.logname;
set.logname = strrep(set.logname, '%jobid%', array{1});
movefile(log, set.logname);

save(fullfile(set.launchdir, 'oarset.mat'), 'set');

fprintf('OAR parameter file: %s\n\n*** LOG FILES ***\n%s\n', oarParamsFile, set.logname)
% replace arrayid number
[~, ~] = unix(['sed -i -e s/' '%jobid%' '.log' '/' array{1} '.log' '/g ' set.logname]);
%
disp(strrep(log, '.log', '.out'))
disp(strrep(log, '.log', '.err'))

fprintf('arrayid:\n   ''%s''\n', array{1})
fprintf('jobid:\n%s\n', sprintf('  ''%s''\n', array{:}) )

% save arrayid into a text file
fid2 = fopen('oar.log', 'a+');
fprintf(fid2, '%s %s %s %s\n', oarStats(1).owner, fname, array{1}, oarStats(1).submissionTime);
fclose(fid2);

fid3 = fopen('arrays.txt', 'a+');
fprintf(fid3, '%s\n', fname, sprintf('%s ', array{:}) );
fclose(fid3);

fprintf('\nCheck the jobs status with the command\n')
fprintf('    ! oarstat -j <jobid> -s\n')
fprintf('You can run this command to check the job status:\n')
fprintf('    gtBatch( ''update'', %s )\n\n', array{1}, array{1})

disp('You can easily look at error and output log files with:')
disp('    gtOarLogView( ''oar'', ''err'', <job_ids> )')
disp('    gtOarLogView( ''oar'', ''out'', <job_ids> )')
disp('')

end % end function
