function gtExternalCompileFunctions(varargin)
    global GT_MATLAB_HOME
    cmd = ['python ' fullfile(GT_MATLAB_HOME, 'dct_launch.py') ' compile_matlab'];
    cmd = [cmd sprintf(' -c %s', varargin{:})];

    [status, msg] = system(cmd, '-echo');
    if (status)
        errorMsg = sprintf('Command: %s\nExited with code: %d, and message:\n%s', ...
            cmd, status, msg);
        gtError('gtExternalCompileFunctions:runtime_error', errorMsg)
    end
end
