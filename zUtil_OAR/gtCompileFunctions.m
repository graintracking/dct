function gtCompileFunctions( force, mfiles, funcs_to_compile )

    force = strcmp(force, 'force');

    should_compile_all = isempty(funcs_to_compile) ...
            || (numel(funcs_to_compile) == 1 && strcmp(funcs_to_compile{1}, 'all'));

    all_keys = fieldnames(mfiles);
    if (should_compile_all)
        wrong_names = {};
        funcs_to_compile = all_keys;
    else
        valid = ismember(funcs_to_compile, all_keys);
        wrong_names = funcs_to_compile(~valid);
        funcs_to_compile = funcs_to_compile(valid);
    end
    funcs_to_compile = reshape(funcs_to_compile, [1 numel(funcs_to_compile)]);
    num_funcs = numel(funcs_to_compile);

    error_log = cell(num_funcs, 2);
    for ii_f = 1:numel(funcs_to_compile)
        func_name = funcs_to_compile{ii_f};
        disp(['Checking function: "' func_name '":'])
        func = mfiles.(func_name);

        mat_file_path = func.('in_mfile');
        comp_file_path = func.('out_file');
        [up_to_date, msg] = gtCheckFunctionUpToDate(mat_file_path, comp_file_path);
        fprintf(msg);
        if (force || ~up_to_date)
            disp(['Compiling function: ' func_name]);

            comp_file_dir = fileparts(comp_file_path);
            try
                gtMcc(func_name, 'bin_path', comp_file_dir)
            catch mexc
                fprintf('Compilation of function "%s" failed!\n', func_name);
                error_log(ii_f, :) = {func_name, mexc};
            end
        end
    end

    errors = find(cellfun(@(x)(~isempty(x)), error_log(:, 1)));
    for ii_e = 1:numel(errors)
        func_name = error_log{errors(ii_e), 1};
        mexc = error_log{errors(ii_e), 2};
        gtPrintException(mexc, ...
            sprintf('Could not compile function: %s', func_name))
    end

    for wr_name = wrong_names
        disp(['Wrong m-file name: ' wr_name{1}]);
    end
end
