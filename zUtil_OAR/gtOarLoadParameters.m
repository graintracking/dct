function oarparameters = gtOarLoadParameters(filename)
    if (exist([filename '.xml'], 'file'))
        oarparameters = readParamsOARfromXML([filename '.xml']);
    elseif (exist([filename '.mat'], 'file'))
        oarparameters = [];
        load([filename '.mat']);
    else
        gtError('gtBatch:OAR:missing_file', ...
            'oarparameters.[xml | mat] is missing')
    end
end

function oarparams = readParamsOARfromXML(filename)
    tree = xmlread(filename);
    oarElem = tree.getFirstChild();
    oarparams = struct();
    funcElements = oarElem.getElementsByTagName('function');
    for numFunc = 1:funcElements.getLength()
        funcElem = funcElements.item(numFunc-1);
        funcName = char(funcElem.getAttribute('name'));
        arrayElements = funcElem.getElementsByTagName('array');
        for numArray = 1:arrayElements.getLength()
            arrayElem = arrayElements.item(numArray-1);
            arrayName = sprintf('array%s', char(arrayElem.getAttribute('id')));
            jobElements = arrayElem.getElementsByTagName('job');

            allocCell = cell(1, jobElements.getLength());

            fields = cell(1, 0);
            fieldElements = jobElements.item(0).getChildNodes();
            for numField = 1:fieldElements.getLength();
                fieldName = char(fieldElements.item(numField-1).getNodeName());
                if (~strcmp(fieldName, '#text'))
                    fields{1, end+1} = fieldName;
                end
            end
            fieldsExpansion = fields;
            fieldsExpansion(2, :) = {allocCell};
            jobs = struct('jobId', allocCell, fieldsExpansion{:});

            for numJob = 1:jobElements.getLength()
                jobElem = jobElements.item(numJob-1);
                jobs(numJob).jobId = str2double(char(jobElem.getAttribute('id')));

                for field = fields
                    fieldElems = jobElem.getElementsByTagName(field{1});
                    jobs(numJob).(field{1}) = char(fieldElems.item(0).getTextContent());
                end
            end
            oarparams.(funcName).(arrayName).job = jobs;
        end
    end
end
