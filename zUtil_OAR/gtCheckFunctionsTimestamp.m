function deps = gtCheckFunctionsTimestamp(functionNames, refTstamp)
% GTCHECKFUNCTIONSTIMESTAMP Checks if any of the timestamps of the given
% functions is bigger than the reference timestamp
% This would mean that the function is outdated.
%
% A list of functions that were modified after the reference timestamp is
% returned

    deps = {};
    for itemNum = 1:length(functionNames)
        name = functionNames{itemNum};
        fileInfo = dir(name);
        if (~isempty(fileInfo) && fileInfo(1).datenum > refTstamp)
            deps{end+1} = name;
        end
    end
end
