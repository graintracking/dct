function gtOarLaunch(executable_name, first, last, njobs, otherparameters, do_submit, varargin)
% FUNCTION gtOarLaunch  Launch jobs on OAR.
%     gtOarLaunch(executable, first, last, njobs, otherparameters, do_submit, varargin)
%     -------------------------------------------------------------------------------
%
%     This function would replace condor_make, using the new cluster
%     OAR, resource manager and batch scheduler for NICE
%
%     INPUT:
%            executable = '/full/path/to/executable.ext' (if not .m)
%                         if executable is not in the pwd;
%                         'executable.ext' if executable is in the pwd;
%                 first = the first image to process
%                  last = the last image to process
%                 njobs = number of total jobs to submit {1}
%       otherparameters = all the other parameters to be passed to the
%                         function as a string {''}
%                         (ex. 'par1 par2 par3')
%            do_submit = logical to sumbit or not the jobs {true}
%              varargin = the values of the OAR directives as pairs
%                         i.e. 'walltime', '3:00:00', 'mem', 4000, ...
%
%                         Examples for available options:
%
%                         NAME                 VALUE         OAR SYNTAX
%                         name                 executable    -n <text> / --name=<text>
%                         walltime             '2:00:00'     -l walltime=<walltime>
%                         mem                  2000          -p mem > <memory>
%                         mem_core_mb          8000          -p mem_core_mb=<mempercore>
%                         gpu                  false         -q nice (true : -q gpu)
%                         node                 1             -l /nodes=<nnode>
%                         cpu                  1             -l /cpu=<ncpu>
%                         core                 1             -l /core=<ncore>
%                         host                 'htc'         -p host like '<name>%'
%                         checkpoint           600           --checkpoint <checkpoint>
%                         signal               2             --signal <signal>
%                         type                 'idempotent'  -t <type> / --type=<type>
%                         resubmit             0             --resubmit=<job_id>
%
%                         distribute           true/false    distribute workload if jobssize prop. to grainID
%                         list                 []            as alternative to use of [first:last]
%
%     Version 014 25-05-2015 by WLudwig
%       Introduce 'distribute' to equalize processing time scaling with grainIDs
%       Introduce the use of 'list' as an alternative to [first : last]
%
%     Version 013 31-10-2013 by LNervo
%       Updated gpu flag : it should be 'queue' directive but it is easier to
%       keep 'gpu' flag
%
%     Version 012 11-10-2013 by LNervo
%       Added directive "mem_core_mb" to get automatic the new segmentation gui
%
%     Version 011 22-06-2012 by LNervo
%         Add resumbission for job with exit_code 99 (signal 2) from checkpoint
%         See http://oar.imag.fr/sources/2.5/docs/documentation/OAR-DOCUMENTATION-USER.pdf
%         (pag. 19)
%
%     Version 010 16-11-2011 by LNervo
%         Using gtOarMakeLaunchScript.m instead of singleton.sh and
%         make_oar_matlab2.sh files
%
%     Version 009 08-11-2011 by LNervo
%         Add more OAR directives
%         Use singleton.sh in the zUtil_OAR folder.
%         Use of make_oar_matlab2.sh
%
%     Version 008 22-09-2011 by LNervo
%         Adjust part of oardirectives (=varargin). Passed by pairs
%
%     Version 007 29-04-2011 by LNervo:
%         Change name of structure parameters into oarparameters
%         Change argument oarparameters into oardirectives
%         Add flag to submit or not the jobs
%         Create a function gtOarSubmit to submit later the jobs
%
%



% clean the screen each time
%clc
debug = false;
out = GtConditionalOutput(debug);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% default OAR directives
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
oarpars = gtOarGetDefaultOarpars(executable_name);
oarpars = parse_pv_pairs(oarpars, varargin);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% title
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp(' ')
disp(' ****************************************')
disp(' *******  Submitting jobs to OAR  *******')
disp(' ****************************************')
disp(' ')

[~, hostname] = system('hostname');
hostname = strtrim(hostname);
user = getenv('USER');

out.fprintf(' ***       version  :  %s\n', version);
out.fprintf(' ***       hostname :  %s\n', hostname);
out.fprintf(' ***       user     :  %s\n', user);

%%% check function arguments

if (~strfind(hostname, 'rnice'))
    error('This function can not be executed on the machine %s!\n', hostname)
end
if (nargin < 3)
    error('gtOarLaunch:notEnoughArgument', ...
          'You must give at least three arguments:\n    gtOarLaunch(executable, first, last)')
end
if (~exist('njobs', 'var') || isempty(njobs) || (nargin < 4))
    njobs = 1;
end
if (~exist('otherparameters', 'var') || (nargin < 5))
    otherparameters = '';
end
if (~exist('do_submit', 'var') || isempty(do_submit) || (nargin < 6))
    do_submit = true;
end

% DEBUG: print arguments
out.fprintf('\n')
out.fprintf(' ***      executable: %s\n', executable_name)
out.fprintf(' ***           first: %d\n', first)
out.fprintf(' ***            last: %d\n', last)
out.fprintf(' ***           njobs: %d\n', njobs)

if ~isempty(otherparameters)
    out.fprintf(' *** otherparameters: %s\n', otherparameters)
end
out.odisp('oardirectives:')
out.odisp(oarpars)
% End DEBUG

%split executable path
[~, fname, ~] = fileparts(executable_name);
%split tempname path
[~, tempfilename] = fileparts(tempname()); % This is actually a matlab function

%%% Setting log dir and log file names

% create temporary directory for temporary files (/tmp_14_days usually)
logdir = fullfile('/tmp_14_days', 'oar', 'log');
subdir = fullfile('/tmp_14_days', 'oar', 'submit');

% check directories
if ~exist(logdir, 'dir')
    [~, msg] = mkdir(logdir); disp(msg)
end
if ~exist(subdir, 'dir')
    [~, msg] = mkdir(subdir); disp(msg)
end

out.odisp(['Submit files now in ' subdir])
out.odisp(['Log files now in ' logdir])

% create temporary .oar file in the system directory for submission files
% this file contains the parameters for oar
tmpname  = [tempfilename '.oar'];
tname    = fullfile(subdir, tmpname);

fileroot = ['oar.' fname '-' getenv('USER') '_' num2str(oarpars.resubmit) '.%jobid%'];
logerr   = fullfile(logdir, [fileroot '.err']);
logout   = strrep(logerr, '.err', '.out');
logname  = strrep(logerr, '.err', '.log');

% update app fields with OAR directives
oarpars.stderr = logerr;
oarpars.stdout = logout;

%%% Setting the parameters struct
set.launchdir  = pwd;
set.matparfile = 'oarparameters';
set.oarsubfile = tname;
set.logname    = logname;
set.version    = version;
set.fname      = fname;
set.njobs      = njobs;
set.hostname   = hostname;
set.user       = user;
set.date       = datestr(now, 'yyyy-mm-dd HH:MM:SS');
set.oarpars    = oarpars;

%%% Create OAR parameters file .oar
% create temporary file
out.fprintf('\nCreating OAR directives file')

% get OAR parameters
set.oardirectives = gtOarMakeLaunchScript(set.launchdir, subdir, tmpname, oarpars);

[~, msg] = unix(['chmod 755 ' set.oardirectives]); disp(msg)
clear msg
out.fprintf('...done!\n')

disp(['OAR directives: ' set.oardirectives])


%%% Create array parameters file .params
out.fprintf('\nCreating the OAR .params file...\n')

set.oarparameters = strrep(set.oardirectives, '.oar', '.params');

% open the file .params and write the parameters for each job on each line
fid = fopen(set.oarparameters, 'w');

if ~isempty(oarpars.list)
    jobsize = floor(numel(oarpars.list)/njobs);
    % construct lists of grainids (will only be used when oar.distribute=true)
    for ii_j = 1 : njobs
        jobs{ii_j} = oarpars.list([ii_j : njobs : numel(oarpars.list)]);
    end
else
    jobsize = floor((last - first + 1) / njobs);
    range_firsts = first:jobsize:last;
    range_lasts = range_firsts(2:end)-1;
    range_lasts(end+1) = last;
end




% build the parameters file
func_path = which(executable_name);
m_func_path = which([executable_name '.m']);

% write comment and help for the function in the .params file
% If the two filenames are the same, it is a matlab function
if strcmpi(func_path, m_func_path)
    fid_tmp = fopen(m_func_path, 'r');
    help_str = fgetl(fid_tmp);
    fclose(fid_tmp);

    if isempty(help_str)
        help_str = ['# This file has been generated by using gtOarLaunch_v10' ...
                    ... Trick to avoid matching of gtOarLaunch by the Python script
                    '(''' executable_name ''')'];
    end
    fprintf(fid, '# %s', help_str);
    fprintf(fid, '# \n');
    
    if oarpars.distribute
        for n = 1:njobs
            fprintf(fid, '%s %d %d %s list ''%s''\n', executable_name, first, ...
                    last, otherparameters, num2str([jobs{n}]));
            sprintf('%s %d %d %s list ''%s''\n', executable_name, first, ...
                    last, otherparameters, num2str([jobs{n}]))
        end
    else
        for n = 1:length(range_firsts)
            fprintf(fid, '%s %d %d %s\n', executable_name, range_firsts(n), ...
                    range_lasts(n), otherparameters);
        end
    end
else
    fid_tmp = fopen(func_path, 'r');
    help_str = fgetl(fid_tmp);
    help_str = char(help_str, fgetl(fid_tmp));
    fclose(fid_tmp);

    if isempty(help_str)
        help_str = ['# This file has been generated by using MATLAB function' ...
                    ' gtOarLaunch, \nrunning the module ''' executable_name ''''];
    end
    fprintf(fid, '# %s', help_str);
    fprintf(fid, '# \n');

    for n = 1:length(range_firsts)
        fprintf(fid, '%s %s\n', func_path, otherparameters);
    end
end

fclose(fid);
out.fprintf('done!\n')

disp(['OAR parameters: ' set.oarparameters])

%%% Save OAR directives in .log file
out.fprintf('Saving parameters in the .log file...')
fid = fopen(set.logname, 'w');
fprintf(fid, '# General information\n');
names = fieldnames(set);
numOarDirectives = length(names);
for ii = 1:numOarDirectives
    if ~strcmp(names{ii}, 'oarpars')
        value = set.(char(names(ii)));
        if ~ischar(value)
            fprintf(fid, '%s = %s\n', char(names(ii)), num2str(value));
        else
            fprintf(fid, '%s = %s\n', char(names(ii)), value);
        end
    end
end
fprintf(fid, '\n# Setting of OAR directives\n');
names = fieldnames(oarpars);
numOarDirectives = length(names);
for ii = 1:numOarDirectives
    value = oarpars.(char(names(ii)));
    if ~ischar(value)
        fprintf(fid, '%s = %s\n', char(names(ii)), num2str(value));
    else
        fprintf(fid, '%s = %s\n', char(names(ii)), value);
    end
end
fprintf(fid, '\n');
fclose(fid);

out.fprintf('done!\n')

%%% Save oarset.mat
out.fprintf('\n\nSaving the oarset.mat file...')

% save also the set structure in set.mat file
cd(set.launchdir);
save([set.launchdir filesep 'oarset.mat'], 'set');
out.fprintf('done!\n')

%%% Submit the job(s)

if (do_submit)
    gtOarSubmit(false);
else
    disp([fname ' is ready for submission']);
    disp('Submit the jobs running the function, choosing true/false for debugging')
    disp('    gtOarSubmit(verbose)')
end

end %function


