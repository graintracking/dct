function gtBatchLogView(batch_system, file_type, job_ids, username)
    if (nargin < 3)
        print_help()
        error('gtBatchLogView:wrong_argument', 'Not enough arguments!')
    end

    if (isempty(batch_system))
        batch_system = 'oar';
    end
    if (~exist('username', 'var') || isempty(username))
        username = getenv('USER');
    end

    switch (lower(batch_system))
        case 'oar'
            oar_log_view(file_type, job_ids, username)
        case 'slurm'
    end
end

function oar_log_view(file_type, job_ids, username)
    if (~ischar(file_type))
        error('gtBatchLogView:wrong_argument', ...
            'File type should be a string among: [ ''err'' | ''out'' | ''log'' | ''params'' | ''oar'' ]')
    end

    oar_stats = gtOarGetStats(job_ids, {'name', 'job_array_id', 'cpuset_name', 'resubmit_job_id'});

    for ii = 1:numel(job_ids)
        fname = oar_stats(ii).('name');
        idarray = str2double(char(oar_stats(ii).('job_array_id')));
        % We match "_[numbers]" because some users have numbers in their
        % usernames (experiments) at the ESRF
        idjob = regexp(oar_stats(ii).('cpuset_name'), '_\d+', 'match');
        idjob = str2double(idjob{ii}(2:end));

        log_file_dir = fullfile('/tmp_14_days', 'oar', 'log');
        base_user_file_name = [fname '-' username '_' oar_stats(ii).('resubmit_job_id') ];
        switch (lower(file_type))
            case {'log'}
                logname = fullfile(log_file_dir, ['oar.' base_user_file_name '.' num2str(idarray) '.' file_type]);
            case {'err', 'out'}
                logname = fullfile(log_file_dir, ['oar.' base_user_file_name '.' num2str(idjob) '.' file_type]);
            case {'oar', 'params'}
                logname = [base_user_file_name '.' file_type];
            otherwise
        end

        if (exist(logname, 'file'))
            disp(['Reading file ' logname '...'])
            disp('-----------------------------------------------------------------------------------------')
            type(logname);
            disp('-----------------------------------------------------------------------------------------')
        else
            fprintf('File not found: %s\n', logname)
        end
    end
end

function print_help()
    fprintf('FUNCTION %s(batch_system, file_type, job_ids)\n', mfilename)
    fprintf('  batch_system: {''oar''} | ''slurm'' (if empty -> ''oar'')\n')
    fprintf('  file_type: ''err'' | ''out'' | ''log'' | ''params'' | ''oar'' (for OAR jobs)\n')
    fprintf('  job_ids: list of job ids\n')
end
