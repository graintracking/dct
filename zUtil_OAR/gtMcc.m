function gtMcc(myfunctionname, varargin)
% gt_mcc.m Compiles an executable and puts it in the right place
    if (isempty(whos('global', 'GT_MATLAB_HOME')))
        error('COMPILE:no_such_variable', ...
              ['GT_MATLAB_HOME variable doesn''t exist in global workspace, '...
               'your environment is not sane, and you need to either ' ...
               're-initialise or re-run matlab.'])
    end
    global GT_MATLAB_HOME
    try
        v = ver('compiler');
        mcc_vers = textscan(v.Version, '%s','delimiter','.');
        mcc_vers_minor = str2double(mcc_vers{1}{2});
        mcc_vers_major = str2double(mcc_vers{1}{1});
    catch mexc
        disp('Error: Is the Matlab Compiler installed?')
        rethrow(mexc)
    end

    if ( mcc_vers_major < 4 || (mcc_vers_major == 4 && mcc_vers_minor < 8))
        error('gtMcc:wrong_matlab_version', ...
            'Compilation using Matlab versions before R2008 is not supported')
    end

    % Parsing arguments
    conf = struct( ...
        'dry_run', false, ...
        'bin_path', fullfile(GT_MATLAB_HOME, 'bin', 'compiled'), ...
        'licence_warning', false, ...
        'remove_old', false );
    conf = parse_pv_pairs(conf, varargin);

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Modify the M name if necessary
    [~, ~, ext] = fileparts(myfunctionname);
    if (~strcmp(ext, '.m'))
        myfunctionname = [myfunctionname '.m'];
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % check the code is visible at the moment
    if (~exist(myfunctionname, 'file'))
        error('gtMcc:wrong_file_name', ...
            'Cannot find your function - is it in the path?')
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Adding Global Workspace
    globalVars = whos('global');
    eval(['global' sprintf(' %s', globalVars.('name'))]);
    if (isempty(whos('global', 'GT_DB')))
        error('gtMcc:no_such_variable', ...
              ['GT_DB variable doesn''t exist in global workspace, '...
               'your environment is not sane, and you need to either ' ...
               're-initialise or re-run matlab.'])
    end
    save('workspaceGlobal.mat', 'GT_DB', 'GT_MATLAB_HOME');

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % startup the compiler
    disp('Starting to compile - this may take a few minutes...');
    if (conf.remove_old)
        fprintf('- Removing previously compiled code..');
        compFile = fullfile(conf.bin_path, myfunctionname(1:end-2));
        runFile = fullfile(conf.bin_path, ['run_' myfunctionname(1:end-1) 'sh']);
        filesToDelele = {compFile, runFile};
        for fileToDelele = filesToDelele
            if (conf.dry_run)
                disp(['I should now be deleting: ' fileToDelele{1}]);
            elseif exist(fileToDelele{1}, 'file')
                delete(fileToDelele{1});
            end
        end
        fprintf('\b\b: Done.\n')
    end

    disp(['- Placing binary in: ' conf.bin_path]);
    if (conf.dry_run)
        cmd = sprintf('mcc -m "%s" -d "%s" -a workspaceGlobal.mat', myfunctionname, conf.bin_path);
        disp(['I should now be executing: ''' cmd '''']);
    else
        try
            mcc('-m', myfunctionname, '-d', conf.bin_path, '-a', 'workspaceGlobal.mat');
        catch mexc
            if (gtCheckExceptionType(mexc, 'mcc_err_checkout_failed') ...
                    || gtCheckExceptionType(mexc, 'MATLAB:req_failed_to_checkout_license'))
                disp('Could not get a compiler license.');
                disp('Surf to http://compweb/public/nice/matlab_licence.php');
                disp('and call the person with the compiler license to see if you could use it!');
            end
            rethrow(mexc)
        end
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Clean the folder
    delete('workspaceGlobal.mat');

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % And let the user know everything is finished
    if (conf.licence_warning)
        disp('PLEASE RESTART MATLAB TO FREE THE COMPILER FOR SOMEONE ELSE!');
    end
end
