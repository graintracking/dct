function oarStats = gtOarResubmit(error_ids)
%     oarStats = gtOarResubmit(error_ids)
%     ---------------------------------
%       Useful when few jobs in an array failed

    if (~exist('error_ids', 'var') || isempty(error_ids))
        error_ids = [];
        oarparams = gtOarLoadParameters('oarparameters');
        funcNames = fieldnames(oarparams);
        for funcName = funcNames'
            func = oarparams.(funcName{1});
            arrayNames = fieldnames(func);
            for arrayName = arrayNames'
                array = func.(arrayName{1});
                matchesStates = strcmp({array.job(:).state}, 'Terminated');
                matchesCodes = strcmp({array.job(:).exit_code}, '0 (0,0,0)');
                matches = matchesStates | matchesCodes;
                error_ids = [error_ids array.job(matches).jobId];
            end
        end
        fprintf('Resubmitting all failed Jobs: \n')
    end

    fprintf('Quering OAR to get fresh information...')
    oarStats = gtOarGetStats(error_ids, {'all'});
    fprintf(' Done.\n')

    for ii = 1:length(oarStats)
        oarStats{ii}.command = sfSplitCommand(oarStats{ii}.command);

        disp(['oarStats.name: ' oarStats{ii}.name])
        disp(['oarStats.Job_Id:    ' num2str(oarStats{ii}.Job_Id)])
        print_structure(oarStats{ii}, 'oarStats', false, true);

        check = inputwdefault('Do you want to resubmit this job? [y/n]', 'y');
        if (strcmpi(check, 'y'))
            oarStats{ii}.resubmit_job_id = oarStats{ii}.Job_Id;
            fprintf('Resubmitting job: %d ...\n', oarStats{ii}.resubmit_job_id)
            gtOarLaunch(oarStats{ii}.name, oarStats{ii}.command.first, ...
                oarStats{ii}.command.last, 1, ...
                oarStats{ii}.command.otherparameters, true, ...
                'walltime', oarStats{ii}.walltime, ...
                'resubmit', oarStats{ii}.resubmit_job_id)
        end
    end
end

function output = sfSplitCommand(command)
    [~, cmd_args] = strtok(command);
    s = regexp(strtrim(cmd_args), '\s+', 'split');

    output.first = str2double(s{2});
    output.last = str2double(s{3});

    s = s(4:end);
    s(2, :) = {' '};
    output.otherparameters = [s{1:end-1}];
end
