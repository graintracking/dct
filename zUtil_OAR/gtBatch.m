function gtBatch(action, varargin)
% GTBATCH
%     gtBatch((action, varargin)
%     ----------------------------
%
%     Check OAR arrayjobs from OAR and update oarparameters.[xml|mat]
%
%     INPUT:
%       action   = { 'update' } : updates fields and shows them
%                   | 'delete'  : deletes entries and oar jobs
%                   | 'clean'   : cleans entries in file (implied by 'update')
%                   | 'view'    : shows logs (it calls gtBatchLogView)
%                   | 'submit'  : submit job (not implemented yet!)
%
%       varargin = 'name'       : expects a string which is a regular expression
%                   | 'array'   : expects a list of array ids
%                   | 'job'     : expects a list of job ids
%                   | <string>  : implies 'name'
%                   | [<double>]: implies 'job'
%                   | {nothing} : implies 'name' with string 'gt*'
%
%     Notes about arguments for action 'view':
%       The full argument structure is required (without implied arguments)
%       and it requires an extra argument being:
%
%       view_arg = 'log'        : display the submission '.log' file for the array
%                   | 'err'     : display the '.err' file for a selected job
%                   | 'out'     : display the '.out' file for a selected job
%                   | 'oar'     : display the submission '.oar' file for the array
%                   | 'params'  : display the submission '.params' file for the array
%

    if (~exist('action', 'var'))
        action = 'update';
    end

    try
        oarparams = gtOarLoadParameters('oarparameters');
    catch mexc
        gtPrintException(mexc)
        oarparams = struct();
        fprintf('oarparameters.[xml|mat] doesn''t exist! Creating a new one!\n')
    end
    
    switch(lower(action))
        case 'update'
            [records, ~] = getRecords(oarparams, varargin);
            oarparams = gtBatchUpdate(oarparams, records);
            oarparams = gtBatchClean(oarparams);
            gtOarSaveParameters('oarparameters', oarparams);
        case 'clean'
            oarparams = gtBatchClean(oarparams);
            gtOarSaveParameters('oarparameters', oarparams);
        case 'delete'
            [records, selectionType] = getRecords(oarparams, varargin);
            oarparams = gtBatchDelete(oarparams, records, selectionType);
            oarparams = gtBatchClean(oarparams);
            gtOarSaveParameters('oarparameters', oarparams);
        case 'view'
            gtBatchLogview(varargin);
        case 'submit'
            oarparams = gtBatchSubmit(oarparms, varargin);
            gtOarSaveParameters('oarparameters', oarparams);
        otherwise
            gtError('gtBatch:wrong_argument', 'Action %s is not valid!', action)
    end
end

function oarparams = gtBatchUpdate(oarparams, records)
    vars = getQueryVars();
    fprintf('Quering OAR to get fresh information...')
    new_stats = gtOarGetStats([records(:).jobId], vars);
    fprintf(' Done.\n')

    if (numel(new_stats) < numel(records))
        fprintf('-> Some jobs are not recorded by OAR any more! Skipping them!\n')
        r_j_ids = [records(:).jobId];
        o_j_ids = [new_stats(:).Job_Id];

        keep = false(size(r_j_ids));
        for ii_k = 1:numel(keep)
            keep(ii_k) = any(r_j_ids(ii_k) == o_j_ids);
            if (~keep(ii_k))
                fprintf(' - Marking Job_Id: "%d" as done!\n', records(ii_k).jobId)
                jobId    = records(ii_k).('jobId');
                arrayid  = records(ii_k).('array_id');
                funcName = records(ii_k).('function');
                arrayName = sprintf('array%d', arrayid);
                idx = [oarparams.(funcName).(arrayName).job(:).jobId] == jobId;
                oarparams.(funcName).(arrayName).job(idx).state = 'Terminated';
                oarparams.(funcName).(arrayName).job(idx).exit_code = '0 (0,0,0)';
            end
        end
        records = records(keep);
    end
    njobs = numel(records);

    display_vars = getDisplayVars();
    %%%%%%%%%%%%%%%%%%%%%%%
    % loop through jobs
    for m = 1 : njobs
        jobId    = new_stats(m).('Job_Id');
        arrayid  = str2double(new_stats(m).('job_array_id'));
        funcName = new_stats(m).('name');

        % Checking presence of fields
        if (~hasAllFields(new_stats(m), vars))
            fprintf('Skipping incomplete record for Job: %d (consider deleting it)\n', jobId)
            continue
        end

        h_name = length(funcName)+2;
        h_job  = length(num2str(jobId))+2;
        h_other = length(datestr(now(),'yyyy-mm-dd HH:MM:SS'))+2;

        arrayName = sprintf('array%d', arrayid);
        jobs = oarparams.(funcName).(arrayName).job;
        indx = find([jobs(:).jobId] == jobId, 1);

        fprintf('%*s %*d ', h_name, funcName, h_job, records(m).jobId)
        for l = 1:length(display_vars)
            temp = new_stats(m).(display_vars{l});
            jobs(indx).(display_vars{l}) = temp;
            fprintf('%*s ', h_other, temp)
        end
        fprintf('\n')
        oarparams.(funcName).(arrayName).job = jobs;
    end
end

function oarparams = gtBatchClean(oarparams)
    fnames = fieldnames(oarparams);
    for fname = fnames'
        funcRecord = oarparams.(fname{1});
        arrayNames = fieldnames(funcRecord);
        for arrayName = arrayNames'
            arrayRecord = funcRecord.(arrayName{1});

            jobs = arrayRecord.job;
            states = {jobs(:).state};
            codes = {jobs(:).exit_code};
            is_terminated = strcmp(states, 'Terminated');
            is_no_error = strcmp(codes, '0 (0,0,0)');
            keep = ~(is_terminated & is_no_error);
            arrayRecord.job = jobs(keep);

            if (isempty(arrayRecord.job))
                funcRecord = rmfield(funcRecord, arrayName{1});
                fprintf('All the jobs from array %s are terminated with exit_code ''0 (0,0,0)''\n', ...
                    arrayName{1}(6:end))
            else
                funcRecord.(arrayName{1}) = arrayRecord;
            end
        end
        if (isempty(fieldnames(funcRecord)))
            oarparams = rmfield(oarparams, fname{1});
            fprintf('All arrays of function %s are terminated with exit_code ''0 (0,0,0)''\n', ...
                fname{1})
        else
            oarparams.(fname{1}) = funcRecord;
        end
    end
end

function oarparams = gtBatchDelete(oarparams, records, selectionType)
    switch(selectionType)
        case 'name'
            funcNames = unique({records(:).function});
            for fname = reshape(funcNames, [1 numel(funcNames)])
                questionStr = sprintf(['Are you sure to remove function ' ...
                    '"%s", from OAR and oarparams.mat? [y/n]'], fname{1});
                check = inputwdefault(questionStr, 'y');
                if strcmpi(check, 'y')
                    fprintf('Deleting jobs from OAR... ')
                    arrayNames = fieldnames(oarparams.(fname{1}));
                    for arrayName = reshape(arrayNames, [1 numel(arrayNames)])
                        deleteOarArray(arrayName{1}(6:end));
                    end
                    fprintf('Done.\nRemoving information about the jobs... ')
                    oarparams = rmfield(oarparams, fname{1});
                    fprintf('Done.\n')
                end
            end
        case 'array'
            arrayIds = unique([records(:).array_id]);
            for array_id = reshape(arrayIds, [1 numel(arrayIds)])
                questionStr = sprintf(['Are you sure to remove array ' ...
                    '"%d", from OAR and oarparams.mat? [y/n]'], array_id);
                check = inputwdefault(questionStr, 'y');
                if strcmpi(check, 'y')
                    fprintf('Deleting jobs from OAR... ')
                    deleteOarArray(array_id);
                    fprintf('Done.\nRemoving information about the jobs... ')
                    funcName = records(find([records(:).array_id] == array_id, 1)).function;
                    arrayName = sprintf('array%d', array_id);
                    oarparams.(funcName) = rmfield(oarparams.(funcName), arrayName);
                    fprintf('Done.\n')
                end
            end
        case 'job'
            jobId = unique(records.jobId);
            warning('gtBatch:wrong_argument', ...
                'OAR doesn''t support deletion of single jobs!')
            questionStr = sprintf(['Are you sure to remove recordo of job ' ...
                    '"%d", from oarparams.mat? [y/n]'], jobId);
            check = inputwdefault(questionStr, 'y');
            if strcmpi(check, 'y')
                fprintf('Removing information about the job... ')
                funcName = records.function;
                arrayName = sprintf('array%d', records.array_id);
                indx = [oarparams.(funcName).(arrayName).job(:).jobId] == jobId;
                oarparams.(funcName).(arrayName).job(indx) = [];
                fprintf('Done.\n')
            end
    end
end

function oarparams = gtBatchSubmit(oarparms, args)
    error(sprintf('%:not_implemented_yet', mfilename), ...
        'Submission is not implemented in gtBatch yet');
end

function hasFields = hasAllFields(structToCheck, fields)
    for n = 1:numel(fields)
        if (~isfield(structToCheck, fields{n}))
            hasFields = false;
            return
        end
    end
    hasFields = true;
end

function deleteOarArray(array)
% DELETEOARARRAY
% deleteOarArray(array)

    if (~ischar(array))
        array = num2str(array);
    end
    [~, message] = unix(['oardel --array ' array]);

    if (strfind(message, '[ERROR]'))
        error('gtBatch:OAR:error_while_deleting', message);
    end
end

function [records, selectionType] = getRecords(oarparams, args)
    possibleSelections = {'name', 'array', 'job'};
    switch(numel(args))
        case 0
            selectionType = 'name';
            selectionValue = 'gt*';
        case 1
            if (ischar(args{1}))
                if (ismember(args{1}, possibleSelections))
                    gtError('gtBatch:wrong_argument', 'Not enough arguments');
                else
                    selectionType = 'name';
                    selectionValue = args{1};
                end
            else
                selectionType = 'array';
                selectionValue = args{1};
            end
        case 2
            if ( (~ischar(args{1})) ...
                    || (ischar(args{1}) && ~ismember(lower(args{1}), possibleSelections)) )
                gtError('gtBatch:wrong_argument', 'First argument should be a string');
            end
            selectionType = lower(args{1});
            selectionValue = args{2};
        otherwise
            gtError('gtBatch:wrong_argument', 'Too many arguments');
    end

    switch(lower(selectionType))
        case 'name'
            records = getJobRecordsByName(oarparams, selectionValue);
        case 'array'
            records = getJobRecordsByArrayIDs(oarparams, selectionValue);
        case 'job'
            records = getJobRecordsByJobIDs(oarparams, selectionValue);
    end
end

function records = getJobRecordsByName(oarparams, funcRegexp)
    records = makeEmptyRecord();
    fnames = fieldnames(oarparams);
    % Queryname will be a regular expression we can use to match function names
    matches = regexp(fnames, funcRegexp, 'once');

    for funcNum = 1:numel(matches)
        if (~isempty(matches{funcNum}))
            funcName = fnames{funcNum};
            arrayIds = fieldnames(oarparams.(funcName));
            for arrayIdName = arrayIds'
                arrayId = str2double(arrayIdName{1}(6:end));
                arrayField = oarparams.(funcName).(arrayIdName{1});
                for n = 1:numel(arrayField.job)
                    job = arrayField.job(n);
                    records(end+1) = makeRecord(funcName, arrayId, job.jobId, ...
                        job.state, job.exit_code, job.submissionTime, ...
                        job.startTime, job.stopTime);
                end
            end
        end
    end
end

function records = getJobRecordsByArrayIDs(oarparams, arrayIds)
    records = makeEmptyRecord();
    fnames = fieldnames(oarparams);

    for funcNum = 1:numel(fnames)
        funcName = fnames{funcNum};
        funcArrayIds = fieldnames(oarparams.(funcName));
        for arrayIdName = funcArrayIds'
            arrayId = str2double(arrayIdName{1}(6:end));
            if (ismember(arrayId, arrayIds))
                arrayField = oarparams.(funcName).(arrayIdName{1});
                for n = 1:numel(arrayField.job)
                    job = arrayField.job(n);
                    records(end+1) = makeRecord(funcName, arrayId, job.jobId, ...
                        job.state, job.exit_code, job.submissionTime, ...
                        job.startTime, job.stopTime);
                end
            end
        end
    end
end

function records = getJobRecordsByJobIDs(oarparams, jobIds)
    records = makeEmptyRecord();
    fnames = fieldnames(oarparams);

    for funcNum = 1:numel(fnames)
        funcName = fnames{funcNum};
        arrayIds = fieldnames(oarparams.(funcName));
        for arrayIdName = arrayIds'
            arrayId = str2double(arrayIdName{1}(6:end));
            arrayField = oarparams.(funcName).(arrayIdName{1});
            for n = 1:numel(arrayField.job)
                job = arrayField.job(n);
                if (job.jobId == jobIds)
                    records(end+1) = makeRecord(funcName, arrayId, job.jobId, ...
                        job.state, job.exit_code, job.submissionTime, ...
                        job.startTime, job.stopTime);
                end
            end
        end
    end
end

function record = makeEmptyRecord()
    record = makeRecord({}, {}, {}, {}, {}, {}, {}, {});
end

function record = makeRecord(func, array, jobId, state, exitCode, submissionTime, startTime, stopTime)
    vars = getDisplayVars();
    vars = [{'jobId', 'function', 'array_id'} vars{:}];
    vars(2, :) = {jobId, func, array, state, exitCode, submissionTime, startTime, stopTime};
    record = struct(vars{:});
end

function vars = getQueryVars()
    vars = getDisplayVars();
    vars = [{'name', 'job_array_id'} vars{:}];
end

function vars = getDisplayVars()
    vars = {'state', 'exit_code', 'submissionTime', 'startTime', 'stopTime'};
end
