function deps = gtGetFunctionDeps(functionNames, deps, pattern, exclude)
% GTGETFUNCTIONDEPS  Returns dependecies of the given functions
%     deps = gtGetFunctionDeps(functionNames, deps, excludePattern)
%     -------------------------------------------------------------
%
%     INPUT:
%       functionNames  = 
%       deps           = 
%       excludePattern = 
%
%     OUTPUT:
%       deps = 
%
%     SUB-FUNCTIONS:
%[sub]- filterListing
%
%     Version 001.2 16-05-2012 by LNervo
%       Formatting and commenting
%

    if (ischar(functionNames))
        functionNames = {functionNames};
    end
    if (~exist('exclude', 'var'))
        exclude = true;
    end
    if (~exist('deps', 'var'))
        deps = functionNames;
    else
        deps = unique(union(deps, functionNames));
    end

    if (~exist('pattern', 'var'))
        pattern = {};
    elseif (ischar(pattern))
        pattern = {pattern};
    end

    for name = functionNames
        if (verLessThan('matlab', '8.6.0'))
            listing = depfun(name, '-toponly', '-quiet'); %#ok<DEPFUN>
            % New suggested alternative is damn slow!! (78 seconds in this
            % recursively filtered setting, otherwise 35 seconds, but still slow
            % if compared to the 1.4 seconds of depfun)
        else
            listing = matlab.codetools.requiredFilesAndProducts(name, 'toponly')';
        end
        listing = setdiff(listing, intersect(deps, listing));
        if (~isempty('pattern'))
            listing = filterListing(listing, pattern, exclude);
        end
        if (~isempty(listing))
            deps = gtGetFunctionDeps(listing, deps, pattern, exclude);
        end
    end
end

function listing = filterListing(listing, patternList, exclude)
    for patternInternal = patternList
        matches = regexp(listing, patternInternal);
        listing = listing(cellfun(@(x)isempty(x), matches) == exclude);
    end
end
