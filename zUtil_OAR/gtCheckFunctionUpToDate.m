function [isUpToDate, msg] = gtCheckFunctionUpToDate(funcName, outputBinary)
% GTCHECKFUNCTIONUPTODATE Checks if a compiled function is up to date.

    srcFileInfo = dir(funcName);
    if (isempty(srcFileInfo))
        error('CHECK:wrong_function_path', ...
              'The given function path: "%s" doesn''t exist', funcName);
    end

    binFileInfo = dir(outputBinary);
    if (~isempty(binFileInfo))
        if (srcFileInfo(1).datenum > binFileInfo(1).datenum)
            msg = sprintf(' + Binary is outdated.\n');
            isUpToDate = false;
        else
            matlabToolboxPattern = fullfile(matlabroot, 'toolbox');
            deps = gtGetFunctionDeps({funcName}, {}, matlabToolboxPattern);
            depsToRebuild = gtCheckFunctionsTimestamp(deps, binFileInfo(1).datenum);
            isUpToDate = isempty(depsToRebuild);

            if (~isUpToDate)
                msg = cellfun(@(x){sprintf(' + "%s" was modified after it.\n', x)}, ...
                        depsToRebuild);
                msg = [msg{:}];
            else
                msg = sprintf(' * Is up to date.\n');
            end
        end
    else
        msg = sprintf(' + Binary doesn''t exist.\n');
        isUpToDate = false;
    end
end
