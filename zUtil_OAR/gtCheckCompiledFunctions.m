function gtCheckCompiledFunctions()
    global GT_MATLAB_HOME

    disp('Checking compiled functions...');
    current_dir = pwd;
    cd(fullfile(GT_MATLAB_HOME, 'bin', 'scripts'));
    mFiles = compile(false, false);
    funcNames = fieldnames(mFiles);

    defaultColor = sprintf(gtGetANSIColour());
    yellowColor = sprintf(gtGetANSIColour('yellow'));

    for funcName = reshape(funcNames, [1 numel(funcNames)])
        func = mFiles.(funcName{1});

        mat_file_path = func.('in_mfile');
        comp_file_path = func.('out_file');
        [upToDate, msg] = gtCheckFunctionUpToDate(mat_file_path, comp_file_path);
        if (~upToDate)
            out = [yellowColor 'Function should be recompiled: ' ...
                defaultColor funcName{1}];
            disp(out);
            fprintf(msg);
        end
    end
    cd(current_dir);
    disp('Finished checking.');
end
