function script_location = gtOarMakeLaunchScript(pathdir, tmpdir, tmpname, oarpars)
    if (isempty(whos('global', 'GT_MATLAB_HOME')))
        error('COMPILE:no_such_variable', ...
              ['GT_MATLAB_HOME variable doesn''t exist in global workspace, '...
               'your environment is not sane, and you need to either ' ...
               're-initialise or re-run matlab.'])
    end
    global GT_MATLAB_HOME

    script_location = fullfile(pathdir, [oarpars.name '-' getenv('USER') '_' num2str(oarpars.resubmit) '.oar']);
    tmp_location    = fullfile(tmpdir, [tmpname '-' getenv('USER') '_' num2str(oarpars.resubmit) '.oar']);

    % matlabroot can be tricky on Macs
    exportPath   = 'export PATH="${PATH}:';
    exportLibary = 'export LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:';
    exportPreload = 'export LD_PRELOAD="${LD_PRELOAD}:';

    xml_conf = gtConfLoadXML(true);

    % External execution time libraries, same as:
%         ['source ' fullfile(GT_MATLAB_HOME, 'setup_gt_env')], ...
    try
        libraries_path = gtConfGetField(xml_conf, 'matlab.libraries.path');

        num_paths = numel(libraries_path);
        external_libs = cell(1, num_paths);
        is_cell = iscell(libraries_path);
        for n = 1:num_paths
            % We add them all, but only the ones that are really reached on
            % the machine will be used. Unfortunately this could
            % potentially create problems, even if it is unlikely
            if (is_cell)
                tmp_path = libraries_path{n};
            else
                tmp_path = libraries_path(n);
            end

            tmp_path = gtConfFilterAttribute(tmp_path, 'hostname');

            if (~isempty(tmp_path))
                external_libs{n} = [exportLibary tmp_path '"'];
            end
        end
    catch
        external_libs = {};
    end

    try
        preloads_path = gtConfGetField(xml_conf, 'matlab.preload.path');

        num_paths = numel(preloads_path);
        preload_libs = cell(1, num_paths);
        is_cell = iscell(preloads_path);
        for n = 1:num_paths
            % We add them all, but only the ones that are really reached on
            % the machine will be used. Unfortunately this could
            % potentially create problems, even if it is unlikely
            if (is_cell)
                tmp_path = preloads_path{n};
            else
                tmp_path = preloads_path(n);
            end

            tmp_path = gtConfFilterAttribute(tmp_path, 'hostname');

            if (~isempty(tmp_path))
                preload_libs{n} = [exportPreload tmp_path '"'];
            end
        end
    catch
        preload_libs = {};
    end

    if (oarpars.gpu)
        oarpars.queue = 'gpu';
    else
        oarpars.queue = 'nice';
    end

    node_string = sprintf('nodes=%d', oarpars.node);
    if (~isempty(oarpars.cpu))
        node_string = [node_string sprintf('/cpu=%d', oarpars.cpu)];
    end
    if (~isempty(oarpars.core))
        node_string = [node_string sprintf('/core=%d', oarpars.core)];
    end

    header =  {'#!/bin/bash', ...
        ['#OAR -l ' node_string ',walltime=' oarpars.walltime], ...
        ['#OAR -p (host like ''' oarpars.host ''') AND (desktop_computing = ''NO'')'] ...
        ['#OAR -p (mem_total_mb > ' num2str(oarpars.mem) ')'], ...
        ['#OAR -p (mem_core_mb >= ' num2str(oarpars.mem_core_mb) ')'], ...
        ['#OAR -q ' oarpars.queue], ...
        ['#OAR -O ' oarpars.stdout], ...
        ['#OAR -E ' oarpars.stderr], ...
        ['#OAR --checkpoint ' num2str(oarpars.checkpoint)], ...
        ['#OAR --signal ' num2str(oarpars.signal)], ...
        ['#OAR --type ' oarpars.type], ...
        ['#OAR --array-param-file ' fullfile(pathdir, [oarpars.name '-' getenv('USER') '_' num2str(oarpars.resubmit) '.params'])], ...
        ['#OAR --name ' oarpars.name], ...
        '#OAR --project graintracking' };
    libraries = [ external_libs, ...
        ... % On linux systems it is surely required, but on windows the path could be different
        [exportLibary fullfile(matlabroot, 'runtime', 'glnxa64') '"'], ...
        [exportLibary fullfile(matlabroot, 'bin', 'glnxa64') '"'], ...
        [exportLibary fullfile(matlabroot, 'sys', 'os', 'glnxa64') '"'], ...
        [exportLibary fullfile(matlabroot, 'sys', 'java', 'jre', 'glnxa64', 'jre', 'lib', 'amd64', 'server') '"'], ...
        ... % To make compiled functions visible
        [exportPath fullfile(GT_MATLAB_HOME, 'bin', 'compiled') '"'] ];
    script_content = [ header, '', libraries, '', preload_libs, '', '"$@"' ];
    script_content(2, :) = {sprintf('\n')};

    fid = fopen(script_location, 'w');
    fprintf(fid, '%s\n', [script_content{:}]);
    fclose(fid);

    fidtmp = fopen(tmp_location, 'w');
    fprintf(fidtmp, '%s\n', [script_content{:}]);
    fclose(fidtmp);
end
