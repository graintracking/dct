function [row,col,val] = findArrayIntoArray(toBeFound,whereToFind)
% FINDARRAYINTOARRAY  Finds array 'whereToFind' elements in array 'toBeFound'
%     [row,col,val] = findArrayIntoArray(toBeFound,whereToFind)
%     ---------------------------------------------------------
%     Inputs are transformed into columns. Uses find().
%
%     INPUT:
%       toBeFound   = vector or element to be found in 'whereToFind' <double>
%       whereToFind = vector or matrix where to find elements of 'toBeFound'
%                     <double>
%
%     OUTPUT:
%       row = row numbers of 'toBeFound' in 'whereToFind' <double>
%       col = column numbers of 'toBeFound' in 'whereToFind' <double>
%       val = value of 'whereTofFind(row,col)'
%
%     Version 002 04-09-2012 by LNervo

% trasform to columns
if size(whereToFind,1)==1 && size(whereToFind,2)>1
    whereToFind=whereToFind';
end
if size(toBeFound,1)==1 && size(toBeFound,2)>1
    toBeFound=toBeFound';
end

% find elements
for ii=1:size(toBeFound,1)
    [row,col,val]=find(whereToFind==toBeFound(ii,1));
end

clear ii

end % end of function
