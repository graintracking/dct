function [Structure, UpdatedList] = gtModifyStructure(Structure, List, Usertype, Header, Debug)
% GTMODIFYSTRUCTURE  GUI to modify a Structure - eg parameters.mat
%     Structure = gtModifyStructure(Structure, List, [Usertype], [Header], [Debug])
%     -----------------------------------------------------------------------------
%
%     INPUT:
%       Structure   = <struct>   structure to be modified
%       List        = <cell>     cell structure corresponding to Structure
%                                (names, descriptions, types, usertype) (N,4)
%       Usertype    = <cell>     type of user (0: not editable, 1: normal user, 2: expert user)
%                                This selects a set of fields as written in build_list_v2.m
%       Header      = <string>   figure header [optional]
%       Debug       = <logical>  print comments {false}
%
%     OUTPUT:
%       Structure   = <struct>   the structure updated of the fields selected by Usertype
%       UpdatedList = <cell>     the cell structure updated that corresponds to Structure
%
%     List is made of four columns:
%       - 1) list of field names is a cell array of strings:
%              list{1,1}='nproj', list{2,1}='name', etc...
%       - 2) captions to help the users are in a second column to the called array:
%              list{1,2}='how many projections?', list{2,2}='name of dataset?', etc...
%
%       - 3) class type (available: 'char','logical','single','double','int8','int16','int32','int64','uint8','uint16','uint32','uint64')
%              list{1,3}='double', list{2,3}='char', etc...
%
%       - 4) usertype:
%                0: administrator/developper for compouted fields and key fields
%                1: normal user for editable fields
%                2: expert user for extra fields
%            list{1,4}=1, list{2,4}=2, list{3,4}=0, etc...
%
%     You can also pass in a header string as a fourth argument, eg:
%       gtModifyStructure(parameters.acq, list.acq, 1, 'Modify acquistion parameters:')
%
%     What is displayed are a series of rows:
%       field name  ||  editable field value  || field class || {recommended} helpful explanation
%
%     It can handle - strings, booleans, single numbers, and vectors of numbers
%     n x 1 vectors are shown as 1 x n, but are repacked correctly
%     NB. it will skip fields which are sub-structures
%         it will skip matrices
%         il will skip cells
%
%
%     Version 007 02-08-2013 by LNervo
%       Added other numeric types, more flexible for choosing usertype (also
%       multiple choice permitted)

%
%     Version 006 29-07-2013 by LNervo
%       Used finally in gtSetup, gtPreprocessing... starting to use it in the DCT code
%
%     Version 005 19-06-2013 by LNervo
%       Added list(:,4) column and automatic check
%
%     Version 004 30-08-2012 by LNervo
%       Changed input list(:,1:3) including parameter class
%       Removed hard-coded text and replaced with regexp for walltime
%       Added third column to GUI with type
%
%     Version 003 16/08/2012 by AKing
%       Fixed a bug that caused an error when entering a walltime of less than one hour (i.e '00:xx:xx')
%
%     Version 002 09-11-2011 by LNervo
%       Add comments and help/doc syntax
%       Subfunctions: sfBuildFigure, sfCell2Struct, sfStruct2Cell
%
%     Version 001 29-04-2011 by AKing
%       Hopefully flexible so it can be incorporated into many things.

%[sub]-sfAddParameter
%[sub]-sfAddAllPsfPrintCellStructurearameter
%[sub]-sfConvertCell2List
%[sub]-sfCheckStructure
%[sub]-sfPrintCellStructure
%[sub]-sfUpdateCellStructure
%[sub]-sfCheckCellStructure
%[sub]-sfClearCellStructure
%[sub]-sfSetType
%[sub]-sfQuit
%[sub]-sfCell2Struct
%[sub]-sfBuildFigure
%[sub]-sfDefineFigure

if ~exist('Debug','var') || isempty(Debug)
    Debug = false;
end

output = GtConditionalOutput(Debug);

% Check inputs
if ~exist('List','var') || isempty(List)
    List = [];
    gtError('gtModifyStructure:missingArgument','Second argument is missing. Quitting...')
end
if ~exist('Usertype','var') || isempty(Usertype)
    Usertype = [];
end
if ~exist('Header','var') || isempty(Header)
    dim.headertext = '';
else
    dim.headertext = Header;
end

% Usertype can be 0 / 1 / 2 / -1 / -2 ......
% Negative values are to add fields
% To use more than one, use Usertype = {1,2}.
if size(List,2) > 3
    [ndx, ~] = findValueIntoCell(List(:,4), Usertype);
    if ~isempty(ndx)
        UsedList = List(ndx(:,1),:);
    else
        output.odisp('Usertype not found... Keeping all the fields')
        UsedList = List;
    end
    usertype = UsedList(:,4);
else
    output.odisp('Usertype not given... Keeping all the fields')
    usertype = [];
end

% unpack the columns as names, captions, types and usermod
list     = UsedList(:,1);
captions = UsedList(:,2);
for ii=1:size(captions,1)
    captions{ii} = char(captions{ii,1});
end
if size(captions,2) > 1
    tooltips = captions(:,2:end);
end
types    = UsedList(:,3);

% allowed data types (as written in build_list_v2.m)
def_types.('single')  = 1;
def_types.('double')  = 1;
def_types.('int8')    = 1;
def_types.('int16')   = 1;
def_types.('int32')   = 1;
def_types.('int64')   = 1;
def_types.('uint8')   = 1;
def_types.('uint16')  = 1;
def_types.('uint32')  = 1;
def_types.('uint64')  = 1;
def_types.('logical') = 2;
def_types.('char')    = 3;
def_types.('cell')    = 4;

% colors for text in GUI after checking fields
def_colors{1} = 'red';     % for numeric
def_colors{2} = 'green';   % for boolean
def_colors{3} = 'blue';    % for chars

dim.def_types         = def_types;
dim.def_colors        = def_colors;
dim.ftitle            = 'gtModifyStructure Version 003';

% not allowed are : struct, cell, function_handle
if ~isempty(Usertype) && any(Usertype < 0)
    % added fields
    % let the class to be chosen
    dim.editableClass = true;
    dim.showcheck     = 'on';
    dim.showButton    = 'off';
else
    dim.editableClass = false;
    dim.showcheck     = 'off';
    dim.showButton    = 'on';
end

% Check input Structure and do the conversion into cell
CellStructure = sfCheckStructure(Structure, list, captions, types, dim, output);

% required
quit = false;

% faff around setting the sizes of things for the GUI
dim = sfDefineFigure(list, captions, types, dim);

for ii=1:length(dim.firsts)

    [fh, CellStructure] = sfBuildFigure(CellStructure, dim.firsts(ii), dim.lasts(ii), Structure, dim, output);

    % wait until finished - thanks Greg
    while ~quit
        drawnow();
    end
    List = sfConvertCell2List(CellStructure, List, Usertype, dim.firsts(ii), dim.lasts(ii));
    
    % quit function
    close(fh);
    
    quit = false;
    assignin('caller', 'quit', quit)
end

if nargout > 1
    UpdatedList = List;
end

end % end function


%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Subfunction functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function sfAddParameter(hObject, eventdata, CellStructure, ii)
% sfAddParameter(hObject, eventdata, CellStructure, ii)

% get the class
if strcmp(get(hObject, 'Enable'), 'on')
    val = get(hObject, 'Value');
    if val == 1 || val==true
        CellStructure(ii).toadd = true;
    elseif val == 0 || val == false
        CellStructure(ii).toadd = false;
    end
end

assignin('caller', 'CellStructure', CellStructure)
quit = false;
assignin('caller', 'quit', quit)


end % end function sfAddParameter
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function sfAddAllParameter(hObject, eventdata, CellStructure)
% sfAddAllParameter(hObject, eventdata, CellStructure)

% get the class
if strcmp(get(hObject, 'Enable'), 'on')
    val = get(hObject, 'Value');
    
    obj = findobj(get(hObject, 'Parent'), 'Tag', 'check');
    set(obj, 'Value', val);
    
    for ii=1:length(CellStructure)
        CellStructure(ii).toadd = logical(val);
        CellStructure(ii).check = obj(ii);
    end
end

assignin('caller', 'CellStructure', CellStructure)
quit = false;
assignin('caller', 'quit', quit)


end % end function sfAddParameter
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function UpdatedList = sfConvertCell2List(CellStructure, List, Usertype, first, last)
% UpdatedList = sfConvertCell2List(CellStructure, List, Usertype, first, last)

UpdatedList = List;

for ii=first:last
    val = get(CellStructure(ii).check, 'Value');
    if strcmp(get(CellStructure(ii).check, 'Enable'), 'on')
        if val == 1
            CellStructure(ii).toadd = true;
        else
            CellStructure(ii).toadd = false;
        end
    end
    
    name = CellStructure(ii).name;
    ndx = findValueIntoCell(UpdatedList(:,1),  {name});
    if ~isempty(ndx)
        UpdatedList{ndx(:,1),1} = CellStructure(ii).name;
        UpdatedList{ndx(:,1),3} = CellStructure(ii).s_type;
    end
    if isfield(CellStructure(ii), 'toadd') && CellStructure(ii).toadd == 0
        ndx = findValueIntoCell(UpdatedList(:,1),  {name});
        if ~isempty(ndx)
            UpdatedList(ndx(:,1),:) = [];
        end
    end
end

%assignin('caller','CellStructure',CellStructure);

end % end function sfConvertCell2List
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function CellStructure = sfCheckStructure(Structure, list, captions, types, dim, output)
% CellStructure = sfCheckStructure(Structure, list, captions, types, dim, output)
%
% turn a Structure into a cell array of strings for easier handling
% read out the values, convert to strings, and record what they were to
% start with
% transpose vectors if neccessary
% remove fields which are themselves structures
% if list, do only those fields mentioned in list - reduce the size of the
% thingy.  Not case sensitive, but preserves fieldnames


nof_fields = size(list,1);
for ii=1:nof_fields
    if ~isfield(Structure, list{ii})
        Structure.(list{ii}) = [];
    end
end

% find max length
dim.names = list(:,1);
dim.nof_fields = size(list,1);

tmpnames = dim.names;
% check first dchar, then numbers then logical
ndx = findValueIntoCell(types,{'char'});
if isempty(ndx)
    ndx = findValueIntoCell(types,{'single','double',...
          'int8','int16','int32','int64',...
          'uint8','uint16','uint32','uint64'});
end
if ~isempty(ndx)
    ndx3 = findValueIntoCell(dim.names, list(ndx(:,1)));
    tmpnames = tmpnames(ndx3(:,1),:);
    dim.maxlength = max(cellfun(@(id) length(num2str(Structure.(id))),tmpnames));
else
    dim.maxlength = 2.5; % all logical values
end
clear ndx ndx2 ndx3 tmpnames

assignin('caller', 'dim', dim);

CellStructure = struct();
fnames = fieldnames(Structure);
count=0;
for ii=1:length(fnames)
    if isempty(list) || any(ismember(list, fnames{ii}))
      
        % which item from list?
        count=count+1;
        % get the field value
        val=Structure.(fnames{ii});

        % field name
        CellStructure(count).name       = fnames{ii};
        CellStructure(count).transposed = false; % default
        CellStructure(count).error      = false; % default
        CellStructure(count).color      = 'black'; % default
        CellStructure(count).curr_type  = class(val);
        CellStructure(count).val        = val;

        listndx=findValueIntoCell(list, fnames(ii));
        if ~isempty(listndx) && ~isempty(captions) && ~isempty(types)
            CellStructure(count).caption = captions{listndx(:,1)};
            CellStructure(count).s_type  = types{listndx(:,1)};
            if ~ismember(CellStructure(count).s_type, {'cell','struct'})
                CellStructure(count).type = dim.def_types.(CellStructure(count).s_type);
            else
                CellStructure(count).type = [];
            end
        else
            gtError(['The type for parameter number ' num2str(count) ' is empty!'])
        end
        if ismember(CellStructure(count).s_type, {'cell'}) && iscell(val) && all(cellfun(@ischar, val))
            % maybe a popupmenu?
            CellStructure(count).type = dim.def_types.(CellStructure(count).curr_type);
        else
            % skip this if it's a structure, or if it's a matrix
            if isstruct(val) || iscell(val) || (length(size(val))>1 && (size(val,1)>1 && size(val, 2)>1))
                continue;
            end

            % set up the type of the current parameters
            if ~isempty(val)
                if ~strcmpi(CellStructure(count).curr_type, CellStructure(count).s_type)
                    CellStructure(count).error = true;
                    CellStructure(count).color = dim.def_colors{CellStructure(count).type};
                end
                if isvector(val)
                    if size(val, 1) > 1
                        val=val';
                        CellStructure(count).transposed = true;
                    end
                    val=num2str(val);
                    % remove excess spaces
                    jj=1;
                    while jj<(length(val)-2)
                        if val(jj)==' ' && val(jj+1)==' ' && val(jj+2)==' '
                            val(jj)=[];
                        else
                            jj=jj+1;
                        end
                    end
                else
                    val=num2str(val);
                end
                if CellStructure(count).type == 1
                    CellStructure(count).val   = str2num(val);
                end
            else % if isempty(val)
                if CellStructure(count).type == 3
                    CellStructure(count).val   = '';
                elseif CellStructure(count).type == 1
                    CellStructure(count).val   = [];
                elseif CellStructure(count).type == 2
                    CellStructure(count).val   = false;
                end
            end
        end
        
    end % end if

end % end for fnames

errors = sfPrintCellStructure(CellStructure, output);

end % end function sfCheckStructure
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function errors = sfPrintCellStructure(CellStructure, output)
% errors = sfPrintCellStructure(CellStructure, output)

errors = arrayfun(@(num) CellStructure(num).error, 1:length(CellStructure));
errors = find(errors);

if ~isempty(errors)
    output.odisp(' ')
    output.odisp('Parameters to be checked:')
    arrayfun(@(num) output.odisp(CellStructure(num)), errors);
else
    output.odisp('Check structure: Done');
end

end % end function sfPrintCellStructure
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function sfUpdateCellStructure(CellStructure, ii)
% sfUpdateCellStructure(CellStructure, ii)
%
% Sets curr_type, type, curr_value, val

   % get the value
    if CellStructure(ii).type == 2
        CellStructure(ii).curr_value = get(CellStructure(ii).object, 'Value');
        CellStructure(ii).val = CellStructure(ii).curr_value;
    elseif CellStructure(ii).type == 4
        tmp = get(CellStructure(ii).object, 'String');
        val = get(CellStructure(ii).object, 'Value');
        CellStructure(ii).curr_value = tmp{val};
        CellStructure(ii).val = CellStructure(ii).curr_value;
    elseif CellStructure(ii).type == 3
        CellStructure(ii).curr_value = get(CellStructure(ii).object, 'String');
        CellStructure(ii).val = CellStructure(ii).curr_value;
    elseif CellStructure(ii).type == 1
        CellStructure(ii).curr_value = get(CellStructure(ii).object, 'String');
        CellStructure(ii).val = str2num(CellStructure(ii).curr_value);
        if CellStructure(ii).transposed
            CellStructure(ii).val=(CellStructure(ii).val)';
        end
    end
    CellStructure(ii).curr_type = class(CellStructure(ii).val);

assignin('caller', 'CellStructure', CellStructure);

end % end function sfUpdateCellStructure
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function CellStructure = sfCheckCellStructure(hObject, eventdata, CellStructure, first, last, dim, output)
% CellStructure = sfCheckCellStructure(hObject, eventdata, CellStructure, first, last, dim, output)
%
% set error, color

for ii=first:last
    
    sfSetType(CellStructure(ii).typeh, eventdata, CellStructure, ii, dim)
    sfUpdateCellStructure(CellStructure, ii);

    CellStructure(ii).error = false;

    if CellStructure(ii).type == 3
        if ~isnan(str2double(CellStructure(ii).curr_value))
            CellStructure(ii).error = true;
        end
    elseif CellStructure(ii).type == 1
        if isempty(CellStructure(ii).val)
             CellStructure(ii).error = true;
        elseif islogical(CellStructure(ii).val)
             CellStructure(ii).error = true;
        end
    end
    
    % set color
    CellStructure(ii).color = 'black';
    
    if isempty(CellStructure(ii).curr_value)
        CellStructure(ii).color = 'magenta';
    elseif CellStructure(ii).error
        CellStructure(ii).color = dim.def_colors{CellStructure(ii).type};
    end
    % set the color of label
    set(CellStructure(ii).label, 'ForegroundColor', CellStructure(ii).color);

end

errors = sfPrintCellStructure(CellStructure, output);

quit = false;
assignin('caller', 'quit', quit)
assignin('caller', 'CellStructure', CellStructure)

set(hObject','Visible','on')
drawnow();

end % end function sfCheckCellStructure
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function sfSetType(hObject, eventdata, CellStructure, ii, dim)
% sfSetType(hObject, eventdata, CellStructure, ii, dim)
%
% set object, s_type, type, typeh
% Called when user activates popup menu 
% double | logical | char | single | int8 | int16 | int32 | int64 | uint8 | uint16 | uint32 | uint64;

% get the class
    
val = get(hObject, 'Value');
string_list = get(hObject, 'String');
if size(string_list,1) > 1
    selected_string = string_list{val}; % Convert from cell array to string
else
    selected_string = string_list;
end

if ~strcmp(CellStructure(ii).s_type, selected_string)

    CellStructure(ii).s_type = selected_string;
    CellStructure(ii).type = dim.def_types.(CellStructure(ii).s_type);

    if val == 2
        set(CellStructure(ii).object, 'Style', 'checkbox', 'Value', false);
    elseif val == 3 && CellStructure(ii).type == 4
        set(CellStructure(ii).object, 'Style', 'popupmenu',...
            'String', CellStructure(ii).val,'Value',1);
    else
        set(CellStructure(ii).object, 'Style', 'edit', 'String', '');
    end
end

CellStructure(ii).typeh = hObject;

quit = false;
assignin('caller', 'CellStructure', CellStructure)
assignin('caller', 'quit', quit)
    
end % end function sfSetType
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function sfClearCellStructure(hObject, eventdata, CellStructure, dim)
% sfClearCellStructure(hObject, eventdata, CellStructure)
%
% reset transposed, error, color, val, curr_value

for ii=1:length(CellStructure)
  
    val = get(CellStructure(ii).typeh, 'Value');
    string_list = get(CellStructure(ii).typeh, 'String');
    if size(string_list,1) > 1
        selected_string = string_list{val}; % Convert from cell array to string
    else
        selected_string = string_list;
    end
    if ~strcmp(CellStructure(ii).s_type, selected_string)
        CellStructure(ii).s_type = selected_string;
        CellStructure(ii).type = dim.def_types.(CellStructure(ii).s_type);
    end
    CellStructure(ii).transposed = false; % default
    CellStructure(ii).error      = false; % default
    CellStructure(ii).color      = 'black'; % default
    CellStructure(ii).curr_value = ''; % default
    if CellStructure(ii).type == 3
        CellStructure(ii).val   = '';
        set(CellStructure(ii).object, 'String', '')
    elseif CellStructure(ii).type == 1
        CellStructure(ii).val   = [];
        set(CellStructure(ii).object, 'String', '')
    elseif CellStructure(ii).type == 2
        CellStructure(ii).val   = false;
        set(CellStructure(ii).object, 'Value', false)
    end
    set(CellStructure(ii).label, 'ForegroundColor', CellStructure(ii).color)
    
end

quit = false;
assignin('caller', 'CellStructure', CellStructure)
assignin('caller', 'quit', quit)
    
end % end function sfClearCellStructure
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function sfQuit(hObject, eventdata, Structure)
% sfQuit(hObject, eventdata, Structure)

quit = true;
assignin('caller', 'Structure', Structure)
assignin('caller', 'quit', quit)

end % end function sfQuit
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function sfCell2Struct(hObject, eventdata, Structure, CellStructure, first, last, dim)
% sfCell2Struct(hObject, eventdata, Structure, CellStructure, first, last, dim)
%
% reads the cell Structure back into the Structure variable, making sure that data types are right.

for ii = first:last
    
    sfSetType(CellStructure(ii).typeh, eventdata, CellStructure, ii, dim);

    sfUpdateCellStructure(CellStructure, ii);
  
    Structure.(CellStructure(ii).name) = CellStructure(ii).val;
    if CellStructure(ii).type == 1 % numeric fields
        if CellStructure(ii).transposed
            Structure.(CellStructure(ii).name) = CellStructure(ii).val';
        end
    elseif CellStructure(ii).type == 2 % boolean fields
        Structure.(CellStructure(ii).name) = logical(CellStructure(ii).val);
    end
    
    val = get(CellStructure(ii).check, 'Value');
    if strcmp(get(CellStructure(ii).check, 'Enable'), 'on')
        if val == 1
            CellStructure(ii).toadd = true;
        else
            CellStructure(ii).toadd = false;
        end
    end
    
    if isfield(CellStructure(ii), 'toadd') && CellStructure(ii).toadd == 0 ...
           && isfield(Structure, CellStructure(ii).name)
        Structure = rmfield(Structure, CellStructure(ii).name);
    end
    
end % end for ii

quit = true;
assignin('caller', 'Structure', Structure)
assignin('caller', 'CellStructure', CellStructure)
assignin('caller', 'quit', quit)
    
end % end function sfCell2Struct
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function [fh, CellStructure] = sfBuildFigure(CellStructure, first, last, Structure, dim, output)
% [fh, CellStructure]=sfBuildFigure(CellStructure, first, last, Structure, dim, output)

% first, what is the height of this figure?
dim.fheight = dim.vspacing*(last-first+1)+80+dim.headerspace;

% now can create the figure
fh = figure('Visible', 'off',...
            'Position', [(dim.ScreenSize(3)-dim.fwidth)/2 (dim.ScreenSize(4)-dim.fheight)/2 dim.fwidth dim.fheight],...
            'menubar', 'none',...
            'Name', dim.ftitle);
% building the GUI

% some text at the top
uicontrol(fh, ...
          'Style', 'text', ...
          'String', 'Edit the values, click "OK" when you are happy', ...
          'BackgroundColor', get(fh,'Color'),...
          'Position', [(dim.fwidth/2)-250 dim.fheight-dim.vspacing 500 dim.bheight])

% if wanted, add the header text
if ~isempty(dim.headertext)
    uicontrol(fh, ...
             'Style', 'text', ...
             'String', dim.headertext, ...
             'FontWeight','bold',...
             'BackgroundColor', get(fh,'Color'),...
             'Position', [dim.fwidth*0.15 dim.fheight-dim.vspacing-dim.headerspace dim.fwidth*0.7 dim.bheight])
end

setposy = (@(ii) dim.fheight-((ii-first+1)*dim.vspacing)-dim.vspacing-dim.headerspace);
  
% now add the lines of name / editable value / caption
for ii = first : last
    % position of objects

    dim.bposy = setposy(ii);
    dim.bheight = dim.bheight;
    
    % add the objects
    CellStructure(ii).check = uicontrol(fh, ...
        'Style', 'checkbox', ...
        'Value', 1, ...
        'Tag', 'check',...
        'Enable', dim.showcheck, ...
        'Visible', dim.showcheck, ...
        'BackgroundColor', get(fh,'Color'),...
        'Position', [dim.bposx, dim.bposy, dim.iwidth, dim.bheight],...
        'Callback', {@sfAddParameter, CellStructure, ii});

    % field name
    CellStructure(ii).label = uicontrol(fh, ...
        'Style', 'text', ...
        'String', CellStructure(ii).name, ...
        'Position', [dim.bposx+dim.iwidth+dim.bposx, dim.bposy, dim.lwidth, dim.bheight],...
        'ForegroundColor', CellStructure(ii).color);
      
    % editable string for the field value (or checkbox for boolean)
    if CellStructure(ii).type==2
        CellStructure(ii).object = uicontrol(fh, ...
            'Style', 'checkbox', ...
            'Value', CellStructure(ii).val,...
            'Position', [dim.bposx+dim.iwidth+dim.bposx+dim.lwidth+dim.bposx+(dim.bwidth/2)-10, dim.bposy, dim.iwidth, dim.bheight]);
    elseif CellStructure(ii).type == 4
        CellStructure(ii).object = uicontrol(fh, ...
            'Style', 'popupmenu', ...
            'String', CellStructure(ii).val,...
            'Value',1,...
            'Position', [dim.bposx+dim.iwidth+dim.bposx+dim.lwidth+10, dim.bposy, dim.bwidth, dim.bheight]);
    elseif ~isempty(CellStructure(ii).type)
      
        if ((CellStructure(ii).type == 1) && ~ischar(CellStructure(ii).val))
            % It is a double and is not converted, yet.
            val = num2str(CellStructure(ii).val);
        else
            % It is either a string or a double in string form
            val = CellStructure(ii).val;
        end
        CellStructure(ii).object = uicontrol(fh, ...
            'Style', 'edit', ...
            'String', val, ...
            'Position', [dim.bposx+dim.iwidth+dim.bposx+dim.lwidth+10, dim.bposy, dim.bwidth, dim.bheight]);
    end
    % add the helpful type/class by LNervo
    if (dim.editableClass)
        typeh_style = 'popupmenu';
        typeh_string = {'double', 'logical', 'char', 'single', 'int8', 'int16', 'int32', 'int64', 'uint8', 'uint16', 'uint32', 'uint64'};
    else
        typeh_style = 'text';
        typeh_string = CellStructure(ii).s_type;
    end
    CellStructure(ii).typeh = uicontrol(fh, ...
        'Style', typeh_style, ...
        'String', typeh_string, ...
        'Value', 1, ...
        'Position', [dim.bposx+dim.iwidth+dim.bposx+dim.lwidth+dim.bposx+dim.bwidth+dim.bposx, dim.bposy, dim.twidth, dim.bheight], ...
        'Callback', {@sfSetType, CellStructure, ii, dim});
    if size(CellStructure(ii).caption,1) > 1
        captionh_style = 'listbox';
    else
        captionh_style = 'text';
    end
    % caption / description
    CellStructure(ii).captionh = uicontrol(fh, ...
        'Style', captionh_style, ...
        'String', CellStructure(ii).caption, ...
        'Enable', 'inactive',...
        'HitTest', 'off',...
        'Selected', 'off',...
        'Position', [dim.bposx+dim.iwidth+dim.bposx+dim.lwidth+dim.bposx+dim.bwidth+dim.bposx+dim.twidth+dim.bposx, dim.bposy, dim.cwidth, dim.bheight]);
end % end for ii

% add Check and OK buttons at bottom of figure
allcheck = uicontrol(fh, ...
        'Style', 'checkbox', ...
        'Value', 1, ...
        'Enable', dim.showcheck, ...
        'Visible', dim.showcheck, ...
        'Position', [dim.bposx, setposy(0), dim.iwidth, dim.bheight],...
        'Callback', {@sfAddAllParameter, CellStructure});

clearbutton = uicontrol(fh, ...
    'Style', 'pushbutton', ...
    'String', 'Clear', ...
    'Position', [(dim.fwidth/2)-80-100 dim.fheight-((last-first+1)*30)-70-dim.headerspace 80 30],...
    'Callback', {@sfClearCellStructure, CellStructure, dim});

quitbutton = uicontrol(fh, ...
    'Style', 'pushbutton', ...
    'String', 'Quit', ...
    'Position', [(dim.fwidth/2)-80 dim.fheight-((last-first+1)*30)-70-dim.headerspace 80 30],...
    'Callback', {@sfQuit, Structure});

checkbutton = uicontrol(fh, ...
    'Style', 'pushbutton', ...
    'String', 'Check', ...
    'Visible', dim.showButton, ...
    'Position', [(dim.fwidth/2)+20 dim.fheight-((last-first+1)*30)-70-dim.headerspace 80 30],...
    'Callback', {@sfCheckCellStructure, CellStructure, first, last, dim, output});

okbutton = uicontrol(fh, ...
    'Style', 'pushbutton', ...
    'String', 'OK', ...
    'Position', [(dim.fwidth/2)+120 dim.fheight-((last-first+1)*30)-70-dim.headerspace 80 30],...
    'Callback', {@sfCell2Struct, Structure, CellStructure, first, last, dim});

output.fprintf(['\n     **********        Legend        **********\n' ...
           '     *                                        *\n' ...
           '     * red     : wrong type - must be double  *\n' ...
           '     * blue    : wrong type - must be char    *\n' ...
           '     * green   : wrong type - must be logical *\n' ...
           '     * magenta : empty field                  *\n'...
           '     ******************************************\n']);

% show GUI
set(fh, 'Visible', 'on')

end % end function sfBuildFigure
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function dim = sfDefineFigure(list, captions, types, dim)
% dim = sfDefineFigure(list, captions, types, dim)

dim.bposx           = 10;

if dim.maxlength==0 || isempty(dim.maxlength)
    dim.maxlength   = 5;
end
dim.namelength      = max(cellfun(@(id) max(length(id)),list));
dim.captionlength   = max(cellfun(@(id) max(length(id)),captions));
dim.typelength      = max(cellfun(@(id) max(length(id)),types));
dim.vallength       = dim.maxlength * 1.3;
% increase val length because sometimes variables get longer!

dim.ScreenSize      = get(0, 'ScreenSize');

% now open the figure
% how big do we need?
% horizontal size
dim.char_spacing    = 8;
dim.objectspacing   = 10;

% impose some sensible maximum, regardless of screensize
dim.namelength      = min(40, dim.namelength);
dim.vallength       = min(110, dim.vallength);
dim.captionlength   = min(75, dim.captionlength);
% Also impose some sensible minium
dim.namelength      = max(15, dim.namelength);
dim.vallength       = max(15, dim.vallength);
dim.captionlength   = max(15, dim.captionlength);
dim.typelength      = 10;
dim.checklength     = 2.5;

% what figure width would this imply?
dim.fwidth          = (dim.checklength + ...
                       dim.namelength + ...
                       dim.vallength + ...
                       dim.captionlength + ...
                       dim.typelength)*dim.char_spacing...
                      + (6*dim.objectspacing);
% does this fit on the screen?
if dim.fwidth < (dim.ScreenSize(3)*0.9);
    dim.iwidth      = dim.checklength*dim.char_spacing;
    dim.lwidth      = dim.namelength*dim.char_spacing;
    dim.bwidth      = dim.vallength*dim.char_spacing;
    dim.cwidth      = dim.captionlength*dim.char_spacing;
    dim.twidth      = dim.typelength*dim.char_spacing;
else
    dim.fwidth      = dim.ScreenSize(3)*0.9;
    dim.iwidth      = dim.checklength*dim.char_spacing;
    dim.lwidth      = dim.namelength*dim.char_spacing;
    dim.bwidth      = dim.vallength*dim.char_spacing;
    % crop the caption
    dim.cwidth      = dim.fwidth-dim.lwidth-dim.bwidth-40;
    dim.twidth      = dim.typelength*dim.char_spacing;    
end

% do we have some header text?
if ~isempty(dim.headertext)
    dim.headertext  = dim.headertext;
    dim.headerspace = 30; % one row
else
    dim.headertext  = [];
    dim.headerspace = 0;
end

% objects are 20 pixels high in 30 pixel space
dim.vspacing        = 30;
dim.bheight         = 20;
dim.captionheight   = cellfun(@(id) size(id,1),captions);

% how many rows can we fit in? 30 pixel for each line, plus space for button
dim.maxfields       = floor((dim.ScreenSize(4)-300-dim.headerspace)/dim.vspacing);
% if we don't have space, split into several sequential figures
if dim.maxfields < dim.nof_fields
    dim.firsts      = 1:dim.maxfields:dim.nof_fields;
    dim.lasts       = dim.firsts+dim.maxfields-1;
    dim.lasts(end)  = dim.nof_fields;
else
    dim.firsts      = 1;
    dim.lasts       = dim.nof_fields;
end

end % end of sfDefineFigure
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
