function [allvalues, ids] = gtGetSinglesValues(cellArray, selected)
% GTGETSINGLEVALUES  Operates on cellArray to get values as column vector
%     [allvalues, ids] = gtGetSinglesValues(cellArray, selected)
%     ----------------------------------------------------------
%
%     The empty element are skipped.
%
%     INPUT:
%       cellArray = cell array as input <cell>
%       selected  = indexes to be taken into account <double>
%
%     OUTPUT:
%       allvalues = column vector as output 
%       ids       = indexes as column vector <double>
%
%     USAGE: cellArray = {1,2,3,4,5};
%            selected = [2 3];
%            [allvalues, ids] = gtGetSinglesValues(cellArray, selected);
%            allvalues =
%                 2
%                 3
%            ids =
%                 2
%                 3
%
%
%            cellArray = {0,4,[],3,5}; (empty entry)
%            selected = [2 3];
%            [allvalues, ids] = gtGetSinglesValues(cellArray, selected)
%            allvalues =
%                 4
%            ids =
%                 2
%
%
%            cellArray = {[1 -1],[2 3 5],[]}; (empty entry and vector as
%                                             element)
%            selected = [2 3];
%            [allvalues, ids] = gtGetSinglesValues(cellArray, selected)
%            allvalues =
%                 2
%                 3
%                 5
%            ids =
%                 2
%                 2
%                 2
%
%
%            confl = {{'2', '-1'},[2 3 5],[]}; (empty entry and vectors as
%                                             element: includes strings)
%            selected = [1 3];
%            [allvalues, ids] = gtGetSinglesValues(cellArray, selected)
%            allvalues =
%                '2'
%                '-1'
%            ids =
%                 1
%                 1
%
%
%     Version 001 03-12-2012 by LNervo


allvalues = [];
ids       = [];

if ~isempty(selected)
    cellArray = arrayfun(@(num) cellArray{num}, selected, 'UniformOutput', false);
else
    selected = 1:length(cellArray); 
end

for ii = 1 : length(cellArray)
    array = cellArray{ii};
    if ~isempty(array)
        for jj = 1 : length(array)
            current = array(jj);
            if ~isempty(current)
                ids = [ids; selected(ii)];
                allvalues = [allvalues; current];
            end
        end
    end
end

end % end of function
