function sublist = gtSubList(list, varargin)
% GTSUBLIST  Creates sub-list keeping only varargin content
%     sublist = gtSubList(list, varargin)
%     -----------------------------------
%
%     Usage: gtSubList(list.acq, 'name', 'dir', energy')

[ndx, sublist] = findValueIntoCell(list, varargin);
if isempty(ndx) || length(ndx) ~= length(varargin)
   disp('problem with some input to gtSubList')
   check = ismember(varargin, list(:,1));
   disp('Following field name not found in main list:')
   disp(varargin{~check})
end

end % end of function
