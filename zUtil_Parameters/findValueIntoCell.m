function [index, outlist, not_inlist] = findValueIntoCell(inlist, values)
% FINDVALUEINTOCELL  Finds value(s) into a cell structure
%     [index, outlist, not_inlist] = findValueIntoCell(inlist, values)
%     ----------------------------------------------------------------
%     INPUT:
%       inlist  = <cell>       list where to look at
%       values  = <cell>       what to search (each row must correspond to each
%                              value
%
%     OUTPUT:
%       index   = <cell>       indexes of values in inlist (rows, colums)
%       outlist = inlist(index,:) <cell>
%       not_inlist
%
%     Version 003 15-03-2013 by LNervo


if ~iscell(values)
    if isvector(values)
        values = arrayfun(@(num) {num}, values);
    else
        values = {values};
    end
end

ind = {};
notFound = {};
isFound = false;
for ii=1:length(values)
    isFound = false;
    for jj=1:size(inlist,1)
        for kk=1:size(inlist,2)
            if all(isequal(inlist{jj,kk}, values{ii}))
                ind{end+1} = [jj, kk];
                isFound = true;
            end
        end
    end
    if ~isFound
        notFound(end+1) = values(ii);
    end
end

index = reshape([ind{:}],2,length(ind))';

if nargout > 1
    outlist = inlist(index(:,1),:);
    if nargout > 2
        not_inlist = notFound;
    end
end


end % end of function
