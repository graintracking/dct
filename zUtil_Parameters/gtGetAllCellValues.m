function val = gtGetAllCellValues(cells, field1, field2, ind1, ind2, cell2mat)
% GTGETALLCELLVALUES  Gets all values from cell structures
%     val = gtGetAllCellValues(cells, field1, [field2], [ind1], [ind2], [cell2mat])
%     -----------------------------------------------------------------------------
%     Returns a column vector containing the values with indices (ind1,ind2) in
%     cell.field1.field2 from all the grains in 'grain'.
%
%     INPUT:
%       cells    = <cell array>     all cells data <cell> (1xN)
%       field1   = <string>         field 1 name
%       field2   = <string>         field 2 name or empty if not applicable {''}
%       ind1     = <double>         row index 1 of variable (1x1) {[]}
%       ind2     = <double>         column index 2 of variable or empty if not
%                                   applicable (1xN) {[]}
%       cell2mat = <logical>        false to keep output as cell array, true to
%                                   convert into a matrix {true} | false
%
%     OUTPUT:
%       val      = cell column array; size(m,n) where m is the number of cells;
%                  each element has the format as the original cell data,
%                  if ind1 and ind2 are empties
%
%     EXAMPLES:
%       gtGetAllCellValues(grain, 'nof_pairs', [], 1, [])
%       gtGetAllCellValues(grain, 'R_vector', [], 1, 1:3)
%       gtGetAllCellValues(grain, 'indST', [], 3, 1:3)
%       gtGetAllCellValues(grain, 'stat', 'dangmean', 1, [])
%       gtGetAllCellValues(grain, 'fsim', [], 1, [])    % takes the struct fsim(1)
%       gtGetAllCellValues(grain, 'fsim', 'uvw', 1, []) % takes 'uvw'
%                                                       % variable from 'fsim(1)'
%
%       twinInfo = gtCrystDetectTwins({rvectors t_rvectors}, triple_cases, 1, 1);
%       gtGetAllCellValues(twinInfo, 'sigmaAnd',[],[],[],false)
%
%     Version 001 14-11-2013 by LNervo


nof_cells = length(cells);
orig_size = size(cells);

if ~all(cellfun(@(num) isfield(num, field1), cells))
    gtError('gtGetAllCellValues:missing_field','Field1 is missing from some cells...Quitting')
end

if ~exist('field2','var')
    field2 = '';
end
if ~exist('ind1','var')
    ind1 = [];
end
if ~exist('ind2','var')
    ind2 = [];
end
if ~exist('cell2mat','var') || isempty(cell2mat)
    cell2mat = true;
end
  
if isstruct(cells{1}.(field1))
    if ~isempty(ind1) && length(cells{1}.(field1)) >= ind1 && size(ind2,2) ~= 2
        tmp = cellfun(@(num) num.(field1)(ind1), cells, 'UniformOutput', false); %was transpose
    else
        tmp = cellfun(@(num) num.(field1), cells, 'UniformOutput', false); %was transpose
    end
    cells = tmp;
    if size(ind2,2) == 2
        ind1 = ind2(1);
        ind2 = ind2(2);
    else
        ind1 = ind2;
        ind2 = [];
    end

    field1 = field2;
    field2 = [];
    clear tmp
    if length(cells{1}) > 1
        cell2mat = false;
    end
    if isempty(ind1) && isempty(field1)
        val = cells;
        return
    end
    
    if ~any(cellfun(@(num) isfield(num, field1), cells))
        gtError('gtGetAllCellValues:missing_field','Field2 is missing from some cells...Quitting')
    end

end %isstruct(cell{1}.(field1))

if isempty(ind1) && isempty(ind2)
    form = 0;   % all values will be concatenated
elseif isempty(ind2)
    form = 1;   % parameter is a one dimensional vector
else
    form = 2;   % parameter is a two dimensional array
end


% If there is no second field 
if isempty(field2)
    
    if (form == 2)
        
        if islogical(cells{1}.(field1))
            val = false(nof_cells, length(ind2));          
        elseif isstruct(cells{1}.(field1))
            val = struct([]);
        elseif iscell(cells{1}.(field1))
            val = cell(nof_cells, length(ind2));
        else
            val = zeros(nof_cells, length(ind2), class(cells{1}.(field1)));
        end
        val = cellfun(@(num) num.(field1)(ind1,ind2), cells, 'UniformOutput', false); %was transpose

    elseif (form == 1)

        if islogical(cells{1}.(field1))
            val = false(nof_cells, 1);
        elseif isstruct(cells{1}.(field1))
            val = struct([]);
        elseif iscell(cells{1}.(field1))
            val = cell(nof_cells, 1);
        else
            val = zeros(nof_cells, 1, class(cells{1}.(field1)));
        end
        val = cellfun(@(num) num.(field1)(ind1), cells, 'UniformOutput', false); %was transpose

    else
        %if length(cells{1}) > 1
        %    val = cellfun(@(num) arrayfun(@(num2) num(num2).(field1), 1:length(num), 'UniformOutput', false), cells, 'UniformOutput', false)';
        %else
            val = cellfun(@(num) num.(field1), cells, 'UniformOutput', false); %was transpose
        %end
    end

% If there is a second field
else

    if ~any(cellfun(@(num) isfield(num.(field1), field2), cells))
        gtError('gtGetAllCellValues:missing_field','Field2 is missing from some cells...Quitting')
    end

    if (form == 2)

        if islogical(cells{1}.(field1).(field2))
            val = false(nof_cells, length(ind2));          
        elseif isstruct(cells{1}.(field1))
            val = struct([]);
        elseif iscell(cells{1}.(field1))
            val = cell(nof_cells, length(ind2));
        else
            val = zeros(nof_cells, length(ind2), class(cells{1}.(field1).(field2)));
        end
        val = cellfun(@(num) num.(field1).(field2)(ind1,ind2), cells, 'UniformOutput', false); %was transpose

    elseif (form == 1)

        if islogical(cells{1}.(field1).(field2))
            val = false(nof_cells, 1);
        elseif isstruct(cells{1}.(field1))
            val = struct([]);
        elseif iscell(cells{1}.(field1))
            val = cell(nof_cells, 1);
        else
            val = zeros(nof_cells, 1, class(cells{1}.(field1).(field2)));
        end
        val = cellfun(@(num) num.(field1).(field2)(ind1), cells, 'UniformOutput', false); %was transpose

    else

        val = cellfun(@(num) num.(field1).(field2), cells, 'UniformOutput', false); %was transpose

    end

end %if isempty(field2)


if (cell2mat)
    dim1 = cellfun(@(num) size(num,1), val);
    dim2 = cellfun(@(num) size(num,2), val);
    if isequaln(dim2, repmat(dim2(1),1,length(val))) || isequaln(dim2, repmat(dim2(1),length(val),1))
        if isequaln(dim1, repmat(dim1(1),1,length(val))) || isequaln(dim1, repmat(dim1(1),length(val),1))
            val = reshape([val{:}],size(val{1},2),length(val))';
        else
            val = reshape([val{:}],size(val{1},2),[])';
        end

    elseif isequaln(dim1, repmat(dim1(1),1,length(val))) && ~isequaln(dim1, repmat(dim1(1),length(val),1))
        val = reshape([val{:}],[],size(val{1},1))';
    else
        val = reshape([val{:}], orig_size(1),orig_size(2));
    end
else
    val = reshape(val,orig_size(1),orig_size(2));
end % if cell2mat

end  % end of function
