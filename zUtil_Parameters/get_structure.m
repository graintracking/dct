function [field,liststruct] = get_structure(par,substr,saveflag,namelist,list)
% GET_STRUCTURE  Gets the field list for each sub structure of par
%     [field,liststruct] = get_structure(par,substr,[saveflag],[namelist],[list])
%     ---------------------------------------------------------------------------
%     Returns automatically the list cell-structure of 'par' and prints
%     the syntax in a text file to be used for building the parameters
%     afterwards.
%
%     INPUT:
%       par        = <struct>     structure to be analysed
%       substr     = <string>     'par' sub-structure name
%                                 i.e. par = parameters.acq  --> substr = 'acq'
%       saveflag   = <logical>    flag to save for each 'struct' field of 'par' into a file
%                                 "[datasetname]_list_[namelist].txt" {false}
%       namelist   = <string>     name of the current 'par' sub-structure {<substr>}
%       list       = <cell>       pre-defined list of {'fieldname','description','class',usertype}
%                                 for each field (N,4)
%
%     OUTPUT:
%       field      = <cell>     built list of {'name','description','class',usertype}
%                               for each field (M,4)
%                               PS. each substructure name is saved separately. 
%                               i.e. parameters.index.strategy 
%                                     ---> list.index__strategy 
%                                    parameters.index.strategy.b
%                                     ---> list.index__strategy__b
%       liststruct = <cell>     sub-structures info found in 'par' structure
%                               liststruct{k}.name      <string>
%                               liststruct{k}.subnames  <cell>
%                               liststruct{k}.nfields   <double>
%                               liststruct{k}.substruct <logical>
%
%     Usage:
%       [list.acq, tmp] = get_structure(parameters.acq,'acq',true,'acq',list);
%
%
%     Version 002 05-09-2013 by LNervo
%       Commented and better formatted


list_struct = [];
field       = [];

if ~exist('namelist','var') || isempty(namelist)
    namelist = substr;
end
if ~exist('list','var')
    list = []; 
end
if ~exist('saveflag','var') || isempty(saveflag)
    saveflag = false;
end

if ~isstruct(par)
    disp('Input parameters must be a structure...Quitting')
    if nargout > 1
        liststruct = list_struct;
    end
    return
end
names = fieldnames(par);
if ~isempty(namelist) && ~isempty(list) && isfield(list, namelist)
    list = list.(namelist);
end
if ~isempty(list)
    [~, names] = findStringIntoCell(names, list, 1);
end

kk = 0;
for ii=1:length(names)
    if length(par) > 1
        par = par(1);
    end
    if ~isempty(substr)
        if ismember(names{ii},list(:,1))
            ind = findStringIntoCell(list,names(ii),1);
            field.(substr){ii,1} = list{ind,1};
            field.(substr){ii,2} = list{ind,2};
            if ~isempty(list{ind,3})
                field.(substr){ii,3} = list{ind,3};
            else
                field.(substr){ii,3} = class(par.(names{ii}));  
            end
            if ~isempty(list{ind,4})
                field.(substr){ii,4} = list{ind,4};
            else
                field.(substr){ii,4} = 1;
            end
        else
            field.(substr){ii,1} = names{ii};
            field.(substr){ii,2} = '(?)';
            field.(substr){ii,3} = class(par.(names{ii}));
            field.(substr){ii,4} = 1;
        end
    else
        field{ii,1} = names{ii};
        field{ii,2} = '(?)';
        field{ii,3} = class(par.(names{ii}));
        field{ii,4} = 1;
    end
    
    if isstruct(par.(names{ii}))
        disp(['Found structure: ' names{ii}])
        name     = names{ii};
        subnames = fieldnames(par.(names{ii}));
        nof      = length(subnames);
        is_in=false;
        for jj=1:length(subnames)
            tmpsub=par.(names{ii}).(subnames{jj});
            if isstruct(tmpsub)
                is_in=true;
            end
        end
        substruct=is_in;
        kk=kk+1;
        list_struct{kk}.name      = name;
        list_struct{kk}.subnames  = subnames;
        list_struct{kk}.nfields   = nof;
        list_struct{kk}.substruct = substruct;
        list_struct{kk}.num       = length(par);
    end
end

if (saveflag)
    
    cdir=pwd;
    ind=strfind(cdir,'/');
    dataset=cdir(ind(end)+1:end);

    if ~isempty(namelist)
        sublist=['list.' namelist];
        basename=[dataset '_list_'];
        filename=[basename namelist '.txt'];
    else
        sublist='sublist';
        basename=[dataset '_list_'];
        filename=[basename '.txt'];
    end
    
    fid=fopen(filename,'w');
    for ii=1:length(names)
        if ~isempty(substr)
        
            if ~isstruct(par.(names{ii}))
                left='%s{end+1,%d} = %s;';
                column1=sprintf(left,sublist,1,['''' char(field.(substr){ii,1}) '''']);
                if size(field.(substr){ii,2},1) > 1
                    column2=sprintf(left,sublist,2,'''[Add description]''');
                else
                    column2=sprintf(left,sublist,2,['''' char(field.(substr){ii,2}) '''']);
                end
                column3=sprintf(left,sublist,3,['''' char(field.(substr){ii,3}) '''']);
                column4=sprintf(left,sublist,4,num2str(field.(substr){ii,4}));

                fprintf(fid,'%s\n%s\n%s\n%s\n',column1,column2,column3,column4);
            end
        
        else
            if ~isstruct(par.(names{ii}))
                left='%s{end+1,%d} = %s;';
                column1=sprintf(left,sublist,1,['''' char(field{ii,1}) '''']);
                if size(field{ii,2},1) > 1
                    column2=sprintf(left,sublist,2,'''[Add description]''');
                else
                    column2=sprintf(left,sublist,2,['''' char(field{ii,2}) '''']);
                end
                column3=sprintf(left,sublist,3,['''' char(field{ii,3}) '''']);
                column4=sprintf(left,sublist,4,num2str(field{ii,4}));

                fprintf(fid,'%s\n%s\n%s\n%s\n',column1,column2,column3,column4);
            end
        end
    end
    disp(['Save list_template in ' filename '.']);
    fclose(fid);
end

if nargout > 0
    if ~isempty(substr)
        field = field.(substr);
    end
    if nargout > 1
        liststruct = list_struct;
    end
end    

end % end of function
