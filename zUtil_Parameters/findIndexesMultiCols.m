function [index, outlist] = findIndexesMultiCols(inlist, values, toll, debug)
% FINDINDEXESMULTICOLS  Finds values into a list with a tollerance
%     [index, outlist] = findIndexesMultiCols(inlist, values, toll, debug)
%     ----------------------------------------------------------------
%     Finds value in inlist using toll as tolerances (can be specified for each column)
%
%     INPUT:
%       inlist = <double>   can be a matrix
%       value  = <double>   can be a vector
%       toll   = <double>   can be a vector {0.000001}
%       debug  = <logical>  print on screen {false}
%
%     OUTPUT:
%       index   = indexes of values in inlist <double>
%       outlist = inlist(index, :) <double>
%
% This functions is awful but I refuse to fix it!

    if (~exist('toll', 'var') || isempty(toll))
        toll = 0.000001;
    end
    if (~exist('debug', 'var') || isempty(debug))
        debug = false;
    end
    out = GtConditionalOutput(debug);

    index = [];
    for ii = 1:size(values, 1)
        switch (size(toll, 2))
            case 1
                out.odisp(['x < ' num2str(values(ii, 1)+toll(1)) ' & x > ' num2str(values(ii, 1)-toll(1))])
                index = [index; find( ...
                    inlist(:, 1) < values(ii, 1) + toll(1) & inlist(:, 1) > values(ii, 1) - toll(1))];
            case 2
                out.odisp(['x < ' num2str(values(ii, 1)+toll(1)) ' & x > ' num2str(values(ii, 1)-toll(1)) ...
                      ' & y < ' num2str(values(ii, 2)+toll(2)) ' & y > ' num2str(values(ii, 2)-toll(2))])
                index = [index; find( ...
                    inlist(:, 1) < values(ii, 1) + toll(1) & inlist(:, 1) > values(ii, 1) - toll(1) & ...
                    inlist(:, 2) < values(ii, 2) + toll(2) & inlist(:, 2) > values(ii, 2) - toll(2)) ];
            case 3
                out.odisp(['x < ' num2str(values(ii, 1)+toll(1)) ' & x > ' num2str(values(ii, 1)-toll(1)) ...
                      ' & y < ' num2str(values(ii, 2)+toll(2)) ' & y > ' num2str(values(ii, 2)-toll(2)) ...
                      ' & z < ' num2str(values(ii, 3)+toll(3)) ' & z > ' num2str(values(ii, 3)-toll(3))])
                index = [index; find( ...
                    inlist(:, 1) < values(ii, 1) + toll(1) & inlist(:, 1) > values(ii, 1) - toll(1) & ...
                    inlist(:, 2) < values(ii, 2) + toll(2) & inlist(:, 2) > values(ii, 2) - toll(2) & ...
                    inlist(:, 3) < values(ii, 3) + toll(3) & inlist(:, 3) > values(ii, 3) - toll(3)) ];
        end
    end

    if (nargout == 2)
        outlist = inlist(index, :);
    end
end

