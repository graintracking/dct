function geom = gtGetGeometry(parameters, geometry, isfed)
%% FUNCTION GTGETGEOMETRY Get geometry from parameters
%  geo = gtGetGeometry(parameters, geometry, isfed)
%
%    isfed = flag for fed simulation analysis <logical> {0}
%          This removes the 0 from the geometry parameters 
%          (detdiru0 -> detdiru)
%
%  Version 002 21-09-2011 LNervo


if ~exist('parameters','var') || isempty(parameters) 
    parameters=[];
    load('parameters.mat');
end
if ~exist('geometry','var') || isempty(geometry)
    geometry='geo';
end
if ~exist('isfed','var') || isempty(isfed)
    isfed=false;
end

g{1} = 'detpos0';
g{2} = 'detdiru0';
g{3} = 'detdirv0';
g{4} = 'beamdir0';
g{5} = 'rotdir0';
g{6} = 'detucen0';
g{7} = 'detvcen0';
g{8}  = 'detdir0';
    
if ~isfed
    g{9}  = 'detanglemin';
    g{10} = 'detanglemax';
end


if isfield(parameters,geometry)
    geo=parameters.(geometry);
else
    geo=parameters;
end

if strcmpi(geometry,'opt')
    geom = geo;
    return;
end
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% save the current geometry
for i=1:length(g) 
    var=char(g(i));
    if isfed
        var=var(1:end-1);
    end
    getgeo.(var)=geo.(char(g(i)));
end

geom = getgeo;


end