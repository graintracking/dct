function complete = complete_structure(parameters,list,saveflag,sep)
% COMPLETE_STRUCTURE  Gets the full field list for each sub structure of
%                     parameters
%     complete = complete_structure(parameters,list,saveflag,sep)
%     -----------------------------------------------------------
%     Returns automatically the list cell-structure of parameters and prints
%     the syntax in a text file to be used for building the parameters
%     afterwards.
%
%     INPUT:
%       parameters = .mat parameters file from which to take the structure
%       list       = pre-defined list of {'fieldname','description','class','usertype'}
%                    for each field (can be optained running build_list_v2.m)
%                    PS. list and parameters have to be the same structure
%       saveflag   = flag to save on disk {false}
%       sep        = separator for sub-structures in list struct {'__'}
%                    i.e. list.index---strategy --> sep = '---' if 'strategy' is a
%                         sub-structure of 'index'.
%                         PS. list.match_calib --> 'match_calib' is a field of
%                             'list'. 'calib' is a sub-structure of 'match'.
%
%     OUTPUT:
%       complete = structure "list" template from 'parameters' structure
%

if ~exist('list','var') || isempty(list)
    nolist = true;
else
    nolist = false;
end
if ~exist('saveflag','var') || isempty(saveflag)
    saveflag = false;
end
if ~exist('sep','var') || isempty(sep)
    sep = '__';
end

if (saveflag)
    disp('Saving txt files with structure list of parameters.mat')
end
if ~isstruct(parameters)
    complete = [];
    gtError('complete_structure:wrong_input','parameters should be a structure...Quitting')
end
% take parameters field names
names = fieldnames(parameters);

tmp_struct = [];
for ii = 1:length(names)
    % level 0
    tmp_field = names{ii};
    foutput = strrep(tmp_field,sep,'.');
    if ( nolist && isstruct(parameters.(names{ii})) )
        subnames = fieldnames(parameters.(names{ii}));
        list.(tmp_field) = cell(size(subnames,1),4);
        list.(tmp_field)(:,1) = subnames;
    end
    [tmp_struct.(tmp_field),tmplist_struct] = get_structure(parameters.(names{ii}),names{ii},saveflag,tmp_field,list.(tmp_field));
    if ~isempty(tmplist_struct)
        tmptmp1 = tmplist_struct;
        for jj = 1:length(tmptmp1)
            tmpname1 = tmptmp1{jj}.name;
            tmp_field = [names{ii} sep tmpname1];
            foutput = strrep(tmp_field,sep,'.');
            if ( nolist && isstruct(parameters.(names{ii}).(tmpname1)) )
                subnames = fieldnames(parameters.(names{ii}).(tmpname1));
                list.(tmp_field) = cell(size(subnames,1),4);
                list.(tmp_field)(:,1) = subnames;
            end
            % level 1
            [tmp_struct.(tmp_field),tmplist_struct] = get_structure(parameters.(names{ii}).(tmpname1),tmpname1,saveflag,tmp_field,list.(tmp_field));
            if ~isempty(tmplist_struct)
                tmptmp2 = tmplist_struct;
                for kk = 1:length(tmptmp2)
                    tmpname2 = tmptmp2{kk}.name;
                    tmp_field = [names{ii} sep tmpname1 sep tmpname2];
                    foutput = strrep(tmp_field,sep,'.');
                    if ( nolist && isstruct(parameters.(names{ii}).(tmpname1).(tmpname2)) )
                        subnames = fieldnames(parameters.(names{ii}).(tmpname1).(tmpname2));
                        list.(tmp_field) = cell(size(subnames,1),4);
                        list.(tmp_field)(:,1) = subnames;
                    end
                    % level 2
                    [tmp_struct.(tmp_field),tmplist_struct] = get_structure(parameters.(names{ii}).(tmpname1).(tmpname2),tmpname2,saveflag,tmp_field,list.(tmp_field));
                    if ~isempty(tmplist_struct)
                        tmptmp3 = tmplist_struct;
                        for ll = 1:length(tmptmp3)
                            tmpname3 = tmptmp3{ll}.name;
                            tmp_field = [names{ii} sep tmpname1 sep tmpname2 sep tmpname3];
                            foutput = strrep(tmp_field,sep,'.');
                            if ( nolist && isstruct(parameters.(names{ii}).(tmpname1).(tmpname2).(tmpname3)) )
                                subnames = fieldnames(parameters.(names{ii}).(tmpname1).(tmpname2).(tmpname3));
                                list.(tmp_field) = cell(size(subnames,1),4);
                                list.(tmp_field)(:,1) = subnames;
                            end
                            % level 3
                            [tmp_struct.(tmp_field),tmplist_struct] = get_structure(parameters.(names{ii}).(tmpname1).(tmpname2).(tmpname3),tmpname3,saveflag,tmp_field,list.(tmp_field));
                            if ~isempty(tmplist_struct)
                                tmptmp4 = tmplist_struct;
                                for mm = 1:length(tmptmp4)
                                    tmpname4 = tmptmp4{mm}.name;
                                    tmp_field = [names{ii} sep tmpname1 sep tmpname2 sep tmpname3 sep tmpname4];
                                    foutput = strrep(tmp_field,sep,'.');
                                    if ( nolist && isstruct(parameters.(names{ii}).(tmpname1).(tmpname2).(tmpname3).(tmpname4)) )
                                        subnames = fieldnames(parameters.(names{ii}).(tmpname1).(tmpname2).(tmpname3).(tmpname4));
                                        list.(tmp_field) = cell(size(subnames,1),4);
                                        list.(tmp_field)(:,1) = subnames;
                                    end
                                    % level 4
                                    [tmp_struct.(tmp_field),tmplist_struct] = get_structure(parameters.(names{ii}).(tmpname1).(tmpname2).(tmpname3).(tmpname4),tmpname4,saveflag,tmp_field,list.(tmp_field));

                                end % end for mm
                            end

                        end % end for ll
                    end

                end % end for kk
            end

        end % end for jj
    end

end % end for ii

complete = tmp_struct;

end % end of function
