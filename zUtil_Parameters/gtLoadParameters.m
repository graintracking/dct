function p = gtLoadParameters(path_to_params)
    parameters_file_name = 'parameters.mat';
    if (exist('path_to_params', 'var'))
        parameters_file_name = fullfile(path_to_params, parameters_file_name);
    end
    p = load(parameters_file_name);
    if (isfield(p, 'parameters'))
        p = p.parameters;
    end
    if (nargout == 0)
        assignin('base', 'parameters', p);
    end
end
