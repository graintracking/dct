function [updateFile, emptyFile] = gtAddMatFile(existingFile, addedFile, update, missing, addall, empty, debug)
% GTADDMATFILE  Merges, adds or updates fields between .mat files
%     [updateFile, emptyFile] = gtAddMatFile(existingFile, addedFile, [update], [missing], [addall], [empty], [debug])
%     ----------------------------------------------------------------------------------------------------------------
%
%     INPUT:
%       existingFile = <struct>     to be updated/modified
%       addedfile    = <struct>     to be added to the 'existingFile'
%       update       = <logical>    flag to update each of the existing fields in 'existingFile' by double-checking {false}
%       missing      = <logical>    flag to add missing fields in 'existingFile' from 'addedFile' {false}
%       addall       = <logical>    flag to add all the fields of 'addedFile' to 'existingFile' without a double-check {false}
%       empty        = <logical>    flag to check empty fields {false}
%       debug        = <logical>    flag to print out comments {false}
%
%     OUTPUT:
%       updateFile   = <struct>     the resulting structure
%       emptyFile    = <struct>     list of empty fields
%
%     Version 004 25/04/2012 by LNervo
%       Add flag 'empty'
%
%     Version 003 24/04/2012 by LNervo
%       Check functionality
%

if (~exist('debug','var') || isempty(debug))
    debug = false;
end
out = GtConditionalOutput(debug);

if (~exist('update','var') || isempty(update))
    update = false;
end
if (~exist('missing','var') || isempty(missing))
    missing = false;
end
if (~exist('addall','var') || isempty(addall))
    addall = false;
end
if (~exist('empty','var') || isempty(empty))
    empty = false;
end

names      = fieldnames(addedFile); % from the structure to be added
existnames = fieldnames(existingFile); % from the existing structure to be modified
        
for ii = 1:length(names) 
    if ~isfield(existingFile, names{ii}) && ~isfield(existingFile, lower(names{ii})) % is not a field of 'existingFile'
        out.odisp(['>>> ' names{ii} ' is not a field... It will be SKIPPED... <<<'])
        if (missing && ~addall && ~update)
            variable = names{ii};
            out.odisp(['--> ' variable ' <--'])
            check = inputwdefault('Do you want to add this field? [y/n]','n');
            if strcmpi(check,'y')
                out.odisp(['added field: ' names{ii}])       
                existingFile.(names{ii}) = addedFile.(names{ii});
            end
        elseif (missing || addall)
            out.odisp(['added field: ' names{ii}])       
            existingFile.(names{ii}) = addedFile.(names{ii});
        end
    end

    if isfield(existingFile, lower(names{ii})) && ~isfield(existingFile, names{ii}) %is a field but lowercase of 'existingFile'
        variable = lower(names{ii});
        if ~update
            oldvalue = existingFile.(variable);
            newvalue = addedFile.(names{ii});
            if ~ischar(oldvalue)
                oldvalue = num2str(oldvalue);
            end
            if ~ischar(newvalue)
                newvalue = num2str(newvalue);
            end
            out.odisp(['--> ' variable ' <--'])
            out.odisp('old: ')
            out.odisp(oldvalue)
            out.odisp('new: ')
            out.odisp(newvalue)
            check = inputwdefault('Are you sure to replace it with the new value? [y/n]','n');
        else
            out.odisp(['--> ' names{ii} ' <--'])
            check = inputwdefault('Are those the same thing? [y/n]','n');
            if strcmpi(check,'y')
                out.odisp(['updated field: ' variable ' with field: ' names{ii}])
                existingFile.(variable) = addedFile.(names{ii});
            end
        end
    end

    if (isfield(existingFile, names{ii})) % is a field of 'existingFile'
        if (~update)
            variable = names{ii};
            oldvalue = existingFile.(names{ii});
            newvalue = addedFile.(names{ii});
            if (~ischar(oldvalue) && ~iscell(oldvalue) && ~islogical(oldvalue) && ~isstruct(oldvalue))
                oldvalue=num2str(oldvalue);
            end
            if (~ischar(newvalue) && ~iscell(newvalue) && ~islogical(newvalue) && ~isstruct(newvalue))
                newvalue=num2str(newvalue);
            end
            if ~isequal(newvalue, oldvalue)
                out.odisp(['--> ' variable ' <--'])
                out.odisp('old: ')
                out.odisp(oldvalue)
                out.odisp('new: ')
                out.odisp(newvalue)
                check = inputwdefault('Do you want to replace it with the new value? [y/n]','n');
            
                if strcmpi(check,'y')
                    out.odisp(['replaced field: ' names{ii}])
                    existingFile.(names{ii}) = addedFile.(names{ii});
                end
            end
        else
            out.odisp(['updated field: ' names{ii}])
            existingFile.(names{ii}) = addedFile.(names{ii});  
        end
    end
end % end for i

list_empty = [];
if empty % check empty fields
    for jj = 1:length(existnames)
        if isempty(existingFile.(existnames{jj}))
            out.odisp(['^^^ ' existnames{jj} ' is empty ^^^'])
            list_empty.(existnames{jj}) = existingFile.(existnames{jj});
        end
    end
    emptyFile = list_empty;
    out.odisp('Empty fields:')
    out.odisp(emptyFile)
end

updateFile = existingFile;

end % end of function
    
