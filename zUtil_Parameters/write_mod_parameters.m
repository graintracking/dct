function textfile = write_mod_parameters(section_name, cell_list)
% WRITE_MOD_PARAMETERS  Writes changes to the parameters via gtCheckParameters
%                       into a text file.
%
%     textfile = write_mod_parameters(section_name, cell_list)
%     --------------------------------------------------------
%     Writes each field of cell_list relative to section_name into a text file 
%     in the current directory. Each field is separated by the string ' | '.
%     
%     INPUT:
%       section_name = <string>     field of parameters: i.e. 'acq'/'seg'....
%       cell_list    = <cell>       updated list from build_list_v2
%
%     OUTPUT:
%       textfile     = <string>     text file where to find the content of cell_list
%                                   Filename is built like
%                                   "<section_name>_mod_<datasetname>[num].log"
%
%
%     Version 001 30-08-2013 by LNervo


% prepare the command to get the use of fields of section_name
dirname = gtGetLastDirName(pwd);
textfile = gtLastFileName([section_name '_mod_' dirname],'new','log');

cell_list(:,4) = cellfun(@(num) num2str(num), cell_list(:,4), 'UniformOutput', false);

fid = fopen(textfile, 'a+t');

for ii = 1 : size(cell_list,1)
    for jj=1:size(cell_list{ii,2},1)
        fprintf(fid,'%s | ', cell_list{ii,1} );
        if iscell(cell_list{ii,2})
            fprintf(fid,'%s | ', cell_list{ii,2}{jj} );
        else
            fprintf(fid,'%s | ', cell_list{ii,2}(jj) );
        end
        fprintf(fid,'%s | ', cell_list{ii,3:end-1} );
        fprintf(fid,'%s', cell_list{ii,end} );
        fprintf(fid,'\n');
    end
end

fclose(fid);

end % end of function
