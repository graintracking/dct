function [update,list_out] = update_parameters(version,saving,par)
% UPDATE_PARAMETERS  Update the parameters file structure to the new type
%     [update,list_out] = update_parameters(version,saving,par)
%     ---------------------------------------------------------
%     
%     INPUT:
%       version = 
%       saving  = 
%       par     = 
%
%     OUTPUT:
%       update   = 
%       list_out = 
%

parameters=[];
if ~exist('par','var') || isempty(par)
    load('parameters.mat');
else
    parameters=par;
end
if ~exist('saving','var') || isempty(saving)
    saving=false;
end

% creates parameters template
[update,list] = make_parameters(version);

names    = fieldnames(update);
namespar = fieldnames(parameters);

for i=1:length(names)
    if isfield(parameters,names{i}) % names{i} is a field of parameters
        disp(['### Found structure ' names{i} ' in parameters... updating...'])
        update.(names{i}) = gtAddMatFile(update.(names{i}),parameters.(names{i}),true,false,false);
    else % is not a field of parameters
        disp(['!!! Structure ' names{i} ' not found in parameters --> EMPTY !!!'])
    end
end

if saving
    save('old_parameters.mat','parameters','-v7.3');
    disp('Saved existing parameters file in old_parameters.mat')
    parameters=update;
    save('parameters.mat','parameters','-v7.3');
    disp('Saved new parameters file in parameters.mat')
else
    disp('Nothing has been saved yet...')
end
if nargout == 2
    list_out = list;
end
clear names subnames namespar
end % end of function
