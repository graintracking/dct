function print_structure(structure, structurename, save, debug)
%     print_structure(structure, [structurename], [save], [debug])
%     ----------------------------------------------------------
%     Prints on screen and on a file (if wanted) the content of a structure
%     variable, with depth = 5.
%
%     Creates [structurename]_struct.txt with list of stuctures inside 'structure'
%     and [structurename].txt with output from the printing of
%     'structure' if 'save' is true
%
%     INPUT:
%        structure     = <struct>     structure to be printed
%        structurename = <struct>     its name {'parameters'}
%        save          = <logical>    keep a diary "<structurename>_content.txt" {false}
%        debug         = <logical>    display comments {true}
%
%
%     Version 002 05-09-2013 by LNervo
%       Commented and better formatted


if ~exist('structurename','var') || isempty(structurename)
    structurename = 'parameters';
end
if ~exist('save','var') || isempty(save)
    save = false;
end
if ~exist('debug','var') || isempty(debug)
    debug = true;
end

names=fieldnames(structure);

out = GtConditionalOutput(debug);

if save
    diary([structurename '_content.txt']);
    diary('on');
end

for ii=1:length(names) % level 1
    isin=false;
    if isstruct(structure.(names{ii}))
        isin=true;
        subnames=fieldnames(structure.(names{ii}));
        if length(structure.(names{ii})) ~= 1
            isin = false;
        else
            for jj=1:length(subnames) % level 2
                isin=false;
                if isstruct(structure.(names{ii}).(subnames{jj}))
                    isin=true;
                    subsubnames=fieldnames(structure.(names{ii}).(subnames{jj}));
                    if length(structure.(names{ii}).(subnames{jj})) ~= 1
                         isin = false;
                    else
                        for kk=1:length(subsubnames) % level 3
                            isin=false;
                            if isstruct(structure.(names{ii}).(subnames{jj}).(subsubnames{kk}))
                                isin=true;
                                subsubsubnames=fieldnames(structure.(names{ii}).(subnames{jj}).(subsubnames{kk}));
                                if length(structure.(names{ii}).(subnames{jj}).(subsubnames{kk})) ~= 1
                                    isin = false;
                                else
                                    for ll=1:length(subsubsubnames) % level 4
                                        isin=false;
                                        if isstruct(structure.(names{ii}).(subnames{jj}).(subsubnames{kk}).(subsubsubnames{ll}))
                                            isin=true;
                                            subsubsubsubnames=fieldnames(structure.(names{ii}).(subnames{jj}).(subsubnames{kk}).(subsubsubnames{ll}));
                                            if length(structure.(names{ii}).(subnames{jj}).(subsubnames{kk}).(subsubsubnames{ll})) ~= 1
                                                isin = false;
                                            else
                                                for mm=1:length(subsubsubsubnames) % level 5
                                                    isin=false;
                                                    if isstruct(structure.(names{ii}).(subnames{jj}).(subsubnames{kk}).(subsubsubnames{ll}).(subsubsubsubnames{mm}))
                                                        isin=false;
                                                        subsubsubsubsubnames=fieldnames(structure.(names{ii}).(subnames{jj}).(subsubnames{kk}).(subsubsubnames{ll}).(subsubsubsubnames{mm}));
                                                        
                                                        if ~isin
                                                            out.odisp([structurename '.' names{ii} '.' subnames{jj} '.' subsubnames{kk} '.' subsubsubnames{ll} '.' subsubsubsubnames{mm} ' ='])
                                                            out.odisp(structure.(names{ii}).(subnames{jj}).(subsubnames{kk}).(subsubsubnames{ll}).(subsubsubsubnames{mm}))
                                                        end
                                                        out.odisp(' ');
                                                    else
                                                        isin=false;
                                                    end
                                                end % level 5
                                            end
                                            if ~isin
                                                out.odisp([structurename '.' names{ii} '.' subnames{jj} '.' subsubnames{kk} '.' subsubsubnames{ll} ' ='])
                                                out.odisp(structure.(names{ii}).(subnames{jj}).(subsubnames{kk}).(subsubsubnames{ll}))
                                            end
                                            out.odisp(' ');
                                        else
                                            isin=false;
                                        end
                                    end % level 4
                                end
                                if ~isin
                                    out.odisp([structurename '.' names{ii} '.' subnames{jj} '.' subsubnames{kk} ' ='])
                                    out.odisp(structure.(names{ii}).(subnames{jj}).(subsubnames{kk}))
                                end
                                out.odisp(' ');
                            else
                                isin=false;
                            end
                        end % level 3
                    end
                    if ~isin
                        out.odisp([structurename '.' names{ii} '.' subnames{jj} ' ='])
                        out.odisp(structure.(names{ii}).(subnames{jj}))
                    end
                    out.odisp(' ');
                else
                    isin=false;
                end
            end % level 2
        end
        if ~isin
            out.odisp([structurename '.' names{ii} ' ='])
            out.odisp(structure.(names{ii}))
        end
        out.odisp(' ');
    else
        isin = false;
    end
end % level 1

if ~isin
    out.odisp([structurename ' ='])
    out.odisp(structure)
end

if save
    diary('off');
end

end % end of function
