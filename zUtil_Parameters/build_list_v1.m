function list = build_list_v1()
% BUILD_LIST  Builds a list of {fieldname, description, class} for each field of parameters.mat
%     list = build_list_v1()
%     ----------------------
%     Parameters version 1 (until Feb 2012)
%
%     OUTPUT:
%       list     = structure with each field as cell (N,3)
%
%     Version 001 18-11-2011 by LNervo
%       New structure of parameters.mat:
%           acq              ----|
%           cryst                |
%           xop                  |---->   old 'acq' structure
%           geo                  |
%           opt              ----|
%           database
%           prep
%           seg
%           match_calib
%           match
%           index
%           rec
%           fed
%
%       list.mat has the same fields as parameters.mat, except for index:
%           acq, cryst, xop, geo, opt, database, prep, seg, match_calib, match, rec
%
%           index__strategy.iter
%           index__strategy.r
%           index__strategy.s
%           index.strategy.b
%           index.strategy.m
%
%

list=[];
list.acq{1,1}  = 'collection_dir';          list.acq{1,2}  = 'Collection directory';            
list.acq{1,3}  = 'char';
list.acq{2,1}  = 'name';            	    list.acq{2,2}  = 'Name of the dataset';             
list.acq{2,3}  = 'char';
list.acq{3,1}  = 'dir';             	    list.acq{3,2}  = 'Directory in which to analyse the data';          
list.acq{3,3}  = 'char';
list.acq{4,1}  = 'date';                    list.acq{4,2}  = 'Date';           
list.acq{4,3}  = 'char';
list.acq{5,1}  = 'xdet';            	    list.acq{5,2}  = 'Detector size X (pixels)';                
list.acq{5,3}  = 'double';
list.acq{6,1}  = 'ydet';            	    list.acq{6,2}  = 'Detector size Y (pixels)';                
list.acq{6,3}  = 'double';
list.acq{7,1}  = 'nproj';           	    list.acq{7,2}  = 'Number of images in *180 DEGREES* of scan';               
list.acq{7,3}  = 'double';
list.acq{8,1}  = 'refon';           	    list.acq{8,2}  = 'References after how many images';               
list.acq{8,3}  = 'double';
list.acq{9,1}  = 'nref';            	    list.acq{9,2}  = 'How many reference images in a group';           
list.acq{9,3}  = 'double';
list.acq{10,1} = 'ndark';           	    list.acq{10,2} = 'How many dark images taken';             
list.acq{10,3} = 'double';
list.acq{11,1} = 'pixelsize';               list.acq{11,2} = 'Detector pixelsize (mm)';          
list.acq{11,3} = 'double';
list.acq{12,1} = 'count_time';              list.acq{12,2} = 'Image integration time (s)';              
list.acq{12,3} = 'double';
list.acq{13,1} = 'energy';          	    list.acq{13,2} = 'Beam energy (keV)';               
list.acq{13,3} = 'double';
list.acq{14,1} = 'dist';            	    list.acq{14,2} = 'Sample-detector distance (mm)';           
list.acq{14,3} = 'double';
list.acq{15,1} = 'sensortype';              list.acq{15,2} = 'Camera type (frelon / kodak4mv1)?';               
list.acq{15,3} = 'char';
list.acq{16,1} = 'type';            	    list.acq{16,2} = 'DCT scan type (360degree, 180degree, etc)';               
list.acq{16,3} = 'char';
list.acq{17,1} = 'interlaced_turns';        list.acq{17,2} = 'Interlaced scan? 0 for normal scan, 1 for one extra turn, etc';           
list.acq{17,3} = 'double';
list.acq{18,1} = 'mono_tune';               list.acq{18,2} = 'Monochromator was tuned after N reference groups, or 0 for not tuned';     
list.acq{18,3} = 'double';
list.acq{19,1} = 'rotation_axis';           list.acq{19,2} = 'Rotation axis orientation (vertical / horizontal)';               
list.acq{19,3} = 'char';
list.acq{20,1} = 'distortion';              list.acq{20,2} = 'Distortion correction file with path (or "none")';                
list.acq{20,3} = 'char';
list.acq{21,1} = 'flip_images';             list.acq{21,2} = 'Flip images for future coordinate system - normally no';          
list.acq{21,3} = 'logical';
list.acq{22,1} = 'no_direct_beam';          list.acq{22,2} = 'Special scan with no direct beam (taper frelon, offset detector)?';        
list.acq{22,3} = 'logical';
list.acq{23,1} = 'rotation_direction';      list.acq{23,2} = 'Horizontal axis scan - rotate images "clockwise" or "counterclockwise"?';  
list.acq{23,3} = 'char';
list.acq{24,1} = 'collection_dir_old';      list.acq{24,2} = 'Old collection directory';                
list.acq{24,3} = 'char';
list.acq{25,1} = 'rotu';            	    list.acq{25,2} = '';                
list.acq{25,3} = 'double';
list.acq{26,1} = 'rotx';                    list.acq{26,2} = '';                
list.acq{26,3} = 'double';
list.acq{27,1} = 'bb';                      list.acq{27,2} = '';                
list.acq{27,3} = 'double';
list.acq{28,1} = 'bbdir';           	    list.acq{28,2} = '';                
list.acq{28,3} = 'double';
list.acq{29,1} = 'maxradius';               list.acq{29,2} = '';                
list.acq{29,3} = 'double';
list.acq{30,1} = 'pair_tablename';          list.acq{30,2} = 'Table for spot pairs';            
list.acq{30,3} = 'char';
list.acq{31,1} = 'calib_tablename';         list.acq{31,2} = 'Table for calibration of pair matching';          
list.acq{31,3} = 'char';



list.xop{1,1} = 'tt';              list.xop{1,2} = 'Twotheta angle';                          list.xop{1,3} = 'single';
list.xop{2,1} = 'd0';              list.xop{2,2} = 'd-spacing';                               list.xop{2,3} = 'single';
list.xop{3,1} = 'int';             list.xop{3,2} = 'Intensity';                               list.xop{3,3} = 'single';
list.xop{4,1} = 'F';               list.xop{4,2} = 'Form factor (?)';                         list.xop{4,3} = 'single';
list.xop{5,1} = 'mult';            list.xop{5,2} = 'Multiplicity';                            list.xop{5,3} = 'int8';
list.xop{6,1} = 'hkl';             list.xop{6,2} = 'hkl Miller indexes';                      list.xop{6,3} = 'int8';
list.xop{7,1} = 'name';            list.xop{7,2} = 'XOP/Diamond crystallographic file';       list.xop{7,3} = 'char';
list.xop{8,1} = 'xop_dir';         list.xop{8,2} = 'Directory for the XOP input file';        list.xop{8,3} = 'char';


list.cryst{1,1}  = 'material';           list.cryst{1,2}  = 'Sample material';
list.cryst{1,3}  = 'char';
list.cryst{2,1}  = 'latticepar';         list.cryst{2,2}  = 'Sample lattice parameters [a b c alpha beta gamma] (angstrom, deg)';
list.cryst{2,3}  = 'double';
list.cryst{3,1}  = 'spacegroup';         list.cryst{3,2}  = 'Sample spacegroup';
list.cryst{3,3}  = 'double';
list.cryst{4,1}  = 'hermann_mauguin';    list.cryst{4,2}  = 'Hermann Mauguin short symbol';
list.cryst{4,3}  = 'char';
list.cryst{5,1}  = 'crystal_system';     list.cryst{5,2}  = 'Crystallographic unit-cell structure system';
list.cryst{5,3}  = 'char';
list.cryst{6,1}  = 'opsym';              list.cryst{6,2}  = 'Symmetry operators list';
list.cryst{6,3}  = 'cell';
list.cryst{7,1}  = 'hkl';                list.cryst{7,2}  = 'reflections families on the detector from xop';
list.cryst{7,3}  = 'int8';
list.cryst{8,1}  = 'tt';                 list.cryst{8,2}  = 'twotheta values from xop';
list.cryst{8,3}  = 'single';
list.cryst{9,1}  = 'd0';                 list.cryst{9,2}  = 'd-spacing values from xop';
list.cryst{9,3}  = 'single';
list.cryst{10,1} = 'hklsp';              list.cryst{10,2} = 'all the signed reflections on the detector';
list.cryst{10,3} = 'double';
list.cryst{11,1} = 'twoth';              list.cryst{11,2} = 'twotheta values';
list.cryst{11,3} = 'double';
list.cryst{12,1} = 'dsp';                list.cryst{12,2} = 'd-spacing values';
list.cryst{12,3} = 'double';
list.cryst{13,1} = 'thtype';             list.cryst{13,2} = 'theta type';
list.cryst{13,3} = 'double';




list.geo{1,1}  = 'detpos0';         list.geo{1,2}  = 'Detector position';               list.geo{1,3}  = 'double';
list.geo{2,1}  = 'detdiru0';        list.geo{2,2}  = 'Detector u-direction';            list.geo{2,3}  = 'double';
list.geo{3,1}  = 'detdirv0';        list.geo{3,2}  = 'Detector v-direction';            list.geo{3,3}  = 'double';
list.geo{4,1}  = 'beamdir0';        list.geo{4,2}  = 'Beam direction';                  list.geo{4,3}  = 'double';
list.geo{5,1}  = 'rotdir0';         list.geo{5,2}  = 'Rotation axis direction';         list.geo{5,3}  = 'double';
list.geo{6,1}  = 'detucen0';        list.geo{6,2}  = 'Detector u-centre';               list.geo{6,3}  = 'double';
list.geo{7,1}  = 'detvcen0';        list.geo{7,2}  = 'Detector v-centre';               list.geo{7,3}  = 'double';
list.geo{8,1}  = 'detdir0';         list.geo{8,2}  = 'Detector normal direction';       list.geo{8,3}  = 'double';
list.geo{9,1}  = 'detanglemin';     list.geo{9,2}  = 'Detector minimum 2Theta angle';	list.geo{9,3}  = 'double';
list.geo{10,1} = 'detanglemax';     list.geo{10,2} = 'Detector maximum 2Theta angle';	list.geo{10,3} = 'double';   


list.opt{1,1} = 'detpos';          list.opt{1,2} = 'Detector position';             list.opt{1,3} = 'double';
list.opt{2,1} = 'detdiru';         list.opt{2,2} = 'Detector u-direction';          list.opt{2,3} = 'double';
list.opt{3,1} = 'detdirv';         list.opt{3,2} = 'Detector v-direction';          list.opt{3,3} = 'double';
list.opt{4,1} = 'beamdir';         list.opt{4,2} = 'Beam direction';		        list.opt{4,3} = 'double';
list.opt{5,1} = 'rotdir';          list.opt{5,2} = 'Rotation axis direction';		list.opt{5,3} = 'double';
list.opt{6,1} = 'detucen';         list.opt{6,2} = 'Detector u-centre';             list.opt{6,3} = 'double';
list.opt{7,1} = 'detvcen';         list.opt{7,2} = 'Detector v-centre';             list.opt{7,3} = 'double';
list.opt{8,1} = 'detdir';          list.opt{8,2} = 'Detector normal direction';		list.opt{8,3} = 'double';


list.database{1,1} = 'name';             list.database{1,2} = 'Database name';            list.database{1,3} = 'char';
list.database{2,1} = 'host';             list.database{2,2} = 'Database host';            list.database{2,3} = 'char';
list.database{3,1} = 'user';             list.database{3,2} = 'Database user';            list.database{3,3} = 'char';
list.database{4,1} = 'password';         list.database{4,2} = 'Database password';        list.database{4,3} = 'char';



list.prep{1,1}  = 'normalisation';          list.prep{1,2}  = 'How to normalise images? margin/fullbeam';                
list.prep{1,3}  = 'char';
list.prep{2,1}  = 'absint';          		list.prep{2,2}  = 'Moving median interval, direct beam (images, mod(total projection, absint)=0)';                
list.prep{2,3}  = 'double';
list.prep{3,1}  = 'absrange';               list.prep{3,2}  = 'Moving median range, direct beam (images, n*2*absint)';                
list.prep{3,3}  = 'double';
list.prep{4,1}  = 'fullint';         		list.prep{4,2}  = 'Moving median interval, diffracted image (images, mod(total projection, fullint)=0)';                
list.prep{4,3}  = 'double';
list.prep{5,1}  = 'fullrange';              list.prep{5,2}  = 'Moving median range, diffracted image (images, n*2*fullint)';                
list.prep{5,3}  = 'double';
list.prep{6,1}  = 'margin';          		list.prep{6,2}  = 'Margin width for normalisation (pixels)';                
list.prep{6,3}  = 'double';
list.prep{7,1}  = 'intensity';              list.prep{7,2}  = 'Assumed direct beam intensity of normalisation';                
list.prep{7,3}  = 'double';
list.prep{8,1}  = 'filtsize';               list.prep{8,2}  = '2D median filter size for full images [pixels x pixels]';                
list.prep{8,3}  = 'double';
list.prep{9,1}  = 'drifts_pad';             list.prep{9,2}  = 'How to pad shifted images ("av" or value)';                
list.prep{9,3}  = 'char';
list.prep{10,1} = 'renumbered';             list.prep{10,2} = '';                
list.prep{10,3} = 'logical';
list.prep{11,1} = 'bb';              		list.prep{11,2} = '';                
list.prep{11,3} = 'double';
list.prep{12,1} = 'correct_drift';          list.prep{12,2} = '';                
list.prep{12,3} = 'char';
list.prep{13,1} = 'udrift';          		list.prep{13,2} = '';                
list.prep{13,3} = 'double';
list.prep{14,1} = 'udriftabs';              list.prep{14,2} = '';                
list.prep{14,3} = 'double';
list.prep{15,1} = 'vdrift';          		list.prep{15,2} = '';                
list.prep{15,3} = 'double';
    


list.seg{1,1}  = 'th2';             	list.seg{1,2}  = '';                
list.seg{1,3}  = 'double';
list.seg{2,1}  = 'th1';             	list.seg{2,2}  = 'Threshold for finding seeds';             
list.seg{2,3}  = 'double';
list.seg{3,1}  = 'th_grow_low';         list.seg{3,2}  = 'Lower limit of threshold for growing seeds';              
list.seg{3,3}  = 'double';
list.seg{4,1}  = 'th_grow_high';        list.seg{4,2}  = 'Upper limit of threshold for growing seeds';              
list.seg{4,3}  = 'double';
list.seg{5,1}  = 'minsize';             list.seg{5,2}  = '';                
list.seg{5,3}  = 'double';
list.seg{6,1}  = 'minint';          	list.seg{6,2}  = '';                
list.seg{6,3}  = 'double';
list.seg{7,1}  = 'icut';            	list.seg{7,2}  = '';                
list.seg{7,3}  = 'double';
list.seg{8,1}  = 'ExtLimImInt';         list.seg{8,2}  = 'First and last image in the stack with this minimum relative intensity';  
list.seg{8,3}  = 'double';
list.seg{9,1}  = 'ExtLimTailInt';       list.seg{9,2}  = 'Size of tails cut off the summed intensity curve through the image stack';   
list.seg{9,3}  = 'double';
list.seg{10,1} = 'DifSpotMask';         list.seg{10,2} = 'Mask used to create difspot.edf from blob';               
list.seg{10,3} = 'char';
list.seg{11,1} = 'dbfields';            list.seg{11,2} = 'Database ''difspot'' table fields list';            
list.seg{11,3} = 'char';
list.seg{12,1} = 'bb';              	list.seg{12,2} = 'Bounding Box';            
list.seg{12,3} = 'double';
list.seg{13,1} = 'background_subtract'; list.seg{13,2} = 'Subtraction of the background';           
list.seg{13,3} = 'logical';
list.seg{14,1} = 'overlaps_removed';    list.seg{14,2} = 'Removing of overlaps';            
list.seg{14,3} = 'logical';


list.match_calib{1,1}  = 'thr_ang';             list.match_calib{1,2}  = '';                list.match_calib{1,3}  = 'double';
list.match_calib{2,1}  = 'thr_ang_scale';       list.match_calib{2,2}  = '';                list.match_calib{2,3}  = 'double';
list.match_calib{3,1}  = 'thr_max_offset';      list.match_calib{3,2}  = '';                list.match_calib{3,3}  = 'double';
list.match_calib{4,1}  = 'thr_ext_offset';      list.match_calib{4,2}  = '';                list.match_calib{4,3}  = 'double';
list.match_calib{5,1}  = 'thr_genim_offset';    list.match_calib{5,2}  = '';                list.match_calib{5,3}  = 'double';
list.match_calib{6,1}  = 'corr_rot_to_det';     list.match_calib{6,2}  = '';                list.match_calib{6,3}  = 'double';
list.match_calib{7,1}  = 'thr_for_corr';        list.match_calib{7,2}  = '';                list.match_calib{7,3}  = 'double';
list.match_calib{8,1}  = 'thr_goodness';        list.match_calib{8,2}  = '';                list.match_calib{8,3}  = 'double';
list.match_calib{9,1}  = 'thr_intint';          list.match_calib{9,2}  = '';                list.match_calib{9,3}  = 'double';
list.match_calib{10,1} = 'thr_area';            list.match_calib{10,2} = '';                list.match_calib{10,3} = 'double';
list.match_calib{11,1} = 'thr_bbsize';          list.match_calib{11,2} = '';                list.match_calib{11,3} = 'double';
list.match_calib{12,1} = 'minsizeX';            list.match_calib{12,2} = '';                list.match_calib{12,3} = 'double';
list.match_calib{13,1} = 'minsizeY';            list.match_calib{13,2} = '';                list.match_calib{13,3} = 'double';
list.match_calib{14,1} = 'tiltY';               list.match_calib{14,2} = '';                list.match_calib{14,3} = 'double';
list.match_calib{15,1} = 'tiltZ';               list.match_calib{15,2} = '';                list.match_calib{15,3} = 'double';
list.match_calib{16,1} = 'centmode';            list.match_calib{16,2} = '';                list.match_calib{16,3} = 'double';
list.match_calib{17,1} = 'addconstr';           list.match_calib{17,2} = '';                list.match_calib{17,3} = 'double';
list.match_calib{18,1} = 'theta_bounds';        list.match_calib{18,2} = '';                list.match_calib{18,3} = 'double';


list.match{1,1}  = 'thr_ang';           list.match{1,2}  = '';                list.match{1,3}  = 'double';
list.match{2,1}  = 'thr_ang_scale';     list.match{2,2}  = '';                list.match{2,3}  = 'double';
list.match{3,1}  = 'thr_max_offset';    list.match{3,2}  = '';                list.match{3,3}  = 'double';
list.match{4,1}  = 'thr_ext_offset';    list.match{4,2}  = '';                list.match{4,3}  = 'double';
list.match{5,1}  = 'thr_genim_offset';  list.match{5,2}  = '';                list.match{5,3}  = 'double';
list.match{6,1}  = 'corr_rot_to_det';   list.match{6,2}  = '';                list.match{6,3}  = 'double';
list.match{7,1}  = 'thr_for_corr';      list.match{7,2}  = '';                list.match{7,3}  = 'double';
list.match{8,1}  = 'thr_goodness';      list.match{8,2}  = '';                list.match{8,3}  = 'double';
list.match{9,1}  = 'thr_intint';        list.match{9,2}  = '';                list.match{9,3}  = 'double';
list.match{10,1} = 'thr_area';          list.match{10,2} = '';                list.match{10,3} = 'double';
list.match{11,1} = 'thr_bbsize';        list.match{11,2} = '';                list.match{11,3} = 'double';
list.match{12,1} = 'minsizeX';          list.match{12,2} = '';                list.match{12,3} = 'double';
list.match{13,1} = 'minsizeY';          list.match{13,2} = '';                list.match{13,3} = 'double';
list.match{14,1} = 'tiltY';             list.match{14,2} = '';                list.match{14,3} = 'double';
list.match{15,1} = 'tiltZ';             list.match{15,2} = '';                list.match{15,3} = 'double';
list.match{16,1} = 'centmode';          list.match{16,2} = '';                list.match{16,3} = 'double';
list.match{17,1} = 'addconstr';         list.match{17,2} = '';                list.match{17,3} = 'double';



list.index{1,1} = 'strategy';                           list.index{1,2} = '';
list.index{1,3} = 'struct';
list.index__strategy{1,1} = 'b';                        list.index__strategy{1,2} = '';                                        
list.index__strategy{1,3} = 'struct';
list.index__strategy{2,1} = 'm';                        list.index__strategy{2,2} = '';                                        
list.index__strategy{2,3} = 'struct';
list.index__strategy{3,1} = 'iter';                     list.index__strategy{3,2} = 'Number of iterations';                                        
list.index__strategy{3,3} = 'double';
list.index__strategy{4,1} = 'r';                        list.index__strategy{4,2} = 'Tolerance for marking outliers in final grain sets (in std)'; 
list.index__strategy{4,3} = 'double';
list.index__strategy{5,1} = 's';                        list.index__strategy{5,2} = 'Tolerance for marking outliers in final grain sets (in std)'; 
list.index__strategy{5,3} = 'double';
list.index__strategy__b{1,1} = 'beg';                   list.index__strategy__b{1,2} = '';
list.index__strategy__b{1,3} = 'struct';
list.index__strategy__b{2,1} = 'end';                   list.index__strategy__b{2,2} = '';
list.index__strategy__b{2,3} = 'struct';
list.index__strategy__b__beg{1,1} = 'ang';              list.index__strategy__b__beg{1,2} = 'angular difference in degrees';    	
list.index__strategy__b__beg{1,3} = 'double';
list.index__strategy__b__beg{2,1} = 'int';              list.index__strategy__b__beg{2,2} = 'max. intensity ratio';         		
list.index__strategy__b__beg{2,3} = 'double';
list.index__strategy__b__beg{3,1} = 'bbxs';             list.index__strategy__b__beg{3,2} = 'max. bbox x size ratio';           	
list.index__strategy__b__beg{3,3} = 'double';
list.index__strategy__b__beg{4,1} = 'bbys';             list.index__strategy__b__beg{4,2} = 'max. bbox y size ratio';           	
list.index__strategy__b__beg{4,3} = 'double';
list.index__strategy__b__beg{5,1} = 'distsf';           list.index__strategy__b__beg{5,2} = 'size factor for maximum distance'; 	
list.index__strategy__b__beg{5,3} = 'double';
list.index__strategy__b__beg{6,1} = 'distmax';          list.index__strategy__b__beg{6,2} = 'max. absolut distance';            	
list.index__strategy__b__beg{6,3} = 'double';
list.index__strategy__b__beg{7,1} = 'ming';             list.index__strategy__b__beg{7,2} = 'min. no. of Friedel pairs in a grain';     
list.index__strategy__b__beg{7,3} = 'double';
list.index__strategy__b__beg{8,1} = 'geo';              list.index__strategy__b__beg{8,2} = 'safety margin around sample volume';       
list.index__strategy__b__beg{8,3} = 'double';
list.index__strategy__b__end{1,1} = 'ang';              list.index__strategy__b__end{1,2} = 'angular difference in degrees';    	
list.index__strategy__b__end{1,3} = 'double';
list.index__strategy__b__end{2,1} = 'int';              list.index__strategy__b__end{2,2} = 'max. intensity ratio';         		
list.index__strategy__b__end{2,3} = 'double';
list.index__strategy__b__end{3,1} = 'bbxs';             list.index__strategy__b__end{3,2} = 'max. bbox x size ratio';           	
list.index__strategy__b__end{3,3} = 'double';
list.index__strategy__b__end{4,1} = 'bbys';             list.index__strategy__b__end{4,2} = 'max. bbox y size ratio';           	
list.index__strategy__b__end{4,3} = 'double';
list.index__strategy__b__end{5,1} = 'distsf';           list.index__strategy__b__end{5,2} = 'size factor for maximum distance'; 	
list.index__strategy__b__end{5,3} = 'double';
list.index__strategy__b__end{6,1} = 'distmax';          list.index__strategy__b__end{6,2} = 'max. absolut distance';            	
list.index__strategy__b__end{6,3} = 'double';
list.index__strategy__b__end{7,1} = 'ming';             list.index__strategy__b__end{7,2} = 'min. no. of Friedel pairs in a grain';     
list.index__strategy__b__end{7,3} = 'double';
list.index__strategy__b__end{8,1} = 'geo';              list.index__strategy__b__end{8,2} = 'safety margin around sample volume';       
list.index__strategy__b__end{8,3} = 'double';
list.index__strategy__m{1,1} = 'beg';                   list.index__strategy__m{1,2} = '';
list.index__strategy__m{1,3} = 'struct';
list.index__strategy__m{2,1} = 'end';                   list.index__strategy__m{2,2} = '';
list.index__strategy__m{2,3} = 'struct';
list.index__strategy__m__beg{1,1} = 'bbxs';             list.index__strategy__m__beg{1,2} = 'max. bbox x size ratio';         		
list.index__strategy__m__beg{1,3} = 'double';
list.index__strategy__m__beg{2,1} = 'bbys';             list.index__strategy__m__beg{2,2} = 'max. bbox y size ratio';         		
list.index__strategy__m__beg{2,3} = 'double';
list.index__strategy__m__beg{3,1} = 'distsf';           list.index__strategy__m__beg{3,2} = 'size factor for maximum distance';         
list.index__strategy__m__beg{3,3} = 'double';
list.index__strategy__m__beg{4,1} = 'int';              list.index__strategy__m__beg{4,2} = 'max. intensity ratio';         		
list.index__strategy__m__beg{4,3} = 'double';
list.index__strategy__m__end{1,1} = 'bbxs';             list.index__strategy__m__end{1,2} = 'max. bbox x size ratio';         		
list.index__strategy__m__end{1,3} = 'double';
list.index__strategy__m__end{2,1} = 'bbys';             list.index__strategy__m__end{2,2} = 'max. bbox y size ratio';         	
list.index__strategy__m__end{2,3} = 'double';
list.index__strategy__m__end{3,1} = 'distsf';           list.index__strategy__m__end{3,2} = 'size factor for maximum distance';         
list.index__strategy__m__end{3,3} = 'double';
list.index__strategy__m__end{4,1} = 'int';              list.index__strategy__m__end{4,2} = 'max. intensity ratio';         		
list.index__strategy__m__end{4,3} = 'double';



list.rec{1,1} = 'ROI';                          list.rec{1,2} = 'Reconstruct ROI (sample coordinates, or "none")';       	
list.rec{1,3} = 'char';
list.rec{2,1} = 'select_projections_auto';  	list.rec{2,2} = '';                             
list.rec{2,3} = 'logical';
list.rec{3,1} = 'do_forward_simulation';    	list.rec{3,2} = 'Do forward simulation to pick up spots missed in indexing';    	
list.rec{3,3} = 'logical';
list.rec{4,1} = 'update_db';                	list.rec{4,2} = 'Do the reconstruction of the grain volume';        	
list.rec{4,3} = 'logical';
list.rec{5,1} = 'show_results';             	list.rec{5,2} = 'Update database with grainIDs';          	
list.rec{5,3} = 'logical';
list.rec{6,1} = 'use_extspots';             	list.rec{6,2} = 'Use extinction spots for reconstruction (typically no)';             	
list.rec{6,3} = 'logical';
list.rec{7,1} = 'percentile1';              	list.rec{7,2} = 'Percentile threshold used to create mask for reconstruction';      	
list.rec{7,3} = 'double';
list.rec{8,1} = 'percentile2';              	list.rec{8,2} = 'Percentile threshold applied to masked reconstruction';                             
list.rec{8,3} = 'double';


list.fed{1,1} = 'dir';                          list.fed{1,2} = 'Data analysis directory';       	
list.fed{1,3} = 'char';
list.fed{2,1} = 'dct_vol';                      list.fed{2,2} = 'Dct reconstructed volume filename (.edf)';                             
list.fed{2,3} = 'char';
list.fed{3,1} = 'dct_offset';                   list.fed{3,2} = 'Offset to be applied to the volume [x y z]';    	
list.fed{3,3} = 'double';
list.fed{4,1} = 'renumber_list';                list.fed{4,2} = 'Renumbering list for grainID';    	
list.fed{4,3} = 'double';


end

