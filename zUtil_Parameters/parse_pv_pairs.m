function [params, rejected_params] = parse_pv_pairs(params, pv_pairs, add_missing)
% parse_pv_pairs: parses sets of property value pairs, allows defaults
% usage: params=parse_pv_pairs(default_params,pv_pairs)
%
% arguments: (input)
%  default_params - structure, with one field for every potential
%             property/value pair. Each field will contain the default
%             value for that property. If no default is supplied for a
%             given property, then that field must be empty.
%
%  pv_array - cell array of property/value pairs.
%             Case is ignored when comparing properties to the list
%             of field names. Also, any unambiguous shortening of a
%             field/property name is allowed.
%
%  add_missing - boolean that says if we want to add the missing fields, or to
%                generate an error, in case it was not supplied the
%                rejected_params out argument. Default is false -> throws error.
%
% arguments: (output)
%  params   - parameter struct that reflects any updated property/value
%             pairs in the pv_array.
%  rejected_params - parameter pairs that don't reflect any updated
%             property/value pairs in the pv_array.
%
% Example usage:
% First, set default values for the parameters. Assume we
% have four parameters that we wish to use optionally in
% the function examplefun.
%
%  - 'viscosity', which will have a default value of 1
%  - 'volume', which will default to 1
%  - 'pie' - which will have default value 3.141592653589793
%  - 'description' - a text field, left empty by default
%
% The first argument to examplefun is one which will always be
% supplied.
%
%   function examplefun(dummyarg1,varargin)
%   params.Viscosity = 1;
%   params.Volume = 1;
%   params.Pie = 3.141592653589793
%
%   params.Description = '';
%   params=parse_pv_pairs(params,varargin);
%   params
%
% Use examplefun, overriding the defaults for 'pie', 'viscosity'
% and 'description'. The 'volume' parameter is left at its default.
%
%   examplefun(rand(10),'vis',10,'pie',3,'Description','Hello world')
%
% params = 
%     Viscosity: 10
%        Volume: 1
%           Pie: 3
%   Description: 'Hello world'
%
% Note that capitalization was ignored, and the property 'viscosity'
% was truncated as supplied. Also note that the order the pairs were
% supplied was arbitrary.

if (~isstruct(params) && ~isobject(params))
    error('PARAMS:wrong_argument', ...
        'No structure or class for defaults was supplied');
end

if (~exist('add_missing', 'var'))
    add_missing = false;
end
rejected_params = {};

if (add_missing && (nargout > 1))
    warning('PARAMS:conflicting_behavior', ...
        'rejected_parameters will be empty');
end

try
    pv_pairs = reshape(pv_pairs, 2, []);
catch mexc
    error('PARAMS:wrong_number', ...
        'Property/value pairs must come in PAIRS.');
end

% there was at least one pv pair. process any supplied
propnames = fieldnames(params);
lpropnames = lower(propnames);

for ii = 1:size(pv_pairs, 2)
    p_i = lower(pv_pairs{1, ii});
    v_i = pv_pairs{2, ii};

    if (~add_missing)
        ind = strcmp(p_i, lpropnames);
        ind = find(ind);

        if (isempty(ind))
            % Here we try to do a submatching
            ind = find(strncmp(p_i, lpropnames, length(p_i)));

            if (isempty(ind))
                if (~exist('rejected_params', 'var'))
                    error('PARAMS:wrong_argument', ...
                        'No matching property found for: %s', ...
                        p_i);
                else
                    rejected_params = [rejected_params, p_i, v_i]; %#ok<AGROW>
                    continue;
                end
            elseif (numel(ind) > 1)
                error('PARAMS:wrong_argument', ...
                    'Ambiguous property name: %s', p_i);
            end
        end

        p_i = propnames{ind};
    end

    % override the corresponding default in params
    params.(p_i) = v_i;
end






