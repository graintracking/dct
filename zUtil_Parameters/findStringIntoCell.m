function [index, outlist] = findStringIntoCell(inlist, values, column, partial)
% FINDSTRINGINTOCELL  Finds string into a cell structure in the specified column
%     [index, outlist] = findStringIntoCell(inlist, values, column, partial)
%     ----------------------------------------------------------
%     INPUT:
%       inlist = <cell> structure of string
%       values = <cell> of string - can be a vector
%       column = <double> {1}
%
%     OUTPUT:
%       index   = indexes of values in inlist <double>
%       outlist = inlist(index, :) <cell>
%
%     Version 002 04-09-2012 by LNervo


    if ~exist('column', 'var') || isempty(column)
        column = 1;
    end
    if ~exist('partial', 'var') || isempty(partial)
        partial = false;
    end

    index = [];
    for ii = 1:size(values, 1)
        for jj = 1:size(inlist, 1)
            tmp = inlist{jj, column};

            if (partial)
                if strfind(tmp, values{ii})
                    index = [index; jj];
                end
            else
                if strcmp(tmp, values{ii})
                    index = [index; jj];
                end
            end
        end
    end

    if (nargout == 2)
        outlist = inlist(index, :);
    end
end
