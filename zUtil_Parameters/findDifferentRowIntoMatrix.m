function [matched,ia,ib] = findDifferentRowIntoMatrix(whereToFind, toBeFound, isPartial)
% FINDDIFFERENTROWINTOMATRIX  Intersetcs matrices
%     [rows,ia,ib] = findDifferentRowIntoMatrix(whereToFind, toBeFound, isPartial)
%     ----------------------------------------------------------------------------
%     Inputs are transformed into columns. 
%     Uses intersect(...,['rows'],'stable') and findArrayIntoArray().
%     Inputs must have the same number of columns.
%
%     INPUT:
%       whereToFind = matrix where to find elements / rows
%       toBeFound   = matrix or elements to be found in 'whereToFind'
%       isPartial   = if true, it looks for 'toBeFound' in 'whereToFind' as a
%                     matrix following the column order and 'toBeFound' is
%                     sorted in ascending order <logical> {false}
%
%                     isPartial=true means 'rows' option not used!
%                     isPartial=false uses 'rows' option
%
%     OUTPUT:
%       rows = matched elements of 'toBeFound' <vector>
%       ia   = indexes of 'toBeFound' in 'whereToFind'
%              [<rows, cols>] if isPartial = true
%              [<row>] if isPartial = false
%       ib   = indexes of 'whereToFind' in 'toBeFound'
%              [<rows, cols>] if isPartial = true
%              [<row>] if isPartial = false
%
%
%     Version 002 06-09-2012 by LNervo


if ~exist('isPartial','var') || isempty(isPartial)
    isPartial = false;
end

% trasform to columns
if size(whereToFind,1)==1 && size(whereToFind,2)>1
    whereToFind = whereToFind';
end
% if size(toBeFound,1)==1 && size(toBeFound,2)>1
%     toBeFound = toBeFound';
% end
if size(whereToFind,2)~=size(toBeFound,2) && ~isPartial
    gtError('findDifferentRowIntoMatrix:wrongSize','The size in the second dimension of a and b must be equal.')
end

if (~isPartial)
    [matched,ia,ib] = intersect(whereToFind,toBeFound,'rows','stable');
    return
else
    [matched,~,~] = intersect(whereToFind,toBeFound,'stable');
end

ia2 = cell(size(matched));
ib2 = cell(size(matched));
% resorting elements in the right order of 'toBefound'
if ~isempty(matched)
  for ii = 1:numel(matched)    
    [row1,col1,~]=findArrayIntoArray(matched(ii), whereToFind);
    [row2,col2,~]=findArrayIntoArray(matched(ii), toBeFound);
    
    ia2{ii} = [row1 col1];
    ib2{ii} = [row2 col2];
  end
end

ia = ia2;
ib = ib2;

end % end of function
