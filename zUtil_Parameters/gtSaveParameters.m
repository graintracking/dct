function gtSaveParameters(parameters)
    if (~exist('parameters', 'var'))
        parameters = evalin('base', 'parameters');
    end
%     if (~strcmp(pwd(), parameters.acq.dir))
%         warning('gtSaveParameters:wrong_path', '')
%     end
    parameters_file_name = fullfile(parameters.acq(1).dir, 'parameters.mat');
    save(parameters_file_name, 'parameters', '-v7.3');
end
