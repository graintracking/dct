function [parameters, tocheck, list] = gtCheckParameters(parameters, section_name, varargin)
% GTCHECKPARAMETERS  Checks that the given section of the parameters contains the
%                    defaults, otherwise adds them!
%     [parameters, tocheck, list] = gtCheckParameters(parameters, section_name, varargin)
%     ------------------------------------------------------------------------------------------------------------
%     Checks also the fields of <section_name> used in the code and if they are saved
%     in the parameters file.
%
%     INPUT:
%       parameters   = <struct>     Parameters file where to check fields
%       section_name = <string>     section name (it must be field of
%                                   parameters).
%                                   It can be chosen between:
%                                     'version', 'acq', 'xop', 'cryst',
%                                     'labgeo', 'samgeo', 'recgeo',
%                                     'prep', 'seg', 'match', 'index',
%                                     'fsim', 'rec', 'fed', 'oar'
%     OPTIONAL INPUT (varargin):
%       check_type   = <string>     type of check: {'default'} | 'used' | 'extra'
%       diary_flag   = <logical>    Flag to save txt files {false} | true
%       verbose      = <logical>    Flag to print on the screen comments {false} | true
%
%     OUTPUT:
%       parameters   = <struct>     Updated parameters
%       tocheck      = <cell>       Structure with information about parameters
%                                   to be checked (M, 5)
%       list         = <cell>       Updated list
%
%[sub]-sfDisplayParameterUse
%
%
%     Version 004 30-09-2013 by LNervo
%       Added checkType argument to check only a set of parameters
%         (default, used, extra, all)
%       Added diaryFlag argument to save txt files or not
%
%     Version 003 27-08-2013 by LNervo
%       Added missing fields and let the user choose the class with GUI
%
%     Version 002 21-08-2013 by LNervo


global GT_MATLAB_HOME

params_sections = { 'version', 'acq', 'xop', 'cryst', 'labgeo', ...
    'samgeo', 'recgeo', 'prep', 'seg', 'match', 'index', 'fsim', 'rec', ...
    'fed', 'oar'};

conf = struct(...
    'check_type', {'default'}, ...
    'diary_flag', {false}, ...
    'verbose', {false} );
conf = parse_pv_pairs(conf, varargin);

if ~any( ismember( fieldnames(parameters), section_name ) )
    if any( ismember(section_name, params_sections) )
        disp('Missing section... adding it to parameters ! Please fill the correct values for this section')
        parameters.(section_name) = get_default_section(section_name, parameters);
    else
        gtError('gtCheckParameters:make_parameters:wrong_argument', ...
            'Section name is wrong: "%s" is not a valid parameters section.', ...
            section_name)
    end
end

out = GtConditionalOutput(conf.verbose);

if (conf.diary_flag)
    % save output in diary
    dirname = gtGetLastDirName(pwd);
    filename = gtLastFileName([section_name '_check_' dirname], 'new', 'log');
    diary(filename);
end

% gets the defaults values
out.odisp(' ')
[default_parameters, default_list] = make_parameters(2, conf.verbose); % 0.025 sec
out.odisp(' ')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% list names
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

list_names    = default_list.(section_name);
list_names    = list_names(:, 1);
% default names from parameters and from list
default_names = fieldnames(default_parameters.(section_name));
% saved names
sec_names     = fieldnames(parameters.(section_name));

% check if it is consistent
if ~all(ismember(list_names, default_names))
     gtError('gtCheckParameters:make_parameters:wrong_list', ...
         'Something is wrong with the default list and the default parameters.')
end

list_tocheck = cell(0, 5);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Info about ALL THE FIELDS used in the code from <section_name>
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[output_all, patterns] = get_used_parameters_info(section_name, '\.', false); % 0.73 sec

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Fields used in the code : get only used VARIABLES and update info
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[~, output_all, singles_vars] = get_used_names(output_all, section_name, patterns, false); % 0.050 sec
% take unique used names
used_names    = unique(singles_vars);

% keep patterns for later (column shape)
patterns = patterns';
% find sections relative to section_name
[~, sections_] = findStringIntoCell(fieldnames(default_list), {section_name});

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Criterion #1 : check used VS saved parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

switch (conf.check_type)
    case 'used'
        out.odisp(' *** Checking used fields... *** ')
        % check used fields in the code that are missing in the parameters.mat file
        checks0 = ~ismember(used_names, sec_names);
        missing_used_names = used_names(checks0);

        % #1 A
        if ~isempty(missing_used_names)
            out.odisp('Missing used fields in parameters.mat:')
            out.odisp(missing_used_names)

            % checks if missing_used_names are default or not
            default = ismember(missing_used_names, default_names);

            % #2 A
            found_default_names = missing_used_names(default);
            if ~isempty(found_default_names)
                % find info relative to missing fields
                [~, tocheck, ~] = findValueIntoCell(output_all, found_default_names);

                sfDisplayParameterUse(found_default_names, tocheck, 'used in the code but default:');

                [~, sections_toadd_] = findStringIntoCell(sections_, found_default_names);
                if ~isempty(sections_toadd_)
                    sections_toadd_(end+1, 1) = {section_name};
                end
                tmp_list = cell2struct( cellfun(@(num) default_list.(num), sections_toadd_, 'UniformOutput', false), sections_toadd_, 1);
                % modify added parameters that are default
                [~, tmp_list.(section_name), ~] = findValueIntoCell(default_list.(section_name), found_default_names);
                action = repmat({'added to parameters'}, size(tmp_list.(section_name), 1), 1);

                tmp_par = build_structure(tmp_list, '__', conf.verbose);
                parameters.(section_name) = gtAddMatFile(parameters.(section_name), tmp_par.(section_name), false, true, true);

                parameters.(section_name) = gtModifyStructure(parameters.(section_name), tmp_list.(section_name), [], 'Adding default missing parameters used in the code');
                list_tocheck = [list_tocheck; [tmp_list.(section_name) action]];
            end

            % #2 B
            found_notdefault_names = missing_used_names(~default);
            if ~isempty(found_notdefault_names)
                % find info relative to missing fields
                [~, tocheck, nottocheck] = findValueIntoCell(output_all, found_notdefault_names);
                found_notdefault_names = setdiff(found_notdefault_names, nottocheck');

                outlist = sfDisplayParameterUse(found_notdefault_names, tocheck, 'used in the code but not default:');

                tmp_par = cell2struct(cell(size(found_notdefault_names)), found_notdefault_names, 1);
                parameters.(section_name)   = gtAddMatFile(parameters.(section_name), tmp_par, false, true, true);
                % build list for new pars
                tmp_list = [found_notdefault_names outlist ...
                            cellfun(@(num) class(parameters.(section_name).(num)), found_notdefault_names, 'UniformOutput', false) ...
                            repmat({-1}, length(found_notdefault_names), 1)];
                default_list.(section_name) = [default_list.(section_name); tmp_list];
                % add gtModifyStructure for the added parameters
                [parameters.(section_name), default_list.(section_name)] = gtModifyStructure(parameters.(section_name), default_list.(section_name), -1, 'Adding not default missing parameters used in the code:');
                [~, tmp_list, ~] = findValueIntoCell(default_list.(section_name), {-1});
                action = repmat({'added to default'}, size(tmp_list, 1), 1);
                if ~isempty(tmp_list)
                    list_tocheck = [list_tocheck; [tmp_list action]];
                    get_structure(parameters.(section_name), section_name, true, section_name, tmp_list);
                end
            end

            default_parameters = build_structure(default_list, '__', false);
            default_names = fieldnames(default_parameters.(section_name));
            list_names    = default_list.(section_name);
            list_names    = list_names(:, 1);
            sec_names     = fieldnames(parameters.(section_name));
        else
            out.odisp('  All the used fields are saved in the file.')
        end % if ~isempty(missing_used_names)

    case 'default' % Criterion #2 : check default VS saved parameters
        out.odisp(' ')
        out.odisp(' *** Checking default fields... *** ')
        checks1 = ~ismember(default_names, sec_names);
        missing_default_names = default_names(checks1);

        if ~isempty(missing_default_names)
            out.odisp('Missing default fields in parameters:')
            out.odisp(missing_default_names)

            % find missing defaults fields that are used/not used in the code
            used = ismember(missing_default_names, used_names);

            % #2 A
            found_default_names = missing_default_names(used);
            if ~isempty(found_default_names)
                % find info relative to missing fields
                [~, tocheck, ~] = findValueIntoCell(output_all, found_default_names);

                sfDisplayParameterUse(found_default_names, tocheck, 'used in the code but default but not saved:');

                [~, sections_toadd_] = findStringIntoCell(sections_, found_default_names);
                if ~isempty(sections_toadd_)
                    sections_toadd_(end+1, 1) = {section_name};
                end
                tmp_list = cell2struct( cellfun(@(num) default_list.(num), sections_toadd_, 'UniformOutput', false), sections_toadd_, 1);
                % modify added parameters that are default
                [~, tmp_list.(section_name), ~] = findValueIntoCell(default_list.(section_name), found_default_names);
                action = repmat({'added to parameters'}, size(tmp_list.(section_name), 1), 1);

                tmp_par = build_structure(tmp_list, '__', conf.verbose);
                parameters.(section_name) = gtAddMatFile(parameters.(section_name), tmp_par.(section_name), false, true, true);

                parameters.(section_name) = gtModifyStructure(parameters.(section_name), tmp_list.(section_name), [], 'Adding default missing parameters used in the code:\nYou should be an expert user; if not, press Quit...');
                list_tocheck = [list_tocheck; [tmp_list.(section_name) action]];
            end

            % #2 C
            notfound_default_names = missing_default_names(~used);
            if ~isempty(notfound_default_names)

                sfDisplayParameterUse(notfound_default_names, [], 'not used in the code but default !');

                [ind_, sections_toadd_] = findStringIntoCell(sections_, notfound_default_names);
                if ~isempty(sections_toadd_)
                    sections_toadd_(end+1, 1) = {section_name};
                end
                tmp_list = cell2struct( cellfun(@(num) default_list.(num), sections_toadd_, 'UniformOutput', false), sections_toadd_, 1);
                tmp_list_notfound = [];
                for ii = 1:length(sections_toadd_)
                    [~, tmp_list.(sections_toadd_{ii}), ~] = findValueIntoCell(default_list.(sections_toadd_{ii}), notfound_default_names);

                    [ind, ~, ~]=findValueIntoCell(default_list.(sections_toadd_{ii}), notfound_default_names);
                    if ~isempty(ind)
                        tmp_list_notfound = [tmp_list_notfound; default_list.(sections_toadd_{ii})(ind(:, 1), :)];
                        default_list.(sections_toadd_{ii})(ind(:, 1), :) = [];
                    elseif ismember(sections_toadd_{ii}, sections_(ind_))
                        tmp_list_notfound = [tmp_list_notfound; default_list.(sections_toadd_{ii})];
                        default_list = rmfield(default_list, sections_toadd_{ii});
                    end
                end
                action = repmat({'removed from default'}, size(tmp_list_notfound, 1), 1);
                out.odisp('  Removed from default parameters and default list !')
                list_tocheck = [list_tocheck; [tmp_list_notfound action]];
            end

            default_parameters = build_structure(default_list, '__', false);
            default_names = fieldnames(default_parameters.(section_name));
            list_names    = default_list.(section_name);
            list_names    = list_names(:, 1);
            sec_names     = fieldnames(parameters.(section_name));
        else
            out.odisp('  All the default fields are saved in the file.')
        end % if ~isempty(missing_default_names)

    case 'extra' % Fields exceeding the defaults : #1 B
        % now let's see if some parameters are just useless
        out.odisp(' ')
        out.odisp(' *** Checking extra fields... *** ')
        checks2 = ~ismember(sec_names, default_names);
        extra_names = sec_names(checks2);

        if ~isempty(extra_names)
            out.odisp('Extra fields in parameters.mat not defined as default:')
            out.odisp(extra_names)

            % checks if extra_names are used or not
            used = ismember(extra_names, used_names);

            % #1 B
            found_extra_names = extra_names(used);
            if ~isempty(found_extra_names)
                % find info relative to missing fields
                [~, tocheck, ~] = findValueIntoCell(output_all, found_extra_names);

                outlist = sfDisplayParameterUse(found_extra_names, tocheck, 'used in the code but not default:');

                % build list for new pars
                tmp_list = [found_extra_names outlist ...
                            cellfun(@(num) class(parameters.(section_name).(num)), found_extra_names, 'UniformOutput', false) ...
                            repmat({-2}, length(found_extra_names), 1)];
                default_list.(section_name) = [default_list.(section_name); tmp_list];

                % add gtModifyStructure for the added parameters
                [parameters.(section_name), default_list.(section_name)] = gtModifyStructure(parameters.(section_name), default_list.(section_name), -2, 'Now possible to remove or not extra fields used in the code:');
                [~, tmp_list, ~] = findValueIntoCell(default_list.(section_name), {-2});
                action = repmat({'added to default'}, size(tmp_list, 1), 1);
                if ~isempty(tmp_list)
                    list_tocheck = [list_tocheck; [tmp_list action]];
                    get_structure(parameters.(section_name), section_name, true, section_name, tmp_list);
                end
            end

            % #4 B
            notfound_extra_names = extra_names(~used);
            if ~isempty(notfound_extra_names)
                sfDisplayParameterUse(notfound_extra_names, [], 'saved but not used in the code !');

                action = repmat({'removed from parameters'}, size(notfound_extra_names, 1), 1);
                list_tocheck = [list_tocheck; [notfound_extra_names repmat({''}, size(notfound_extra_names, 1), 1) ...
                                               cellfun(@(num) class(parameters.(section_name).(num)), notfound_extra_names, 'UniformOutput', false) ...
                                               cell(size(notfound_extra_names, 1), 1) action]];
                parameters.(section_name) = rmfield(parameters.(section_name), notfound_extra_names);
            end

            % re-build default parameters and update all the names' lists
            default_parameters = build_structure(default_list, '__', false);
            default_names = fieldnames(default_parameters.(section_name));
            list_names    = default_list.(section_name);
            list_names    = list_names(:, 1);
            sec_names     = fieldnames(parameters.(section_name));
        else
            out.odisp('  There are not extra fields (not used) saved in the file.')
        end % if ~isempty(extra_names)
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% summary
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if ~isempty(list_tocheck)

    disp(['Parameters to be checked for section ''' section_name ''':'])
    disp(list_tocheck)

    if (conf.diary_flag)
        textfile = write_mod_parameters(section_name, list_tocheck);

        disp('A log file is saved here:')
        disp(textfile)
    end
end

if (nargout > 1)
    if ~isempty(list_tocheck)
        tocheck = list_tocheck;
    else
        clear tocheck;
        [tocheck.isfield, tocheck.isdefault, tocheck.isextra, tocheck.ismissing_default, tocheck.ismissing_used] = ...
             deal(sec_names, default_names, extra_names, missing_default_names, missing_used_names);
    end
    if (nargout > 2)
        list = default_list;
    end
end

if (conf.diary_flag)
    diary('off')
end

end % end of function

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% sub-functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function outlist = sfDisplayParameterUse(list_names, output_all, flag)

    outlist = {};
    for ii = 1:length(list_names)
        name = list_names{ii};
        disp(['  Parameter ' name ' is ' flag])
        [ind, tocheck] = findValueIntoCell(output_all, {name});
        if ~isempty(ind)
            disp(unique(tocheck(:, 1)))
            outlist{ii} = unique(tocheck(:, 1));
        else
            outlist{ii} = [];
        end
    end

    if (nargout > 0)
        outlist = outlist';
    end
end

function section = get_default_section(section_name, parameters)
    switch(section_name)
        case 'labgeo'
            section = gtGeoLabDefaultParameters(parameters.acq);
        case 'detgeo'
            section = gtGeoDetDefaultParameters(parameters.acq);
        case 'samgeo'
            section = gtGeoSamDefaultParameters();
        case 'recgeo'
            section = gtGeoRecDefaultParameters(parameters.labgeo, parameters.samgeo);
        case 'seg'
            section = gtSegDefaultParameters();
        case 'match'
            section = gtMatchDefaultParametersGUI();
        case 'index'
            section = gtIndexDefaultParameters();
        case 'fsim'
            section = gtFsimDefaultParameters();
        case 'rec'
            section = gtRecDefaultParameters();
        otherwise
            section = struct();
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
