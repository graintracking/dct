function finalStruct = gtMergeStructs(struct1, struct2, var)
% GTMERGESTRUCTS  Gather two structures into a single one.
%     -------------------------------------------------------------------------
%     finalStruct = gtMergeStructs(struct1, struct2, var)
% 
%     INPUT:
%       struct1     = <struct> 1st structure
%       struct2     = <struct> 2nd structure
%       var         = <string> name of variable added (as empty field) if not 
%                              present in the 2nd structure
%
%     OUTPUT:
%       finalStruct = <struct> resulting structure
%
%     Version 002 22-08-2012 by YGuilhem
%       Format header
%
%     Version 001 06-07-2012 by NVigano

    fn = [ fieldnames(struct1); fieldnames(struct2) ];
    tempStruct1 = struct2cell(struct1);
    tempStruct2 = struct2cell(struct2);
    if (any(size(tempStruct2) == 0))
        % In case the variable was not given, we make it an empty string
        struct2 = [];
        struct2.(var) = '';
        tempStruct2 = struct2cell(struct2);
        fn = [ fieldnames(struct1); fieldnames(struct2) ];
    end

    finalStruct = {cell2struct([tempStruct1; tempStruct2], fn, 1)};
end % end of function

