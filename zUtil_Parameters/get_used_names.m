function [used_vars, output_all, single_vars] = get_used_names(output_all, section_name, pattern, debug)
% GET_USED_NAMES  Makes a list of variable names used in the code, corresponding
%                 to to output_all list and also the unique list.
%
%     [used_vars, output_all, singles_vars] = get_used_names(output_all, section_name, [pattern], [debug])
%     ----------------------------------------------------------------------------------------------------
%     To be run after this command:
%       output_all = get_used_parameters(section_name)
%
%     INPUT:
%       output_all   = <cell>       information relative to the use of all the 
%                                   parameters for the specific section; it
%                                   contains the function in which it is used,
%                                   the line number, the line of the code (N,3)
%       section_name = <string>     field of parameters: i.e. 'acq'/'seg'....
%       pattern      = <string>     pattern for the grep command after
%                                   section_name {'\.(?<field>\w*)'}
%       debug        = <logiacal>   flag to display comments {false}
%
%     OUTPUT:
%       used_vars    = <cell>       list of variables used as listed in
%                                   output_all (N,1+)
%       output_all   = <cell>       information relative to the use of all the 
%                                   parameters for the specific section; it
%                                   contains the function in which it is used,
%                                   the line number, the line of the code (N,4)
%       single_vars = <cell>        same as used_vars, except that for each
%                                   entry only one variable is allowed. 
%                                   The length could be different from the original 
%                                   length of output_all (M,1)
%
%
%     Version 001 30-08-2013 by LNervo


if ~exist('pattern','var') || isempty(pattern)
    pattern = {[section_name '\.(?<fieldVar>\w*)']};
end
if ~exist('debug','var') || isempty(debug)
    debug = false;
end

if length(pattern) > 1 && ~isempty(strfind(pattern{1}, 'parameters'))
    pattern(1) = [];
end

if ~isempty(pattern)
    pattern = arrayfun(@(num) strcat(pattern{num}, '\.(?<fieldVar>\w*)'), 1:length(pattern), 'UniformOutput', false);
    pattern = arrayfun(@(num) strrep(pattern{num}, '\.\.', '\.'), 1:length(pattern), 'UniformOutput', false);
end

if strcmp(section_name, 'cryst') && ~isempty(pattern)
    if ~isempty(strfind(pattern{1}, [section_name '\.']))
        pattern = [pattern, arrayfun(@(num) strrep(pattern{num}, [section_name '\.'], [section_name '\(\w*\)\.']), 1:length(pattern), 'UniformOutput', false)];
    elseif ~isempty(strfind(pattern{1}, '\(\w*\)\.'))
        pattern = [pattern, arrayfun(@(num) strrep(pattern{num}, '\(\w*\)\.' , '\.'), 1:length(pattern), 'UniformOutput', false)];
    end
end

pattern = arrayfun(@(num) strrep(pattern{num}, '\.\.', '\.'), 1:length(pattern), 'UniformOutput', false);

out = GtConditionalOutput(debug);

out.odisp('Pattern used:')
arrayfun(@(num) out.odisp([sprintf('%2d)  ',num) pattern{num}]), 1:length(pattern))


% take field without section_name using tokens
for ii=1:length(pattern)
    tmp_vars{ii} = cellfun(@(num) regexp(num, pattern{ii}, 'tokens'), output_all(:,3), 'UniformOutput', false); % same size
    index{ii} = find(cellfun(@(num) ~isempty(num), tmp_vars{ii}));
end

% should be a matrix
tmp_vars = [tmp_vars{:}];

used_vars = arrayfun(@(num) [tmp_vars{num,:}], 1:size(tmp_vars,1), 'UniformOutput', false)';

% transform to cell array
used_vars = arrayfun(@(num) [used_vars{num}{:}], 1:length(used_vars), 'UniformOutput', false);

toremove = [];
for ii=1:length(used_vars)
    if ~isempty(used_vars{ii})
        used_vars{ii} = unique(used_vars{ii}); % same size
        
        ind = findValueIntoCell(used_vars{ii}, {'mat','html','txt','log','xml'});
        if ~isempty(ind)
            tmp = used_vars{ii};
            tmp(ind(2)) = [];
            used_vars{ii} = tmp;
            if isempty(used_vars{ii})
                toremove = [toremove; ii];
                out.odisp(['Empty variable at entry # ' num2str(ii)])
            end
            clear tmp
        end
    else
        toremove = [toremove; ii];
        out.odisp(['Empty variable at entry # ' num2str(ii)])
    end
end

% remove empties
output_all(toremove,:) = [];
used_vars(toremove(:)) = [];

tmp=[used_vars{:}]';
length_match = cellfun(@(num) size(num,2), used_vars);
single_vars = tmp;

% find not single values
index=find(length_match>1);
for ii=1:length(index)
    ind = index(ii);
    tmp{ind} = tmp(ind:ind+length_match(ind)-1)';
    tmp(ind+1:ind+length_match(ind)-1)=[];
end
% update variables
used_vars = tmp;

% remove empties
isEmpty = cellfun(@(num) isempty(num), used_vars);
output_all(isEmpty,:) = [];
used_vars(isEmpty(:)) = [];
isEmpty2 = cellfun(@(num) isempty(num), single_vars);
single_vars(isEmpty2(:)) = [];

if nargout > 1
    output_all(:,4) = used_vars;
end

end % end function get_used_names
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
