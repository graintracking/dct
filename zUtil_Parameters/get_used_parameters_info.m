function [output_all, section_name] = get_used_parameters_info(section_name, patternSingle, debug)
% GET_USED_PARAMETERS_INFO  Reads all the macro and grep all the uses of all the
%                           parameters for a specific section
%
%     [output_all, section_name] = get_used_parameters_info(section_name, [patternSingle], [debug])
%     ---------------------------------------------------------------------------------------------
%     It creates also a text file with the result of the search. The file is in
%     the Matlab home with filename like "all_<section_name>.log".
%
%     INPUT:
%       section_name  = <string>     field of parameters: i.e. 'acq'/'seg'....
%       patternSingle = <string>     pattern for the grep command after
%                                    section_name {'\.'}
%       debug         = <logical>    flag to display comments {false}
%
%     OUTPUT:
%       output_all    = <cell>       information relative to the use of all the 
%                                    parameters for the specific section; it
%                                    contains the function in which it is used,
%                                    the line number, the line of the code (N, 3)
%       section_name  = <cell>       updated section_name (was a field of
%                                    parameters: i.e. 'acq'/'seg'....)
%
%
%     Version 001 30-08-2013 by LNervo

%     TO DO: - add use of <section_name> without the '.';
%            - check for sub-structure like 'index'

global GT_MATLAB_HOME

if ~exist('patternSingle', 'var') || isempty(patternSingle)
    patternSingle = '\.';
end
if ~exist('debug', 'var') || isempty(debug)
    debug = false;
end

section_names = {'version', 'acq', 'xop', 'cryst', 'labgeo', 'samgeo', ...
    'prep', 'seg', 'match', 'index', 'fsim', 'rec', 'fed', 'oar'};

% check if it is a field of parameters
if any( ismember(section_name, section_names) )
    section_name = {['parameters\.' section_name], section_name};
elseif ~iscell(section_name)
    section_name = {section_name};
end

if ismember('cryst', section_name)
    section_name    = [section_name, {'cryst', 'cryst'}];
    pattern         = {'\(\w*\)\.', '\(\w*\)\.', '\(\w*\)\.', '\(\w*\)\.'};
    subsection_name = {'', '', ['sg' patternSingle], ['symm' patternSingle]};
elseif ismember('strategy', section_name)
    section_name    = {'index\.strategy', 'strategy', ...
                       'strategy', 'strategy', 'strategy', 'strategy', ...
                       'strategy', 'strategy', ...
                       'strategy', 'strategy'};
    pattern         = repmat({patternSingle}, 1, length(section_name));
    subsection_name = {'', '', ...
                       ['b' patternSingle], ['m' patternSingle], ['s' patternSingle], ['x' patternSingle], ...
                       ['b' patternSingle 'beg' patternSingle], ['b' patternSingle 'end' patternSingle], ...
                       ['m' patternSingle 'beg' patternSingle], ['m' patternSingle 'end' patternSingle]};
elseif ismember('index', section_name)
    section_name    = [section_name, {'index'}];
    pattern         = repmat({patternSingle}, 1, length(section_name));
    subsection_name = {'', '', ['strategy' patternSingle]};
else
    pattern         = repmat({patternSingle}, 1, length(section_name));
    subsection_name = repmat({''}, 1, length(section_name));
end

out = GtConditionalOutput(debug);

content = [];
Ccell = {};
for ii = 1:length(section_name)
    % prepare the command to get the use of fields of section_name
    textfile{ii} = fullfile( GT_MATLAB_HOME, [ 'all_' section_name{ii} '_' subsection_name{ii} '.log']);
    if ~isempty(strfind(textfile{ii}, '\.'))
        textfile{ii} = strrep(textfile{ii}, '\.', '_');
    end
    
    % take all the info
    cmd = ['find ' GT_MATLAB_HOME ' -iname "*.m" -exec grep "' section_name{ii} pattern{ii} subsection_name{ii} '" -n {} + ' ...
           '> ' textfile{ii}];
    [~, msg] = unix(cmd); out.odisp(msg)
    % reads the output
    
    if (ii == 2)
        % taking only extra matches: doing diff btw files
        tmpfile = [textfile{ii} '.tmp'];
        tmpcmd = ['diff ' textfile{ii-1} ' ' textfile{ii} ' > ' tmpfile];
        [~, msg] = unix(tmpcmd); out.odisp(msg)
        tmpcmd = ['sed -i -e "s/[<>] //g" ' tmpfile];
        [~, msg] = unix(tmpcmd); out.odisp(msg)
        
        delete(textfile{ii})
        movefile(tmpfile, textfile{ii})
    end
        
    [content(ii).Ccell, ~] = gtReadTextFile(textfile{ii}, '%s', [], false, 'Delimiter', '\n');
    
    Ccell = [Ccell; content(ii).Ccell{:}];
end

% save the fields
output_all = Ccell;

% split text into columns
output_all = cellfun(@(num) regexp(num, ':', 'split'), output_all, 'UniformOutput', false);
% find comments
% TO DO: better handle comments
%comments = cellfun(@(num) regexp(num, '\w*%\w*', 'match'), output_all, 'UniformOutput', false);
%iscomment = arrayfun(@(num2) any(cellfun(@(num) ~isempty(num), comments{num2})), 1:length(comments));
% remove comments
%output_all = output_all(~iscomment, :);
% merge bigger rows into 3 columns cell
output_all(find(cellfun(@(num) length(num), output_all) < 3)) = [];
length_output = cellfun(@(num) length(num), output_all);

ind = find(length_output > 3);
tmp = output_all(ind, :);
for ii = 1:length(ind)
    tmp{ii}{3} = [tmp{ii}{3} sprintf(':%s', tmp{ii}{4:end})];
    tmp{ii}(4:end) = [];
end
output_all(ind, :) = tmp;

% summary cell
output_all = reshape([output_all{:}], 3, [])'; % three columns cell array

% combine section_name, pattern, subsection_name
patterns = [section_name; pattern; subsection_name];
patterns(end+1, 1:end-1) = {' '};
patterns(end, end) = {''};
patterns = regexp([patterns{:}], ' ', 'split');
  
out.odisp(['Patterns used for section_name "' section_name{end} '":'])
out.odisp(patterns)

if (nargout > 1)
    section_name = patterns;
end

end % end function get_used_parameters
