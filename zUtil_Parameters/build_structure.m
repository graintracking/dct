function structure = build_structure(list, separator, debug)
% BUILD_STRUCTURE  Builds some structure starting from a list template in 'list'
%     structure = build_structure(list, [separator], [debug])
%     -------------------------------------------------------
%     
%     INPUT:
%       list          = <struct>     cell structure with names, description,
%                                    classes and usertypes columns
%       separator     = <string>     separator for sub-structures in list struct {'__'}
%                                    i.e. list.index__strategy --> parameters.index.strategy
%                                      'strategy' is a sub-structure of 'index'.
%                                    PS. list.match_calib --> 'match_calib' is a field of
%                                      'list'. 'calib' is not a sub-structure of 'match'.
%
%     OUTPUT:
%       structure     = <struct>     structure built from 'list'
%       substructures = <string>     array of sub-structures found in each field of
%                                    'structure'
%
%     Version 001 18-11-2011 by LNervo
%       For each sub-structure an empty structure is created properly and
%       the name is given in the output 'substructures'


if ~exist('separator', 'var') || isempty(separator)
    separator = '__';
end
if ~exist('debug', 'var') || isempty(debug)
    debug = false;
end

out = GtConditionalOutput(debug);

structure = [];
field     = struct();

if isstruct(list)
    names    = fieldnames(list);
    newnames = fieldnames(list);
    
    for ll = 1:length(names)
        if ~isempty(strfind(names{ll}, separator))
            tmp = names{ll};
            tmp = strrep(tmp, separator, '.'); % default separator is '__'
            newnames{ll} = tmp;
        end
    end

    for ii = 1:length(names)

        name = names{ii};
        out.odisp(['### structure ' name ' ###'])
        if ~isstruct(list.(name))
            for jj = 1:size(list.(name), 1)
                try
                    sub_field_name = list.(name){jj, 1};
                    sub_field_type = list.(name){jj, 3};
                catch mexc
                    name
                    jj
                    rethrow(mexc)
                end

                switch (lower(sub_field_type))
                    case 'char'
                        field.(name).(sub_field_name) = char([]);
                        %out.odisp('Found char...')
                    case 'single'
                        field.(name).(sub_field_name) = single([]);
                        %out.odisp('Found single...')
                    case 'double'
                        field.(name).(sub_field_name) = double([]);
                        %out.odisp('Found double...')    
                    case 'int8'
                        field.(name).(sub_field_name) = int8([]);
                        %out.odisp('Found int8...')
                    case 'int16'
                        field.(name).(sub_field_name) = int16([]);
                        %out.odisp('Found int16...')
                    case 'int32'
                        field.(name).(sub_field_name) = int32([]);
                        %out.odisp('Found int32...')
                    case 'int64'
                        field.(name).(sub_field_name) = int64([]);
                        %out.odisp('Found int64...')
                    case 'uint'
                        field.(name).(sub_field_name) = uint([]);
                        %out.odisp('Found uint...')
                    case 'uint8'
                        field.(name).(sub_field_name) = uint8([]);
                        %out.odisp('Found uint8...')
                    case 'uint16'
                        field.(name).(sub_field_name) = uint16([]);
                        %out.odisp('Found uint16...')
                    case 'uint32'
                        field.(name).(sub_field_name) = uint32([]);
                        %out.odisp('Found uint32...')
                    case 'uint64'
                        field.(name).(sub_field_name) = uint64([]);
                        %out.odisp('Found uint64...')
                    case 'logical'
                        field.(name).(sub_field_name) = false();
                        %out.odisp('Found logical...')
                    case 'cell'
                        field.(name).(sub_field_name) = cell(1, 1);
                        %out.odisp('Found cell...')
                    case 'struct'
                        field.(name).(sub_field_name) = struct();
                        out.odisp('Found structure...')
                    otherwise
                        field.(name).(sub_field_name) = NaN;
                end
                
            end % end for loop jj
        else
            out.odisp(['Found structure... Run again the function with input the structure <oldinput>.' name])
        end % end isstruct(list.(name))

    end % end for loop ii
    
    for ii = 1:length(names)

        if ~strcmp(names{ii}, newnames{ii})
            newname = textscan(newnames{ii}, '%s', 'delimiter', '.');
            newname = newname{1};
            switch size(newname, 1)
                case 1
                    field.(newname{1}) = field.(names{ii});
                case 2
                    field.(newname{1}).(newname{2}) = field.(names{ii});
                case 3
                    field.(newname{1}).(newname{2}).(newname{3}) = field.(names{ii});
                case 4
                    field.(newname{1}).(newname{2}).(newname{3}).(newname{4}) = field.(names{ii});
                case 5
                    field.(newname{1}).(newname{2}).(newname{3}).(newname{4}).(newname{5}) = field.(names{ii});
            end
            field = rmfield(field, names{ii});
        end

    end % end for loop ii
    
    structure = field;
else
    disp('Please give as input a structure');
    return;
end % end for loop isstruct(list)

end % end of function
