function [parameters, list] = make_parameters(version, debug)
% MAKE_PARAMETERS  Creates a parameters.mat file with empty fields
%     [parameters, list] = make_parameters(version, [debug])
%     ------------------------------------------------------
%
%     INPUT:
%       version    = <double>     -1 (loads a specific parameters.mat file)
%                                  0 (old parameters (<2011))
%                                  1 (single phase / old matching parameters)
%                                  2 (multiple phases parameters)
%       debug      = <logical>     print comments on screen {false}
%
%     OUTPUT:
%       parameters = <struct>      parameters file
%       list       = <struct>      cell structure with field names,
%                                  descriptions, classes and usertypes columns
%
%
%     Version 001 18-11-2011 by LNervo
%       Creates a template of parameters, then with gtSetup it is filled properly
%       Creates a .mat file with description for each field in the parameters.mat


if ~exist('debug','var') || isempty(debug)
    debug = false;
end

out = GtConditionalOutput(debug);

out.fprintf('Getting the field list of parameters...')

switch (version)
    case -1
        filename = inputwdefault('Insert filepath of the mat file to load', ...
            './parameters.mat');
        pars = load(filename, 'parameters');
        pars = pars.parameters;
        list       = complete_structure(pars, [], true, '__');       
        parameters = build_structure(list, '__');
    case 0
        list       = build_list_v0();
        parameters = build_structure(list, '__');
    case 1
        list       = build_list_v1();
        parameters = build_structure(list, '__');
    case 2
        list       = build_list_v2();
        parameters = build_structure(list, '__');
        parameters.version.number = 2;
end

out.fprintf('...Done!\n')

end % end of function
