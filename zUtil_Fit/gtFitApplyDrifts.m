function [pld, posd, sinthd] = gtFitApplyDrifts(pl, pos, dsp, om, drift, ...
                               energy)
% GTFITAPPLYDRIFTS Applies sample drifts to a set of reflections.
%
% [pld, posd, sinthd] = gtFitApplyDrifts(pl, pos, dsp, om, drift, energy)
% --------------------------------
% Applies the specified sample drifts in the Sample reference and returns 
% plane normals, positions and sin(theta) values after the drifts. 
%
% INPUT
%   pl    = plane normals before drifts (in deformed state) in Sample ref.
%           (3xn)
%   pos   = position in Sample reference before drifts (3xn)
%   dsp   = d-spacing (in deformed state) (1xn)
%   om    = omega-s on which to interpolate drifts (1xn)
%   drift     = structure defining drifts on an interpolation grid
%    .method  = interpolation method
%    .omega   = omega rotation angles at the interpolation grid points
%               (deg) (1xn) 
%    .posx    = X positions (1xn) 
%    .posy    = Y positions
%    .posz    = Z positions
%    .rotx    = rotations X Rodrigues vector component (1xn)
%    .roty    = rotations Y Rodrigues vector component
%    .rotz    = rotations Z Rodrigues vector component
%    .energy  = beam energies (keV) (1xn)
%
% OUTPUT
%   pld    = plane normals after drifts in the Sample ref. (3xn)
%   posd   = positions after drifts in the Sample ref. (3xn)
%   sinthd = sin(theta) values after drifts
%
%
% Version 001 18-04-2014 by P.Reischig
%


if isempty(drift)
    pld    = pl;
    posd   = pos;
    sinthd = 0.5*gtConvEnergyToWavelength(energy)./dsp;
    return
end


% ROTATION
if (isempty(drift.rotx) && isempty(drift.roty) && isempty(drift.rotz))
    gI   = [];
    pld  = pl;
    posd = pos;
else
    % Interpolate Rodrigues vectors for rotational drifts
    % (avoid using interp1, it's slower)
    
    % Set up interpolant object with arbitrary values
    rod = zeros(3, length(om));
    gI  = griddedInterpolant(drift.omega, drift.omega, drift.method);
    
    if ~isempty(drift.rotx)
        gI.Values = drift.rotx;
        rod(1,:)  = gI(om);
    end
    
    if ~isempty(drift.roty)
        gI.Values = drift.roty;
        rod(2,:)  = gI(om);
    end
    
    if ~isempty(drift.rotz)
        gI.Values = drift.rotz;
        rod(3,:)  = gI(om);
    end
    
    % Rotation matrix of the drift for each plane normal
    %  (3x3xn) -> (3x3n)
    rotdrift = reshape(gtFedRod2RotTensor(rod),3,[]);
    
    % Apply drift rotations to plane normals;
    % mutiply the corresponding matrices and vectors
    pl1 = reshape(pl,1,[]);
    pl2 = pl1([1 1 1],:) .* rotdrift;
    pld = pl2(:,1:3:end) + pl2(:,2:3:end) + pl2(:,3:3:end);
    pld = reshape(pld,3,[]);
    
    % Apply rotational drifts to grain centers
    gcd1 = reshape(pos,1,[]);
    gcd2 = gcd1([1 1 1],:) .* rotdrift;
    posd = gcd2(:,1:3:end) + gcd2(:,2:3:end) + gcd2(:,3:3:end);
    posd = reshape(posd,3,[]);
end

% TRANSLATION
if (~isempty(drift.posx) || ~isempty(drift.posy) || ~isempty(drift.posz))
    % Translate grain center according to interpolated drift
    % (avoid using interp1 - slower).

    % Set up interpolant object with arbitrary values
    if isempty(gI)
        gI = griddedInterpolant(drift.omega, drift.omega, drift.method);
    end
    
    if ~isempty(drift.posx)
        gI.Values = drift.posx;
        posd(1,:) = posd(1,:) + gI(om);
    end
    
    if ~isempty(drift.posy)
        gI.Values = drift.posy;
        posd(2,:) = posd(2,:) + gI(om);
    end
    
    if ~isempty(drift.posz)
        gI.Values = drift.posz;
        posd(3,:) = posd(3,:) + gI(om);
    end
end

% ENERGY
if isempty(drift.energy)
    wavelength = gtConvEnergyToWavelength(energy);
    sinthd     = 0.5*wavelength./dsp;
else
    % Calculate energies according to interpolated drift
    % (avoid using interp1 - it takes longer)
    gI = griddedInterpolant(drift.omega, drift.energy, drift.method);
    wavelength = gtConvEnergyToWavelength(gI(om));
    sinthd     = 0.5*wavelength./dsp;
end


end % of function
