function opt = gtFITParameters(spot, par, grain, parameters)
% GTFITPARAMETERS Fitting (optimisation) of average grain properties and
% global setup parameters.
%
% opt = gtFITParameters(spot, par, grain, parameters)
%
% INPUT
%   spot       - diffraction spot data (structure from gtINDEXIndividualSpots)
%   par        - fitting parameters (as from gtFitDefaultPars); if empty,
%                defaults are used
%   grain      - grain data structure from individual spot indeexing; it
%                needs to contain the 'spot' field; (cell array)
%
% OPTIONAL INPUT
%   parameters - as in parameters.mat; if undefined, it will be loaded from
%                the parameters file; used fields: acq, labgeo, samgeo.
%
% OUTPUT
%   opt  = updated optimised setup, grain and drift parameters
%    .labgeo   - updated labgeo
%    .energy   - updated energy (keV)
%	 .grain    - grain structure with the updated fitted properties; added
%                the strain field
%    .grainfit - concise, vectorised data of only the fitted grains (for 
%                statitics)
%	 .drift    - sample and energy drifts
%    .duvw_ini - initial u,v,w deviations
%    .duvw_end - optimised u,v,w deviations
%    .std_duvw_ini  - std of initial u,v,w deviations
%    .std_duvw_end  - std of optimised u,v,w deviations
%    .norm_duvw_ini - norm of all initial u,v,w deviations
%    .norm_duvw_end - norm of all optimised u,v,w deviations
%	 .jacobian - Jacobian matrix of variables and residuals (sparse matrix)
%    .par      - fitting parameters used 
%
%
% Run these functions beforehand:
%   index = load('4_grains/phase_##/index.mat')
%   grain1 = gtIndexAddInfo(index.grain);
%   [spot, grain2] = gtINDEXIndividualSpots(grain1);
%   par = gtFitDefaultPars()
%   opt = gtFITParameters(spot, par, grain2)
%
% Currently only handles a single phase. Hydrostatic strain (lattice 
% expansion) is not listed as an option, but can be found by fitting the 
% beam energy and adjusting the lattice parameters accordingly.
%
% In case of multiphase materials, fit global parameters first on the 
% phase of which lattice parameters are known most precisely (e.g.
% a stochiometric phase or has low solubility of alloying elements).
%
% Currently it does not use crystallography info, as the reference plane 
% normals of the grains are available in spot.plref. Thus any change in the
% crystallography (e.g. lattice parameters) has to be applied upstream 
% (before or in gtMATCHGUI).
%
% What cannot be fitted simultaneously:
%   1. detposZ  &  grain position
% 	   (Or another detpos variable parallel with the rotation axis.)
% 	   Translation along the rotation axis would be a free, arbitrary
%      variable.
% 
%   2. mean pixel size  &  grain position
%      The size of the entire setup would be scalable whilst still 
%      satisfying all constraints.
%      
%   3. beam energy  &  grain strain
% 	   The beam energy and hydrostatic strain have equivalent effects via
%      Bragg's law.
%
%   4. drifts, if scan was interlaced
%      In an interlaced scan each diffraction spot is combined from images 
%      recorded at several different omega rotational positions, thus
%      the spot centroid may have errors from drifts at various times.
% 
% To change default fitting parameters, load:
%   par = gtFitDefaultPars();
% And change as required.
%
% Which spots the different parameter types affect:
%   global parameters: affect all spot positions
%   grain parameters:  affect only the grain's spot positions
%   drift parameters:  affect spot positions in a limited omega range
%
%
% Version 002 13-01-2014 by P.Reischig
%   Changes in input and output structure, help, comments.
% Version 001 18-04-2014 by P.Reischig
%


% DEVELOPER'S NOTES
% 
% It uses spot.plref as the reference plane normal directions, so the
% fitted strain state is in addition to any pre-existing strain. However,
% spot.plref normally wouldn't account for any strain. It outputs zero
% strain for not fitted grains, overwrites whatever it was before.
% 
% Currently it does not use crystallography info, as the reference plane 
% normals of the grains are available in spot.plref.
%
% Notes on nomenclature:
%   fitted parameters: optimum setup parameters and grain properties to be found      
%   target parameters: components of the optimisation target function     


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Prepare input
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% if (nargin>3) && strcmp(varargin{1},'par')
%     par = varargin{2};
%     varargin(1:2) = [];
%     par.fit_drift_allpos = false;
%     par.fit_drift_allrot = false;
% else
%     par = gtFitDefaultPars();
%     par.fit_drift_allpos = false;
%     par.fit_drift_allrot = false;
% end
% par = parse_pv_pairs(par, varargin);
% disp('Fitting parameters used:')
% disp(par)

% grain.spot field needed
% if isempty(grain)
%     fname = sprintf('4_grains/phase_%02d/index.mat', par.phase);
%     fprintf('Loading grain info from file %s\n', fname)
%     index = load(fname);
%     grain = index.grain;
% end

if isempty(par)
	par = gtFitDefaultPars();
end

% Currently no need for crystallography, as plref is available in spot.plref 
% if isempty(par.cryst)
%     fname = sprintf('4_grains/phase_%02d/index.mat', par.phase);
%     fprintf('Loading crystallographic info from file %s\n', fname)
%     index = load(fname);
%     par.cryst = index.cryst;
% end
% par.latticepar = par.cryst.latticepar;
% par.Bmat       = gtCrystHKL2CartesianMatrix(par.latticepar);

if (~exist('parameters','var') || isempty(parameters))
    disp('Loading parameters from parameters file...')
    parameters = gtLoadParameters();
end

if isempty(par.energy)
    par.energy = parameters.acq.energy;
end

if isempty(par.labgeoini)
    disp('Using initial labgeo parameters from the parameters file.')
    if (isfield(parameters, 'detgeo'))
        par.labgeoini = gtGeoConvertDetgeo2LegacyLabgeo(parameters.detgeo, parameters.labgeo);
    else
        par.labgeoini = parameters.labgeo;
    end
end

par.labgeoini.detorig = gtGeoDetOrig(par.labgeoini);
par.labgeoini.detnorm = cross(par.labgeoini.detdiru, par.labgeoini.detdirv);

par.wavelength = gtConvEnergyToWavelength(par.energy);
par.samgeo     = parameters.samgeo;
par.omstep     = 180/parameters.acq.nproj;
par.numimages  = 2*parameters.acq.nproj;


% Check available Optimization Toolbox License
ok = license('checkout','Optimization_Toolbox');
if ~ok
    disp('Could not check out Optimization Toolbox license.')
    return
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Check fitting options
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if (par.fit_global_detposZ && par.fit_grain_position)
	disp('If the rotation axis is not parallel with Z, you can ignore the following warning.')
	disp('Note: translation along the rotation axis would be a free, arbitrary variable.')
	error('Cannot fit both the detector position along the rotation axis and the grain positions.')
end

if (par.fit_global_pixmean && par.fit_grain_position)
	disp('Note: with the current settings the size of the entire setup would be scalable.') 
	error('Cannot fit both the mean pixel size and the grain positions.')
end

if (par.fit_global_energy && par.fit_grain_strain)
	disp('Note: the beam energy and hydrostatic strain have equivalent effects.') 
	error('Cannot fit both the beam energy and grain strain.')
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Global variables
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% old variables: all geometry variables that could be fitted and their
%                initial value
% new variables: global variables that are actually being fitted

% Order of fitted parameters in newvars and oldvars:
fitname{1}  = 'Position X';
fitname{2}  = 'Position Y';
fitname{3}  = 'Position Z';
fitname{4}  = 'Tilt in plane';    % rad
fitname{5}  = 'Angle U-V';        % rad
fitname{6}  = 'Tilt around U';    % rad
fitname{7}  = 'Tilt around V';    % rad
fitname{8}  = 'Pixel size mean';
fitname{9}  = 'Pixel size ratio';
fitname{10} = 'Energy';
fitname{11} = 'Rot wedge angle';



% Indices of fitted labgeo parameters
activeglobal = false(11,1);
if par.fit_global_detposX;  activeglobal(1)  = true; end
if par.fit_global_detposY;  activeglobal(2)  = true; end
if par.fit_global_detposZ;  activeglobal(3)  = true; end
if par.fit_global_detroll;  activeglobal(4)  = true; end
if par.fit_global_detangUV; activeglobal(5)  = true; end
if par.fit_global_dettiltU; activeglobal(6)  = true; end
if par.fit_global_dettiltV; activeglobal(7)  = true; end
if par.fit_global_pixmean;  activeglobal(8)  = true; end
if par.fit_global_pixratio; activeglobal(9)  = true; end
if par.fit_global_energy;   activeglobal(10) = true; end
if par.fit_global_rotwedge; activeglobal(11) = true; end

fitnameused = fitname(activeglobal);

disp('Fitted acquisition parameters:')
disp(fitnameused')
                   
% Number of global fitted parameters
par.nvarglobalall = length(activeglobal);
par.nvarglobal    = sum(activeglobal);

tStart = tic;

%h_fitpars = figure('name','Fitted parameters','Units','normalized','Position',[0.6 0.6 0.4 0.4]);

strainT     = [];
strainTmean = [];
residual{2} = [];

%figure('Name','Residuals after final fit (excl. outliers)','Units','normalized','Position',[0.6 0.6 0.4 0.4])

disp('Optimised parameters:')
disp(fitnameused)
%disp(varsopt(:,1:sum(newind)))
disp(' ')

%figure(h_fitpars);
%plot(varsopt,'.-')
%legend(fitnameused,'Location','best')



% Old global variables: the starting point for fitting
% Geometry
iniglobal    = zeros(11,1);
iniglobal(1) = par.labgeoini.detrefpos(1);
iniglobal(2) = par.labgeoini.detrefpos(2);
iniglobal(3) = par.labgeoini.detrefpos(3);
iniglobal(4) = 0;
iniglobal(5) = acos(par.labgeoini.detdiru * par.labgeoini.detdirv');
iniglobal(6) = 0;
iniglobal(7) = 0;
iniglobal(8) = 1000*0.5*(par.labgeoini.pixelsizeu + par.labgeoini.pixelsizev);
iniglobal(9) = par.labgeoini.pixelsizeu/par.labgeoini.pixelsizev;
iniglobal(10)= par.energy;
iniglobal(11)= 0;

% Initial guesses for active fitted global variables
guess = iniglobal(activeglobal);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Drift variables
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Drift data is stored in 'drift'. Active Those variables that are not 
%   drift.om   - omega rotation angle; input points for interpolation 
%   drift.ipol - interpolation method
%   drift.nfit - no. of fitted points; normally length(omega)-1 because 
%                the initial point is the reference which is not fitted
% 

% Parameters for interpolation
drift.omega  = par.drift_omega;
drift.method = par.drift_method;
drift.nfit   = max(length(par.drift_omega) - 1, 0);
numom        = length(par.drift_omega);


% Setting active variables

% if par.fit_drift_allpos
%     par.fit_drift_posX = true;
%     par.fit_drift_posY = true;
%     par.fit_drift_posZ = true;
% end
% 
% if par.fit_drift_allrot
%     par.fit_drift_rotX = true;
%     par.fit_drift_rotY = true;
%     par.fit_drift_rotZ = true;
% end


% Active variable indices
nind = par.nvarglobal + 1;

if par.fit_drift_posX 
    drift.posx             = zeros(1,numom);
    par.ind.fit_drift_posX = nind : (nind + drift.nfit - 1);
    nind                   = nind + drift.nfit;
else
    drift.posx             = [];
    par.ind.fit_drift_posX = [];
end

if par.fit_drift_posY
    drift.posy             = zeros(1,numom);
    par.ind.fit_drift_posY = nind : (nind + drift.nfit - 1);
    nind                   = nind + drift.nfit;
else
    drift.posy             = [];
    par.ind.fit_drift_posY = [];
end

if par.fit_drift_posZ
    drift.posz             = zeros(1,numom);
    par.ind.fit_drift_posZ = nind : (nind + drift.nfit - 1);
    nind                   = nind + drift.nfit;
else
    drift.posz             = [];
    par.ind.fit_drift_posZ = [];
end

if par.fit_drift_rotX
    drift.rotx             = zeros(1,numom);
    par.ind.fit_drift_rotX = nind : (nind + drift.nfit - 1);
    nind                   = nind + drift.nfit;
else
    drift.rotx             = [];
    par.ind.fit_drift_rotX = [];
end

if par.fit_drift_rotY
    drift.roty             = zeros(1,numom);
    par.ind.fit_drift_rotY = nind : (nind + drift.nfit - 1);
    nind                   = nind + drift.nfit;
else
    drift.roty             = [];
    par.ind.fit_drift_rotY = [];
end

if par.fit_drift_rotZ
    drift.rotz             = zeros(1,numom);
    par.ind.fit_drift_rotZ = nind : (nind + drift.nfit - 1);
    nind                   = nind + drift.nfit;
else
    drift.rotz             = [];
    par.ind.fit_drift_rotZ = [];
end

if par.fit_drift_energy
    drift.energy             = par.energy(1, ones(1,numom));
    par.ind.fit_drift_energy = nind : (nind + drift.nfit - 1);
    nind                     = nind + drift.nfit;
else
    drift.energy             = [];
    par.ind.fit_drift_energy = [];
end


par.nvardrift = drift.nfit * (par.fit_drift_energy + ...
    par.fit_drift_posX + par.fit_drift_posY + par.fit_drift_posZ + ...
    par.fit_drift_rotX + par.fit_drift_rotY + par.fit_drift_rotZ);


% Add initial guesses for fitted drift variables
iniglobal = [iniglobal; drift.posx(2:end)'];
iniglobal = [iniglobal; drift.posy(2:end)'];
iniglobal = [iniglobal; drift.posz(2:end)'];
iniglobal = [iniglobal; drift.rotx(2:end)'];
iniglobal = [iniglobal; drift.roty(2:end)'];
iniglobal = [iniglobal; drift.rotz(2:end)'];
iniglobal = [iniglobal; drift.energy(2:end)'];

guess = [guess; iniglobal((par.nvarglobalall+1):end)];

par.iniglobal = iniglobal;
par.drift     = drift;


%%%%%%%%%%%%%%%%%%%%%%%%
% Grain variables
%%%%%%%%%%%%%%%%%%%%%%%%

% Reorganised and checked grain data in a structure array 'gr' that
% excludes not active grains. Grain parameters added to guess parameters.
[gr, spotgr, guess] = sfGrainInfo(grain, spot, guess, par);

par.nvargrain = length(guess) - par.nvarglobal - par.nvardrift;

% Jacobian pattern: a sparse matrix showing dependencies of target parameters
% from fitted parameters
fprintf('No. of spots input for fitting; %d\n', length(spot.omind))
disp('Creating Jacobian pattern...')
tic
JacPatt = gtFitJacobianPattern(guess, spotgr, gr, par);
toc

% Initial value of target function
duvw   = gtFitSpotPosDeviations(guess, activeglobal, spotgr, gr, par);
resini = duvw;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Run fitting
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Matlab otimisation settings
optset = optimset(...
   'Algorithm', par.algorithm, 'FunValCheck','off', ...
   'Display','iter', 'Diagnostics','off', 'Jacobian','off', ...
   'DiffMinChange',par.finitediff, 'DiffMaxChange',par.finitediff, 'MaxIter',par.maxiter, ...
    'TolFun', par.tolfun, 'TolX',par.tolvar, 'MaxFunEvals',par.maxfunevals, ...
   'FinDiffType','forward', 'PlotFcns',par.plotfun);
   
% Jacobian pattern
optset.JacobPattern = JacPatt;

% Using Matlab's built-in non-linear least square solver lsqnonlin 
tic
[varsopt, resnorm, resend, exitflag, optimout, lagrange, jacobian] = ...
    lsqnonlin(@(newvars) gtFitSpotPosDeviations(newvars, activeglobal, spotgr, gr, par),...
    guess, [], [], optset);
toc


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Update fitted variables
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Active, fitted grains in a vector for statistics: grainfit
grainfit.center  = vertcat(gr(:).center)';
grainfit.Rvec    = vertcat(gr(:).Rvec)';
grainfit.strain  = zeros(6, length(gr));  % vertcat(gr(:).strain)';
grainfit.origind = zeros(1, length(gr));

% Extract updated parameters from optimisation results
for ii = 1:length(gr)
    if par.fit_grain_position
        grainfit.center(:,ii) = varsopt(gr(ii).indcenter);
    end
    if par.fit_grain_orientation
        grainfit.Rvec(:,ii)   = varsopt(gr(ii).indRvec);
    end
    if par.fit_grain_strain
		if ~isempty(gr(ii).indstrain)
			grainfit.strain(:,ii) = varsopt(gr(ii).indstrain);
		end
	end
	grainfit.origind(ii) = gr(ii).origind;
end


% Update final grain data from optimisation results
for ii = 1:length(grain)
	grain{ii}.strain.strainT  = zeros(3,3);
    grain{ii}.strain.fitted   = false;
	grain{ii}.strain.coplanar = false;
end

for ii = 1:length(gr)
    if par.fit_grain_position
        grain{gr(ii).origind}.center = varsopt(gr(ii).indcenter)';
    end
    if par.fit_grain_orientation
        grain{gr(ii).origind}.Rvec = varsopt(gr(ii).indRvec)';
    end
    if par.fit_grain_strain
		if isempty(gr(ii).indstrain)
			grain{gr(ii).origind}.strain.coplanar = true;
		else
			st = varsopt(gr(ii).indstrain)';
			grain{gr(ii).origind}.strain.fitted  = true;
			grain{gr(ii).origind}.strain.strainT = [st(1) st(6) st(5); ...
				                                    st(6) st(2) st(4); ...
				                                    st(5) st(4) st(3)];
		end
	end
end

% Global
[detgeo, labgeo] = gtGeoConvertLegacyLabgeo2Detgeo(par.labgeoini);
if any(activeglobal)
    [labgeo, detgeo, energy] = gtFitUpdateLabgeo(varsopt(1:sum(activeglobal)), ...
                       activeglobal, par.iniglobal, labgeo, detgeo);
else
    energy = par.iniglobal(10);
end
if (isfield(labgeo, 'detnorm'))
    labgeo = rmfield(labgeo,'detnorm');
end
if (isfield(labgeo, 'detorig'))
    labgeo = rmfield(labgeo,'detorig');
end

% Drifts
drift = par.drift;
if ~isempty(par.ind.fit_drift_posX); drift.posx = [0, varsopt(par.ind.fit_drift_posX)']; end
if ~isempty(par.ind.fit_drift_posY); drift.posy = [0, varsopt(par.ind.fit_drift_posY)']; end
if ~isempty(par.ind.fit_drift_posZ); drift.posz = [0, varsopt(par.ind.fit_drift_posZ)']; end
if ~isempty(par.ind.fit_drift_rotX); drift.rotx = [0, varsopt(par.ind.fit_drift_rotX)']; end
if ~isempty(par.ind.fit_drift_rotY); drift.roty = [0, varsopt(par.ind.fit_drift_rotY)']; end
if ~isempty(par.ind.fit_drift_rotZ); drift.rotz = [0, varsopt(par.ind.fit_drift_rotZ)']; end
if ~isempty(par.ind.fit_drift_energy); drift.energy = [0, varsopt(par.ind.fit_drift_energy)']; end

% Plot strain
meanstrain = mean(grainfit.strain, 2);

if ~isempty(strainTmean)
    figure('name','Average normal strain components','Units','normalized','Position',[0 0.6 0.4 0.4])
    hold on
    plot(meanstrain(1),'-r.')
    plot(meanstrain(2),'-g.')
    plot(meanstrain(3),'-b.')
    
    figure('name','Average shear strain components','Units','normalized','Position',[0 0.6 0.4 0.4])
    hold on
    plot(meanstrain(4),'-r.')
    plot(meanstrain(5),'-g.')
    plot(meanstrain(6),'-b.') 
end


% figure('name','Jacobian components')
% for ii = 1:size(varsopt,2)
%     subplot(2,size(varsopt,2),ii)
%     hist(jacobian{1}(:,ii),100)
%     subplot(2,size(varsopt,2),size(varsopt,2)+ii)
%     hist(jacobian{end}(:,ii),100)    
% end

% Output structure
opt.detgeo        = detgeo;
opt.labgeo        = labgeo;
opt.energy        = energy;
opt.grain         = grain;
opt.grainfitted   = grainfit;  % vectorised data of only active,fitted grains
opt.drift         = drift;
opt.duvw_ini      = reshape(resini, [], 3);
opt.duvw_end      = reshape(resend, [], 3);
opt.norm_duvw_ini = sum(opt.duvw_ini(:).^2);
opt.norm_duvw_end = sum(opt.duvw_end(:).^2);
opt.std_duvw_ini  = std(opt.duvw_ini);
opt.std_duvw_end  = std(opt.duvw_end);
opt.jacobian      = jacobian;
opt.par           = par;
% These don't make sense until they are normalised
%opt.std_dall_ini  = std(opt.duvw_ini(:));
%opt.std_dall_end  = std(opt.duvw_end(:));


disp(' ')
disp('Initial std of all spot coordinates (u,v,w):')
disp([opt.std_duvw_ini])
disp('Final std of all spot coordinates (u,v,w):')
disp([opt.std_duvw_end])
disp('Initial and final norm of all residuals:')
disp([opt.norm_duvw_ini, opt.norm_duvw_end])

if any([par.fit_drift_posX, par.fit_drift_posY, par.fit_drift_posZ, ...
        par.fit_drift_rotX, par.fit_drift_rotY, par.fit_drift_rotZ, ...
        par.fit_drift_energy]);
    gtFITPlotDrifts(drift);
end

sfPlotRes(opt)

fprintf('\nTotal elapsed time (sec): %g\n', toc(tStart))
beep, pause(0.5), beep, pause(0.5), beep

end % of function




%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SUB-FUNCTIONS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function [gr, spotgr, guess] = sfGrainInfo(grain, spot, guess, par)
% Recreate grain info of active grains in concise form (structure array)   
    
    gr = struct('center',{},'Rvec',{},'strain',{},'numspots',{},'coplanar',{},'plref',{},...
                'plsam',{},'dspref',{},'indcenter',{},'indRvec',{},...
                'indstrain',{},'mesu',{},'mesv',{},'mesw',{},...
                'simu',{},'simv',{},'simw',{});

    % Loop through all grains, gather data of only the active grains
    gi    = 1;
    spind = false(length(spot.grainind),1); 
    
    for ii = 1:length(grain)
        
        if ~grain{ii}.active
            continue
        end
        
        ind0 = length(guess);
        
        gr(gi).origind = ii;
        
        gr(gi).numspots = length(grain{ii}.spot.difspotid);
        gr(gi).center = grain{ii}.center;
        gr(gi).Rvec   = grain{ii}.R_vector;        
        gr(gi).plref  = grain{ii}.spot.plref;
        gr(gi).dspref = grain{ii}.spot.dspref;
        gr(gi).plsam  = grain{ii}.spot.plsam0;

        gr(gi).simu   = grain{ii}.spot.simu;
        gr(gi).simv   = grain{ii}.spot.simv;
        gr(gi).simw   = grain{ii}.spot.simw;

        gr(gi).mesu   = grain{ii}.spot.mesu;
        gr(gi).mesv   = grain{ii}.spot.mesv;
        gr(gi).mesw   = grain{ii}.spot.mesw;

        gr(gi).indspot = grain{ii}.spot.spotind; 
        
        spind(grain{ii}.spot.spotind) = true;
        
        if par.fit_grain_position
			% indices of grain center in the vector of all fitted parameters 
            gr(gi).indcenter = (ind0+1) : (ind0+3);
            guess = [guess; gr(gi).center'];
            ind0  = length(guess);
        end
        
        if par.fit_grain_orientation
			% indices of Rvec in the vector of all fitted parameters
			gr(gi).indRvec = (ind0+1) : (ind0+3);
            guess = [guess; gr(gi).Rvec'];
            ind0  = length(guess);      
        end
        
        if par.fit_grain_strain       
            % Check that plane normals are not all in one plane (determinant is
            % significantly larger than 0 or some tolerance)
            detlim = 1;
            pl     = grain{ii}.spot.plref;
            detpl  = det(pl*pl');
            
            if (detpl >= detlim)
                % Plane normals are not coplanar
                gr(gi).strain    = [0, 0, 0, 0, 0, 0];
                gr(gi).coplanar  = false;
				% indices of strain components in the vector of all fitted parameters
                gr(gi).indstrain = (ind0+1) : (ind0+6);
                guess            = [guess; zeros(6,1)];
            else
                % Plane normals are coplanar
                gr(gi).strain    = [];
                gr(gi).coplanar  = true;
                gr(gi).indstrain = [];
            end
        end
        
        gi = gi + 1;
    end
    
    % Keep only those spots which belong to one of the active grains
    fs = fieldnames(spot);
    for ii = 1:length(fs)
        spotgr.(fs{ii}) = spot.(fs{ii})(spind,:);     
    end

end


function sfPlotRes(opt)
% Plots the initial and optimised residuals
    hf = figure('name', 'Histograms of initial and optimised errors of spot positions');
	
	h1 = subplot(1,3,1);
	[ne, be] = hist(opt.duvw_end(:,1), 100);
	ni = hist(opt.duvw_ini(:,1), be);
	bar(be,ne)
	hold on
	plot(be, ni, 'r-', 'LineWidth',2)
	title(h1, 'U coordinates', 'FontWeight','bold', 'FontSize',14)
	
	h2 = subplot(1,3,2);
	[ne, be] = hist(opt.duvw_end(:,2), 100);
	ni = hist(opt.duvw_ini(:,2), be);
	bar(be, ne)
	hold on
	plot(be, ni, 'r-', 'LineWidth',2)
	title(h2, 'V coordinates', 'FontWeight','bold', 'FontSize',14)
	
	h3 = subplot(1,3,3);
	[ne, be] = hist(opt.duvw_end(:,3), 100);
	ni = hist(opt.duvw_ini(:,3), be);
	bar(be, ne)
	hold on
	plot(be, ni, 'r-', 'LineWidth',2)
	title(h3, 'W coordinates', 'FontWeight','bold', 'FontSize',14)
	
	legend({'Optimised','Initial'})
	
	set(hf, 'Units','normalized', 'Position',[0 0.3 1 0.6])

end % sfPlotRes


%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Unused
%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Typical magnitudes to scale finite differences for computing derivatives
% typ    = zeros(11,1);
% typ(1) = par.typ_detrefpos;
% typ(2) = par.typ_detrefpos;
% typ(3) = par.typ_detrefpos;
% typ(4) = par.typ_dettilt;
% typ(5) = par.typ_detangUV;
% typ(6) = par.typ_dettilt;
% typ(7) = par.typ_dettilt;
% typ(8) = par.typ_pixmean;
% typ(9) = par.typ_pixratio;
% typ(10)= par.typ_energy;
% typ(11)= par.typ_rotwedge;
% typ    = typ(activeglobal);

