function duvw = gtFitSpotPosDeviations(newvars, activeglobal, spot, gr, par)
% GTFITSPOTPOSDEVIATIONS Computes the deviations in (u,v,w) between
% simulated and measured values.
%
% OUTPUT
%   duvw  - deviations in u,v,w in one vector (3*no_of_grains,1):
%           [all_du; all_dv, all_dw]
%
% If no strain of the grains is fitted, it uses gr.plref to compute spot
% positions. So it may account for existing strain, depending on how
% gr.plref was computed.
%
%
% Version 001 18-04-2014 by P.Reischig
%


% Currently monochromatic mode only

% If polychromatic
%if isnan(energy)                                 
    % Fwd simulated U,V,W for spots
    %uvw = sfFwdSim_poly(pairs, pairs.om, ...
    %                    drift, labgeo, samgeo, Qdet, omstep, energy);
    
% If monochromatic
%else
    % Fwd simulated U,V,W
    duvw = sfFwdSim_mono(newvars, activeglobal, spot, gr, par);
%end


end % of main function


%%%%%%%%%%%%%%%%%%%%%%
%%% SUB-FUNCTIONS
%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%
%%% Monochromatic
%%%%%%%%%%%%%%%%%%%%%%

function duvw = sfFwdSim_mono(newvars, activeglobal, spot, gr, par) 
% OUTPUT
%   duvw  - deviations in u,v,w in one vector (3*ng,1):
%           [all du; all dv, all dw]

if (par.niter_drift < 3)
    error('Parameter niter_drift has to be min. 3.')
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Update grain parameters; no drifts applied
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

sp.gcenter = zeros(3, length(spot.grainind));

if (par.fit_grain_orientation || par.fit_grain_strain)
    sp.pl = zeros(3, length(spot.grainind));
else
    sp.pl = [gr(:).plsam];
end

if (par.fit_grain_strain)
    sp.dsp = zeros(1, length(spot.grainind));
else
    sp.dsp = [gr(:).dspref];
end
   
for ii = 1:length(gr)
    
    if (par.fit_grain_position)
        % Grain center before drifts
        gc = newvars(gr(ii).indcenter);
    else
        gc = gr(ii).center';
    end
    
    sp.gcenter(:,gr(ii).indspot) = gc(:, ones(1,gr(ii).numspots));
     
    if (par.fit_grain_orientation || par.fit_grain_strain)
        % Rotation tensor describing crystal orientation;
        % (gtCrystRodriguesVector and gtFedRod2RotTensor (or Rod2g) used together
        % gives the transformation tensor from CRYSTAL TO SAMPLE coordinates)
        cry2sam = gtFedRod2RotTensor(newvars(gr(ii).indRvec));
        
        % Plane normals in the undrifted SAMPLE reference
        plsam = cry2sam * gr(ii).plref;
        sp.pl(:,gr(ii).indspot)  = plsam;
    end
    
    if (~par.fit_grain_strain)
        continue
    end
    
    % Impose strain on grain
    if (isempty(gr(ii).indstrain))
        % No strain info, using unstrained plane normals
        dsp = gr(ii).dspref;
    else
        % New deformed plane normals and relative elongations        
        str = newvars(gr(ii).indstrain);
        strainT = [str(1) str(6) str(5) ; ...
                   str(6) str(2) str(4) ; ...
                   str(5) str(4) str(3)];
        
        [plsam, drel] = gtStrainPlaneNormals(plsam, strainT);
        
        % d-spacing after applying the strain tensor
        dsp = drel .* gr(ii).dspref;
    end
    
    sp.pl(:,gr(ii).indspot)  = plsam;
    sp.dsp(:,gr(ii).indspot) = dsp;
end


%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Update labgeo
%%%%%%%%%%%%%%%%%%%%%%%%%

[detgeo, labgeo] = gtGeoConvertLegacyLabgeo2Detgeo(par.labgeoini);
% Recalculate labgeo
if any(activeglobal)    
    [labgeo, detgeo, energy] = gtFitUpdateLabgeo( ...
        newvars(1:sum(activeglobal)), activeglobal, par.iniglobal, labgeo, detgeo);
else
    energy = par.iniglobal(10);
end

% Rotation tensor components
rotcomp = gtMathsRotationMatrixComp(labgeo.rotdir','col');

% In case the Sample axes are not aligned with the Lab axes at omega=0,
% the plane normals need to be brought in the Lab reference where the
% beam direction and rotation axis are defined.
% For free vectors it's a rotation (samgeo.dir norm is
% always 1, so sam2lab will preserve vector norms).
% The rotation angle omega is considered zero at this point.
sam2lab0 = [par.samgeo.dirx', par.samgeo.diry', par.samgeo.dirz'];
    
if (all(sam2lab0(:) == [1 0 0 0 1 0 0 0 1]'))
    sam2lab0 = [];
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Compute omega and drifts
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
drift = par.drift;
if ~isempty(par.ind.fit_drift_posX); drift.posx = [0, newvars(par.ind.fit_drift_posX)']; end
if ~isempty(par.ind.fit_drift_posY); drift.posy = [0, newvars(par.ind.fit_drift_posY)']; end
if ~isempty(par.ind.fit_drift_posZ); drift.posz = [0, newvars(par.ind.fit_drift_posZ)']; end
if ~isempty(par.ind.fit_drift_rotX); drift.rotx = [0, newvars(par.ind.fit_drift_rotX)']; end
if ~isempty(par.ind.fit_drift_rotY); drift.roty = [0, newvars(par.ind.fit_drift_rotY)']; end
if ~isempty(par.ind.fit_drift_rotZ); drift.rotz = [0, newvars(par.ind.fit_drift_rotZ)']; end
if ~isempty(par.ind.fit_drift_energy); drift.energy = [0, newvars(par.ind.fit_drift_energy)']; end

% The omega has to be iterated, because it's not known initially
% when drifts are present, as drifts are also dependent on omega.
% Test showed that 3 iterations provide enough precision ().

% ITERATION STEP 1
% Plane normals and sin(theta) after drifts in the Sample reference
% Omega input in degrees !!
[pld, ~, sp.sinth] = gtFitApplyDrifts(sp.pl, sp.gcenter, ...
    sp.dsp, spot.mesw' * par.omstep, drift, energy);
% Plane normals in Lab reference after drifts, but before rotation
if ~isempty(sam2lab0)
    pld = sam2lab0 * pld;
end

% ITERATION STEP 2..N-1
for ii = 1:par.niter_drift-2
    % Keep grain centers before the drift as the base; update omega
    sp.om = gtFedPredictOmegaMultiple(pld, sp.sinth, labgeo.beamdir', ...
        labgeo.rotdir', rotcomp, spot.omind');

    [pld, ~, sp.sinth] = gtFitApplyDrifts(sp.pl, sp.gcenter, sp.dsp, sp.om, ...
                         drift, energy);
    
    if ~isempty(sam2lab0)
        pld = sam2lab0 * pld;
    end
end

% ITERATION STEP LAST
[sp.om, ~, ~, sp.rot] = gtFedPredictOmegaMultiple(pld, sp.sinth, ...
                      labgeo.beamdir', labgeo.rotdir', rotcomp, spot.omind');
[sp.pl, sp.gcenter, sp.sinth] = gtFitApplyDrifts(sp.pl, sp.gcenter, ...
                                sp.dsp, sp.om, drift, energy);


% Plane normal in Lab reference at omega
sp.pl = gtGeoSam2Lab(sp.pl', sp.om, labgeo, par.samgeo, true, 'only_rot', true)';

% From degrees to images:
sp.om = sp.om/par.omstep;

% Check if all reflections occur
%disp('Computing residuals...')
if any(isnan(sp.om))
    %[spot.simw, spot.mesw, om2', sp.om']
    warning('No reflection was predicted for some planes.');
end

% Absolute difference between predicted and measured ():
dom = sp.om - spot.mesw';

% Find the indices with the smallest absolute difference; consider
% errors and periodicity of 360deg in omega
[~, ind] = min( abs( [dom; dom - par.numimages; dom + par.numimages] ) , [], 1);

% Pedicted omega closest to measured
sp.om = sp.om - (ind == 2)*par.numimages + (ind == 3)*par.numimages;


%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Predict spot U,V
%%%%%%%%%%%%%%%%%%%%%%%%%   

% Diffraction vector
sp.dveclab = gtFedPredictDiffVecMultiple(sp.pl, labgeo.beamdir');

% Absolute detector coordinates U,V in pixels
uv = gtFedPredictUVMultiple(sp.rot, sp.dveclab, sp.gcenter, detgeo.detrefpos', ...
    detgeo.detnorm', detgeo.Qdet, [detgeo.detrefu; detgeo.detrefv]);

% Absolute coordinates U,V,W (pixel,pixel,image); to account for
% measurement errors, do not correct for the image number
% to be mod(om,360)/omstep
%sp.uvw = [uv; sp.om];
     
% Difference between simulated and measured spot positions    
%duvw = sp.uvw - [spot.mesu'; spot.mesv'; spot.mesw'];

duvw = [uv', sp.om'] - [spot.mesu, spot.mesv, spot.mesw];

check3 = [abs(duvw(:,1)) > par.warning_lim_u, abs(duvw(:,2)) > par.warning_lim_v, ...
          abs(duvw(:,3)) > par.warning_lim_w];
check1 = any(check3, 2);


if any(check1)
    disp('WARNING! Large error in position of diff. spots:')
    disp(find(check1)')
end


duvw = duvw(:);


end % sfFwdSim_mono


%%%%%%%%%%%%%%%%%%%%%%
%%% Polychromatic
%%%%%%%%%%%%%%%%%%%%%%

function uvw = sfFwdSim_poly(pairs, om, drift, labgeo, ...
                samgeo, Qdet, omstep, energy)

% Use the measured omega position of the images to calculate
% the drifts.

error('Function is under development...' )

    % Predicted plane normals, grain centers and sin(theta) after drifts
    % in the Sample reference
    [pld, gcd] = gtFitApplyDrifts(pairs.pl, pairs.center, ...
                 pairs.dsp, om, drift, energy);
    
                     
    % The plane normals and grain centers need to be brought in the Lab 
    % reference where the beam direction and rotation axis are defined.
    %(Should be done the other way, in the Sample ref., as that's faster)
    pllab = gtGeoSam2Lab(pld', om, labgeo, samgeo, true, 'only_rot', true)';
    gclab = gtGeoSam2Lab(gcd', om, labgeo, samgeo, false)';
   
    % Check if all reflections occur
    %if any(isnan(omp))
    %    error('No reflection was predicted for some pair(s).');
    %end
        
    % Diffraction vector
    dveclab = gtFedPredictDiffVecMultiple(pllab, labgeo.beamdir');
    %dvecsam = gtGeoLab2Sam(dveclab', omp, labgeo, samgeo, true, 'only_rot', true)';

    % Absolute detector coordinates U,V in pixels 
    uv = gtFedPredictUVMultiple([], dveclab, gclab, labgeo.detrefpos', ...
         labgeo.detnorm', Qdet, [labgeo.detrefu; labgeo.detrefv]);

     
    % Absolute coordinates U,V,W (pixel,pixel,image); to account for 
    % measurement errors, do not correct for the image number
    % to be mod(om,360)/omstep 
    uvw = [uv; om/omstep];
   
end
