function [labgeonew, detgeonew, energy] = gtFitUpdateLabgeo(newvars, ...
    newvarsind, oldvars, labgeobase, detgeobase)
% GTFITUPDATELABGEO  Updates the labgeo and energy parameters with the new
% input values.
%
% [labgeonew, energy] = gtFitUpdateLabgeo(newvars, newvarsind, oldvars,...
%                                         labgeobase)
%
% Recalculates labgeo parameters based on new and old values relative to
% labgeobase. New values come from the user or from fitting.
% It uses all the 'newvars' that are specified. For those parameters which 
% have no new value specified, it uses the 'oldvars'. 'Labgeo' is then 
% recalculated according to this complete list relative to 'labgeobase'.
%
% List of parameters in oldvars: 
% 1  = 'Position X (mm or labunit)';
% 2  = 'Position Y (mm or labunit)';
% 3  = 'Position Z (mm or labunit)';
% 4  = 'Tilt in plane (rad)';
% 5  = 'Angle U-V (rad)';
% 6  = 'Tilt around U (rad)';
% 7  = 'Tilt around V (rad)';
% 8  = 'Pixel size mean (um or 1000*labunit)';
% 9  = 'Pixel size ratio';
% 10 = 'Energy (keV)'
% 11 = 'Rot. wedge (rad)'
%
% Version 001 18-04-2014 by P.Reischig
%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Detector position, pixel sizes, tilts 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

actvars = oldvars;
actvars(newvarsind) = newvars;

% Detector position
detgeo.detrefpos(1, :) = actvars(1:3);

% Pixel sizes
detgeo.pixelsizeu = 0.001*actvars(8)*sqrt(actvars(9));
detgeo.pixelsizev = 0.001*actvars(8)/sqrt(actvars(9));

% Tilts around U,V and detector normal
rmcU = gtMathsRotationMatrixComp(detgeobase.detdiru, 'row');
rmcV = gtMathsRotationMatrixComp(detgeobase.detdirv, 'row');
rmcN = gtMathsRotationMatrixComp(detgeobase.detnorm, 'row');

rotU = gtMathsRotationTensor(actvars(6)*(180/pi), rmcU);
rotV = gtMathsRotationTensor(actvars(7)*(180/pi), rmcV);
rotN = gtMathsRotationTensor(actvars(4)*(180/pi), rmcN);

% Complete rotation matrix 
rot = rotU * rotV * rotN;

% Apply rotation matrix to row vectors
detdiru = detgeobase.detdiru * rot;
detdirv = detgeobase.detdirv * rot;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Recalculate V according to angle between U and V 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Actual U-V angle
actangle = acos(detdiru * detdirv');

% Update detector normal
detnorm = cross(detdiru, detdirv);
detnorm = detnorm / sqrt(detnorm * detnorm');

% Rotation matrix to adjust angle between U-V
rmcN = gtMathsRotationMatrixComp(detnorm, 'row');
rotN = gtMathsRotationTensor(0.5*(actvars(5) - actangle)*(180/pi), rmcN);

% Apply correction opposite ways 
detdiru = detdiru * rotN';
detdirv = detdirv * rotN;

% Renormalise U and V vector
detgeo.detdiru = detdiru/sqrt(detdiru*detdiru');
detgeo.detdirv = detdirv/sqrt(detdirv*detdirv');

% Recalculate angle for checking (should be = actvars(5))
%uvangle = acos(labgeo.detdiru*labgeo.detdirv')

% Energy
if (newvarsind(10))
    energy = newvars(sum(newvarsind(1:10)));
else
    energy = oldvars(10);
end

% Rotation axis
if newvarsind(11)
    wedgeaxis = cross(labgeobase.beamdir, labgeobase.rotdir);
    rmc       = gtMathsRotationMatrixComp(wedgeaxis, 'row');
    rot       = gtMathsRotationTensor(actvars(11)*(180/pi), rmc);
    labgeo.rotdir = labgeobase.rotdir*rot;
else
    labgeo.rotdir = labgeobase.rotdir;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Update other 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Update detector normal
detgeo.detnorm = detnorm;

% Update detector origin: pixel (0,0) coordinates in LAB ref.
detgeo.detrefu = detgeobase.detrefu;
detgeo.detrefv = detgeobase.detrefv;
detgeo.orig = gtGeoDetOrig(detgeo);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Create output
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
labgeonew = labgeobase;
detgeonew = detgeobase;

% Update the fields that have been recalculated
fnames = fieldnames(labgeo);
for ii = 1:length(fnames)
    labgeonew.(fnames{ii}) = labgeo.(fnames{ii});
end
fnames = fieldnames(detgeo);
for ii = 1:length(fnames)
    detgeonew.(fnames{ii}) = detgeo.(fnames{ii});
end

end