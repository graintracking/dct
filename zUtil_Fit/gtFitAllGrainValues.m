function val = gtFitAllGrainValues(grain, field1, field2, ind1, ind2)
% A temporary substitute for gtIndexAllGrainValues.
%
% Returns a column vector containing the values with indices (ind1,ind2) in 
% grain.field1.field2 from all the grains in 'grain'. 
% 
% val = gtFitAllGrainValues(grain, field1, field2, ind1, ind2)
%
% INPUT
%   grain  - all grain data (cell vector)
%   field1 - field 1 name (string)
%   field2 - field 2 name (string) or empty is not applicable
%   ind1   - index 1 of variable (1x1)
%   ind2   - index 2 of variable or empty is not applicable (1xn)
% 
% OUTPUT
%   val    - column vector; size(m,n) where m is the number of grains
%
% EXAMPLE
%   gtIndexAllGrainValues(grain, 'nof_pairs', [], 1, [])
%   gtIndexAllGrainValues(grain, 'R_vector', [], 1, 1:3)
%   gtIndexAllGrainValues(grain, 'indST', [], 3, 1:3)
%   gtIndexAllGrainValues(grain, 'stat', 'dangmean', 1, [])
%
%
% Version 001 18-04-2014 by P.Reischig
%

nof_grains = length(grain);


if ~exist('ind1','var')
    ind1 = [];
end
   
if ~exist('ind2','var')
    ind2 = [];
end

if ~exist('field2','var')
    field2 = [];
end


if isempty(ind1) && isempty(ind2)
    form = 0;   % all values will be concatenated
elseif isempty(ind2)
    form = 1;   % parameter is a one dimensional vector
else
    form = 2;   % parameter is a two dimensional array
end


% If there is no second field 
if isempty(field2)
    
    if (form == 2)
        
        if islogical(grain{1}.(field1))
            val = false(nof_grains, length(ind2));          
        else
            val = zeros(nof_grains, length(ind2), class(grain{1}.(field1)));
        end
        
        for ii = 1:nof_grains
            val(ii,:) = grain{ii}.(field1)(ind1,ind2);
        end
        
    elseif (form == 1)
        
        if islogical(grain{1}.(field1))
            val = false(nof_grains, 1);          
        else
            val = zeros(nof_grains, 1, class(grain{1}.(field1)));
        end
        
        for ii = 1:nof_grains
            val(ii,:) = grain{ii}.(field1)(ind1);
        end
        
    else
        
        val = [];
        
        for ii = 1:nof_grains
            val = [val, grain{ii}.(field1)];
        end
        
    end
    
% If there is a second field
else
    
    if (form == 2)

        if islogical(grain{1}.(field1).(field2))
            val = false(nof_grains, length(ind2));          
        else
            val = zeros(nof_grains, length(ind2), class(grain{1}.(field1).(field2)));
        end
        
        for ii = 1:nof_grains
            val(ii,:) = grain{ii}.(field1).(field2)(ind1,ind2);
        end
        
    elseif (form == 1)
        
        if islogical(grain{1}.(field1).(field2))
            val = false(nof_grains, 1);          
        else
            val = zeros(nof_grains, 1, class(grain{1}.(field1).(field2)));
        end
        
        for ii = 1:nof_grains
            val(ii,:) = grain{ii}.(field1).(field2)(ind1);
        end
        
    else
        
        val = [];
        
        for ii = 1:nof_grains
            val = [val, grain{ii}.(field1).(field2)];
        end
   
    end
    
end



end  % of function
