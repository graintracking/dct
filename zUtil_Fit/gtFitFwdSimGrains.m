function grain = gtFitFwdSimGrains(grain, parameters, offdet, keepNaN, drift)
% This function should be merged with gtIndexFwdSimGrains! 
%
% GTFITFWDSIMGRAINS Forward simulates the spot positions for all 
% reflections in all grains.
% 
% grain = gtFitFwdSimGrains(grain, parameters, offdet, keepNaN, drift)
%
% ---------------------------------------------------
%
% INPUT
%   grain      - cell array containing grains as in index.mat
%   parameters - parameters as in the parameters file
%   offdet     - if true, spots that fall outside the detector area
%                will be valued NaN-s
%   keepNaN    - if true, all spots will be present in output (NaN where 
%                no reflection occurs), otherwise only those where 
%                reflection occurs;
%   drift      - sample and energy drifts (structure)
%
% OUTPUT
%   The output is not ordered in any way and does not correspond to the 
%   original grain data.
%   grain.fsim. - added field containing fwd simulation data:
%    .cu        - U coordinate of center
%    .cv        - U coordinate of center
%    .cw        - W (image no.) coordinate of center
%    .eta       - eta diffraction angle in degrees
%    .sinth     - sin(theta)
%    .plref     - plane normal in reference crystal (unstrained theoretical)
%    .refind    - linear index of reflection in grain
%    .hklspind  - same as refind
%    .thetatype - thetatype
%    .hklind    - same as thetatype
%
%
% Version 001 18-04-2014 by P.Reischig
%    

if (~exist('parameters','var') || isempty(parameters))
    parameters = gtLoadParameters();
end


labgeo = parameters.labgeo;
if (~isfield(labgeo, 'omstep') || isempty(labgeo.omstep))
    labgeo.omstep = gtAcqGetOmegaStep(parameters);
end
if (~isfield(parameters, 'detgeo') || isempty(parameters.detgeo))
    parameters.detgeo = gtGeoConvertLegacyLabgeo2Detgeo(labgeo);
end
detgeo = parameters.detgeo;
samgeo = parameters.samgeo;
cryst  = parameters.cryst(grain{1}.phaseid);
energy = parameters.acq.energy;

beamdir = labgeo.beamdir';
rotdir  = labgeo.rotdir';

% Rotation tensor components
rotcomp = gtMathsRotationMatrixComp(labgeo.rotdir','col');

% B matrix for transformation from (hkl) to Cartesian Crystal reference 
Bmat = gtCrystHKL2CartesianMatrix(cryst.latticepar);

% Coordinates of the plane normals in the Cartesian Crystal system
plref  = gtCrystHKL2Cartesian(double(cryst.hklsp), Bmat);
refind = 1:size(plref,2);

% D-spacings in the reference crystal for the given reflections
dspref = gtCrystDSpacing(double(cryst.hkl(:,cryst.thetatypesp)), Bmat);

                                 
% Rotation tensor describing crystal orientation; calculate all grains 
% at once for speed-up.
% (gtCrystRodriguesVector and gtFedRod2RotTensor used together 
% gives the transformation tensor from CRYSTAL TO SAMPLE coordinates)
Rvec    = gtIndexAllGrainValues(grain,'R_vector',[],1,1:3);
cry2sam = gtFedRod2RotTensor(Rvec');


% Parallel loop through grains
for ii = 1:length(grain)
                    
    % Plane normals in the Sample reference
    plsam = cry2sam(:,:,ii) * plref;
    
    % Impose strain on grain
    if (~isfield(grain{ii},'strain'))
        grain{ii}.strain.strainT = NaN(3,3);
    end
    
    if (any(isnan(grain{ii}.strain.strainT(:))))
        %fprintf('No strain info in grain #%d. Using as measured plane normals.\n',ii)
        plsamd = plsam;
        drel   = ones(1,size(plsam,2));
    else
        % New deformed plane normals and relative elongations
        [plsamd, drel] = gtStrainPlaneNormals(plsam, grain{ii}.strain.strainT);
    end
    
    % d-spacing after applying the strain tensor
    dspd = drel .* dspref;

    % Bragg angles theta in the deformed state
    sinth = 0.5 * gtConvEnergyToWavelength(energy) ./ dspd;
    
    % Grain center
    gcsam = grain{ii}.center';
     
    % The plane normals need to be brought in the Lab reference where the
    % beam direction and rotation axis are defined.
    % Use the Sample -> Lab orientation transformation assuming omega=0;
    % (vector length preserved for free vectors)
    sam2lab0 = [samgeo.dirx; samgeo.diry; samgeo.dirz]';
    pllab0   = sam2lab0*plsamd;

    
    %%%%%%%%%%%%%%%%%%%%%%%%%
    %%% Apply drifts
    %%%%%%%%%%%%%%%%%%%%%%%%%
    
    if isempty(drift)           
        
        % Predict omega angles: 4 for each (hkl) plane normal (the Friedel pairs are
        % om1-om3 and om2-om4); -(hkl) plane normals are assumed not to be in the list
        [om4, pllab4, ~, rot4, omind4] = gtFedPredictOmegaMultiple(...
            pllab0, sinth, labgeo.beamdir', labgeo.rotdir', rotcomp, []);
        
        % Diffraction vectors in Lab and Sample
        dveclab = gtFedPredictDiffVecMultiple(reshape(pllab4,3,[]), labgeo.beamdir');
        
        % Extend grain centers
        gcsam = gcsam(:,ones(1,4*size(plsam,2)));
        
        % Absolute detector coordinates U,V in pixels
        uv = gtFedPredictUVMultiple(rot4, dveclab, gcsam, detgeo.detrefpos', ...
            detgeo.detnorm', detgeo.Qdet, [detgeo.detrefu; detgeo.detrefv]);
        
        
        % Absolute coordinates U,V,W (pixel,pixel,image); to account for
        % measurement errors, do not correct for the image number
        % to be mod(om,360)/omstep
        grain{ii}.fsim.cu        = uv(1,:);
        grain{ii}.fsim.cv        = uv(2,:);
        grain{ii}.fsim.cw        = reshape(om4',[],1)'/labgeo.omstep;
        grain{ii}.fsim.omind     = reshape(omind4',[],1)';
        grain{ii}.fsim.eta       = gtGeoEtaFromDiffVec(dveclab', labgeo)';
        grain{ii}.fsim.sinth     = [sinth, sinth, sinth, sinth];
        grain{ii}.fsim.plref     = [plref, plref, plref, plref];
        grain{ii}.fsim.refind    = [refind, refind, refind, refind];
        grain{ii}.fsim.hklspind  = grain{ii}.fsim.refind;
		thetatype                = cryst.thetatypesp(:)';
        grain{ii}.fsim.thetatype = [thetatype, thetatype, thetatype, thetatype];
        grain{ii}.fsim.hklind    = grain{ii}.fsim.thetatype;
           
    else
        % Plane normals, grain centers and sin(theta) after drifts in the
        % Sample reference.
        % The omega has to be iterated, because it's not known initially 
        % when drifts are present, as drifts are also dependent on omega.
        
        gcsam = gcsam(:,ones(1,size(plsam,2)));

        % ITERATION STEP 1
    
        % Predict omega angles: 4 for each (hkl) plane normal (the Friedel pairs are
        % om1-om3 and om2-om4); -(hkl) plane normals are assumed not to be in the hkl list
        [om, ~, ~, ~, omind] = gtFedPredictOmegaMultiple(...
                               pllab0, sinth, beamdir, rotdir, rotcomp, []);
        
        [plsamd1, ~, sinthd1] = gtFitApplyDrifts(plsamd, gcsam, dspd, om(1,:), drift, energy);
        [plsamd2, ~, sinthd2] = gtFitApplyDrifts(plsamd, gcsam, dspd, om(2,:), drift, energy);
        [plsamd3, ~, sinthd3] = gtFitApplyDrifts(plsamd, gcsam, dspd, om(3,:), drift, energy);
        [plsamd4, ~, sinthd4] = gtFitApplyDrifts(plsamd, gcsam, dspd, om(4,:), drift, energy);

        pllab01 = sam2lab0 * plsamd1;
        pllab02 = sam2lab0 * plsamd2;
        pllab03 = sam2lab0 * plsamd3;
        pllab04 = sam2lab0 * plsamd4;

        
        % ITERATION STEP 2
        for jj = 1:10
            om1 = gtFedPredictOmegaMultiple(pllab01, sinthd1, beamdir, rotdir, rotcomp, omind(1,:));
            om2 = gtFedPredictOmegaMultiple(pllab02, sinthd2, beamdir, rotdir, rotcomp, omind(2,:));
            om3 = gtFedPredictOmegaMultiple(pllab03, sinthd3, beamdir, rotdir, rotcomp, omind(3,:));
            om4 = gtFedPredictOmegaMultiple(pllab04, sinthd4, beamdir, rotdir, rotcomp, omind(4,:));
            
            [plsamd1, ~, sinthd1] = gtFitApplyDrifts(plsamd, gcsam, dspd, om1, drift, energy);
            [plsamd2, ~, sinthd2] = gtFitApplyDrifts(plsamd, gcsam, dspd, om2, drift, energy);
            [plsamd3, ~, sinthd3] = gtFitApplyDrifts(plsamd, gcsam, dspd, om3, drift, energy);
            [plsamd4, ~, sinthd4] = gtFitApplyDrifts(plsamd, gcsam, dspd, om4, drift, energy);
            
            pllab01 = sam2lab0 * plsamd1;
            pllab02 = sam2lab0 * plsamd2;
            pllab03 = sam2lab0 * plsamd3;
            pllab04 = sam2lab0 * plsamd4;
        end
        
        
        % ITERATION STEP 3
        
        [om1,~,~,rot1] = gtFedPredictOmegaMultiple(pllab01, sinthd1, beamdir, rotdir, rotcomp, omind(1,:)); 
        [om2,~,~,rot2] = gtFedPredictOmegaMultiple(pllab02, sinthd2, beamdir, rotdir, rotcomp, omind(2,:));
        [om3,~,~,rot3] = gtFedPredictOmegaMultiple(pllab03, sinthd3, beamdir, rotdir, rotcomp, omind(3,:));
        [om4,~,~,rot4] = gtFedPredictOmegaMultiple(pllab04, sinthd4, beamdir, rotdir, rotcomp, omind(4,:));

        % Plane normals in Sample, grain centers and sin(theta) after drifts
        [plsamd1, gcsamd1, sinthd1] = gtFitApplyDrifts(plsamd, gcsam, dspd, om1, drift, energy);
        [plsamd2, gcsamd2, sinthd2] = gtFitApplyDrifts(plsamd, gcsam, dspd, om2, drift, energy);
        [plsamd3, gcsamd3, sinthd3] = gtFitApplyDrifts(plsamd, gcsam, dspd, om3, drift, energy);
        [plsamd4, gcsamd4, sinthd4] = gtFitApplyDrifts(plsamd, gcsam, dspd, om4, drift, energy);
        
        % Plane normals in Lab at omega positions after drifts
        pllab1 = gtGeoSam2Lab(plsamd1', om1, labgeo, samgeo, true, 'only_rot', true);
        pllab2 = gtGeoSam2Lab(plsamd2', om2, labgeo, samgeo, true, 'only_rot', true);
        pllab3 = gtGeoSam2Lab(plsamd3', om3, labgeo, samgeo, true, 'only_rot', true);
        pllab4 = gtGeoSam2Lab(plsamd4', om4, labgeo, samgeo, true, 'only_rot', true);
        pllab  = [pllab1; pllab2; pllab3; pllab4]';
    
        %pllab01 = sam2lab0 * plsamd1;
        %pllab02 = sam2lab0 * plsamd2;
        %pllab03 = sam2lab0 * plsamd3;
        %pllab04 = sam2lab0 * plsamd4;
        %pllab0  = [pllab01, pllab02, pllab03, pllab04];
        %pllabd  = 
        
        rot = cat(3,rot1,rot2,rot3,rot4);
        
        
        % SPOT POSITIONS

        % Extend grain center
        %gcsamd1 = gcsamd1(:, ones(size(pllabd1,2) ,1));
        %gcsamd2 = gcsamd2(:, ones(size(pllabd2,2), 1));
        %gcsamd3 = gcsamd3(:, ones(size(pllabd3,2), 1));
        %gcsamd4 = gcsamd4(:, ones(size(pllabd4,2), 1));
        gcsamd  = [gcsamd1, gcsamd2, gcsamd3, gcsamd4];
        
        % Diffraction vectors in Lab    
        dveclab = gtFedPredictDiffVecMultiple(pllab, beamdir);
        
        %dveclab1 = gtFedPredictDiffVecMultiple(pllabd1, beamdir);
        %dveclab2 = gtFedPredictDiffVecMultiple(pllabd2, beamdir);
        %dveclab3 = gtFedPredictDiffVecMultiple(pllabd3, beamdir);
        %dveclab4 = gtFedPredictDiffVecMultiple(pllabd4, beamdir);
        
        % Absolute detector coordinates U,V in pixels
        uv = gtFedPredictUVMultiple(rot, dveclab, gcsamd, detgeo.detrefpos', ...
             detgeo.detnorm, detgeo.Qdet, [detgeo.detrefu; detgeo.detrefv]);
          
         %         uv2 = gtFedPredictUVMultiple(rot2, dveclab, gcsamd2, labgeo.detrefpos', ...
         %               detnorm, Qdet, [labgeo.detrefu; labgeo.detrefv]);
         %         uv3 = gtFedPredictUVMultiple(rot3, dveclab, gcsamd3, labgeo.detrefpos', ...
         %               detnorm, Qdet, [labgeo.detrefu; labgeo.detrefv]);
         %         uv4 = gtFedPredictUVMultiple(rot4, dveclab, gcsamd4, labgeo.detrefpos', ...
         %               detnorm, Qdet, [labgeo.detrefu; labgeo.detrefv]);
        
        
        % Absolute coordinates U,V,W (pixel,pixel,image); to account for
        % measurement errors, do not correct for the image number
        % to be mod(om,360)/omstep
        grain{ii}.fsim.cu        = uv(1,:);
        grain{ii}.fsim.cv        = uv(2,:);
        grain{ii}.fsim.cw        = [om1, om2, om3, om4]/labgeo.omstep;
        grain{ii}.fsim.omind     = reshape(omind',[],1)';
        grain{ii}.fsim.eta       = gtGeoEtaFromDiffVec(dveclab', labgeo)';
        grain{ii}.fsim.sinth     = [sinthd1, sinthd2, sinthd3, sinthd4];
        grain{ii}.fsim.plref     = [plref, plref, plref, plref];
        grain{ii}.fsim.refind    = [refind, refind, refind, refind];
        grain{ii}.fsim.hklspind  = grain{ii}.fsim.refind;
		thetatype                = cryst.thetatypesp(:)';
        grain{ii}.fsim.thetatype = [thetatype, thetatype, thetatype, thetatype];
        grain{ii}.fsim.hklind    = grain{ii}.fsim.thetatype;
             
    end

    % Set NaN for spots that fall outside the detector area 
    if (~offdet)
        offd = (uv(1,:) <= 0.5) | (detgeo.detsizeu+0.5 <= uv(1,:)) | ...
               (uv(2,:) <= 0.5) | (detgeo.detsizev+0.5 <= uv(2,:));

        fn = fieldnames(grain{ii}.fsim);
        for jj = 1:length(fn)
            grain{ii}.fsim.(fn{jj})(:,offd) = NaN;
        end
    end

    % Delete spots with NaN values (no reflection (omega=NaN) or off
    % the detector) 
    if (~keepNaN)
        todel = isnan(grain{ii}.fsim.cu);

        fn = fieldnames(grain{ii}.fsim);

        for jj = 1:length(fn)
            grain{ii}.fsim.(fn{jj})(:,todel) = [];
        end
    end
end

end



