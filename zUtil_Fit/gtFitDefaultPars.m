function par = gtFitDefaultPars()
% Default parameters for fitting.
%
% Version 001 18-04-2014 by P.Reischig
%

% Input data; initial variables at start of optimisation
par.phase     = 1;
par.energy    = []; 
par.cryst     = [];
par.grain     = [];
par.labgeoini = [];

% Choose which variables to fit
par.fit_global_detposX    = true;
par.fit_global_detposY    = true;
par.fit_global_detposZ    = false;   % false, if along rotation axis
par.fit_global_detroll    = true;
par.fit_global_detangUV   = false;
par.fit_global_dettiltU   = true;
par.fit_global_dettiltV   = true;
par.fit_global_pixmean    = false;
par.fit_global_pixratio   = false;
par.fit_global_energy     = true;
par.fit_global_rotwedge   = false;

par.fit_grain_position    = true;
par.fit_grain_orientation = true;
par.fit_grain_strain      = false;

par.fit_drift_posX   = false;
par.fit_drift_posY   = false;
par.fit_drift_posZ   = false;
par.fit_drift_rotX   = false;
par.fit_drift_rotY   = false;
par.fit_drift_rotZ   = false;
par.fit_drift_energy = false;

% Optimisation algorithm
par.algorithm      = 'trust-region-reflective'; % or 'levenberg-marquardt';
par.tolfun         = 1e-12;
par.tolvar         = 1e-6;
par.maxiter        = 1000;
par.maxfunevals    = 1e6;
par.plotfun        = 'optimplotfval';
par.finitediff     = 1e-6;   
par.niter_drift    = 5;  % min. 3

% Drift interpolation
par.drift_omega    = 0:15:360;
par.drift_method   = 'pchip';

% Warning settings
par.warning_lim_u  = 100;
par.warning_lim_v  = 100;
par.warning_lim_w  = 100;


end % function



%%%%%%%%%%%%%%%%
% UNUSED
%%%%%%%%%%%%%%%%

% Typical values
% par.typ_strain     = 1e-2;  % dimensionless
% par.typ_orient     = 1e-2;  % dimensionless
% par.typ_detrefpos  = 1;     % in mm (lab units)
% par.typ_dettilt    = 1e-2;  % in rad
% par.typ_detangUV   = 1e-2;  % in rad
% par.typ_pixmean    = 1;     % in um (0.001*labunits)
% par.typ_pixratio   = 1;     % dimensionless 
% par.typ_energy     = 10;    % in keV
% par.typ_drift      = 1e-2;  % in mm (lab units)
% par.typ_driftrot   = 1e-2;  % in rad*2 (Rodrigues coordinates)
% par.typ_driftnrg   = 10;    % in keV
% par.typ_rotwedge   = 1e-2;  % in rad

% par.bnd_strain     = 0.01;  % dimensionless
% par.bnd_orient     = 0.1;   % dimensionless
% par.bnd_detrefpos  = 1;     % in mm (lab units)
% par.bnd_dettilt    = 0.1;   % in rad
% par.bnd_detangUV   = 0.1;   % in rad
% par.bnd_pixmean    = 1;     % in um (0.001*labunits)
% par.bnd_pixratio   = 0.5;   % dimensionless 
% par.bnd_drift      = 0.1;   % in mm (lab units)
% par.bnd_driftrot   = 0.1;   % in rad*2 (Rodrigues coordinates)
% par.bnd_driftnrg   = 5;     % in keV
% par.bnd_driftnrg   = 0.1;   % in rad

%par.tol_strain     = 1e-5;  % dimensionless
%par.tol_orient     = 1e-5;  % dimensionless
%par.tol_geo        = 1e-5;  % for all labgeo and drift parameters

%par.tolfun_strain  = 1e-12; % dimensionless
%par.tolfun_orient  = 1e-12; % dimensionless
%par.tolfun_geo     = 1e-12; % for all labgeo and drift parameters

%par.resU_geo       = true;
%par.resV_geo       = true;
%par.resW_geo       = true;

%par.alg_geo        = 'trust-region-reflective';
%par.alg_geo        = 'levenberg-marquardt';
%par.maxeval_geo    = [];
