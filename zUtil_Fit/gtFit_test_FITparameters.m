function [opt, graint, grainm, spot, fitpar, par_err] = ...
         gtFit_test_FITparameters(numgrain, parameters, fitpar, par_err, drift, graint, grainm, spot)
% Test function to test the main optimisation function.
%
% It takes or generates the input, adds noise to it optionally, runs the 
% optimisation and presents the results.
%
% graint: true grain
% grainm: "measured" grain
%
%
% Version 001 18-04-2014 by P.Reischig
%
	 
% !!! May be wrong if Sample and Lab reference frames are misaligned.

par_true = parameters;

tSetup = tic;

% Fit parameters
if ~exist('fitpar','var') || isempty(fitpar)
	fitpar = gtFitDefaultPars();
	
	fitpar.fit_global_detposX    = 1;
	fitpar.fit_global_detposY    = 1;
	fitpar.fit_global_detposZ    = 0;  % !!
	fitpar.fit_global_detroll    = 1;
	fitpar.fit_global_detangUV   = 1;
	fitpar.fit_global_dettiltU   = 1;
	fitpar.fit_global_dettiltV   = 1;
	fitpar.fit_global_pixmean    = 0;  % !!
	fitpar.fit_global_pixratio   = 1;
	fitpar.fit_global_energy     = 0;  % !!
	fitpar.fit_global_rotwedge   = 1;
	
	fitpar.fit_grain_position    = 1;
	fitpar.fit_grain_orientation = 1;
	fitpar.fit_grain_strain      = 1;
	
	fitpar.fit_drift_posX   = 1;
	fitpar.fit_drift_posY   = 1;
	fitpar.fit_drift_posZ   = 1;
	fitpar.fit_drift_rotX   = 1;
	fitpar.fit_drift_rotY   = 1;
	fitpar.fit_drift_rotZ   = 1;
	fitpar.fit_drift_energy = 0;  % !!
		
	fitpar.algorithm   = 'trust-region-reflective'; %'levenberg-marquardt'; %
	fitpar.tolfun      = 1e-12;
	fitpar.tolvar      = 1e-7;
	fitpar.finitediff  = 1e-7;
	fitpar.niter_drift = 5;
end



if ~exist('par_err','var') || isempty(par_err)
    
    samsize   = 1;     % labunit
    rodsize   = 0.4;   % Rodrigues space limits
    strainmax = 0.00; %0.005; % strain components limit 
    
    lim.spotu = 100;
    lim.spotv = 100;
    lim.spotw = 100;
    
    par_err = parameters;

    
    % SET PARAMETERS
    
    % Max. amplitude of initial errors
    inierr.grcenter  = 0.02;   % 0.02;
    inierr.grrod     = 0.002;  % 0.002;
    
    inierr.energy    = 0;      % 0.1;
    inierr.detrefpos = [0.007 0.007 0];  % [0.01 0.01 0];
    inierr.detdiru   = [0.0005 0.0007 0.0009];      % [0.001 1 0.001];
    inierr.detdirv   = [0.001 0.0011 0.0012];      % [0.001 0.001 -1];
    inierr.pixsizeu  = -0.00001;      % 0.00001
    inierr.pixsizev  = +0.00001;      % 0.00001
    inierr.rotwedge  = 0.1;      % 0.1;
    
    inierr.driftpos = 0.01; % 0.01;
    inierr.driftrot = 0.001; %0.0005;
    inierr.driftene = 0; % 0.1;
    
    noise.u = 1; %1;  % pixel
    noise.v = 1; %1;  % pixel
    noise.w = 1; %1;  % image
       
    % Drifts
    drift.omega  = 0:15:360;
    drift.method = 'pchip';
    drift.nfit   = max(length(fitpar.drift_omega) - 1, 0);
    drift.posx   = [0, inierr.driftpos * (2*rand(1,drift.nfit) - 1)];
    drift.posy   = [0, inierr.driftpos * (2*rand(1,drift.nfit) - 1)];
    drift.posz   = [0, inierr.driftpos * (2*rand(1,drift.nfit) - 1)];
    drift.rotx   = [0, inierr.driftrot * (2*rand(1,drift.nfit) - 1)];
    drift.roty   = [0, inierr.driftrot * (2*rand(1,drift.nfit) - 1)];
    drift.rotz   = [0, inierr.driftrot * (2*rand(1,drift.nfit) - 1)];
    drift.energy = par_true.acq.energy + [0, inierr.driftene * (2*rand(1,drift.nfit) - 1)];
   
    
    % Geometry affected by error
    
    rotcomp = gtMathsRotationMatrixComp([0 1 0], 'row');
    rot     = gtMathsRotationTensor(inierr.rotwedge, rotcomp);
    
    par_err.acq.energy        = par_true.acq.energy        + inierr.energy;
    par_err.labgeo.detrefpos  = par_true.labgeo.detrefpos  + inierr.detrefpos;
    par_err.labgeo.detdiru    = par_true.labgeo.detdiru    + inierr.detdiru;
    par_err.labgeo.detdirv    = par_true.labgeo.detdirv    + inierr.detdirv;
    par_err.labgeo.pixelsizeu = par_true.labgeo.pixelsizeu + inierr.pixsizeu;
    par_err.labgeo.pixelsizev = par_true.labgeo.pixelsizev + inierr.pixsizev;
    par_err.labgeo.rotdir     = par_true.labgeo.rotdir * rot;
    
    par_err.labgeo.detdiru = par_err.labgeo.detdiru/norm(par_err.labgeo.detdiru);
    par_err.labgeo.detdirv = par_err.labgeo.detdirv/norm(par_err.labgeo.detdirv);
    par_err.labgeo.rotdir  = par_err.labgeo.rotdir/norm(par_err.labgeo.rotdir);
    
    %par_true.labgeo.detnorm = cross(par_true.labgeo.detdiru, par_true.labgeo.detdirv);
    
    
    %disp('NOTE! Spots may be outside the detector area!')
    
    % Pre-allocate spot data
    spot.id       = [];
    spot.grainind = [];
    spot.fsimind  = [];
    spot.hklind   = [];
    spot.hklspind = [];
    spot.omind    = [];
    
    spot.mesu     = [];
    spot.mesv     = [];
    spot.mesw     = [];
    
    spot.simu     = [];
    spot.simv     = [];
    spot.simw     = [];
    
    spot.bbx      = [];
    spot.bby      = [];
    spot.int      = [];
    
    
    for ii = 1:numgrain
        
        % TRUE GRAIN
        % !!! Sample and Lab ref. misalignments may not be taken into account.
        graint{ii}.active   = 1;
        graint{ii}.center   = (2*rand(1,3)-1)*samsize;
        graint{ii}.R_vector = (2*rand(1,3)-1)*rodsize;
        strcmpt             = (2*rand(1,6)-1)*strainmax;
        graint{ii}.strain.strainT = [strcmpt(1), strcmpt(6), strcmpt(5); ...
                                     strcmpt(6), strcmpt(2), strcmpt(4); ...
                                     strcmpt(5), strcmpt(4), strcmpt(3)];
        graint{ii}.strain.straincmp = strcmpt;
        
        % Fwd simulate individual spots: adds fsim field
        graint(ii) = gtIndexFwdSimGrains(graint(ii), par_true, 0, 1, drift);
        
        ngsp = length(graint{ii}.fsim.cu);

        
        % INITIALLY MEASURED GRAIN AFFECTED BY ERRORS
        grainm{ii}.active   = 1;
        grainm{ii}.center   = graint{ii}.center   + (2*rand(1,3)-1)*inierr.grcenter;
        grainm{ii}.R_vector = graint{ii}.R_vector + (2*rand(1,3)-1)*inierr.grrod;
        grainm{ii}.strain.strainT = zeros(3,3);
              
        % Fwd simulate individual spots: adds fsim field
        grainm(ii) = gtIndexFwdSimGrains(grainm(ii), par_err, 0, 1, drift);
        
        % Check correspondence of reflections between graint and grainm        
        check(1,:) = (grainm{ii}.fsim.hklind   == graint{ii}.fsim.hklind);
        check(2,:) = (grainm{ii}.fsim.hklspind == graint{ii}.fsim.hklspind);
        check(3,:) = (grainm{ii}.fsim.omind    == graint{ii}.fsim.omind);
        check(4,:) = ~isnan(grainm{ii}.fsim.cu) & ~isnan(graint{ii}.fsim.cu);
        check(5,:) = abs(grainm{ii}.fsim.cu - graint{ii}.fsim.cu) < lim.spotu; 
        check(6,:) = abs(grainm{ii}.fsim.cv - graint{ii}.fsim.cv) < lim.spotv; 
        check(7,:) = abs(grainm{ii}.fsim.cw - graint{ii}.fsim.cw) < lim.spotw; 
        ok         = all(check,1);
        ngspok     = sum(ok);
        
        % Spot index
        spind = (length(spot.id)+1:length(spot.id)+ngspok);
        
        grainm{ii}.spot.difspotid = spind;
        grainm{ii}.spot.spotind   = spind;
        grainm{ii}.spot.plref     = grainm{ii}.fsim.plref(:,ok);
        grainm{ii}.spot.hklind    = grainm{ii}.fsim.hklind(ok);
        grainm{ii}.spot.hklspind  = grainm{ii}.fsim.hklspind(ok);
        grainm{ii}.spot.omind     = grainm{ii}.fsim.omind(ok);
        grainm{ii}.spot.dspref    = par_err.cryst.dspacing(:, grainm{ii}.spot.hklind);
        grainm{ii}.spot.plsam0    = gtFedRod2RotTensor(grainm{ii}.R_vector) *...
                                    grainm{ii}.spot.plref;
        
        % Simulated spot positions for measured grain = wrong spot positions
        grainm{ii}.spot.simu   = grainm{ii}.fsim.cu(ok);
        grainm{ii}.spot.simv   = grainm{ii}.fsim.cv(ok);
        grainm{ii}.spot.simw   = grainm{ii}.fsim.cw(ok);
        
        % Measured spot positions for measured grain = true spot positions;
        % Add noise to measurement
        grainm{ii}.spot.mesu   = graint{ii}.fsim.cu(ok) + (2*rand(1,ngspok)-1)*noise.u;
        grainm{ii}.spot.mesv   = graint{ii}.fsim.cv(ok) + (2*rand(1,ngspok)-1)*noise.v;
        grainm{ii}.spot.mesw   = graint{ii}.fsim.cw(ok) + (2*rand(1,ngspok)-1)*noise.w;
        
        % Check if discrepancies are within limits
        %         check2(1) = all(abs(grainm{ii}.spot.simu - grainm{ii}.spot.mesu) < lim.spotu);
        %         check2(2) = all(abs(grainm{ii}.spot.simv - grainm{ii}.spot.mesv) < lim.spotv);
        %         check2(3) = all(abs(grainm{ii}.spot.simw - grainm{ii}.spot.mesw) < lim.spotw);
        %
        %         if ~all(check2)
        %             disp(['Bad reflections in grain ' num2str(ii)])
        %         end
        
        
        % INITIALLY MEASURED SPOT DATA (INPUT TO FITTING) 
        spot.id       = [spot.id; spind'];
        spot.grainind = [spot.grainind; ii(ones(ngspok,1),1)];
        spot.fsimind  = [spot.fsimind; find(ok)'];
        spot.hklind   = [spot.hklind; grainm{ii}.spot.hklind'];
        spot.hklspind = [spot.hklspind; grainm{ii}.spot.hklspind'];
        spot.omind    = [spot.omind; grainm{ii}.spot.omind'];
        
        spot.mesu     = [spot.mesu; grainm{ii}.spot.mesu'];
        spot.mesv     = [spot.mesv; grainm{ii}.spot.mesv'];
        spot.mesw     = [spot.mesw; grainm{ii}.spot.mesw'];
        
        spot.simu     = [spot.simu; grainm{ii}.spot.simu'];
        spot.simv     = [spot.simv; grainm{ii}.spot.simv'];
        spot.simw     = [spot.simw; grainm{ii}.spot.simw'];
        
        spot.bbx      = [spot.bbx; ones(ngspok,1)];
        spot.bby      = [spot.bby; ones(ngspok,1)];
        spot.int      = [spot.int; ones(ngspok,1)];
    end
end

fitpar.cryst = par_err.cryst;

tSetUp = toc(tSetup);

fprintf('No. of spots input for fitting; %d\n', length(spot.id))

%%%%%%%%%%%%%%%%%%%%%%%%%%
% Run fitting
%%%%%%%%%%%%%%%%%%%%%%%%%%
tFit = tic;
disp('Starting fitting function...')

opt = gtFITParameters(spot, fitpar, grainm, par_err);

tFit = toc(tFit);


%%%%%%%%%%%%%%%%%%%%%%%%%%
% Display Results
%%%%%%%%%%%%%%%%%%%%%%%%%%

disp('Error in grain centers (x,y,x,norm):')
dc     = opt.grain.center' - gtIndexAllGrainValues(graint,'center',[],1,1:3);
dcnorm = sqrt(sum(dc.^2,2));
disp([(1:length(graint))', dc, dcnorm]);

disp('Error in grain Rod. vectors (x,y,z, norm):')
dr     = opt.grain.Rvec' - gtIndexAllGrainValues(graint,'R_vector',[],1,1:3);
drnorm = sqrt(sum(dr.^2,2));
disp([(1:length(graint))', dr, drnorm]);

disp('Error in grain strain components (norm of 6 comp.):')
st     = gtIndexAllGrainValues(graint,'strain','straincmp',1,1:6);
ds     = opt.grain.strain' - st;
dsnorm = sqrt(sum(ds.^2,2));
disp([(1:length(graint))', dsnorm]);


disp('Setup time (sec):')
disp(tSetUp)
disp('Fitting time (sec):')
disp(tFit)


disp('Extreme error norm in grain centers:')
disp([min(dcnorm), max(dcnorm)])
disp('Extreme error norm in grain Rod vec.:')
disp([min(drnorm), max(drnorm)])
disp('Extreme error norm in grain strain:')
disp([min(dsnorm), max(dsnorm)])


% Drifts
disp(' ')
if ~isempty(opt.drift.posx)
    disp('Error in drift posX:')
    disp(opt.drift.posx - drift.posx)
end
if ~isempty(opt.drift.posy)
    disp('Error in drift posY:')
    disp(opt.drift.posy - drift.posy)
end
if ~isempty(opt.drift.posz)
    disp('Error in drift posZ:')
    disp(opt.drift.posz - drift.posz)
end
if ~isempty(opt.drift.rotx)
    disp('Error in drift rotX:')
    disp(opt.drift.rotx - drift.rotx)
end
if ~isempty(opt.drift.roty)
    disp('Error in drift rotY:')
    disp(opt.drift.roty - drift.roty)
end
if ~isempty(opt.drift.rotz)
    disp('Error in drift rotZ:')
    disp(opt.drift.rotz - drift.rotz)
end
if ~isempty(opt.drift.energy)
    disp('Error in drift energy:')
    disp(opt.drift.energy - drift.energy)
end
disp(' ')

% disp('True labgeo:')
% par_true.labgeo
% disp('Found labgeo:')
% labgeo
% 
% disp('True energy:')
% par_true.acq.energy
% disp('Found energy:')
% energy



fprintf('No. of spots input for fitting; %d\n\n', length(spot.id))

fprintf('ERRORS\n')
fprintf(' Energy:     %s\n', num2str(opt.energy            - par_true.acq.energy))
fprintf(' Detrefpos:  %s\n', num2str(opt.labgeo.detrefpos  - par_true.labgeo.detrefpos))
fprintf(' Detdiru:    %s\n', num2str(opt.labgeo.detdiru    - par_true.labgeo.detdiru))
fprintf(' Detdirv:    %s\n', num2str(opt.labgeo.detdirv    - par_true.labgeo.detdirv))
fprintf(' Rotdir:     %s\n', num2str(opt.labgeo.rotdir     - par_true.labgeo.rotdir))
fprintf(' Pixelsizeu: %s\n', num2str(opt.labgeo.pixelsizeu - par_true.labgeo.pixelsizeu))
fprintf(' Pixelsizev: %s\n', num2str(opt.labgeo.pixelsizev - par_true.labgeo.pixelsizev))

ri = reshape(opt.duvwini,[],3);
re = reshape(opt.duvwend,[],3);
ri2 = sqrt(sum(ri.^2,1));
re2 = sqrt(sum(re.^2,1));

fprintf('\nRESIDUALS (u,v,w)\n\n') 
fprintf('Initial residual min:  %s\n', num2str(min(ri)))
fprintf('Initial residual max:  %s\n', num2str(max(ri)))
fprintf('Initial residual norm: %s\n', num2str(ri2))

fprintf('Final residual min:    %s\n', num2str(min(re)))
fprintf('Final residual max:    %s\n', num2str(max(re)))
fprintf('Final residual norm:   %s\n', num2str(re2))

figure('name', 'Initial and optimized errors in spot positions')
subplot(1,2,1)
hist(opt.duvwini(:),100)
title('Initial errors')
subplot(1,2,2)
hist(opt.duvwend(:),100)
title('Optimized errors')



end

% % Plane normals, grain centers and sin(theta) after drifts in the
% % Sample reference;
% [sp.pl, sp.center, sp.sinth] = gtFitApplyDrifts(sp.pl, sp.center, ...
%     sp.dsp, spot.mesw', par.drift, energy);
% 
% % Now predict the omega angles for each plane normal where diffraction 
% % occurs. (omega index defines which omega value to calculate)
% [sp.om, sp.pl, ~, sp.rot] = gtFedPredictOmegaMultiple(...
%     sp.pl, sp.sinth, labgeo.beamdir', labgeo.rotdir', rotcomp, spot.omind');
% 
% % From degrees to images:
% sp.om = sp.om/par.omstep;
% 
% % Check if all reflections occur
% if any(isnan(sp.om))
%     warning('No reflection was predicted for some pair(s).');
% end



