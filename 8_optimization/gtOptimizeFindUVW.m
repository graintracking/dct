function [F,M,x] = gtOptimizeFindUVW(x,xdata,csam,uvorig)
% detdiru (3x1)
% detdirv (3x1)
% detpos (3x1)
% uvorig (2x1)
% x (1x9)
%
% xdata (Nx4,3)
% srot (Nx3,3)
% dvec (N,3)

if size(x,1) ~= 1 && size(x,2) ~= 9
    disp('Please check the size of x')
    return
end

%detdiru=[x(1);sqrt(1-x(1)^2-x(2)^2);x(2)];
%detdirv=[x(3);x(4);sqrt(1-x(3)^2-x(4)^2)];
%detpos=[x(5);x(6);x(7)];

detpos=[x(7);x(8);x(9)];

% Gram - Schmidt process for orthonormalization

x1=[x(1);x(2);x(3)];
x2=[x(4);x(5);x(6)];
x3=cross(x1,x2);
norm12=dot(x1,x2); % alpha
norm23=dot(x2,x3); % beta
norm13=dot(x1,x3); % gamma

%disp('Gram-Schmidt process')
y=orthonormGS(x1,x2,x3);
x(1:6)=[y(1,:),y(2,:)];
% new detector reference system
M=y;


detdiru=y(1,:)';
normu=norm(detdiru);
detdirv=y(2,:)';
normv=norm(detdirv);
tn=y(3,:)';
normt=norm(tn);

% normalize
detdiru=detdiru/norm(detdiru);
detdirv=detdirv/norm(detdirv);

% split the xdata into two matrices
n3=size(xdata,1)/4*3;
srot=xdata(1:n3,:);
dvec=xdata(n3+1:end,:);

detscaleu=1;
detscalev=1;

%    Q - projection matrix from LAB to DETECTOR coordinates %2x3
Qdet = [ detdiru'*detscaleu; detdirv'*detscalev];

F=[];
n=size(dvec,1);
for j=1:n
    % rotation tensor
    S = srot(3*j-2:3*j,:); %3x3
    % diffraction vector
    D = dvec(j,:)'; %3x1 unit vector
    
    %grain coord. in LAB system
    clab = S*csam; %3x1
    % diffraction factor
    dfac = tn'*(detpos-clab)/(tn'*D); %1x1 
    
    % diffraction vector in the u,v plane from uvorig
    duvpl = clab + dfac*D - detpos; %3x1 
    
    uv = uvorig + Qdet*duvpl; %2x1
    
    F = [F; uv(1),uv(2)]; % Nx2
   
end



end % end function