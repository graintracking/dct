function noiseuv=gtOptimizePlotUVW(im,uv,color,geo,scale,titlename,debug,noise,numnoise)
% noiseuv=gtOptimizePlotUVW(im,uv,color,geo,scale,titlename,debug,noise,numnoise)
% 
% INPUT:
%           im = output from gtPlaceDifspotsinfull <double 2048x2048>
%           uv = u,v list to plot <double Nx2>
%        color = color to be used in the plot <char>
%          geo = output from gtOptimizeTuneDetector
%        scale = gray scaled from -100 to 400
%    titlename = title name <string>
%        debug = print comments or info <logical> {0}
%        noise = number of pixels noise <int8> {2}
%     numnoise = percentage of noised spots <double> {}
% 
% OUTPUT:
%      noiseuv = list u,v noised

figure
if ~isempty(im)
    if ~isempty(scale) 
        imshow(im,scale)
    else
        imshow(im,[])
    end
    hold on
end

if ~exist('titlename','var') || isempty(titlename)
    titlename='';
end

if ~exist('debug','var') || isempty(debug)
    debug=false;
end
if ~exist('noise','var') || isempty(noise)
    noise=2;
end
if ~exist('numnoise','var') || isempty(numnoise)
    numnoise=[];
end

N=size(uv,1);

detsizeu = 2048;
detsizev=2048;
beamdir=geo.beamdir';
detdiru=geo.detdiru';
detdirv=geo.detdirv';
detdir=geo.detnorm;
detpos=geo.detrefpos; % mm
detrefu=geo.detrefu;
detrefv=geo.detrefv;

if isfield(geo,'Xzero')
    olddiru=geo.Xzero(1:3);
    olddirv=geo.Xzero(4:6);
    oldpos=geo.Xzero(7:9).*geo.pixelsize; % mm
else
   olddiru=detdiru;
   olddirv=detdirv;
   oldpos=geo.detrefpos;%.*geo.pixelsize
end


% Image frame
plot([0 detsizeu+1],[0 0],'k')
plot([0 detsizeu+1],[detsizev+1 detsizev+1],'k')
plot([0 0],[0 detsizev+1],'k')
plot([detsizeu+1 detsizeu+1],[0 detsizev+1],'k')

%% Midlines
plot([0 detsizeu+1],[detsizev/2+0.5 detsizev/2+0.5],'-.y','Linewidth',1)
plot([detsizeu/2+0.5 detsizeu/2+0.5],[0 detsizev+1],'-.y','Linewidth',1)

Qdet = gtFedDetectorProjectionTensor(detdiru,detdirv,1,1);

%% Beam direction in image
beamuv = Qdet*beamdir;
% Arrow in figure indicating beam direction

if (dot(beamdir,detdir)-1)<0.001 % // 1
    plot(detrefu,detrefv,'.w','MarkerSize',20)
elseif (dot(beamdir,detdir)+1)<0.001 % // -1
    plot(detrefu,detrefv,'xw','MarkerSize',20)
else
    quiver(detrefu,detrefv,beamuv(1),beamuv(2),1000,'-w','Linewidth',2)
end
impixelinfo

%% origin u,v
plot(0,0,'.y','MarkerSize',20)
plot(0,0,'oy','MarkerSize',10)

% unit vectors u,v
quiver(0,0,0,1,150,'-y','Linewidth',2)
quiver(0,0,1,0,150,'-y','Linewidth',2)

%% center displacement
shift=oldpos-detpos;
newcenu=detrefu+dot(shift,olddiru)./geo.pixelsizeu;
newcenv=detrefv+dot(shift,olddirv)./geo.pixelsizev;
plot(newcenu,newcenv,'+y','MarkerSize',20)


for i=1:N
    plot(uv(i,1),uv(i,2),['o' color],'MarkerSize',10)
end

% title
title(titlename)


%% noise

if ~isempty(numnoise)
    
    rnoise=(rand(N,size(uv,2))-0.5)*noise*2;
    ind=randperm(N);
    color=rand(1,3);
    newuv(ind,:)=rnoise(ind,:)+uv(ind,:);
    plot(newuv(ind,1),newuv(ind,2),['o' color],'MarkerSize',10)
    noiseuv=newuv;

else
    noiseuv=uv;
end


if debug
    disp('detpos:')
    disp(detpos)
    disp('oldpos:')
    disp(oldpos)
    disp('shift:')
    disp(shift)
    disp('beamuv:')
    disp(beamuv)
end

end
    
    