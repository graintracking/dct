function [geo,newgr]=gtOptimizeInlineDetector(grainFed,parameters)
%% newgr=gtOptimizeInlineDetector(grainFed,parameters)
%  
%  Get geometry from parameters, get difspots positions and bbox from
%  database, predict positions from fed code and 
%
%  INPUT:
%    grainFed = output from gtFedGenerateRandomGrain
%    parameters
%
%  OUTPUT: 
%      geo = geometry from the parameters file
%    newgr = structure with geometry (current geometry), 
%                           bbox (values read from database)
%                           fed (values predicted with fed code)
%                           table (comparison between fed and database
%                                  values)

dofed=false;
if isempty(grainFed)
    dofed=true;
end
if ~exist('parameters','var') || isempty(parameters)
    parameters=[];
    load('parameters.mat');
end

geo=gtGetGeometry(parameters,false)

%% Current geometry

disp('Getting the detector geometry...')

detdiru = geo.detdiru0';%
detdirv = geo.detdirv0';%
Qdet = gtFedDetectorProjectionTensor(detdiru,detdirv,1,1);%
tn = cross(detdiru,detdirv);%
detpos = (geo.detpos0./geo.pixelsize)'; % pixels
omstep = 180/geo.nproj;
uvorig = [geo.detucen0, geo.detvcen0]';

disp('...done!')

%% Getting info from grain

if dofed
   disp('Calculating output from gtFedGenerateRandomGrain...')
   grain=load('grain.mat');
   full=gtPlaceDifspotinFull(grain{grainFed.id}.difspots,parameters);
   gr=gtFedGenerateRandomGrain(grain{grainFed.id},full);
   grainFed=gr;
end

grainID=grainFed.id;
csam = grainFed.center';
R_vector=grainFed.R_vector;
difspots=grainFed.difspots;

all=grainFed.allblobs;
fedtable=[];


% NaN rows
indn=find(isnan(all.uvw(:,1)));

hklsp=all.hklsp;hklsp(indn,:)=[];
srot=all.srot;
for i=1:size(indn)
    srot(indn*3-2:indn*3,:)=[];
end
gfomega=all.omega;gfomega(indn,:)=[];
dvec=all.dvec;dvec(indn,:)=[];
theta=all.theta;theta(indn,:)=[];
eta=all.eta;eta(indn,:)=[];
uvwfed=all.uvw;uvwfed(indn,:)=[];
uvfed=all.uv;uvfed(indn,:)=[];
images=round((gfomega/180)*geo.nproj);


fed.grainID=grainID;
fed.center=csam;
fed.R_vector=R_vector;
fed.hklsp=hklsp;
fed.omega=gfomega;
fed.theta=theta;
fed.eta=eta;
fed.uvwfed=uvwfed;
fed.uvfed=uvfed;
fed.difspots=difspots';

%% Get info from the database

match=parameters.match;
name=parameters.acq.name;

%get the mean difspot, X and Y sizes for the current spots in the grain
dif_Xsize=grainFed.stat.bbxsmean;
dif_Xsize_ratio=grainFed.stat.bbxsrat*1.3;
dif_Ysize=grainFed.stat.bbysmean;
dif_Ysize_ratio=grainFed.stat.bbysrat*1.3;

dif_Xsearch=2*grainFed.stat.bbxsstd;
dif_Ysearch=4*grainFed.stat.bbysstd;

if ~isfield(grainFed.stat,'areamean')
    % we need to add this value in gtINDEXTER
    grainFed.stat.areamean=0.7*grainFed.stat.bbxsmean*grainFed.stat.bbysmean;  % 0.7 is a rough estimate of the spot area / BB-area ratio
end

dif_int=grainFed.stat.intmean;
dif_area=grainFed.stat.areamean;
dif_area_ratio=dif_Xsize_ratio*dif_Ysize_ratio;




gtDBConnect();

grain_id=[];
expected_id=[];
found_id=[];
image=[];
omega=[];
x0=[];
y0=[];
xsize=[];
ysize=[];
xcen=[];
ycen=[];
bbxcen=[];
bbycen=[];
area=[];
        

for i=1:length(gfomega)
    
    thr_max_offset=min(20,round(match.thr_max_offset/(abs(sind(eta(i))))));  % allow for a 1/sin(eta) dependency on omega search range - limited to a max offset of 20 images
    mysqlcmd=sprintf(['select grainID, difspotID, CentroidX, CentroidY, '...
        'BoundingBoxXorigin, BoundingBoxYorigin, BoundingBoxXsize, '...
        'BoundingBoxYsize, CentroidImage, Area  from %sdifspot where '...
        '(%d between CentroidImage-%d and CentroidImage+%d) '...
        'and (abs(%sdifspot.CentroidX-%d)/%d)<0.5 '...
        'and (abs(%sdifspot.CentroidY-%d)/%d)<0.5 '...
        'order by ((abs(BoundingBoxXsize-%d)/%d)+(abs(BoundingBoxYsize-%d)/%d)) limit 1'],...
        name,...
        round(gfomega(i)/omstep), thr_max_offset, thr_max_offset,...
        name, uvfed(i,1), dif_Xsize,...
        name, uvfed(i,2), dif_Ysize,...
        dif_Xsize, dif_Xsize, dif_Ysize, dif_Ysize);


    [grainID, expected, CentroidX, CentroidY, ...
        BoundingBoxXorigin, BoundingBoxYorigin, ...
        BoundingBoxXsize, BoundingBoxYsize, ...
        CentroidImage, Area] = mym(mysqlcmd);

    if isempty(expected)
        grain_id=[grain_id; 0];
        expected_id=[expected_id; 0];
        image=[image; 0];
        omega=[omega; 0];
        x0=[x0; 0];
        y0=[y0; 0];
        xsize=[xsize; 0];
        ysize=[ysize; 0];
        xcen=[xcen; 0];
        ycen=[ycen; 0];
        bbxcen=[bbxcen; 0];
        bbycen=[bbycen; 0];
        area=[area; 0];
        found_id=[found_id; 0];
    else
        grain_id=[grain_id; grainID];
        expected_id=[expected_id; expected];
        fprintf('expected_id: %d \n',expected);
        image=[image; CentroidImage];
        omega=[omega; CentroidImage*omstep];
        x0=[x0; BoundingBoxXorigin];
        y0=[y0; BoundingBoxYorigin];
        xsize=[xsize; BoundingBoxXsize];
        ysize=[ysize; BoundingBoxYsize];
        xcen=[xcen; CentroidX];
        ycen=[ycen; CentroidY];
        bbxcen=[bbxcen; BoundingBoxXorigin+BoundingBoxXsize/2];
        bbycen=[bbycen; BoundingBoxYorigin+BoundingBoxYsize/2];
        area=[area; Area];

    query=sprintf(['select difspotID from %sdifspot where '...
        '(%d between CentroidImage-%d and CentroidImage+%d) '...
        'and (abs(%sdifspot.CentroidX-%d)/%d)<%f '...
        'and (abs(%sdifspot.CentroidY-%d)/%d)<%f '...
        'and (BoundingBoxXsize between %f and %f) and (BoundingBoxYsize between %f and %f) '...
        'and (Area between %f and %f) '...
        'order by ((abs(BoundingBoxXsize-%d)/%d)+(abs(BoundingBoxYsize-%d)/%d)) limit 1'],...   % this could be improved according to Peters goodness criteria...
        name,...
        round(gfomega(i)/omstep), thr_max_offset, thr_max_offset,...
        name, uvfed(i,1), dif_Xsize, dif_Xsearch/dif_Xsize,...
        name, uvfed(i,2), dif_Ysize, dif_Ysearch/dif_Ysize,...    % use the same (more relaxed) X condition for the Y COM position
        dif_Xsize/dif_Xsize_ratio, dif_Xsize*dif_Xsize_ratio, dif_Ysize/dif_Ysize_ratio, dif_Ysize*dif_Ysize_ratio,...
        dif_area/dif_area_ratio, dif_area*dif_area_ratio,...
        dif_Xsize, dif_Xsize, dif_Ysize, dif_Ysize);

    [found] = mym(query);

    if ~isempty(found)
        found_id=[found_id; found];
        fprintf('found_id: %d \n',found);
    else
        found_id=[found_id; 0];
    end

    end % end expected

end %loop on omega

% save info into parameters

bbox.grain_id=grain_id;
bbox.expected_id=expected_id;
bbox.found_id=found_id;
bbox.x0=x0;
bbox.y0=y0;
bbox.xsize=xsize;
bbox.ysize=ysize;
bbox.xcen=xcen;
bbox.ycen=ycen;
bbox.bbxcen=bbxcen;
bbox.bbycen=bbycen;
bbox.image=image;
bbox.omega=omega;
bbox.area=area;

fed.omstep=omstep;

%% Find the differences between found and expected  

diff=findDifspots(bbox.expected_id,bbox.found_id,name);
diff.query='expected_id and found_id';
bbox.diff=diff;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Compute uv coordinates and w(omega) values

% fed.UV = [];
% fed.UVW = [];
% 
% n=size(gfomega,1)/4;
% for j=1:n % must be from 0 to n-1
%     
%     % from grainFed=gtFedGenerateRandomGRain2(grain{grainid},full)
%     S=all.srot(12*j-11:12*j,:);
%     D=all.dvec(4*j-3:4*j,:);
%     O=all.omega(4*j-3:4*j); % omega values ordered like small, small+180, big, big+180 for the 4
%         
%     uv1a=gtFedPredictUVW(S(1:3,:),D(1,:)',csam,detpos,tn,Qdet,uvorig,O(1),omstep)
%     uv1b=gtFedPredictUVW(S(4:6,:),D(2,:)',csam,detpos,tn,Qdet,uvorig,O(2),omstep)
%     uv2a=gtFedPredictUVW(S(7:9,:),D(3,:)',csam,detpos,tn,Qdet,uvorig,O(3),omstep)
%     uv2b=gtFedPredictUVW(S(10:12,:),D(4,:)',csam,detpos,tn,Qdet,uvorig,O(4),omstep)
%     
%     fed.UV  =  [fed.UV;uv1a(1),uv1a(2);uv1b(1),uv1b(2);uv2a(1),uv2a(2);uv2b(1),uv2b(2)];
%     fed.UVW =  [fed.UVW;uv1a(1),uv1a(2),uv1a(3);uv1b(1),uv1b(2),uv1b(3);uv2a(1),uv2a(2),uv2a(3);uv2b(1),uv2b(2),uv2b(3)];
% end % size /4

newgr.fed=fed;
newgr.geometry=geo;


%% Try to match the difspots and the hkl values

if ~isempty(bbox.found_id)
    diff2=findDifspots(bbox.found_id,fed.difspots,name);
diff2.query='found_id and difspots';
bbox.diff2=diff2;
end

newgr.bbox=bbox;

% size(fed.omega)
% size(bbox.omega)
% size(fed.uvfed)
% size(bbox.xcen)
% size(bbox.ycen)
% size(bbox.expected_id)
% size(bbox.found_id)
% size(fed.hklsp)

if size(fed.omega,1)~=size(bbox.omega,1)
    return
end
% table to compare fed results with database 
table=int32([fed.omega bbox.omega ...
             fed.uvfed(:,1) bbox.xcen ...
             fed.uvfed(:,2) bbox.ycen ...
             bbox.expected_id bbox.found_id ...
             fed.hklsp]);
newgr.table=table;
nonzero=find(table(:,2)~=0);
newgr.ind=nonzero;

%newgr.ind=find(bbox.expected_id~=0);

end % end function


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function list=findDifspots(list1,list2,name)


[rows,ia,ib,neq,dif]=findDifferentRowIntoMatrix(list1,list2);

dif(find(dif(:,1)==0),:)=[];
list.ind=dif(:,2)';
list.ID=dif(:,1)';

for j=1:size(dif(:,1),1)
    mysqlcmd=sprintf(['select grainID, CentroidX, CentroidY, CentroidImage, Integral, '...
        'BoundingBoxXorigin, BoundingBoxYorigin, BoundingBoxXsize, BoundingBoxYsize, Area from %sdifspot where difspotID=%d'],...
        name, dif(j,1));
    
    [grainID, CentroidX, CentroidY, CentroidImage, Integral, BoundingBoxXorigin, BoundingBoxYorigin, BoundingBoxXsize, BoundingBoxYsize, Area]=mym(mysqlcmd);
    
    if ~isempty(BoundingBoxXorigin)
        list.x0(j)=BoundingBoxXorigin;
        list.y0(j)=BoundingBoxYorigin;
        list.xsize(j)=BoundingBoxXsize;
        list.ysize(j)=BoundingBoxYsize;
        list.xcen(j)=CentroidX;
        list.ycen(j)=CentroidY;
        list.image(j)=CentroidImage;
        list.int(j)=Integral;
        list.area(j)=Area;
        list.grainid(j)=grainID;
    end
end

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

