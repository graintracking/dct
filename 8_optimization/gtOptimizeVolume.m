function geo = gtOptimizeVolume(first,last,workingdirectory)

if ~exist('workingdirectory','var') || isempty(workingdirectory)
    workingdirectory=pwd;
end

cd(workingdirectory)
    
    
parameters=[];
grain=[];
if exist('parameters.mat','file')
    load('parameters.mat');
end
if exist('grain.mat','file')
    load('grain.mat');
end

nog=length(grain);

if last > nog
    disp('last must be less than the number of grains.')
    return
end

if isdeployed
    global GT_DB
    global GT_MATLAB_HOME
    load('workspaceGlobal.mat');
    first=str2num(first);
    last=str2num(last);
end


opt=[];
detpos=[];
detdiru=[];
detdirv=[];
newdetucen=[];
newdetvcen=[];

for i=first:last
    gtOptimizeFull(grain{i},parameters)
    opt=load(sprintf('opt%d.mat',i));
    opt=opt.opt;
    detpos=[detpos;opt.fit.geo.detpos0];
    detdiru=[detdiru;opt.fit.geo.detdiru0];
    detdirv=[detdirv;opt.fit.geo.detdirv0];
    newdetucen=[newdetucen;opt.fit.geo.newdetucen0];
    newdetvcen=[newdetvcen;opt.fit.geo.newdetvcen0];
end


if last-first > 1
    disp('Calculating the mean od the detector position and u,v direction...')
    detposm=mean(detpos);
    detdirum=mean(detdiru);
    detdirvm=mean(detdirv);
    newdetucenm=mean(newdetucen);
    newdetvcenm=mean(newdetvcen);

    disp('Displaying mean values...')
    disp(' ')
    disp('detpos: ')
    disp(detposm)
    disp('detdiru: ')
    disp(detdirum)
    disp('detdirv: ')
    disp(detdirvm)
    disp(['newdetucen: ' num2str(newdetucenm)])
    disp(['newdetvcen: ' num2str(newdetvcenm)])

    geo.detpos0=detposm;
    geo.detdiru0=detdirum;
    geo.detdirv0=detdirvm;
    geo.newdetucen0=newdetucenm;
    geo.newdetvcen0=newdetvcenm;

    save('optmean.mat','geo');
end

end

