function [spot,range,box,omegamean]=gtOptimizeFindSpot(parameters,imagemin,imagemax,ymin,ymax,xmin,xmax,number)

dir=parameters.acq.dir;
cd(sprintf('%s/1_preprocessing/full',dir));

sum=zeros(parameters.acq.xdet);
for i=imagemin:imagemax
   im=edf_read(sprintf('full%04d.edf',i));
   sum=sum+im;
end

spot=sum(ymin:ymax,xmin:xmax);
box=[ymin ymax xmin xmax];
range=[imagemin imagemax];

cd(dir);
name=sprintf('spot%d.edf',number);
if exist(name,'file')
    fprintf('The file %s already exist. ',name)
    check = inputwdefault('Overwrite it? [y/n]', 'y');
    if strcmpi(check,'y')
        edf_write(spot,name);
    end
else
    edf_write(spot,name);
end
omegamean=mean([imagemin imagemax])*180/parameters.acq.nproj;

end
