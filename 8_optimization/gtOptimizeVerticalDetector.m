function [newgeo, bbox, full_all, gr] = gtOptimizeVerticalDetector(grain, parameters, bbxsizemean, areamin, areamax, color, greys, save)
% [newgeo, bbox, full_all, gr] = gtOptimizeVerticalDetector(grain, parameters, bbxsizemean, areamin, areamax, [color], [greys], [save])
%
% INPUT:
%             grain  =  grain{grainid}
%        parameters  =  parameters file
%       bbxsizemean  =  discrepancy between fed u, v and spot centroid u, v
%                     || difspot.CentroidX - u || < 0.5 * bbxsizemean
%                     || difspot.CentroidY - v || < 0.5 * bbxsizemean
%   areamin/areamax  =  Area < areamax & Area > areamin
%             color  =  color of predicted u, v points
%             greys  =  grey scale
%              save  =  true if saving
%
% OUTPUT:
%     newgeo  =  optimized geometry
%       bbox  =  structure with information from the database and from fed
%   full_all  =  image with spots
%         gr  =  grain updated structure

if (nargin < 5)
    disp('Usage: gtOptimizeVerticalDetector(grain, parameters, bbxsizemean, areamin, areamax, [color], [greys], [save])')
    return
end
if (isempty(color))
    color = 'b';
end
if (isempty(greys))
    greys = [];
end
if (~exist('save', 'var') || isempty(save))
    save = false;
end

gr = gtFedGenerateRandomGrain(grain, zeros(parameters.acq.xdet), parameters, 'save', save);
uv = gr.allblobs(1).detector.uvw(:, 1:2);
omega = gr.allblobs(1).omega;
ondet = gr.allblobs(1).detector.ondet;

% find spots' indexes intersecting the detector
uvdet = uv(ondet, :);
omegadet = omega(ondet);
length(uv)

% search in the database
bbox = gtOptimizeFindSpotsWoIndexter(parameters, uvdet, omegadet, bbxsizemean, areamin, areamax);
size(bbox.feduv)

% place spots in the image (figure not shown)
full_all = gtPlaceDifspotinFull(bbox.expected_id, parameters, 'save', save, 'connected', true, 'id', grain.id);

% plot the spots using the current geometry
tmp = gtOptimizePlotUVW(full_all, bbox.feduv, 'b', parameters.acq, greys, sprintf('Before optimization : |x-u| < %2.0f : area [%d %d]', bbxsizemean*0.5, areamin, areamax), false);

% get the indexes of spots found in the database from the full uv list
[r, ia, ib, d] = findRowIntoMatrix(uv, bbox.feduv);
[r2, ia2, ib2, d2] = findRowIntoMatrix(uv(ia, :), bbox.feduv);
ind = sortrows([ib2 ia]);
indsorted = ind(:, 2);
bbox.indsorted = indsorted;

% optimize detector
newgeo = gtOptimizeTuneDetector(parameters.acq, gr, bbox.dbuv, indsorted);

disp('detdiru0: ')
disp(newgeo.detdiru0)
disp('detdirv0: ')
disp(newgeo.detdirv0)

% plot the spots positions with the optimized geometry
tmp2 = gtOptimizePlotUVW(full_all, newgeo.Fnew(:, 1:2), color, newgeo, greys, sprintf('After optimization : blue [bbox.dbuv] %s [bbox.feduv]', color), true);
hold on
for ii = 1:length(bbox.dbuv)
    plot(bbox.dbuv(ii, 1), bbox.dbuv(ii, 2), 'g.', 'MarkerSize', 20)
end

end

