function gtOptimizeFull(grainid, parameters, varargin)
% GTOPTIMIZERFULL  Optimize detector position basing on difspots
%     gtOptimizeFull(grainid, parameters, [varargin])
%     -----------------------------------------------
%     INPUT:
%       grainid     =  grain{grainid}
%       parameters  =  parameters file
%
%     OPTIONAL INPUT:
%       difspots    =  list of spots id (optional if grain contains difspots)
%       loadfed     =  true if exists gr%d_fed.mat (from gtFedGenerateRandomGrain)
%       showfigure  =  <logical> {0}
%       save        =  <logical> {1}
%       Grayscale   =  Gray scale {[]}
%
%

    app.difspots   = [];
    app.loadfed    = false;
    app.showfigure = false;
    app.save       = true;
    app.Grayscale  = [];
    app = parse_pv_pairs(app, varargin);

    % save some info in the opt structure
    opt.id = grainid.id;
    opt.workdir = pwd;

    if ~isfield(grainid, 'difspots')
        if ~isempty(app.difspots)
            grainid.difspots = app.difspots;
        else
            disp('you must give a list of difspots id.')
            return
        end
    end

    ind = strfind(pwd, '/');
    opt.dataset = opt.workdir(ind(end)+1:end);

    opt.grain = 'grain.mat';
    opt.parfile = 'parameters.mat';
    opt.gcenter = grainid.center;
    opt.gR_vector = grainid.R_vector;

    % Create the full summed difspots image
    fprintf('Taking spots from the database for grain %d...\n', grainid.id)
    if isfield(grainid, 'difspots')
       full = gtPlaceDifspotinFull(grainid.difspots, parameters, 'save', app.save, 'id', grainid.id);
    else
       full = gtPlaceDifspotinFull(app.difspots, parameters, 'id', grainid.id);
    end
    disp('...done!')

    % generate grain fed
    name = sprintf('gr%d_fed.mat', grainid.id);
    fprintf('Predicting spots positions with fed for grain %d...\n', grainid.id)
    if ~exist(name, 'file')
        gr = gtFedGenerateRandomGrain(grainid, full, parameters, 'save', app.save);
    else
        if app.loadfed
            gr = load(name);
            gr = gr.grain;
        else
            gr = gtFedGenerateRandomGrain(grainid, full, parameters);
        end
    end
    disp('...done!')

    % load original geometry
    fprintf('Getting geometry and info from database for grain %d...\n', grainid.id)
    [geometry, opt.info] = gtOptimizeInlineDetector(gr, parameters);
    opt.fed.geo = geometry;
    opt.fed.origuv = gr.allblobs(1).detector.uvw(:, 1:2);
    opt.fed.uv = opt.fed.origuv;
    % removes NaN
    ind = find(isnan(opt.fed.uv(:, 1))); % size -> size/2
    opt.ind.nan = ind;
    opt.fed.uv(ind, :) = [];
    opt.db.cen = [opt.info.bbox.xcen opt.info.bbox.ycen];
    opt.db.uv = opt.db.cen;
    % removes zeros from database
    ind2 = find(opt.db.uv(:, 1) == 0);
    ind3 = find(opt.db.uv(:, 1) ~= 0);
    opt.db.uv(ind2, :) = [];
    opt.ind.zeros = ind2;
    opt.ind.nonzeros = ind3;
    disp('...done!')


    % fit the spots positions taken from the database with the current geometry as guess
    disp('*** Fit ***')
    fprintf('Fitting the u, v positions from fed with the spots centroids for grain %d...\n', grainid.id)
    fitgeo = gtOptimizeTuneDetector(opt.fed.geo, gr, opt.db.uv, ind3);
    opt.fit.geo = fitgeo;
    opt.fit.uv = fitgeo.Fnew(:, 1:2);
    disp('...done!')

    name = sprintf('opt%d.mat', grainid.id);
    save(name, 'opt');

    maxfull = max(full(:));

    if app.showfigure
        tmp = gtOptimizePlotUVW(full, opt.fit.uv, 'r', opt.fed.geo, app.Grayscale, 'After optimization : ');
    else
        disp('Fitted geometry...')
        disp(opt.fit.geo)
    end
end
