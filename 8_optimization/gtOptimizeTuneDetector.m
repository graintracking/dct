function newgeo = gtOptimizeTuneDetector(geometry, grainFed, uvdb, induv, debug, varargin)
% newgeo = gtOptimizeTuneDetector(geometry, grainFed, uvdb, induv, debug, varargin)
%    INPUT:
%      geometry = output from gtOptimizeInlineDetector
%      grainFed = output from gtFedGenerateRandomGrain
%          uvdb = uv from database
%         induv = indexes of uvdb to be used from the full list of
%                 grainFed.allblobs(1).detector.uvw(:, 1:2)
%         debug = print comments
%      varargin = optional arguments for options
%                 options = optimset(varargin);
%                 DEFAULT: options  =
%                 optimset('TolFun', 1e-2, 'MaxFunEvals', 3000, 'MaxIter', 700)
%
%    OUTPUT:
%      newgeo = new geometry
%
%
% OPTIMSET PARAMETERS for MATLAB
%     Display - Level of display [ off || iter || notify || final ]
%     MaxFunEvals - Maximum number of function evaluations allowed
%                          [ positive integer ]
%     MaxIter - Maximum number of iterations allowed [ positive scalar ]
%     TolFun - Termination tolerance on the function value [ positive scalar ]
%     TolX - Termination tolerance on X [ positive scalar ]
%     FunValCheck - Check for invalid values, such as NaN or complex, from
%                   user-supplied functions [ {off} || on ]
%     OutputFcn - Name(s) of output function [ {[]} || function ]
%               All output functions are called by the solver after each
%               iteration.
%     PlotFcns - Name(s) of plot function [ {[]} || function ]
%               Function(s) used to plot various quantities in every iteration
%
% Commentary: I actually doubt that this function is still of any use
%

    c = tic();
    disp('Start gtOptimizeTuneDetector...')

    if (~exist('debug', 'var') || isempty(debug))
       debug = false;
    end

    %%% get info from newgr and grainFed
    newgeo = geometry;

    pixelsize = geometry.pixelsize;
    detdiru = geometry.detdiru0;
    detdirv = geometry.detdirv0;
    detpos  = geometry.detpos0 ./ pixelsize;

    uvorig = [geometry.detucen0, geometry.detvcen0]';
    omstep = 180 / geometry.nproj;
    pixelsize = geometry.pixelsize;

    if (dot(detdiru, detdirv) > 1e-4)
        disp('u, v are not perpendicular. Quitting...')
        return
    end

    % apply induv to the lists with size (Nx3, 1)
    % grain.allblobs(1).[variable]
    srot  = grainFed.allblobs(1).srot(:, :, induv);
    dvec  = grainFed.allblobs(1).dvec(induv, :);
    omega = grainFed.allblobs(1).omega(induv, :);
    csam  = grainFed.center';

    uvwfed = grainFed.allblobs(1).detector.uvw;

    %%% method minimum squares

    % parameters for lsqcurvefit
    % ux uy uz vx vy vz dx dy dz
    X0 = [detdiru detdirv detpos];

    % Fixfing srot to be used in the fitting function
    srot = reshape(permute(srot, [1 3 2]), [], 3);

    xdata = [srot; dvec];
    ydata = uvdb; % uv coordinates found in the database (Nx2)

    if (debug)
        size(srot)
        size(dvec)
        size(omega)
        size(xdata)
        size(ydata)
    end

    newgeo.uvwfed = uvwfed;
    newgeo.Xzero = X0;

    % calculate the old uvw coordinates from the current geometry
    [Fzero, Mzero] = gtOptimizeFindUVW(X0, xdata, csam, uvorig);
    Fzero(:, 3) = mod(omega, 360)/omstep;
    newgeo.Fzero = Fzero;
    % current detector reference system
    newgeo.Mzero = Mzero;

    % comparison between f(x, xdata) and ydata
    % min sum [(f(x, xdata, par)-ydata)^2]
    %  x
    %
    % input: X0    = the current geometry parameters 1x12
    %        xdata = (Nx4, 3) list from [srot;dvec]
    %        ydata = [bbox.xcen, bbox.ycen]
    %        par   = list of constants in the function (csam, detpos)
    %
    % output: X    = the optimized geometry parameters from lsq

    % change some options for the fit
    if (~exist('varargin', 'var')|| isempty(varargin))
        options = optimset('TolFun', 1e-2, 'MaxFunEvals', 3000, 'MaxIter', 700);
    else
        options = optimset(varargin);
    end

    disp('Start fitting...')
    f = @(x, xdata) gtOptimizeFindUVW(x, xdata, csam, uvorig);
    [X, ~, ~, ~, ~, ~, ~] = lsqcurvefit(f, X0, xdata, ydata, [], [], options);

    % calculate the new uvw (F) values with the optimized geometry (X)
    [F, M, Xnew] = gtOptimizeFindUVW(X, xdata, csam, uvorig);
    F(:, 3) = mod(omega, 360) / omstep;
    newgeo.X = X;
    newgeo.F = F;
    newgeo.M = M;
    % recalculate uvw (Fnew) with the orthonormal axes (Xnew)

    [Fnew, Mnew] = gtOptimizeFindUVW(Xnew, xdata, csam, uvorig);
    Fnew(:, 3) = mod(omega, 360) / omstep;

    newgeo.Xnew = Xnew;
    newgeo.Fnew = Fnew;
    % new detector reference system
    newgeo.Mnew = Mnew;

    % calculate the dot products
    newgeo.dotuv = dot(Mnew(1, :), Mnew(2, :));
    newgeo.dotut = dot(Mnew(1, :), Mnew(3, :));
    newgeo.dotvt = dot(Mnew(2, :), Mnew(3, :));

    % get the new geometry
    newgeo.detdiru0 = Mnew(1, :);
    newgeo.detdirv0 = Mnew(2, :);
    newpos = Xnew(7:9) .* pixelsize;

    newgeo.detdir0 = Mnew(3, :);

    % new center
    oldpos = X0(7:9) .* pixelsize;
    newgeo.detpos0 = newpos;
    olddiru = X0(1:3);
    olddirv = X0(4:6);
    shift = oldpos - newpos;
    newcenu = uvorig(1) + dot(shift, olddiru) ./ pixelsize;
    newcenv = uvorig(2) + dot(shift, olddirv) ./ pixelsize;

    newgeo.newdetucen0 = newcenu;
    newgeo.newdetvcen0 = newcenv;
    newgeo.dist = norm(newpos);

    toc(c)
end
