function bbox=gtOptimizeFindSpotsWoIndexter(parameters,uvfed,omegafed,bbsizemean,areamin,areamax)
% bbox=gtOptimizeFindSpotsWoIndexter(parameters,uvfed,omegafed,bbsizemean,areamin,areamax)

if nargin < 6
    disp('Usage: bbox=gtOptimizeFindSpotsWoIndexter(parameters,uvfed,omegafed,bbsizemean,areamin,areamax)')
    return
end

if isfield(parameters,'acq')
    name=parameters.acq.name;
    omstep=180/parameters.acq.nproj;
else
    name=parameters.name;
    omstep=180/parameters.nproj;
end
bbsize=bbsizemean; % pixels

expected_id=[];
image=[];
omega=[];
x0=[];
y0=[];
xsize=[];
ysize=[];
xcen=[];
ycen=[];
feduv=[];
area=[];
ind=[];
dbuv=[];

dif_Xsize=bbsize;
dif_Ysize=bbsize;
deltaImage=10;


gtDBConnect();

for i=1:length(uvfed)
    
    myquery=sprintf(['select difspotID from %sdifspot where '...
        '(%d between CentroidImage-%d and CentroidImage+%d) '...
        'and (abs(%sdifspot.CentroidX-%f)/%d)<0.5 '...
        'and (abs(%sdifspot.CentroidY-%f)/%d)<0.5 '...   
        'and (Area between %d and %d) '...
        'order by ((abs(BoundingBoxXsize-%d)/%d)+(abs(BoundingBoxYsize-%d)/%d)) limit 1'],...
        name,...
        round(omegafed(i)/omstep),deltaImage,deltaImage,...
        name, uvfed(i,1), dif_Xsize,...
        name, uvfed(i,2), dif_Ysize,...
        areamin,areamax,...
        dif_Xsize, dif_Xsize, dif_Ysize, dif_Ysize);
    
        [difspotID] = mym(myquery);
    
    if ~isempty(difspotID)
        expected_id=[expected_id; difspotID];
        secquery=sprintf('select BoundingBoxXOrigin, BoundingBoxYOrigin, BoundingBoxXSize, BoundingBoxYSize, CentroidX, CentroidY, CentroidImage, Area from %sdifspot where difspotID=%d',...
            name, difspotID);
        
        [BoundingBoxXorigin, BoundingBoxYorigin, BoundingBoxXsize, BoundingBoxYsize, CentroidX, CentroidY, CentroidImage, Area]=mym(secquery);
        
        % fprintf('CentroidImage between CentroidImage-10 and CentroidImage+10 = %d',CentroidImage)
        % fprintf('abs(CentroidX-uvfed(i,1))/dif_Xsize = %f',abs(CentroidX-uvfed(i,1))/dif_Xsize)
        % fprintf('abs(CentroidY-uvfed(i,2))/dif_Ysize = %f',abs(CentroidY-uvfed(i,2))/dif_Ysize)
        % fprintf('Area between %d and %d = %d',areamin,areamax,Area)
        
        
        
        image=[image; CentroidImage];
        omega=[omega; CentroidImage*omstep];
        x0=[x0; BoundingBoxXorigin];
        y0=[y0; BoundingBoxYorigin];
        xsize=[xsize; BoundingBoxXsize];
        ysize=[ysize; BoundingBoxYsize];
        xcen=[xcen; CentroidX];
        ycen=[ycen; CentroidY];
        uvdb=[CentroidX CentroidY];
        dbuv=[dbuv;uvdb];
        area=[area; Area];
        ind=[ind;i];
        feduv=[feduv;uvfed(i,:)];
    end
%     
end

bbox.expected_id=expected_id;
bbox.x0=x0;
bbox.y0=y0;
bbox.xsize=xsize;
bbox.ysize=ysize;
bbox.xcen=xcen;
bbox.ycen=ycen;
bbox.image=image;
bbox.omega=omega;
bbox.area=area;
bbox.dbuv=dbuv;
bbox.all=[expected_id x0 y0 xcen ycen xsize ysize omega];
bbox.title='expected_id x0 y0 xcen ycen xsize ysize omega';
bbox.query={'CentroidImage',[-deltaImage deltaImage],'Area',[areamin areamax],...
    'CentroidX-u', [dif_Xsize 0.5],...
    'CentroidY-v', [dif_Ysize 0.5]};
bbox.ind=ind;
bbox.feduv=feduv;

end
