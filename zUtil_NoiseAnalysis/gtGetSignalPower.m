function p = gtGetSignalPower(s)
    if (iscell(s))
        p = sum(cellfun(@gtGetSignalPower, reshape(s, [1, numel(s)]) ));
    else
        p = gtImgMeanValue(s .* s);
    end
end