function e = gtGetSignalEnergy(s)
    if (iscell(s))
        e = sum(cellfun(@gtGetSignalEnergy, reshape(s, [1, numel(s)])));
    else
        e = sum(sum(abs(s)));
        e = sum(e(:)) / numel(s);
    end
end