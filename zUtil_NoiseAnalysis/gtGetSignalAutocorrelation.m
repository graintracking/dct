function s = gtGetSignalAutocorrelation(s)
    s = convn(s, s, 'same') / numel(s);
end