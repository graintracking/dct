function n = gtGenerateNonUniformAdditiveWhiteNoise(dims, thetatypes, cryst, total_energy, data_type)
    if (~exist('data_type', 'var'))
        data_type = 'single';
    end

    n = cell(dims);

    coeffs = cryst.int(thetatypes);
    coeffs = coeffs ./ norm(coeffs);
    coeffs = 1 ./ coeffs;
    coeffs = coeffs ./ norm(coeffs);

    summed_energy = 0;
    for ii = 1:numel(n)
        n{ii} = coeffs(ii) .* rand(dims{ii}, data_type);
        summed_energy = summed_energy + gtGetSignalEnergy(n{ii});
    end

    for ii = 1:numel(n)
        n{ii} = n{ii} * (total_energy / summed_energy);
    end
end
