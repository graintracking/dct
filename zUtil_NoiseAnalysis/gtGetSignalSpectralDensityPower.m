function s = gtGetSignalSpectralDensityPower(s)
    s = gtGetSignalAutocorrelation(s);
    s = fftn(ifftshift(s));
end