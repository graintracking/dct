function n = gtGenerateAdditiveWhiteNoise(dims, total_energy, data_type)
    if (~exist('data_type', 'var'))
        data_type = 'single';
    end
    if (iscell(dims))
        n = cell(dims);
        summed_energy = 0;
        for ii = 1:numel(n)
            n{ii} = rand(dims{ii}, data_type);
            summed_energy = summed_energy + gtGetSignalEnergy(n{ii});
        end
        for ii = 1:numel(n)
            n{ii} = n{ii} * (total_energy / summed_energy);
        end
    else
        n = rand(dims, data_type);
        n = n * (total_energy / gtGetSignalEnergy(n));
    end
end
