function n = gtGenerateExtintionNoise(blobs, attenuation_range, num, data_type)
    if (~exist('data_type', 'var'))
        data_type = 'single';
    end

    if (num)
        blob_indxs = round((numel(blobs)-1) * rand(1, num) +1);
    else
        num = numel(blobs);
        blob_indxs = 1:num;
    end

    attenuation_vals = (rand(1, num) * (max(attenuation_range) - min(attenuation_range))) + min(attenuation_range);

    max_radiuses = cellfun(@(x)(norm(size(x)) / 2), blobs);
    max_radiuses = reshape(max_radiuses, [1 numel(max_radiuses)]) .* rand(1, numel(max_radiuses)) ./ 2;

    min_radiuses = cellfun(@(x)(norm(size(x)) / 5), blobs);
    min_radiuses = reshape(min_radiuses, [1 numel(min_radiuses)]) .* rand(1, numel(min_radiuses)) ./ 2;

    n = cell(size(blobs));

    % Do the attentuations
    for ii = 1:numel(blob_indxs)
        indx = blob_indxs(ii);
        n{indx} = blobs{indx};

        att_indxes = getIndexes(size(n{indx}), max_radiuses(indx), min_radiuses(indx));
        n{indx}(att_indxes) = n{indx}(att_indxes) * (1 - attenuation_vals(ii));
    end

    % Now let's convert to noise
    for ii = 1:numel(n)
        if (isempty(n{ii}))
            n{ii} = zeros(size(blobs{ii}), data_type);
        else
            n{ii} = n{ii} - blobs{ii};
        end
    end
end

function indx = getIndexes(dims, max_radius, min_radius)
    center = dims .* rand(1, 3) / 3;

    [xx, yy, zz] = meshgrid(1:dims(2), 1:dims(1), 1:dims(3));
    XX = xx - (center(2) + dims(2)) /2;
    YY = yy - (center(1) + dims(1)) /2;
    ZZ = zz - (center(3) + dims(3)) /2;

    radius = (max_radius - min_radius) * rand(1) + min_radius;
    indx = (XX .^ 2 + YY .^ 2 + ZZ .^ 2) <= (radius ^ 2);
end