function r = gtGetSignalNoiseRatio(s, n, unit)
    if (~exist('unit', 'var'))
        unit = 'logaritmic';
    end
    r = gtGetSignalPower(s) / gtGetSignalPower(n);
    switch(unit)
        case 'decimal'
            % Nothing
        case 'logaritmic'
            r = 10 * log10(r);
    end
end