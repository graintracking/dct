function a = gtEvalSignalAutocorrelation(s, t)
    a = gtImgMeanValue(s .* circshift(s, t));
end