function gtError(varargin)
% Overriding function of the builtin one.
    mexc = GtException(varargin{:});
    throwAsCaller(mexc);
end
