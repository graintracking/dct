function [varargout] = gtPrintException(ex, varargin)
% gtPrintException(ex, varargin)
% prints the exception information, where varargin is the error message and is
% passed to sprintf

    outString = sprintf('\n');
    if (nargin > 1)
        outString = [outString '--> ' sprintf(varargin{:}) ' <-- Error:' sprintf('\n')];
        outString = [outString print_exception(ex)];
    else
        outString = [outString sprintf('-- Error:\n')];
        outString = [outString print_exception(ex)];
    end
    outString = [outString sprintf('\n')];

    if (nargout == 0)
        disp(outString);
    else
        varargout{1} = outString;
    end
end

function out = print_exception(ex, indent)
    if (~exist('indent', 'var'))
        indent = '';
    end

    cPlain = gtGetANSIColour();
    cBold = gtGetANSIColour('bold');

    out = '';
    out = [out sprintf([indent 'Identifier: ' cBold ex.identifier cPlain '\n'])];
    out = [out sprintf([indent '   Message: ' ex.message '\n'])];

    if (~isempty(ex.stack))
        out = [out sprintf([indent '     Stack:\n'])];
        for ii = 1:length(ex.stack)
            out = [out print_stack_entry(ex.stack(ii), indent)];
        end
    end

    out = [out print_cause(ex, indent)];
end

function out = print_stack_entry(entry, indent)
    out = '';
    out = [out indent '      - File: ' gtPrintHyperlink(entry.file, entry.line) sprintf('\n')];
    out = [out indent '        Name: ' entry.name sprintf('\n')];
    out = [out indent '        Line: ' colourString(num2str(entry.line), 'magenta') sprintf('\n')];
end

function out = print_cause(ex, indent)
    out = '';
    if (~isempty(ex.cause))
        out = [out sprintf([indent '  * Causes:\n'])];
        for ii = 1:length(ex.cause)
            out = [out print_exception(ex.cause{ii}, ['  ' indent])];
        end
    end
end

function out = gtPrintHyperlink(destfile, destline)
    if (usejava('desktop'))
        out = ['<a href="matlab: opentoline(''' destfile ''', ' num2str(destline) ', 0)" style="font-weight:bold">' destfile '</a>'];
    else
        out = colourString(destfile, 'green');
    end
end

function out = colourString(content, color)
    if (usejava('desktop'))
        out = ['<a href="" style="text-decoration:none;color:' gtGetHTMLColour(color) '">' content '</a>'];
    else
        out = [sprintf(gtGetANSIColour(color)) content sprintf(gtGetANSIColour())];
    end
end



