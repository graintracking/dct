
clear MeCa1 MeCa2 MeCa3 Me
clear GtException

try
  meCa1 = MException('TestError:error2', 'message2');
  meCa2 = MException('TestError:error3', 'message3');
  meCa3 = MException('TestError:error4', 'message4');
  meCa1 = addCause(meCa1, meCa2);
  meCa1 = addCause(meCa1, meCa3);

  me = MException('TestError:error1', 'message?');
  me = addCause(me, meCa1);

  throw(me)
catch mexc
  disp('Without message')
  gtPrintException(mexc)
  disp(' ')

  disp('With message')
  errorMessage = 'Couldn''t process whatever.';
  gtPrintException(mexc, errorMessage)

  returnedString = gtPrintException(mexc, errorMessage);
  disp(' ')

  disp('Returned string - With message')
  disp(returnedString)
end

clear MeCa1 MeCa2 MeCa3 Me
clear GtException

disp(' ')

disp('Testing GtException - With message')

meCa1 = GtException('GtTestError:error2', 'GtMessage2');
meCa2 = GtException('GtTestError:error3', 'GtMessage3');
meCa3 = GtException('GtTestError:error4', 'GtMessage4');
meCa1 = meCa1.addCause(meCa2);
meCa1 = meCa1.addCause(meCa3);

me = GtException('GtTestError:error1', 'GtMessage?');
disp('Adding Cause')
me = me.addCause(meCa1);

disp('Using getReport - return')
disp(getReport(me))

disp('Using getReport - ans')
getReport(me)

disp('Letting Matlab use getReport')
throw(me)
