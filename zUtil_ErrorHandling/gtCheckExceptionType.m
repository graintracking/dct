function got_it = gtCheckExceptionType(mexc, type, action)
% GTCHECKEXCEPTIONTYPE Checks the type of the exception, and if requested
%       performs an action.
% Input:
%       mexc - the exception
%       type - the type to check (can be a regular expression)
%       action - function handle to perform in case of match

    got_it = ~isempty(regexp(mexc.identifier, type, 'once'));

    if (got_it && exist('action', 'var') && isa(action, 'function_handle'))
        action();
    end
end
