classdef GtException < MException
% extension class for matlab's builtin MException
    methods
        function obj = GtException(varargin)
            obj = obj@MException(varargin{:});
        end
        function report = getReport(obj, varargin)
            if (desktop('-inuse'))
                try
                    report = getReport@MException(obj, varargin{:});
                catch mexc
                    mexc.throwAsCaller();
                end
            else
                report = gtPrintException(obj);
            end
        end
    end
end
