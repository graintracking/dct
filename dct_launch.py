#!/usr/bin/env python
"""
Created on Feb 3, 2013

@author: vigano
"""

import sys
import os

import zUtil_Python as dct


class DCTLauncher(object):
    def __init__(self, args):
        self.initialized = False

        self.dct_dir = os.path.abspath(os.path.dirname(args[0]))

        try:
            if len(args) == 1:
                print("Defaulting to matlab")
                args.append("matlab")

            cmd = args[1]
            if cmd in ("-h", "--help"):
                self.command = "help"
            else:
                self.command = cmd

            self.args = args[2:]
            self.initialized = True
        except ValueError as ex:
            dct.io_xml.DCTOutput.printError(ex.args)

    def run(self):
        if self.command == "help":
            self.print_help()
        elif self.command == "matlab":
            self._launch_matlab()
        elif self.command == "matlab_script":
            self._launch_matlab_script()
        elif self.command == "update":
            self._launch_update()
        elif self.command == "compile_mex":
            self._launch_compile_mex()
        elif self.command == "compile_matlab":
            self._launch_compile_matlab()
        elif self.command == "update_conf":
            self._launch_update_conf()
        elif self.command == "reset_conf":
            self._launch_cleanup_conf()
        elif self.command == "batch":
            self._launch_batch()
        elif self.command == "make":
            self._launch_make()
        elif self.command == "publish":
            self._launch_publish()
        else:
            raise ValueError("Command not recognized: '%s'" % self.command)

    def _launch_matlab(self):
        if len(self.args) > 0 and self.args[0] in ("-h", "--help"):
            print(" Usage:")
            print("   # python %s matlab [options]" % os.path.basename(__file__))
            print(" Where [options] are the options for matlab")
        else:
            invoker = dct.matlab_invocation.DCTMatlabInvocation(dct_dir=self.dct_dir, extra_args=self.args)

            func_builder = dct.compile_matlab_functions.FunctionsBuilder(
                matlab_path="", dct_dir=self.dct_dir, verbose_lvl=0
            )
            func_builder.generateMfile()

            invoker.invoke()

    def _launch_matlab_script(self):
        if len(self.args) > 0 and self.args[0] in ("-h", "--help"):
            print(" Usage:")
            print("   # python %s matlab_script [commands]" % os.path.basename(__file__))
            print(" Options:")
            print("  -h | --help : to show this help")
            print("  --profile : to specify the type of profiling. Options: 'cuda', 'oprofile'")
            print("  --debug : to specify the type of profiling. Options: 'valgrind'")
            print("  --script : to specify the script/commands to be launched")
        else:
            decoded_args = dct.matlab_invocation.DCTMatlabInvocation.filter_parameters(self.args)

            invoker = dct.matlab_invocation.DCTMatlabInvocation(
                dct_dir=self.dct_dir, extra_cmds=decoded_args["script"], skip_functions_check=True
            )

            invoker.invoke(profiler=decoded_args["profile"], debugger=decoded_args["debug"])

    def _launch_update(self):
        util = dct.utils_git.DCTGit(self.dct_dir)
        util.updateRepo()

    def _launch_compile_mex(self):
        try:
            args_compile = [""] + self.args
            mex_builder = dct.compile_mex_functions.MexBuilder.getInstanceFromArgs(args_compile, dct_dir=self.dct_dir)
            mex_builder.find_mex_files()
            mex_builder.compile_mex_files()
        except ValueError as ex:
            if len(ex.args) > 0 and not (ex.args[0] == ""):
                dct.io_xml.DCTOutput.printError(ex.args)
            dct.compile_mex_functions.MexBuilder(dct_dir=self.dct_dir).print_help()
        except EnvironmentError as ex:
            if ex.filename is not None:
                args = ", ".join([str(x) for x in ex.args])
                ":".join((args, " "))
                dct.io_xml.DCTOutput.printError((args, ex.filename))
            else:
                dct.io_xml.DCTOutput.printError(ex.args)
        except Exception as ex:
            dct.io_xml.DCTOutput.printError(ex.args)

    def _launch_compile_matlab(self):
        try:
            args_compile = [""] + self.args
            func_builder = dct.compile_matlab_functions.FunctionsBuilder.getInstanceFromArgs(args_compile, dct_dir=self.dct_dir)

            if func_builder.do_generate is True:
                func_builder.generateMfile()
            if func_builder.do_compile is True:
                func_builder.compileFuncs()
        except ValueError as ex:
            if len(ex.args) > 0 and ex.args[0] != "":
                dct.io_xml.DCTOutput.printError(ex.args)
            dct.compile_matlab_functions.FunctionsBuilder(dct_dir=self.dct_dir).print_help()

    def _launch_update_conf(self):
        upd = dct.io_xml.DCTConfUpdater(self.dct_dir)
        confexamplefile = os.path.join(self.dct_dir, "zUtil_Conf", "conf.default.xml")
        upd.safely_install_new_file(confexamplefile, "conf.xml")

    def _launch_cleanup_conf(self):
        upd = dct.io_xml.DCTConfUpdater(self.dct_dir)
        upd.cleanup("conf.xml")

    def _launch_batch(self):
        args = [__file__] + self.args
        bt = dct.batch.DCTBatch.getInstanceFromArgs(args)
        bt.execute(self.args)

    def _launch_make(self):
        if len(self.args) == 0:
            raise ValueError("Not enough arguments")

        if self.args[0] == "install_zip":
            builder = dct.distrib.DCTMakeInstallBundle()
            if len(self.args) >= 2:
                builder.run(self.dct_dir, destFile=self.args[1])
            else:
                builder.run(self.dct_dir)
        else:
            raise ValueError("Unsupported make option: '%s'" % " ".join(self.args))

    def _launch_publish(self):
        if len(self.args) == 0:
            raise ValueError("Not enough arguments")

        if self.args[0] == "install_zip":
            utils = dct.utils_git.DCTGit(self.dct_dir)
            sf_account = utils.getSourceForgeAccount()

            if len(self.args) >= 2:
                filename = self.args[1]
            else:
                filename = "dct-install.zip"
            destination = "%s,dct@frs.sourceforge.net:/home/frs/project/dct/%s" % (sf_account, os.path.basename(filename))
            utils_p = dct.utils_platform.UtilsFactory.getMachineDep()
            utils_p.scpFile(filename, destination)
        else:
            raise ValueError("Unsupported publish option: '%s'" % " ".join(self.args))

    def print_help(self):
        launchname = os.path.basename(__file__)
        print('"%s" launches dct or one of the maintenance routines in the DCT directory: "%s"' % (launchname, self.dct_dir))
        print(" Usage:")
        print("   # python %s command [options]" % launchname)
        print("   # python %s command -h | --help : to show command's help" % launchname)
        print("   # python %s -h | --help : to show this help" % launchname)
        print(" Commands:")
        print("   matlab\n   update\n   compile_mex\n   compile_matlab\n   update_conf\n   batch")


if __name__ == "__main__":
    launcher = DCTLauncher(sys.argv)
    if launcher.initialized is False:
        launcher.print_help()
        sys.exit(1)

    try:
        launcher.run()
    except SystemError as ex:
        dct.io_xml.DCTOutput.printError(ex.args)
        print("Try running:\n  # python %s update_conf" % __file__)
    except ValueError as ex:
        dct.io_xml.DCTOutput.printError(ex.args)
