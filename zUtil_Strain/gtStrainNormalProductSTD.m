function stdnn=gtStrainNormalProductSTD(n1,n2,dn1z,dn2z,da1,da2,...
                                        std_allnz1,std_allnz2,std_alla1,std_alla2)

% Expected std of dot product n1*n2.
%
% INPUT  n1,n2             normal vectors
%        dn1z,dn2z         measured error of the variables (~2std); or NaN; 
%        da1,da2             z components of n and the azimuth in radians (!)
%        std_avnz,std_ava  computed std (1 and not 2 std); 
%                            used if the ones above are unknown
%

% STD to be used; known or the average
if isnan(dn1z)
  stdn1z=std_allnz1;
else
  stdn1z=dn1z/2;
end
if isnan(da1)
  stda1=std_alla1;
else
  stda1=da1/2;
end

if isnan(dn2z)
  stdn2z=std_allnz2;
else
  stdn2z=dn2z/2;
end
if isnan(da2)
  stda2=std_alla2;
else
  stda2=da2/2;
end

n1z=n1(3);
n2z=n2(3);
a1=atan2(n1(2),n1(1)); % azimuth angle
a2=atan2(n2(2),n2(1));

% Normal product:
% F=n1*n2=n1x*n2x+n1y*n2y+n1z*n2z
% F=Fx+Fy+Fz

% Derivatives of Fx:
% Fx=sqrt(1-n1z^2)*cosd(a1)*sqrt(1-n2z^2)*cosd(a2)
dFxdn1z=-n1z/sqrt(1-n1z^2)*cos(a1)*sqrt(1-n2z^2)*cos(a2);
dFxdn2z=sqrt(1-n1z^2)*cos(a1)*(-n2z)/sqrt(1-n2z^2)*cos(a2);
dFxda1=sqrt(1-n1z^2)*(-sin(a1))*sqrt(1-n2z^2)*cos(a2);
dFxda2=sqrt(1-n1z^2)*cos(a1)*sqrt(1-n2z^2)*(-sin(a2));

% Derivatives of Fy:
% Fx=sqrt(1-n1z^2)*sind(a1)*sqrt(1-n2z^2)*sind(a2)
dFydn1z=-n1z/sqrt(1-n1z^2)*sin(a1)*sqrt(1-n2z^2)*sin(a2);
dFydn2z=sqrt(1-n1z^2)*sin(a1)*(-n2z)/sqrt(1-n2z^2)*sin(a2);
dFyda1=sqrt(1-n1z^2)*cos(a1)*sqrt(1-n2z^2)*sin(a2);
dFyda2=sqrt(1-n1z^2)*sin(a1)*sqrt(1-n2z^2)*cos(a2);

% Derivatives of Fz:
% Fz=n1z*n2z
dFzdn1z=n2z;
dFzdn2z=n1z;
dFzda1=0;
dFzda2=0;

termn1z=dFxdn1z^2 + dFydn1z^2 + dFzdn1z^2 + ...
  2*dFxdn1z*dFydn1z + 2*dFxdn1z*dFzdn1z + 2*dFydn1z*dFzdn1z ;
termn2z=dFxdn2z^2 + dFydn2z^2 + dFzdn2z^2 + ...
  2*dFxdn2z*dFydn2z + 2*dFxdn2z*dFzdn2z + 2*dFydn2z*dFzdn2z ;
terma1=dFxda1^2 + dFyda1^2 + dFzda1^2 + ...
  2*dFxda1*dFyda1 + 2*dFxda1*dFzda1 + 2*dFyda1*dFzda1 ;
terma2=dFxda2^2 + dFyda2^2 + dFzda2^2 + ...
  2*dFxda2*dFyda2 + 2*dFxda2*dFzda2 + 2*dFyda2*dFzda2 ;

varnn= termn1z*stdn1z^2 + termn2z*stdn2z^2 + terma1*stda1^2 + terma2*stda2^2;

stdnn=sqrt(varnn);

end



























































