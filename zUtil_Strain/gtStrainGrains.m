% FUNCTION grainsout=gtStrainGrains(grainsinp,ACM,latticeparref)
%
% INPUT
% grainsinp: grain structure
% ACM: angular consistency matrix  ( ACM=gtAngConsMat(spacegroup) )
% latticeparref (optional): lattice parameter, reference at zero strain
%                            if not specified, parameters.cryst.latticepar(1)

function grainsout=gtStrainGrains(grainsinp,limiterr_flag,ACM,latticeparref)

if ~exist('limiterr_flag','var')
  limiterr_flag=false;
end

if ~exist('ACM','var')
  ACM=[];
end
parameters=[];
if ~exist('latticeparref','var')
  load('parameters.mat');
  latticeparref=parameters.cryst.latticepar(1);
end

if isempty(ACM)
  load('parameters.mat');
  ACM=gtAngConsMat(parameters.cryst.spacegroup);
end

tic

nof_grains=length(grainsinp);

% Handle cell arrays or a single one
if nof_grains==1
  grains{1}=grainsinp;
else
  grains=grainsinp;
end

faults=[];
comments=[];

errest=gtEstimateErrors(grainsinp);

for i=1:nof_grains

  grains{i}=gtStrainPlanes(grains{i},latticeparref);
  
  grains{i}=gtStrainTensorFit(grains{i},ACM,errest,limiterr_flag);
 
  if ~isnan(grains{i}.strain.comment)
    faults=[faults; grains{i}.id];
    comments=[comments; grains{i}.strain.comment];
  end
  
end

disp(' ')
fprintf('Strain tensor determination failed in %d grains with the following ID-s:',length(faults));
for j=1:length(faults)
  fprintf('   %4d   %s',faults(j), comments(j,:));
end
    

% Handle cell arrays or a single one as output
if nof_grains==1
  grainsout=grains{1};
else
  grainsout=grains;
end

grain=grainsout;

save('straingrains_lastrun.mat','grain');

toc

end
