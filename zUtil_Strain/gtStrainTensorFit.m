% Linear solution (or least squares fit) of the strain tensor in a grain.
% Uses  grain.uni.pl  and  grain.uni.plstrain  values.


function grain=gtStrainTensorFit(grain,ACM,errest,limiterr_flag)

nof_unipl=size(grain.uni.pl,1);

% Set output
grain.strain.ST=NaN(3);
grain.strain.ST_sp=NaN(3);
grain.strain.ST_dev=NaN(3);

grain.strain.Eps=NaN(1,6);

grain.strain.std=NaN(1,6);
grain.strain.std_sp=NaN(1,6);
grain.strain.std_dev=NaN(1,6);

grain.strain.stdy_sp=NaN;
grain.strain.stdy_dev=NaN;

grain.strain.errfact=NaN(6,3);
grain.strain.errfact_sp=NaN(6,3);
grain.strain.errfact_dev=NaN(6,3);

grain.strain.straininlab=NaN(1,3);

grain.strain.pstrain=NaN(1,3);
grain.strain.pstraindir=NaN(3); % principal dirs are in columns
grain.strain.pstrainmax=NaN;
grain.strain.pstrainmaxdir=NaN(1,3);

grain.strain.comment=NaN;

if size(grain.uni.pl,1)<3
  grain.strain.comment='Not enough unique planes.';
  return
end

%% Prepare input values %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Linear operators to create the system of equations
Lop5=@(a,b) [a(1)*b(1)-a(3)*b(3), a(2)*b(2)-a(3)*b(3), a(1)*b(2)+a(2)*b(1), ...
     a(1)*b(3)+a(3)*b(1), a(2)*b(3)+a(3)*b(2)];
Xop5=@(a,b) (a*b')*Lop5(a,a)+(a*b')*Lop5(b,b)-2*Lop5(a,b);
   
Lop6=@(a,b) [a(1)*b(1), a(2)*b(2), a(3)*b(3), a(1)*b(2)+a(2)*b(1), ...
     a(1)*b(3)+a(3)*b(1), a(2)*b(3)+a(3)*b(2)];
Xop6=@(a,b) (a*b')*Lop6(a,a)+(a*b')*Lop6(b,b)-2*Lop6(a,b);

% System of equation
%  number of equations:
Ns_dev=nof_unipl*(nof_unipl-1)/2; % deviatoric (angles)
Ns_sp=nof_unipl;                  % spacing
Ns=Ns_dev+Ns_sp;                  % all

% Left side:
y_dev=zeros(Ns_dev,1); % vector
y_sp=zeros(Ns_sp,1); % vector
y=zeros(Ns,1); % vector

N10n20_dev=zeros(Ns_dev,1); % vector

% Derivatives of the plane normal coordinates wrt.
% the base parameters. Diagonal matrices.
Nz_dy=zeros(nof_unipl); % dnz/d(dy)
Nz_dz=zeros(nof_unipl); % dnz/d(dz)
Nz_om=zeros(nof_unipl); % dnz/d(om)
Az_dy=zeros(nof_unipl); % daz/d(dy)
Az_dz=zeros(nof_unipl); % daz/d(dz)
Az_om=zeros(nof_unipl); % daz/d(om)
Th_dy=zeros(nof_unipl); % dth/d(dy)
Th_dz=zeros(nof_unipl); % dth/d(dz)
Th_om=zeros(nof_unipl); % dth/d(om)

% Derivatives of y wrt. the (plane) normal coordinates
Y_nz_dev=zeros(Ns_dev,nof_unipl); % dy/nz
Y_az_dev=zeros(Ns_dev,nof_unipl); % dy/az
Y_th_dev=zeros(Ns_dev,nof_unipl); % dy/th

Y_nz_sp=zeros(Ns_sp,nof_unipl); % dy/nz
Y_az_sp=zeros(Ns_sp,nof_unipl); % dy/az
Y_th_sp=zeros(Ns_sp,nof_unipl); % dy/th
Y_nz=zeros(Ns,nof_unipl); % dy/nz
Y_az=zeros(Ns,nof_unipl); % dy/az
Y_th=zeros(Ns,nof_unipl); % dy/th

% Derivatives of y wrt. the base coordinates
Y_dy_dev=zeros(Ns_dev,nof_unipl); % dy/d(dy)
Y_dz_dev=zeros(Ns_dev,nof_unipl); % dy/d(dz)
Y_om_dev=zeros(Ns_dev,nof_unipl); % dy/d(om)

Y_dy_sp=zeros(Ns_sp,nof_unipl); % dy/d(dy)
Y_dz_sp=zeros(Ns_sp,nof_unipl); % dy/d(dz)
Y_om_sp=zeros(Ns_sp,nof_unipl); % dy/d(om)

Y_dy=zeros(Ns,nof_unipl); % dy/d(dy)
Y_dz=zeros(Ns,nof_unipl); % dy/d(dz)
Y_om=zeros(Ns,nof_unipl); % dy/d(om)

% Standard deviation and variance of y-s and the base coordinates
Std_y_dev=zeros(Ns_dev,1);
Std_y_sp=zeros(Ns_sp,1);
Std_y=zeros(Ns,1);
Var_y_dev=zeros(Ns_dev,1);
Var_y_sp=zeros(Ns_sp,1);
Var_y=zeros(Ns,1);

Std_dy=zeros(nof_unipl,1);
Std_dz=zeros(nof_unipl,1); 
Std_om=zeros(nof_unipl,1); 
Var_dy=zeros(nof_unipl,1);
Var_dz=zeros(nof_unipl,1);
Var_om=zeros(nof_unipl,1);

% Derivatives of epsilon wrt. the y-s
E_dev=zeros(5,Ns_dev);
E_sp=zeros(6,Ns_sp);
E=zeros(6,Ns);

Eb_dev=zeros(5,Ns_dev);
Eb_sp=zeros(6,Ns_sp);
Eb=zeros(6,Ns);

% Derivatives of epsilon wrt. the base coordinates
Gdy_devo=zeros(5,nof_unipl);
Gdz_devo=zeros(5,nof_unipl);
Gom_devo=zeros(5,nof_unipl);

Gdy_dev=zeros(6,nof_unipl);
Gdz_dev=zeros(6,nof_unipl);
Gom_dev=zeros(6,nof_unipl);

Gdy_sp=zeros(6,nof_unipl);
Gdz_sp=zeros(6,nof_unipl);
Gom_sp=zeros(6,nof_unipl);

Gdy=zeros(6,nof_unipl);
Gdz=zeros(6,nof_unipl);
Gom=zeros(6,nof_unipl);

% Right side: model matrix
X_dev=zeros(Ns_dev,5);
X_sp=zeros(Ns_sp,6);
X=zeros(Ns,6);

% Plane indices used for a given equation
Plid_dev=zeros(Ns_dev,2);
Plid_sp=zeros(Ns_sp,2);
Plid=zeros(Ns,2);

%% Variance of dy,dz and Nz,Az,Th matrices
for i=1:nof_unipl

%   if isnan(grain.uni.ddy(i))
%     Std_dy(i)=errest.std_dy;
%   else
%     Std_dy(i)=sqrt(2)*grain.uni.ddy(i);
%   end
%   Var_dy(i)=Std_dy(i)^2;
%   
%   if isnan(grain.uni.ddz(i))
%     Std_dz(i)=errest.std_dz;
%   else
%     Std_dz(i)=sqrt(2)*grain.uni.ddz(i);
%   end
%   Var_dz(i)=Std_dz(i)^2;
 
  if isnan(grain.uni.ddy(i)) 
    if length(errest.std_dy_thetat) >= grain.uni.thetatype(i)
      Std_dy(i)=errest.std_dy_thetat(grain.uni.thetatype(i));
      Std_dz(i)=errest.std_dz_thetat(grain.uni.thetatype(i));
      Std_om(i)=errest.std_om_thetat(grain.uni.thetatype(i));
    else
      Std_dy(i)=errest.std_dy_thetat(grain.uni.thetatype(end));
      Std_dz(i)=errest.std_dz_thetat(grain.uni.thetatype(end));
      Std_om(i)=errest.std_om_thetat(grain.uni.thetatype(end));
    end
  else
    Std_dy(i)=sqrt(2)*abs(grain.uni.ddy(i));
    Std_dz(i)=sqrt(2)*abs(grain.uni.ddz(i));
    Std_om(i)=sqrt(2)*abs(grain.uni.dom(i));
  end
  Var_dy(i)=Std_dy(i)^2;
  Var_dz(i)=Std_dz(i)^2;
  Var_om(i)=Std_om(i)^2;

  
  Nz_dy(i,i)=grain.uni.dnz_ddy(i); % dnz/d(dy); diagonal
  Nz_dz(i,i)=grain.uni.dnz_ddz(i); % dnz/d(dz); diagonal
  Nz_om(i,i)=grain.uni.dnz_dom(i); % dnz/d(om); diagonal
  
  Az_dy(i,i)=grain.uni.daz_ddy(i); % daz/d(dy); diagonal
  Az_dz(i,i)=grain.uni.daz_ddz(i); % daz/d(dz); diagonal
  Az_om(i,i)=grain.uni.daz_dom(i); % daz/d(om); diagonal

  Th_dy(i,i)=grain.uni.dth_ddy(i); % dth/d(dy); diagonal
  Th_dz(i,i)=grain.uni.dth_ddz(i); % dth/d(dz); diagonal
  Th_om(i,i)=grain.uni.dth_dom(i); % dth/d(om); diagonal

end

%% Equations - angles

i=0;

for ii=1:nof_unipl-1
  for jj=ii+1:nof_unipl

  i=i+1;
    
  n1=grain.uni.pl(ii,:); % plane normal
  n2=grain.uni.pl(jj,:);
  
  a1=grain.uni.az(ii); % azimuth
  a2=grain.uni.az(jj);
  
  Plid_dev(i,1:2)=[ii,jj];
  Plid(i,1:2)=[ii,jj];
 
  % LEFT SIDE - measurements of change in n1*n2
  
  % find original (unstrained) angle between those plane normals
  ACV=ACM{grain.uni.thetatype(ii),grain.uni.thetatype(jj)};
  ang=gt2PlanesAngle(n1,n2);
  [minval,minloc]=min(abs(ACV-ang));
  n10n20=abs(cosd(ACV(minloc))); % the original dot product (undeformed)

  % Note: ACV should contain 0<= angles <=90. Two different
  % unique planes can't have 0 deg between them.

  if n1*n2'<0 % if plane normals direction opposite,
    n10n20=-n10n20; % the original dot product should be negative as well
  end

  N10n20_dev(i)=n10n20;
  y_dev(i)=n10n20-n1*n2'; % change in dot product
  y(i)=n10n20-n1*n2'; % change in dot product
  
  % Y MATRICES
  n1z=n1(3);
  n2z=n2(3);
%   a1=atan2(n1(2),n1(1)); % azimuth angle
%   a2=atan2(n2(2),n2(1));

  % Normal product:
  % y=n1*n2=n1x*n2x+n1y*n2y+n1z*n2z
  % y=Yx+Yy+Yz

  % Derivatives of Yx:
  %   Yx=sqrt(1-n1z^2)*cosd(a1)*sqrt(1-n2z^2)*cosd(a2)
  dYx_dn1z=-n1z/sqrt(1-n1z^2)*cos(a1)*sqrt(1-n2z^2)*cos(a2);
  dYx_dn2z=sqrt(1-n1z^2)*cos(a1)*(-n2z)/sqrt(1-n2z^2)*cos(a2);
  dYx_da1=sqrt(1-n1z^2)*(-sin(a1))*sqrt(1-n2z^2)*cos(a2);
  dYx_da2=sqrt(1-n1z^2)*cos(a1)*sqrt(1-n2z^2)*(-sin(a2));

  % Derivatives of Yy:
  %   Yy=sqrt(1-n1z^2)*sind(a1)*sqrt(1-n2z^2)*sind(a2)
  dYy_dn1z=-n1z/sqrt(1-n1z^2)*sin(a1)*sqrt(1-n2z^2)*sin(a2);
  dYy_dn2z=sqrt(1-n1z^2)*sin(a1)*(-n2z)/sqrt(1-n2z^2)*sin(a2);
  dYy_da1=sqrt(1-n1z^2)*cos(a1)*sqrt(1-n2z^2)*sin(a2);
  dYy_da2=sqrt(1-n1z^2)*sin(a1)*sqrt(1-n2z^2)*cos(a2);

  % Derivatives of Yz:
  %   Yz=n1z*n2z
  dYz_dn1z=n2z;
  dYz_dn2z=n1z;
  dYz_da1=0;
  dYz_da2=0;

  % Derivatives of y:
  dY_dn1z=dYx_dn1z+dYy_dn1z+dYz_dn1z;
  dY_dn2z=dYx_dn2z+dYy_dn2z+dYz_dn2z;
  
  dY_da1=dYx_da1+dYy_da1+dYz_da1;
  dY_da2=dYx_da2+dYy_da2+dYz_da2;
  
  % Y matrices
  Y_nz(i,ii)=dY_dn1z; % dy/nz
  Y_az(i,ii)=dY_da1; % dy/az

  Y_nz(i,jj)=dY_dn2z; % dy/nz
  Y_az(i,jj)=dY_da2; % dy/az

  Y_nz_dev(i,ii)=dY_dn1z; % dy/nz
  Y_az_dev(i,ii)=dY_da1; % dy/az

  Y_nz_dev(i,jj)=dY_dn2z; % dy/nz
  Y_az_dev(i,jj)=dY_da2; % dy/az

  
  % MODEL MATRIX
  X_dev(i,1:5)=Xop5(n1,n2);
  X(i,1:6)=Xop6(n1,n2);
  
  end
end


%% Equations - d-spacing

for ii=1:nof_unipl

  i=i+1;
    
  n1=grain.uni.pl(ii,:);
  
  Plid_sp(ii,1:2)=[ii,ii];
  Plid(i,1:2)=[ii,ii];
  
  % LEFT SIDE - measurements of d-spacing (Bragg's law)
  
  % y=reverse strain in each plane
  y_sp(ii)=-grain.uni.plstrain(ii);
  y(i)=-grain.uni.plstrain(ii);

  % Derivatives of y:
  % Y matrices
  Y_th(i,ii)=-(grain.uni.plstrain(ii)+1)/tand(grain.uni.theta(ii)); % dy/dth
  Y_th_sp(ii,ii)=Y_th(i,ii);
  
  % MODEL MATRIX
  X_sp(ii,1:6)=Lop6(n1,n1);
  X(i,1:6)=Lop6(n1,n1);
end

%% Main stuff

% Derivative of y wrt. the base parameters
Y_dy_dev = Y_nz_dev*Nz_dy + Y_az_dev*Az_dy + Y_th_dev*Th_dy ; % d(y)/d(dy); N_dev*n
Y_dz_dev = Y_nz_dev*Nz_dz + Y_az_dev*Az_dz + Y_th_dev*Th_dz ; % d(y)/d(dz); N_dev*n
Y_om_dev = Y_nz_dev*Nz_om + Y_az_dev*Az_om + Y_th_dev*Th_om ; % d(y)/d(om); N_dev*n

Var_y_dev= Y_dy_dev.^2*Var_dy + Y_dz_dev.^2*Var_dz + Y_om_dev.^2*Var_om;
Std_y_dev=sqrt(Var_y_dev);

Y_dy_sp = Y_nz_sp*Nz_dy + Y_az_sp*Az_dy + Y_th_sp*Th_dy ; % d(y)/d(dy); N_sp*n
Y_dz_sp = Y_nz_sp*Nz_dz + Y_az_sp*Az_dz + Y_th_sp*Th_dz ; % d(y)/d(dz); N_sp*n
Y_om_sp = Y_nz_sp*Nz_om + Y_az_sp*Az_om + Y_th_sp*Th_om ; % d(y)/d(om); N_sp*n

Var_y_sp= Y_dy_sp.^2*Var_dy + Y_dz_sp.^2*Var_dz + Y_om_sp.^2*Var_om;
Std_y_sp=sqrt(Var_y_sp);

Y_dy = Y_nz*Nz_dy + Y_az*Az_dy + Y_th*Th_dy ; % d(y)/d(dy); N*n
Y_dz = Y_nz*Nz_dz + Y_az*Az_dz + Y_th*Th_dz ; % d(y)/d(dz); N*n
Y_om = Y_nz*Nz_om + Y_az*Az_om + Y_th*Th_om ; % d(y)/d(om); N*n

Var_y= Y_dy.^2*Var_dy + Y_dz.^2*Var_dz + Y_om.^2*Var_om;
Std_y=sqrt(Var_y);

% System matrices
A_dev=X_dev./repmat(Std_y_dev,1,5);
A_sp=X_sp./repmat(Std_y_sp,1,6);
A=X./repmat(Std_y,1,6);

b_dev=y_dev./Std_y_dev;
b_sp=y_sp./Std_y_sp;
b=y./Std_y;

% Solution
if grain.uni.nof_unipl>=4
  Eb_dev=inv(A_dev'*A_dev)*A_dev';
else
  Eb_dev(:,:)=NaN;
end

if grain.uni.nof_unipl>=6
  Eb_sp=inv(A_sp'*A_sp)*A_sp';
else
  Eb_sp(:,:)=NaN;
end

if grain.uni.nof_unipl>=3
 Eb=inv(A'*A)*A';
else
 Eb(:,:)=NaN;
end


E_dev=Eb_dev*diag(1./Std_y_dev);
E_sp=Eb_sp*diag(1./Std_y_sp);
E=Eb*diag(1./Std_y);

% Epsilon derivative matrices
%  for 5 epsilon components from fit
Gdy_devo = E_dev*Y_dy_dev;
Gdz_devo = E_dev*Y_dz_dev;
Gom_devo = E_dev*Y_om_dev;
%  extended to 6 components, including epsilon_dev33
Gdy_dev = [Gdy_devo(1:2,:); -Gdy_devo(1,:)-Gdy_devo(2,:); Gdy_devo(3:5,:)];
Gdz_dev = [Gdz_devo(1:2,:); -Gdz_devo(1,:)-Gdz_devo(2,:); Gdz_devo(3:5,:)];
Gom_dev = [Gom_devo(1:2,:); -Gom_devo(1,:)-Gom_devo(2,:); Gom_devo(3:5,:)];

Gdy_sp = E_sp*Y_dy_sp;
Gdz_sp = E_sp*Y_dz_sp;
Gom_sp = E_sp*Y_om_sp;

Gdy = E*Y_dy;
Gdz = E*Y_dz;
Gom = E*Y_om;

Errfact_eps_dev=[Gdy_dev.^2*Var_dy , Gdz_dev.^2*Var_dz , Gom_dev.^2*Var_om];
Var_eps_dev=sum(Errfact_eps_dev,2);
Errfact_eps_dev=Errfact_eps_dev./repmat(Var_eps_dev,1,3);
Std_eps_dev=sqrt(Var_eps_dev);

Errfact_eps_sp=[Gdy_sp.^2*Var_dy , Gdz_sp.^2*Var_dz , Gom_sp.^2*Var_om];
Var_eps_sp=sum(Errfact_eps_sp,2);
Errfact_eps_sp=Errfact_eps_sp./repmat(Var_eps_sp,1,3);
Std_eps_sp=sqrt(Var_eps_sp);

Errfact_eps=[Gdy.^2*Var_dy , Gdz.^2*Var_dz , Gom.^2*Var_om];
Var_eps=sum(Errfact_eps,2);
Errfact_eps=Errfact_eps./repmat(Var_eps,1,3);
Std_eps=sqrt(Var_eps);

%% Solution 

% Singular Value Decomposition (SVD)
%   see e.g. Numerical Recipes
% Gives maximum likelihood fit when including the uncertainty of each
% measurement.
% A=U*S*V'
% inv(A)=V*diag(1./diag(S))*U'
% [U,S,V]=svd(A,0);
% omega=diag(S);
% erev_svd=V*diag(1./diag(S))*U'*b
% Covariance matrix of the fitted vector EDrev_vec
% from the SVD output: not correct for dependent x variables !
% for j=1:6
%   for k=1:6
%     Cov_erev(j,k)=(V(j,:).*V(k,:))*(1./omega.^2);
%   end
% end
% Note that in general Cov_erev=inv(A'*A);
% std_erev=sqrt(diag(Cov_erev));

% Strain components - normal equations
%   this could be done by avoiding the matrix inversion in Eb 
Epsrev_dev=Eb_dev*b_dev;
Epsrev_sp=Eb_sp*b_sp;
Epsrev=Eb*b;

% Strain tensors
Trev_dev=[Epsrev_dev(1) , Epsrev_dev(3) , Epsrev_dev(4) ;...
          Epsrev_dev(3) , Epsrev_dev(2) , Epsrev_dev(5) ;...
          Epsrev_dev(4) , Epsrev_dev(5) , -Epsrev_dev(1)-Epsrev_dev(2) ];
Trev_sp=[Epsrev_sp(1) , Epsrev_sp(4) , Epsrev_sp(5) ;...
         Epsrev_sp(4) , Epsrev_sp(2) , Epsrev_sp(6) ;...
         Epsrev_sp(5) , Epsrev_sp(6) , Epsrev_sp(3) ];
Trev=[Epsrev(1) , Epsrev(4) , Epsrev(5) ;...
      Epsrev(4) , Epsrev(2) , Epsrev(6) ;...
      Epsrev(5) , Epsrev(6) , Epsrev(3) ];

I=eye(3);    

if isnan(Trev_dev(1,1))
  T_dev=NaN(3);
else
  T_dev=inv(I+Trev_dev)-I;
end

if isnan(Trev_sp(1,1))
  T_sp=NaN(3);
else
  T_sp=inv(I+Trev_sp)-I;
end

if isnan(Trev(1,1))
  T=NaN(3);
else
  T=inv(I+Trev)-I;
end

% Principal strains        
[eigvecTmat,eigvalTmat]=eig(T);
eigvalT=[eigvalTmat(1,1) eigvalTmat(2,2) eigvalTmat(3,3)];

[maxval,maxloc]=max(abs(eigvalT));


% Solution output

if limiterr_flag

	if max(Std_eps)>maxval % errors are too large
		grain.strain.comment='Errors are too large.    ';
		return
	end

	if max(Std_eps_dev)>maxval % errors are too large
		%grain.strain.comment='Errors are too large.    ';
		T_dev=NaN(3);
		Std_eps_dev=NaN(6,1);
		Errfact_eps_dev=NaN(6,3);
	end

	if max(Std_eps_sp)>maxval % errors are too large
		%grain.strain.comment='Errors are too large.    ';
		T_sp=NaN(3);
		Std_eps_sp=NaN(6,1);
		Errfact_eps_sp=NaN(6,3);
	end

end

grain.strain.ST=T;
grain.strain.ST_sp=T_sp;
grain.strain.ST_dev=T_dev;

grain.strain.Eps=[T(1,1), T(2,2), T(3,3), T(1,2), T(1,3), T(2,3)];

grain.strain.std=Std_eps';
grain.strain.std_sp=Std_eps_sp';
grain.strain.std_dev=Std_eps_dev';

grain.strain.stdy_sp=mean(Std_y_sp);
grain.strain.stdy_dev=mean(Std_y_dev);

grain.strain.errfact=Errfact_eps;
grain.strain.errfact_sp=Errfact_eps_sp;
grain.strain.errfact_dev=Errfact_eps_dev;

grain.strain.straininlab=diag(T)';

grain.strain.pstrain=eigvalT;
grain.strain.pstraindir=eigvecTmat; % principal dirs are in columns
grain.strain.pstrainmax=eigvalT(maxloc);
grain.strain.pstrainmaxdir=eigvecTmat(:,maxloc)';

  
end



