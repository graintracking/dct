% Linear solution (or least squares fit) of the strain tensor in a grain.
% Uses  grain.uni.pl  and  grain.uni.plstrain  values.


function grain=gtStrainTensorFitDev(grain,ACM,errest)

nof_unipl=size(grain.uni.pl,1);

if size(grain.uni.pl,1)<3
  return
end

%% Fit Dev %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Linear operators to create the system of equations
Lop=@(a,b) [a(1)*b(1)-a(3)*b(3), a(2)*b(2)-a(3)*b(3), a(1)*b(2)+a(2)*b(1), ...
     a(1)*b(3)+a(3)*b(1), a(2)*b(3)+a(3)*b(2)];

Aop=@(a,b) (a*b')*Lop(a,a)+(a*b')*Lop(b,b)-2*Lop(a,b);


sigma_n1n2=[];
n10n20=[];
Dn1n2o=[];
Adevo=[];

% System of equation

for i=1:nof_unipl
  for j=i+1:nof_unipl

  n1=grain.uni.pl(i,:);
  n2=grain.uni.pl(j,:);

  % EXPECTED STD-s (measurement error in n1*n2 of the given n1,n2)
  
  if length(errest.std_anghor_thetat) >= grain.uni.thetatype(i)
    std_alla1=errest.std_anghor_thetat(grain.uni.thetatype(i));
    std_allnz1=errest.std_nz_thetat(grain.uni.thetatype(i));
  else
    std_alla1=errest.std_anghor_thetat(end);
    std_allnz1=errest.std_nz_thetat(end);
  end

  if length(errest.std_anghor_thetat) >= grain.uni.thetatype(j)
    std_alla2=errest.std_anghor_thetat(grain.uni.thetatype(j));
    std_allnz2=errest.std_nz_thetat(grain.uni.thetatype(j));
  else
    std_alla2=errest.std_anghor_thetat(end);
    std_allnz2=errest.std_nz_thetat(end);
  end

  sigmaij=gtStrainNormalProductSTD(n1,n2,...
    grain.uni.dn(i,3),grain.uni.dn(j,3),...
    grain.uni.danghor(i),grain.uni.danghor(j),...
    std_allnz1,std_allnz2,std_alla1,std_alla2);

  sigma_n1n2=[sigma_n1n2; sigmaij];

  
  % LEFT SIDE - measurements of change in n1*n2
  
  % find original (unstrained) angle between those plane normals
  ACV=ACM{grain.uni.thetatype(i),grain.uni.thetatype(j)};
  ang=gt2PlanesAngle(n1,n2);
  [minval,minloc]=min(abs(ACV-ang));
  n10n20=abs(cosd(ACV(minloc))); % the original dot product (undeformed)

  % Note: ACV should contain 0<= angles <=90. Two different
  % unique planes can't have 0 deg between them.

  if n1*n2'<0 % if plane normals direction opposite,
    n10n20=-n10n20; % the original dot product should be negative as well
  end

  Dn1n2o=[Dn1n2o; n10n20-n1*n2']; % change in dot product


  % RIGHT SIDE - model prediction for change in n1*n2
  Adevo=[Adevo; Aop(n1,n2)];

  end
end

m=size(Adevo,2);

Dn1n2=Dn1n2o./sigma_n1n2;
Adev=Adevo./repmat(sigma_n1n2,1,m);

% Test
%Adev=Adev(end-14:end,:);
%Dn1n2=Dn1n2(end-14:end);
%Adev=Adev(end-10:end,:);
%Dn1n2=Dn1n2(end-10:end);
%Adev=[Adev(1:6,:);Adev(1:6,:)];
%Dn1n2=[Dn1n2(1:6);Dn1n2(1:6)];
%Adev=Adev(1:5,:);
%Dn1n2=Dn1n2(1:5);


[U,S,V]=svd(Adev,0);

% Singular Value Decomposition (SVD)
% see e.g. Numerical Recipes
% Gives maximum likelihood fit when including the uncertainty of each
% measurement.
% A=U*S*V'
% inv(A)=V*diag(1./diag(S))*U'
omega=diag(S);
EDrev_vec=V*diag(1./diag(S))*U'*Dn1n2

% Covariance matrix of the fitted vector EDrev_vec
% from the SVD output:
for j=1:m
  for k=1:m
    Cov_EDrev(j,k)=(V(j,:).*V(k,:))*(1./omega.^2);
  end
end
% Note that in general Cov_EDrev=inv(Adev'*Adev);

std_EDrev=sqrt(diag(Cov_EDrev))







% %Test
% A=repmat(Adev,100,1);
% b=repmat(Dn1n2,100,1);
% [U,S,V]=svd(A,0);
% omega=diag(S);
% E=V*diag(1./diag(S))*U'*b;
% for j=1:m
%   for k=1:m
%     Covt(j,k)=(V(j,:).*V(k,:))*(1./omega.^2);
%   end
% end
% stdt=sqrt(diag(Covt));
% 
% %Test
% A=Adev(1:5,1:5);
% b=Dn1n2(1:5);
% [U,S,V]=svd(A,0);
% omega=diag(S);
% E=V*diag(1./diag(S))*U'*b;
% for j=1:m
%   for k=1:m
%     Covt(j,k)=(V(j,:).*V(k,:))*(1./omega.^2);
%   end
% end
% stdt=sqrt(diag(Covt))


  
  
  
        
%     [eigvecSTmat,eigvalSTmat]=eig(ST);
%     eigvalST=[eigvalSTmat(1,1) eigvalSTmat(2,2) eigvalSTmat(3,3)];
%     [STeigval,ordeigval]=sort(eigvalST,'descend');
%     STeigvec1=eigvecSTmat(:,ordeigval(1));
%     STeigvec2=eigvecSTmat(:,ordeigval(2));
%     STeigvec3=eigvecSTmat(:,ordeigval(3));
%     % Principal strain values in order
%     grains{i}.pstrain1=STeigval(1);
%     grains{i}.pstrain2=STeigval(2);
%     grains{i}.pstrain3=STeigval(3);   




%    
%     grains{i}.strainGT.pstrainvalues=[NaN NaN NaN];
%     grains{i}.strainGT.pstrainvalues=[NaN NaN NaN];
%     grains{i}.strainGT.pstraindir=NaN(3);
%   
%     % Strain in directions of the lab system
%     grains{i}.strainGT.straininlab=[NaN NaN NaN]  
%     grains{i}.strainGT.straintensor=NaN(3);
%     grains{i}.strainGT.errors.variance
  
end
