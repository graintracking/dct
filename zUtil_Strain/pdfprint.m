function pdfprint(fname,xlab,ylab,lw,fs,res)
%
% FUNCTION pdfprint(fname,xlab,ylab,lw,fs)
%
% USAGE    pdfprint('test1','\theta','\eta',2,14)
%
% Prints current figure into pdf file. Sets papersize, fontsize, linewidth.
% See hardcoded settings.
% 

% Directory
fdir=[fname '.pdf'];
%fdir=['/Users/reischig/thesis/figures/' fname '.pdf'];


if ~exist('xlab','var')
  xlab=[];
end
if ~exist('ylab','var')
  ylab=[];
end
if ~exist('lw','var')
  lw=[];
end
if ~exist('fs','var')
  fs=[];
end
if ~exist('res','var')
  res=[];
end
if isempty(res)
  res=100;
end


% Fontsize
if ~isempty(fs)
  set(gca,'fontunits', 'points','fontsize',fs);   
  textobj = findobj('type', 'text');
  set(textobj, 'fontunits', 'points');
  set(textobj, 'fontsize', fs);
else
  fs=14;
end

% Labels
if ~isempty(xlab)
  xlabel(xlab,'fontunits', 'points','fontsize',fs);
end

if ~isempty(ylab)
  ylabel(ylab,'fontunits', 'points','fontsize',fs);
end

% Linewidth
if ~isempty(lw)
  lineobj = findobj('type', 'line');
  set(lineobj, 'linewidth', lw);
end


% Papersize
set(gcf,'units','centimeters','paperunits','centimeters','paperpositionmode','manual');
fpos=get(gcf,'position');

%%%%%%
papersize=[fpos(3) fpos(4)]; % width and height of figure
paperpos=[0 0 fpos(3) fpos(4)];
%%%%%%

%%%%%%
%papersize=[fpos(3)-2 fpos(4)];
%paperpos=[-1 0 fpos(3) fpos(4)];
%%%%%%


set(gcf,'paperposition',paperpos,'papersize',papersize);

% Resolution, fileformat
resin=sprintf('-r%d',res);
%print('-r200','-dpdf',fdir)
print(resin,'-dpdf',fdir);

disp(['Figure saved in ' fdir]);

unix(['acroread ' fdir ' &']);

end
