function grainsout=gtSTRDeleteStrainInGrains(grainsinp,grainids)

nof_grains=length(grainsinp);

% Handle cell arrays or single one
if nof_grains==1
  grains{1}=grainsinp;
else
  grains=grainsinp;
end


for i=grainids

    grains{i}.pstrain1=NaN;
    grains{i}.pstrain2=NaN;
    grains{i}.pstrain3=NaN;
  
    % Principal strain directions in order
    grains{i}.pstrainvec1=NaN;
    grains{i}.pstrainvec2=NaN;
    grains{i}.pstrainvec3=NaN;

    % Strain in directions of the lab system
    grains{i}.strainnormX=NaN;
    grains{i}.strainnormY=NaN;
    grains{i}.strainnormZ=NaN;
    
    grains{i}.straintensor=NaN;

end


if nof_grains==1
  grainsout=grains{1};
else
  grainsout=grains;
end


end
