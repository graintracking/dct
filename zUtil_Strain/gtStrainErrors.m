
function [std_Bragg_plstrain_abs,std_Bragg_plstrain_rel,std_ang_pl,...
  bboxarea,std_distcom,std_lRdist,nof_pairids,nof_uniids]=gtStrainErrors(grain)

nof_grains=length(grain);

for i=1:nof_grains
  if any(isnan(grain{i}.straintensor))
    continue
  end
    
    
    
  std_Bragg_plstrain_abs(i)=std(grain{i}.stat.err.plstrain_abs);
  std_Bragg_plstrain_rel(i)=std(grain{i}.stat.err.plstrain_rel);

  std_ang_pl(i)=std(grain{i}.stat.strainerror_abs(3:4:end));

  bboxarea(i)=grain{i}.stat.bbxsmean*grain{i}.stat.bbysmean;
  
  std_distcom(i)=grain{i}.stat.distcomstd;
  
  std_lRdist(i)=grain{i}.stat.lRdiststd;
  
  nof_pairids(i)=length(grain{i}.pairid);

  nof_uniids(i)=size(grain{i}.uni.plid,1);
  
end

% out=[std_Bragg_plstrain_abs' std_Bragg_plstrain_rel' ...
%      std_ang_plstrain' std_distcom' std_lRdist'];

figure('name','nof_pairids - std_Bragg_plstrain_abs')
plot(nof_pairids,std_Bragg_plstrain_abs,'b.')

figure('name','nof_uniids - std_Bragg_plstrain_abs')
plot(nof_uniids,std_Bragg_plstrain_abs,'b.')

figure('name','bboxarea - std_Bragg_plstrain_abs')
plot(bboxarea,std_Bragg_plstrain_abs,'b.')

figure('name','std_distcom - std_Bragg_plstrain_abs')
plot(std_distcom,std_Bragg_plstrain_abs,'b.')

figure('name','std_lRdist - std_Bragg_plstrain_abs')
plot(std_lRdist,std_Bragg_plstrain_abs,'b.')


figure('name','nof_pairids - std_ang_pl')
plot(nof_pairids,std_ang_pl,'b.')

figure('name','nof_uniids - std_ang_pl')
plot(nof_uniids,std_ang_pl,'b.')

figure('name','bboxarea - std_ang_pl')
plot(bboxarea,std_ang_pl,'b.')

figure('name','std_distcom - std_ang_pl')
plot(std_distcom,std_ang_pl,'b.')

figure('name','std_lRdist - std_ang_pl')
plot(std_lRdist,std_ang_pl,'b.')






end











