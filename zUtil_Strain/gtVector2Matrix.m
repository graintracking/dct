function m=gtVector2Matrix(v)


           m=[v(1) v(6) v(5);...
			  v(6) v(2) v(4);...
			  v(5) v(4) v(3)];
end			