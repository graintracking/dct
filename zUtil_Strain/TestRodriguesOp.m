function TestRodriguesOp(spacegroup)

% Euler angles in degrees
gamma=10
beta=20
alpha=30


RG=[cosd(gamma) -sind(gamma) 0;
    sind(gamma) cosd(gamma)  0;
    0                      0 1]

RB=[1 0 0;
    0 cosd(beta) -sind(beta);
    0 sind(beta) cosd(beta)]

RA=[cosd(alpha) -sind(alpha) 0;
    sind(alpha) cosd(alpha) 0;
    0 0 1]

% Rotation matrix
RR=RG*RB*RA

% {100} planes aligned with the reference
v0=gtCrystSignedHKLs([1 0 0], gtCrystGetSymmetryOperators([], spacegroup) )'
hkl0=repmat([1 0 0],6,1)

% Rotated {100] planes:
vRR=RR*v0

% R vector from rotated planes by the macro:
[R_vector,rodr_goods]=plot_rodrigues_consistancy(vRR',hkl0,spacegroup,1)

% Results
disp(' ');
disp('Input rotation matrix:');
disp(RR);
disp('Rotation matrix from Rodrigues vector:');
Rg = gtMathsRod2OriMat(R_vector).';
disp(Rg);
disp('Difference:');
disp(Rg - RR);

% I.e. plot_rodrigues_consistancy gives the
% transformation tensor from the lab into the crystal reference.
