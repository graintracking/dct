% function gtPlotUniErrors(errest,write_flag)

function gtPlotUniErrors(errest,write_flag)

%sdir='/Users/reischig/thesis/figures';
fs=16;

close all

d=errest.data;


binw.etaq=10;
edges_st.etaq=10;
edges_end.etaq=90;

binw.ddy=0.00002;
edges_st.ddy=-0.0005;
edges_end.ddy=0.0005;

binw.ddz=binw.ddy;
edges_st.ddz=edges_st.ddy;
edges_end.ddz=edges_end.ddy;

binw.dom=0.0001;
edges_st.dom=-0.002;
edges_end.dom=0.002;

binw.dazd=0.00002;
edges_st.dazd=-0.0005;
edges_end.dazd=0.0005;

binw.daz=0.0001;
edges_st.daz=-0.002;
edges_end.daz=0.002;

binw.deta=0.0001;
edges_st.deta=-0.002;
edges_end.deta=0.002;

binw.dtheta=0.00002;
edges_st.dtheta=-0.0005;
edges_end.dtheta=0.0005;

binw.dang=0.0001;
edges_st.dang=0;
edges_end.dang=0.005;


cfPlotHistNo('etaq','No. of inputs','\eta [deg]')

cfPlotHistVs('ddy','etaq','Mean \Delta d_{y}','\eta [deg]')
cfPlotHistVs('ddz','etaq','Mean \Delta d_{z}','\eta [deg]')
cfPlotHistVs('dom','etaq','Mean \Delta \omega [rad]','\eta [deg]')
cfPlotHistVs('dazd','etaq','Mean \Delta \vartheta_{d} [rad]','\eta [deg]')
cfPlotHistVs('deta','etaq','Mean \Delta \eta [rad]','\eta [deg]')
cfPlotHistVs('dtheta','etaq','Mean \Delta \theta [rad]','\eta [deg]')
cfPlotHistVs('daz','etaq','Mean \Delta \vartheta [rad]','\eta [deg]')
cfPlotHistVs('dang','etaq','Mean \Delta \beta [rad]','\eta [deg]')


cfPlotHist('ddy','Histogram of \Delta d_{y}','\Delta d_{y}')
cfPlotHist('ddz','Histogram of \Delta d_{z}','\Delta d_{z}')
cfPlotHist('dom','Histogram of \Delta \omega [rad]','\Delta \omega [rad]')
cfPlotHist('dtheta','Histogram of \Delta \theta [rad]','\Delta \theta [rad]')
cfPlotHist('deta','Histogram of \Delta \eta [rad]','\Delta \eta [rad]')
cfPlotHist('daz','Histogram of \Delta \vartheta [rad]','\Delta \vartheta [rad]')
cfPlotHist('dazd','Histogram of \Delta \vartheta_{d} [rad]','\Delta \vartheta_{d} [rad]')
cfPlotHist('dang','Histogram of \Delta \beta [rad]','\Delta \beta [rad]')



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function cfPlotHistVs(dv,vs,ylab,xlab)

edges_act=edges_st.(vs):binw.(vs):edges_end.(vs);
bins_act=(edges_st.(vs)+binw.(vs)/2):binw.(vs):edges_end.(vs);
  
[no,b]=histc(d.(vs),edges_act);
no(end)=[];

for ii=1:length(bins_act)
  m1(ii,1)=mean(abs(d.(dv)(b==ii & d.thetatype==1)));
  m2(ii,1)=mean(abs(d.(dv)(b==ii & d.thetatype==2)));
end

figure
set(gca,'fontunits', 'points','fontsize',fs)
bar(bins_act,[m1,m2],'group')
legend('\{110\}','\{200\}') % ,'fontsize',fs)
%xlabel(xlab,'fontunits', 'points','fontsize',fs)
%ylabel(ylab,'fontunits', 'points','fontsize',fs)
xlabel(xlab,'fontsize',fs)
ylabel(ylab,'fontsize',fs)

if write_flag
	fname=['ch5-HistUniErrors_',dv,'_vs_',vs];
	pdfprint(fname)
end

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function cfPlotHistNo(vs,ylab,xlab)

edges_act=edges_st.(vs):binw.(vs):edges_end.(vs);
bins_act=(edges_st.(vs)+binw.(vs)/2):binw.(vs):edges_end.(vs);

no1=histc(d.(vs)(d.thetatype==1),edges_act);
no2=histc(d.(vs)(d.thetatype==2),edges_act);
no1(end)=[];
no2(end)=[];

figure
set(gca,'fontunits', 'points','fontsize',fs)
bar(bins_act,[no1,no2],'group')
legend('\{110\}','\{200\}')% ,'fontsize',fs)
xlabel(xlab,'fontsize',fs)
ylabel(ylab,'fontsize',fs)

if write_flag
	fname=['ch5-HistNofErrorInputs_vs_',vs];
	pdfprint(fname)
end

end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function cfPlotHist(dv,ylab,xlab)

edges_act=edges_st.(dv):binw.(dv):edges_end.(dv);
bins_act=(edges_st.(dv)+binw.(dv)/2):binw.(dv):edges_end.(dv);

no1=histc(d.(dv)(d.thetatype==1),edges_act);
no2=histc(d.(dv)(d.thetatype==2),edges_act);
no1(end)=[];
no2(end)=[];

figure
set(gca,'fontunits', 'points','fontsize',fs)
bar(bins_act,[no1,no2],'group')
legend('\{110\}','\{200\}','Location','Best')% ,'fontsize',fs)

xlabel(xlab,'fontsize',fs)
ylabel(ylab,'fontsize',fs)

set(gca,'XTick',edges_act*5)

if write_flag
	fname=['ch5-HistUniErrors_',dv];
	pdfprint(fname)
end

end

end