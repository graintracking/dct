volfull=edf_read('5_reconstruction/grains.edf');
load grain
load('parameters.mat');

vol=volfull(:,:,50:500);
%vol=volfull(:,:,170:200);
%vol=volfull(:,:,320:340);
%vol=volfull(:,:,211:500); % the right one
%vol=vol(:,:,160:180);  % minimum region
%vol=vol(:,:,120:140);  % max region

%vol=volfull;
 
Fg_measured=149.9;

%Lop=@(a,b) [a(1)*b(1), a(2)*b(2), a(3)*b(3), a(1)*b(2)+a(2)*b(1), ...
%     a(1)*b(3)+a(3)*b(1), a(2)*b(3)+a(3)*b(2)];

Lop=@(a,b) [a(1)*b(1), a(2)*b(2), a(3)*b(3), a(2)*b(3)+a(3)*b(2), ...
     a(1)*b(3)+a(3)*b(1), a(1)*b(2)+a(2)*b(1)];

Bop=@(a,b) [a(1)*b(1)+a(2)*b(2)+a(3)*b(3), ...
            a(1)*(b(2)+b(3))+a(2)*(b(1)+b(3))+a(3)*(b(1)+b(2)), ... 
            a(4)*b(4)+a(5)*b(5)+a(6)*b(6)];

ng=length(grain); % number of grains
nl=size(vol,3);    % number of layers        

ps=(parameters.acq.pixelsize.^2)*1e-6;  % pixel area in m2 (pixelsize is in mm)

AI=zeros(nl,ng);  % cross-section of each grain in each layer  
BI=zeros(ng,3);
zlab=[0 0 1];     % loading direction in the LAB system
Fg=zeros(nl,1);
gidused=(1:ng)';


Fg(:)=Fg_measured;

% Cross-sectional area of each grain in each layer
for ii = 1:nl      %  # actual layer
  slice=vol(:, :, ii);
  indb=zeros(ng+1,1);
  for k=1:length(slice(:))
    indb(slice(k)+1)=indb(slice(k)+1)+1;
  end
  AI(ii, :)=indb(2:end)';  
end
AI=AI*ps;

% tic
% for i=1:nl      %  # actual layer
%     slice=vol(:,:,i);
%   for j=1:ng    %  # actual grain
%     ind=find(slice==j);
%     AI(i,j)=length(ind)*ps;   
%   end  
% end
% toc

% Extract all the Rodrigues vectors into column vectors
R_vectors = gtIndexAllGrainValues(grain, 'R_vector', [], 1, 1:3);

% Compute all orientation matrices g
all_g = gtMathsRod2OriMat(R_vectors.');

% Express zlab in LAB CS
all_zcryst = gtVectorLab2Cryst(zlab, all_g);

for jj = 1:ng

    % Rearrange the vector to agree with definition in Noyen and Cohen
    elab = grain{jj}.strain.Eps([1 2 3 6 5 4])';

    ELAB = gtVector2Matrix(elab);

    g = all_g(:, :, jj); % transform from LAB to CRYSTAL

    ECRYST = g*ELAB*g'; % check how it's done gtDrawGrainCubes2 !!!!
    ecryst = gtMatrix2Vector(ECRYST);

    % Get zcryst as column vector
    zcryst = all_zcryst(jj, :).';

    BI(jj, :) = Bop(Lop(zcryst, zcryst), ecryst);
end

% Delete grain data where strain couldn'e be measured (-> NaN)

todel=any(isnan(BI),2);

BI(todel,:)=[];
AI(:,todel)=[];
gidused(todel)=[];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% MAXIMUM LIKELIHOOD FIT OF THE SYSTEM

% Singular Value Decomposition (SVD)
%   see e.g. Numerical Recipes
% Gives maximum likelihood fit when the uncertainties of the b 
%   vector components are normalized (the rows of b and A are divided with 
%   the corresponding variances).
%
% System to solve: b=A*x  , where x is to be fitted
%
% Decompose the A matrix: A=U*S*V'
%   [U,S,V]=svd(A,0) ;
% The inverse of A is given by:
%   omega=diag(S) ;
%   inv_A=V*diag(1./diag(S))*U' ;
% So the maximum likelihood fit of x is:   
%   x=V*diag(1./diag(S))*U'*b ;
%
% Covariance matrix of the fitted solution vector from the SVD output: 
%   (not correct for dependent x variables !)
%   for j=1:6
%     for k=1:6
%       Cov(j,k)=(V(j,:).*V(k,:))*(1./omega.^2) ;
%     end
%   end
% Note that in general Cov_x=inv(A'*A);
%
% Standard deviation of the fitted x vector:
%   std_x=sqrt(diag(Cov)) ;

% System to solve: Fg=AI*BI*c=M*c , where c contains the elastic constants

% System matrix:
M=AI*BI;

[U,S,V]=svd(M,0);
%omega=diag(S);
c=V*diag(1./diag(S))*U'*Fg

% Fitted force:
Ff=M*c;

bar(Ff)



return

ctest=[152e9; 124e9; 38e9]
ctest=[250.2e9; 19.0e9; 115.3e9]


ctest=[56e9; 25e9; 121e9];

Ff=M*ctest;
figure('name','Test')
bar(Ff)




%cout=c;
%cout(2)=-cout(2);
% C=[c11 c12 c12   0   0   0;...
%    c12 c11 c12   0   0   0;...
%    c12 c12 c11   0   0   0;...
%      0   0   0 c44   0   0;...
%      0   0   0   0 c44   0;...
%  	   0   0   0   0   0 c44];
