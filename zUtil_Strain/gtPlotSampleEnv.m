function gtPlotSampleEnv(fig_no,sample,rottodet)
    figure(fig_no) ;
    t=0:20:360 ;
    vec(1:19,1)=1 ;
    hold on ;
   
    for i= 0.01: 0.05: 1 % sample top and bottom plane
      plot3(i*sample.rad*sind(t),i*sample.rad*cosd(t),-sample.top*vec,'b') ;
      plot3(i*sample.rad*sind(t),i*sample.rad*cosd(t),-sample.bot*vec,'b') ;
    end

    for i=0: 0.05: 1 % sample cylinder
      plot3(sample.rad*sind(t),sample.rad*cosd(t),(i*(-sample.top+sample.bot)-sample.bot)*vec,'b') ;
    end

    plot3(sample.rad*sind(t),sample.rad*cosd(t),-sample.top*vec,'k','LineWidth',1) ; % sample edge
    plot3(sample.rad*sind(t),sample.rad*cosd(t),-sample.bot*vec,'k','LineWidth',1) ; % sample edge
        
    for i=0:-20:-2048 % detector cylinder
      plot3(rottodet*sind(t),rottodet*cosd(t),i*vec,'c') ;
    end
   
    plot3(rottodet*sind(t),rottodet*cosd(t),0*vec,'k','LineWidth',1) ; % detector edge
    plot3(rottodet*sind(t),rottodet*cosd(t),-2048*vec,'k','LineWidth',1) ; % detector edge
    
    plot3([rottodet,rottodet],[0,0],[0,-2048],'r','LineWidth',2) ; % beam direction
    plot3(rottodet,0,0,'rx','LineWidth',2) ;
    
    plot3([0,0],[0,0],[0,-2048],'-.b','LineWidth',2) ; % rotation axis
    plot3(0,0,0,'bx','LineWidth',2) ;
    
    axis square; grid on
    drawnow ;
end
