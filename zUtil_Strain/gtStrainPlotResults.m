function gtStrainPlotResults(grainsinp,write_flag,prefix)
% gtStrainPlotResults(grainsinp,write_flag,prefix)


if ~exist('write_flag','var')
  write_flag = false;
end

if ~exist('prefix','var')
  prefix = [];
end

ds = dir('strainresults');
if (write_flag && isempty(ds))
  mkdir('strainresults');
end

fdir = 'strainresults/';
fs = 16;
c  = [ 0.2778    0.1736    0.1106];

%close all

nof_grains = length(grainsinp);

% Handle cell arrays or a single one
if nof_grains == 1
  grain{1} = grainsinp;
else
  grain = grainsinp;
end

cfBarEstdEcompFittype

cfBarErrfactEcompFitALL

%return

cfBarEstdAllgrains(1)
cfBarEstdAllgrains(2)
cfBarEstdAllgrains(3)
cfBarEstdAllgrains(4)
cfBarEstdAllgrains(5)
cfBarEstdAllgrains(6)

cfBarEAllgrains(1)
cfBarEAllgrains(2)
cfBarEAllgrains(3)
cfBarEAllgrains(4)
cfBarEAllgrains(5)
cfBarEAllgrains(6)

% binw.E12=0.0002;
% edges_st.E12=-0.004;
% edges_end.E12=0.004;
% 
% binw.E33=0.0005;
% edges_st.E33=-0.005;
% edges_end.E33=0.01;
% 
% binw.E11=0.0005;
% edges_st.E11=-0.007;
% edges_end.E11=0.007;

binw.E12=0.0002;
edges_st.E12=-0.004;
edges_end.E12=0.004;

binw.E33=0.0002;
edges_st.E33=-0.004;
edges_end.E33=0.004;

binw.E11=0.0002;
edges_st.E11=-0.004;
edges_end.E11=0.004;

%keyboard
pause(10)

cfHistE(1,'E11')
cfHistE(2,'E11')
cfHistE(3,'E33')
cfHistE(4,'E12')
cfHistE(5,'E12')
cfHistE(6,'E12')


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function cfBarEstdEcompFittype
    
if (~isfield(grain{1}.strain,'std_sp') || ~isfield(grain{1}.strain,'std_dev') || ...
    ~isfield(grain{1}.strain,'std'))
    return
end

name = [prefix, 'BarEStdEcompFittype'];
%xlab = '\epsilon_{11}  \epsilon_{22}  \epsilon_{33}  \epsilon_{12}  \epsilon_{13}  \epsilon_{23}';
ylab = 'Mean error of \epsilon components [std]';


for ii = 1:6
  m1 = gtIndexAllGrainValues(grain,'strain','std_sp',ii);
  m2 = gtIndexAllGrainValues(grain,'strain','std_dev',ii);
  m3 = gtIndexAllGrainValues(grain,'strain','std',ii);
  m(ii,1) = mean(m1(~isnan(m1)));
  m(ii,2) = mean(m2(~isnan(m2)));
  m(ii,3) = mean(m3(~isnan(m3)));
end

disp('Grand average of Std(epsilon) vs. fit type:')
mean(m,1)

figure('name',name)
bar(m,'group')

set(gca,'xtick',[])
text(1,0,'\epsilon_{11}','VerticalAlignment','top','HorizontalAlignment','center','FontSize',fs)
text(2,0,'\epsilon_{22}','VerticalAlignment','top','HorizontalAlignment','center','FontSize',fs)
text(3,0,'\epsilon_{33}','VerticalAlignment','top','HorizontalAlignment','center','FontSize',fs)
text(4,0,'\epsilon_{12}','VerticalAlignment','top','HorizontalAlignment','center','FontSize',fs)
text(5,0,'\epsilon_{13}','VerticalAlignment','top','HorizontalAlignment','center','FontSize',fs)
text(6,0,'\epsilon_{23}','VerticalAlignment','top','HorizontalAlignment','center','FontSize',fs)

set(gca,'fontunits', 'points','fontsize',fs)
legend('Fit d-spacings','Fit angles','Fit both','Location','best') % ,'fontsize',fs)
%xlabel(xlab,'fontunits', 'points','fontsize',fs)
%ylabel(ylab,'fontunits', 'points','fontsize',fs)
%xlabel(xlab,'fontsize',fs)
ylabel(ylab,'fontsize',fs)
colormap autumn

if write_flag
  pdfprint([fdir, name])
end

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function cfBarErrfactEcompFitALL
    
if ~isfield(grain{1}.strain,'errfact')
    return
end

name = [prefix, 'BarErrfactEcompFitALL'];
%xlab = '\epsilon_{11}  \epsilon_{22}  \epsilon_{33}  \epsilon_{12}  \epsilon_{13}  \epsilon_{23}';
ylab = 'Mean error factors of \epsilon components';

% xlab{1} = ' \epsilon_{11}';
% xlab{2} = ' \epsilon_{12}';
% xlab{3} = ' \epsilon_{13}';
% xlab{4} = ' \epsilon_{12}';
% xlab{5} = ' \epsilon_{13}';
% xlab{6} = ' \epsilon_{23}';


for ii=1:6
  m1 = gtIndexAllGrainValues(grain,'strain','errfact',ii,1); % dy
  m2 = gtIndexAllGrainValues(grain,'strain','errfact',ii,2); % dz
  m3 = gtIndexAllGrainValues(grain,'strain','errfact',ii,3); % om
  m(ii,1) = mean(m1(~isnan(m1)));
  m(ii,2) = mean(m2(~isnan(m2)));
  m(ii,3) = mean(m3(~isnan(m3)));
end

figure('name',name)
bar(m,'group')

set(gca,'xtick',[])
text(1,0,'\epsilon_{11}','VerticalAlignment','top','HorizontalAlignment','center','FontSize',fs)
text(2,0,'\epsilon_{22}','VerticalAlignment','top','HorizontalAlignment','center','FontSize',fs)
text(3,0,'\epsilon_{33}','VerticalAlignment','top','HorizontalAlignment','center','FontSize',fs)
text(4,0,'\epsilon_{12}','VerticalAlignment','top','HorizontalAlignment','center','FontSize',fs)
text(5,0,'\epsilon_{13}','VerticalAlignment','top','HorizontalAlignment','center','FontSize',fs)
text(6,0,'\epsilon_{23}','VerticalAlignment','top','HorizontalAlignment','center','FontSize',fs)

set(gca,'fontunits', 'points','fontsize',fs)
legend('d_{y}','d_{z}','\omega','Location','best') % ,'fontsize',fs)
%xlabel(xlab,'fontsize',fs)
ylabel(ylab,'fontsize',fs)
%set(gca,'XTicklabel',xlab,'fontsize',fs)
colormap winter

if write_flag
  pdfprint([fdir, name])
end

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function cfBarEstdAllgrains(E)

if ~isfield(grain{1}.strain,'std')
    return
end

name = [prefix, 'BarEStdAllGrains'];

switch E
  case 1
    E_string = 'Error in \epsilon_{11} [std]';
    fname    = [name '11'];
  case 2
    E_string = 'Error in \epsilon_{22} [std]';
    fname    = [name '22'];
  case 3
    E_string = 'Error in \epsilon_{33} [std]';
    fname    = [name '33'];
  case 4
    E_string = 'Error in \epsilon_{12} [std]';
    fname    = [name '12'];
  case 5
    E_string = 'Error in \epsilon_{13} [std]';
    fname    = [name '13'];
  case 6
    E_string = 'Error in \epsilon_{23} [std]';
    fname    = [name '23'];
end

xlab = 'Grain ID';
ylab = E_string;

m = gtIndexAllGrainValues(grain,'strain','std',E); % E11, Fit ALL

figure('name',fname)
%bar(m,'group')
bar(m,'b')
set(gcf,'units','centimeters','paperunits','centimeters','paperpositionmode','manual');
set(gcf,'Position',[0 0 40 15])
set(gca,'Position',[0.06 0.12 0.92 0.8])
set(gca,'fontunits', 'points','fontsize',fs)
xlabel(xlab,'fontsize',fs)
ylabel(ylab,'fontsize',fs)
set(gca,'XTick',0:10:length(m))

if write_flag
  pdfprint([fdir, fname])
end

end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function cfBarEAllgrains(E)

name = [prefix, 'BarEAllGrains'];

switch E
  case 1
    E_string = '\epsilon_{11}';
    fname    = [name '11'];
  case 2
    E_string = '\epsilon_{22}';
    fname    = [name '22'];
  case 3
    E_string = '\epsilon_{33}';
    fname    = [name '33'];
  case 4
    E_string = '\epsilon_{12}';
    fname    = [name '12'];
  case 5
    E_string = '\epsilon_{13}';
    fname    = [name '13'];
  case 6
    E_string = '\epsilon_{23}';
    fname    = [name '23'];
end

xlab = 'Grain ID';
ylab = E_string;

m = gtIndexAllGrainValues(grain,'strain','Eps',E);

figure('name',fname)
%bar(m,'group')
bar(m,'FaceColor',c)
set(gcf,'units','centimeters','paperunits','centimeters','paperpositionmode','manual');
set(gcf,'Position',[0 0 40 15])
set(gca,'Position',[0.06 0.12 0.92 0.8])
set(gca,'fontunits', 'points','fontsize',fs)
xlabel(xlab,'fontsize',fs)
ylabel(ylab,'fontsize',fs)
set(gca,'XTick',0:10:length(m))

if write_flag
  pdfprint([fdir, fname])
end

end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function cfHistE(E,b)

name = [prefix, 'Histogram of ',cfEString(E)];
xlab = cfEString(E);
ylab = [prefix, 'Frequency of ',cfEString(E)];

edges_act = edges_st.(b):binw.(b):edges_end.(b);
bins_act  = (edges_st.(b)+binw.(b)/2):binw.(b):edges_end.(b);

m = gtIndexAllGrainValues(grain,'strain','Eps',E); % E11, Fit ALL

m = histc(m,edges_act);
m(end) = [];

figure('name',name)
bar(bins_act,m,'FaceColor',c,'EdgeColor','w')
set(gca,'fontunits', 'points','fontsize',fs)
xlabel(xlab,'fontsize',fs)
ylabel(ylab,'fontsize',fs)
grid on
line([0 0], [max(m(:)) max(m(:))], '.-r')
fname = [prefix, 'HistE', num2str(E)];

if write_flag
  pdfprint([fdir, fname])
end

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function E_string = cfEString(E)

switch E
  case 1
    E_string = '\epsilon_{11}';
  case 2
    E_string = '\epsilon_{22}';
  case 3 
    E_string = '\epsilon_{33}';
  case 4
    E_string = '\epsilon_{12}';
  case 5
    E_string = '\epsilon_{13}';
  case 6
    E_string = '\epsilon_{23}';
end

end


end
