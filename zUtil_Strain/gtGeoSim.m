% FUNCTION gtGeoSim(rtiltY,rtiltZ,astiltY,astiltZ,hordist,thetatype,rottodet_r,parameters)
%
% Tilt simulator to see possible effects from scintilator tilts on 
% diffraction spot pairs. Shows the result what gtMatchDifspots would give.
%
% X,Y,Z are the lab coordinates.
%
% INPUT
%
%   rtiltY:     real tilt around Y' (i.e. Y turned around Z by tiltZ)
%   rtiltZ:     real tilt around Z (in degrees)
%   astiltY:    assumed tilt around Y'
%   astiltZ:    assumed tilt around Z
%
%  Optional
%   hordist:    horizontal distortion in images (stretch relative to vertical)
%   thetatype:  which plane families to show (1,[2 3],...)
%   rottodet_r: real rotation-axis to detector distance (in mm)
%   parameters: assumed rotx,rottodet,pixelsize,(thetas) are taken from
%               here
%

function gtGeoSim(rtiltY,rtiltZ,astiltY,astiltZ,hordist,thetatype,rottodet_r,pars)

parameters=[];

if ~exist('pars','var')
    pars=load('parameters.mat');
    parameters=pars.parameters;
else
    parameters=pars;
end

if ~exist('hordist','var')
    hordist=1;
end

if ~exist('thetatype','var')
    thetatype=1;
end

valid_2thetas=parameters.cryst.twoth; % coloumn vector of all valid 2theta angles in degrees

rottodet_as=parameters.acq.dist/parameters.acq.pixelsize;
rotx=parameters.acq.rotx;

if ~exist('rottodet_r','var')
  rottodet_r=rottodet_as;
else
  rottodet_r=rottodet_r/parameters.acq.pixelsize;
end

% Set sample coordinate system
% sorX = on the rotation axis by definition 
% sorY = on the rotation axis by definition
sorZ=parameters.acq.ydet/2+0.5;

eta=(0:1:360)';

%figure
 hold on
 xlabel('theta (deg)') ;
 ylabel('eta (deg)') ;
 title('eta - theta') ;

 
for i=1:length(thetatype)
  theta=valid_2thetas(thetatype(i))/2;

  % Generate fictitious data:
  
  [x1,y1,z1]=line_plane_sect(eta,rottodet_r,theta,rtiltZ,rtiltY);
  [x2,y2,z2]=line_plane_sect(180-eta,rottodet_r,theta,rtiltZ,rtiltY);

  plotrange=max(abs([y1;y2;z1;z2]));
  
  % unit vectors of image in the lab system:
  imj=[-sind(rtiltY)*cosd(rtiltZ) -sind(rtiltY)*sind(rtiltZ) -cosd(rtiltY)]/hordist;
  imi=[-sind(rtiltZ) cosd(rtiltZ) 0];

  centXimA=[x1-rottodet_r y1 z1]*imi'+rotx;
  centYimA=[x1-rottodet_r y1 z1]*imj'+sorZ;
  centXimB=[x2-rottodet_r y2 z2]*imi'+rotx;
  centYimB=[x2-rottodet_r y2 z2]*imj'+sorZ;

  
  % Detected data:

  theta_s=gtThetaOfPairs(centXimA,centYimA,centXimB,centYimB,rottodet_as,astiltY,astiltZ,rotx,sorZ);

  eta_s=gtEtaOfPairs(centXimA,centYimA,centXimB,centYimB,astiltY,astiltZ,rotx,sorZ);

  plot(theta_s,eta_s,'.r','MarkerSize',6);
  plot([theta theta],[0 360],'-.k','LineWidth',1);

end  


%For checking
%dvec_s=gtDiffVecInLab(centXimA,centYimA,centXimB,centYimB,rottodet_as,astiltY,astiltZ,rotx,sorZ);
%dvec_c=[x1+x2 y1+y2 z1-z2]./repmat(sqrt((x1+x2).^2+(y1+y2).^2+(z2-z1).^2),1,3);
% eta_s2=gtEtaOfPoint(dvec_s(:,2),dvec_s(:,3));
% eta_c=gtEtaOfPoint(dvec_c(:,2),dvec_c(:,3));
%
% [dvec_s dvec_c]
% [eta_s eta_s2 eta_c]

% figure
%   hold on
%   plot(centXimA,centYimA,'b.')
%   plot(centXimB,centYimB,'ro')
% 
%   plot([rotx rotx],[sorZ-plotrange sorZ+plotrange],'k.-.')
%   plot([rotx-plotrange rotx+plotrange],[sorZ sorZ],'k.--')
%   axis equal
%   set(gca,'YDir','reverse')
% 
 
  
end % of function


%% Sub-functions


function [z1,z2]=cone_plane_sectZ(y,r,theta,rtiltZ,rtiltY)
  % r = real rottodet
  nx=-cosd(rtiltY).*cosd(rtiltZ);
  ny=-cosd(rtiltY).*sind(rtiltZ);
  nz=sind(rtiltY);

  a=nx^2./tand(2*theta)-nz.^2;
  b=2*nx*ny*y-2*nx*r*nz;
  c=ny^2*y.^2+nx^2*r^2-2*ny*nx*r*y-nx^2/tand(2*theta)*y^2;

  z1=(-b+sqrt(b.^2-4.*a.*c))/2/a;
  z2=(-b-sqrt(b.^2-4.*a.*c))/2/a;
end


function [x,y,z]=line_plane_sect(eta,r,theta,rtiltZ,rtiltY)
  % r = real rottodet
  nx=-cosd(rtiltY).*cosd(rtiltZ);
  ny=-cosd(rtiltY).*sind(rtiltZ);
  nz=sind(rtiltY);

  x=zeros(size(eta));
  y=zeros(size(eta));
  z=zeros(size(eta));
  
  for i=1:length(eta)
    % 3 equations for X Y Z
    A=[-tand(2*theta)*sind(eta(i)) 1 0; ...  % diffraction line y-x
      -tand(2*theta)*cosd(eta(i)) 0 1; ... % diffraction line z-x
      nx ny nz];                          % plane

    B=[0; 0; nx*r];

    C=A\B;

    x(i,1)=C(1);
    y(i,1)=C(2);
    z(i,1)=C(3);
  end
  
end

