function grainsout=gtStrainSpotSpread(grainsinp,thetatype)

% if ~exist('difspottable','var')
 	load('parameters.mat');
 	difspottable=[parameters.acq.name 'difspot'];
% end

if ~exist('thetatype','var')
	thetatype=[];
end

nof_grains=length(grainsinp);

% Handle cell arrays or a single one
if nof_grains==1
  grains{1}=grainsinp;
else
  grains=grainsinp;
end

gtDBConnect();

figure('name','Average spot spread vs. principal strain')
hold on
xlabel('Maximum principal strain')
ylabel('Normalised extension')

for i=1:nof_grains

	normext=[];
	ext=[];
	lf=[];
	
	for j=1:grains{i}.nof_pairs

		mycmd=sprintf('SELECT (EndImage-StartImage+1) FROM %s WHERE difspotID=%d',...
			    difspottable,grains{i}.difspots(j));
		ext1=mym(mycmd);
		
		mycmd=sprintf('SELECT (EndImage-StartImage+1) FROM %s WHERE difspotID=%d',...
			    difspottable,grains{i}.difspots(j+grains{i}.nof_pairs));
		ext2=mym(mycmd);

		lf(j)=1/sind(2*grains{i}.theta(j))/abs(sind(grains{i}.eta(j)));
  	%lf(j)=1/abs(sind(grains{i}.eta(j)));

		normext(j)=(ext1+ext2)/2/lf(j);
		ext(j)=(ext1+ext2)/2;
	
	end
	
	grains{i}.Lorentzfactor=lf;
	grains{i}.normext=normext;
	grains{i}.ext=ext;

	if isempty(thetatype)
		m=mean(normext);
	else
		m=mean(normext(ismember(grains{i}.thetatype,thetatype)));
	end
	
	%plot(grains{i}.strain.Eps(3),mean(ext),'.')
	plot(grains{i}.strain.pstrainmax,m,'.')
	
end


% Handle cell arrays or a single one as output
if nof_grains==1
  grainsout=grains{1};
else
  grainsout=grains;
end

end