function [dpstrain,dnormext]=gtStrainSpotSpreadDiff(grainb,grainc,matchg)

nof_grainsb=length(grainb);


figure('name','Diff in average spot spread vs. diff. in principal strain')
hold on
xlabel('Diff. in absolut maximum principal strain')
ylabel('Diff. in normalised extension')

for i=1:nof_grainsb

	if ~isnan(matchg(i,2))
		dnormext(i)=mean(grainc{matchg(i,2)}.normext)-mean(grainb{i}.normext);
%		dpstrain(i)=abs(grainc{matchg(i,2)}.strain.pstrainmax)-abs(grainb{i}.strain.pstrainmax);
		dpstrain(i)=grainc{matchg(i,2)}.strain.pstrainmax-grainb{i}.strain.pstrainmax;
%		dpstrain(i)=grainc{matchg(i,2)}.strain.Eps(3)-grainb{i}.strain.Eps(3);
		
		plot(dpstrain(i),dnormext(i),'.')
	else
		dnormext(i)=NaN;
		dpstrain(i)=NaN;
	end
	
	
end

end