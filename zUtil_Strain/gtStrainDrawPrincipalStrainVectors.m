function gtStrainDrawPrincipalStrainVectors(grainsinp)

nof_grains=length(grainsinp);

% Handle cell arrays or single one
if nof_grains==1
  grains{1}=grainsinp;
else
  grains=grainsinp;
end

% strainscale=0.01;

for i=1:nof_grains
  psvec(i,:)=grains{i}.strain.pstraindir(:,1);
end

figure;
set(gca,'fontunits', 'points','fontsize',14);
compass(psvec(:,1),psvec(:,2));
axis([-1 1 -1 1])
%axis equal;
xlabel('X axis','Fontsize',14);
ylabel('Y axis','Fontsize',14);

h = findobj(gca,'Type','line');
set(h,'Color','b','Linewidth',1.5);

figure
for i=1:nof_grains
  pseta(i)=gtEtaOfPoint(psvec(i,2),psvec(i,1));
end
set(gca,'fontunits', 'points','fontsize',14);
rose(pseta,24);
xlabel('X axis','Fontsize',14);
ylabel('Y axis','Fontsize',14);

h = findobj(gca,'Type','patch');
set(h,'FaceColor','b','EdgeColor','w');

h = findobj(gca,'Type','line');
set(h,'Color','b','Linewidth',2);

% Extract all the Rodrigues vectors into column vectors
R_vectors = gtIndexAllGrainValues(grains, 'R_vector', [], 1, 1:3);

% Compute all orientation matrices g
all_g = gtMathsRod2OriMat(R_vectors.');

figure('name','Hist grain orientation');
for ii = 1:nof_grains
    R = all_g(:, :, ii).';
    v1 = R(1,3);
    v2 = R(2,3);
    oeta(ii) = gtEtaOfPoint(v1,v2);
end
set(gca,'fontunits', 'points','fontsize',14);
rose(oeta,24);
xlabel('X axis','Fontsize',14);
ylabel('Y axis','Fontsize',14);

h = findobj(gca,'Type','patch');
set(h,'FaceColor','b','EdgeColor','w');

h = findobj(gca,'Type','line');
set(h,'Color','b','Linewidth',2);


figure('name','Grain Orientation');
set(gca,'fontunits', 'points','fontsize',14);
for ii = 1:nof_grains
    R = all_g(:, :, ii).';
    v1(ii) = R(1,3);
    v2(ii) = R(2,3);
end
plot(v1,v2,'.');
axis([-1 1 -1 1]);
%axis equal;
xlabel('X axis','Fontsize',14);
ylabel('Y axis','Fontsize',14);

h = findobj(gca,'Type','line');
set(h,'Color','b','Linewidth',1.5);

end % end of function

