% FUNCTION [angprec_hor,angprec_ver]=gtEstimateAngularPrecision
% 
% Estimates the average angular precision in the whole scan, based on plane
% normals that were detected twice (2 pairs, 2x2 difspots). It looks in all
% the grains.
%

function errest=gtEstimateErrors(grainsinp)

load('parameters.mat');

nof_grains=length(grainsinp);

% Handle cell arrays or a single one    
if nof_grains==1 && ~iscell(grainsinp) 
  grains{1}=grainsinp;
else
  grains=grainsinp;
end

dang=[]; % will contain all the angular discrepancies
daz=[]; % will contain all the azimuth
dazd=[];
errver_pix=[]; % 
errvernorm=[]; % all the vertical coordinate discrepancies
errhor_pix=[]; % 
errhornorm=[]; % all the horizontal coordinate discrepancies
ddfrometa=[];
dnz=[];
dtheta=[];
deta=[];
dom=[];
%omfactor=[];
thetatype=[];
theta=[];
eta=[];
etaq=[];
%pla=[];

ddy=[];
%ddy_az=[];
ddz=[];

% Loop through grains
for i=1:nof_grains
  
  % Loop through unique planes and fetch the errors
  for j=1:size(grains{i}.uni.plid,1)
    if ~isnan(grains{i}.uni.ddy(j)) % those that have been detected twice
      ddy=[ddy; grains{i}.uni.ddy(j)];
      %ddy_az=[ddy_az; grains{i}.uni.ddy_az(j)];
      ddz=[ddz; grains{i}.uni.ddz(j)];
      dom=[dom; grains{i}.uni.dom(j)];
      
      dang=[dang; grains{i}.uni.dang(j)];
      daz=[daz; grains{i}.uni.daz(j)];
      dazd=[dazd; grains{i}.uni.dazd(j)];
      errvernorm=[errvernorm; grains{i}.uni.errvernorm(j)];
      errver_pix=[errver_pix; grains{i}.uni.errvernorm(j)*2*parameters.acq.dist/parameters.acq.pixelsize];
      errhornorm=[errhornorm; grains{i}.uni.errhornorm(j)];
      errhor_pix=[errhor_pix; grains{i}.uni.errhornorm(j)*2*parameters.acq.dist/parameters.acq.pixelsize];
      ddfrometa=[ddfrometa; grains{i}.uni.ddfrometa(j)];
      dnz=[dnz; grains{i}.uni.dn(j,3)];

      dtheta=[dtheta; grains{i}.uni.dtheta(j)];
      deta=[deta; grains{i}.uni.deta(j)];
      %omfactor=[omfactor; grains{i}.uni.omfactor(j)];

      thetatype=[thetatype; grains{i}.uni.thetatype(j)];
      theta=[theta; grains{i}.uni.theta(j)];
      eta=[eta; grains{i}.uni.eta(j)];
      etaq=[etaq; grains{i}.uni.etaq(j)];
      %pla=[pla; grains{i}.uni.pla(j)];
    end
  end

end % of grain

% Average errors of all
load('parameters.mat');

errest.std_dy=mean(abs(ddy))/sqrt(2);
%errest.std_dy_az=mean(sqrt(2)*ddy_az);
errest.std_dz=mean(abs(ddz))/sqrt(2);

%errest.std_om=sqrt(mean(errest.std_dy^2-errest.std_dz^2)/2);
errest.std_om=mean(abs(dom))/sqrt(2);

errest.std_ddfrometa=mean(abs(ddfrometa))/sqrt(2);

errest.std_errvernorm=mean(abs(errvernorm))/sqrt(2);
errest.std_errver_pix=mean(abs(errver_pix))/sqrt(2);
errest.std_errhornorm=mean(abs(errhornorm))/sqrt(2);
errest.std_errhor_pix=mean(abs(errhor_pix))/sqrt(2);


errest.std_ang=mean(abs(dang))/sqrt(2);
errest.std_az=mean(abs(daz))/sqrt(2);
errest.std_azd=mean(abs(dazd))/sqrt(2);
errest.std_theta=mean(abs(dtheta))/sqrt(2);
errest.std_eta=mean(abs(deta))/sqrt(2);

errest.std_om_deg=rad2deg(errest.std_om);
errest.std_ang_deg=rad2deg(errest.std_ang);
errest.std_az_deg=rad2deg(errest.std_az);
errest.std_azd_deg=rad2deg(errest.std_azd);
errest.std_theta_deg=rad2deg(errest.std_theta);
errest.std_eta_deg=rad2deg(errest.std_eta);

% Error for each thetatype
for j=1:max(thetatype)
%   N=sum(thetatype==j);
%   errest.std_dang_tht(j)=sqrt(sum((dang(thetatype==j)/2).^2)/N);
%   errest.std_danghor_tht(j)=sqrt(sum((danghor(thetatype==j)/2).^2)/N);
%   errest.std_pixerrvernorm_tht(j)=sqrt(sum((pixerrvernorm(thetatype==j)/2).^2)/N);
%   errest.std_ddfrometa_tht(j)=sqrt(sum((ddfrometa(thetatype==j)/2).^2)/N);
%   errest.std_dtheta_tht(j)=sqrt(sum((dtheta(thetatype==j)/2).^2)/N);
%   errest.std_deta_tht(j)=sqrt(sum((deta(thetatype==j)/2).^2)/N);

  %errest.std_pixerrvernorm_thetat(j)=mean(pixerrvernorm(thetatype==j))/2;
  
  
  errest.std_dy_thetat(j)=mean(abs(ddy(thetatype==j)))/sqrt(2);
 % errest.std_dy_az_thetat(j)=mean(sqrt(2)*ddy_az(thetatype==j));
  errest.std_dz_thetat(j)=mean(abs(ddz(thetatype==j)))/sqrt(2);
  errest.std_om_thetat(j)=mean(abs(dom(thetatype==j)))/sqrt(2);
   
  errest.std_ddfrometa_thetat(j)=mean(abs(ddfrometa(thetatype==j)))/sqrt(2);

  %errest.std_om_thetat(j)=sqrt(mean(ddy(thetatype==j).^2-ddz(thetatype==j).^2)/2);
  
  errest.std_errvernorm_thetat(j)=mean(abs(errvernorm(thetatype==j)))/sqrt(2);
  errest.std_errver_pix_thetat(j)=mean(abs(errver_pix(thetatype==j)))/sqrt(2);
  errest.std_errhornorm_thetat(j)=mean(abs(errhornorm(thetatype==j)))/sqrt(2);
  errest.std_errhor_pix_thetat(j)=mean(abs(errhor_pix(thetatype==j)))/sqrt(2);
  
  errest.std_nz_thetat(j)=mean(abs(dnz(thetatype==j)))/sqrt(2);
  errest.std_ang_thetat(j)=mean(abs(dang(thetatype==j)))/sqrt(2);
  errest.std_az_thetat(j)=mean(abs(daz(thetatype==j)))/sqrt(2);
  errest.std_azd_thetat(j)=mean(abs(dazd(thetatype==j)))/sqrt(2);
  errest.std_theta_thetat(j)=mean(abs(dtheta(thetatype==j)))/sqrt(2);
  errest.std_eta_thetat(j)=mean(abs(deta(thetatype==j)))/sqrt(2);
  
  %errest.std_om_thetat(j)=sqrt(errest.std_anghor_thetat(j)^2-mean(omfactor(thetatype==j))*errest.std_pixerrvernorm_thetat(j)^2);

  errest.nofinput_thetat(j)=sum(thetatype==j);

end

% Store data
errest.data.ddy=ddy;
%errest.data.ddy_az=ddy_az;
errest.data.ddz=ddz;
errest.data.dom=dom;

errest.data.ddfrometa=ddfrometa;

errest.data.errvernorm=errvernorm;
errest.data.errver_pix=errver_pix;
errest.data.errhornorm=errhornorm;
errest.data.errhor_pix=errhor_pix;

errest.data.dang=dang;
errest.data.daz=daz;
errest.data.dazd=dazd;

%errest.data.pixerrvernorm=pixerrvernorm;
errest.data.dtheta=dtheta;
errest.data.deta=deta;
%errest.data.omfactor=omfactor;
errest.data.thetatype=thetatype;
errest.data.theta=theta;
errest.data.eta=eta;
errest.data.etaq=etaq;
%errest.data.pla=pla;


end % of function












