function [grainsout,allpla_mean,allpla_med]=gtAddGrainLatticePar(grainsinp,parameters)

% Applies Bragg's law to each plane in the grain structure(s).

if ~exist('parameters','var')
  load('parameters.mat');
end

lambda=gtConvEnergyToWavelength(parameters.acq.energy);   % in angstroms


nof_grains=length(grainsinp);

% Handle cell arrays or a single one
if nof_grains==1
  grains{1}=grainsinp;
else
  grains=grainsinp;
end

allpla=[]; % will contain all plane normals

for i=1:nof_grains
  nof_pl=size(grains{i}.pl,1); % number of planes in grain

  % Factor to get interplanar distance from lattice parameter for each plane: d=a*v 
  for j=1:nof_pl
    plv= 1/norm(grains{i}.hkl(j,:)); % dimensionless v
    pld= lambda / (2*sind(grains{i}.theta(j))); % d in angstroms
    pla= pld/plv ; % lattice parameter resulting from this measurement
    
    %grain{i}.plv(j)=plv;
    grains{i}.pld(j)=pld;
    grains{i}.pla(j)=pla;
    
    allpla=[allpla, pla];
  end
 
  % Same for unified planes
  if isfield(grains{i},'uni')
  nof_unipl=size(grains{i}.uni.pl,1); % number of planes in grain
  
  for j=1:nof_unipl
    plv= 1/norm(grains{i}.uni.hkl(j,:));
    pld= lambda / (2*sind(grains{i}.uni.theta(j))); % in angstroms
    pla= pld/plv ;
    
    %grain{i}.uni.plv(j)=plv;
    grains{i}.uni.pld(j)=pld;
    grains{i}.uni.pla(j)=pla;
    
  end
  end 
end

allpla_mean=mean(allpla);
allpla_med=median(allpla);

% Handle cell arrays or a single one as output
if nof_grains==1
  grainsout=grains{1};
else
  grainsout=grains;
end

end
