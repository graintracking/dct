function grain=gtStrainDoAll

load grain.mat

grain=gtINUniquePlaneNormals(grain);
grain=gtAddGrainLatticePar(grain);
grain=gtStrainGrains(grain);
