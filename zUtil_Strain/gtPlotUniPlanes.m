% function gtPlotUniPlanes(grain)

function gtPlotUniPlanes(grain)

z = zeros(1,size(grain.uni.pl,2));

figure
quiver3(z,z,z,grain.uni.pl(1,:),grain.uni.pl(2,:),grain.uni.pl(3,:))

end

% function gtPlotUniPlanes(grain)
% 
% z=zeros(grain.uni.nof_unipl,1);
% 
% figure
% quiver3(z,z,z,grain.uni.pl(:,1),grain.uni.pl(:,2),grain.uni.pl(:,3))
% 
% end


% 
% figure
% hold on
% 
% xlabel('X')
% ylabel('Y')
% zlabel('Z')
% axis equal
% grid
% 
% tmp=summer(4);
% c1=tmp(1,:);
% c2=tmp(3,:);
% 
% i=1; quiver3(0,0,0,g.uni.pl(i,1),g.uni.pl(i,2),g.uni.pl(i,3),'color',c1,'linewidth',2);
% i=2; quiver3(0,0,0,g.uni.pl(i,1),g.uni.pl(i,2),g.uni.pl(i,3),'color',c1,'linewidth',2);
% i=3; quiver3(0,0,0,g.uni.pl(i,1),g.uni.pl(i,2),g.uni.pl(i,3),'color',c1,'linewidth',2);
% i=4; quiver3(0,0,0,g.uni.pl(i,1),g.uni.pl(i,2),g.uni.pl(i,3),'color',c1,'linewidth',2);
% i=5; quiver3(0,0,0,-g.uni.pl(i,1),-g.uni.pl(i,2),-g.uni.pl(i,3),'color',c1,'linewidth',2);
% i=6; quiver3(0,0,0,g.uni.pl(i,1),g.uni.pl(i,2),g.uni.pl(i,3),'color',c2,'linewidth',2);
% i=7; quiver3(0,0,0,g.uni.pl(i,1),g.uni.pl(i,2),g.uni.pl(i,3),'color',c2,'linewidth',2);
% 

