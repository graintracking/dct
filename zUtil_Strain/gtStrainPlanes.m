% Adds strain values to each plane in a grain structure, given a reference lattice
% parameter.
% If not a fix reference lattice parameter, but relative measurements between 
% two datasets are to be used, this is the macro to be extended.

function grainsout=gtStrainPlanes(grainsinp,latticeparref)

nof_grains=length(grainsinp);

% Handle cell arrays or a single one
if nof_grains==1
  grains{1}=grainsinp;
else
  grains=grainsinp;
end

for i=1:nof_grains

  % Strain in each plane
  nof_pl=size(grains{i}.pl,1); % number of planes in grain
  for j=1:nof_pl
    grains{i}.plstrain(j)= (grains{i}.pla(j) / latticeparref) - 1 ;
  end

  % Same for unique planes
  nof_unipl=size(grains{i}.uni.pl,1); % number of unique planes in grain
  for j=1:nof_unipl
    grains{i}.uni.plstrain(j)= (grains{i}.uni.pla(j) / latticeparref) - 1 ;
  end

  grains{i}.latticeparref=latticeparref;

end
  
% Handle cell arrays or a single one as output
if nof_grains==1
  grainsout=grains{1};
else
  grainsout=grains;
end

end
