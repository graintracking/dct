function g = myRod2g(r)
% function g = Rod2g(r)
% This function calculates the orientation matrix g given a Rodrigues
% vector r. 
% See e.g. equation 3.13 in Three-Dimensional X-Ray Diffraction Microscopy
% by H.F. Poulsen
%
% Written by EML Sept. 04

g=zeros(3,3);   % Initialise the orientation matrix
%initialise the permutation tensor
e=calc_perm_tensor;

% Loop over the elements of the orientation matrix
for i=1:3
    for j=1:3
        
        g(i,j)= (1-dot(r,r))*delta(i,j) + 2*r(i)*r(j);
       
        % Summing over the remaining component   
        for k=1:3
          %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
          %    g(i,j) = g(i,j) - 2*e(i,j,k)*r(k);
          % changed into:
          g(i,j) = g(i,j) + 2*e(i,j,k)*r(k);
          %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
          % this just gives the inverse of g! 
          
          
       end
        g(i,j) = g(i,j)/(1+dot(r,r));
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Helper function %%%%%%%%%%%%%%%%%%%%%%%%%%%%
function  e=calc_perm_tensor
% Calculates the permutation tensor
%e_ijk(i,j,k)=(x_i dot (x_j cross x_k))
I=eye(3);   % Identity matrix
for i=1:3
    for j=1:3
        for k=1:3
            e(i,j,k) = dot(I(:,i),cross(I(:,j),I(:,k)));
        end
    end
end

function res=delta(i,j)
% Calculates the Kronecker delta
if i==j
    res = 1;
else
    res= 0;
end

    