% Least squares fit of the strain tensor in a grain.
% Uses  grain.pl, .uni.plid, .thetatype, .plstrain  values.


function [ST,res,Mplid]=gtStrainTensor(grain,ACM,mode_flag)

if mode_flag==2
  disp(' ')
  disp('Infinite number of solutions....')
  disp(' ')
  ST=NaN(3);
  totressqr=NaN;
  res=NaN;
  Mplid=[NaN NaN];
  return
end



if size(grain.uni.plid,1)<3 % not enough input data; strain tensor undetermined
  ST=NaN(3);
  totressqr=NaN;
  res=NaN;
  Mplid=[NaN NaN];
  return
end


nof_pl=size(grain.pl,1);  

STINP=[];

% Different projections of the strain values (relative displacement vector)
for i=1:nof_pl
  for j=(i+1):nof_pl

    if find(grain.uni.plid(:,i))~=find(grain.uni.plid(:,j))
      % they don't come from the same plane normal

      n1=grain.pl(i,:);
      n2=grain.pl(j,:);

      % find original (unstrained) angle between those plane normals
      ACV=ACM{grain.thetatype(i),grain.thetatype(j)};
      ang=gt2PlanesAngle(n1,n2);
      [minval,minloc]=min(abs(ACV-ang));
      origdotpro=cosd(ACV(minloc));

      if n1*n2'<0 % plane normals direction opposite
        origdotpro=-origdotpro; % set the sign of original dot product correctly
      end

      STINP=[STINP; [i j origdotpro]];

    end

  end
end

Sg0=gtStrainTensor_allplBragg(grain);
if any(isnan(Sg0))
  Sg0=zeros(1,6);
else
  Sg0=[Sg0(1,1) Sg0(2,2) Sg0(3,3) Sg0(1,2) Sg0(1,3) Sg0(2,3)] ;
end

[Dopt,fval,exitflag]=fminsearch(@(Sg) ...
  sfLSQresidual(Sg,grain,STINP,mode_flag),Sg0,...
  optimset('TolX',1e-10,'MaxIter',5000,'MaxFunEvals',5000)); %,'Display','iter'));

if exitflag~=1
  disp(' ')
  disp('No optimized values for correction were found.')
  disp(' ')
  ST=NaN(3);
  totressqr=NaN;
  res=NaN;
  Mplid=[NaN NaN];
  return
end


% aaa=evalin('base','ST0')';aaa=[aaa(1,1) aaa(2,2) aaa(3,3)  aaa(1,2) aaa(1,3) aaa(2,3)];
% [totressqr2,DT2,res2,Mplid2]=sfLSQresidual(aaa,grain,STINP,mode_flag);

[totressqr,ST,res,Mplid]=sfLSQresidual(Dopt,grain,STINP,mode_flag);


 
end 






%% Function to be optimized - sum of residual squares (LSQM)


function [totressqr,ST,res,Mplid]=sfLSQresidual(Sg,grain,STINP,mode_flag)

switch mode_flag
  case 2
    ST=[Sg(1) Sg(3) Sg(4);...
        Sg(3) Sg(2) Sg(5);...
        Sg(4) Sg(5) 0 ];
  case {0,1}
    ST=[Sg(1) Sg(4) Sg(5);...
        Sg(4) Sg(2) Sg(6);...
        Sg(5) Sg(6) Sg(3)];
end 
  
nof_pl=size(grain.pl,1);  
res=[];
Mplid=[];


I=eye(3); % identity matrix

B=inv(I+ST);

% Changes in the interplanar angles
for i=1:size(STINP,1)
  plid1=STINP(i,1);
  plid2=STINP(i,2);
  origdotpro=STINP(i,3);
  
  n1=grain.pl(plid1,:);
  n2=grain.pl(plid2,:);
  
  e12=cross(n1,n2);
  e1=cross(n1,e12);
  e2=cross(n2,e12);
  
  e12=e12/norm(e12);
  e1=e1/norm(e1);
  e2=e2/norm(e2);

  e12o=(B*e12')'; 
  e1o=(B*e1')';
  e2o=(B*e2')';

  A1o=norm(cross(e12o,e1o));
  A2o=norm(cross(e12o,e2o));
  
  e12o=e12o/norm(e12o);
  e1o=e1o/norm(e1o);
  e2o=e2o/norm(e2o);
  
  n1o=cross(e12o,e1o);
  n1o=n1o/norm(n1o);

  n2o=cross(e12o,e2o);
  n2o=n2o/norm(n2o);
  
  switch mode_flag
    case 0
      resnew1=grain.plstrain(plid1)-(det(ST+I)*A1o-1);
      resnew2=grain.plstrain(plid2)-(det(ST+I)*A2o-1);
      resnew3=n1o*n2o'-origdotpro;
      resnew4=n1o*n2o'-origdotpro;
      resnew=[resnew1; resnew2; resnew3; resnew4];
      Mplidnew=[STINP(i,1:2); STINP(i,1:2); STINP(i,1:2); STINP(i,1:2)];
    case 1
      resnew1=grain.plstrain(plid1)-(det(ST+I)*A1o-1);
      resnew2=grain.plstrain(plid2)-(det(ST+I)*A2o-1);
      resnew=[resnew1; resnew2];
      Mplidnew=[STINP(i,1:2); STINP(i,1:2)];
    case 2
      resnew=n1o*n2o'-origdotpro;
      Mplidnew=STINP(i,1:2);
  end
  
  res=[res; resnew]; 
  Mplid=[Mplid; Mplidnew];  
  
end

totressqr=sum((res).^2);

end







% function [totressqr,ST,res,Mplid]=sfLSQresidual(Sg,grain,STINP,mode_flag)
% 
% switch mode_flag
%   case 2
%     ST=[Sg(1) Sg(3) Sg(4);...
%         Sg(3) Sg(2) Sg(5);...
%         Sg(4) Sg(5) 0 ];
%   case {0,1}
%     ST=[Sg(1) Sg(4) Sg(5);...
%         Sg(4) Sg(2) Sg(6);...
%         Sg(5) Sg(6) Sg(3)];
% end 
%   
% nof_pl=size(grain.pl,1);  
% res=[];
% Mplid=[];
% 
% 
% I=eye(3); % identity matrix
% 
% B=inv(I+ST);
% 
% % Changes in the interplanar angles
% for i=1:size(STINP,1)
%   plid1=STINP(i,1);
%   plid2=STINP(i,2);
%   origdotpro=STINP(i,3);
%   
%   n1=grain.pl(plid1,:);
%   n2=grain.pl(plid2,:);
%   
%   e12=cross(n1,n2);
%   e1=cross(n1,e12);
%   e2=cross(n2,e12);
%   
%   e12=e12/norm(e12);
%   e1=e1/norm(e1);
%   e2=e2/norm(e2);
% 
%   e12o=(B*e12')'; 
%   e1o=(B*e1')';
%   e2o=(B*e2')';
% 
%   A1o=norm(cross(e12o,e1o));
%   A2o=norm(cross(e12o,e2o));
%   
%   e12o=e12o/norm(e12o);
%   e1o=e1o/norm(e1o);
%   e2o=e2o/norm(e2o);
%   
%   n1o=cross(e12o,e1o);
%   n1o=n1o/norm(n1o);
% 
%   n2o=cross(e12o,e2o);
%   n2o=n2o/norm(n2o);
%   
%   switch mode_flag
%     case 0
%       resnew1=grain.plstrain(plid1)-(det(ST+I)*A1o-1);
%       resnew2=grain.plstrain(plid2)-(det(ST+I)*A2o-1);
%       resnew3=n1o*n2o'-origdotpro;
%       resnew4=n1o*n2o'-origdotpro;
%       resnew=[resnew1; resnew2; resnew3; resnew4];
%       Mplidnew=[STINP(i,1:2); STINP(i,1:2); STINP(i,1:2); STINP(i,1:2)];
%     case 1
%       resnew1=grain.plstrain(plid1)-(det(ST+I)*A1o-1);
%       resnew2=grain.plstrain(plid2)-(det(ST+I)*A2o-1);
%       resnew=[resnew1; resnew2];
%       Mplidnew=[STINP(i,1:2); STINP(i,1:2)];
%     case 2
%       resnew=n1o*n2o'-origdotpro;
%       Mplidnew=STINP(i,1:2);
%   end
%   
%   res=[res; resnew]; 
%   Mplid=[Mplid; Mplidnew];  
%   
% end
% 
% totressqr=sum((res*1e5).^2);
% 
% end


