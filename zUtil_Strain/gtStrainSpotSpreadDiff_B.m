function [dpstrain,dnormext]=gtStrainSpotSpreadDiff_B(grainb,grainc,matchg)

nof_grainsb=length(grainb);


figure('name','Diff in average spot spread vs. diff. in principal strain')
hold on
xlabel('Diff. in absolut maximum principal strain')
ylabel('Diff. in normalised extension')

for i=1:nof_grainsb

	if ~isnan(matchg(i,2))

		[ab,bb]=sort(grainb{i}.uni.etaq,'ascend');
		nb=bb(1:3);
		mb=[];
		for ii=1:length(nb)
			mb=[mb, find(grainb{i}.uni.plid(nb(ii),:))];
		end

		[ac,bc]=sort(grainc{matchg(i,2)}.uni.etaq,'ascend');
		nc=bc(1:3);
		mc=[];
		for ii=1:length(nc)
			mc=[mc, find(grainc{matchg(i,2)}.uni.plid(nc(ii),:))];
		end
	
		
		dnormext(i)=mean(grainc{matchg(i,2)}.normext(mc))-mean(grainb{i}.normext(mb));
%		dpstrain(i)=abs(grainc{matchg(i,2)}.strain.pstrainmax)-abs(grainb{i}.strain.pstrainmax);
		dpstrain(i)=grainc{matchg(i,2)}.strain.pstrainmax-grainb{i}.strain.pstrainmax;
%		dpstrain(i)=grainc{matchg(i,2)}.strain.Eps(3)-grainb{i}.strain.Eps(3);
		
		plot(dpstrain(i),dnormext(i),'.')
	else
		dnormext(i)=NaN;
		dpstrain(i)=NaN;
	end
	
	
end

end