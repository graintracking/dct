# Instructions to install the DCT code
#
# Prerequisites:
# - python (version >= 2.6)  www.python.org
# - git    (version >= 1.6)  www.git-scm.com
# - g++    (version >= 4.4)  www.gcc.gnu.org
# - matlab (version >= 2012) www.mathworks.com
# - astra  (version >= 1.0)  www.code.google.com/p/astra-toolbox
# - cuda   (version >= 4.0)  www.nvidia.com/object/cuda_home_new.html

 1. Download the DCT install bundle from http://sourceforge.net/projects/dct/

 2. Extract the zip in a temporary directory (here it is /tmp)

user1@host1 ~/Downloads $ ls
dct-install.zip
user@host1 ~/Downloads $ unzip dct-install.zip -d /tmp
Archive:  dct-install.zip
 extracting: dct-install/dct_compile_matlab_functions.py
 extracting: dct-install/dct_compile_mex_functions.py
 extracting: dct-install/dct_io_xml.py
 extracting: dct-install/dct_utils_git.py
 extracting: dct-install/dct_utils_platform.py
 extracting: dct-install/dct_setup.py
user1@host1 ~/Downloads $ cd /tmp/dct-install
user1@host1 /tmp/dct-install $ ls
dct_compile_matlab_functions.py  dct_setup.py
dct_compile_mex_functions.py     dct_utils_git.py
dct_io_xml.py                    dct_utils_platform.py
dct_matlab_invocation.py

 3. Then, run the python script to print help to install the code
    (need python >= 2.6)

user1@host1 /tmp/dct-install $ python dct_setup.py -h
Installs DCT for both users and developers
 Options:
  -h | --help : to show this help
  -v | --verbose : to show more detailed output
  -b <branch_name> : to select a specific branch from the listed here:
     master
     stable_1.0
  -d <dir_name> : to specify directory name, where dct will be
  -u <user_name> : selects developer mode and specifies the developer sf_username
  -m <matlab_version> : selects the matlab version to be used
  -mp <matlab_dir> : selects the path to matlab 

 4. Now you can run the installation with your personal settings. As a user, 
    you don't need to specify a user name (only for developers). So to install 
    the master version in ~/dct directory, let's run, for instance at ESRF:

 user1@host1 /tmp/dct-install $ python dct_setup.py -b master -d ~/dct -m 2013a -mp /sware/com/matlab_2013a
Checking system...
Done.

System information:
  System Description: debian6 x86_64
  Matlab Version: 2013a
  Gcc Version: 4.4.5
  Git Version: 1.7.2.5

Git repository information:
  Mode: User
  Branch: master
  DCT Dir: /users/user1/dct

Installing...
  * Cloning repository...
Cloning into /users/user1/dct...
remote: Counting objects: 1978, done.
remote: Compressing objects: 100% (1632/1632), done.
remote: Total 1978 (delta 506), reused 1435 (delta 329)
Receiving objects: 100% (1978/1978), 26.10 MiB | 5.15 MiB/s, done.
Resolving deltas: 100% (506/506), done.
Done.

Initializing configuration files..
Creating new file: 'conf.xml'
New file path: /users/user1/dct/conf.xml
Updated '/users/user1/dct/conf.xml' with: 'matlab/version' <- '2013a'
Done.

Compiling MEX files..
[...]
Done.

Compiling batch functions... (may take several minutes)
[...]

Done.

DCT is now Installed in: '/users/user1/dct'

 5. Using the dct_launch.py script with the proper argument, you can:
- run matlab with the DCT code    [ matlab ] (works also with no arguments)
- update the code                 [ update ]
- update the configuration        [ update_conf ]
- recompile the mex functions     [ compile_mex ] (sub-options, see --help)
- recompile the matlab functions  [ compile_matlab ] (sub-options, see --help)
- list all options                [ -h | --help ] 

$ python /users/user1/dct/dct_launch.py

 6. Have fun with DCT!

 7. Update regularly the code with "update" command and the configuration with
    "update_conf" command.

 8. Submit bug reports to https://sourceforge.net/p/dct/tickets

 9. For more information or support, contact graintracking@esrf.fr


# dev

## pre-commit hooks

[pre-commit's official website](https://pre-commit.com/)

first, let's create an isolated environment for pre-commit

```
# using a conda installation that I found at `cat $(which silx)` 
os=$(/csadmin/common/scripts/get_os) 
source /sware/isdd/packages/${os}/dau/miniconda3/etc/profile.d/conda.sh

# create an empty installation of python 3.8
# src: https://stackoverflow.com/a/37216638/9582881
conda create --yes --prefix $HOME/.pre-commit-env -c conda-forge python=3.8 pre-commit
```

> **attention**
> now the thing loading conda above is in `scripts/get_conda.sh`
> you can source it to load dau's miniconda installation

Make the pre-commit executable accessible locally:

```
# link the executable to your local path
ln -s ~/.pre-commit-env/bin/pre-commit ~/.local/bin/
```

Now check that you have pre-commit properly installed:

```
which pre-commit 
# should be like /nana/ni/nana/na/.local/bin/pre-commit

pre-commit -V
# should be  >= 2.12.0 
```

Finally, let's activate it for the project:

```
cd ~/dct  # or wherever you put it...
pre-commit install
```

## environments

the python environment is with conda using a void python 3.8 installation

then, we install the depedencies in `zUtil_Python/setup.py`

**important: if you are working on a branch and wants to install a new package**

you can create another isolated env for your branch:

1. add the desired depedencies to `setup.py` (mentioned above)
2. in `scripts/envs.sh`, follow the instructions to build another env
3. you will have different envs in your ~/.conda/envs; manage them as you switch branches! 