function labgeo = gtMatchRecalculateLabgeo(newvars,newvarsind,oldvars,...
                  labgeobase)

% Recalculates labgeo parameters based on new and old values relative to
% labgeobase. New values come from the user or from fitting.
% It uses all the 'newvars' that are specified. For those parameters which 
% have no new value specified, it uses the 'oldvars'. 'Labgeo' is then 
% recalculated according to this complete list relative to 'labgeobase'.
%
% List of parameters in oldvars: 
%
% 1  = Position X (mm)
% 2  = Position Y (mm)
% 3  = Position Z (mm)
% 4  = Tilt in plane (deg)
% 5  = Angle U-V     (deg)
% 6  = Tilt around U (deg)
% 7  = Tilt around V (deg)
% 8  = Pixel size mean  (um)
% 9  = Pixel size ratio (1)
% 10 = lengtha (A)
% 12 = lengthb (A)
% 13 = lengthc (A)
% 14 = alpha   (deg)
% 15 = beta    (deg)
% 16 = gamma   (deg)



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Detector position, pixel sizes, tilts 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

actvars = oldvars;
actvars(newvarsind) = newvars;

% Detector position
labgeo.detrefpos(1,:) = actvars(1:3);

% Pixel sizes
labgeo.pixelsizeu = 0.001*actvars(8)*sqrt(actvars(9));
labgeo.pixelsizev = 0.001*actvars(8)/sqrt(actvars(9));

% Tilts around U,V and detector normal
rmcU = gtMathsRotationMatrixComp(labgeobase.detdiru, 'row');
rmcV = gtMathsRotationMatrixComp(labgeobase.detdirv, 'row');
rmcN = gtMathsRotationMatrixComp(labgeobase.detnorm, 'row');

rotU = gtMathsRotationTensor(actvars(6), rmcU);
rotV = gtMathsRotationTensor(actvars(7), rmcV);
rotN = gtMathsRotationTensor(actvars(4), rmcN);

% Complete rotation matrix 
rot = rotU*rotV*rotN;

% Apply rotation matrix to row vectors
detdiru = labgeobase.detdiru*rot;
detdirv = labgeobase.detdirv*rot;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Recalculate V according to angle between U and V 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Actual U-V angle
actangle = acosd(detdiru*detdirv');

% Update detector normal
detnorm = cross(detdiru, detdirv);
detnorm = detnorm/sqrt(detnorm*detnorm');

% Rotation matrix to adjust angle between U-V
rmcN = gtMathsRotationMatrixComp(detnorm, 'row');
rotN = gtMathsRotationTensor(0.5*(actvars(5) - actangle), rmcN);

% Apply correction opposite ways 
detdiru = detdiru*rotN';
detdirv = detdirv*rotN;

% Renormalise U and V vector
labgeo.detdiru = detdiru/sqrt(detdiru*detdiru');
labgeo.detdirv = detdirv/sqrt(detdirv*detdirv');

% Recalculate angle for checking (should be = actvars(5))
%uvangle = acosd(labgeo.detdiru*labgeo.detdirv')


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Update other 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Update detector normal
labgeo.detnorm = detnorm;

% Update detector origin: pixel (0,0) coordinates in LAB ref.
labgeo.detorig = labgeo.detrefpos - ...
    labgeo.detdiru*labgeobase.detrefu*labgeo.pixelsizeu - ...
    labgeo.detdirv*labgeobase.detrefv*labgeo.pixelsizev;


end % of function