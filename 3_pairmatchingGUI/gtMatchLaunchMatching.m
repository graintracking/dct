function handles = gtMatchLaunchMatching(handles)

% The matching process is launched via this function. It updates the 
% selected reflections to be used, resets the candidates list and sets
% the random index to 0.

gtMatchSetBusy(handles,'on')

set(handles.MatchPairs_Push_PreMatch,'Enable','off')
set(handles.MatchPairs_Push_Match,'Enable','off')
set(handles.MatchPairs_Push_Reset,'Enable','off')
set(handles.MatchPairs_Push_StopCont,'String',handles.stopcont{1})
set(handles.MatchPairs_Push_StopCont,'Enable','on')

drawnow

% Prepare dataset, create set A and B
handles = gtMatchSplitSpotData(handles);

% Assign random indices
handles.randindA(1:handles.maxindA) = randperm(handles.maxindA);

% Set random index to zero, to start from beginning
handles.LastRandomIndex = 0;

% Reset pairs candidates data
handles.pairscand.ind      = zeros(length(handles.randindA),handles.maxcands);
handles.pairscand.err      = zeros(length(handles.randindA),handles.maxcands);
handles.pairscand.theta    = zeros(length(handles.randindA),handles.maxcands);
handles.pairscand.diffvecX = zeros(length(handles.randindA),handles.maxcands);
handles.pairscand.diffvecY = zeros(length(handles.randindA),handles.maxcands);
handles.pairscand.diffvecZ = zeros(length(handles.randindA),handles.maxcands);
handles.pairscand.num      = zeros(length(handles.randindA),1);

% Update active reflections (reflections may have been (un)ticked)
handles = gtMatchUpdateUsedReflections(handles,0);



% Search pairs
%   (adds 'pairscand' and 'pairs' fields to handles)
handles = gtMatchFindPairCandidates(handles);

handles = gtMatchFilterPairs(handles, 1);
  

drawnow

