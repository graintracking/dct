function handles = gtMatchSaveFitParameters(handles)
 
% Saves the fitted detector and lattice parameters in the parameters file.


% % Make sure to use the same par file for the whole session, also when
% % saving the matching results later.
% if isempty(handles.parfilename)
%     backup_filename = gtLastFileName(sprintf('%s/parameters_old_%s_', ...
%         handles.workingdir, date), 'new');
%     handles.parfilename = backup_filename;
% else
%     backup_filename = handles.parfilename;
% end


% Dialog window
msg{1} = 'This will overwrite the current parameters in the parameters file.';
%msg{2} = 'Initial parameters have been backed up in 3_pairmatching/.'; 
msg{2} = 'Would you like to proceed?';
choice = questdlg(msg, 'Save parameters', 'Yes', 'Cancel', 'Yes');

switch (choice)
    case 'Cancel'
        return
end

parameters = handles.parameters;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Update labgeo
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

handles.DetParsCorrSaved = handles.DetParsCorrAct;

labgeo = rmfield(handles.labgeo, 'detorig');
labgeo = rmfield(labgeo, 'detnorm');
[parameters.detgeo, parameters.labgeo] = gtGeoConvertLegacyLabgeo2Detgeo(labgeo);
parameters.labgeo.omstep = gtAcqGetOmegaStep(parameters);

% Update detector parameters table
dettable = get(handles.DetPars_Table, 'Data');
dettable(:, 1) = num2cell(handles.DetParsCorrSaved);
set(handles.DetPars_Table, 'Data', dettable);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Update reflections
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if ~isempty(handles.UsedPhases)   

    % Active phase
    ph = handles.UsedPhases(get(handles.Phases_Listbox_Active, 'Value'));
    
    % For each phase, update parameters
    for ip = 1:handles.nof_phases
        
        % Lattice parameters
        parameters.cryst(ip).latticepar = handles.LattParsCorrAct(ip, :);
        
        % Plane families
        parameters.cryst(ip).theta = handles.CompPhaseThetas{ip};
        
        % 'B matrix': HKL -> Cartesian transformation
        Bmat = gtCrystHKL2CartesianMatrix(handles.LattParsCorrAct(ip, :));
     
        parameters.cryst(ip).dspacing = gtCrystDSpacing(handles.CompPhaseHKLs{ip}, ...
                                        Bmat);
        
        for ii = 1:length(parameters.cryst(ip).thetatypesp)
            spind = find(parameters.cryst(ip).thetatype == ... 
                         parameters.cryst(ip).thetatypesp(ii));
            
            parameters.cryst(ip).thetasp(ii)    = parameters.cryst(ip).theta(spind);
            
            parameters.cryst(ip).dspacingsp(ii) = parameters.cryst(ip).dspacing(spind);
        end
       
        % trying to save used families in cryst structure
        % !!! Notes by PReischig: 
        % To be fixed! This may save the wrong data. The fitting may be done with reduced or 
        % different families than the actual matching. It should be in the 
        % result saving function. This should be contained in
        % parameters.match instead. Double check the use of UsedHKLs!
        parameters.cryst(ip).usedfam = ismember(parameters.cryst(ip).hkl', handles.UsedHKLs', 'rows')';
    end
    
    handles.LattParsCorrSaved = handles.LattParsCorrAct;
    
    
    % Update lattice parameters table
    handles.LatticeParTable(:, 1, ph) = num2cell(handles.LattParsCorrSaved(ph, :)');
    set(handles.LattPars_Table, 'Data', handles.LatticeParTable(:, :, ph));
    
end

handles.parameters = parameters;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Save file
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

try 
    gtSaveParameters(parameters);
   
    disp(' ')
    disp('Parameters file has been updated.:')
    disp(' ')
    
    handles = gtMatchUpdateStatus(1, 'fitsaved', handles);
    set(handles.Text_Progress, 'String', 'Parameters saved.')

catch err
    set(handles.Text_Progress, 'String', 'Could not save parameters.')
    disp(' ')
    disp('Could not save parameters file.')
    disp(' ')
    rethrow(err)
end

end




