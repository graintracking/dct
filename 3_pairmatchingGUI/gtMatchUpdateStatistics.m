function gtMatchUpdateStatistics(handles)

% Calculates and displays histograms in the Statistics GUI.


% Pair data
if isfield(handles,'pairs')
    pairs = handles.pairs;
else
    clf(handles.GUIStatistics.figure1)
    return
end

% Progress status
set(handles.Text_Progress,'String','Calculating statistics...')
gtMatchSetBusy(handles,'on');
drawnow


% Matching parameters
mpars = handles.matchpars;

% Number of histogram bins to use
nbins = max(round(length(pairs.centimdiff)/20),10);

% Bins for end and start images (integers)
extbins  = 0:ceil(mpars.thr_ext_offset)+1;



% Clear figure and plot 
clf(handles.GUIStatistics.figure1)
figure(handles.GUIStatistics.figure1)


subplot(3,3,1)
title('Deviation in centroid images','FontSize',12,'FontWeight','bold')
hold on
[hist_centim,bins] = hist(abs(pairs.centimdiff),nbins);
hist_centim_max = max(hist_centim);
bar(bins,hist_centim,'EdgeColor','none');
plot([mpars.thr_max_offset mpars.thr_max_offset],[0 hist_centim_max],'r--')
xlim([-0.1 bins(end)+0.1])


subplot(3,3,2)
title('Deviation in reflection start images','FontSize',12,'FontWeight','bold')
hold on
xlim([-1 max(extbins)])
hist_extstim = hist(abs(pairs.extstimdiff),extbins);
hist_extstim_max = max(hist_extstim);
bar(extbins,hist_extstim,'EdgeColor','none');
plot([mpars.thr_ext_offset mpars.thr_ext_offset],[0 hist_extstim_max],'r--')


subplot(3,3,3)
title('Deviation in reflection end images','FontSize',12,'FontWeight','bold')
hold on
xlim([-1 max(extbins)])
hist_extendim = hist(abs(pairs.extendimdiff),extbins);
hist_extendim_max = max(hist_extendim);
bar(extbins,hist_extendim,'EdgeColor','none');
plot([mpars.thr_ext_offset mpars.thr_ext_offset],[0 hist_extendim_max],'r--')


subplot(3,3,4);
title('Theta deviation','FontSize',12,'FontWeight','bold')
hold on
[hist_thdiff, bins] = hist(abs(pairs.thetadiff),nbins);
hist_thdiff_max = max(hist_thdiff);
bar(bins,hist_thdiff,'EdgeColor','none');
if mpars.thr_theta ~= Inf
    plot([ mpars.thr_theta  mpars.thr_theta],[0 hist_thdiff_max],'r--')
end


subplot(3,3,5)
title('Ratio of integrated intensities','FontSize',12,'FontWeight','bold')
hold on
[hist_int,bins] = hist(pairs.intratio,nbins);
hist_int_max = max(hist_int);
bar(bins,hist_int,'EdgeColor','none');
plot([mpars.thr_intint mpars.thr_intint],[0 hist_int_max],'r--')


subplot(3,3,6)
title('Ratio of areas','FontSize',12,'FontWeight','bold')
hold on
[hist_area,bins] = hist(pairs.arearatio,nbins);
hist_area_max = max(hist_area);
bar(bins,hist_area,'EdgeColor','none');
plot([mpars.thr_area mpars.thr_area],[0 hist_area_max],'r--')


subplot(3,3,7);
title('Mean error','FontSize',12,'FontWeight','bold')
hold on
[hist_merr, bins] = hist(pairs.error,nbins);
hist_merr_max = max(hist_merr);
bar(bins,hist_merr,'EdgeColor','none');


subplot(3,3,8)
title('Ratio of bbox U size','FontSize',12,'FontWeight','bold')
hold on
[hist_bbsizeu,bins] = hist(pairs.bbsizeuratio,nbins);
hist_bbsizeu_max = max(hist_bbsizeu);
bar(bins,hist_bbsizeu,'EdgeColor','none');
plot([mpars.thr_bbsize mpars.thr_bbsize],[0 hist_bbsizeu_max],'r--')


subplot(3,3,9)
title('Ratio of bbox V size','FontSize',12,'FontWeight','bold')
hold on
[hist_bbsizev,bins] = hist(pairs.bbsizevratio,nbins);
hist_bbsizev_max = max(hist_bbsizev);
bar(bins,hist_bbsizev,'EdgeColor','none');
plot([mpars.thr_bbsize mpars.thr_bbsize],[0 hist_bbsizev_max],'r--')


set(handles.Text_Progress,'String','Statistics done.')
gtMatchSetBusy(handles,'off');
drawnow


