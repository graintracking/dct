function gtMatchUpdateThetaMarkers(handles,prop,newval)

% Updates the position or visibility of the theta markers in the Eta-Theta
% GUI figure when the theoretical theta values or other parameters change.

switch prop
    
    case 'height'
        
        for ip = 1:handles.nof_phases
            
            pos = cell2mat(get([handles.ThetaRectHist{ip,:}],'Position'));
            
            pos(:,4) = newval;
            
            for it = 1:numel(handles.ThetaRectHist(ip,:))
                set(handles.ThetaRectHist{ip,it},'Position',pos(it,:));
                set(handles.ThetaLineHist{ip,it},'YData',[0 newval]);
            end
            
        end
        
    case 'width'
        
        
        for ip = 1:handles.nof_phases

            nt = length(handles.CompPhaseThetas{ip});
            orig  = zeros(1,nt);
            width = zeros(1,nt);
          
            % New widths
            for it = 1:nt
                orig(it)  = handles.CompPhaseThetas{ip}(it) - handles.matchpars.thr_theta - ...
                    handles.CompPhaseThetas{ip}(it)*handles.matchpars.thr_theta_scale;
                width(it) = 2*(handles.matchpars.thr_theta + ...
                    handles.CompPhaseThetas{ip}(it)*handles.matchpars.thr_theta_scale);
            end
            
            orig(isnan(orig)) = 0;
            width(isnan(width)) = 0;

            % Keep a minimum width for plotting
            width(width<=0) = 1e-6;
            
            % Eta plot
            pos = get([handles.ThetaRectPlot{ip,:}],'Position');
            pos = cat(1,pos{:});
            pos(:,1) = orig;
            pos(:,3) = width;
            
            for it = 1:nt
                set(handles.ThetaRectPlot{ip,it},'Position',pos(it,:));
            end
            
            % Histogram
            pos = get([handles.ThetaRectHist{ip,:}],'Position');
            pos = cat(1, pos{:});
            pos(:,1) = orig;
            pos(:,3) = width;
            
            for it = 1:nt
                set(handles.ThetaRectHist{ip,it},'Position',pos(it,:));
            end
        end

    case 'center'
        
        % Update center lines of theta markers
        for ip = 1:handles.nof_phases
            for it = 1:length(handles.CompPhaseThetas{ip})
                xdata = [handles.CompPhaseThetas{ip}(it), handles.CompPhaseThetas{ip}(it)];
                
                if isnan(xdata(1))
                    xdata = [0 0];
                end

                % Eta vs. theta plot
                ydata = get(handles.ThetaLinePlot{ip,it},'YData');
                set(handles.ThetaLinePlot{ip,it},'XData',xdata,'YData',ydata);
        
                % Histogram
                ydata = get(handles.ThetaLineHist{ip,it},'YData');
                set(handles.ThetaLineHist{ip,it},'XData',xdata,'YData',ydata);

                % HKL Labels
                pos = get(handles.HKLLabelsPlot{ip,it},'Position');
                pos(1) = handles.CompPhaseThetas{ip}(it);
                if isnan(pos(1))
                    pos(1) = 0;
                end
                set(handles.HKLLabelsPlot{ip,it},'Position',pos);

            end
        end


   
    case 'select'
        
        for ip = 1:handles.nof_phases
            
            % Visible indices
            vis = (handles.CompThetasTicked{ip} & ~isnan(handles.CompThetasTicked{ip}));

            if (ismember(ip,handles.UsedPhases) && get(handles.GUIEtaTheta.Radio_ThetaRanges,'Value'))
                
                set([handles.ThetaRectPlot{ip, vis}],'Visible','on');
                set([handles.ThetaRectPlot{ip,~vis}],'Visible','off');
                
                set([handles.ThetaRectHist{ip, vis}],'Visible','on');
                set([handles.ThetaRectHist{ip,~vis}],'Visible','off');
                
                set([handles.ThetaLinePlot{ip, vis}],'Visible','on');
                set([handles.ThetaLinePlot{ip,~vis}],'Visible','off');
                
                set([handles.ThetaLineHist{ip, vis}],'Visible','on');
                set([handles.ThetaLineHist{ip,~vis}],'Visible','off');
                
            else                
                set([handles.ThetaRectPlot{ip,:}],'Visible','off');
                set([handles.ThetaRectHist{ip,:}],'Visible','off');
                set([handles.ThetaLinePlot{ip,:}],'Visible','off');
                set([handles.ThetaLineHist{ip,:}],'Visible','off');
            end
            
            if (ismember(ip,handles.UsedPhases) && get(handles.GUIEtaTheta.Radio_Labels,'Value'))
                set([handles.HKLLabelsPlot{ip, vis}],'Visible','on');
                set([handles.HKLLabelsPlot{ip,~vis}],'Visible','off');
            else
                set([handles.HKLLabelsPlot{ip,:}],'Visible','off');
            end
        end
end


drawnow

