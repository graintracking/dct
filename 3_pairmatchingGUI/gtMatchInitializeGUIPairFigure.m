function handles = gtMatchInitializeGUIPairFigure(handles)

% Sets initial graphic objects and figure properties for the pair figure GUI.

% Turn on standard toolbar for figure to have zoom and pan tools
set(handles.figure1,'Toolbar','figure')

mainhandles = guidata(handles.MainGUI);

labgeo = mainhandles.labgeo;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Full image Axes
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

cla(handles.Axes_Full);

% Create image and handle
blankfullim = zeros(labgeo.detsizeu,labgeo.detsizev);

% Create image object for full image (use imagesc; imshow would need Image 
% Proc. Toolbox License)
colormap(handles.Axes_Full,gray(256));
handles.Fullim_Image = imagesc(blankfullim,'Parent',handles.Axes_Full);

set(handles.Axes_Full,'NextPlot','add'); % same as 'hold on'

% If plot in 3D, draw a frame around image that stays visible in 3D (image
% does not). Rectangle is 2D object, but plot creates 3D line object.
if mainhandles.plot3D
    handles.Fullim_Frame = plot3([1,labgeo.detsizeu,labgeo.detsizeu,1,1],...
        [1,1,labgeo.detsizev,labgeo.detsizev,1],[0,0,0,0,0],...
        'Color','k','Parent',handles.Axes_Full);
else
    handles.Fullim_Frame = [];
end

axis(handles.Axes_Full,'off');
axis(handles.Axes_Full,'image');

zoom(handles.Axes_Full,'reset');

% Color limits
handles.CLimsFull = [];

% Full image number
handles.Fullimno = 0;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Sample envelope projection
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Set some default size to create object
% bbox for 'rectangle' is: [originX originY width height]
bbox = [1, 1, labgeo.detsizeu-1,labgeo.detsizev-1];

handles.Fullim_SamEnvProj = rectangle('Position', bbox,...
    'Parent',handles.Axes_Full,...
    'LineStyle','--','EdgeColor','y','LineWidth',1,'Clipping','on',...
    'Visible','on');

set(handles.Radio_SamEnvProj,'Value',get(handles.Radio_SamEnvProj,'Max'));


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Projection line, mirror point
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Radio button on
set(handles.Radio_ProjLine,'Value',get(handles.Radio_ProjLine,'Max'));

% Create object for A centroid
handles.Fullim_CentA = plot(NaN,NaN,'y+','Parent',handles.Axes_Full);
set(handles.Fullim_CentA,'XDataSource','centAu');
set(handles.Fullim_CentA,'YDataSource','centAv');

% Create the line object for projection line
handles.Fullim_ProjLine = plot(NaN,NaN,'-.y','Parent',handles.Axes_Full);
set(handles.Fullim_ProjLine,'XDataSource','projlineu');
set(handles.Fullim_ProjLine,'YDataSource','projlinev');

% Plot in 3D
if mainhandles.plot3D

    % Create the line object in 3D
    handles.Fullim_ProjLine3D = plot3(NaN,NaN,NaN,'-.y','Parent',handles.Axes_Full);
    set(handles.Fullim_ProjLine3D,'XDataSource','projline3Du');
    set(handles.Fullim_ProjLine3D,'YDataSource','projline3Dv');
    set(handles.Fullim_ProjLine3D,'ZDataSource','projline3Dz');

    % Create the line object for mirrored point in 3D
    handles.Fullim_CentAmirr = plot3(NaN,NaN,NaN,'-y+','Parent',handles.Axes_Full);
    set(handles.Fullim_CentAmirr,'XDataSource','centAmirru');
    set(handles.Fullim_CentAmirr,'YDataSource','centAmirrv');
    set(handles.Fullim_CentAmirr,'ZDataSource','centAmirrz');

    % Create the line object for mirror point in detector plane
    handles.Fullim_MirrorPoint = plot3(NaN,NaN,NaN,'-yo','Parent',handles.Axes_Full,...
        'MarkerSize',8);
    set(handles.Fullim_MirrorPoint,'XDataSource','mirrorpointu');
    set(handles.Fullim_MirrorPoint,'YDataSource','mirrorpointv');
    set(handles.Fullim_MirrorPoint,'ZDataSource','mirrorpointz');

% Plot in 2D
else

    handles.Fullim_ProjLine3D  = [];
    
      % Create the line object for mirrored point in 3D
    handles.Fullim_CentAmirr = plot(NaN,NaN,'-y+','Parent',handles.Axes_Full);
    set(handles.Fullim_CentAmirr,'XDataSource','centAmirru');
    set(handles.Fullim_CentAmirr,'YDataSource','centAmirrv');

    % Create the line object for mirror point in detector plane
    handles.Fullim_MirrorPoint = plot(NaN,NaN,'-yo','Parent',handles.Axes_Full,...
        'MarkerSize',8);
    set(handles.Fullim_MirrorPoint,'XDataSource','mirrorpointu');
    set(handles.Fullim_MirrorPoint,'YDataSource','mirrorpointv');
  
end




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Theta search ranges
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
handles.Fullim_ThetaLines  = [];
handles.Fullim_ThetaLabels = [];

set(handles.Radio_ThetaRegions,'Value',get(handles.Radio_ThetaRegions,'Max'));


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Bbox frames and labels for spot A
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Spot A
bbox = [1, 1, labgeo.detsizeu,labgeo.detsizev];

handles.Fullim_SpotARect  = rectangle('Position',bbox,'Parent',handles.Axes_Full,...
    'EdgeColor','y','LineStyle','--','Clipping','on');

handles.Fullim_SpotALabel = text(bbox(1),bbox(2)+bbox(4),' ',...
    'Parent',handles.Axes_Full,...
    'HorizontalAlignment','left',...
    'VerticalAlignment','top',...
    'Color','y');

set(handles.Radio_SpotA,'Value',get(handles.Radio_SpotA,'Max'));


% Spot B
handles.Fullim_SpotBRect  = [];
handles.Fullim_SpotBLabel = [];
handles.Fullim_SpotBCent  = [];

set(handles.Radio_Candidates,'Value',get(handles.Radio_Candidates,'Max'));


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Rotation axis
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Initial rot. axis position (recalculated as labgeo parameters change)
[handles.rotaxisendsuv, handles.rotaxisendsz] = gtMatchRotAxisProjection(labgeo);

if ~isempty(handles.rotaxisendsuv)
     handles.Fullim_RotAxis = line(handles.rotaxisendsuv(:,1),handles.rotaxisendsuv(:,2),...
     'LineStyle','-.','Color',[1 1 0],'Parent',handles.Axes_Full,'LineWidth',2);
 
else
    handles.Fullim_RotAxis = [];
end


% Set radio button and visibility
set(handles.Radio_RotAxis,'Value',get(handles.Radio_RotAxis,'Max'));



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Base vectors - lab geometry
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Get transformation matrix lab2det
%[~,labgeo.lab2det] = gtGeoLab2Det([0 0 0],labgeo,1);
[origxyz,labgeo.lab2det] = gtGeoLab2Det([0 0 0],labgeo,0);
  
% Scaling for quivers
scale = 0.5;

% 3D plots can be very slow for visualization; offer option
if mainhandles.plot3D
    
    % Rotation axis
    handles.Fullim_RotAxis3D = line(handles.rotaxisendsuv(:,1),handles.rotaxisendsuv(:,2),...
        handles.rotaxisendsz,'LineStyle','-.','Color',[1 1 0],...
        'Parent',handles.Axes_Full,'LineWidth',2);

    % Use lab2det  transformation matrix to get U,V coordinates. To display
    % everything relative to the image coordinate system, addition of a Z 
    % coordinate is needed. In order to keep this new reference right-handed,
    % the Z coordinate direction is defined negative from detnorm 
    % and the scaling is chosen reasonably as:
    transmat = [labgeo.lab2det, -labgeo.detnorm'/labgeo.pixelsizeu];

    % Not that if linux (X client) font settings are not correct, some 
    % fonts may not show up and plotting may get extremely slow. 

    
    % Detector orientation
    
    % Origin of plotted vectors
    origuvz = [1 1 0];
    
    dirvec = labgeo.detdiru*transmat;
    handles.Fullim_BaseVectors(1) = quiver3(origuvz(1),origuvz(2),origuvz(3),...
        dirvec(1),dirvec(2),dirvec(3),...
        scale,'Color','w','LineWidth',2,'Parent',handles.Axes_Full);
    handles.Fullim_BaseVectors(2) = text('Position',origuvz+dirvec*scale,...
        'String','Detdiru','Color','w','FontSize',14,'Parent',handles.Axes_Full,...
        'VerticalAlignment','Top');
    
    dirvec = labgeo.detdirv*transmat;
    handles.Fullim_BaseVectors(3) = quiver3(origuvz(1),origuvz(2),origuvz(3),...
        dirvec(1),dirvec(2),dirvec(3),...
        scale,'Color','w','LineWidth',2,'Parent',handles.Axes_Full);
    handles.Fullim_BaseVectors(4) = text('Position',origuvz+dirvec*scale,...
        'String','Detdirv','Color','w','FontSize',14,'Parent',handles.Axes_Full,...
        'VerticalAlignment','Top');

    
    % Origin of plotted vectors for rotdir, beamdir, Lab axes
    origuvz([1 2]) = origxyz([1 2]);
    origuvz(3)= ([0 0 0] - labgeo.detrefpos)*(-labgeo.detnorm')/labgeo.pixelsizeu;
    
    % Lab origin
    handles.Fullim_BaseVectors(15) = plot3(origuvz(1),origuvz(2),origuvz(3), ...
        'mo','LineWidth',2,'Parent',handles.Axes_Full);

    
    % Rotdir
    dirvec = labgeo.rotdir*transmat;
    handles.Fullim_BaseVectors(5) = quiver3(origuvz(1),origuvz(2),origuvz(3),...
        dirvec(1),dirvec(2),dirvec(3),...
        scale,'Color','y','LineWidth',2,'Parent',handles.Axes_Full);
    handles.Fullim_BaseVectors(6) = text('Position',origuvz+dirvec*scale,...
        'String','Rotdir','Color','y','FontSize',14,'Parent',handles.Axes_Full,...
        'VerticalAlignment','Top');
    
    
    % Beamdir
    dirvec = labgeo.beamdir*transmat;
    handles.Fullim_BaseVectors(7) = quiver3(origuvz(1),origuvz(2),origuvz(3),...
        dirvec(1),dirvec(2),dirvec(3),...
        scale,'Color','r','LineWidth',2,'Parent',handles.Axes_Full);
    handles.Fullim_BaseVectors(8) = text('Position',origuvz+dirvec*scale,...
        'String','Beamdir','Color','r','FontSize',14,'Parent',handles.Axes_Full,...
        'VerticalAlignment','Top','HorizontalAlignment','Right');
    
   
    % Lab axes
    dirvec = [1 0 0]*transmat;
    handles.Fullim_BaseVectors(9) = quiver3(origuvz(1),origuvz(2),origuvz(3),...
        dirvec(1),dirvec(2),dirvec(3),...
        scale,'Color','m','LineWidth',2,'Parent',handles.Axes_Full);
    handles.Fullim_BaseVectors(10) = text('Position',origuvz+dirvec*scale,...
        'String','X','Color','m','FontSize',14,'Parent',handles.Axes_Full,...
        'VerticalAlignment','Bottom','HorizontalAlignment','Right');
    
    dirvec = [0 1 0]*transmat;
    handles.Fullim_BaseVectors(11) = quiver3(origuvz(1),origuvz(2),origuvz(3),...
        dirvec(1),dirvec(2),dirvec(3),...
        scale,'Color','m','LineWidth',2,'Parent',handles.Axes_Full);
    handles.Fullim_BaseVectors(12) = text('Position',origuvz+dirvec*scale,...
        'String','Y','Color','m','FontSize',14,'Parent',handles.Axes_Full,...
        'VerticalAlignment','Bottom','HorizontalAlignment','Right');
    
    dirvec = [0 0 1]*transmat;
    handles.Fullim_BaseVectors(13) = quiver3(origuvz(1),origuvz(2),origuvz(3),...
        dirvec(1),dirvec(2),dirvec(3),...
        scale,'Color','m','LineWidth',2,'Parent',handles.Axes_Full);
    handles.Fullim_BaseVectors(14) = text('Position',origuvz+dirvec*scale,...
        'String','Z','Color','m','FontSize',14,'Parent',handles.Axes_Full,...
        'VerticalAlignment','Bottom','HorizontalAlignment','Right');

    
% Else plot in 2D
else
    
    handles.Fullim_RotAxis3D = [];
    
    % Use transformation matrix
    transmat = labgeo.lab2det;

    % Detector orientation

    % Origin of plotted vectors
    origuv = [1 1];
    
    dirvec = labgeo.detdiru*transmat;
    handles.Fullim_BaseVectors(1) = quiver(origuv(1),origuv(2),...
        dirvec(1),dirvec(2),...
        scale,'Color','w','LineWidth',2,'Parent',handles.Axes_Full);
    handles.Fullim_BaseVectors(2) = text('Position',origuv+dirvec*scale,...
        'String','Detdiru','Color','w','FontSize',14,'Parent',handles.Axes_Full,...
        'VerticalAlignment','Top','HorizontalAlignment','Right');
 
    dirvec = labgeo.detdirv*transmat;
    handles.Fullim_BaseVectors(3) = quiver(origuv(1),origuv(2),...
        dirvec(1),dirvec(2),...
        scale,'Color','w','LineWidth',2,'Parent',handles.Axes_Full);
    handles.Fullim_BaseVectors(4) = text('Position',origuv+dirvec*scale,...
        'String','Detdirv','Color','w','FontSize',14,'Parent',handles.Axes_Full,...
        'VerticalAlignment','Top','HorizontalAlignment','Left');
    
    % Origin of plotted vectors for rotdir, beamdir, Lab axes
    origuv = origxyz([1 2]);

    % Lab origin
    handles.Fullim_BaseVectors(15) = plot3(origuv(1),origuv(2), ...
        'mo','LineWidth',2,'Parent',handles.Axes_Full);

    % Rotdir
    dirvec = labgeo.rotdir*transmat;
    handles.Fullim_BaseVectors(5) = quiver(origuv(1),origuv(2),...
        dirvec(1),dirvec(2),...
        scale,'Color','y','LineWidth',2,'Parent',handles.Axes_Full);
    handles.Fullim_BaseVectors(6) = text('Position',origuv+dirvec*scale,...
        'String','Rotdir','Color','y','FontSize',14,'Parent',handles.Axes_Full,...
        'VerticalAlignment','Top');
    
    
    % Beamdir
    dirvec = labgeo.beamdir*transmat;
    handles.Fullim_BaseVectors(7) = quiver(origuv(1),origuv(2),...
        dirvec(1),dirvec(2),...
        scale,'Color','r','LineWidth',2,'Parent',handles.Axes_Full);
    handles.Fullim_BaseVectors(8) = text('Position',origuv+dirvec*scale,...
        'String','Beamdir','Color','r','FontSize',14,'Parent',handles.Axes_Full,...
        'VerticalAlignment','Top','HorizontalAlignment','Right');
    
    
    % Lab axes
    dirvec = [1 0 0]*transmat;
    handles.Fullim_BaseVectors(9) = quiver(origuv(1),origuv(2),...
        dirvec(1),dirvec(2),...
        scale,'Color','m','LineWidth',2,'Parent',handles.Axes_Full);
    handles.Fullim_BaseVectors(10) = text('Position',origuv+dirvec*scale,...
        'String','X','Color','m','FontSize',14,'Parent',handles.Axes_Full,...
        'VerticalAlignment','Bottom','HorizontalAlignment','Right');
    
    dirvec = [0 1 0]*transmat;
    handles.Fullim_BaseVectors(11) = quiver(origuv(1),origuv(2),...
        dirvec(1),dirvec(2),...
        scale,'Color','m','LineWidth',2,'Parent',handles.Axes_Full);
    handles.Fullim_BaseVectors(12) = text('Position',origuv+dirvec*scale,...
        'String','Y','Color','m','FontSize',14,'Parent',handles.Axes_Full,...
        'VerticalAlignment','Bottom','HorizontalAlignment','Right');
    
    dirvec = [0 0 1]*transmat;
    handles.Fullim_BaseVectors(13) = quiver(origuv(1),origuv(2),...
        dirvec(1),dirvec(2),...
        scale,'Color','m','LineWidth',2,'Parent',handles.Axes_Full);
    handles.Fullim_BaseVectors(14) = text('Position',origuv+dirvec*scale,...
        'String','Z','Color','m','FontSize',14,'Parent',handles.Axes_Full,...
        'VerticalAlignment','Bottom','HorizontalAlignment','Right'); 
    
end


% Rot. axis visibility
if ishandle(handles.Fullim_RotAxis)
    set(handles.Fullim_RotAxis,'Visible','on')
    set(handles.Fullim_RotAxis3D,'Visible','on')
end


% Set radio button and visibility
set(handles.Radio_BaseVectors,'Value',get(handles.Radio_BaseVectors,'Max'));

if ishandle(handles.Fullim_BaseVectors)
    set(handles.Fullim_BaseVectors,'Visible','on')
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Spot A Axes
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

cla(handles.Axes_SpotA);

set(handles.Axes_SpotA,'XTick',[]);
set(handles.Axes_SpotA,'YTick',[]);

handles.CLimsSpotA = [];

% Radio buttons
set(handles.Radio_FlipHor,'Value',0)
set(handles.Radio_FlipVer,'Value',0)


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Spot B Axes
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

cla(handles.Axes_SpotB);

set(handles.Axes_SpotB,'XTick',[]);
set(handles.Axes_SpotB,'YTick',[]);
set(handles.Axes_SpotB,'NextPlot','add'); % same as hold on

handles.CLimsCand = [];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Eta angles
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

beamdir = labgeo.beamdir/norm(labgeo.beamdir);
rotdir  = labgeo.rotdir/norm(labgeo.rotdir);

% The diffraction vectors and the vector of the rotation axis direction 
% are projected and analysed in a plane perpendicular to the beam.

% Vector pointing to eta=0 (rotation axis direction):
handles.etavec0 = rotdir - beamdir*(rotdir*beamdir');

% Vector pointing to eta=90:
handles.etavec90 = cross(beamdir,rotdir);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Spot selection
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

handles.spotsel.difspotID = 1;
handles.spotsel.spotindex = 1;
handles.spotsel.pairID    = 1;

set(handles.Edit_DifspotID,'String','1')
set(handles.Edit_SpotIndex,'String','1')
set(handles.Edit_PairID,'String','1')


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Interruptibles
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Set every callack non-interruptible
allch = get(handles.Panel_SpotSelection,'Children');
set(allch,'Interruptible','off');

