function varargout = gtMatchGUIEtaVsTheta(varargin)

% Eta-Theta plotting GUI main function and callbacks.
% It is launched by the main GUI.

% GTMATCHGUIETAVSTHETA MATLAB code for gtMatchGUIEtaVsTheta.fig
%      GTMATCHGUIETAVSTHETA, by itself, creates a new GTMATCHGUIETAVSTHETA or raises the existing
%      singleton*.
%
%      H = GTMATCHGUIETAVSTHETA returns the handle to a new GTMATCHGUIETAVSTHETA or the handle to
%      the existing singleton*.
%
%      GTMATCHGUIETAVSTHETA('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in GTMATCHGUIETAVSTHETA.M with the given input arguments.
%
%      GTMATCHGUIETAVSTHETA('Property','Value',...) creates a new GTMATCHGUIETAVSTHETA or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before gtMatchGUIEtaVsTheta_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to gtMatchGUIEtaVsTheta_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help gtMatchGUIEtaVsTheta

% Last Modified by GUIDE v2.5 01-Feb-2012 18:21:16

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @gtMatchGUIEtaVsTheta_OpeningFcn, ...
                   'gui_OutputFcn',  @gtMatchGUIEtaVsTheta_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Opening and output function
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% --- Executes just before gtMatchGUIEtaVsTheta is made visible.
function gtMatchGUIEtaVsTheta_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to gtMatchGUIEtaVsTheta (see VARARGIN)

% Choose default command line output for gtMatchGUIEtaVsTheta
handles.output.figure1 = hObject;
handles.output.Axes_EtaPlot     = handles.Axes_EtaPlot;
handles.output.Axes_ThetaHist   = handles.Axes_ThetaHist;
handles.output.Axes_ThetaLimits = handles.Axes_ThetaLimits;

% Input data for plotting
handles.mainguihandle = varargin{1};

% Set layout and link data to figures
handles = gtMatchInitializeGUIEtaVsTheta(handles);

% Provide in the output those handles which will be used by the main GUI 
handles.output.EtaHandle  = handles.EtaHandle;
handles.output.HistHandle = handles.HistHandle;
handles.output.Radio_ThetaFilter = handles.Radio_ThetaFilter;
handles.output.Radio_ThetaRanges = handles.Radio_ThetaRanges;
handles.output.Radio_Labels      = handles.Radio_Labels;

% Set figure
set(hObject,'Name','Eta vs Theta - Matching GUI')

% Set location in window
set(hObject,'Units','Normalized');
pos    = get(hObject,'Position');
pos(1) = 1-pos(3);
pos(2) = 1-pos(4);
set(hObject,'Position',pos);

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes gtMatchGUIEtaVsTheta wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = gtMatchGUIEtaVsTheta_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% MainGUI button
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% --- Executes on button press in Push_MainGUI.
function Push_MainGUI_Callback(hObject, eventdata, handles)
% hObject    handle to Push_MainGUI (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

figure(handles.mainguihandle);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Radio buttons for display selection
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% --- Executes on button press in Radio_ThetaFilter.
function Radio_ThetaFilter_Callback(hObject, eventdata, handles)
% hObject    handle to Radio_ThetaFilter (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hint: get(hObject,'Value') returns toggle state of Radio_ThetaFilter

val = get(hObject,'Value');

mainhandles = guidata(handles.mainguihandle);

if val 
    %gtMatchUpdateThetaRegionDisplay(mainhandles);
    set(cell2mat(mainhandles.ThetaFilterPlot(:)),'Visible','on');
else
    set(cell2mat(mainhandles.ThetaFilterPlot(:)),'Visible','off');
end



% --- Executes on button press in Radio_ThetaRanges.
function Radio_ThetaRanges_Callback(hObject, eventdata, handles)
% hObject    handle to Radio_ThetaRanges (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hint: get(hObject,'Value') returns toggle state of Radio_ThetaRanges

val = get(hObject,'Value');

mainhandles = guidata(handles.mainguihandle);

if val 
    gtMatchUpdateThetaMarkers(mainhandles,'select');
else
    set(cell2mat(mainhandles.ThetaRectPlot(:)),'Visible','off');
    set(cell2mat(mainhandles.ThetaRectHist(:)),'Visible','off');
    set(cell2mat(mainhandles.ThetaLinePlot(:)),'Visible','off');
    set(cell2mat(mainhandles.ThetaLineHist(:)),'Visible','off');
end



% --- Executes on button press in Radio_Labels.
function Radio_Labels_Callback(hObject, eventdata, handles)
% hObject    handle to Radio_Labels (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hint: get(hObject,'Value') returns toggle state of Radio_Labels

val = get(hObject,'Value');

mainhandles = guidata(handles.mainguihandle);

if val
    % Get current position of active labels
    hhkl = mainhandles.HKLLabelsPlot(mainhandles.UsedPhases,:);
    hhkl = cell2mat(hhkl(:));
    pos =  cell2mat(get(hhkl,'Position'));
    
    % Set Y position for lables so that they are in the visible region 
    % of the plot
    mineta = min(mainhandles.pairsfilt.eta);
    
    if isnan(mineta) % if there are no pairs yet, plot in axes center
        yl   = get(mainhandles.GUIEtaTheta.Axes_EtaPlot,'Ylim');
        newy = (yl(1)+yl(2))/2;
    else
        newy = mineta;
    end
    
    % Update position and set visibility
    for ii = 1:length(hhkl)
        pos(ii,2) = newy;
        set(hhkl(ii),'Position',pos(ii,:))
    end
    
    gtMatchUpdateThetaMarkers(mainhandles,'select');

else
    set(cell2mat(mainhandles.HKLLabelsPlot(:)),'Visible','off');
end
