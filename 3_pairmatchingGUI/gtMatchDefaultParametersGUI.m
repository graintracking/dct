function par_match = gtMatchDefaultParametersGUI()
% GTMATCHDEFAULTPARAMETERSGUI  Stores and provides the default parameters for Friedel pair matching.
%     par_match = gtMatchDefaultParametersGUI()
%     -----------------------------------------
%

par_match.thr_theta        = 0.2;
par_match.thr_theta_scale  = 0;

par_match.thr_max_offset   = 2;
par_match.thr_ext_offset   = 10;
par_match.thr_genim_offset = 2;

par_match.thr_bbsize       = 1.5;
par_match.thr_area         = 1.6;
par_match.thr_intint       = 6;

par_match.minsizeu         = 5;
par_match.minsizev         = 5;
par_match.addconstr        = '';

par_match.thr_meanerror    = Inf;
par_match.thetalimits      = [0 90];
par_match.uniquetheta      = 0.1;


end % end of function