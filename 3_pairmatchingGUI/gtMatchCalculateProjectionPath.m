function labAmirr = gtMatchCalculateProjectionPath(centA,centB,handles,labgeo)

% Calculates the projection path in the pair figure for the current spot A.
% Called by gtMatchDrawPairs.

% Transform into Lab coordinates (account also for pixel size)
labA = gtGeoDet2Lab(centA,labgeo,0);

% Mirror A on the rotation axis while staying in the same LAB reference:
labAmirr = gtMathsMirrorPointsOnAxis(labA,labgeo.rotpos,labgeo.rotdir);

% Project mirrored point back onto the detector plane 
projAmirr = gtGeoLab2Det(labAmirr,labgeo,0);

% Points along projection A-A'-B as seen on detector plane
projline = [centA; projAmirr; centB]; % if centB empty, last row is empty

% Refresh plotted line data
projlineu = projline(:,1);  % used in refreshdata
projlinev = projline(:,2);  % used in refreshdata
refreshdata(handles.Fullim_ProjLine,'caller');

% Refresh plotted mirror point
centAu = centA(1); % used in refreshdata
centAv = centA(2); % used in refreshdata
refreshdata(handles.Fullim_CentA,'caller');


% Points out of plane
projAmirrZ = (labAmirr-labgeo.detrefpos)*(-labgeo.detnorm')/labgeo.pixelsizeu;

% Points along projection A-A'-B in 3D
if ~isempty(handles.Fullim_ProjLine3D)

    if isempty(centB)
        projlineuvz = [centA, 0; ...
            projAmirr(1), projAmirr(2), projAmirrZ];
    else
        projlineuvz = [centA, 0; ...
            projAmirr(1), projAmirr(2), projAmirrZ; ...
            centB, 0];
    end
    
    projline3Du = projlineuvz(:,1);
    projline3Dv = projlineuvz(:,2);
    projline3Dz = projlineuvz(:,3);
    
    % Refresh plotted line
    refreshdata(handles.Fullim_ProjLine3D,'caller');
    
end


% Refresh plotted mirror point
mirrorpointu = (centA(1) + projAmirr(1))/2; % used in refreshdata
mirrorpointu = [mirrorpointu, mirrorpointu];
mirrorpointv = (centA(2) + projAmirr(2))/2; % used in refreshdata
mirrorpointv = [mirrorpointv, mirrorpointv];
mirrorpointz = [0, projAmirrZ]/2;
refreshdata(handles.Fullim_MirrorPoint,'caller');


% Refresh plotted mirror point
centAmirru = [projAmirr(1), projAmirr(1)]; % used in refreshdata
centAmirrv = [projAmirr(2), projAmirr(2)]; % used in refreshdata
centAmirrz = [0, projAmirrZ]; % used in refreshdata
refreshdata(handles.Fullim_CentAmirr,'caller');

