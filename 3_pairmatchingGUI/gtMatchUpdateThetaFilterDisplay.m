function handles = gtMatchUpdateThetaFilterDisplay(handles)

% Updates the display of theta filter in the Eta-Theta GUI figure when its
% limits have changed.


delete(get(handles.GUIEtaTheta.Axes_ThetaLimits,'Children'))

thetalims = handles.ThetaLimits;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Set rectangles in theta filter limits bar
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for il = 1:size(thetalims,1)
    
    % Position [x y width height]
    pos(1) = thetalims(il,1);
    pos(2) = 0;
    pos(3) = thetalims(il,2) - thetalims(il,1);
    pos(4) = 1;
 
    rectangle('Parent',handles.GUIEtaTheta.Axes_ThetaLimits, ...
        'Position',pos,'FaceColor',[0 1 0],'Clipping','on');
end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Set rectangles in Eta-Theta plot
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fcolor = [0.55 1 0.55];

for ith = 1:length(handles.ThetaFilterPlot)
    if ishandle(handles.ThetaFilterPlot{ith})
        delete(handles.ThetaFilterPlot{ith})
    end
end


handles.ThetaFilterPlot = cell(1,size(thetalims,1));

for ith = 1:size(thetalims,1)
    
    pos(1) = thetalims(ith,1);
    pos(2) = 0;
    pos(3) = thetalims(ith,2) - thetalims(ith,1); 
    
    % Rectangles in eta-theta plot
    pos(4) = 360;
    
    handles.ThetaFilterPlot{ith} = rectangle('Position',pos,...
        'Parent',handles.GUIEtaTheta.Axes_EtaPlot,...
        'Facecolor',fcolor,'Clipping','on','EdgeColor','none',...
        'Visible','off');
    
end


% Put the filter rectangles behind everything else in plot
% (plotting order is by order in 'children') 
hchildren       = get(handles.GUIEtaTheta.Axes_EtaPlot,'Children');
hind            = ismember(hchildren,[handles.ThetaFilterPlot{:}]);
hchildren(hind) = [];
hchildren       = [hchildren; [handles.ThetaFilterPlot{:}]'];
set(handles.GUIEtaTheta.Axes_EtaPlot,'Children',hchildren);

% If radio button was on, keep it visible
keepon = get(handles.GUIEtaTheta.Radio_ThetaFilter,'Value');

if keepon 
    set(cell2mat(handles.ThetaFilterPlot(:)),'Visible','on');
end
