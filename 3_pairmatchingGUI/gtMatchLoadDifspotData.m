function handles = gtMatchLoadDifspotData(handles)

% Loads difspot metadata from the Difspot table of the database to be used 
% in the matching. After loading all the difspot data via this function,
% they will be stored and accessed from Matlab's memory, and the database 
% will not be queried anymore. 


%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Load difspot data
%%%%%%%%%%%%%%%%%%%%%%%%%%%%

gtDBConnect;

matchpars = handles.matchpars;

% Number of spots to be considered ...
%  ... in first half
% handles.nof_spotsA = mym(sprintf(['SELECT count(*) FROM %s WHERE CentroidImage BETWEEN '...
% 	         '%0.20g AND %0.20g AND BoundingBoxXsize>%0.20g AND BoundingBoxYsize>%0.20g %s'],...
% 			 handles.difspottable,0,handles.nproj,matchpars.minsizeu,matchpars.minsizev,matchpars.addconstr));
handles.nof_spotsA = mym(sprintf(['SELECT count(*) FROM %s WHERE CentroidImage BETWEEN '...
	         '%0.20g AND %0.20g'], handles.difspottable, 0, handles.nproj));
%  ... in second half
% handles.nof_spotsB = mym(sprintf(['SELECT count(*) FROM %s WHERE CentroidImage BETWEEN '...
%              '%0.20g AND %0.20g AND BoundingBoxXsize>%0.20g AND BoundingBoxYsize>%0.20g %s'],...
%              handles.difspottable,handles.nproj,2*handles.nproj,matchpars.minsizeu,matchpars.minsizev,matchpars.addconstr));
handles.nof_spotsB = mym(sprintf(['SELECT count(*) FROM %s WHERE CentroidImage BETWEEN '...
             '%0.20g AND %0.20g'], handles.difspottable, handles.nproj, 2*handles.nproj));
% Total:
% handles.nof_allspots = mym(sprintf(['SELECT count(*) FROM %s WHERE '... 
%              'BoundingBoxXsize>%0.20g AND BoundingBoxYsize>%0.20g %s'],...
%              handles.difspottable,matchpars.minsizeu,matchpars.minsizev,matchpars.addconstr));
handles.nof_allspots = mym(sprintf('SELECT count(*) FROM %s', handles.difspottable));
        
disp(' ')
disp('Total number of spots considered:'), disp(handles.nof_allspots) ;
disp('Number of spots considered in the first  half (A):'), disp(handles.nof_spotsA) ;
disp('Number of spots considered in the second half (B):'), disp(handles.nof_spotsB) ;

handles.max_difspotID = mym(sprintf('SELECT max(difspotID) FROM %s',handles.difspottable));

% Creat mysql command text
% mysqlcmd = sprintf(['SELECT difspotID,Area,Integral,'...
%     'CentroidImage,ExtStartImage,ExtEndImage,'...
%     'CentroidX,CentroidY,'...
%     'BoundingBoxXorigin,BoundingBoxYorigin,BoundingBoxXsize,BoundingBoxYsize'...
%     ' FROM %s WHERE BoundingBoxXsize>%0.20g AND BoundingBoxYsize>%0.20g'...
%     ' %s ORDER BY CentroidImage asc, Integral desc'],...
%     handles.difspottable,matchpars.minsizeu,matchpars.minsizev,matchpars.addconstr);
mysqlcmd = sprintf(['SELECT difspotID,Area,Integral,'...
    'CentroidImage,ExtStartImage,ExtEndImage,'...
    'CentroidX,CentroidY,'...
    'BoundingBoxXorigin,BoundingBoxYorigin,BoundingBoxXsize,BoundingBoxYsize'...
    ' FROM %s ORDER BY CentroidImage asc, Integral desc'], handles.difspottable);

% Load all spot data from difspot table
[allsp.difspotid, allsp.area, allsp.integral,...
 allsp.centim, allsp.extstim, allsp.extendim,...
 allsp.centu, allsp.centv, ...
 allsp.bborigu, allsp.bborigv, allsp.bbsizeu, allsp.bbsizev] = mym(mysqlcmd);

% Assign index used in matching (different from spot ID !)
allsp.ind = (1:length(allsp.difspotid))';

allsp.pairid = NaN(length(allsp.ind),1);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Existing pairs in table
%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Initial no. of pairs
handles.nof_pairs_ini = mym(sprintf('SELECT count(*) FROM %s',handles.pairtable));

disp('Number of Friedel pairs already present in database:')
disp(handles.nof_pairs_ini)


% Display status
if handles.nof_pairs_ini > 0
    set(handles.Text_PairtableEmpty,'String','NOT EMPTY!')
    set(handles.Text_PairtableEmpty,'BackgroundColor',[1 0.6 0])
else
    set(handles.Text_PairtableEmpty,'String','Empty')
    set(handles.Text_PairtableEmpty,'BackgroundColor',[0 1 0])
end


handles.pairscand.num = zeros(length(allsp.difspotid),1);
handles.pairscand.ind = zeros(length(allsp.difspotid),1);
handles.pairscand.err = zeros(length(allsp.difspotid),1);


if handles.nof_pairs_ini > 0

    handles.dataloaded = true;
    
    disp('NOTE: change or fitting of parameters cannot be done on data loaded from the database.')
    disp('  It needs resetting the pairs and redoing the matching.')
    
    % Creat mysql command text
    mysqlcmd = sprintf(['SELECT pairID, difAID, difBID, meanerror, theta, '...
        'eta, etaB, omega, omegaB, phasetype, thetatype '...
        'FROM %s ORDER BY meanerror asc'], handles.pairtable);
    
    [ps.pairid, ps.difspotidA, ps.difspotidB, ps.error, ps.theta, ...
     ps.eta, ps.etaB, ps.omegaA, ps.omegaB, ps.phasetype, ps.thetatype] = ...
     mym(mysqlcmd);
     
    np = length(ps.pairid);
     
    ps.indA       = zeros(np,1);
    ps.indB       = zeros(np,1);
    ps.centimA    = zeros(np,1);
    ps.centimB    = zeros(np,1);
    ps.centimdiff = zeros(np,1);
    ps.thetadiff  = NaN(np,1);
    ps.hkl        = NaN(np,3);
    ps.hklunique  = false(np,1);

    gauge = GtGauge(numel(ps.difspotidA), 'Loading Diffraction Spots info: ');
    for ii = 1:numel(ps.difspotidA)
        gauge.incrementAndDisplay();

        indA = find(ps.difspotidA(ii)==allsp.difspotid,1,'first');
        indB = find(ps.difspotidB(ii)==allsp.difspotid,1,'first');
        
        if (isempty(indA) || isempty(indB))
            disp('WARNING! Inconsistency in difspot table, spotpairs table or matching parameters.');
            gauge.rePrint();
            continue;
        else
            ps.indA(ii,1) = indA;
            ps.indB(ii,1) = indB;
        end
        
        ps.centimA(ii,1)    = allsp.centim(ps.indA(ii));
        ps.centimB(ii,1)    = allsp.centim(ps.indB(ii));
        ps.centimdiff(ii,1) = ps.centimB(ii) - ps.centimA(ii) - ...
                              handles.parameters.acq(1).nproj;

        if (ps.phasetype(ii) && ps.thetatype(ii))
            
            tmphkl = handles.parameters.cryst(ps.phasetype(ii)).hkl(:,ps.thetatype(ii));
            
            if size(tmphkl,1)==3
                ps.hkl(ii,:) = tmphkl;
            elseif size(tmphkl,1)==4
                ps.hkl(ii,:) = tmphkl([1 2 4]);
            else
                error('Expected format for field hkl is 3xn or 4xn.');
            end
                
            ps.hklunique(ii,1) = true;
            
        else
            ps.hkl(ii,:) = [NaN NaN NaN];
            ps.hklunique(ii,1) = false;
        end

        allsp.pairid(ps.indA(ii,1)) = ps.pairid(ii);
        allsp.pairid(ps.indB(ii,1)) = ps.pairid(ii);
        
        handles.pairscand.num(ps.indA(ii,1),1) = 1;
        handles.pairscand.ind(ps.indA(ii,1),1) = ps.indB(ii);
        handles.pairscand.err(ps.indA(ii,1),1) = ps.error(ii);
    end
    gauge.delete();
   
    handles.pairs           = ps;
    handles.pairs.filttheta = true(size(handles.pairs.indA));
    handles.pairs.filterr   = true(size(handles.pairs.indA));
    handles.pairs.filtind   = true(size(handles.pairs.indA));

    handles.pairsfilt = handles.pairs;

else
    handles.dataloaded = false;
end

handles.allsp = allsp;

end % end of function

