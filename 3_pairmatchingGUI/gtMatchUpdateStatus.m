function handles = gtMatchUpdateStatus(status,prop,handles)

% Updates the status display of parameters fitting or matching in the
% main GUI.

switch prop
    
    case 'fitdone'

        handles.fitdone = logical(status);
        
        if handles.fitdone
            set(handles.Text_FitDone,'String',handles.statusstring{2});
            set(handles.Text_FitDone,'BackgroundColor',handles.statuscolor{2});
        else
            set(handles.Text_FitDone,'String',handles.statusstring{1});
            set(handles.Text_FitDone,'BackgroundColor',handles.statuscolor{1});
            set(handles.Text_FitSaved,'String',handles.statusstring{1});
            set(handles.Text_FitSaved,'BackgroundColor',handles.statuscolor{1});
            
            handles.fitsaved = false;
        end
       
    case 'fitsaved'
  
        handles.fitsaved = logical(status);
        
        if handles.fitsaved
            set(handles.Text_FitSaved,'String',handles.statusstring{2});
            set(handles.Text_FitSaved,'BackgroundColor',handles.statuscolor{2});
        else
            set(handles.Text_FitSaved,'String',handles.statusstring{1});
            set(handles.Text_FitSaved,'BackgroundColor',handles.statuscolor{1});
        end
        
    case 'matchdone'
        
        handles.matchdone = logical(status);
        
        if handles.matchdone
            set(handles.Text_MatchDone,'String',handles.statusstring{2});
            set(handles.Text_MatchDone,'BackgroundColor',handles.statuscolor{2});
        else
            set(handles.Text_MatchDone,'String',handles.statusstring{1});
            set(handles.Text_MatchDone,'BackgroundColor',handles.statuscolor{1});
            set(handles.Text_MatchSaved,'String',handles.statusstring{1});
            set(handles.Text_MatchSaved,'BackgroundColor',handles.statuscolor{1});
            
            handles.matchsaved = false;
        end
        
    case 'matchsaved'
        
         handles.matchsaved = logical(status);
        
        if handles.matchsaved
            set(handles.Text_MatchSaved,'String',handles.statusstring{2});
            set(handles.Text_MatchSaved,'BackgroundColor',handles.statuscolor{2});
        else
            set(handles.Text_MatchSaved,'String',handles.statusstring{1});
            set(handles.Text_MatchSaved,'BackgroundColor',handles.statuscolor{1});
        end
       
end




