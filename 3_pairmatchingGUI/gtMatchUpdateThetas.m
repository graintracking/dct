function handles = gtMatchUpdateThetas(handles)

% Recalculates the theoretical d-spacings and thetas for each phase when
% the lattice parameters have been changed by the user or fitting. Also 
% updates via other functions the used reflections and theta markers in 
% the Eta-Theta plot.

% X-ray beam wavelength in Angstroms 
lambda_true = gtConvEnergyToWavelength(handles.parameters.acq(1).energy);

% Update computed theta values for all phases
for ip = 1:handles.nof_phases
    % 'B matrix': HLK -> Cartesian transformation
    Bmat = gtCrystHKL2CartesianMatrix(handles.LattParsCorrAct(ip,:));
    
    % d-spacings
    dsp_comp   = gtCrystDSpacing(handles.CompPhaseHKLs{ip}, Bmat);
    
    % Bragg angle theta
    handles.CompPhaseThetas{ip} = asind(0.5*lambda_true./dsp_comp);

    % If theta is not between 0 and 90deg, set it to NaN 
    handles.CompPhaseThetas{ip}(handles.CompPhaseThetas{ip}>=90) = NaN;
    handles.CompPhaseThetas{ip}(handles.CompPhaseThetas{ip}<=0) = NaN;
end


% Update reflections
handles = gtMatchUpdateUsedReflections(handles,1);

% Update markers
gtMatchUpdateThetaMarkers(handles,'center');
gtMatchUpdateThetaMarkers(handles,'width');
