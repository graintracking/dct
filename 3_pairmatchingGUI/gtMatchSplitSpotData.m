function handles = gtMatchSplitSpotData(handles)

% Splits difspot data in A and B subsets according to omega, such that
% A contains spots over narrow omega subranges covering omega 0-180deg 
% +- tolerances, and B contains spots on the corresponding subranges 180deg
% offset. These matched subranges have limited number of spots and will be 
% analysed independently in other functions.
%

% Image range for subgroups A:
rangeA = max(ceil(handles.matchpars.thr_max_offset),1);


% Check spot size
sizeok = ((handles.allsp.bbsizeu >= handles.matchpars.minsizeu) & ...
          (handles.allsp.bbsizev >= handles.matchpars.minsizev));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Create spot set A:
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Subset indices:
maxindA = find(handles.allsp.centim < handles.nproj+rangeA,1,'last');
handles.maxindA = maxindA;

handles.allsp.subindA = NaN(length(handles.allsp.ind),1);

% Use floor so if centim=0 -> subindex=1
handles.allsp.subindA(1:maxindA) = floor(handles.allsp.centim(1:maxindA)/rangeA)+1;

% Pair and filtered pair IDs
handles.allsp.pairfiltid = NaN(length(handles.allsp.ind),1);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Create spot B subsets:
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
nof_subranges = max(handles.allsp.subindA(1:maxindA));

% No. of images per 180deg
nproj = handles.nproj;

% Spot field names
fn = fieldnames(handles.allsp);


% For each A set create a subset of B over a small range 180deg offset:
spB = cell(nof_subranges,1);

parfor ii = 1 : nof_subranges
   
    % Range limits for the centroid image of spot B-s:
    %   Centered around rangeA+180deg and 3*rangeA long.
    rangemin = nproj + (ii-2)*rangeA;
    rangemax = nproj + (ii+1)*rangeA;
    
    % Indices in the given range; use <= and < to according to 
    % subindex = floor()+1 above
    inds = find((rangemin <= handles.allsp.centim) & (handles.allsp.centim < rangemax) & sizeok);
   
    
    % Copy data of spots found in the given range into spB
    for j = 1:length(fn)
        spB{ii,1}.(fn{j}) = handles.allsp.(fn{j})(inds);
    end
    
    % Subset indices:
    spB{ii,1}.subindB = repmat(ii,length(inds),1);
   
end

handles.spB = spB;




