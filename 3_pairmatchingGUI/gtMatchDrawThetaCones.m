function pairfighandles = gtMatchDrawThetaCones(pairfighandles,mainhandles,sourcep)

% Called by gtMatchDrawPairs to plot the intersection of diffraction cones 
% emerging from point 'sourcep' with the detector.
%


if (~isempty(pairfighandles.Fullim_ThetaLines) && ishandle(pairfighandles.Fullim_ThetaLines(1)))
    delete(pairfighandles.Fullim_ThetaLines);
end

if ~isempty(pairfighandles.Fullim_ThetaLabels)
    delete(pairfighandles.Fullim_ThetaLabels);
end

pairfighandles.Fullim_ThetaLines = [];
pairfighandles.Fullim_ThetaLabels = [];

% A set of eta angles to plot
etas = (0:0.2:360)';


% Create theta lines
for it = 1:length(mainhandles.UsedThetas)

    % Choose line color according to phase
    phasetype = mainhandles.UsedPhaseTypes(it);
    if isnan(phasetype)
        cline = [1 1 1];
    else
        cline = mainhandles.ThetaLineColor(phasetype,:);
    end
   
    twotheta = 2*mainhandles.UsedThetas(it);
    
    % Diffraction vectors along the range of etas for the given twotheta:
    dvec = cosd(twotheta)*repmat(mainhandles.labgeo.beamdir,length(etas),1) + ...
           sind(twotheta)*cosd(etas)*pairfighandles.etavec0  + ...
           sind(twotheta)*sind(etas)*pairfighandles.etavec90;

    % Projection of point A' onto the detector plane
    projUV = gtGeoPointProjectionsOnDetector(sourcep,dvec,mainhandles.labgeo);

    % Plot line
    pairfighandles.Fullim_ThetaLines(it) = plot(projUV(:,1),projUV(:,2),'-.',...
        'Parent',pairfighandles.Axes_Full,...
        'Color',cline);

    % Create label
    if isnan(mainhandles.UsedPhaseTypes(it))
        labeltext = {'NaN';'\{NaN\}'};
    else
        %labeltext = sprintf('%s\n\{%s\}',mainhandles.PhaseNames{mainhandles.UsedPhaseTypes(it)},...
        %    num2str(mainhandles.UsedHKLs(:,it)'));
        labeltext{1} = mainhandles.PhaseNames{mainhandles.UsedPhaseTypes(it)};
        labeltext{2} = ['\{' num2str(mainhandles.UsedHKLs(:,it)') '\}'];
    end

    % Find first projection on detector
    projind = find(~isnan(projUV(:,1)),1,'last');
    
    % Put hkl label
    if ~isempty(projind)
        if mainhandles.plot3D % plot in 3D
            newhandle = text(projUV(projind,1),projUV(projind,2),1,...
                labeltext,...
                'Parent',pairfighandles.Axes_Full,...
                'Color',cline,...
                'VerticalAlignment','Middle',...
                'HorizontalAlignment','Center');
        else % plot in 2D
            
             newhandle = text(projUV(projind,1),projUV(projind,2),...
                labeltext,...
                'Parent',pairfighandles.Axes_Full,...
                'Color',cline,...
                'VerticalAlignment','Middle',...
                'HorizontalAlignment','Center');
        end
        
        pairfighandles.Fullim_ThetaLabels = ...
            [pairfighandles.Fullim_ThetaLabels, newhandle];

    end

end


% Bring the labels in front of everything else in figure
hchildren       = get(pairfighandles.Axes_Full,'Children');
hind            = ismember(hchildren,pairfighandles.Fullim_ThetaLabels);
hchildren(hind) = [];
hchildren       = [pairfighandles.Fullim_ThetaLabels'; hchildren];
set(pairfighandles.Axes_Full,'Children',hchildren);


% Set visibility according to radio button
if (get(pairfighandles.Radio_ThetaRegions,'Value') == ...
        get(pairfighandles.Radio_ThetaRegions,'Max'))
    set(pairfighandles.Fullim_ThetaLines,'Visible','on')
    set(pairfighandles.Fullim_ThetaLabels,'Visible','on')
else
    set(pairfighandles.Fullim_ThetaLines,'Visible','off')
    set(pairfighandles.Fullim_ThetaLabels,'Visible','off')
end




