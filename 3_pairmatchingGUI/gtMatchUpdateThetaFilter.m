function gtMatchUpdateThetaFilter(handles,op)

% Updates the theta range values in handles.ThetaLimits if changed by the user 
% via the GUI. Calls other functions to update the graphical display of 
% the theta ranges and the pairs in the eta-theta plot that pass the filter.


% Check input
if handles.ThetaLimLastLow >= handles.ThetaLimLastHigh
  return
end


switch op
    
    case 'add'
    % Add new values to array
    handles.ThetaLimits(end+1,1:2) = [handles.ThetaLimLastLow, handles.ThetaLimLastHigh];

    case 'delete'
    if isempty(handles.ThetaLimits)
        return
    end
    % Current index in list
    ind = get(handles.ThetaLims_Listbox,'Value');
    % Delete currently selected limits
    handles.ThetaLimits(ind,:) = [];
    set(handles.ThetaLims_Listbox,'Value',1);

    case 'change'
    % Current index in list
    ind = get(handles.ThetaLims_Listbox,'Value');
    % Change limits
    handles.ThetaLimits(ind,1:2) = [handles.ThetaLimLastLow, handles.ThetaLimLastHigh];

end


% Change string in list box
if isempty(handles.ThetaLimits)
    set(handles.ThetaLims_Listbox,'String','!! None !!');
else
    set(handles.ThetaLims_Listbox,'String',num2str(handles.ThetaLimits));
end


% Update theta limits in figure
handles = gtMatchUpdateThetaFilterDisplay(handles);


% Apply filter to pairs
tmpfilt = false(size(handles.pairs.theta,1),size(handles.ThetaLimits,1));
for ii = 1:size(handles.ThetaLimits,1)
   tmpfilt(:,ii) = ((handles.ThetaLimits(ii,1) <= handles.pairs.theta)...
       & (handles.pairs.theta <= handles.ThetaLimits(ii,2)));
end

handles.pairs.filttheta = any(tmpfilt,2);


% Update eta-theta and text display
handles = gtMatchFilterPairs(handles, 1);


% Update GUI data
guidata(handles.figure1,handles);



