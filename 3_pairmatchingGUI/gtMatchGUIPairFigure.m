function varargout = gtMatchGUIPairFigure(varargin)

% Pair figure GUI main function and callbacks. It is launched by the main GUI.

% GTMATCHGUIPAIRFIGURE MATLAB code for gtMatchGUIPairFigure.fig
%      GTMATCHGUIPAIRFIGURE, by itself, creates a new GTMATCHGUIPAIRFIGURE or raises the existing
%      singleton*.
%
%      H = GTMATCHGUIPAIRFIGURE returns the handle to a new GTMATCHGUIPAIRFIGURE or the handle to
%      the existing singleton*.
%
%      GTMATCHGUIPAIRFIGURE('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in GTMATCHGUIPAIRFIGURE.M with the given input arguments.
%
%      GTMATCHGUIPAIRFIGURE('Property','Value',...) creates a new GTMATCHGUIPAIRFIGURE or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before gtMatchGUIPairFigure_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to gtMatchGUIPairFigure_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help gtMatchGUIPairFigure

% Last Modified by GUIDE v2.5 11-Jun-2012 16:54:44

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @gtMatchGUIPairFigure_OpeningFcn, ...
                   'gui_OutputFcn',  @gtMatchGUIPairFigure_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Opening and output function
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% --- Executes just before gtMatchGUIPairFigure is made visible.
function gtMatchGUIPairFigure_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to gtMatchGUIPairFigure (see VARARGIN)


handles.MainGUI = varargin{1};

handles = gtMatchInitializeGUIPairFigure(handles);

% Set command line output 
% (all handles that are needed for control from the main gui)
handles.output.figure1    = hObject;
handles.output.Axes_Full  = handles.Axes_Full;
handles.output.Axes_SpotA = handles.Axes_SpotA;
handles.output.Axes_SpotB = handles.Axes_SpotB;
handles.output.Push_GO    = handles.Push_Close;
handles.output.Fullim_RotAxis   = handles.Fullim_RotAxis;
handles.output.Fullim_RotAxis3D = handles.Fullim_RotAxis3D;


% Turn on standard toolbar for figure
set(handles.figure1,'Toolbar','figure')

% Set figure name
set(hObject,'Name','Pair figures - Matching GUI')

% Set location in window
set(hObject,'Units','Normalized');
pos    = get(hObject,'Position');
pos(1) = 1-pos(3);
pos(2) = 1-pos(4);
set(hObject,'Position',pos);

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes gtMatchGUIPairFigure wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = gtMatchGUIPairFigure_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Main GUI button
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% --- Executes on button press in Push_Close.
function Push_Close_Callback(hObject, eventdata, handles)
% hObject    handle to Push_Close (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%close(handles.figure1)
delete(handles.figure1)


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Difspot ID
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% --- Executes on button press in Push_NextDifspotID.
function Push_NextDifspotID_Callback(hObject, eventdata, handles)
% hObject    handle to Push_NextDifspotID (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

handles.spotsel.difspotID = handles.spotsel.difspotID + 1;
guidata(hObject,handles);

set(handles.Edit_DifspotID,'String',num2str(handles.spotsel.difspotID))

gtMatchDrawPairs(handles,'difspotID');



% --- Executes on button press in Push_PrevDifspotID.
function Push_PrevDifspotID_Callback(hObject, eventdata, handles)
% hObject    handle to Push_PrevDifspotID (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

handles.spotsel.difspotID = handles.spotsel.difspotID - 1;
guidata(hObject,handles);

set(handles.Edit_DifspotID,'String',num2str(handles.spotsel.difspotID))

gtMatchDrawPairs(handles,'difspotID');



function Edit_DifspotID_Callback(hObject, eventdata, handles)
% hObject    handle to Edit_DifspotID (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of Edit_DifspotID as text
%        str2double(get(hObject,'String')) returns contents of Edit_DifspotID as a double

% Validate input
inp = str2double(get(hObject,'String'));
if isnan(inp) ...            % isdouble returns NaN for non-numbers
        || ~isreal(inp) ...  % should not be complex
        || (inp<0)
    % Give the edit text box focus so user can correct the error
    uicontrol(hObject)
else 
    handles.spotsel.difspotID = round(inp);
    guidata(hObject,handles);
end

gtMatchDrawPairs(handles,'difspotID');



% --- Executes during object creation, after setting all properties.
function Edit_DifspotID_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Edit_DifspotID (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Spot index
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% --- Executes on button press in Push_NextSpotIndex.
function Push_NextSpotIndex_Callback(hObject, eventdata, handles)
% hObject    handle to Push_NextSpotIndex (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

handles.spotsel.spotindex = handles.spotsel.spotindex + 1;
guidata(hObject,handles);

set(handles.Edit_SpotIndex,'String',num2str(handles.spotsel.spotindex))

gtMatchDrawPairs(handles,'spotindex');



% --- Executes on button press in Push_PrevSpotIndex.
function Push_PrevSpotIndex_Callback(hObject, eventdata, handles)
% hObject    handle to Push_PrevSpotIndex (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

handles.spotsel.spotindex = handles.spotsel.spotindex - 1;
guidata(hObject,handles);

set(handles.Edit_SpotIndex,'String',num2str(handles.spotsel.spotindex))

gtMatchDrawPairs(handles,'spotindex');



function Edit_SpotIndex_Callback(hObject, eventdata, handles)
% hObject    handle to Edit_SpotIndex (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of Edit_SpotIndex as text
%        str2double(get(hObject,'String')) returns contents of Edit_SpotIndex as a double

% Validate input
inp = str2double(get(hObject,'String'));
if isnan(inp) ...            % isdouble returns NaN for non-numbers
        || ~isreal(inp) ...  % should not be complex
        || (inp<0)
    % Give the edit text box focus so user can correct the error
    uicontrol(hObject)
else 
    handles.spotsel.spotindex = round(inp);
    guidata(hObject,handles);
end

gtMatchDrawPairs(handles,'spotindex');



% --- Executes during object creation, after setting all properties.
function Edit_SpotIndex_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Edit_SpotIndex (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Pair ID
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% --- Executes on button press in Push_NextPairID.
function Push_NextPairID_Callback(hObject, eventdata, handles)
% hObject    handle to Push_NextPairID (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

handles.spotsel.pairID = handles.spotsel.pairID + 1;
guidata(hObject,handles);

set(handles.Edit_PairID,'String',num2str(handles.spotsel.pairID))

gtMatchDrawPairs(handles,'pairID');



% --- Executes on button press in Push_PrevPairID.
function Push_PrevPairID_Callback(hObject, eventdata, handles)
% hObject    handle to Push_PrevPairID (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

handles.spotsel.pairID = handles.spotsel.pairID - 1;
guidata(hObject,handles);

set(handles.Edit_PairID,'String',num2str(handles.spotsel.pairID))

gtMatchDrawPairs(handles,'pairID');



function Edit_PairID_Callback(hObject, eventdata, handles)
% hObject    handle to Edit_PairID (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of Edit_PairID as text
%        str2double(get(hObject,'String')) returns contents of Edit_PairID as a double

% Validate input
inp = str2double(get(hObject,'String'));
if isnan(inp) ...            % isdouble returns NaN for non-numbers
        || ~isreal(inp) ...  % should not be complex
        || (inp<0)
    % Give the edit text box focus so user can correct the error
    uicontrol(hObject)
else 
    handles.spotsel.pairID = round(inp);
    guidata(hObject,handles);
end

gtMatchDrawPairs(handles,'pairID');



% --- Executes during object creation, after setting all properties.
function Edit_PairID_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Edit_PairID (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Full image
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function Edit_FullImage_Callback(hObject, eventdata, handles)
% hObject    handle to Edit_FullImage (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of Edit_FullImage as text
%        str2double(get(hObject,'String')) returns contents of Edit_FullImage as a double

% Validate input
inp = str2double(get(hObject,'String'));
if isnan(inp) ...            % isdouble returns NaN for non-numbers
        || ~isreal(inp) ...  % should not be complex
        || (inp<0)
    % Give the edit text box focus so user can correct the error
    uicontrol(hObject)
else 
    handles.Fullimno = round(inp);
    guidata(hObject,handles);
end

% Main GUI handles
mh = guidata(handles.MainGUI);

handles.Fullimno = mod(handles.Fullimno,mh.parameters.acq.nproj*2);
   
% Display fullim no.
strcounters    = get(handles.Text_Counters,'String');
strcounters{1} = num2str(handles.Fullimno,'%6.2f');
strcounters{4} = num2str(handles.Fullimno/mh.parameters.acq.nproj*180,'%6.3f');
set(handles.Text_Counters,'String',strcounters);

% Directory of full images
dirfull = sprintf('%s/1_preprocessing/full',mh.parameters.acq.dir);

% Load full image
fullim = edf_read([dirfull, sprintf('/full%0.4d.edf',handles.Fullimno)]);

% Change source image in axes
set(handles.Fullim_Image,'CData',fullim)
    
guidata(hObject,handles);

drawnow


% --- Executes during object creation, after setting all properties.
function Edit_FullImage_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Edit_FullImage (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Push_NextFull_Callback(hObject, eventdata, handles)
% hObject    handle to Push_NextFull (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Main GUI handles
mh = guidata(handles.MainGUI);

% if ((handles.Fullimno >= mh.parameters.acq.nproj*2) || (handles.Fullimno < 0))
%     handles.Fullimno = 0;
% else
%     handles.Fullimno = handles.Fullimno + 1;
% end

handles.Fullimno = handles.Fullimno + 1;
handles.Fullimno = mod(handles.Fullimno,mh.parameters.acq.nproj*2);

set(handles.Edit_FullImage,'String',num2str(handles.Fullimno))


% Display fullim no.
strcounters = get(handles.Text_Counters,'String');
strcounters{1} = num2str(handles.Fullimno,'%6.2f');
strcounters{4} = num2str(handles.Fullimno/mh.parameters.acq.nproj*180,'%6.3f');
set(handles.Text_Counters,'String',strcounters);

% Directory of full images
dirfull = sprintf('%s/1_preprocessing/full',mh.parameters.acq.dir);

% Load full image
fullim = edf_read([dirfull, sprintf('/full%0.4d.edf',handles.Fullimno)]);

% Change source image in axes
set(handles.Fullim_Image,'CData',fullim)
    
guidata(hObject,handles);

drawnow



% --- Executes on button press in Push_PrevFull.
function Push_PrevFull_Callback(hObject, eventdata, handles)
% hObject    handle to Push_PrevFull (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Main GUI handles
mh = guidata(handles.MainGUI);

% if ((handles.Fullimno > mh.parameters.acq.nproj*2) || (handles.Fullimno <= 0))
%     handles.Fullimno = mh.parameters.acq.nproj*2;
% else
%     handles.Fullimno = handles.Fullimno - 1;
% end

handles.Fullimno = handles.Fullimno - 1;
handles.Fullimno = mod(handles.Fullimno,mh.parameters.acq.nproj*2);

set(handles.Edit_FullImage,'String',num2str(handles.Fullimno))

% Display fullim no.
strcounters = get(handles.Text_Counters,'String');
strcounters{1} = num2str(handles.Fullimno,'%6.2f');
strcounters{4} = num2str(handles.Fullimno/mh.parameters.acq.nproj*180,'%6.3f');
set(handles.Text_Counters,'String',strcounters);

% Directory of full images
dirfull = sprintf('%s/1_preprocessing/full',mh.parameters.acq.dir);

% Load full image
fullim = edf_read([dirfull, sprintf('/full%0.4d.edf',handles.Fullimno)]);

% Change source image in axes
set(handles.Fullim_Image,'CData',fullim)
    
guidata(hObject,handles);

drawnow



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Radio buttons 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% --- Executes on button press in Radio_ThetaRegions.
function Radio_ThetaRegions_Callback(hObject, eventdata, handles)
% hObject    handle to Radio_ThetaRegions (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hint: get(hObject,'Value') returns toggle state of Radio_ThetaRegions

if (get(hObject,'Value') == get(hObject,'Max'))
    set(handles.Fullim_ThetaLines,'Visible','on')
    set(handles.Fullim_ThetaLabels,'Visible','on')
else
    set(handles.Fullim_ThetaLines,'Visible','off')
    set(handles.Fullim_ThetaLabels,'Visible','off')
end

drawnow



% --- Executes on button press in Radio_SamEnvProj.
function Radio_SamEnvProj_Callback(hObject, eventdata, handles)
% hObject    handle to Radio_SamEnvProj (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hint: get(hObject,'Value') returns toggle state of Radio_SamEnvProj

if (get(hObject,'Value') == get(hObject,'Max'))
    set(handles.Fullim_SamEnvProj,'Visible','on')
else
    set(handles.Fullim_SamEnvProj,'Visible','off')
end

drawnow



% --- Executes on button press in Radio_Candidates.
function Radio_Candidates_Callback(hObject, eventdata, handles)
% hObject    handle to Radio_Candidates (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hint: get(hObject,'Value') returns toggle state of Radio_Candidates

if (get(hObject,'Value') == get(hObject,'Max'))
    set(handles.Fullim_SpotBRect,'Visible','on')
    set(handles.Fullim_SpotBLabel,'Visible','on')
    set(handles.Fullim_SpotBCent,'Visible','on')
else
    set(handles.Fullim_SpotBRect,'Visible','off')
    set(handles.Fullim_SpotBLabel,'Visible','off')
    set(handles.Fullim_SpotBCent,'Visible','off')
end

drawnow



% --- Executes on button press in Radio_RotAxis.
function Radio_RotAxis_Callback(hObject, eventdata, handles)
% hObject    handle to Radio_RotAxis (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hint: get(hObject,'Value') returns toggle state of Radio_RotAxis

if (get(hObject,'Value') == get(hObject,'Max'))
    set(handles.Fullim_RotAxis,'Visible','on')
    set(handles.Fullim_RotAxis3D,'Visible','on')
else
    set(handles.Fullim_RotAxis,'Visible','off')
    set(handles.Fullim_RotAxis3D,'Visible','off')
end

drawnow



% --- Executes on button press in Radio_SpotA.
function Radio_SpotA_Callback(hObject, eventdata, handles)
% hObject    handle to Radio_SpotA (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hint: get(hObject,'Value') returns toggle state of Radio_SpotA

if (get(hObject,'Value') == get(hObject,'Max'))
    set(handles.Fullim_SpotARect,'Visible','on')
    set(handles.Fullim_SpotALabel,'Visible','on')
    set(handles.Fullim_CentA,'Visible','on')
else
    set(handles.Fullim_SpotARect,'Visible','off')
    set(handles.Fullim_SpotALabel,'Visible','off')
    set(handles.Fullim_CentA,'Visible','off')
end

drawnow



% --- Executes on button press in Radio_ProjLine.
function Radio_ProjLine_Callback(hObject, eventdata, handles)
% hObject    handle to Radio_ProjLine (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hint: get(hObject,'Value') returns toggle state of Radio_ProjLine

allhandles = [handles.Fullim_ProjLine, handles.Fullim_MirrorPoint,...
              handles.Fullim_ProjLine3D, handles.Fullim_CentAmirr];

if (get(hObject,'Value') == get(hObject,'Max'))
    set(allhandles,'Visible','on');
else
    set(allhandles,'Visible','off');
end

drawnow



% --- Executes on button press in Radio_BaseVectors.
function Radio_BaseVectors_Callback(hObject, eventdata, handles)
% hObject    handle to Radio_BaseVectors (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hint: get(hObject,'Value') returns toggle state of Radio_BaseVectors

if (get(hObject,'Value') == get(hObject,'Max'))
    set(handles.Fullim_BaseVectors,'Visible','on');
else
    set(handles.Fullim_BaseVectors,'Visible','off');
end

drawnow



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Color limits
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function Edit_CLimsSpotAUpper_Callback(hObject, eventdata, handles)
% hObject    handle to Edit_CLimsSpotAUpper (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of Edit_CLimsSpotAUpper as text
%        str2double(get(hObject,'String')) returns contents of Edit_CLimsSpotAUpper as a double

inp = str2double(get(hObject,'String'));
if isnan(inp) || ~isreal(inp);    % isdouble returns NaN for non-numbers    
    % Give the edit text box focus so user can correct the error
    uicontrol(hObject)
else 
    handles.CLimsSpotA(2) = inp;
    set(handles.Axes_SpotA,'CLim',handles.CLimsSpotA);
    guidata(hObject,handles);
end



% --- Executes during object creation, after setting all properties.
function Edit_CLimsSpotAUpper_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Edit_CLimsSpotAUpper (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Edit_CLimsSpotALower_Callback(hObject, eventdata, handles)
% hObject    handle to Edit_CLimsSpotALower (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of Edit_CLimsSpotALower as text
%        str2double(get(hObject,'String')) returns contents of Edit_CLimsSpotALower as a double

inp = str2double(get(hObject,'String'));
if isnan(inp) || ~isreal(inp)    % isdouble returns NaN for non-numbers
    % Give the edit text box focus so user can correct the error
    uicontrol(hObject)
else 
    handles.CLimsSpotA(1) = inp;
    set(handles.Axes_SpotA,'CLim',handles.CLimsSpotA);
    guidata(hObject,handles);
end



% --- Executes during object creation, after setting all properties.
function Edit_CLimsSpotALower_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Edit_CLimsSpotALower (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Edit_CLimsCandUpper_Callback(hObject, eventdata, handles)
% hObject    handle to Edit_CLimsCandUpper (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of Edit_CLimsCandUpper as text
%        str2double(get(hObject,'String')) returns contents of Edit_CLimsCandUpper as a double

inp = str2double(get(hObject,'String'));
if isnan(inp) || ~isreal(inp)     % isdouble returns NaN for non-numbers
    % Give the edit text box focus so user can correct the error
    uicontrol(hObject)
else 
    handles.CLimsCand(2) = inp;
    set(handles.Axes_SpotB,'CLim',handles.CLimsCand);
    guidata(hObject,handles);
end



% --- Executes during object creation, after setting all properties.
function Edit_CLimsCandUpper_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Edit_CLimsCandUpper (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Edit_CLimsCandLower_Callback(hObject, eventdata, handles)
% hObject    handle to Edit_CLimsCandLower (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of Edit_CLimsCandLower as text
%        str2double(get(hObject,'String')) returns contents of Edit_CLimsCandLower as a double

inp = str2double(get(hObject,'String'));
if isnan(inp) || ~isreal(inp)      % isdouble returns NaN for non-numbers
    % Give the edit text box focus so user can correct the error
    uicontrol(hObject)
else 
    handles.CLimsCand(1) = inp;
    set(handles.Axes_SpotB,'CLim',handles.CLimsCand);
    guidata(hObject,handles);
end



% --- Executes during object creation, after setting all properties.
function Edit_CLimsCandLower_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Edit_CLimsCandLower (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Edit_CLimsFullUpper_Callback(hObject, eventdata, handles)
% hObject    handle to Edit_CLimsFullUpper (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of Edit_CLimsFullUpper as text
%        str2double(get(hObject,'String')) returns contents of Edit_CLimsFullUpper as a double

inp = str2double(get(hObject,'String'));
if isnan(inp) || ~isreal(inp)     % isdouble returns NaN for non-numbers
    % Give the edit text box focus so user can correct the error
    uicontrol(hObject)
else 
    handles.CLimsFull(2) = inp;
    set(handles.Axes_Full,'CLim',handles.CLimsFull);
    guidata(hObject,handles);
end



% --- Executes during object creation, after setting all properties.
function Edit_CLimsFullUpper_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Edit_CLimsFullUpper (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Edit_CLimsFullLower_Callback(hObject, eventdata, handles)
% hObject    handle to Edit_CLimsFullLower (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of Edit_CLimsFullLower as text
%        str2double(get(hObject,'String')) returns contents of Edit_CLimsFullLower as a double

inp = str2double(get(hObject,'String'));
if isnan(inp) || ~isreal(inp)      % isdouble returns NaN for non-numbers
    % Give the edit text box focus so user can correct the error
    uicontrol(hObject)
else 
    handles.CLimsFull(1) = inp;
    set(handles.Axes_Full,'CLim',handles.CLimsFull);
    guidata(hObject,handles);
end



% --- Executes during object creation, after setting all properties.
function Edit_CLimsFullLower_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Edit_CLimsFullLower (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in Push_MainGUI.
function Push_MainGUI_Callback(hObject, eventdata, handles)
% hObject    handle to Push_MainGUI (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

figure(handles.MainGUI);
