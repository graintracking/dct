function [endpointsuv,endpointsz] = gtMatchRotAxisProjection(labgeo)

% Calculates two endpoints of the projection of the rotation axis onto 
% the detector plane. The endpoints are on the edge of the detector area,
% and can directly be used for plotting.
% If the axis is close to perpendicular to the detector, gives empty.
%
% OUTPUT  endpointsuv - u,v coordinates of end points of the projected axis
%                       (u1 v1; u2 v2)
%         endpointsz  - z coordinate of end points; it is their height above 
%                       the detector plane, scaled with pixelsizeu
%
                                               
% Projection of rot. axis position onto the detector plane (U,V coordinates) 
rotposuv = gtGeoLab2Det(labgeo.rotpos,labgeo,0);

% Projection of rot. axis direction vector onto detector plane
% (i.e. the component parallel with the plane); not normalised
rotdiruv   = gtGeoLab2Det(labgeo.rotdir,labgeo,1);

% If the axis is not too perpendicular to detector, use its projection
if (rotdiruv*rotdiruv' > 1e-10)
    
    % Possible intersection points of projected axis and detector edges:
    ispu1 = rotposuv + (1-rotposuv(1))/rotdiruv(1)*rotdiruv;
    ispu2 = rotposuv + (labgeo.detsizeu-rotposuv(1))/rotdiruv(1)*rotdiruv;
    ispv1 = rotposuv + (1-rotposuv(2))/rotdiruv(2)*rotdiruv;
    ispv2 = rotposuv + (labgeo.detsizev-rotposuv(2))/rotdiruv(2)*rotdiruv;
    
    % Endpoint of projected axis section
    endp = [];
    
    % Check which intersections are inside detector area (normally 2 of them)
    if (1 <= ispu1(2)) && (ispu1(2) <= labgeo.detsizev)
        endp = [endp; ispu1];
    end
    
    if (1 <= ispu2(2)) && (ispu2(2) <= labgeo.detsizev)
        endp = [endp; ispu2];
    end
    
    if (1 <= ispv1(1)) && (ispv1(1) <= labgeo.detsizeu)
        endp = [endp; ispv1];
    end
    
    if (1 <= ispv2(1)) && (ispv2(1) <= labgeo.detsizeu)
        endp = [endp; ispv2];
    end
    
    % Make sure there is not more than two
    if size(endp,1) > 1
        endpointsuv = endp(1:2,:); % U,V
    else
        endpointsuv = [];
    end

else
    endpointsuv = [];
end


% Calculate height of end points relative to detector plane
if (nargout > 1) && ~isempty(endpointsuv)
    
    % Projection of rot. axis position onto detector plane (lab coordinates)
    rotposproj = gtMathsProjectPointsOnPlane(labgeo.rotpos,labgeo.detnorm,labgeo.detrefpos,labgeo.detnorm);
  
    % In-plane end points 3D lab coordinates
    end1plane = gtGeoDet2Lab(endpointsuv(1,:),labgeo,0);
    end2plane = gtGeoDet2Lab(endpointsuv(2,:),labgeo,0);
    
    vecplane1 = (end1plane - rotposproj);
    vecplane2 = (end2plane - rotposproj);

    vecplane1_unit = vecplane1/sqrt(vecplane1*vecplane1');
    vecplane2_unit = vecplane2/sqrt(vecplane2*vecplane2');

    % Real end points 3D lab coordinates
    end1lab = labgeo.rotpos + labgeo.rotdir*(labgeo.rotdir*vecplane1')/...
         (vecplane1_unit*labgeo.rotdir')^2;
    end2lab = labgeo.rotpos + labgeo.rotdir*(labgeo.rotdir*vecplane2')/...
         (vecplane2_unit*labgeo.rotdir')^2;

    % Height above detector plane - scaled with pixel size U.
    % Note that there will be a distortion in this 3D figure compared to 
    % reality, if the pixel sizes are different or detdiru and detdirv are 
    % not orthogonal. It is only for plotting over the detector image.
    endpointsz(1) = (end1lab - labgeo.detrefpos)*(-labgeo.detnorm')/labgeo.pixelsizeu;
    endpointsz(2) = (end2lab - labgeo.detrefpos)*(-labgeo.detnorm')/labgeo.pixelsizeu;

else
    endpointsz = [];
end


