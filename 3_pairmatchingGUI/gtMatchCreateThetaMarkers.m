function handles = gtMatchCreateThetaMarkers(handles)

% Creates the line and rectangle objects in the Theta-Eta plot and Theta
% histogram for all phases and computed reflections.
% These handles are kept until the GUI closes. The position of the markers 
% will be updated in gtMatchUpdateThetaMarkers when needed.

thr_theta       = handles.matchpars.thr_theta;
thr_theta_scale = handles.matchpars.thr_theta_scale;

% Create the graphics objects for each phase

% Loop backwards, so first phase should be in top visible layer
for iph = length(handles.CompPhaseThetas):-1:1

    thetas = handles.CompPhaseThetas{iph};

    for ith = 1:length(thetas)

        pos(1) = thetas(ith) - thr_theta - thetas(ith)*thr_theta_scale;
        pos(2) = 0;
        pos(3) = 2*(thr_theta + thetas(ith)*thr_theta_scale);
        
        % Rectangles in eta-theta plot
        pos(4) = 360;
        
        handles.ThetaRectPlot{iph,ith} = rectangle('Position',pos,...
            'Parent',handles.GUIEtaTheta.Axes_EtaPlot,...
            'Facecolor',handles.ThetaRectColor(iph,:),...
            'Clipping','on','EdgeColor','none');

        % Rectangles in histogram
        pos(4) = 100;
        
        handles.ThetaRectHist{iph,ith} = rectangle('Position',pos,...
            'Parent',handles.GUIEtaTheta.Axes_ThetaHist,...
            'Facecolor',handles.ThetaRectColor(iph,:),...
            'Clipping','on','EdgeColor','none');
    end
end



% Lines at theta values
for iph = length(handles.CompPhaseThetas):-1:1

    thetas = handles.CompPhaseThetas{iph};
   
    for ith = 1:length(thetas)
       
        % Line in eta-theta plot
        handles.ThetaLinePlot{iph,ith} = line([thetas(ith), thetas(ith)],[0 360],...
            'LineStyle','-.','Color',handles.ThetaLineColor(iph,:),...
            'LineWidth',1.5,...
            'Parent',handles.GUIEtaTheta.Axes_EtaPlot);
        
        % {hkl} labels
        handles.HKLLabelsPlot{iph,ith} = text(double(thetas(ith)), 0, ...
            sprintf('%s\n%s',handles.PhaseNames{iph},handles.CompPhaseHKLLabels{iph,ith}), ...
            'HorizontalAlignment','center', ...
            'VerticalAlignment','top', ...
            'Color',handles.ThetaLineColor(iph,:), ...
            'Fontsize', 10, ...
            'Visible','off', ...
            'Parent',handles.GUIEtaTheta.Axes_EtaPlot);       
        
        % Line in histogram
        handles.ThetaLineHist{iph,ith} = line([thetas(ith), thetas(ith)],[0 100],...
            'LineStyle','-.','Color',handles.ThetaLineColor(iph,:),...
            'LineWidth',1.5,...
            'Parent',handles.GUIEtaTheta.Axes_ThetaHist);


       
    end
end


% Bring the eta plotted values in front of the theta markers
hchildren = get(handles.GUIEtaTheta.Axes_EtaPlot,'Children');
hchildren(hchildren==handles.GUIEtaTheta.EtaHandle) = [];
hchildren = [handles.GUIEtaTheta.EtaHandle; hchildren];
set(handles.GUIEtaTheta.Axes_EtaPlot,'Children',hchildren);

% Bring the histogram in front of the theta markers
hchildren = get(handles.GUIEtaTheta.Axes_ThetaHist,'Children');
hchildren(hchildren==handles.GUIEtaTheta.HistHandle) = [];
hchildren = [handles.GUIEtaTheta.HistHandle; hchildren];
set(handles.GUIEtaTheta.Axes_ThetaHist,'Children',hchildren);


drawnow

disp(' ')


