function varargout = gtMATCHGUI(varargin)

%
% FUNCTION varargout = gtMATCHGUI(varargin)
% 
% This is the main GUI to be launched for the Friedel pair matching. It 
% then launches other GUI figures.
% Functions that load data, do the matching and visualization and save 
% results are all called through this GUI.


% GTMATCHGUI MATLAB code for gtMATCHGUI.fig
%      GTMATCHGUI, by itself, creates a new GTMATCHGUI or raises the existing
%      singleton*.
%
%      H = GTMATCHGUI returns the handle to a new GTMATCHGUI or the handle to
%      the existing singleton*.
%
%      GTMATCHGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in GTMATCHGUI.M with the given input arguments.
%
%      GTMATCHGUI('Property','Value',...) creates a new GTMATCHGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before gtMATCHGUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to gtMATCHGUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help gtMATCHGUI

% Last Modified by GUIDE v2.5 30-Apr-2012 16:22:54

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @gtMATCHGUI_OpeningFcn, ...
                   'gui_OutputFcn',  @gtMATCHGUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Opening function
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% --- Executes just before gtMATCHGUI is made visible.
function gtMATCHGUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to gtMATCHGUI (see VARARGIN)
% UIWAIT makes gtMATCHGUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);

% Load data and initialize components and handles
handles = gtMatchInitialize(handles);
guidata(hObject, handles);

% Launch the other GUIs (other 2 separate figures)
% Do not launch pair figure by default, it may be slow. 
handles.GUIPairFig.figure1 = [];
handles.GUIStatistics = gtMatchGUIStatistics;
handles.GUIEtaTheta   = gtMatchGUIEtaVsTheta(hObject);

% Set markers and sort reflections
handles = gtMatchCreateThetaMarkers(handles);
handles = gtMatchUpdateUsedReflections(handles,1);
handles = gtMatchUpdateThetaFilterDisplay(handles);

handles = gtMatchFilterPairs(handles, 1);

% Choose default command line output for gtMATCHGUI
handles.output = hObject;

% Set figure name and position
set(hObject,'Name','Main - Matching GUI')

% Set location in window
set(hObject,'Units','Normalized');
pos    = get(hObject,'Position');
pos(1) = 0;
pos(2) = 1-pos(4);
set(hObject,'Position',pos);

% Update handles structure
guidata(hObject, handles);




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Output and quit functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% --- Outputs from this function are returned to the command line.
function varargout = gtMATCHGUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



% --- Executes on button press in Push_Quit.
function Push_Quit_Callback(hObject, eventdata, handles)
% hObject    handle to Push_Quit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Transfer 'handles' to Matlab base workspace
assignin('base','handles',handles)

% Close other GUI figures
if ishandle(handles.GUIEtaTheta.figure1)
    delete(handles.GUIEtaTheta.figure1);
    %close(handles.GUIEtaTheta.figure1);
end;
if ishandle(handles.GUIPairFig.figure1)
    delete(handles.GUIPairFig.figure1);
    %close(handles.GUIPairFig.figure1);
end;
if ishandle(handles.GUIStatistics.figure1)
    delete(handles.GUIStatistics.figure1);
    %close(handles.GUIStatistics.figure1);
end;
%close(handles.figure1)
delete(handles.figure1)



% --- Executes on button press in Push_ExportHandles.
function Push_ExportHandles_Callback(hObject, eventdata, handles)
% hObject    handle to Push_ExportHandles (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Transfer 'handles' to Matlab base workspace
assignin('base','handles',handles)

set(handles.Text_Progress,'String','Handles exported.')



% --- Executes on button press in Push_SaveResults.
function Push_SaveResults_Callback(hObject, eventdata, handles)
% hObject    handle to Push_SaveResults (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

handles = gtMatchSaveResults(handles);

guidata(hObject,handles);



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Match pairs
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% --- Executes on button press in MatchPairs_Push_PreMatch.
function MatchPairs_Push_PreMatch_Callback(hObject, eventdata, handles)
% hObject    handle to MatchPairs_Push_PreMatch (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

handles.mode ='prematch';
handles.matchpars.thr_theta_used = Inf;

handles = gtMatchLaunchMatching(handles);

guidata(hObject,handles);


% --- Executes on button press in MatchPairs_Push_Match.
function MatchPairs_Push_Match_Callback(hObject, eventdata, handles)
% hObject    handle to MatchPairs_Push_Match (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

handles.mode ='match';
handles.matchpars.thr_theta_used = handles.matchpars.thr_theta;

handles = gtMatchLaunchMatching(handles);

guidata(hObject,handles);



% --- Executes on button press in MatchPairs_Push_StopCont.
function MatchPairs_Push_StopCont_Callback(hObject, eventdata, handles)
% hObject    handle to MatchPairs_Push_StopCont (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if strcmp(get(hObject,'String'),handles.stopcont{1})  % pause
    set(hObject,'String',handles.stopcont{2})
else % continue running                                                 
    set(hObject,'String',handles.stopcont{1})
    set(handles.MatchPairs_Push_Reset,'Enable','off')
    handles = gtMatchUpdateStatus(0,'matchsaved',handles);
    handles = gtMatchFindPairCandidates(handles);
    handles = gtMatchFilterPairs(handles, 1);
    
    guidata(hObject,handles);
end



% --- Executes on button press in MatchPairs_Push_Reset.
function MatchPairs_Push_Reset_Callback(hObject, eventdata, handles)
% hObject    handle to MatchPairs_Push_Reset (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Delete all pairs data
if isfield(handles,'pairs')
    handles = rmfield(handles,'pairs');
end
if isfield(handles,'pairsfilt')
    handles = rmfield(handles,'pairsfilt');
end

handles.dataloaded = false;

% Reset fields needed to run again
handles.pairscand.num = zeros(length(handles.allsp.difspotid),1);
handles.pairscand.ind = zeros(length(handles.allsp.difspotid),1);
handles.pairscand.err = zeros(length(handles.allsp.difspotid),1);

handles.pairsfilt.eta    = NaN;
handles.pairsfilt.theta  = NaN;

% Update Eta-Theta figure
eta   = handles.pairsfilt.eta;
theta = handles.pairsfilt.theta;
refreshdata(handles.GUIEtaTheta.EtaHandle,'caller')

% Compute Theta Histogram, update figure
% thetabins = min(theta):0.01:max(theta);
% histtheta = hist(theta,thetabins);
thetabins = NaN;
histtheta = NaN;
refreshdata(handles.GUIEtaTheta.HistHandle,'caller')

% Reset results display
pairtext{1} = '';
pairtext{2} = '0000'; 
pairtext{3} = '0000'; 
set(handles.Text_PairsResults,'String',pairtext);

pairtext{1} = '00.0%';      
pairtext{2} = '00.0%'; 
pairtext{3} = '00.0%'; 
set(handles.Text_PairsResultsPercent,'String',pairtext);

set(handles.MatchPairs_Push_Reset,'Enable','off');
set(handles.MatchPairs_Push_StopCont,'Enable','off')
set(handles.MatchPairs_Push_PreMatch,'Enable','on')
set(handles.MatchPairs_Push_Match,'Enable','on')

handles = gtMatchUpdateStatus(0,'matchdone',handles);
handles = gtMatchUpdateStatus(0,'matchsaved',handles);

set(handles.Text_Progress,'String','Reset.')

guidata(hObject,handles);



function MatchPairs_Edit_Filter_Callback(hObject, eventdata, handles)
% hObject    handle to MatchPairs_Edit_Filter (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of MatchPairs_Edit_Filter as text
%        str2double(get(hObject,'String')) returns contents of MatchPairs_Edit_Filter as a double

% Validate input
inp = str2double(get(hObject,'String'));
if isnan(inp) ...            % isdouble returns NaN for non-numbers
        || ~isreal(inp) ...  % should not be complex
        || (inp<0)
    % Give the edit text box focus so user can correct the error
    uicontrol(hObject)
else 
    handles.ErrorFilter = inp;
end

handles = gtMatchFilterPairs(handles, 0);

guidata(hObject,handles);



% --- Executes during object creation, after setting all properties.
function MatchPairs_Edit_Filter_CreateFcn(hObject, eventdata, handles)
% hObject    handle to MatchPairs_Edit_Filter (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Theta filter limits
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function ThetaLims_Edit_Low_Callback(hObject, eventdata, handles)
% hObject    handle to ThetaLims_Edit_Low (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of ThetaLims_Edit_Low as text
%        str2double(get(hObject,'String')) returns contents of ThetaLims_Edit_Low as a double

% Validate input
inp = str2double(get(hObject,'String'));
if isnan(inp) ...         % isdouble returns NaN for non-numbers
        || ~isreal(inp)   % should not be complex
    % Disable buttons
    set(handles.ThetaLims_Push_Add, 'Enable','off')
    % Give the edit text box focus so user can correct the error
    uicontrol(hObject)
else 
    handles.ThetaLimLastLow = inp;
    guidata(hObject,handles);
    % Enable buttons
    set(handles.ThetaLims_Push_Add, 'Enable','on')
end



% --- Executes during object creation, after setting all properties.
function ThetaLims_Edit_Low_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ThetaLims_Edit_Low (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function ThetaLims_Edit_High_Callback(hObject, eventdata, handles)
% hObject    handle to ThetaLims_Edit_High (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of ThetaLims_Edit_High as text
%        str2double(get(hObject,'String')) returns contents of ThetaLims_Edit_High as a double

% Validate input
inp = str2double(get(hObject,'String'));
if isnan(inp) ...         % isdouble returns NaN for non-numbers
        || ~isreal(inp)   % should not be complex
    % Disable buttons
    set(handles.ThetaLims_Push_Add, 'Enable','off')
    % Give the edit text box focus so user can correct the error
    uicontrol(hObject)
else 
    handles.ThetaLimLastHigh = inp;
    guidata(hObject,handles);
    % Enable buttons
    set(handles.ThetaLims_Push_Add, 'Enable','on')
end



% --- Executes during object creation, after setting all properties.
function ThetaLims_Edit_High_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ThetaLims_Edit_High (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



% --- Executes on button press in ThetaLims_Push_Add.
function ThetaLims_Push_Add_Callback(hObject, eventdata, handles)
% hObject    handle to ThetaLims_Push_Add (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Check input
if handles.ThetaLimLastLow >= handles.ThetaLimLastHigh
  return
end

% Add new values to array
handles.ThetaLimits(end+1,1:2) = [handles.ThetaLimLastLow, handles.ThetaLimLastHigh];

% Change string in list box
set(handles.ThetaLims_Listbox,'String',num2str(handles.ThetaLimits));

% Update theta limits in figure
handles = gtMatchUpdateThetaFilterDisplay(handles);

guidata(hObject,handles);



% --- Executes on button press in ThetaLims_Push_Delete.
function ThetaLims_Push_Delete_Callback(hObject, eventdata, handles)
% hObject    handle to ThetaLims_Push_Delete (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if isempty(handles.ThetaLimits)
    return
end

% Current index in list
ind = get(handles.ThetaLims_Listbox,'Value'); 

% Delete currently selected limits
handles.ThetaLimits(ind,:) = [];
set(handles.ThetaLims_Listbox,'Value',1); 

% Set string
if isempty(handles.ThetaLimits)
    set(handles.ThetaLims_Listbox,'String','!! None !!');
else
    set(handles.ThetaLims_Listbox,'String',num2str(handles.ThetaLimits));
end

% Update theta limits in figure
handles = gtMatchUpdateThetaFilterDisplay(handles);

guidata(hObject,handles);



% --- Executes on button press in ThetaLims_Push_Change.
function ThetaLims_Push_Change_Callback(hObject, eventdata, handles)
% hObject    handle to ThetaLims_Push_Change (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Check input
if handles.ThetaLimLastLow >= handles.ThetaLimLastHigh
  return
end

% Change values in array
ind = get(handles.ThetaLims_Listbox,'Value');  % current index in list
handles.ThetaLimits(ind,1:2) = [handles.ThetaLimLastLow, handles.ThetaLimLastHigh];

% Change string in list box
set(handles.ThetaLims_Listbox,'String',num2str(handles.ThetaLimits));

% Update theta limits in figure
handles = gtMatchUpdateThetaFilterDisplay(handles);

guidata(hObject,handles);



% --- Executes during object creation, after setting all properties.
function ThetaLims_Listbox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ThetaLims_Listbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Parameters fitting
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% --- Executes on button press in ParsFit_Push_Fit.
function ParsFit_Push_Fit_Callback(hObject, eventdata, handles)
% hObject    handle to ParsFit_Push_Fit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

handles = gtMatchFitParameters(handles);

guidata(hObject,handles);



% --- Executes on button press in ParsFit_Push_Store.
function ParsFit_Push_Store_Callback(hObject, eventdata, handles)
% hObject    handle to ParsFit_Push_Store (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Update detector parameters table
handles.DetParsCorrLast = handles.DetParsCorrAct;
dettable = get(handles.DetPars_Table,'Data');
dettable(:,3) = num2cell(handles.DetParsCorrLast);
set(handles.DetPars_Table, 'Data',dettable);

if isempty(handles.UsedPhases)   
    guidata(hObject,handles);
    return
end

% Active phase
ph = handles.UsedPhases(get(handles.Phases_Listbox_Active,'Value'));

% Update lattice parameters table
handles.LattParsCorrLast(ph,:) = handles.LattParsCorrAct(ph,:);
handles.LatticeParTable(:,3,ph) = num2cell(handles.LattParsCorrLast(ph,:)');
set(handles.LattPars_Table,'Data',handles.LatticeParTable(:,:,ph));

guidata(hObject,handles);



% --- Executes on button press in ParsFit_Push_Reset.
function ParsFit_Push_Reset_Callback(hObject, eventdata, handles)
% hObject    handle to ParsFit_Push_Reset (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
 
% Update detector parameters table
handles.DetParsCorrAct(handles.DetParsTicked) = ...
    handles.DetParsCorrSaved(handles.DetParsTicked);
dettable = get(handles.DetPars_Table,'Data');
dettable(:,4) = num2cell(handles.DetParsCorrAct);
set(handles.DetPars_Table, 'Data',dettable);

% Update labgeo and pairs angles
handles = gtMatchUpdateLabgeo(handles);
handles = gtMatchUpdatePairs(handles, 0);

if isempty(handles.UsedPhases)   
    guidata(hObject,handles);
    return
end

% Active phase
ph = handles.UsedPhases(get(handles.Phases_Listbox_Active,'Value'));

% Update lattice parameters table
handles.LattParsCorrAct(ph,handles.LattParsTicked(ph,:)) = ...
    handles.LattParsCorrSaved(ph,handles.LattParsTicked(ph,:));
handles.LatticeParTable(:,4,ph) = num2cell(handles.LattParsCorrAct(ph,:)');
set(handles.LattPars_Table,'Data',handles.LatticeParTable(:,:,ph));

handles = gtMatchUpdateThetas(handles);

guidata(hObject,handles);



% --- Executes on button press in ParsFit_Push_Restore.
function ParsFit_Push_Restore_Callback(hObject, eventdata, handles)
% hObject    handle to ParsFit_Push_Restore (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Update detector parameters table
handles.DetParsCorrAct(handles.DetParsTicked) = ...
    handles.DetParsCorrLast(handles.DetParsTicked);
dettable = get(handles.DetPars_Table,'Data');
dettable(:,4) = num2cell(handles.DetParsCorrAct);
set(handles.DetPars_Table, 'Data',dettable);

% Update labgeo and pairs angles
handles = gtMatchUpdateLabgeo(handles);
handles = gtMatchUpdatePairs(handles, 0);

if isempty(handles.UsedPhases)   
    guidata(hObject,handles);
    return
end

% Active phase
ph = handles.UsedPhases(get(handles.Phases_Listbox_Active,'Value'));

% Update lattice parameters table
handles.LattParsCorrAct(ph,handles.LattParsTicked(ph,:)) = ...
    handles.LattParsCorrLast(ph,handles.LattParsTicked(ph,:));
handles.LatticeParTable(:,4,ph) = num2cell(handles.LattParsCorrAct(ph,:)');
set(handles.LattPars_Table,'Data',handles.LatticeParTable(:,:,ph));

handles = gtMatchUpdateThetas(handles);

guidata(hObject,handles);



% --- Executes on button press in ParsFit_Push_Save.
function ParsFit_Push_Save_Callback(hObject, eventdata, handles)
% hObject    handle to ParsFit_Push_Save (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

handles = gtMatchSaveFitParameters(handles);

guidata(hObject,handles);



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Detector parameters table
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% --- Executes when entered data in editable cell(s) in DetPars_Table.
function DetPars_Table_CellEditCallback(hObject, eventdata, handles)
% hObject    handle to DetPars_Table (see GCBO)
% eventdata  structure with the following fields (see UITABLE)
%	Indices: row and column indices of the cell(s) edited
%	PreviousData: previous data for the cell(s) edited
%	EditData: string(s) entered by the user
%	NewData: EditData or its converted form set on the Data property. Empty if Data was not changed
%	Error: error string when failed to convert EditData to appropriate value for Data
% handles    structure with handles and user data (see GUIDATA)

% Check if data is loaded or fresh
if handles.dataloaded
    set(handles.Text_Progress,'String','Cannot update loaded data.')
    dettable = get(handles.DetPars_Table,'Data');
    dettable{eventdata.Indices(1),eventdata.Indices(2)} = eventdata.PreviousData;
    dettable(:,[5,6]) = {false};
    set(handles.DetPars_Table,'Data',dettable);
    return
end


if eventdata.Indices(2) == 2
    handles.DetParsTicked(eventdata.Indices(1)) = eventdata.NewData;

elseif eventdata.Indices(2) == 4
    handles.DetParsCorrAct(eventdata.Indices(1)) = eventdata.NewData;
    % Update labgeo
    handles = gtMatchUpdateLabgeo(handles);
    handles = gtMatchUpdatePairs(handles, 0);

elseif eventdata.Indices(2) == 5
    handles = gtMatchModifyDetPars(handles,eventdata.Indices(1),'+');

elseif eventdata.Indices(2) == 6
    handles = gtMatchModifyDetPars(handles,eventdata.Indices(1),'-');

end

handles = gtMatchUpdateStatus(0,'matchdone',handles);
handles = gtMatchUpdateStatus(0,'fitdone',handles);

guidata(hObject,handles);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Lattice parameters table
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% --- Executes when entered data in editable cell(s) in LattPars_Table.
function LattPars_Table_CellEditCallback(hObject, eventdata, handles)
% hObject    handle to LattPars_Table (see GCBO)
% eventdata  structure with the following fields (see UITABLE)
%	Indices: row and column indices of the cell(s) edited
%	PreviousData: previous data for the cell(s) edited
%	EditData: string(s) entered by the user
%	NewData: EditData or its converted form set on the Data property. Empty if Data was not changed
%	Error: error string when failed to convert EditData to appropriate value for Data
% handles    structure with handles and user data (see GUIDATA)

if isempty(handles.UsedPhases)
    return
end

% Active phase
ph = handles.UsedPhases(get(handles.Phases_Listbox_Active,'Value'));

% Crystal system
cryst.crystal_system  = handles.parameters.cryst(ph).crystal_system;
cryst.hermann_mauguin = handles.parameters.cryst(ph).hermann_mauguin;

% If a parameter has been (un)ticked
if eventdata.Indices(2) == 2

    % Get dependencies of lattice parameters for the actual crystal system
    [fitpar,equalpars] = gtMatchLatticeParDependencies(eventdata.Indices(1),cryst);
    handles.LattParsTicked(ph,equalpars) = eventdata.NewData;
    
    % Update table
    handles.LatticeParTable(:,2,ph) = num2cell(handles.LattParsTicked(ph,:)');
    set(handles.LattPars_Table,'Data',handles.LatticeParTable(:,:,ph));

    % If ticked add parameter to list; if unticked, remove it 
    if eventdata.NewData
        handles.LattParsToFit(ph,fitpar) = true;
    else
        handles.LattParsToFit(ph,fitpar) = false;
    end

% Elseif a parameter value has been changed
elseif eventdata.Indices(2) == 4
  
    % Get dependencies of lattice parameters for the actual crystal system
    [~,equalpars] = gtMatchLatticeParDependencies(eventdata.Indices(1),cryst);
    handles.LattParsCorrAct(ph,equalpars) = eventdata.NewData;
    
    % Update table
    handles.LatticeParTable(:,4,ph) = num2cell(handles.LattParsCorrAct(ph,:)');
    set(handles.LattPars_Table,'Data',handles.LatticeParTable(:,:,ph));

    handles = gtMatchUpdateThetas(handles);

% If a value incremented
elseif eventdata.Indices(2) == 5
    handles = gtMatchModifyLattPars(handles,eventdata.Indices(1),'+');

% If a value decremented
elseif eventdata.Indices(2) == 6
    handles = gtMatchModifyLattPars(handles,eventdata.Indices(1),'-');

end

handles = gtMatchUpdateStatus(0,'matchdone',handles);
handles = gtMatchUpdateStatus(0,'fitdone',handles);
guidata(hObject,handles);





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Phase selection
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% --- Executes on selection change in Phases_Listbox_Select.
function Phases_Listbox_Select_Callback(hObject, eventdata, handles)
% hObject    handle to Phases_Listbox_Select (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: contents = cellstr(get(hObject,'String')) returns Phases_Listbox_Select contents as cell array
%        contents{get(hObject,'Value')} returns selected item from Phases_Listbox_Select



% --- Executes during object creation, after setting all properties.
function Phases_Listbox_Select_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Phases_Listbox_Select (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



% --- Executes on button press in Phases_Push_Add.
function Phases_Push_Add_Callback(hObject, eventdata, handles)
% hObject    handle to Phases_Push_Add (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

ind = get(handles.Phases_Listbox_Select,'Value');

% Add the selected phase(s)
handles.UsedPhases = union(handles.UsedPhases,ind);

phaselist = handles.PhaseNames(handles.UsedPhases);

set(handles.Phases_Listbox_Active,'String',phaselist);
set(handles.Phases_Listbox_Active,'Max',length(handles.UsedPhases));

handles = gtMatchUpdateUsedReflections(handles,1);

guidata(hObject,handles);



% --- Executes on button press in Phases_Push_Delete.
function Phases_Push_Delete_Callback(hObject, eventdata, handles)
% hObject    handle to Phases_Push_Delete (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if isempty(handles.UsedPhases)
    return
end
    
ind = get(handles.Phases_Listbox_Active,'Value');

% Remove the selected phase(s)
handles.UsedPhases(ind) = [];

if isempty(handles.UsedPhases)
    set(handles.Phases_Listbox_Active,'String','!! None !!');
else
    phaselist = handles.PhaseNames(handles.UsedPhases);
    set(handles.Phases_Listbox_Active,'String',phaselist);
end

set(handles.Phases_Listbox_Active,'Value',1);
set(handles.Phases_Listbox_Active,'Max',length(handles.UsedPhases));

handles = gtMatchUpdateUsedReflections(handles,1);

guidata(hObject,handles);



% --- Executes on selection change in Phases_Listbox_Active.
function Phases_Listbox_Active_Callback(hObject, eventdata, handles)
% hObject    handle to Phases_Listbox_Active (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: contents = cellstr(get(hObject,'String')) returns Phases_Listbox_Active contents as cell array
%        contents{get(hObject,'Value')} returns selected item from Phases_Listbox_Active

if isempty(handles.UsedPhases)
    tablesize = size(handles.LatticeParTable(:,:,1));
    set(handles.LattPars_Table,'Data',cell(tablesize));
else
    ph = handles.UsedPhases(get(handles.Phases_Listbox_Active,'Value'));
    set(handles.LattPars_Table,'Data',handles.LatticeParTable(:,:,ph));
end



% --- Executes during object creation, after setting all properties.
function Phases_Listbox_Active_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Phases_Listbox_Active (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Plots
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% --- Executes on selection change in Plot_Listbox.
function Plot_Listbox_Callback(hObject, eventdata, handles)
% hObject    handle to Plot_Listbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: contents = cellstr(get(hObject,'String')) returns Plot_Listbox contents as cell array
%        contents{get(hObject,'Value')} returns selected item from Plot_Listbox

val = get(hObject,'Value');

% Set current figure
switch val
    case 1
        figure(handles.GUIEtaTheta.figure1);
    case 2
        figure(handles.GUIStatistics.figure1);
    case 3
        if ishandle(handles.GUIPairFig.figure1)
            figure(handles.GUIPairFig.figure1);
        else
            % Launch pair figure
            handles.GUIPairFig = gtMatchGUIPairFigure(handles.figure1);
            figure(handles.GUIPairFig.figure1);
            
            % Update handles structure
            guidata(hObject, handles);
        end
end



% --- Executes during object creation, after setting all properties.
function Plot_Listbox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Plot_Listbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



% --- Executes on button press in Push_ResetFigs.
function Push_ResetFigs_Callback(hObject, eventdata, handles)
% hObject    handle to Push_ResetFigs (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Close other GUI figures
if ishandle(handles.GUIEtaTheta.figure1)
   close(handles.GUIEtaTheta.figure1);
end;
if ishandle(handles.GUIPairFig.figure1)
    close(handles.GUIPairFig.figure1);
end;
if ishandle(handles.GUIStatistics.figure1)
    close(handles.GUIStatistics.figure1);
end;

% Relaunch the three GUIs
handles.GUIStatistics = gtMatchGUIStatistics;
handles.GUIEtaTheta   = gtMatchGUIEtaVsTheta(hObject);

% Update figures
handles = gtMatchCreateThetaMarkers(handles);
handles = gtMatchUpdateUsedReflections(handles,1);
handles = gtMatchUpdateThetaFilterDisplay(handles);

gtMatchUpdateStatistics(handles);
handles = gtMatchFilterPairs(handles, 1);

% Update handles structure
guidata(hObject, handles);

figure(handles.figure1)



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Matching parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% --- Executes when entered data in editable cell(s) in MatchParams_Table.
function MatchParams_Table_CellEditCallback(hObject, eventdata, handles)
% hObject    handle to MatchParams_Table (see GCBO)
% eventdata  structure with the following fields (see UITABLE)
%	Indices: row and column indices of the cell(s) edited
%	PreviousData: previous data for the cell(s) edited
%	EditData: string(s) entered by the user
%	NewData: EditData or its converted form set on the Data property. Empty if Data was not changed
%	Error: error string when failed to convert EditData to appropriate value for Data
% handles    structure with handles and user data (see GUIDATA)

if strcmp('uniquetheta', handles.matchparnames{eventdata.Indices(1)})
    mptable = get(hObject,'Data');
    mptable{eventdata.Indices(1),eventdata.Indices(2)} = eventdata.PreviousData;
    set(hObject,'Data',mptable);
    helpdlg('Parameter ''uniquetheta'' can only be set before laucnhing the GUI.')
    return
end

handles.matchpars.(handles.matchparnames{eventdata.Indices(1)}) = eventdata.NewData;

% Update theta regions display in figure, if needed
if strcmp('thr_theta',handles.matchparnames{eventdata.Indices(1)}) || ...
   strcmp('thr_theta_scale',handles.matchparnames{eventdata.Indices(1)})
    gtMatchUpdateThetaMarkers(handles,'width');
end

handles = gtMatchUpdateStatus(0,'matchdone',handles);

guidata(hObject,handles);



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Reflections table
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% --- Executes when entered data in editable cell(s) in Theta_Table.
function Theta_Table_CellEditCallback(hObject, eventdata, handles)
% hObject    handle to Theta_Table (see GCBO)
% eventdata  structure with the following fields (see UITABLE)
%	Indices: row and column indices of the cell(s) edited
%	PreviousData: previous data for the cell(s) edited
%	EditData: string(s) entered by the user
%	NewData: EditData or its converted form set on the Data property. Empty if Data was not changed
%	Error: error string when failed to convert EditData to appropriate value for Data
% handles    structure with handles and user data (see GUIDATA)

% Phase and theta indices
phaseind = handles.SortedPhaseTypes(eventdata.Indices(1));
thetaind = handles.SortedThetaIndices(eventdata.Indices(1));

handles.CompThetasTicked{phaseind}(thetaind) = eventdata.NewData;

% % Update active reflections
% handles = gtMatchUpdateUsedReflections(handles,0);

% Update theta display in Eta plots
gtMatchUpdateThetaMarkers(handles,'select');

handles = gtMatchUpdateStatus(0,'matchdone',handles);

guidata(hObject,handles);



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Not Used
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% --- Executes on selection change in ThetaLims_Listbox.
function ThetaLims_Listbox_Callback(hObject, eventdata, handles)
% hObject    handle to ThetaLims_Listbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: contents = cellstr(get(hObject,'String')) returns ThetaLims_Listbox contents as cell array
%        contents{get(hObject,'Value')} returns selected item from ThetaLims_Listbox
