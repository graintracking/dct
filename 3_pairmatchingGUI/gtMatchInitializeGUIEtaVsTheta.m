function handles = gtMatchInitializeGUIEtaVsTheta(handles)

% Sets initial graphic objects and figure properties for the Eta-Theta GUI.

% Turn on standard toolbar for figure
set(handles.figure1,'Toolbar','figure')

set(handles.Radio_ThetaRanges,'Value',1);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Theta filter rectangles
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Clearing axes is needed, otherwise previous plots may appear, even from
% already deleted figures.
cla(handles.Axes_ThetaLimits);

set(handles.Axes_ThetaLimits,'XTick',[]);
set(handles.Axes_ThetaLimits,'YTick',[]);
set(handles.Axes_ThetaLimits,'Color','r');
%hold(handles.Axes_ThetaLimits,'on');

hp = pan;
setAllowAxesPan(hp,handles.Axes_ThetaLimits,false);

set(handles.Axes_ThetaLimits,'XLimMode','auto');
set(handles.Axes_ThetaLimits,'YLimMode','manual');
set(handles.Axes_ThetaLimits,'YLim',[0 1]);



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Eta-Theta plot
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

cla(handles.Axes_EtaPlot);

set(handles.Axes_EtaPlot,'XLimMode','auto');
set(handles.Axes_EtaPlot,'YLimMode','auto');

% Generate a lineseries for eta plot:
handles.EtaHandle = plot(handles.Axes_EtaPlot,NaN,NaN,'d',...
                    'Markersize',4, ...
                    'MarkerFaceColor','k','MarkerEdgeColor','k');

% Link data and set plotted data names
set(handles.EtaHandle,'xdatasource','theta');
set(handles.EtaHandle,'ydatasource','eta');
     


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Theta histogram
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

cla(handles.Axes_ThetaHist);

set(handles.Axes_ThetaHist,'XLimMode','auto');
set(handles.Axes_ThetaHist,'YLimMode','auto');
%hold(handles.Axes_ThetaHist,'on');

% Create bar plot for histogram
handles.HistHandle = bar(handles.Axes_ThetaHist,NaN,NaN,'FaceColor','b',...
                     'EdgeColor','b');

% Link data and set plotted data names
set(handles.HistHandle,'xdatasource','thetabins');
set(handles.HistHandle,'ydatasource','histtheta');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Plot options
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Link the x axes of the plots (always show same theta range)
linkaxes([handles.Axes_EtaPlot, handles.Axes_ThetaHist, handles.Axes_ThetaLimits],'x');
            
set(handles.Axes_EtaPlot,'XLim',[0 90]);
set(handles.Axes_EtaPlot,'YLim',[0 360]);
set(handles.Axes_ThetaHist,'YLim',[0 100]);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Interruptible
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Set every callack non-interruptible
allh = get(handles.figure1,'Children');
set(allh,'Interruptible','off');



