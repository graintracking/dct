% 
% FUNCTION [theta,theta_diff,thetaind,theta_OK,diffvec] = gtMatchIdentifyTheta(...
%             centAUV,centBUV,labgeo,compthetas,thr_theta,thr_theta_scale)
%
% Identifies theta index (i.e. the plane family and phase) as the nearest 
% theta value and checks if its deviation is in the given tolerance range.
%

function [theta,theta_diff,thetaind,theta_OK,diffvec] = gtMatchIdentifyTheta(...
             centAUV,centBUV,labgeo,compthetas,thr_theta,thr_theta_scale, shiftA, shiftB)

         
% Diffraction vector and theta
[diffvec,~,theta] = gtGeoDiffVecInLabPair(centAUV,centBUV,labgeo, shiftA, shiftB);

% Discrepancy in theta
diff = theta - compthetas;

if any(abs(diff) <= thr_theta + compthetas*thr_theta_scale)
    theta_OK = true ;
    [~,thetaind] = min(abs(diff));
    theta_diff = diff(thetaind);
else
    theta_diff = [];
    thetaind   = [];
    theta_OK   = false ;
end
