function handles = gtMatchFitParameters(handles)

% Does the detector and lattice parameter fitting simultaneously for all
% the specified parameters using the reflections marked by the user. It uses 
% Matlab's built-in 'lsqnonlin' algorithm.
%
% The initial labgeo parameters used in the fitting are loaded from the
% parameters file into handles.fitting.labgeobase when the GUI is launched.
% These values are then corrected by finding the best fitting parameters.
% Some fitting parameters are directly listed in labgeobase and some are 
% rotations relative to labgeobase. As an initial guess for the fitting, 
% the latest corrections are applied to labgeobase.
% To change labgeobase to the latest (saved) version, the GUI needs 
% restarting. In this case the rotations will be reset to 0.

% Check available Optimization Toolbox License
ok = license('checkout','Optimization_Toolbox');
if ~ok
    set(handles.Text_Progress,'String','Toolbox license problem.')
    return
end

% Data loaded or fresh
if handles.dataloaded
    set(handles.Text_Progress,'String','Cannot fit loaded data.')
    return
end


% Active phase
if isempty(handles.UsedPhases)
    set(handles.Text_Progress,'String','No phases to fit.')
    return
else
    actphase = handles.UsedPhases(get(handles.Phases_Listbox_Active,'Value'));
end

% If no parameters to be fitted, return
if ~(any(handles.DetParsTicked) || any(handles.LattParsToFit(actphase,:)))
    return
elseif isnan(handles.pairsfilt.eta(1))
    return
end

gtMatchSetBusy(handles,'on');
set(handles.Text_Progress,'String','Fitting...')

handles = gtMatchUpdateStatus(0,'matchdone',handles);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Select pairs for fitting
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Update active reflections (reflections may have been (un)ticked)
handles = gtMatchUpdateUsedReflections(handles,0);

% Fitting is based on filtered pairs of which phasetypes are defined
%  (it may belong to more than one phase)
usepair = false(length(handles.pairsfilt.phasetype),1);
for ii = 1:length(handles.pairsfilt.phasetype)
    if ((~isnan(handles.pairsfilt.phasetype(ii))) && (handles.pairsfilt.phasetype(ii)==actphase))
        usepair(ii) = handles.CompThetasTicked{handles.pairsfilt.phasetype(ii)}...
                      (handles.pairsfilt.sortedthetaind(ii));
    end
end


% If nothing left
if ~any(usepair)
    set(handles.Text_Progress,'String','No valid pairs to fit.')
    gtMatchSetBusy(handles,'off');
    return
end

% Crude option to equalize 'weight' between different hkl families
% if activated, we pick the same number of pairs for each family
if handles.fitting.equalize_hkls
    tt = unique(handles.pairsfilt.thetatype(usepair));
    for j = 1 : numel(tt)
      ind_tt(:,j) = handles.pairsfilt.thetatype == tt(j);
      ind_tt(:,j) = ind_tt(:,j) & usepair;
      num_tt(j)   =   numel(find(ind_tt(:,j)));
    end

    min_num = min(num_tt);

    for j=1: numel(tt)
       ind = find(ind_tt(:,j));
       last = ind(min_num);
       ind_tt(last+1:end,j) = false;
    end
    usepair = any(ind_tt,2);
end


pairs.centAU  = handles.pairsfilt.centAU(usepair);
pairs.centAV  = handles.pairsfilt.centAV(usepair);
pairs.centBU  = handles.pairsfilt.centBU(usepair);
pairs.centBV  = handles.pairsfilt.centBV(usepair);
pairs.shiftA  = handles.pairsfilt.shiftA(usepair,:);
pairs.shiftB  = handles.pairsfilt.shiftB(usepair,:);
pairs.hkl     = handles.pairsfilt.hkl(usepair,:);





% Show shortly the pairs to fit in the Eta plot
eta   = handles.pairsfilt.eta(usepair);
theta = handles.pairsfilt.theta(usepair);
hold(handles.GUIEtaTheta.Axes_EtaPlot,'on')
hfitplot = plot(handles.GUIEtaTheta.Axes_EtaPlot,theta,eta,'ro');
drawnow


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Fitted parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Order of fitted parameters in newvars and oldvars:
%
% 1  = 'Position X';
% 2  = 'Position Y';
% 3  = 'Position Z';
% 4  = 'Tilt in plane';
% 5  = 'Angle U-V';
% 6  = 'Tilt around U';
% 7  = 'Tilt around V';
% 8  = 'Pixel size mean';
% 9  = 'Pixel size ratio';
% 10 = 'lengtha';
% 12 = 'lengthb';
% 13 = 'lengthc';
% 14 = 'alpha';
% 15 = 'beta';
% 16 = 'gamma';

% Fitted variables indices
newvarsind = [handles.DetParsTicked, handles.LattParsToFit(actphase,:)];

% Old variables: the starting point for fitting
oldvars = [handles.DetParsCorrAct, handles.LattParsCorrAct(actphase,:)];

% Guess for fitted variables 
varsguess = oldvars(newvarsind);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Constants
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Labgeo base
labgeobase = handles.fitting.labgeobase;

% Beam wavelength in Angstrom
lambda_true = gtConvEnergyToWavelength(handles.parameters.acq(1).energy);

% Is lambda defined or use the mean value in the fitting ('defined' or 'mean')? 
lambdamode = 'defined';

% Crystal system
cryst.crystal_system  = handles.parameters.cryst(actphase).crystal_system;
cryst.hermann_mauguin = handles.parameters.cryst(actphase).hermann_mauguin;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Lower and upper boundaries for fitting
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Detector ref. position; based on detector size
allboundslow(1:3)  = handles.DetParsCorrAct(1:3) - ...
                     handles.labgeo.detsizeu*...
                     handles.labgeo.pixelsizeu;
allboundshigh(1:3) = handles.DetParsCorrAct(1:3) + ...
                     handles.labgeo.detsizeu*...
                     handles.labgeo.pixelsizeu;

% Detector vector rotations in degrees;
allboundslow([4,6,7])  = -30;
allboundshigh([4,6,7]) =  30;

% Detector vectors U-V angle in degrees
allboundslow(5)  = 60;
allboundshigh(5) = 120;

% Pixel sizes
allboundslow(8)  = handles.DetParsCorrAct(8)*0.5;
allboundslow(9)  = handles.DetParsCorrAct(9)*0.5;
allboundshigh(8) = handles.DetParsCorrAct(8)*2;
allboundshigh(9) = handles.DetParsCorrAct(9)*2;

% Lattice parameters
allboundslow(10:12)  = handles.LattParsCorrAct(actphase,[1,2,3])*0.5;
allboundshigh(10:12) = handles.LattParsCorrAct(actphase,[1,2,3])*2;
allboundslow([13,14,15]) = 0;
allboundslow([13,14,15]) = 180;

% Select the active ones
boundslow  =  allboundslow(newvarsind);
boundshigh = allboundshigh(newvarsind);



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Run fitting
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp('PARAMETERS FITTING')

% Using Matlab's built-in non-linear least square solver. 
[varsopt,resnorm,residual,exitflag,optimout,lagrange,jacobian] = lsqnonlin(...
    @(newvars) sfDeviation(newvars,newvarsind,oldvars,labgeobase,cryst,...
    lambdamode,lambda_true,pairs),...
    varsguess, boundslow, boundshigh,...
    optimset('TolX',handles.fitting.tolx, 'TolFun',handles.fitting.tolfun, ...
    'DiffMinChange',handles.fitting.derstep, 'DiffMaxChange',handles.fitting.derstep, ...
    'FunValCheck','off', 'Display','iter'));
    %'TypicalX',handles.fitting.typicalx(newvarsind),'FunValCheck','off', 'Display','iter'));

% if any(exitflag==[1,2,3,4])
%     disp(' ')
%     disp(optimout.message)
%     disp('An optimimum for correction has been found.')
%     disp(' ')
% else
%     disp(' ')
%     disp(optimout.message)
%     disp('No optimized values for correction were found.')
%     disp(' ')
% end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Create output; update
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Delete markers on pairs
delete(hfitplot)
drawnow

% Detector parameters
handles.DetParsCorrAct(newvarsind(1:9)) = varsopt(1:sum(newvarsind(1:9)));
handles = gtMatchUpdateLabgeo(handles);


% Lattice parameters

changepars = find(newvarsind(10:15));
for ii = 1:length(changepars)
    % Get dependencies of lattice parameters for the actual crystal system
    [~,equalpars] = gtMatchLatticeParDependencies(changepars(ii),cryst);
    % Set all dependent parameters
    handles.LattParsCorrAct(actphase,equalpars) =  varsopt(sum(newvarsind(1:9))+ii);
end


% Update thetas
handles = gtMatchUpdateThetas(handles);

% Update pairs 
handles = gtMatchUpdatePairs(handles, 0);
gtMatchSetBusy(handles,'on');


handles.fitting.labgeo     = handles.labgeo;
handles.fitting.phasename  = handles.PhaseNames{actphase};
handles.fitting.latticepar = handles.LattParsCorrAct(actphase,:);
handles.fitting.optimout = optimout;
handles.fitting.resnorm  = resnorm;
handles.fitting.residual = residual;
handles.fitting.exitflag = exitflag;
handles.fitting.lagrange = lagrange;
handles.fitting.jacobian = jacobian;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Update tables
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Detector parameters
dettable = get(handles.DetPars_Table,'Data');
dettable(:,3) = num2cell(handles.DetParsCorrLast');
dettable(:,4) = num2cell(handles.DetParsCorrAct');
set(handles.DetPars_Table,'Data',dettable);

% Lattice parameters
handles.LatticeParTable(:,3,actphase) = num2cell(handles.LattParsCorrLast(actphase,:)');
handles.LatticeParTable(:,4,actphase) = num2cell(handles.LattParsCorrAct(actphase,:)');
set(handles.LattPars_Table,'Data',handles.LatticeParTable(:,:,actphase));

handles = gtMatchUpdateStatus(0,'fitsaved',handles);
gtMatchSetBusy(handles,'off');

if exitflag > 0
    handles = gtMatchUpdateStatus(1,'fitdone',handles);
   set(handles.Text_Progress,'String','Fitting successful.')
else
    handles = gtMatchUpdateStatus(0,'fitdone',handles);
    set(handles.Text_Progress,'String','Fitting failed.')
end


end % of main function


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Sub-functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function devs = sfDeviation(newvars,newvarsind,oldvars,labgeobase,cryst,...
    lambdamode,lambda_true,pairs)

% Change labgeo and lattice parameters according to the new active variables
[labgeo,latticepar] = gtMatchApplyFitParams(newvars,newvarsind,oldvars,...
                               labgeobase,cryst);

% Bragg angles theta via the diffraction vector
[~,~,theta] = gtGeoDiffVecInLabPair([pairs.centAU,pairs.centAV], ...
                                    [pairs.centBU,pairs.centBV], labgeo, pairs.shiftA, pairs.shiftB);

% 'B matrix': HKL -> Cartesian transformation
Bmat = gtCrystHKL2CartesianMatrix(latticepar);
                                
% d-spacings
dsp = gtCrystDSpacing(pairs.hkl', Bmat)';

% lambda-s from Friedel pairs should ideally be equal to lambda_input (Bragg's law)
lambda = 2*dsp.*sind(theta);

% Value to be minimized (a vector required for lsqnonlin)
switch lambdamode
    case 'defined' % Deviation of lambda from input value is to be minimized
        devs = lambda-lambda_true;
    case 'mean'	   % Standard deviation of lambda is to be minimized (doesn't use input value)
        devs = lambda-mean(lambda);
end

end % of sfDeviation


