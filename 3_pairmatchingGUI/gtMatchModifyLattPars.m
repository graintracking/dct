function handles = gtMatchModifyLattPars(handles,ind,incsign)

% It is called when the lattice parameters are incremented via the table 
% in the main GUI. It applies the increment and by calling other functions 
% updates the lattice parameters, the d-spacings and thetas of the active 
% phase, and updates the theta markers in the plots.


inc = handles.LattParsIncrement(ind);

if incsign=='+';
elseif incsign=='-';
    inc = -inc;
else
    return
end

if isempty(handles.UsedPhases)
    return
end

% Active phase
ph = handles.UsedPhases(get(handles.Phases_Listbox_Active,'Value'));

% Crystal system
cryst.crystal_system  = handles.parameters.cryst(ph).crystal_system;
cryst.hermann_mauguin = handles.parameters.cryst(ph).hermann_mauguin;

% Get dependencies of lattice parameters for the actual crystal system
[~,equalpars] = gtMatchLatticeParDependencies(ind,cryst);
handles.LattParsCorrAct(ph,equalpars) = handles.LattParsCorrAct(ph,equalpars)...
                                        + inc;

% Update table
handles.LatticeParTable(:,4,ph) = num2cell(handles.LattParsCorrAct(ph,:)');
set(handles.LattPars_Table,'Data',handles.LatticeParTable(:,:,ph));

handles = gtMatchUpdateThetas(handles);



