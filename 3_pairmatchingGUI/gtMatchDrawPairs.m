function gtMatchDrawPairs(handles,inptype)

% The main plotting function for the pair figure. It plots the full image,
% the difspots, their candidate pairs and all the geometric features.

% Notation used in the function: 
% Spot A - a difspot in the 1st half of the scan (omega 0 to 180deg)
% Spot B - the selected Friedel pair of A in 2nd half of set
% Spot C - candidates of possible Friedel pairs of A
%
% Three different indices are used to refer to difspots:
%  - difspotID: as in difspot table
%  - spot index: index in the list of all spots ordered by omega
%    (only used intrenally in the matching process)
%  - pair index: index of matched but not filtered pairs
%    (the output is a restricted set, the filtered pairs)

% Constant gap for spot B labels in tile image
labelgapC = 10; % pixels


mainhandles = guidata(handles.MainGUI);
allsp = mainhandles.allsp;


if (~isfield(mainhandles,'pairs') && ~strcmp(inptype,'difspotID'))
    return
elseif (~isfield(mainhandles,'pairs') && strcmp(inptype,'difspotID'))
    pairs.difspotidA = [];
    pairs.difspotidB = [];
    mainhandles.pairscand.num = [];
else
    pairs = mainhandles.pairs;
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Get dispotIDs and indices from input
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

switch inptype
    
  case 'difspotID'
    
    % DifspotID from user input
    difspotid = handles.spotsel.difspotID;

    % Check if spot is present in pairs
    pairindA = find(pairs.difspotidA==difspotid,1,'first');
    pairindB = find(pairs.difspotidB==difspotid,1,'first');
    
    % If spot is present in pairs as spotA
    if ~isempty(pairindA)
      
      % Difspot IDs
      difspotidA = difspotid;
      difspotidB = pairs.difspotidB(pairindA);
   
      % Full image to show
      fullimno  = round(pairs.centimB(pairindA));
      
      % Spot indices in allsp
      indA = pairs.indA(pairindA);
      indB = pairs.indB(pairindA);
  
      
    % If spot is present in pairs as spotB
    elseif ~isempty(pairindB)
      
      % Difspot IDs
      difspotidA = pairs.difspotidA(pairindB);
      difspotidB = difspotid;
   
      % Full image to show
      fullimno  = round(pairs.centimB(pairindB));
      
      % Spot indices in allsp
      indA = pairs.indA(pairindB);
      indB = pairs.indB(pairindB);
      
      
    % Spot is unpaired; show as spot A
    else
      
      % Difspot IDs
      difspotidA = difspotid;
      difspotidB = [];
         
      % Spot indices in allsp
      indA = find(allsp.difspotid==difspotid,1,'first');
      indB = [];
   
      if isempty(indA)
        return
      end
      
      % Full image to show
      fullimno  = round(mod(allsp.centim(indA)+mainhandles.parameters.acq(1).nproj,...
        2*mainhandles.parameters.acq(1).nproj));
      
    end
    
    
  case 'spotindex'
    
    % Spot index from user input
    ind = handles.spotsel.spotindex;

    % Check if spot is present in pairs
    % (pair index and pairid should be the same)
    pairind = allsp.pairid(ind);
        
    
    % Spot is unpaired; show as spot A
    if isnan(pairind)
      
      % Spot indices in allsp
      indA = ind;
      indB = [];
      
      % Difspot IDs
      difspotidA = allsp.difspotid(indA);
      difspotidB = [];
   
      % Full image to show
      fullimno  = round(mod(allsp.centim(indA)+mainhandles.parameters.acq(1).nproj,...
        2*mainhandles.parameters.acq(1).nproj));
     
    
    % If spot is present in pairs as spotA
    elseif (pairs.indA(pairind)==ind)

      % Spot indices in allsp
      indA = ind;
      indB = pairs.indB(pairind);
      
      % Difspot IDs
      difspotidA = pairs.difspotidA(pairind);
      difspotidB = pairs.difspotidB(pairind);
   
      % Full image to show
      fullimno  = round(pairs.centimB(pairind));
      
      
    % If spot is present in pairs as spotB
    elseif (pairs.indB(pairind)==ind)
      
      % Spot indices in allsp
      indA = pairs.indA(pairind);
      indB = ind;
      
      % Difspot IDs
      difspotidA = pairs.difspotidA(pairind);
      difspotidB = pairs.difspotidB(pairind);
  
      % Full image to show
      fullimno  = round(pairs.centimB(pairind));
      
    else
      return
    end
    
    
  case 'pairID'
    
    % Pairs are ordered, so (pair indices = pairIDs)
    pairind = handles.spotsel.pairID;
    
    if ((pairind > length(pairs.indA)) || (pairind < 1))
      return
    else
      
      difspotidA = pairs.difspotidA(pairind);
      difspotidB = pairs.difspotidB(pairind);
     
      fullimno  = round(pairs.centimB(pairind));

      % Recover spot indices
      indA = pairs.indA(pairind);
      indB = pairs.indB(pairind);
      
    end
      
end % of switch

handles.Fullimno = fullimno;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Spot A image
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Directory of full images
dirfull = sprintf('%s/1_preprocessing/full',mainhandles.parameters.acq(1).dir) ;

% Load difspot from file or database
spotimA = gtGetSummedDifSpot(difspotidA,mainhandles.parameters,1);

climA = max(spotimA(:));
climA = [-climA/8 climA];
handles.CLimsSpotA = climA;
set(handles.Edit_CLimsSpotALower,'String',num2str(handles.CLimsSpotA(1)));
set(handles.Edit_CLimsSpotAUpper,'String',num2str(handles.CLimsSpotA(2)));

% Draw image
cla(handles.Axes_SpotA)

% Use IMAGESC (IMSHOW would need Matlab Image Proc. Toolbox License)
colormap(handles.Axes_SpotA,gray(256));
set(handles.Axes_SpotA,'CLim',climA);

handles.Image_SpotA = imagesc(spotimA,'Parent',handles.Axes_SpotA);

axis(handles.Axes_SpotA,'off');
axis(handles.Axes_SpotA,'image');
zoom(handles.Axes_SpotA,'reset');
set(handles.Axes_SpotA,'NextPlot','add');  % same as hold on


% Set horizontal flip
if get(handles.Radio_FlipHor,'Value')
    set(handles.Axes_SpotA,'XDir','reverse');     
    labelposhor = 'right';
else
    set(handles.Axes_SpotA,'XDir','normal');
    labelposhor = 'left';
end


% Set vertical flip
if get(handles.Radio_FlipVer,'Value')
    % Y axis for a flipped image is positive upwards (normal)
    set(handles.Axes_SpotA,'YDir','normal');  
    labelposverY = -1;
else
    % Y axis for an image is by default positive downwards (reverse)
    set(handles.Axes_SpotA,'YDir','reverse');    
    labelposverY = size(spotimA,1)+1;
end


% Difspot label under image
text(2, labelposverY, num2str(difspotidA),...
    'Parent', handles.Axes_SpotA,...
    'HorizontalAlignment', labelposhor,...
    'VerticalAlignment', 'top',...
    'Color', 'y', 'BackgroundColor', 'k', 'Margin', 1);


% Draw rectangle around it
rectApos = [1, 1, size(spotimA,2)-1, size(spotimA,1)-1];
rectangle('Position',rectApos,'Parent',handles.Axes_SpotA,...
            'EdgeColor','y','LineStyle','--','Clipping','on');
set(handles.Axes_SpotA,'NextPlot','replace')  % same as hold off


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Display of spot properties
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Display image data for spots
strcounters    = get(handles.Text_Counters,'String');
strcounters{1} = [num2str(fullimno,'%6.2f'), ' '];
strcounters{2} = [num2str(allsp.centim(indA),'%6.2f'), ' '];
strcounters{3} = ' ';
strcounters{4} = [num2str(fullimno/mainhandles.parameters.acq(1).nproj*180,'%6.3f'), ' '];
strcounters{5} = [num2str(allsp.centim(indA)/mainhandles.parameters.acq(1).nproj*180,'%6.3f'), ' '];
strcounters{6} = ' ';

if isnan(allsp.pairid(indA))
    strcounters(7:11) = {' '};
else
    pairind = allsp.pairid(indA);
    strcounters{7} = [num2str(pairs.error(pairind),'%6.4f'), ' '];
    strcounters{8} = [num2str(pairs.centimdiff(pairind)/...
                     mainhandles.parameters.acq(1).nproj*180,'%6.4f'), ' '];
    strcounters{9} = [num2str(pairs.thetadiff(pairind),'%6.4f'), ' '];
    if isnan(pairs.phasetype(pairind))
        strcounters{10} = 'NaN ';
    else
        strcounters{10} = [mainhandles.PhaseNames{pairs.phasetype(pairind)}, ' '];
    end
    if (isnan(pairs.hkl(pairind,1)) || ~pairs.hklunique(pairind))
        strcounters{11} = 'NaN ';
    else
        strcounters{11} = [num2str(pairs.hkl(allsp.pairid(indA),:)), ' '];
    end
end

set(handles.Text_Counters,'String',strcounters);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Display full image
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Color limits
climfull = [climA(1) climA(2)/2];
handles.CLimsFull = climfull;
set(handles.Edit_CLimsFullLower,'String',num2str(handles.CLimsFull(1)));
set(handles.Edit_CLimsFullUpper,'String',num2str(handles.CLimsFull(2)));

% Load full image
fullim = edf_read([dirfull, sprintf('/full%0.4d.edf',fullimno)]);

% Refresh image
set(handles.Fullim_Image,'CData',fullim);
set(handles.Axes_Full,'CLim',climfull);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Sample envelope projection in full image
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Position pos for the envelope rectangle is: [originX originY width height]

if (get(handles.Radio_SamEnvProj,'Value') == get(handles.Radio_SamEnvProj,'Max'))
  pp = gtGeoProjectedSampleEnvelope(allsp.centu(indA),...
                                     allsp.centv(indA),mainhandles.labgeo);
  pos = [pp(1) pp(2) pp(3)-pp(1) pp(4)-pp(2)];
                                  
else
  pos = [1, 1, mainhandles.labgeo.detsizeu-1, mainhandles.labgeo.detsizeu-1];
end

set(handles.Fullim_SamEnvProj,'Position',pos)


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Projection line spot A-B in full image
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Update projection path
labAmirr = gtMatchCalculateProjectionPath([allsp.centu(indA),allsp.centv(indA)],...
           [allsp.centu(indB), allsp.centv(indB)],handles,mainhandles.labgeo);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Theta search ranges in full image
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

handles = gtMatchDrawThetaCones(handles,mainhandles,labAmirr);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Spot A bbox frame in full image
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Spot A
bbox = [allsp.bborigu(indA), allsp.bborigv(indA), ...
        allsp.bbsizeu(indA)-1, allsp.bbsizev(indA)-1];

set(handles.Fullim_SpotARect,'Position',bbox);
set(handles.Fullim_SpotALabel,'String',num2str(difspotidA));
set(handles.Fullim_SpotALabel,'Position',[bbox(1),bbox(2)+bbox(4)]);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Difspot images of candidates
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Delete previous bboxes and labels
delete(handles.Fullim_SpotBRect)
delete(handles.Fullim_SpotBLabel)
delete(handles.Fullim_SpotBCent)
handles.Fullim_SpotBRect  = [];
handles.Fullim_SpotBLabel = [];
handles.Fullim_SpotBCent  = [];

% Candidate indices
if (indA > length(mainhandles.pairscand.num))
  indC = [];
  errC = [];
else
  indC = mainhandles.pairscand.ind(indA,1:mainhandles.pairscand.num(indA));
  errC = mainhandles.pairscand.err(indA,1:mainhandles.pairscand.num(indA));
end


if isempty(indC)
    cla(handles.Axes_SpotB);

    % Image Processing Toolbox license test before callling impixelinfo
    % This is to avoid crashing when no license is available.
    ok = license('checkout','Image_Toolbox');
    if ok
        handles.Impixelinfo = impixelinfoval(handles.figure1, ...
            [handles.Fullim_Image, handles.Image_SpotA]);
        pos = get(handles.Panel_CLims,'Position');
        set(handles.Impixelinfo,'Units','Normalized');
        set(handles.Impixelinfo,'FontUnits','Normalized');
        set(handles.Impixelinfo,'Position',[0.005 0 pos(3)-0.005 0.02]);
    end
    guidata(handles.figure1,handles);
    drawnow
    return
end

[~, sortind] = sort(errC);
indC         = indC(sortind);
nofC         = length(indC);

difspotidsC = mainhandles.allsp.difspotid(indC);


spotimsC = cell(nofC,1);
sizesC   = zeros(nofC,2);

for ii = 1:nofC
    spotimsC{ii} = gtGetSummedDifSpot(difspotidsC(ii),mainhandles.parameters,1);
    sizesC(ii,:) = size(spotimsC{ii});
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Frames and labels of candidates in full image
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Colormap for candidates
cmap = cool(nofC);

% Loop through all candidates
for ii = 1:nofC
    
    % Bonding box in full image
    bbox = [allsp.bborigu(indC(ii)), allsp.bborigv(indC(ii)), ...
            allsp.bbsizeu(indC(ii)), allsp.bbsizev(indC(ii))];
    
    if indC(ii)==indB % the pair of spot A
        handles.Fullim_SpotBRect(ii) = rectangle('Position',bbox,...
            'Parent',handles.Axes_Full,...
            'EdgeColor','g','Clipping','on');
              
        handles.Fullim_SpotBLabel(ii) = text(bbox(1),bbox(2)+bbox(4),...
            num2str(difspotidsC(ii)),...
            'Parent',handles.Axes_Full,...
            'HorizontalAlignment','left',...
            'VerticalAlignment','top',...
            'Color','g');

        handles.Fullim_SpotBCent(ii) = plot(allsp.centu(indC(ii)),...
            allsp.centv(indC(ii)),...
            'Marker','+','MarkerEdgeColor','g','Parent',handles.Axes_Full);
  
    else  % other candidate
        handles.Fullim_SpotBRect(ii) = rectangle('Position',bbox,...
            'Parent',handles.Axes_Full,...
            'EdgeColor',cmap(ii,:),'Clipping','on');
      
        handles.Fullim_SpotBLabel(ii) = text(bbox(1),bbox(2)+bbox(4),...
            num2str(difspotidsC(ii)),...
            'Parent',handles.Axes_Full,...
            'HorizontalAlignment','left',...
            'VerticalAlignment','top',...
            'Color',cmap(ii,:));

        handles.Fullim_SpotBCent(ii) = plot(allsp.centu(indC(ii)),...
            allsp.centv(indC(ii)),...
            'Marker','+','MarkerEdgeColor',cmap(ii,:),'Parent',handles.Axes_Full);
      
    end
end

% Set visibility according to radio button
if (get(handles.Radio_Candidates,'Value') == get(handles.Radio_Candidates,'Max'))
    set(handles.Fullim_SpotBRect,'Visible','on')
    set(handles.Fullim_SpotBLabel,'Visible','on')
else
    set(handles.Fullim_SpotBRect,'Visible','off')
    set(handles.Fullim_SpotBLabel,'Visible','off')
end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Build tile image of candidates
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Max. size of candidate spot images
maxsizeC = max(sizesC,[],1);
maxheightC = maxsizeC(1);
maxwidthC  = maxsizeC(2);


% Position and size of axes for candidates
units = get(handles.Axes_SpotB,'Units');
set(handles.Axes_SpotB,'Units','pixels');
pos = get(handles.Axes_SpotB,'Position');
axwidth  = pos(3);
axheight = pos(4);
set(handles.Axes_SpotB,'Units',units);

% Number of spot images to tile in axes
nh = max(1,round(sqrt(nofC*axwidth/axheight))); % horizontal
nv = ceil(nofC/nh);                             % vertical

% Fill blank image with spot images
imtile = zeros((maxheightC+labelgapC)*nv+labelgapC, maxwidthC*nh);

rectpos = zeros(nofC,4);

for ii = 1:nofC
   
   % Image tile index
   iv = floor((ii-1)/nh);
   ih = ii-iv*nh-1;
   
   % Image position [hor,ver,width,height]
   imhor = ih*maxwidthC + 1;
   imver = iv*(maxheightC + labelgapC) + 1 ;
   imwidth  = size(spotimsC{ii},2);
   imheight = size(spotimsC{ii},1);
   
   imtile(imver : imver-1+imheight, imhor : imhor-1+imwidth) = spotimsC{ii};

   rectpos(ii,:) = [imhor,imver,imwidth-1,imheight-1];
   
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Display tile image
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

handles.CLimsCand = climA;
set(handles.Edit_CLimsCandLower,'String',num2str(handles.CLimsCand(1)));
set(handles.Edit_CLimsCandUpper,'String',num2str(handles.CLimsCand(2)));

% Clear candidates axes
cla(handles.Axes_SpotB)

% Put image in axes
% Use IMAGESC (IMSHOW would need Matlab Image Proc. Toolbox License)
colormap(handles.Axes_SpotB,gray(256));
set(handles.Axes_SpotB,'CLim',climA);

handles.Image_SpotB = imagesc(imtile,'Parent',handles.Axes_SpotB);

axis(handles.Axes_SpotB,'off');
axis(handles.Axes_SpotB,'image');

% This seems to be needed otherwise Y axis may be flipped when first
% plotting a difspot (irregular behaviour)
set(handles.Axes_SpotB,'YDir','reverse'); 
                                           
zoom(handles.Axes_SpotB,'reset');
set(handles.Axes_SpotB,'NextPlot','add')  % same as hold on


% Rectangles around spot images
cmap = cool(nofC);

for ii = 1:nofC
    if indC(ii)==indB   % the pair of spot A
        rectangle('Position',rectpos(ii,:),'Parent',handles.Axes_SpotB,...
            'EdgeColor','g','Clipping','on');
        
        text(rectpos(ii,1)+1,rectpos(ii,2)+rectpos(ii,4),num2str(difspotidsC(ii)),...
            'Parent',handles.Axes_SpotB,...
            'HorizontalAlignment','left',...
            'VerticalAlignment','top',...
            'Color','g');
        
    else   % other candidates
        rectangle('Position',rectpos(ii,:),'Parent',handles.Axes_SpotB,...
            'EdgeColor',cmap(ii,:),'Clipping','on');

        text(rectpos(ii,1)+1,rectpos(ii,2)+rectpos(ii,4),num2str(difspotidsC(ii)),...
            'Parent',handles.Axes_SpotB,...
            'HorizontalAlignment','left',...
            'VerticalAlignment','top',...
            'Color',cmap(ii,:));
        
    end
end
   
set(handles.Axes_SpotB,'NextPlot','replace')  % same as hold off


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Impixelinfo and finish up
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Recreate impixelinfo
% Image Processing toolbox license test for impixelinfo to avoid crashing
% when license is not available
ok = license('checkout','Image_Toolbox');
if ok
    handles.Impixelinfo = impixelinfoval(handles.figure1, ...
            [handles.Fullim_Image, handles.Image_SpotA, handles.Image_SpotB]);
    pos = get(handles.Panel_CLims,'Position');
    set(handles.Impixelinfo,'Units','Normalized');
    set(handles.Impixelinfo,'FontUnits','Normalized');
    set(handles.Impixelinfo,'Position',[0.005 0 pos(3)-0.005 0.02]);
end


guidata(handles.figure1,handles);

drawnow





