function handles = gtMatchRankCandidates(handles)

% Ranks pair candidates by their mean error, produces a unique 
% list of pairs in 'handles.pairs' and calculates their additional 
% properties. It is called by gtMatchFindPairCandidates.


pc = handles.pairscand;

% Total number of candidate pairs
nofcands = sum(pc.num);

if nofcands == 0
    return
end

set(handles.Text_Progress,'String','Ranking...')
gtMatchSetBusy(handles,'on');
drawnow


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Rank pair candidates
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Put list of candidates in one array; keep already computed info
% [spotA index, spotB index, mean error, theta, diff vector, thetadiff, usedthetaind, shiftA, shiftB]
candslist = zeros(nofcands,3);
lastind = 0;

% Copy candidate data from arrays into candslist
for ii = 1:size(pc.ind,1)
    
    if pc.num(ii) == 0
        continue  % there is no candidate in this row
    end
    
    % Copy spot A index
    candslist(lastind+1:lastind+pc.num(ii),1) = ii;
    
    % Copy spot B-s indices
    candslist(lastind+1:lastind+pc.num(ii),2) = pc.ind(ii,1:pc.num(ii));
    
    % Copy mean error
    candslist(lastind+1:lastind+pc.num(ii),3) = pc.err(ii,1:pc.num(ii));
 
    % Copy theta
    candslist(lastind+1:lastind+pc.num(ii),4) = pc.theta(ii,1:pc.num(ii));
    
    % Copy diffvec
    candslist(lastind+1:lastind+pc.num(ii),5) = pc.diffvecX(ii,1:pc.num(ii));
    candslist(lastind+1:lastind+pc.num(ii),6) = pc.diffvecY(ii,1:pc.num(ii));
    candslist(lastind+1:lastind+pc.num(ii),7) = pc.diffvecZ(ii,1:pc.num(ii));
 
    % Theta difference
    candslist(lastind+1:lastind+pc.num(ii),8) = pc.thetadiff(ii,1:pc.num(ii));

    % Copy usedthetaind
    candslist(lastind+1:lastind+pc.num(ii),9) = pc.usedthetaind(ii,1:pc.num(ii));

    % Copy shifts
    candslist(lastind+1:lastind+pc.num(ii),10) = pc.shiftA(ii, 1);
    candslist(lastind+1:lastind+pc.num(ii),11) = pc.shiftA(ii, 2);
    candslist(lastind+1:lastind+pc.num(ii),12) = pc.shiftA(ii, 3);
    candslist(lastind+1:lastind+pc.num(ii),13) = pc.shiftBX(ii,1:pc.num(ii));
    candslist(lastind+1:lastind+pc.num(ii),14) = pc.shiftBY(ii,1:pc.num(ii));
    candslist(lastind+1:lastind+pc.num(ii),15) = pc.shiftBZ(ii,1:pc.num(ii));
    % Update last index in candslist
    lastind = lastind + pc.num(ii);
    
end
    
% Sort candidates in ascending order according to mean error
candsranked = sortrows(candslist,3);

% Keep only spot A and B indices
candsrankedAB = candsranked(:,1:2);

% Remove duplicates in spot A or spot B, and order the rest ('unique').
% This means picking the best spots with the smallest error, and only once.
% Use transpose to keep them ordered by the error.
[~,uniind] = unique(candsrankedAB','first');

% Recover original indices for candsrankedAB:
origind = false(size(candsrankedAB'));
origind(uniind) = true;
origind = origind';

% Those pairs are kept of which both spot A and B were picked
pairsind = origind(:,1) & origind(:,2);

% Copy ranked pair info in handles
handles.pairs.pairid       = (1:sum(pairsind))';
handles.pairs.indA         = candsranked(pairsind,1);
handles.pairs.indB         = candsranked(pairsind,2);
handles.pairs.error        = candsranked(pairsind,3);
handles.pairs.theta        = candsranked(pairsind,4);
handles.pairs.diffveclab   = candsranked(pairsind,[5 6 7]);
handles.pairs.thetadiff    = candsranked(pairsind,8);
handles.pairs.usedthetaind = candsranked(pairsind,9);
handles.pairs.shiftA       = candsranked(pairsind,[10 11 12]);
handles.pairs.shiftB       = candsranked(pairsind,[13 14 15]);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Add other pair info
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

handles.pairs.sortedthetaind = handles.UsedThetaIndices(handles.pairs.usedthetaind)';

handles.pairs.phasetype  = handles.UsedPhaseTypes(handles.pairs.usedthetaind)';

handles.pairs.thetatype  = NaN(size(handles.pairs.phasetype));
for ii = 1:length(handles.pairs.thetatype)
    handles.pairs.thetatype(ii)  = handles.parameters.cryst(...
        handles.pairs.phasetype(ii)).thetatype(handles.pairs.sortedthetaind(ii));
end

handles.pairs.hkl        = handles.UsedHKLs(:,handles.pairs.usedthetaind)';
handles.pairs.hklunique  = handles.UsedHKLUnique(handles.pairs.usedthetaind)';

handles.pairs.difspotidA = handles.allsp.difspotid(handles.pairs.indA);
handles.pairs.difspotidB = handles.allsp.difspotid(handles.pairs.indB);

handles.pairs.centimA    = handles.allsp.centim(handles.pairs.indA);
handles.pairs.centimB    = handles.allsp.centim(handles.pairs.indB);

handles.pairs.omegaA = (handles.pairs.centimA + handles.pairs.centimB)*90/...
                       handles.parameters.acq(1).nproj - 90;
handles.pairs.omegaB = handles.pairs.omegaA + 180;
handles.pairs.shiftA = handles.shifts(round(handles.pairs.centimA) + 1, :);
handles.pairs.shiftB = handles.shifts(round(handles.pairs.centimB) + 1, :);

handles.pairs.eta    = gtGeoEtaFromDiffVec(handles.pairs.diffveclab,handles.labgeo);

handles.pairs.etaB   = mod(handles.pairs.eta+180,360);

handles.pairs.area     = (handles.allsp.area(handles.pairs.indA) + ...
                          handles.allsp.area(handles.pairs.indB))/2;
handles.pairs.integral = (handles.allsp.integral(handles.pairs.indA) + ...
                          handles.allsp.integral(handles.pairs.indB))/2;
handles.pairs.bbsizeu  = (handles.allsp.bbsizeu(handles.pairs.indA) + ...
                          handles.allsp.bbsizeu(handles.pairs.indB))/2;
handles.pairs.bbsizev  = (handles.allsp.bbsizev(handles.pairs.indA) + ...
                          handles.allsp.bbsizev(handles.pairs.indB))/2;

handles.pairs.centimdiff   = (handles.allsp.centim(handles.pairs.indA) - ...
                              handles.allsp.centim(handles.pairs.indB) + ...
                              handles.parameters.acq(1).nproj);
handles.pairs.extstimdiff  = (handles.allsp.extstim(handles.pairs.indA) - ...
                              handles.allsp.extstim(handles.pairs.indB) + ...
                              handles.parameters.acq(1).nproj);
handles.pairs.extendimdiff = (handles.allsp.extendim(handles.pairs.indA) - ...
                              handles.allsp.extendim(handles.pairs.indB) + ...
                              handles.parameters.acq(1).nproj);

arearatio    = handles.allsp.area(handles.pairs.indA)./...
               handles.allsp.area(handles.pairs.indB);

intratio     = handles.allsp.integral(handles.pairs.indA)./...
               handles.allsp.integral(handles.pairs.indB);
                                        
bbsizeuratio = handles.allsp.bbsizeu(handles.pairs.indA)./...
               handles.allsp.bbsizeu(handles.pairs.indB);

bbsizevratio = handles.allsp.bbsizev(handles.pairs.indA)./...
               handles.allsp.bbsizev(handles.pairs.indB);
           
handles.pairs.arearatio    = max([arearatio,    1./arearatio],   [],2);
handles.pairs.intratio     = max([intratio,     1./intratio],    [],2);
handles.pairs.bbsizeuratio = max([bbsizeuratio, 1./bbsizeuratio],[],2);
handles.pairs.bbsizevratio = max([bbsizevratio, 1./bbsizevratio],[],2);

handles.pairs.centAU = handles.allsp.centu(handles.pairs.indA);
handles.pairs.centAV = handles.allsp.centv(handles.pairs.indA);

handles.pairs.centBU = handles.allsp.centu(handles.pairs.indB);
handles.pairs.centBV = handles.allsp.centv(handles.pairs.indB);

handles.pairs.filttheta = true(size(handles.pairs.indA));
handles.pairs.filterr   = true(size(handles.pairs.indA));
handles.pairs.filtind   = true(size(handles.pairs.indA));


% Copy pairid-s into allsp:
handles.allsp.pairid = NaN(length(handles.allsp.difspotid),1);
handles.allsp.pairid(handles.pairs.indA) = handles.pairs.pairid;
handles.allsp.pairid(handles.pairs.indB) = handles.pairs.pairid;


set(handles.Text_Progress,'String','Ranking done.')
gtMatchSetBusy(handles,'off');
