function handles = gtMatchUpdateLabgeo(handles)
% When the detector parameters have been changed by the user or fitting,
% this function applies those changes and updates labgeo, and the rotation 
% axis plots.

    % Recalculate labgeo with all parameters as new
    newvars = handles.DetParsCorrAct;
    oldvars = handles.DetParsCorrAct;
    newvarsind = true(1, length(handles.DetParsCorrAct));

    labgeo = gtMatchRecalculateLabgeo(newvars, newvarsind, oldvars, ...
                     handles.fitting.labgeobase);

    % Store the fields that have been recalculated
    fnames = fieldnames(labgeo);
    for ii = 1:length(fnames)
        handles.labgeo.(fnames{ii}) = labgeo.(fnames{ii});
    end

    % Update rotation axis projection in full image
    [endpointsuv, endpointsz] = gtMatchRotAxisProjection(handles.labgeo);

    if ishandle(handles.GUIPairFig.Fullim_RotAxis)
        if (~isempty(endpointsuv) && ~isempty(handles.GUIPairFig.Fullim_RotAxis))
            set(handles.GUIPairFig.Fullim_RotAxis, 'XData', endpointsuv([1, 2], 1), ...
                'YData', endpointsuv([1, 2], 2));
        else
            set(handles.GUIPairFig.Fullim_RotAxis, 'XData', NaN, 'YData', NaN);
        end
    end

    if ishandle(handles.GUIPairFig.Fullim_RotAxis3D)
        if (~isempty(endpointsuv) && ~isempty(handles.GUIPairFig.Fullim_RotAxis3D))
            set(handles.GUIPairFig.Fullim_RotAxis3D, 'XData', endpointsuv([1, 2], 1), ...
                'YData', endpointsuv([1, 2], 2), 'ZData', endpointsz);
        else
            set(handles.GUIPairFig.Fullim_RotAxis3D, 'XData', NaN, 'YData', NaN, 'ZData', NaN);
        end
    end
end
