function [fitpar,equalpars] = gtMatchLatticeParDependencies(lp,cryst)

% Defines and sets dependecies of the crystal lattice parameters for
% various crytal systems. Returns one fitting parameter index 'fitpar' that
% represents all the dependent parameters which are indicated in equalpars.
%
% INPUT
%   lp                    - lattice parameter index (scalar: 1 to 6)
%                           [a b c alpha beta gamma]   
%   cryst.crystal_system  - crystal system
%   cryst.hermann_mauguin - Hermann-Mauguin symbol
%


equalpars = false(1,6);
fitpar    = [];


switch cryst.crystal_system

    case 'triclinic'
    % all parameters free
        equalpars(lp) = true;
        fitpar = lp;
        
    case 'monoclinic'
    % beta,gamma = 90
        if any(lp==[1 2 3 4])
            equalpars(lp) = true;
            fitpar = lp;
        end
        
    case 'orthorhombic'
    % alpha,beta,gamma = 90
        if any(lp==[1 2 3])
            equalpars(lp) = true;
            fitpar = lp;
        end
        
    case 'tetragonal'
    % alpha,beta,gamma = 90; lengthb = lengthc
        if (lp == 3)
            equalpars(3) = true;
            fitpar = 3;
        elseif any(lp==[1,2])
            equalpars([1,2]) = true;
            fitpar = 1;
        end
       
    case 'trigonal'
    % Either rhombohedral or hexagonal:
    
        if ~isempty(strfind(cryst.hermann_mauguin,'P'))
            % Hexagonal
            % a1 = a2;  a3 = b ~= c;  alpha = beta = 90;  gamma = 120;  
            % !!! function needs reviewing here 
            if any(lp==[1 2])
                equalpars([1,2]) = true;
                fitpar = 1;
            elseif (lp==3)
                equalpars(3) = true;
                fitpar = 3;
            end
            
        elseif ~isempty(strfind(cryst.hermann_mauguin,'R'))  
            % Rhombohedral: lengtha = lengthb = lengthc
            if any(lp==[1 2 3])
                equalpars([1 2 3]) = true;
                fitpar = 1;
            end

        else
            % Unknown; leave all parameters free
            warning('Unknown lattice system.')
            equalpars(lp) = true;
            fitpar = lp;
        end
        
    case 'hexagonal'
    % a1 = a2;  a3 = b ~= c;  alpha = beta = 90;  gamma = 120;
        % !!! function needs reviewing here
        if any(lp==[1 2])
            equalpars([1,2]) = true;
            fitpar = 1;
        elseif (lp==3)
            equalpars(3) = true;
            fitpar = 3;
        end
        
    case 'cubic'
    % alpha,beta,gamma = 90; lengtha = lengthb = lengthc
        if any(lp==[1 2 3])
            equalpars([1,2,3]) = true;
            fitpar = 1;
        end
        
    otherwise
        warning('Crystal system not recognised.')
        % leave all parameters free
        equalpars(lp) = true;
        fitpar = lp;

end
