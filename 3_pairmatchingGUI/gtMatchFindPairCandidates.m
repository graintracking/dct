function handles = gtMatchFindPairCandidates(handles)

% The core of the matching algorithm which finds and evaluates all possible
% Friedel pairs among all the difspots through a systematic search.
% It applies all the matching criteria and creates a list of all possible
% pairs and their mean error. It then calls gtMatchRankCandidates which
% sorts and selects the best combination of pairs based on their mean error
% creates the 'pairs' set. Results are displayed in the main GUI.


% The maximum no. of candidates to be considered for a spot (only needed to
% predefine a big enough blank array)
maxcands = handles.maxcands;

% Initialize
nproj   = handles.nproj;
allsp   = handles.allsp;
spB     = handles.spB;
nof_spA = length(handles.randindA);
iw      = handles.LastRandomIndex;
state   = 1;
shifts  = handles.shifts;

% If matching have already been completed
if (iw >= nof_spA)
    return
end

% Matching parameters
mp = handles.matchpars;

% Limits for spot candidates properties (field order defines execution
% order)
lim.bbsizeu  = [];
lim.bbsizev  = [];
lim.area     = [];
lim.integral = [];
lim.centim   = [];
lim.extstim  = [];
lim.extendim = [];
lim.centu    = [];
lim.centv    = [];

limgenim.cent   =[];
limgenim.extst  =[];
limgenim.extend =[];

pairscand   = handles.pairscand;
progpercent = 0;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Loop through all spot A-s

gtMatchSetBusy(handles,'on')

% Loop through all spot A-s to find all possible pair candidates.
% Loop as long as figure exists and loop is not paused by user.
% This loop could be parallelized by making a shorter for loop inside it
% without 'drawnow' to collect all possible candidates.
while (ishandle(handles.figure1) && (state == 1))
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Set search criteria for candidates

    % Select next index for A from random list where they were shuffled
    iw   = iw + 1;
    indA = handles.randindA(iw);
    shiftA = shifts(round(allsp.centim(indA) + 1), :);
    % Check if spot A bbox size is large enough
    if ((allsp.bbsizeu(indA) >= mp.minsizeu) && (allsp.bbsizev(indA) >= mp.minsizev))

        % Setting search limits for spot B

        % Boundaries of the sample envelope projected on the opposite pseudo
        % detector plane from the location of Spot A. Spot B must lie inside this
        % rectangle.
        searchbb = gtGeoProjectedSampleEnvelope(allsp.centu(indA),...
            allsp.centv(indA),handles.labgeo) ;

        lim.centu(1) = searchbb(1);
        lim.centv(1) = searchbb(2);
        lim.centu(2) = searchbb(3);
        lim.centv(2) = searchbb(4);

        % Set limits for search between lim1..lim2
        % CentroidImage, ExtStart, ExtEnd limits for spot B
        lim.centim   = sfCalcLimitsImage(allsp.centim(indA)  ,mp.thr_max_offset,nproj);
        lim.extstim  = sfCalcLimitsImage(allsp.extstim(indA) ,mp.thr_ext_offset,nproj);
        lim.extendim = sfCalcLimitsImage(allsp.extendim(indA),mp.thr_ext_offset,nproj);

        % At least one of them should also fulfill thr_genim_offset.
        % It can be helpful if spot is affected by overlap on one side of the
        % omega range.
        limgenim.cent   = sfCalcLimitsImage(allsp.centim(indA)  ,mp.thr_genim_offset,nproj);
        limgenim.extst  = sfCalcLimitsImage(allsp.extstim(indA) ,mp.thr_genim_offset,nproj);
        limgenim.extend = sfCalcLimitsImage(allsp.extendim(indA),mp.thr_genim_offset,nproj);

        % Bounding box sizes:
        lim.bbsizeu = sfCalcLimitsProp(allsp.bbsizeu(indA),mp.thr_bbsize);
        lim.bbsizev = sfCalcLimitsProp(allsp.bbsizev(indA),mp.thr_bbsize);

        % Intensity
        lim.integral = sfCalcLimitsProp(allsp.integral(indA),mp.thr_intint);

        % Area
        lim.area     = sfCalcLimitsProp(allsp.area(indA),mp.thr_area);

        % Select spot candidates that meet all above criteria
        spC = gtMatchSelectSpotCandidates(spB{allsp.subindA(indA)},lim,limgenim);

    else
        % No candidates
        spC = [];
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % If found any candidates
    if (~isempty(spC))

        nofCs = length(spC.ind);
        goodcands     = false(1,nofCs);
        meanerrs      = zeros(1,nofCs);
        thetas        = zeros(1,nofCs);
        thetadiffs    = zeros(1,nofCs);
        usedthetainds = zeros(1,nofCs);
        diffvecsX     = zeros(1,nofCs);
        diffvecsY     = zeros(1,nofCs);
        diffvecsZ     = zeros(1,nofCs);

        % Loop through all its candidates ...
        for ic = 1:nofCs
            shiftB(ic, :) = shifts(round(allsp.centim(spC.ind(ic))) + 1, :);
            % Check theta angle and plane family for each candidate
            [theta,theta_diff,usedthetaind,theta_OK,diffvec] = gtMatchIdentifyTheta(...
                [allsp.centu(indA),allsp.centv(indA)], [spC.centu(ic),spC.centv(ic)],...
                handles.labgeo, handles.UsedThetas, mp.thr_theta_used, mp.thr_theta_scale, shiftA, shiftB(ic, :));

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % If candidate is good
            if (theta_OK)
                % Check if theta is in the user specified limits
                ttvec = theta(ones(size(handles.ThetaLimits,1), 1), 1);
                goodranges = (handles.ThetaLimits(:,1) < ttvec) & (ttvec < handles.ThetaLimits(:,2));

                if (~any(goodranges))
                    continue;  % go to next candidate
                end

                % If theta angle is acceptable, compute errors, store candidate

                % Relative deviations of spot properties ...
                reldevs = [max(spC.area(ic)/allsp.area(indA), allsp.area(indA)/spC.area(ic))-1; ...
                    max(spC.integral(ic)/allsp.integral(indA),  allsp.integral(indA)/spC.integral(ic))-1; ...
                    max(spC.bbsizeu(ic) /allsp.bbsizeu(indA),   allsp.bbsizeu(indA)/spC.bbsizeu(ic))-1; ...
                    max(spC.bbsizev(ic) /allsp.bbsizev(indA),   allsp.bbsizev(indA)/spC.bbsizev(ic))-1; ...
                    abs(spC.extstim(ic) -allsp.extstim(indA)   -nproj); ...
                    abs(spC.extendim(ic)-allsp.extendim(indA)  -nproj); ...
                    abs(spC.centim(ic)  -allsp.centim(indA)    -nproj); ...
                    abs(theta_diff)];

                % ... weighted by corresponding tolerances ...
                reldevsw = reldevs./[mp.thr_area; mp.thr_intint; ...
                    mp.thr_bbsize; mp.thr_bbsize; ...
                    mp.thr_ext_offset; mp.thr_ext_offset; mp.thr_max_offset; ...
                    mp.thr_theta_used];

                % .. give the mean error (goodness of match) for each candidate:
                meanerrs(ic)   = sqrt(sum(reldevsw.^2));

                % theta type
                usedthetainds(ic) = usedthetaind;

                % store good index
                goodcands(ic)  = true;

                % theta
                thetas(ic) = theta;
                thetadiffs(ic) = theta_diff;

                % diffraction vector
                diffvecsX(ic) = diffvec(1);
                diffvecsY(ic) = diffvec(2);
                diffvecsZ(ic) = diffvec(3);
            end
        end

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % If there are any good candidates for spot A
        if any(goodcands)
            % Indices of good candidates (the ones that are not 0)
            nofgoods = sum(goodcands);

            % Delete if there happen to be too many candidates to store
            if nofgoods > maxcands
                nofgoods = maxcands;
                goodcands(nofgoods+1:end) = false;
            end

            % Collect candidate data in big pairscand array
            pairscand.ind(indA,1:nofgoods) = spC.ind(goodcands);
            pairscand.num(indA,1)          = nofgoods;

            pairscand.err(indA,1:nofgoods)          = meanerrs(goodcands);
            pairscand.theta(indA,1:nofgoods)        = thetas(goodcands);
            pairscand.thetadiff(indA,1:nofgoods)    = thetadiffs(goodcands);
            pairscand.usedthetaind(indA,1:nofgoods) = usedthetainds(goodcands);
            pairscand.diffvecX(indA,1:nofgoods)     = diffvecsX(goodcands);
            pairscand.diffvecY(indA,1:nofgoods)     = diffvecsY(goodcands);
            pairscand.diffvecZ(indA,1:nofgoods)     = diffvecsZ(goodcands);
            pairscand.shiftA(indA,:)                = shiftA;
            pairscand.shiftBX(indA,1:nofgoods)      = shiftB(goodcands, 1);
            pairscand.shiftBY(indA,1:nofgoods)      = shiftB(goodcands, 2);
            pairscand.shiftBZ(indA,1:nofgoods)      = shiftB(goodcands, 3);
        end
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Progress and loop control
    handles.LastRandomIndex = iw;

    % Display matching progress
    if (mod(iw,10) == 0)
        progpercent = iw/length(handles.randindA)*100;
        set(handles.Text_Progress,'String',sprintf('Matching... %0.1f%%',progpercent))
    end

    % Conditions to continue the while loop

    % Determine which string the label matches (1-> running, 2-> paused)
    state = find(strcmp(get(handles.MatchPairs_Push_StopCont,'String'),...
        handles.stopcont));

    % If last index is reached -> finish
    if (iw == nof_spA)
        state = 2;  % will stop while loop

        if strcmp(handles.mode,'match')
            handles = gtMatchUpdateStatus(1,'matchdone',handles);
        end
        progpercent = 100;
        set(handles.Text_Progress,'String','Matching done.')
        set(handles.MatchPairs_Push_StopCont,'Enable','off')
    end

    % Drawnow to update progress display.
    % A drawnow or similar command is also needed for the callback function to
    % be interruptible by user action. This is why this loop can't be
    % directly parallelized.

    if (mod(iw, 50) == 0)
        drawnow();
    else
        drawnow('expose')
    end
end

% Copy candidates in handles
handles.pairscand = pairscand;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Rank all pair candidates, create output

% Rank all candidates based on mean error to find best matching pairs first;
% create pairs output ('.pairs' field will be added to handles)
handles = gtMatchRankCandidates(handles);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Finish up

% Update statistics
gtMatchUpdateStatistics(handles);

% Display result, no. of pairs
pairstext    = get(handles.Text_PairsResults,'String');
pairstext{1} = num2str(iw);

% If any pairs found
if (isfield(handles,'pairs'))
    pairstext{2} = num2str(length(handles.pairs.indA));
    mpercent     = length(handles.pairs.indA)/min(handles.nof_spotsA,handles.nof_spotsB)*100;
else
    pairstext{2} = '0';
    mpercent     = 0;
    gtMatchSetBusy(handles,'off')
    drawnow
end

set(handles.Text_PairsResults,'String',pairstext);

% Display result, progress and matching percentage
pairstext    = get(handles.Text_PairsResultsPercent,'String');
pairstext{1} = sprintf('%0.1f%%',progpercent);
pairstext{2} = sprintf('%0.1f%%',mpercent);
set(handles.Text_PairsResultsPercent,'String',pairstext);

set(handles.MatchPairs_Push_Reset,'Enable','on')

end % of main function

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Sub-functions

function lims = sfCalcLimitsImage(imA,tol,nproj)
% Calculates limits of the search range in image indices 180deg +-tol offset.
    lims(1) = imA + nproj - tol;
    lims(2) = imA + nproj + tol;
end

function lims = sfCalcLimitsProp(prop,tol)
% Calculates limits of the search range for a given property.
    lims(1) = prop/tol;
    lims(2) = prop*tol;
end


