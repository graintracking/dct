function [labgeo,latticepar] = gtMatchApplyFitParams(newvars,newvarsind,oldvars,...
                               labgeobase,cryst)

% It applies the fit paramaters to recalculate labgeo and the lattice 
% parameters. Those new input parameters that are specified in newvars will 
% substitute the corresponding old ones for the calculation.
%
% Parameters order:
% 1  = 'Position X';
% 2  = 'Position Y';
% 3  = 'Position Z';
% 4  = 'Tilt in plane';
% 5  = 'Angle U-V';
% 6  = 'Tilt around U';
% 7  = 'Tilt around V';
% 8  = 'Pixel size mean';
% 9  = 'Pixel size ratio';
% 10 = 'lengtha';
% 12 = 'lengthb';
% 13 = 'lengthc';
% 14 = 'alpha';
% 15 = 'beta';
% 16 = 'gamma';
%


% Recalculate labgeo
labgeo = gtMatchRecalculateLabgeo(newvars,newvarsind,oldvars,labgeobase);

% Add constant fields
labgeo.beamdir = labgeobase.beamdir;
labgeo.rotpos  = labgeobase.rotpos;
labgeo.rotdir  = labgeobase.rotdir;


% Lattice parameters - apply changes

newlattpars = newvars(sum(newvarsind(1:9))+1:end);
lattparsind = newvarsind(10:15);
latticepar  = oldvars(10:15);


changepars=find(lattparsind);
for ii = 1:length(changepars)
    % Get dependencies of lattice parameters for the actual crystal system
    [~,equalpars] = gtMatchLatticeParDependencies(changepars(ii),cryst);
    % Set all dependent parameters
    latticepar(equalpars) = newlattpars(ii);
end
