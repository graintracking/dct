function [shifts_lab, shifts_sam, abspos, relpos] = gtMatchGetSampleShifts(parameters, omega_values, varargin)
%   function [shifts_lab, shifts_sam, abspos, relpos] = gtMatchGetSampleShifts(parameters, omega_values)
%
    conf = struct('display', false, 'offset_pos', 0, 'det_index', 1);
    conf = parse_pv_pairs(conf, varargin);

    tot_proj = gtAcqTotNumberOfImages(parameters, conf.det_index);
    omstep   = gtAcqGetOmegaStep(parameters, conf.det_index);
    refon    = parameters.acq.refon;
    ref_grps = tot_proj / refon;

    acq_dir = parameters.acq.dir;
    acq_name = parameters.acq.name;
    base_dir = fullfile(acq_dir, '0_rawdata', 'Orig');

    if (~exist('parameters', 'var') || isempty(parameters))
        parameters = gtLoadParameters();
    end
    if isfield(parameters.acq,'shifts_sam')
        if (exist('omega_values', 'var') && ~isempty(omega_values))
            % set NaN entries in omega to 0;
            nans = isnan(omega_values);
            omega_values(nans) = 0;
            % transform into image_numbers
            im_num = round(omega_values / omstep) + 1;
            shifts_lab = parameters.acq.shifts_lab(im_num, :);
            shifts_sam = parameters.acq.shifts_sam(im_num, :);
        else
            shifts_lab = parameters.acq.shifts_lab;
            shifts_sam = parameters.acq.shifts_sam;
        end
        return
    end
    tot_proj = gtAcqTotNumberOfImages(parameters, conf.det_index);
    omstep   = gtAcqGetOmegaStep(parameters, conf.det_index);
    refon    = parameters.acq.refon;
    ref_grps = tot_proj / refon;

    acq_dir = parameters.acq.dir;
    acq_name = parameters.acq.name;
    base_dir = fullfile(acq_dir, '0_rawdata', 'Orig');

    filename = sprintf('%s/%s%04d.edf', base_dir, acq_name, conf.offset_pos);
    info     = edf_info(filename);
    samx     = getfield(info.motor, parameters.acq.samx);
    samy     = getfield(info.motor, parameters.acq.samy);
    samz     = getfield(info.motor, parameters.acq.samz);
    offset   = [samx, samy, samz];

    shifts_lab = zeros(tot_proj, 3);
    shifts_sam = zeros(tot_proj, 3);
    abspos = zeros(ref_grps + 1, 3);
    relpos = zeros(ref_grps + 1, 3);
    rotcomp = gtMathsRotationMatrixComp(parameters.labgeo.rotdir, 'row');

    for ii = 1 : ref_grps
        filename = sprintf('%s/%s%04d.edf', base_dir, acq_name, (ii-1) * refon);
        info = edf_info(filename);
        samx = getfield(info.motor, parameters.acq.samx);
        samy = getfield(info.motor, parameters.acq.samy);
        samz = getfield(info.motor, parameters.acq.samz);
        abspos(ii, :) = [samx, samy, samz];
        relpos(ii, :) = abspos(ii, :) - offset;

        for jj = 1 : refon
            im_num = (ii - 1) * refon + jj - 1;
            omega = omstep * im_num;
            rottens = gtMathsRotationTensor(omega, rotcomp);
            shifts_lab(im_num + 1, :) = relpos(ii, :) * rottens;
            shifts_sam(im_num + 1, :) = relpos(ii, :);
        end
    end

    % handle 360 degree image after scan
    filename = sprintf('%s/%s%04d.edf', base_dir, acq_name, tot_proj);
    info = edf_info(filename);
    samx     = getfield(info.motor, parameters.acq.samx);
    samy     = getfield(info.motor, parameters.acq.samy);
    samz     = getfield(info.motor, parameters.acq.samz);
    abspos(ref_grps + 1, :) = [samx, samy, samz];
    relpos(ref_grps + 1, :) = abspos(ref_grps + 1, :) - offset;
    shifts_lab(tot_proj + 1, :) = relpos(ref_grps + 1, :) * eye(3);
    shifts_sam(tot_proj + 1, :) = relpos(ref_grps + 1, :);

    if (exist('omega_values', 'var') && ~isempty(omega_values))
        % set NaN entries in omega to 0;
        nans = isnan(omega_values);
        omega_values(nans) = 0;
        % transform into image_numbers
        im_num = round(omega_values / omstep) + 1;
        shifts_lab = shifts_lab(im_num, :);
        shifts_sam = shifts_sam(im_num, :);
        refgrp = floor(im_num / refon) + 1;
        abspos = abspos(refgrp, :);
        relpos = relpos(refgrp, :);
    end

    if (conf.display)
        f = figure();
        ax = axes('parent', f);
        plot(ax, shifts_lab(:, 1)', shifts_lab(:, 2)', '*')
    end
end