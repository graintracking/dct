function gtMatchSetBusy(handles,status)

% Sets 'Busy' or 'Ready' progress label in the main GUI.

if strcmp(status,'on')
    set(handles.Text_Busy,'BackgroundColor',[1 0 0])
    set(handles.Text_Busy,'String','Busy')
else
    set(handles.Text_Busy,'BackgroundColor',[0 1 0])
    set(handles.Text_Busy,'String','Ready')
end

drawnow
