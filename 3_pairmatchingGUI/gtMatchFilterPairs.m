function handles = gtMatchFilterPairs(handles, resetzoom)

% It applies the mean error filter set in the main GUI to the pairs and
% creates its filtered subset 'pairsfilt'. It updates the Theta-Eta plot
% and histogram, and displays the number of filtered pairs. 

if ~isfield(handles,'pairs')
    figure(handles.GUIEtaTheta.figure1)
    return
end

set(handles.Text_Progress,'String','Updating...')
gtMatchSetBusy(handles,'on');

% Indices with an error smaller than the error filter 
handles.pairs.filterr = handles.pairs.error <= handles.ErrorFilter;

% Combine the theta and error filters
handles.pairs.filtind = (handles.pairs.filterr & handles.pairs.filttheta);


% Copy all fields from pairs to pairsfilt
fn = fieldnames(handles.pairs);
for ii = 1:length(fn)
    handles.pairsfilt.(fn{ii}) = handles.pairs.(fn{ii})(handles.pairs.filtind,:);
end


% Update Eta-Theta figure in other GUI
eta   = handles.pairsfilt.eta;
theta = handles.pairsfilt.theta;
refreshdata(handles.GUIEtaTheta.EtaHandle,'caller')

% Compute Theta Histogram, update figure
thetabins = min(theta):0.01:max(theta);
histtheta = hist(theta,thetabins);
refreshdata(handles.GUIEtaTheta.HistHandle,'caller')

if ~isempty(theta)

    % Eta-Theta plot
    if resetzoom
        set(handles.GUIEtaTheta.Axes_EtaPlot,'XLim',[min(theta)-3 max(theta)+3]);
        set(handles.GUIEtaTheta.Axes_EtaPlot,'YLim',[min(eta)-3 max(eta)+3]);
        zoom(handles.GUIEtaTheta.Axes_EtaPlot,'reset')
    end
    
    % Histogram
    newylim = max(histtheta)*1.1;
    set(handles.GUIEtaTheta.Axes_ThetaHist,'YLim',[0 newylim]);
    gtMatchUpdateThetaMarkers(handles,'height',newylim);
    if resetzoom
        zoom(handles.GUIEtaTheta.Axes_ThetaHist,'reset')
    end

end


% Display result
pairstext    = get(handles.Text_PairsResults,'String');
pairstext{3} = num2str(length(handles.pairsfilt.eta));
set(handles.Text_PairsResults,'String',pairstext);

% Display result, filtered percentage
fpercent     = length(handles.pairsfilt.eta)/min(handles.nof_spotsA,handles.nof_spotsB)*100;
pairstext    = get(handles.Text_PairsResultsPercent,'String');
pairstext{3} = sprintf('%0.1f%%',fpercent);
set(handles.Text_PairsResultsPercent,'String',pairstext);


set(handles.Text_Progress,'String','Updating done.')
gtMatchSetBusy(handles,'off');

figure(handles.GUIEtaTheta.figure1)

drawnow
 