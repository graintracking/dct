function handles = gtMatchUpdatePairs(handles, resetzoom)

% When labgeo have changed, this function applies the changes to existing 
% pairs' by recalculating eta and theta values for the plots.

if ~isfield(handles,'pairs')
    return
end


% Diffracton vectors and theta
[handles.pairs.diffveclab,~,handles.pairs.theta] = gtGeoDiffVecInLabPair(...
    [handles.pairs.centAU,handles.pairs.centAV], ...
    [handles.pairs.centBU,handles.pairs.centBV], handles.labgeo, handles.pairs.shiftA, handles.pairs.shiftB);


% Discrepancy in theta
handles.pairs.thetadiff = handles.pairs.theta - ...
                          handles.SortedThetas(handles.pairs.sortedthetaind)';


% Eta angles
handles.pairs.eta = gtGeoEtaFromDiffVec(handles.pairs.diffveclab,handles.labgeo);


% Refilter pairs and plot them
handles = gtMatchFilterPairs(handles, resetzoom);
