function varargout = gtMatchGUIStatistics(varargin)

% Statistics GUI figure main function and callbacks.
% It is launched by the main GUI.

% GTMATCHGUISTATISTICS MATLAB code for gtMatchGUIStatistics.fig
%      GTMATCHGUISTATISTICS, by itself, creates a new GTMATCHGUISTATISTICS or raises the existing
%      singleton*.
%
%      H = GTMATCHGUISTATISTICS returns the handle to a new GTMATCHGUISTATISTICS or the handle to
%      the existing singleton*.
%
%      GTMATCHGUISTATISTICS('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in GTMATCHGUISTATISTICS.M with the given input arguments.
%
%      GTMATCHGUISTATISTICS('Property','Value',...) creates a new GTMATCHGUISTATISTICS or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before gtMatchGUIStatistics_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to gtMatchGUIStatistics_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help gtMatchGUIStatistics

% Last Modified by GUIDE v2.5 26-Jan-2012 17:45:14

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @gtMatchGUIStatistics_OpeningFcn, ...
                   'gui_OutputFcn',  @gtMatchGUIStatistics_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before gtMatchGUIStatistics is made visible.
function gtMatchGUIStatistics_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to gtMatchGUIStatistics (see VARARGIN)

% Set normal figure toolbar visible
set(hObject,'Toolbar','figure');

% Set figure name
set(hObject,'Name','Statistics - Matching GUI');

% Set location in window
set(hObject,'Units','Normalized');
pos    = get(hObject,'Position');
pos(1) = 1-pos(3);
pos(2) = 1-pos(4);
set(hObject,'Position',pos);

% Choose default command line output for gtMatchGUIStatistics
handles.output.figure1 = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes gtMatchGUIStatistics wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = gtMatchGUIStatistics_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;
