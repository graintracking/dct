function cand = gtMatchSelectSpotCandidates(sps,lim,limgenim)

% Returns those spot candidates 'sps' which passed all selection criteria 
% defined in 'lim' and 'limgenim'.
% All values specified by the fieldnames in 'lim' are checked. 'sps' will 
% have those values between those lower and upper limits, and at least 
% one of them defined in 'limgenim' for the fields described in this
% function.


cand = [];

% Initial indices of all spots; those which fall outside at least one of 
% limits will be deleted.
good = (1:length(sps.centim))';  

% Fieldnames to be checked:
fn = fieldnames(lim);

% Check which values are between given limits (consistency).
%  Order is determined by the order of fields in lim.
for ii = 1:length(fn)
    cons = (lim.(fn{ii})(1) <= sps.(fn{ii})(good)) & (sps.(fn{ii})(good) <= lim.(fn{ii})(2)); 
    good(~cons) = [];
    if isempty(good)
        return
    end
end


% Check general image offset; at least one of three should be within limits
cons1 = (limgenim.cent(1)   <= sps.centim(good))   & (sps.centim(good)   <= limgenim.cent(2));
cons2 = (limgenim.extst(1)  <= sps.extstim(good))  & (sps.extstim(good)  <= limgenim.extst(2));
cons3 = (limgenim.extend(1) <= sps.extendim(good)) & (sps.extendim(good) <= limgenim.extend(2));
cons = cons1 | cons2 | cons3;
good(~cons) = []; 


% Keep only good ones:
fn = fieldnames(sps);

for ii = 1:length(fn)
    cand.(fn{ii}) = sps.(fn{ii})(good);
end



