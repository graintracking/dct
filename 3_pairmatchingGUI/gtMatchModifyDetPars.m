function handles = gtMatchModifyDetPars(handles, ind, incsign)
% It is called when the detector parameters are incremented via the table 
% in the main GUI. It applies the increment and updates labgeo and the pair
% data.

    inc = handles.DetParsIncrement;

    if (incsign == '+');
    elseif (incsign == '-');
        inc = -inc;
    else
        return
    end

    handles.DetParsCorrAct(ind) = handles.DetParsCorrAct(ind) + inc(ind);

    dettable = get(handles.DetPars_Table, 'Data');
    dettable{ind, 4} = handles.DetParsCorrAct(ind);

    dettable(:, [5, 6]) = {false};

    set(handles.DetPars_Table, 'Data', dettable);

    % Update labgeo and pairs angles
    handles = gtMatchUpdateLabgeo(handles);
    handles = gtMatchUpdatePairs(handles, 0);
end
