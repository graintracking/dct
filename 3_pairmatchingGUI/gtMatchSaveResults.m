function handles = gtMatchSaveResults(handles)
% GTMATCHSAVERESULTS  
%     handles = gtMatchSaveResults(handles)
%     -------------------------------------
%     Saves the output of matching. Saves updates in the parameters file,
%     puts the filtered pairs into the pair table of the database, writes a 
%     .mat file in the 3_Matching folder.
%  
%     Version 001.1 03-05-2012 by LNervo
%       Formatting: remove sprintf 
%                   use [] multi-strings and fullfile
%                   add () to functions

set(handles.Text_Progress,'String','Saving results...')

if ~handles.matchdone
    msg{1} = 'Matching has not been completed.';
    msg{2} = 'If the geometry or lattice paremeters have been changed,';
    msg{3} = 'the pair data might be obsolete.';
    msg{4} = 'Are you sure to save current results?';
    choice = questdlg(msg,'Save matching results','Yes','Cancel','Yes');

    switch choice
        case 'Cancel'
            set(handles.Text_Progress,'String','Saving cancelled.')
            return
    end
end


if ~handles.fitdone
    msg    = [];
    msg{1} = 'Parameters fitting has not been completed.';
    msg{2} = 'Are you sure to save current results?';
    choice = questdlg(msg,'Save matching results','Yes','Cancel','Cancel');

    switch choice
        case 'Cancel'
            set(handles.Text_Progress,'String','Saving cancelled.')
            return
    end
end


if ~handles.fitsaved
    msg    = [];
    msg{1} = 'Parameters fitting results have not been saved.';
    msg{2} = 'Changes to the lattice parameters, if any, will not be saved.';
    msg{3} = 'Are you sure to save current results?';
    choice = questdlg(msg,'Save matching results','Yes','Cancel','Cancel');

    switch choice
        case 'Cancel'
            set(handles.Text_Progress,'String','Saving cancelled.')
            return
    end
end


% Connect again to database (connection may have been lost)
gtDBConnect();

% Existing pairs
handles.nof_pairs_ini = mym(['SELECT count(*) FROM ' handles.pairtable]);

if handles.nof_pairs_ini > 0

    % Display database status
    set(handles.Text_PairtableEmpty,'String','NOT EMPTY!')
    set(handles.Text_PairtableEmpty,'BackgroundColor',[1 0.6 0])
   
    % Dialog window
    msg    = [];
    msg{1} = 'Pairtable is not empty. Saving may overwrite existing pairs.';
    msg{2} = 'Would you like to overwrite existing pairs or add new ones ?';
    choice = questdlg(msg,'Save matching results','Overwrite','Add','Cancel','Overwrite');

    switch choice
        case 'Cancel'
            set(handles.Text_Progress,'String','Saving cancelled.')
            return
        case 'Overwrite'
            set(handles.Text_Progress,'String','Deleting table...')
            % Delete pairtable content
            mym(['truncate ' handles.pairtable]) ;
    end
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Sample reference system
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Define SAMPLE reference, if doesn't yet exist
if ~isfield(handles.parameters,'samgeo')
    samgeo = gtGeoSamDefaultParameters();
else
    samgeo = handles.parameters.samgeo;
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Create output
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Scattering vector
scattveclab = gtGeoScattVecFromDiffVec(handles.pairsfilt.diffveclab, ...
                                       handles.labgeo.beamdir);

handles.pairsfilt.planenorm = gtGeoLab2Sam(scattveclab,...
    handles.pairsfilt.omegaA, handles.labgeo, samgeo, true, 'only_rot', true);


% Diffraction vector
handles.pairsfilt.diffvecsam = gtGeoLab2Sam(handles.pairsfilt.diffveclab,...
    handles.pairsfilt.omegaA, handles.labgeo, samgeo, true, 'only_rot', true);


% Difspot A in Sample reference
% Attention - spotA positions are shifted in order to correct for sample
% movements
centAlab = gtGeoDet2Lab([handles.pairsfilt.centAU,handles.pairsfilt.centAV],...
           handles.labgeo, 0) - handles.pairsfilt.shiftA;

handles.pairsfilt.centsamA = gtGeoLab2Sam(centAlab, handles.pairsfilt.omegaA,...
                                          handles.labgeo, samgeo, false);

% Spot B data is not important.
%handles.pairsfilt.centsamB = zeros(length(handles.pairsfilt.indA),3);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Fill spot pairs table
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

saveok = false(1,3);

ps = handles.pairsfilt;

ps.thetatype(~ps.hklunique) = 0;
ps.hkl(~ps.hklunique,:)     = 0;


% Insert fields in database (addition of rows if table is not empty)
try
    gtDBInsertMultiple(handles.pairtable,...
    'difAID',ps.difspotidA, 'difBID',ps.difspotidB,...
    'meanerror',ps.error, 'theta',ps.theta,...
    'eta',ps.eta, 'etaB',ps.etaB,...
    'omega',ps.omegaA, 'omegaB',ps.omegaB,...
    'avintint',ps.integral, 'avbbXsize',ps.bbsizeu, 'avbbYsize',ps.bbsizev,...
    'samcentXA',ps.centsamA(:,1), 'samcentYA',ps.centsamA(:,2), 'samcentZA',ps.centsamA(:,3),...
    'ldirX', ps.diffvecsam(:,1), 'ldirY',ps.diffvecsam(:,2), 'ldirZ',ps.diffvecsam(:,3),...
    'plX',ps.planenorm(:,1), 'plY',ps.planenorm(:,2), 'plZ',ps.planenorm(:,3),...
    'phasetype',ps.phasetype, 'thetatype',ps.thetatype);
    %'samcentXB',ps.centsamB(1), 'samcentYB',ps.centsamB(2), 'samcentZB',ps.centsamB(3),...
    
    disp(' ')
    disp('Matching results saved in database table:')
    disp(handles.pairtable)
    
    % Check no. of pairs in table
    nof_pairs = mym(['SELECT count(*) FROM ' handles.pairtable]);
    
    % Display status
    if nof_pairs > 0
        set(handles.Text_PairtableEmpty,'String','NOT EMPTY!')
        set(handles.Text_PairtableEmpty,'BackgroundColor',[1 0.6 0])
    else
        set(handles.Text_PairtableEmpty,'String','Empty')
        set(handles.Text_PairtableEmpty,'BackgroundColor',[0 1 0])
    end
   
    saveok(1) = true;
    
catch err
    disp(' ')
    disp('Could not save matching results in database table:')
    disp(handles.pairtable)
    disp(err.message)
end
    
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Update parameters file
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

parameters = [];
load('parameters.mat');
handles.parameters.cryst = parameters.cryst;
handles.parameters.match = handles.matchpars;
handles.parameters.match = rmfield(handles.parameters.match,'thr_theta_used');
 
handles.parameters.match.thr_meanerror = handles.ErrorFilter;
handles.parameters.match.thetalimits   = handles.ThetaLimits;
handles.parameters.samgeo              = samgeo;
handles.parameters.labgeo              = handles.labgeo;
handles.parameters.labgeo = rmfield(handles.parameters.labgeo,'detorig');
handles.parameters.labgeo = rmfield(handles.parameters.labgeo,'detnorm');


% % Make sure to use the same par file for the whole session, also when
% % saving the matching results later.
% if isempty(handles.parfilename)
%     backup_filename = gtLastFileName( fullfile( handles.workingdir, ['parameters_old_' date' _']), 'new' );
%     handles.parfilename = backup_filename;
% else
%     backup_filename = handles.parfilename;
% end


try
    save('parameters.mat','-struct','handles','parameters');

    disp(' ')
    disp('Parameters file has been updated.')
    disp(' ')

    saveok(2) = true;

catch err
    disp(' ')
    disp('Could not save parameters file.')
    disp(err.message)
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Save matching results file
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Structure to save 
match.parameters     = handles.parameters;
match.pairs          = handles.pairs;
match.pairsfilt      = handles.pairsfilt;
match.lastfitting    = handles.fitting;
match.usedphases     = handles.UsedPhases;
match.usedthetas     = handles.UsedThetas;
match.usedhkls       = handles.UsedHKLs;
match.usedhklunique  = handles.UsedHKLUnique;
match.usedphasetypes = handles.UsedPhaseTypes;

filename = gtLastFileName( fullfile( handles.workingdir, '3_pairmatching', ['match_' date '_']), 'new' );

% Save matching.mat file
try
    save(filename,'match');
   
    disp(' ')
    disp('Matching output saved as:')
    disp(filename)
    disp(' ')

    saveok(3) = true;
     
catch err
    disp(' ')
    disp('Could not save matching output as:')
    disp(filename)
    disp(err.message)
    disp(' ')
end


if all(saveok)
    handles = gtMatchUpdateStatus(1,'matchsaved',handles);
    set(handles.Text_Progress,'String','Results saved.')
else
    handles = gtMatchUpdateStatus(0,'matchsaved',handles);
    set(handles.Text_Progress,'String','Could not save properly.')
end

end % end of function
