function strain = gtStrainGrainsGeneral(varargin)
% GTSTRAINGRAINSGENERAL Elastic strain fitting for multiple grains.
% 
% strain = gtStrainGrainsGeneral(varargin)
%
% Runs a strain calculation for all the grains in the cell array 'grain{i}'.
%
% It calls 'gtStrainSingleGrain' to do the strain calculations (see that
% help for more details).
%
% Grains with all coplanar plane normals or with outlier principal 
% strain values will have their strain components set to NaN.
%
% It filters outliers based on their smallest or largest principal strain
% component being away from the median value of all grains more than 
% 'stdlim' times the corresponding standard deviation.
%
% OPTIONAL INPUT
%   grain   - multpile grain data in cell array as output from Indexter;
%             if undefined, loaded from 4_grains/phase_01/index.mat
%   cryst   - crystallography data as in 4_grains/phase_##/index.mat file 
%             or parameters file; if undefined, loaded from 
%             4_grains/phase_01/index.mat
%   phase   - phase ID (default is 1);
%   energy  - energy of the incident X-ray beam in keV
%   Bmat    - matrix to transform HKL indices into Cartesian coordinates
%   errfilt - error filter for pairs (0 < errfilt <= 1); this best fraction
%             of Friedel pairs will be used for error fitting; 
%             e.g. 0.9 meaning the best 90% based on their path distance 
%             from the grain center of mass; (default is 0.9)
%   stdlim  - limits in std of all grains' smallest and largest 
%             principal strain components to reject outlier grains
%             (default is Inf)
%   save    - if true, results are saved in two files {false}:
%               4_grains/phase_##/index.mat
%               4_grains/phase_##/index_DD-MMM-YYYY_N.mat
%
%   Either 'grain.hklsp' or 'cryst.shklfam' field is required.
%
% OUTPUT
%   strain.grain.strain - new field added containing strain data for each 
%                         grain
%   strain.strainT      - all strain tensors in one array; it contains 
%                         NaN-s for the grain ID-s in strain.copl
%                         (3,3,length(grain))
%   strain.strainTmean  - mean strain tensor of all grains
%   strain.strainTmed   - median strain tensor of all grains
%   strain.copl         - indices of grains having plane normals that are 
%                         all close to being coplanar
%   strain.out          - grain indices that have their principal strain 
%                         components identified as outliers
%   strain.par          - parameters used for strain fitting
%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Set input
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

tic

par.phase   = 1;
par.errfilt = 0.9; 
par.stdlim  = Inf;
par.energy  = []; 
par.Bmat    = [];
par.cryst   = [];
par.grain   = [];
par.save    = false;

par = parse_pv_pairs(par, varargin);


% Store grain outside the par structure
grain = par.grain;
par   = rmfield(par, 'grain');

if isempty(grain)
    fname = sprintf('4_grains/phase_%02d/index.mat', par.phase);
    fprintf('Loading grain info from file %s\n', fname)
    index = load(fname);
    grain = index.grain;
end

if isempty(par.cryst)
    fname = sprintf('4_grains/phase_%02d/index.mat', par.phase);
    fprintf('Loading crystallographic info from file %s\n', fname)
    index = load(fname);
    par.cryst = index.cryst;
end

if isempty(par.energy)
    disp('Loading beam energy from file parameters.mat.')
    parameters = load('parameters.mat');
    parameters = parameters.parameters;
    par.energy = parameters.acq.energy;
end

if isempty(par.Bmat)
    par.Bmat = gtCrystHKL2CartesianMatrix(par.cryst.latticepar);
end


par.wavelength = gtConvEnergyToWavelength(par.energy);

fprintf('Using the parameters:\n')
fprintf('  Beam energy (keV):     %g\n', par.energy)
fprintf('  Beam wavelength (A):   %g\n', par.wavelength)
fprintf('  Error filter:          %g%%\n', par.errfilt*100)
fprintf('  Strain outliers (std): %g\n', par.stdlim)


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Run strain fitting
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp('Running strain fitting...')

parfor ii = 1:length(grain)
    
    % Strain fitting (returns 'strain' structure)
    grain{ii}.strain = gtStrainSingleGrain(grain{ii}, par.cryst, ...
                       par.wavelength, par.Bmat, par.errfilt);
    
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Filter outliers
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Grains with non coplanar plane normals
ok = ~gtIndexAllGrainValues(grain,'strain','coplanar',1);

% Statistics on principal strain values
med_psmax = median(gtIndexAllGrainValues(grain(ok),'strain','pstrain',1));
med_psmin = median(gtIndexAllGrainValues(grain(ok),'strain','pstrain',3));

std_psmax = std(gtIndexAllGrainValues(grain(ok),'strain','pstrain',1));
std_psmin = std(gtIndexAllGrainValues(grain(ok),'strain','pstrain',3));

% Grains with strain uut of range
psout = ((gtIndexAllGrainValues(grain,'strain','pstrain',1) > med_psmax + par.stdlim*std_psmax) | ...
         (gtIndexAllGrainValues(grain,'strain','pstrain',3) < med_psmin - par.stdlim*std_psmin));


for ii = 1:length(grain)
    
    if (grain{ii}.strain.coplanar || psout(ii))        
        
        grain{ii}.strain.strainT(:)    = NaN;
        grain{ii}.strain.Eps(:)        = NaN;
        grain{ii}.strain.pstrain(:)    = NaN;
        grain{ii}.strain.pstraindir(:) = NaN;
    end
    
    if grain{ii}.strain.coplanar
        fprintf('Planes are coplanar in grain no.    %d\n',ii)
    end
    
    if psout(ii)
        fprintf('Strain is out of range in grain no. %d\n',ii)
    end
    
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Creat output
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

strain.grain = grain;

% Grain indices with coplanar plane normals
strain.copl = find(gtIndexAllGrainValues(grain,'strain','coplanar',1));

% Grain indices with strain out of range
strain.out  = find(psout);

% Store all strain tensors in an array as well
strain.strainT        = zeros(3, 3, length(grain));
strain.strainT(1,1,:) = gtIndexAllGrainValues(grain,'strain','strainT', 1,1);
strain.strainT(1,2,:) = gtIndexAllGrainValues(grain,'strain','strainT', 1,2);
strain.strainT(1,3,:) = gtIndexAllGrainValues(grain,'strain','strainT', 1,3);
strain.strainT(2,1,:) = gtIndexAllGrainValues(grain,'strain','strainT', 2,1);
strain.strainT(2,2,:) = gtIndexAllGrainValues(grain,'strain','strainT', 2,2);
strain.strainT(2,3,:) = gtIndexAllGrainValues(grain,'strain','strainT', 2,3);
strain.strainT(3,1,:) = gtIndexAllGrainValues(grain,'strain','strainT', 3,1);
strain.strainT(3,2,:) = gtIndexAllGrainValues(grain,'strain','strainT', 3,2);
strain.strainT(3,3,:) = gtIndexAllGrainValues(grain,'strain','strainT', 3,3);


% Mean and median values
ind = ~isnan(strain.strainT(1,1,:));
strain.strainTmean = mean(strain.strainT(:,:,ind),3);
strain.strainTmed  = median(strain.strainT(:,:,ind),3);

disp(' ')
disp('Mean strain tensor:')
disp(strain.strainTmean)
disp('Median strain tensor:')
disp(strain.strainTmed)


% Strain parameters
strain.par = par;

toc


% Plot histograms
gtStrainPlotCompHists(strain.strainT);


% Save results
disp(' ')
if par.save
    fname0 = sprintf('4_grains/phase_%02d/strain.mat', par.phase);
    fname1 = gtLastFileName(sprintf('4_grains/phase_%02d/strain_%s_',...
                par.phase, date), 'new');
    disp('Saving results in:')
    disp(fname0)
    disp(fname1)
    save(fname0, 'strain')
    save(fname1, 'strain')
else
    disp('Saving is disabled, results are not saved.')
end



end % of function