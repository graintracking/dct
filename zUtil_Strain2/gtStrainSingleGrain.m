function strain = gtStrainSingleGrain(grain, cryst, lambda, Bmat, errfilt)
% GTSTRAINSINGLEGRAIN  Computes the strain tensor for a single grain.
%  
%   strain = gtStrainSingleGrain(grain, cryst, lambda, Bmat, errfilt)
% 
% Computes the average elastic strain tensor for a single indexed grain
% (crystal system is irrelevant). The 6 strain matrix components are
% computed in the same reference as the plane normals. Small deformations
% are assumed: the strain tensor is the approximated as the symmetric part 
% of the deformation gradient tensor. No further approximation is used.
%
% The best fraction of Friedel pairs (determined by 'errfilt')
% based on their path distance from the center of mass of the grain is 
% used for the fitting. Additional pairs are added if needed to avoid all 
% plane normals being coplanar. If all plane normals are coplanar, 
% there is no strain sensitivity out of that plane, and the results
% are expected to be poor.
%
% The strain components are found by fitting the interplanar angles and 
% interplanar distances from the pairs against an undeformed reference 
% crystal. Currently there is no weighting of the contribution from the 
% different pairs.
%
% The function 'lsqnonlin' from Matlab's Optimization Toolbox is used for
% the fitting.
%
%
% INPUT
%   grain   - a single grain data as from Indexter
%   cryst   - crystallography data as in index.mat file or parameters file
%   lambda  - wavelength of the X-ray beam
%   Bmat    - matrix to transform HKL indices into Cartesian coordinates
%             (if empty, it will be calculated inside the function)
%   errfilt - error filter for pairs (0 < errfilt <= 1); this best fraction
%             of Friedel pairs will be used for error fitting; 
%             e.g. 0.75 meaning the best 75% based on their path distance 
%             from the grain center of mass
%
%   Either 'grain.hklsp' or 'cryst.shklfam' field is required.
%
% OUTPUT
%   strain.
%     strainT    - symmetric small strain tensor
%     Eps        - strain components in 'strainT' as a vector
%                  [ST(1,1) ST(2,2) ST(3,3) ST(2,3) ST(1,3) ST(1,2)]
%     pstrain    - the three principal strain values in descending order
%     pstraindir - the three principal directions as column vectors
%                  corresponding to 'pstrain'
%     coplanar   - true if the all the plane normals in the grain are close
%                  to being in the same plane (poor results expected)
%     plfitted   - true for the plane normals which were used for the
%                  fitting
%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Parameters to use
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Minimum number of Friedel pairs to be used:
minn = 5;

% Lower limit for the determinant value that estimates how coplanar the
% fitted plane normals are. Should be a value significantly larger than 0.
detlim = 1;

% Initial guess values for fitting the 6 strain components
scguess = [0 0 0 0 0 0];

% Lower and upper bounds for the strain components solution
boundslow  = -0.1*ones(1,6); 
boundshigh =  0.1*ones(1,6);

% For Matlab's 'lsqnonlin' optimization routine
par.tolx     = 1e-6;  % precision for components (1e-6 is reasonable and fast)
par.tolfun   = 1e-12; % optimization function target value (should be very low)
par.typicalx = 1e-5*ones(1,6); % typical component values for calculating gradients


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Filter out the worst pairs
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Plane indices used for strain fitting
plind = false(size(grain.hklind));


% Filter which plane normals to use
if (~exist('errfilt','var') || isempty(errfilt) || (errfilt >= 1) || (errfilt <= 0))
    
    % Use all
    plind(:) = true;
    
    % Check that plane normals are not all in one plane (determinant is
    % significantly larger than 0 or some tolerance)
    detpl = det(grain.pl*grain.pl');
    if (detpl >= detlim)
        coplanar = false;
    else
        coplanar = true;
    end

else
    
    % Sort pairs according to distance from center of mass
    [~, ind] = sort(grain.dcom);
    
    pl = grain.pl(:,ind);
    
    % Find highest index, but at least 'minn' and not more than length
    maxind = max(round(errfilt*length(grain.dcom)), minn);
    maxind = min(maxind, length(grain.dcom));
    
    coplanar = true;
    go = true;
   
    while go
       
       % Check that plane normals are not all in one plane (determinant is 
       % significantly larger than 0 or some tolerance) 
       detpl = det(pl(:,1:maxind)*pl(:,1:maxind)');
       
       if (detpl >= detlim)
           coplanar = false;
           go = false;
       elseif (maxind >= length(grain.dcom))
           go = false;
       else
           maxind = maxind + 1;
       end
    end
    
    plind(ind(1:maxind)) = true;
end


hklind  = grain.hklind(plind);
shklind = grain.shklind(plind);
theta   = grain.theta(plind);
pl      = grain.pl(:,plind);


% All measured signed hkl-s in the grain 
if isfield(grain,'hklsp')
    shkl = grain.hklsp(:,plind);
else
    shkl = [];
    for ii = 1:length(hklind)
        shkl(:,ii) = cryst.shklfam{hklind(ii)}(shklind(ii),:)';
    end
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Properties of undeformed reference crytal
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Measured d-spacings
dsp = 0.5*lambda./sind(theta);

% D-spacings in the undeformed reference crystal
dsp_ref = cryst.dspacing(hklind);

% Required relative elongations to achieve unstrained state from deformed state
elo_ref = dsp_ref./dsp;


% Get B matrix for hkl -> Cartesian coordinate transforms
if isempty(Bmat)
    Bmat = gtCrystHKL2CartesianMatrix(cryst.latticepar);
end

% Cartesian coordinates of plane normals in the unstrained reference crystal
% (output plane normals are normalized)
pl_ref = gtCrystHKL2Cartesian(shkl, Bmat);

% All combinations of plane normal dot products in the unstrained reference crystal
dotpro_ref = abs(pl_ref'*pl_ref);
dotpro_ref(dotpro_ref>1) = 1;  % correction for numerical errors


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Run optimization routine
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Find the "unstrain tensor" components 'sc' that would bring the
% observed, deformed grain into the undeformed reference state. Note that
% rotations have no effect on the interplanar angles or distances,
% therefore not needed for the fitting.
% Using Matlab's built-in non-linear least square solver. 
scopt = lsqnonlin( @(sc) sfDeviation(sc, pl, elo_ref, dotpro_ref),...
        scguess, boundslow, boundshigh,...
        optimset('TolX',par.tolx, 'TolFun',par.tolfun,...
        'TypicalX',par.typicalx, 'FunValCheck','off', 'Display','off'));

%[scopt, resnorm, residual, exitflag, optimout, lagrange, jacobian] = ...
%    lsqnonlin( @(sc) sfDeviation(sc, pl, elo_ref, dotpro_ref),...
%    scguess, boundslow, boundshigh,...
%    optimset('TolX',par.tolx, 'TolFun',par.tolfun,...
%    'TypicalX',par.typicalx, 'FunValCheck','off', 'Display','off'));


% "Unstrain tensor"
STundef = [scopt(1) scopt(6) scopt(5) ; ...
           scopt(6) scopt(2) scopt(4) ; ...
           scopt(5) scopt(4) scopt(3)];

I = [1 0 0; 0 1 0; 0 0 1];

% The real strain tensor is a kind of inverse of STundef
ST = inv(I + STundef) - I;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Principal strains
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

strain.strainT = ST;
strain.Eps     = [ST(1,1) ST(2,2) ST(3,3) ST(2,3) ST(1,3) ST(1,2)];

% Principal strains - eigenvalues and eigenvectors
[ivec, ival] = eig(ST);

[ivalsort, ivalinds] = sort([ival(1,1), ival(2,2), ival(3,3)], 2, 'descend');

% principal strain values ordered
strain.pstrain = ivalsort;

% principal strain directions in columns
strain.pstraindir = ivec(:, ivalinds);

% Plane normals coplanar
strain.coplanar = coplanar;

% Plane indices used for strain
strain.plfitted = plind;


end % of main function



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Sub-functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Gives a vector of the deviations in interplanar angles and distances 
% as a function of the 6 strain components 'sc'. These differences
% should be eliminated by finding the right 'sc' in the fitting. 


function dev = sfDeviation(sc, pl0, elo_ref, dotpro_ref)

    ST = [sc(1) sc(6) sc(5) ; ...
          sc(6) sc(2) sc(4) ; ...
          sc(5) sc(4) sc(3)];
    
    % 'pl0' is the as observed plane normals; 'pl' is 'pl0' after
    % deformation 'ST'
    [pl, elo] = gtStrainPlaneNormals(pl0, ST);
    
    
    % Difference in elongation between computed and reference  
    elodiff = elo - elo_ref;
    
    
    % Difference in dot products between computed and reference
    dotprodiff = abs(pl'*pl) - dotpro_ref;
    
    % Take upper triangle of matrix (this avoids loops and seems to be 
    % the fastest way)
    inds       = triu(true(size(dotprodiff)),1);
    dotprodiff = dotprodiff(inds);
    
    
    % All differences in one vector
    dev = [elodiff(:); dotprodiff];

end



