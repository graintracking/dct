function hf = gtStrainPlotCompHists(inp, varargin)
% Plots histograms of the six strain components of all grains.

par.fontsize   = 12;
par.edgecolor  = [0.28 0.17 0.11]; %'w';
par.facecolor  = [0.28 0.17 0.11];
par.linecolor  = 'r';
par.boundnorm  = [-0.01 0.01];
par.boundshear = [-0.01 0.01];
par.binnorm    = 0.0002;
par.binshear   = 0.0002;
%par.ticks      = -0.05 : 0.002 : 0.05;

par = parse_pv_pairs(par, varargin);



if iscell(inp)
    % Store all strain tensors in an array
    st        = zeros(3, 3, length(inp));
    st(1,1,:) = gtIndexAllGrainValues(inp,'strain','strainT', 1,1);
    st(1,2,:) = gtIndexAllGrainValues(inp,'strain','strainT', 1,2);
    st(1,3,:) = gtIndexAllGrainValues(inp,'strain','strainT', 1,3);
    st(2,1,:) = gtIndexAllGrainValues(inp,'strain','strainT', 2,1);
    st(2,2,:) = gtIndexAllGrainValues(inp,'strain','strainT', 2,2);
    st(2,3,:) = gtIndexAllGrainValues(inp,'strain','strainT', 2,3);
    st(3,1,:) = gtIndexAllGrainValues(inp,'strain','strainT', 3,1);
    st(3,2,:) = gtIndexAllGrainValues(inp,'strain','strainT', 3,2);
    st(3,3,:) = gtIndexAllGrainValues(inp,'strain','strainT', 3,3);

elseif isnumeric(inp)
    st = inp;
end


hf = figure('name','Histograms of strain components','Units','normalized',...
            'Position',[0 0.5 0.5 0.5]);

hs(1) = subplot(2,3,1);
[minv(1), maxv(1)] = sfDrawHist(st, 1,1,'\epsilon_{11}', par);

hs(2) = subplot(2,3,2);
[minv(2), maxv(2)] = sfDrawHist(st, 2,2,'\epsilon_{22}', par);

hs(3) = subplot(2,3,3);
[minv(3), maxv(3)] = sfDrawHist(st, 3,3,'\epsilon_{33}', par);

hs(4) = subplot(2,3,4);
[minv(4), maxv(4)] = sfDrawHist(st, 2,3,'\epsilon_{23}', par);

hs(5) = subplot(2,3,5);
[minv(5), maxv(5)] = sfDrawHist(st, 1,3,'\epsilon_{13}', par);

hs(6) = subplot(2,3,6);
[minv(6), maxv(6)] = sfDrawHist(st, 1,2,'\epsilon_{12}', par);


ylims = get(hs,'ylim');
ylims = vertcat(ylims{:});

ymin = min(ylims(:,1));
ymax = max(ylims(:,2));

set(hs, 'xlim', [min(minv), max(maxv)])


for ii = 1:6
    plot(hs(ii), [0 0], [ymin ymax], 'LineStyle', '-.', 'LineWidth', 2,...
         'Color', par.linecolor)
end


end % of main function



%%%%%%%%%%%%%%%%%%%%%%%%%
%% Draw histogram
%%%%%%%%%%%%%%%%%%%%%%%%%
function [minval,maxval] = sfDrawHist(st, i1, i2, label, par)

    if (i1 == i2)
        edges = par.boundnorm(1) : par.binnorm : par.boundnorm(2);
        bins  = (par.boundnorm(1) + par.binnorm/2) : par.binnorm : (par.boundnorm(2) - par.binnorm/2);
    else
        edges = par.boundshear(1) : par.binshear : par.boundshear(2);
        bins  = (par.boundshear(1) + par.binshear/2) : par.binshear : (par.boundshear(2) - par.binshear/2);
    end
   
    hst = histc(squeeze(st(i1,i2,:)), edges);
    hst(end) = [];
    
    bar(bins, hst, 'FaceColor',par.facecolor, 'EdgeColor',par.edgecolor)
    set(gca, 'fontunits','points', 'fontsize',par.fontsize); %, 'XTick',par.ticks)
    xlabel(label, 'fontsize', par.fontsize)
    grid on
    hold on
    
    minval = find(hst,1,'first');
    maxval = find(hst,1,'last');
    
    minval = bins(minval);
    maxval = bins(maxval);
    
end

