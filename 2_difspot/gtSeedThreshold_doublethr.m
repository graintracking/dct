function gtSeedThreshold_doublethr(first, last, workingdirectory)
% GTSEEDTHRESHOLD_DOUBLETHR  Seed thresholding for double threshold segmentation
%     gtSeedThreshold_doublethr(first, last, workingdirectory)
%     --------------------------------------------------------
%
%     It finds seeds by thresholding full images individually at the value
%     parameters.seg.thr_seed. Seeds are the single highest pixels in each
%     connected region.
%
%     Puts the seeds into a database table to be used for double threshold
%     segmention. It also puts the median background value for the full
%     images into a database table.
%
%     The segmentation script waits for the full median values table to be
%     completed before starting. For this reason complete the table even
%     if not subtracting the background, doesn't waste that much effort
%
%
% Version 002 May-2012 by P.Reischig
%   Update to new parameters names; use of bwconncomp instead of bwlabel for
%   labelling images; cleaning
%
% Modified by Nicola Vigano', 2012, nicola.vigano@esrf.fr
%   Added EDF header recycling
%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% setup
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if isdeployed
    global GT_DB
    global GT_MATLAB_HOME
    load('workspaceGlobal.mat');
    first = str2double(first);
    last  = str2double(last);
end % special case for running interactively in current directory

if ~exist('workingdirectory','var')
    workingdirectory = pwd;
end

fprintf('Changing directory to %s\n',workingdirectory)
cd(workingdirectory)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% load and unpack parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
parameters = [];
load('parameters.mat');

table_seeds          = sprintf('%sseeds', parameters.acq.name);
table_fullmedianvals = sprintf('%sfullmedianvals', parameters.acq.name);

gtDBConnect();

info = [];
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% main loop through images
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for zz = first:last
    % NaN argument means this returns the greyscale image without thresholding
    [im, med_val, info] = gtReadAndThreshold(zz, parameters, NaN, true, info);

    % while doing this, fill in the fullmedianvals table
    % if we are not subtracting the median, med_val is returned as NaN -
    % this can't be inserted, so change it to zero
    if isnan(med_val)
        med_val=0;
    end
    mym(sprintf('UPDATE %s SET val=%0.20g WHERE ndx=%d', ...
        table_fullmedianvals, med_val, zz));

    % thr_seed threshold
    thr_im = im > parameters.seg.thr_seed;
    thr_im = bwmorph(thr_im, 'open');

    % label (2D only)
    % bwconncomp gives:
    %  labels.NumObjects       - number of labels
    %  labels.PixelIdxList{ii} - linear indices in input image for label ii
    labels = bwconncomp(thr_im);

    % pick out seeds
    list = [];

    for ii = 1:labels.NumObjects
        % marker size test
        if (length(labels.PixelIdxList{ii}) > parameters.seg.seedminarea)

            [graylevel, index] = max(im(labels.PixelIdxList{ii}));

            [yy, xx] = ind2sub(size(im), labels.PixelIdxList{ii}(index));

            list(end+1, 1:5) = [xx yy zz graylevel 0];
            % could also add the bounding boxes of the seeds
        end
    end

    % write seeds to database
    % seedID x y z value bad
    if (size(list, 1) > 0)
        gtDBInsertMultiple( table_seeds, ...
                          'x', list(:,1)', 'y', list(:,2)', 'z', list(:,3)', ...
                          'graylevel', list(:,4)', 'bad', list(:,5)');
    end
end % end for

end % of main function

