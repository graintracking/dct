function gtConvertSpots2Blobs(acq_name, first, last)
    if (~exist('first', 'var'))
        first = 1;
    end
    difblob_num = mym(['select count(*) from ' acq_name 'difblob']);
    if (~exist('last', 'var'))
        last = difblob_num;
    end

    stats = GtTasksStatistics();
    stats.add_task('load_blob', 'Time to load from DB');
    stats.add_task('save_blob', 'Time to save to HDF5');
    stats.add_task('convert_blob', 'Time for converting from DB to HDF5')

    fprintf('Progress: ')
    c = tic();
    for ii = first:last
        num_chars = fprintf('%06d/%06d (%s)', ii, (last - first + 1), stats.get_formatted_total('convert_blob'));
        stats.tic('convert_blob')
        stats.tic('load_blob')
        [blob, bb] = gtDBBrowseDiffractionVolume(acq_name, ii);
        stats.toc('load_blob')

        stats.tic('save_blob')
        gtWriteBlobToHDF5('2_difblob', ii, blob, bb);
        stats.toc('save_blob')
        stats.toc('convert_blob')

        fprintf(repmat('\b', [1 num_chars]))
    end
    fprintf('%06d Done in %f seconds.\n', (last - first + 1), toc(c));
end
