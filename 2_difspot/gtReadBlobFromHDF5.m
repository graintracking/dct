function [blob_vol, blob_bb, blob_mask, blob_cont] = gtReadBlobFromHDF5(blob_dir, blob_id)
% [blob_vol, blob_bb, blob_mask] = gtReadBlobFromHDF5(blob_dir, blob_id)

    if (~exist('blob_dir', 'var') || isempty(blob_dir))
        blob_dir = '2_difblob';
    end
    sub_dir = fullfile(blob_dir, sprintf('%06d', blob_id - mod(blob_id, 1e4)));
    fnameout = fullfile(sub_dir, sprintf('difblob_%06d.hdf5', blob_id) );
    if (isempty(dir(fnameout)))
        error('gtReadBlobFromHDF5:file_not_existing', ...
            'The file: %s does not exist!', fnameout);
    end

    blob_vol = h5read(fnameout, '/rawblob');
    if (nargout >= 2)
        blob_bb = h5readatt(fnameout, '/rawblob', 'bb');
        blob_bb = [blob_bb(2:3:end), blob_bb(1:3:end), blob_bb(3:3:end)];
        blob_bb = double([blob_bb(1, :), blob_bb(2, :)]);
    end
    if (nargout >= 3)
        blob_mask = h5read(fnameout, '/mask');
        blob_mask = logical(blob_mask);
    end
    if (nargout >= 4)
        blob_cont = bwperim(blob_mask, 8);
    end
end