function gtConvertRawImages2Blobs(first, last, workingdirectory, varargin)
% GTCONVERTRAWIMAGES2BLOBS Loads diff blobs from database and saves them as
% hdf5 files. 
%
% gtConvertRawImages2Blobs(first, last, workingdirectory, varargin)
%
% To speed up, run it in parallel using OAR (gtOarLaunch).


if (isdeployed)
    global GT_DB
    global GT_MATLAB_HOME
    load('workspaceGlobal.mat')
end

cd(workingdirectory);
gtDBConnect();
parameters = gtLoadParameters();

if (~exist('first', 'var') || isempty(first))
    first = 1;
end
difblob_num = mym(['select count(*) from ' parameters.acq.name 'difblob']);
if (~exist('last', 'var') || isempty(last))
    last = difblob_num;
end

if (isdeployed)
    first   = str2double(first);
    last    = str2double(last);
end

conf = struct('list', first:last, 'dilate', [5 5 0]);
conf = parse_pv_pairs(conf, varargin);

info = edf_info(fullfile('1_preprocessing', 'full', 'full0000.edf'));
totproj = gtAcqTotNumberOfImages(parameters);
stats = GtTasksStatistics();
stats.add_task('load_blob', 'Time to load from DB');
stats.add_task('load_mask', 'Time to load mask');
stats.add_task('save_blob', 'Time to save to HDF5');
stats.add_task('convert_blob', 'Time for converting from DB to HDF5')

fprintf('Progress: ')
c = tic();
for ii = conf.list
    num_chars = fprintf('%06d/%06d (%s)', ii, numel(conf.list), ...
        stats.get_formatted_total('convert_blob'));
    stats.tic('convert_blob')
    stats.tic('load_mask')
    [mask, bb] = get_mask('2_difspot', ii, parameters, conf);
    mask(mask < 0) = 0;
    stats.toc('load_mask')
    
    stats.tic('load_blob')
    blob = get_blob(bb, info, totproj);
    blob(blob < 0) = 0;
    stats.toc('load_blob')
    
    stats.tic('save_blob')
    gtWriteBlobToHDF5('2_difblob', ii, blob, bb, mask);
    stats.toc('save_blob')
    stats.toc('convert_blob')
    
    fprintf(repmat('\b', [1 num_chars]))
end
fprintf('%06d Done in %f seconds.\n', numel(conf.list), toc(c));
stats.printStats()

end

function blob = get_blob(bb, info, totproj)
    for s_ii = bb(6):-1:1
        image_num = mod(bb(3) - 1 + s_ii, totproj);
        filename = fullfile('1_preprocessing', 'full', sprintf('full%04d.edf', image_num));
        GT_bb = [bb(1), bb(2), bb(4), bb(5)];
        blob(:, :, s_ii) = edf_read(filename, GT_bb, false, info);
    end
end

function [mask, bb] = get_mask(spot_dir, blob_id, parameters, conf)

    if (parameters.seg.writeblobs)
        [mask, bb] = gtDBBrowseDiffractionVolume(parameters.acq.name, blob_id);
        mask = mask ~= 0;
    else
        table_difspot = sprintf('%sdifspot', parameters.acq.name);

        colnames(1, :) = { 'Integral', 'StartImage', 'EndImage' ...
            'BoundingBoxXorigin', 'BoundingBoxYorigin', ...
            'BoundingBoxXsize', 'BoundingBoxYsize',  };
        colnames(2, :) = {', '};

        query = sprintf('SELECT %s FROM %s WHERE difspotID = %d', ...
            [colnames{1:end-1}], table_difspot, blob_id );

        [~, bb(3), bb(6), bb(1), bb(2), bb(4), bb(5)] = mym(query);
        if (bb(6) < bb(3))
            bb(6) = parameters.acq.nproj * 2 + bb(6) - bb(3) + 1;
        else
            bb(6) = bb(6) - bb(3) + 1;
        end

        sub_dir = fullfile(spot_dir, sprintf('%05d', blob_id - mod(blob_id, 1e4)));
        filename = fullfile(sub_dir, sprintf('difspot%05d.edf', blob_id));
        mask = edf_read(filename) ~= 0;
        mask = mask(:, :, ones(1, bb(6)));
    end

    if (any(conf.dilate > 0))
        img_size = [parameters.acq.xdet parameters.acq.ydet];
        % What about enlarging it? Let's do a fixed 3-5 pixels?
        num_pixels = conf.dilate;

        new_blob_bb = bb + [-1 -1 -1 2 2 2] .* repmat(num_pixels, 1, 2);

        new_limits = [new_blob_bb(1:3), new_blob_bb(1:3)+new_blob_bb(4:6)-1];

        chopped_limits = [ ...
            max(new_limits(1:2), [1 1]), new_limits(3), ...
            min(new_limits(4:5), img_size), new_limits(6) ];

        chopped_bb = [chopped_limits(1:3), ...
            chopped_limits(4:6)-chopped_limits(1:3)+1 ];

        shifts = [(num_pixels(1:2) - (chopped_bb(1:2) - new_blob_bb(1:2))), 0];

        new_blob_mask = gtPlaceSubVolume( ...
            zeros([chopped_bb(5), chopped_bb(4), chopped_bb(6)], 'uint8'), ...
            uint8(mask), shifts);
        if (num_pixels(3))
            new_blob_mask = imdilate(new_blob_mask, strel('ball', num_pixels(1), num_pixels(3)));
        else
            new_blob_mask = imdilate(new_blob_mask, strel('disk', num_pixels));
        end

        bb = chopped_bb;
        mask = logical(new_blob_mask);
    end
end


