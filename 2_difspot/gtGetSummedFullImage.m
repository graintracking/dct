function [im, uv]=gtGetSummedFullImage(n, varargin)
% GTGETFULLIMAGE  GET SUMMED FULLS CONTAIN DIFSPOT N
%                         
%     im = =gtGetSummedFullImage(n,varargin)
%     -----------------------------------------------
%     Read and sum all full images in which difspot n appears.
%     Option second argument is parameters.  If this is passed in then
%     parameters.acq.dir will be used as the path.  Otherwise, the function
%     will assume you are in the parameters.acq.dir directory and work
%     relative to this.
%
%     INPUT:
%       n = difspot ID number
%       parameters {optional}
%
%     OUTPUT:
%      summed full image
%      the uv position of the spot centre

gtDBConnect();

if length(varargin)==1
    parameters=varargin{1};
else
  disp('Loading parameter file')
  load('parameters.mat');
end
dir=parameters.acq.dir;

im=zeros(parameters.acq.ydet, parameters.acq.xdet);
[EndIm,StartIm,x,y]=mym(sprintf('select EndImage,StartImage,CentroidX, CentroidY from %sdifspot where difspotID=%d',parameters.acq.name,n));
uv=[x,y];

disp( [num2str(EndIm-StartIm+1), ' images in sum'])
for i=StartIm:EndIm
  im = im + edf_read(fullfile(dir, '1_preprocessing/full', sprintf('full%04d.edf',i)));;
end

end
