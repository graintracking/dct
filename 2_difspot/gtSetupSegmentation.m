function gtSetupSegmentation(parameters)
% GTSETUPSEGMENTATION  Interactive GUI to set the segmentation parameters
%     (parameters.seg) for 'single threshold' segmentation.
%
%     gtSetupSegmentation()
%     ---------------------
%
%     After the parameters have been set interactively based on a few
%     sample images from the dataset, the parameters can be saved and the
%     segmentation launched.
%
%     Uses a compiled segmentation function distributed on the cluster by
%     gtOarLaunch.
%
%     Comments:
%     Copy interface from gtSetupSegmentation_doublethr, which is much
%     nicer to use. Can simplify a bit because the single threshold
%     calculation is so much faster if not labelling.
%
%
%     Version 002 08-05-2012 by P.Reischig
%       Updated to new parameters (thr_single) and cleaned.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% load('parameters.mat'); get seg data
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if (~exist('parameters', 'var') || isempty(parameters))
    parameters = gtLoadParameters();
end
parameters_name = fullfile(parameters.acq.dir, 'parameters.mat');

check = inputwdefault('Do you want to reset all the parameters for segmentation? [y/n]', 'n');
if strcmpi(check,'y')
    parameters.seg = gtSegDefaultParameters();
    save(parameters_name,'parameters');
    disp('Segmentation parameters have been reset to default values')
end

% Let's check every field is in, and in case let's update it!
parameters = gtCheckParameters(parameters, 'seg', 'verbose', true);
gtSaveParameters(parameters)

disp(' ')
gtDBConnect();
disp(' ')

% get the seg.bbox if it exists, or guess it from acq.bb
if isempty(parameters.seg.bbox)
    disp('Segmentation bounding box is not defined. Setting it equal to')
    disp('sample bounding box (parameters.acq.bb).')
    parameters.seg.bbox = parameters.acq.bb;
end

% set default for subtraction of median background
parameters.seg.background_subtract = true;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% recompile for condor?
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
check = inputwdefault('Recompile gtSegmentDiffractionBlobs? (not normally needed) [y/n]', 'n');
if strcmpi(check, 'y')
    gtExternalCompileFunctions('gtSegmentDiffractionBlobs');
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Do we already have data to overwrite
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
test = mym(['select count(*) from ' parameters.acq.name 'difblob']);
if test > 0
    disp(' ')
    disp('WARNING: Difblob data already exists in table!')
    check = inputwdefault('Recreate tables deleting this data? [y/n]', 'n');
    if strcmpi(check, 'y')
        overwrite = 1;
        gtDBCreateDifblobTable(parameters.acq.name, overwrite);
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% how many images in this scan?
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
totproj = gtAcqTotNumberOfImages(parameters);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Safety check - are all the images present?
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
d = dir(fullfile(parameters.acq.dir,'1_preprocessing/full/full*edf'));
test_full = length(d);
target_ims = (totproj+1);
if (test_full < target_ims)
    % some fulls are missing!
    fprintf('!!! Not all full images are present (%d found, %d expected) \n', test_full, target_ims)
    disp('This could be normal if you are processing "live" and still producing images');
    check = inputwdefault('Quit gtPreprocessing? [y/n]', 'y');
    if strcmpi(check, 'y')
        % quit, but first check what other images are missing
        disp('quitting - first checking what images are present....')
        d=dir(fullfile(parameters.acq.dir,'1_preprocessing/abs/abs*edf'));
        test_abs=length(d);
        d=dir(fullfile(parameters.acq.dir,'1_preprocessing/abs/med*edf'));
        test_abs_med=length(d);
        d=dir(fullfile(parameters.acq.dir,'1_preprocessing/full/med*edf'));
        test_full_med=length(d);
        % how many should we have?
        target_abs_med=(target_ims/parameters.prep.absint)+1;
        target_full_med=(target_ims/parameters.prep.fullint)+1;
        fprintf('Found full: %d/%d; abs: %d/%d; abs median: %d/%d; full median: %d/%d\n', ...
            test_full, target_ims, test_abs, target_ims, test_abs_med, target_abs_med, test_full_med, target_full_med)
        disp('Probably some preprocessing OAR jobs have failed')
        error('Missing images!')
    end
else
    fprintf('Found all full images (%d found, %d expected) \n', test_full, totproj+1)
end

% if all images are present, check intensities in direct beam
if ~parameters.acq.no_direct_beam && test_full==target_ims
    check = inputwdefault('Check intensities in direct beam? [y/n]', 'y');
    if strcmpi(check, 'y')
        disp(['Checking every 20th image in 0_rawdata/' parameters.acq.name '...'])
        figure();
        hold on
        fint=zeros(1, target_ims);
        fbb=floor(parameters.acq.bb(1:2)+(parameters.acq.bb(3:4)/2));
        fbb(3:4)=2; % read in 2x2 pixels in centre of sample
        for ii = 1:20:target_ims
            froi=edf_read(fullfile(parameters.acq.dir,'0_rawdata', sprintf('%s/%s%04d.edf', parameters.acq.name, parameters.acq.name, ii-1)), fbb);
            fint(ii)=sum(froi(:))/4;
            plot(ii-1, fint(ii), 'gx')
            if mod(ii, 100)==1
                drawnow
            end
        end
        disp('Does the intensity look okay?')
        check = inputwdefault('Continue with segmentation? [y/n]', 'y');
        if strcmp(check, 'n')
            disp('quitting... ')
            return
        end
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% get some test images
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% wait until at least 4 images are present
listfull = dir(fullfile(parameters.acq.dir, '1_preprocessing', 'full', 'full*edf'));
while length(listfull) < 4
    disp('waiting for at least 4 full images to appear!...')
    pause(5);
    listfull = dir(fullfile(parameters.acq.dir', '1_preprocessing', 'full', 'full*edf'));
end

% put in date order
fdates = [listfull.('datenum')];
fnames = {listfull.('name')};

[~, indexes] = sort(fdates, 'ascend');
fnames = fnames(indexes);

% get 4 evenly spaced images
getfulls = [round(1:(length(fnames)/3):length(fnames)) length(fnames)];

im.full(:,:,4) = edf_read( fullfile('1_preprocessing','full',fnames{getfulls(4)}) );
im.full(:,:,1) = edf_read( fullfile('1_preprocessing','full',fnames{getfulls(1)}) );
im.full(:,:,2) = edf_read( fullfile('1_preprocessing','full',fnames{getfulls(2)}) );
im.full(:,:,3) = edf_read( fullfile('1_preprocessing','full',fnames{getfulls(3)}) );
im.full(:,:,5) = max(im.full(:,:,1:4), [], 3);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% set threshold and bb interactively
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% improve this!
hfig  = figure('menubar', 'none');
scrsz = get(0,'ScreenSize');
figsz = round(0.75*scrsz(3:4));

set(hfig, 'Position', [50 150 figsz(1) figsz(2)]);
set(hfig, 'Name', 'Setup single threshold segmentation');
set(hfig, 'windowbuttondownfcn', @sfDragBBFunction);


% setup buttons
QuitButton = uicontrol(hfig, 'Style', 'pushbutton', 'string', 'Quit', 'position', [figsz(1)-510 figsz(2)-40 150 30]);
set(QuitButton, 'callback', @sfQuitButtonFunction);

SaveButton = uicontrol(hfig, 'Style', 'pushbutton', 'string', 'Save+Quit', 'position', [figsz(1)-360 figsz(2)-40 150 30]);
set(SaveButton, 'callback', @sfSaveButtonFunction);

OARButton = uicontrol(hfig, 'Style', 'pushbutton', 'string', 'Save+Launch Seg.', 'position', [figsz(1)-210 figsz(2)-40 200 30]);
set(OARButton, 'callback', @sfOARButtonFunction);


% labels and boxes
bb_label = uicontrol(hfig, 'Style', 'text', 'String', 'Change bounding box using the mouse', 'position', [0 585 150 30]);
bb_label = uicontrol(hfig, 'Style', 'text', 'String', 'Change parameters below, click update to threshold', 'position', [0 545 150 30]);

th_label = uicontrol(hfig, 'Style', 'text', 'String', 'Threshold (thr_single)', 'position', [0 520 150 15]);
th_box   = uicontrol(hfig, 'Style', 'edit', 'String', num2str(parameters.seg.thr_single), 'position', [0 490 150 30], 'keypressfcn', @sfKeyPressFunction);

median_label    = uicontrol(hfig, 'Style', 'text', 'String', 'Subtract median background', 'position', [0 470 150 15]);
median_checkbox = uicontrol(hfig, 'Style', 'checkbox', 'Value', parameters.seg.background_subtract, 'position', [70 450 20 20], 'callback', @sfDoThresholding);


% update parameters button
ParametersButton = uicontrol(hfig, 'Style', 'pushbutton', 'string', 'Update Params', 'position', [0 400 150 30]);
set(ParametersButton, 'callback', @sfDoThresholding);


% communication string
StatusMessage = uicontrol(hfig, 'Style', 'text', 'String', 'Status: ', 'Position', [500 30 300 30]);


% Create three radio buttons in the button group.
h = uibuttongroup('visible','off','Position',[0.9 0 0.1 1]);

uicontrol(hfig, 'Style', 'text', 'String', 'Which full image to look at?  Choice of four images, or the max of the four', 'position', [figsz(1)-160 500 150 60]);

u1 = uicontrol('Style','Radio','String','Full A',...
    'pos',[10 450 100 30],'parent',h,'HandleVisibility','off');
u2 = uicontrol('Style','Radio','String','Full B',...
    'pos',[10 400 100 30],'parent',h,'HandleVisibility','off');
u3 = uicontrol('Style','Radio','String','Full C',...
    'pos',[10 350 100 30],'parent',h,'HandleVisibility','off');
u4 = uicontrol('Style','Radio','String','Full D',...
    'pos',[10 300 100 30],'parent',h,'HandleVisibility','off');
u5 = uicontrol('Style','Radio','String','Max(Fulls)',...
    'pos',[10 250 100 30],'parent',h,'HandleVisibility','off');

% Initialize some button group properties.
set(h,'SelectionChangeFcn',@sfChangeStackIndex);
set(h,'SelectedObject', u5) % start with max image
set(h,'Visible','on');

StackIndex = 5;
sfDoThresholding();

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Sub-functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % change the full image
    function sfChangeStackIndex(~, eventdata)
        set(StatusMessage, 'String', 'Changing Image...')
        drawnow();
        StackIndex = find([u1 u2 u3 u4 u5]==eventdata.NewValue);
        sfDoThresholding();
    end

    % change the full image
    function sfKeyPressFunction(~, ~)
        c = get(hfig,'CurrentCharacter');
        if c == 13
            sfDoThresholding();
        end
    end


    function sfUpdateImages(varargin)
        set(StatusMessage, 'String', 'Drawing...')
        drawnow();

        % draw the greyscale images
        subplot(1,2,1);
        try % for image toolbox licence
            hgreyim = imshow(im.full(:,:,StackIndex), gtAutolim(im.full(:,:,StackIndex)));
        catch Mexc
            gtPrintException(Mexc)
            hgreyim = imshow(im.full(:,:,StackIndex), []);
        end

        sfPlotBB();

        % draw the thresholded image
        subplot(1,2,2);
        hthreshim = imshow(im.thresh(:,:,StackIndex), []);
        sfPlotBB();
        set(StatusMessage, 'String', 'Ready')

        % open impixelinfo in upper left corner
        hfigpos = get(hfig,'Position');
        himp    = impixelinfo(hfig);
        set(himp,'Position', [10 hfigpos(4)-30 150 30]);

    end


    function sfUpdateParameters(varargin)
        % read the parameters
        parameters.seg.thr_single = str2double(get(th_box, 'String'));
        parameters.seg.background_subtract = get(median_checkbox, 'Value');
    end


    function sfDoThresholding(varargin)
        set(StatusMessage, 'String', 'Thresholding...')
        drawnow();

        % update the parameters by reading from the boxes
        sfUpdateParameters();

        % remove the median background at this point - copy paste from
        % gtReadAndThreshold
        tmpim = im.full(:,:,StackIndex);
        if isfield(parameters.seg, 'background_subtract') && ...
           (parameters.seg.background_subtract==true)
             %subtract the median of the background
             %taken from gtFullStatCondor - thanks Peter!
             nullbb    = NaN(parameters.seg.bbox(4), parameters.seg.bbox(3));
             tmpim     = gtPlaceSubImage(nullbb, tmpim, parameters.seg.bbox(1),...
                                         parameters.seg.bbox(2));
             med_value = tmpim(~isnan(tmpim(:)));
             med_value = median(med_value);
             tmpim     = tmpim-med_value;
        end

        % set bb to zeros
        tmpim(parameters.seg.bbox(2):(parameters.seg.bbox(2)+parameters.seg.bbox(4)-1), ...
              parameters.seg.bbox(1):(parameters.seg.bbox(1)+parameters.seg.bbox(3)-1)) = 0;

        % threshold
        im.thresh(:,:,StackIndex) = tmpim > parameters.seg.thr_single;
        sfUpdateImages();
    end


    function sfDragBBFunction(varargin)
        % get a new bounding box
        % how to get this to Values?
        p1 = get(gca,'CurrentPoint');
        p1 = p1(1, 1:2);

        % use rubberband box area selection
        bbox = rbbox();

        p2 = get(gca,'CurrentPoint');
        p2 = p2(1, 1:2);

        bborigin = min(p1, p2);
        bbsize   = abs(p1-p2)+1;

        % check that values are okay (no stray clicks outside image), or
        % tiny bboxes
        if any(bborigin < 1) || any((bborigin+bbsize)>[parameters.acq.xdet parameters.acq.ydet]) ...
           || any(bbsize < 5)
            return
        end

        parameters.seg.bbox = round([bborigin bbsize]);

        % redo the threshold and update the images
        sfDoThresholding();
    end


    function sfPlotBB(varargin)
        hold on

        xdata = [parameters.seg.bbox(1), ...
                 parameters.seg.bbox(1) + parameters.seg.bbox(3),...
                 parameters.seg.bbox(1) + parameters.seg.bbox(3),...
                 parameters.seg.bbox(1), ...
                 parameters.seg.bbox(1)] - 0.5;

        ydata = [parameters.seg.bbox(2),...
                 parameters.seg.bbox(2),...
                 parameters.seg.bbox(2) + parameters.seg.bbox(4),...
                 parameters.seg.bbox(2) + parameters.seg.bbox(4),...
                 parameters.seg.bbox(2)] - 0.5;

        plot(xdata, ydata, '.-r')
        hold off
    end


    function sfSaveButtonFunction(varargin)
        % write the values in Values to parameters.seg
        set(StatusMessage, 'String', 'Saving values to parameters file')
        drawnow();
        pause(0.5)
        sfUpdateParameters();
        parameters.seg.method = 'singlethr';

        save(parameters_name, 'parameters');
        sfQuitButtonFunction();
    end


    function sfOARButtonFunction(varargin)
        % save to parameters, then launch OAR
        sfUpdateParameters();
        parameters.seg.method = 'singlethr';

        % if using this function, need to remove overlapping difblobs
        parameters.seg.overlaps_removed=false;
        set(StatusMessage, 'String', 'Saving values to parameters file')
        drawnow();
        pause(0.5);
        save(parameters_name,'parameters');
        
        disp('Launching several separate OAR jobs');

        % get OAR parameters from the user
        oar.njobs    = 20;
        oar.walltime = 7200; % sec
        oar.mem      = 2000; % Mb
        
        % oar parameters are defined in build_list_v2
        list = build_list_v2();
        OAR_parameters = gtModifyStructure(oar, list.oar, 1, 'OAR parameters:');

        OARtmp = ceil(OAR_parameters.walltime / 60); % minutes
        OARm   = mod(OARtmp, 60); % minutes
        OARh   = (OARtmp - OARm) / 60; % hours

        OAR_parameters.walltime = sprintf('%02d:%02d:00', OARh, OARm);

        disp('Using OAR parameters:')
        disp(OAR_parameters)
        disp(['Using ' num2str(oar.njobs) ' OAR jobs']);
        
        set(StatusMessage, 'String', sprintf('Status: launching %d OAR jobs', OAR_parameters.njobs))
        drawnow();
        pause(0.5);

        tot_proj = gtAcqTotNumberOfImages(parameters);
        % launch the two functions
        % blob segmentation waits for seed segmentation to finish
        gtOarLaunch('gtSegmentDiffractionBlobs', 0, tot_proj, OAR_parameters.njobs, ...
                 parameters.acq.dir, true, ...
                 'walltime', OAR_parameters.walltime, 'mem', OAR_parameters.mem)

        % now quit - copy from quit button function
        set(StatusMessage, 'String', 'Quitting...')
        drawnow();

        pause(0.5);
        close(hfig);
    end


    function sfQuitButtonFunction(varargin)
        set(StatusMessage, 'String', 'Quitting...')
        drawnow();

        pause(0.5);
        close(hfig);
    end


end % end of the function
