function varargout = gtSegmentationGUI(varargin)
% GTSEGMENTATIONGUI MATLAB code for gtSegmentationGUI.fig
%      GTSEGMENTATIONGUI, by itself, creates a new GTSEGMENTATIONGUI or raises the existing
%      singleton*.
%
%      H = GTSEGMENTATIONGUI returns the handle to a new GTSEGMENTATIONGUI or the handle to
%      the existing singleton*.
%
%      GTSEGMENTATIONGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in GTSEGMENTATIONGUI.M with the given input arguments.
%
%      GTSEGMENTATIONGUI('Property','Value',...) creates a new GTSEGMENTATIONGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before gtSegmentationGUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to gtSegmentationGUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help gtSegmentationGUI

% Last Modified by GUIDE v2.5 30-Aug-2013 15:47:31


% some notes for me -
% error checking of variables can be extended to all
% need a colormap button
% need a slider
% need clim adjustment?

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @gtSegmentationGUI_OpeningFcn, ...
                   'gui_OutputFcn',  @gtSegmentationGUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before gtSegmentationGUI is made visible.
function gtSegmentationGUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to gtSegmentationGUI (see VARARGIN)

% deal with tables, etc
gtSegmentationPreGUI();

% get segmentation parameters and populate figure
handles = gtSegmentationInitialise(handles);

% load data and threshold
handles = gtSegmentationUpdate(handles);

% Choose default command line output for gtSegmentationGUI
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes gtSegmentationGUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = gtSegmentationGUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function OAR_njobs_Callback(hObject, eventdata, handles)
% hObject    handle to OAR_njobs (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of OAR_njobs as text
%        str2double(get(hObject,'String')) returns contents of OAR_njobs as a double


% --- Executes during object creation, after setting all properties.
function OAR_njobs_CreateFcn(hObject, eventdata, handles)
% hObject    handle to OAR_njobs (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function OAR_walltime_Callback(hObject, eventdata, handles)
% hObject    handle to OAR_walltime (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of OAR_walltime as text
%        str2double(get(hObject,'String')) returns contents of OAR_walltime as a double


% --- Executes during object creation, after setting all properties.
function OAR_walltime_CreateFcn(hObject, eventdata, handles)
% hObject    handle to OAR_walltime (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function u_start_Callback(hObject, eventdata, handles)
% hObject    handle to u_start (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.ROI_BB_status_label, 'string', 'ROI changed', 'backgroundColor', 'y')
handles.roi_changed=true;
guidata(hObject,handles);
% Hints: get(hObject,'String') returns contents of u_start as text
%        str2double(get(hObject,'String')) returns contents of u_start as a double


% --- Executes during object creation, after setting all properties.
function u_start_CreateFcn(hObject, eventdata, handles)
% hObject    handle to u_start (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function u_end_Callback(hObject, eventdata, handles)
% hObject    handle to u_end (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.ROI_BB_status_label, 'string', 'ROI changed', 'backgroundColor', 'y')
handles.roi_changed=true;
guidata(hObject,handles);
% Hints: get(hObject,'String') returns contents of u_end as text
%        str2double(get(hObject,'String')) returns contents of u_end as a double


% --- Executes during object creation, after setting all properties.
function u_end_CreateFcn(hObject, eventdata, handles)
% hObject    handle to u_end (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function v_start_Callback(hObject, eventdata, handles)
% hObject    handle to v_start (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.ROI_BB_status_label, 'string', 'ROI changed', 'backgroundColor', 'y')
handles.roi_changed=true;
guidata(hObject,handles);
% Hints: get(hObject,'String') returns contents of v_start as text
%        str2double(get(hObject,'String')) returns contents of v_start as a double


% --- Executes during object creation, after setting all properties.
function v_start_CreateFcn(hObject, eventdata, handles)
% hObject    handle to v_start (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function v_end_Callback(hObject, eventdata, handles)
% hObject    handle to v_end (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.ROI_BB_status_label, 'string', 'ROI changed', 'backgroundColor', 'y')
handles.roi_changed=true;
guidata(hObject,handles);
% Hints: get(hObject,'String') returns contents of v_end as text
%        str2double(get(hObject,'String')) returns contents of v_end as a double


% --- Executes during object creation, after setting all properties.
function v_end_CreateFcn(hObject, eventdata, handles)
% hObject    handle to v_end (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function image_start_Callback(hObject, eventdata, handles)
% hObject    handle to image_start (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.ROI_BB_status_label, 'string', 'ROI changed', 'backgroundColor', 'y')
handles.roi_changed=true;
guidata(hObject,handles);
% Hints: get(hObject,'String') returns contents of image_start as text
%        str2double(get(hObject,'String')) returns contents of image_start as a double


% --- Executes during object creation, after setting all properties.
function image_start_CreateFcn(hObject, eventdata, handles)
% hObject    handle to image_start (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function image_end_Callback(hObject, eventdata, handles)
% hObject    handle to image_end (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.ROI_BB_status_label, 'string', 'ROI changed', 'backgroundColor', 'y')
handles.roi_changed=true;
guidata(hObject,handles);
% Hints: get(hObject,'String') returns contents of image_end as text
%        str2double(get(hObject,'String')) returns contents of image_end as a double


% --- Executes during object creation, after setting all properties.
function image_end_CreateFcn(hObject, eventdata, handles)
% hObject    handle to image_end (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function thr_grow_low_Callback(hObject, eventdata, handles)
% hObject    handle to thr_grow_low (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% update the minor figure, but don't relaunch the full double threshold
handles = gtSegmentationUpdateROIimages(handles);
set(handles.status_label, 'string', 'Threshold changed', 'backgroundColor', 'y')
handles.thresholds_changed = true;
guidata(hObject,handles);
% Hints: get(hObject,'String') returns contents of thr_grow_low as text
%        str2double(get(hObject,'String')) returns contents of thr_grow_low as a double


% --- Executes during object creation, after setting all properties.
function thr_grow_low_CreateFcn(hObject, eventdata, handles)
% hObject    handle to thr_grow_low (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function thr_grow_high_Callback(hObject, eventdata, handles)
% hObject    handle to thr_grow_high (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% update the minor figure, but don't relaunch the full double threshold
handles = gtSegmentationUpdateROIimages(handles);
set(handles.status_label, 'string', 'Threshold changed', 'backgroundColor', 'y')
handles.thresholds_changed = true;
guidata(hObject,handles);
% Hints: get(hObject,'String') returns contents of thr_grow_high as text
%        str2double(get(hObject,'String')) returns contents of thr_grow_high as a double


% --- Executes during object creation, after setting all properties.
function thr_grow_high_CreateFcn(hObject, eventdata, handles)
% hObject    handle to thr_grow_high (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function seed_thr_Callback(hObject, eventdata, handles)
% hObject    handle to seed_thr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% update the minor figure, but don't relaunch the full double threshold
handles = gtSegmentationUpdateROIimages(handles);
set(handles.status_label, 'string', 'Threshold changed', 'backgroundColor', 'y')
handles.thresholds_changed = true;
guidata(hObject,handles);
% Hints: get(hObject,'String') returns contents of seed_thr as text
%        str2double(get(hObject,'String')) returns contents of seed_thr as a double


% --- Executes during object creation, after setting all properties.
function seed_thr_CreateFcn(hObject, eventdata, handles)
% hObject    handle to seed_thr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function seed_min_area_Callback(hObject, eventdata, handles)
% hObject    handle to seed_min_area (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% update the minor figure, but don't relaunch the full double threshold
handles = gtSegmentationUpdateROIimages(handles);
set(handles.status_label, 'string', 'Threshold changed', 'backgroundColor', 'y')
handles.thresholds_changed = true;
guidata(hObject,handles);
% Hints: get(hObject,'String') returns contents of seed_min_area as text
%        str2double(get(hObject,'String')) returns contents of seed_min_area as a double


% --- Executes during object creation, after setting all properties.
function seed_min_area_CreateFcn(hObject, eventdata, handles)
% hObject    handle to seed_min_area (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit23_Callback(hObject, eventdata, handles)
% hObject    handle to edit23 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit23 as text
%        str2double(get(hObject,'String')) returns contents of edit23 as a double


% --- Executes during object creation, after setting all properties.
function edit23_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit23 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit24_Callback(hObject, eventdata, handles)
% hObject    handle to edit24 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit24 as text
%        str2double(get(hObject,'String')) returns contents of edit24 as a double


% --- Executes during object creation, after setting all properties.
function edit24_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit24 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in subtract_median.
function subtract_median_Callback(hObject, eventdata, handles)
% hObject    handle to subtract_median (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

set(handles.ROI_BB_status_label, 'string', 'subtract median changed', 'backgroundColor', 'y')
handles.roi_changed=true;
handles.thresholds_changed = true;
guidata(hObject,handles);
% Hint: get(hObject,'Value') returns toggle state of subtract_median


% --- Executes on button press in recalculate_button.
function recalculate_button_Callback(hObject, eventdata, handles)
% hObject    handle to recalculate_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.status_label, 'string', 'Thresholding started...', 'backgroundColor', 'r')
handles = gtSegmentationRecalculate(handles);
guidata(hObject,handles);


% --- Executes on button press in launch_button.
function launch_button_Callback(hObject, eventdata, handles)
% first, save the parameters
handles = gtSegmentationSaveParameters(handles);
% now launch OAR
handles = gtSegmentationLaunchOAR(handles);
% now can quit
set(handles.status_label, 'String', 'Quitting', 'backgroundcolor', 'y')
pause(1)
close(handles.figure1)
% hObject    handle to launch_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in save_button.
function save_button_Callback(hObject, eventdata, handles)
%save the parameters
handles = gtSegmentationSaveParameters(handles);
% hObject    handle to save_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in quit_button.
function quit_button_Callback(hObject, eventdata, handles)
set(handles.status_label, 'String', 'Quitting - no saving', 'backgroundcolor', 'y')
pause(1)
close(handles.figure1)
% hObject    handle to quit_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



function min_blob_size_Callback(hObject, eventdata, handles)
% hObject    handle to min_blob_size (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% error checking:
tmp = str2num(get(handles.min_blob_size, 'string'));
if ~all(size(tmp)==size(handles.parameters.seg.minblobsize))
    set(handles.status_label, 'string', 'bad format; reverted', 'backgroundColor', 'y')
    pause(0.5)
    set(handles.status_label, 'string', 'bad format; reverted', 'backgroundColor', 'g')
    set(handles.min_blob_size, 'string', num2str(handles.parameters.seg.minblobsize))
else
    set(handles.status_label, 'string', 'Threshold changed', 'backgroundColor', 'y')
    handles.thresholds_changed = true;
    guidata(hObject,handles);
end
% Hints: get(hObject,'String') returns contents of min_blob_size as text
%        str2double(get(hObject,'String')) returns contents of min_blob_size as a double


% --- Executes during object creation, after setting all properties.
function min_blob_size_CreateFcn(hObject, eventdata, handles)
% hObject    handle to min_blob_size (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function max_blob_size_Callback(hObject, eventdata, handles)
% hObject    handle to max_blob_size (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
tmp = str2num(get(handles.max_blob_size, 'string'));
if ~all(size(tmp)==size(handles.parameters.seg.maxblobsize))
    set(handles.status_label, 'string', 'bad format; reverted', 'backgroundColor', 'y')
    pause(0.5)
    set(handles.status_label, 'string', 'bad format; reverted', 'backgroundColor', 'g')
    set(handles.max_blob_size, 'string', num2str(handles.parameters.seg.maxblobsize))
else
    set(handles.status_label, 'string', 'Threshold changed', 'backgroundColor', 'y')
    handles.thresholds_changed = true;
    guidata(hObject,handles);
end
% Hints: get(hObject,'String') returns contents of max_blob_size as text
%        str2double(get(hObject,'String')) returns contents of max_blob_size as a double


% --- Executes during object creation, after setting all properties.
function max_blob_size_CreateFcn(hObject, eventdata, handles)
% hObject    handle to max_blob_size (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function extend_blob_inc_Callback(hObject, eventdata, handles)
% hObject    handle to extend_blob_inc (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of extend_blob_inc as text
%        str2double(get(hObject,'String')) returns contents of extend_blob_inc as a double
tmp = str2num(get(handles.extend_blob_inc, 'string'));
if ~all(size(tmp)==size(handles.parameters.seg.extendblobinc))
    set(handles.status_label, 'string', 'bad format; reverted', 'backgroundColor', 'y')
    pause(0.5)
    set(handles.status_label, 'string', 'bad format; reverted', 'backgroundColor', 'g')
    set(handles.extend_blob_inc, 'string', num2str(handles.parameters.seg.extendblobinc))
end

% --- Executes during object creation, after setting all properties.
function extend_blob_inc_CreateFcn(hObject, eventdata, handles)
% hObject    handle to extend_blob_inc (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function thr_grow_ratio_Callback(hObject, eventdata, handles)
% hObject    handle to thr_grow_ratio (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.status_label, 'string', 'Threshold changed', 'backgroundColor', 'y')
handles.thresholds_changed = true;
guidata(hObject,handles);
% Hints: get(hObject,'String') returns contents of thr_grow_ratio as text
%        str2double(get(hObject,'String')) returns contents of thr_grow_ratio as a double


% --- Executes during object creation, after setting all properties.
function thr_grow_ratio_CreateFcn(hObject, eventdata, handles)
% hObject    handle to thr_grow_ratio (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in Update_ROI_button.
function Update_ROI_button_Callback(hObject, eventdata, handles)
% hObject    handle to Update_ROI_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%handles.roi_changed = true; - this should be set when we change the values
%- don't need to force it here.
handles = gtSegmentationUpdate(handles);
guidata(hObject,handles);

% --- If Enable == 'on', executes on mouse press in 5 pixel border.
% --- Otherwise, executes on mouse press in 5 pixel border or over u_start.
function u_start_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to u_start (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on key press with focus on u_start and none of its controls.
function u_start_KeyPressFcn(hObject, eventdata, handles)
% hObject    handle to u_start (see GCBO)
% eventdata  structure with the following fields (see UICONTROL)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on mouse press over axes background.
function select_bb_image_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to select_bb_image (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA) 



% --- Executes on mouse press over figure background, over a disabled or
% --- inactive control, or over an axes background.
function figure1_WindowButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles = gtSegmentationDrawNewBB(handles);
guidata(hObject,handles);


% --- Executes on slider movement.
function image_slider_Callback(hObject, eventdata, handles)
% hObject    handle to image_slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
slider_value = round(get(hObject,'Value'));
if slider_value ~= handles.current_image
handles.current_image = slider_value;
set(handles.slider_label, 'String', sprintf('Current image %d', slider_value))
handles = gtSegmentationUpdateROIimages(handles);
guidata(hObject,handles);
end



% --- Executes during object creation, after setting all properties.
function image_slider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to image_slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes when selected object is changed in colourmap_buttons.
function colourmap_buttons_SelectionChangeFcn(hObject, eventdata, handles)
% hObject    handle to the selected object in colourmap_buttons 
% eventdata  structure with the following fields (see UIBUTTONGROUP)
%	EventName: string 'SelectionChanged' (read only)
%	OldValue: handle of the previously selected object or empty if none was selected
%	NewValue: handle of the currently selected object
% handles    structure with handles and user data (see GUIDATA)
switch get(eventdata.NewValue,'Tag') % Get Tag of selected object.
    case 'gray_button'
        set(handles.figure1, 'Colormap', gray)
    case 'jet_button'
         set(handles.figure1, 'Colormap', jet)
    case 'random_button'
        % make a colormap appropriate to the output volume
        maxval = max(handles.segvol(:));
        maxval = max(maxval, 63);
        cmap = gtRandCmap(maxval);
        set(handles.figure1, 'Colormap', cmap)
    otherwise
        set(handles.figure1, 'Colormap', jet)       
end
handles = gtSegmentationUpdateROIimages(handles);
guidata(hObject,handles);


% --- Executes on slider movement.
function slider_min_Callback(hObject, eventdata, handles)
% hObject    handle to slider_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
slider_value = round(get(hObject,'Value'));
if slider_value ~= handles.clims_raw(1)
    handles.clims_raw(1) = slider_value;
    % update the figures
    handles = gtSegmentationUpdateBBimage(handles);
    handles = gtSegmentationUpdateROIimages(handles);
    guidata(hObject,handles);
end

% --- Executes during object creation, after setting all properties.
function slider_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function slider_max_Callback(hObject, eventdata, handles)
% hObject    handle to slider_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
slider_value = round(get(hObject,'Value'));
if slider_value ~= handles.clims_raw(2)
    handles.clims_raw(2) = slider_value;
    % update the figures
    handles = gtSegmentationUpdateBBimage(handles);
    handles = gtSegmentationUpdateROIimages(handles);
    guidata(hObject,handles);
end


% --- Executes during object creation, after setting all properties.
function slider_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on button press in debug_checkbox.
function debug_checkbox_Callback(hObject, eventdata, handles)
% hObject    handle to debug_checkbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of debug_checkbox
handles.debug = get(handles.debug_checkbox, 'value');
if handles.debug
    disp('Verbose command line output switched on!')
else
    disp('Verbose command line output switched off!')
end
guidata(hObject,handles);

% --- If Enable == 'on', executes on mouse press in 5 pixel border.
% --- Otherwise, executes on mouse press in 5 pixel border or over debug_checkbox.
function debug_checkbox_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to debug_checkbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on selection change in difspot_mask_menu.
function difspot_mask_menu_Callback(hObject, eventdata, handles)
% hObject    handle to difspot_mask_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns difspot_mask_menu contents as cell array
%        contents{get(hObject,'Value')} returns selected item from difspot_mask_menu


% --- Executes during object creation, after setting all properties.
function difspot_mask_menu_CreateFcn(hObject, eventdata, handles)
% hObject    handle to difspot_mask_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in write_blobs_checkbox.
function write_blobs_checkbox_Callback(hObject, eventdata, handles)
% hObject    handle to write_blobs_checkbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of write_blobs_checkbox
if ~get(handles.write_blobs_checkbox,'Value')
    % if we don't save difblobs, we should save difspots
    set(handles.write_edfs_checkbox,'Value', true)
    set(handles.write_difspot_checkbox,'Value', true)
end


% --- Executes on button press in write_edfs_checkbox.
function write_edfs_checkbox_Callback(hObject, eventdata, handles)
% hObject    handle to write_edfs_checkbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of write_edfs_checkbox
if ~get(handles.write_edfs_checkbox,'Value')
    % if we don't save edfs, we should save difblobs
    set(handles.write_blobs_checkbox,'Value', true)
else
    % if we do save edfs, we should save difspot meta data
    set(handles.write_difspot_checkbox,'Value', true)
end

% --- Executes on button press in write_difspot_checkbox.
function write_difspot_checkbox_Callback(hObject, eventdata, handles)
% hObject    handle to write_difspot_checkbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of write_difspot_checkbox
if ~get(handles.write_difspot_checkbox,'Value')
    % if we don't save difspots, we should save difblobs
    set(handles.write_blobs_checkbox,'Value', true)
end
