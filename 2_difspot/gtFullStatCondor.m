% FUNCTION gtFullStatCondor(first,last,workingdir,flag_overwrite)
%
% Gives mean, median, peak and std values of the given full images
% discarding the segmentation bounding box seg.bbox area.
% Mean and std are also given after 1-1% cut off the extremes values in a given
% image.
% Use gtFullStatAssembleOutputFiles to assemble the output files. They can
% be deleted afterwards.
%
% If flag_overwrite==true, it overwrites the full edf-s with the median
% value offset.
%

function gtFullStatCondor(first,last,workingdir,flag_overwrite)


if ~exist('flag_overwrite','var')
  flag_overwrite=false; 
end

if isdeployed
  global GT_DB
  global GT_MATLAB_HOME
  load('workspaceGlobal.mat');
  first=str2num(first);
  last=str2num(last);
end

cd(workingdir)

load('parameters.mat');

ims=first:last;

bb=parameters.seg.bbox; 
nullbb=NaN(bb(4),bb(3));

nofims=length(ims);

framey=0;
framez=0;
if isfield(parameters,'calib')
  if isfield(parameters.calib,'ydrift')
    framey=ceil(max(abs(parameters.calib.ydrift)));
  end 
  if isfield(parameters.calib,'zdrift')
    framez=ceil(max(abs(parameters.calib.zdrift)));
  end
end

med_full=NaN(nofims,1);
std_full=NaN(nofims,1);
mean_full=NaN(nofims,1);
peak_full=NaN(nofims,1);

std_full_1=NaN(nofims,1);
mean_full_1=NaN(nofims,1);

for i=1:length(ims)
 
  fname=sprintf('1_preprocessing/full/full%04d.edf',ims(i));
   
  disp(sprintf('Reading %s',fname)) 
  info=edf_info(fname);
  im=edf_read(fname);
  
  im_cent=gtCrop(im,bb);
  imn=gtPlaceSubImage(nullbb,im,bb(1),bb(2));
  

  imn(1:framez,:)=[];
  imn(end-framez+1:end,:)=[];
  imn(:,1:framey)=[];
  imn(:,end-framey+1:end)=[];
  
  imn=imn(~isnan(imn(:)));


  %[n,xout]=hist(imn(:),-20:0.01:20);
  %xout(1)=[];
  %xout(end)=[];
  %n(1)=[];
  %n(end)=[];
  %bar(xout,n)

  n=hist(imn(:),-20:0.01:20);
  n(1)=[];
  n(end)=[];
  [tmp,peakloc]=max(n);
  peakloc=(peakloc-2000)*0.01;

  med_full(i)=median(imn);
  std_full(i)=std(imn);
  mean_full(i)=mean(imn);
  peak_full(i)=peakloc;
  
  
  % 1% top and bottom values cut off
  imn_1=sort(imn);
  cutimn_1=round(length(imn_1)*0.01);
  imn_1(1:cutimn_1)=[];
  imn_1(end-cutimn_1+1:end)=[];
  
  std_full_1(i)=std(imn_1);
  mean_full_1(i)=mean(imn_1);

  % offset grayscale values in fulls, outside the bb
  if flag_overwrite
    im=im-med_full(i);
    im=gtPlaceSubImage(im_cent,im,bb(1),bb(2));
    im=single(im);
    edf_write(im,fname,info);
  end
  
end

fnameout=sprintf('fullstatpart_%04d_%04d.mat',first,last);

disp(['Results written into ' fnameout])

save(fnameout,'med_full','std_full','mean_full','peak_full',...
  'std_full_1','mean_full_1')


figure('name','Median of fullimages')
plot(med_full,'b+')

figure('name','Peak of fullimages')
plot(peak_full,'b+')

figure('name','Mean of fullimages')
plot(mean_full,'b+')

figure('name','Mean of fullimages; 1% cut off')
plot(mean_full_1,'b+')

figure('name','STD of fullimages')
plot(std_full,'b+')

figure('name','STD of fullimages; 1% cut off')
plot(std_full_1,'b+')


end

 
