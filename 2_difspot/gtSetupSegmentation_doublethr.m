function gtSetupSegmentation_doublethr(parameters)
% GTSETUPSEGMENTATION_DOUBLETHR  GUI to set up double threshold segmentation
%     gtSetupSegmentation_doublethr()
%     -------------------------------
%       The GUI helps to find appropriate parameters for double threshold
%       segmentation. The parameters then can be saved in the parameters
%       file and the segmentation launched.
%
%       This GUI chooses four evenly spaced full images from what is
%       available (ie. the scan doesn't have to be finished). You can look
%       at an individual image, or an image created as the maximum
%       of the four (a worst case for overlap and noise). What is shown
%       here is NOT 3D thresholding, only 2D, therefore the 3rd dimension
%       max and min sizes do not affect the displayed results.
%       However, they are considered by the real function.
%
%       The segmentation first finds seeds of potential diffraction blobs
%       as areas with intensity above a high threshold (thr_seed) and
%       with size above a minimum value (seedminarea).
%
%       As a secondstep, it extends those seed areas according to a lower
%       threshold value that is dependent on the maximum intensity of the
%       actual seed. Thus this lower threshold (thr_grow) defines the final
%       blob boundaries. It is calculated as:
%            thr_grow = max(seed)*thr_grow_rat
%       but its final value will be limited to the absolut range:
%            thr_grow_low < thr_grow < thr_grow_high
%
%
%      INPUT
%       In the parameters file:
%       parameters.seg.
%
%       bbox          - the segmentation bounding box; this area is omitted
%                       in the segmentation
%
%       seedminarea   - the minimum seed size in a single image that will
%                       be used to grow a seed into a blob (in pixels)
%
%       minblobsize   - blobs under this size will not be considered
%
%       maxblobsize   - blobs outside these dimensions will not be
%                       written to the database; important to stop things
%                       from blowing up - like the glow around the direct
%                       beam for eg.
%
%       thr_seed      - the seed threshold to find potential blobs
%
%       thr_grow_rat  - adaptive threshold parameters for growing seeds
%       thr_grow_low    into blobs
%       thr_grow_high
%
%       extendblobinc - size of incremental blob bbox extension
%
%
%      OUTPUT
%       It can save the updated patameters file and launch the segmentation
%       which will write segmented diffraction blobs in the database.
%
%
%
%     Version 004 May-2012 by L.Nervo and P.Reischig
%       update to new parameters names; use of bwconncomp instead of bwlabel
%       for labelling images; cleaning
%
%     Version 003 18-03-2012 by AKing
%       Add thr_grow_high and thr_grow_low to gui and function
%
%     Version 002 22-11-2011 by LNervo
%       ***first attemp to a clean DCT code***


disp(' ');
disp('! Note that what is displayed here is 2D thresholding, and the real routine does 3D!');
disp(' ');
pause(1);

if (~exist('parameters', 'var') || isempty(parameters))
    parameters = gtLoadParameters();
end
parameters_name = fullfile(parameters.acq.dir,'parameters.mat');

check = inputwdefault('Do you want to reset all the parameters for segmentation? [y/n]', 'n');
if strcmpi(check,'y')
    parameters.seg = gtSegDefaultParameters();
    gtSaveParameters(parameters);
    disp('Segmentation parameters have been reset to default values')
end

% Let's check every field is in, and in case let's update it!
parameters = gtCheckParameters(parameters, 'seg', 'verbose', true);
gtSaveParameters(parameters);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% recompile binaries - not often needed.  But more often than it should be...
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
check = inputwdefault('Recompile functions for OAR? Not normally needed! [y/n]', 'n');
if strcmpi(check, 'y')
    disp('Recompiling all functions...');
    gtExternalCompileFunctions('gtSeedThreshold_doublethr', 'gtSegmentDiffractionBlobs_doublethr');
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% how many images in this scan?
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
totproj = gtAcqTotNumberOfImages(handles.parameters) - 1;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Safety check - are all the images present?
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp('Checking for preprocessed images...');
d = dir(fullfile(parameters.acq.dir,'1_preprocessing/full/full*edf'));
test_full = length(d);
target_ims = (totproj+1);
if (test_full < target_ims)
    % some fulls are missing!
    fprintf('!!! Not all full images are present (%d found, %d expected)\n', test_full, target_ims);
    disp('This could be normal if you are processing "live" and still producing images');

    check = inputwdefault('Quit gtSetupSegmentation_doublethr? [y/n]', 'y');
    if strcmpi(check, 'y')
        % quit, but first check what other images are missing
        disp('quitting - first checking what images are present....');
        d = dir(fullfile(parameters.acq.dir,'1_preprocessing/abs/abs*edf'));
        test_abs = length(d);
        d = dir(fullfile(parameters.acq.dir,'1_preprocessing/abs/med*edf'));
        test_abs_med = length(d);
        d = dir(fullfile(parameters.acq.dir,'1_preprocessing/full/med*edf'));
        test_full_med = length(d);
        % how many should we have?
        target_abs_med = (target_ims/parameters.prep.absint)+1;
        target_full_med = (target_ims/parameters.prep.fullint)+1;
        fprintf('Found full: %d/%d; abs: %d/%d; abs median: %d/%d; full median: %d/%d\n', ...
            test_full, target_ims, test_abs, target_ims, test_abs_med, target_abs_med, test_full_med, target_full_med);
        disp('Probably some preprocessing OAR jobs have failed');
        gtError('SEG:invalid_state', 'Error: Missing images!');
    end
else
    fprintf('Found all full images (%d found, %d expected)\n', test_full, totproj+1);
end

% if all images are present, check intensities in direct beam
if (~parameters.acq.no_direct_beam && (test_full == target_ims))
    check = inputwdefault('Check intensities in direct beam? [y/n]', 'y');
    if strcmpi(check, 'y')
        fprintf('Checking every 20th image in 0_rawdata/%s...\n', parameters.acq.name);
        hf = figure('Name', 'Checking direct beam intensity', 'NumberTitle', 'off');
        xlabel('Image number');
        ylabel('Intensity');
        hold on;
        fint = zeros(1, target_ims);
        fbb = floor(parameters.acq.bb(1:2) + (parameters.acq.bb(3:4)/2));
        fbb(3:4) = 2; % read in 2x2 pixels in centre of sample
        for img_num = 1:20:target_ims
            froi = edf_read(fullfile(parameters.acq.dir, '0_rawdata', sprintf('%s/%s%04d.edf', parameters.acq.name, parameters.acq.name, img_num-1)), fbb);
            fint(img_num) = sum(froi(:))/4;
            plot(img_num-1, fint(img_num), 'bx');
            if (mod(img_num, 100) == 1)
                drawnow;
            end
        end
        disp('Does the intensity look okay?');
        check = inputwdefault('Continue with segmentation? [y/n]', 'y');
        if strcmp(check, 'n')
            disp('Quitting... ');
            close(hf);
            return;
        end
        close(hf);
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% before starting, set up the required database tables
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp(' ');
gtDBConnect();
disp(' ');
disp('NOTE! When restarting the segmentation, all tables should be dropped!');
disp(' ');

% Are there already data in the tables?
test = mym(['select count(*) from ' parameters.acq.name 'difblobdetails']);
if test > 0
    disp('There is already data in the difblob table');
    check = inputwdefault('Drop and recreate difblob tables? [y/n]', 'y');
    if strcmpi(check, 'y')
        gtDBCreateDifblobTable(parameters.acq.name, 1);
    end
end


% seed table
test = mym(sprintf('show tables like "%sseeds"', parameters.acq.name));

if ~isempty(test) % if exists, check before dropping
    check = inputwdefault('Drop and recreate seed table? [y/n]', 'y');

    if strcmp(check, 'y')
        mym(['drop table ' parameters.acq.name 'seeds']);
        mysqlcmd = ['create table ' parameters.acq.name 'seeds '...
            '(seedID int unsigned not null auto_increment, '...
            'x smallint unsigned not null, y smallint unsigned not null, '...
            'z smallint unsigned not null, '...
            'graylevel double, bad boolean, index(seedID),  '...
            'index(x), index(y), index(z), primary key (seedID)) ENGINE=MyISAM'];
        mym(mysqlcmd);
    end
else
    mysqlcmd = ['create table ' parameters.acq.name 'seeds '...
        '(seedID int unsigned not null auto_increment, '...
        'x smallint unsigned not null, y smallint unsigned not null, '...
        'z smallint unsigned not null, '...
        'graylevel double, bad boolean, index(seedID),  index(x), '...
        'index(y), index(z), primary key (seedID)) ENGINE=MyISAM'];
    mym(mysqlcmd);
end


% seeds_tmp table - for controlling OAR
test = mym(['show tables like "' parameters.acq.name 'seeds_tmp"']);
if ~isempty(test) % if exists, check before dropping
    check = inputwdefault('Drop and recreate seeds_tmp table (for controlling OAR jobs)? [y/n]', 'y');
    if strcmp(check, 'y')
        mym(['drop table ' parameters.acq.name 'seeds_tmp']);
        mysqlcmd = ['create table ' parameters.acq.name ...
            'seeds_tmp (seedID int, unique key (seedID) ) ENGINE=MyISAM'];
        mym(mysqlcmd);
    end
else
    mysqlcmd = ['create table ' parameters.acq.name 'seeds_tmp (seedID int, unique key (seedID) ) ENGINE=MyISAM'];
    mym(mysqlcmd);
end


% fullmedianvals table
test = mym(['show tables like "' parameters.acq.name 'fullmedianvals"']);
if ~isempty(test) % if exists, check before dropping
    check = inputwdefault('Drop and recreate fullmedianvals table? [y/n]', 'y');
    if strcmpi(check, 'y')
        mym(['drop table ' parameters.acq.name 'fullmedianvals']);
        mysqlcmd = ['create table ' parameters.acq.name ...
            'fullmedianvals (ndx smallint, val double, primary key (ndx)) ENGINE=MyISAM'];
        mym(mysqlcmd);

        % insert the image numbers in the table
        gtDBInsertMultiple([parameters.acq.name 'fullmedianvals'], 'ndx', 0:totproj);
    end
else
    mysqlcmd = ['create table ' parameters.acq.name ...
        'fullmedianvals (ndx smallint, val double, primary key (ndx)) ENGINE=MyISAM'];
    mym(mysqlcmd);

    % insert the image numbers in the table
    gtDBInsertMultiple([parameters.acq.name 'fullmedianvals'], 'ndx', 0:totproj);
end


% one question - does user want to subtract the background before
% segmentation?
% cound make this a buttun / option in the function
disp(' ');
check = inputwdefault('Offset the median value of each full image to 0 (recommended)? [y/n]', 'y');
parameters.seg.background_subtract = strcmpi(check, 'y');

save(parameters_name, 'parameters');

disp(' ');
disp('Colour key in images:');
disp('  black       - background');
disp('  red         - good marker');
disp('  blue        - good mask');
disp('  yellow      - bad undersized marker');
disp('  dull purple - bad oversized marker');
disp('  dull green  - bad oversized mask');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% get some test images
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% wait until at least 4 images are present
listfull = dir( fullfile(parameters.acq.dir,'1_preprocessing','full','full*edf') );
while length(listfull) < 4
    disp('waiting for at least 4 full images to appear!...');
    pause(5);
    listfull = dir( fullfile(parameters.acq.dir,'1_preprocessing','full','full*edf') );
end

% put in date order
fdates = [listfull.('datenum')];
fnames = {listfull.('name')};

[~, ind] = sort(fdates, 'ascend');
fnames = fnames(ind);

% get 4 evenly spaced images
getfulls = 1;
check = inputwdefault('Do you have any suggested numbers for images? [y/n]','n');
if strcmp(check, 'y')
    getfulls = input('Insert image numbers in squared brackets (the first 4 are used):');
end

if length(getfulls) < 4
    getfulls = [getfulls, round(1:(length(fnames)/(4-length(getfulls))):length(fnames)) length(fnames)];
elseif length(getfulls) > 4
  % remove exceeding numbers
    getfulls(5:end,:) = [];
end
% remove duplicates if existing
getfulls = unique(getfulls);
disp('Using those images to segment: ');
disp(getfulls);

info = [];
fullstack = zeros([parameters.acq.ydet parameters.acq.xdet length(getfulls)]);
for kk=1:length(getfulls)
    [fullstack(:,:,kk), info] = edf_read( fullfile('1_preprocessing','full',fnames{getfulls(kk)}), [], false, info );
end

% make a maximum full image
maxfull = max(fullstack, [], 3);
fullstack(:,:,5) = maxfull;

threshstack = NaN(size(fullstack));


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% segmentation parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Values.thr_seed      = parameters.seg.thr_seed;
Values.thr_grow_rat  = parameters.seg.thr_grow_rat;
Values.maxblobsize   = parameters.seg.maxblobsize;
Values.minblobsize   = parameters.seg.minblobsize;
Values.seedminarea   = parameters.seg.seedminarea;
Values.extendblobinc = parameters.seg.extendblobinc;
Values.show_badspots = false;
Values.ShowThresholded = false;

if isfield(parameters.seg, 'bbox') && ~isempty(parameters.seg.bbox)
    Values.bbox = parameters.seg.bbox;
else
    disp('Segmentation bounding box is not specified. Using the sample');
    disp('bounding box extended.');
    Values.bbox = parameters.acq.bb + [-20 -20 40 40];
    Values.bbox = max(Values.bbox, [1 1 1 1]);
end


Values.bboxChanged = 0;

Values.OARJobs = 30;
Values.OARTime = 21600; % try 6 hours default


% Colour key - background black; marker red; mask blue; undersized marker
%   yellow; bad blob size marker dull purple; bad blob size mask dull green
Values.ThreshCmap = [0 0 0; 1 0 0; 0 0 1; 1 1 0; 0.5 0 0.5; 0.5 0.5 0];


% limits to adaptive threshold
if isfield(parameters.seg, 'thr_grow_high')
    Values.thr_grow_high = parameters.seg.thr_grow_high;
else
    Values.thr_grow_high = [];
end
if isfield(parameters.seg, 'thr_grow_low')
    Values.thr_grow_low = parameters.seg.thr_grow_low;
else
    Values.thr_grow_low = [];
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% setup the figure
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Main GUI figure handle
hfig = figure();

scrsz = get(0, 'ScreenSize');
figsz = round(0.75 * scrsz(3:4));

set(hfig,'Position',[50 150 figsz(1) figsz(2)]);
set(hfig,'windowbuttondownfcn', @(src, evt)sfDragBBFunction(hfig));

% setup buttons
ToggleButton = uicontrol(hfig, 'Style', 'pushbutton', 'string', 'Greyscale/Thresholded', 'position', [330 30 150 30]);
set(ToggleButton, 'callback', @(src, evt)sfToggleButtonFunction(hfig));

UpdateButton = uicontrol(hfig, 'Style', 'pushbutton', 'string', 'Update', 'position', [180 30 150 30]);
set(UpdateButton, 'callback', @(src, evt)sfUpdateButtonFunction(hfig));

QuitButton   = uicontrol(hfig, 'Style', 'pushbutton', 'string', 'Quit', 'position', [figsz(1)-460 figsz(2)-40 150 30]);
set(QuitButton, 'callback', @(src, evt)sfQuitButtonFunction(hfig));

SaveButton   = uicontrol(hfig, 'Style', 'pushbutton', 'string', 'Save', 'position', [figsz(1)-310 figsz(2)-40 150 30]);
set(SaveButton, 'callback', @(src, evt)sfSaveButtonFunction(hfig));

OARButton    = uicontrol(hfig, 'Style', 'pushbutton', 'string', 'Save+Launch segmentation', 'position', [figsz(1)-160 figsz(2)-40 150 30]);
set(OARButton, 'callback', @(src, evt)sfOARButtonFunction(hfig));


% communication string
StatusMessage = uicontrol(hfig, 'Style', 'text', 'String', 'Status: ', 'Position', [480 30 150 30]);

% Create three radio buttons in a button group.
hbg = uibuttongroup('visible','off','Position',[0.9 0 0.1 1]);

uicontrol(hfig, 'Style', 'text', 'String', 'Which full image to look at?  Choice of four images, or the max of the four', 'position', [figsz(1)-160 300 150 60]);

u1 = uicontrol('Style','Radio','String','Full A',...
    'pos',[10 250 100 30],'parent',hbg,'HandleVisibility','off');

u2 = uicontrol('Style','Radio','String','Full B',...
    'pos',[10 200 100 30],'parent',hbg,'HandleVisibility','off');

u3 = uicontrol('Style','Radio','String','Full C',...
    'pos',[10 150 100 30],'parent',hbg,'HandleVisibility','off');

u4 = uicontrol('Style','Radio','String','Full D',...
    'pos',[10 100 100 30],'parent',hbg,'HandleVisibility','off');

u5 = uicontrol('Style','Radio','String','Max(Fulls)',...
    'pos',[10 50 100 30],'parent',hbg,'HandleVisibility','off');

% Initialize some button group properties.
set(hbg, 'SelectionChangeFcn', @(src, evt)sfChangeFull(hfig, evt));
set(hbg, 'SelectedObject', u1);
set(hbg, 'Visible', 'on');

% input text boxes for user input
bb_label = uicontrol(hfig, 'Style', 'text', 'String', 'Change bounding box using the mouse', 'position', [0 485 120 30]);
bb_label = uicontrol(hfig, 'Style', 'text', 'String', 'You can edit the values below', 'position', [0 455 120 30]);

thr_seed_label = uicontrol(hfig, 'Style', 'text', 'String', 'thr_seed', 'position', [0 435 120 15]);
thr_seed_box   = uicontrol(hfig, 'Style', 'edit', 'String', num2str(Values.thr_seed), 'position', [0 415 120 20]);

thr_grow_rat_label = uicontrol(hfig, 'Style', 'text', 'String', 'thr_grow_rat', 'position', [0 380 120 30]);
thr_grow_rat_box   = uicontrol(hfig, 'Style', 'edit', 'String', num2str(Values.thr_grow_rat), 'position', [0 360 120 20]);

grhigh_label = uicontrol(hfig, 'Style', 'text', 'String', 'thr_grow_high', 'position', [0 325 120 30]);
grhigh_box   = uicontrol(hfig, 'Style', 'edit', 'String', num2str(Values.thr_grow_high), 'position', [0 305 120 20]);

grlow_label = uicontrol(hfig, 'Style', 'text', 'String', 'thr_grow_low', 'position', [0 270 120 30]);
grlow_box   = uicontrol(hfig, 'Style', 'edit', 'String', num2str(Values.thr_grow_low), 'position', [0 250 120 20]);

maxblobsize_label = uicontrol(hfig, 'Style', 'text', 'String', 'maxBlobSize', 'position', [0 230 120 15]);
maxblobsize_box   = uicontrol(hfig, 'Style', 'edit', 'String', num2str(Values.maxblobsize), 'position', [0 210 120 20]);

minblobsize_label = uicontrol(hfig, 'Style', 'text', 'String', 'minblobsize', 'position', [0 190 120 15]);
minblobsize_box   = uicontrol(hfig, 'Style', 'edit', 'String', num2str(Values.minblobsize), 'position', [0 170 120 20]);

seedminarea_label = uicontrol(hfig, 'Style', 'text', 'String', 'seedminarea', 'position', [0 150 120 15]);
seedminarea_box   = uicontrol(hfig, 'Style', 'edit', 'String', num2str(Values.seedminarea), 'position', [0 130 120 20]);

extendblobinc_label = uicontrol(hfig, 'Style', 'text', 'String', 'extendblobinc', 'position', [0 110 120 15]);
extendblobinc_box   = uicontrol(hfig, 'Style', 'edit', 'String', num2str(Values.extendblobinc), 'position', [0 90 120 20]);

OARJobs_label = uicontrol(hfig, 'Style', 'text', 'String', 'Number of OAR jobs', 'position', [0 70 120 15]);
OARJobs_box   = uicontrol(hfig, 'Style', 'edit', 'String', num2str(Values.OARJobs), 'position', [0 50 120 20]);

OARTime_label = uicontrol(hfig, 'Style', 'text', 'String', 'WallTime for OAR (s)', 'position', [0 30 120 15]);
OARTime_box   = uicontrol(hfig, 'Style', 'edit', 'String', num2str(Values.OARTime), 'position', [0 10 120 20]);


StackIndex = 1; % from 1 to 5, index into fullstack

[Values, ValuesChanged] = sfUpdateValues(Values);

full = fullstack(:,:,StackIndex); % take the first image from full stack

ThreshFull = threshstack(:,:,StackIndex); % ie NaN's to start with


% draw the images
subplot(1,2,1);
imshow(full, gtAutolim(full));

sfPlotBB(Values);

% open impixelinfo in upper left corner
h_figpos = get(hfig,'Position');
h_imp    = impixelinfo(hfig);
set(h_imp,'Position', [10 h_figpos(4)-30 150 30]);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SUB-FUNCTIONS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% change the full image
    function sfChangeFull(fig, eventdata)
        StackIndex = find([u1 u2 u3 u4 u5] == eventdata.NewValue);
        full    = fullstack(:, :, StackIndex);

        ThreshFull = threshstack(:,:,StackIndex);
        Values.ShowThresholded = false;

        subplot(1,2,1);
        imshow(full, gtAutolim(full));

        % open impixelinfo in upper left corner
        figpos = get(fig,'Position');
        im_p   = impixelinfo(fig);
        set(im_p, 'Position', [10 figpos(4)-30 150 30]);

        sfPlotBB(Values);
        colormap(gray);
    end

% do thresholding
    function sfToggleButtonFunction(fig)
        %invert ShowThresholded
        Values.ShowThresholded  = ~Values.ShowThresholded;
        [Values, ValuesChanged] = sfUpdateValues(Values);

        if (Values.ShowThresholded)
            % check just the first pixel
            if (ValuesChanged || isnan(ThreshFull(1)))
                ThreshFull = sfDoThresholding(full, Values);  %changed here
                threshstack(:, :, StackIndex) = ThreshFull;
            else
                % if we already have a non NaN ThreshFull
                ThreshFull = threshstack(:, :, StackIndex);
            end

            subplot(1, 2, 2);
            imshow(ThreshFull, [0 5]);
            sfPlotBB(Values);
            colormap(Values.ThreshCmap);
        else
            subplot(1, 2, 1);
            imshow(full, gtAutolim(full));
            sfPlotBB(Values);
            colormap(gray);
        end

        % open impixelinfo in upper left corner
        figpos = get(fig,'Position');
        im_p   = impixelinfo(fig);
        set(im_p, 'Position', [10 figpos(4)-30 150 30]);
    end


    function ThreshFull = sfUpdateButtonFunction(fig)
        [Values, ValuesChanged] = sfUpdateValues(Values);
        if (ValuesChanged)
            if (Values.ShowThresholded)
                ThreshFull = sfDoThresholding(full, Values);
                imshow(ThreshFull, [0 5]);
                colormap(Values.ThreshCmap);
            else
                imshow(full, gtAutolim(full));
                colormap(gray);
            end

            sfPlotBB(Values);

            % open impixelinfo in upper left corner
            figpos = get(fig,'Position');
            im_p   = impixelinfo(fig);
            set(im_p,'Position', [10 figpos(4)-30 150 30]);
        end
    end



    function ThreshFull = sfDoThresholding(full, Values)
    % Thresholds

        set(StatusMessage, 'String', 'Status: Calculating... ');
        drawnow();

        % 2D approximation to the real 3D functionality
        % Calculate the new thresholded image
        ThreshFull = zeros(size(full));

        % remove the median background in the full image
        if (isfield(parameters.seg, 'background_subtract') ...
                && parameters.seg.background_subtract)

            %subtract the median of the background
            imn = full;
            imn(Values.bbox(2):(Values.bbox(2)+Values.bbox(4)-1), ...
                Values.bbox(1):(Values.bbox(1)+Values.bbox(3)-1)) = NaN;

            imn = imn(~isnan(imn(:)));

            med_value = median(imn);
            full      = full - med_value;
        end

        % set bb to zeros
        full(Values.bbox(2):(Values.bbox(2)+Values.bbox(4)-1), ...
             Values.bbox(1):(Values.bbox(1)+Values.bbox(3)-1) ) = 0;

        % get markers
        %Markers = double(full > Values.thr_seed);
        Seeds = full > Values.thr_seed;
        Seeds = bwmorph(Seeds, 'open');
        % labelling of connected regions
        %Markers = bwlabel(Markers);
        Seeds = bwconncomp(Seeds);

        %for ii = 1:max(Markers(:))
        for ii = 1:Seeds.NumObjects

            % marker size
            %count = length(find(Markers==ii));

            %if count > Values.seedminarea
            if (length(Seeds.PixelIdxList{ii}) > Values.seedminarea)
                % do double threshold

                %Seed     = (Markers==ii);
                %GreyVals = Seed(:).*full(:);

                % if thr_seed is smaller then the proportional threshold,
                % use it instead
                %th_grow = min(max(GreyVals)*Values.thr_grow_rat, Values.thr_seed);
                %                 thr_grow = min(max(full(Seeds.PixelIdxList{ii}))*Values.thr_grow_rat,...
                %                               Values.thr_seed);
                [maxval, maxndx] = max(full(Seeds.PixelIdxList{ii}));

                thr_grow = min(maxval*Values.thr_grow_rat,...
                    Values.thr_grow_high);

                if ~isempty(Values.thr_grow_high)
                    thr_grow = min(Values.thr_grow_high, thr_grow);
                end

                if ~isempty(Values.thr_grow_low)
                    thr_grow = max(Values.thr_grow_low, thr_grow);
                end

                % create label image of connected areas above thr_grow
                % (-> Mask)
                % find the mask label at location seed ii

                Mask = full > thr_grow;
                Mask = bwlabel(Mask);
                masklabel = Mask(Seeds.PixelIdxList{ii}(maxndx));

                % set mask to the curent spot
                Mask = (Mask == masklabel);

                %blobbb = regionprops(double(Mask), 'BoundingBox');
                blob = regionprops(Mask, 'BoundingBox');

                % check the size of the blob
                if (all(blob.BoundingBox(3:4) < Values.maxblobsize(1:2)) ...
                        && all(blob.BoundingBox(3:4) > Values.minblobsize(1:2)) )

                    % colour the valid seed
                    ThreshFull(Seeds.PixelIdxList{ii}) = 1;
                    % colour the extended bit
                    ThreshFull(Mask) = 2;
                end

                if (Values.show_badspots)
                    % colour a badly sized blob

                    % colour the extended bit
                    ThreshFull(Mask) = 5;
                    % colour the invalid seed
                    ThreshFull(Seeds.PixelIdxList{ii}) = 4;
                end
            else
                % seed below area
                ThreshFull(Seeds.PixelIdxList{ii}) = 3;
            end
        end

        set(StatusMessage, 'String', 'Status: Done! ');
        drawnow();
    end

    function [values, valuesChanged] = sfUpdateValues(values)
    % Read the values from the GUI boxes

        values_old      = values;

        values.thr_seed      = str2double(get(thr_seed_box, 'String'));
        values.thr_grow_rat  = str2double(get(thr_grow_rat_box, 'String'));
        values.thr_grow_high = str2double(get(grhigh_box, 'String'));
        values.thr_grow_low  = str2double(get(grlow_box, 'String'));
        values.seedminarea   = str2double(get(seedminarea_box, 'String'));

        values.maxblobsize = get(maxblobsize_box, 'String');
        values.maxblobsize = ['[ ' values.maxblobsize ' ]'];
        values.maxblobsize = str2num(values.maxblobsize); %#ok<ST2NM>

        if (length(values.maxblobsize) ~= 3)
            disp('maxblobsize should have three values');
            values.maxblobsize = values_old.maxblobsize;
        end

        values.minblobsize = get(minblobsize_box, 'String');
        values.minblobsize = ['[ ' values.minblobsize ' ]'];
        values.minblobsize = str2num(values.minblobsize); %#ok<ST2NM>

        if (length(values.minblobsize) ~= 3)
            disp('minblobsize should have three values');
            values.minblobsize = values_old.minblobsize;
        end

        values.extendblobinc = get(extendblobinc_box, 'String');
        values.extendblobinc = ['[ ' values.extendblobinc ' ]'];
        values.extendblobinc = str2num(values.extendblobinc); %#ok<ST2NM>

        if (length(values.extendblobinc) ~= 3)
            disp('extendblobinc should have three values');
            values.extendblobinc = values_old.extendblobinc;
        end

        valuesChanged = ((values.thr_seed ~= values_old.thr_seed) ...
            || (values.thr_grow_rat ~= values_old.thr_grow_rat) ...
            || (values.thr_grow_high ~= values_old.thr_grow_high) ...
            || (values.thr_grow_low ~= values_old.thr_grow_low) ...
            || (values.seedminarea ~= values_old.seedminarea) ...
            || any(values.maxblobsize ~= values_old.maxblobsize) ...
            || any(values.minblobsize ~= values_old.minblobsize) ...
            || values.bboxChanged ...
            || any(values.extendblobinc ~= values_old.extendblobinc));

        % if values have changed, we should reset the threshstack to NaNs
        if (valuesChanged)
            threshstack = NaN(size(fullstack));
            ThreshFull  = NaN(size(ThreshFull));
        end

        if (values.bboxChanged)
            values.bboxChanged = false;
        end
    end


    function sfDragBBFunction(fig)
        % get a new bounding box
        % how to get this to Values?
        p1 = get(gca, 'CurrentPoint');
        p1 = p1(1, 1:2);

        % use rubberband box area selection
        bbox = rbbox();

        p2 = get(gca, 'CurrentPoint');
        p2 = p2(1, 1:2);

        bborigin = min(p1, p2);
        bbsize   = abs(p1 - p2) + 1;

        % check that values are okay (no stray clicks outside image), or
        % tiny bboxes
        if (any(bborigin < 1) ...
                || any((bborigin+bbsize) > [parameters.acq.xdet parameters.acq.ydet]) ...
                || any(bbsize < 5) )
            return;
        end

        Values.bbox        = round([bborigin bbsize]);
        Values.bboxChanged = 1;

        sfUpdateButtonFunction(fig); % returns ThreshFull so ;
    end


    function sfPlotBB(values)
        hold on;

        xdata = [values.bbox(1), ...
            values.bbox(1) + values.bbox(3), ...
            values.bbox(1) + values.bbox(3), ...
            values.bbox(1), ...
            values.bbox(1)] - 0.5;

        ydata = [values.bbox(2), ...
            values.bbox(2), ...
            values.bbox(2) + values.bbox(4), ...
            values.bbox(2) + values.bbox(4), ...
            values.bbox(2)] - 0.5;

        plot(xdata, ydata, '.-r');
        hold off;
    end

    function sfSaveButtonFunction(fig)
        % write the values in Values to parameters.seg

        set(StatusMessage, 'String', 'Status: Saving values to parameters file');

        pause(0.5);

        parameters.seg.thr_seed      = Values.thr_seed;
        parameters.seg.thr_grow_rat  = Values.thr_grow_rat;
        parameters.seg.thr_grow_high = Values.thr_grow_high;
        parameters.seg.thr_grow_low  = Values.thr_grow_low;
        parameters.seg.maxblobsize   = Values.maxblobsize;
        parameters.seg.minblobsize   = Values.minblobsize;
        parameters.seg.seedminarea   = Values.seedminarea;
        parameters.seg.extendblobinc = Values.extendblobinc;
        parameters.seg.bbox          = Values.bbox;
        parameters.seg.method = 'doublethr';

        save(parameters_name, 'parameters');

        sfQuitButtonFunction(fig);
    end

    function sfOARButtonFunction(fig)
        % save to parameters, then launch OAR

        parameters.seg.thr_seed         = Values.thr_seed;
        parameters.seg.thr_grow_rat     = Values.thr_grow_rat;
        parameters.seg.thr_grow_high    = Values.thr_grow_high;
        parameters.seg.thr_grow_low     = Values.thr_grow_low;
        parameters.seg.maxblobsize      = Values.maxblobsize;
        parameters.seg.minblobsize      = Values.minblobsize;
        parameters.seg.seedminarea      = Values.seedminarea;
        parameters.seg.extendblobinc    = Values.extendblobinc;
        parameters.seg.bbox             = Values.bbox;
        parameters.seg.method = 'doublethr';

        % if using this function, don't need to remove overlapping difblobs
        parameters.seg.overlaps_removed = true;

        save(parameters_name, 'parameters');
        set(StatusMessage, 'String', 'Status: Saving values to parameters file');

        pause(0.5);

        njobs   = str2double(get(OARJobs_box, 'String'));
        OARtime = str2double(get(OARTime_box, 'String'));
        OARtmp  = ceil(OARtime/60); % minutes
        OARm    = mod(OARtmp, 60);  % minutes
        OARh    = (OARtmp-OARm)/60; % hours

        OAR_parameters.walltime       = sprintf('%02d:%02d:00', OARh, OARm);
        OAR_parameters.mem            = 2000;
        OAR_parameters_seeds.walltime = '2:00:00';
        OAR_parameters_seeds.mem      = 2000;

        disp('Seeds: Using OAR parameters:');
        disp(OAR_parameters_seeds);
        disp('Thresholding: Using OAR parameters:');
        disp(OAR_parameters);
        disp(['Launching ' num2str(njobs) ' OAR jobs...']);

        set(StatusMessage, 'String', sprintf('Status: launching %d OAR jobs', njobs));

        pause(0.5);

        % launch the two functions
        % blob segmentation waits for seed segmentation to finish
        gtOarLaunch('gtSeedThreshold_doublethr', 0, totproj, njobs, ...
            parameters.acq.dir, 1, ...
            'walltime', OAR_parameters_seeds.walltime, ...
            'mem', OAR_parameters_seeds.mem);

        gtOarLaunch('gtSegmentDiffractionBlobs_doublethr', 0, totproj, ...
            njobs, parameters.acq.dir, 1, ...
            'walltime', OAR_parameters.walltime, ...
            'mem', OAR_parameters.mem);

        sfQuitButtonFunction(fig);
    end

    function sfQuitButtonFunction(fig)
        set(StatusMessage, 'String', 'Status: Quitting...');
        pause(1);
        close(fig);
    end

end % end of main function
