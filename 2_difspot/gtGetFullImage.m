function [im, info] = gtGetFullImage(n, parameters, info)
% GTGETFULLIMAGE  Gets the full image number n
%                         
%     im = gtGetFullImage(n[, parameters])
%     ------------------------------------
%     Read full image n from 1_preprocessing/full/
%     Option second argument is parameters.  If this is passed in then
%     parameters.acq.dir will be used as the path.  Otherwise, the function
%     will assume you are in the parameters.acq.dir directory and work
%     relative to this.
%
%     INPUT:
%       n          = <int>    Image number
%
%     OPTIONAL INPUT:
%       parameters = <struct> DCT parameters structure
%
%     OUTPUT:
%       full image = <double> Output full image

    if (exist('parameters', 'var') && ~isempty(parameters))
        dir = parameters.acq.dir;
    else
        dir = pwd();
    end

    full_image_name = fullfile(dir, ...
        '1_preprocessing', 'full', sprintf('full%04d.edf', n) );

    if (exist('info', 'var') && ~isempty(info))
        [im, info] = edf_read(full_image_name, [], false, info);
    else
        [im, info] = edf_read(full_image_name);
    end
end
