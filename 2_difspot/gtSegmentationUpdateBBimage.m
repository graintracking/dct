

function handles = gtSegmentationUpdateBBimage(handles)

% focus on the figure
set(handles.figure1, 'CurrentAxes', handles.select_bb_image)
redraw=false;
% update the image if the roi has changed
if handles.roi_changed
    hold off
    set(handles.select_bb_title, 'string', sprintf('Image %s (entire image)', get(handles.image_start, 'string')))
    imagesc(handles.im)
    % here we lose the previously drawn bb, so we must clear its handle
    % and redraw it
    handles.bb_handle=[];
    redraw=true;
    hold on
    % draw also the ROI 
    ustart = str2num(get(handles.u_start, 'String'));
    vstart = str2num(get(handles.v_start, 'String'));
    uend = str2num(get(handles.u_end, 'String'));
    vend = str2num(get(handles.v_end, 'String'));
    ROIx = [ustart ustart uend uend ustart];
    ROIy = [vstart vend vend vstart vstart];
    plot(ROIx, ROIy, 'w.-.')
end

% update the bb if this has changed
if handles.bb_changed || redraw
    bb = handles.parameters.seg.bbox;
    xdata = [bb(1) bb(1) bb(1)+bb(3) bb(1)+bb(3) bb(1)];
    ydata = [bb(2) bb(2)+bb(4) bb(2)+bb(4) bb(2) bb(2)];
    
    if isempty(handles.bb_handle)
        handles.bb_handle = plot(xdata, ydata, 'r');
    else
        set(handles.bb_handle, 'XData', xdata, 'YData', ydata)
    end
    
    % we have updated the image
    % however, we haven't reloaded or recalculated the thresholded
    % images.  When we do this, we need to deal with the new bounding
    % box.  Therefore, keep the flag set to true.
end

% update the clims
set(handles.select_bb_image, 'Clim', handles.clims_raw)


if handles.debug
    disp('in gtSegmentationUpdateBBimage')
end


end