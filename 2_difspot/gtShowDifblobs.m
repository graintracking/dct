
function gtShowDifblobs(ids)
%
% GTSHOWBIFBLOBS  HELPER FUNCTION FOR LOOKING AT SEGMENTATION RESULTS
%
%   gtShowDifblobs(difblobIDs)
%   -------------------------------------------------------------------
%   For a list of difblobIDs, show the results of segmentation (from the
%   blob table), the summed full images, and the blob in a GtVolView
%   viewer.
%
%   Version ?...  slightly updated by Andy, 15/06/2012.  Probably needs
%   further work to be optimally useful, but worth keeping.
%


clims=[-50 50];
figpos=[0.2 0.2 0.6 0.6];

gtDBConnect;
load('parameters.mat');

maxID=mym(sprintf('SELECT MAX(difblobID) FROM %sdifblob',parameters.acq.name));

close all

  fig_full=figure('name','Full image #');
  set(gcf,'units','normalized','position',[0 0.2 0.3 0.3])

  fig_fullreg=figure('name','Summed full region');
  set(gcf,'units','normalized','position',[0 0.6 0.3 0.3])

  fig_blob=figure('name','Difblob ');
  set(gcf,'units','normalized','position',[0.3 0.6 0.3 0.3])

  fig_diff=figure('name','Difference between the two');
  set(gcf,'units','normalized','position',[0.3 0.2 0.3 0.3])
 

for iw=1:length(ids)

  if ids(iw)>maxID
    break
  end
  
  [vol,bb]=gtDBBrowseDiffractionVolume(parameters.acq.name, ids(iw));
  difspot=sum(vol,3);

  im=zeros(bb(5),bb(4));
  for i=bb(3):bb(3)+bb(6)-1
    im=im+edf_read(sprintf('1_preprocessing/full/full%04d.edf',i),bb([1 2 4 5]));
  end

  fullimID=round((bb(3)+bb(3)+bb(6)-1)/2);
  fullim=edf_read(sprintf('1_preprocessing/full/full%04d.edf',fullimID));

  
  figure(fig_full)
  clf
  set(gcf,'name',sprintf('Full image #%d',fullimID))
  imshow(fullim,clims)
  set(gcf,'units','normalized','position',[0 0.2 0.3 0.3])
  hold on
  rectangle('position',bb([1 2 4 5])+[-1 -1 2 2],'edgecolor','g')

  figure(fig_fullreg)
  clf
  set(gcf,'name',sprintf('Summed full #%d-#%d',bb(3),bb(3)+bb(6)-1))
  imshow(im,[])
  set(gcf,'units','normalized','position',[0 0.6 0.3 0.3])
  set(gca,'position',figpos)

  figure(fig_blob)
  clf
  set(gcf,'name',sprintf('Difblob #%d',ids(iw)));
  imshow(difspot,[])
  set(gcf,'units','normalized','position',[0.3 0.6 0.3 0.3])
  set(gca,'position',figpos)

  figure(fig_diff)
  clf
  imshow(im-difspot,[])
  set(gcf,'units','normalized','position',[0.3 0.2 0.3 0.3])
  colorbar
  set(gca,'position',figpos)

  if bb(6)>1
    GtVolView(vol);
    fig_vol=gcf;
    set(gcf,'units','normalized','position',[0.6 0.6 0.4 0.4])
  else
    fig_vol=[];
  end
  
  pause

  close(fig_vol)
    
end
