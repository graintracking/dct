function handles = gtSegmentationUpdate(handles)

% read data if required / re-threshold data if required
% note that we must read full images (despite the ROI) if we want to 
% subtract the median background

%handles.values_changed = true;
% more specific details
%handles.bb_changed = true; % re-threshold stacks
%handles.thresholds_changed = true; % re-threshold stacks
%handles.roi_changed = true; % reload stacks, then rethreshold

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% load image data
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


if handles.roi_changed || handles.bb_changed 
% if the bb has changed, the median subtraction changes so must reload
% if bb has changed, have to reload to update the part which is blanked out.  This could be cleverer...        

    set(handles.status_label, 'string', 'updating images', 'backgroundColor', 'y')
    set(handles.ROI_BB_status_label, 'string', 'updating images', 'backgroundColor', 'y')
    
    drawnow
    
    % Get the ROI
    ustart = str2double(get(handles.u_start, 'string'));
    vstart = str2double(get(handles.v_start, 'string'));
    uend = str2double(get(handles.u_end, 'string'));
    vend = str2double(get(handles.v_end, 'string'));
    bb = [ustart vstart uend-ustart+1 vend-vstart+1];
    
    ndx1 = str2double(get(handles.image_start, 'string'));
    ndx2 = str2double(get(handles.image_end, 'string'));
    nimages = ndx2 - ndx1 + 1;
    % Allocate the stack of images
    handles.stack = zeros(bb(4), bb(3), nimages);
    % eventually required for output
    handles.segvol = zeros(bb(4), bb(3), nimages);
    handles.segvol_jet = zeros(bb(4), bb(3), nimages);
    
    handles.Segmented_result_clims = [0 1];
    handles.Segmented_result_cmap = gtRandCmap(63);
    
    if handles.debug
        disp('Loading data')
    end
        
    % Get the entire first image for the bounding box gui
    handles.im = gtGetFullImage(ndx1);
%     % Update the image
%     handles = gtSegmentationUpdateBBimage(handles);
    
    if get(handles.subtract_median, 'value')
        % read full images for the median background
        for ii=ndx1:ndx2
            if ii==ndx1
                % get the image and the info to recycle
                [img, med_value, info] = gtReadAndThreshold(ii, handles.parameters, NaN, true);
                % subtract the median
                img=img-med_value;
            else
                % recycle info
                [img, med_value, info] = gtReadAndThreshold(ii, handles.parameters, NaN, true, info);
                % subtract the median
                img=img-med_value;
            end
            % add this image to the stack
            handles.stack(:,:,ii-ndx1+1) = img(bb(2):(bb(2)+bb(4)-1), bb(1):(bb(1)+bb(3)-1));
            set(handles.ROI_BB_status_label, 'string', sprintf('updating images: %d', ii), 'backgroundColor', 'y')
            drawnow
        end
    else
        % Get the stack of images
        for ii=ndx1:ndx2
            img = edf_read(fullfile(handles.parameters.acq(1).dir, '1_preprocessing/full', sprintf('full%04d.edf', ii)), bb);
            % blank out the seg.bbox
            segbb=handles.parameters.seg.bbox;
            % change to start and end coordinates
            seg_ustart=segbb(1);
            seg_vstart=segbb(2);
            seg_uend=segbb(1)+segbb(3)-1;
            seg_vend=segbb(2)+segbb(4)-1;
            % segbb in roi bb:
            seg_ustart = max(1, seg_ustart - ustart + 1);
            seg_vstart = max(1, seg_vstart - vstart + 1);
            seg_uend   = min(seg_uend - ustart + 1, uend);
            seg_vend   = min(seg_vend - vstart + 1, vend);
            % anything to change?
            if seg_uend>seg_ustart && seg_vend>seg_vstart
                img(seg_vstart:seg_vend, seg_ustart:seg_uend)=0;
            end
            % add to the stack
            handles.stack(:,:,ii-ndx1+1) = img;
            set(handles.ROI_BB_status_label, 'string', sprintf('updating images: %d', ii), 'backgroundColor', 'y')
            drawnow
        end
        
    end
    
    % Update the labels on the current image slider
    set(handles.first_image_slider_label, 'string', sprintf('Image: %d', ndx1))
    set(handles.last_image_slider_label, 'string', sprintf('Image: %d', ndx2))
    set(handles.image_slider, 'Min', ndx1, 'Max', ndx2, 'Value', ndx1, 'SliderStep', [1/(ndx2-ndx1) 1/(ndx2-ndx1)])
    set(handles.slider_label, 'String', sprintf('Current image %d', ndx1))
    handles.current_image = ndx1;
    
    % Update the clims slider
    clims = [min([handles.stack(:); handles.im(:)]) max([handles.stack(:); handles.im(:)])];
    set(handles.slider_min, 'Min', clims(1), 'Max', clims(2), 'Value', -0.5, 'SliderStep', [0.01 0.01])
    set(handles.slider_max, 'Min', clims(1), 'Max', clims(2), 'Value', clims(2)/2, 'SliderStep', [0.01 0.01])
    handles.clims_raw=[-0.5 clims(2)/2];
    
    % Update the bb selection image
    handles = gtSegmentationUpdateBBimage(handles);
    % Update the ROI figures
    handles = gtSegmentationUpdateROIimages(handles);
    
    % We have updated, so reset the flag.
    handles.roi_changed = false;
    handles.bb_changed = false;
    set(handles.ROI_BB_status_label, 'string', 'up to date', 'backgroundColor', 'g')
    
    drawnow
else
    % XXX - to remove?
    disp('Not reloading images')
end


if handles.thresholds_changed
        % Update the ROI figures
    handles = gtSegmentationUpdateROIimages(handles);

    set(handles.status_label, 'string', 'up to date', 'backgroundColor', 'g')
    drawnow
end