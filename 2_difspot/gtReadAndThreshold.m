function [img, med_value, info] = gtReadAndThreshold(ndx, parameters, thr_single, nodisp, info)
% GTREADANDTHRESHOLD  Reads the full image with number 'ndx' and thresholds
% it. It can also return the median value of the full image.
%
% It sets the image to zero inside the segmentation bounding box
% (parameters.seg.bbox) and outside where it is under the threshold.
% Can subtract the remaining median background of the full image
% (calculated and applied outside seg.bbox).
% Thresholds to seg.thr_single by default, or allow another threshold
% value to be passed in. Pass in NaN to return the greyscale image without
% thresholding.
%
% INPUT
%   ndx                       - full image number to be read
%   parameters.
%     prep.correct_drift
%     prep.correct_drift_iteration
%
%     seg.thr_single          - the threshold value to be applied
%     seg.bbox                - bounding box of area omitted in segmentation
%     seg.background_subtract - if true, the full image's median will be
%                               offset to zero
%
% Optional INPUT
%   thr_single                - threshold value that overrides seg.thr_single;
%                               if NaN, no thresholding is done
%   nodisp                    - if true, no text will be displayed
%   info                      - EDF header structure
%
% OUTPUT
%   im        - segmented image
%   med_value - median value of the full image
%   info      - EDF header structure
%
% [img, med_value, info] = gtReadAndThreshold(ndx, parameters, thr_single, nodisp, info)
%
%
% Version 002 08-05-2012 by P.Reischig
%   Updated to new parameters (thr_single) and cleaned.
%
% Modified by Nicola Vigano', 2012, nicola.vigano@esrf.fr
%   Changed gtPlaceSubImage -> gtPlaceSubImage2, added EDF header recycling.
%

    seg  = parameters.seg;
    prep = parameters.prep;

    % if a threshold other than th2 is passed in
    if (~exist('thr_single', 'var'))
        thr_single = seg.thr_single;
    end
    if (~exist('nodisp', 'var'))
        nodisp = false;
    end
    out = GtConditionalOutput(~nodisp);

    % should we wait for images to be shifted by gtApplyDrifts?
    waitfortranslated = false;

    if isfield(prep, 'correct_drift') && strcmp(prep.correct_drift, 'required')
        waitfortranslated = prep.correct_drift_iteration;
    end

    fname = sprintf('1_preprocessing/full/full%04d.edf', ndx);

    out.fprintf('now processing image %s\n', fname);

    if (~exist('info', 'var') || isempty(info))
        [im_full, info] = edf_wait_read(fname, [], waitfortranslated);
    else
        [im_full, info] = edf_wait_read(fname, [], waitfortranslated, info);
    end

    % would be very easy to add this as an option in gtCreateFullLive - do
    % it once there and then not again...
    if (isfield(seg, 'background_subtract') && (seg.background_subtract == true))
        %subtract the median of the background
        %taken from gtFullStatCondor - thanks Peter!
        nullbb = zeros(seg.bbox(4), seg.bbox(3));
        imn    = gtPlaceSubImage2(nullbb, im_full, seg.bbox(1), seg.bbox(2));
        
        % can use a sub set of pixel to calculate the median, it's faster
        if isfield(seg, 'background_subtract_accelerate') && seg.background_subtract_accelerate == true
            out.fprintf('/// andy speed up by calculating median on subset of pixels ///\n');
            imn = imn(1:127:end);
        else
            % the original routine
            out.fprintf('subtracting median background value\n');
        end
        
        imn    = imn(~isnan(imn));
        med_value = median(imn);
        im_full   = im_full - med_value;
         
    else
        med_value = NaN;
    end

    % now blank out the extinction image in the centre
    img = gtPlaceSubImage2( zeros(seg.bbox(4), seg.bbox(3)), im_full, ...
                            seg.bbox(1), seg.bbox(2));

    % do the thresholding
    if ~isnan(thr_single)
        img(img < thr_single) = 0;
    end
end
