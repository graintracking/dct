

function gtSegmentDiffractionBlobs2D(first, last, workingdirectory)

% GTSEGMENTDIFFRACTIONBLOBS_DOUBLETHR  Double threshold segmentation
%     gtSegmentDiffractionBlobs_doublethr(first, last, workingdirectory)
%     -----------------------------------------------------------
%
%       A simple segmentation function that doesn't integrate blobs in the
%       stack direction, suitable for polychromatic experiments.  It is a
%       marker-mask segmentation, with non-adaptive thresholds.  There is
%       no setup gui at the moment.  It writes difblobs to the difblob
%       table, so it is necessarry to run gtDBBlob2SpotTable afterwards...
%
%  
%      INPUT
%       In the parameters file:
%       parameters.seg.
%
%       bbox          - the segmentation bounding box; this area is omitted 
%                       in the segmentation
%
%       seedminarea   - the minimum seed size in a single image that will 
%                       be used to grow a seed into a blob (in pixels)
%
%       minblobsize   - blobs under this size will not be considered
%
%       maxblobsize   - blobs outside these dimensions will not be
%                       written to the database; important to stop things
%                       from blowing up - like the glow around the direct 
%                       beam for eg.
%
%       thr_seed      - the seed threshold to find potential blobs (marker)
%
%       thr_single    - the segmentation threshold (mask)
%
%
%      OUTPUT
%       Segmented diffraction blobs in the databse.
%
%
if 0
gtOarLaunch('gtSegmentDiffractionBlobs2D', 0, totproj-1, 30, parameters.acq.dir, true,...
            'walltime',OAR_parameters.walltime);
end

if isdeployed
    global GT_DB
    global GT_MATLAB_HOME
    load('workspaceGlobal.mat');
    % don't show all the messages - as this makes the .out file very large
    out = GtConditionalOutput(false);
    disp('running compiled, so will show minimal messages only')
else
    out = GtConditionalOutput(true);
    disp('displaying the info messages')
end % special case for running interactively in current directory


if isdeployed
    first=str2double(first);
    last=str2double(last);
end % special case for running interactively in current directory

if ~exist('workingdirectory','var')
    workingdirectory=pwd;
end

fprintf('Changing directory to %s\n',workingdirectory)
cd(workingdirectory)
parameters=[];
load('parameters.mat');
acq=parameters.acq;
seg=parameters.seg;

MaxBlobSize=seg.maxblobsize; % limits blob size against blowing up
MinBlobSize=seg.minblobsize; % don't save tiny blobs
SeedMinArea=seg.seedminarea; % minimum marker area (in a single image)

background_subtract=false;
if isfield(seg, 'background_subtract') && seg.background_subtract==true
    background_subtract=true;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% connect to database
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
gtDBConnect
table_difblob=sprintf('%sdifblob',parameters.acq.name);
table_difblobdetails=sprintf('%sdifblobdetails',parameters.acq.name);


%%%%% LOOP THROUGH IMAGES

for imagez=first:last
    
    % read, subtract median background
    [im, med_val] = gtReadAndThreshold(imagez, parameters, NaN);
    % NaN arguement means this returns the greyscale image without thresholding
    
    % th1 threshold - markers
    marker_im=im>parameters.seg.thr_seed;
    % th2 threshold - mask for reconstruct
    mask_im=im>parameters.seg.thr_single;
    
    % label (2D only)
    marker_im=bwlabel(marker_im);
    mask_im=bwlabel(mask_im);
    L=max(marker_im(:))
    
    for j=1:L
        
        marker=marker_im==j;
        
        if length(find(marker))<SeedMinArea
           % disp('skip a badly sized marker')
            continue
        end
        
        %spot=imreconstruct(marker, mask_im);
        mask_id = unique(mask_im(find(marker)));
        spot = mask_im==mask_id;
        
        % test size
        %bb=regionprops(double(spot), 'BoundingBox');
        %bb=bb.BoundingBox;
        %bb=ceil(bb);
        
        indicies=find(spot);
        [yy,xx]=ind2sub(size(spot), indicies);
        bb(3) = max(xx)-min(xx)+1;
        bb(4) = max(yy)-min(yy)+1;
        
        
        if all(bb(3:4)>MinBlobSize(1:2)) && all(bb(3:4)<MaxBlobSize(1:2)) % size test
            
            % update marker and mask to avoid duplicating spots
            marker_im(spot)=0;
            mask_im(spot)=0;
                        
            % make the grayscale spot
            spot=spot.*im;
            
            sfWriteSpot(spot, imagez, table_difblob, table_difblobdetails);
        else
          %  disp('skip a badly sized spot')
        end
        
    end
    fprintf('done image %d...\n', imagez)
end

%%%%%% END OF MAIN LOOP

end

% 
% function [testimage, testimage2]=sfWriteSpot(greyspot, zz, table_difblob, table_difblobdetails, testimage, testimage2)
% 
% testimage=testimage+greyspot;
% testimage2=testimage2+(greyspot>0);
% 
% 
% 
% end



function sfWriteSpot(greyspot, zz, table_difblob, table_difblobdetails)
% write a good blob to the database

% convert to DB format, write to database
% format is x, y, z, greyval
indicies=find(greyspot>0);
[yy,xx]=ind2sub(size(greyspot), indicies);

vals=greyspot(indicies);

% from original gtSegmentDiffractionBlobs
mym(gtDBInsert(table_difblob,{'difblobID'},0))
difblobID=mym('select last_insert_id()');

disp('***DB WRITE A BLOB *** ')

gtDBInsertMultiple(table_difblobdetails,...
    'difblobID',repmat(difblobID,1,length(xx)),...
    'xIndex',xx,...
    'yIndex',yy,...
    'zIndex',repmat(zz,1,length(xx)),...
    'graylevel',vals);

end







