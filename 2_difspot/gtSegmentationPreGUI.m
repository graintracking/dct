function gtSegmentationPreGUI(parameters)

if (~exist('parameters', 'var') || isempty(parameters))
    parameters = gtLoadParameters();
end

% Needed to avoid "static workspace assignment" error
parameters_name = fullfile(parameters.acq(1).dir,'parameters.mat');

check = inputwdefault('Do you want to reset all the parameters for segmentation? [y/n]','n');
if strcmpi(check, 'y')
     parameters.seg = gtSegDefaultParameters();
     save(parameters_name,'parameters');
     disp('Segmentation parameters have been reset to default values')
 end

% Let's check every field is in, and in case let's update it!
parameters = gtCheckParameters(parameters, 'seg', 'verbose', true);
save(parameters_name, 'parameters')


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% recompile binaries - not often needed.  But more often than it should be...
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
check = inputwdefault('Recompile functions for OAR? Not normally needed! [y/n]', 'n');
if strcmpi(check,'y')
    gtExternalCompileFunctions('gtSegmentationDoubleThreshold')
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% how many images in this scan?
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
totproj = gtAcqTotNumberOfImages(parameters) - 1;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Safety check - are all the images present?
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp('checking for preprocessed images...')
d=dir(fullfile(parameters.acq(1).dir,'1_preprocessing/full/full*edf'));
test_full=length(d);
target_ims = (totproj+1);
if (test_full < target_ims)
    % some fulls are missing!
    fprintf('!!! Not all full images are present (%d found, %d expected)\n', test_full, target_ims)
    disp('This could be normal if you are processing "live" and still producing images')

    check = inputwdefault('Quit gtSetupSegmentation_doublethr? [y/n]', 'y');
    if strcmpi(check, 'y')
        % quit, but first check what other images are missing
        disp('quitting - first checking what images are present....')
        d = dir(fullfile(parameters.acq(1).dir,'1_preprocessing/abs/abs*edf'));
        test_abs = length(d);
        d = dir(fullfile(parameters.acq(1).dir,'1_preprocessing/abs/med*edf'));
        test_abs_med = length(d);
        d = dir(fullfile(parameters.acq(1).dir,'1_preprocessing/full/med*edf'));
        test_full_med = length(d);
        % how many should we have?
        target_abs_med = (target_ims/parameters.prep.absint)+1;
        target_full_med = (target_ims/parameters.prep.fullint)+1;
        fprintf('Found full: %d/%d; abs: %d/%d; abs median: %d/%d; full median: %d/%d\n', ...
            test_full, target_ims, test_abs, target_ims, test_abs_med, target_abs_med, test_full_med, target_full_med)
        disp('Probably some preprocessing OAR jobs have failed')
        gtError('SEG:invalid_state', 'Error: Missing images!')
    end
else
    fprintf('Found all full images (%d found, %d expected)\n', test_full, totproj+1)
end

% if all images are present, check intensities in direct beam
if (~parameters.acq(1).no_direct_beam && (test_full == target_ims))
    check = inputwdefault('Check intensities in direct beam ? [y/n]', 'y');
    if strcmpi(check, 'y')
        fprintf('Checking every 20th image in 0_rawdata/%s...\n', parameters.acq(1).name)
        figure
        hold on
        fint = zeros(1, target_ims);
        fbb = floor(parameters.acq(1).bb(1:2) + (parameters.acq(1).bb(3:4)/2));
        fbb(3:4) = 2; % read in 2x2 pixels in centre of sample
        for img_num = 1:20:target_ims
            froi = edf_read(fullfile(parameters.acq(1).dir, '0_rawdata', sprintf('%s/%s%04d.edf', parameters.acq(1).name, parameters.acq(1).name, img_num-1)), fbb);
            fint(img_num) = sum(froi(:))/4;
            plot(img_num-1, fint(img_num), 'gx')
            if (mod(img_num, 100) == 1)
                drawnow
            end
        end
        disp('Does the intensity look okay?')
        check = inputwdefault('Continue with segmentation? [y/n]', 'y');
        if strcmp(check, 'n')
            disp('quitting... ')
            return
        end
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% before starting, set up the required database tables
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp(' ')
gtDBConnect();
disp(' ')
disp('NOTE! When restarting the segmentation, all tables should be dropped!')
disp(' ')

% Are there already data in the tables?
test1 = mym(['select count(*) from ' parameters.acq(1).name 'difblobdetails']);
test2 = mym(['select count(*) from ' parameters.acq(1).name 'difblob']);
if test1 || test2
    disp('There is already data in the difblob tables')
    check = inputwdefault('Drop and recreate difblob tables? [y/n]', 'y');
    if strcmpi(check, 'y')
        gtDBCreateDifblobTable(parameters.acq(1).name, 1)
    end
end
test = mym(['select count(*) from ' parameters.acq(1).name 'difspot']);
if test > 0
    disp('There is already data in the difspot table')
    check = inputwdefault('Drop and recreate difspot tables? [y/n]', 'y');
    if strcmpi(check, 'y')
        gtDBCreateDifspotTable(parameters.acq(1).name, 1)
    end
end

% fullmedianvals table
test = mym(['show tables like "' parameters.acq(1).name 'fullmedianvals"']);
if ~isempty(test) % if exists, check before dropping
    check = inputwdefault('Drop and recreate fullmedianvals table? [y/n]', 'y');
    if strcmpi(check, 'y')
        mym(['drop table ' parameters.acq(1).name 'fullmedianvals'])
        mysqlcmd = ['create table ' parameters.acq(1).name ...
            'fullmedianvals (ndx int, val double, primary key (ndx)) ENGINE=MyISAM'];
        mym(mysqlcmd);

        % insert the image numbers in the table
        gtDBInsertMultiple([parameters.acq(1).name 'fullmedianvals'], 'ndx', 0:totproj)
    end
else
    mysqlcmd = ['create table ' parameters.acq(1).name ...
        'fullmedianvals (ndx int, val double, primary key (ndx)) ENGINE=MyISAM'];
    mym(mysqlcmd);

    % insert the image numbers in the table
    gtDBInsertMultiple([parameters.acq(1).name 'fullmedianvals'], 'ndx', 0:totproj)
end

parameters.seg.method = 'doublethr_new';

% one question - does user want to subtract the background before
% segmentation?
% cound make this a buttun / option in the function
disp(' ')
check = inputwdefault('Offset the median value of each full image to 0 (recommended)? [y/n]', 'y');
parameters.seg.background_subtract = strcmpi(check, 'y');

gtSaveParameters(parameters);

% check existing difspot images and delete them (also the directories)
if exist(fullfile(parameters.acq(1).dir,'2_difspot','00000','difspot00001.edf'),'file')
    check = inputwdefault('Do you want to remove all the difspot images from *0000 sub-folders before going on? [y/n]','y');
    if strcmpi(check, 'y')
        listDirs=dir(fullfile(parameters.acq(1).dir,'2_difspot','*0000'));
        for ii=1:length(listDirs)
            rmdir(fullfile(parameters.acq(1).dir,'2_difspot',listDirs(ii).name),'s');
        end
    end
elseif exist(fullfile(parameters.acq(1).dir,'2_difspot','difspot00001.edf'),'file')
    check = inputwdefault('Do you want to remove all the difspot images before going on? [y/n]','y');
    if strcmpi(check, 'y')
        delete(fullfile(parameters.acq(1).dir,'2_difspot','difspot*.edf'));
    end
end

% create the first '00000' folder by default
[~,msg] = mkdir(fullfile(parameters.acq.dir,'2_difspot','00000')); disp(msg)

end % end of gtSegmentationPreGUI
