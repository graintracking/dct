function gtCheckDiffractionBlobs(startndx)
% GTCHECKDIFFRACTIONBLOBS  Display diffraction blobs loaded from the database
%
%     gtCheckDiffractionBlobs(start)
%     -------------------------------------------------------------------------
%
%     OPTIONAL INPUT:
%       startndx = <int>  Starting index, you can use L/R arrows to change
%
%     KEYS:
%       Arrows - 1 index shift (10 indexes by pressing Shift)
%       Enter  - close GUI and quit
%
%     SUB-FUNCTIONS:
%[sub]- sfInitialise(im0, im1, varargin)
%[sub]- sfKeyPress
%[sub]- sfUpdateFigure
%[sub]- closeFigure()
%
%     Version 001 23-05-2013

if (~exist('startndx', 'var') || isempty(startndx))
    startndx = 1;
end

% Initialization
app = sfInitialise(startndx);
app.pix = [];

% Image Processing Toolbox license test before callling impixelinfo
% This is to avoid crashing when no license is available.
showpix = license('checkout','Image_Toolbox');


disp('KEYS:');
disp('  Arrows - 1 index shift (10 indexes by pressing Shift)');
disp('  Enter  - close GUI and quit');
sfUpdateFigure();

% Main loop for console-blocking mode
%if strcmp(app.block,'on')
    % if user wants nothing to return until findshift is finished
    while 1
        if (app.quit), break; end
        pause(0.001);
        drawnow;
    end
%end

%if strcmp(app.block, 'on')
    set(app.window, 'KeyPressFcn', '');
    delete(app.window);
%end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Private functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    function conf = sfInitialise(start)
        
        iptsetpref('ImtoolInitialMagnification', 'fit');
        
        par = load('parameters');
        conf = [];
        conf.scanName = par.parameters.acq.name;
        conf.index = start;
        
        gtDBConnect();
        mymCmd = sprintf('select count(*) from %sdifblob', conf.scanName);
        conf.indexMin = 1;
        conf.indexMax = mym(mymCmd);
        
        conf.quit  = false;
        %conf.block = 'off';
        conf.image = [];
		
        % Let's create the figure
        conf.window = figure( ...
                'Name', 'Segmented diffraction blobs', ...
                'NumberTitle', 'off', ...
                'MenuBar', 'none', ...
                'Toolbar', 'none'); % create a new figure

        % Setting axes to display the image
        conf.axes1 = axes( 'Parent', conf.window, ...
                           'ActivePositionProperty', 'OuterPosition');
        colormap(conf.axes1, 'gray');

        % Callbacks
        set(conf.window, 'KeyPressFcn', @sfKeyPress);
        set(conf.window, 'CloseRequestFcn', @closeFigure);
    end

    function sfKeyPress(~, evt)
    % Usage guide, from matlab doc: 
    %
    % -- KeyPressFcn Event Structure.
    % When you specify the callback as a function handle, MATLAB passes to it a
    % structure to containing the following fields.
    %
    % Field     | Contents
    % ----------|------------------
    % Character | The character displayed as a result of the pressing the
    %           | key(s), which can be empty or unprintable
    % Key       | The key being pressed, identified by the lowercase label on
    %           | key or a descriptive string
    % Modifier  | A cell array containing the names of one or more modifier
    %           | keys being pressed (i.e., control, alt, shift).
    %           | On Macintosh computers, it contains 'command' when pressing
    %           | the command modifier key.
        switch (evt.Key)
            case 'leftarrow'
                if strcmp(evt.Modifier, 'shift')
                    app.index = app.index - 10;
                else
                    app.index = app.index - 1;
                end
                app.index = max(app.index, app.indexMin);
                app.index = min(app.index, app.indexMax);
                
                sfUpdateFigure();
            case 'rightarrow'
                if strcmp(evt.Modifier, 'shift')
                    app.index = app.index + 10;
                else
                    app.index = app.index + 1;
                end
                app.index = max(app.index, app.indexMin);
                app.index = min(app.index, app.indexMax);
                
                sfUpdateFigure();
            case 'return'
                % Exit interactive mode and return current shift
                closeFigure(0,0);
        end
    end

    function sfUpdateFigure()
        img = sum(gtDBBrowseDiffractionVolume(app.scanName, app.index), 3);
        delete(app.image);
		app.image = imagesc(img, 'Parent', app.axes1);
        axis(app.axes1, 'image');
        title_str = sprintf('Blob %d / %d', app.index, app.indexMax);
        title(app.axes1, title_str);
		
		% Image pixel values (needs to be called each time image changes)
		if showpix
			% add pixel info (it deletes automatically when image is deleted) 
			app.pix = impixelinfoval(app.window, app.image);
		end
    end

    function closeFigure(~, ~)
        %if strcmp(app.block,'on')
            app.quit = true;
        %else
        %   delete(app.window);
        %end
    end

end
