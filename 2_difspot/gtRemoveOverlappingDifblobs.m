function gtRemoveOverlappingDifblobs()
% function gtRemoveOverlappingDifblobs()
% after running gtSegmentDiffractionBlobs in parallel, need to clean up
% where blobs have been split between two condor jobs.

gtDBConnect

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% load('parameters.mat');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
load('parameters.mat');

if isfield(parameters.seg, 'overlaps_removed') && parameters.seg.overlaps_removed==true
    % check that this has not already been done
    disp('gtRemoveOverlappingDifblobs has already been run for this dataset!')
    disp('quitting...')
    return
else
    % otherwise, update parameters file and run
    parameters.seg.overlaps_removed=true;
    save parameters parameters
end

% construct table names
tdetails=sprintf('%sdifblobdetails',parameters.acq.name);
tinfo=sprintf('%sdifblobinfo',parameters.acq.name);
tblob=sprintf('%sdifblob',parameters.acq.name);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Run function
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  [first,last]=mym(sprintf('select startIndex,endIndex from %s order by startIndex',tinfo));
  length(first);
  first=first';

  for chunk=first(2:end)
    chunk
    blobIDs=mym(sprintf('select distinct difblobID from %s where zIndex=%d order by difblobID',tdetails,chunk));
    % keyboard
    %  blobIDs=[359];
    n=1;
    double=0;
    single=0;
    while n<=length(blobIDs)
      % grab a single voxel from a blob
      [x,y]=mym(sprintf('select xIndex,yIndex from %s where difblobID=%d and zIndex=%d limit 1',tdetails,blobIDs(n),chunk));
      % find the two blobx
      twoblobs=mym(sprintf('select distinct difblobID from %s where xIndex=%d and yIndex=%d and zIndex=%d',tdetails,x,y,chunk));
      % sometimes there are two entries with the same difblobID - where does
      % this come from???  the 'distinct' fixes it, but still...

      if length(twoblobs)==2
        fprintf('Double blob: %d of %d\n',n,length(blobIDs));
        double=double+1;
        % first get rid of the overlapping voxels in the second blob:
        mysqlcmd=sprintf('delete from %s where difblobID=%d and zIndex=%d',tdetails,max(twoblobs),chunk);
        mym(mysqlcmd)
        % then merge them
        mysqlcmd=sprintf('update %s set difblobID = %d where difblobID=%d',tdetails,min(twoblobs),max(twoblobs));
        mym(mysqlcmd)
        % now clean up the listing for blobIDs
        mysqlcmd=sprintf('delete from %s where difblobID=%d',tblob,max(twoblobs));
        mym(mysqlcmd)
        %keyboard
        blobIDs(find(blobIDs==max(twoblobs)))=[];
      elseif length(twoblobs)==1
        % keyboard
        disp('Single')
        single=single+1;
      else
        disp('MORE THAN TWO BLOBS!')
      end
      n=n+1;

    end
    disp(sprintf('Single/Double %d %d',single,double))
  end

  
