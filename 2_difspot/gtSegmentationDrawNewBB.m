function handles = gtSegmentationDrawNewBB(handles)

p1 = get(handles.select_bb_image, 'CurrentPoint');
p1 = p1(1, 1:2);

% use rubberband box area selection
bbox = rbbox();
p2 = get(gca, 'CurrentPoint');
p2 = p2(1, 1:2);
% convert to bbox
bborigin = round(min(p1, p2));
bbsize   = round(abs(p1 - p2) + 1);

% check that values are okay (no stray clicks outside image), or
% tiny bboxes
if (any(bborigin < 1) ...
        || any((bborigin+bbsize) > [handles.parameters.acq(1).xdet handles.parameters.acq(1).ydet]) ...
        || any(bbsize < 5) )
    return
end

% put this info after the test to avoid contast output!
if handles.debug
    disp('in gtSegmentationDrawNewBB')
end

bbox = [bborigin bbsize];
handles.parameters.seg.bbox = bbox;
handles.bb_changed = true;

% Update the bb selection image
handles = gtSegmentationUpdateBBimage(handles);
set(handles.ROI_BB_status_label, 'string', 'BB changed', 'backgroundColor', 'y')



