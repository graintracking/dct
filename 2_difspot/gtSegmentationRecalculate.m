function handles = gtSegmentationRecalculate(handles)
% threshold the ROI, with full double threshold algorithm

% make sure that the threshold parameters are up to date
thr_grow_rat = str2num(get(handles.thr_grow_ratio, 'string'));
minblobsize = str2num(get(handles.min_blob_size, 'string'));
maxblobsize = str2num(get(handles.max_blob_size, 'string'));
extendblobinc = str2num(get(handles.extend_blob_inc, 'string'));
thr_seed = str2num(get(handles.seed_thr, 'string'));
seedminarea = str2num(get(handles.seed_min_area, 'string'));
thr_grow_high = str2num(get(handles.thr_grow_high, 'string'));
thr_grow_low = str2num(get(handles.thr_grow_low, 'string'));

% volume for output
vol = handles.stack;
segvol = zeros(size(vol));
segvol_jet = zeros(size(vol));
[volY, volX, volZ] = size(vol);

% find the seed pixels
stack_structure.vol = handles.stack;
stack_structure.seg.thr_seed = thr_seed;
stack_structure.seg.seedminarea = seedminarea;
stack_structure.sizeX = volX;
stack_structure.sizeY = volY;
stack_structure.sizeZ = volZ;
seedList = gtSegmentationFindSeeds(stack_structure);
set(handles.status_label, 'string', sprintf('Found %d seeds...', size(seedList, 1)), 'backgroundColor', 'y')
drawnow

% grow blobs from these seeds
blobCount = 0;
for ii = 1:size(seedList, 1)
    
    if seedList(ii, 7)
        if handles.debug
         disp('seed already marked as bad')
        end
        continue
    end
    
    % Calculate threshold, considering lower and upper grow limits
    seedInt = seedList(ii, 6);
    thr_grow = thr_grow_rat * seedInt;
    if thr_grow < thr_grow_low
        thr_grow = thr_grow_low;
    end
    if thr_grow > thr_grow_high
        thr_grow = thr_grow_high;
    end
        
   % bounding box of seed voxel
   bb = [seedList(ii, 2:4) 1 1 1];  
   seedX = seedList(ii, 2);
   seedY = seedList(ii, 3);
   seedZ = seedList(ii, 4);
   
   % extend bb, threshold, and test whether blob fits within the bb
   test = ones(1,6);
    
      
   while any(test)
       
       % extend the bounding box
       %disp('extending blob');
       bb = sfExtendBB(test, bb, extendblobinc, volZ, volX, volY);
       
       % get the sub volume to work on
       greyvol = vol(bb(2):(bb(2)+bb(5)-1), bb(1):(bb(1)+bb(4)-1), bb(3):(bb(3)+bb(6)-1));
       
       % label volume
       threshvol = greyvol > thr_grow;
       threshvol = bwlabeln(threshvol);
       
       % pick out label containing seed
       bloblabel = threshvol(seedY-bb(2)+1, seedX-bb(1)+1, seedZ-bb(3)+1);
       threshvol = (threshvol == bloblabel);
       
       % apply the label as a mask
%       rawgreyvol = greyvol; % save the un-masked greyvol
       greyvol    = greyvol .* threshvol;
       
       % test intensity
       test_int = sfTestInt(greyvol, seedInt);
       if (test_int)
           if handles.debug
               disp('blob contains a value higher than the seed - bad');
           end
       end
       
       % test - max size exceeded?
       % ... set all connected lower seeds to bad - any lower seed will give a
       % lower th2 and hence larger blob
       test_size = sfTestBlobMaxSize(threshvol, maxblobsize);
       if (test_size)
           if handles.debug
               disp('blob blob exceeds the maximum size - bad')
           end
       end
       
       if (test_int || test_size)
           % blob is bad - set contain, lower value seeds to zero
           seedList = sfBadBlob(greyvol, bb, seedInt, seedList);
           break
       end
       
       % test - does blob reach edges of sub volume?
       % ...do the while loop again
       test = sfTestVol(threshvol, bb, volZ, volX, volY);
       
   end % extend ROI loop
   
   % minimum size check
   test_min_size = sfTestBlobMinSize(threshvol, minblobsize);
   
   if test_int==0 && test_size==0 && test_min_size==0
       % only write good blobs
       % could use sfNoEdges here, but I think not needed for setup
       blobCount = blobCount+1;
       % add to the ouput volume
       if handles.debug
           disp('add a blob to the output volume')
       end
       
       [segvol, segvol_jet] = sfWriteBlob(blobCount, segvol, segvol_jet, threshvol, bb);
   end
   
end % loop through seed volumes

% copy output to handles
handles.segvol = segvol;
handles.segvol_jet = segvol_jet;

set(handles.status_label, 'string', sprintf('Found %d blobs...', blobCount), 'backgroundColor', 'g')

% change to random colour map
set(handles.colourmap_buttons, 'SelectedObject', handles.random_button)
% make a colormap appropriate to the output volume
% this code is repeated in the button group callback
maxval = max(handles.segvol(:));
maxval = max(maxval, 63);
cmap = gtRandCmap(maxval);

handles.Segmented_result_cmap = cmap;
handles.Segmented_result_clims = [0 maxval];
set(handles.figure1, 'Colormap', cmap)
        
% display the output
handles = gtSegmentationUpdateROIimages(handles);

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SUB-FUNCTIONS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%




function test = sfTestInt(greyvol, seedInt)
% trivial test of blob max intensity
% working with double precision numbers - build in a tiny safety margin for
% rounding
    test = (max(greyvol(:)) > (seedInt+0.00000000001));
end % of sub-function


function test = sfTestBlobMaxSize(threshvol, maxblobsize)
% find the extent of the thresholded blob, check against max size
    profilex = sum(sum(abs(threshvol), 3), 1)';
    profiley = sum(sum(abs(threshvol), 3), 2);
    profilez = squeeze(sum(sum(abs(threshvol), 1), 2));

    sizex = find(profilex, 1, 'last') - find(profilex, 1, 'first');
    sizey = find(profiley, 1, 'last') - find(profiley, 1, 'first');
    sizez = find(profilez, 1, 'last') - find(profilez, 1, 'first');

    test = any([sizex sizey sizez] > maxblobsize(1:3));

    % Additional test for consistency check! should not be needed.
    [sizex, sizey, sizez] = size(threshvol);
    if ((~test) && any([sizex sizey sizez] > (2 * maxblobsize(1:3))) )
        test = true;
        % If this happens, it's not a good thing: the growth of the blob is gone
        % beyond the limits and it is now including useless things
        warning('SEGMENTATION:logic_fail', ...
                'Blob BB extended with zeros beyond the double of blob size');
    end
end % of sub-function

function test = sfTestBlobMinSize(threshvol, minblobsize)
% find the extent of the thresholded blob, check against min size
    profilex = sum(sum(abs(threshvol), 3), 1)';
    profiley = sum(sum(abs(threshvol), 3), 2);
    profilez = squeeze(sum(sum(abs(threshvol), 1), 2));

    sizex = find(profilex, 1, 'last') - find(profilex, 1, 'first');
    sizey = find(profiley, 1, 'last') - find(profiley, 1, 'first');
    sizez = find(profilez, 1, 'last') - find(profilez, 1, 'first');

    test = any([sizex sizey sizez] < minblobsize(1:3));
end

    function seedList = sfBadBlob(greyvol, bb, seedInt, seedList)
        % not talking to the table
        % rather than labelling the blobs and search for seeds within them,
        % run through the list of known seeds with intensity<seedInt
        
        % find the seed voxels within the blob with intensity<seedInt,
        % set these to bad=1 in the table
        % voxels at the edge of the subvol could be indentified wrongly as seeds.
        % mostly copied from seed_threshold
        
        % find candidate bad seeds based on value and position
        % only consider seeds that are currently 'good'
        badseeds = find(seedList(:,6)<=seedInt & ...
            seedList(:,7)==0 & ...
            seedList(:,2)>=bb(1) & seedList(:,2)<(bb(1)+bb(4)) & ...
            seedList(:,3)>=bb(2) & seedList(:,3)<(bb(2)+bb(5)) & ...
            seedList(:,4)>=bb(3) & seedList(:,4)<(bb(3)+bb(6)) );
        % are these candidates part of the blob?
        for i = 1:length(badseeds)
            ndx = badseeds(i);
            sub = seedList(ndx, 2:4);
            % offset to our bounding box
            sub = sub - bb(1:3) + 1;
            if greyvol(sub(2), sub(1), sub(3)) > 0
                % this is a bad seed - lower intensity and connected to our
                % seed
                seedList(ndx, 7) = 1;
            end
        end
    end % % of sub-function


function test = sfTestVol(vol, bb, totproj, xdet, ydet)
% does a thresholded blob extend to the edges of the ROI?  And which
% edges?  Account for the limits of the subvol...

    test = zeros(1, 6); % ~same format as bb

    % low x
    test(1) = ( (bb(1) > 1) && ~isempty(find(vol(:, 1, :), 1, 'first')) );
    % low y
    test(2) = ( (bb(2) > 1) && ~isempty(find(vol(1, :, :), 1, 'first')) );
    % low z (omega) - note 1 because subvol, not 0 for image zero
    test(3) = ( (bb(3) > 1) && ~isempty(find(vol(:, :, 1), 1, 'first')) );
    % high x
    test(4) = ( ((bb(1)+bb(4)-1) < xdet) && ~isempty(find(vol(:, end, :), 1, 'first')) );
    % high y
    test(5) = ( ((bb(2)+bb(5)-1) < ydet) && ~isempty(find(vol(end, :, :), 1, 'first')) );
    % high z (omega)
    test(6) = ( ((bb(3)+bb(6)-1) < totproj) && ~isempty(find(vol(:, :, end), 1, 'first')) );
end % of sub-function


    function bb = sfExtendBB(test, bb, extendblobinc, totproj, xdet, ydet)
        %given a grey volume, and test which tells which sides to extend,
        %recalculate the bounding box
        if test(1) % extend bb in x -ve
            dif   = min(bb(1)-1, extendblobinc(1));
            bb(1) = bb(1) - dif;
            bb(4) = bb(4) + dif;
        end
        if test(2) % extend bb in y -ve
            dif   = min(bb(2)-1, extendblobinc(2));
            bb(2) = bb(2) - dif;
            bb(5) = bb(5) + dif;
        end
        if test(3) % extend bb in z -ve
            dif   = min(bb(3)-1, extendblobinc(3)); % subvolume ndx goes to 1, not image number which can go to zero
            bb(3) = bb(3) - dif;
            bb(6) = bb(6) + dif;
        end
        if test(4) % extend bb in x +ve
            dif   = min(xdet-(bb(1)+bb(4)-1), extendblobinc(1));
            bb(4) = bb(4) + dif;
        end
        if test(5) % extend bb in y +ve
            dif   = min(ydet-(bb(2)+bb(5)-1), extendblobinc(2));
            bb(5) = bb(5) + dif;
        end    
        if test(6) % extend bb in z +ve
            dif   = min(totproj-(bb(3)+bb(6)-1), extendblobinc(3));
            bb(6) = bb(6) + dif;
        end
    end % of sub-function
    
    function [segvol, segvol_jet] = sfWriteBlob(ndx, segvol, segvol_jet, threshvol, bb)
    % add the blob to the output volume
    segvol(bb(2):(bb(2)+bb(5)-1), bb(1):(bb(1)+bb(4)-1), bb(3):(bb(3)+bb(6)-1)) = ...
        segvol(bb(2):(bb(2)+bb(5)-1), bb(1):(bb(1)+bb(4)-1), bb(3):(bb(3)+bb(6)-1)) + ...
        threshvol*ndx;
    % generate a number between 1 and 6
    jet_ndx = mod(ndx, 6)+1;
    segvol_jet(bb(2):(bb(2)+bb(5)-1), bb(1):(bb(1)+bb(4)-1), bb(3):(bb(3)+bb(6)-1)) = ...
        segvol_jet(bb(2):(bb(2)+bb(5)-1), bb(1):(bb(1)+bb(4)-1), bb(3):(bb(3)+bb(6)-1)) + ...
        threshvol*jet_ndx;
    end
    
    
    
    
