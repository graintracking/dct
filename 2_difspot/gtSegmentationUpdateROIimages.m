function handles = gtSegmentationUpdateROIimages(handles)

if handles.debug
    disp('in gtSegmentationUpdateROIimages')
end

ndx1 = str2num(get(handles.image_start, 'string'));
stackndx = handles.current_image - ndx1 + 1;

% Show the raw data image
set(handles.figure1, 'CurrentAxes', handles.Raw_data_image)
imagesc(handles.stack(:,:,stackndx))
set(handles.Raw_data_image, 'Clim', handles.clims_raw)

% Show the seed threshold image
set(handles.figure1, 'CurrentAxes', handles.Seed_threshold_image)
thr_im =  handles.stack(:,:,stackndx) > str2num(get(handles.seed_thr, 'string'));
% apply the minimum area criteria
labels = bwconncomp(thr_im);
seedminarea = str2num(get(handles.seed_min_area, 'string'));
tmp=zeros(size(thr_im));

for ii = 1:labels.NumObjects
    % marker size test
    if (length(labels.PixelIdxList{ii}) > seedminarea)
        tmp(labels.PixelIdxList{ii})=1;
    end
end

imagesc(tmp)
if get(handles.colourmap_buttons, 'SelectedObject')==handles.jet_button
    set(handles.Seed_threshold_image, 'Clim', [0 2])
    % better contrast like this
else
     set(handles.Seed_threshold_image, 'Clim', [0 1])
end
    


% Show the threshold limits image
set(handles.figure1, 'CurrentAxes', handles.Threshold_grow_limits_image)
tmp =  uint8(handles.stack(:,:,stackndx)>str2num(get(handles.thr_grow_low, 'string')));
tmp =  tmp + uint8(handles.stack(:,:,stackndx)>str2num(get(handles.thr_grow_high, 'string')));
imagesc(tmp)

% Show the segmented result
set(handles.figure1, 'CurrentAxes', handles.Segmented_result)
if get(handles.colourmap_buttons, 'SelectedObject')==handles.random_button
    % use blobID labels for random cmap
    imagesc(handles.segvol(:,:,stackndx))
    set(handles.Segmented_result, 'Clim', handles.Segmented_result_clims)
else
    % use 1-8 labels for gray or jet cmaps
    imagesc(handles.segvol_jet(:,:,stackndx))
    set(handles.Segmented_result, 'Clim', [0 6])
end

end





    