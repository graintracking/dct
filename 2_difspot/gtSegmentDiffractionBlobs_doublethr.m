function gtSegmentDiffractionBlobs_doublethr(~, ~, workingdirectory)
% GTSEGMENTDIFFRACTIONBLOBS_DOUBLETHR  Double threshold segmentation
%     gtSegmentDiffractionBlobs_doublethr(~, ~, workingdirectory)
%     -----------------------------------------------------------
%
%       You can run the setup GUI instead of directly this function.
%
%       The segmentation first finds seeds of potential diffraction blobs 
%       as areas with intensity above a high threshold (thr_seed) and 
%       with size above a minimum value (seedminarea). 
%       
%       As a secondstep, it extends those seed areas according to a lower 
%       threshold value that is dependent on the maximum intensity of the 
%       actual seed. Thus this lower threshold (thr_grow) defines the final
%       blob boundaries. It is calculated as:
%            thr_grow = max(seed)*thr_grow_rat
%       but its final value will be limited to the absolut range:   
%            thr_grow_low < thr_grow < thr_grow_high
%
%
%       Comments:
%       When using this function, don't need to remove overlapping difblobs
%       (to avoid problems:  parameters.seg.overlaps_removed=true)
%
%       It could write difspot.edfs on disk, without ever passing through 
%       difblobs in the database. Would need to add functionality from 
%       gtDBBlob2SpotTable_WriteDifspots.
%
%  
%      INPUT
%       In the parameters file:
%       parameters.seg.
%
%       bbox          - the segmentation bounding box; this area is omitted 
%                       in the segmentation
%
%       seedminarea   - the minimum seed size in a single image that will 
%                       be used to grow a seed into a blob (in pixels)
%
%       minblobsize   - blobs under this size will not be considered
%
%       maxblobsize   - blobs outside these dimensions will not be
%                       written to the database; important to stop things
%                       from blowing up - like the glow around the direct 
%                       beam for eg.
%
%       thr_seed      - the seed threshold to find potential blobs
%
%       thr_grow_rat  - adaptive threshold parameters for growing seeds 
%       thr_grow_low    into blobs
%       thr_grow_high
%
%       extendblobinc - size of incremental blob bbox extension
%
%
%      OUTPUT
%       Segmented diffraction blobs in the databse.
%
%
%
%     Version 003 May-2012 by L.Nervo and P.Reischig
%       update to new parameters names; cleaning
%  
%     Version 002 04-08-2009 by P.Reischig
%       modification to adaptive threshold
%
%     Version 001 Jan-2009 by A.King
%
%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% setup
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if isdeployed
    global GT_DB
    global GT_MATLAB_HOME
    load('workspaceGlobal.mat');
    %     first=str2double(first);
    %     last=str2double(last);
    % don't show all the messages - as this makes the .out file very large
    out = GtConditionalOutput(false);
    disp('running compiled, so will show minimal messages only')
else
    out = GtConditionalOutput(true);
    disp('displaying the info messages')
end % special case for running interactively in current directory

if ~exist('workingdirectory','var')
    workingdirectory = pwd;
end

fprintf('Changing directory to %s\n',workingdirectory)
cd(workingdirectory)


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% load and unpack parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
parameters = [];
load('parameters.mat');

if strcmpi(parameters.acq.type, '360degree')
    totproj = parameters.acq.nproj*2;
else
    totproj = parameters.acq.nproj;
end
totproj = totproj-1; % final image number

seg = parameters.seg;

maxblobsize = seg.maxblobsize; % limits blob size against blowing up
minblobsize = seg.minblobsize; % don't save tiny blobs

thr_seed     = seg.thr_seed;     % seed threshold
thr_grow_rat = seg.thr_grow_rat; % relative threshold for growing seeds


% thr_grow is used for growing the seeds, and linearly depends on thr_seed.
% This linearity is constrained by the upper and lower limits for thr_grow:
if isfield(parameters.seg, 'thr_grow_low')
    thr_grow_low = parameters.seg.thr_grow_low;
else
    thr_grow_low = [];
end

if isfield(parameters.seg, 'thr_grow_high')
    thr_grow_high = parameters.seg.thr_grow_high;
else
    thr_grow_high = [];
end

if isfield(seg, 'extendblobinc')
    extendblobinc = seg.extendblobinc; % when increasing bounding box use these incs.
else
    disp('Using default extendblobinc [30 30 2] - can add to parameters file')
    extendblobinc = [30 30 2];
end

background_subtract = false;
if isfield(seg, 'background_subtract') && seg.background_subtract==true
    background_subtract = true;
end

xdet = parameters.acq.xdet;
ydet = parameters.acq.ydet;

% if the median background is not subtracted from the fulls as they are
% created, then we need a record of what the median background of each full
% is, to be used by sfGetVol (which only reads part images and therefore
% can't calculate the median itself.
fullmedianvals = NaN(totproj+1,1); % if totproj-7199, need 7200

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% connect to database
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
gtDBConnect();

table_difblob        = sprintf('%sdifblob',parameters.acq.name);
table_difblobdetails = sprintf('%sdifblobdetails',parameters.acq.name);

% for median background subtraction
table_fullmedianvals = sprintf('%sfullmedianvals',parameters.acq.name);

% for seed data
table_seeds = sprintf('%sseeds',parameters.acq.name);

% for controlling parallel jobs
table_seeds_tmp = sprintf('%sseeds_tmp',parameters.acq.name);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% wait until gtSeedSegmentation has finished
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
wait = 1;
while wait
    test = mym(sprintf('SELECT count(val) FROM %sfullmedianvals', ...
                       parameters.acq.name));
    
    if test > totproj % test will reach (for eg) 7200, totproj will be 7199
        wait = 0; % go go go!
    else
        pause(30)
        fprintf('Waiting for SeedSegmentation to finish: done %d of %d\n', ...
                 test, totproj+1)
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% get seed data
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[a, b, c, d, e, ~] = mym(sprintf('select * from %s order by graylevel desc',...
                                 table_seeds));
seedlist = [a, b, c, d, e]; % don't need bad at this point

% might want bb info too?

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% main loop over seeds
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

edf_info = [];

for ss = 1:length(seedlist)
    % this seed data
    seedID  = seedlist(ss, 1);
    seedX   = seedlist(ss, 2);
    seedY   = seedlist(ss, 3);
    seedZ   = seedlist(ss, 4);
    seedInt = seedlist(ss, 5); % Intensity

    % tests - has this seed been taken? file table style
    try
        test = mym(sprintf('INSERT INTO %s (seedID) VALUES (%d)',...
                           table_seeds_tmp, seedID));
    catch mexc
        % Let's just silence the error!
        test = false;
    end

    if (test ~= 1)
        out.fprintf('another job has already taken this seed - skipping\n')
        continue
    end


    % tests - is this seed now bad?
    test = mym(sprintf('SELECT bad FROM %s WHERE seedID=%d', table_seeds,...
                       seedID));
    
    if (test == 1)
        out.fprintf('this seed already bad - skipping\n');
        continue
    end

    %%% this one is redundant from the point of view of simply thresholding
    %%% blobs - it would only come into use for blob cracking.  Comment for
    %%% the moment to reduce database traffic.
    %     % tests - seed already part of a blob? (seed can be bad but not part of a blob)
    %     test=mym(sprintf('select difblobID from %s where xIndex=%d and yIndex=%d and zIndex=%d', table_difblobdetails, seedx, seedy, seedz));
    %     if ~isempty(test)
    %         if verbose
    %             disp('this seed already part of a grain - skipping')
    %         end
    %         mym(sprintf('update %s set bad=1 where seedID=%d', table_seeds, seedID));
    %         continue
    %     end

    out.fprintf('seed ok\n');

    % if seed ok, grow seed to blob
    test = [1 1 1 1 1 1];

    % Apply lower and upper limit for thr_grow to grow seed
    thr_grow = thr_grow_rat * seedInt;

    if ~isempty(thr_grow_low) && (thr_grow < thr_grow_low)
        thr_grow = thr_grow_low;
    end
    
    if ~isempty(thr_grow_high) && (thr_grow > thr_grow_high)
        thr_grow = thr_grow_high;
    end

    % for the moment use a dummy bb +-15 around seed pixel
    % use extendblobinc for this
    ebi = ceil(extendblobinc/2);
    
    % find min. and max. x,y,z values for the origin of the temporary seed
    % bounding box 
    tx1 = max(seedX-ebi(1), 1);
    ty1 = max(seedY-ebi(2), 1);
    
    tx2 = min(seedX+ebi(1), parameters.acq.xdet);
    ty2 = min(seedY+ebi(2), parameters.acq.ydet);
    
    tz1 = max(seedZ-ebi(3), 0);
    tz2 = min(seedZ+ebi(3), totproj);
    
    % temporary bounding box around seed
    bb = [tx1 ty1 tz1 tx2-tx1+1 ty2-ty1+1 tz2-tz1+1];

    while any(test)
        % extend the bb
        bb = sfExtendBB(test, bb, extendblobinc, totproj, xdet, ydet);

        % get the volume - trying to save header decoding
        [greyvol, fullmedianvals, edf_info] = sfGetVol(bb, table_fullmedianvals, ...
                        fullmedianvals, background_subtract, seg.bbox, edf_info);

        % label volume
        threshvol = greyvol > thr_grow;
        threshvol = bwlabeln(threshvol);
        
        % pick out label containing seed
        bloblabel = threshvol(seedY-bb(2)+1, seedX-bb(1)+1, seedZ-bb(3)+1);
        threshvol = (threshvol == bloblabel);
        
        % apply the label as a mask
        rawgreyvol = greyvol; % save the un-masked greyvol
        greyvol    = greyvol .* threshvol;


        % test - connected to a higher seed value?
        % ... set all lower connected seeds to bad
        test_int = sfTestInt(greyvol, seedInt);
        if (test_int)
            out.fprintf('blob contains a value higher than the seed - bad\n');
        end

        % test - max size exceeded?
        % ... set all connected lower seeds to bad - any lower seed will give a
        % lower th2 and hence larger blob
        test_size = sfTestBlobSize(threshvol, maxblobsize);
        if (test_size)
            out.fprintf('blob blob exceeds the maximum size - bad\n')
        end

        if (test_int || test_size)
            % blob is bad - set contain, lower value seeds to zero
            sfBadBlob(greyvol, bb, seedInt, thr_seed, table_seeds);
            break
        end

        % test - does blob reach edges of sub volume?
        % ...do the while loop again
        test = sfTestVol(threshvol, bb, totproj, xdet, ydet);

        if ~any(test)
            out.fprintf('blob fits within volume...\n');

            if isfield(parameters.seg, 'no_edges') && parameters.seg.no_edges
                % adjust the mask to avoid edges in the projection when
                % summing.
                greyvol = sfNoEdges(rawgreyvol, threshvol);
            end

            sfWriteBlob(greyvol, bb, minblobsize, table_difblob, ...
                        table_difblobdetails, seedID);

        else
            out.fprintf('increasing subvolume around blob\n');
        end
    end % - while growing blob
end % - of seedlist loop

end % - of main function

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SUB-FUNCTIONS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% write a good blob to the database
function sfWriteBlob(greyvol, th2_bb, minblobsize, table_difblob, ...
                     table_difblobdetails, seedID)

writevol = greyvol;
% check size of blob before writing
% not the here "bb" from regionprops, and "th2_bb" is the subvolume
im = sum(writevol,3);
bb = regionprops(double(im > 0), 'BoundingBox');
bb = bb.BoundingBox;
bb = ceil(bb);

if all(bb(3:4) > minblobsize(1:2)) % size test
    if 0 % for testing
        im = im(bb(2):(bb(2)+bb(4)-1),bb(1):(bb(1)+bb(3)-1));
        figure
        imshow(im,[])
    end

    if 1 % to write too database
        % convert to DB format, write to database
        % format is x, y, z, greyval
        indicies = find(writevol);
        [yy, xx, zz] = ind2sub(size(writevol), indicies);
        
        xx = xx + th2_bb(1) - 1;
        yy = yy + th2_bb(2) - 1;
        zz = zz + th2_bb(3) - 1;
        
        vals = writevol(indicies);

        % from original gtSegmentDiffractionBlobs
        mym(gtDBInsert(table_difblob, {'difblobID'}, 0))
        difblobID = mym('SELECT last_insert_id()');
        fprintf('***DB WRITE *** Blob %d:inserting %d rows, grown from seed %d\n\n',...
                 difblobID, length(xx), seedID)

        gtDBInsertMultiple(table_difblobdetails,...
            'difblobID', difblobID(1, ones(length(xx), 1)), ...
            'xIndex', xx, ...
            'yIndex', yy, ...
            'zIndex', zz, ...
            'graylevel', vals);
    end

    % could write difspot.edfs here, without ever passing
    % through difblobs in the database.  Would need to add
    % functionality from gtDBBlob2SpotTable_WriteDifspots
end

end % of sub-function


function test = sfTestInt(greyvol, seedInt)
% trivial test of blob max intensity
% working with double precision numbers - build in a tiny safety margin for
% rounding
    test = (max(greyvol(:)) > (seedInt+0.00000000001));
end % of sub-function


function test = sfTestBlobSize(vol, maxblobsize)
% find the extent of the thresholded blob, check against max size
    profilex = sum(sum(abs(vol), 3), 1)';
    profiley = sum(sum(abs(vol), 3), 2);
    profilez = squeeze(sum(sum(abs(vol), 1), 2));

    sizex = find(profilex, 1, 'last') - find(profilex, 1, 'first');
    sizey = find(profiley, 1, 'last') - find(profiley, 1, 'first');
    sizez = find(profilez, 1, 'last') - find(profilez, 1, 'first');

    test = any([sizex sizey sizez] > maxblobsize(1:3));

    % Additional test for consistency check! should not be needed.
    [sizex, sizey, sizez] = size(vol);
    if ((~test) && any([sizex sizey sizez] > (2 * maxblobsize(1:3))) )
        test = true;
        % If this happens, it's not a good thing: the growth of the blob is gone
        % beyond the limits and it is now including useless things
        warning('SEGMENTATION:logic_fail', ...
                'Blob BB extended with zeros beyond the double of blob size');
    end
end % of sub-function


function sfBadBlob(greyvol, bb, seedInt, thr_seed, table_seeds)
% find the seed voxels within the blob with intensity<seedInt,
% set these to bad=1 in the table
% voxels at the edge of the subvol could be indentified wrongly as seeds.
% mostly copied from seed_threshold
    for zz = 1:size(greyvol, 3)

        slice = greyvol(:, :, zz);

        if (max(slice(:)) > 0)

            labels = bwlabel(slice > thr_seed);
            for ii = 1:max(labels)

                spot         = find(labels == ii);
                [val, index] = max(slice(spot));
                [yy, xx]     = ind2sub(size(slice), spot(index));

                if (xx == 1) || (yy == 1) || (xx == size(slice, 2)) ...
                        || (yy == size(slice, 1)) || (val > seedInt)
                    % not a real seed / intensity higher than original seed
                    continue
                else
                    % set to bad in the seeds table
                    mym(sprintf('UPDATE %s SET bad=1 WHERE x=%d AND y=%d AND z=%d', ...
                                table_seeds, xx+bb(1)-1, yy+bb(2)-1, zz+bb(3)-1))
                end
            end % loop through labels
        end % skip empty slices
    end % z loop
end % % of sub-function


function test = sfTestVol(vol, bb, totproj, xdet, ydet)
% does a thresholded blob extend to the edges of a volume?  And which
% edges?  Account for the limits of the dataset...

    test = zeros(1, 6); % ~same format as bb

    % low x
    test(1) = ( (bb(1) > 1) && ~isempty(find(vol(:, 1, :), 1, 'first')) );
    % low y
    test(2) = ( (bb(2) > 1) && ~isempty(find(vol(1, :, :), 1, 'first')) );
    % low z (omega)
    test(3) = ( (bb(3) > 0) && ~isempty(find(vol(:, :, 1), 1, 'first')) );
    % high x
    test(4) = ( ((bb(1)+bb(4)-1) < xdet) && ~isempty(find(vol(:, end, :), 1, 'first')) );
    % high y
    test(5) = ( ((bb(2)+bb(5)-1) < ydet) && ~isempty(find(vol(end, :, :), 1, 'first')) );
    % high z (omega)
    test(6) = ( ((bb(3)+bb(6)-1) < totproj) && ~isempty(find(vol(:, :, end), 1, 'first')) );
end % of sub-function


function [vol, fullmedianvals, info] = sfGetVol(bb, table_fullmedianvals, ...
                                 fullmedianvals, background_subtract, segbb, info)
%get the full volume of the given bounding box
% median background info in table

    % define a volume
    vol = zeros([bb(5) bb(4) bb(6)]);

    % read the images
    for z = bb(3):(bb(3)+bb(6)-1)
        % if we don't have the median value we need, calculate it
        if background_subtract
            if isnan(fullmedianvals(z + 1))
                bg = mym(sprintf('SELECT val FROM %s WHERE ndx = %d', ...
                                table_fullmedianvals, z));
                fullmedianvals(z + 1) = bg;
            else
                bg = fullmedianvals(z + 1);
            end
        else
            bg = 0;
        end

        % read into the volume, subtracting the median background
        filename = sprintf('1_preprocessing/full/full%04d.edf', z);
        [img, info] = edf_read(filename, bb([1 2 4 5]), false, info);
        vol(:, :, z-bb(3)+1) = img - bg;
    end

    % Cut negative (unphysical) values (should also solve some unlikely issues)
    vol(vol < 0) = 0;

    % set anything inside the seg bounding box to zeros
    % put seg.bbox relative to the volume
    % work out which pixels to set to zero
    mins = segbb(1:2) - bb(1:2) + 1;
    maxs = mins + segbb(3:4) - 1;

    % fix limits
    mins = max(mins, 1);
    maxs = min(maxs, bb(4:5));

    % if we have to do something about it
    vol(mins(2):maxs(2), mins(1):maxs(1), :) = 0;
end % of sub-function


function bb = sfExtendBB(test, bb, extendblobinc, totproj, xdet, ydet)
%given a grey volume, and test which tells which sides to extend, grow the
%volume, repeat thresholding and labelling
% this shouldn't be hard coded...

if test(1) % extend bb in x -ve
    dif   = min(bb(1)-1, extendblobinc(1));
    bb(1) = bb(1) - dif;
    bb(4) = bb(4) + dif;
end

if test(2)
    dif   = min(bb(2)-1, extendblobinc(2));
    bb(2) = bb(2) - dif;
    bb(5) = bb(5) + dif;
end

if test(3)
    dif   = min(bb(3), extendblobinc(3)); % bb(3) can go to zero for image 0
    bb(3) = bb(3) - dif;
    bb(6) = bb(6) + dif;
end

if test(4)
    dif   = min(xdet-(bb(1)+bb(4)-1), extendblobinc(1));
    bb(4) = bb(4) + dif;
end

if test(5)
    dif   = min(ydet-(bb(2)+bb(5)-1), extendblobinc(2));
    bb(5) = bb(5) + dif;
end

if test(6)
    dif   = min(totproj-(bb(3)+bb(6)-1), extendblobinc(3));
    bb(6) = bb(6) + dif;
end

end % of sub-function


function greyvol = sfNoEdges(rawgreyvol, threshvol)
% rather than threshvol, create a mask where the lateral extent of the blob
% is defined by the maximum of the threshvol in x,y, and omega.  This way
% the outline of the summed spot is true to th2 (intensities may be
% different) but there will be no lines due to summing of subsequent images
% omega range
    profilez = squeeze(sum(sum(threshvol, 1), 2));
    startz   = find(profilez, 1, 'first');
    endz     = find(profilez, 1, 'last');

    % xy mask
    xymask = (sum(threshvol, 3)) > 0;

    % new mask
    for z = startz:endz
        threshvol(:, :, z) = xymask;
    end

    % apply the new mask
    greyvol = rawgreyvol .* threshvol;
end  % of sub-function
