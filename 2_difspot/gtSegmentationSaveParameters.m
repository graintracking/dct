function handles = gtSegmentationSaveParameters(handles)

% save all the updated values into parameters file
set(handles.status_label, 'string', 'Saving parameters', 'backgroundColor', 'y')
drawnow
pause(0.5)

% get the latest parameters, then make sure everything is up to date
parameters = handles.parameters;

% Subtract the background?
parameters.seg.background_subtract = get(handles.subtract_median, 'value');

% Seed thresholding parameters
parameters.seg.thr_seed = str2num(get(handles.seed_thr, 'string'));
parameters.seg.seedminarea = str2num(get(handles.seed_min_area, 'string'));

% Threshold hard limits
parameters.seg.thr_grow_low = str2num(get(handles.thr_grow_low, 'string'));
parameters.seg.thr_grow_high = str2num(get(handles.thr_grow_high, 'string'));

% Grow ratio, Min/Max size, extend increment
parameters.seg.thr_grow_rat = str2num(get(handles.thr_grow_ratio, 'string'));
parameters.seg.minblobsize = str2num(get(handles.min_blob_size, 'string'));
parameters.seg.maxblobsize = str2num(get(handles.max_blob_size, 'string'));
parameters.seg.extendblobinc = str2num(get(handles.extend_blob_inc, 'string'));

% Seg bounding box - should be upto date as this is stored in
% handles.parameters.seg.bbox

% Get the difspot parameters too
difspotmask_list = get(handles.difspot_mask_menu, 'string');
parameters.seg.difspotmask = difspotmask_list{get(handles.difspot_mask_menu, 'value')};
parameters.seg.writeblobs = get(handles.write_blobs_checkbox, 'value');
parameters.seg.writespots = get(handles.write_difspot_checkbox, 'value');
parameters.seg.writeedfs = get(handles.write_edfs_checkbox, 'value');

% save the parameters (assume we are in the working directory?)
system('cp parameters.mat parameters_backup.mat');
set(handles.status_label, 'string', 'Save parameters_backup.mat', 'backgroundColor', 'g')
drawnow
pause(1)
save parameters parameters 

set(handles.status_label, 'string', 'Parameters saved', 'backgroundColor', 'g')
drawnow

handles.parameters = parameters;

end % end of function
