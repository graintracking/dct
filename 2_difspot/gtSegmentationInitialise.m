function handles = gtSegmentationInitialise(handles, parameters)

if (~exist('parameters', 'var') || isempty(parameters))
    parameters = gtLoadParameters();
end
parameters.oar.njobs = 6;
handles.parameters = parameters;

% initialise handles structure for the segmentation GUI
% with acknowledgement of Peter's structure!

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% to display output at command line
set(handles.debug_checkbox, 'value', true)
handles.debug = true;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if (~strcmp(handles.parameters.seg.method, 'doublethr_new'))
    handles.parameters.seg.method = 'doublethr_new';
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% how many images in this scan?
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
totproj = gtAcqTotNumberOfImages(parameters, 1) - 1;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% populate the GUI with current parameters
% (option to reset to default parameters?)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% One quarter of detector, 10 images
set(handles.u_start, 'string', '1')
set(handles.u_end, 'string', num2str(round(parameters.detgeo(1).detsizeu / 2)))
set(handles.v_start, 'string', '1')
set(handles.v_end, 'string', num2str(round(parameters.detgeo(1).detsizev / 2)))
set(handles.image_start, 'string', num2str(round(totproj / 2)))
set(handles.image_end, 'string', num2str(round(totproj / 2) + 9))
% start with the first image
handles.current_image = round(totproj / 2);

% Subtract the background?
set(handles.subtract_median, 'value', parameters.seg.background_subtract)

% Seed thresholding parameters
set(handles.seed_thr, 'string', num2str(parameters.seg.thr_seed))
set(handles.seed_min_area, 'string', num2str(parameters.seg.seedminarea))

% Threshold hard limits
set(handles.thr_grow_low, 'string', num2str(parameters.seg.thr_grow_low))
set(handles.thr_grow_high, 'string', num2str(parameters.seg.thr_grow_high))

% Grow ratio, Min/Max size, extend increment
set(handles.thr_grow_ratio, 'string', num2str(parameters.seg.thr_grow_rat))
set(handles.min_blob_size, 'string', num2str(parameters.seg.minblobsize))
set(handles.max_blob_size, 'string', num2str(parameters.seg.maxblobsize))
set(handles.extend_blob_inc, 'string', num2str(parameters.seg.extendblobinc))

% Seg bounding box - start with the prep.bbox
handles.parameters.seg.bbox = parameters.prep.bbox;
handles.bb_handle = []; % a handle to the line drawn on the image

% OAR defaults
set(handles.OAR_walltime, 'string', '21600')
set(handles.OAR_njobs, 'string', parameters.oar.njobs)

% set status label to white
set(handles.status_label, 'BackgroundColor', 'w')
set(handles.status_label, 'string', 'Loading data...')


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Record if figures need to be updated
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% "global value" - something has changed
handles.values_changed = true;
% more specific details
handles.bb_changed = true; % re-threshold stacks
handles.thresholds_changed = true; % re-threshold stacks
handles.roi_changed = true; % reload stacks, then rethreshold
end
