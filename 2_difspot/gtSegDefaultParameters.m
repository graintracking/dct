function par_seg = gtSegDefaultParameters()

par_seg.bbox                = [];
par_seg.method              = 'doublethr'; % 'singlethr' | 'doublethr' | 'doublethr_new'
par_seg.thr_single          = 3;         % threshold for ''single threshold'' segmentation
par_seg.thr_seed            = 50;        % threshold for finding seeds (for double threshold)
par_seg.thr_grow_rat        = 0.1;       % relative threshold for growing seeds (for double threshold)
par_seg.thr_grow_low        = 7;         % lower limit of threshold for growing seeds (for double threshold)
par_seg.thr_grow_high       = 20;        % upper limit of threshold for growing seeds (for double threshold)
par_seg.seedminarea         = 10;
par_seg.minsize             = 50;
par_seg.omintlimmin         = 0.0005;    % indicates first and last image in the stack with this minimum relative intensity
par_seg.omintlimtail        = 0.0005;    % size of tails cut off the summed intensity curve through the image stack
par_seg.minblobsize         = [10 10 0];
par_seg.maxblobsize         = [300 300 40];
par_seg.extendblobinc       = [30 30 2];
par_seg.background_subtract = true;
par_seg.overlaps_removed    = false;
par_seg.difspotmask         = 'blob3Dsoft'; % mask used to create difspot.edf from blob
                                            % ('none'/'blob2D'/'blob2Dsoft'/'blob3D'/'blob3Dsoft')
par_seg.debug                           = false; % display messages
par_seg.writeblobs                      = true;  % write difblobs to the table
par_seg.writespots                      = true;  % write difspot metadata to the table
par_seg.writeedfs                       = true;  % save difspots as edf files
par_seg.writehdf5                       = true;  % write difblobs to the table
par_seg.wrapping                        = true;  % for 360 degree data, wrap from 
                                                 % the last image back to the first image
par_seg.segmentation_stack_size         = 1000;  % how many images in memory? (1000 images approx 32Gb)
par_seg.background_subtract_accelerate  = true;  % calculate median on a subset of
                                                 % pixels (faster)

end
