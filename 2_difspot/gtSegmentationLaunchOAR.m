function handles = gtSegmentationLaunchOAR(handles)
% launch OAR jobs

set(handles.status_label, 'string', 'Launching OAR', 'backgroundColor', 'y')
drawnow()
totproj = gtAcqTotNumberOfImages(handles.parameters, 1) - 1;



handles = gtSegmentationSaveParameters(handles);

totproj = gtAcqTotNumberOfImages(handles.parameters, 1) - 1;

check = questdlg('Launch Segmentation with OAR?','Question','Yes','No','Yes');
if strcmpi(check,'Yes')

  % should be one big job
    handles.parameters.oar.njobs    = str2num(get(handles.OAR_njobs, 'string'));
    %handles.parameters.oar.njobs = 1;
    OARtime = str2num(get(handles.OAR_walltime, 'string'));
    OARtmp  = ceil(OARtime/60); % minutes
    OARm    = mod(OARtmp, 60);  % minutes
    OARh    = (OARtmp-OARm)/60; % hours
    handles.parameters.oar.walltime = sprintf('%02d:%02d:00', OARh, OARm);
    
    handles.parameters.oar.mem_core_mb = 8000; %Mb
    handles.parameters.oar.node = 1;
    handles.parameters.oar.core = 6;

    list = build_list_v2();

    list.oar(end+1,:) = {'mem_core_mb','Memory for each core in Mb','double', 2};
    list.oar(end+1,:) = {'node','Number of nodes','double', 2};
    list.oar(end+1,:) = {'core','Number of cores per node','double', 2};

    handles.parameters.oar = gtModifyStructure(handles.parameters.oar, list.oar, 2, 'OAR parameters:');
    handles.parameters.seg.segmentation_stack_size = ceil(totproj ./ handles.parameters.oar.njobs);
    parameters = handles.parameters;
    save('parameters.mat','parameters')
    
    disp('Using OAR parameters:')
    disp(handles.parameters.oar)

    pause(0.5);
    gtOarLaunch('gtSegmentationDoubleThreshold', 0, totproj, ...
        handles.parameters.oar.njobs, handles.parameters.acq(1).dir, 1, ...
        'mem_core_mb', handles.parameters.oar.mem_core_mb, ...
        'node', handles.parameters.oar.node, ...
        'core', handles.parameters.oar.core, ...
        'walltime', handles.parameters.oar.walltime);

    set(handles.status_label, 'string', 'OAR launched', 'backgroundColor', 'g')
    drawnow()

else

    disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
    disp(' ')
    disp('You should try running this in a single, large, interactive OAR job')
    disp(' ')
    disp('I suggest something like this:')
    disp(' ')
    disp('From rnice6, get an interactive OAR job:')
    disp('oarsub -I -p "mem_core_mb=8000" -l nodes=1/core=6,walltime=2')
    disp(' ')
    disp('then start matlab as normal via python')
    disp(' ')
    disp('You should have enough RAM to use >1000 images in memory')
    disp('Set parameters.seg.segmentation_stack_size = 1000')
    disp('Otherwise, default is a conservative 250')
    disp(' ')
    disp('In the dataset directory, run:')
    fprintf('gtSegmentationDoubleThreshold(0, %d, pwd)\n', totproj)
    disp(' ')
    disp('...and see what happens.  Best of luck!')
    disp(' ')
    disp('If things work, we can automate properly...')
    disp('Andy, 30/08/2013')
    disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')

    set(handles.status_label, 'string', 'OAR (not!) launched', 'backgroundColor', 'g')
    drawnow()
end

end % end of function
