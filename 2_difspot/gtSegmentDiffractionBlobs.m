function gtSegmentDiffractionBlobs(first, last, workingdirectory)
% GTSEGMENTDIFFRACTIONBLOBS Single threshold segmentation for whole datasets. 
% Usually compiled and used via OAR on multiple cores on the cluster.
%
% It loads the full images; thresholds them with a single threshold; 
% finds and labels connected regions in three dimensions; writes each blob
% to the database as it finds them to ease database traffic.
%
% Uses gtReadAndThreshold that can subtract each full image's remaining
% median background to offset it to zero before segmentation (it is
% calculated and applied outside the seg.bbox area).
%
% INPUT
%   first            - first full image ID in stack to be processed 
%   last             - last full image ID in stack to be processed 
%   workingdirectory - specify to run in via OAR; if not specified, 
%                      the function will change to and run in the current
%                      directory
%
%   In parameters file: 
%   parameters.
%     seg.thr_single          - the threshold to be applied
%     seg.background_subtract - if true, each full image's background will
%                               be subtracted to offset its median to zero
%
% 
%
% Version 002 08-05-2012 by P.Reischig
%   Updated to new parameters (thr_single) and cleaned. Median filter 
%   is cancelled to preserve resolution since the filter is applied in 
%   preprocessing.
%
%
%
%

if (isdeployed)
    global GT_DB %#ok<TLEV,NUSED>
    global GT_MATLAB_HOME %#ok<TLEV,NUSED>
    load('workspaceGlobal.mat');
    first = str2double(first);
    last  = str2double(last);
end

% special case for running interactively in current directory
if (~exist('workingdirectory','var'))
    workingdirectory = pwd;
end

fprintf('Changing directory to %s\n', workingdirectory)
cd(workingdirectory)

if (~exist('parameters', 'var') || isempty(parameters))
    parameters = gtLoadParameters();
end

% if using this function, need to remove overlapping difblobs afterwards
% move this to the setup function to avoid causing problems
%parameters.seg.overlaps_removed = false;
%save parameters parameters
nimages = gtAcqTotNumberOfImages(parameters);

% note images numbered 0 - 7199 for example
last = min(last + 1, nimages - 1);

fprintf(' ~~~  This Job: Image %d to %d, for %s  ~~~   \n', first, last, workingdirectory)
disp('THIS DOES NOT WRAP OVER THE END OF THE DATASET PROPERLY!')

% read the size of one image to make an estimate for the number we can read
% at once:
test    = gtReadAndThreshold(first, parameters, NaN);
details = whos('test');

maxMemoryUsage = 100;  % in MB
step = floor(maxMemoryUsage * 1024 * 1024 / details.bytes);

% open database
gtDBConnect();

allSpotsIndx = 0;

for currentfilendx = first:step:last
    
    disp(['current file ndx: ' num2str(currentfilendx)])
    
    step = min(step, last - currentfilendx + 1);

    fprintf('Reading %d images (~%dMB) in this chunk\n', step, ...
            round(step*details.bytes/1024/1024));
        
    shortVol = zeros([parameters.acq.ydet, parameters.acq.xdet, step]);
    
    for n = 1:step
        
        % Read and threshold image with parameters.seg.thr_single
        shortVol(:,:,n) = gtReadAndThreshold(currentfilendx + n - 1, parameters);
        
        % add a median filter to de-noise the thresholded images
        % !!! This a second median filter on the full images that
        % have been median filtered in the preprocessing !!!
        % It would decrease resolution, so is cancelled. If one still needs 
        % to smooth the images before segmentation, the final segmented 
        % grayscales should be reloaded from the full images.
        % shortvol(:,:,n) = gtImgMedianFilter2(im);
        
    end

    labels    = bwlabeln(shortVol);
    numLabels = max(labels(:));      % the number of blobs
    
    for ndx = 1:numLabels  % iterate over all the blobs
        
        ind = find(labels == ndx);
        
        if (length(ind) < 10)  % discard small blobs
            continue
        end
        
        allSpotsIndx = allSpotsIndx + 1;
        
        grayVals = shortVol(ind);
        
        [ii, jj, kk] = ind2sub(size(shortVol), ind);
        kk           = kk + currentfilendx - 1;  % offset shortvol
        ind2D        = sub2ind(size(shortVol(:, :, 1)), ii, jj);

        % now record the blob
        allSpots{allSpotsIndx} = [];
        
        for z = min(kk):max(kk) % iterate over all slices for this blob
            howManyInZ = length(find(kk == z));
            allSpots{allSpotsIndx} = [allSpots{allSpotsIndx}; ...
                [repmat(z, [howManyInZ 1]) ind2D(kk == z) grayVals(kk == z)]];
        end
        
        if (currentfilendx > first)
            if any(kk == currentfilendx)  % blob is at top of sub volume
                % look through all spots to find ones touching bottom of previous
                % subvolume
                q = 1;
                
                while (true)

                    if (~isempty(allSpots{q}) ...
                            && any(allSpots{q}(:, 1) == (currentfilendx - 1)) )
                        if intersect(allSpots{q}(:,2),allSpots{allSpotsIndx}(:,2))
                            allSpots{q} = union(allSpots{q},allSpots{allSpotsIndx},'rows');
                            allSpots    = {allSpots{1:allSpotsIndx-1}};  % drop that last one
                            allSpotsIndx = allSpotsIndx - 1;  % decrement - this spotndx will be redone
                        end
                    end

                    q = q + 1;

                    % list has been shortened - if we're at the end, get out
                    if (q > (allSpotsIndx-1))
                        break
                    end
                    
                end
            end
        end
    end
    
    if (currentfilendx+step < last)
        % write all completed blobs to database and erase from cell array
        for qwe = 1:length(allSpots)
            if (~isempty(allSpots{qwe}) && max(allSpots{qwe}(:, 1)) < (currentfilendx - 1))
                % this blob is finished - write it out
                % keyboard
                sfDBWriteBlob(allSpots{qwe}, parameters)
                
                disp(['clearing out blob ' num2str(qwe)])
                allSpots{qwe} = [];
            end
        end
    else
        disp('Writing at the end of the loop')
        % write all of them
        for qwe = 1:length(allSpots)
            if ~isempty(allSpots{qwe})
                sfDBWriteBlob(allSpots{qwe}, parameters)
                
                allSpots{qwe} = [];
            end
        end
    end
end

table_difblobinfo = [parameters.acq.name 'difblobinfo'];
% finished processing - update the chunk table
mym(gtDBInsert(table_difblobinfo, {'StartIndex', 'EndIndex'}, [first, last]));

end %end function

function sfDBWriteBlob(blob, parameters)

    zz = blob(:, 1);
    if (length(zz) > parameters.seg.minsize)
        %should only proceed if the blob is bigger than the minimum 
        % size defined in the parameters file

        full_img_size = [parameters.acq.ydet parameters.acq.xdet];
        [yy, xx] = ind2sub(full_img_size, blob(:, 2));
        vals     = blob(:, 3);

        table_difblob = [parameters.acq.name 'difblob'];
        % get a new difblobID
        mym(gtDBInsert(table_difblob, {'difblobID'}, 0))
        difblobID = mym('select last_insert_id()');

        fprintf('***DB WRITE *** Blob %d:inserting %d rows\n\r\n', difblobID, length(xx))

        table_difblobdetails = [parameters.acq.name 'difblobdetails'];
        gtDBInsertMultiple(table_difblobdetails,...
            'difblobID', repmat(difblobID, 1, length(xx)), ...
            'xIndex', xx, ...
            'yIndex', yy, ...
            'zIndex', zz, ...
            'graylevel', vals);
    end
end

