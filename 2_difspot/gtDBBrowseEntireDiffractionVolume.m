function [vol,labels]=gtDBBrowseEntireDiffractionVolume(table,varargin)

load parameters

table=[table 'difblobdetails'];

params.xrange=[1 parameters.acq.xdet];
params.yrange=[1 parameters.acq.ydet];
maxz=mym(sprintf('select max(zIndex) from %s',table));
params.zrange=[1 maxz];

params=parse_pv_pairs(params,varargin)
xrange=params.xrange;
yrange=params.yrange;
zrange=params.zrange;

[id,x,y,z,vals]=mym(sprintf('select difblobID,xIndex,yIndex,zIndex,graylevel from %s where xIndex between %d and %d and yIndex between %d and %d and zIndex between %d and %d',table,xrange(1),xrange(2),yrange(1),yrange(2),zrange(1),zrange(2)));
fprintf('Inserting %d voxels\n',length(id));

%%% ak - I think this is wrong, because it will crop the returned volume if
%%% there are empty rows/columes
%z=z-min(z)+1;
%x=x-min(x)+1;
%y=y-min(y)+1;
%vol=zeros(max(y),max(x),max(z));
%labels=vol;
%for n=1:length(id)
%  vol(y(n),x(n),z(n))=  vals(n);
%  labels(y(n),x(n),z(n))=id(n);
%end

z=z-zrange(1)+1;
x=x-xrange(1)+1;
y=y-yrange(1)+1;
vol=zeros(yrange(2)-yrange(1)+1, xrange(2)-xrange(1)+1, zrange(2)-zrange(1)+1);
labels=vol;
for n=1:length(id)
  vol(y(n),x(n),z(n))=  vals(n);
  labels(y(n),x(n),z(n))=id(n);
end
