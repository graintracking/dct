function gtSegmentationBlob2SpotTable(blob, stack)
% GTSEGMENTATIONBLOB2SPOTTABLE  Calculates diffraction spot properties
%                               from the blobs and writes spot data in the database spotpair table
%
% gtSegmentationBlob2SpotTable(blob, stack)
% -----------------------------------------
%
% Loads 3D diffraction blob data from the databse, calculates spot
% centroid [u,v,omega], intensity, area and final bounding box.
%
% Creates 2D spots by summing the full images in the blob
% bounding box. Can restrict the omega range that is included in the spots
% and can apply a mask under which the intensities are summed according to
% the segmented blob in 2D or 3D.
%
% It writes the spot data in the database. Can create edf files for each
% spot in the difspot folder.
%
% INPUT
%  first, last      - the difspotID range to process
%
%  workingdirectory - where the dataset is
%
%  flag_writeedf:   - if true, difspot images are written as edf files in
%                     folder 2_difspot (default is true)
%
%  thr_area         - if specified, only spots with an area greater than
%                     this value will be processed
%
%  In parameters file
%  parameters.seg.
%   'omintlimtail' and 'omintlimmin' are used to limit the extent of
%   a segmented blob along the omega image stack (ExtStartImage and
%   ExtEndImage in spotpairs table). The final omega range will have the
%   wider limits (less strict values) of the two on both ends. Values
%   should be chosen having accounted for signal-to-noise ratio in the
%   images and the remaining background intensity.
%
%   omintlimtail  - the size of tails cut off the cumulated intensity
%                   curve through the image stack; use NaN to have no effect
%                   e.g. at 0.01, at least 98% of the total blob intensity
%                   is considered;
%
%   omintlimmin   - minimum relative intensity of the first and last images
%                   in final omega stack (relative to the integrated
%                   intensity); use NaN to have no effect
%                   e.g. at 0.01, the cut off in omega on the two ends
%                   will be at 1% of the peak intensity; use
%
%   difspotmask   - how to mask summed full images to write 2D difspot image
%                   'none'       - no mask applied
%                   'blob2D'     - 2D mask according to segmented blob in
%                                  (x,y)/(u,v)
%                   'blob2Dsoft' - 2D mask according to segmented blob in
%                                  (x,y)/(u,v) and then soft thresholded
%                   'blob3D'     - 3D mask according to segmented blob
%                   'blob3Dsoft' - 3D soft thresholded mask according to
%                                  segmented blob
%
% OUTPUT
%   Difspot metadata in difspot table is filled in, difspots are saved as edf
%   files.
%
%
% Version 002 11-05-2012 by P.Reischig
%   update to new parameters names; small adjustment to omega range calculation;
%   cleaning
%
% Version 003 15/06/2012 by A.King
%   As this version can either write edfs or not as desired, make this the
%   standard version in all cases. Therefore I have changed the name to
%   gtDBBlob2SpotTable and have removed the old function of that name.
%
% Modified by Nicola Vigano', 2012, nicola.vigano@esrf.fr
%   Added soft thresholding options, and added EDF header recycling.
%


if isfield(stack, 'thr_area')
    thr_area = stack.thr_area;
else
    thr_area = [];
end
if isfield(stack, 'writeedfs')
    writeedfs = stack.writeedfs;
else
    writeedfs = true ; % make this default behaviour
end

% we have the parameters in "stack"
seg = stack.seg;
difspot_table = stack.table_difspot;

% spot ID is passed in
spot.difspotID = blob.difblobID;

% get the blob
blobvol = blob.greyvol;

% get the bb, in the frame of the dataset
bb = blob.bb;
bb(3) = bb(3) + stack.first - 1;

% Blob summed through stack
if (strcmpi(seg.difspotmask, 'blob3Dsoft'))
    thr = min(blobvol(blobvol > 0));
    blob2D = sum(gtSoftThreshold2(blobvol, thr), 3);
else
    blob2D = sum(blobvol, 3);
end

% Find inetgration range through omega stack
spot = sfFindIntegrationRange(spot, blobvol, bb, seg, stack.totproj, stack.wrapping);

% Centroid and bb
spot = sfFindCentroid(spot, blob2D, bb);

% Write in difspot table
sfDoInsert(spot, difspot_table)

% Write edf file on disk
if (writeedfs)
    
    if (ismember(seg.difspotmask, {'none', 'blob2D', 'blob2Dsoft'}))
        % create summed image
        im = sum(blob.rawgreyvol, 3);
    end
    
    % Check minimum area condition
    if (~isempty(thr_area))
        mask2D = blob2D > 0 ;
        if (sum(mask2D(:)) < thr_area)
            % we don't write this spot,so  quit
            return
        end
    else
        mask2D = [];
    end
    
    % Use image mask for the difspot image
    switch (seg.difspotmask)
        case 'none'
            % The final 2D spot is simply the full images summed through
            % the omega stack.
            
        case 'blob2D'
            % A 2D mask is created from the segmented blob in database
            % which then is applied on the summed full images.
            if isempty(mask2D)
                mask2D = blob2D > 0 ;
            end
            im = im .* mask2D;
            
        case 'blob2Dsoft'
            % A 2D mask is created from the segmented blob in database
            % which then is applied on the summed full images, and soft
            % thresholded.
            if isempty(mask2D)
                mask2D = blob2D > 0 ;
            end
            im = im .* mask2D;
            
            % Get approximately the threshold used to create the mask.
            lower_thresh = min(im(im > 0));
            
            % Apply soft threshold
            im = gtSoftThreshold2(im, lower_thresh);
            
        case {'blob3D', 'blob3Dsoft'}
            % The segmented blob volume is simply summed through the omega
            % stack to get a 2D spot.
            % (in 'blob3Dsoft' the omega steps are also soft thresholded)
            im = blob2D;
            
        otherwise
            error('Option not recognised in parameters.seg.difspotmask ')
    end

    % save the file
    sfWriteDifspotToEDF(stack.difspotdir, im, spot.difspotID)
    
end % end if writeedfs

end % end of function


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Sub-functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% sfWriteDifspotToEDF
function sfWriteDifspotToEDF(savedir, image, index)
    sub_dir = fullfile(savedir, sprintf('%05d', index - mod(index, 1e4)));
    if (~exist(sub_dir, 'dir'))
        [status, message, messageid] = mkdir(sub_dir);
        if (~status)
            gtError(messageid, message)
        elseif (~isempty(messageid))
            warning(messageid, message)
        end
    end
    fnameout = fullfile(sub_dir, sprintf('difspot%05d.edf', index) );
    edf_write(image, fnameout, 'float32', true);
end

% sfFindIntegrationRange
function spot = sfFindIntegrationRange(spot, vol, bb, seg, totproj, wrapping)

% vol should be the blob volume. It's preferred to the summed full
% images.

%note - bb(3) can be zero, but the first value in integral has index 1.
%Thus need to -1 to get correct CentroidImage
spot.StartImage = bb(3);
spot.EndImage   = bb(3) + bb(6) - 1;

% intensity integrated along stack dimension
Intensity = squeeze(sum(sum(vol,1),2))';

% Find the first and last image with reasonable intensity
% (and later perhaps area).
% Caution - an empty matrix may be returned for spots which are very
% extended in omega.
totIntensity   = sum(Intensity);
firstim_imint  = find( Intensity >= seg.omintlimmin*totIntensity, 1,'first');
lastim_imint   = find( Intensity >= seg.omintlimmin*totIntensity, 1,'last');

if isempty(firstim_imint)
    firstim_imint = NaN;
end
if isempty(lastim_imint)
    lastim_imint  = NaN;
end

% Find the tails of blob beyond limits (consider at least
% (1-2*seg.extlim_tailint) part of the cumulated intensity curve).
% Cumulated intensities starting from first layer in stack:
Integral        = cumsum(Intensity);
firstim_tailint = find( Integral >=    seg.omintlimtail *Integral(end), 1,'first');
lastim_tailint  = find( Integral >= (1-seg.omintlimtail)*Integral(end), 1,'first');

if isempty(firstim_tailint)
    firstim_tailint = NaN;
end
if isempty(lastim_tailint)
    lastim_tailint  = NaN;
end

% Choose the condition that's met first
if (isnan(firstim_imint) && isnan(firstim_tailint))
    spot.ExtStartImage = bb(3);
else
    spot.ExtStartImage = bb(3) + min(firstim_imint, firstim_tailint) -1;
end

if (isnan(lastim_imint) && isnan(lastim_tailint))
    spot.ExtEndImage = bb(3) + bb(6) - 1;
else
    spot.ExtEndImage = bb(3) + max(lastim_imint,  lastim_tailint)  -1;
end

% just to make sure
if spot.ExtStartImage > spot.ExtEndImage
    spot.ExtStartImage = spot.ExtEndImage;
end

% Max Image - round(mean(...)) needed if there happen to be more
% than one image of the maximum intensity, not to make database command
% fail
spot.MaxImage = round( mean(bb(3) + find(Intensity==max(Intensity)) -1) );
spot.Integral = Integral(end);

image_cen = (Intensity*(1:length(Intensity))')/spot.Integral;
spot.CentroidImage = bb(3) + image_cen - 1;

% we now allow blobs to wrap over the end of the dataset.  Therefore, all
% of these quantities should be put on the interval 0 - totproj
if (wrapping)
    spot.CentroidImage =mod(spot.CentroidImage, totproj);
    spot.MaxImage =mod(spot.MaxImage, totproj);
    spot.StartImage =mod(spot.StartImage, totproj);
    spot.EndImage =mod(spot.EndImage, totproj);
    spot.ExtStartImage =mod(spot.ExtStartImage, totproj);
    spot.ExtEndImage =mod(spot.ExtEndImage, totproj);
end

end


% sfFindCentroid (greyscale centroid)
function spot = sfFindCentroid(spot, im, bb)

spot.BoundingBoxXorigin = bb(1);
spot.BoundingBoxYorigin = bb(2);
spot.BoundingBoxXsize   = bb(4);
spot.BoundingBoxYsize   = bb(5);

%im=sum(vol,3);

xprofile = sum(im,1);
yprofile = sum(im,2);

xcen = (xprofile*(1:size(xprofile,2))')/sum(xprofile);
ycen = (yprofile'*(1:size(yprofile,1))')/sum(yprofile);

spot.CentroidX = spot.BoundingBoxXorigin + xcen - 1;
spot.CentroidY = spot.BoundingBoxYorigin + ycen - 1;

spot.Area = length(find(im~=0));
end


% sfDoInsert
function sfDoInsert(spot, difspot_table)
%write info to difspot table
%collect the data in values - not autoincrement difspotID in this case
values = [spot.difspotID, spot.Area, ...
    spot.CentroidX, spot.CentroidY, ...
    spot.BoundingBoxXorigin, spot.BoundingBoxYorigin, ...
    spot.BoundingBoxXsize, spot.BoundingBoxYsize, ...
    spot.Integral, ...
    spot.StartImage, spot.EndImage, spot.MaxImage, ...
    spot.ExtStartImage, spot.ExtEndImage, ...
    spot.CentroidImage, 0]';
colnames = { 'difspotID', 'Area', 'CentroidX', 'CentroidY', ...
    'BoundingBoxXorigin', 'BoundingBoxYorigin', ...
    'BoundingBoxXsize', 'BoundingBoxYsize', ...
    'Integral', 'StartImage', 'EndImage', 'MaxImage', ...
    'ExtStartImage', 'ExtEndImage', 'CentroidImage', 'Bad'};

try
    cmd = gtDBInsert(difspot_table, colnames, values);
    mym(cmd);
catch mexc
    errorMsg = [ '%%%%%%%%%%%   Apparent problem with difblobID ', ...
        num2str(spot.difspotID), '  %%%%%%%%%%%%%%'];
    gtPrintException(mexc, errorMsg);
end

end
