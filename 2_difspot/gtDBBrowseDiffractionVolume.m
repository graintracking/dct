function [vol, bb] = gtDBBrowseDiffractionVolume(dataname, ndx)
% GTDBBROWSEDIFFRACTIONVOLUME  Loads the 3D volume of difblob #ndx from the database for dataset 'dataname'
%     [vol, bb] = gtDBBrowseDiffractionVolume(dataname, ndx)
%     ------------------------------------------------------
%

    table = [dataname 'difblobdetails'];

    query = sprintf(['SELECT xIndex,yIndex,zIndex,graylevel'...
                     '   FROM %s WHERE difblobID = %d' ], table, ndx);
    [xx, yy, zz, vals] = mym(query);

    %fprintf('Volume extent:\n\t%4d %4d X\n\t%4d %4d Y\n\t%4d %4d Z\n',...
    %         min(x),max(x),min(y),max(y),min(z),max(z));

    if isempty(vals)
        vol = [];
        bb  = [];
    else
        bb = [ min(xx) min(yy) min(zz) ...
               (max(xx) - min(xx) + 1) (max(yy) - min(yy) + 1) (max(zz) - min(zz) + 1)];

        xx = xx - min(xx) + 1;
        yy = yy - min(yy) + 1;
        zz = zz - min(zz) + 1;

        vol = zeros(max(yy), max(xx), max(zz));

        linind      = sub2ind(bb([5 4 6]), yy, xx, zz);
        vol(linind) = vals;
    end
end % of function
