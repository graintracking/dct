function gtSegmentationDoubleThreshold(first_img, last_img, workingdir)
% GTSEGMENTATIONDOUBLETHRESHOLD  Faster, more efficient segmentation of
%                                 blobs and spots
%
% gtSegmentationDoubleThreshold(first_img, last_img, workingdir)
% ---------------------------------------------------------------
% Replaces previous double threshold segmentation, keeping essentially the
% same parameters and functionality.  Designed to work on a single large
% machine, or a few machines.  Reduces file I/O time by reading a stack of
% images into memory, and then finding all blobs within the stack.  Note
% that it reads a volume padded by the maxblobsize 3rd dimension.
% Writing blobs to the database is optional, for further speedup and space
% saving.
% Coordinate systems are a bit confusing, because we segment blobs from an
% ROI of a stack, which is in turn an ROI within the dataset...
%
% Control is via options in the parameters file .seg field.  Most of these
% can be set using gtSegmentationGUI.  Default values will be used if they
% are not specified.
%
% New parameters:
%
% seg.debug                      - (true/false) display messages
% seg.writeblobs                 - (true/false) write difblobs to the table
% seg.writespots                 - (true/false) write difspot metadata to the table
% seg.writeedfs                  - (true/false) save difspots as edf files
% seg.wrapping                   - (true/false) for 360 degree data, wrap from
%                                         the last image back to the first image
% seg.segmentation_stack_size    - how many images in memory? (1000 images approx 32Gb)
% seg.background_subtract_accelerate  - (true/false) calculate median on a subset of
%                                           pixels (faster)
%
% Standard parameters:
%
% seg.seedminarea                - the minimum seed size, in pixels, that will be
%                                           considered.  NOTE THIS IS NOW THE
%                                           NUMBER OF CONNECTED PIXELS IN 3D!
% seg.bbox          - the segmentation bounding box; this area is omitted
%                       in the segmentation
% seg.minblobsize   - blobs under this size will not be considered
% seg.maxblobsize   - blobs outside these dimensions will not be
%                       written to the database; important to stop things
%                       from blowing up - like the glow around the direct
%                       beam for eg.
% seg.thr_seed      - the seed threshold to find potential blobs
% seg.thr_grow_rat  - adaptive threshold parameters for growing seeds into
%                       blobs
% seg.thr_grow_low  - lower limit on adaptive threshold
% seg.thr_grow_high - upper limit on adaptive threshold
% seg.extendblobinc - size of incremental blob bbox extension
%
% As difspots are written from this function, the masking and blob omega
% limit functions are also required.  Currently, there is no GUI for
% handling these.  For info, look in the header of
% gtSegmentationBlob2SpotTable.
%
% Version 001 30/09/2013 by A.King
%   "beta" version for testing.  On Mg2b_dct_ dataset, segmented ~50,000
%   blobs from 7200 images in 1:12, using 1000 images in memory, in one
%   OAR job, 1 node/8 cores


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%    load and unpack parameters    %%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if (isdeployed)
    global GT_DB
    global GT_MATLAB_HOME
    load('workspaceGlobal.mat');
    first_img = str2double(first_img);
    last_img  = str2double(last_img);
end

cd(workingdir)
parameters = gtLoadParameters();

% display messages
if (isfield(parameters.seg, 'debug'))
    stack.debug = parameters.seg.debug;
else
    stack.debug = false;
end
if (stack.debug)
    disp('displaying debugging output')
else
    disp('no debugging output...  edit gtSegmentationDoubleThreshold.m to change!')
end

t0 = clock;
fprintf('gtSegmentationDoubleThreshold for image %d to %d, starting at %02dh%02d...\n', first_img, last_img, t0(4), t0(5))
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%           connect to database       %%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

gtDBConnect();
stack.table_difblob        = sprintf('%sdifblob',parameters.acq(1).name);
stack.table_difblobdetails = sprintf('%sdifblobdetails',parameters.acq(1).name);
stack.table_fullmedianvals = sprintf('%sfullmedianvals',parameters.acq(1).name);
stack.table_difspot        = sprintf('%sdifspot',parameters.acq(1).name);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%             output options          %%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if isfield(parameters.seg, 'writeblobs')
    stack.writeblobs = parameters.seg.writeblobs;
else
    stack.writeblobs = false;
end
if isfield(parameters.seg, 'writespots')
    stack.writespots = parameters.seg.writespots;
else
    stack.writespots = true;
end
if isfield(parameters.seg, 'writeedfs')
    stack.writeedfs = parameters.seg.writeedfs;
else
    stack.writeedfs = true;
end 
if isfield(parameters.seg, 'writehdf5')
    stack.writehdf5 = parameters.seg.writehdf5;
else
    stack.writehdf5 = true;
end
% do we want to wrap around? Not for live, but for post mortem we could
if isfield(parameters.seg, 'wrapping')
    stack.wrapping = parameters.seg.wrapping;
else
    stack.wrapping = false;
end
if isfield(parameters.seg, 'segmentation_stack_size')
    stack.sizeZ = parameters.seg.segmentation_stack_size;
else
    stack.sizeZ = 1000;
end

% you must write either blobs or edfs
if (stack.writeblobs) || (stack.writehdf5) || (stack.writespots && stack.writeedfs)
    fprintf('Output configuration:\n   (writeblobs=%d, writehdf5=%d, writespots=%d, writeedfs=%d)\n',...
        stack.writeblobs, stack.writehdf5, stack.writespots, stack.writeedfs)
else
    fprintf(['Output configuration:\n   (writeblobs=%d, writespots=%d, writeedfs=%d) '...
        '\n    is not valid!\nYou must save images in some way\n  Quitting... \n'], ...
        stack.writeblobs,stack.writespots,stack.writeedfs)
    return
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%      prepare "stack"       %%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% parameters
stack.parameters = parameters;
stack.seg = parameters.seg;
stack.fulldir = fullfile(parameters.acq(1).dir, '1_preprocessing', 'full');
stack.difspotdir = fullfile(parameters.acq(1).dir, '2_difspot');
stack.difblobdir = fullfile(parameters.acq(1).dir, '2_difblob');

% stack range
stack.totproj = gtAcqTotNumberOfImages(parameters);

stack.pad = parameters.seg.maxblobsize(3);
stack.sizeZ = last_img - first_img + 1 + 2 * stack.pad

% don't take more than allowed
parameters.seg.segmentation_stack_size = ...
    min(stack.sizeZ, parameters.seg.segmentation_stack_size);

% read in the volume to work on
% check that the size makes sense
fprintf('Using %d images in memory (from parameters.seg)\n\n', stack.sizeZ)

stack.blockZ = stack.sizeZ - (2*stack.pad);
if (stack.blockZ < 1)
    disp('maxblobsize and stack size not compatible - quitting')
    return
end
stack.sizeY = parameters.acq(1).ydet;
stack.sizeX = parameters.acq(1).xdet;
stack.vol = zeros(stack.sizeY, stack.sizeX, stack.sizeZ);
disp('preallocated stack')

% before we start the loop, we have to read in the whole of the first
% volume
% what are the stack limits?
stack.first = first_img - stack.pad;
stack.last = first_img + stack.blockZ - 1 + stack.pad;
stack.allndx = stack.first : stack.last;

if (stack.wrapping)
    % put all values on the data interval
    stack.allndx = mod(stack.allndx, stack.totproj);
else
    % ignore those that are outside the interval
    stack.allndx(stack.allndx < 0 | stack.allndx >= stack.totproj)=nan ;
end

% reading the stack
thr_single = NaN; % no thresholding
nodisp = true ; % no display
firsttime = true;
for ii = 1:length(stack.allndx)
    if (~isnan(stack.allndx(ii)))
        ndx = stack.allndx(ii);
        if (firsttime)
            [stack.vol(:,:,ii), ~, stack.info] = gtReadAndThreshold(ndx, parameters, thr_single, nodisp);
            fprintf('loading image %04d (%04d of %04d)', ndx, ii, stack.sizeZ)
            firsttime = false;
        else
            stack.vol(:,:,ii) = gtReadAndThreshold(ndx, parameters, thr_single, nodisp, stack.info);
        end
        fprintf('\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b')
        fprintf('%04d (%04d of %04d)', ndx, ii, stack.sizeZ)
    end
end

fprintf('\n\nloaded image stack: %d x %d x %d voxels\n',...
    stack.sizeX, stack.sizeY, stack.sizeZ)


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%    main loop through data    %%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% recycle firsttime flag
firsttime = true;
% now we can loop
for zz = first_img : stack.blockZ : last_img

    if (firsttime)
        % first time around, we already have all the data we need
        firsttime = false ;
    else
        % subsequent loops, we have to update the stack
        newfirst = zz - stack.pad;
        newlast = zz + stack.blockZ -1 + stack.pad;
        newallndx = newfirst : newlast ;

        % we don't want to go beyond totproj-1+pad
        newallndx(newallndx > stack.totproj-1+stack.pad)=nan;
        % also, don't want to go beyond last_img+pad
        newallndx(newallndx > last_img+stack.pad)=nan;

        if (stack.wrapping)
            % put all values on the data interval
            newallndx = mod(newallndx, stack.totproj);
        else
            % ignore those that are outside the interval
            newallndx(newallndx < 0 | newallndx >= stack.totproj)=nan ;
        end

        overlap = stack.last - newfirst + 1 ;
        stack.vol(:,:,1:overlap) = stack.vol(:,:,(stack.sizeZ-overlap+1):stack.sizeZ);
        % read the rest
        fprintf('loading image ')
        for ii = (overlap+1):length(newallndx)
            if (~isnan(newallndx(ii)))
                % read the image
                ndx = newallndx(ii);
                fprintf('%04d (%04d of %04d)', ndx, ii, stack.sizeZ)
                stack.vol(:,:,ii) = gtReadAndThreshold(ndx, parameters, thr_single, nodisp, stack.info);
            else
                % make sure this one is zeros
                stack.vol(:,:,ii) = 0;
            end
            fprintf('\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b')
        end
        % keep last output
        fprintf('%04d (%04d of %04d\n\n)', ndx, ii, stack.sizeZ)
        % update the values in stack
        stack.first = newfirst;
        stack.last = newlast;
        stack.allndx = newallndx;
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%    find the seed pixels    %%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % this ignores the images padding the stack
    seedList = gtSegmentationFindSeeds(stack);
    fprintf('Found %d seeds...\n', size(seedList, 1))

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%      segment "blobs"      %%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    blobCount = 0;
    for seedndx = 1:size(seedList, 1)

        if seedList(seedndx, 7)
            if stack.debug
                disp('seed already marked as bad')
            end
            continue
        end

        blob = []; % new blob
        blob.seedID = seedList(seedndx, 1);

        % Calculate threshold, considering lower and upper grow limits
        blob.seedInt = seedList(seedndx, 6);
        blob.thr_grow = stack.seg.thr_grow_rat * blob.seedInt;

        blob.thr_grow = max(blob.thr_grow, stack.seg.thr_grow_low);
        blob.thr_grow = min(blob.thr_grow, stack.seg.thr_grow_high);

        % bounding box of seed voxel
        blob.bb = [seedList(seedndx, 2:4) 1 1 1];
        blob.seedX = seedList(seedndx, 2);
        blob.seedY = seedList(seedndx, 3);
        blob.seedZ = seedList(seedndx, 4);

        % extend bb, threshold, and test whether blob fits within the bb
        blob.test = ones(1,6);

        while any(blob.test)

            % extend the bounding box
            blob = sfExtendBB(blob, stack);

            % get the sub volume to work on from the stack
            bb = blob.bb;
            blob.rawgreyvol = stack.vol(bb(2):(bb(2)+bb(5)-1), bb(1):(bb(1)+bb(4)-1), bb(3):(bb(3)+bb(6)-1));

            % label volume
            blob.threshvol = blob.rawgreyvol > blob.thr_grow;
            blob.threshvol = bwlabeln(blob.threshvol);

            % pick out label containing seed
            bloblabel = blob.threshvol(blob.seedY-blob.bb(2)+1, blob.seedX-blob.bb(1)+1, blob.seedZ-blob.bb(3)+1);
            blob.threshvol = (blob.threshvol == bloblabel);

            % apply the label as a mask
            blob.greyvol    = blob.rawgreyvol .* blob.threshvol;

            % test intensity
            test_int = sfTestInt(blob);
            if (test_int)
                if (stack.debug)
                    disp('blob contains a value higher than the seed - bad');
                end
            end

            % test - max size exceeded?
            [test_size, blob] = sfTestBlobSize(blob, stack.seg.maxblobsize, stack.seg.minblobsize);
            if (test_size)
                if stack.debug
                    if (test_size == 1)
                        disp('blob exceeds the maximum size - bad')
                    elseif (test_size ==2)
                        disp('blob less than minimum size - bad')
                    end
                end
            end

            if (test_int || test_size)
                % blob is bad - set contain, lower value seeds to zero
                seedList = sfBadBlob(blob, seedList);
                break
            end

            % test - does blob reach edges of sub volume?
            % ...if so, do the while loop again
            blob = sfTestVol(blob, stack);

        end % extend ROI loop

        %    % minimum size check
        % combine this with the maximum size check as its slow
        %    test_min_size = sfTestBlobMinSize(blob, stack.seg.minblobsize);
        %
        if (test_int == 0 && test_size == 0) %&& test_min_size == 0
            % only write good blobs
            % could use sfNoEdges here, but I think not needed for setup
            blobCount = blobCount+1;
            blob.blobCount = blobCount ;
            % add to the ouput volume
            if (stack.debug)
                fprintf('Output a blob (%d)\n', blobCount)
            end

            % before we write the blob, trim the bb / volume to the limits of
            % the thresholded blob
            blob = sfTrimBlob(blob);
            % save the blob - to database and .edf as requested
            sfWriteBlob(blob, stack, parameters);
        end

    end % loop through seed volumes

end % loop through each block of data

% finally
t0 = clock;
fprintf('gtSegmentationDoubleThreshold for image %d to %d, finished at %dh%d...\n', ...
    first_img, last_img, t0(4), t0(5))

end % end of function


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%      sub functions      %%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function blob = sfTrimBlob(blob)
% We extend the blob according to extendblobincrement, but this can be more
% than we need, so trim down in z and adjust bb
% we have thhe profiles from sfTestBlobSize

    zstart = find(blob.profilez, 1, 'first');
    zend = find(blob.profilez, 1, 'last');
    xstart = find(blob.profilex, 1, 'first');
    xend = find(blob.profilex, 1, 'last');
    ystart = find(blob.profiley, 1, 'first');
    yend = find(blob.profiley, 1, 'last');
    % trim volumes
    blob.threshvol = blob.threshvol(ystart:yend, xstart:xend, zstart:zend);
    blob.greyvol = blob.greyvol(ystart:yend, xstart:xend, zstart:zend);
    blob.rawgreyvol = blob.rawgreyvol(ystart:yend, xstart:xend, zstart:zend);
    % update bb
    blob.bb(3) = blob.bb(3) + zstart - 1;
    blob.bb(6) = zend - zstart + 1;
    blob.bb(1) = blob.bb(1) + xstart - 1;
    blob.bb(4) = xend - xstart + 1;
    blob.bb(2) = blob.bb(2) + ystart - 1;
    blob.bb(5) = yend - ystart + 1;
    % set anything NaN to zeros
    mask = isnan(blob.greyvol);
    blob.threshvol(mask) = 0;
    blob.greyvol(mask) = 0;
    blob.rawgreyvol(mask) = 0;
end % end function sfTrimBlob

function sfWriteBlob(blob, stack, parameters)
% Calculate the difspot image and metadata.
% Depending on options chosen, write the blob and/or difspot to the
% database, and save the .edf file.

    % from original gtSegmentDiffractionBlobs
    % get a difblobID
    mym(gtDBInsert(stack.table_difblob, {'difblobID'}, 0))
    blob.difblobID = mym('SELECT last_insert_id()');

    fprintf('blobID %d output, blob %d from this block \n', blob.difblobID, blob.blobCount)

    if (stack.writeblobs)
        % convert to DB format, write to database
        % format is x, y, z, greyval
        indicies = find(blob.greyvol);
        [yy, xx, zz] = ind2sub(size(blob.greyvol), indicies);

        xx = xx + blob.bb(1) - 1;
        yy = yy + blob.bb(2) - 1;
        % put z into reference of the dataset
        zz = zz + stack.first + blob.bb(3) - 2;
        zz = mod(zz, stack.totproj);

        vals = blob.greyvol(indicies);

        if (stack.debug)
            fprintf('***DB WRITE *** Blob %d:inserting %d rows, grown from seed %d\n\n',...
                blob.difblobID, length(xx), blob.seedID)
        end

        gtDBInsertMultiple(stack.table_difblobdetails,...
            'difblobID', blob.difblobID(1, ones(length(xx), 1)), ...
            'xIndex', xx, ...
            'yIndex', yy, ...
            'zIndex', zz, ...
            'graylevel', vals);
    end

    if (stack.writehdf5)
        blob_bb = blob.bb;

        blob_mask = blob.greyvol ~= 0;

        % We still don't have any parameter that can enable this behavior
        if (true)
            img_size = [parameters.acq(1).xdet parameters.acq(1).ydet];

            % What about enlarging it? Let's do a fixed 3-5 pixels?
            num_pixels = 5;

            new_blob_bb = blob_bb + [-1 -1 0 2 2 0] .* num_pixels;

            new_limits = [new_blob_bb(1:3), new_blob_bb(1:3)+new_blob_bb(4:6)-1];

            chopped_limits = [ ...
                max(new_limits(1:2), [1 1]), new_limits(3), ...
                min(new_limits(4:5), img_size), new_limits(6) ];
            chopped_bb = [chopped_limits(1:3), ...
                chopped_limits(4:6)-chopped_limits(1:3)+1 ];

            shifts = [(num_pixels - (chopped_bb(1:2) - new_blob_bb(1:2))), 0];

            new_blob_mask = gtPlaceSubVolume(...
                zeros([chopped_bb(5), chopped_bb(4), chopped_bb(6)], 'uint8'), ...
                uint8(blob_mask), shifts);
            new_blob_mask = imdilate(new_blob_mask, strel('disk', num_pixels));

            blob_bb = chopped_bb;

            new_blob_vol = stack.vol(...
                blob_bb(2):(blob_bb(2)+blob_bb(5)-1), ...
                blob_bb(1):(blob_bb(1)+blob_bb(4)-1), ...
                blob_bb(3):(blob_bb(3)+blob_bb(6)-1));

            blob_mask = logical(new_blob_mask);
            blob_vol = new_blob_vol;
        else
            blob_vol = blob.rawgreyvol;
        end
        blob_vol(blob_vol < 0) = 0;
        % in case we want to have difspots with identical size to difblobs
        if (stack.seg.write_same_size)
            blob.greyvol = blob_vol;
            blob.bb = blob_bb;
            blob.rawgreyvol = new_blob_vol;
        end
        % put z into reference of the dataset
        blob_bb(3) = blob_bb(3) + stack.first - 1;
        blob_bb(3) = mod(blob_bb(3), stack.totproj);

        gtWriteBlobToHDF5(stack.difblobdir, blob.difblobID, blob_vol, blob_bb, blob_mask);
    end

    if (stack.writespots)
        gtSegmentationBlob2SpotTable(blob, stack)
    end

end % end function sfWriteBlob

function test = sfTestInt(blob)
% trivial test of blob max intensity
% working with double precision numbers - build in a tiny safety margin for
% rounding
    test = (max(blob.greyvol(:)) > (blob.seedInt+0.00000000001));
end % end function sfTestInt

function [test, blob] = sfTestBlobSize(blob, maxblobsize, minblobsize)
% find the extent of the thresholded blob, check against max size and
% min size
% could this be written in the form of sfTestVol, which is faster than
% adding to 1D profiles?
% return the profiles to use later
    profilex = sum(sum(abs(blob.threshvol), 3), 1)';
    profiley = sum(sum(abs(blob.threshvol), 3), 2);
    profilez = squeeze(sum(sum(abs(blob.threshvol), 1), 2));

    sizex = find(profilex, 1, 'last') - find(profilex, 1, 'first');
    sizey = find(profiley, 1, 'last') - find(profiley, 1, 'first');
    sizez = find(profilez, 1, 'last') - find(profilez, 1, 'first');

    test1 = any([sizex sizey sizez] > maxblobsize(1:3));
    test2 = any([sizex sizey sizez] < minblobsize(1:3));

    test = 0;
    if (test1)
        test = 1;
    elseif (test2)
        test = 2;
    end

    % Additional test for consistency check! should not be needed.
    [sizex, sizey, sizez] = size(blob.threshvol);
    if ((~test) && any([sizex sizey sizez] > (2 * maxblobsize(1:3))) )
        test = true;
        % If this happens, it's not a good thing: the growth of the blob is gone
        % beyond the limits and it is now including useless things
        warning('SEGMENTATION:logic_fail', ...
                'Blob BB extended with zeros beyond the double of blob size');
    end
    blob.profilex = profilex;
    blob.profiley = profiley;
    blob.profilez = profilez;
end % end function sfTestBlobSize

function seedList = sfBadBlob(blob, seedList)
    % not talking to the table
    % rather than labelling the blobs and search for seeds within them,
    % run through the list of known seeds with intensity<seedInt

    % find the seed voxels within the blob with intensity<seedInt,
    % set these to bad=1 in the table
    % voxels at the edge of the subvol could be indentified wrongly as seeds.
    % mostly copied from seed_threshold

    % find candidate bad seeds based on value and position
    % only consider seeds that are currently 'good'
    bb = blob.bb;
    badseeds = find( (seedList(:, 6) <= blob.seedInt) & ...
        (seedList(:, 7) == 0) & ...
        (seedList(:, 2) >= bb(1)) & (seedList(:, 2) < (bb(1)+bb(4))) & ...
        (seedList(:, 3) >= bb(2)) & (seedList(:, 3) < (bb(2)+bb(5))) & ...
        (seedList(:, 4) >= bb(3)) & (seedList(:, 4) < (bb(3)+bb(6))) );
    % are these candidates part of the blob?
    for ii = 1:length(badseeds)
        ndx = badseeds(ii);
        sub = seedList(ndx, 2:4);
        % offset to our bounding box
        sub = sub - bb(1:3) + 1;
        if (blob.greyvol(sub(2), sub(1), sub(3)) > 0)
            % this is a bad seed - lower intensity and connected to our
            % seed
            seedList(ndx, 7) = 1;
        end
    end
end % end function sfBadBlob

function blob = sfExtendBB(blob, stack)
    % test which tells which sides to extend,
    % recalculate the bounding box
    % the stack is padded according to the maximum blob size -
    % therefore a blob must not extend beyond the stack
    bb = blob.bb;
    blob.oldbb = bb;
    extendblobinc = stack.seg.extendblobinc;

    if blob.test(1) % extend bb in x -ve
        dif   = min(bb(1)-1, extendblobinc(1));
        bb(1) = bb(1) - dif;
        bb(4) = bb(4) + dif;
    end
    if blob.test(2) % extend bb in y -ve
        dif   = min(bb(2)-1, extendblobinc(2));
        bb(2) = bb(2) - dif;
        bb(5) = bb(5) + dif;
    end
    if blob.test(3) % extend bb in z -ve
        dif   = min(bb(3)-1, extendblobinc(3));
        bb(3) = bb(3) - dif;
        bb(6) = bb(6) + dif;
    end
    if blob.test(4) % extend bb in x +ve
        dif   = min(stack.sizeX-(bb(1)+bb(4)-1), extendblobinc(1));
        bb(4) = bb(4) + dif;
    end
    if blob.test(5) % extend bb in y +ve
        dif   = min(stack.sizeY-(bb(2)+bb(5)-1), extendblobinc(2));
        bb(5) = bb(5) + dif;
    end
    if blob.test(6) % extend bb in z +ve
        dif = min(stack.sizeZ-(bb(3)+bb(6)-1), extendblobinc(3)); % should limit it the stack
        bb(6) = bb(6) + dif;
    end
    % this should be redundent - keep for testing
    if ( bb(3)<1 || (bb(3)+bb(6)-1)>stack.sizeZ ) && any(blob.test)
        % if we go beyond the zrange of the stack, any extension
        % requires special behaviour
        blob.special = true;
        keyboard
    else
        blob.special = false;
    end
    blob.bb = bb;
end % end function sfExtendBB

function blob = sfTestVol(blob, stack)
% does a thresholded blob extend to the edges of the ROI?  And which
% edges?

    bb = blob.bb;
    totproj = stack.totproj-1;
    xdet = stack.sizeX;
    ydet = stack.sizeY;
    test = zeros(1, 6); % ~same format as bb

    % low x
    test(1) = ( (bb(1) > 1) && ~isempty(find(blob.threshvol(:, 1, :), 1, 'first')) );
    % low y
    test(2) = ( (bb(2) > 1) && ~isempty(find(blob.threshvol(1, :, :), 1, 'first')) );
    % low z (omega) - note 1 because subvol, not 0 for image zero
    if (stack.wrapping)
        test(3) = ~isempty(find(blob.threshvol(:, :, 1), 1, 'first'));
    else
        test(3) = ( ((stack.first+bb(3)-1)> 0) && ~isempty(find(blob.threshvol(:, :, 1), 1, 'first')) );
    end
    % high x
    test(4) = ( ((bb(1)+bb(4)-1) < xdet) && ~isempty(find(blob.threshvol(:, end, :), 1, 'first')) );
    % high y
    test(5) = ( ((bb(2)+bb(5)-1) < ydet) && ~isempty(find(blob.threshvol(end, :, :), 1, 'first')) );
    % high z (omega)
    if (stack.wrapping)
        test(6) =  ~isempty(find(blob.threshvol(:, :, end), 1, 'first'));
    else
        test(6) = ( ((stack.first+bb(3)+bb(6)-2) < totproj) && ~isempty(find(blob.threshvol(:, :, end), 1, 'first')) );
    end
    % output
    blob.test = test;
end % end function sfTestVol
