function gtWriteBlobToHDF5(blob_dir, blob_id, blob_vol, blob_bb, blob_mask)
    sub_dir = fullfile(blob_dir, sprintf('%06d', blob_id - mod(blob_id, 1e4)));
    if (~exist(sub_dir, 'dir'))
        [status, message, messageid] = mkdir(sub_dir);
        if (~status)
            gtError(messageid, message)
        elseif (~isempty(messageid))
            warning(messageid, message)
        end
    end
    fnameout = fullfile(sub_dir, sprintf('difblob_%06d.hdf5', blob_id) );

    if (exist(fnameout, 'file'))
        delete(fnameout);
    end
    vol_size = [size(blob_vol, 1) size(blob_vol, 2) size(blob_vol, 3)];
    chunk_size = min([16 16 4], vol_size);
    if (exist(fnameout, 'file'))
        delete(fnameout);
    end
    h5create(fnameout, '/rawblob', vol_size, 'Datatype', 'single', 'Deflate', 1, 'ChunkSize', chunk_size);

    h5write(fnameout, '/rawblob', single(blob_vol));
    h5writeatt(fnameout, '/rawblob', 'bb', uint32(blob_bb));

    % Mask comes from segmentation
    h5create(fnameout, '/mask', vol_size, 'Datatype', 'uint8', 'Deflate', 1, 'ChunkSize', chunk_size);
    if (~exist('blob_mask', 'var'))
        h5write(fnameout, '/mask', ones(size(blob_vol), 'uint8'));
    else
        h5write(fnameout, '/mask', uint8(blob_mask));
    end
end