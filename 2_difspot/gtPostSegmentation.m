function gtPostSegmentation()
% GTPOSTSEGMENTATION
%     gtPostSegmentation()
%     --------------------
%     helper function to do the steps after segmentation and before pair
%     matching
%
%
%     SUB-FUNCTIONS:
%       gtRemoveOverlappingDifblobs
%       gtDBCreateDifspotTable
%[oar]- gtDBBlob2SpotTable_WriteDifspots
%
%
% Version 002 15/06/2012 by A.King
%     As gtDBBlob2SpotTable_WriteDifspots has become the standard version,
%     with a flag to control writing/not writing difspots, modify
%     gtPostSegmentation to reflect this.
%


% load('parameters.mat'); file to look for flags
parameters = [];
load('parameters.mat');
gtDBConnect();

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% should gtRemoveOverlapping difspots be run?
if isfield(parameters.seg, 'overlaps_removed') && (parameters.seg.overlaps_removed == 0)
    % automatic case
    disp('Running gtRemoveOverlappingDifblobs  --  this may take a few minutes')
    pause(2);
    gtRemoveOverlappingDifblobs();
    % note gtRemoveOverlappingDifblobs updates the parameters flags
else
    % if not sure, ask :-)
    disp('If you used single threshold segmentation, you should also run gtRemoveOverlappingDifspots')
    disp('to deal with blobs split between OAR jobs.')
    disp('Double threshold segmentation does not need this.')
    % modify default response according to parameters file flags
    if isfield(parameters.seg, 'overlaps_removed') && (parameters.seg.overlaps_removed == 1)
        check=inputwdefault('Run gtRemoveOverlappingDifspots? [y/n]', 'n');
    else
        check=inputwdefault('Run gtRemoveOverlappingDifspots? [y/n]', 'y');
    end
    if strcmpi(check, 'y')
        gtRemoveOverlappingDifblobs();
    end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% launch conversion of blobs to spots with OAR
disp('setup writing difspots:')
disp(' ')
disp(' ')
% get values for OAR jobs
gtDBConnect();
nblobs = mym(['SELECT max(difblobid) FROM ' parameters.acq.name 'difblob']);
if (nblobs > 500)
    njobs = min(round(nblobs/500), 20);
else
    njobs = 1;
end

% check if difspots have already been created
test = mym(['SELECT count(*) FROM ' parameters.acq.name 'difspot']);
if (test ~= 0)
    % check if we want to drop and re-create the tables, and run again
    check = inputwdefault('Data exists in difspot table! Drop table and re-create data? [y/n]', 'n');
    if strcmpi(check, 'y')
        overwrite_flag = true;
        gtDBCreateDifspotTable(parameters.acq.name, overwrite_flag);
    else
        disp('Keeping difspot data')
    end
end
        
% Otherwise, continue...
% ask user whether to write out difspots
check = inputwdefault('Launch gtDBBlob2SpotTable with OAR? [y/n]', 'y');
if strcmpi(check, 'y')
    check2 = inputwdefault('Write out difspots as .edf files? [y/n]', 'y');
    if strcmpi(check2, 'y')
        % Old EDFs into the folder
        baseDir = fullfile(parameters.acq.dir, '2_difspot');
        deadFiles = fullfile(baseDir, 'difspot*.edf');
        % Directories containing EDFs
        deadDirs = dir(fullfile(baseDir, '*000'));
        deadDirs = {deadDirs([deadDirs.isdir] == true).name};

        fprintf('In %s, removing previous spots images..', baseDir);
        delete(deadFiles);
        gauge = GtGauge(numel(deadDirs), '\b\b, removing directories: ');
        for n = 1:numel(deadDirs)
            gauge.incrementAndDisplay();
            
            rmdir(fullfile(baseDir, deadDirs{n}), 's');
        end
        gauge.delete()
        gtOarLaunch('gtDBBlob2SpotTable', 1, nblobs, njobs, [parameters.acq.dir, ' 1'], true)
    else
        gtOarLaunch('gtDBBlob2SpotTable', 1, nblobs, njobs, [parameters.acq.dir, ' 0'], true)
    end
else
    disp('Run gtDBBlob2SpotTable yourself')
end

end
