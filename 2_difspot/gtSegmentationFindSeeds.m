function seedList = gtSegmentationFindSeeds(stack)
% pass in "stack".  This is a structure containing a volume, the seg
% field of parameters.mat, and the size of the volume as sizeY (dim1),
% sizeX(dim2), sizeZ (dim3)
%
% find seed pixels - highest pixels within each connected region
% over the seed threshold
% 3D seed threshold
% used by both the setup GUI and the segmentation function
% seed min area should become seed min volume


seeds = stack.vol > stack.seg.thr_seed;
if isfield(stack, 'pad')
    % in the function, stack is padded
    % don't look at these seeds
    seeds(:,:,[1:stack.pad (end-stack.pad+1):end]) = 0;
end
   
% label with bwconncomp
CC = bwconncomp(seeds);

% highest pixel within each seed blob
seedList=[];
seedCount=1;
for ii=1:length(CC.PixelIdxList)
    % test size
    if length(CC.PixelIdxList{ii}) < stack.seg.seedminarea
        continue
    end
    
    % find the max pixel - seed
    seedndxlist=CC.PixelIdxList{ii};
    zz = stack.vol(seedndxlist);
    [seedInt, listndx] = max(zz);
    seedndx = seedndxlist(listndx);
    [seedY,seedX,seedZ] = ind2sub([stack.sizeY, stack.sizeX, stack.sizeZ], seedndx);
    
    % seedList: [id x y z ndx int bad]
    seedList(seedCount, 1:7) = [seedCount seedX seedY seedZ seedndx seedInt 0];
    seedCount = seedCount+1;
end

% sort list, descending order of intensity
if seedCount>1
    seedList = sortrows(seedList, -6);
end
