# -*- coding: utf-8 -*-
"""
XML handling package.

Created on Dec 27, 2011.

@author: Nicola VIGANÒ, ESRF - The European Synchrotron, Grenoble, France
"""

import os

import datetime
import shutil
import fnmatch

import xml.etree.ElementTree as ET


class bcolors:
    PLAIN = "\033[39;0m"
    ERROR = "\033[31;1m"
    WARNING = "\033[33;1m"
    INFO = "\033[36;1m"
    JOB = "\033[39;1m"
    SUBJOB = "\033[32;1m"
    SUBSUBJOB = "\033[32m"


class DCTOutput(object):
    def __init__(self, verbose_lvl=1):
        if verbose_lvl:
            verbose_lvl = 1
        self.verbose_lvl = verbose_lvl

    @staticmethod
    def printError(args):
        print(bcolors.ERROR + "Error: " + "".join([str(x) for x in args]) + bcolors.PLAIN)

    @staticmethod
    def printWarning(args):
        print(bcolors.WARNING + "Warning: " + "".join(args) + bcolors.PLAIN)

    def printInfoRecord(self, title, args):
        if self.verbose_lvl >= 1:
            print("  " + title + ": " + bcolors.INFO + "".join(args) + bcolors.PLAIN)

    def printJob(self, args):
        if self.verbose_lvl >= 1:
            print(bcolors.JOB + "".join(args) + bcolors.PLAIN)

    def printSubJob(self, args):
        if self.verbose_lvl >= 1:
            print("  * " + bcolors.SUBJOB + "".join(args) + bcolors.PLAIN)

    def printSubSubJob(self, title, args):
        if self.verbose_lvl >= 1:
            print("    - " + bcolors.SUBSUBJOB + title + bcolors.PLAIN + ": " + "".join(args))

    def printSubSubSubJob(self, title, args):
        if self.verbose_lvl >= 1:
            print("      + " + title + ": %s" + "".join(args))


class DCTXMLBase(object):
    def __init__(self, xml_file=None):
        self.xml_file = xml_file
        self.tree = ET.parse(self.xml_file)

    def save(self, new_name=None):
        if new_name is not None:
            self.xml_file = new_name
        self.tree.write(self.xml_file)


class DCTCheck(object):
    @staticmethod
    def checkCwdWithDCTDir(dctdir=os.getenv("DCT_DIR")):
        print("Checking if current directory matches with DCT_DIR environment variable...")
        indir = os.getcwd()
        try:
            os.chdir(dctdir)
            testdir = os.getcwd()
        except FileNotFoundError:
            print("DCT_DIR environment variable (%s) points to inexistent directory!" % dctdir)
            raise
        os.chdir(indir)
        if indir == testdir:
            print("OK")
        else:
            raise EnvironmentError(
                "Current working directory does not match with DCT_DIR environment variable!\n%s != %s" % (indir, dctdir)
            )


class DCTConf(DCTXMLBase):
    def __init__(self, dct_dir, xml_file_custom=None):
        self.dct_dir = dct_dir

        self.xml_file_defaults = os.path.join(self.dct_dir, "zUtil_Conf", "conf.default.xml")
        self.xml_file_defaults = os.path.abspath(self.xml_file_defaults)

        if xml_file_custom is None or xml_file_custom == "":
            xml_file_custom = os.path.join(self.dct_dir, "conf.xml")
        else:
            xml_file_custom = os.path.join(self.dct_dir, xml_file_custom)
        xml_file_custom = os.path.abspath(xml_file_custom)
        self.xml_file_custom = xml_file_custom

        if os.path.exists(self.xml_file_custom):
            print("Using local configuration file: {}{}{}".format(bcolors.INFO, self.xml_file_custom, bcolors.PLAIN))
            DCTXMLBase.__init__(self, self.xml_file_custom)
        else:
            print("Using default configuration file: {}{}{}".format(bcolors.INFO, self.xml_file_defaults, bcolors.PLAIN))
            DCTXMLBase.__init__(self, self.xml_file_defaults)

    def get_version(self):
        return self.tree.findtext("version", "0")

    def get_matlab_version(self):
        return self.tree.findtext("matlab/version", "2013a")

    def get_matlab_path(self, hostname=None):
        matlab_ver = self.get_matlab_version()

        items = self.tree.findall("matlab/path")
        for item in items:
            if (hostname is not None) and ("hostname" in item.attrib.keys()):
                hosts = item.attrib.get("hostname")
                for pattern in hosts.split(","):
                    if fnmatch.fnmatch(hostname, pattern):
                        return item.text.format(matlab_ver)
            else:
                return item.text.format(matlab_ver)

        # If none was found, we fall back to the default ESRF paths
        return os.path.join("/sware", "com", "matlab_{}".format(matlab_ver))

    def get_matlab_options(self):
        items = self.tree.findall("matlab/options/opt")
        output = []
        for item in items:
            output.append(item.text)
        return output

    def get_ignore_ID19(self):
        element = self.tree.find("dct/ignore_id19")
        return element.get("value")

    def get_paths_list(self, path_name, hostname=None, matlab_ver=None):
        items = self.tree.findall(path_name)
        output = []
        for item in items:
            if (hostname is not None) and ("hostname" in item.attrib.keys()):
                hosts = item.attrib.get("hostname")
                for pattern in hosts.split(","):
                    if fnmatch.fnmatch(hostname, pattern):
                        if (matlab_ver is not None) and "version" in item.attrib.keys():
                            versions = item.attrib.get("version")
                            for pattern in versions.split(","):
                                if fnmatch.fnmatch(matlab_ver, pattern):
                                    output.append(item.text)
                                    break
                        else:
                            output.append(item.text)
                            break
            else:
                output.append(item.text)
        return output

    def get_runtime_libs_path(self):
        return "matlab/libraries/path"

    def get_preload_libs_path(self):
        return "matlab/preload/path"

    def get_hdf5_plugins_path(self):
        return "matlab/hdf5_plugins/path"

    def get_root_dir(self):
        return self.tree.findtext("root_dir")

    def set_root_dir(self, newRoot):
        self.tree.find("root_dir").text = newRoot

    def get_mex_files(self):
        return MexConf(self.tree.find("mex_files"))

    def set_conf(self, conf_section, value):
        self.tree.find(conf_section).text = value


class MexConf(object):
    def __init__(self, tree):
        self.tree = tree

    def is_excluded(self, test_file, dct_dir):
        for exclude in self.tree.findall("exclude_list/path"):
            exclude_path = os.path.join(dct_dir, exclude.text)
            if fnmatch.fnmatch(test_file, exclude_path):
                return True
        else:
            return False

    def get_file_properties(self, filePath):
        for fileEntry in self.tree.findall("file"):
            if fileEntry.findtext("path") in filePath:
                output = {}

                output["includes"] = []
                for include in fileEntry.findall("includes/path"):
                    output["includes"].append(include.text)

                output["lib_paths"] = []
                for lib_path in fileEntry.findall("libraries/path"):
                    output["lib_paths"].append(lib_path.text)

                output["libs"] = []
                for lib in fileEntry.findall("libraries/lib"):
                    output["libs"].append(lib.text)

                output["defines"] = []
                for lib in fileEntry.findall("preprocessor/define"):
                    output["defines"].append(lib.text)

                output["options"] = []
                for lib in fileEntry.findall("preprocessor/option"):
                    output["options"].append(lib.text)

                return output
        else:
            return None


class DCTConfUpdater(object):
    def __init__(self, base_dir):
        self.base_dir = base_dir

    def _get_new_filename(self, filepath):
        timestamp = datetime.datetime.fromtimestamp(os.path.getmtime(filepath))
        pieces = os.path.splitext(filepath)
        return pieces[0] + "-" + timestamp.strftime("%Y-%m-%d-%H-%M") + pieces[1]

    def backup(self, filename):
        filepath = os.path.join(self.base_dir, filename)
        if not os.path.exists(filepath):
            raise ValueError("No such file %s" % filename)
        newFilePath = self._get_new_filename(filepath)
        os.rename(filepath, newFilePath)
        print("Made backup copy of file: '%s' -> '%s'" % (filepath, newFilePath))

    def cleanup(self, file_name):
        try:
            self.backup(file_name)
        except ValueError:
            print("Configuration already cleaned")

    def copy(self, src_path, dest_filename):
        newfilepath = os.path.join(self.base_dir, dest_filename)
        print("New file path: %s" % newfilepath)
        shutil.copy2(src_path, newfilepath)

    def safely_install_new_file(self, src_path, dest_filename):
        try:
            self.backup(dest_filename)
        except ValueError:
            print("Creating new file: '%s'" % dest_filename)
        self.copy(src_path, dest_filename)

    def update_xml_conf_file(self, conf_section, value, path_conf_xml="conf.xml"):
        path_conf_xml = os.path.join(self.base_dir, path_conf_xml)
        conf = DCTConf(self.base_dir, path_conf_xml)
        conf.set_conf(conf_section, value)
        conf.save()
        print("Updated '%s' with: '%s' <- '%s'" % (path_conf_xml, conf_section, value))
