#!/usr/bin/env python
"""
Created May 22nd, 2013

@author: StefanS
"""

import os
import sys


def print_help():
    scrName = os.path.basename(__file__)
    print("Usage:")
    print("%s stemImages n_proj, stemOut, dirImages, dirLinks" % scrName)
    print("where:")
    print("stemImages = Stem of the source images (i.e. without _0_0000.edf)")
    print("n_proj     = 1800 for a scan with 3600 images")
    print("stemOut    = Name you want to give the symbolic links, e.g. SampleName_")
    print("dirImages  = Relative path to the source images")
    print("dirLinks   = Relative path to where you want to place the symbolic links.")
    print("             If the directory doesn't exist, it will be created.")


def renumber_interlaced_scan(stemImages, n_proj, stemOut, dirImages, dirLinks):
    # checking paths
    if not os.path.isdir(dirImages):
        print("Couldn't find the specified directory to the source images. Check the given path.")
        sys.exit(1)
    if not os.path.isdir(dirLinks):
        print("%s does not exist...therefore creating it." % dirLinks)
        make_directory(dirLinks)
    # create the source dest dictonary for the symlinks
    dictLinks = create_dict(stemImages, stemOut, n_proj)
    # Create the symlinks
    errList, symLinkError = create_symlinks(dictLinks, dirImages, dirLinks)
    # Output the list of files on which we didn't succeed
    if symLinkError is True:
        print("Some symbolic links for following files couldn't be created:")
        print(errList)
        return 1
    else:
        return 0


def create_dict(stem, linkname, n_proj):
    stemlist = []
    linklist = []
    for x in xrange(n_proj):
        stemlist.append(stem + "_0_" + format(x, "04d") + ".edf")
        linklist.append(linkname + format(x * 2, "04d") + ".edf")
    for x in xrange(n_proj):
        stemlist.append(stem + "_1_" + format(x, "04d") + ".edf")
        linklist.append(linkname + format(((x * 2) + 1), "04d") + ".edf")
    return dict(zip(stemlist, linklist))


def create_symlinks(dictLinks, dirTarget, dirLinks):
    symLinkError = False
    errList = []
    pwd = os.path.abspath(".")
    for src, lname in dictLinks.iteritems():
        srcabs = os.path.join(pwd, dirTarget, src)
        lnameabs = os.path.join(pwd, dirLinks, lname)
        # Checking the target exists
        if os.path.isfile(srcabs):
            try:
                os.symlink(srcabs, lnameabs)
            except:
                symLinkError = True
                errList.append(lnameabs)
        else:
            print("Target doesn't exist. Check stem and target directory arguments!")
            symLinkError = True
            errList.append(srcabs)
        if not os.path.islink(lnameabs):
            errList.append(lnameabs)
    return errList, symLinkError


def make_directory(path):
    try:
        os.makedirs(path)
    except OSerror as err:
        if err.errno != errno.EEXIST:
            print("Couldn't create the directory. Exiting...")
            sys.exit(1)


if __name__ == "__main__":

    if len(sys.argv) < 6:
        print("Forgot an argument?")
        print_help()
        sys.exit(1)

    stemImages = sys.argv[1]
    n_proj = int(sys.argv[2])
    stemOut = sys.argv[3]
    dirImages = sys.argv[4]
    dirLinks = sys.argv[5]
    err = renumber_interlaced_scan(stemImages, n_proj, stemOut, dirImages, dirLinks)
    sys.exit(err)
