import pathlib
from setuptools import find_packages, setup

repo_name = "dct"

setup(
    name=repo_name,
    version="0.0.0",
    description="A fresh python DCT code.",
    url="https://gitlab.esrf.fr/graintracking/dct",
    classifiers=[
        "Programming Language :: Python :: 3",
    ],
    packages=find_packages(where=repo_name),
    python_requires=">=3.8",
    install_requires=[
        "silx==0.15.0",
    ],
    extras_require={
        "dev": ["pytest==6.2.3"],
    },
)
