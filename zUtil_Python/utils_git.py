"""
Created on Jan 30, 2013

@author: ben
"""

import os
import subprocess
import shlex
import re


class DCTGit(object):

    repo_url = "git.code.sf.net/p/dct/code-git"

    @staticmethod
    def getAvailableRemoteBranches():
        cmd = "git ls-remote git://%s master stable*" % DCTGit.repo_url
        cmd = shlex.split(cmd)
        subobj = subprocess.Popen(cmd, stdout=subprocess.PIPE)
        retcode = subobj.wait()
        if retcode != 0:
            raise ValueError("Couldn't fetch branches information")

        branches_txt = subobj.stdout.read()
        regexp = re.compile("refs/heads/.*")
        matches = regexp.findall(branches_txt)
        for index in range(len(matches)):
            matches[index] = matches[index][11:]

        return matches

    @staticmethod
    def setGlobalConfig(conf_path, value):
        cmd = ["git", "config", "--global", conf_path, value]
        subobj = subprocess.Popen(cmd, stderr=subprocess.PIPE)

        if subobj.wait() != 0:
            raise ValueError("Couldn't set GLOBAL property: %s, as %s." % (conf_path, value))

    @staticmethod
    def getGlobalConfig(conf_path):
        cmd = ["git", "config", "--global", conf_path]
        subobj = subprocess.Popen(cmd, stderr=subprocess.PIPE, stdout=subprocess.PIPE)

        if subobj.wait() != 0:
            raise ValueError("Couldn't get GLOBAL property: %s." % conf_path)

        value = subobj.stdout.read()
        return value[:-1]

    def __init__(self, repo_dir=""):
        if repo_dir == "":
            repo_dir = os.path.join(os.getcwd(), "dct")
        self.repo_dir = repo_dir

    def _cloneRepo(self, cmd, dest_dir="", branch=""):
        if dest_dir == "":
            dest_dir = self.repo_dir
        dest_dir = os.path.join(dest_dir, "")
        dest_dir = os.path.abspath(dest_dir)
        cmd = " ".join([cmd, dest_dir])

        self.repo_dir = dest_dir

        #        parent_dir = os.path.abspath(os.path.join(dest_dir, os.path.pardir))
        #        os.chdir(parent_dir)

        if not branch == "":
            cmd = "%s -b %s" % (cmd, branch)

        cmd = shlex.split(cmd)
        return subprocess.call(cmd)

    def createDeveloperRepo(self, username, dest_dir="", branch=""):
        cmd = "git clone ssh://%s@%s" % (username, DCTGit.repo_url)
        return self._cloneRepo(cmd, dest_dir, branch)

    def createUserRepo(self, dest_dir="", branch=""):
        cmd = "git clone git://%s --depth 1" % DCTGit.repo_url
        return self._cloneRepo(cmd, dest_dir, branch)

    def updateRepo(self, repo_dir=""):
        if not repo_dir == "":
            self.repo_dir = repo_dir
        currentDir = os.getcwd()

        os.chdir(self.repo_dir)
        try:
            cmd = "git pull"
            cmd = shlex.split(cmd)
            retcode = subprocess.call(cmd)
        finally:
            os.chdir(currentDir)
        return retcode

    def getFilePathInRepo(self, *args):
        return os.path.join(self.repo_dir, *args)

    def setRepoConfig(self, conf_path, value):
        currentDir = os.getcwd()
        os.chdir(self.repo_dir)
        cmd = ["git", "config", "--global", conf_path, value]
        subobj = subprocess.Popen(cmd, stderr=subprocess.PIPE)

        if not subobj.wait() == 0:
            raise ValueError("Couldn't set property: %s, as %s, in repo %s." % (conf_path, value, self.repo_dir))

        os.chdir(currentDir)

    def getRepoConfig(self, conf_path):
        currentDir = os.getcwd()
        os.chdir(self.repo_dir)
        cmd = ["git", "config", "--global", conf_path]
        subobj = subprocess.Popen(cmd, stderr=subprocess.PIPE, stdout=subprocess.PIPE)

        if not subobj.wait() == 0:
            raise ValueError("Couldn't get property: %s, in repo %s." % (conf_path, self.repo_dir))

        os.chdir(currentDir)
        value = subobj.stdout.read()
        return value[:-1]

    def getSourceForgeAccount(self):
        currentDir = os.getcwd()
        os.chdir(self.repo_dir)
        cmd = ["git", "config", "remote.origin.url"]
        subobj = subprocess.Popen(cmd, stderr=subprocess.PIPE, stdout=subprocess.PIPE)
        subobj.wait()
        url = subobj.stdout.read()
        os.chdir(currentDir)

        regexp = re.compile("//.*@")
        match = regexp.findall(url)
        if len(match) > 0:
            return match[0][2:-1]
        else:
            return ""
