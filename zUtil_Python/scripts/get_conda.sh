# using a conda installation that I found at `cat $(which silx)`
echo "Loading dau's miniconda."
echo "Warning: this will only work at the esrf..."

os=$(/csadmin/common/scripts/get_os)
source /sware/isdd/packages/${os}/dau/miniconda3/etc/profile.d/conda.sh
