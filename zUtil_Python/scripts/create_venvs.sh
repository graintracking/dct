#!/bin/bash

set -e

SCRIPTDIR="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

echo "source $SCRIPTDIR/get_conda.sh"
source $SCRIPTDIR/get_conda.sh

source $SCRIPTDIR/envs.sh

ENV_NAME=$DCTENV
SETUP_PY_PATH=$DCTSOURCEPARENTPATH

# =================================== DCT ===================================

echo "deleting previously installed env '$ENV_NAME'"
rm -rf $HOME/.conda/envs/$ENV_NAME

echo "installing env '$ENV_NAME'"
conda create --yes --force --name ${ENV_NAME} -c conda-forge python=3.8
conda activate ${ENV_NAME}
cd $SETUP_PY_PATH
pip install .[dev]





exit 0
