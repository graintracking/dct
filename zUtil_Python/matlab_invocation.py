# -*- coding: utf-8 -*-
"""
Matlab invocation class.

Created on Feb 7, 2013.

@author: Nicola VIGANÒ, ESRF - The European Synchrotron, Grenoble, France
"""

import os
import subprocess

from . import io_xml
from . import utils_platform


class DCTMatlabInvocation(object):
    def __init__(
        self, dct_dir, work_dir=os.getcwd(), extra_args=[], extra_cmds="", ctrl_c_kill=False, skip_functions_check=False
    ):
        self.dct_dir = dct_dir
        self.work_dir = work_dir

        self.args = extra_args
        self.extra_cmds = extra_cmds

        self.ctrl_c_kill = ctrl_c_kill
        self.skip_functions_check = skip_functions_check

        self.utils = utils_platform.UtilsFactory.getMachineDep()

    @staticmethod
    def filter_parameters(args):
        launcher_accepted_args = ("--profile", "--debug", "--script")
        decoded_args = {"profile": None, "debug": None, "script": None}
        skip_next = False
        for ind_arg in range(len(args)):
            if skip_next is True:
                skip_next = False
                continue
            for modifier in launcher_accepted_args:
                if args[ind_arg] == modifier:
                    if len(args) <= (ind_arg + 1):
                        raise ValueError("Not enough arguments for modifier: %s" % modifier)
                    if decoded_args[modifier[2:]] is not None:
                        io_xml.DCTOutput.printWarning(
                            "Overwriting previous argument '%s' for modifier '%s', with argument '%s'"
                            % decoded_args[modifier[2:]],
                            modifier,
                            args[ind_arg + 1],
                        )
                    decoded_args[modifier[2:]] = args[ind_arg + 1]
                    skip_next = True
        if decoded_args["profile"] is not None and decoded_args["debug"] is not None:
            raise ValueError("Profiling and debugging cannot be used at the same time!")
        return decoded_args

    def _add_paths_to_env(self, os_var_name, xml_path_name):
        try:
            prev_env_var = os.environ[os_var_name]
        except KeyError:
            prev_env_var = ""

        paths = self.conf.get_paths_list(xml_path_name, hostname=self.utils.hostname)
        paths.append(prev_env_var)
        os.environ[os_var_name] = ":".join(paths)

        return prev_env_var


    def invoke(self, profiler=None, debugger=None):
        self.conf = io_xml.DCTConf(self.dct_dir)

        # Adding runtime libraries
        prev_runtime = self._add_paths_to_env("LD_LIBRARY_PATH", self.conf.get_runtime_libs_path())

        # Adding preload libraries
        prev_preload = self._add_paths_to_env("LD_PRELOAD", self.conf.get_preload_libs_path())

        # Adding HDF5 plugins
        prev_hdf5_pi = self._add_paths_to_env("HDF5_PLUGIN_PATH", self.conf.get_hdf5_plugins_path())

        if debugger == "valgrind":
            os.environ["MALLOC_CHECK_"] = "1"

        cmd = []
        if profiler == "cuda":
            cmd = cmd + ["/usr/local/cuda/bin/nvprof", "-f", "-o", "profile_cuda_matlab_script"]  # , "--analysis-metrics"
        elif profiler == "oprofile":
            cmd = cmd + ["operf"]
        cmd = cmd + [os.path.join(self.conf.get_matlab_path(self.utils.hostname), "bin", "matlab")]

        cmd = cmd + self.conf.get_matlab_options()
        cmd = cmd + self.args

        if debugger == "valgrind":
            cmd = cmd + [
                "-Dvalgrind --leak-check=yes --error-limit=no --log-file=memcheck_matlab_script.log",
                "-nojvm",
                "-nosplash",
            ]
        if profiler == "callgrind":
            cmd = cmd + ["-Dvalgrind --tool=callgrind --log-file=callgrind_matlab_script.log", "-nojvm", "-nosplash"]

        cmd.append("-r")

        ignore_id19 = self.conf.get_ignore_ID19()
        if self.skip_functions_check is True:
            skip_functions = "true"
        else:
            skip_functions = "false"
        cmd.append(
            "cd('%s');initialise_gt(%s,%s);cd('%s');%s"
            % (self.dct_dir, ignore_id19, skip_functions, self.work_dir, "".join(self.extra_cmds))
        )

        print("Calling: {}{}{} ".format(io_xml.bcolors.JOB, cmd[0], io_xml.bcolors.PLAIN) + " ".join(cmd[1:]))

        subobj = subprocess.Popen(cmd)
        while subobj.poll() is None:
            try:
                subobj.wait()
            except KeyboardInterrupt:
                if self.ctrl_c_kill is True:
                    print("Sending kill signal to matlab...")
                    subobj.kill()

        os.environ["LD_LIBRARY_PATH"] = prev_runtime
        os.environ["LD_PRELOAD"] = prev_preload
        os.environ["HDF5_PLUGIN_PATH"] = prev_hdf5_pi
