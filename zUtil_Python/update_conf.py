# -*- coding: utf-8 -*-
"""
Configuration updating script.

Created on Apr 25, 2012.

@author: Nicola VIGANÒ, ESRF - The European Synchrotron, Grenoble, France
"""

import os
import sys

from . import io_xml

if __name__ == "__main__":
    dctPath = os.path.abspath(os.path.dirname(sys.argv[0]))
    dctPath = os.path.join(dctPath, os.path.pardir)
    dctPath = os.path.abspath(dctPath)

    upd = io_xml.DCTConfUpdater(dctPath)
    confexamplefile = os.path.join(dctPath, "zUtil_Conf", "conf.default.xml")
    upd.safely_install_new_file(confexamplefile, "conf.xml")
