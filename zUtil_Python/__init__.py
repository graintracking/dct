# -*- coding: utf-8 -*-
"""
Top-level package for DCT.

Created on Feb 3, 2013.

@author: Nicola VIGANÒ, ESRF - The European Synchrotron, Grenoble, France
"""

__author__ = """Nicola VIGANÒ"""
__email__ = "nicola.vigano@esrf.fr"


def __get_version():
    import os.path

    version_filename = os.path.join(os.path.dirname(__file__), "VERSION")
    with open(version_filename) as version_file:
        version = version_file.read().strip()
    return version


__version__ = __get_version()

# Import all definitions from main module.
from . import io_xml  # noqa: F401, F402

from . import matlab_invocation  # noqa: F401, F402

from . import utils_git  # noqa: F401, F402
from . import utils_platform  # noqa: F401, F402

from . import compile_mex_functions  # noqa: F401, F402
from . import compile_matlab_functions  # noqa: F401, F402

from . import distrib  # noqa: F401, F402

from . import batch  # noqa: F401, F402
