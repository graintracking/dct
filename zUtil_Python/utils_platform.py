# -*- coding: utf-8 -*-
"""
Platform handling classes.

Created on Jan 30, 2013.

@author: Nicola VIGANÒ, ESRF - The European Synchrotron, Grenoble, France
"""

import subprocess
import os
import re
import platform
import stat


class UtilsFactory(object):
    @staticmethod
    def getMachineDep():
        machine = platform.machine()
        system = platform.system()
        hostname = platform.node()
        if system == "Linux":
            return UtilsLinux(machine=machine, hostname=hostname)
        else:
            raise SystemError("Unsupported system: '%s' on machine: '%s'" % (system, machine))


class UtilsInterface(object):
    def __init__(self, machine, hostname):
        self.machine = machine
        self.hostname = hostname

    def getUserConfDir(self, dct_version):
        raise NotImplementedError("User's conf directory determination not available.")

    def getUserConfFile(self, dct_version, file_name):
        raise NotImplementedError("User's conf file determination not available.")

    def getSystemDescription(self):
        raise NotImplementedError("System's description not available.")

    def guessUserRealName(self):
        raise NotImplementedError("User's real name determination not available.")

    def guessUserEmail(self):
        raise NotImplementedError("User's email address determination not available.")

    def checkGitVersion(self, min_ver=[0, 0, 0, 0]):
        subobj = subprocess.Popen(["git", "--version"], stdout=subprocess.PIPE)

        if subobj.poll() > 0:
            raise SystemError("Git is not installed!")

        output = subobj.stdout.read().split()

        version = [int(piece) if piece.isdigit() else 0 for piece in output[2].split(".")]

        for indx in range(len(version)):
            if version[indx] < min_ver[indx]:
                raise SystemError("Git version: %s is too old, upgrade to at least %s" % (output[2], "%d.%d.%d.%d" % min_ver))

        return output[2]

    def getGccVersion(self):
        subobj = subprocess.Popen(["g++", "--version"], stdout=subprocess.PIPE)

        if subobj.poll() > 0:
            raise SystemError("g++ is not installed!")

        output = subobj.stdout.readline()

        regexp = re.compile("\d.\d.\d")
        match = regexp.search(output)

        version_txt = match.group()

        version = [int(piece) for piece in version_txt.split(".")]

        return (version, version_txt)

    def isGccNewerThan(self, gcc_ver=None, min_ver=[0, 0, 0]):
        if gcc_ver is None:
            (gcc_ver, _) = self.getGccVersion()
        return gcc_ver > min_ver

    def checkGccVersion(self, min_ver=[0, 0, 0]):
        (gcc_ver, gcc_ver_txt) = self.getGccVersion()

        if gcc_ver < min_ver:
            raise SystemError(
                "g++ version: %s is too old, upgrade to at least %s" % ("%d.%d.%d" % gcc_ver, "%d.%d.%d" % min_ver)
            )

        return gcc_ver_txt

    def resetMatlabMexopts(self, searchPatterns, matlabVersion, matlabPath=None):
        mexoptsfile = self._get_matlab_mexopts_path(matlabVersion)

        if os.path.exists(mexoptsfile):
            os.remove(mexoptsfile)

        if matlabPath is None:
            # Try ESRF Paths
            matlabPath = self._get_matlab_path(matlabVersion)
        self._create_matlab_mexopts(matlabVersion, matlabPath)

        stats = os.stat(mexoptsfile)
        os.chmod(mexoptsfile, stats.st_mode | stat.S_IWRITE)

        with open(mexoptsfile, "r") as f:
            lines = f.readlines()

        for searchP, subP in searchPatterns:
            for lineNum in range(len(lines)):
                m = re.search(searchP, lines[lineNum])
                if m is not None:
                    lines[lineNum] = re.sub(searchP, subP, lines[lineNum])

        with open(mexoptsfile, "w+") as f:
            f.writelines(lines)

    def isMatlabNewerThan(self, test_ver, ref_ver):
        major_test = test_ver[0:4]
        major_ref = ref_ver[0:4]
        minor_test = test_ver[4]
        minor_ref = ref_ver[4]
        if major_ref == major_test:
            return minor_test > minor_ref
        else:
            return major_test > major_ref

    def scpFile(self, filename, destination):
        return False

    def _get_matlab_mexopts_path(self, matlabVersion):
        raise NotImplementedError("Matlab's mexopts path determination not available.")

    def _create_matlab_mexopts(self, matlabVersion, matlabPath):
        raise NotImplementedError("Matlab's mexopts determination not available.")

    def _get_matlab_path(self, matlabVersion):
        raise NotImplementedError("Matlab's path determination not available.")


class UtilsLinux(UtilsInterface):
    """Linux specific utility class"""

    def getUserConfDir(self, dct_version):
        return os.path.join(os.getenv("HOME"), ".dct", dct_version)

    def getUserConfFile(self, dct_version, file_name):
        return os.path.join(os.getenv("HOME"), ".dct", dct_version, file_name)

    def getSystemDescription(self):
        return "Linux-" + self.machine

    def _get_matlab_mexopts_path(self, matlabVersion):
        userpath = os.getenv("HOME", "")
        if self.isMatlabNewerThan(matlabVersion, "2014a"):
            # Should maybe react on 32bit vs 64bit
            return os.path.join(userpath, ".matlab", ("R%s" % matlabVersion), "mex_C++_glnxa64.xml")
        else:
            return os.path.join(userpath, ".matlab", ("R%s" % matlabVersion), "mexopts.sh")

    def _create_matlab_mexopts(self, matlabVersion, matlabPath):
        cmd = [os.path.join(matlabPath, "bin", "mex"), "-setup"]
        if self.isMatlabNewerThan(matlabVersion, "2014a"):
            cmd.append("c++")

        try:
            subobj = subprocess.Popen(cmd, stdin=subprocess.PIPE, stdout=subprocess.PIPE)
            if not self.isMatlabNewerThan(matlabVersion, "2014a"):
                subobj.communicate(input="1\n")
            subobj.wait()
        except OSError as ex:
            print(ex.args)
            raise ValueError("Error with location: %s\nCheck if it exists!" % cmd[0])

    def _get_matlab_path(self, matlabVersion):
        return os.path.join("/sware", "com", "matlab_%s" % matlabVersion)

    def resetMatlabMexopts(self, matlabVersion, matlabPath=""):
        if True:
            arch_flags = ""
        else:
            if self.isGccNewerThan(min_ver=[4, 8, 99]):
                arch_flags = "-march=sandybridge"
            else:
                arch_flags = "-march=corei7-avx"

        if self.isMatlabNewerThan(matlabVersion, "2014a"):
            searchPatterns = [
                ('CXXOPTIMFLAGS="-O -DNDEBUG"', 'CXXOPTIMFLAGS="-O -DNDEBUG -fopenmp -std=c++11 %s"' % arch_flags),
                ('LDOPTIMFLAGS="-O"', 'LDOPTIMFLAGS="-O -fopenmp"'),
            ]
        else:
            searchPatterns = [
                ("COPTIMFLAGS='-O -DNDEBUG'", "COPTIMFLAGS='-O -DNDEBUG -fopenmp'"),
                ("CXXOPTIMFLAGS='-O -DNDEBUG'", "CXXOPTIMFLAGS='-O -DNDEBUG -fopenmp -std=c++11 %s'" % arch_flags),
                ('(CLIBS="\$RPATH \$MLIBS -lm)"', '\g<1> -fopenmp"'),
                ('(CXXLIBS="\$RPATH \$MLIBS -lm)"', '\g<1> -fopenmp"'),
            ]

        UtilsInterface.resetMatlabMexopts(
            self, searchPatterns=searchPatterns, matlabVersion=matlabVersion, matlabPath=matlabPath
        )

    def guessUserRealName(self):
        username = os.getenv("USER")
        cmd = ["grep", "^%s:" % username, "/etc/passwd"]
        subobj = subprocess.Popen(cmd, stdout=subprocess.PIPE)
        subobj.wait()
        cmd = ["awk", "-F:", "{print $5}"]
        subobj1 = subprocess.Popen(cmd, stdout=subprocess.PIPE, stdin=subobj.stdout)
        (name, _) = subobj1.communicate()

        if name == "":
            # Try ESRF
            possibilies = ("".join([username[0], "*", username[1:]]), username)
            for poss in possibilies:
                cmd = ["td", poss]
                subobj = subprocess.Popen(cmd, stdout=subprocess.PIPE)
                cmd = ["awk", "--field-separator= ", '{print $2 " " $1}']
                subobj1 = subprocess.Popen(cmd, stdout=subprocess.PIPE, stdin=subobj.stdout)
                (name, _) = subobj1.communicate()
                if name[0:10] != "The string":
                    name = name[:-1].lower()
                    name = name.split(" ")
                    name[0] = name[0].capitalize()
                    name[1] = name[1].capitalize()
                    return " ".join(name)
            else:
                raise SystemError("Couldn't determine user real name")
        else:
            return name[:-1]

    def guessUserEmail(self):
        username = os.getenv("USER")
        # Try ESRF
        possibilies = ("".join([username[0], "*", username[1:]]), username)
        for poss in possibilies:
            cmd = ["td", poss]
            subobj = subprocess.Popen(cmd, stdout=subprocess.PIPE)
            subobj.wait()
            name = subobj.stdout.read()
            if name[0:10] != "The string":
                name = name[:-1].lower()
                name = name.split(" ")
                name = "".join([name[-1], "@esrf.fr"])
                return name
        else:
            raise SystemError("Couldn't determine user real name")

    def scpFile(self, filename, destination):
        cmd = ["scp", filename, destination]
        return subprocess.call(cmd) == 0
