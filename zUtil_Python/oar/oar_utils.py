"""
Created on Jun 24, 2013

@author: ben
"""

from . import oar_api

import time


class OARMonitor(object):
    def __init__(self):
        self.oar_conn = oar_api.OarAPI()

    def _update_info(self):
        raise NotImplementedError("Use derived Class!")

    def _terminated(self):
        return all(job_info["state"] == "Terminated" for job_info in self.info.itervalues())

    def _terminated_correctly(self):
        return all(job_info["exit_code"] == "0 (0,0,0)" for job_info in self.info.itervalues())

    def _get_error_records(self):
        return [job_info for job_info in self.info.itervalues() if job_info["exit_code"] != "0 (0,0,0)"]

    def monitor(self, delay=10, timeout=None):
        result = None
        try:
            t = time.time()
            self._update_info()
            while not self._terminated():
                if timeout is not None and ((time.time() - t) > timeout):
                    raise RuntimeError("Reached Timeout!")
                time.sleep(delay)
                self._update_info()

            result = {"terminated_correctly": self._terminated_correctly()}
            if result["terminated_correctly"] is False:
                result.update({"error_records": self._get_error_records()})
        except KeyboardInterrupt:
            print("Terminated by user.")
        except RuntimeError as exc:
            print(exc)

        return result


class OARMonitorArray(OARMonitor):
    def __init__(self, array):
        OARMonitor.__init__(self)

        self.array = array

    def _update_info(self):
        self.info = self.oar_conn.query(array=self.array)


class OARMonitorJobs(OARMonitor):
    def __init__(self, jobs):
        OARMonitor.__init__(self)

        self.jobs = jobs

    def _update_info(self):
        self.info = self.oar_conn.query(jobs=self.jobs)
