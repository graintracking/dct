# Embedded file name: oar_api.py
"""
Created on May 22, 2013

@author: ben
"""
import subprocess

from yaml import load, load_all

try:
    from yaml import CLoader as YamlLoader
except ImportError as exc:
    print(exc)
    print("No C implementation of the YAML parser, using pure Python")
    from yaml import YamlLoader


class OarAPI(object):
    def __init__(self):
        self.backend = OarBackendCmdAPI()

    def query(self, jobs=[], array=None, full_details=True, username=""):
        """
        records = query(jobs = [], array = None, full_details = True, username = "")
        """
        result = {}
        if len(jobs) > 0:
            output = self.backend.query(jobs, None, full_details, username)
        if array is not None:
            output = self.backend.query([], array, full_details, username)
        if len(jobs) == 0 and array is None:
            output = self.backend.query([], None, full_details, username)

        output = load(output, Loader=YamlLoader)
        if output is not None:
            result.update(output)
        return result

    def delete(self, jobs=[], array=None):
        """
        delete(jobs = [], array = None)
        """
        if len(jobs) > 0 and array is not None:
            raise ValueError("OAR: it is only possible to work exclusively on jobs or array")
        elif len(jobs) == 0 and array is None:
            raise ValueError("OAR: no jobs or array specified")
        else:
            self.backend.delete(jobs, array)

    def submit(self, command_path):
        """
        job_ids = submit(command_path)
        """
        output = self.backend.submit(command_path)
        output = load_all(output, Loader=YamlLoader)
        job_ids = [elem["job_id"] for elem in output]
        return {"array_id": job_ids[0], "job_ids": job_ids}

    def resubmit(self, prev_job_id):
        """
        job_id = resubmit(prev_job_id)
        """
        return self.backend.resubmit(prev_job_id)


class OarBackendAPI(object):
    def query(self, jobs, array, full_details, username):
        raise NotImplementedError("Use one of the derived classes!")

    def delete(self, jobs, array):
        raise NotImplementedError("Use one of the derived classes!")

    def submit(self, command_path):
        raise NotImplementedError("Use one of the derived classes!")

    def resubmit(self, prev_job_id):
        raise NotImplementedError("Use one of the derived classes!")


class OarBackendCmdAPI(OarBackendAPI):
    def _call_oar(self, cmd):
        try:
            subproc = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        except OSError as exc:
            print("\nCouldn't execute command: '%s'" % " ".join(cmd))
            print(exc)
            print("\n")
            raise exc

        try:
            (stdoudata, stderrdata) = subproc.communicate()
            ret_code = subproc.wait()
        except KeyboardInterrupt as ex:
            print("User interrupted..")
            if subproc.poll() is None:
                print("Killing query..")
                subproc.kill()
            raise ex

        if ret_code != 0:
            raise ValueError("OAR: query failed with return code %d and message:\n%s" % (ret_code, stderrdata))

        return stdoudata

    def query(self, jobs, array, full_details, username):
        cmd = ["oarstat", "--yaml"]

        if full_details is True:
            cmd.append("--full")

        if username != "":
            cmd = cmd + ["--user", username]

        for job in jobs:
            cmd = cmd + ["--job", "%d" % job]

        if array is not None:
            cmd = cmd + ["--array", "%d" % array]

        result = self._call_oar(cmd)

        if result == "--- {}\n":
            raise ValueError("OAR: No jobs that satisfy the criteria")

        return result

    def delete(self, jobs, array):
        cmd = ["oardel"]

        if array is not None:
            cmd = cmd + ["--array", "%d" % array]

        cmd = cmd + ["%d" % job for job in jobs]

        return self._call_oar(cmd)

    def submit(self, command_path):
        cmd = ["oarsub", "--yaml", "-S", command_path]

        output = self._call_oar(cmd)

        result = []
        is_yaml = False
        for line in output.split("\n"):
            if line == "##########":
                is_yaml = not is_yaml
            elif is_yaml is True:
                result.append(line)

        return "\n".join(result)

    def resubmit(self, prev_job_id):
        cmd = ["oarsub", "--yaml", "--resubmit=%d" % prev_job_id]

        output = self._call_oar(cmd)
        print(output)

        import re

        matches = re.search("OAR_JOB_ID=.*", output)
        if matches is None:
            raise ValueError("Wrong output from RESUBMIT: %s" % output)

        return int(matches.group().split("=")[1])
