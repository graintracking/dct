# -*- coding: utf-8 -*-
"""
Batch processing handling base classes.

Created on May 2, 2013.

@author: Nicola VIGANÒ, ESRF - The European Synchrotron, Grenoble, France
"""

import re
import fnmatch
import os
import sys

from . import io_xml

try:
    from . import batch_gui

    can_use_batch_gui = True
except ImportError as exc:
    print(exc)
    print("Batch GUI not available (Probably: 'PyQt4' module is not available)")
    can_use_batch_gui = False

try:
    from .oar import oar_utils

    oar_available = True
except ImportError as exc:
    io_xml.DCTOutput.printWarning(exc.args)
    print("Turning 'OAR services' off")
    oar_available = False


class DCTBatchConf(io_xml.DCTXMLBase):
    @staticmethod
    def getNewFile(xml_file):
        tree = io_xml.ET.Element("batch")
        etree = io_xml.ET.ElementTree(tree)
        etree.write(xml_file)
        conf = DCTBatchConf(xml_file, read_only=False)
        conf.fix_structure()
        return conf

    def __init__(self, xml_file=os.path.join("zUtil_Conf", "batch-dev-conf.xml"), read_only=True):
        io_xml.DCTXMLBase.__init__(self, xml_file)
        self.read_only = read_only

    def fix_structure(self):
        if self.tree.find("options") is None:
            io_xml.ET.SubElement(self.tree.getroot(), "options")
        if self.tree.find("functions") is None:
            io_xml.ET.SubElement(self.tree.getroot(), "functions")

    def get_path(self, xml_path, base_dir, base_node=None):
        if base_node is None:
            node = self.tree.find(xml_path)
        else:
            node = base_node.find(xml_path)
        if node is not None:
            if node.attrib.get("absolute") == "false":
                return os.path.join(base_dir, node.text)
            else:
                return node.text
        else:
            raise ValueError("No path defined for '%s" % xml_path)

    def get_bin_dir(self, base_dir):
        return self.get_path("options/binaries/path", base_dir)

    def add_function(self, name, path, is_absolute=False):
        if self.read_only is True:
            raise ValueError("File %s is open in read only mode" % self.xml_file)

        func = self.tree.find("functions/function[@name='%s']" % name)
        if func is None:
            funcs = self.tree.find("functions")
            func = io_xml.ET.SubElement(funcs, "function", {"name": name})

        path_node = func.find("path")
        if path_node is None:
            path_node = io_xml.ET.SubElement(func, "path")

        path_node.text = path
        if is_absolute is True:
            path_node.attrib["absolute"] = "true"
        else:
            path_node.attrib["absolute"] = "false"

        self.save()

    def get_functions(self, base_dir):
        functions = {}
        nodes = self.tree.findall("functions/function")
        for node in nodes:
            functions[node.attrib.get("name")] = self.get_path("path", base_dir, base_node=node)
        return functions


class DCTBatch(object):
    @staticmethod
    def getInstanceFromArgs(args):
        dct_dir = os.path.abspath(os.path.dirname(sys.argv[0]))
        if os.path.basename(dct_dir) == "zUtil_Python":
            dct_dir = os.path.dirname(dct_dir)
        dev_mode = "--developer" in args
        return DCTBatch(dct_dir=dct_dir, dev_mode=dev_mode)

    def __init__(self, dct_dir="", dev_mode=False):
        if dct_dir == "":
            dct_dir = os.path.abspath(os.path.dirname(sys.argv[0]))
            if os.path.basename(dct_dir) == "zUtil_Python":
                dct_dir = os.path.dirname(dct_dir)
        self.dct_dir = dct_dir

        devel_conf_path = os.path.join(self.dct_dir, "zUtil_Conf", "batch-dev-conf.xml")
        try:
            self.dev_conf = DCTBatchConf(xml_file=devel_conf_path, read_only=(not dev_mode))
        except:
            self.dev_conf = DCTBatchConf.getNewFile(devel_conf_path)

        user_conf_path = os.path.join(self.dct_dir, "batch-user-conf.xml")
        try:
            self.user_conf = DCTBatchConf(xml_file=user_conf_path, read_only=False)
            # We assume system configuration is always consistent, so we only
            # check user configuration
            self.user_conf.fix_structure()
        except:
            self.user_conf = DCTBatchConf.getNewFile(user_conf_path)

    def auto_detect_functions(self, user=True):
        simple_match = "OAR_make"
        search_pattern = re.compile("[^%]*\s*" + simple_match + "\s*\(\s*'.*'")

        files_list = []
        for (root, _, file_names) in os.walk(self.dct_dir):
            for file_name in file_names:
                if fnmatch.fnmatch(file_name, "*.m"):
                    files_list.append((root, file_name))

        # First creating list of file matching methods name
        m_files = {}
        for dir_path, file_name in files_list:
            filepath = os.path.join(dir_path, file_name)
            m_files[file_name[:-2]] = filepath

        # Then looking for complex regexp inside matching files
        for dir_path, file_name in m_files.iteritems():
            filepath = os.path.join(dir_path, file_name)
            fid = open(filepath, "r")

            for line in fid:
                if simple_match in line:
                    matched = search_pattern.search(line)
                    if matched:
                        function_name = matched.group().split("'")[1]
                        self.add_function(m_files[function_name], user)

            fid.close()

    def add_function(self, path, user=True):
        name = os.path.basename(path)
        name = name[0:-2]

        # Let's make the path relative
        abs_path = os.path.abspath(path)
        abs_dct_path = os.path.abspath(self.dct_dir)
        rel_path = abs_path[len(abs_dct_path) + 1 :]

        if user is True:
            self.user_conf.add_function(name, rel_path)
        else:
            self.dev_conf.add_function(name, rel_path)

    def get_bin_dir(self):
        try:
            bin_path = self.user_conf.get_bin_dir(self.dct_dir)
        except ValueError:
            bin_path = self.dev_conf.get_bin_dir(self.dct_dir)
        return bin_path

    def get_functions(self):
        functions = self.dev_conf.get_functions(self.dct_dir)
        functions.update(self.user_conf.get_functions(self.dct_dir))
        return functions

    def print_help(self):
        launchname = os.path.basename(__file__)
        print('"%s" manages batching: "%s"' % (launchname, self.dct_dir))
        print(" Options:")
        print("  -h | --help : to show this help")
        print("  -v | --verbose : to show more detailed output")
        print("  --developer : to change structural components")
        print("  gui : to open a multi-function GUI")
        print("  monitor array <array_id> : to monitor an array and getting notifications")
        print("  monitor jobs <job_ids> : to monitor an array and getting notifications")
        print("  add_function <path_to_function> : to add some function to the batching management")
        print("  del_function <function_name> : to remove some function to the batching management")

    def execute(self, command):
        user = "--developer" not in command
        if len(command) == 0 or command[0] in ("-h", "--help"):
            self.print_help()
        elif command[0] == "monitor":
            if len(command) < 3:
                self.print_help()
            elif command[1] == "array":
                self.monitor_array(int(command[2]))
            elif command[1] == "jobs":
                raise NotImplementedError("Not implemented yet!")
            else:
                self.print_help()
        elif command[0] == "add_function":
            if len(command) < 2:
                self.print_help()
            else:
                self.add_function(command[1], user)
        elif command[0] == "gui":
            launcher = DCTBatchGuiLauncher()
            launcher.launch()
        elif command[0] == "del_function":
            raise NotImplementedError("Not implemented yet!")
        else:
            self.print_help()


class DCTBatchGuiLauncher(object):
    def __init__(self):
        if can_use_batch_gui is False:
            raise EnvironmentError("Qt is not available!")

    def launch(self):
        app = batch_gui.QtGui.QApplication([])

        self.batch_gui = batch_gui.DCTBatchGui()
        self.batch_gui.show()

        return app.exec_()


if __name__ == "__main__":
    inst = DCTBatch.getInstanceFromArgs(sys.argv)
    inst.execute(sys.argv[1:])
