# -*- coding: utf-8 -*-
"""
Batch processing handling GUI.

Created on Nov 18, 2014.

@author: Nicola VIGANÒ, ESRF - The European Synchrotron, Grenoble, France
"""

from .oar import oar_api

try:
    from PyQt4 import QtGui
    from PyQt4 import QtCore
except ImportError as ex:
    import os
    print(os.path.split(__file__)[1] + ":", ex, "Trying with PyQt5..")
    try:
        from PyQt5 import QtWidgets as QtGui
        from PyQt5 import QtCore
    except ImportError as ex1:
        print(ex1)
        raise ex1

import datetime
import os
import functools

from . import batch_oar_parameters


def convert_timestamp_to_string(tstamp):
    if tstamp is None or tstamp == 0:
        return ""
    else:
        if tstamp is not float:
            tstamp = float(tstamp)
        return datetime.datetime.fromtimestamp(tstamp).strftime("%Y-%m-%d %H:%M:%S")


class DCTBatchGui(QtGui.QWidget):

    def __init__(self, cwd=""):
        super(DCTBatchGui, self).__init__()
        self.qapi = oar_api.OarAPI()
        self.arrays = {}
        self.records = batch_oar_parameters.DCTBatchOarParameters(self)
        if cwd == "":
            cwd = os.getcwd()
        self._init_gui()
        self.set_current_cwd(cwd)
        try:
            self._pbutt_load_params()
        except EnvironmentError as exc:
            print(exc)

    def _init_gui(self):
        self.resize(1280, 768)
        self._center_gui()
        self.setWindowTitle("DCT Batch GUI")

        self.array_list = QtGui.QTreeWidget()
        # Allow to select multiple arrays/jobs
        self.array_list.setSelectionMode(QtGui.QAbstractItemView.ExtendedSelection)
        self.array_list.setColumnCount(8)
        self.array_list.setHeaderLabels(
            ["ID", "Name", "Status", "Exit code", "Submitted", "Launched", "Finished", "Resubmitted ID"]
        )
        self.array_list.header().resizeSection(0, 140)
        self.array_list.header().resizeSection(1, 140)
        self.array_list.header().resizeSection(2, 80)
        self.array_list.header().resizeSection(3, 80)
        self.array_list.header().resizeSection(4, 140)
        self.array_list.header().resizeSection(5, 140)
        self.array_list.header().resizeSection(6, 140)

        hbox_array_control = QtGui.QHBoxLayout()
        hbox_array_control.addWidget(self.array_list)

        vbox_array_control = QtGui.QVBoxLayout()
        vbox_array_control.setAlignment(QtCore.Qt.AlignTop)

        spacer = QtGui.QSpacerItem(30, 30)
        vbox_array_control.insertSpacerItem(-1, spacer)

        label = QtGui.QLabel("Array:")
        vbox_array_control.addWidget(label)

        pbutt_view_oar_params = QtGui.QPushButton("Submission")
        callback = functools.partial(self._report_error_to_user, self._pbutt_open_oar_submit_file)
        pbutt_view_oar_params.clicked.connect(callback)
        vbox_array_control.addWidget(pbutt_view_oar_params)
        pbutt_view_oar_sub = QtGui.QPushButton("Parameters")
        callback = functools.partial(self._report_error_to_user, self._pbutt_open_oar_params_file)
        pbutt_view_oar_sub.clicked.connect(callback)
        vbox_array_control.addWidget(pbutt_view_oar_sub)
        pbutt_view_oar_log = QtGui.QPushButton("Log")
        vbox_array_control.addWidget(pbutt_view_oar_log)

        spacer = QtGui.QSpacerItem(30, 30)
        vbox_array_control.insertSpacerItem(-1, spacer)

        pbutt_view_oar_aclear = QtGui.QPushButton("Clear")
        callback = functools.partial(self._report_error_to_user, self._pbutt_clear_array)
        pbutt_view_oar_aclear.clicked.connect(callback)
        vbox_array_control.addWidget(pbutt_view_oar_aclear)

        pbutt_view_oar_adel = QtGui.QPushButton("Delete")
        callback = functools.partial(self._report_error_to_user, self._pbutt_delete_array)
        pbutt_view_oar_adel.clicked.connect(callback)
        vbox_array_control.addWidget(pbutt_view_oar_adel)

        spacer = QtGui.QSpacerItem(30, 30)
        vbox_array_control.insertSpacerItem(-1, spacer)

        label = QtGui.QLabel("Job:")
        vbox_array_control.addWidget(label)

        pbutt_view_oar_inf = QtGui.QPushButton("Info")
        callback = functools.partial(self._report_error_to_user, self._pbutt_open_job_info_dialog)
        pbutt_view_oar_inf.clicked.connect(callback)
        vbox_array_control.addWidget(pbutt_view_oar_inf)
        pbutt_view_oar_out = QtGui.QPushButton("std_out")
        callback = functools.partial(self._report_error_to_user, self._pbutt_open_oar_out_file)
        pbutt_view_oar_out.clicked.connect(callback)
        vbox_array_control.addWidget(pbutt_view_oar_out)
        pbutt_view_oar_err = QtGui.QPushButton("std_err")
        callback = functools.partial(self._report_error_to_user, self._pbutt_open_oar_err_file)
        pbutt_view_oar_err.clicked.connect(callback)
        vbox_array_control.addWidget(pbutt_view_oar_err)

        spacer = QtGui.QSpacerItem(30, 30)
        vbox_array_control.insertSpacerItem(-1, spacer)

        pbutt_view_oar_resub = QtGui.QPushButton("Resubmit")
        callback = functools.partial(self._report_error_to_user, self._pbutt_resubmit_job)
        pbutt_view_oar_resub.clicked.connect(callback)
        vbox_array_control.addWidget(pbutt_view_oar_resub)

        pbutt_view_oar_jdel = QtGui.QPushButton("Delete")
        callback = functools.partial(self._report_error_to_user, self._pbutt_delete_job)
        pbutt_view_oar_jdel.clicked.connect(callback)
        vbox_array_control.addWidget(pbutt_view_oar_jdel)

        hbox_array_control.addLayout(vbox_array_control)

        hbox_cwd = QtGui.QHBoxLayout()

        cwd_label = QtGui.QLabel("Current Directory:")
        hbox_cwd.addWidget(cwd_label)
        self.edit_cwd = QtGui.QLineEdit()
        callback = functools.partial(self._report_error_to_user, self._change_cwd_from_edit)
        self.edit_cwd.editingFinished.connect(callback)
        mod_cwd_compl = QtGui.QCompleter(self)
        mod_dir = QtGui.QDirModel(mod_cwd_compl)
        mod_dir.setFilter(QtCore.QDir.NoDotAndDotDot | QtCore.QDir.Dirs)
        mod_cwd_compl.setModel(mod_dir)
        self.edit_cwd.setCompleter(mod_cwd_compl)
        hbox_cwd.addWidget(self.edit_cwd)
        self.pbutt_cwd = QtGui.QPushButton("Path")
        callback = functools.partial(self._report_error_to_user, self._change_cwd_from_butt)
        self.pbutt_cwd.clicked.connect(callback)
        hbox_cwd.addWidget(self.pbutt_cwd)

        hbox_actions = QtGui.QHBoxLayout()

        pbutt_query_oar = QtGui.QPushButton("Query OAR..")
        callback = functools.partial(self._report_error_to_user, self._pbutt_query_dialog)
        pbutt_query_oar.clicked.connect(callback)
        hbox_actions.addWidget(pbutt_query_oar)
        pbutt_load_file = QtGui.QPushButton("Load file..")
        callback = functools.partial(self._report_error_to_user, self._pbutt_load_file)
        pbutt_load_file.clicked.connect(callback)
        hbox_actions.addWidget(pbutt_load_file)
        pbutt_load_pars = QtGui.QPushButton("Load local parameters..")
        callback = functools.partial(self._report_error_to_user, self._pbutt_load_params)
        pbutt_load_pars.clicked.connect(callback)
        hbox_actions.addWidget(pbutt_load_pars)

        spacer = QtGui.QSpacerItem(30, 30)
        hbox_actions.insertSpacerItem(-1, spacer)

        self.combo_load_updt = QtGui.QComboBox(self)
        self.combo_load_updt.addItem("Arrays")
        self.combo_load_updt.addItem("Jobs")
        hbox_actions.addWidget(self.combo_load_updt)
        pbutt_load_updt = QtGui.QPushButton("Update!")
        callback = functools.partial(self._report_error_to_user, self._pbutt_update_params)
        pbutt_load_updt.clicked.connect(callback)
        hbox_actions.addWidget(pbutt_load_updt)

        vbox = QtGui.QVBoxLayout()
        vbox.addLayout(hbox_cwd)
        vbox.addLayout(hbox_actions)
        vbox.addLayout(hbox_array_control)

        self.setLayout(vbox)

    def _center_gui(self):
        qr = self.frameGeometry()
        cp = QtGui.QDesktopWidget().availableGeometry().center()
        qr.moveCenter(cp)
        self.move(qr.topLeft())

    def _change_cwd_from_edit(self):
        self.set_current_cwd(self.edit_cwd.text())

    def _change_cwd_from_butt(self):
        dir_path = QtGui.QFileDialog.getExistingDirectory(parent=self, caption="Select file")
        if not dir_path == "":
            self.set_current_cwd(dir_path)

    def _report_error_to_user(self, callback):
        try:
            callback()
        except ValueError as ex:
            dial = QtGui.QMessageBox(QtGui.QMessageBox.Information, "Error!", ex.message, buttons=QtGui.QMessageBox.Ok)
            dial.exec_()

            print("\nException information:\n")
            print(ex)

            print("\nTraceback information:\n")
            import traceback

            traceback.print_exc()

    def _pbutt_load_file(self):
        file_path = QtGui.QFileDialog.getOpenFileName(parent=self, caption="Select file", directory=self.cwd)
        if isinstance(file_path, tuple):
            file_path = file_path[0]
        if file_path and not file_path == "":
            self.load_file(file_path)

    def _pbutt_load_params(self):
        file_path = self.get_oarparams_filepath()

        if os.path.isfile(file_path):
            self.load_oar_pars_xml_file(file_path)
        else:
            raise EnvironmentError(2, "No such file: %s" % file_path)

    def _pbutt_update_params(self):
        if self.combo_load_updt.currentText() == "Arrays":
            self.records.update_content_arrays()
        else:
            self.records.update_content_jobs()
        self.update_display_arrays()

    def _pbutt_query_dialog(self):
        dial = DCTBatchOARQueryDialog(self)
        ret_code = dial.exec_()
        if ret_code == QtGui.QDialog.Rejected or dial.edit.text() == "":
            print("Not doing anything")
            return

        [sel_id, _] = int(dial.edit.text())
        if dial.combo.currentText() == "Array":
            self.records.load_content_from_arrayids_query_oar([sel_id])
        else:
            self.records.load_content_from_jobids_query_oar([sel_id])

        self.update_display_arrays()

    def _pbutt_clear_array(self):
        (array_id, job_info) = self.get_current_array_and_job()
        if array_id is not None:
            dial = QtGui.QMessageBox(
                QtGui.QMessageBox.Question,
                "Clearing",
                "Do you want to remove record of array: %d ?\n(The array will not be stopped!)" % (array_id,),
                buttons=QtGui.QMessageBox.Yes | QtGui.QMessageBox.No,
            )

            if dial.exec_() == QtGui.QMessageBox.Yes:
                # Clear from XML!
                file_path = self.get_oarparams_filepath()
                self.remove_array_from_oar_pars_xml_file(file_path, array_id)
        else:
            raise ValueError("Click on an array please!")

    def _pbutt_delete_array(self):
        (array_id, job_info) = self.get_current_array_and_job()
        if array_id is not None:
            dial = QtGui.QMessageBox(
                QtGui.QMessageBox.Question,
                "Deletion",
                "Do you want to delete array: %d ?" % (array_id,),
                buttons=QtGui.QMessageBox.Yes | QtGui.QMessageBox.No,
            )

            if dial.exec_() == QtGui.QMessageBox.Yes:
                self.qapi.delete(array=array_id)

                # Clear from XML!
                file_path = self.get_oarparams_filepath()
                self.remove_array_from_oar_pars_xml_file(file_path, array_id)
        else:
            raise ValueError("Click on an array please!")

    def _pbutt_open_oar_submit_file(self):
        (_, job_info) = self.get_current_array_and_job()
        if job_info is None:
            raise ValueError("Click on a job please!")

        filename = ".".join([self._get_base_log_file_name(job_info), "oar"])
        full_file = os.sep.join([job_info["launchingDirectory"], filename])
        dial = DCTBatchSimpleLogFileReader(self)
        dial.set_text(job_info, full_file)

    def _pbutt_open_oar_params_file(self):
        (_, job_info) = self.get_current_array_and_job()
        if job_info is None:
            raise ValueError("Click on a job please!")

        filename = ".".join([self._get_base_log_file_name(job_info), "params"])
        full_file = os.sep.join([job_info["launchingDirectory"], filename])
        dial = DCTBatchSimpleLogFileReader(self)
        dial.set_text(job_info, full_file)

    def _pbutt_open_oar_out_file(self):
        (_, job_info) = self.get_current_array_and_job()
        if job_info is None:
            raise ValueError("Click on a job please!")

        full_file = job_info["stdout_file"]
        dial = DCTBatchSimpleLogFileReader(self)
        dial.set_text(job_info, full_file)

    def _pbutt_open_oar_err_file(self):
        (_, job_info) = self.get_current_array_and_job()
        if job_info is None:
            raise ValueError("Click on a job please!")

        full_file = job_info["stderr_file"]
        dial = DCTBatchSimpleLogFileReader(self)
        dial.set_text(job_info, full_file)

    def _pbutt_open_job_info_dialog(self):
        (_, job_info) = self.get_current_array_and_job()
        if job_info is None:
            raise ValueError("Click on a job please!")

        DCTBatchDialogJobInfo(self, job_info)

    def _pbutt_resubmit_job(self):
        job_infos = self.get_selected_arrays_and_jobs()
        orig_jobs_id = []
        str_old_ids = ""
        str_new_ids = ""
        comma = ", "

        for i in range(0, len(job_infos)):
            job_info = job_infos[i][1]
            if job_info is None:
                raise ValueError("Click on a job please!")
            if i != 0 & i != len(job_infos) - 1:
                str_old_ids += comma

            orig_jobs_id.append(job_info["Job_Id"])
            str_old_ids += str(orig_jobs_id[i])

        dial = QtGui.QMessageBox(
            QtGui.QMessageBox.Question,
            "Resubmission",
            "Do you want to resubmit these jobs: %s ?" % str_old_ids,
            buttons=QtGui.QMessageBox.Yes | QtGui.QMessageBox.No,
        )

        if dial.exec_() == QtGui.QMessageBox.Yes:
            for i in range(0, len(orig_jobs_id)):
                if i != 0 & i != len(orig_jobs_id) - 1:
                    str_new_ids += comma
                new_job_id = self.qapi.resubmit(orig_jobs_id[i])
                str_new_ids += str(new_job_id)

            dial = QtGui.QMessageBox(
                QtGui.QMessageBox.Information,
                "Resubmission",
                "Job(s) %s resubmitted with id(s): %s" % (str_old_ids, str_new_ids),
            )
            dial.exec_()

            self.records.update_content_arrays()
            self.update_display_arrays()

    def _pbutt_delete_job(self):
        (array_id, job_info) = self.get_current_array_and_job()
        if job_info is None:
            raise ValueError("Click on a job please!")
        else:
            orig_job_id = job_info["Job_Id"]

            dial = QtGui.QMessageBox(
                QtGui.QMessageBox.Question,
                "Deletion",
                "Do you want to delete job: %d ?" % (orig_job_id,),
                buttons=QtGui.QMessageBox.Yes | QtGui.QMessageBox.No,
            )
            if dial.exec_() == QtGui.QMessageBox.Yes:
                self.qapi.delete(jobs=[orig_job_id])

                self.update_display_arrays()

    def _get_base_log_file_name(self, job_info):
        return "%s-%s_%d" % (job_info["name"], job_info["job_user"], job_info["resubmit_job_id"])

    def get_current_array_and_job(self):
        indexes = self.array_list.selectionModel().selectedIndexes()
        if len(indexes) > 8:  # One index obtained by selectedIndexes() contains 8 "fields" of data, but only one is necessary
            raise ValueError("Select only one job please !")
        selected = indexes[0]
        return self.records.get_current_array_and_job(selected)

    def get_selected_arrays_and_jobs(self):
        selected = self.array_list.selectionModel().selectedIndexes()
        return self.records.get_selected_arrays_and_jobs(selected)

    def load_oar_pars_xml_file(self, file_path):
        self.records.load_content_from_oarparameters(file_path)

        self.update_display_arrays()

    def load_file(self, file_path):
        with open(file_path, "r") as f:
            content = oar_api.load(f, Loader=oar_api.YamlLoader)
        self.records.load_content_from_structure(content)

        self.update_display_arrays()

    def remove_array_from_oar_pars_xml_file(self, file_path, array_id):
        if os.path.isfile(file_path):
            self.records.remove_array_from_oarparameters(file_path, array_id)

            self.update_display_arrays()
        else:
            raise EnvironmentError(2, "No such file: %s" % file_path)

    def update_display_arrays(self):
        self.array_list.clear()
        for array in self.records.arrays:
            array_item = QtGui.QTreeWidgetItem(["Array: %d" % array["array_id"], array["function"]])
            for job in array["jobs"]:
                if job["exit_code"] is None:
                    exit_code = ""
                else:
                    exit_code = "%d" % job["exit_code"]

                if job["name"] is None:
                    name = ""
                else:
                    name = job["name"]

                sub_time = convert_timestamp_to_string(job["submissionTime"])
                start_time = convert_timestamp_to_string(job["startTime"])
                stop_time = convert_timestamp_to_string(job["stopTime"])

                if job["resubmit_job_id"] == 0:
                    resub_id = ""
                else:
                    resub_id = "%d" % job["resubmit_job_id"]
                job_info = [
                    "Job: %d" % job["Job_Id"],
                    name,
                    job["state"],
                    exit_code,
                    sub_time,
                    start_time,
                    stop_time,
                    resub_id,
                ]
                if not all(isinstance(x, str) for x in job_info):
                    raise ValueError(
                        (
                            "Display Job Info: Some of the items for "
                            + "job %d couldn't be properly converted "
                            + "to strings! (OAR might have been "
                            + "updated)"
                        )
                        % job["Job_Id"]
                    )
                job_item = QtGui.QTreeWidgetItem(QtCore.QStringList(job_info))
                array_item.addChild(job_item)
            array_item.setExpanded(True)
            self.array_list.addTopLevelItem(array_item)

    def set_current_cwd(self, cwd):
        self.cwd = cwd
        self.edit_cwd.setText(cwd)

    def get_oarparams_filepath(self):
        return os.sep.join((str(self.cwd), "oarparameters.xml"))


class DCTBatchOARQueryDialog(QtGui.QDialog):
    def __init__(self, parent):
        super(QtGui.QDialog, self).__init__(parent=parent)

        hbox = QtGui.QHBoxLayout()

        self.combo = QtGui.QComboBox(self)
        self.combo.addItem("Array")
        self.combo.addItem("Job")
        hbox.addWidget(self.combo)
        label = QtGui.QLabel("ID:")
        hbox.addWidget(label)
        self.edit = QtGui.QLineEdit()
        hbox.addWidget(self.edit)
        okbutt = QtGui.QPushButton("Ok")
        okbutt.clicked.connect(self._butt_ok)
        hbox.addWidget(okbutt)

        self.setLayout(hbox)
        self.show()

    def _butt_ok(self):
        self.accept()


class DCTBatchSimpleLogFileReader(QtGui.QDialog):
    def __init__(self, parent):
        super(QtGui.QDialog, self).__init__(parent=parent)
        self.resize(800, 600)

        vbox = QtGui.QVBoxLayout()

        self.vbox_info = QtGui.QVBoxLayout()

        hbox = QtGui.QHBoxLayout()
        label = QtGui.QLabel("Function:")
        self.function = QtGui.QLineEdit()
        hbox.addWidget(label)
        hbox.addWidget(self.function)
        self.vbox_info.addLayout(hbox)

        hbox = QtGui.QHBoxLayout()
        label = QtGui.QLabel("Array ID:")
        self.array = QtGui.QLineEdit()
        hbox.addWidget(label)
        hbox.addWidget(self.array)
        label = QtGui.QLabel("Job ID:")
        self.job = QtGui.QLineEdit()
        hbox.addWidget(label)
        hbox.addWidget(self.job)
        self.vbox_info.addLayout(hbox)

        hbox = QtGui.QHBoxLayout()
        label = QtGui.QLabel("Submitted:")
        self.subtime = QtGui.QLineEdit()
        hbox.addWidget(label)
        hbox.addWidget(self.subtime)
        self.vbox_info.addLayout(hbox)

        hbox = QtGui.QHBoxLayout()
        label = QtGui.QLabel("Filename:")
        self.filename = QtGui.QLineEdit()
        hbox.addWidget(label)
        hbox.addWidget(self.filename)
        self.vbox_info.addLayout(hbox)

        self.text = QtGui.QTextBrowser()

        vbox.addLayout(self.vbox_info)
        vbox.addWidget(self.text)

        self.setLayout(vbox)
        self.show()

    def set_text(self, job_info, filename):
        self.function.setText(job_info["name"])
        self.function.setReadOnly(True)
        self.array.setText("%d" % job_info["array_id"])
        self.array.setReadOnly(True)
        self.job.setText("%d" % job_info["Job_Id"])
        self.job.setReadOnly(True)
        sub_time = convert_timestamp_to_string(job_info["submissionTime"])
        self.subtime.setText(sub_time)
        self.subtime.setReadOnly(True)

        self.filename.setText(filename)
        self.filename.setReadOnly(True)
        self.text.setText(open(filename).read())
        self.text.setReadOnly(True)


class DCTBatchDialogJobInfo(QtGui.QDialog):
    def __init__(self, parent, job_info):
        super(QtGui.QDialog, self).__init__(parent=parent)
        self.resize(800, 600)

        vbox = QtGui.QVBoxLayout()

        for (field, value) in job_info.iteritems():
            hbox = QtGui.QHBoxLayout()
            hbox.addWidget(QtGui.QLabel(field + ":"))
            edit = QtGui.QLineEdit()
            edit.setText(self.get_string_from_job_info(field, value))
            edit.setReadOnly(True)
            hbox.addWidget(edit)
            vbox.addLayout(hbox)

        self.setLayout(vbox)
        self.show()

    def get_string_from_job_info(self, field, value):
        if field in ("submissionTime", "startTime", "stopTime"):
            return convert_timestamp_to_string(value)
        elif value is None:
            return ""
        elif type(value) is int:
            return "%d" % value
        elif type(value) is dict:
            return ", ".join((self.get_string_from_job_info(x, y) for x, y in value.iteritems()))
        elif type(value) is list:
            return ", ".join((self.get_string_from_job_info(field, x) for x in value))
        elif type(value) is bool:
            if value is True:
                return "True"
            else:
                return "False"
        else:
            return value
