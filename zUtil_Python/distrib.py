# -*- coding: utf-8 -*-
"""
Install bundle creation class.

Created on Feb 4, 2013.

@author: Nicola VIGANÒ, ESRF - The European Synchrotron, Grenoble, France
"""

import os
import zipfile
import glob


class DCTMakeInstallBundle(object):

    support_files = ("zUtil_Python/dct_*.py", "zUtil_Python/__init__.py")
    install_bundle_files = ("zUtil_Python/dct_setup.py", "README.txt")

    def __init__(self, *args, **kwargs):
        object.__init__(self, *args, **kwargs)

    def run(self, dct_dir, dest_file=""):
        currentDir = os.getcwd()
        os.chdir(dct_dir)

        if dest_file == "":
            dest_file = os.path.join(dct_dir, "dct-install.zip")
        with zipfile.ZipFile(dest_file, mode="w") as zfid:
            for f in DCTMakeInstallBundle.install_bundle_files:
                n = os.path.join("dct-install", os.path.basename(f))
                print("Including file: %s as -> %s" % (f, n))
                zfid.write(f, arcname=n)
            for f_base in DCTMakeInstallBundle.support_files:
                fs = glob.glob(f_base)
                for f in fs:
                    n = os.path.join("dct-install", "dct", os.path.basename(f))
                    print("Including support file: %s as -> %s" % (f, n))
                    zfid.write(f, arcname=n)

        print("Created file: %s" % dest_file)
        os.chdir(currentDir)
