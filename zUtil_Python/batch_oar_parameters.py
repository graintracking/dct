# -*- coding: utf-8 -*-
"""
Batch processing handling datastructures.

Created on Nov 20, 2014.

@author: Nicola VIGANÒ, ESRF - The European Synchrotron, Grenoble, France
"""

try:
    from PyQt4 import QtXml
    from PyQt4 import QtCore
except ImportError as ex:
    import os
    print(os.path.split(__file__)[1] + ":", ex, "Trying with PyQt5..")
    from PyQt5 import QtXml
    from PyQt5 import QtCore


class DCTBatchOarParameters(object):

    def __init__(self, parent, stream=None):
        self.parent = parent
        self.arrays = []
        self.array_ids = {}

        if stream is not None:
            self.load_content_from_oarparameters(stream)

    def _sort_by_field(self, in_list, field):
        contents = [x[field] for x in in_list]
        indices = sorted(range(len(contents)), key=contents.__getitem__)
        return [in_list[x] for x in indices]

    def _remove_by_field_value(self, in_list, field, value):
        contents = [x[field] for x in in_list]
        ind = contents.index(value)
        return in_list[:ind] + in_list[(ind + 1) :]

    def _get_arrayids_from_xml_file(self, file_path):
        stream = QtCore.QFile(file_path)
        array_ids = []

        doc = QtXml.QDomDocument()
        doc.setContent(stream, True)
        stream.close()

        xmlroot = doc.firstChildElement()
        func_nodes = xmlroot.elementsByTagName("function")
        for func_n in range(func_nodes.count()):
            func = func_nodes.at(func_n)
            if func.isElement():
                func = func.toElement()
                array_nodes = func.elementsByTagName("array")
                for array_n in range(array_nodes.count()):
                    array = array_nodes.at(array_n)
                    if array.isElement():
                        array = array.toElement()
                        (array_id, _) = array.attribute("id").toInt()
                        array_ids.append(array_id)
        return array_ids

    def _remove_arrayid_from_xml_file(self, file_path, array_id):
        stream = QtCore.QFile(file_path)

        doc = QtXml.QDomDocument()
        doc.setContent(stream, True)
        stream.close()

        found = False
        # xmlroot = QtXml.QDomElement()
        xmlroot = doc.firstChildElement()
        func_nodes = xmlroot.elementsByTagName("function")
        for func_n in range(func_nodes.count()):
            func = func_nodes.at(func_n)
            if func.isElement():
                # func = QtXml.QDomElement()
                func = func.toElement()
                array_nodes = func.elementsByTagName("array")
                for array_n in range(array_nodes.count()):
                    array = array_nodes.at(array_n)
                    if array.isElement():
                        array = array.toElement()
                        (curr_array_id, _) = array.attribute("id").toInt()
                        if curr_array_id == array_id:
                            old_func = func
                            old_root = xmlroot
                            func.removeChild(array)
                            xmlroot.replaceChild(func, old_func)
                            doc.replaceChild(xmlroot, old_root)
                            found = True
                    if found:
                        break
            if found:
                break

        stream.open(QtCore.QIODevice.ReadWrite)
        stream.writeData(doc.toString(indent=4))
        stream.close()

    def load_content_from_arrayids_query_oar(self, array_ids):
        if array_ids is None:
            print("Not doing anything")
            return

        arrays_content = {}
        for aid in array_ids:
            print("Query Array ID: %d" % aid)
            content = self.parent.qapi.query(array=aid)
            if len(content) > 0:
                arrays_content.update(content)
            else:
                print(" - Skipping because not valid any more!")

        self.load_content_from_structure(arrays_content)

    def load_content_from_jobids_query_oar(self, jobs):
        if jobs is None:
            print("Not doing anything")
            return

        print("Query Job IDs: %s" % ", ".join(("%d" % x for x in jobs)))
        content = self.parent.qapi.query(jobs=jobs)

        self.load_content_from_structure(content)

    def load_content_from_oarparameters(self, file_path):
        array_ids = self._get_arrayids_from_xml_file(file_path)
        self.load_content_from_arrayids_query_oar(array_ids)

    def remove_array_from_oarparameters(self, file_path, array_id):
        self._remove_arrayid_from_xml_file(file_path, array_id)
        # self.arrays = self._remove_by_field_value(self.arrays, 'array_id', array_id)
        ind = self.array_ids[array_id]
        del self.arrays[ind]
        del self.array_ids[array_id]

    def load_content_from_structure(self, content):
        # Content is supposed to be in structures, where the Job ID is the main
        # thing
        jobs = [job for job in content.itervalues()]
        jobs = self._sort_by_field(jobs, "Job_Id")

        map_arrays = {}

        array_ids = (x for x in set((job["array_id"] for job in jobs)))

        for aid in array_ids:
            map_arrays[aid] = {"array_id": aid, "function": "", "jobs": []}

        for job in jobs:
            array_id = job["array_id"]
            if len(map_arrays[array_id]["jobs"]) == 0:
                map_arrays[array_id]["function"] = job["name"]
            map_arrays[array_id]["jobs"].append(job)

        for aid in array_ids:
            map_arrays[aid]["jobs"] = self._sort_by_field(map_arrays[aid]["jobs"], "Job_Id")

        self.arrays = [array for array in map_arrays.itervalues()]
        self.arrays = self._sort_by_field(self.arrays, "array_id")

        self.array_ids = {}
        for ia in range(len(self.arrays)):
            aid = self.arrays[ia]["array_id"]
            self.array_ids[aid] = ia

    def get_current_array_and_job(self, selected):
        if selected.row() == -1:
            return (None, None)

        parent = selected.parent()

        array_ids = self._get_array_ids()
        if parent.row() == -1:
            array_id = array_ids[selected.row()]
            return (array_id, None)
        else:
            array_id = array_ids[parent.row()]
            return (array_id, self.arrays[parent.row()]["jobs"][selected.row()])

    def get_selected_arrays_and_jobs(self, selected):
        list_ai = []
        for i in range(0, len(selected), 8):
            if selected[i].row() == -1:
                list_ai.append((None, None))
            else:
                parent = selected[i].parent()

                array_ids = self._get_array_ids()
                if parent.row() == -1:
                    array_id = array_ids[selected[i].row()]
                    list_ai.append((array_id, None))
                else:
                    array_id = array_ids[parent.row()]
                    list_ai.append((array_id, self.arrays[parent.row()]["jobs"][selected[i].row()]))
        return list_ai

    def update_content_arrays(self):
        array_ids = self._get_array_ids()
        self.load_content_from_arrayids_query_oar(array_ids)

    def update_content_jobs(self):
        jobs = []

        for jobs_list in self.arrays.itervalues():
            jobs.extend(job_info["Job_Id"] for job_info in jobs_list)

        self.load_content_from_jobids_query_oar(jobs)

    def _get_array_ids(self):
        return [x["array_id"] for x in self.arrays]
