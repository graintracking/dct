#!/usr/bin/python26

"""
Created on Dec 03, 2012
@author: lnervo

Based on the algorithm described in 
    Le Page and Gabe (1979) J. Appl. Cryst., 12, 464-466

and implemented in python by
    Henning Osholm Sorensen, University of Copenhagen, July 22, 2010.
"""

import sys, os, numpy as np

from xfab import *
from xfab import symmetry
from xfab.symmetry import *
from xfab.symmetry import tools

_a = float(sys.argv[1])
_b = float(sys.argv[2])
_c = float(sys.argv[3])
_alpha = float(sys.argv[4])
_beta = float(sys.argv[5])
_gamma = float(sys.argv[6])

_mintheta = float(sys.argv[7])
_maxtheta = float(sys.argv[8])
_sgno = int(sys.argv[9])
_type = sys.argv[10]
_output = sys.argv[11]

print("")
print("    *** Input data ***")
print("    lattice parameters: (%f,%f,%f,%f,%f,%f)" % (_a, _b, _c, _alpha, _beta, _gamma))
print("    sin(theta)/lambda range: %f,%f" % (_mintheta, _maxtheta))
print("    spacegroup: %s" % _sgno)
print("    type:       %s" % _type)
print("")
print("    *** Reflections list calculation ***")

if _type == "all":
    list = symmetry.tools.genhkl_all([_a, _b, _c, _alpha, _beta, _gamma], _mintheta, _maxtheta, sgno=_sgno, output_stl=True)
elif _type == "unique":
    list = symmetry.tools.genhkl_unique([_a, _b, _c, _alpha, _beta, _gamma], _mintheta, _maxtheta, sgno=_sgno, output_stl=True)
else:
    print("Type not known...Existing")
    list = None

_outputfile = _output + _type

if list is not None:
    print("Saving %d reflections generated for spacegroup %d - from symmetry.tools.genhkl_%s...\n" % (len(list), _sgno, _type))

    with file(_outputfile + ".txt", "w") as outfile:
        outfile.write("# Reflection list: {0}\n".format(list.shape))
        outfile.write("# h k l sin(theta)/lambda\n")
        np.savetxt(outfile, list, fmt=("%d", "%d", "%d", "%f"))

    print("...done!")
