# -*- coding: utf-8 -*-
"""
Matlab mex compilation module.

@author: Nicola VIGANÒ, ESRF - The European Synchrotron, Grenoble, France
"""

import fnmatch
import os
import sys
import subprocess
import string
import re

from . import io_xml
from . import utils_platform


class MexBuilder(object):

    file_extensions = ["*.c", "*.cxx", "*.cpp"]

    @staticmethod
    def getInstanceFromArgs(args, dct_dir=""):
        if dct_dir == "":
            dct_dir = os.path.abspath(os.path.dirname(args[0]))
            dct_dir = os.path.join(dct_dir, os.path.pardir)
            dct_dir = os.path.abspath(dct_dir)
        confPath = os.path.join(dct_dir, "conf.xml")
        confPath = os.path.abspath(confPath)

        args = args[1:]

        matlab_path = ""
        skip_next = False
        force_compile = False
        fresh_mexopts = False
        verbose = 1
        mfiles_to_consider = []
        unclassified_args = []
        debug = False

        for index in range(len(args)):
            if skip_next is False:
                if args[index] == "-force-compile":
                    force_compile = True
                elif args[index] == "-fresh-mexopts":
                    fresh_mexopts = True
                elif args[index] in ("--verbose", "-v"):
                    verbose = 2
                elif args[index] == "--debug":
                    debug = True
                elif args[index] == "-c":
                    skip_next = True
                    if len(sys.argv) > (index + 1):
                        mfiles_to_consider.append(args[index + 1])
                    else:
                        raise ValueError("Not enough arguments to '-c' option")
                elif args[index] == "-mp":
                    skip_next = True
                    if len(args) > (index - 1):
                        matlab_path = args[index + 1]
                    else:
                        raise ValueError("Not enough arguments for '-mp' option")
                elif args[index] in ("--help", "-h"):
                    raise ValueError("")
                else:
                    unclassified_args.append(args[index])
                    io_xml.DCTOutput.printWarning("Option '{}' not recognized. It will not be used!\n".format(args[index]))
            else:
                skip_next = False

        return MexBuilder(
            matlab_path=matlab_path,
            dct_dir=dct_dir,
            extraArgs=unclassified_args,
            force_compile=force_compile,
            verbose=verbose,
            mfiles_to_consider=mfiles_to_consider,
            fresh_mexopts=fresh_mexopts,
            debug=debug,
        )

    def __init__(
        self,
        matlab_path="",
        dct_dir=None,
        outdir=None,
        extraArgs=[],
        force_compile=False,
        verbose=1,
        mfiles_to_consider=[],
        fresh_mexopts=False,
        debug=False,
    ):
        """
         - force_compile: if true, compiles even if the file is recent
         - verbose: generates a lot of output
         - mfiles_to_consider: list mex files to compile (if empty, will compile all)
        """
        if dct_dir is None:
            dct_dir = os.getcwd()
        if outdir is None:
            outdir = os.path.join(dct_dir, "bin", "mex")
        self.dct_dir = dct_dir
        self.outdir = outdir

        self.c_files = {}
        self.excluded_c_files = {}
        self.mfiles_to_consider = mfiles_to_consider

        self.force_compile = force_compile
        self.fresh_mexopts = fresh_mexopts
        self.debug = debug

        self.out = io_xml.DCTOutput(verbose_lvl=verbose)

        self.conf = io_xml.DCTConf(dct_dir)

        self.utils = utils_platform.UtilsFactory.getMachineDep()

        if matlab_path == "":
            matlab_path = self.conf.get_matlab_path(self.utils.hostname)
        self.matlab_dir = matlab_path

        try:
            self.mex_info = self.conf.get_mex_files()
        except:
            io_xml.DCTOutput.printWarning("Couldn't get mex files info. Using plain config.")
            self.mex_info = None

    def print_help(self):
        launchname = os.path.basename(__file__)
        print('"%s" builds your mex files in the DCT dir: "%s"' % (launchname, self.dct_dir))
        print(" Options:")
        print("  -h | --help : to show this help")
        print("  -v | --verbose : to show more detailed output")
        print("  --debug : to compile with debug symbols and no optimization")
        print("  -c <filename> : to compile only a specific file (wildcards also apply)")
        print("  -mp <matlab_dir> : to specify matlab's directory")
        print("  -force-compile : to force the compilation even if not needed")
        print("  -fresh-mexopts : to force the resetting of mexopts file")

    def is_mex_excluded(self, file_path):
        if self.mex_info is not None:
            return self.mex_info.is_excluded(file_path, self.dct_dir)
        return False

    def find_mex_files(self):
        """
        Finds the mex files (well, the C and Cxx sources) and saves the files into the object
        """
        self.out.printSubJob("Finding mex files, from: '%s'" % self.dct_dir)
        for (root, _, file_names) in os.walk(self.dct_dir):
            for file_extension in self.file_extensions:
                for file_name in file_names:
                    if fnmatch.fnmatch(file_name, file_extension):
                        filepath = os.path.join(root, file_name)
                        if self.is_mex_excluded(filepath):
                            self.excluded_c_files[file_name] = filepath
                        else:
                            self.c_files[file_name] = filepath
        self.out.printSubJob("These files were found, but excluded:")
        for file_name in self.excluded_c_files:
            self.out.printSubSubJob("File", file_name + " ( " + self.excluded_c_files[file_name] + " )")
        self.out.printSubJob("These files were found, and they can be compiled:")
        for file_name in self.c_files:
            self.out.printSubSubJob("File", file_name + " ( " + self.c_files[file_name] + " )")
        print("")

    def _compile_mex_file(self, file_name):
        """
        Tries to compile the given file.
        """
        file_path = self.c_files[file_name]

        compiler_path = os.path.join(self.matlab_dir, "bin", "mex")
        cmd = [compiler_path, file_path, "-outdir", self.outdir]

        includes = [os.path.join(self.dct_dir, "zUtil_Cxx", "include")]
        lib_paths = []
        libs = []
        defines = []
        options = []

        if self.mex_info is not None:
            props = self.mex_info.get_file_properties(file_name)
            if props is None:
                props = self.mex_info.get_file_properties(file_path)
            if props is not None:
                # No info at all into the xml file
                includes = includes + props.get("includes")
                lib_paths = lib_paths + props.get("lib_paths")
                libs = libs + props.get("libs")
                defines = defines + props.get("defines")
                options = options + props.get("options")

        if self.out.verbose_lvl > 1:
            cmd.append("-v")
        if self.debug is True:
            cmd.append("-g")
            cmd.append("-DDEBUG")
        for include in includes:
            cmd.append("-I" + include)
        for lib_path in lib_paths:
            cmd.append("-L" + lib_path)
        for lib in libs:
            cmd.append("-l" + lib)
        for define in defines:
            cmd.append("-D" + define)
        for option in options:
            cmd.append(option)
        if "-compatibleArrayDims" not in options:
            cmd.append("-largeArrayDims")
        cmd.append("-lut")
        #         cmd.append("-largeArrayDims") # This unfortunately conflicts with mym.cpp
        self.out.printSubSubJob("File", cmd[1] + " (cmd: " + " ".join(cmd) + " )")
        return subprocess.call(cmd)

    def compile_mex_files(self):
        """
        Compiles the mex files from the list.
        """

        if self.fresh_mexopts is True:
            self.out.printSubJob("Resetting mexopts file")
            utils = utils_platform.UtilsFactory.getMachineDep()
            utils.resetMatlabMexopts(self.conf.get_matlab_version(), self.conf.get_matlab_path())

        errors_list = []

        if not os.path.exists(self.outdir):
            self.out.printSubJob("Creating mex directory")
            os.makedirs(self.outdir)
        self.out.printSubJob("Compiling mex files:")

        if (len(self.mfiles_to_consider) == 0) or (
            (len(self.mfiles_to_consider) == 1) and (self.mfiles_to_consider[0] == "all")
        ):
            # No preferences, so let's compile everything
            files_to_compile = self.c_files
        else:
            # Specific files
            files_to_compile = self.mfiles_to_consider

        # Now let's compile
        for file_name in files_to_compile:
            # Check if it is an actual file or a regular expression
            if file_name in self.c_files:
                match_list = [file_name]
            else:
                match_list = []
                for cfile in self.c_files:
                    if re.search(file_name, cfile) is not None:
                        match_list.append(cfile)
                if len(match_list) == 0:
                    error_msg = "No file corresponds to: %s" % file_name
                    io_xml.DCTOutput.printError(error_msg)
                    errors_list.append(error_msg)

            # Let's compile each match
            for cfile in match_list:
                ret_val = self._compile_mex_file(cfile)
                if ret_val != 0:
                    io_xml.DCTOutput.printError("Failed!")
                    errors_list.append("Error compiling MEX: %s (Return Value: %d)" % (file_name, ret_val))
        print("")
        if len(errors_list) > 0:
            print("Errors happened during compilation:")
            for error_msg in errors_list:
                io_xml.DCTOutput.printError("- %s" % error_msg)


if __name__ == "__main__":
    try:
        mex_builder = MexBuilder.getInstanceFromArgs(sys.argv)
        mex_builder.find_mex_files()
        mex_builder.compile_mex_files()
    except ValueError as ex:
        if (len(ex.args) > 0) and (ex.args[0] != ""):
            io_xml.DCTOutput.printError(ex.args)
        MexBuilder("").print_help()
    except BaseException as ex:
        io_xml.DCTOutput.printError(ex.args)
        exit(1)
