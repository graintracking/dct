# -*- coding: utf-8 -*-
"""
Matlab function compilation module.

@author: Nicola VIGANÒ, ESRF - The European Synchrotron, Grenoble, France
"""

import os
import sys

from . import io_xml
from . import matlab_invocation


class FunctionsBuilder(object):
    @staticmethod
    def getInstanceFromArgs(args, dct_dir=""):
        if dct_dir == "":
            dct_dir = os.path.abspath(os.path.dirname(args[0]))
            dct_dir = os.path.join(dct_dir, os.path.pardir)
            dct_dir = os.path.abspath(dct_dir)

        args = args[1:]

        conf = io_xml.DCTConf(dct_dir)
        matlab_path = conf.get_matlab_path()

        skip_next = False
        will_compile = True
        will_generate = True
        force_compile = False
        scan_sources = False
        verbose = 1

        mfiles_to_consider = []
        for index in range(len(args)):
            if skip_next is False:
                if args[index] == "-just-generate":
                    will_compile = False
                    will_generate = True
                elif args[index] == "-just-compile":
                    will_compile = True
                    will_generate = False
                elif args[index] == "-force-compile":
                    will_compile = True
                    force_compile = True
                elif args[index] == "-scan-sources":
                    scan_sources = True
                elif args[index] == "-c":
                    skip_next = True
                    if len(args) > (index - 1):
                        mfiles_to_consider.append(args[index + 1])
                    else:
                        raise ValueError("Not enough arguments for '-c' option")
                elif args[index] in ("--help", "-h"):
                    raise ValueError("")
                elif args[index] in ("--verbose", "-v"):
                    verbose = 2
                elif args[index] == "-mp":
                    skip_next = True
                    if len(args) > (index - 1):
                        matlab_path = args[index + 1]
                    else:
                        raise ValueError("Not enough arguments for '-mp' option")
                else:
                    io_xml.DCTOutput.printWarning("Unknown argument (at position %d): %s" % (index, args[index]))
            else:
                skip_next = False

        return FunctionsBuilder(
            matlab_path=matlab_path,
            dct_dir=dct_dir,
            force_compile=force_compile,
            mfiles_to_consider=mfiles_to_consider,
            do_compile=will_compile,
            do_generate=will_generate,
            scan_sources=scan_sources,
            verbose_lvl=verbose,
        )

    def __init__(
        self,
        matlab_path,
        dct_dir=os.getcwd(),
        script_dir=None,
        bin_dir=None,
        force_compile=False,
        mfiles_to_consider=[],
        do_compile=True,
        do_generate=True,
        scan_sources=False,
        verbose_lvl=1,
    ):
        self.dct_dir = dct_dir
        if script_dir is None:
            script_dir = os.path.join(self.dct_dir, "bin", "scripts")
        if bin_dir is None:
            bin_dir = os.path.join(self.dct_dir, "bin", "compiled")

        self.script_dir = script_dir
        self.bin_dir = bin_dir

        self.matlab_dir = matlab_path
        self.force_compile = force_compile
        self.mfiles_to_consider = mfiles_to_consider

        self.do_compile = do_compile
        self.do_generate = do_generate
        self.scan_sources = scan_sources

        self.compile_records = {}

        self.out = io_xml.DCTOutput(verbose_lvl=verbose_lvl)

    def print_help(self):
        launchname = os.path.basename(__file__)
        print('"%s" builds your mex files in the DCT dir: "%s"' % (launchname, self.dct_dir))
        print(" Options:")
        print("  -h | --help : to show this help")
        print("  -v | --verbose : to show more detailed output")
        print("  -mp <matlab_dir> : to specify matlab's directory")
        print("  -c <filename> : to compile only a specific matlab function")
        print("  -force-compile : to force the compilation even if not needed")
        print("  -just-compile : to skip the generation of the script")
        print("  -just-generate : to skip the actual compilation")
        print("  -scan-sources : to scan the matlab sources")

    def _buildFuncsList(self):
        from . import batch

        batch = batch.DCTBatch(self.dct_dir)
        if self.scan_sources is True:
            self.out.printSubJob("Finding functions that may be compiled, from: '%s'" % self.dct_dir)
            batch.auto_detect_functions()
        self.out.printSubJob("Functions available for compilation:")
        funcs = batch.get_functions()
        for function in funcs:
            self.out.printSubSubJob(function, funcs[function])
        bin_dir = batch.get_bin_dir()
        self.out.printSubJob("Adding information about functions..")
        for function in funcs:
            outpath = os.path.join(bin_dir, function)
            self.compile_records[function] = (outpath, funcs[function])

    def _generateMCompileFile(self):
        if not os.path.exists(self.script_dir):
            self.out.printSubJob("Creating script directory: '%s'" % self.script_dir)
            os.makedirs(self.script_dir)

        self.out.printSubJob("Generating m-file that will compile those Functions..")
        script_content = ["function varargout = compile(force, doCompile, varargin)\n\n", "mfiles = [];\n"]
        for func in self.compile_records:
            out_file = self.compile_records[func][0]
            in_mfile = self.compile_records[func][1]
            script_content.append("mfiles.('%s') = struct( ...\n" % func)
            script_content.append("    'out_file', '%s', ...\n" % out_file)
            script_content.append("    'in_mfile', '%s' );\n" % in_mfile)
        script_content.append("\nif (nargout > 0)\n    varargout{1} = mfiles;\nend\n")
        script_content.append(
            "\nif (doCompile)\n    gtCompileFunctions(force, mfiles, varargin)\nend\n\nend % end of function\n"
        )

        script_loc = os.path.join(self.script_dir, "compile.m")
        self.out.printSubJob("Writing '%s' script to disk.." % script_loc)
        try:
            fid = open(script_loc, "w")
            fid.writelines(script_content)
            fid.close()
        except IOError as ex:
            io_xml.DCTOutput.printWarning("Impossible to write script file: '%s'" % script_loc)
            print(ex)

    def compileFuncs(self):
        if not os.path.exists(self.bin_dir):
            self.out.printSubJob("Creating binaries directory")
            os.makedirs(self.bin_dir)
        self.out.printSubJob("Calling matlab..")

        extra_args = ["-nodesktop", "-nosplash"]

        if self.force_compile is True:
            compile_args = "'force'"
        else:
            compile_args = "'no-force'"
        compile_args = compile_args + ",true"
        if len(self.mfiles_to_consider) > 0:
            compile_args = compile_args + ",'" + "','".join(self.mfiles_to_consider) + "'"

        cmd = "compile(%s);quit;" % compile_args

        invoker = matlab_invocation.DCTMatlabInvocation(
            dct_dir=self.dct_dir,
            work_dir=self.script_dir,
            extra_args=extra_args,
            extra_cmds=cmd,
            ctrl_c_kill=True,
            skip_functions_check=True,
        )
        invoker.invoke()

        print("")

    def generateMfile(self):
        self._buildFuncsList()
        self._generateMCompileFile()

    def buildMatlabFunctions(self):
        self.generateMfile()
        self.compileFuncs()


if __name__ == "__main__":
    func_builder = FunctionsBuilder.getInstanceFromArgs(sys.argv)

    if func_builder.do_generate is True:
        func_builder.generateMfile()
    if func_builder.do_compile is True:
        func_builder.compileFuncs()
