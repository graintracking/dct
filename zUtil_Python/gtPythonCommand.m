function [status, msg, ver] = gtPythonCommand(args, use_fable, use_exec)
% GTPYTHONCOMMAND  Calls python with the proper command
%
%                  If your python script can't be found although it should
%                  be known by the system, try full path and remove trailing 
%                  newline char:
%
%                  [~, path_to_cmd] = system('which script.py');
%                  path_to_cmd = path_to_cmd(1:end-1);
%
%
%     [status, msg, ver] = gtPythonCommand(args, [use_fable], [use_exec])
%     -------------------------------------------------------------------
%     INPUT:
%       args      = <string>    Arguments passed to the python invocation
%       use_fable = <logical>   Source fable configuration file before {false}
%       use_exec  = <logical>   Use the python command before the script {true}
%
%     OUTPUT:
%       status    = <int>       System command return code
%       msg       = <string>    Python command output
%       ver       = <int>       Version of python
%
%     Version 002 08-11-2013 by LNervo
%       Added option to use or not the python executable
%
%     Version 001 08-02-2013 by YGuilhem


% Default paramater value
if ~exist('use_fable', 'var') || isempty(use_fable)
    use_fable = false;
end
if ~exist('use_exec', 'var') || isempty(use_exec)
    use_exec = true;
end

pythonExes = {'python', 'python26'};

% Loop over each python executables
ok = false;
for ii=1:length(pythonExes)
    pythonExe = pythonExes{ii};
    [status, msg] = system([pythonExe ' -V']);
    if ~status
        version = str2num(strrep(strrep(msg, 'Python ', ''), '.', ' '));
        if version(1) >= 2 && version(2) >= 6
            ok = true;
            break;
        end
    end
end

if ~ok
    gtError('gtPythonCommand:old_python_version', ...
        'Cannot find python > 2.6 on this machine!');
end

% Add fable settings if needed
fableSet = [];
if use_fable
    fableConfig = '/sware/exp/fable/bin/fable.bash';
    fableSet = ['[ -f ' fableConfig ' ] && . ' fableConfig ' >& /dev/null ; '];
end

% Run command
if ~use_exec
    pythonExe = '';
end
[status, msg] = system([fableSet pythonExe ' ' args]);

% Add version output if asked
if nargout > 2
    ver = version;
end

end % end of function
