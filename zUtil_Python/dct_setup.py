# -*- coding: utf-8 -*-
"""
Setup class.

Created on Jan 30, 2013.

@author: Nicola VIGANÒ, ESRF - The European Synchrotron, Grenoble, France
"""

import sys

from dct import utils_platform
from dct import utils_git

from dct.io_xml import DCTOutput, DCTConfUpdater, DCTConf
from dct.compile_mex_functions import MexBuilder
from dct.compile_matlab_functions import FunctionsBuilder


class DCTSetup(object):
    def __init__(self, args):
        self.verbose = 1
        self.initialized = False
        self.branch = ""
        self.repo_dir = ""
        self.sf_username = ""
        self.matlab_ver = ""
        self.matlab_dir = ""
        self.default_matlab_ver = "2013a"

        self.gcc_version = ""
        self.git_version = ""
        self.system_type = ""

        skip_next = False
        unclassified_args = []

        try:
            for index in range(len(args)):
                if skip_next is False:
                    if args[index] == "--verbose" or args[index] == "-v":
                        self.verbose = 2
                    elif args[index] == "-b":
                        skip_next = True
                        if len(args) > (index + 1):
                            self.branch = args[index + 1]
                        else:
                            raise ValueError("Not enough arguments to '-b' option")
                    elif args[index] == "-d":
                        skip_next = True
                        if len(args) > (index + 1):
                            self.repo_dir = args[index + 1]
                        else:
                            raise ValueError("Not enough arguments to '-d' option")
                    elif args[index] == "-u":
                        skip_next = True
                        if len(args) > (index + 1):
                            self.sf_username = args[index + 1]
                        else:
                            raise ValueError("Not enough arguments to '-u' option")
                    elif args[index] == "-m":
                        skip_next = True
                        if len(args) > (index + 1):
                            self.matlab_ver = args[index + 1]
                        else:
                            raise ValueError("Not enough arguments to '-m' option")
                    elif args[index] == "-mp":
                        skip_next = True
                        if len(args) > (index + 1):
                            self.matlab_dir = args[index + 1]
                        else:
                            raise ValueError("Not enough arguments to '-mp' option")
                    elif args[index] == "--help" or args[index] == "-h":
                        raise ValueError("")
                    else:
                        unclassified_args.append(args[index])
                else:
                    skip_next = False

            if self.branch == "":
                self.branch = "master"

            if self.repo_dir == "":
                if self.branch == "master":
                    self.repo_dir = "dct"
                else:
                    stable_ver = self.branch.split("_")
                    stable_ver = stable_ver[1]
                    self.repo_dir = "_".join(["dct", stable_ver])

            self.initialized = True
            self.out = DCTOutput(self.verbose)

            for arg in unclassified_args:
                DCTOutput.printWarning("Unused argument: %s" % arg)
        except ValueError as ex:
            if (len(ex.args) > 0) and (ex.args[0] != ""):
                DCTOutput.printError(ex.args)
            self.print_help()

    def print_help(self):
        try:
            branches = utils_git.DCTGit.getAvailableRemoteBranches()
        except ValueError as ex:
            self.out.printWarning(ex.args)
            branches = []
        print("Installs DCT for both users and developers")
        print(" Options:")
        print("  -h | --help : to show this help")
        print("  -v | --verbose : to show more detailed output")
        print("  -b <branch_name> : to select a specific branch from the listed here:")
        for b in branches:
            print("     %s" % b)
        print("  -d <dir_name> : to specify directory name, where dct will be")
        print("  -u <user_name> : selects developer mode and specifies the developer sf_username")
        print("  -m <matlab_version> : selects the matlab version to be used")
        print("  -mp <matlab_dir> : selects the path to matlab (not needed at ESRF)")

    def printRepoSummary(self):
        print("\nGit repository information:")

        if self.sf_username != "":
            self.out.printInfoRecord("Mode", "Developer")
            self.out.printInfoRecord("Sourceforge Username", self.sf_username)
            self.out.printInfoRecord("User Real Name", self.user_real_name)
            self.out.printInfoRecord("User e-mail", self.user_email)
        else:
            self.out.printInfoRecord("Mode", "User")

        self.out.printInfoRecord("Branch", self.branch)
        self.out.printInfoRecord("DCT Dir", self.repo_dir)

    def printSystemSummary(self):
        chosen_matlab_ver = self.matlab_ver
        if chosen_matlab_ver == "":
            chosen_matlab_ver = "defaulted to -> %s" % self.default_matlab_ver
        print("\nSystem information:")
        self.out.printInfoRecord("System Description", self.system_type)
        self.out.printInfoRecord("Matlab Version", chosen_matlab_ver)
        self.out.printInfoRecord("Gcc Version", self.gcc_version)
        self.out.printInfoRecord("Git Version", self.git_version)

    def checkSystem(self, platform_utils):
        self.git_version = platform_utils.checkGitVersion()
        #        platform_utils.checkSvnServers()
        self.gcc_version = platform_utils.checkGccVersion()

    def prepareUserEnvironment(self, proxy=""):
        if proxy != "":
            utils_git.DCTGit.setGlobalConfig("http.proxy", proxy)

    def prepareDeveloperEnvironment(self, utils):
        try:
            self.user_real_name = utils_git.DCTGit.getGlobalConfig("user.name")
        except ValueError as ex:
            DCTOutput.printWarning(ex.args)
            self.out.printSubJob("Trying to guess the Real Name of the user")
            self.user_real_name = utils.guessUserRealName()
            utils_git.DCTGit.setGlobalConfig("user.name", self.user_real_name)
        try:
            self.user_email = utils_git.DCTGit.getGlobalConfig("user.email")
        except ValueError as ex:
            DCTOutput.printWarning(ex.args)
            self.out.printSubJob("Trying to guess the e-mail of the user")
            self.user_email = utils.guessUserEmail()
            utils_git.DCTGit.setGlobalConfig("user.email", self.user_email)


if __name__ == "__main__":
    dct_sup = DCTSetup(sys.argv[1:])
    if dct_sup.initialized is False:
        sys.exit(1)

    utils = utils_platform.UtilsFactory.getMachineDep()
    dct_sup.system_type = utils.getSystemDescription()

    dct_sup.out.printJob("Checking system...")
    try:
        dct_sup.checkSystem(utils)
        dct_sup.prepareUserEnvironment("proxy.esrf.fr:3128")  # assuming ESRF
        if dct_sup.sf_username != "":
            dct_sup.prepareDeveloperEnvironment(utils)
    except ValueError as ex:
        DCTOutput.printError(ex.args)
        sys.exit(1)
    dct_sup.out.printJob("Done.")

    dct_sup.printSystemSummary()
    dct_sup.printRepoSummary()
    print("")

    dct_sup.out.printJob("Installing...")
    utils_git = utils_git.DCTGit()

    dct_sup.out.printSubJob("Cloning repository...")
    if dct_sup.sf_username != "":
        retcode = utils_git.createDeveloperRepo(username=dct_sup.sf_username, dest_dir=dct_sup.repo_dir, branch=dct_sup.branch)
        utils_git.setRepoConfig("branch.master.rebase", "true")
    else:
        retcode = utils_git.createUserRepo(dest_dir=dct_sup.repo_dir, branch=dct_sup.branch)
    if retcode != 0:
        dct_sup.out.printSubJob("Directory exists. Checking it is a git repository, and updating...")
        utils_git.updateRepo()
    dct_sup.out.printJob("Done.")
    print("")

    dct_sup.out.printJob("Initializing configuration files..")
    empty_mdir = dct_sup.matlab_dir == ""
    empty_mver = dct_sup.matlab_ver == ""

    if not empty_mdir and not empty_mver:
        dct_sup.out.printSubJob(
            "Using user specified matlab, this forces a local conf.xml to be created, which will override the defaults..."
        )
        upd = DCTConfUpdater(utils_git.repo_dir)
        confexamplefile = utils_git.getFilePathInRepo("zUtil_Conf", "conf.default.xml")
        upd.safely_install_new_file(confexamplefile, "conf.xml")
        upd.update_xml_conf_file("matlab/path", dct_sup.matlab_dir, "conf.xml")
        upd.update_xml_conf_file("matlab/version", dct_sup.matlab_ver, "conf.xml")
    if empty_mdir and empty_mver:
        dct_sup.matlab_ver = dct_sup.default_matlab_ver

    dct_conf = DCTConf(utils_git.repo_dir)

    if dct_sup.matlab_dir == "":
        dct_sup.matlab_dir = dct_conf.get_matlab_path()

    try:
        utils.resetMatlabMexopts(matlabVersion=dct_sup.matlab_ver, matlabPath=dct_sup.matlab_dir)
    except ValueError:
        if empty_mdir != empty_mver:
            DCTOutput.printError(
                "If you want to use a specific matlab version, you have to set both matlab version and path!"
                + " (matlab_dir: '%s', matlab_ver: '%s')" % (dct_sup.matlab_dir, dct_sup.matlab_ver)
            )
        else:
            raise

    dct_sup.out.printJob("Done.")
    print("")

    dct_sup.out.printJob("Compiling MEX files..")
    mex_builder = MexBuilder(
        matlab_path=dct_sup.matlab_dir,
        dct_dir=utils_git.repo_dir,
        mfiles_to_consider=[],
        force_compile=True,
        verbose=dct_sup.verbose,
    )
    mex_builder.find_mex_files()
    mex_builder.compile_mex_files()
    dct_sup.out.printJob("Done.")
    print("")

    dct_sup.out.printJob("Compiling batch functions... (may take several minutes)")
    func_builder = FunctionsBuilder(
        dct_dir=utils_git.repo_dir, matlab_path=dct_sup.matlab_dir, mfiles_to_consider=[], force_compile=True
    )
    func_builder.buildMatlabFunctions()
    dct_sup.out.printJob("Done.")
    print("")

    dct_sup.out.printJob("DCT is now Installed in: '%s'" % utils_git.repo_dir)
