#!/usr/bin/python26

"""
Created on Dec 10, 2012
@author: lnervo

Based on the algorithm described in 
    Le Page and Gabe (1979) J. Appl. Cryst., 12, 464-466

and implemented in python by
    Henning Osholm Sorensen, University of Copenhagen, July 22, 2010.
"""

import sys, numpy as np

from xfab import *
from xfab import symmetry
from xfab.symmetry import *
from xfab.symmetry import tools
from xfab import sg

_sgno = int(sys.argv[1])
_crystalsystem = sys.argv[2]

print("")
print("    *** Input data ***")
print("    spacegroup    : %d" % _sgno)

if _sgno >= 0 and _sgno <= 230:
    spg = sg.sg(sgno=_sgno)

    # number of symmetry operators
    nsymop = spg.nsymop
    # crystal system
    if _crystalsystem is not None:
        crystal_system = _crystalsystem
    else:
        crystal_system = spg.crystal_system

    print("    crystal system: %s" % crystal_system)
    print("")

    if crystal_system == "triclinic":
        nsystem = 1
    elif crystal_system == "monoclinic":
        nsystem = 2
    elif crystal_system == "orthorhombic":
        nsystem = 3
    elif crystal_system == "tetragonal":
        nsystem = 4
    elif crystal_system == "trigonal":
        nsystem = 5
    elif crystal_system == "hexagonal":
        nsystem = 6
    elif crystal_system == "cubic":
        nsystem = 7
    else:
        raise ValueError, "The crystal system is not know"

    print("Found %d symmetry operators for crystal system %s\n" % (nsymop, crystal_system))

    # rotations list
    rot = spg.rot
    # trans info
    trans = spg.trans

    if rot is not None:
        print("Saving %d rotations for crystal system '%s' - from sg.sg(sgno=%d).rot\n" % (len(rot), crystal_system, _sgno))

        with file("sg_rot_%s.txt" % crystal_system, "w") as outfile:
            outfile.write("# Rotations list: {0}\n".format(rot.shape))

            for data_rot in rot:
                np.savetxt(outfile, data_rot, fmt="%5.3f")

                outfile.write("# New rotation\n")

        print("...done!")

    if trans is not None:
        print("Saving %d trans for crystal system '%s' - from sg.sg(sgno=%d).trans\n" % (len(trans), crystal_system, _sgno))

        with file("sg_trans_%s.txt" % crystal_system, "w") as outfile:
            outfile.write("# Trans list: {0}\n".format(trans.shape))
            np.savetxt(outfile, trans, fmt=("%f", "%f", "%f"))

            outfile.write("# New trans\n")

        print("...done!")

    # calculation of rotations
    calc_rot = symmetry.rotations(nsystem)
    # calc_rot_nf = calc_rot.reshape( calc_rot.shape[0]*calc_rot.shape[1], calc_rot.shape[2])

    if calc_rot is not None:
        print(
            "Saving %d rotations for crystal system '%s' - from symmetry.rotations(%d)\n"
            % (len(calc_rot), crystal_system, nsystem)
        )

        with file("rot_%s.txt" % crystal_system, "w") as outfile:
            outfile.write("# Rotations list: {0}\n".format(calc_rot.shape))
            for data_rot in calc_rot:
                np.savetxt(outfile, data_rot, fmt="%5.3f")
                outfile.write("# New rotation\n")

        print("...done!")

    # calculation of permutations
    calc_perm = symmetry.permutations(nsystem)

    if calc_perm is not None:
        print(
            "Saving %d permutations for crystal system '%s' - from symmetry.permutations(%d)\n"
            % (len(calc_perm), crystal_system, nsystem)
        )

        with file("perm_%s.txt" % crystal_system, "w") as outfile:
            outfile.write("# Permutations list: {0}\n".format(calc_perm.shape))
            for data_perm in calc_perm:
                np.savetxt(outfile, data_perm, fmt="%5.3f")
                outfile.write("# New permutation\n")

        print("...done!")

else:
    raise ValueError, "Space group information is wrong"
