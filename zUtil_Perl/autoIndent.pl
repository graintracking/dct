#!/usr/bin/perl

sub doIndent
{
  local ($howMuch, $spaces, $newLine);
  $newLine = $_[0];
  $howMuch = $_[1];
  $spaces = "    " x $howMuch;
  $newLine =~ s/^(\s*)/$spaces/;
  return $newLine;
}

sub checkArray
{
  for ($elem = 1; $elem <= $#_; $elem++) {
    local ($testElem);
    $testElem = $_[$elem];
    if ($_[0] =~ /^(\s*)$testElem\s/) {
      return $testElem;
    }
  }
  return "";
}

@indentRaisers = ("if", "for", "while", "try", "function", "switch");
@indentKeepers = ("else", "elseif", "catch", "case", "otherwise");
@indentLowers  = ("end");

foreach $file (@ARGV) {
  print "Autoindent file: $file\n";

  open(INFO, $file);
  @lines = <INFO>;
  close(INFO);

  $indent = 0;
  $isContinuedLine = 0;
  $isPostRaiser = 0;

  foreach $line (@lines) {
    if ($isContinuedLine) {
      $line = &doIndent($line, $indent+2);
      $isContinuedLine = 0;
    } elsif ($line =~ /^\s*$/) {
      if ($isPostRaiser) {
        $line = "";
      } else {
        $line = "\n";
      }
    } elsif ($isPostRaiser and $isPostRaiser =~ /function/ and $line =~ /^(\s*)%/) {
      $line = &doIndent($line, $indent-1);
    } elsif ($isPostRaiser = &checkArray($line, @indentRaisers)) {
      $line = &doIndent($line, $indent++);
    } elsif (&checkArray($line, @indentLowers)) {
      $line = &doIndent($line, --$indent);
      $isPostRaiser = 0;
    } elsif (&checkArray($line, @indentKeepers)) {
      $line = &doIndent($line, $indent-1);
      $isPostRaiser = 0;
    } else {
      $line = &doIndent($line, $indent);
      $isPostRaiser = 0;
    }

    $copyOfLine = $line;
    $copyOfLine =~ s/\'(.*?)\'//g;
    $copyOfLine =~ s/%(.*)$//;

    if ($copyOfLine =~ /(.*)\.\.\./)
    {
      $isContinuedLine = !0;
    }
  } # Finished auto indentation

  if ($indent == 1) {
    print "Found non totally reentrant indentation, did you miss a 'end'?\n";
    #if ($ARGV[1] =~ /-add-end/) {
      push(@lines, "end % end of function\n");
    #}
  }

  # Reduce number of multiple blank lines
  $isBlanckLine = 0;
  foreach $line (@lines) {
    if ($line =~ /^\s*$/) {
      if ($isBlanckLine) {
        $line = "";
      } else {
        $isBlanckLine = !0;
      }
    } else {
      $isBlanckLine = 0;
    }
    #print $line;
  }

  $fileOut = $file;
  $fileOut =~ s/\.(\w*)$/_ai1\.$1/;
  print "Output filename: $fileOut\n";
  open(INFO, ">$fileOut");
  print INFO @lines;
  close(INFO);
}

