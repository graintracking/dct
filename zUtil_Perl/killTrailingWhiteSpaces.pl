#!/usr/bin/perl

foreach $file (@ARGV) {
	print "Kill trailing whitespaces in file: $file\n";

	open(INFO, $file);
	@lines = <INFO>;
	close(INFO);

	foreach $line (@lines) {
	    $line =~ s/\t/    /g;
	    $line =~ s/(\s*)$/\n/;
	}

	open(INFO, ">$file");
	print INFO @lines;
	close(INFO);
}