#!/usr/bin/perl

@parsOpen = ("(", "[", "{");
@parsClose = (")", "]", "}");

foreach $file (@ARGV) {
  open(INFO, $file);
  @lines = <INFO>;
  close(INFO);

  for ($parNum = 0; $parNum < $#parsOpen; $parNum++) {
    for ($lineNum = 0; $lineNum < $#lines; $lineNum++) {
      $_ = $lines[$lineNum];
      s/\'(.*?)\'//g;
      s/%(.*)$//;
      if ($isContinuedLine) {
        $countOpen += eval "tr/$parsOpen[$parNum]/$parsOpen[$parNum]/";
        $countClose += eval "tr/$parsClose[$parNum]/$parsClose[$parNum]/";
      } else {
        $countOpen = eval "tr/$parsOpen[$parNum]/$parsOpen[$parNum]/";
        $countClose = eval "tr/$parsClose[$parNum]/$parsClose[$parNum]/";
      }

      if (/(.*)\.\.\./) {
        $isContinuedLine++;
      } else {
        if ($countOpen != $countClose) {
          $blockNumLines = $isContinuedLine+1;
          print "\nChecking file: $file\n";
          print "Unbalanced number of parenthesis in a block of $blockNumLines lines\n";
          if ($countOpen < $countClose) {
            print "Missing Opening parenthesis: $parsOpen[$parNum]";
          } else {
            print "Missing Closing parenthesis: $parsClose[$parNum]";
          }
          print "\n";
          for ($dispLineCount = $lineNum+1 -$blockNumLines; $dispLineCount <= $lineNum; $dispLineCount++) {
            print "" . $dispLineCount+1 . " $lines[$dispLineCount]";
          }
        }
        $isContinuedLine = 0;
      }
    }
  }
}
