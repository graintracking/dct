function [p, grain] = gtImportTTScan(phase_id, gr_id, p, tt_id, detgeo)
    conf.spec_file = '/data/visitor/ma3921/id11/ts3_/ts3_dct_top__tt_positions.mac'
    conf.basedir   = '/data/visitor/ma3921/id11/ts3_';
    conf.name      = 'ts3_dct_top_';

    if (isstruct(gr_id))
        grain = gr_id;
    else
        grain = gtLoadGrain(phase_id, gr_id);
    end

    scanname  = sprintf('%sgrain%04d_tt_%d', conf.name, grain.id, tt_id)
    macroname = sprintf('%sgrain%04d_ttpar_%d', conf.name, grain.id, tt_id)

    [range_basetilt, nproj_basetilt] = gtAcqGetRangeFromSpecFile(conf.spec_file, macroname)
    xml_filename = fullfile(conf.basedir, scanname, [scanname '.xml']);

    p = gtGrainTopotomoUpdateParameters(xml_filename, range_basetilt, nproj_basetilt, p, detgeo, [], grain, 'det_roi_offset_uv', [0, -200]);
    grain = gtGrainImportTopotomoScan(grain, p, 2);
end

%gr24_t.allblobs(2).detector.uvpw(3:4:end, 1:2)'

% fwd_stack = gtAstraCreateProjections(gr24r.VOL3D.intensity, gr24_t.proj(2).geom, [501, 501]);

% Only topotomo recosnturction
% gtReconstructGrainOrientation(grain_id, phase_id, parameters, 'det_ind', 2)

% Joint DCT and Topotomo recosntruction
% gtReconstructGrainOrientation(grain_id, phase_id, parameters, 'det_ind', [1 2])
