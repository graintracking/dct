function gr_fwd = gtCreateVoxelFwdProjection(grain, p, det_ind, varargin)

    conf.rectype = 'VOL3D';
    conf.phase_id = 1;
    conf.dilate_steps = 5;
    conf.volume_super_sampling = 1;

    grain_rec = gtLoadGrainRec(conf.phase_id, grain.id);

    [int_vol, dm_vol, mask_vol] = gtCreateMaskedVolumes(grain_rec, grain, conf.dilate_steps, 'type', conf.rectype);
    fedpars = gtFedCreateGrainParsMasked(grain, mask_vol, dm_vol, p, 'keep_ondet', true, 'volume_super_sampling', conf.volume_super_sampling);

    gr_fwd = gtDefSyntheticGrainCreate(fedpars, grain, p, dm_vol, int_vol, det_ind); % intvol could be substituted by mask_vol
end