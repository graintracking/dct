function [range_basetilt, nproj_basetilt] = gtAcqGetRangeFromSpecFile(filename, scanname)

    fid = fopen(filename,'r');
    a = textscan(fid, '%s');
    pos = find(strcmp(a{1}(:), scanname));
    range_basetilt = str2double([a{1}(pos + 21), a{1}(pos + 24)]);
    nproj_basetilt = str2double(a{1}(pos + 27));
end
