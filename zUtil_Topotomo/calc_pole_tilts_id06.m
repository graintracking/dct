function [phx, phy] = calc_pole_tilts_id11(im1, im2)
% position a given reflection in the pole
% modified for ID11 goniometer: W. Ludwig, March 2014
% input parameters:

disp('this version assumes that the lower tilt (SAMRY) of the goniometer is at 0 position and aligned with Lab X at diffrz = 0 !')

basedir = '/data/id11/inhouse/align';
prefix  = 'spot_';

[im1, info1] = edf_read('%s/%s_%04d.edf', basedir, prefix, close);  

[im2, info2] = edf_read('%/%s_%04d.edf', basedir, prefix, far);

diffrz_offset = -90;
ps = 0.0014     % pixelsize in mm
dist = 6.3;

figure(1); imshow(im1 + im2, []);
[u, v] = ginput(2);

pos1 = [-dist, -u(1) * ps, v(1) * ps];
pos2 = [ dist, -(2048-u(2)) * ps, v(2) * ps];

dx = pos2 - pos1;


%--------------------------------------------------------------------------


energy = parameters.acq.energy; % keV.
lambda = gtConvEnergyToWavelength(energy);

hkl = [1 1 1];

a = 4.05;  % Al lattice spacing....

theta = asind(lambda*norm(hkl)/2/a);
tth   = 2 * theta



% Coordinate systems are identical to 3DXRD convensions.
% All rotation matrices are right hand rotations.

eta_measured = atand(dx(2)/dx(3))


tth_measured = atand( sqrt(dx(2)^2 + dx(3)^2) / dx(1) )

% output parameters:

% phi_x_final (deg) : final setting for x-Tilt
% phi_y_final (deg) : final setting for y-Tilt

% --------------------------------------------------------------------

diffrz = info1.motor.diffrz + diffrz_offset;  
diffry = info1.motor.diffry; 

samrx  = info1.motor.samrx;
samry  = info1.motor.samry;


%% Laboratory Reference defined by  X beam (X along beam) and gravity (negative Z)
%  Coincicent with sample reference system for omega = 0 (diffrz = -90)
LabX = [1 0 0]';
LabY = [0 1 0]';
LabZ = [0 0 1]';

%% Instrument reference system 
% Coincident with Sample system for diffrz = -90  since we run scans from diffrz:  -90 to 270 degre
instrgeo.dirx = [ 1  0  0];
instrgeo.diry = [ 0  1  0];
instrgeo.dirz = [ 0  0  1]; 

%% Sample reference system
samgeo.dirx = [0 -1 0];
samgeo.diry = [1 0 0];
samgeo.dirz = [0 0 1];

%% Rotation Matrix  (based on angle & axis  -> Rodrigues Rotation Formula)  v_rotated = R(angle, axis) * v

R = @(angle,axis)gtMathsRotationTensor(angle, gtMathsRotationMatrixComp(axis, 'col'));

% Plane normal in Sample Reference system
% G = cosd(tth_measured/2)*[-tand(tth_measured/2) -sind(eta_measured) cosd(eta_measured)];
% G_sam = R(-samry, LabY) * R(-diffrz, LabZ) * R(-diffry, LabY) * G;   % rotate SAMRY back to 0:  SAMRX will now rotate around Lab X

%% GInstr =  Plane normal in Instrument reference system:
% samrx is upper tilt and rotates around LabX at diffrz = 0 (omega = 90)
% ramry is lower tile and rotates around LabY at diffrz = 0 (omega = 90)

G_instr = gtGeoSam2Sam(G_sam, samgeo, instrgeo, 1, 1);

% first solve for upper tilt:  RX * G_sam = [r 0 s]   ->  G_sam(2)*cos(phx) - G_sam(3)*sin(phx) = 0
phx = atand(G_instr(2)/G_instr(3));

tmp = R(phx, LabX) * G_instr';

% now solve for lower tilt: RY * tmp = [0 0 1], RY = [cos(phy) 0  sin(phy); 0 1 0;  -sin(phy) 0 cos(phy)];  -> 
% phy = atand(-tmp(1)/tmp(3)); 

phy = atand(-tmp(1)/tmp(3)) ; 

G_aligned = R(phy, LabY) * tmp;


['mvr samrx ',num2str(phx)]
['mvr samry ',num2str(phy)]
