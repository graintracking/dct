function offsets = gtAcqGetOffsets(info, acq, beamline)
% function offsets = gtAcqGetOffsets(info, acq, beamline)

    if isempty(info)
        if acq.interlaced_turns
            info = edf_info(sprintf('%s/0_rawdata/Orig/%s0_0000.edf', acq.dir, acq.name));
        else
            info = edf_info(sprintf('%s/0_rawdata/Orig/%s0000.edf', acq.dir, acq.name));
        end
    end

    switch (beamline)
        case 'id11'
            offsets.samrx_offset  = info.motor.samrx;
            offsets.samry_offset  = info.motor.samry;
            offsets.diffrz_offset = info.motor.diffrz;
            offsets.diffry_offset = info.motor.diffry;
            offsets.samtx_offset  = info.motor.samtx;
            offsets.samty_offset  = info.motor.samty;
            offsets.samtz_offset  = info.motor.samtz;
        case 'id06'
            offsets.phi_offset    = info.motor.phi;
            offsets.chi_offset    = info.motor.chi;
            offsets.diffrz_offset = info.motor.diffry;
            offsets.samtx_offset  = 0;
            offsets.samty_offset  = 0;
            offsets.samtz_offset  = 0;
    end
end