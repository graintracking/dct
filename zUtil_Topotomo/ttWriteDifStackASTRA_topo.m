%function grain=ttWriteDifStackASTRA
% write full .dat projection STACK for ASTRA reconstruction
% calculate Projection geometry 
% projetions are oversized (zeropadded) by a factor of 2 



directory='/data/visitor/ma1317/id11/id06/newgrain_near_';
name='topo';
nproj=60;  % number of images - image numbers start with 0 and run until nproj-1, typically



range=180;
pixelsize=0.0007;

omega=[0:3:177];

eta=0;
theta=7.25;
dist=25;


rotdir0=[sind(theta), 0, -cosd(theta)];

offset=0*pixelsize;

detpos0=[dist, offset, 0];     


detucen0=354.5
detvcen0=250.5;

detdiru0=[0 ;1; 0];
detdirv0=[0; 0; -1];

detpos0=(detpos0/pixelsize)';

%detposcorner=detpos0-detucen0*detdiru0-detvcen0*detdirv0;  % the (0,0) corner of the detector in Lab system 

rotcomp = gtFedRotationMatrixComp(rotdir0');


% allocate memory for matrices
difstack=zeros(500,708,1,nproj);     % nproj

for n=1:nproj
  
    filename=sprintf('%s/%s%04d.edf',directory,name,n-1)
    im=edf_read(filename);
   
	im=im(:,240:end-79)/10000;
	imshow(im,[-1 2]);
    drawnow;
    M(n)=getframe;
    
    difstack(:,:,1,n)=im;
	Omega=omega(n);
     
    %r=[cosd(2*theta);sind(2*theta)*sind(eta);sind(2*theta)*cosd(eta)];
    r=[1; 0; 0];
	
	
	% rotate these cordinates
	Rottens = gtFedRotationTensor(-Omega,rotcomp);   % minus Omega because the detector and beam move - not 

	detdiru = Rottens*detdiru0;
    detdirv = Rottens*detdirv0;
    detpos=Rottens*detpos0;
	r=Rottens*r;
  
    V(n,:)=[r(1), r(2), r(3), detpos(1), detpos(2), detpos(3), detdiru(1), detdiru(2), detdiru(3), detdirv(1), detdirv(2), detdirv(3)];
end
  


%% save the projection stack and geometry information into the grain.mat file...
grain.difstack=difstack;
grain.proj_geom=V;

save('tt.mat','grain');


%end



