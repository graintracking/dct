function im=gtGetDifImage(ndx,struct_id,parameters, varargin)
%returns the full image requested, cropped to the specified difspot
%bounding box

if ~exist('parameters','var')
  load('parameters.mat');
end 

% connect to database
connected=0;
if ~isempty(varargin)
	connected=varargin{1};
end
if ~connected
gtDBConnect
end

name=parameters.acq.name;

mysqlcmd=sprintf(['select BoundingBoxXorigin, BoundingBoxYorigin, BoundingBoxXsize, BoundingBoxYsize '...
  'from %sdifspot where difspotID=%d'],...
  name, struct_id);

[BoundingBoxXorigin, BoundingBoxYorigin, BoundingBoxXsize, BoundingBoxYsize]=mym(mysqlcmd);

im=edf_read(sprintf('1_preprocessing/full/full%04d.edf',ndx),[BoundingBoxXorigin, BoundingBoxYorigin, BoundingBoxXsize, BoundingBoxYsize]);
