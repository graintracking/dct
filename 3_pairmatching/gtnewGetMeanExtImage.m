function im=gtnewGetMeanExtImage(ndx)
% returns the mean extinction image corresponding to a given difspot

parameters=[]; global difspot;

if isempty(parameters)
  disp('Loading parameter file')
  load('parameters.mat');
end
if isempty(difspot)
  disp('Loading difspot file')
  load('2_difspot/difspot.mat');
end

im=zeros(parameters.acq.bb(4), parameters.acq.bb(3));
disp([num2str(difspot(ndx).ExtEndImage-difspot(ndx).ExtStartImage+1) ' images in sum'])
for i=difspot(ndx).ExtStartImage:difspot(ndx).ExtEndImage
  im = im + edf_read(sprintf('1_preprocessed/ext/ext%04d.edf',i));
end
im=im/(difspot(ndx).ExtEndImage-difspot(ndx).ExtStartImage+1);
end
