function im=gtGetMeanFullImage(struct_id, parameters, varargin)
% returns the mean full direct beam corresponding to a given difspot


% modified December 2006 to support database

%if isempty(parameters)
    if ~exist('parameters', 'var')
  disp('Loading parameter file')
  load('parameters.mat');
end

name=parameters.acq.name;

% connect to database
connected=0;
if ~isempty(varargin)
	connected=varargin{1};
end
if ~connected
gtDBConnect
end

im=zeros(parameters.acq.bb(4), parameters.acq.bb(3));
mysqlcmd=sprintf('select ExtStartImage,ExtEndImage from %sdifspot where difspotID=%d',name,struct_id);

[ExtStartImage,ExtEndImage]=mym(mysqlcmd);

fprintf('%d images in sum\n',ExtEndImage-ExtStartImage+1);

%path ensures that correct half of a 360 scan is read
for i=ExtStartImage:ExtEndImage
  im = im + edf_read(sprintf('1_preprocessing/full/full%04d.edf',i), parameters.acq.bb);
end
im=im/(ExtEndImage-ExtStartImage+1);
end
