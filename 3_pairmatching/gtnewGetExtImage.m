function im=gtnewGetExtImage(ndx)
%returns the direct beam part of the requested full image

parameters=[];
if isempty(parameters)
  disp('Loading parameter file')
  load('parameters.mat');
end

im = edf_read(sprintf('1_preprocessed/full/full%04d.edf',ndx),parameters.acq.bb);


