function [spot, bb] = gtGetSummedDifSpot(difspotID, parameters, connected, det_index, info)
% GTGETSUMMEDDIFSPOT  Returns the summed difspot image (saved as *0000/difspot%05d.edf) or in difblobtable
% [spot, bb] = gtGetSummedDifSpot(difspotID, [parameters], [connected], [det_index], [info])
% -----------------------------------------------------------------------
% INPUT:
%   difspotID  = <double>   diffraction spot ID
%   parameters = <struct>   parameters.mat
%   connected  = <logical>  true if already connected to the database {false}
%   info       = <struct>   image header info {[]}
%
% OUTPUT:
%   spot       = <double>   diffraction spot image
%   bb         = <double>   diffraction spot bounding box
%
% 360degree scan convention - ak 10/2007
% speed up by providing edfheader info

    if (~exist('parameters','var') || isempty(parameters))
        parameters = gtLoadParameters();
    end
    if (~exist('det_index', 'var') || isempty(det_index))
        det_index = 1;
    end

    % connect to database
    if (~exist('connected', 'var'))
        connected = false;
    end
    if (~connected)
        gtDBConnect()
    end

    if (~exist('info', 'var'))
        info = [];
    end

    bb = [];

    base_dir = fullfile(parameters.acq(det_index).dir, '2_difspot');
    sub_dir  = fullfile(base_dir, sprintf('%05d', difspotID - mod(difspotID, 1e4)));
    filename = fullfile(sub_dir, sprintf('difspot%05d.edf', difspotID) );

    dataset_name = parameters.acq(det_index).name;

    if exist(filename, 'file')
        % read the edf file
        spot = edf_read(filename, [], [], info);
        if (nargout == 2)
            query = sprintf([ ...
                'SELECT BoundingBoxXOrigin, BoundingBoxYOrigin, ' ...
                '       BoundingBoxXSize, BoundingBoxYSize' ...
                '   FROM %sdifspot' ...
                '   WHERE difspotID = %d'], dataset_name, difspotID);
            [bb(1), bb(2), bb(3), bb(4)] = mym(query);
        end
    else
        try
            [vol, bb] = gtDBBrowseDiffractionVolume(dataset_name, difspotID);
            spot = sum(vol, 3);
        catch mexc
            gtPrintException(mexc, 'cannot find difspot as edf or as difblob!')
            rethrow(mexc)
        end
    end

    if (isa(spot, 'double'))
        spot = single(spot);
    end
end
