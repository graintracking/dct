function im = gtPlaceDifspotinFull(difspotIDs, parameters, varargin)
% GTPLACEDIFSPOTINFULL  Draws all the difspots together
%     im = gtPlaceDifspotinFull(struct_id, parameters, varargin)
%     ----------------------------------------------------------
%     Returns the full image requested, cropped to the specified difspot
%     bounding box
%
%     INPUT:
%       difspotIDs = <double>   list of difspot (i.e. grain{grainid}.difspotID)
%       parameters = <struct>   parameters.mat
%
%     OPTIONAL INPUT (parse by pairs):
%       connected  = <logical>  already connected to the database {false}
%       save       = <logical>  desire to save the produced image {false}
%       replace    = <logical>  replace existing <name>_summed.edf file {false}
%       draw       = <logical>  show image {false}
%       id         = <double>   grain id {0}
% 
%     OUTPUT:
%       im         = <double>   image with all the difspots summed
%
%     Version 002 13-12-2013 by LNervo

    conf = struct('id', 0, 'draw', false, 'connected', false, ...
        'save', false, 'replace', false, 'det_ind', 1, 'contour', false);
    conf = parse_pv_pairs(conf, varargin);

    if (~exist('parameters','var') || isempty(parameters))
        parameters = gtLoadParameters();
    end
    name = parameters.acq(conf.det_ind).name;
    namefull = [name '_grain' num2str(conf.id) '_summed.edf'];

    summed = false;
    if (exist(namefull, 'file') && conf.replace)
        im = edf_read(namefull);
        summed = true;
    else
        detgeo = parameters.detgeo(conf.det_ind);
        im = zeros(detgeo.detsizev, detgeo.detsizeu);

        % connect to database
        if (~conf.connected)
            gtDBConnect();
            conf.connected = true;
        end

        for ii = 1:numel(difspotIDs)
            full = zeros(size(im));
            [difspot_img, bb] = gtGetSummedDifSpot(difspotIDs(ii), ...
                parameters, conf.connected, conf.det_ind);


        if conf.contour
            difspot_img = bwperim(difspot_img);
            difspot_img = single(bwmorph(difspot_img,'dilate'));
        end
        full = gtPlaceSubImage2(difspot_img, full, bb(1), bb(2));
        im = im + full;
    end

    if (conf.draw)
        figure();
        imshow(im, []);
    end

    if (conf.save && ~summed)
        edf_write(im, namefull);
    end
end
