function spot=gtGetSummedDifSpot(ndx)
global app
if isempty(app)
  disp('Loading parameter file')
  load('common.mat');
end

spot = edf_read(sprintf('full/full%04d.edf',app.extspot(ndx).MaxImage),app.extspot(ndx).BoundingBox);

end
