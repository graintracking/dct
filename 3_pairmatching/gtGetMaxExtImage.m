function im=gtGetMaxExtImage(ndx)

global app
if isempty(app)
  disp('Loading parameter file')
  load('common.mat');
end

%read maximum intensity ext image from full
im = edf_read(sprintf('full/full%04d.edf',app.extspot(ndx).MaxImage),app.bb.directbeam);
end
