function [summed,bb]=gtGetMeanFullImage(difspotID, parameters, varargin)
% returns the mean full direct beam corresponding to a given difspot


app.clims=[];
if ~exist('parameters', 'var')
  disp('Loading parameter file')
  load('parameters.mat');
end
app.StartImage = [];
app.EndImage = [];
app=parse_pv_pairs(app,varargin);


name=parameters.acq.name;
mysqlcmd=sprintf('select StartImage,EndImage,BoundingBoxXorigin, BoundingBoxYorigin, BoundingBoxXsize, BoundingBoxYsize from %sdifspot where difspotID=%d',name,difspotID);

if isempty(app.StartImage)
    [StartImage,EndImage, bb(1), bb(2), bb(3) bb(4)]=mym(mysqlcmd);
    nim=EndImage-StartImage+1;
    %fprintf('%d images in sum\n',nim);
else
    StartImage = app.StartImage;
    EndImage = app.EndImage;
end

%im=zeros(bb(4),bb(3),nim);
im=zeros(parameters.acq.ydet,parameters.acq.xdet);


j=1;
%path ensures that correct half of a 360 scan is read
for i=StartImage:EndImage
  im(:,:,j) = edf_read(sprintf('1_preprocessing/full/full%04d.edf',i));
  j=j+1;
end

summed=sum(im,3);
%figure;
%imshow(summed,app.clims);
%text=sprintf('spot %d',difspotID);
%title(text);

end
