function im=gtGetDifImage(ndx,struct_id)
%returns the full image requested, cropped to the specified difspot bounding box

global difspot

if isempty(difspot)
  disp('Loading difspot file')
  load('2_difspot/difspot.mat');
end

im = edf_read(sprintf('1_preprocessed/full/full%04d.edf',ndx),difspot(struct_id).BoundingBox);
