function [ims,im]=gtGetSummedExtImage(extspotID,parameters)

if ~exist('parameters','var')
  load('parameters.mat');
end  
acq=parameters.acq;

[start,endim]=mym(sprintf('select ExtStartImage, ExtEndImage from %sdifspot where difspotID=%d',acq.name,extspotID));

im=zeros(acq.bb(4), acq.bb(3),endim-start+1);
ims=zeros(acq.bb(4), acq.bb(3));
disp([num2str(endim-start+1) ' images in sum'])
j=1;
for i=start:endim
  im(:,:,j) = edf_read(sprintf('%s/1_preprocessing/ext/ext%04d.edf',acq.dir,i));
  ims=ims+im(:,:,j);
  j=j+1;
end


end
