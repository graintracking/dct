function im=gtGetExtImage(ndx, parameters)%, varargin)
%returns the direct beam part of the requested full image
%varargin - ABflag for 360 data - struct_id refers to which half of the scan?
%will take from the working directory (whichever parameter.mat is loaded!)
%by default, so can be used as before


if ~exist('parameters','var')
  load('parameters.mat');
end 

name=parameters.acq.name;

path=sprintf('../%s/',name);

im = edf_read(sprintf('%s1_preprocessing/full/full%04d.edf',path,ndx),parameters.acq.bb);


