function subscripts=gtFindMaxSubscripts(im)

[tmp,tmp2] = max(im(:));
[tmp3,tmp4] = ind2sub(size(im),tmp2);
subscripts = [tmp3 tmp4];
end
