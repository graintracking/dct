/*
 * gtCxxMathsSumCellVolumes.cpp
 *
 *  Created on: 17 feb 2017
 *      Author: nicola
 */

#include "DctMatlabData.h"

static const char * reduce_volumes_error_id = "gtCxxMathsSumCellVolumes:wrong_argument";

void mexFunction( int nlhs, mxArray * plhs[], int nrhs, const mxArray * prhs[] )
{
  if (nrhs < 1) {
    mexErrMsgIdAndTxt(reduce_volumes_error_id,
        "Not enough arguments!");
    return;
  }

  const mxArray * const vol_cells = prhs[0];

  int32_t dim = -1;
  if (nrhs > 1) {
    dim = (int32_t)mxGetScalar(prhs[1]);
    if (dim > -1) {
      dim--;
    }
  }
  size_t num_threads = std::thread::hardware_concurrency();
  if (nrhs > 2)
  {
    num_threads = mxGetScalar(prhs[2]);
  }

  if (!mxIsCell(vol_cells)) {
    mexErrMsgIdAndTxt(reduce_volumes_error_id,
        "The first argument should be a Cell array");
    return;
  }

  const mxClassID datatype = dct::DctMatlabData::get_class_of_cell(vol_cells);

  if (datatype != mxDOUBLE_CLASS && datatype != mxSINGLE_CLASS){
    mexErrMsgIdAndTxt(reduce_volumes_error_id,
        "The cells have to be of a coherent floating point types");
    return;
  }

  switch (datatype)
  {
    case mxDOUBLE_CLASS:
    {
      dct::DctVolumeGridTransform<double, dct::MatlabAllocator<double> > transformer(num_threads);

      try {
        auto vols = dct::DctMatlabData::import_volume_grid<double>((mxArray *)vol_cells);
        vols.check_consistency();
        transformer.set_vols(vols);

        const OpBinaryPlus<double> op;
        auto out_vols = transformer.accumulate(dim, op);

        plhs[0] = dct::DctMatlabData::produce_volume_grid(out_vols, dim);

      } catch (const dct::BasicException & e) {
        mexErrMsgIdAndTxt(reduce_volumes_error_id, e.what());
        return;
      }
      break;
    }
    case mxSINGLE_CLASS:
    {
      dct::DctVolumeGridTransform<float, dct::MatlabAllocator<float> > transformer(num_threads);

      try {
        auto vols = dct::DctMatlabData::import_volume_grid<float>((mxArray *)vol_cells);
        vols.check_consistency();
        transformer.set_vols(vols);

        const OpBinaryPlus<float> op;
        auto out_vols = transformer.accumulate(dim, op);

        plhs[0] = dct::DctMatlabData::produce_volume_grid(out_vols, dim);

      } catch (const dct::BasicException & e) {
        mexErrMsgIdAndTxt(reduce_volumes_error_id, e.what());
        return;
      }
      break;
    }
    default:
    {
      mexErrMsgIdAndTxt(reduce_volumes_error_id,
          "The argument needs to be a Cell array of floating point numbers");
      return;
    }
  }
}
