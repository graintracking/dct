/*
 * gtCxxMathsCellCopy.cpp
 *
 *  Created on: Sep 10, 2017
 *      Author: Nicola Vigano', 2017, CWI, N.R.Vigano@cwi.nl
 */

#include "DctMatlabData.h"

const char * error_id = "gtCxxMathsCellCopy:wrong_argument";

void mexFunction( int nlhs, mxArray * plhs[], int nrhs, const mxArray * prhs[] )
{
  const size_t base_args = 1;
  if (nrhs < base_args) {
    mexErrMsgIdAndTxt(error_id, "Not enough arguments!");
    return;
  }

  size_t num_threads = std::thread::hardware_concurrency();

  if (nrhs > base_args) {
    if ((nrhs - base_args) % 2) {
      mexErrMsgIdAndTxt(error_id,
          "Extra options should be specified in pairs");
      return;
    }
    for (mwIndex opt_ind = 1; opt_ind < nrhs; opt_ind += 2) {
      std::string option_type = dct::DctMatlabData::get_string(prhs[opt_ind]);
      if (option_type == "threads") {
        if (!mxIsEmpty(prhs[opt_ind+1])) {
          num_threads = mxGetScalar(prhs[opt_ind+1]);
        }
      } else {
        mexErrMsgIdAndTxt(error_id,
            "Extra options allowed: 'threads'");
        return;
      }
    }
  }

  const mxArray * const first = prhs[0];

  if (!mxIsCell(first))
  {
    mexErrMsgIdAndTxt(error_id,
        "The first argument should be a Cell array");
    return;
  }

  const mxClassID firstClass = dct::DctMatlabData::get_class_of_cell(first);

  switch (firstClass)
  {
    case mxDOUBLE_CLASS:
    {
      dct::DctVolumeGridTransform<double, dct::MatlabAllocator<double> > transformer(num_threads);

      try {
        auto vols_first = dct::DctMatlabData::import_volume_grid<double>((mxArray *)first);
        vols_first.check_consistency();

        const OpUnaryAssign<double> op;
        transformer.volume_wise_unary(op, vols_first);

        plhs[0] = dct::DctMatlabData::produce_volume_grid(transformer.get_vols(), 0);

      } catch (const dct::BasicException & e) {
        mexErrMsgIdAndTxt(error_id, e.what());
        return;
      }
      break;
    }
    case mxSINGLE_CLASS:
    {
      dct::DctVolumeGridTransform<float, dct::MatlabAllocator<float> > transformer(num_threads);

      try {
        auto vols_first = dct::DctMatlabData::import_volume_grid<float>((mxArray *)first);
        vols_first.check_consistency();

        const OpUnaryAssign<float> op;
        transformer.volume_wise_unary(op, vols_first);

        plhs[0] = dct::DctMatlabData::produce_volume_grid(transformer.get_vols(), 0);

      } catch (const dct::BasicException & e) {
        mexErrMsgIdAndTxt(error_id, e.what());
        return;
      }
      break;
    }
    case mxUNKNOWN_CLASS:
    {
      mexErrMsgIdAndTxt(error_id,
          "The argument needs to be a non empty Cell array of coherent floating point types");
      return;
    }
    default:
    {
      mexErrMsgIdAndTxt(error_id,
          "The argument needs to be a Cell array of floating point numbers");
      return;
    }
  }
}

