/*
 * gtCxxMathsCellMin.cpp
 *
 *  Created on: Sep 10, 2017
 *      Author: Nicola Vigano', 2017, CWI, N.R.Vigano@cwi.nl
 */

#include "DctMatlabData.h"

const char * error_id = "gtCxxMathsCellMin:wrong_argument";

void mexFunction( int nlhs, mxArray * plhs[], int nrhs, const mxArray * prhs[] )
{
  const size_t base_args = 2;
  if (nrhs < base_args) {
    mexErrMsgIdAndTxt(error_id,
        "Not enough arguments! you need to provide at least two arguments");
    return;
  }

  size_t num_threads = std::thread::hardware_concurrency();
  bool copy = true;

  if (nrhs > base_args) {
    if ((nrhs - base_args) % 2) {
      mexErrMsgIdAndTxt(error_id,
          "Extra options should be specified in pairs");
      return;
    }
    for (mwIndex opt_ind = 2; opt_ind < nrhs; opt_ind += 2) {
      std::string option_type = dct::DctMatlabData::get_string(prhs[opt_ind]);
      if (option_type == "threads") {
        if (!mxIsEmpty(prhs[opt_ind+1])) {
          num_threads = mxGetScalar(prhs[opt_ind+1]);
        }
      } else  if (option_type == "copy") {
        if (!mxIsEmpty(prhs[opt_ind+1])) {
          copy = mxGetScalar(prhs[opt_ind+1]);
        }
      } else {
        mexErrMsgIdAndTxt(error_id,
            "Extra options allowed: 'threads' and 'copy'");
        return;
      }
    }
  }

  const mxArray * const first = prhs[0];
  const mxArray * const second = prhs[1];

  if (!mxIsCell(first))
  {
    mexErrMsgIdAndTxt(error_id,
        "The first argument should be a Cell array");
    return;
  }

  const bool second_scalar = mxIsNumeric(second) && mxGetNumberOfElements(second) == 1;
  if (!(mxIsCell(second) || second_scalar))
  {
    mexErrMsgIdAndTxt(error_id,
        "The second argument should be a cell array or a scalar");
    return;
  }

  const mxClassID firstClass = dct::DctMatlabData::get_class_of_cell(first);

  if (!second_scalar)
  {
    const mwSize num_elems_1 = mxGetNumberOfElements(first);
    const mwSize num_elems_2 = mxGetNumberOfElements(second);

    if (num_elems_1 != num_elems_2)
    {
      mexErrMsgIdAndTxt(error_id,
          "The Cell arrays should have the same size");
      return;
    }

    if (firstClass != dct::DctMatlabData::get_class_of_cell(second))
    {
      mexErrMsgIdAndTxt(error_id,
          "The arguments need to be Cell arrays of coherent floating point types");
      return;
    }
  }

  switch (firstClass)
  {
    case mxDOUBLE_CLASS:
    {
      dct::DctVolumeGridTransform<double, dct::MatlabAllocator<double> > transformer(num_threads);

      try {
        auto vols_first = dct::DctMatlabData::import_volume_grid<double>((mxArray *)first);
        vols_first.check_consistency();

        if (copy) {
          if (second_scalar) {
            const OpUnaryMinScalar<double> op(mxGetScalar(second));
            transformer.volume_wise_unary(op, vols_first);
          } else{
            auto vols_second = dct::DctMatlabData::import_volume_grid<double>((mxArray *)second);
            const OpBinaryMin<double> op;
            transformer.volume_wise_binary(op, vols_first, vols_second);
          }

          plhs[0] = dct::DctMatlabData::produce_volume_grid(transformer.get_vols(), 0);
        } else {
          transformer.set_vols(vols_first);

          if (second_scalar) {
            const OpUnaryMinScalar<double> op(mxGetScalar(second));
            transformer.volume_wise_unary(op);
          } else{
            auto vols_second = dct::DctMatlabData::import_volume_grid<double>((mxArray *)second);
            const OpBinaryMin<double> op;
            transformer.volume_wise_binary(op, vols_second);
          }

          plhs[0] = mxCreateSharedDataCopy(prhs[0]);
        }

      } catch (const dct::BasicException & e) {
        mexErrMsgIdAndTxt(error_id, e.what());
        return;
      }
      break;
    }
    case mxSINGLE_CLASS:
    {
      dct::DctVolumeGridTransform<float, dct::MatlabAllocator<float> > transformer(num_threads);

      try {
        auto vols_first = dct::DctMatlabData::import_volume_grid<float>((mxArray *)first);
        vols_first.check_consistency();

        if (copy) {
          if (second_scalar) {
            const OpUnaryMinScalar<float> op(mxGetScalar(second));
            transformer.volume_wise_unary(op, vols_first);
          } else{
            auto vols_second = dct::DctMatlabData::import_volume_grid<float>((mxArray *)second);
              const OpBinaryPlus<float> op;
              transformer.volume_wise_binary(op, vols_first, vols_second);
          }

          plhs[0] = dct::DctMatlabData::produce_volume_grid(transformer.get_vols(), 0);
        } else {
          transformer.set_vols(vols_first);

          if (second_scalar) {
            const OpUnaryMinScalar<float> op(mxGetScalar(second));
            transformer.volume_wise_unary(op);
          } else{
            auto vols_second = dct::DctMatlabData::import_volume_grid<float>((mxArray *)second);
            const OpBinaryMin<float> op;
            transformer.volume_wise_binary(op, vols_second);
          }

          plhs[0] = mxCreateSharedDataCopy(prhs[0]);
        }

      } catch (const dct::BasicException & e) {
        mexErrMsgIdAndTxt(error_id, e.what());
        return;
      }
      break;
    }
    case mxUNKNOWN_CLASS:
    {
      mexErrMsgIdAndTxt(error_id,
          "The argument needs to be a non empty Cell array of coherent floating point types");
      return;
    }
    default:
    {
      mexErrMsgIdAndTxt(error_id,
          "The argument needs to be a Cell array of floating point numbers");
      return;
    }
  }
}

