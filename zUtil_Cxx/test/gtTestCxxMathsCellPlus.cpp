/*
 * gtCxxMathsCellPlus.cpp
 *
 *  Created on: Jul 11, 2012
 *
 * Nicola Vigano', 2012, ID11 @ ESRF vigano@esrf.eu
 */

#include <valgrind/drd.h>
#define _GLIBCXX_SYNCHRONIZATION_HAPPENS_BEFORE(addr) ANNOTATE_HAPPENS_BEFORE(addr)
#define _GLIBCXX_SYNCHRONIZATION_HAPPENS_AFTER(addr)  ANNOTATE_HAPPENS_AFTER(addr)

#include "DctMatlabData.h"

#include <mat.h>
#include <valgrind/callgrind.h>

#include <chrono>

const char * error_id = "gtCxxMathsCellPlus:wrong_argument";

static const char filename_in[] = "test_gtCxxMathsCellPlus_data_in.mat";
static const char filename_out[] = "test_gtCxxMathsCellPlus_data_out.mat";

int main()
{
  MATFile * mfPtr = matOpen(filename_in, "r");
  if (mfPtr == nullptr) {
    mexErrMsgTxt("Error opening file: 'test_gtCxxMathsCellPlus_data_in.mat'");
    return 1;
  }

  const mxArray * const first = matGetVariable(mfPtr, "first");
  if (first == nullptr) {
    mexErrMsgTxt("mxArray not found: 'first'");
    return 1;
  }

  const mxArray * const second = matGetVariable(mfPtr, "second");
  if (second == nullptr) {
    mexErrMsgTxt("mxArray not found: 'dim'");
    return 1;
  }

  const mxArray * const tot_threads = matGetVariable(mfPtr, "tot_threads");
  if (tot_threads == nullptr) {
    mexErrMsgTxt("mxArray not found: 'num_threads'");
    return 1;
  }
  const size_t num_threads = mxGetScalar(tot_threads);

  const mxArray * const mx_copy = matGetVariable(mfPtr, "copy");
  if (mx_copy == nullptr) {
    mexErrMsgTxt("mxArray not found: 'mx_copy'");
    return 1;
  }
  const bool copy = mxGetScalar(mx_copy);

  if (matClose(mfPtr) != 0) {
    mexErrMsgTxt("Error closing file: 'test_gtCxxMathsCellPlus_data_in.mat'");
    return 1;
  }

  if (!mxIsCell(first)) {
    mexErrMsgIdAndTxt(error_id,
        "The first argument should be a Cell array (Volume sizes)");
    return 1;
  }

  if (!mxIsCell(first))
  {
    mexErrMsgIdAndTxt(error_id,
        "The first argument should be a Cell array");
    return 1;
  }
  if (!(mxIsCell(second) || mxIsScalar(second)))
  {
    mexErrMsgIdAndTxt(error_id,
        "The second argument should be a Cell array");
    return 1;
  }

  const bool second_scalar = mxIsScalar(second);
  const mxClassID firstClass = dct::DctMatlabData::get_class_of_cell(first);

  mexPrintf("Num threads: %lu, copy: %u, second_Scalar: %u\n", num_threads, copy, second_scalar);

  if (!second_scalar)
  {
    const mwSize num_elems_1 = mxGetNumberOfElements(first);
    const mwSize num_elems_2 = mxGetNumberOfElements(second);

    if (num_elems_1 != num_elems_2)
    {
      mexErrMsgIdAndTxt(error_id,
          "The Cell arrays should have the same size");
      return 1;
    }

    if (firstClass != dct::DctMatlabData::get_class_of_cell(second))
    {
      mexErrMsgIdAndTxt(error_id,
          "The arguments need to be Cell arrays of coherent floating point types");
      return 1;
    }
  }

  mxArray * vol;

  CALLGRIND_TOGGLE_COLLECT;

  switch (firstClass)
  {
    case mxDOUBLE_CLASS:
    {
      dct::DctVolumeGridTransform<double, dct::MatlabAllocator<double> > transformer(num_threads);

      try {
        auto vols_first = dct::DctMatlabData::import_volume_grid<double>((mxArray *)first);
        vols_first.check_consistency();

        if (copy) {
          if (second_scalar) {
            const OpUnaryPlusScalar<double> op(mxGetScalar(second));
            transformer.volume_wise_unary(op, vols_first);
          } else{
            const OpBinaryPlus<double> op;
            auto vols_second = dct::DctMatlabData::import_volume_grid<double>((mxArray *)second);
            transformer.volume_wise_binary(op, vols_first, vols_second);
          }
        } else {
          transformer.set_vols(vols_first);

          if (second_scalar) {
            const OpUnaryPlusScalar<double> op(mxGetScalar(second));
            transformer.volume_wise_unary(op);
          } else{
            const OpBinaryPlus<double> op;
            auto vols_second = dct::DctMatlabData::import_volume_grid<double>((mxArray *)second);
            transformer.volume_wise_binary(op, vols_second);
          }
        }

        vol = dct::DctMatlabData::produce_volume_grid(transformer.get_vols(), 0);

      } catch (const dct::BasicException & e) {
        mexErrMsgIdAndTxt(error_id, e.what());
        return 1;
      }
      break;
    }
    case mxSINGLE_CLASS:
    {
      std::chrono::system_clock::time_point t1, t2, t3, t4, t5;

      t1 = std::chrono::high_resolution_clock::now();

      dct::DctVolumeGridTransform<float, dct::MatlabAllocator<float> > transformer(num_threads);

      t2 = std::chrono::high_resolution_clock::now();

      try {
        auto vols_first = dct::DctMatlabData::import_volume_grid<float>((mxArray *)first);
        vols_first.check_consistency();

        t3 = std::chrono::high_resolution_clock::now();

        if (copy) {
          if (second_scalar) {
            const OpUnaryPlusScalar<float> op(mxGetScalar(second));
            transformer.volume_wise_unary(op, vols_first);

            t4 = std::chrono::high_resolution_clock::now();
          } else{
            const OpBinaryPlus<float> op;
            auto vols_second = dct::DctMatlabData::import_volume_grid<float>((mxArray *)second);
            transformer.volume_wise_binary(op, vols_first, vols_second);

            t4 = std::chrono::high_resolution_clock::now();
          }
        } else {
          transformer.set_vols(vols_first);

          if (second_scalar) {
            const OpUnaryPlusScalar<float> op(mxGetScalar(second));
            transformer.volume_wise_unary(op);

            t4 = std::chrono::high_resolution_clock::now();
          } else{
            const OpBinaryPlus<float> op;
            auto vols_second = dct::DctMatlabData::import_volume_grid<float>((mxArray *)second);
            transformer.volume_wise_binary(op, vols_second);

            t4 = std::chrono::high_resolution_clock::now();
          }
        }

        vol = dct::DctMatlabData::produce_volume_grid(transformer.get_vols(), 0);

        t5 = std::chrono::high_resolution_clock::now();

        const std::chrono::duration<double, std::milli> d1 = t2-t1;
        const std::chrono::duration<double, std::milli> d2 = t3-t2;
        const std::chrono::duration<double, std::milli> d3 = t4-t3;
        const std::chrono::duration<double, std::milli> d4 = t5-t4;

        printf("Times: %f, %f, %f, %f\n", d1.count(), d2.count(), d3.count(), d4.count());

      } catch (const dct::BasicException & e) {
        mexErrMsgIdAndTxt(error_id, e.what());
        return 1;
      }
      break;
    }
    case mxUNKNOWN_CLASS:
    {
      mexErrMsgIdAndTxt(error_id,
          "The argument needs to be a non empty Cell array of coherent floating point types");
      return 1;
    }
    default:
    {
      mexErrMsgIdAndTxt(error_id,
          "The argument needs to be a Cell array of floating point numbers");
      return 1;
    }
  }

  CALLGRIND_TOGGLE_COLLECT;

  MATFile * mfPtrOut = matOpen(filename_out, "w");
  if (mfPtrOut == nullptr) {
    mexErrMsgTxt("Error opening file: 'test_blob2sino_perf_data_out.mat'");
    return 1;
  }

  int status = matPutVariable(mfPtrOut, "vol", vol);
  if (status != 0) {
      printf("%s :  Error using matPutVariable on line %d\n", __FILE__, __LINE__);
      return(EXIT_FAILURE);
  }

  if (matClose(mfPtrOut) != 0) {
    mexErrMsgTxt("Error closing file: 'test_gtMathsSumCellVolumes_data_corr.mat'");
    return 1;
  }

  return 0;
}

