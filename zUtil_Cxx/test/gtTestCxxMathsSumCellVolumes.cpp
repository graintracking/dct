/*
 * gt6DBlobsToSingleSino_c.cpp
 *
 *  Created on: Oct 30, 2014
 *      Author: vigano
 */

#include <valgrind/drd.h>
#define _GLIBCXX_SYNCHRONIZATION_HAPPENS_BEFORE(addr) ANNOTATE_HAPPENS_BEFORE(addr)
#define _GLIBCXX_SYNCHRONIZATION_HAPPENS_AFTER(addr)  ANNOTATE_HAPPENS_AFTER(addr)

#include "DctMatlabData.h"

#include <mat.h>
#include <valgrind/callgrind.h>

#include <chrono>

static const char * reduce_volumes_error_id = "gtCxxMathsSumCellVolumes:wrong_argument";

static const char filename_in1[] = "test_gtMathsSumCellVolumes_data_corr.mat";
//static const char filename_in2[] = "test_gtMathsSumCellVolumes_data_grad.mat";
static const char filename_out[] = "test_gtMathsSumCellVolumes_data_out.mat";

int main()
{
  MATFile * mfPtr = matOpen(filename_in1, "r");
  if (mfPtr == nullptr) {
    mexErrMsgTxt("Error opening file: 'test_gtMathsSumCellVolumes_data_corr.mat'");
    return 1;
  }

  const mxArray * const vol_cells = matGetVariable(mfPtr, "grid");
  if (vol_cells == nullptr) {
    mexErrMsgTxt("mxArray not found: 'sinos'");
    return 1;
  }

  const mxArray * const mxdim = matGetVariable(mfPtr, "dim");
  if (mxdim == nullptr) {
    mexErrMsgTxt("mxArray not found: 'dim'");
    return 1;
  }
  int32_t dim = mxGetScalar(mxdim);
  if (dim > -1) {
    dim--;
  }

  const mxArray * const tot_threads = matGetVariable(mfPtr, "tot_threads");
  if (tot_threads == nullptr) {
    mexErrMsgTxt("mxArray not found: 'num_threads'");
    return 1;
  }
  const size_t num_threads = mxGetScalar(tot_threads);

  if (matClose(mfPtr) != 0) {
    mexErrMsgTxt("Error closing file: 'test_gtMathsSumCellVolumes_data_corr.mat'");
    return 1;
  }

  if (!mxIsCell(vol_cells)) {
    mexErrMsgIdAndTxt(reduce_volumes_error_id,
        "The first argument should be a Cell array (Volume sizes)");
    return 1;
  }

  const mxClassID datatype = dct::DctMatlabData::get_class_of_cell(vol_cells);

  if (datatype == mxUNKNOWN_CLASS)
  {
    mexErrMsgIdAndTxt(reduce_volumes_error_id,
        "The volumes need to be a non empty Cell array of coherent floating point types");
    return 1;
  }

  mxArray * vol;

  CALLGRIND_TOGGLE_COLLECT;

  switch (datatype)
  {
    case mxDOUBLE_CLASS:
    {
      dct::DctVolumeGridTransform<double, dct::MatlabAllocator<double> > transformer(num_threads);

      try {
        auto vols = dct::DctMatlabData::import_volume_grid<double>((mxArray *)vol_cells);
        vols.check_consistency();
        transformer.set_vols(vols);

        const OpBinaryPlus<double> op;
        auto out_vols = transformer.accumulate(dim, op);

        vol = dct::DctMatlabData::produce_volume_grid(out_vols, dim);

      } catch (const dct::BasicException & e) {
        mexErrMsgIdAndTxt(reduce_volumes_error_id, e.what());
        return 1;
      }
      break;
    }
    case mxSINGLE_CLASS:
    {
      std::chrono::system_clock::time_point t1, t2, t3, t4, t5;

      t1 = std::chrono::high_resolution_clock::now();

      dct::DctVolumeGridTransform<float, dct::MatlabAllocator<float> > transformer(num_threads);

      t2 = std::chrono::high_resolution_clock::now();

      try {
        auto vols = dct::DctMatlabData::import_volume_grid<float>((mxArray *)vol_cells);
        vols.check_consistency();
        transformer.set_vols(vols);

        t3 = std::chrono::high_resolution_clock::now();

        const OpBinaryPlus<float> op;
        auto out_vols = transformer.accumulate(dim, op);

        t4 = std::chrono::high_resolution_clock::now();

        vol = dct::DctMatlabData::produce_volume_grid(out_vols, dim);

        t5 = std::chrono::high_resolution_clock::now();

      } catch (const dct::BasicException & e) {
        mexErrMsgIdAndTxt(reduce_volumes_error_id, e.what());
        return 1;
      }

      const std::chrono::duration<double, std::milli> d1 = t2-t1;
      const std::chrono::duration<double, std::milli> d2 = t3-t2;
      const std::chrono::duration<double, std::milli> d3 = t4-t3;
      const std::chrono::duration<double, std::milli> d4 = t5-t4;

      printf("Times: %f, %f, %f, %f\n", d1.count(), d2.count(), d3.count(), d4.count());

      break;
    }
    default:
    {
      mexErrMsgIdAndTxt(reduce_volumes_error_id,
          "The argument needs to be a Cell array of floating point numbers");
      return 1;
    }
  }

  CALLGRIND_TOGGLE_COLLECT;

  MATFile * mfPtrOut = matOpen(filename_out, "w");
  if (mfPtrOut == nullptr) {
    mexErrMsgTxt("Error opening file: 'test_blob2sino_perf_data_out.mat'");
    return 1;
  }

  int status = matPutVariable(mfPtrOut, "vol", vol);
  if (status != 0) {
      printf("%s :  Error using matPutVariable on line %d\n", __FILE__, __LINE__);
      return(EXIT_FAILURE);
  }

  if (matClose(mfPtrOut) != 0) {
    mexErrMsgTxt("Error closing file: 'test_gtMathsSumCellVolumes_data_corr.mat'");
    return 1;
  }

  return 0;
}


