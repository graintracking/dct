/*
 * gt6DBlobsToSingleSino_c.cpp
 *
 *  Created on: Oct 30, 2014
 *      Author: vigano
 */

#include <valgrind/drd.h>
#define _GLIBCXX_SYNCHRONIZATION_HAPPENS_BEFORE(addr) ANNOTATE_HAPPENS_BEFORE(addr)
#define _GLIBCXX_SYNCHRONIZATION_HAPPENS_AFTER(addr)  ANNOTATE_HAPPENS_AFTER(addr)

#include "DctMatlabData.h"

#include <mat.h>
#include <valgrind/callgrind.h>

#include <chrono>

static const char * transforms_error_id = "C_FUN:gt6DBlobsSinogramsTransforms:wrong_argument";

static const char filename_in[] = "test_blob2sino_perf_data.mat";
static const char filename_out[] = "test_blob2sino_perf_data_out.mat";

int main()
{
  MATFile * mfPtr = matOpen(filename_in, "r");
  if (mfPtr == nullptr) {
    mexErrMsgTxt("Error opening file: 'test_blob2sino_perf_data.mat'");
    return 1;
  }

  const mxArray * const sinos_cells = matGetVariable(mfPtr, "sinos");
  if (sinos_cells == nullptr) {
    mexErrMsgTxt("mxArray not found: 'sinos'");
    return 1;
  }

  const mxArray * const coeffs = matGetVariable(mfPtr, "offss");
  if (coeffs == nullptr) {
    mexErrMsgTxt("mxArray not found: 'offss'");
    return 1;
  }

  mxArray * const blobs_cells = matGetVariable(mfPtr, "blobs");
  if (blobs_cells == nullptr) {
    mexErrMsgTxt("mxArray not found: 'blobs'");
    return 1;
  }

  const mxArray * const tot_threads = matGetVariable(mfPtr, "tot_threads");
  if (tot_threads == nullptr) {
    mexErrMsgTxt("mxArray not found: 'num_threads'");
    return 1;
  }
  const size_t num_threads = mxGetScalar(tot_threads);

  if (matClose(mfPtr) != 0) {
    mexErrMsgTxt("Error closing file: 'test_blob2sino_perf_data.mat'");
    return 1;
  }

  if (!mxIsCell(sinos_cells)) {
    mexErrMsgIdAndTxt(transforms_error_id,
        "The first argument should be a Cell array (Sinogram sizes)");
    return 1;
  }
  if (!mxIsCell(blobs_cells)) {
    mexErrMsgIdAndTxt(transforms_error_id,
        "The second argument should be a Cell array (Blobs)");
    return 1;
  }


  const mxClassID datatype = dct::DctMatlabData::get_class_of_cell(blobs_cells);

  if (datatype == mxUNKNOWN_CLASS)
  {
    mexErrMsgIdAndTxt(transforms_error_id,
        "The blobs need to be a non empty Cell array of coherent floating point types");
    return 1;
  }

//  CALLGRIND_START_INSTRUMENTATION;
  CALLGRIND_TOGGLE_COLLECT;

  switch (datatype)
  {
    case mxDOUBLE_CLASS:
    {
      dct::DctProjectionTransform<double, dct::MatlabAllocator<double> > transformer(num_threads);

      try {
        auto && metad = dct::DctMatlabData::import_coefficients_list<double>(coeffs);
        transformer.set_transforms(metad);

        auto && sinos = dct::DctMatlabData::import_collection<double>((mxArray *)sinos_cells);
        transformer.set_sinos(sinos);

        auto && blobs = dct::DctMatlabData::import_collection<double>(blobs_cells);
        transformer.set_blobs(blobs);

        transformer.check_consistency();

      } catch (const dct::BasicException & e) {
        mexErrMsgIdAndTxt(transforms_error_id, e.what());
        return 1;
      }

      transformer.produce_blobs();
      break;
    }
    case mxSINGLE_CLASS:
    {
      std::chrono::system_clock::time_point t1, t2, t3, t4, t5, t6, t7, t8;

      t1 = std::chrono::high_resolution_clock::now();

      dct::DctProjectionTransform<float, dct::MatlabAllocator<float> > transformer(num_threads);

      t2 = std::chrono::high_resolution_clock::now();

      try {
        auto && metad = dct::DctMatlabData::import_coefficients_list<float>(coeffs);
        transformer.set_transforms(metad);

        t3 = std::chrono::high_resolution_clock::now();

        auto && sinos = dct::DctMatlabData::import_collection<float>((mxArray *)sinos_cells);
        transformer.set_sinos(sinos);

        t4 = std::chrono::high_resolution_clock::now();

        auto && blobs = dct::DctMatlabData::import_collection<float>(blobs_cells);
        transformer.set_blobs(blobs);

        t5 = std::chrono::high_resolution_clock::now();

        transformer.check_consistency();

        t6 = std::chrono::high_resolution_clock::now();

      } catch (const dct::BasicException & e) {
        mexErrMsgIdAndTxt(transforms_error_id, e.what());
        return 1;
      }

      transformer.produce_blobs();

      t7 = std::chrono::high_resolution_clock::now();

      const std::chrono::duration<double, std::milli> d1 = t2-t1;
      const std::chrono::duration<double, std::milli> d2 = t3-t2;
      const std::chrono::duration<double, std::milli> d3 = t4-t3;
      const std::chrono::duration<double, std::milli> d4 = t5-t4;
      const std::chrono::duration<double, std::milli> d5 = t6-t5;
      const std::chrono::duration<double, std::milli> d6 = t7-t6;

      printf("Times: %f, %f, %f, %f, %f, %f\n", d1.count(), d2.count(), d3.count(), d4.count(), d5.count(), d6.count());

      break;
    }
    default:
    {
      mexErrMsgIdAndTxt(transforms_error_id,
          "The argument needs to be a Cell array of floating point numbers");
      return 1;
    }
  }

  CALLGRIND_TOGGLE_COLLECT;
//  CALLGRIND_STOP_INSTRUMENTATION;

  MATFile * mfPtrOut = matOpen(filename_out, "w");
  if (mfPtrOut == nullptr) {
    mexErrMsgTxt("Error opening file: 'test_blob2sino_perf_data_out.mat'");
    return 1;
  }

  int status = matPutVariable(mfPtrOut, "blobs", blobs_cells);
  if (status != 0) {
      printf("%s :  Error using matPutVariable on line %d\n", __FILE__, __LINE__);
      return(EXIT_FAILURE);
  }

  if (matClose(mfPtrOut) != 0) {
    mexErrMsgTxt("Error closing file: 'test_blob2sino_perf_data_out.mat'");
    return 1;
  }

  return 0;
}


