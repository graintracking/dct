/*
 * gtCxxPlaceSubVolumeAssign.cpp
 *
 *  Created on: Jan 28, 2015
 *      Author: vigano
 */

#include "gtCxxPlaceSubVolOps.h"

void mexFunction( int nlhs, mxArray * plhs[], int nrhs, const mxArray * prhs[] )
{
  if (nrhs < 5) {
    mexErrMsgIdAndTxt(GT3D::place_subvol_error_id,
        "Not enough arguments!");
    return;
  }

  if (nrhs >= 6)
  {
    const mxArray * const num_threads = prhs[5];
    initialize_multithreading(*mxGetPr(num_threads));
  }
  else
  {
    initialize_multithreading();
  }

  const mxArray * const mat_input = prhs[1];
  const mxArray * const mat_shifts_op = prhs[2];
  const mxArray * const mat_shifts_ip = prhs[3];
  const mxArray * const mat_dims = prhs[4];

  /* Don't copy, just modify the incoming matrix */
  plhs[0] = mxCreateSharedDataCopy(prhs[0]);
  mxArray * const mat_output = plhs[0];

  //  mxArray * const mat_output = (mxArray *)prhs[0];
  //  mxUnshareArray(mat_output, true);

  GT3D::GtMatlabSubVolumePlacer placer(mat_output, mat_input, mat_shifts_op,
      mat_shifts_ip, mat_dims);

  placer.loop(GT3D::ASSIGN);
}
