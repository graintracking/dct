/*
 * exceptions.h
 *
 *  Created on: Jan 22, 2017
 *      Author: Nicola Vigano', 2017, CWI, N.R.Vigano@cwi.nl
 */

#ifndef ZUTIL_CXX_INCLUDE_EXCEPTIONS_H_
#define ZUTIL_CXX_INCLUDE_EXCEPTIONS_H_

#include <stdexcept>
#include <string>

namespace dct {

  class BasicException : public std::exception {
    std::string message;
  public:
    BasicException() = default;
    BasicException(const char * _mess) : message(_mess) { }
    BasicException(const std::string& _mess) : message(_mess) { }
    BasicException(const BasicException& _ex) : message(_ex.message) { }

    virtual ~BasicException() throw() = default;

    virtual const char * what() const throw() { return message.c_str(); }
    const std::string& getMessage() const throw() { return message; }

    virtual void setMessage(const std::string & _mess) { message = _mess; }
    virtual void appendMessage(const std::string & _mess) { message += _mess; }
    virtual void prefixMessage(const std::string & _mess) { message = _mess + message; }
  };

  class WrongArgumentException : public BasicException {
  public:
    WrongArgumentException() = default;
    WrongArgumentException(const char * _mess) : BasicException(_mess) { }
    WrongArgumentException(const std::string & _mess) : BasicException(_mess) { }
  };

#define THROW(exception, cl, message) \
  throw exception(std::string(#cl) + ", line " + std::to_string(__LINE__) + ", error: " + message)

}  // namespace dct

#endif /* ZUTIL_CXX_INCLUDE_EXCEPTIONS_H_ */
