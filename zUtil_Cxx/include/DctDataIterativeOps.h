/*
 * DctDataIterativeOps.h
 *
 *  Created on: Sep 10, 2017
 *      Author: Nicola Vigano', 2017, CWI, N.R.Vigano@cwi.nl
 */

#ifndef ZUTIL_CXX_INCLUDE_GT6DOPS_H_
#define ZUTIL_CXX_INCLUDE_GT6DOPS_H_

#include "DctDataAlgorithms.h"

namespace dct {

  //////////////////////////////////////////////////////////////////////////////
  // Operators

  template<typename Type>
  class OpBiBinaryUpdatePrimal {
  public:
    template<const size_t vector_size>
    using VecType = SIMDRegister<Type, vector_size>;

    OpBiBinaryUpdatePrimal(const Type & _tau) : op_non_neg(0.0), op_times_tau(_tau) { }

    const Type
    operator()(Type & solution, const Type & correction_tomo) const throw()
    {
      const Type new_solution = op_non_neg(solution + op_times_tau(correction_tomo));
      const Type new_enh_solution = new_solution + (new_solution - solution);
      solution = new_solution;
      return new_enh_solution;
    }
    template<const size_t vector_size>
    const VecType<vector_size>
    operator()(VecType<vector_size> & solution, const VecType<vector_size> & correction_tomo) const throw()
    {
      const VecType<vector_size> new_solution = op_non_neg(solution + op_times_tau(correction_tomo));
      const VecType<vector_size> new_enh_solution = new_solution + (new_solution - solution);
      solution = new_solution;
      return new_enh_solution;
    }
  protected:
    const OpUnaryMaxScalar<Type> op_non_neg;
    const OpUnaryTimesScalar<Type> op_times_tau;
  };

  template<typename Type>
  class OpNaryUpdateDualDetector : public OpBase {
  public:
    OpType get_type() const { return OpType::Nary; }
  };

  template<typename Type>
  class OpNaryUpdateDualDetectorL2 : public OpNaryUpdateDualDetector<Type> {
  public:
    template<const size_t vector_size>
    using VecType = SIMDRegister<Type, vector_size>;

    const Type
    operator()(const Type & dual, const Type & blobs,
        const Type & computed_blobs, const Type & sigma1,
        const Type & sigma1_1) const throw()
    {
      return (dual + (computed_blobs - blobs) * sigma1) * sigma1_1;
    }
    template<const size_t vector_size>
    const VecType<vector_size>
    operator()(const VecType<vector_size> & dual, const VecType<vector_size> & blobs,
        const VecType<vector_size> & computed_blobs, const VecType<vector_size> & sigma1,
        const VecType<vector_size> & sigma1_1) const throw()
    {
      return (dual + (computed_blobs - blobs) * sigma1) * sigma1_1;
    }
  };

  template<typename Type>
  class OpNaryUpdateDualDetectorL1 : public OpNaryUpdateDualDetector<Type> {
  public:
    template<const size_t vector_size>
    using VecType = SIMDRegister<Type, vector_size>;

    OpNaryUpdateDualDetectorL1() : op_box_1() { }

    const Type
    operator()(const Type & dual, const Type & blobs,
        const Type & computed_blobs, const Type & sigma) const throw()
    {
      return op_box_1(dual + (computed_blobs - blobs) * sigma);
    }
    template<const size_t vector_size>
    const VecType<vector_size>
    operator()(const VecType<vector_size> & dual, const VecType<vector_size> & blobs,
        const VecType<vector_size> & computed_blobs, const VecType<vector_size> & sigma) const throw()
    {
      return op_box_1(dual + (computed_blobs - blobs) * sigma);
    }
  protected:
    const OpUnaryBoxOneL1<Type> op_box_1;
  };

  template<typename Type>
  class OpUnaryPow2 : public OpUnary {
  public:
    template<const size_t vector_size>
    using VecType = SIMDRegister<Type, vector_size>;

    OpUnaryPow2() : op_times() { }

    const Type
    operator()(const Type & data) const throw()
    {
      return op_times(data, data);
    }
    template<const size_t vector_size>
    const VecType<vector_size>
    operator()(const VecType<vector_size> & data) const throw()
    {
      return op_times(data, data);
    }
  protected:
    const OpBinaryTimes<Type> op_times;
  };

  template<typename Type>
  class OpNaryUpdateDualDetectorKL : public OpNaryUpdateDualDetector<Type> {
  public:
    template<const size_t vector_size>
    using VecType = SIMDRegister<Type, vector_size>;

    OpNaryUpdateDualDetectorKL()
    : op_times_4(4.0), op_pow_2(), op_minus_1(1.0), op_sqrt(), op_plus_1(1.0)
    , op_times_05(0.5) { }

    const Type
    operator()(const Type & dual, const Type & blobs,
        const Type & computed_blobs, const Type & sigma) const throw()
    {
      const Type temp_p = dual + sigma * computed_blobs;
      const Type temp_bls = op_times_4(sigma * blobs);
      const Type temp_d = op_sqrt(op_pow_2(op_minus_1(temp_p)) + temp_bls);
      return op_times_05(op_plus_1(temp_p) - temp_d);
    }
    template<const size_t vector_size>
    const VecType<vector_size>
    operator()(const VecType<vector_size> & dual, const VecType<vector_size> & blobs,
        const VecType<vector_size> & computed_blobs, const VecType<vector_size> & sigma) const throw()
    {
      const VecType<vector_size> temp_p = dual + sigma * computed_blobs;
      const VecType<vector_size> temp_bls = op_times_4(sigma * blobs);
      const VecType<vector_size> temp_d = op_sqrt(op_pow_2(op_minus_1(temp_p)) + temp_bls);
      return op_times_05(op_plus_1(temp_p) - temp_d);
    }
  protected:
    const OpUnaryTimesScalar<Type> op_times_4;
    const OpUnaryPow2<Type> op_pow_2;
    const OpUnaryMinusScalar<Type> op_minus_1;
    const OpUnarySqrt<Type> op_sqrt;
    const OpUnaryPlusScalar<Type> op_plus_1;
    const OpUnaryTimesScalar<Type> op_times_05;
  };

  //////////////////////////////////////////////////////////////////////////////
  // Operation classes

  template<typename Type, class Alloc = VectorAllocator<Type>, const size_t vector_size = VECTOR_SIZE_SSE>
  class DctVolumeGridSolutionUpdate : public DctDataProcess<Type, Alloc, vector_size> {
  protected:
    DctVolumeGrid<Type, Alloc> solutions;
    DctVolumeGrid<Type, Alloc> solutions_enh;

  public:
    DctVolumeGridSolutionUpdate(const size_t & num_threads = std::thread::hardware_concurrency())
    : DctDataProcess<Type, Alloc, vector_size>(num_threads), solutions(), solutions_enh() { }
    DctVolumeGridSolutionUpdate(DctVolumeGrid<Type, Alloc> & sols, DctVolumeGrid<Type, Alloc> & sols_e, const size_t & num_threads = std::thread::hardware_concurrency())
    : DctDataProcess<Type, Alloc, vector_size>(num_threads), solutions(sols), solutions_enh(sols_e)
    { solutions.check_elementwise_compatibility(solutions_enh); }

    void set_solutions(DctVolumeGrid<Type, Alloc> & new_vols);
    void set_solutions_enh(DctVolumeGrid<Type, Alloc> & new_vols);
    DctVolumeGrid<Type, Alloc> & get_solutions() { return this->solutions; }
    const DctVolumeGrid<Type, Alloc> & get_solutions() const { return this->solutions; }
    DctVolumeGrid<Type, Alloc> & get_solutions_enh() { return this->solutions_enh; }
    const DctVolumeGrid<Type, Alloc> & get_solutions_enh() const { return this->solutions_enh; }

    void update(const DctVolumeGrid<Type, Alloc> & vols_in, const DctVolumeGrid<Type, Alloc> & taus);

  protected:
    template<class Op>
    void transform_line_bibinary(Type * __restrict const sol_ehn,
        Type * __restrict const inout, const Type * __restrict const in,
        const size_t & line_length, Op op);

    template<class Op>
    void op_element_wise_bibinary_inplace(Op op,
        DctData<Type, Alloc> & out, DctData<Type, Alloc> & inout,
        const DctData<Type, Alloc> & in, const ChunkSpecs & cs);
  };


  template<typename Type, class Alloc = VectorAllocator<Type>, const size_t vector_size = VECTOR_SIZE_SSE>
  class DctProjectionVectorDetectorUpdate : public DctDataProcess<Type, Alloc, vector_size> {
  protected:
    DctProjectionVector<Type, Alloc> detector_dual;

  public:
    DctProjectionVectorDetectorUpdate(const size_t & num_threads = std::thread::hardware_concurrency())
    : DctDataProcess<Type, Alloc, vector_size>(num_threads), detector_dual() { }
    DctProjectionVectorDetectorUpdate(DctProjectionVector<Type, Alloc> & det_dual, const size_t & num_threads = std::thread::hardware_concurrency())
    : DctDataProcess<Type, Alloc, vector_size>(num_threads), detector_dual(det_dual) { }

    void set_detector_dual(DctProjectionVector<Type, Alloc> & new_blobs);
    DctProjectionVector<Type, Alloc> & get_detector_dual() { return this->detector_dual; }
    const DctProjectionVector<Type, Alloc> & get_solutions() const { return this->detector_dual; }

    void update_l2(const DctProjectionVector<Type, Alloc> & blobs,
        const DctProjectionVector<Type, Alloc> & proj_blobs,
        const DctProjectionVector<Type, Alloc> & sigma,
        const DctProjectionVector<Type, Alloc> & sigma_1);
    void update_l1(const DctProjectionVector<Type, Alloc> & blobs,
        const DctProjectionVector<Type, Alloc> & proj_blobs,
        const DctProjectionVector<Type, Alloc> & sigma);
    void update_kl(const DctProjectionVector<Type, Alloc> & blobs,
        const DctProjectionVector<Type, Alloc> & proj_blobs,
        const DctProjectionVector<Type, Alloc> & sigma);

  protected:
    template<class Op>
    void transform_line_quinary(Type * __restrict const dual,
        const Type * __restrict const blobs,
        const Type * __restrict const proj_blobs,
        const Type * __restrict const sigma,
        const Type * __restrict const sigma_1,
        const size_t & line_length, Op op);
    template<class Op>
    void transform_line_quaternary(Type * __restrict const dual,
        const Type * __restrict const blobs,
        const Type * __restrict const proj_blobs,
        const Type * __restrict const sigma,
        const size_t & line_length, Op op);

    template<class Op>
    void op_element_wise_quinary_inplace(Op op,
        DctData<Type, Alloc> & out, const DctData<Type, Alloc> & blobs,
        const DctData<Type, Alloc> & proj_blobs,
        const DctData<Type, Alloc> & sigma,
        const DctData<Type, Alloc> & sigma1,
        const ChunkSpecs & cs);
    template<class Op>
    void op_element_wise_quaternary_inplace(Op op,
        DctData<Type, Alloc> & out, const DctData<Type, Alloc> & blobs,
        const DctData<Type, Alloc> & proj_blobs,
        const DctData<Type, Alloc> & sigma,
        const ChunkSpecs & cs);
  };

  //////////////////////////////////////////////////////////////////////////////
  // Implementation of Operation classes

  template<typename Type, class Alloc, const size_t vector_size>
  void
  DctVolumeGridSolutionUpdate<Type, Alloc, vector_size>::set_solutions(DctVolumeGrid<Type, Alloc> & new_vols)
  {
    if (new_vols.empty()) {
      THROW(WrongArgumentException, DctVolumeGridSolutionUpdate, "DctVolumeGridSolutionUpdate: an empty DctVolumeGrid was passed!");
    }
    this->solutions = new_vols;
  }

  template<typename Type, class Alloc, const size_t vector_size>
  void
  DctVolumeGridSolutionUpdate<Type, Alloc, vector_size>::set_solutions_enh(DctVolumeGrid<Type, Alloc> & new_vols)
  {
    if (new_vols.empty()) {
      THROW(WrongArgumentException, DctVolumeGridSolutionUpdate, "DctVolumeGridSolutionUpdate: an empty DctVolumeGrid was passed!");
    }
    solutions.check_elementwise_compatibility(new_vols);
    this->solutions_enh = new_vols;
  }

  template<typename Type, class Alloc, const size_t vector_size>
  template<class Op>
  inline void
  DctVolumeGridSolutionUpdate<Type, Alloc, vector_size>::transform_line_bibinary(Type * __restrict const out, Type * __restrict const inout, const Type * __restrict const in, const size_t & line_length, Op op)
  {
    const SIMDUnrolling<Type, vector_size> simd4(4);
    const SIMDUnrolling<Type, vector_size> simd1(1);

    using VecType = SIMDRegister<Type, vector_size>;

    const Type * __restrict const end_line_inout_unroll4 = inout + simd4.get_unroll(line_length);
    const Type * __restrict const end_line_inout_unroll1 = inout + simd1.get_unroll(line_length);
    const Type * __restrict const end_line_inout = inout + line_length;

    const Type * __restrict temp_in = in;

    Type * __restrict temp_inout = inout;
    Type * __restrict temp_out = out;

    for (; temp_inout < end_line_inout_unroll4;
        temp_out += simd4.block, temp_inout += simd4.block, temp_in += simd4.block)
    {
      VecType a0 = this->access_u.load(temp_inout + 0 * simd4.shift);
      VecType a1 = this->access_u.load(temp_inout + 1 * simd4.shift);
      VecType a2 = this->access_u.load(temp_inout + 2 * simd4.shift);
      VecType a3 = this->access_u.load(temp_inout + 3 * simd4.shift);

      const VecType & b0 = this->access_u.load(temp_in + 0 * simd4.shift);
      const VecType & b1 = this->access_u.load(temp_in + 1 * simd4.shift);
      const VecType & b2 = this->access_u.load(temp_in + 2 * simd4.shift);
      const VecType & b3 = this->access_u.load(temp_in + 3 * simd4.shift);

      const VecType c0 = op(a0, b0);
      const VecType c1 = op(a1, b1);
      const VecType c2 = op(a2, b2);
      const VecType c3 = op(a3, b3);

      this->access_u.store(temp_inout + 0 * simd4.shift, a0);
      this->access_u.store(temp_inout + 1 * simd4.shift, a1);
      this->access_u.store(temp_inout + 2 * simd4.shift, a2);
      this->access_u.store(temp_inout + 3 * simd4.shift, a3);

      this->access_u.store(temp_out + 0 * simd4.shift, c0);
      this->access_u.store(temp_out + 1 * simd4.shift, c1);
      this->access_u.store(temp_out + 2 * simd4.shift, c2);
      this->access_u.store(temp_out + 3 * simd4.shift, c3);
    }
    for (; temp_inout < end_line_inout_unroll1;
        temp_out += simd1.block, temp_inout += simd1.block, temp_in += simd1.block)
    {
      VecType a0 = this->access_u.load(temp_inout);
      const VecType & b0 = this->access_u.load(temp_in);
      const VecType c0 = op(a0, b0);
      this->access_u.store(temp_inout, a0);
      this->access_u.store(temp_out, c0);
    }
    for (; temp_inout < end_line_inout; temp_out++, temp_inout++, temp_in++)
    {
      *temp_out = op(*temp_inout, *temp_in);
    }
  }

  template<typename Type, class Alloc, const size_t vector_size>
  template<class Op>
  void
  DctVolumeGridSolutionUpdate<Type, Alloc, vector_size>::op_element_wise_bibinary_inplace(Op op, DctData<Type, Alloc> & out, DctData<Type, Alloc> & inout, const DctData<Type, Alloc> & in, const ChunkSpecs & cs)
  {
    const size_t tot_elems = inout.get_tot_elements();

    const size_t safe_chunk_start = std::min(std::max(cs.chunk_start, size_t(0)), tot_elems);
    const size_t safe_chunk_size = std::min(tot_elems - safe_chunk_start, cs.chunk_size);

    Type * __restrict const data_out = out.get_data() + safe_chunk_start;
    Type * __restrict const data_inout = inout.get_data() + safe_chunk_start;
    const Type * __restrict const data_in = in.get_data() + safe_chunk_start;

    this->transform_line_bibinary(data_out, data_inout, data_in, safe_chunk_size, op);
  }

  template<typename Type, class Alloc, const size_t vector_size>
  void
  DctVolumeGridSolutionUpdate<Type, Alloc, vector_size>::update(const DctVolumeGrid<Type, Alloc> & vols_in, const DctVolumeGrid<Type, Alloc> & taus)
  {
    solutions.check_elementwise_compatibility(vols_in);

    const size_t tot_elems_per_vol = this->solutions[0].get_tot_elements();
    const size_t tot_vols = solutions.get_tot_vols();

    std::vector<ChunkSpecs> chunks = this->partition_grid_volumes(tot_vols, tot_elems_per_vol);

    std::vector< std::function<void()> > funcs;
    funcs.reserve(chunks.size());

    for (const ChunkSpecs & cs : chunks) {
      DctData<Type, Alloc> & inout_vol = solutions[cs.data_ind];
      DctData<Type, Alloc> & out_vol = solutions_enh[cs.data_ind];

      const DctData<Type, Alloc> & in_vol = vols_in[cs.data_ind];

      const DctData<Type, Alloc> & tau_vol = taus[cs.data_ind];
      const OpBiBinaryUpdatePrimal<Type> op(tau_vol.get_data()[0]);

      // Change called function
      funcs.emplace_back(std::function<void()>(
          std::bind(&DctVolumeGridSolutionUpdate<Type, Alloc, vector_size>::op_element_wise_bibinary_inplace<OpBiBinaryUpdatePrimal<Type>>, this, op, out_vol, inout_vol, in_vol, cs)));
    }

    this->process_tasks(funcs);
  }

  template<typename Type, class Alloc, const size_t vector_size>
  void
  DctProjectionVectorDetectorUpdate<Type, Alloc, vector_size>::set_detector_dual(DctProjectionVector<Type, Alloc> & new_blobs)
  {
    if (new_blobs.empty()) {
      THROW(WrongArgumentException, DctProjectionTransform, "No blobs were passed!");
    }
    new_blobs.check_consistency();
    this->detector_dual = new_blobs;
  }

  template<typename Type, class Alloc, const size_t vector_size>
  template<class Op>
  inline void
  DctProjectionVectorDetectorUpdate<Type, Alloc, vector_size>::transform_line_quinary(
      Type * __restrict const dual,
      const Type * __restrict const blobs,
      const Type * __restrict const proj_blobs,
      const Type * __restrict const sigma,
      const Type * __restrict const sigma_1,
      const size_t & line_length, Op op)
  {
    const SIMDUnrolling<Type, vector_size> simd2(2);
    const SIMDUnrolling<Type, vector_size> simd1(1);

    using VecType = SIMDRegister<Type, vector_size>;

    const Type * __restrict const end_line_dual_unroll4 = dual + simd2.get_unroll(line_length);
    const Type * __restrict const end_line_dual_unroll1 = dual + simd1.get_unroll(line_length);
    const Type * __restrict const end_line_dual = dual + line_length;

    const Type * __restrict temp_bls = blobs;
    const Type * __restrict temp_prj_bls = proj_blobs;

    const Type * __restrict temp_sigma = sigma;
    const Type * __restrict temp_sigma_1 = sigma_1;

    Type * __restrict temp_dual = dual;

    for (; temp_dual < end_line_dual_unroll4; temp_dual += simd2.block,
        temp_bls += simd2.block, temp_prj_bls += simd2.block,
        temp_sigma += simd2.block, temp_sigma_1 += simd2.block)
    {
      const VecType & a0 = this->access_u.load(temp_dual + 0 * simd2.shift);
      const VecType & a1 = this->access_u.load(temp_dual + 1 * simd2.shift);

      const VecType & b0 = this->access_u.load(temp_bls + 0 * simd2.shift);
      const VecType & b1 = this->access_u.load(temp_bls + 1 * simd2.shift);

      const VecType & c0 = this->access_u.load(temp_prj_bls + 0 * simd2.shift);
      const VecType & c1 = this->access_u.load(temp_prj_bls + 1 * simd2.shift);

      const VecType & d0 = this->access_u.load(temp_sigma + 0 * simd2.shift);
      const VecType & d1 = this->access_u.load(temp_sigma + 1 * simd2.shift);

      const VecType & e0 = this->access_u.load(temp_sigma_1 + 0 * simd2.shift);
      const VecType & e1 = this->access_u.load(temp_sigma_1 + 1 * simd2.shift);

      const VecType f0 = op(a0, b0, c0, d0, e0);
      const VecType f1 = op(a1, b1, c1, d1, e1);

      this->access_u.store(temp_dual + 0 * simd2.shift, f0);
      this->access_u.store(temp_dual + 1 * simd2.shift, f1);
    }
    for (; temp_dual < end_line_dual_unroll1;temp_dual += simd1.block,
        temp_bls += simd1.block, temp_prj_bls += simd1.block,
        temp_sigma += simd1.block, temp_sigma_1 += simd1.block)
    {
      const VecType & a0 = this->access_u.load(temp_dual);
      const VecType & b0 = this->access_u.load(temp_bls);
      const VecType & c0 = this->access_u.load(temp_prj_bls);
      const VecType & d0 = this->access_u.load(temp_sigma);
      const VecType & e0 = this->access_u.load(temp_sigma_1);

      const VecType f0 = op(a0, b0, c0, d0, e0);

      this->access_u.store(temp_dual, f0);
    }
    for (; temp_dual < end_line_dual;
        temp_dual++, temp_bls++, temp_prj_bls++, temp_sigma++, temp_sigma_1++)
    {
      *temp_dual = op(*temp_dual, *temp_bls, *temp_prj_bls, *temp_sigma, *temp_sigma_1);
    }
  }

  template<typename Type, class Alloc, const size_t vector_size>
  template<class Op>
  inline void
  DctProjectionVectorDetectorUpdate<Type, Alloc, vector_size>::transform_line_quaternary(
      Type * __restrict const dual,
      const Type * __restrict const blobs,
      const Type * __restrict const proj_blobs,
      const Type * __restrict const sigma,
      const size_t & line_length, Op op)
  {
    const SIMDUnrolling<Type, vector_size> simd2(2);
    const SIMDUnrolling<Type, vector_size> simd1(1);

    using VecType = SIMDRegister<Type, vector_size>;

    const Type * __restrict const end_line_dual_unroll2 = dual + simd2.get_unroll(line_length);
    const Type * __restrict const end_line_dual_unroll1 = dual + simd1.get_unroll(line_length);
    const Type * __restrict const end_line_dual = dual + line_length;

    const Type * __restrict temp_bls = blobs;
    const Type * __restrict temp_prj_bls = proj_blobs;

    const Type * __restrict temp_sigma = sigma;

    Type * __restrict temp_dual = dual;

    for (; temp_dual < end_line_dual_unroll2; temp_dual += simd2.block,
        temp_bls += simd2.block, temp_prj_bls += simd2.block,
        temp_sigma += simd2.block)
    {
      const VecType & a0 = this->access_u.load(temp_dual + 0 * simd2.shift);
      const VecType & a1 = this->access_u.load(temp_dual + 1 * simd2.shift);

      const VecType & b0 = this->access_u.load(temp_bls + 0 * simd2.shift);
      const VecType & b1 = this->access_u.load(temp_bls + 1 * simd2.shift);

      const VecType & c0 = this->access_u.load(temp_prj_bls + 0 * simd2.shift);
      const VecType & c1 = this->access_u.load(temp_prj_bls + 1 * simd2.shift);

      const VecType & d0 = this->access_u.load(temp_sigma + 0 * simd2.shift);
      const VecType & d1 = this->access_u.load(temp_sigma + 1 * simd2.shift);

      const VecType f0 = op(a0, b0, c0, d0);
      const VecType f1 = op(a1, b1, c1, d1);

      this->access_u.store(temp_dual + 0 * simd2.shift, f0);
      this->access_u.store(temp_dual + 1 * simd2.shift, f1);
    }
    for (; temp_dual < end_line_dual_unroll1;temp_dual += simd1.block,
        temp_bls += simd1.block, temp_prj_bls += simd1.block,
        temp_sigma += simd1.block)
    {
      const VecType & a0 = this->access_u.load(temp_dual);
      const VecType & b0 = this->access_u.load(temp_bls);
      const VecType & c0 = this->access_u.load(temp_prj_bls);
      const VecType & d0 = this->access_u.load(temp_sigma);

      const VecType f0 = op(a0, b0, c0, d0);

      this->access_u.store(temp_dual, f0);
    }
    for (; temp_dual < end_line_dual;
        temp_dual++, temp_bls++, temp_prj_bls++, temp_sigma++)
    {
      *temp_dual = op(*temp_dual, *temp_bls, *temp_prj_bls, *temp_sigma);
    }
  }

  template<typename Type, class Alloc, const size_t vector_size>
  template<class Op>
  void
  DctProjectionVectorDetectorUpdate<Type, Alloc, vector_size>::op_element_wise_quinary_inplace(Op op,
      DctData<Type, Alloc> & dual, const DctData<Type, Alloc> & blobs,
      const DctData<Type, Alloc> & proj_blobs,
      const DctData<Type, Alloc> & sigma,
      const DctData<Type, Alloc> & sigma1,
      const ChunkSpecs & cs)
  {
    const size_t tot_elems = dual.get_tot_elements();

    const size_t safe_chunk_start = std::min(std::max(cs.chunk_start, size_t(0)), tot_elems);
    const size_t safe_chunk_size = std::min(tot_elems - safe_chunk_start, cs.chunk_size);

    Type * __restrict const data_dual = dual.get_data() + safe_chunk_start;
    const Type * __restrict const data_bls = blobs.get_data() + safe_chunk_start;
    const Type * __restrict const data_prj_bls = proj_blobs.get_data() + safe_chunk_start;
    const Type * __restrict const data_sigma = sigma.get_data() + safe_chunk_start;
    const Type * __restrict const data_sigma1 = sigma1.get_data() + safe_chunk_start;

    this->transform_line_quinary(data_dual, data_bls, data_prj_bls, data_sigma, data_sigma1, safe_chunk_size, op);
  }

  template<typename Type, class Alloc, const size_t vector_size>
  template<class Op>
  void
  DctProjectionVectorDetectorUpdate<Type, Alloc, vector_size>::op_element_wise_quaternary_inplace(Op op,
      DctData<Type, Alloc> & dual,
      const DctData<Type, Alloc> & blobs,
      const DctData<Type, Alloc> & proj_blobs,
      const DctData<Type, Alloc> & sigma,
      const ChunkSpecs & cs)
  {
    const size_t tot_elems = dual.get_tot_elements();

    const size_t safe_chunk_start = std::min(std::max(cs.chunk_start, size_t(0)), tot_elems);
    const size_t safe_chunk_size = std::min(tot_elems - safe_chunk_start, cs.chunk_size);

    Type * __restrict const data_dual = dual.get_data() + safe_chunk_start;
    const Type * __restrict const data_bls = blobs.get_data() + safe_chunk_start;
    const Type * __restrict const data_prj_bls = proj_blobs.get_data() + safe_chunk_start;
    const Type * __restrict const data_sigma = sigma.get_data() + safe_chunk_start;

    this->transform_line_quaternary(data_dual, data_bls, data_prj_bls, data_sigma, safe_chunk_size, op);
  }

  template<typename Type, class Alloc, const size_t vector_size>
  void
  DctProjectionVectorDetectorUpdate<Type, Alloc, vector_size>::update_l2(
      const DctProjectionVector<Type, Alloc> & blobs,
      const DctProjectionVector<Type, Alloc> & proj_blobs,
      const DctProjectionVector<Type, Alloc> & sigma,
      const DctProjectionVector<Type, Alloc> & sigma_1)
  {
    this->detector_dual.check_consistency(blobs);
    this->detector_dual.check_consistency(proj_blobs);
    this->detector_dual.check_consistency(sigma);
    this->detector_dual.check_consistency(sigma_1);

    const size_t tot_vols = detector_dual.size();

    const std::vector<size_t> tot_elems_each_vol = this->detector_dual.get_tot_elements_each_proj();
    std::vector<ChunkSpecs> chunks = this->partition_proj_vector(tot_vols, tot_elems_each_vol);

    std::vector< std::function<void()> > funcs;
    funcs.reserve(chunks.size());

    for (const ChunkSpecs & cs : chunks) {
      DctData<Type, Alloc> & det_dual = this->detector_dual[cs.data_ind];
      const DctData<Type, Alloc> & bls = blobs[cs.data_ind];
      const DctData<Type, Alloc> & prj_bls = proj_blobs[cs.data_ind];
      const DctData<Type, Alloc> & sig = sigma[cs.data_ind];
      const DctData<Type, Alloc> & sig_1 = sigma_1[cs.data_ind];

      const OpNaryUpdateDualDetectorL2<Type> op;

      // Change called function
      funcs.emplace_back(std::function<void()>(
          std::bind(&DctProjectionVectorDetectorUpdate<Type, Alloc, vector_size>::op_element_wise_quinary_inplace<OpNaryUpdateDualDetectorL2<Type>>,
              this, op, det_dual, bls, prj_bls, sig, sig_1, cs)));
    }

    this->process_tasks(funcs);
  }

  template<typename Type, class Alloc, const size_t vector_size>
  void
  DctProjectionVectorDetectorUpdate<Type, Alloc, vector_size>::update_l1(
      const DctProjectionVector<Type, Alloc> & blobs,
      const DctProjectionVector<Type, Alloc> & proj_blobs,
      const DctProjectionVector<Type, Alloc> & sigma)
  {
    this->detector_dual.check_consistency(blobs);
    this->detector_dual.check_consistency(proj_blobs);
    this->detector_dual.check_consistency(sigma);

    const size_t tot_vols = detector_dual.size();

    const std::vector<size_t> tot_elems_each_vol = this->detector_dual.get_tot_elements_each_proj();
    std::vector<ChunkSpecs> chunks = this->partition_proj_vector(tot_vols, tot_elems_each_vol);

    std::vector< std::function<void()> > funcs;
    funcs.reserve(chunks.size());

    for (const ChunkSpecs & cs : chunks) {
      DctData<Type, Alloc> & det_dual = this->detector_dual[cs.data_ind];
      const DctData<Type, Alloc> & bls = blobs[cs.data_ind];
      const DctData<Type, Alloc> & prj_bls = proj_blobs[cs.data_ind];
      const DctData<Type, Alloc> & sig = sigma[cs.data_ind];

      const OpNaryUpdateDualDetectorL1<Type> op;

      // Change called function
      funcs.emplace_back(std::function<void()>(
          std::bind(&DctProjectionVectorDetectorUpdate<Type, Alloc, vector_size>::op_element_wise_quaternary_inplace<OpNaryUpdateDualDetectorL1<Type>>,
              this, op, det_dual, bls, prj_bls, sig, cs)));
    }

    this->process_tasks(funcs);
  }

  template<typename Type, class Alloc, const size_t vector_size>
  void
  DctProjectionVectorDetectorUpdate<Type, Alloc, vector_size>::update_kl(
      const DctProjectionVector<Type, Alloc> & blobs,
      const DctProjectionVector<Type, Alloc> & proj_blobs,
      const DctProjectionVector<Type, Alloc> & sigma)
  {
    this->detector_dual.check_consistency(blobs);
    this->detector_dual.check_consistency(proj_blobs);
    this->detector_dual.check_consistency(sigma);

    const size_t tot_vols = detector_dual.size();

    const std::vector<size_t> tot_elems_each_vol = this->detector_dual.get_tot_elements_each_proj();
    std::vector<ChunkSpecs> chunks = this->partition_proj_vector(tot_vols, tot_elems_each_vol);

    std::vector< std::function<void()> > funcs;
    funcs.reserve(chunks.size());

    for (const ChunkSpecs & cs : chunks) {
      DctData<Type, Alloc> & det_dual = this->detector_dual[cs.data_ind];
      const DctData<Type, Alloc> & bls = blobs[cs.data_ind];
      const DctData<Type, Alloc> & prj_bls = proj_blobs[cs.data_ind];
      const DctData<Type, Alloc> & sig = sigma[cs.data_ind];

      const OpNaryUpdateDualDetectorKL<Type> op;

      // Change called function
      funcs.emplace_back(std::function<void()>(
          std::bind(&DctProjectionVectorDetectorUpdate<Type, Alloc, vector_size>::op_element_wise_quaternary_inplace<OpNaryUpdateDualDetectorKL<Type>>,
              this, op, det_dual, bls, prj_bls, sig, cs)));
    }

    this->process_tasks(funcs);
  }

}  // namespace dct

#endif /* ZUTIL_CXX_INCLUDE_GT6DOPS_H_ */
