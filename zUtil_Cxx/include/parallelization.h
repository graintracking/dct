/*
 * parallelization.h
 *
 *  Created on: Jan 25, 2017
 *      Author: Nicola Vigano', 2017, CWI, N.R.Vigano@cwi.nl
 */

#ifndef ZUTIL_CXX_INCLUDE_PARALLELIZATION_H_
#define ZUTIL_CXX_INCLUDE_PARALLELIZATION_H_


#include <queue>
#include <memory>
#include <mutex>
#include <condition_variable>

#include <thread>
#include <functional>
#include <atomic>

namespace dct {

  template<typename Type>
    class ThreadSafeQueue {
    private:
      mutable std::mutex mut;
      std::queue<Type> data_queue;
      std::condition_variable data_cond;

    public:
      ThreadSafeQueue() { }
      ThreadSafeQueue(ThreadSafeQueue const & other) {
        std::lock_guard<std::mutex> lk(other.mut);
        data_queue = other.data_queue;
      }

      void push(const Type & new_value) {
        std::lock_guard<std::mutex> lk(mut);
        data_queue.emplace(new_value);
        data_cond.notify_one();
      }

      void push(const std::vector<Type> & new_values) {
        std::lock_guard<std::mutex> lk(mut);
        for (const auto & item : new_values) {
          data_queue.emplace(item);
        }
        data_cond.notify_one();
      }

      bool try_pop(Type & value) {
        std::lock_guard<std::mutex> lk(mut);
        if (data_queue.empty()) {
          return false;
        } else {
          value = data_queue.front();
          data_queue.pop();
          return true;
        }
      }

      bool empty() const {
        std::lock_guard<std::mutex> lk(mut);
        return data_queue.empty();
      }
    };

  class JoinThreads {
    std::vector<std::thread> & threads;
  public:
    explicit
    JoinThreads(std::vector<std::thread> & threads_)
    : threads(threads_) { }

    ~JoinThreads() {
      for (size_t t = 0; t < threads.size(); t++) {
        if (threads[t].joinable()) {
          threads[t].join();
        }
      }
    }
  };

  class ThreadPool {
    std::atomic_bool done;

    std::vector<std::thread> threads;
    JoinThreads joiner;

    ThreadSafeQueue< std::function<void()> > work_queue;

    void
    worker_thread()
    {
      while (!done) {
        std::function < void() > task;
        if (work_queue.try_pop(task)) {
          task();
        } else {
          std::this_thread::yield();
        }
      }
    }

  public:
    ThreadPool(const size_t & tot_threads = std::thread::hardware_concurrency())
    : done(false), joiner(threads)
    {
      try {
        for (size_t t = 0; t < tot_threads; t++) {
          threads.push_back(std::thread(&ThreadPool::worker_thread, this));
        }
      } catch (...) {
        done = true;
        throw;
      }
    }

    ~ThreadPool() {
      done = true;
    }

    template<typename FunctionType>
    void
    submit_multiple(const std::vector<FunctionType> & fs) {
      work_queue.push(fs);
    }

    template<typename FunctionType>
    void
    submit(FunctionType & f) {
      work_queue.push(std::function < void() > (f));
    }

    bool work_queue_is_empty() const { return work_queue.empty(); }

    const size_t get_num_threads() const noexcept { return threads.size(); }
  };

}  // namespace dct



#endif /* ZUTIL_CXX_INCLUDE_PARALLELIZATION_H_ */
