/*
 * gtCxxPlaceSubVolOps.h
 *
 *  Created on: Jan 28, 2015
 *      Author: vigano
 */

#ifndef ZUTIL_CXX_INCLUDE_GTCXXPLACESUBVOLOPS_H_
#define ZUTIL_CXX_INCLUDE_GTCXXPLACESUBVOLOPS_H_

#include <algorithm>
#include <omp.h>
#include "mex.h"

/* Let's take care of MS VisualStudio before 2010 that don't ship with stdint */
#if defined(_MSC_VER) && _MSC_VER < 1600
  typedef __int8 int8_t;
  typedef unsigned __int8 uint8_t;
  typedef __int32 int32_t;
  typedef unsigned __int32 uint32_t;
  typedef __int64 int64_t;
  typedef unsigned __int64 uint64_t;
#else
# include <stdint.h>
#endif

#if (MX_API_VER < 0x07400000)
extern "C"
{
  bool mxUnshareArray(mxArray *array_ptr, bool noDeepCopy);
  mxArray *mxCreateSharedCopy(const mxArray *pr);
  mxArray *mxCreateSharedDataCopy(const mxArray *pr);
  mxArray *mxUnreference(const mxArray *pr);
}
#endif

void
initialize_multithreading(const double & suggested_num_threads = 0)
{
#ifndef DEBUG
  if (suggested_num_threads > 0)
  {
    int num_threads = (const int)suggested_num_threads;
    num_threads = std::min(num_threads, omp_get_num_procs());
    num_threads = std::max(num_threads, 1);
    omp_set_num_threads(num_threads);
  }
  else
  {
    omp_set_num_threads(std::max(omp_get_num_procs()/4*3, 1));
  }
#endif
}

# define ROUND_DOWN(x, s) ((x) & ~((s)-1))

namespace GT3D {

  enum PLACER_OP {
    ASSIGN,
    SUM,
    INTERF
  };

  const char * place_subvol_error_id = "C_FUN:gtCxxPlaceSubVol:wrong_argument";

  template<typename Type>
  class assign_voxel {
  public:
    void
    operator()(Type & voxel_op, const Type & voxel_ip) const throw()
    {
      if (voxel_ip)
      {
        voxel_op = voxel_ip;
      }
    }
  protected:
  };

  template<typename Type>
  class sum_voxel {
  public:
    void
    operator()(Type & voxel_op, const Type & voxel_ip) const throw()
    {
      if (voxel_ip)
      {
        voxel_op += voxel_ip;
      }
    }
  protected:
  };

  template<typename Type>
  class interf_voxel {
  public:
    void
    operator()(Type & voxel_op, const Type & voxel_ip) const throw()
    {
      if (voxel_ip)
      {
        voxel_op = (-1 + (! voxel_op) * (voxel_ip + 1));
      }
    }
  protected:
  };

  template<>
  void
  interf_voxel<float>::operator()(float & voxel_op, const float & voxel_ip) const throw()
  {
    if (voxel_ip)
    {
      voxel_op = (-1.0f + (! voxel_op) * (voxel_ip + 1.0f));
    }
  }

  template<>
  void
  interf_voxel<double>::operator()(double & voxel_op, const double & voxel_ip) const throw()
  {
    if (voxel_ip)
    {
      voxel_op = (-1.0 + (! voxel_op) * (voxel_ip + 1.0f));
    }
  }

  class GtMatlabSubVolumePlacer {
  protected:
    mxArray * const big_vol;
    const mxArray * const sub_vol;
    const mxArray * const shift_big_vol;
    const mxArray * const shift_sub_vol;
    const mxArray * const mat_chunk_dims;
  public:
    GtMatlabSubVolumePlacer(mxArray * const _big_vol,
        const mxArray * const _sub_vol,
        const mxArray * const _shift_big_vol,
        const mxArray * const _shift_sub_vol,
        const mxArray * const _chunk_dims)
      : big_vol(_big_vol), sub_vol(_sub_vol)
      , shift_big_vol(_shift_big_vol), shift_sub_vol(_shift_sub_vol)
      , mat_chunk_dims(_chunk_dims)
    { }

    void
    loop(const PLACER_OP op)
    {
      switch(mxGetClassID(big_vol))
      {
        case mxDOUBLE_CLASS:
        {
          this->loop_op<double>(op);
          break;
        }
        case mxSINGLE_CLASS:
        {
          this->loop_op<float>(op);
          break;
        }
        case mxINT8_CLASS:
        {
          this->loop_op<int8_t>(op);
          break;
        }
        case mxUINT8_CLASS:
        {
          this->loop_op<uint8_t>(op);
          break;
        }
        case mxINT16_CLASS:
        {
          this->loop_op<int16_t>(op);
          break;
        }
        case mxUINT16_CLASS:
        {
          this->loop_op<uint16_t>(op);
          break;
        }
        case mxINT32_CLASS:
        {
          this->loop_op<int32_t>(op);
          break;
        }
        case mxUINT32_CLASS:
        {
          this->loop_op<uint32_t>(op);
          break;
        }
        case mxINT64_CLASS:
        {
          this->loop_op<int64_t>(op);
          break;
        }
        case mxUINT64_CLASS:
        {
          this->loop_op<uint64_t>(op);
          break;
        }
        default:
          break;
      }
    }

  protected:
    const mwSize compute_shift(const mwSize & num_dims,
        const mwSize * const dims, const double * const shifts) const
    {
      mwSize shift = 0;
      mwSize multiplier = 1;
      for (mwIndex dim = 0; dim < num_dims; dim++)
      {
        shift += multiplier * ((mwSize) shifts[dim]);
        multiplier *= dims[dim];
      }
      return shift;
    }

    template<typename Type>
    void
    loop_op(const PLACER_OP op)
    {
      switch (op)
      {
        case ASSIGN:
          this->loopOverVols<Type, assign_voxel>();
          break;
        case SUM:
          this->loopOverVols<Type, sum_voxel>();
          break;
        case INTERF:
          this->loopOverVols<Type, interf_voxel>();
          break;
        default:
          break;
      }
    }

    template<typename Type, template<typename TypeA> class action>
    void loopOverVols()
    {
      if (mxIsCell(sub_vol)) {
        this->loopOverCell3D<Type, action>();
      } else {
        this->loopOver3D<Type, action>();
      }
    }

    template<typename Type, template<typename TypeA> class action>
    void loopOver3D()
    {
      action<Type> act;

      const mwSize num_big_vol_dims = mxGetNumberOfDimensions(big_vol);
      const mwSize num_sub_vol_dims = mxGetNumberOfDimensions(sub_vol);

      const mwSize * big_vol_dims = mxGetDimensions(big_vol);
      const mwSize * sub_vol_dims = mxGetDimensions(sub_vol);

      const mwIndex initial_big_vol_shift = this->compute_shift(
          num_big_vol_dims, big_vol_dims, (double *)mxGetData(shift_big_vol));

      const mwIndex initial_sub_vol_shift = this->compute_shift(
          num_sub_vol_dims, sub_vol_dims, (double *)mxGetData(shift_sub_vol));

      Type * const big_vol_data = ((Type *) mxGetData(big_vol)) + initial_big_vol_shift;
      const Type * const sub_vol_data = ((const Type *) mxGetData(sub_vol)) + initial_sub_vol_shift;

      const mwSize big_vol_skip_dim_3 = big_vol_dims[0] * big_vol_dims[1];
      const mwSize big_vol_skip_dim_2 = big_vol_dims[0];

      const mwSize sub_vol_skip_dim_3 = sub_vol_dims[0] * sub_vol_dims[1];
      const mwSize sub_vol_skip_dim_2 = sub_vol_dims[0];

      const double * const chunk_dims_d = (double *) mxGetData(mat_chunk_dims);
      const mwSize chunk_dims[3] = {(mwSize)chunk_dims_d[0], (mwSize)chunk_dims_d[1], (mwSize)chunk_dims_d[2]};

      const mwSize line_length_unroll = ROUND_DOWN(chunk_dims[0], 4);

      /* These loops extensively use pointer arithmetics to determine the chuck
       * of the matrix to be computed */
#pragma omp parallel for
      for(mwIndex counter3 = 0; counter3 < chunk_dims[2]; counter3++)
      {
        /* Base vectors, which save computation of the 3rd dimension */
        Type * const base_big_vol_3 = big_vol_data + counter3 * big_vol_skip_dim_3;
        const Type * const base_sub_vol_3 = sub_vol_data + counter3 * sub_vol_skip_dim_3;

        for(mwIndex counter2 = 0; counter2 < chunk_dims[1]; counter2++)
        {
          /* Base vectors, which save computation of the 2nd dimension */
          Type * const base_big_vol_2 = base_big_vol_3 + counter2 * big_vol_skip_dim_2;
          const Type * const base_sub_vol_2 = base_sub_vol_3 + counter2 * sub_vol_skip_dim_2;

          /* Inner loop on the 1st dimension: the contiguous one in memory */
          for(mwIndex counter1 = 0; counter1 < line_length_unroll; counter1 += 4)
          {
            act(base_big_vol_2[counter1 + 0], base_sub_vol_2[counter1 + 0]);
            act(base_big_vol_2[counter1 + 1], base_sub_vol_2[counter1 + 1]);
            act(base_big_vol_2[counter1 + 2], base_sub_vol_2[counter1 + 2]);
            act(base_big_vol_2[counter1 + 3], base_sub_vol_2[counter1 + 3]);
          }
          for(mwIndex counter1 = line_length_unroll; counter1 < chunk_dims[0]; counter1++)
          {
            act(base_big_vol_2[counter1], base_sub_vol_2[counter1]);
          }
        }
      }
    }

    template<typename Type, template<typename TypeA> class action>
    void loopOverCell3D()
    {
      action<Type> act;

      const mwSize num_big_vol_dims = mxGetNumberOfDimensions(big_vol);
      const mwSize * big_vol_dims = mxGetDimensions(big_vol);

      const mwSize num_sub_vols = mxGetNumberOfElements(sub_vol);

      const double * data_shift_big_vol = (double *)mxGetData(shift_big_vol);
      const double * data_shift_sub_vol = (double *)mxGetData(shift_sub_vol);

      double big_vol_shifts[num_big_vol_dims];

      const mwSize big_vol_skip_dim_3 = big_vol_dims[0] * big_vol_dims[1];
      const mwSize big_vol_skip_dim_2 = big_vol_dims[0];

      const double * const chunk_dims_d = (double *) mxGetData(mat_chunk_dims);

//#if defined(__GNUC__) && (__GNUC__ > 4 || (__GNUC__ == 4 && __GNUC_MINOR__ >= 9))
//# pragma omp parallel for simd
//#else
//# pragma omp parallel for
//#endif
#pragma omp parallel for
      for (mwIndex vol_num = 0; vol_num < num_sub_vols; vol_num++)
      {
        const mxArray * const the_sub_vol = mxGetCell(sub_vol, vol_num);

        const mwSize num_sub_vol_dims = mxGetNumberOfDimensions(the_sub_vol);
        const mwSize * sub_vol_dims = mxGetDimensions(the_sub_vol);

        for (mwSize shift_ind = 0; shift_ind < num_big_vol_dims; shift_ind++)
        {
          big_vol_shifts[shift_ind] = data_shift_big_vol[vol_num + shift_ind * num_sub_vols];
        }
        const mwIndex initial_big_vol_shift = this->compute_shift(
            num_big_vol_dims, big_vol_dims, big_vol_shifts);

        double sub_vol_shifts[num_sub_vol_dims];
        for (mwSize shift_ind = 0; shift_ind < num_sub_vol_dims; shift_ind++)
        {
          sub_vol_shifts[shift_ind] = data_shift_sub_vol[vol_num + shift_ind * num_sub_vols];
        }
        const mwIndex initial_sub_vol_shift = this->compute_shift(
            num_sub_vol_dims, sub_vol_dims, sub_vol_shifts);

        Type * const big_vol_data = ((Type *) mxGetData(big_vol)) + initial_big_vol_shift;
        const Type * const sub_vol_data = ((const Type *) mxGetData(the_sub_vol)) + initial_sub_vol_shift;

        const mwSize sub_vol_skip_dim_3 = sub_vol_dims[0] * sub_vol_dims[1];
        const mwSize sub_vol_skip_dim_2 = sub_vol_dims[0];

        const mwSize chunk_dims[3] = {
            (mwSize)chunk_dims_d[0 * num_sub_vols + vol_num],
            (mwSize)chunk_dims_d[1 * num_sub_vols + vol_num],
            (mwSize)chunk_dims_d[2 * num_sub_vols + vol_num]};

        const mwSize line_length_unroll = ROUND_DOWN(chunk_dims[0], 4);

        /* These loops extensively use pointer arithmetics to determine the chuck
         * of the matrix to be computed */
        for(mwIndex counter3 = 0; counter3 < chunk_dims[2]; counter3++)
        {
          /* Base vectors, which save computation of the 3rd dimension */
          Type * const base_big_vol_3 = big_vol_data + counter3 * big_vol_skip_dim_3;
          const Type * const base_sub_vol_3 = sub_vol_data + counter3 * sub_vol_skip_dim_3;

          for(mwIndex counter2 = 0; counter2 < chunk_dims[1]; counter2++)
          {
            /* Base vectors, which save computation of the 2nd dimension */
            Type * const base_big_vol_2 = base_big_vol_3 + counter2 * big_vol_skip_dim_2;
            const Type * const base_sub_vol_2 = base_sub_vol_3 + counter2 * sub_vol_skip_dim_2;

            /* Inner loop on the 1st dimension: the contiguous one in memory */
            for(mwIndex counter1 = 0; counter1 < line_length_unroll; counter1 += 4)
            {
              act(base_big_vol_2[counter1 + 0], base_sub_vol_2[counter1 + 0]);
              act(base_big_vol_2[counter1 + 1], base_sub_vol_2[counter1 + 1]);
              act(base_big_vol_2[counter1 + 2], base_sub_vol_2[counter1 + 2]);
              act(base_big_vol_2[counter1 + 3], base_sub_vol_2[counter1 + 3]);
            }
            for(mwIndex counter1 = line_length_unroll; counter1 < chunk_dims[0]; counter1++)
            {
              act(base_big_vol_2[counter1], base_sub_vol_2[counter1]);
            }
          }
        }
      }
    }

  };

}  // namespace GT3D


#endif /* ZUTIL_CXX_INCLUDE_GTCXXPLACESUBVOLOPS_H_ */
