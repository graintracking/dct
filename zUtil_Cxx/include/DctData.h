/*
 * DctData.h
 *
 *  Created on: Jan 22, 2017
 *      Author: Nicola Vigano', 2017, CWI, N.R.Vigano@cwi.nl
 */

#ifndef ZUTIL_CXX_INCLUDE_DCTDATA_H_
#define ZUTIL_CXX_INCLUDE_DCTDATA_H_

#include <vector>
#include <algorithm>
#include <numeric>

#include "vectorization.h"
#include <parallelization.h>

namespace dct {

  static const size_t VERSION_MAJOR = 0;
  static const size_t VERSION_MINOR = 1;

  // This class is intentionally not memory-safe!! Be careful with it..
  template<typename Type, class Alloc = VectorAllocator<Type> >
  class DctData : public VectorOperations<Type> {
  public:
    typedef typename SIMDUnrolling<Type>::vVvf vVvf;

  protected:
    size_t dims[3];

    Type * data;

  public:
    DctData() : dims{1, 1, 1}, data(nullptr) { }
    template<typename TypeDims>
    DctData(const TypeDims * init_dims, const size_t & num_dims = 3, const size_t & skip_dims = 1, Type * const new_data = nullptr);
    DctData(const std::vector<size_t> & new_dims, Type * const new_data = nullptr);

    void allocate();
    void deallocate();

    const size_t * get_dims() const noexcept { return dims; }

    void set_dims(const std::vector<size_t> & new_dims);
    template<typename TypeDims>
    void set_dims(const TypeDims * init_dims, const size_t & num_dims, const size_t & skip = 1);

    constexpr Type * get_data() const noexcept { return data; }
    Type * get_data() noexcept { return data; }

    void set_data(Type * const new_data) { data = new_data; };
    void set_data(Type * const new_data, const std::vector<size_t> & new_dims) {
      this->set_dims(new_dims); data = new_data;
    };

    constexpr size_t get_tot_elements() const {
      return (dims[0] * dims[1] * dims[2]);
    }
  };

  template<typename Type, class Alloc = VectorAllocator<Type> >
  class DctProjection : public DctData<Type, Alloc> {
  protected:
    size_t skip;

    void update_skip() noexcept { this->skip = this->dims[0] * this->dims[1]; }

  public:
    DctProjection() : DctData<Type, Alloc>(), skip(0) { }
    template<typename TypeDims>
    DctProjection(const TypeDims * init_dims, const size_t & num_dims = 3, const size_t & skip_dims = 1, Type * const new_data = nullptr);
    DctProjection(const std::vector<size_t> & new_dims, Type * const new_data = nullptr);

    template<typename TypeDims>
    void set_dims(const TypeDims * init_dims, const size_t & num_dims, const size_t & skip_dims = 1);
    void set_dims(const std::vector<size_t> & new_dims);

    const size_t & get_skip() const noexcept { return skip; }
  };

  template<typename Type, class Alloc = VectorAllocator<Type> >
  class DctProjectionVector : public std::vector<DctProjection<Type, Alloc> > {
  public:
    void check_consistency() const;
    void check_consistency(const DctProjectionVector<Type, Alloc> &) const;

    std::vector<size_t> get_tot_elements_each_proj() const;

    void allocate() { for (auto & item : (*this)) { item.allocate(); } }
    void deallocate() { for (auto & item : (*this)) { item.deallocate(); } }
  };

  template<typename Type, class Alloc = VectorAllocator<Type> >
  class DctVolumeGrid : public std::vector<DctData<Type, Alloc> > {
  public:
    enum class ReshapePolicy : uint8_t {
      NoMatchThrowError,
      NoMatchClear,
      NoMatchIgnore,
    };

  protected:
    std::vector<size_t> grid_dims;

    ReshapePolicy reshape_policy;

  public:
    DctVolumeGrid(ReshapePolicy rs_pol = ReshapePolicy::NoMatchThrowError) : grid_dims(), reshape_policy(rs_pol) { }
    template<typename TypeDims>
    DctVolumeGrid(const TypeDims * init_dims, const size_t & num_dims, const size_t & skip_dims = 1,
        const std::vector<DctData<Type, Alloc> > & new_vols = std::vector<DctData<Type, Alloc> >());
    DctVolumeGrid(const std::vector<size_t> & new_dims,
        const std::vector<DctData<Type, Alloc> > & new_vols = std::vector<DctData<Type, Alloc> >());
    DctVolumeGrid(const DctVolumeGrid<Type, Alloc> & other) = default;

    void check_consistency() const;
    void check_elementwise_compatibility(const DctVolumeGrid<Type, Alloc> &) const;

    template<typename TypeDims>
    void set_dims(const TypeDims * init_dims, const size_t & num_dims, const size_t & skip_dims = 1);
    void set_dims(const std::vector<size_t> & new_dims);

    const std::vector<size_t> & get_grid_dims() const noexcept { return grid_dims; }

    void set_vols(const std::vector<DctData<Type, Alloc> > & new_vols);
    void create_vols(const std::vector<size_t> & vol_dims);
    void create_vols(const size_t vol_dims[3]);

    void allocate() { for (auto & item : (*this)) { item.allocate(); } }
    void deallocate() { for (auto & item : (*this)) { item.deallocate(); } }

    const DctData<Type, Alloc> & get_vol(const std::vector<size_t> & pos) const;
    DctData<Type, Alloc> & get_vol(const std::vector<size_t> & pos);

    constexpr size_t get_tot_vols() const {
      return std::accumulate(grid_dims.begin(), grid_dims.end(), size_t(1), std::multiplies<size_t>());
    }

    const size_t sub_to_ind(const std::vector<size_t> & subs) const;
    const std::vector<size_t> subs_to_inds(const std::vector<size_t> & subs, const size_t & ext_dim, const std::vector<size_t> & ext_inds) const;
    const std::vector<size_t> ind_to_sub(const size_t & subs) const;
  };

} /* namespace dct */


#include "exceptions.h"

namespace dct {

  static const char error_blob_index_out_of_hlim[] = "Requested a BLOB outside the range ('blob_offsets' too large)!";
  static const char error_blob_index_out_of_llim[] = "Requested a BLOB outside the range ('blob_offsets' less then 1)!";

  static const char error_sinogram_not_3D[] = "One of the SINOGRAMS is not 3D";
  static const char error_blob_not_3D[] = "One of the BLOBS is not 3D";

  static const char error_wrong_dim_0[] = "Blobs and sinograms should have same length along 1st dimension";
  static const char error_wrong_dim_2[] = "Blobs and sinograms should have same length along 3rd dimension";

  static const char error_incons_use_shifts[] = "Inconsistent use of shifts";

  static const char error_wrong_dim_u_sh[] = "Sinograms should fit into blobs along 1st dimension";
  static const char error_wrong_dim_v_sh[] = "Sinograms should fit into blobs along 3rd dimension";

  static const char error_sino_slice_out_of_hlim[] = "Requested a SINOGRAM SLICE outside the range ('sino_offsets' too large)!";
  static const char error_blob_slice_out_of_hlim[] = "Requested a BLOB SLICE outside the range ('proj_offsets' too large)!";

  static const char error_sino_slice_out_of_llim[] = "Requested a SINOGRAM SLICE outside the range ('sino_offsets' less than 1)!";
  static const char error_blob_slice_out_of_llim[] = "Requested a BLOB SLICE outside the range ('proj_offsets' less than 1)!";

  //////////////////////////////////////////////////////////////////////////////
  // DctData

  template<typename Type, class Alloc>
  void
  DctData<Type, Alloc>::allocate()
  {
    Alloc alloc;
    this->data = alloc.allocate(this->get_tot_elements());
  }

  template<typename Type, class Alloc>
  void
  DctData<Type, Alloc>::deallocate()
  {
    Alloc alloc;
    alloc.deallocate(this->data);
    this->data = nullptr;
  }

  template<typename Type, class Alloc>
  void
  DctData<Type, Alloc>::set_dims(const std::vector<size_t> & new_dims)
  {
    this->set_dims(new_dims.data(), new_dims.size(), 1);
  }

  template<typename Type, class Alloc>
  template<typename TypeDims>
  void
  DctData<Type, Alloc>::set_dims(const TypeDims* init_dims, const size_t & num_dims, const size_t & skip_dims)
  {
    if (num_dims > 3) {
      THROW(WrongArgumentException, DctData, "Expected three dimensions for the initialization of Projection Data");
    }

    for (size_t dim = 0; dim < num_dims; dim++) {
      this->dims[dim] = init_dims[dim * skip_dims];
    }
  }

  template<typename Type, class Alloc>
  DctData<Type, Alloc>::DctData(const std::vector<size_t> & new_dims, Type * const new_data)
  : data(new_data)
  {
    this->set_dims(new_dims);
  }

  template<typename Type, class Alloc>
  template<typename TypeDims>
  DctData<Type, Alloc>::DctData(const TypeDims * init_dims, const size_t & num_dims, const size_t & skip_dims, Type * const new_data)
  : data(new_data)
  {
    this->set_dims(init_dims, num_dims, skip_dims);
  }

  //////////////////////////////////////////////////////////////////////////////
  // DctProjection

  template<typename Type, class Alloc>
  DctProjection<Type, Alloc>::DctProjection(const std::vector<size_t> & new_dims, Type * const new_data)
  : skip(0)
  {
    this->set_dims(new_dims);
    this->set_data(new_data);
  }

  template<typename Type, class Alloc>
  template<typename TypeDims>
  DctProjection<Type, Alloc>::DctProjection(const TypeDims * init_dims, const size_t & num_dims, const size_t & skip_dims, Type * const new_data)
  : skip(0)
  {
    this->set_dims(init_dims, num_dims, skip_dims);
    this->set_data(new_data);
  }

  template<typename Type, class Alloc>
  void
  DctProjection<Type, Alloc>::set_dims(const std::vector<size_t> & new_dims)
  {
    if (new_dims.size() != 3) {
      THROW(WrongArgumentException, DctProjection, "Expected three dimensions for the initialization of Projection Data");
    }

    DctData<Type, Alloc>::set_dims(new_dims);

    this->update_skip();
  }

  template<typename Type, class Alloc>
  template<typename TypeDims>
  void
  DctProjection<Type, Alloc>::set_dims(const TypeDims * init_dims, const size_t & num_dims, const size_t & skip_dims)
  {
    if (num_dims != 3) {
      THROW(WrongArgumentException, DctProjection, "Expected three dimensions for the initialization of Projection Data");
    }

    DctData<Type, Alloc>::set_dims(init_dims, num_dims, skip_dims);

    this->update_skip();
  }

  //////////////////////////////////////////////////////////////////////////////
  // DctProjectionVector

  template<typename Type, class Alloc>
  void
  DctProjectionVector<Type, Alloc>::check_consistency() const
  {
    if (this->empty()) {
      THROW(WrongArgumentException, DctProjectionVector, "DctProjectionVector is empty! Cannot call consistency check!");
    }
    const size_t * first_dims = (*this)[0].get_dims();
    for (size_t num = 1; num < this->size(); num++) {
      const size_t * current_dims = (*this)[num].get_dims();
      if (first_dims[0] != current_dims[0] || first_dims[2] != current_dims[2]) {
        THROW(WrongArgumentException, DctProjectionVector, "All the sinograms/blobs should have the same sizes along the first and third dimension");
      }
    }
  }

  template<typename Type, class Alloc>
  void
  DctProjectionVector<Type, Alloc>::check_consistency(const DctProjectionVector<Type, Alloc> & other) const
  {
    if (other.empty()) {
      THROW(WrongArgumentException, DctProjectionVector, "Other DctProjectionVector is empty! Cannot call cross-consistency check!");
    }
    if (this->size() != other.size()) {
      THROW(WrongArgumentException, DctProjectionVector, "DctProjectionVectors don't have a matching number of elements!");
    }
    other.check_consistency();

    const size_t * first_dims_this = (*this)[0].get_dims();
    const size_t * first_dims_other = other[0].get_dims();

    if (first_dims_this[0] != first_dims_other[0] || first_dims_this[2] != first_dims_other[2]) {
      THROW(WrongArgumentException, DctProjectionVector, "DctProjectionVectors don't have a matching lengths of rows and/or columns!");
    }

    for (size_t num = 0; num < this->size(); num++) {
      const size_t * current_dims_this = (*this)[num].get_dims();
      const size_t * current_dims_other = (*this)[num].get_dims();
      if (current_dims_this[1] != current_dims_other[1]) {
        THROW(WrongArgumentException, DctProjectionVector, "DctProjectionVectors don't have a matching depths of blobs!");
      }
    }
  }

  template<typename Type, class Alloc>
  inline std::vector<size_t>
  DctProjectionVector<Type, Alloc>::get_tot_elements_each_proj() const
  {
    std::vector<size_t> output;
    output.reserve(this->size());
    for (const auto & p : *this) { output.emplace_back(p.get_tot_elements()); }
    return output;
  }

  //////////////////////////////////////////////////////////////////////////////
  // DctVolumeGrid

  template<typename Type, class Alloc>
  template<typename TypeDims>
  DctVolumeGrid<Type, Alloc>::DctVolumeGrid(const TypeDims * init_dims, const size_t & num_dims, const size_t & skip_dims, const std::vector<DctData<Type, Alloc> > & new_vols)
  : grid_dims(), reshape_policy(ReshapePolicy::NoMatchThrowError)
  {
    this->set_dims(init_dims, num_dims, skip_dims);
    if (!new_vols.empty())
    {
      this->set_vols(new_vols);
    }
  }

  template<typename Type, class Alloc>
  DctVolumeGrid<Type, Alloc>::DctVolumeGrid(const std::vector<size_t> & new_dims, const std::vector<DctData<Type, Alloc> > & new_vols)
  : grid_dims(), reshape_policy(ReshapePolicy::NoMatchThrowError)
  {
    this->set_dims(new_dims);
    if (!new_vols.empty())
    {
      this->set_vols(new_vols);
    }
  }

  template<typename Type, class Alloc>
  void
  DctVolumeGrid<Type, Alloc>::check_consistency() const
  {
    if (this->empty()) {
      THROW(WrongArgumentException, DctVolumeGrid, "no volumes stored! Cannot call consistency check!");
    }
    const size_t * first_dims = (*this)[0].get_dims();
    for (size_t num = 1; num < this->size(); num++) {
      const size_t * current_dims = (*this)[num].get_dims();
      if ((first_dims[0] != current_dims[0])
          || (first_dims[1] != current_dims[1])
          || (first_dims[2] != current_dims[2]))
      {
        printf("First volume dims: [%lu, %lu, %lu], volume %lu dims: [%lu %lu %lu]",
            first_dims[0], first_dims[1], first_dims[2], num,
            current_dims[0], current_dims[1], current_dims[2]);
        THROW(WrongArgumentException, DctVolumeGrid, "all volumes should have the same dimensions");
      }
    }
  }

  template<typename Type, class Alloc>
  void
  DctVolumeGrid<Type, Alloc>::check_elementwise_compatibility(const DctVolumeGrid<Type, Alloc> & other) const
  {
    this->check_consistency();
    other.check_consistency();

    if (this->grid_dims != other.grid_dims) {
      THROW(WrongArgumentException, DctVolumeGrid, "Volume grids should have the same shape!");
    }
    const size_t * const local_vol_dims = (*this)[0].get_dims();
    const size_t * const other_vol_dims = other[0].get_dims();
    if ((local_vol_dims[0] != other_vol_dims[0])
        || (local_vol_dims[1] != other_vol_dims[1])
        || (local_vol_dims[2] != other_vol_dims[2]))
    {
      printf("Num dims out: [%lu, %lu, %lu], Num dims in: [%lu, %lu, %lu]\n",
          local_vol_dims[0], local_vol_dims[1], local_vol_dims[2],
          other_vol_dims[0], other_vol_dims[1], other_vol_dims[2] );
      THROW(WrongArgumentException, DctVolumeGrid, "Internal volumes should have the same shape!");
    }
  }

  template<typename Type, class Alloc>
  template<typename TypeDims>
  void
  DctVolumeGrid<Type, Alloc>::set_dims(const TypeDims * init_dims, const size_t & num_dims, const size_t & skip_dims)
  {
    std::vector<size_t> new_dims;
    new_dims.resize(num_dims);

    for (size_t dim = 0; dim < num_dims; dim++) {
      new_dims[dim] = init_dims[dim * skip_dims];
    }

    this->set_dims(new_dims);
  }

  template<typename Type, class Alloc>
  void
  DctVolumeGrid<Type, Alloc>::set_dims(const std::vector<size_t> & new_dims)
  {
    const size_t new_tot_elems = std::accumulate(new_dims.begin(), new_dims.end(), size_t(1), std::multiplies<size_t>());

    if (!this->empty() && (new_tot_elems != this->size())) {
      switch (reshape_policy) {
        case ReshapePolicy::NoMatchThrowError:
        {
          THROW(WrongArgumentException, DctVolumeGrid, "Requested reshape of wrong total number of elements");
        }
        case ReshapePolicy::NoMatchClear:
        {
          this->clear();
          break;
        }
        default: // The NoMatchIgnore case
          break;
      }
    }

    grid_dims = new_dims;
  }

  template<typename Type, class Alloc>
  void
  DctVolumeGrid<Type, Alloc>::set_vols(const std::vector<DctData<Type, Alloc> > & new_vols)
  {
    if (new_vols.size() != this->size()) {
      switch (reshape_policy) {
        case ReshapePolicy::NoMatchThrowError:
        {
          THROW(WrongArgumentException, DctVolumeGrid, "assigned wrong total number of elements");
        }
        case ReshapePolicy::NoMatchClear:
        {
          this->grid_dims.resize(1);
          this->grid_dims[0] = new_vols.size();
          break;
        }
        default: // The NoMatchIgnore case
          break;
      }
    }

    *this = new_vols;
  }

  template<typename Type, class Alloc>
  void
  DctVolumeGrid<Type, Alloc>::create_vols(const std::vector<size_t> & vol_dims)
  {
    this->reserve(this->get_tot_vols());
    for (size_t vol_num = 0; vol_num < this->get_tot_vols(); vol_num++)
    {
      this->emplace_back(vol_dims);
    }
  }

  template<typename Type, class Alloc>
  void
  DctVolumeGrid<Type, Alloc>::create_vols(const size_t vol_dims[3])
  {
    this->reserve(this->get_tot_vols());
    for (size_t vol_num = 0; vol_num < this->get_tot_vols(); vol_num++)
    {
      this->emplace_back(vol_dims, 3);
    }
  }

  template<typename Type, class Alloc>
  const DctData<Type, Alloc> &
  DctVolumeGrid<Type, Alloc>::get_vol(const std::vector<size_t> & pos) const
  {
    const size_t elem_pos = this->sub_to_ind(pos);
    return this->at(elem_pos);
  }

  template<typename Type, class Alloc>
  DctData<Type, Alloc> &
  DctVolumeGrid<Type, Alloc>::get_vol(const std::vector<size_t> & pos)
  {
    const size_t elem_pos = this->sub_to_ind(pos);
    return this->at(elem_pos);
  }

  template<typename Type, class Alloc>
  const size_t
  DctVolumeGrid<Type, Alloc>::sub_to_ind(const std::vector<size_t> & subs) const
  {
    if (subs.size() != this->grid_dims.size()) {
      THROW(WrongArgumentException, DctVolumeGrid, "requested volume position has a wrong dimensionality");
    }
    if (subs.empty()) {
      THROW(WrongArgumentException, DctVolumeGrid, "requested volume position is empty");
    }

    size_t dim_mult = 1;
    size_t ind = subs[0];

    for (size_t dim = 1; dim < subs.size(); dim++)
    {
      if (subs[dim] > grid_dims[dim]) {
        THROW(WrongArgumentException, DctVolumeGrid, "requested volume position outside the grid limits");
      }

      dim_mult *= grid_dims[dim-1];
      ind += subs[dim] * dim_mult;
    }

    return ind;
  }

  template<typename Type, class Alloc>
  const std::vector<size_t>
  DctVolumeGrid<Type, Alloc>::subs_to_inds(const std::vector<size_t> & subs, const size_t & ext_dim, const std::vector<size_t> & ext_inds) const
  {
    std::vector<size_t> inds;

    if (ext_dim >= this->grid_dims.size()) {
      THROW(WrongArgumentException, DctVolumeGrid, "extended volumes dimension is bigger than the grid dimensions");
    }
    if (ext_dim < 0) {
      THROW(WrongArgumentException, DctVolumeGrid, "extended volumes dimension is less than 0");
    }

    inds.resize(ext_inds.size());

    const size_t base_ind = this->sub_to_ind(subs);

    const size_t dim_mult = std::accumulate(grid_dims.begin(), grid_dims.begin()+ext_dim, size_t(1), std::multiplies<size_t>());;

    for (size_t ind_ind = 0; ind_ind < inds.size(); ind_ind++)
    {
      inds[ind_ind] = base_ind + dim_mult * ext_inds[ind_ind];
    }

    return inds;
  }

  template<typename Type, class Alloc>
  const std::vector<size_t>
  DctVolumeGrid<Type, Alloc>::ind_to_sub(const size_t & ind) const
  {
    std::vector<size_t> subs;

    if (ind >= this->get_tot_vols()) {
      THROW(WrongArgumentException, DctVolumeGrid, "requested volume position is higher than the number of volumes");
    }
    if (ind < 0) {
      THROW(WrongArgumentException, DctVolumeGrid, "requested volume position is less than 0");
    }

    subs.resize(grid_dims.size());
    size_t rem = ind;

    for (size_t dim = 0; dim < subs.size(); dim++)
    {
      subs[dim] = rem % grid_dims[dim];
      rem /= grid_dims[dim];
    }

    return subs;
  }

} /* namespace dct */

#endif /* ZUTIL_CXX_INCLUDE_DCTDATA_H_ */
