/*
 * GtMatlabVolumeLooper.h
 *
 *  Created on: Mar 21, 2012
 *      Author: vigano
 */

#ifndef GTMATLABVOLUMELOOPER_H_
#define GTMATLABVOLUMELOOPER_H_

#include "mex.h"

#if (MX_API_VER < 0x07400000)
extern "C"
{
  bool mxUnshareArray(mxArray *array_ptr, bool noDeepCopy);
  mxArray *mxCreateSharedCopy(const mxArray *pr);
  mxArray *mxCreateSharedDataCopy(const mxArray *pr);
  mxArray *mxUnreference(const mxArray *pr);
}
#endif

template<typename Type>
class GtMatlabPointer {
  const mwSize multiplier;
  Type * const base;

  Type * pointer;
public:
  GtMatlabPointer(Type * const _b, const mwSize & _m, const mwIndex & _c = 0)
  : multiplier(_m), base(_b)
  {
    setPointer(_c);
  }

  void setPointer(const mwIndex & _c)
  {
    pointer = base + _c * multiplier;
  }
};

template<typename TypeOut, typename TypeIn>
class GtMatlabVolumeLooper {
protected:
  TypeOut * output;
  TypeIn * vol;
  const mwSize * const dimsOut, * const dimsVol;
  const double & index;
  const double * const limits;

  mwIndex lim1[2], lim2[2], lim3[2];

  virtual void doWork(TypeOut & dest, const TypeIn & orig) { }
public:
  GtMatlabVolumeLooper(void * _out, void * _vol, const double * _limits,
      const mwSize * const dims_out, const mwSize * const dims_vol, const double & indx )
    : output((TypeOut *)_out), vol((TypeIn *)_vol)
    , dimsOut(dims_out), dimsVol(dims_vol), index(indx), limits(_limits)
  { }
  virtual ~GtMatlabVolumeLooper() { }

  virtual void loopOver3D()
  {
    lim1[0] = limits[0]-1, lim1[1] = limits[1]-1;
    lim2[0] = limits[2]-1, lim2[1] = limits[3]-1;
    lim3[0] = limits[4]-1, lim3[1] = limits[5]-1;

    /* Counters for the bigger volume */
    mwIndex counter1 = 0, counter2 = 0, counter3 = 0;
    /* Counters for the grain bounding box */
    mwIndex counterVol1 = 0, counterVol2 = 0, counterVol3 = 0;

    /* These loops use extensively pointer arithmetics in determining the chuck
     * of the matrix to be computed */
    for(counter3 = lim3[0], counterVol3 = 0; counter3 <= lim3[1]; counter3++, counterVol3++) {
      /* Base vectors, which save computation of the 3rd dimension */
      TypeOut * const base3 = output + counter3*dimsOut[0]*dimsOut[1];
      const TypeIn * const baseVol3 = vol + counterVol3*dimsVol[0]*dimsVol[1];
      for(counter2 = lim2[0], counterVol2 = 0; counter2 <= lim2[1]; counter2++, counterVol2++) {
        /* Base vectors, which save computation of the 2nd dimension */
        TypeOut * const base2 = base3 + counter2*dimsOut[0];
        const TypeIn * const baseVol2 = baseVol3 + counterVol2*dimsVol[0];
        /* Inner loop on the 1st dimension: the contiguous in memory */
        for(counter1 = lim1[0], counterVol1 = 0; counter1 <= lim1[1]; counter1++, counterVol1++) {
          this->doWork(base2[counter1], baseVol2[counterVol1]);
        }
      }
    }

    /* Could be parallelized, if modified this way: */
    /*  {
        TypeOut * const output = mxGetPr(plhs[0]);
        const mwSize mult_out = dimsOut[0]*dimsOut[1],
                     mult_vol = dimsVol[0]*dimsVol[1];
        mwIndex counter1 = 0, counter2 = 0, counter3 = 0;

        const mwIndex tot1 = (lim1[1] - lim1[0]), tot2 = (lim2[1] - lim2[0]),
                      tot3 = (lim3[1] - lim3[0]);

    # pragma omp parallel for private(counter3, counter2, counter1)
        for(counter3 =  0; counter3 < tot3; counter3++) {
          TypeOut * const base3         = output + (lim3[0] + counter3)* mult_out;
          const TypeIn * const baseVol3 = vol + counter3 * mult_vol;
          for(counter2 = 0; counter2 < tot2; counter2++) {
            TypeOut * const base2 = base3 + (lim2[0] + counter2)*dimsOut[0];
            const TypeIn * const baseVol2 = baseVol3 + counter2*dimsVol[0];
            for(counter1 = 0; counter1 < tot1; counter1++) {
              if (baseVol2[counter1]) {
                base2[lim1[0] + counter1] += index;
              }
            }
          }
        }
      }*/
  }
};

#endif /* GTMATLABVOLUMELOOPER_H_ */
