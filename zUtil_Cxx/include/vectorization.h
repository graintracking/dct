/*
 * vectorization.h
 *
 *  Created on: Jan 23, 2017
 *      Author: Nicola Vigano', 2017, CWI, N.R.Vigano@cwi.nl
 */

#ifndef ZUTIL_CXX_INCLUDE_VECTORIZATION_H_
#define ZUTIL_CXX_INCLUDE_VECTORIZATION_H_

#include <immintrin.h>
#include <cmath>
#include <memory>

#if defined ( _WIN32 )
#define ALIGNED(x)  __declspec(align(x))
#elif defined( __GNUC__ ) || defined(__MINGW64__)
#define  ALIGNED(x) __attribute__((aligned(x)))
#endif

#define ROUND_DOWN(x, s) ((x) & ~((s) - 1))
#define ROUND_UP(x, s) (((x) + ((s) - 1)) & ~((s) - 1))

#define VECTOR_SIZE_SSE 16
#define VECTOR_SIZE_AVX 32

////////////////////////////////////////////////////////////////////////////////
// Utility operators

template <typename Type, const size_t vector_size>
class SIMDRegister
{
  typedef Type vVvf __attribute__((vector_size(vector_size))) ALIGNED(vector_size);
public:
  using ValueType = vVvf;
  enum { size = vector_size };

  inline operator vVvf&() { return value; }
  inline operator const vVvf&() const { return value; }
  inline SIMDRegister() {}
  inline SIMDRegister(const vVvf & v) : value(v) {} // Not explicit
  inline SIMDRegister & operator=(const vVvf & v) { value = v; return *this; }

  inline SIMDRegister operator+(const vVvf & v) const { return (const vVvf) value + v; }
  inline SIMDRegister operator-(const vVvf & v) const { return (const vVvf) value - v; }
  inline SIMDRegister operator*(const vVvf & v) const { return (const vVvf) value * v; }
  inline SIMDRegister operator/(const vVvf & v) const { return (const vVvf) value / v; }

  vVvf value;
};

using vsfs = SIMDRegister<float, VECTOR_SIZE_SSE>;
using vsds = SIMDRegister<double, VECTOR_SIZE_SSE>;
#if defined(__AVX__)
using vsfa = SIMDRegister<float, VECTOR_SIZE_AVX>;
using vsda = SIMDRegister<double, VECTOR_SIZE_AVX>;
#endif

template<typename Type, const size_t vector_size = VECTOR_SIZE_SSE>
class SIMDUnrolling {
public:
  const size_t shift;
  const size_t unrolling;
  const size_t block;

  typedef Type vVvf __attribute__((vector_size(vector_size))) ALIGNED(vector_size);
  typedef Type vs_s __attribute__((vector_size(VECTOR_SIZE_SSE))) ALIGNED(VECTOR_SIZE_SSE);
  typedef Type vs_a __attribute__((vector_size(VECTOR_SIZE_AVX))) ALIGNED(VECTOR_SIZE_AVX);

  SIMDUnrolling(const size_t & _unroll)
  : shift(vector_size / sizeof(Type)), unrolling(_unroll)
  , block(shift * unrolling)
  { }

  const size_t
  get_unroll(const size_t & tot_size) const
  {
    return ROUND_DOWN(tot_size, block);
  }
  const size_t
  get_shift_to_align(const Type * const start) const
  {
    return ((vector_size - (reinterpret_cast<size_t>(start) % vector_size)) % vector_size);
  }
};


template<typename Type, const size_t vector_size>
class Coeff {
public:
  static const SIMDRegister<Type, vector_size> get(const Type & coeff);
};

template<>
class Coeff<float, VECTOR_SIZE_SSE> {
public:
  using RegTypeSSE = SIMDRegister<float, VECTOR_SIZE_SSE>;

  static const RegTypeSSE get(const float & coeff)
  {
    return (const RegTypeSSE) _mm_set1_ps(coeff);
  }
};

template<>
class Coeff<double, VECTOR_SIZE_SSE> {
public:
  using RegTypeSSE = SIMDRegister<double, VECTOR_SIZE_SSE>;

  static const RegTypeSSE get(const double & coeff)
  {
    return (const RegTypeSSE) _mm_set1_pd(coeff);
  }
};

#if defined(__AVX__)
template<>
class Coeff<float, VECTOR_SIZE_AVX> {
public:
  using RegTypeAVX = SIMDRegister<float, VECTOR_SIZE_AVX>;

  static const RegTypeAVX get(const float & coeff)
  {
    return (const RegTypeAVX) _mm256_set1_ps(coeff);
  }
};

template<>
class Coeff<double, VECTOR_SIZE_AVX> {
public:
  using RegTypeAVX = SIMDRegister<double, VECTOR_SIZE_AVX>;

  static const RegTypeAVX get(const double & coeff)
  {
    return (const RegTypeAVX) _mm256_set1_pd(coeff);
  }
};
#endif

////////////////////////////////////////////////////////////////////////////////
// Access operators

template<typename Type, const size_t vector_size>
class AccessAligned {
public:
  using VecType = SIMDRegister<Type, vector_size>;

  const VecType load(const Type * const __restrict in) const
  {
    return *(VecType *)in;
  }
  void store(Type * const __restrict out, const VecType & in) const
  {
    *(VecType *)out = in;
  }
};

template<typename Type, const size_t vector_size>
class AccessStreamed : public AccessAligned<Type, vector_size> {
public:
  using VecType = SIMDRegister<Type, vector_size>;

  void store(Type * const __restrict out, const VecType & in) const;
};

template<>
inline void
AccessStreamed<float, VECTOR_SIZE_SSE>::store(float * const __restrict out, const SIMDRegister<float, VECTOR_SIZE_SSE> & in) const
{
  _mm_stream_ps(out, in);
}

template<>
inline void
AccessStreamed<double, VECTOR_SIZE_SSE>::store(double * const __restrict out, const SIMDRegister<double, VECTOR_SIZE_SSE> & in) const
{
  _mm_stream_pd(out, in);
}

#if defined(__AVX__)
template<>
inline void
AccessStreamed<float, VECTOR_SIZE_AVX>::store(float * const __restrict out, const SIMDRegister<float, VECTOR_SIZE_AVX> & in) const
{
  _mm256_stream_ps(out, in);
}

template<>
inline void
AccessStreamed<double, VECTOR_SIZE_AVX>::store(double * const __restrict out, const SIMDRegister<double, VECTOR_SIZE_AVX> & in) const
{
  _mm256_stream_pd(out, in);
}
#endif

template<typename Type, const size_t vector_size>
class AccessUnaligned {
public:
  using VecType = SIMDRegister<Type, vector_size>;

  const VecType load(const Type * const __restrict in) const;
  void store(Type * const __restrict out, const VecType & in) const;
};

template<>
class AccessUnaligned<double, VECTOR_SIZE_SSE> {
public:
  using SSERegT = SIMDRegister<double, VECTOR_SIZE_SSE>;

  const SSERegT load(const double * const __restrict in) const
  {
    return _mm_loadu_pd(in);
  }
  void store(double * const __restrict out, const SSERegT & in) const
  {
    _mm_storeu_pd(out, in);
  }
};

template<>
class AccessUnaligned<float, VECTOR_SIZE_SSE> {
public:
  using SSERegT = SIMDRegister<float, VECTOR_SIZE_SSE>;

  const SSERegT load(const float * const __restrict in) const
  {
    return _mm_loadu_ps(in);
  }
  void store(float * const __restrict out, const SSERegT & in) const
  {
    _mm_storeu_ps(out, in);
  }
};

#if defined(__AVX__)
template<>
class AccessUnaligned<float, VECTOR_SIZE_AVX> {
public:
  using AVXRegT = SIMDRegister<float, VECTOR_SIZE_AVX>;

  const AVXRegT load(const float * const __restrict in) const
  {
    return _mm256_loadu_ps(in);
  }
  void store(float * const __restrict out, const AVXRegT & in) const
  {
    _mm256_storeu_ps(out, in);
  }
};

template<>
class AccessUnaligned<double, VECTOR_SIZE_AVX> {
public:
  using AVXRegT = SIMDRegister<double, VECTOR_SIZE_AVX>;

  const AVXRegT load(const double * const __restrict in) const
  {
    return _mm256_loadu_pd(in);
  }
  void store(double * const __restrict out, const AVXRegT & in) const
  {
    _mm256_storeu_pd(out, in);
  }
};
#endif

////////////////////////////////////////////////////////////////////////////////
// Computation operators

enum class OpType : uint8_t {
  Unary,
  Binary,
  Trinary,
  Nary
};

class OpBase {
};

class OpUnary : public OpBase {
public:
  OpType get_type() const { return OpType::Unary; }
};

class OpBinary : public OpBase {
public:
  OpType get_type() const { return OpType::Binary; }
};

// Base operators

template<typename Type>
class OpUnaryAssign : public OpUnary {
public:
  template<const size_t vector_size>
  using VecType = SIMDRegister<Type, vector_size>;

  const Type
  operator()(const Type & data) const throw()
  {
    return data;
  }
  template<const size_t vector_size>
  const VecType<vector_size>
  operator()(const VecType<vector_size> & data) const throw()
  {
    return data;
  }
};

template<typename Type>
class OpBinaryPlus : public OpBinary {
public:
  template<const size_t vector_size>
  using VecType = SIMDRegister<Type, vector_size>;

  const Type
  operator()(const Type & data1, const Type & data2) const throw()
  {
    return (data1 + data2);
  }
  template<const size_t vector_size>
  const VecType<vector_size>
  operator()(const VecType<vector_size> & data1, const VecType<vector_size> & data2) const throw()
  {
    return (data1 + data2);
  }
};

template<typename Type>
class OpBinaryTimes : public OpBinary {
public:
  template<const size_t vector_size>
  using VecType = SIMDRegister<Type, vector_size>;

  const Type
  operator()(const Type & data1, const Type & data2) const throw()
  {
    return (data1 * data2);
  }
  template<const size_t vector_size>
  const VecType<vector_size>
  operator()(const VecType<vector_size> & data1, const VecType<vector_size> & data2) const throw()
  {
    return (data1 * data2);
  }
};

template<typename Type>
class OpBinaryMinus : public OpBinary {
public:
  template<const size_t vector_size>
  using VecType = SIMDRegister<Type, vector_size>;

  const Type
  operator()(const Type & data1, const Type & data2) const throw()
  {
    return (data1 - data2);
  }
  template<const size_t vector_size>
  const VecType<vector_size>
  operator()(const VecType<vector_size> & data1, const VecType<vector_size> & data2) const throw()
  {
    return (data1 - data2);
  }
};

template<typename Type>
class OpBinaryDivide : public OpBinary {
public:
  template<const size_t vector_size>
  using VecType = SIMDRegister<Type, vector_size>;

  const Type
  operator()(const Type & data1, const Type & data2) const throw()
  {
    return (data1 / data2);
  }
  template<const size_t vector_size>
  const VecType<vector_size>
  operator()(const VecType<vector_size> & data1, const VecType<vector_size> & data2) const throw()
  {
    return (data1 / data2);
  }
};

// Generic operators

template<typename Type, template<typename TypeOp> class Op>
class OpUnaryGenericScalar : public OpUnary {
public:
  template<const size_t vector_size>
  using VecType = SIMDRegister<Type, vector_size>;
  using VecTypeSSE = VecType<VECTOR_SIZE_SSE>;
  using VecTypeAVX = VecType<VECTOR_SIZE_AVX>;

  OpUnaryGenericScalar() = delete;
  OpUnaryGenericScalar(const Type & _scalar)
  : op(), scalar(_scalar), scalar_vs(Coeff<Type, VECTOR_SIZE_SSE>::get(_scalar))
#if defined(__AVX__)
  , scalar_va(Coeff<Type, VECTOR_SIZE_AVX>::get(_scalar))
#endif
  { }

  const Type
  operator()(const Type & data) const throw()
  {
    return op(data, scalar);
  }
  const VecTypeSSE
  operator()(const VecTypeSSE & data) const throw()
  {
    return op(data, scalar_vs);
  }
#if defined(__AVX__)
  const VecTypeAVX
  operator()(const VecTypeAVX & inData) const throw()
  {
    return op(inData, scalar_va);
  }
#endif
protected:
  const Op<Type> op;

  const Type scalar;
  const VecTypeSSE scalar_vs;
#if defined(__AVX__)
  const VecTypeAVX scalar_va;
#endif
};

template<typename Type, template<typename TypeOp> class Op>
class OpBinaryGenericScaled : public OpBinary {
public:
  template<const size_t vector_size>
  using VecType = SIMDRegister<Type, vector_size>;
  using VecTypeSSE = VecType<VECTOR_SIZE_SSE>;
  using VecTypeAVX = VecType<VECTOR_SIZE_AVX>;

  OpBinaryGenericScaled() = delete;
  OpBinaryGenericScaled(const Type & _scale)
  : op(), op_times(), scale(_scale), scale_vs(Coeff<Type, VECTOR_SIZE_SSE>::get(_scale))
#if defined(__AVX__)
  , scale_va(Coeff<Type, VECTOR_SIZE_AVX>::get(_scale))
#endif
  { }

  const Type
  operator()(const Type & data1, const Type & data2) const throw()
  {
    return op(data1, op_times(data2, scale));
  }
  const VecTypeSSE
  operator()(const VecTypeSSE & data1, const VecTypeSSE & data2) const throw()
  {
    return op(data1, op_times(data2, scale_vs));
  }
#if defined(__AVX__)
  const VecTypeAVX
  operator()(const VecTypeAVX & data1, const VecTypeAVX & data2) const throw()
  {
    return op(data1, op_times(data2, scale_va));
  }
#endif
protected:
  const Op<Type> op;
  const OpBinaryTimes<Type> op_times;

  const Type scale;
  const VecTypeSSE scale_vs;
#if defined(__AVX__)
  const VecTypeAVX scale_va;
#endif
};

template<typename Type>
using OpUnaryPlusScalar = OpUnaryGenericScalar<Type, OpBinaryPlus>;

template<typename Type>
using OpBinaryPlusScaled = OpBinaryGenericScaled<Type, OpBinaryPlus>;

template<typename Type>
using OpUnaryTimesScalar = OpUnaryGenericScalar<Type, OpBinaryTimes>;

template<typename Type>
using OpUnaryMinusScalar = OpUnaryGenericScalar<Type, OpBinaryMinus>;

template<typename Type>
using OpBinaryMinusScaled = OpBinaryGenericScaled<Type, OpBinaryMinus>;

template<typename Type>
using OpUnaryDivideScalar = OpUnaryGenericScalar<Type, OpBinaryDivide>;

// Special operators

template<typename Type>
class OpBinaryPow : public OpBinary {
public:
  typedef typename SIMDUnrolling<Type>::vs_s vs_s;
  typedef typename SIMDUnrolling<Type>::vs_a vs_a;

  const Type
  operator()(const Type & inData1, const Type & inData2) const throw()
  {
    return pow(inData1, inData2);
  }
  const vsfs operator()(const vsfs & inData1, const vsfs & inData2) const throw()
  {
    const float * const inData1_s = (const float *) & inData1;
    const float * const inData2_s = (const float *) & inData2;
    return _mm_set_ps(
      pow(inData1_s[0], inData2_s[0]), pow(inData1_s[1], inData2_s[1]),
      pow(inData1_s[2], inData2_s[2]), pow(inData1_s[3], inData2_s[3])
    );
  }
  const vsds operator()(const vsds & inData1, const vsds & inData2) const throw()
  {
    const double * const inData1_d = (const double *) & inData1;
    const double * const inData2_d = (const double *) & inData2;
    return _mm_set_pd(
      pow(inData1_d[0], inData2_d[0]), pow(inData1_d[1], inData2_d[1])
    );
  }
#if defined(__AVX__)
  const vsfa operator()(const vsfa & inData1, const vsfa & inData2) const throw()
  {
    const float * const inData1_s = (const float *) & inData1;
    const float * const inData2_s = (const float *) & inData2;
    return _mm256_set_ps(
      pow(inData1_s[0], inData2_s[0]), pow(inData1_s[1], inData2_s[1]),
      pow(inData1_s[2], inData2_s[2]), pow(inData1_s[3], inData2_s[3]),
      pow(inData1_s[4], inData2_s[4]), pow(inData1_s[5], inData2_s[5]),
      pow(inData1_s[6], inData2_s[6]), pow(inData1_s[7], inData2_s[7])
    );
  }
  const vsda operator()(const vsda & inData1, const vsda & inData2) const throw()
  {
    const double * const inData1_d = (const double *) & inData1;
    const double * const inData2_d = (const double *) & inData2;
    return _mm256_set_pd(
      pow(inData1_d[0], inData2_d[0]), pow(inData1_d[1], inData2_d[1]),
      pow(inData1_d[2], inData2_d[2]), pow(inData1_d[3], inData2_d[3])
    );
  }
#endif
};

template<typename Type>
class OpUnaryPowScalar : public OpUnary {
  const Type exponent;
public:
  OpUnaryPowScalar(const Type & _exp) : exponent(_exp) { }

  const Type
  operator()(const Type & inData) const throw()
  {
    return pow(inData, exponent);
  }
  const vsds
  operator()(const vsds & data) const throw()
  {
    const double * const data_d = (const double *) & data;
    return _mm_set_pd(
      pow(data_d[0], exponent), pow(data_d[1], exponent)
    );
  }
  const vsfs
  operator()(const vsfs & data) const throw()
  {
    const float * const data_s = (const float *) & data;
    return _mm_set_ps(
      pow(data_s[0], exponent), pow(data_s[1], exponent),
      pow(data_s[2], exponent), pow(data_s[3], exponent)
    );
  }
#if defined(__AVX__)
  const vsda
  operator()(const vsda & data) const throw()
  {
    const double * const data_d = (const double *) & data;
    return _mm256_set_pd(
      pow(data_d[0], exponent), pow(data_d[1], exponent),
      pow(data_d[2], exponent), pow(data_d[3], exponent)
    );
  }
  const vsfa
  operator()(const vsfa & data) const throw()
  {
    const float * const data_s = (const float *) & data;
    return _mm256_set_ps(
      pow(data_s[0], exponent), pow(data_s[1], exponent),
      pow(data_s[2], exponent), pow(data_s[3], exponent),
      pow(data_s[4], exponent), pow(data_s[5], exponent),
      pow(data_s[6], exponent), pow(data_s[7], exponent)
    );
  }
#endif
};

template<typename Type>
class OpUnarySqrt : public OpUnary {
public:
  const Type
  operator()(const Type & data) const throw()
  {
    return sqrt(data);
  }
  const vsds
  operator()(const vsds & data) const throw()
  {
    return _mm_sqrt_pd(data);
  }
  const vsfs
  operator()(const vsfs & data) const throw()
  {
    return _mm_sqrt_ps(data);
  }
#if defined(__AVX__)
  const vsda
  operator()(const vsda & data) const throw()
  {
    return _mm256_sqrt_pd(data);
  }
  const vsfa
  operator()(const vsfa & data) const throw()
  {
    return _mm256_sqrt_ps(data);
  }
#endif
};

template<typename Type>
class OpUnaryAbs : public OpUnary {
public:
  OpUnaryAbs()
    : abs_mask_v2df(_mm_castsi128_pd(_mm_set1_epi64x(0x7fffffffffffffffL)))
    , abs_mask_v4sf(_mm_castsi128_ps(_mm_set1_epi32(0x7fffffff)))
#if defined(__AVX__)
    , abs_mask_v4df(_mm256_castsi256_pd(_mm256_set1_epi64x(0x7fffffffffffffffL)))
    , abs_mask_v8sf(_mm256_castsi256_ps(_mm256_set1_epi32(0x7fffffff)))
#endif
  { }
  const Type
  operator()(const Type & data) const throw()
  {
    return std::abs(data);
  }
  const vsds
  operator()(const vsds & data) const throw()
  {
    return _mm_and_pd(data, abs_mask_v2df);
  }
  const vsfs
  operator()(const vsfs & data) const throw()
  {
    return _mm_and_ps(data, abs_mask_v4sf);
  }
#if defined(__AVX__)
  const vsda
  operator()(const vsda & data) const throw()
  {
    return _mm256_and_pd(data, abs_mask_v4df);
  }
  const vsfa
  operator()(const vsfa & data) const throw()
  {
    return _mm256_and_ps(data, abs_mask_v8sf);
  }
#endif
protected:
  const vsds abs_mask_v2df;
  const vsfs abs_mask_v4sf;
#if defined(__AVX__)
  const vsda abs_mask_v4df;
  const vsfa abs_mask_v8sf;
#endif
};

template<typename Type>
class OpBinaryPlusScaledDifference : public OpBinary {
public:
  typedef typename SIMDUnrolling<Type>::vs_s vs_s;
  typedef typename SIMDUnrolling<Type>::vs_a vs_a;

  OpBinaryPlusScaledDifference(const Type & _scale)
  : scale(_scale), scale_vs(Coeff<Type, VECTOR_SIZE_SSE>::get(_scale))
#if defined(__AVX__)
  , scale_va(Coeff<Type, VECTOR_SIZE_AVX>::get(_scale))
#endif
  { }

  const Type
  operator()(const Type & inData1, const Type & inData2) const throw()
  {
    return (inData1 + scale * (inData1 - inData2));
  }
  const vs_s
  operator()(const vs_s & inData1, const vs_s & inData2) const throw()
  {
    return (inData1 + scale_vs * (inData1 - inData2));
  }
#if defined(__AVX__)
  const vs_a
  operator()(const vs_a & inData1, const vs_a & inData2) const throw()
  {
    return (inData1 + scale_vs * (inData1 - inData2));
  }
#endif
protected:
  const Type scale;
  const vs_s scale_vs;
#if defined(__AVX__)
  const vs_a scale_va;
#endif
};


////////////////////////////////////////////////////////////////////////////////
// Comparison operators

template<typename Type>
class OpBinaryMax : public OpBinary {
public:
  template<const size_t vector_size>
  using VecType = SIMDRegister<Type, vector_size>;
  using VecTypeSSE = VecType<VECTOR_SIZE_SSE>;
  using VecTypeAVX = VecType<VECTOR_SIZE_AVX>;

  const Type
  operator()(const Type & in1, const Type & in2) const throw()
  {
    return std::max(in1, in2);
  }
  template<const size_t vector_size>
  const VecType<vector_size>
  operator()(const VecType<vector_size> & data1, const VecType<vector_size> & data2) const throw();
};

template<>
class OpBinaryMax<float> : public OpBinary {
public:
  template<const size_t vector_size>
  using VecType = SIMDRegister<float, vector_size>;
  using VecTypeSSE = VecType<VECTOR_SIZE_SSE>;
  using VecTypeAVX = VecType<VECTOR_SIZE_AVX>;

  const float
  operator()(const float & in1, const float & in2) const throw()
  {
    return std::max(in1, in2);
  }
  const VecTypeSSE
  operator()(const VecTypeSSE & in1, const VecTypeSSE & in2) const throw()
  {
    return _mm_max_ps(in1, in2);
  }
#if defined(__AVX__)
  const VecTypeAVX
  operator()(const VecTypeAVX & in1, const VecTypeAVX & in2) const throw()
  {
    return _mm256_max_ps(in1, in2);
  }
#endif
};

template<>
class OpBinaryMax<double> : public OpBinary {
public:
  template<const size_t vector_size>
  using VecType = SIMDRegister<double, vector_size>;
  using VecTypeSSE = VecType<VECTOR_SIZE_SSE>;
  using VecTypeAVX = VecType<VECTOR_SIZE_AVX>;

  const double
  operator()(const double & in1, const double & in2) const throw()
  {
    return std::max(in1, in2);
  }
  const VecTypeSSE
  operator()(const VecTypeSSE & in1, const VecTypeSSE & in2) const throw()
  {
    return _mm_max_pd(in1, in2);
  }
#if defined(__AVX__)
  const VecTypeAVX
  operator()(const VecTypeAVX & in1, const VecTypeAVX & in2) const throw()
  {
    return _mm256_max_pd(in1, in2);
  }
#endif
};

template<typename Type>
class OpBinaryMin : public OpBinary {
public:
  template<const size_t vector_size>
  using VecType = SIMDRegister<Type, vector_size>;
  using VecTypeSSE = VecType<VECTOR_SIZE_SSE>;
  using VecTypeAVX = VecType<VECTOR_SIZE_AVX>;

  const Type
  operator()(const Type & in1, const Type & in2) const throw()
  {
    return std::min(in1, in2);
  }
  template<const size_t vector_size>
  const VecType<vector_size>
  operator()(const VecType<vector_size> & data1, const VecType<vector_size> & data2) const throw();
};

template<>
class OpBinaryMin<float> : public OpBinary {
public:
  template<const size_t vector_size>
  using VecType = SIMDRegister<float, vector_size>;
  using VecTypeSSE = VecType<VECTOR_SIZE_SSE>;
  using VecTypeAVX = VecType<VECTOR_SIZE_AVX>;

  const float
  operator()(const float & in1, const float & in2) const throw()
  {
    return std::min(in1, in2);
  }
  const VecTypeSSE
  operator()(const VecTypeSSE & in1, const VecTypeSSE & in2) const throw()
  {
    return _mm_min_ps(in1, in2);
  }
#if defined(__AVX__)
  const VecTypeAVX
  operator()(const VecTypeAVX & in1, const VecTypeAVX & in2) const throw()
  {
    return _mm256_min_ps(in1, in2);
  }
#endif
};

template<>
class OpBinaryMin<double> : public OpBinary {
public:
  template<const size_t vector_size>
  using VecType = SIMDRegister<double, vector_size>;
  using VecTypeSSE = VecType<VECTOR_SIZE_SSE>;
  using VecTypeAVX = VecType<VECTOR_SIZE_AVX>;

  const double
  operator()(const double & in1, const double & in2) const throw()
  {
    return std::min(in1, in2);
  }
  const VecTypeSSE
  operator()(const VecTypeSSE & in1, const VecTypeSSE & in2) const throw()
  {
    return _mm_min_pd(in1, in2);
  }
#if defined(__AVX__)
  const VecTypeAVX
  operator()(const VecTypeAVX & in1, const VecTypeAVX & in2) const throw()
  {
    return _mm256_min_pd(in1, in2);
  }
#endif
};

template<typename Type>
using OpUnaryMaxScalar = OpUnaryGenericScalar<Type, OpBinaryMax>;

template<typename Type>
using OpUnaryMinScalar = OpUnaryGenericScalar<Type, OpBinaryMin>;

////////////////////////////////////////////////////////////////////////////////
// Complex math operations

template<typename Type>
class OpUnaryBoxOneL1 : public OpUnary {
public:
  template<const size_t vector_size>
  using VecType = SIMDRegister<Type, vector_size>;

  OpUnaryBoxOneL1() : op_max_1(1.0), op_abs() { }

  const Type
  operator()(const Type & data) const throw()
  {
    return data / op_max_1(op_abs(data));
  }
  template<const size_t vector_size>
  const VecType<vector_size>
  operator()(const VecType<vector_size> & data) const throw()
  {
    return data / op_max_1(op_abs(data));
  }
protected:
  const OpUnaryMaxScalar<Type> op_max_1;
  const OpUnaryAbs<Type> op_abs;
};

////////////////////////////////////////////////////////////////////////////////
// Conversion operators

//template<typename TypeIn, typename TypeOut>
//class OpUnaryConvert : public OpUnary {
//public:
//  template<const size_t vector_size>
//  using VecTypeIn = SIMDRegister<TypeIn, vector_size>;
//  using VecTypeInSSE = VecTypeIn<VECTOR_SIZE_SSE>;
//  using VecTypeInAVX = VecTypeIn<VECTOR_SIZE_AVX>;
//
//  template<const size_t vector_size>
//  using VecTypeOut = SIMDRegister<TypeOut, vector_size>;
//  using VecTypeOutSSE = VecTypeOut<VECTOR_SIZE_SSE>;
//  using VecTypeOutAVX = VecTypeOut<VECTOR_SIZE_AVX>;
//
//  const TypeOut
//  operator()(const TypeIn & in) const throw()
//  {
//    return (TypeOut) in;
//  }
//  template<const size_t vector_size>
//  const VecTypeOut
//  operator()(const VecTypeIn & in) const throw();
//};

////////////////////////////////////////////////////////////////////////////////
// Legacy operators

template<typename Type>
class OpUnaryAssignLegacy : public OpBinary {
public:
  typedef typename SIMDUnrolling<Type>::vVvf vVvf;

  const Type
  operator()(const Type & inData1, const Type & inData2) const throw()
  {
    return (inData2);
  }
  const vVvf
  operator()(const vVvf & inData1, const vVvf & inData2) const throw()
  {
    return (inData2);
  }
};

template<typename Type>
class OpUnaryPowLegacy : public OpBinary {
  const double exponent;
public:
  OpUnaryPowLegacy(const double & _exp) : exponent(_exp) { }

  const Type
  operator()(const Type & inData1, const Type & inData2) const throw()
  {
    return pow(inData2, exponent);
  }
  const vsds
  operator()(const vsds & inData1, const vsds & inData2) const throw()
  {
    const double * const inData1_d = (const double *) &inData1;
    return _mm_set_pd(
      pow(inData1_d[0], exponent), pow(inData1_d[1], exponent)
    );
  }
  const vsfs
  operator()(const vsfs & inData1, const vsfs & inData2) const throw()
  {
    const float * const inData1_s = (const float *) &inData1;
    return _mm_set_ps(
      pow(inData1_s[0], exponent), pow(inData1_s[1], exponent),
      pow(inData1_s[2], exponent), pow(inData1_s[3], exponent)
    );
  }
#if defined(__AVX__)
  const vsda
  operator()(const vsda & inData1, const vsda & inData2) const throw()
  {
    const double * const inData1_d = (const double *) &inData1;
    return _mm256_set_pd(
      pow(inData1_d[0], exponent), pow(inData1_d[1], exponent),
      pow(inData1_d[2], exponent), pow(inData1_d[3], exponent)
    );
  }
  const vsfa
  operator()(const vsfa & inData1, const vsfa & inData2) const throw()
  {
    const float * const inData1_s = (const float *) &inData1;
    return _mm256_set_ps(
      pow(inData1_s[0], exponent), pow(inData1_s[1], exponent),
      pow(inData1_s[2], exponent), pow(inData1_s[3], exponent),
      pow(inData1_s[4], exponent), pow(inData1_s[5], exponent),
      pow(inData1_s[6], exponent), pow(inData1_s[7], exponent)
    );
  }
#endif
};

template<typename Type>
class OpUnarySqrtLegacy : public OpBinary {
public:
  const Type
  operator()(const Type & inData1, const Type & inData2) const throw()
  {
    return sqrt(inData2);
  }
  const vsds
  operator()(const vsds & inData1, const vsds & inData2) const throw()
  {
    return _mm_sqrt_pd(inData2);
  }
  const vsfs
  operator()(const vsfs & inData1, const vsfs & inData2) const throw()
  {
    return _mm_sqrt_ps(inData2);
  }
#if defined(__AVX__)
  const vsda
  operator()(const vsda & inData1, const vsda & inData2) const throw()
  {
    return _mm256_sqrt_pd(inData2);
  }
  const vsfa
  operator()(const vsfa & inData1, const vsfa & inData2) const throw()
  {
    return _mm256_sqrt_ps(inData2);
  }
#endif
};

template<typename Type>
class OpUnaryNonNegLegacy : public OpBinary {
public:
  const Type
  operator()(const Type & inData1, const Type & inData2) const throw()
  {
    return inData1 * (inData1 > 0);
  }
  const vsfs
  operator()(const vsfs & inData1, const vsfs & inData2) const throw()
  {
    return _mm_max_ps(inData1, _mm_setzero_ps());
  }
  const vsds
  operator()(const vsds & inData1, const vsds & inData2) const throw()
  {
    return _mm_max_pd(inData1, _mm_setzero_pd());
  }
#if defined(__AVX__)
  const vsfa
  operator()(const vsfa & inData1, const vsfa & inData2) const throw()
  {
    return _mm256_max_ps(inData1, _mm256_setzero_ps());
  }
  const vsda
  operator()(const vsda & inData1, const vsda & inData2) const throw()
  {
    return _mm256_max_pd(inData1, _mm256_setzero_pd());
  }
#endif
protected:
};


template<typename Type>
class OpUnaryPlusScalarLegacy : public OpBinary {
public:
  typedef typename SIMDUnrolling<Type>::vVvf vVvf;

  OpUnaryPlusScalarLegacy(const Type & _scalar)
  : scalar(_scalar), scalar_v(Coeff<Type, VECTOR_SIZE_SSE>::get(_scalar)) { }

  const Type
  operator()(const Type & inData1, const Type & inData2) const throw()
  {
    return (inData2 + scalar);
  }
  const vVvf
  operator()(const vVvf & inData1, const vVvf & inData2) const throw()
  {
    return (inData2 + scalar_v);
  }
protected:
  const Type scalar;
  const vVvf scalar_v;
};

template<typename Type>
class OpUnaryTimesScalarLegacy : public OpBinary {
public:
  typedef typename SIMDUnrolling<Type>::vVvf vVvf;

  OpUnaryTimesScalarLegacy(const Type & _scalar)
  : scalar(_scalar), scalar_v(Coeff<Type, VECTOR_SIZE_SSE>::get(_scalar)) { }

  const Type
  operator()(const Type & inData1, const Type & inData2) const throw()
  {
    return (inData2 * scalar);
  }
  const vVvf
  operator()(const vVvf & inData1, const vVvf & inData2) const throw()
  {
    return (inData2 * scalar_v);
  }
protected:
  const Type scalar;
  const vVvf scalar_v;
};

////////////////////////////////////////////////////////////////////////////////
// Conditional Operators

//template<typename Type>
//class OpBinaryAssignConditional : public OpBinary {
//public:
//  typedef typename SIMDUnrolling<Type>::vVvf vVvf;
//
//  OpBinaryAssignConditional() : zero_v(Coeff<Type>::get(0.0f)) { }
//
//  const Type
//  operator()(const Type & outData, const Type & inData) const throw()
//  {
//    if (inData) {
//      return inData;
//    } else {
//      return outData;
//    }
//  }
//  const vVvf
//  operator()(const vVvf & outData, const vVvf & inData) const throw();
//
//protected:
//  const vVvf zero_v;
//};
//
//template<>
//inline const OpBinaryAssignConditional<float>::vVvf
//OpBinaryAssignConditional<float>::operator()(
//    const OpBinaryAssignConditional<float>::vVvf & outData,
//    const OpBinaryAssignConditional<float>::vVvf & inData) const throw()
//{
//#if defined(__AVX__)
//  const vVvf is_valid = _mm256_cmp_ps(inData1, zero_v, _CMP_NEQ_UQ);
//  return _mm256_blendv_ps(outData, inData, is_valid);
//  //return _mm256_or_ps(_mm256_and_ps(is_valid, inData), _mm256_andnot_ps(is_valid, outData));
//#else
//  const vVvf is_valid = _mm_cmpneq_ps(inData, zero_v);
//# if defined(__SSE4_1__)
//  return _mm_blendv_ps(outData, inData, is_valid);
//# else
//  return _mm_or_ps(_mm_and_ps(is_valid, inData), _mm_andnot_ps(is_valid, outData));
//# endif
//#endif
//}
//
//template<>
//inline const OpBinaryAssignConditional<double>::vVvf
//OpBinaryAssignConditional<double>::operator()(
//    const OpBinaryAssignConditional<double>::vVvf & outData,
//    const OpBinaryAssignConditional<double>::vVvf & inData) const throw()
//{
//#if defined(__AVX__)
//  const vVvf is_valid = _mm256_cmp_pd(inData1, zero_v, _CMP_NEQ_UQ);
//  return _mm256_blendv_pd(outData, inData, is_valid);
//  //return _mm256_or_pd(_mm256_and_pd(is_valid, inData), _mm256_andnot_pd(is_valid, outData));
//#else
//  const vVvf is_valid = _mm_cmpneq_pd(inData, zero_v);
//# if defined(__SSE4_1__)
//  return _mm_blendv_pd(outData, inData, is_valid);
//# else
//  return _mm_or_pd(_mm_and_pd(is_valid, inData), _mm_andnot_pd(is_valid, outData));
//# endif
//#endif
//}
//
//
//template<typename Type>
//class OpBinaryInterferenceConditional : public OpBinary {
//public:
//  typedef typename SIMDUnrolling<Type>::vVvf vVvf;
//
//  OpBinaryInterferenceConditional() : zero_v(Coeff<Type>::get(0.0f)), minusone_v(Coeff<Type>::get(-1.0f)) { }
//
//  const Type
//  operator()(const Type & voxel_op, const Type & voxel_ip) const throw()
//  {
//    if (voxel_ip) {
//      return (-1 + (! voxel_op) * (voxel_ip + 1));
//    } else {
//      return voxel_op;
//    }
//  }
//
//  const vVvf
//  operator()(const vVvf & voxel_op, const vVvf & voxel_ip) const throw();
//
//protected:
//  const vVvf zero_v;
//  const vVvf minusone_v;
//};
//
//template<>
//const OpBinaryInterferenceConditional<float>::vVvf
//OpBinaryInterferenceConditional<float>::operator()(
//    const OpBinaryInterferenceConditional<float>::vVvf & outData,
//    const OpBinaryInterferenceConditional<float>::vVvf & inData) const throw()
//{
//#if defined(__AVX__)
//  const vVvf is_valid = _mm256_cmp_ps(inData1, zero_v, _CMP_NEQ_UQ);
//  const vVvf has_conflict = _mm256_cmp_ps(outData, zero_v, _CMP_NEQ_UQ);
//  const vVvf conf_res = _mm256_blendv_ps(inData, minusone_v, has_conflict);
//  return _mm256_blendv_ps(outData, inData, is_valid);
//#else
//  const vVvf is_valid = _mm_cmpneq_ps(inData, zero_v);
//  const vVvf has_conflict = _mm_cmpneq_ps(outData, zero_v);
//# if defined(__SSE4_1__)
//  const vVvf conf_res = _mm_blendv_ps(inData, minusone_v, has_conflict);
//  return _mm_blendv_ps(outData, inData, is_valid);
//# else
//  const vVvf conf_res = _mm_or_ps(_mm_and_ps(has_conflict, minusone_v), _mm_andnot_ps(has_conflict, inData));
//  return _mm_or_ps(_mm_and_ps(is_valid, conf_res), _mm_andnot_ps(is_valid, outData));
//# endif
//#endif
//}
//
//template<>
//const OpBinaryInterferenceConditional<double>::vVvf
//OpBinaryInterferenceConditional<double>::operator()(
//    const OpBinaryInterferenceConditional<double>::vVvf & outData,
//    const OpBinaryInterferenceConditional<double>::vVvf & inData) const throw()
//{
//#if defined(__AVX__)
//  const vVvf is_valid = _mm256_cmp_pd(inData1, zero_v, _CMP_NEQ_UQ);
//  const vVvf has_conflict = _mm256_cmp_pd(outData, zero_v, _CMP_NEQ_UQ);
//  const vVvf conf_res = _mm256_blendv_pd(inData, minusone_v, has_conflict);
//  return _mm256_blendv_pd(outData, inData, is_valid);
//#else
//  const vVvf is_valid = _mm_cmpneq_pd(inData, zero_v);
//  const vVvf has_conflict = _mm_cmpneq_pd(outData, zero_v);
//# if defined(__SSE4_1__)
//  const vVvf conf_res = _mm_blendv_pd(inData, minusone_v, has_conflict);
//  return _mm_blendv_pd(outData, inData, is_valid);
//# else
//  const vVvf conf_res = _mm_or_pd(_mm_and_pd(has_conflict, minusone_v), _mm_andnot_pd(has_conflict, inData));
//  return _mm_or_pd(_mm_and_pd(is_valid, conf_res), _mm_andnot_pd(is_valid, outData));
//# endif
//#endif
//}


////////////////////////////////////////////////////////////////////////////////
// Vector class

namespace dct {

  template<typename Type, const size_t vector_size = VECTOR_SIZE_SSE>
  class VectorOperations {
  public:
    using VecType = SIMDRegister<Type, vector_size>;

  protected:
    AccessUnaligned<Type, vector_size> access_u;
    AccessStreamed<Type, vector_size> access_s;
    AccessAligned<Type, vector_size> access_a;

  public:
    constexpr size_t get_roundup_to_fit_moves(const size_t & line_size) {
      return ROUND_UP(line_size, vector_size / sizeof(Type));
    }
  };

  template<typename Type, const size_t vector_size = VECTOR_SIZE_SSE>
  class VectorAllocator : public std::allocator<Type> {
  public:
    Type * allocate(const size_t & num_elements){
      return (Type *) _mm_malloc(num_elements * sizeof(Type), vector_size);
    }
    void deallocate(Type * p) { _mm_free(p); }
  };

}  // namespace dct


#endif /* ZUTIL_CXX_INCLUDE_VECTORIZATION_H_ */
