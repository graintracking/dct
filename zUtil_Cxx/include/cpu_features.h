/*
 * cpu_features.h
 *
 *  Created on: Aug 29, 2017
 *      Author: Nicola Vigano', 2017, CWI, N.R.Vigano@cwi.nl
 */

#ifndef ZUTIL_CXX_INCLUDE_CPU_FEATURES_H_
#define ZUTIL_CXX_INCLUDE_CPU_FEATURES_H_

#if defined(_WIN32)
# define cpuid(info, x) __cpuidex(info, x, 0)
# define xgetbv(x) _xgetbv(x)
#else
# include <cpuid.h>
# include <stdint.h>
void cpuid(int info[4], int InfoType) {
  __cpuid_count(InfoType, 0, info[0], info[1], info[2], info[3]);
}
uint64_t xgetbv(unsigned int index){
    uint32_t eax, edx;
    __asm__ __volatile__("xgetbv" : "=a"(eax), "=d"(edx) : "c"(index));
    return ((uint64_t)edx << 32) | eax;
}
# define _XCR_XFEATURE_ENABLED_MASK 0
#endif

bool cpu_has_avx() {
  int cpu_info[4];
  cpuid(cpu_info, 1);
  return (cpu_info[2] & (1 << 28)) != 0;
}

bool can_use_avx() {
  int cpu_info[4];
  cpuid(cpu_info, 1);

  bool os_uses_XSAVE_XRSTORE = (cpu_info[2] & (1 << 27)) != 0;
  bool cpu_AVX_suport = (cpu_info[2] & (1 << 28)) != 0;

  if (os_uses_XSAVE_XRSTORE && cpu_AVX_suport) {
    uint64_t xcrFeatureMask = xgetbv(_XCR_XFEATURE_ENABLED_MASK);
    return (xcrFeatureMask & 0x6) == 0x6;
  }
  return false;
}

#endif /* ZUTIL_CXX_INCLUDE_CPU_FEATURES_H_ */
