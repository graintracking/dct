/*
 * DctMatlabData.h
 *
 *  Created on: Jan 23, 2017
 *      Author: Nicola Vigano', 2017, CWI, N.R.Vigano@cwi.nl
 */

#ifndef ZUTIL_CXX_INCLUDE_DCTMATLABDATA_H_
#define ZUTIL_CXX_INCLUDE_DCTMATLABDATA_H_

#include <mex.h>

#if (MX_API_VER < 0x07400000)
extern "C"
{
  bool mxUnshareArray(mxArray *array_ptr, bool noDeepCopy);
  mxArray *mxCreateSharedCopy(const mxArray *pr);
  mxArray *mxCreateSharedDataCopy(const mxArray *pr);
  mxArray *mxUnreference(const mxArray *pr);
}
#endif

#include "DctData.h"
#include "DctDataAlgorithms.h"

namespace dct {

  template<typename Type>
  class MatlabAllocator {
  public:
    Type * allocate(const size_t & num_elements){
      return (Type *) mxMalloc(num_elements * sizeof(Type));
    }
    void deallocate(Type * p) { mxFree(p); }
  };

  template<typename Type>
  class DctMatlabDatatype {
  public:
    static constexpr mxClassID get_matlab_type() noexcept;
  };

  template<>
  inline constexpr mxClassID
  DctMatlabDatatype<double>::get_matlab_type() noexcept { return mxDOUBLE_CLASS; }

  template<>
  inline constexpr mxClassID
  DctMatlabDatatype<float>::get_matlab_type() noexcept { return mxSINGLE_CLASS; }

  class DctMatlabData {
  protected:
    template<typename Type, class DataContainer = DctData<Type, MatlabAllocator<Type> > >
    static mxArray * create_cell_from_vector(const std::vector<DataContainer> &, const std::vector<size_t> &);

    template<typename Type>
    static mxArray * create_array_from_data(const DctData<Type, MatlabAllocator<Type> > &);

  public:
    static mxClassID get_class_of_cell(const mxArray * const cell);

    static std::string get_string(const mxArray * const str);

    template<typename Type>
    static DctProjectionVector<Type, MatlabAllocator<Type> > import_collection(mxArray * const);

    template<typename Type>
    static DctProjectionVector<Type, MatlabAllocator<Type> > create_collection_from_dims(const mxArray * const);

    template<typename Type>
    static DctProjectionVector<Type, MatlabAllocator<Type> > create_projection_vector_like(const mxArray * const);

    template<typename Type>
    static DctVolumeGrid<Type, MatlabAllocator<Type> > import_volume_grid(mxArray * const);

    template<typename Type>
    static DctProjectionTransformCoefficients<Type> import_coefficients(const mxArray * const);
    template<typename Type>
    static std::vector<DctProjectionTransformCoefficients<Type> > import_coefficients_list(const mxArray * const);

    static mxArray * vol_allocate(const size_t & num_dims, const size_t * const dims, const mxClassID & type);

    template<typename Type>
    static mxArray * produce_collection(const DctProjectionVector<Type, MatlabAllocator<Type> > &, const bool force_cell);

    template<typename Type>
    static mxArray * produce_volume_grid(const DctVolumeGrid<Type, MatlabAllocator<Type> > &, const int32_t & dim);
  };

} /* namespace dct */


// Ugly trick to not deal with instantiation

#include "exceptions.h"

#include <emlrt.h>

namespace dct {

  mxClassID
  DctMatlabData::get_class_of_cell(const mxArray * const cell_array)
  {
    const mwSize numElems = mxGetNumberOfElements(cell_array);
    if (numElems > 0) {
      const mxArray* const firstArray = mxGetCell(cell_array, 0);
      const mxClassID first_class = mxGetClassID(firstArray);

      if (first_class == mxSINGLE_CLASS || first_class == mxDOUBLE_CLASS) {
        for (mwIndex count = 1; count < numElems; count++) {
          const mxArray* const array = mxGetCell(cell_array, count);

          if (first_class != mxGetClassID(array)) {
            // Damn mixed types!
            return mxUNKNOWN_CLASS;
          }
        }
        // OK it is a float type!
        return first_class;
      } else {
        // It is still an error!
        return first_class;
      }
    } else {
      return mxUNKNOWN_CLASS;
    }
  }

  std::string
  DctMatlabData::get_string(const mxArray * const str)
  {
    if (mxIsChar(str)) {
      mwSize str_len = mxGetNumberOfElements(str) + 1;
      char * buffer = new char[str_len];
      mxGetString(str, buffer, str_len);
      std::string result = std::string(buffer);
      delete[] buffer;
      return result;
    } else {
      return std::string("");
    }
  }

  template<typename Type>
  DctProjectionVector<Type, MatlabAllocator<Type> >
  DctMatlabData::import_collection(mxArray * const mxdata)
  {
    DctProjectionVector<Type, MatlabAllocator<Type> > output;

    bool is_cell = mxIsCell(mxdata);
    const mxClassID datatype = is_cell
        ? get_class_of_cell(mxdata) : mxGetClassID(mxdata);

    if (DctMatlabDatatype<Type>::get_matlab_type() != datatype) {
      THROW(WrongArgumentException, DctMatlabData, "Input array should be of the same type as the object type");
    }

    if (is_cell) {
      const mwSize tot_cells = mxGetNumberOfElements(mxdata);
      output.reserve(tot_cells);
      for (mwIndex num_cell = 0; num_cell < tot_cells; num_cell++) {
        mxArray * cell = mxGetCell(mxdata, num_cell);

        const mwSize * dims = mxGetDimensions(cell);
        const mwSize num_dims = mxGetNumberOfDimensions(cell);
        Type * const data = (Type *)mxGetData(cell);

        output.emplace_back(dims, num_dims, 1, data);
      }
    } else {
      const mwSize * dims = mxGetDimensions(mxdata);
      const mwSize num_dims = mxGetNumberOfDimensions(mxdata);
      Type * const data = (Type *)mxGetData(mxdata);

      output.emplace_back(dims, num_dims, 1, data);
    }

    return output;
  }

  template<typename Type>
  DctProjectionVector<Type, MatlabAllocator<Type> >
  DctMatlabData::create_collection_from_dims(const mxArray * const mxdata)
  {
    DctProjectionVector<Type, MatlabAllocator<Type> > output;

    const mwSize num_dims_dims = mxGetNumberOfDimensions(mxdata);
    const mwSize * dims_dims = mxGetDimensions(mxdata);

    const mwSize num_dims_sinos = dims_dims[1];

    if (!(mxIsNumeric(mxdata) && num_dims_dims == 2 && num_dims_sinos == 3)) {
      THROW(WrongArgumentException, DctMatlabData, "Array dimensions should be in a Mx3 matrix, where M is the number of volumes");
    }

    const mxClassID datatype = mxGetClassID(mxdata);

    if (mxDOUBLE_CLASS != datatype) {
      THROW(WrongArgumentException, DctMatlabData, "Array dimensions should be 'double'");
    }
//    if (mxDOUBLE_CLASS != datatype && mxSINGLE_CLASS != datatype) {
//      THROW(WrongArgumentException, DctMatlabData, "Array dimensions should either be 'single' or 'double'");
//    }

    const mwSize tot_projs = dims_dims[0];
    output.reserve(tot_projs);

    const double * const dims_sinos = (const double *) mxGetData(mxdata);

    for (mwIndex num_proj = 0; num_proj < tot_projs; num_proj++) {
      output.emplace_back(dims_sinos + num_proj, num_dims_sinos, tot_projs);
    }

    output.allocate();

    return output;
  }

  template<typename Type>
  DctProjectionVector<Type, MatlabAllocator<Type> >
  DctMatlabData::create_projection_vector_like(const mxArray * const mxdata)
  {
    DctProjectionVector<Type, MatlabAllocator<Type> > output;

    bool is_cell = mxIsCell(mxdata);
    const mxClassID datatype = is_cell
        ? get_class_of_cell(mxdata) : mxGetClassID(mxdata);

    if (DctMatlabDatatype<Type>::get_matlab_type() != datatype) {
      THROW(WrongArgumentException, DctMatlabData, "Input array should be of the same type as the object type");
    }

    if (is_cell) {
      const mwSize tot_cells = mxGetNumberOfElements(mxdata);
      output.reserve(tot_cells);
      for (mwIndex num_cell = 0; num_cell < tot_cells; num_cell++) {
        mxArray * cell = mxGetCell(mxdata, num_cell);

        const mwSize * dims = mxGetDimensions(cell);
        const mwSize num_dims = mxGetNumberOfDimensions(cell);

        output.emplace_back(dims, num_dims, 1);
      }
    } else {
      const mwSize * dims = mxGetDimensions(mxdata);
      const mwSize num_dims = mxGetNumberOfDimensions(mxdata);

      output.emplace_back(dims, num_dims, 1);
    }

    output.allocate();

    return output;
  }

  template<typename Type>
  DctVolumeGrid<Type, MatlabAllocator<Type> >
  DctMatlabData::import_volume_grid(mxArray * const mxdata)
  {
    DctVolumeGrid<Type, MatlabAllocator<Type> > output;

    bool is_cell = mxIsCell(mxdata);
    const mxClassID datatype = is_cell
        ? get_class_of_cell(mxdata) : mxGetClassID(mxdata);

    if (DctMatlabDatatype<Type>::get_matlab_type() != datatype) {
      THROW(WrongArgumentException, DctMatlabData, "Input array should be of the same type as the object type");
    }

    if (is_cell)
    {
      const mwSize tot_cells = mxGetNumberOfElements(mxdata);
      output.reserve(tot_cells);
      for (mwIndex num_cell = 0; num_cell < tot_cells; num_cell++) {
        mxArray * cell = mxGetCell(mxdata, num_cell);

        const mwSize * dims = mxGetDimensions(cell);
        const mwSize num_dims = mxGetNumberOfDimensions(cell);
        Type * const data = (Type *)mxGetData(cell);

        output.emplace_back(dims, num_dims, 1, data);
      }

      const mwSize * cells_dims = mxGetDimensions(mxdata);
      const mwSize cells_num_dims = mxGetNumberOfDimensions(mxdata);
      output.set_dims(cells_dims, cells_num_dims, 1);
    }
    else
    {
      const mwSize * dims = mxGetDimensions(mxdata);
      const mwSize num_dims = mxGetNumberOfDimensions(mxdata);
      Type * const data = (Type *)mxGetData(mxdata);

      output.emplace_back(dims, num_dims, 1, data);

      std::vector<size_t> grid_dims = {1};
      grid_dims[0] = 1;
      output.set_dims(grid_dims);
    }

    return output;
  }

  template<typename Type>
  DctProjectionTransformCoefficients<Type>
  DctMatlabData::import_coefficients(const mxArray * const coeff)
  {
    DctProjectionTransformCoefficients<Type> output;

    if (!mxIsStruct(coeff)) {
      THROW(WrongArgumentException, DctMatlabData,
          "Invalid Coeffs: third argument should be a structure!");
    }
    const mxArray * const sino_offsets_a = mxGetField(coeff, 0, "sino_offsets");
    const mxArray * const blob_offsets_a = mxGetField(coeff, 0, "blob_offsets");
    const mxArray * const proj_offsets_a = mxGetField(coeff, 0, "proj_offsets");
    const mxArray * const proj_coeffs_a = mxGetField(coeff, 0, "proj_coeffs");

    const mxArray * const proj_shifts_uv_a = mxGetField(coeff, 0, "proj_shifts_uv");
    output.use_shifts = proj_shifts_uv_a != nullptr;

    if (!sino_offsets_a) {
      THROW(WrongArgumentException, DctMatlabData,
          "Invalid Coeffs: 'sino_offsets' is missing!");
    }
    if (!blob_offsets_a) {
      THROW(WrongArgumentException, DctMatlabData,
          "Invalid Coeffs: 'blob_offsets' is missing!");
    }
    if (!proj_offsets_a) {
      THROW(WrongArgumentException, DctMatlabData,
          "Invalid Coeffs: 'proj_offsets' is missing!");
    }
    if (!proj_coeffs_a) {
      THROW(WrongArgumentException, DctMatlabData,
          "Invalid Coeffs: 'proj_coeffs' is missing!");
    }

    const mwSize num_elems = mxGetNumberOfElements(blob_offsets_a);
    if (num_elems != mxGetNumberOfElements(sino_offsets_a)) {
      THROW(WrongArgumentException, DctMatlabData,
          "Invalid Coeffs: number of 'sino_offsets' is different from 'blob_offsets'!");
    }
    if (num_elems != mxGetNumberOfElements(proj_offsets_a)) {
      THROW(WrongArgumentException, DctMatlabData,
          "Invalid Coeffs: number of 'proj_offsets' is different from 'blob_offsets'!");
    }
    if (num_elems != mxGetNumberOfElements(proj_coeffs_a)) {
      THROW(WrongArgumentException, DctMatlabData,
          "Invalid Coeffs: number of 'proj_coeffs' is different from 'blob_offsets'!");
    }
    if (output.use_shifts && (num_elems * 2) != mxGetNumberOfElements(proj_shifts_uv_a)) {
      THROW(WrongArgumentException, DctMatlabData,
          "Invalid Coeffs: number of 'proj_shifts_uv' is different from 'blob_offsets'!");
    }

    if (mxDOUBLE_CLASS != mxGetClassID(sino_offsets_a)) {
      THROW(WrongArgumentException, DctMatlabData,
          "Invalid Coeffs: type of 'sino_offsets' should be 'double'!");
    }
    if (mxDOUBLE_CLASS != mxGetClassID(blob_offsets_a)) {
      THROW(WrongArgumentException, DctMatlabData,
          "Invalid Coeffs: type of 'blob_offsets' should be 'double'!");
    }
    if (mxDOUBLE_CLASS != mxGetClassID(proj_offsets_a)) {
      THROW(WrongArgumentException, DctMatlabData,
          "Invalid Coeffs: type of 'proj_offsets' should be 'double'!");
    }
    if (mxDOUBLE_CLASS != mxGetClassID(proj_coeffs_a)) {
      THROW(WrongArgumentException, DctMatlabData,
          "Invalid Coeffs: type of 'proj_coeffs' should be 'double'!");
    }
    if (output.use_shifts && mxDOUBLE_CLASS != mxGetClassID(proj_shifts_uv_a)) {
      THROW(WrongArgumentException, DctMatlabData,
          "Invalid Coeffs: type of 'proj_shifts_uv' should be 'double'!");
    }

    output.resize(num_elems);

    const double * const sinos_pos = (const double *) mxGetData(sino_offsets_a);
    const double * const blobs_pos = (const double *) mxGetData(blob_offsets_a);
    const double * const projs_pos = (const double *) mxGetData(proj_offsets_a);

    // Converting from Matlab's indexing, starting from 1, to C's indexing starting from 0
    OpUnaryPlusScalar<double> op(-1);

    std::transform(sinos_pos, sinos_pos + num_elems, output.dest_slice.begin(), op);
    std::transform(blobs_pos, blobs_pos + num_elems, output.src_vol.begin(), op);
    std::transform(projs_pos, projs_pos + num_elems, output.src_slice.begin(), op);

    const double * const coeffs = (const double *) mxGetData(proj_coeffs_a);
    std::copy_n(coeffs, num_elems, output.coeff.begin());

    if (output.use_shifts) {
      const double * const shifts_uv = (const double *) mxGetData(proj_shifts_uv_a);
      for (size_t ind = 0; ind < num_elems; ind++) {
        // shifts always refer to inside the blobs
        output.shift_u[ind] = shifts_uv[ind*2];
        output.shift_v[ind] = shifts_uv[ind*2+1];
      }
    } else {
      std::fill_n(output.shift_u.begin(), num_elems, 0);
      std::fill_n(output.shift_v.begin(), num_elems, 0);
    }

    return output;
  }

  template<typename Type>
  std::vector<DctProjectionTransformCoefficients<Type> >
  DctMatlabData::import_coefficients_list(const mxArray * const coeffs)
  {
    std::vector<DctProjectionTransformCoefficients<Type> > output;

    const mwSize num_coeff_structs = mxGetNumberOfElements(coeffs);
    output.reserve(num_coeff_structs);

    for (mwIndex num_struct = 0; num_struct < num_coeff_structs; num_struct++) {
      const mxArray * const coeff_cell = mxGetCell(coeffs, num_struct);
      output.emplace_back(DctMatlabData::import_coefficients<Type>(coeff_cell));
    }

    return output;
  }

  mxArray *
  DctMatlabData::vol_allocate(const size_t & num_dims, const size_t * const dims, const mxClassID & type)
  {
    #if defined(EMLRT_VERSION_INFO) && EMLRT_VERSION_INFO >= 0x2015a && !defined(MX_COMPAT_32)
        return mxCreateUninitNumericArray(num_dims, (mwSize *)dims, type, mxREAL);
    #else
        const mwSize zero_dims[2] = {0, 0};
        mxArray * const xOutput = mxCreateNumericArray(2, zero_dims, type, mxREAL);
        mxSetDimensions(xOutput, dims, num_dims);
        const mwSize num_elems = mxGetNumberOfElements(xOutput);
        const mwSize elem_size = mxGetElementSize(xOutput);
        mxSetData(xOutput, mxMalloc(elem_size * num_elems));
        return xOutput;
    #endif
  }

  template<typename Type>
  mxArray *
  DctMatlabData::produce_collection(const DctProjectionVector<Type, MatlabAllocator<Type> > & vec, const bool force_cell)
  {
    const size_t tot_cells = vec.size();
    if (tot_cells > 1 || force_cell) {
      const std::vector<size_t> out_dims = {tot_cells, 1};
      return DctMatlabData::create_cell_from_vector<Type>(vec, out_dims);
    } else {
      return DctMatlabData::create_array_from_data<Type>(vec[0]);
    }
  }

  template<typename Type>
  mxArray *
  DctMatlabData::produce_volume_grid(const DctVolumeGrid<Type, MatlabAllocator<Type> > & grid, const int32_t & dim)
  {
    if (dim >= 0) {
      const std::vector<size_t> & grid_dims = grid.get_grid_dims();
      return DctMatlabData::create_cell_from_vector<Type>(grid, grid_dims);
    } else {
      return DctMatlabData::create_array_from_data<Type>(grid[0]);
    }
  }

  template<typename Type, class DataContainer>
  mxArray *
  DctMatlabData::create_cell_from_vector(const std::vector<DataContainer> & vec, const std::vector<size_t> & dims)
  {
    mxArray * const output = mxCreateCellArray(dims.size(), dims.data());

    for (mwIndex num_cell = 0; num_cell < vec.size(); num_cell++) {
      mxArray * const cell = DctMatlabData::create_array_from_data<Type>(vec[num_cell]);
      mxSetCell(output, num_cell, cell);
    }

    return output;
  }

  template<typename Type>
  mxArray *
  DctMatlabData::create_array_from_data(const DctData<Type, MatlabAllocator<Type> > & vol)
  {
    const mwSize zero_dims[2] = {0, 0};

    const mxClassID datatype = DctMatlabDatatype<Type>::get_matlab_type();

    mxArray * const output = mxCreateNumericArray(2, zero_dims, datatype, mxREAL);

    const size_t * const dims = vol.get_dims();
    mxSetDimensions(output, dims, 3);

    mxSetData(output, vol.get_data());

    return output;
  }

} /* namespace dct */

#endif /* ZUTIL_CXX_INCLUDE_DCTMATLABDATA_H_ */
