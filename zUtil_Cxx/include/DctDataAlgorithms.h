/*
 * DctDataAlgorithms.h
 *
 *  Created on: Feb 5, 2017
 *      Author: Nicola Vigano', 2017, CWI, N.R.Vigano@cwi.nl
 */

#ifndef ZUTIL_CXX_INCLUDE_DCTDATAALGORITHMS_H_
#define ZUTIL_CXX_INCLUDE_DCTDATAALGORITHMS_H_

#include <DctData.h>

#ifdef ENABLE_CALLGRIND
# include <valgrind/callgrind.h>
#endif

namespace dct {

  struct ChunkSpecs {
    size_t data_ind;
    size_t chunk_start;
    size_t chunk_size;
  };

  template<typename Type, class Alloc = VectorAllocator<Type>, const size_t vector_size = VECTOR_SIZE_SSE>
  class DctDataProcess : public VectorOperations<Type, vector_size> {
  public:
    typedef typename VectorOperations<Type, vector_size>::VecType vVvf;

  protected:
    const size_t num_threads;

    ThreadSafeQueue< std::function<void()> > work_queue;

    void initialize_line(Type * __restrict const out, const size_t & line_length, const Type & val = 0.0);
    template<class Op>
    void transform_line_unary(Type * __restrict const outin, const size_t & line_length, Op op);
    template<class Op>
    void transform_line_unary(Type * __restrict const out, const Type * __restrict const in, const size_t & line_length, Op op);
    template<class Op>
    void transform_line_binary(Type * __restrict const out, const Type * __restrict const in, const size_t & line_length, Op op);
    template<class Op>
    void transform_line_binary(Type * __restrict const out, const Type * __restrict const in1, const Type * __restrict const in2, const size_t & line_length, Op op);
    void copy_line(Type * __restrict const out, const Type * __restrict const in, const size_t & line_length);

    void worker_thread() {
#ifdef ENABLE_CALLGRIND
      CALLGRIND_TOGGLE_COLLECT;
#endif
      std::function < void() > task;
      while (work_queue.try_pop(task)) { task(); }
#ifdef ENABLE_CALLGRIND
      CALLGRIND_TOGGLE_COLLECT;
#endif
    }

    void process_tasks(std::vector< std::function<void()> > & tasks);

    DctDataProcess(const size_t & num_threads = std::thread::hardware_concurrency())
    : num_threads(num_threads) { }

    std::vector<ChunkSpecs> partition_grid_volumes(const size_t tot_vols, const size_t tot_elems_per_vol);
    std::vector<ChunkSpecs> partition_proj_vector(const size_t tot_vols, const std::vector<size_t> & tot_elems_each_vol);
  };

  //////////////////////////////////////////////////////////////////////////////
  // Initializer class

  template<typename Type, class Alloc = VectorAllocator<Type>, const size_t vector_size = VECTOR_SIZE_SSE>
  struct DctDataInitializer : public DctDataProcess<Type, Alloc, vector_size> {
  public:
    typedef typename DctDataProcess<Type, Alloc, vector_size>::vVvf vVvf;

  protected:
    const Type val;

    void initialize_line(Type * const __restrict data, const size_t & line_length, const Type & val = 0.0);
    void initialize_chunk(const DctData<Type, Alloc> & vol, const size_t & start_pos, const size_t & chunk_size);

  public:
    DctDataInitializer(const Type & value, const size_t & num_threads = std::thread::hardware_concurrency())
    : DctDataProcess<Type, Alloc, vector_size>(num_threads), val(value) { }

    void initialize(DctProjectionVector<Type, Alloc> & c);
    void initialize(DctVolumeGrid<Type, Alloc> & c);
  };

  //////////////////////////////////////////////////////////////////////////////
  // Projection transforms

  template<typename Type>
  struct DctProjectionTransformCoefficients {
    size_t num_projections;
    bool use_shifts;

    std::vector<size_t> dest_slice; // <-- slice in destination sinogram / blob

    std::vector<size_t> src_vol; // <-- position of source blob / sinogram
    std::vector<size_t> src_slice; // <-- slice in source blob / sinogram

    std::vector<Type> coeff; // <-- saxpy coefficient

    std::vector<size_t> shift_u; // <-- U shift for sinos into blobs
    std::vector<size_t> shift_v; // <-- V shift for sinos into blobs

  public:
    DctProjectionTransformCoefficients() : num_projections(0), use_shifts(false) { };
    DctProjectionTransformCoefficients(const size_t & num_projs, const bool & do_use_shifts)
    : num_projections(num_projs), use_shifts(do_use_shifts)
    {
      this->resize(num_projs);
    }

    void resize(const size_t & num_projs) {
      num_projections = num_projs;

      dest_slice.resize(num_projs);
      src_vol.resize(num_projs);
      src_slice.resize(num_projs);
      coeff.resize(num_projs);

      shift_u.resize(num_projs);
      shift_v.resize(num_projs);
    }
    void reserve(const size_t & num_projs) {
      dest_slice.reserve(num_projs);
      src_vol.reserve(num_projs);
      src_slice.reserve(num_projs);
      coeff.reserve(num_projs);

      shift_u.reserve(num_projs);
      shift_v.reserve(num_projs);
    }
    void emplace_back(const size_t & d_slice, const size_t & s_vol, const size_t & s_slice, const Type & c, const size_t & d_shift_u = 0, const size_t & d_shift_v = 0)
    {
      dest_slice.emplace_back(d_slice);
      src_vol.emplace_back(s_vol);
      src_slice.emplace_back(s_slice);
      coeff.emplace_back(c);

      shift_u.emplace_back(d_shift_u);
      shift_v.emplace_back(d_shift_v);

      num_projections++;
    }
  };

  template<typename Type, class Alloc = VectorAllocator<Type>, const size_t vector_size = VECTOR_SIZE_SSE>
  class DctProjectionTransform : public DctDataProcess<Type, Alloc, vector_size> {
  public:
    typedef typename DctDataProcess<Type, Alloc, vector_size>::vVvf vVvf;

  protected:
    DctProjectionVector<Type, Alloc> blobs;
    DctProjectionVector<Type, Alloc> sinos;

    std::vector<DctProjectionTransformCoefficients<Type> > transforms_to_sinos;
    std::vector<DctProjectionTransformCoefficients<Type> > transforms_to_blobs;

    size_t blobs_size_u;
    size_t blobs_size_v;
    size_t sinos_size_u;
    size_t sinos_size_v;

    bool use_shifts;

  public:
    DctProjectionTransform(const size_t & num_threads = std::thread::hardware_concurrency())
    : DctDataProcess<Type, Alloc, vector_size>(num_threads)
    , blobs_size_u(0), blobs_size_v(0), sinos_size_u(0), sinos_size_v(0)
    , use_shifts(false)
    { }

    void set_blobs(const DctProjectionVector<Type, Alloc> & new_blobs);
    void set_sinos(const DctProjectionVector<Type, Alloc> & new_sinos);
    void set_transforms(const std::vector<DctProjectionTransformCoefficients<Type> > & transforms);

    void set_blobs(const DctProjectionVector<Type, Alloc> && new_blobs);
    void set_sinos(const DctProjectionVector<Type, Alloc> && new_sinos);
    void set_transforms(const std::vector<DctProjectionTransformCoefficients<Type> > && transforms);

    void check_consistency() const;

    void produce_sinograms();
    void produce_blobs();

    const DctProjectionVector<Type, Alloc> & get_sinos() const noexcept { return sinos; }
    const DctProjectionVector<Type, Alloc> & get_blobs() const noexcept { return blobs; }

  protected:
    template<typename FunctionType>
    void submit_transforms(FunctionType func, const size_t & tot_vols);

    void transform_blobs2sino(const size_t & sino_num, const size_t & min_v, const size_t & max_v);
    void transform_sinos2blob(const size_t & blob_num, const size_t & min_v, const size_t & max_v);

    void initialize_transforms_to_blobs();
  };

  //////////////////////////////////////////////////////////////////////////////
  // Volume Grid transform

  template<typename Type, class Alloc = VectorAllocator<Type>, const size_t vector_size = VECTOR_SIZE_SSE>
  class DctVolumeGridTransform : public DctDataProcess<Type, Alloc, vector_size> {
  public:
    typedef typename DctDataProcess<Type, Alloc, vector_size>::vVvf vVvf;

  protected:
    DctVolumeGrid<Type, Alloc> vols;

  public:
    DctVolumeGridTransform(const size_t & num_threads = std::thread::hardware_concurrency())
    : DctDataProcess<Type, Alloc>(num_threads), vols() { }
    DctVolumeGridTransform(DctVolumeGrid<Type, Alloc> & vols_, const size_t & num_threads = std::thread::hardware_concurrency())
    : DctDataProcess<Type, Alloc>(num_threads), vols(vols_) { }

    void set_vols(DctVolumeGrid<Type, Alloc> & new_vols);
    DctVolumeGrid<Type, Alloc> & get_vols() { return this->vols; }
    const DctVolumeGrid<Type, Alloc> & get_vols() const { return this->vols; }

    template<class Op>
    DctVolumeGrid<Type, Alloc> accumulate(const int32_t & dim = -1, Op op = OpBinaryPlus<Type>());

    template<class Op>
    void volume_wise_unary(Op op);
    template<class Op>
    void volume_wise_unary(Op op, const DctVolumeGrid<Type, Alloc> &);
    template<class Op>
    void volume_wise_binary(Op op, const DctVolumeGrid<Type, Alloc> &);
    template<class Op>
    void volume_wise_binary(Op op, const DctVolumeGrid<Type, Alloc> &, const DctVolumeGrid<Type, Alloc> &);

  protected:
    template<class Op>
    void accumulate_sub_block(DctData<Type, Alloc> & out, const size_t & chunk_start, const size_t & chunk_size, const std::vector<size_t> & vols_list, Op op);

    template<class Op>
    void op_element_wise_sub_block_unary_inplace(Op op, DctData<Type, Alloc> & out, const ChunkSpecs & cs);
    template<class Op>
    void op_element_wise_sub_block_unary_copy(Op op, DctData<Type, Alloc> & out, const DctData<Type, Alloc> & in, const ChunkSpecs & cs);
    template<class Op>
    void op_element_wise_sub_block_binary_inplace(Op op, DctData<Type, Alloc> & out, const DctData<Type, Alloc> & in, const ChunkSpecs & cs);
    template<class Op>
    void op_element_wise_sub_block_binary_copy(Op op, DctData<Type, Alloc> & out, const DctData<Type, Alloc> & in1, const DctData<Type, Alloc> & in2, const ChunkSpecs & cs);
  };

}  // namespace dct

namespace dct {

  //////////////////////////////////////////////////////////////////////////////
  // DctDataProcess

  template<typename Type, class Alloc, const size_t vector_size>
  inline void
  DctDataProcess<Type, Alloc, vector_size>::initialize_line(Type * const __restrict out, const size_t & line_length, const Type & val)
  {
    const SIMDUnrolling<Type, vector_size> simd4(4);
    const SIMDUnrolling<Type, vector_size> simd1(1);

    const SIMDRegister<Type, vector_size> vec_val = Coeff<Type, vector_size>::get(val);

    Type * __restrict temp_out = out;
    Type * __restrict const end_line_out = out + line_length;

    const size_t shift_to_align = simd1.get_shift_to_align(out);
    if (shift_to_align < line_length)
    {
      Type * __restrict const out_a = out + shift_to_align;
      const size_t line_length_a = line_length - shift_to_align;

      Type * __restrict const end_line_u4 = out_a + simd4.get_unroll(line_length_a);
      Type * __restrict const end_line_u1 = out_a + simd1.get_unroll(line_length_a);

      for (; temp_out < out_a; temp_out++)
      {
        *temp_out = val;
      }
      for (; temp_out < end_line_u4; temp_out += simd4.block)
      {
        this->access_u.store(temp_out + 0 * simd4.shift, vec_val);
        this->access_u.store(temp_out + 1 * simd4.shift, vec_val);
        this->access_u.store(temp_out + 2 * simd4.shift, vec_val);
        this->access_u.store(temp_out + 3 * simd4.shift, vec_val);
      }
      for (; temp_out < end_line_u1; temp_out += simd1.block)
      {
        this->access_u.store(temp_out, vec_val);
      }
    }
    for (; temp_out < end_line_out; temp_out++)
    {
      *temp_out = val;
    }
  }

  template<typename Type, class Alloc, const size_t vector_size>
  template<class Op>
  inline void
  DctDataProcess<Type, Alloc, vector_size>::transform_line_unary(Type * __restrict const inout, const size_t & line_length, Op op)
  {
    const SIMDUnrolling<Type, vector_size> simd4(4);
    const SIMDUnrolling<Type, vector_size> simd1(1);

    Type * __restrict temp_inout = inout;
    const Type * __restrict const end_line_inout = inout + line_length;

    const size_t shift_to_align = simd1.get_shift_to_align(inout);
    if (shift_to_align < line_length)
    {
      Type * __restrict const out_a = inout + shift_to_align;
      const size_t line_length_a = line_length - shift_to_align;

      const Type * __restrict const end_line_inout_u4 = out_a + simd4.get_unroll(line_length_a);
      const Type * __restrict const end_line_inout_u1 = out_a + simd1.get_unroll(line_length_a);

      for (; temp_inout < out_a; temp_inout++)
      {
        *temp_inout = op(*temp_inout);
      }
      for (; temp_inout < end_line_inout_u4; temp_inout += simd4.block)
      {
        const vVvf & a0 = this->access_u.load(temp_inout + 0 * simd4.shift);
        const vVvf & a1 = this->access_u.load(temp_inout + 1 * simd4.shift);
        const vVvf & a2 = this->access_u.load(temp_inout + 2 * simd4.shift);
        const vVvf & a3 = this->access_u.load(temp_inout + 3 * simd4.shift);

        const vVvf c0 = op(a0);
        const vVvf c1 = op(a1);
        const vVvf c2 = op(a2);
        const vVvf c3 = op(a3);

        this->access_u.store(temp_inout + 0 * simd4.shift, c0);
        this->access_u.store(temp_inout + 1 * simd4.shift, c1);
        this->access_u.store(temp_inout + 2 * simd4.shift, c2);
        this->access_u.store(temp_inout + 3 * simd4.shift, c3);
      }
      for (; temp_inout < end_line_inout_u1; temp_inout += simd1.block)
      {
        const vVvf & a0 = this->access_u.load(temp_inout);
        const vVvf c0 = op(a0);
        this->access_u.store(temp_inout, c0);
      }
    }
    for (; temp_inout < end_line_inout; temp_inout++)
    {
      *temp_inout = op(*temp_inout);
    }
  }

  template<typename Type, class Alloc, const size_t vector_size>
  template<class Op>
  inline void
  DctDataProcess<Type, Alloc, vector_size>::transform_line_unary(Type * __restrict const out, const Type * __restrict const in, const size_t & line_length, Op op)
  {
    const SIMDUnrolling<Type, vector_size> simd4(4);
    const SIMDUnrolling<Type, vector_size> simd1(1);

    const Type * __restrict temp_in = in;
    Type * __restrict temp_out = out;
    const Type * __restrict const end_line_out = out + line_length;

    const size_t shift_to_align = simd1.get_shift_to_align(out);
    if (shift_to_align < line_length)
    {
      Type * __restrict const out_a = out + shift_to_align;
      const size_t line_length_a = line_length - shift_to_align;

      const Type * __restrict const end_line_out_u4 = out_a + simd4.get_unroll(line_length_a);
      const Type * __restrict const end_line_out_u1 = out_a + simd1.get_unroll(line_length_a);

      for (; temp_out < out_a; temp_out++, temp_in++)
      {
        *temp_out = op(*temp_in);
      }
      for (; temp_out < end_line_out_u4; temp_out += simd4.block, temp_in += simd4.block)
      {
        const vVvf & a0 = this->access_u.load(temp_in + 0 * simd4.shift);
        const vVvf & a1 = this->access_u.load(temp_in + 1 * simd4.shift);
        const vVvf & a2 = this->access_u.load(temp_in + 2 * simd4.shift);
        const vVvf & a3 = this->access_u.load(temp_in + 3 * simd4.shift);

        const vVvf c0 = op(a0);
        const vVvf c1 = op(a1);
        const vVvf c2 = op(a2);
        const vVvf c3 = op(a3);

        this->access_u.store(temp_out + 0 * simd4.shift, c0);
        this->access_u.store(temp_out + 1 * simd4.shift, c1);
        this->access_u.store(temp_out + 2 * simd4.shift, c2);
        this->access_u.store(temp_out + 3 * simd4.shift, c3);
      }
      for (; temp_out < end_line_out_u1; temp_out += simd1.block, temp_in += simd1.block)
      {
        const vVvf & a0 = this->access_u.load(temp_in);
        const vVvf c0 = op(a0);
        this->access_u.store(temp_out, c0);
      }
    }
    for (; temp_out < end_line_out; temp_out++, temp_in++)
    {
      *temp_out = op(*temp_in);
    }
  }

  template<typename Type, class Alloc, const size_t vector_size>
  template<class Op>
  inline void
  DctDataProcess<Type, Alloc, vector_size>::transform_line_binary(Type * __restrict const out, const Type * __restrict const in, const size_t & line_length, Op op)
  {
    const SIMDUnrolling<Type, vector_size> simd4(4);
    const SIMDUnrolling<Type, vector_size> simd1(1);

    const Type * __restrict temp_in = in;
    Type * __restrict temp_out = out;
    const Type * __restrict const end_line_out = out + line_length;

    const size_t shift_to_align = simd1.get_shift_to_align(out);
    if (shift_to_align < line_length)
    {
      Type * __restrict const out_a = out + shift_to_align;
      const size_t line_length_a = line_length - shift_to_align;

      const Type * __restrict const end_line_out_u4 = out_a + simd4.get_unroll(line_length_a);
      const Type * __restrict const end_line_out_u1 = out_a + simd1.get_unroll(line_length_a);

      for (; temp_out < out_a; temp_out++, temp_in++)
      {
        *temp_out = op(*temp_out, *temp_in);
      }
      for (; temp_out < end_line_out_u4; temp_out += simd4.block, temp_in += simd4.block)
      {
        const vVvf & a0 = this->access_u.load(temp_out + 0 * simd4.shift);
        const vVvf & a1 = this->access_u.load(temp_out + 1 * simd4.shift);
        const vVvf & a2 = this->access_u.load(temp_out + 2 * simd4.shift);
        const vVvf & a3 = this->access_u.load(temp_out + 3 * simd4.shift);

        const vVvf & b0 = this->access_u.load(temp_in + 0 * simd4.shift);
        const vVvf & b1 = this->access_u.load(temp_in + 1 * simd4.shift);
        const vVvf & b2 = this->access_u.load(temp_in + 2 * simd4.shift);
        const vVvf & b3 = this->access_u.load(temp_in + 3 * simd4.shift);

        const vVvf c0 = op(a0, b0);
        const vVvf c1 = op(a1, b1);
        const vVvf c2 = op(a2, b2);
        const vVvf c3 = op(a3, b3);

        this->access_u.store(temp_out + 0 * simd4.shift, c0);
        this->access_u.store(temp_out + 1 * simd4.shift, c1);
        this->access_u.store(temp_out + 2 * simd4.shift, c2);
        this->access_u.store(temp_out + 3 * simd4.shift, c3);
      }
      for (; temp_out < end_line_out_u1; temp_out += simd1.block, temp_in += simd1.block)
      {
        const vVvf & a0 = this->access_u.load(temp_out);
        const vVvf & b0 = this->access_u.load(temp_in);
        const vVvf c0 = op(a0, b0);
        this->access_u.store(temp_out, c0);
      }
    }
    for (; temp_out < end_line_out; temp_out++, temp_in++)
    {
      *temp_out = op(*temp_out, *temp_in);
    }
  }

  template<typename Type, class Alloc, const size_t vector_size>
  template<class Op>
  inline void
  DctDataProcess<Type, Alloc, vector_size>::transform_line_binary(Type * __restrict const out, const Type * __restrict const in1, const Type * __restrict const in2, const size_t & line_length, Op op)
  {
    const SIMDUnrolling<Type, vector_size> simd4(4);
    const SIMDUnrolling<Type, vector_size> simd1(1);

    const Type * __restrict temp_in1 = in1;
    const Type * __restrict temp_in2 = in2;
    Type * __restrict temp_out = out;
    const Type * __restrict const end_line_out = out + line_length;

    const size_t shift_to_align = simd1.get_shift_to_align(out);
    if (shift_to_align < line_length)
    {
      Type * __restrict const out_a = out + shift_to_align;
      const size_t aligned_line_length = line_length - shift_to_align;

      const Type * __restrict const end_line_out_u4 = out_a + simd4.get_unroll(aligned_line_length);
      const Type * __restrict const end_line_out_u1 = out_a + simd1.get_unroll(aligned_line_length);

      for (; temp_out < out_a; temp_out++, temp_in1++, temp_in2++)
      {
        *temp_out = op(*temp_in1, *temp_in2);
      }
      for (; temp_out < end_line_out_u4;
          temp_out += simd4.block, temp_in1 += simd4.block, temp_in2 += simd4.block)
      {
        const vVvf & a0 = this->access_u.load(temp_in1 + 0 * simd4.shift);
        const vVvf & a1 = this->access_u.load(temp_in1 + 1 * simd4.shift);
        const vVvf & a2 = this->access_u.load(temp_in1 + 2 * simd4.shift);
        const vVvf & a3 = this->access_u.load(temp_in1 + 3 * simd4.shift);

        const vVvf & b0 = this->access_u.load(temp_in2 + 0 * simd4.shift);
        const vVvf & b1 = this->access_u.load(temp_in2 + 1 * simd4.shift);
        const vVvf & b2 = this->access_u.load(temp_in2 + 2 * simd4.shift);
        const vVvf & b3 = this->access_u.load(temp_in2 + 3 * simd4.shift);

        const vVvf c0 = op(a0, b0);
        const vVvf c1 = op(a1, b1);
        const vVvf c2 = op(a2, b2);
        const vVvf c3 = op(a3, b3);

        this->access_u.store(temp_out + 0 * simd4.shift, c0);
        this->access_u.store(temp_out + 1 * simd4.shift, c1);
        this->access_u.store(temp_out + 2 * simd4.shift, c2);
        this->access_u.store(temp_out + 3 * simd4.shift, c3);
      }
      for (; temp_out < end_line_out_u1;
          temp_out += simd1.block, temp_in1 += simd1.block, temp_in2 += simd1.block)
      {
        const vVvf & a0 = this->access_u.load(temp_in1);
        const vVvf & b0 = this->access_u.load(temp_in2);
        const vVvf c0 = op(a0, b0);
        this->access_u.store(temp_out, c0);
      }
    }
    for (; temp_out < end_line_out; temp_out++, temp_in1++, temp_in2++)
    {
      *temp_out = op(*temp_in1, *temp_in2);
    }
  }

  template<typename Type, class Alloc, const size_t vector_size>
  inline void
  DctDataProcess<Type, Alloc, vector_size>::copy_line(Type * __restrict const out, const Type * __restrict const in, const size_t & line_length)
  {
    OpUnaryAssign<Type> op;

    this->transform_line_unary(out, in, line_length, op);
  }

  template<typename Type, class Alloc, const size_t vector_size>
  void
  DctDataProcess<Type, Alloc, vector_size>::process_tasks(std::vector< std::function<void()> > & tasks)
  {
    work_queue.push(tasks);

    std::vector<std::thread> threads;
    threads.reserve(num_threads);

    for (size_t t = 0; t < num_threads; t++) {
      threads.emplace_back(std::thread(&DctDataProcess<Type, Alloc, vector_size>::worker_thread, this));
    }

    JoinThreads joiner(threads);
  }

  template<typename Type, class Alloc, const size_t vector_size>
  std::vector<ChunkSpecs>
  DctDataProcess<Type, Alloc, vector_size>::partition_grid_volumes(const size_t tot_vols, const size_t tot_elems_per_vol)
  {
    std::vector<ChunkSpecs> cs;

    const size_t remaining_vols = tot_vols % this->num_threads;
    const size_t complete_vols = tot_vols - remaining_vols;

    cs.reserve(complete_vols);

    for (size_t num_vol = 0; num_vol < complete_vols; num_vol++)
    {
      cs.emplace_back(ChunkSpecs({num_vol, size_t(0), tot_elems_per_vol}));
    }

    if (remaining_vols > 0) {
      const size_t tot_chunks_per_vol = this->num_threads / remaining_vols + ((this->num_threads % remaining_vols) > 0);
      const size_t elems_per_chunk = tot_elems_per_vol / tot_chunks_per_vol + ((tot_elems_per_vol % tot_chunks_per_vol) > 0);
      const size_t chunk_size = this->get_roundup_to_fit_moves(elems_per_chunk);

      cs.reserve(complete_vols + remaining_vols * tot_chunks_per_vol);

      for (size_t num_vol = complete_vols; num_vol < tot_vols; num_vol++)
      {
        size_t chunk_start = 0;

        for (size_t num_chunk = 0; num_chunk < tot_chunks_per_vol; num_chunk++)
        {
          cs.emplace_back(ChunkSpecs({num_vol, chunk_start, chunk_size}));

          chunk_start += chunk_size;
        }
      }
    }

    return cs;
  }

  template<typename Type, class Alloc, const size_t vector_size>
  std::vector<ChunkSpecs>
  DctDataProcess<Type, Alloc, vector_size>::partition_proj_vector(const size_t tot_vols, const std::vector<size_t> & tot_elems_each_vol)
  {
    // TODO: Too simplistic at the moment, and it can lead to too many jobs
    std::vector<ChunkSpecs> cs;

    const std::vector<size_t>::const_iterator min_el = std::min_element(tot_elems_each_vol.begin(), tot_elems_each_vol.end());
    const size_t min_elements = *min_el;

    std::vector<size_t> num_chunks;
    num_chunks.resize(tot_elems_each_vol.size());
    for (size_t ind = 0; ind < tot_elems_each_vol.size(); ind++) {
      num_chunks[ind] = tot_elems_each_vol[ind] / min_elements + ((tot_elems_each_vol[ind] % min_elements) > 0);
    }
    const size_t tot_jobs = std::accumulate(num_chunks.begin(), num_chunks.end(), size_t(1), std::plus<size_t>());
    cs.reserve(tot_jobs);

    for (size_t num_vol = 0; num_vol < tot_vols; num_vol++)
    {
      const size_t elems_per_chunk = tot_elems_each_vol[num_vol] / num_chunks[num_vol] + ((tot_elems_each_vol[num_vol] % num_chunks[num_vol]) > 0);
      const size_t chunk_size = this->get_roundup_to_fit_moves(elems_per_chunk);

      size_t chunk_start = 0;

      for (size_t num_chunk = 0; num_chunk < num_chunks[num_vol]; num_chunk++)
      {
        cs.emplace_back(ChunkSpecs({num_vol, chunk_start, chunk_size}));

        chunk_start += chunk_size;
      }
    }

    return cs;
  }

  //////////////////////////////////////////////////////////////////////////////
  // DctDataInitializer

  template<typename Type, class Alloc, const size_t vector_size>
  inline void
  DctDataInitializer<Type, Alloc, vector_size>::initialize_line(Type * const __restrict data, const size_t & line_length, const Type & val)
  {
    const SIMDUnrolling<Type, vector_size> simd4(4);
    const SIMDUnrolling<Type, vector_size> simd1(1);

    const SIMDRegister<Type, vector_size> vec_val = Coeff<Type, vector_size>::get(val);

    Type * __restrict const end_line_unroll4 = data + simd4.get_unroll(line_length);
    Type * __restrict const end_line_unroll1 = data + simd1.get_unroll(line_length);
    Type * __restrict const end_line = data + line_length;

    Type * __restrict temp_data = data;

    for (; temp_data < end_line_unroll4; temp_data += simd4.block)
    {
      this->access_s.store(temp_data + 0 * simd4.shift, vec_val);
      this->access_s.store(temp_data + 1 * simd4.shift, vec_val);
      this->access_s.store(temp_data + 2 * simd4.shift, vec_val);
      this->access_s.store(temp_data + 3 * simd4.shift, vec_val);
    }
    for (; temp_data < end_line_unroll1; temp_data += simd1.block)
    {
      this->access_s.store(temp_data, vec_val);
    }
    for (; temp_data < end_line; temp_data++)
    {
      *temp_data = val;
    }
  }

  template<typename Type, class Alloc, const size_t vector_size>
  void
  DctDataInitializer<Type, Alloc, vector_size>::initialize_chunk(const DctData<Type, Alloc> & vol, const size_t & start_pos, const size_t & chunk_size)
  {
    const size_t tot_elems = vol.get_tot_elements();

    const size_t safe_start_pos = std::min(std::max(start_pos, size_t(0)), tot_elems);
    const size_t safe_chunk_size = std::min(tot_elems - safe_start_pos, chunk_size);

    Type * __restrict const base_data = vol.get_data() + safe_start_pos;
    this->initialize_line(base_data, safe_chunk_size, val);
  }

  template<typename Type, class Alloc, const size_t vector_size>
  void
  DctDataInitializer<Type, Alloc, vector_size>::initialize(DctVolumeGrid<Type, Alloc> & c)
  {
    const size_t tot_vols = c.size();
    const size_t tot_elems = c[0]->get_tot_elements();
    std::vector<ChunkSpecs> chunks = this->partition_grid_volumes(tot_vols, tot_elems);

    std::vector< std::function<void()> > funcs;
    funcs.reserve(chunks.size());

    for (const ChunkSpecs & cs : chunks) {
      DctData<Type, Alloc> & out_vol = c[cs.data_ind];

      // Change called function
      funcs.emplace_back(std::function<void()>(
          std::bind(&DctDataInitializer<Type, Alloc, vector_size>::initialize_chunk, this, out_vol, cs.chunk_start, cs.chunk_size)));
    }

    this->process_tasks(funcs);
  }

  template<typename Type,class Alloc, const size_t vector_size>
  void
  DctDataInitializer<Type, Alloc, vector_size>::initialize(DctProjectionVector<Type, Alloc> & c)
  {
    const size_t tot_vols = c.size();
    const std::vector<size_t> tot_elems_each_vol = c.get_tot_elements_each_proj();
    std::vector<ChunkSpecs> chunks = this->partition_proj_vector(tot_vols, tot_elems_each_vol);

    std::vector< std::function<void()> > funcs;
    funcs.reserve(chunks.size());

    for (const ChunkSpecs & cs : chunks) {
      DctData<Type, Alloc> & out_vol = c[cs.data_ind];

      // Change called function
      funcs.emplace_back(std::function<void()>(
          std::bind(&DctDataInitializer<Type, Alloc, vector_size>::initialize_chunk, this, out_vol, cs.chunk_start, cs.chunk_size)));
    }

    this->process_tasks(funcs);
  }

  //////////////////////////////////////////////////////////////////////////////
  // DctProjectionTransform

  template<typename Type, class Alloc, const size_t vector_size>
  void
  DctProjectionTransform<Type, Alloc, vector_size>::set_blobs(
      const DctProjectionVector<Type, Alloc> & new_blobs)
  {
    if (new_blobs.empty()) {
      THROW(WrongArgumentException, DctProjectionTransform, "No blobs were passed!");
    }
    this->blobs = new_blobs;

    const auto & dims = blobs[0].get_dims();
    this->blobs_size_u = dims[0];
    this->blobs_size_v = dims[2];
  }

  template<typename Type, class Alloc, const size_t vector_size>
  void
  DctProjectionTransform<Type, Alloc, vector_size>::set_sinos(
      const DctProjectionVector<Type, Alloc> & new_sinos)
  {
    if (new_sinos.empty()) {
      THROW(WrongArgumentException, DctProjectionTransform, "No sinograms were passed!");
    }
    this->sinos = new_sinos;

    const auto & dims = sinos[0].get_dims();
    this->sinos_size_u = dims[0];
    this->sinos_size_v = dims[2];
  }

  template<typename Type, class Alloc, const size_t vector_size>
  void
  DctProjectionTransform<Type, Alloc, vector_size>::set_transforms(
      const std::vector<DctProjectionTransformCoefficients<Type> > & new_transforms)
  {
    this->transforms_to_sinos = new_transforms;
    this->transforms_to_blobs.clear();

    if (this->transforms_to_sinos.size() > 0) {
      this->use_shifts = transforms_to_sinos[0].use_shifts;
    }
  }

  template<typename Type, class Alloc, const size_t vector_size>
  void
  DctProjectionTransform<Type, Alloc, vector_size>::set_blobs(
      const DctProjectionVector<Type, Alloc> && new_blobs)
  {
    if (new_blobs.empty()) {
      THROW(WrongArgumentException, DctProjectionTransform, "No blobs were passed!");
    }
    this->blobs = std::move(new_blobs);

    const auto & dims = blobs[0].get_dims();
    this->blobs_size_u = dims[0];
    this->blobs_size_v = dims[2];
  }

  template<typename Type, class Alloc, const size_t vector_size>
  void
  DctProjectionTransform<Type, Alloc, vector_size>::set_sinos(
      const DctProjectionVector<Type, Alloc> && new_sinos)
  {
    if (new_sinos.empty()) {
      THROW(WrongArgumentException, DctProjectionTransform, "No sinograms were passed!");
    }
    this->sinos = std::move(new_sinos);

    const auto & dims = sinos[0].get_dims();
    this->sinos_size_u = dims[0];
    this->sinos_size_v = dims[2];
  }

  template<typename Type, class Alloc, const size_t vector_size>
  void
  DctProjectionTransform<Type, Alloc, vector_size>::set_transforms(
      const std::vector<DctProjectionTransformCoefficients<Type> > && new_transforms)
  {
    this->transforms_to_sinos = std::move(new_transforms);
    this->transforms_to_blobs.clear();

    if (this->transforms_to_sinos.size() > 0) {
      this->use_shifts = transforms_to_sinos[0].use_shifts;
    }
  }

  template<typename Type, class Alloc, const size_t vector_size>
  void
  DctProjectionTransform<Type, Alloc, vector_size>::check_consistency() const
  {
    sinos.check_consistency();
    blobs.check_consistency();

    const size_t tot_sinos = sinos.size();
    const size_t tot_coeffs = transforms_to_sinos.size();

    if (tot_sinos != tot_coeffs) {
      THROW(WrongArgumentException, DctProjectionTransform, "Number of sinograms and projection coefficients structures should be the same!");
    }

    for (size_t num_sino = 0; num_sino < tot_sinos; ++num_sino) {
      const std::vector<size_t> & sino_offsets = transforms_to_sinos[num_sino].dest_slice;
      const std::vector<size_t> & blob_offsets = transforms_to_sinos[num_sino].src_vol;
      const std::vector<size_t> & proj_offsets = transforms_to_sinos[num_sino].src_slice;

      const std::vector<size_t> & shifts_u = transforms_to_sinos[num_sino].shift_u;
      const std::vector<size_t> & shifts_v = transforms_to_sinos[num_sino].shift_v;

      const size_t * const dims_sino = sinos[num_sino].get_dims();

      for (size_t m = 0; m < transforms_to_sinos[num_sino].num_projections; m++) {
        const size_t & p = proj_offsets[m];
        const size_t & b = blob_offsets[m];
        const size_t & s = sino_offsets[m];

        if (b >= blobs.size()) {
          THROW(WrongArgumentException, DctProjectionTransform, error_blob_index_out_of_hlim);
        }

        if (b < 0) {
          THROW(WrongArgumentException, DctProjectionTransform, error_blob_index_out_of_llim);
        }

        const size_t * const dims_blob = this->blobs[b].get_dims();
        if (transforms_to_sinos[num_sino].use_shifts != this->use_shifts) {
          THROW(WrongArgumentException, DctProjectionTransform, error_incons_use_shifts);
        }

        if (!transforms_to_sinos[num_sino].use_shifts) {
          // Sino and Blob have the same size along 1st and 3rd dimensions in initial version

          if (dims_blob[0] != dims_sino[0]) {
            THROW(WrongArgumentException, DctProjectionTransform, error_wrong_dim_0);
          }
          if (dims_blob[2] != dims_sino[2]) {
            THROW(WrongArgumentException, DctProjectionTransform, error_wrong_dim_2);
          }
        } else {
          // In this case, all the sinos are supposed to fit into the blobs
          const size_t & su = shifts_u[m];
          const size_t & sv = shifts_v[m];

          if (dims_blob[0] < (dims_sino[0] + su)) {
            printf("Num sino: %lu, num blob: %lu, blobs UV size [%lu, %lu], sino UV size [%lu, %lu], sino shift UV [%lu, %lu]\n",
                num_sino, b, dims_blob[0], dims_blob[2], dims_sino[0], dims_sino[2], su, sv);
            THROW(WrongArgumentException, DctProjectionTransform, error_wrong_dim_u_sh);
          }
          if (dims_blob[2] < (dims_sino[2] + sv)) {
            printf("Num sino: %lu, num blob: %lu, blobs UV size [%lu, %lu], sino UV size [%lu, %lu], sino shift UV [%lu, %lu]\n",
                num_sino, b, dims_blob[0], dims_blob[2], dims_sino[0], dims_sino[2], su, sv);
            THROW(WrongArgumentException, DctProjectionTransform, error_wrong_dim_v_sh);
          }
        }

        if (s >= dims_sino[1]) {
          THROW(WrongArgumentException, DctProjectionTransform, error_sino_slice_out_of_hlim);
        }
        if (p >= dims_blob[1]) {
          THROW(WrongArgumentException, DctProjectionTransform, error_blob_slice_out_of_hlim);
        }
        if (s < 0) {
          THROW(WrongArgumentException, DctProjectionTransform, error_sino_slice_out_of_llim);
        }
        if (p < 0) {
          THROW(WrongArgumentException, DctProjectionTransform, error_blob_slice_out_of_llim);
        }
      }
    }
  }

  template<typename Type, class Alloc, const size_t vector_size>
  void
  DctProjectionTransform<Type, Alloc, vector_size>::initialize_transforms_to_blobs()
  {
    const size_t tot_blobs = blobs.size();
    const size_t tot_sinos = sinos.size();

    transforms_to_blobs.resize(tot_blobs);
    for (size_t b = 0; b < tot_blobs; b++)
    {
      transforms_to_blobs[b].reserve(2 * tot_sinos);
      transforms_to_blobs[b].use_shifts = this->use_shifts;
    }

    for (size_t num_sino = 0; num_sino < tot_sinos; ++num_sino)
    {
      const auto & metad_s = transforms_to_sinos[num_sino];

      for (size_t m = 0; m < metad_s.num_projections; m++)
      {
        const auto & s = metad_s.dest_slice[m];
        const auto & b = metad_s.src_vol[m];
        const auto & p = metad_s.src_slice[m];
        const auto & c = metad_s.coeff[m];

        // (d_slice, s_vol, s_slice, coefficient)
        if (metad_s.use_shifts) {
          const auto & su = metad_s.shift_u[m];
          const auto & sv = metad_s.shift_v[m];
          transforms_to_blobs[b].emplace_back(p, num_sino, s, c, su, sv);
        } else {
          transforms_to_blobs[b].emplace_back(p, num_sino, s, c);
        }
      }
    }
  }

  template<typename Type, class Alloc, const size_t vector_size>
  void
  DctProjectionTransform<Type, Alloc, vector_size>::produce_sinograms()
  {
    const size_t tot_sinos = sinos.size();

    this->submit_transforms(&DctProjectionTransform<Type, Alloc, vector_size>::transform_blobs2sino, tot_sinos);
  }

  template<typename Type, class Alloc, const size_t vector_size>
  void
  DctProjectionTransform<Type, Alloc, vector_size>::produce_blobs()
  {
    if (transforms_to_blobs.empty()) {
      this->initialize_transforms_to_blobs();
    }

    const size_t tot_blobs = blobs.size();

    this->submit_transforms(&DctProjectionTransform<Type, Alloc, vector_size>::transform_sinos2blob, tot_blobs);
  }

  template<typename Type, class Alloc, const size_t vector_size>
  template<typename FunctionType>
  void
  DctProjectionTransform<Type, Alloc, vector_size>::submit_transforms(FunctionType func, const size_t & tot_vols)
  {
    const size_t remaining_vols = tot_vols % this->num_threads;
    const size_t complete_vols = tot_vols - remaining_vols;

    std::vector< std::function<void()> > funcs;
    funcs.reserve(complete_vols);

    for (size_t num_vol = 0; num_vol < complete_vols; num_vol++)
    {
      funcs.emplace_back(std::function<void()>(std::bind(func, this, num_vol, size_t(0), sinos_size_v)));
    }

    if (remaining_vols > 0) {
      if (this->use_shifts && func == &DctProjectionTransform<Type, Alloc, vector_size>::transform_sinos2blob) {
        for (size_t num_vol = complete_vols; num_vol < tot_vols; num_vol++)
        {
          funcs.emplace_back(std::function<void()>(std::bind(func, this, num_vol, size_t(0), sinos_size_v)));
        }
      } else {
        const size_t tot_theo_slabs = this->num_threads / remaining_vols + ((this->num_threads % remaining_vols) > 0);
        const size_t tot_slabs = std::min(tot_theo_slabs, std::max(sinos_size_v / 2, size_t(1)));
        const size_t lines_per_slab = sinos_size_v / tot_slabs + ((sinos_size_v % tot_slabs) > 0);

        funcs.reserve(complete_vols + remaining_vols * tot_slabs);

        for (size_t num_vol = complete_vols; num_vol < tot_vols; num_vol++)
        {
          size_t slab_start = 0;
          for (size_t num_slab = 0; num_slab < tot_slabs; ++num_slab) {
            const size_t slab_end = std::min(slab_start + lines_per_slab, sinos_size_v);

            funcs.emplace_back(std::function<void()>(std::bind(func, this, num_vol, slab_start, slab_end)));

            slab_start = slab_end;
          }
        }
      }
    }

    this->process_tasks(funcs);
  }

  template<typename Type, class Alloc, const size_t vector_size>
  void
  DctProjectionTransform<Type, Alloc, vector_size>::transform_blobs2sino(const size_t & sino_num, const size_t & min_v, const size_t & max_v)
  {
    Type ALIGNED(vector_size) temp_line[sinos_size_u];

    const size_t safe_min_v = std::max(min_v, size_t(0));
    const size_t safe_max_v = std::min(max_v, sinos_size_v);

    const DctProjectionTransformCoefficients<Type> & metad = transforms_to_sinos[sino_num];

    for (size_t v = safe_min_v; v < safe_max_v; v++)
    {
      Type * __restrict const sino_base = sinos[sino_num].get_data() + v * sinos[sino_num].get_skip();

      this->initialize_line(temp_line, sinos_size_u);

      for (size_t m = 0; m < metad.num_projections; m++)
      {
        const size_t & p = metad.src_slice[m];
        const size_t & b = metad.src_vol[m];
        const size_t & s = metad.dest_slice[m];

        const size_t & su = metad.shift_u[m];
        const size_t & sv = metad.shift_v[m];
        const size_t b_shift = this->use_shifts ? (su + sv * blobs[b].get_skip()) : 0;

        OpBinaryPlusScaled<Type> op(metad.coeff[m]);

        const Type * __restrict const blob_data_line = blobs[b].get_data() + blobs_size_u * p + v * blobs[b].get_skip() + b_shift;
        this->transform_line_binary(temp_line, blob_data_line, sinos_size_u, op);

        const bool is_last = (m+1) == metad.num_projections;
        if (is_last || metad.dest_slice[m+1] != s) {
          Type * const __restrict sino_data_line = sino_base + sinos_size_u * s;
          this->copy_line(sino_data_line, temp_line, sinos_size_u);

          if (!is_last) {
            this->initialize_line(temp_line, sinos_size_u);
          }
        }
      }
    }
  }

  template<typename Type, class Alloc, const size_t vector_size>
  void
  DctProjectionTransform<Type, Alloc, vector_size>::transform_sinos2blob(const size_t & blob_num, const size_t & min_v, const size_t & max_v)
  {
    const size_t safe_min_v = std::max(min_v, size_t(0));
    const size_t safe_max_v = std::min(max_v, sinos_size_v);

    const DctProjectionTransformCoefficients<Type> & metad = transforms_to_blobs[blob_num];

    for (size_t v = safe_min_v; v < safe_max_v; v++)
    {
      Type * const base_blob = blobs[blob_num].get_data() + v * blobs[blob_num].get_skip();

      for (size_t m = 0; m < metad.num_projections; m++)
      {
        const size_t & s = metad.src_slice[m];
        const size_t & sino_num = metad.src_vol[m];
        const size_t & p = metad.dest_slice[m];

        const size_t & su = metad.shift_u[m];
        const size_t & sv = metad.shift_v[m];
        const size_t b_shift = this->use_shifts ? (su + sv * blobs[blob_num].get_skip()) : 0;

        OpBinaryPlusScaled<Type> op(metad.coeff[m]);

        const Type * __restrict const sino_data_line = sinos[sino_num].get_data() + v * sinos[sino_num].get_skip() + sinos_size_u * s;
        Type * const blob_data_line = base_blob + blobs_size_u * p + b_shift;

        this->transform_line_binary(blob_data_line, sino_data_line, sinos_size_u, op);
      }
    }
  }

  //////////////////////////////////////////////////////////////////////////////
  // DctVolumeGridTransform

  template<typename Type, class Alloc, const size_t vector_size>
  void
  DctVolumeGridTransform<Type, Alloc, vector_size>::set_vols(DctVolumeGrid<Type, Alloc> & new_vols)
  {
    if (new_vols.empty()) {
      THROW(WrongArgumentException, DctVolumeGridTransform, "DctVolumeGridTransform: an empty DctVolumeGrid was passed!");
    }
    this->vols = new_vols;
  }

  template<typename Type, class Alloc, const size_t vector_size>
  template<class Op>
  void
  DctVolumeGridTransform<Type, Alloc, vector_size>::accumulate_sub_block(DctData<Type, Alloc> & out, const size_t & chunk_start, const size_t & chunk_size, const std::vector<size_t> & vols_list, Op op)
  {
    const size_t tot_elems = out.get_tot_elements();

    const size_t safe_chunk_start = std::min(std::max(chunk_start, size_t(0)), tot_elems);
    const size_t safe_chunk_size = std::min(tot_elems - safe_chunk_start, chunk_size);

    Type * __restrict const data_out = out.get_data() + safe_chunk_start;

    if (vols_list.size() == 1)
    {
      const Type * __restrict const data_in0 = this->vols[0].get_data() + safe_chunk_start;
      this->copy_line(data_out, data_in0, safe_chunk_size);
    }
    else
    {
      const auto & vol_0 = this->vols[vols_list[0]];
      const auto & vol_1 = this->vols[vols_list[1]];
      const Type * __restrict const data_in0 = vol_0.get_data() + safe_chunk_start;
      const Type * __restrict const data_in1 = vol_1.get_data() + safe_chunk_start;

      this->transform_line_binary(data_out, data_in0, data_in1, safe_chunk_size, op);

      for (size_t pos = 2; pos < vols_list.size(); pos++) {
        const size_t & vol_id = vols_list[pos];
        const Type * __restrict const data_in = this->vols[vol_id].get_data() + safe_chunk_start;

        this->transform_line_binary(data_out, data_in, safe_chunk_size, op);
      }
    }
  }

  template<typename Type, class Alloc, const size_t vector_size>
  template<class Op>
  void
  DctVolumeGridTransform<Type, Alloc, vector_size>::op_element_wise_sub_block_unary_inplace(Op op, DctData<Type, Alloc> & out, const ChunkSpecs & cs)
  {
    const size_t tot_elems = out.get_tot_elements();

    const size_t safe_chunk_start = std::min(std::max(cs.chunk_start, size_t(0)), tot_elems);
    const size_t safe_chunk_size = std::min(tot_elems - safe_chunk_start, cs.chunk_size);

    Type * __restrict const data_out = out.get_data() + safe_chunk_start;

    this->transform_line_unary(data_out, safe_chunk_size, op);
  }

  template<typename Type, class Alloc, const size_t vector_size>
  template<class Op>
  void
  DctVolumeGridTransform<Type, Alloc, vector_size>::op_element_wise_sub_block_unary_copy(Op op, DctData<Type, Alloc> & out, const DctData<Type, Alloc> & in, const ChunkSpecs & cs)
  {
    const size_t tot_elems = out.get_tot_elements();

    const size_t safe_chunk_start = std::min(std::max(cs.chunk_start, size_t(0)), tot_elems);
    const size_t safe_chunk_size = std::min(tot_elems - safe_chunk_start, cs.chunk_size);

    Type * __restrict const data_out = out.get_data() + safe_chunk_start;
    const Type * __restrict const data_in = in.get_data() + safe_chunk_start;

    this->transform_line_unary(data_out, data_in, safe_chunk_size, op);
  }

  template<typename Type, class Alloc, const size_t vector_size>
  template<class Op>
  void
  DctVolumeGridTransform<Type, Alloc, vector_size>::op_element_wise_sub_block_binary_inplace(Op op, DctData<Type, Alloc> & out, const DctData<Type, Alloc> & in, const ChunkSpecs & cs)
  {
    const size_t tot_elems = out.get_tot_elements();

    const size_t safe_chunk_start = std::min(std::max(cs.chunk_start, size_t(0)), tot_elems);
    const size_t safe_chunk_size = std::min(tot_elems - safe_chunk_start, cs.chunk_size);

    Type * __restrict const data_out = out.get_data() + safe_chunk_start;
    const Type * __restrict const data_in = in.get_data() + safe_chunk_start;

    this->transform_line_binary(data_out, data_in, safe_chunk_size, op);
  }

  template<typename Type, class Alloc, const size_t vector_size>
  template<class Op>
  void
  DctVolumeGridTransform<Type, Alloc, vector_size>::op_element_wise_sub_block_binary_copy(Op op, DctData<Type, Alloc> & out, const DctData<Type, Alloc> & in1, const DctData<Type, Alloc> & in2, const ChunkSpecs & cs)
  {
    const size_t tot_elems = out.get_tot_elements();

    const size_t safe_chunk_start = std::min(std::max(cs.chunk_start, size_t(0)), tot_elems);
    const size_t safe_chunk_size = std::min(tot_elems - safe_chunk_start, cs.chunk_size);

    Type * __restrict const data_out = out.get_data() + safe_chunk_start;
    const Type * __restrict const data_in1 = in1.get_data() + safe_chunk_start;
    const Type * __restrict const data_in2 = in2.get_data() + safe_chunk_start;

    this->transform_line_binary(data_out, data_in1, data_in2, safe_chunk_size, op);
  }

  template<typename Type, class Alloc, const size_t vector_size>
  template<class Op>
  DctVolumeGrid<Type, Alloc>
  DctVolumeGridTransform<Type, Alloc, vector_size>::accumulate(const int32_t & dim, Op op)
  {
    DctVolumeGrid<Type, Alloc> out = {DctVolumeGrid<Type, Alloc>::ReshapePolicy::NoMatchClear};

    std::vector<size_t> out_dims;
    if (dim < 0) {
      out_dims.push_back(1);
    } else {
      out_dims = vols.get_grid_dims();

      if (size_t(dim) >= out_dims.size()) {
        THROW(WrongArgumentException, DctVolumeGridTransform, "DctVolumeGridTransform: accumulation dimension outside the grid dimensions");
      }

      out_dims[dim] = 1;
    }
    out.set_dims(out_dims);
    out.create_vols(this->vols[0].get_dims());
    out.allocate();

    const size_t tot_elems_per_vol = this->vols[0].get_tot_elements();
    const size_t tot_out_vols = out.get_tot_vols();
    const size_t tot_in_vols = vols.get_tot_vols();

    std::vector<ChunkSpecs> chunks = this->partition_grid_volumes(tot_out_vols, tot_elems_per_vol);

    std::vector< std::function<void()> > funcs;
    funcs.reserve(chunks.size());

    size_t n = {0};
    if (dim < 0) {
      std::vector<size_t> in_vols_inds;
      in_vols_inds.resize(tot_in_vols);
      std::generate(in_vols_inds.begin(), in_vols_inds.end(), [&n]{ return n++; });

      for (const ChunkSpecs & cs : chunks) {
        funcs.emplace_back(std::function<void()>(
            std::bind(&DctVolumeGridTransform<Type, Alloc, vector_size>::accumulate_sub_block<Op>, this, out[0], cs.chunk_start, cs.chunk_size, in_vols_inds, op)));
      }
    } else {
      std::vector<size_t> vols_sub_inds;
      vols_sub_inds.resize(vols.get_grid_dims()[dim]);
      std::generate(vols_sub_inds.begin(), vols_sub_inds.end(), [&n]{ return n++; });

      for (const ChunkSpecs & cs : chunks) {
        DctData<Type, Alloc> & out_vol = out[cs.data_ind];
        std::vector<size_t> subs = out.ind_to_sub(cs.data_ind);
        std::vector<size_t> in_vols_inds = this->vols.subs_to_inds(subs, dim, vols_sub_inds);

        funcs.emplace_back(std::function<void()>(
            std::bind(&DctVolumeGridTransform<Type, Alloc, vector_size>::accumulate_sub_block<Op>, this, out_vol, cs.chunk_start, cs.chunk_size, in_vols_inds, op)));
      }
    }

    this->process_tasks(funcs);

    return out;
  }

  template<typename Type, class Alloc, const size_t vector_size>
  template<class Op>
  void
  DctVolumeGridTransform<Type, Alloc, vector_size>::volume_wise_unary(Op op)
  {
    const size_t tot_elems_per_vol = this->vols[0].get_tot_elements();
    const size_t tot_vols = vols.get_tot_vols();

    std::vector<ChunkSpecs> && chunks = this->partition_grid_volumes(tot_vols, tot_elems_per_vol);

    std::vector< std::function<void()> > funcs;
    funcs.reserve(chunks.size());

    for (const ChunkSpecs & cs : chunks) {
      DctData<Type, Alloc> & out_vol = vols[cs.data_ind];

      // Change called function
      funcs.emplace_back(std::function<void()>(
          std::bind(&DctVolumeGridTransform<Type, Alloc, vector_size>::op_element_wise_sub_block_unary_inplace<Op>, this, op, out_vol, cs)));
    }

    this->process_tasks(funcs);
  }

  template<typename Type, class Alloc, const size_t vector_size>
  template<class Op>
  void
  DctVolumeGridTransform<Type, Alloc, vector_size>::volume_wise_unary(Op op, const DctVolumeGrid<Type, Alloc> & vols_in)
  {
    vols.set_dims(vols_in.get_grid_dims());
    vols.create_vols(vols_in[0].get_dims());
    vols.allocate();

    const size_t tot_elems_per_vol = this->vols[0].get_tot_elements();
    const size_t tot_vols = vols.get_tot_vols();

    std::vector<ChunkSpecs> chunks = this->partition_grid_volumes(tot_vols, tot_elems_per_vol);

    std::vector< std::function<void()> > funcs;
    funcs.reserve(chunks.size());

    for (const ChunkSpecs & cs : chunks) {
      DctData<Type, Alloc> & out_vol = vols[cs.data_ind];
      const DctData<Type, Alloc> & in_vol = vols_in[cs.data_ind];

      // Change called function
      funcs.emplace_back(std::function<void()>(
          std::bind(&DctVolumeGridTransform<Type, Alloc, vector_size>::op_element_wise_sub_block_unary_copy<Op>, this, op, out_vol, in_vol, cs)));
    }

    this->process_tasks(funcs);
  }

  template<typename Type, class Alloc, const size_t vector_size>
  template<class Op>
  void
  DctVolumeGridTransform<Type, Alloc, vector_size>::volume_wise_binary(Op op, const DctVolumeGrid<Type, Alloc> & vols_in)
  {
    vols.check_elementwise_compatibility(vols_in);

    const size_t tot_elems_per_vol = this->vols[0].get_tot_elements();
    const size_t tot_vols = vols.get_tot_vols();

    std::vector<ChunkSpecs> chunks = this->partition_grid_volumes(tot_vols, tot_elems_per_vol);

    std::vector< std::function<void()> > funcs;
    funcs.reserve(chunks.size());

    for (const ChunkSpecs & cs : chunks) {
      DctData<Type, Alloc> & out_vol = vols[cs.data_ind];
      const DctData<Type, Alloc> & in_vol = vols_in[cs.data_ind];

      // Change called function
      funcs.emplace_back(std::function<void()>(
          std::bind(&DctVolumeGridTransform<Type, Alloc, vector_size>::op_element_wise_sub_block_binary_inplace<Op>, this, op, out_vol, in_vol, cs)));
    }

    this->process_tasks(funcs);
  }

  template<typename Type, class Alloc, const size_t vector_size>
  template<class Op>
  void
  DctVolumeGridTransform<Type, Alloc, vector_size>::volume_wise_binary(Op op, const DctVolumeGrid<Type, Alloc> & vols_in1, const DctVolumeGrid<Type, Alloc> & vols_in2)
  {
    vols.set_dims(vols_in1.get_grid_dims());
    vols.create_vols(vols_in1[0].get_dims());
    vols.allocate();

    vols.check_elementwise_compatibility(vols_in1);
    vols.check_elementwise_compatibility(vols_in2);

    const size_t tot_elems_per_vol = this->vols[0].get_tot_elements();
    const size_t tot_vols = vols.get_tot_vols();

    std::vector<ChunkSpecs> chunks = this->partition_grid_volumes(tot_vols, tot_elems_per_vol);

    std::vector< std::function<void()> > funcs;
    funcs.reserve(chunks.size());

    for (const ChunkSpecs & cs : chunks) {
      DctData<Type, Alloc> & out_vol = vols[cs.data_ind];
      const DctData<Type, Alloc> & in_vol_1 = vols_in1[cs.data_ind];
      const DctData<Type, Alloc> & in_vol_2 = vols_in2[cs.data_ind];

      // Change called function
      funcs.emplace_back(std::function<void()>(
          std::bind(&DctVolumeGridTransform<Type, Alloc, vector_size>::op_element_wise_sub_block_binary_copy<Op>, this, op, out_vol, in_vol_1, in_vol_2, cs)));
    }

    this->process_tasks(funcs);
  }

}// namespace dct

#endif /* ZUTIL_CXX_INCLUDE_DCTDATAALGORITHMS_H_ */
