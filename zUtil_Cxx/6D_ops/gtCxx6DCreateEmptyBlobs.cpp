/*
 * gtCxx6DCreateEmptyBlobs.cpp
 *
 *  Created on: Sep 20, 2017
 *      Author: Nicola Vigano', 2017, CWI, N.R.Vigano@cwi.nl
 */

#include "DctDataIterativeOps.h"
#include "DctMatlabData.h"
#include "cpu_features.h"

const char * error_id = "gtCxx6DCreateEmptyBlobs:wrong_argument";

void mexFunction( int nlhs, mxArray * plhs[], int nrhs, const mxArray * prhs[] )
{
  const size_t base_args = 1;

  if (nrhs < base_args) {
    mexErrMsgIdAndTxt(error_id,
        "Not enough arguments!");
    return;
  }

  const mxArray * const blobs = prhs[0];

  if (!mxIsCell(blobs)) {
    mexErrMsgIdAndTxt(error_id,
        "The second argument should be a cell (blobs)");
    return;
  }

  const mxClassID blobs_class = dct::DctMatlabData::get_class_of_cell(blobs);

  size_t num_threads = std::thread::hardware_concurrency();
  bool use_avx = false;

  if (nrhs > base_args) {
    if ((nrhs - base_args) % 2) {
      mexErrMsgIdAndTxt(error_id,
          "Extra options should be specified in pairs");
      return;
    }
    for (mwIndex opt_ind = base_args; opt_ind < nrhs; opt_ind += 2) {
      std::string option_type = dct::DctMatlabData::get_string(prhs[opt_ind]);
      if (option_type == "threads") {
        if (!mxIsEmpty(prhs[opt_ind+1])) {
          num_threads = mxGetScalar(prhs[opt_ind+1]);
        }
      } else if (option_type == "use_avx") {
        if (!mxIsEmpty(prhs[opt_ind+1])) {
          use_avx = mxGetScalar(prhs[opt_ind+1]);
        }
      } else {
        mexErrMsgIdAndTxt(error_id,
            "Extra options allowed: 'threads', and 'use_avx'");
        return;
      }
    }
  }

  if (use_avx) {
    mexPrintf("AVX instructions not available in this build! Turning them off...\n");
    use_avx = false;
  }

  switch (blobs_class)
  {
    case mxDOUBLE_CLASS:
    {
      try {
        auto pv_blobs = dct::DctMatlabData::create_projection_vector_like<double>((mxArray *)blobs);

        dct::DctDataInitializer<double, dct::MatlabAllocator<double>, VECTOR_SIZE_SSE> transformer(0, num_threads);
        transformer.initialize(pv_blobs);

        plhs[0] = dct::DctMatlabData::produce_collection(pv_blobs, true);

      } catch (const dct::BasicException & e) {
        mexErrMsgIdAndTxt(error_id, e.what());
        return;
      }
      break;
    }
    case mxSINGLE_CLASS:
    {
      try {
        auto pv_blobs = dct::DctMatlabData::create_projection_vector_like<float>((mxArray *)blobs);

        dct::DctDataInitializer<float, dct::MatlabAllocator<float>, VECTOR_SIZE_SSE> transformer(0, num_threads);
        transformer.initialize(pv_blobs);

        plhs[0] = dct::DctMatlabData::produce_collection(pv_blobs, true);

      } catch (const dct::BasicException & e) {
        mexErrMsgIdAndTxt(error_id, e.what());
        return;
      }
      break;
    }
    case mxUNKNOWN_CLASS:
    {
      mexErrMsgIdAndTxt(error_id,
          "The argument needs to be a non empty Cell array of coherent floating point types");
      return;
    }
    default:
    {
      mexErrMsgIdAndTxt(error_id,
          "The argument needs to be a Cell array of floating point numbers");
      return;
    }
  }
}


