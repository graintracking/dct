/*
 * gtCxx6DUpdatePrimal.cpp
 *
 *  Created on: Aug 30, 2017
 *      Author: Nicola Vigano', 2017, CWI, N.R.Vigano@cwi.nl
 */

#include "DctDataIterativeOps.h"
#include "DctMatlabData.h"
#include "cpu_features.h"

const char * error_id = "gtCxx6DUpdatePrimal:wrong_argument";

void mexFunction( int nlhs, mxArray * plhs[], int nrhs, const mxArray * prhs[] )
{
  if (nrhs < 4) {
    mexErrMsgIdAndTxt(error_id,
        "Not enough arguments!");
    return;
  }
  if (nlhs != 2) {
    mexErrMsgIdAndTxt(error_id,
        "Not enough output!");
    return;
  }

  const mxArray * const curr_sol = prhs[0];
  const mxArray * const curr_enh_sol = prhs[1];
  const mxArray * const corrections = prhs[2];
  const mxArray * const tau = prhs[3];

  if (!mxIsCell(curr_sol)) {
    mexErrMsgIdAndTxt(error_id,
        "The first argument should be a cell (old solution variable)");
    return;
  }
  if (!mxIsCell(curr_enh_sol)) {
    mexErrMsgIdAndTxt(error_id,
        "The second argument should be a cell (old enhanced solution variable)");
    return;
  }
  if (!mxIsCell(corrections)) {
    mexErrMsgIdAndTxt(error_id,
        "The third argument should be a cell (Corrections)");
    return;
  }
  if (!mxIsCell(tau)) {
    mexErrMsgIdAndTxt(error_id,
        "The fourth argument should be a cell of scalars (tau)");
    return;
  }

  const mxClassID sols_class = dct::DctMatlabData::get_class_of_cell(curr_sol);
  const mxClassID sols_enh_class = dct::DctMatlabData::get_class_of_cell(curr_enh_sol);
  const mxClassID corr_class = dct::DctMatlabData::get_class_of_cell(corrections);

  const mwSize num_elems_1 = mxGetNumberOfElements(curr_sol);
  const mwSize num_elems_2 = mxGetNumberOfElements(curr_enh_sol);
  const mwSize num_elems_3 = mxGetNumberOfElements(corrections);

  if ((num_elems_1 != num_elems_2) ||(num_elems_1 != num_elems_3))
  {
    mexErrMsgIdAndTxt(error_id,
        "The Cell arrays should have the same size");
    return;
  }

  if (sols_class != sols_enh_class || sols_class != corr_class)
  {
    mexErrMsgIdAndTxt(error_id,
        "The arguments need to be Cell arrays of coherent floating point types");
    return;
  }

  size_t num_threads = std::thread::hardware_concurrency();
  bool use_avx = false;

  if (nrhs > 4) {
    if (!(nrhs == 6 || nrhs == 8)) {
      mexErrMsgIdAndTxt(error_id,
          "Extra options should be specified in pairs");
      return;
    }
    for (mwIndex opt_ind = 4; opt_ind < nrhs; opt_ind += 2) {
      std::string option_type = dct::DctMatlabData::get_string(prhs[opt_ind]);
      if (option_type == "threads") {
        if (!mxIsEmpty(prhs[opt_ind+1])) {
          num_threads = mxGetScalar(prhs[opt_ind+1]);
        }
      } else if (option_type == "use_avx") {
        if (!mxIsEmpty(prhs[opt_ind+1])) {
          use_avx = mxGetScalar(prhs[opt_ind+1]);
        }
      } else {
        mexErrMsgIdAndTxt(error_id,
            "Extra options allowed: 'threads', and 'use_avx'");
        return;
      }
    }
  }

  plhs[0] = mxCreateSharedDataCopy(curr_sol);
  plhs[1] = mxCreateSharedDataCopy(curr_enh_sol);

  if (use_avx) {
#if defined(__AVX__)
    if (!can_use_avx()) {
      mexPrintf("AVX instructions not available on this CPU! Turning them off...\n");
      use_avx = false;
    }
#else
    mexPrintf("AVX instructions not available in this build! Turning them off...\n");
    use_avx = false;
#endif
  }

  switch (sols_class)
  {
    case mxDOUBLE_CLASS:
    {
      try {
        auto vols_sol = dct::DctMatlabData::import_volume_grid<double>((mxArray *)curr_sol);
        auto vols_enh_sol = dct::DctMatlabData::import_volume_grid<double>((mxArray *)curr_enh_sol);
        vols_sol.check_consistency();
        vols_enh_sol.check_consistency();

        auto vols_corr = dct::DctMatlabData::import_volume_grid<double>((mxArray *)corrections);
        auto vols_taus = dct::DctMatlabData::import_volume_grid<double>((mxArray *)tau);

#if defined(__AVX__)
        if (use_avx) {
          dct::DctVolumeGridSolutionUpdate<double, dct::MatlabAllocator<double>, VECTOR_SIZE_AVX> transformer(vols_sol, vols_enh_sol, num_threads);
          transformer.update(vols_corr, vols_taus);
        } else {
          dct::DctVolumeGridSolutionUpdate<double, dct::MatlabAllocator<double>, VECTOR_SIZE_SSE> transformer(vols_sol, vols_enh_sol, num_threads);
          transformer.update(vols_corr, vols_taus);
        }
#else
        dct::DctVolumeGridSolutionUpdate<double, dct::MatlabAllocator<double>, VECTOR_SIZE_SSE> transformer(vols_sol, vols_enh_sol, num_threads);
        transformer.update(vols_corr, vols_taus);
#endif

      } catch (const dct::BasicException & e) {
        mexErrMsgIdAndTxt(error_id, e.what());
        return;
      }
      break;
    }
    case mxSINGLE_CLASS:
    {
      try {
        auto vols_sol = dct::DctMatlabData::import_volume_grid<float>((mxArray *)curr_sol);
        auto vols_enh_sol = dct::DctMatlabData::import_volume_grid<float>((mxArray *)curr_enh_sol);
        vols_sol.check_consistency();
        vols_enh_sol.check_consistency();

        auto vols_corr = dct::DctMatlabData::import_volume_grid<float>((mxArray *)corrections);
        auto vols_taus = dct::DctMatlabData::import_volume_grid<float>((mxArray *)tau);

#if defined(__AVX__)
        if (use_avx) {
          dct::DctVolumeGridSolutionUpdate<float, dct::MatlabAllocator<float>, VECTOR_SIZE_AVX> transformer(vols_sol, vols_enh_sol, num_threads);
          transformer.update(vols_corr, vols_taus);
        } else {
          dct::DctVolumeGridSolutionUpdate<float, dct::MatlabAllocator<float>, VECTOR_SIZE_SSE> transformer(vols_sol, vols_enh_sol, num_threads);
          transformer.update(vols_corr, vols_taus);
        }
#else
        dct::DctVolumeGridSolutionUpdate<float, dct::MatlabAllocator<float>, VECTOR_SIZE_SSE> transformer(vols_sol, vols_enh_sol, num_threads);
        transformer.update(vols_corr, vols_taus);
#endif

      } catch (const dct::BasicException & e) {
        mexErrMsgIdAndTxt(error_id, e.what());
        return;
      }
      break;
    }
    case mxUNKNOWN_CLASS:
    {
      mexErrMsgIdAndTxt(error_id,
          "The argument needs to be a non empty Cell array of coherent floating point types");
      return;
    }
    default:
    {
      mexErrMsgIdAndTxt(error_id,
          "The argument needs to be a Cell array of floating point numbers");
      return;
    }
  }
}


