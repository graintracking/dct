/*
 * gtCxx6DBlobsToMultipleSinos.cpp
 *
 *  Created on: Jan 10, 2017
 *      Author: Nicola Vigano', 2017, CWI, N.R.Vigano@cwi.nl
 */

#include "DctMatlabData.h"
#include "cpu_features.h"

static const char * error_id = "gtCxx6DBlobsToMultipleSinos:wrong_argument";

template<typename Type, const size_t vector_size>
mxArray * process_data(const mxArray * const blobs_cells, const mxArray * const sinos_sizes, const mxArray * const coeffs, const size_t num_threads)
{
  dct::DctProjectionTransform<Type, dct::MatlabAllocator<Type> > transformer(num_threads);

  auto && metad = dct::DctMatlabData::import_coefficients_list<Type>(coeffs);
  transformer.set_transforms(metad);

  auto && sinos = dct::DctMatlabData::create_collection_from_dims<Type>(sinos_sizes);
  transformer.set_sinos(sinos);

  auto && blobs = dct::DctMatlabData::import_collection<Type>((mxArray *)blobs_cells);
  transformer.set_blobs(blobs);

  transformer.check_consistency();

  transformer.produce_sinograms();

  return dct::DctMatlabData::produce_collection(transformer.get_sinos(), true);
}

void mexFunction( int nlhs, mxArray * plhs[], int nrhs, const mxArray * prhs[] )
{
  size_t num_threads = std::thread::hardware_concurrency();
  bool use_avx = false;

  if (nrhs == 1) {
    std::string option = dct::DctMatlabData::get_string(prhs[0]);
    if (option == "version") {
      plhs[0] = mxCreateDoubleScalar(dct::VERSION_MAJOR);
      plhs[1] = mxCreateDoubleScalar(dct::VERSION_MINOR);
    } else if (option == "info") {
      mexPrintf("[gtCxx6DBlobsToMultipleSinos] Defaults - num_threads: %lu, use_avx: %s\n", num_threads, use_avx ? "true" : "false");
    } else if (option == "help") {
      mexPrintf("sinos = gtCxx6DBlobsToMultipleSinos(sinos_sizes, blobs, coeff_struct [, 'threads', num_threads, 'use_avx', true|false])\n");
    } else {
      mexErrMsgIdAndTxt(error_id,
          "Options allowed: 'version', 'info', and 'help'");
    }
    return;
  }

  const size_t base_args = 3;
  if (nrhs < base_args) {
    mexErrMsgIdAndTxt(error_id,
        "Not enough arguments! Three needed: projection sizes, blobs, and coefficients");
    return;
  }

  const mxArray * const sinos_sizes = prhs[0];
  const mxArray * const blobs_cells = prhs[1];
  const mxArray * const coeffs = prhs[2];

  if (!mxIsNumeric(sinos_sizes)) {
    mexErrMsgIdAndTxt(error_id,
        "The first argument should be an array (projection sizes)");
    return;
  }
  if (!mxIsCell(blobs_cells)) {
    mexErrMsgIdAndTxt(error_id,
        "The second argument should be a cell array (blobs)");
    return;
  }

  const mxClassID datatype = dct::DctMatlabData::get_class_of_cell(blobs_cells);

  if (datatype == mxUNKNOWN_CLASS) {
    mexErrMsgIdAndTxt(error_id,
        "The blobs need to be a non empty cell array of coherent floating point types");
    return;
  }

  if (nrhs > base_args) {
    if ((nrhs - base_args) % 2) {
      mexErrMsgIdAndTxt(error_id,
          "Extra options should be specified in pairs");
      return;
    }
    for (mwIndex opt_ind = base_args; opt_ind < nrhs; opt_ind += 2) {
      std::string option_type = dct::DctMatlabData::get_string(prhs[opt_ind]);
      if (option_type == "threads") {
        if (!mxIsEmpty(prhs[opt_ind+1])) {
          num_threads = mxGetScalar(prhs[opt_ind+1]);
        }
      } else if (option_type == "use_avx") {
        if (!mxIsEmpty(prhs[opt_ind+1])) {
          use_avx = mxGetScalar(prhs[opt_ind+1]);
        }
      } else {
        mexErrMsgIdAndTxt(error_id,
            "Extra options allowed: 'threads', and 'use_avx'");
        return;
      }
    }
  }

  if (use_avx) {
#if defined(__AVX__)
    if (!can_use_avx()) {
      mexPrintf("AVX instructions not available on this CPU! Turning them off...\n");
      use_avx = false;
    }
#else
    mexPrintf("AVX instructions not available in this build! Turning them off...\n");
    use_avx = false;
#endif
  }

  switch (datatype)
  {
    case mxDOUBLE_CLASS:
    {
      try {
#if defined(__AVX__)
        if (use_avx) {
          plhs[0] = process_data<double, VECTOR_SIZE_AVX>(blobs_cells, sinos_sizes, coeffs, num_threads);
        } else {
          plhs[0] = process_data<double, VECTOR_SIZE_SSE>(blobs_cells, sinos_sizes, coeffs, num_threads);
        }
#else
        plhs[0] = process_data<double, VECTOR_SIZE_SSE>(blobs_cells, sinos_sizes, coeffs, num_threads);
#endif
      } catch (const dct::BasicException & e) {
        mexErrMsgIdAndTxt(error_id, e.what());
        return;
      }
      break;
    }
    case mxSINGLE_CLASS:
    {
      try {
#if defined(__AVX__)
        if (use_avx) {
          plhs[0] = process_data<float, VECTOR_SIZE_AVX>(blobs_cells, sinos_sizes, coeffs, num_threads);
        } else {
          plhs[0] = process_data<float, VECTOR_SIZE_SSE>(blobs_cells, sinos_sizes, coeffs, num_threads);
        }
#else
        plhs[0] = process_data<float, VECTOR_SIZE_SSE>(blobs_cells, sinos_sizes, coeffs, num_threads);
#endif
      } catch (const dct::BasicException & e) {
        mexErrMsgIdAndTxt(error_id, e.what());
        return;
      }
      break;
    }
    default:
    {
      mexErrMsgIdAndTxt(error_id,
          "The argument needs to be a Cell array of floating point numbers");
      return;
    }
  }
}


