/*
 * gtCxx6DMultipleSinosToBlobs.cpp
 *
 *  Created on: Jan 10, 2017
 *      Author: Nicola Vigano', 2017, CWI, N.R.Vigano@cwi.nl
 */

#include "DctMatlabData.h"
#include "cpu_features.h"

static const char * error_id = "gtCxx6DMultipleSinosToBlobs:wrong_argument";

template<typename Type, const size_t vector_size>
void process_data(mxArray * const out_blobs, const mxArray * const sinos_cells, const mxArray * const coeffs, const size_t num_threads)
{
  dct::DctProjectionTransform<Type, dct::MatlabAllocator<Type>, vector_size> transformer(num_threads);

  auto && metad = dct::DctMatlabData::import_coefficients_list<Type>(coeffs);
  transformer.set_transforms(metad);

  auto && sinos = dct::DctMatlabData::import_collection<Type>((mxArray *)sinos_cells);
  transformer.set_sinos(sinos);

  auto && blobs = dct::DctMatlabData::import_collection<Type>(out_blobs);
  transformer.set_blobs(blobs);

  transformer.check_consistency();

  transformer.produce_blobs();
}

void mexFunction( int nlhs, mxArray * plhs[], int nrhs, const mxArray * prhs[] )
{
  size_t num_threads = std::thread::hardware_concurrency();
  bool use_avx = false;

  if (nrhs == 1) {
    std::string option = dct::DctMatlabData::get_string(prhs[0]);
    if (option == "version") {
      plhs[0] = mxCreateDoubleScalar(dct::VERSION_MAJOR);
      plhs[1] = mxCreateDoubleScalar(dct::VERSION_MINOR);
    } else if (option == "info") {
      mexPrintf("[gtCxx6DMultipleSinosToBlobs] Defaults - num_threads: %lu, use_avx: %s\n", num_threads, use_avx ? "true" : "false");
    } else if (option == "help") {
      mexPrintf("blobs = gtCxx6DMultipleSinosToBlobs(blobs, sinos, coeff_struct [, 'threads', num_threads, 'use_avx', true|false])\n");
    } else {
      mexErrMsgIdAndTxt(error_id,
          "Options allowed: 'version', 'info', and 'help'");
    }
    return;
  }

  const size_t base_args = 3;
  if (nrhs < base_args) {
    mexErrMsgIdAndTxt(error_id,
        "Not enough arguments! Three needed: blobs, projections, and coefficients");
    return;
  }

  const mxArray * const blobs_cells = prhs[0];
  const mxArray * const sinos_cells = prhs[1];
  const mxArray * const coeffs = prhs[2];

  if (!mxIsCell(sinos_cells)) {
    mexErrMsgIdAndTxt(error_id,
        "The first argument should be a cell array (projections)");
    return;
  }
  if (!mxIsCell(blobs_cells)) {
    mexErrMsgIdAndTxt(error_id,
        "The second argument should be a cell array (blobs)");
    return;
  }

  const mxClassID datatype = dct::DctMatlabData::get_class_of_cell(blobs_cells);
  const mxClassID datatype_sinos = dct::DctMatlabData::get_class_of_cell(sinos_cells);

  if (datatype == mxUNKNOWN_CLASS)
  {
    mexErrMsgIdAndTxt(error_id,
        "The blobs need to be a non empty cell array of coherent floating point types");
    return;
  }
  if (datatype != datatype_sinos)
  {
    mexErrMsgIdAndTxt(error_id,
        "The blobs need to be a non empty cell array of a coherent floating point types with the projections");
    return;
  }

  if (nrhs > base_args) {
    if ((nrhs - base_args) % 2) {
      mexErrMsgIdAndTxt(error_id,
          "Extra options should be specified in pairs");
      return;
    }
    for (mwIndex opt_ind = base_args; opt_ind < nrhs; opt_ind += 2) {
      std::string option_type = dct::DctMatlabData::get_string(prhs[opt_ind]);
      if (option_type == "threads") {
        if (!mxIsEmpty(prhs[opt_ind+1])) {
          num_threads = mxGetScalar(prhs[opt_ind+1]);
        }
      } else if (option_type == "use_avx") {
        if (!mxIsEmpty(prhs[opt_ind+1])) {
          use_avx = mxGetScalar(prhs[opt_ind+1]);
        }
      } else {
        mexErrMsgIdAndTxt(error_id,
            "Extra options allowed: 'threads', and 'use_avx'");
        return;
      }
    }
  }

  if (use_avx) {
#if defined(__AVX__)
    if (!can_use_avx()) {
      mexPrintf("AVX instructions not available on this CPU! Turning them off...\n");
      use_avx = false;
    }
#else
    mexPrintf("AVX instructions not available in this build! Turning them off...\n");
    use_avx = false;
#endif
  }

  plhs[0] = mxCreateSharedDataCopy(blobs_cells);

  switch (datatype)
  {
    case mxDOUBLE_CLASS:
    {
      try {
#if defined(__AVX__)
        if (use_avx) {
          process_data<double, VECTOR_SIZE_AVX>(plhs[0], sinos_cells, coeffs, num_threads);
        } else {
          process_data<double, VECTOR_SIZE_SSE>(plhs[0], sinos_cells, coeffs, num_threads);
        }
#else
        process_data<double, VECTOR_SIZE_SSE>(plhs[0], sinos_cells, coeffs, num_threads);
#endif
      } catch (const dct::BasicException & e) {
        mexErrMsgIdAndTxt(error_id, e.what());
        return;
      }
      break;
    }
    case mxSINGLE_CLASS:
    {
      try {
#if defined(__AVX__)
        if (use_avx) {
          process_data<float, VECTOR_SIZE_AVX>(plhs[0], sinos_cells, coeffs, num_threads);
        } else {
          process_data<float, VECTOR_SIZE_SSE>(plhs[0], sinos_cells, coeffs, num_threads);
        }
#else
        process_data<float, VECTOR_SIZE_SSE>(plhs[0], sinos_cells, coeffs, num_threads);
#endif
      } catch (const dct::BasicException & e) {
        mexErrMsgIdAndTxt(error_id, e.what());
        return;
      }
      break;
    }
    default:
    {
      mexErrMsgIdAndTxt(error_id,
          "The argument needs to be a Cell array of floating point numbers");
      return;
    }
  }
}


