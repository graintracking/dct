/*
 * gtCxx6DUpdateDualDetector.cpp
 *
 *  Created on: Sep 14, 2017
 *      Author: Nicola Vigano', 2017, CWI, N.R.Vigano@cwi.nl
 */

#include "DctDataIterativeOps.h"
#include "DctMatlabData.h"
#include "cpu_features.h"

const char * error_id = "gtCxx6DUpdateDualDetector:wrong_argument";

void mexFunction( int nlhs, mxArray * plhs[], int nrhs, const mxArray * prhs[] )
{
  if (nrhs < 5) {
    mexErrMsgIdAndTxt(error_id,
        "Not enough arguments!");
    return;
  }

  const mxArray * const norm_m = prhs[0];
  const mxArray * const curr_dual = prhs[1];
  const mxArray * const blobs = prhs[2];
  const mxArray * const proj_blobs = prhs[3];
  const mxArray * const sigma = prhs[4];

  if (!mxIsChar(norm_m)) {
    mexErrMsgIdAndTxt(error_id,
        "The first argument should be a string (the norm)");
    return;
  }
  if (!mxIsCell(curr_dual)) {
    mexErrMsgIdAndTxt(error_id,
        "The second argument should be a cell (old dual)");
    return;
  }
  if (!mxIsCell(blobs)) {
    mexErrMsgIdAndTxt(error_id,
        "The third argument should be a cell (blobs)");
    return;
  }
  if (!mxIsCell(proj_blobs)) {
    mexErrMsgIdAndTxt(error_id,
        "The fourth argument should be a cell (projected blobs)");
    return;
  }
  if (!mxIsCell(sigma)) {
    mexErrMsgIdAndTxt(error_id,
        "The fifth argument should be a cell (sigma)");
    return;
  }

  const mxClassID dual_class = dct::DctMatlabData::get_class_of_cell(curr_dual);
  const mxClassID blobs_class = dct::DctMatlabData::get_class_of_cell(blobs);
  const mxClassID proj_blobs_class = dct::DctMatlabData::get_class_of_cell(proj_blobs);
  const mxClassID sigma_class = dct::DctMatlabData::get_class_of_cell(sigma);

  const mwSize num_elems_1 = mxGetNumberOfElements(curr_dual);
  const mwSize num_elems_2 = mxGetNumberOfElements(blobs);
  const mwSize num_elems_3 = mxGetNumberOfElements(proj_blobs);
  const mwSize num_elems_4 = mxGetNumberOfElements(sigma);

  if ((num_elems_1 != num_elems_2)
      ||(num_elems_1 != num_elems_3)
      ||(num_elems_1 != num_elems_4))
  {
    mexErrMsgIdAndTxt(error_id, "The Cell arrays should have the same size");
    return;
  }

  if (dual_class != blobs_class
      || dual_class != proj_blobs_class
      || dual_class != sigma_class)
  {
    mexErrMsgIdAndTxt(error_id, "The arguments need to be Cell arrays of coherent floating point types");
    return;
  }

  const std::string norm = dct::DctMatlabData::get_string(norm_m);
  const mxArray * sigma1;
  const bool is_l2norm = norm == "l2";
  if (is_l2norm) {
    if (nrhs < 6) {
      mexErrMsgIdAndTxt(error_id, "Not enough arguments!");
      return;
    }

    sigma1 = prhs[5];

    if (!mxIsCell(sigma1)) {
      mexErrMsgIdAndTxt(error_id, "The sixth argument should be a cell (sigma1)");
      return;
    }
    const mxClassID sigma1_class = dct::DctMatlabData::get_class_of_cell(sigma1);
    const mwSize num_elems_5 = mxGetNumberOfElements(sigma1);

    if (num_elems_1 != num_elems_5) {
      mexErrMsgIdAndTxt(error_id, "The Cell arrays should have the same size");
      return;
    }
    if (dual_class != sigma1_class) {
      mexErrMsgIdAndTxt(error_id, "The arguments need to be Cell arrays of coherent floating point types");
      return;
    }

  }

  size_t num_threads = std::thread::hardware_concurrency();
  bool use_avx = false;

  const size_t base_args = is_l2norm ? 6 : 5;
  if (nrhs > base_args) {
    if ((nrhs - base_args) % 2) {
      mexErrMsgIdAndTxt(error_id,
          "Extra options should be specified in pairs");
      return;
    }
    for (mwIndex opt_ind = base_args; opt_ind < nrhs; opt_ind += 2) {
      std::string option_type = dct::DctMatlabData::get_string(prhs[opt_ind]);
      if (option_type == "threads") {
        if (!mxIsEmpty(prhs[opt_ind+1])) {
          num_threads = mxGetScalar(prhs[opt_ind+1]);
        }
      } else if (option_type == "use_avx") {
        if (!mxIsEmpty(prhs[opt_ind+1])) {
          use_avx = mxGetScalar(prhs[opt_ind+1]);
        }
      } else {
        mexErrMsgIdAndTxt(error_id,
            "Extra options allowed: 'threads', and 'use_avx'");
        return;
      }
    }
  }

  plhs[0] = mxCreateSharedDataCopy(curr_dual);

  if (use_avx) {
    mexPrintf("AVX instructions not available in this build! Turning them off...\n");
    use_avx = false;
  }

  switch (dual_class)
  {
    case mxDOUBLE_CLASS:
    {
      try {
        auto pv_dual = dct::DctMatlabData::import_collection<double>((mxArray *)curr_dual);
        auto pv_blobs = dct::DctMatlabData::import_collection<double>((mxArray *)blobs);
        auto pv_pblobs = dct::DctMatlabData::import_collection<double>((mxArray *)proj_blobs);
        auto pv_sigma = dct::DctMatlabData::import_collection<double>((mxArray *)sigma);

        dct::DctProjectionVectorDetectorUpdate<double, dct::MatlabAllocator<double>, VECTOR_SIZE_SSE> transformer(pv_dual, num_threads);
        if (is_l2norm) {
          auto pv_sigma1 = dct::DctMatlabData::import_collection<double>((mxArray *)sigma1);
          transformer.update_l2(pv_blobs, pv_pblobs, pv_sigma, pv_sigma1);
        } else if (norm == "l1") {
          transformer.update_l1(pv_blobs, pv_pblobs, pv_sigma);
        } else if (norm == "kl") {
          transformer.update_kl(pv_blobs, pv_pblobs, pv_sigma);
        } else {
          THROW(dct::WrongArgumentException, gtCxx6DUpdateDualDetector, std::string("Unknown norm: ") + norm);
        }

      } catch (const dct::BasicException & e) {
        mexErrMsgIdAndTxt(error_id, e.what());
        return;
      }
      break;
    }
    case mxSINGLE_CLASS:
    {
      try {
        auto pv_dual = dct::DctMatlabData::import_collection<float>((mxArray *)curr_dual);
        auto pv_blobs = dct::DctMatlabData::import_collection<float>((mxArray *)blobs);
        auto pv_pblobs = dct::DctMatlabData::import_collection<float>((mxArray *)proj_blobs);
        auto pv_sigma = dct::DctMatlabData::import_collection<float>((mxArray *)sigma);

        dct::DctProjectionVectorDetectorUpdate<float, dct::MatlabAllocator<float>, VECTOR_SIZE_SSE> transformer(pv_dual, num_threads);
        if (is_l2norm) {
          auto pv_sigma1 = dct::DctMatlabData::import_collection<float>((mxArray *)sigma1);
          transformer.update_l2(pv_blobs, pv_pblobs, pv_sigma, pv_sigma1);
        } else if (norm == "l1") {
          transformer.update_l1(pv_blobs, pv_pblobs, pv_sigma);
        } else if (norm == "kl") {
          transformer.update_kl(pv_blobs, pv_pblobs, pv_sigma);
        } else {
          THROW(dct::WrongArgumentException, gtCxx6DUpdateDualDetector, std::string("Unknown norm: ") + norm);
        }

      } catch (const dct::BasicException & e) {
        mexErrMsgIdAndTxt(error_id, e.what());
        return;
      }
      break;
    }
    case mxUNKNOWN_CLASS:
    {
      mexErrMsgIdAndTxt(error_id,
          "The argument needs to be a non empty Cell array of coherent floating point types");
      return;
    }
    default:
    {
      mexErrMsgIdAndTxt(error_id,
          "The argument needs to be a Cell array of floating point numbers");
      return;
    }
  }
}


