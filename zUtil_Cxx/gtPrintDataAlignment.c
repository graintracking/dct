/**
 * Prints to which which multiple of bytes, the array is aligned
 *
 * Used just for inquiry purposes.
 *
 * Nicola Vigano', 2012, ID11 @ ESRF vigano@esrf.eu
 */

#include <mex.h>

#define LAST_02_BYTES(x)  (((long unsigned int)x) & 0x1)
#define LAST_04_BYTES(x)  (((long unsigned int)x) & 0x3)
#define LAST_08_BYTES(x)  (((long unsigned int)x) & 0x7)
#define LAST_16_BYTES(x)  (((long unsigned int)x) & 0xF)
#define LAST_32_BYTES(x)  (((long unsigned int)x) & 0xFF)

const char * const alignedTo(const double * const image_in);

void mexFunction( int nlhs, mxArray * plhs[], int nrhs, const mxArray * prhs[] )
{
  /* Pointers to incoming matrices: */
  const double * const image_in = mxGetPr(prhs[0]);

  mexPrintf("Array starting at address: %p, aligned to %s. SSE friendly: %s\n",
      image_in, alignedTo(image_in), LAST_16_BYTES(image_in) ? "false" : "true"
      );
}

const char * const
alignedTo(const double * const image_in)
{
  if (!(LAST_32_BYTES(image_in))) {
    return "long long double (256bit)";
  } else if (!(LAST_16_BYTES(image_in))) {
    return "long double (128bit)";
  } else if (!(LAST_08_BYTES(image_in))) {
    return "double (64bit)";
  } else if (!(LAST_04_BYTES(image_in))) {
    return "float (32bit)";
  } else if (!(LAST_02_BYTES(image_in))) {
    return "short float (16bit)";
  } else {
    return "byte (8bit)";
  }
}
