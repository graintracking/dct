/**
 * Adds a grain to the volume, taking care of the superposition between grains.
 * The shared voxels are put to -1, while the others are set to the index of the
 * grain.
 *
 * Nicola Vigano', 2012, ID11 @ ESRF vigano@esrf.eu
 */

#include <limits>

#include "mex.h"

#include "GtMatlabVolumeLooper.h"

using namespace std;

template<typename TypeIn>
void extDoAssignment(mxArray * output, void * vol, const double * limits,
                  const mwSize * dimsOut, const mwSize * dimsVol,
                  const double & index);

template<typename TypeOut, typename TypeIn>
class GtMatlabVolumeVoxel : public GtMatlabVolumeLooper<TypeOut, TypeIn> {
protected:
  void doWork(TypeOut & dest, const TypeIn & orig)
  {
    if (orig && (orig < dest)) { dest = orig; }
  }
public:
  GtMatlabVolumeVoxel(void * _out, void * _vol, const double * limits,
      const mwSize * dims_out, const mwSize * dims_vol, const double & indx)
  : GtMatlabVolumeLooper<TypeOut, TypeIn>(_out, _vol, limits, dims_out, dims_vol, indx)
  { }

  void loopOver3D()
  {
    numeric_limits<TypeOut> numLims;
    /* Counters for the bigger volume */
    mwIndex counter1 = 0, counter2 = 0, counter3 = 0;

    const mwSize & numElems = this->dimsOut[0];

    for (mwIndex elem = 0; elem < numElems; elem++) {
      const double * limpos = this->limits + elem*6;
      this->lim1[0] = limpos[0]-1, this->lim1[1] = limpos[1]-1;
      this->lim2[0] = limpos[2]-1, this->lim2[1] = limpos[3]-1;
      this->lim3[0] = limpos[4]-1, this->lim3[1] = limpos[5]-1;
      TypeOut tempOut = numLims.max();

      /* These loops use extensively pointer arithmetics in determining the chuck
       * of the matrix to be computed */
      for(counter3 = this->lim3[0]; counter3 <= this->lim3[1]; counter3++) {
        /* Base vectors, which save computation of the 3rd dimension */
        const TypeIn * const base3 = this->vol + counter3*this->dimsVol[0]*this->dimsVol[1];
        for(counter2 = this->lim2[0]; counter2 <= this->lim2[1]; counter2++) {
          /* Base vectors, which save computation of the 2nd dimension */
          const TypeIn * const base2 = base3 + counter2*this->dimsVol[0];
          /* Inner loop on the 1st dimension: the contiguous in memory */
          for(counter1 = this->lim1[0]; counter1 <= this->lim1[1]; counter1++) {
            this->doWork(tempOut, base2[counter1]);
          }
        }
      }
      if (tempOut < numLims.max()) {
        this->output[elem] = tempOut;
      }
    }
  }
};

void mexFunction( int nlhs, mxArray * plhs[], int nrhs, const mxArray * prhs[] )
{
  /* Incoming image dimensions */
  const mwSize * dimsOut = mxGetDimensions(prhs[0]);
  const mwSize * dimsVol = mxGetDimensions(prhs[1]);

  /* Value of the grain */
  const double index_1 = *mxGetPr(prhs[2]);

  /* Limits of the grain bounding box */
  const double * const limits = mxGetPr(prhs[3]);

  /* -- No assert on correctness of parameters -- */

  /* Don't copy, just modify the incoming matrix */
  plhs[0] = mxCreateSharedDataCopy(prhs[0]);

  switch(mxGetClassID(prhs[1])) {
    case mxDOUBLE_CLASS: {
      extDoAssignment<const double>(plhs[0], mxGetData(prhs[1]), limits, dimsOut, dimsVol, index_1);
      break;
    }
    case mxSINGLE_CLASS: {
      extDoAssignment<const float>(plhs[0], mxGetData(prhs[1]), limits, dimsOut, dimsVol, index_1);
      break;
    }
    case mxINT8_CLASS: {
      extDoAssignment<const char>(plhs[0], mxGetData(prhs[1]), limits, dimsOut, dimsVol, index_1);
      break;
    }
    case mxUINT8_CLASS: {
      extDoAssignment<const unsigned char>(plhs[0], mxGetData(prhs[1]), limits, dimsOut, dimsVol, index_1);
      break;
    }
    case mxINT16_CLASS: {
      extDoAssignment<const short int>(plhs[0], mxGetData(prhs[1]), limits, dimsOut, dimsVol, index_1);
      break;
    }
    case mxUINT16_CLASS: {
      extDoAssignment<const unsigned short int>(plhs[0], mxGetData(prhs[1]), limits, dimsOut, dimsVol, index_1);
      break;
    }
    case mxINT32_CLASS: {
      extDoAssignment<const int>(plhs[0], mxGetData(prhs[1]), limits, dimsOut, dimsVol, index_1);
      break;
    }
    case mxUINT32_CLASS: {
      extDoAssignment<const unsigned int>(plhs[0], mxGetData(prhs[1]), limits, dimsOut, dimsVol, index_1);
      break;
    }
    case mxINT64_CLASS: {
      extDoAssignment<const long int>(plhs[0], mxGetData(prhs[1]), limits, dimsOut, dimsVol, index_1);
      break;
    }
    case mxUINT64_CLASS: {
      extDoAssignment<const unsigned long int>(plhs[0], mxGetData(prhs[1]), limits, dimsOut, dimsVol, index_1);
      break;
    }
    default:
      break;
  }
}

template<typename TypeIn>
void
extDoAssignment(mxArray * output, void * vol, const double * limits,
                  const mwSize * dimsOut, const mwSize * dimsVol,
                  const double & index)
{
  switch(mxGetClassID(output)) {
    case mxDOUBLE_CLASS: {
      GtMatlabVolumeVoxel<double, TypeIn>
                looper(mxGetData(output), vol, limits, dimsOut, dimsVol, index);
      looper.loopOver3D();
      break;
    }
    case mxSINGLE_CLASS: {
      GtMatlabVolumeVoxel<float, TypeIn>
                looper(mxGetData(output), vol, limits, dimsOut, dimsVol, index);
      looper.loopOver3D();
      break;
    }
    case mxINT8_CLASS: {
      GtMatlabVolumeVoxel<char, TypeIn>
                looper(mxGetData(output), vol, limits, dimsOut, dimsVol, index);
      looper.loopOver3D();
      break;
    }
    case mxUINT8_CLASS: {
      GtMatlabVolumeVoxel<unsigned char, TypeIn>
                looper(mxGetData(output), vol, limits, dimsOut, dimsVol, index);
      looper.loopOver3D();
      break;
    }
    case mxINT16_CLASS: {
      GtMatlabVolumeVoxel<short int, TypeIn>
                looper(mxGetData(output), vol, limits, dimsOut, dimsVol, index);
      looper.loopOver3D();
      break;
    }
    case mxUINT16_CLASS: {
      GtMatlabVolumeVoxel<unsigned short int, TypeIn>
                looper(mxGetData(output), vol, limits, dimsOut, dimsVol, index);
      looper.loopOver3D();
      break;
    }
    case mxINT32_CLASS: {
      GtMatlabVolumeVoxel<int, TypeIn>
                looper(mxGetData(output), vol, limits, dimsOut, dimsVol, index);
      looper.loopOver3D();
      break;
    }
    case mxUINT32_CLASS: {
      GtMatlabVolumeVoxel<unsigned int, TypeIn>
                looper(mxGetData(output), vol, limits, dimsOut, dimsVol, index);
      looper.loopOver3D();
      break;
    }
    case mxINT64_CLASS: {
      GtMatlabVolumeVoxel<long int, TypeIn>
                looper(mxGetData(output), vol, limits, dimsOut, dimsVol, index);
      looper.loopOver3D();
      break;
    }
    case mxUINT64_CLASS: {
      GtMatlabVolumeVoxel<unsigned long int, TypeIn>
                looper(mxGetData(output), vol, limits, dimsOut, dimsVol, index);
      looper.loopOver3D();
      break;
    }
    default:
      break;
  }
}

