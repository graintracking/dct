/**
 * Does exactly the same as 'medfilt2' function from Imaging toolbox, but using
 * SciPy modified implementation, found in PyMca.
 *
 * PyMca is developed by V. Armando Sole', in ISDD/DAU @ ESRF, sole@esrf.eu
 * http://pymca.sourceforge.net/
 *
 * The aim was to get rid of license requirements, imposed by Matlab Imaging
 * Toolbox.
 *
 * This code is distributed under the terms of GPL 2.1 license, where not under
 * the SciPy license.
 *
 * Nicola Vigano', 2012, ID11 @ ESRF vigano@esrf.eu
 */

#include <stdlib.h>

#include <mex.h>

/* defined below */
void f_medfilt2(float *, float *, int *, int *);
void d_medfilt2(double *, double *, int *, int *);
void i8_medfilt2(signed char *, signed char *, int *, int *);
void u8_medfilt2(unsigned char *, unsigned char *, int *, int *);
void i16_medfilt2(signed short int *, signed short int *, int *, int *);
void u16_medfilt2(unsigned short int *, unsigned short int *, int *, int *);
void i32_medfilt2(signed int *, signed int *, int *, int *);
void u32_medfilt2(unsigned int *, unsigned int *, int *, int *);
void i64_medfilt2(signed long int *, signed long int *, int *, int *);
void u64_medfilt2(unsigned long int *, unsigned long int *, int *, int *);

void mexFunction( int nlhs, mxArray * plhs[], int nrhs, const mxArray * prhs[] )
{
  /* Incoming image dimensions */
  int dims[2] = {mxGetN(prhs[0]), mxGetM(prhs[0])};

  /* Pointers to incoming matrices: */
  double * filterSizes = mxGetPr(prhs[1]);
  int filt_sizes[2] = {filterSizes[0], filterSizes[1]};

  /* No assert on dimensions of the filter. If you want any error detection, use
   * a Matlab wrapper, called gtImgMedianFilter2.m, which in turn calls this
   * function */

  switch(mxGetClassID(prhs[0])) {
    case mxDOUBLE_CLASS: {
      plhs[0] = mxCreateDoubleMatrix(dims[1], dims[0], mxREAL);
      d_medfilt2(mxGetPr(prhs[0]), mxGetPr(plhs[0]), filt_sizes, dims);
      break;
    }
    case mxSINGLE_CLASS: {;
      plhs[0] = mxCreateNumericMatrix(dims[1], dims[0], mxSINGLE_CLASS, mxREAL);
      f_medfilt2(mxGetData(prhs[0]), mxGetData(plhs[0]), filt_sizes, dims);
      break;
    }
    case mxINT8_CLASS: {
      plhs[0] = mxCreateNumericMatrix(dims[1], dims[0], mxINT8_CLASS, mxREAL);
      i8_medfilt2(mxGetData(prhs[0]), mxGetData(plhs[0]), filt_sizes, dims);
      break;
    }
    case mxUINT8_CLASS: {
      plhs[0] = mxCreateNumericMatrix(dims[1], dims[0], mxUINT8_CLASS, mxREAL);
      u8_medfilt2(mxGetData(prhs[0]), mxGetData(plhs[0]), filt_sizes, dims);
      break;
    }
    case mxINT16_CLASS: {
      plhs[0] = mxCreateNumericMatrix(dims[1], dims[0], mxINT16_CLASS, mxREAL);
      i16_medfilt2(mxGetData(prhs[0]), mxGetData(plhs[0]), filt_sizes, dims);
      break;
    }
    case mxUINT16_CLASS: {
      plhs[0] = mxCreateNumericMatrix(dims[1], dims[0], mxUINT16_CLASS, mxREAL);
      u16_medfilt2(mxGetData(prhs[0]), mxGetData(plhs[0]), filt_sizes, dims);
      break;
    }
    case mxINT32_CLASS: {
      plhs[0] = mxCreateNumericMatrix(dims[1], dims[0], mxINT32_CLASS, mxREAL);
      i32_medfilt2(mxGetData(prhs[0]), mxGetData(plhs[0]), filt_sizes, dims);
      break;
    }
    case mxUINT32_CLASS: {
      plhs[0] = mxCreateNumericMatrix(dims[1], dims[0], mxUINT32_CLASS, mxREAL);
      u32_medfilt2(mxGetData(prhs[0]), mxGetData(plhs[0]), filt_sizes, dims);
      break;
    }
    case mxINT64_CLASS: {
      plhs[0] = mxCreateNumericMatrix(dims[1], dims[0], mxINT64_CLASS, mxREAL);
      i64_medfilt2(mxGetData(prhs[0]), mxGetData(plhs[0]), filt_sizes, dims);
      break;
    }
    case mxUINT64_CLASS: {
      plhs[0] = mxCreateNumericMatrix(dims[1], dims[0], mxUINT64_CLASS, mxREAL);
      u64_medfilt2(mxGetData(prhs[0]), mxGetData(plhs[0]), filt_sizes, dims);
      break;
    }
    default:
      break;
  }
}

/* WHAT FOLLOWS HAS BEEN IMPORTED COMPLETELY FROM PYMCA.
 * The only difference is just in the names of some functions */

/* This is a shameless copy with minor mofifications of the medianfilter
 * provided with scipy. Therefore is distributed under the terms of the
 * scipy license.
 *
 * The purpose of having it separately is not to introduce a dependency
 * on scipy that is big and potentially difficult to built on some
 * platforms.
 *
 * Using this code outside PyMca:
 *
 * The check_malloc function has to be provided for error handling.
 *
 *--------------------------------------------------------------------*/

/* The QUICK_SELECT routine is based on Hoare's Quickselect algorithm,
 * with unrolled recursion.
 * Author: Thouis R. Jones, 2008
 */

#define ELEM_SWAP(t, a, x, y) {register t temp = (a)[x]; (a)[x] = (a)[y]; (a)[y] = temp;}
#define FIRST_LOWEST(x, y, z) (((x) < (y)) && ((x) < (z)))
#define FIRST_HIGHEST(x, y, z) (((x) > (y)) && ((x) > (z)))
#define LOWEST_IDX(a, x, y) (((a)[x] < (a)[y]) ? (x) : (y))
#define HIGHEST_IDX(a, x, y) (((a)[x] > (a)[y]) ? (x) : (y))

/* if (l is index of lowest) {return lower of mid,hi} else if (l is index of highest) {return higher of mid,hi} else return l */
#define MEDIAN_IDX(a, l, m, h) (FIRST_LOWEST((a)[l], (a)[m], (a)[h]) ? LOWEST_IDX(a, m, h) : (FIRST_HIGHEST((a)[l], (a)[m], (a)[h]) ? HIGHEST_IDX(a, m, h) : (l)))

#define QUICK_SELECT(NAME, TYPE)                                        \
TYPE NAME(TYPE arr[], int n)                                            \
{                                                                       \
    int lo, hi, mid, md;                                                \
    int median_idx;                                                     \
    int ll, hh;                                                         \
    TYPE piv;                                                           \
                                                                        \
    lo = 0; hi = n-1;                                                   \
    median_idx = (n - 1) / 2; /* lower of middle values for even-length arrays */ \
                                                                        \
    while (1) {                                                         \
        if ((hi - lo) < 2) {                                            \
            if (arr[hi] < arr[lo]) ELEM_SWAP(TYPE, arr, lo, hi);        \
            return arr[median_idx];                                     \
        }                                                               \
                                                                        \
        mid = (hi + lo) / 2;                                            \
        /* put the median of lo,mid,hi at position lo - this will be the pivot */ \
        md = MEDIAN_IDX(arr, lo, mid, hi);                              \
        ELEM_SWAP(TYPE, arr, lo, md);                                   \
                                                                        \
        /* Nibble from each end towards middle, swapping misordered items */ \
        piv = arr[lo];                                                  \
        for (ll = lo+1, hh = hi;; ll++, hh--) {                         \
        while (arr[ll] < piv) ll++;                                     \
        while (arr[hh] > piv) hh--;                                     \
        if (hh < ll) break;                                             \
        ELEM_SWAP(TYPE, arr, ll, hh);                                   \
        }                                                               \
        /* move pivot to top of lower partition */                      \
        ELEM_SWAP(TYPE, arr, hh, lo);                                   \
        /* set lo, hi for new range to search */                        \
        if (hh < median_idx) /* search upper partition */               \
            lo = hh+1;                                                  \
        else if (hh > median_idx) /* search lower partition */          \
            hi = hh-1;                                                  \
        else                                                            \
            return piv;                                                 \
    }                                                                   \
}


/* 2-D median filter with zero-padding on edges. */
#define MEDIAN_FILTER_2D(NAME, TYPE, SELECT)                            \
void NAME(TYPE* in, TYPE* out, int* Nwin, int* Ns)                      \
{                                                                       \
    int nx, ny, hN[2];                                                  \
    int pre_x, pre_y, pos_x, pos_y;                                     \
    int subx, suby, k, totN;                                            \
    TYPE *myvals, *fptr1, *fptr2, *ptr1, *ptr2;                         \
                                                                        \
    totN = Nwin[0] * Nwin[1];                                           \
    myvals = (TYPE *) mxMalloc( totN * sizeof(TYPE));                   \
                                                                        \
    hN[0] = Nwin[0] >> 1;                                               \
    hN[1] = Nwin[1] >> 1;                                               \
    ptr1 = in;                                                          \
    fptr1 = out;                                                        \
    for (ny = 0; ny < Ns[0]; ny++)                                      \
        for (nx = 0; nx < Ns[1]; nx++) {                                \
            pre_x = hN[1];                                              \
            pre_y = hN[0];                                              \
            pos_x = hN[1];                                              \
            pos_y = hN[0];                                              \
            if (nx < hN[1]) pre_x = nx;                                 \
            if (nx >= Ns[1] - hN[1]) pos_x = Ns[1] - nx - 1;            \
            if (ny < hN[0]) pre_y = ny;                                 \
            if (ny >= Ns[0] - hN[0]) pos_y = Ns[0] - ny - 1;            \
            fptr2 = myvals;                                             \
            ptr2 = ptr1 - pre_x - pre_y*Ns[1];                          \
            for (suby = -pre_y; suby <= pos_y; suby++) {                \
                for (subx = -pre_x; subx <= pos_x; subx++)              \
                    *fptr2++ = *ptr2++;                                 \
                ptr2 += Ns[1] - (pre_x + pos_x + 1);                    \
            }                                                           \
            ptr1++;                                                     \
                                                                        \
            k = (pre_x + pos_x + 1)*(pre_y + pos_y + 1);                \
            /* Prefer a shrinking window to zero padding */             \
            if (k > totN){                                              \
                k = totN;                                               \
            }                                                           \
            *fptr1++ = SELECT(myvals, k);                               \
            /* Zero pad alternative*/                                   \
            /*for ( ; k < totN; k++)                                    \
                *fptr2++ = 0;                                           \
                                                                        \
            *fptr1++ = SELECT(myvals,totN); */                          \
        }                                                               \
    mxFree(myvals);                                                     \
}

QUICK_SELECT(f_quick_select, float)
QUICK_SELECT(d_quick_select, double)
QUICK_SELECT(i8_quick_select, signed char)
QUICK_SELECT(u8_quick_select, unsigned char)
QUICK_SELECT(i16_quick_select, signed short int)
QUICK_SELECT(u16_quick_select, unsigned short int)
QUICK_SELECT(i32_quick_select, signed int)
QUICK_SELECT(u32_quick_select, unsigned int)
QUICK_SELECT(i64_quick_select, signed long int)
QUICK_SELECT(u64_quick_select, unsigned long int)

MEDIAN_FILTER_2D(f_medfilt2, float, f_quick_select)
MEDIAN_FILTER_2D(d_medfilt2, double, d_quick_select)
MEDIAN_FILTER_2D(i8_medfilt2, signed char, i8_quick_select)
MEDIAN_FILTER_2D(u8_medfilt2, unsigned char, u8_quick_select)
MEDIAN_FILTER_2D(i16_medfilt2, signed short int, i16_quick_select)
MEDIAN_FILTER_2D(u16_medfilt2, unsigned short int, u16_quick_select)
MEDIAN_FILTER_2D(i32_medfilt2, signed int, i32_quick_select)
MEDIAN_FILTER_2D(u32_medfilt2, unsigned int, u32_quick_select)
MEDIAN_FILTER_2D(i64_medfilt2, signed long int, i64_quick_select)
MEDIAN_FILTER_2D(u64_medfilt2, unsigned long int, u64_quick_select)
