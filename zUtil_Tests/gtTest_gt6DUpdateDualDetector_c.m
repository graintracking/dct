function gtTest_gt6DUpdateDualDetector_c()
    fprintf('Test function: %s\n', mfilename)

    try
        num_threads = feature('NumThreads');    
    catch
        num_threads = 2;
    end

    num_vols = 100;
    size_vols = [50, 50, 50];

    test_dual = cell(num_vols, 1);
    test_blobs = cell(num_vols, 1);
    test_pblobs = cell(num_vols, 1);
    test_sigma = cell(num_vols, 1);
    for ii = 1:num_vols
        test_dual{ii} = rand(size_vols, 'single');
        test_blobs{ii} = rand(size_vols, 'single');
        test_pblobs{ii} = rand(size_vols, 'single');
        test_sigma{ii} = rand(size_vols, 'single');
    end

    test_dual_correct = gtCxxMathsCellCopy(test_dual);

    test_dual = gt6DUpdateDualDetector_c(test_dual, test_blobs, test_pblobs, test_sigma, test_sigma, num_threads);

    for n = 1:numel(test_dual)
        test_dual_correct{n} = test_dual_correct{n} + test_sigma{n} .* (test_pblobs{n} - test_blobs{n});
        test_dual_correct{n} = test_dual_correct{n} .* test_sigma{n};
    end

    GtBenchmarks.check_errors(test_dual_correct, test_dual);
end
