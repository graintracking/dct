function gtTest_gt6DUpdateDualL1_c()
    fprintf('Test function: %s\n', mfilename)

    try
        num_threads = feature('NumThreads');    
    catch
        num_threads = 2;
    end

    num_vols = 100;
    size_vols = [50, 50, 50];

    test_dual = cell(num_vols, 1);
    test_dsES = cell(num_vols, 1);
    for ii = 1:num_vols
        test_dual{ii} = rand(size_vols, 'single');
        test_dsES{ii} = rand(size_vols, 'single');
    end

    test_dual_correct = gtCxxMathsCellCopy(test_dual);

    test_dual = gt6DUpdateDualL1_c(test_dual, test_dsES, 1, num_threads);

    for n = 1:numel(test_dual_correct)
        test_dual_correct{n} = test_dual_correct{n} + 1 * test_dsES{n};
        test_dual_correct{n} = test_dual_correct{n} ./ max(1, abs(test_dual_correct{n}));
    end

    GtBenchmarks.check_errors(test_dual_correct, test_dual);
end