function gtTest_gt6DUpdatePrimal_c()
    fprintf('Test function: %s\n', mfilename)

    try
        num_threads = feature('NumThreads');    
    catch
        num_threads = 2;
    end

    size_vols = [50, 50, 50];

    test_primal = rand(size_vols, 'single');
    test_ehn = rand(size_vols, 'single');
    test_correction = rand(size_vols, 'single');

    tau = 1;
    theta = 1;

    test_primal_correct = gtCxxMathsCellCopy({test_primal});
    test_primal_correct = test_primal_correct{1};

    [test_primal, test_ehn] = gt6DUpdatePrimal_c(test_primal, test_ehn, test_correction, tau, num_threads);

    v = test_primal_correct + test_correction .* tau;
    v(v < 0) = 0;
    test_ehn_correct = v + theta .* (v - test_primal_correct);
    test_primal_correct = v;

    GtBenchmarks.check_errors(test_primal_correct, test_primal);
    GtBenchmarks.check_errors(test_ehn_correct, test_ehn);
end
