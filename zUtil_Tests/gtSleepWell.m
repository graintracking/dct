function gtSleepWell(first, last, sleep_time)
%% function gtSleepWell(first, last, sleep_time)
% test function for transition from OAR to SLURM
% This function simply pauses (last - first) + 1 times for sleep_time and
% prints which sleeping task it is going to handle...
    if (isdeployed)
        global GT_DB %#ok<TLEV,NUSED>
        global GT_MATLAB_HOME %#ok<NUSED,TLEV>
        load('workspaceGlobal.mat');
        first= str2double(first);
        last  = str2double(last);
        sleep_time = str2double(sleep_time);
    end

    for ii = first : last
        fprintf('I am now going to handle task number %d and sleep for %d seconds\n', ii, sleep_time)
        pause(sleep_time);
    end
end
