function snaked=gtSnakeBW(im, varargin)

%pass in snakeoptions, else use defaults
snakeoptions=[];
if ~isempty(varargin)
  snakeoptions=varargin{1};
else
  snakeoptions.elasticity=.5; %3;
  snakeoptions.rigidity=15;  % keep this high to prevent spurs from local features
  snakeoptions.viscosity=5;
  snakeoptions.forcefactor=5;
  snakeoptions.iterations=5;
end


  val=max(im(:));  % store input image value

  im(im==val)=1;
  % polarity of image not important because only magnitude of gradient is used
  [fx,fy]=gradient(im);
  fmag=sqrt(fx.^2+fy.^2);

  % use a small part of the image to find the starting snake
  [roffset,coffset]=find(im);
  if length(roffset) > 10  % need AT LEAST 10 points to be reasonable
    % TODO should this be replace with a convex hull? might be quicker?
    [x_orig,y_orig]=gtMask2Poly(...
      im(...
      min(roffset):max(roffset),...
      min(coffset):max(coffset)));

    % adjust back to original coordinates
    x_orig=x_orig+min(coffset)-1;
    y_orig=y_orig+min(roffset)-1;

    x=x_orig;y=y_orig;


    %%%%%%%%%%%%%%
    % make some measures of the spot we are considering:
    defaultmaskarea=29000;
    maskarea=numel(im);
    arearatio=maskarea/defaultmaskarea;
    %fprintf('Area ratio: %3.6f\n',arearatio);

    defaultmaskperim=575;
    tmp=regionprops(double(im),'perimeter');
    maskperim=round(tmp.Perimeter);
    perimratio=maskperim/defaultmaskperim;
    %fprintf('Perimeter ratio: %3.2f\n',perimratio);


    % snakeoptions.elasticity=snakeoptions.elasticity*arearatio.^2;
    %    snakeoptions.forcefactor=snakeoptions.forcefactor*perimratio;
    snakeoptions;

    %%%%%%%%%%%%%%%

    pad=25;
   % tmp_bbox=round([min(x(:))-pad min(y(:))-pad gtRange(x)+(2*pad) gtRange(y)+(2*pad)]);
   tmp_bbox=[1 1 size(im,2) size(im,1)];
   
   h_gvf=figure(1);
   % clf
   imagesc(gtCrop(im,tmp_bbox))
    hold on
    axis image

    %    keyboard
    [u,v] = GVF(gtExtract(fmag,tmp_bbox), 0.05, 15);  % 0.2,80 % 0.3 15
    % Normalizing the GVF external force
    mag = sqrt(u.^2+v.^2);
    pxnorm = u./(mag+1e-10);pynorm = v./(mag+1e-10);

    forcefield.x=pxnorm;
    forcefield.y=pynorm;

    x=x-(tmp_bbox(1));
    y=y-(tmp_bbox(2));




    [x,y]=snakeinterp(x,y,20,1);
    origlength=length(x);
    clear hp
    converged=false;
    snakecollapse=false;
    sfConverged([]); % reset sfConverged
    i=0;
    while not(converged)
      i=i+1;
      if i>50 % *** CHANGED
        break % get out of loop - we've taken too long
      end
      if length(x)<origlength/2
        %      disp('Snake is too short!')
        % break
      end
      if length(x)<10
        disp('Snake has collapsed')
        snakecollapse=true;
        break
      end




      [x2,y2]=snakedeform(x,y,forcefield,snakeoptions);
      tmp=poly2mask(x,y,round(max(x)+5),round(max(y)+5));
      area=sum(tmp(:));
      converged=sfConverged(area);
      if length(x2)<15 % snake has collapsed
        disp('Snake has collapsed')
        snakecollapse=true;
        break
      end
      if false

        if exist('hp','var')
          delete(hp)
        end
        hp=snakedisp(x,y,'g.-');
        axis image
        title(['Deformation in progress,  iter = ' num2str(i*snakeoptions.iterations)])
 %       drawnow

        % print('-dtiff',sprintf('perrin_%03d.tif',i));
      end

      [x,y]=snakeinterp(x2,y2,20,1);

    end
    if converged
      disp('Converged')
    else
      disp('Did NOT converge')
    end


    figure(h_gvf);

    snakedisp(x+tmp_bbox(1)-1,y+tmp_bbox(2)-1,'g.-');

    [xgrid,ygrid]=meshgrid(1:size(im,2),1:size(im,1));
    snaked=inpolygon(xgrid,ygrid,x+(tmp_bbox(1))-1,y+(tmp_bbox(2))-1);


    snaked_area=regionprops(double(snaked),'area');
    im_area=regionprops(double(im),'area');
    try
      if 0 %snaked_area.Area>2*im_area.Area
        disp('********PROBABLY A BAD SNAKE')
        snaked=im;
      end
    catch
      snaked=im;
    end

    if snakecollapse
      snaked=im;
    end
  else
    % not enough points to be sensible
    snaked=zeros(size(im));
  end
  % restore value used in input image
  snaked=double(snaked)*val;
%waitforbuttonpress

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  function flag=sfConverged(data1)
    persistent diffdata data h_history
    if isempty(data1)
      clear data diffdata h_history
      return
    end
    if numel(data)==0
      data=[data1];
      flag=false;
      %   disp('*******************************************STARTING')

    else
      data=[data;data1];
      diffdata=diff(data);

      if abs(diffdata(end))<1
        flag=true;
      else
        flag=false;
      end

    end
  end
end
