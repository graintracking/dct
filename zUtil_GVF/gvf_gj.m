% EXAMPLE     GVF snake examples on two simulated object boundaries.
%
% NOTE: 
% 
% traditional snake and distance snake differed from GVF snake only 
%   by using different external force field. In order to produce the
%   corresponding external force field, use the following (all
%   assuming edge map f is in memory).
%
% traditional snake:
%   f0 = gaussianBlur(f,1);
%   [px,py] = gradient(f0);
%
% distance snake:
%   D = dt(f>0.5);  % distance transform (dt) require binary edge map
%   [px,py] = gradient(-D);
%
% [px,py] is the external force field in both cases
%
% balloon model using a different matlab function "snakedeform2"
% instead of "snakedeform". The external force could be any force
% field generated above.
%
% an example of using it is as follows
%       [x,y] = snakedeform2(x,y, alpha, beta, gamma, kappa, kappap, px,py,2);
% do a "help snakedeform2" for more details
% 

  
   if 1
     [I,map] = rawread('/data/id19/archive/matlab/greg/gvf_dist_v4.2c/images/U64.pgm'); 
      f = 1 - I/255; 
   else
     I=ones(64)*256;
     I(20:50,20:50)=1;
     I(22:48,22:48)=256;
     I=double(imread('test.tif'));
     I=I(50:220,210:330);
     I=I-min(I(:));
     I=I./max(I(:));
     I=I(1:1:end,1:1:end);
     blur=gaussianBlur(I,0.8);
     clf
     [e,thresh]=edge(I,'canny',0.1,3);
     thresh
     figure(2)
     colormap(gray)
     e=imfill(e,'holes');
     q=imdilate(imerode(e,strel(ones(2))),strel(ones(2)));
     e=bwperim(q);
     imagesc(e)
     f=double(e);
     drawnow
     keyboard
   end
     
   % Compute the GVF of the edge map f
     disp(' Compute GVF ...');
     [u,v] = GVF(f, 0.2, 80);  % 0.2,80
%     [u2,v2]=GVF(f,0.2,40);
     disp(' Nomalizing the GVF external force ...');
     mag = sqrt(u.*u+v.*v);
     px = u./(mag+1e-10); py = v./(mag+1e-10); 

     % display the gradient of the edge map
     [fx,fy] = gradient(f); 
%     subplot(223); quiver(fx,fy); 
%     axis off; axis equal; axis 'ij';     % fix the axis 
%     title('edge map gradient');

     % display the GVF 
     figure(3); quiver(px,py);
%     axis off; axis equal; axis 'ij';     % fix the axis 
 %    title('normalized GVF field');
%%
   % snake deformation
     figure(1); clf
     colormap(gray); 
     a=imagesc(I);
     set(a,'alphadata',0.5)
     hold on
     b=imagesc(e);
     set(b,'alphadata',0.5);
     t = 0:0.05:6.28;
     t=0:0.5:2*pi;
     x = 32 + 30*cos(t);
     y = 32 + 30*sin(t);
     borderx=0.1*size(I,2);
     bordery=0.1*size(I,1);
     x=[borderx size(I,2)-borderx size(I,2)-borderx borderx];
     y=[bordery bordery size(I,1)-bordery size(I,1)-bordery];
     
     [x,y] = snakeinterp(x,y,20,0.5);
     snakedisp(x,y,'r.-') 

pause(1);

     elastic=0.15;
     rigid=0.01;
     viscous=0.3;
     force=.3;
     iter=5;
     for i=1:25,
       [x,y] = snakedeform(x,y,elastic,rigid,viscous,force,px,py,iter);
       [x,y] = snakeinterp(x,y,5,0.5);
       snakedisp(x,y,'r.-') 
       title(['Deformation in progress,  iter = ' num2str(i*iter)])
       drawnow
     end
pause(1)
clf
     colormap(gray); 
     a=imagesc(I);
     hold on
     b=imagesc(e);
     set(b,'alphadata',0.5)
     snakedisp(x,y,'g.-')
