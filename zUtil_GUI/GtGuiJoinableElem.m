classdef GtGuiJoinableElem < GtBaseGuiElem
% Class GtGuiJoinableElem: base class for joinable graphical user interfaces. It
% is useful in case you want to build a GUI object that should return a value.
%
% It makes the object oriented GUI implementations equivalent to function
% implementations.
%
% Working with it is simple: you first instanciate your object, and then you
% expect a result from the call to the join() method.
%
% >> objHandle = DerivedClass(blabla);
% >> expectedResult = objHandle.join();
%
    properties
        jointRetval = [];
        jointIsSet = false;
        isJoint = false;
    end

    methods (Access = public)
        function obj = GtGuiJoinableElem()
            obj = obj@GtBaseGuiElem();
        end

        function retval = join(obj)
            savedInitialVal = obj.jointRetval;
            obj.isJoint = true;
            try
                while(~obj.jointIsSet)
                    pause(0.0156);
                end
                retval = obj.jointRetval;
                delete(obj);
            catch mexc
                retval = savedInitialVal;

                % If the exception is of that type, we don't care, because it
                % simply means that the object was deleted.
                if (~gtCheckExceptionType(mexc, 'MATLAB:class:InvalidHandle'))
                    gtPrintException(mexc);
                    delete(obj);
                end
            end
        end
    end

    methods (Access = protected)
        function setJointReturnVal(obj, val)
            obj.jointRetval = val;
            obj.jointIsSet = true;
        end
    end
end
