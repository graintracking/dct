function selected = gtGuiGrainMontage(gr, selected, use_blobs, det_ind, is_topotomo)
% GUIGTMONTAGE
%     selected = gtGuiGrainMontage(gr, selected, use_blobs, det_ind)
%     --------------------------------------------------------------------------
%

    if (~exist('selected', 'var'))
        selected = [];
    end

    if (~exist('use_blobs', 'var') || isempty(use_blobs))
        use_blobs = false;
    end

    if (~exist('det_ind', 'var'))
        det_ind = 1;
    end

    if (~exist('is_topotomo', 'var'))
        is_topotomo = 0;
    end

    % Init the GUI
    gui_data = gtGuiGrainMontage_init(gr, selected, use_blobs, det_ind);

    while (1)
        % Wait for the application to close
        if (gui_data.close), break; end
        pause(0.001);
    end

    % Save results
    selected = gui_data.selected;

    % Close the figure
    delete( gui_data.window );

    % End of function

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Private functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    function gui_data = gtGuiGrainMontage_init(gr, selected, use_blobs, det_ind)

        gui_data = [];

        % Coping with different versions of FwdSim
        if (isempty(selected))
            if (isfield(gr.proj(det_ind), 'selected'))
                selected = gr.proj(det_ind).selected;
            else
                selected = gr.selected;
            end
        end
        if (isfield(gr.proj(det_ind), 'included'))
            included = gr.proj(det_ind).included;
            ondet = gr.proj(det_ind).ondet;
        else
            included = gr.included;
            ondet = gr.ondet;
        end
        if (use_blobs && ~isfield(gr.proj(det_ind), 'bl'))
            gr.proj(det_ind).bl = gr.bl;
        end
        if (isfield(gr, 'fwd_sim') && ~is_topotomo)
            spot_ids = gr.fwd_sim(det_ind).spotid(included);
            intensities = gr.fwd_sim(det_ind).intensity;
        else
            if (isfield(gr, 'spotid') && ~is_topotomo)
                spot_ids = gr.spotid(included);
            else
                spot_ids = zeros(numel(included), 1);
            end
            if (isfield(gr, 'intensity'))
                intensities = gr.intensity;
            else
                intensities = zeros(numel(included), 1);
            end
        end
        if is_topotomo
            spot_ids = [1 : numel(included)];
            intensities = zeros(numel(included), 1);
        end
        % Condition to exit from the gui
        gui_data.close = false;

        if (use_blobs)
            title = sprintf('Blobs of grain %d in phase %d', gr.id, gr.phaseid);
        else
            title = sprintf('Spots of grain %d in phase %d', gr.id, gr.phaseid);
        end

        % The figure
        gui_data.window = figure( ...
                'Name', title, ...
                'NumberTitle', 'off', ...
                'MenuBar', 'none', ...
                'Toolbar', 'none', ...
                'HandleVisibility', 'off' );

        % Boxes to help with the formatting of the window
        gui_data.main_boxes = uiextras.VBox('Parent', gui_data.window);

        % Will contain the images
        gui_data.imgs_panel = uiextras.Panel('Parent', gui_data.main_boxes);

        % Will contain the buttons
        gui_data.buttons_panel = uiextras.Panel('Parent', gui_data.main_boxes);

        set(gui_data.main_boxes, 'Sizes', [-1 40]);

        % Layout for the buttons
        gui_data.butt_boxes = uiextras.HButtonBox('Parent', gui_data.buttons_panel);

        gui_data.guiOkButton = uicontrol( 'Style', 'PushButton', ...
                                         'Parent', gui_data.butt_boxes, ...
                                         'String', 'Ok', ...
                                         'Callback', @onOkButton );
        gui_data.guiCancelButton = uicontrol( 'Style', 'PushButton', ...
                                             'Parent', gui_data.butt_boxes, ...
                                             'String', 'Cancel', ...
                                             'Callback', @onCancelButton );
        gui_data.guiResetButton = uicontrol( 'Style', 'PushButton', ...
                                            'Parent', gui_data.butt_boxes, ...
                                            'String', 'Reset', ...
                                            'Callback', @onResetButton );

        % Grid that holds the images
        gui_data.imgs_grid = uiextras.Grid('Parent', gui_data.imgs_panel);

        % I expect a matrix like this stack(:,:,1,n) which will have n images.
        % For each of these images, we will display them in proper boxes

        %%% Kind of a hack - presetting the values to zeros, forces the handles
        %%% to be doubles, which then forces the use of the
        %%% hg.uicontrol/{get,set} methods instead of the overloaded uiextras.
        %%% This makes it possible to assign the 'ButtonDownFcn' callback on the
        %%% panel, while with the uiextras/{get,set} would not be accessible
        gui_data.listOfPlainPanels = zeros(length(selected));

        drawnow();

        %%% Let's now get to the real deal :)

        % Let's store the selected images vector in input
        gui_data.selected = selected;

        omegas_th = gr.allblobs(det_ind).omega(ondet(included));

        % hack to make it display the images: may be a matlab bug
        % related to flushing
        fig = figure();
        ax = axes('parent', fig);

        % Creates the panels containing the images
        % Sorting by Omegas
        [~, order] = sort(omegas_th);

        for ii = 1:length(selected)
            index = order(ii);

            if (use_blobs)
                img = sum(gr.proj(det_ind).bl(index).intm, 3)';
            else
                img = squeeze(gr.proj(det_ind).stack(:, index, :))'; % - gr.proj(det_ind).sim_stack(:, index, :))';
            end

            % hack to make it display the images: may be a matlab bug
            % related to flushing
            if (ii == 1)
                imagesc(img, 'parent', ax);
            end

            gui_data = createDiffSpotPanel(gui_data, img, selected(index), ...
                index, spot_ids(index), omegas_th(index), intensities(index));
        end

        % hack to make it display the images: may be a matlab bug
        % related to flushing
        delete(fig);

        % Rescale the panels to fit as much as possible (and to be as square as
        % possible)
        resizeImgs(0,0,gui_data);

        drawnow;

        % Callbacks for the figure
        set(gui_data.window, 'ResizeFcn', {@resizeImgs, gui_data});
        set(gui_data.window, 'CloseRequestFcn', @onCancelButton);
    end

    function onOkButton(~, ~)
        % Let's read the status of the panels
        for ii = 1:length(gui_data.selected)
            panel = gui_data.listOfPanels{ii};

            colorVec = get(panel, 'TitleColor');
            % Encoding of green color in matlab
            greenVec = [0 1 0];
            result = xor(colorVec, greenVec);

            % If result contains just zeros, then it is green
            gui_data.selected(ii) = isempty(find(result == 1, 1));
        end
        gui_data.close = true;
    end

    function onCancelButton(~, ~)
        gui_data.close = true;
    end

    function onResetButton(~, ~)
        for ii = 1:length(gui_data.selected)
            panel = gui_data.listOfPanels{ii};

            if (gui_data.selected(ii)), set(panel, 'TitleColor', 'g');
            else set(panel, 'TitleColor', 'y'); end
        end
    end

    function gui_data = createDiffSpotPanel(gui, img, active, ii, num, omega, intensity)
    % It creates the panel that contains the difspot.
        text = sprintf('w: %2.1f, I: %2.1g, Id: %d', omega, intensity, num);
        panel = uiextras.BoxPanel('Parent', gui.imgs_grid, 'Title', text);

        if (active), set(panel, 'TitleColor', 'g');
        else set(panel, 'TitleColor', 'y'); end

        axes1 = axes( 'Parent', panel, 'ActivePositionProperty', 'Position');
        colormap(axes1, 'gray');
        image1 = imagesc(img, 'Parent', axes1);
        axis(axes1, 'off');
        axis(axes1, 'image');

        % Let's save the handles in double copy (panel)
        gui.listOfPanels{ii} = panel;
        gui.listOfPlainPanels(ii) = panel;
        gui.listOfAxes{ii} = axes1;
        gui_data = gui;

        % Callbacks to change the state of the panels selected/unselected
        set(gui.listOfPlainPanels(ii), 'ButtonDownFcn', {@togleCheck, ii});
        set(gui.listOfAxes{ii}, 'ButtonDownFcn', {@togleCheck, ii});
        set(image1, 'ButtonDownFcn', {@togleCheck, ii});
    end

    function resizeImgs(~, ~, gui_data)
    % Rescale the panels to fit as much as possible (and to be as square as
    % possible)

        num_panels = numel(gui_data.listOfAxes);
        if (num_panels > 1)
            % This position is relative to the figure position.
            set(gui_data.imgs_panel, 'Units', 'pixels');
            pos = get(gui_data.imgs_panel, 'Position');
            ratio = pos(3)/pos(4);

            numInRow = floor(sqrt(num_panels/ratio));
            numInColumn = ceil(sqrt(num_panels/ratio));

            set( gui_data.imgs_grid, 'ColumnSizes', -1*ones(numInColumn,1), ...
                 'RowSizes', -1*ones(numInRow,1) );
        end
    end

    function togleCheck(~, ~, num)
        panel = gui_data.listOfPanels{num};

        % Get's the color
        colorVector = get( panel, 'TitleColor');
        % Encoding of green color in matlab
        greenVec = [0 1 0];

        result = xor(colorVector, greenVec);

        % If result contains just zeros, then it is green
        if (isempty(find(result == 1, 1)))
            set( panel, 'TitleColor', 'y');
        else
            set( panel, 'TitleColor', 'g');
        end
    end
end
