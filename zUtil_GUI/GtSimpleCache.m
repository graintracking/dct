classdef GtSimpleCache < handle
% Extremely Simple cache class for mat files.
% WARNING: Totally not-coherent and for just read-only applications.
% The cache model can be only addressable by integer indexes.
% The path model is an 'fprintf-like' model
%
% TODO: Add complete objects support.
% TODO: Add whole model flush (single object flush is already in).
    properties
        workspace;
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Public object API - Interaction with outside
    methods (Access = public)
        function createModel(obj, modelPath, modelName, modelFlushPolicy, modelRefreshPolicy)
        % GTSIMPLECACHE/CREATEMODEL Creates a caching model. Input:
        %  - modelPath: fprintf-like path which will map the files.
        %  - modelName: name to identify the model.
        %  - modelFlushPolicy [optional, default = 'error']: Gives the chance to
        %       have different behavior in case of timestamp conflict. For
        %       instance if a file was modified from the last time we read it!
        %       Possible choices are: 'error' and 'overwrite'
        %  - modelRefreshPolicy [optional, default = 'never']: Controls refresh
        %       of files, so that files do not get appended too much.
        %       Available options: 'never', 'always'

            if (~exist('modelFlushPolicy', 'var'))
                modelFlushPolicy = 'error';
            elseif (~ismember(modelFlushPolicy, {'error', 'overwrite'}))
                warning('CACHE:wrong_argument', ...
                        'The input policy (%s) is not valid. Forcing "error".', ...
                        modelFlushPolicy)
                modelFlushPolicy = 'error';
            end

            if (~exist('modelRefreshPolicy', 'var'))
                modelRefreshPolicy = 'never';
            elseif (~ismember(modelRefreshPolicy, {'never', 'always'}))
                warning('CACHE:wrong_argument', ...
                        'The input policy (%s) is not valid. Forcing "never".', ...
                        modelRefreshPolicy)
                modelRefreshPolicy = 'never';
            end

            obj.workspace.(modelName).modelPath = modelPath;
            obj.workspace.(modelName).dimsNum = length(regexp(modelPath, '%(.*?)d'));
            obj.workspace.(modelName).data = {};
            obj.workspace.(modelName).timestamp = {};
            obj.workspace.(modelName).flushPolicy = modelFlushPolicy;
            obj.workspace.(modelName).refreshPolicy = modelRefreshPolicy;
        end

        function modifyModelPath(obj, modelName, modelPath)
            obj.workspace.(modelName).modelPath = modelPath;
            obj.workspace.(modelName).data = {};
            obj.workspace.(modelName).timestamp = {};
        end

        function data = get(obj, modelName, indexes, field)
            obj.checkModelName(modelName);
            indexes = obj.checkIndexes(modelName, indexes);

            if (~exist('field', 'var'))
                fields = obj.getFieldNames(modelName, indexes);
                data = obj.getFields(modelName, indexes, fields);
            elseif (iscellstr(field))
                data = obj.getFields(modelName, indexes, field);
            else
                data = obj.getField(modelName, indexes, field);
            end
        end

        function set(obj, modelName, indexes, data, field)
            obj.checkModelName(modelName);
            indexes = obj.checkIndexes(modelName, indexes);

            if (exist('field', 'var'))
                obj.setField(modelName, indexes, field, data);
            else
                error('CACHE:not_implemented_yet', ...
                      'Caching of entire objects is not supported, yet.');
            end
        end

        function flush(obj, modelName)
            error('CACHE:not_implemented_yet', ...
                  'Cache flushing is not supported, yet.');
        end

        function flushObjectField(obj, modelName, indexes, field)
            obj.checkModelName(modelName);
            indexes = obj.checkIndexes(modelName, indexes);

            obj.flushSingleField(modelName, indexes, field);
        end

        function is_model = isModel(obj, modelName)
            is_model = isfield(obj.workspace, modelName);
        end
    end

    methods (Access = private)
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Error Check Functions
        function checkModelName(obj, modelName)
            if (~obj.isModel(modelName))
                error('CACHE:no_such_model', ...
                      'Model "%s" doesn''t exist.', modelName);
            end
        end

        function indexes = checkIndexes(obj, modelName, indexes)
            if (~iscell(indexes))
                error('CACHE:wrong_argument', ...
                      'Expected cell array containing indices');
            end
            numDims = obj.workspace.(modelName).dimsNum;
            if (numDims ~= length(indexes))
                error('CACHE:wrong_argument', ...
                      'Expected %d indices, but got %d', numDims, length(indexes));
            end
            if (numDims == 0)
                % Otherwise in this case indexes are reported wrongly
                indexes = {1};
            end
        end

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Cache Conditions Check Functions
        function available = isAvailable(obj, modelName, indexes)
            available = (all(size(obj.workspace.(modelName).data) >= [indexes{:}]) ...
                    && ~isempty(obj.workspace.(modelName).data{indexes{:}}));
        end

        function available = isAvailableField(obj, modelName, indexes, field)
            available = (obj.isAvailable(modelName, indexes) ...
                    && isfield(obj.workspace.(modelName).data{indexes{:}}, field));
        end
        function available = isAvailableTstamp(obj, modelName, indexes)
            available = (all(size(obj.workspace.(modelName).timestamp) >= [indexes{:}]) ...
                    && ~isempty(obj.workspace.(modelName).timestamp{indexes{:}}));
        end

        function available = isAvailableTstampField(obj, modelName, indexes, field)
            available = (obj.isAvailableTstamp(modelName, indexes) ...
                    && isfield(obj.workspace.(modelName).timestamp{indexes{:}}, field));
        end

        function upToDate = isUpToDate(obj, modelName, indexes, field)
            datenum = obj.getFileDatenum(modelName, indexes);
            if (~exist('field', 'var'))
                upToDate = (datenum == obj.getTstamp(modelName, indexes));
            else
                upToDate = (isfield(obj.workspace.(modelName).timestamp{indexes{:}}, field) ...
                        && (datenum == obj.getTstamp(modelName, indexes, field)));
            end
        end

        function ensureTstampPresence(obj, modelName, indexes, field)
            if (~exist('field', 'var'))
                if (~obj.isAvailableTstamp(modelName, indexes))
                    obj.setTstamp(modelName, indexes, 0)
                end
            else
                if (~obj.isAvailableTstampField(modelName, indexes, field))
                    obj.setTstamp(modelName, indexes, 0, field)
                end
            end
        end

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Shorter expressions Functions
        function modPath = getPath(obj, modelName, indexes)
            modPath = sprintf(obj.workspace.(modelName).modelPath, indexes{:});
        end

        function datenum = getFileDatenum(obj, modelName, indexes)
            fileInfo = dir(obj.getPath(modelName, indexes));
            if (~isempty(fileInfo))
                datenum = fileInfo(1).datenum;
            else
                datenum = 0;
            end
        end

        function policy = getModelPolicy(obj, modelName)
            policy = obj.workspace.(modelName).flushPolicy;
        end

        function do_refresh = getShouldRefresh(obj, modelName, indexes)
            % indexes not used yet, but could allow for finer control on number
            % of times
            do_refresh = strcmpi(obj.workspace.(modelName).refreshPolicy, 'always');
        end

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Core Setter Functions
        function setField(obj, modelName, indexes, field, data)
            obj.workspace.(modelName).data{indexes{:}}.(field) = data;
            % Ensuring Timestamp, if not present, setting it to 0
            obj.ensureTstampPresence(modelName, indexes, field);
        end

        function setTstamp(obj, modelName, indexes, tstamp, field)
            if (~exist('field', 'var'))
                obj.workspace.(modelName).timestamp{indexes{:}} = tstamp;
            else
                obj.workspace.(modelName).timestamp{indexes{:}}.(field) = tstamp;
            end
        end

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Core Getter Functions
        function data = getField(obj, modelName, indexes, field)
            % Be sure we have a timestamp field for this field
            obj.ensureTstampPresence(modelName, indexes, field);
            % Now test availability
            if (obj.isAvailableField(modelName, indexes, field) ...
                    && obj.isUpToDate(modelName, indexes, field))
                % Already loaded and nothing has changed
                data = obj.workspace.(modelName).data{indexes{:}}.(field);
            else
                % Reloading
                dataName = obj.getPath(modelName, indexes);
                mfobj = matfile(dataName);
                data = mfobj.(field);
                obj.setField(modelName, indexes, field, data);

                % Updating timestamp
                datenum = obj.getFileDatenum(modelName, indexes);
                obj.setTstamp(modelName, indexes, datenum, field)
            end
        end

        function data = getFields(obj, modelName, indexes, fields)
            for ii = 1:numel(fields)
                field = fields{ii};
                data.(field) = getField(obj, modelName, indexes, field);
            end
        end

        function tstamp = getTstamp(obj, modelName, indexes, field)
            if (~exist('field', 'var'))
                tstamp = obj.workspace.(modelName).timestamp{indexes{:}};
            else
                tstamp = obj.workspace.(modelName).timestamp{indexes{:}}.(field);
            end
        end

        function fields = getFieldNames(obj, modelName, indexes)
            dataName = obj.getPath(modelName, indexes);
            mfobj = matfile(dataName);
            fields = setdiff(fieldnames(mfobj), {'Properties'});
        end

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Flushing Functions
        function flushSingleField(obj, modelName, indexes, field)
            if (obj.getFileDatenum(modelName, indexes) ...
                    ~= obj.getTstamp(modelName, indexes, field))
                switch (obj.getModelPolicy(modelName))
                    case 'error'
                        error('CACHE:flush_conflict_with_timestamp', ...
                              'Trying to flush to a file which has changed since the last read');
                    case 'overwrite'
                        warning('CACHE:flush_conflict_with_timestamp', ...
                                'Overwriting a file which has potentially changed since the last read');
                end
            end

            % Let's write the field to the file
            mfobj = matfile(obj.getPath(modelName, indexes), 'Writable', true);
            mfobj.(field) = obj.workspace.(modelName).data{indexes{:}}.(field);
            delete(mfobj);

            if (obj.getShouldRefresh(modelName, indexes))
                obj.refreshFile(modelName, indexes);
            end

            % Let's update the timestamp
            datenum = obj.getFileDatenum(modelName, indexes);
            obj.setTstamp(modelName, indexes, datenum, field);
        end

        function refreshFile(obj, modelName, indexes)
            fileName = obj.getPath(modelName, indexes);
            data = load(fileName);
            save(fileName, '-struct', 'data', '-v7.3');
        end
    end
end
