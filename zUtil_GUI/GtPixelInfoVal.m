classdef GtPixelInfoVal < handle
    properties
        parent;
        frame_h;

        img_h;
        axes_h;

        conf;

        pointerCbk;
    end

    properties (Access = private)
        string_empty = '(-, -, -) -';
        string_pattern = '(%d, %d, %d) %g';
    end

    methods (Access = public)
        function obj = GtPixelInfoVal(parent, image_handler, pointCbk, pattern, empty_pattern)
            obj.parent = parent;
            obj.img_h = image_handler;
            obj.frame_h = gtGetParentFigure(parent);
            obj.axes_h = ancestor(obj.img_h, 'axes');

            obj.pointerCbk = pointCbk;

            if (exist('pattern', 'var') && ~isempty(pattern))
                obj.string_pattern = pattern;
            end
            if (exist('empty_pattern', 'var') && ~isempty(empty_pattern))
                obj.string_empty = empty_pattern;
            end

            obj.conf = [];
            obj.conf.box_h = uicontrol('parent', obj.parent, ...
                'Style', 'text', 'HorizontalAlignment', 'left', ...
                'String', obj.string_empty);
            set(obj.conf.box_h, 'Unit', 'normalized');
            set(obj.conf.box_h, 'Position', [0 0 1 1]);
        end

        function onMouseMoveCbk(obj)
            point = get(obj.axes_h, 'CurrentPoint');
            x = point(1,1);
            y = point(1,2);
            xlim = get(obj.axes_h, 'Xlim');
            ylim = get(obj.axes_h, 'Ylim');

            % At the border, rounding gives a wrong pixel position
            if (x >= xlim(1) && x < xlim(2) && y >= ylim(1) && y < ylim(2))
                try
                    [completePoint, value] = obj.pointerCbk(x, y);
                    obj.displayString(completePoint, value);
                catch mexc
                    if (~gtCheckExceptionType(mexc, 'MATLAB:.*'))
                        gtPrintException(mexc)
                    else
                        rethrow(mexc)
                    end
                    obj.displayDefaultString();
                end
            else
                obj.displayDefaultString();
            end
        end

        function changePatternAndCbk(obj, pattern, cbk)
            if (~isempty(pattern))
                obj.string_pattern = pattern;
            end
            if (~isempty(cbk))
                obj.pointerCbk = cbk;
            end
        end

        function clipString = getString(obj)
            clipString = get(obj.conf.box_h, 'String');
        end

        function pattern = getPattern(obj)
            pattern = obj.string_pattern;
        end
    end

    methods (Access = protected)

        function displayString(obj, point, value)
            set(obj.conf.box_h, 'String', sprintf(obj.string_pattern, [point, value]));
        end

        function displayDefaultString(obj)
            set(obj.conf.box_h, 'String', obj.string_empty);
        end
    end
end
