function selected = gtGuiMontage(imgStack, selectedImages, spotsIds, omegas, title, varargin)
% GUIGTMONTAGE
%     selected = gtGuiMontage(imgStack, selectedImages, spotsIds, omegas, title)
%     --------------------------------------------------------------------------
%

    if (~exist('selectedImages', 'var'))
        Mexc = MException(  'GUI:not_enough_input_args', ...
                            'You didn''t specify the selected Images');
        throw(Mexc);
    end

    if (~exist('spotsIds', 'var'))
        spotsIds = 1:length(selectedImages);
    end
    if (~exist('omegas', 'var'))
        omegas = zeros(length(selectedImages), 1);
    end

    if (size(imgStack, 2) ~= numel(selectedImages))
        error('GUI:wrong_input_args', ...
            'Wrong size for selected Images vector: %d, while having %d selected', ...
            size(imgStack, 2), numel(selectedImages));
    elseif (size(imgStack,2) ~= length(omegas))
        error('GUI:wrong_input_args', 'Wrong size of Omegas vector');
    end

    if (~exist('title', 'var'))
        title = 'Montage Diffspot';
    end

    % Init the GUI
    guiData = gtGuiMontage_init(imgStack, selectedImages, spotsIds, omegas, title, varargin{:});

    while (1)
        % Wait for the application to close
        if (guiData.close), break; end
        pause(0.001);
    end

    % Save results
    selected = guiData.selected;

    % Close the figure
    delete( guiData.window );

% End of function

%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Private functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    function guiData = gtGuiMontage_init(imgStack, selectedImages, spotsIds, omegas, title, varargin)

        guiData = [];

        % Condition to exit from the gui
        guiData.close = false;

        % Let's store the selected images vector in input
        guiData.selected = selectedImages;

        % Let's store the omegas vector in input
        guiData.omegas = omegas;

        % The figure
        guiData.window = figure( ...
                'Name', title, ...
                'NumberTitle', 'off', ...
                'MenuBar', 'none', ...
                'Toolbar', 'none', ...
                'HandleVisibility', 'off' );

        % Boxes to help with the formatting of the window
        guiData.main_boxes = uiextras.VBox('Parent', guiData.window);

        % Will contain the images
        guiData.imgs_panel = uiextras.Panel('Parent', guiData.main_boxes);

        % Will contain the buttons
        guiData.buttons_panel = uiextras.Panel('Parent', guiData.main_boxes);

        set(guiData.main_boxes, 'Sizes', [-1 40]);

        % Layout for the buttons
        guiData.butt_boxes = uiextras.HButtonBox('Parent', guiData.buttons_panel);

        guiData.guiOkButton = uicontrol( 'Style', 'PushButton', ...
                                         'Parent', guiData.butt_boxes, ...
                                         'String', 'Ok', ...
                                         'Callback', @onOkButton );
        guiData.guiCancelButton = uicontrol( 'Style', 'PushButton', ...
                                             'Parent', guiData.butt_boxes, ...
                                             'String', 'Cancel', ...
                                             'Callback', @onCancelButton );
        guiData.guiResetButton = uicontrol( 'Style', 'PushButton', ...
                                            'Parent', guiData.butt_boxes, ...
                                            'String', 'Reset', ...
                                            'Callback', @onResetButton );

        % Grid that holds the images
        guiData.imgs_grid = uiextras.Grid('Parent', guiData.imgs_panel);

        % I expect a matrix like this stack(:,:,1,n) which will have n images.
        % For each of these images, we will display them in proper boxes

        %%% Kind of a hack - presetting the values to zeros, forces the handles
        %%% to be doubles, which then forces the use of the
        %%% hg.uicontrol/{get,set} methods instead of the overloaded uiextras.
        %%% This makes it possible to assign the 'ButtonDownFcn' callback on the
        %%% panel, while with the uiextras/{get,set} would not be accessible
        guiData.listOfPlainPanels = zeros(length(selectedImages));

        drawnow;

        % hack to make it display the images: may be a matlab bug
        % related to flushing
        fig = figure();
        ax = axes('parent', fig);

        % Creates the panels containing the images
        % Sorting by Omegas
        if ~isempty(varargin)
            if length(varargin) == 1
                guiData.key = varargin{1};
                guiData.sortOrder = 'ascend';
            elseif length(varargin) > 1
                guiData.key = varargin{1};
                guiData.sortOrder = varargin{2};
            end
        else
            guiData.key       = omegas;
            guiData.sortOrder = 'ascend';
        end
        [~, guiData.order] = sort(guiData.key, guiData.sortOrder);
        for ii = 1:length(selectedImages)
            index = guiData.order(ii);
            img = squeeze(imgStack(:,index,:))';

            % hack to make it display the images: may be a matlab bug
            % related to flushing
            if (ii == 1)
                imagesc(img, 'parent', ax);
            end

            if (omegas(index) ~= 0)
                guiData = createDiffSpotPanel(guiData, img, selectedImages(index), index, spotsIds(index), omegas(index));
            else
                guiData = createDiffSpotPanel(guiData, img, selectedImages(index), index, spotsIds(index));
            end
        end

        % hack to make it display the images: may be a matlab bug
        % related to flushing
        delete(fig);

        % Rescale the panels to fit as much as possible (and to be as square as
        % possible)
        resizeImgs(0,0,guiData);

        drawnow;

        % Callbacks for the figure
        set(guiData.window, 'ResizeFcn', {@resizeImgs, guiData});
        set(guiData.window, 'CloseRequestFcn', @onCancelButton);
    end

    function onOkButton(~, ~)
        % Let's read the status of the panels
        for ii = 1:length(guiData.selected)
            panel = guiData.listOfPanels{ii};

            colorVec = get(panel, 'TitleColor');
            % Encoding of green color in matlab
            greenVec = [0 1 0];
            result = xor(colorVec, greenVec);

            % If result contains just zeros, then it is green
            guiData.selected(ii) = isempty(find(result == 1, 1));
        end
        guiData.close = true;
    end

    function onCancelButton(~, ~)
        guiData.close = true;
    end

    function onResetButton(~, ~)
        for ii = 1:length(guiData.selected)
            panel = guiData.listOfPanels{ii};

            if (guiData.selected(ii))
                set(panel, 'TitleColor', 'g');
            else
                set(panel, 'TitleColor', 'y');
            end
        end
    end

    function guiData = createDiffSpotPanel(gui, img, active, ii, num, omega)
    % It creates the panel that contains the difspot.
        if (exist('omega', 'var'))
            text = sprintf('w: %2.1f, Id: %d', omega, num);
        else
            text = sprintf('Id: %d', num);
        end
        panel = uiextras.BoxPanel('Parent', gui.imgs_grid, 'Title', text);

        if (active)
            set(panel, 'TitleColor', 'g');
        else
            set(panel, 'TitleColor', 'y');
        end

        axes1 = axes( 'Parent', panel, 'ActivePositionProperty', 'Position');
        colormap(axes1, 'gray');
        image1 = imagesc(img, 'Parent', axes1);
        axis(axes1, 'off');
        axis(axes1, 'image');

        % Let's save the handles in double copy (panel)
        gui.listOfPanels{ii} = panel;
        gui.listOfPlainPanels(ii) = panel;
        gui.listOfAxes{ii} = axes1;
        guiData = gui;

        % Callbacks to change the state of the panels selected/unselected
        set(gui.listOfPlainPanels(ii), 'ButtonDownFcn', {@toggleCheck, ii});
        set(gui.listOfAxes{ii}, 'ButtonDownFcn', {@toggleCheck, ii});
        set(image1, 'ButtonDownFcn', {@toggleCheck, ii});
        set(panel, 'TooltipString', sprintf('index: %d',ii));
    end

    function resizeImgs(~, ~, guiData)
    % Rescale the panels to fit as much as possible (and to be as square as
    % possible)

        if (length(guiData.listOfAxes) > 1)
            posWindow = get(guiData.window, 'Position');

            % This position is relative to the figure position.
            pos = get(guiData.imgs_panel, 'Position');
            % To get the true dimensions, let's multiply component-wise
            pos = pos .* posWindow;

            ratio = pos(3)/pos(4);

            numInRow = floor(sqrt(length(guiData.selected)/ratio));
            numInColumn = ceil(sqrt(length(guiData.selected)/ratio));

            set( guiData.imgs_grid, 'ColumnSizes', -1*ones(numInColumn,1), ...
                 'RowSizes', -1*ones(numInRow,1) );
        end
    end

    function toggleCheck(~, ~, num)
        panel = guiData.listOfPanels{num};

        % Get's the color
        colorVector = get( panel, 'TitleColor');
        % Encoding of green color in matlab
        greenVec = [0 1 0];

        result = xor(colorVector, greenVec);

        % If result contains just zeros, then it is green
        if (isempty(find(result == 1, 1)))
            set( panel, 'TitleColor', 'y');
        else
            set( panel, 'TitleColor', 'g');
        end
    end
end
