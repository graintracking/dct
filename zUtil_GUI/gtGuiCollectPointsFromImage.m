function points = gtGuiCollectPointsFromImage(map, do_rescale)
    f = figure();
    ax = axes('parent', f);
    if (exist('do_rescale', 'var') && ~isempty(do_rescale) && do_rescale)
        map = (map - min(map(:))) ./ (max(map(:)) - min(map(:)));
        im = imagesc(map, 'parent', ax);
    else
        im = imshow(map, 'parent', ax);
    end

    cm = uicontextmenu('Parent', f, 'Callback', @(src, evt)save_clicked_point());
    set(im, 'UIContextMenu', cm);
    uimenu(cm, 'Label', 'Add point..', 'Callback', @(src, evt)add_current_point());
    uimenu(cm, 'Label', 'Exit', 'Callback', @(src, evt)get_finally_out());

    point = zeros(1, 2);

    points = zeros(0, 2);

    get_out = false;

    while(~get_out)
        pause(0.001);
    end

    close(f);

    function save_clicked_point()
        point = get(ax, 'CurrentPoint');
        point = round(point(1, 1:2));
    end

    function add_current_point()
        points(end+1, :) = point;
    end

    function get_finally_out()
        get_out = true;
    end
end
