function gtTriggerUpdateCallback(callback_info)
% GTTRIGGERUPDATECALLBACK If passed a callback, it calls it!
%
% Input:
% either a function handle, or a structure with the fields:
%   - function: which contains the function handle
%   - arguments: which contains the arguments to pass to the function

    if (~isempty(callback_info))
        if (isstruct(callback_info))
            if (~isfield(callback_info, 'function'))
                Mexc = MException('CBK:couldn''t_find_a_function_handle', ...
                                  'The provided structure doesn''t contain the "function" field');
                throw(Mexc)
            end

            if (~isa(callback_info.function, 'function_handle'))
                Mexc = MException('CBK:not_a_function_handle', ...
                                'The provided object is not a function_handle');
                throw(Mexc)
            end

            if (~isfield(callback_info, 'arguments'))
                Mexc = MException('CBK:couldn''t_find_arguments', ...
                                  'The provided structure doesn''t contain the "arguments" field');
                throw(Mexc)
            end

            callback_info.function(callback_info.arguments);
        elseif (isa(callback_info, 'function_handle'))
            callback_info();
        else
            Mexc = MException('CBK:not_a_function_handle', ...
                                'The provided object is not a function_handle');
            throw(Mexc)
        end
    end
end
