function varargout = guiGtSetup(varargin)
% GUIGTSETUP MATLAB code for guiGtSetup.fig
%      GUIGTSETUP, by itself, creates a new GUIGTSETUP or raises the existing
%      singleton*.
%
%      H = GUIGTSETUP returns the handle to a new GUIGTSETUP or the handle to
%      the existing singleton*.
%
%      GUIGTSETUP('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in GUIGTSETUP.M with the given input arguments.
%
%      GUIGTSETUP('Property','Value',...) creates a new GUIGTSETUP or raises
%      the existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before guiGtSetup_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to guiGtSetup_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help guiGtSetup

% Last Modified by GUIDE v2.5 23-Jan-2012 18:10:13

    % Begin initialization code - DO NOT EDIT
    gui_Singleton = 1;
    gui_State = struct('gui_Name',       mfilename, ...
                       'gui_Singleton',  gui_Singleton, ...
                       'gui_OpeningFcn', @guiGtSetup_OpeningFcn, ...
                       'gui_OutputFcn',  @guiGtSetup_OutputFcn, ...
                       'gui_LayoutFcn',  [] , ...
                       'gui_Callback',   []);
    if nargin && ischar(varargin{1})
        gui_State.gui_Callback = str2func(varargin{1});
    end

    if nargout
        [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
    else
        gui_mainfcn(gui_State, varargin{:});
    end
    % End initialization code - DO NOT EDIT
end

% --- Executes just before guiGtSetup is made visible.
function guiGtSetup_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to guiGtSetup (see VARARGIN)

    % Choose default command line output for guiGtSetup
    handles.output = hObject;

    % Update handles structure
    guidata(hObject, handles);

    initialize_gui(hObject, handles, false);

    % UIWAIT makes guiGtSetup wait for user response (see UIRESUME)
    % uiwait(handles.figure1);
end

% --- Outputs from this function are returned to the command line.
function varargout = guiGtSetup_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

    % Get default command line output from handles structure
    varargout{1} = handles.output;
end

% --------------------------------------------------------------------
function initialize_gui(fig_handle, handles, isreset)
% If the metricdata field is present and the reset flag is false, it means
% we are we are just re-initializing a GUI by calling it from the cmd line
% while it is up. So, bail out as we dont want to reset the data.

    % if isfield(handles, 'metricdata') && ~isreset
    %     return;
    % end
    %
    % handles.metricdata.density = 0;
    % handles.metricdata.volume  = 0;
    %
    % set(handles.density, 'String', handles.metricdata.density);
    % set(handles.volume,  'String', handles.metricdata.volume);
    % set(handles.mass, 'String', 0);
    %
    % set(handles.unitgroup, 'SelectedObject', handles.english);
    %
    % set(handles.text4, 'String', 'lb/cu.in');
    % set(handles.text5, 'String', 'cu.in');
    % set(handles.text6, 'String', 'lb');

    handles.guiEditPath = setAcquisitionPath(handles.guiEditPath, pwd);

    [handles.parameters, handles.list] = make_parameters(2);
    updateListbox(handles.guiListbox, handles.parameters);

    % Update handles structure
    guidata(handles.figure1, handles);
end

function guiAcquisPathOut = setAcquisitionPath(guiAcquisitionPath, pathname)
    set(guiAcquisitionPath, 'String', pathname);
    guiAcquisPathOut = guiAcquisitionPath;
end

% --- Executes on selection change in guiListbox.
function guiListbox_Callback(hObject, eventdata, handles)
% hObject    handle to guiListbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns guiListbox contents as cell array
%        contents{get(hObject,'Value')} returns selected item from guiListbox
end

% --- Executes during object creation, after setting all properties.
function guiListbox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to guiListbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

    % Hint: listbox controls usually have a white background on Windows.
    %       See ISPC and COMPUTER.
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end
end

% --- Executes on button press in guiSaveButton.
function guiSaveButton_Callback(hObject, eventdata, handles)
% hObject    handle to guiSaveButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

    [filename, pathname] = uiputfile('parameters.mat', 'Save Parameters as');
    disp(['File: ''' filename ''' and path: ''' pathname ''''])

    if filename
        save(handles.parameters, fullfile(pathname, filename));
    end
end

% --- Executes on button press in guiLoadButton.
function guiLoadButton_Callback(hObject, eventdata, handles)
% hObject    handle to guiLoadButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

    [filename, pathname] = uigetfile('*.mat', 'Select a Parameter file');
    disp(['File: ''' filename ''' and path: ''' pathname ''''])

    if filename
        completePath = fullfile(pathname, filename);
        [handles.parameters, handles.list] = make_parameters(2);
        handles.parameters = load(completePath);

        % Update handles structure
        guidata(handles.figure1, handles);
    end
end


% --- Executes on button press in guiEditButton.
function guiEditButton_Callback(hObject, eventdata, handles)
% hObject    handle to guiEditButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

    namesList = fieldnames(handles.parameters);

    currentValue = get(handles.guiListbox, 'Value');
    section_name = namesList(currentValue);
    section_name = section_name{1};

    section = getfield(handles.parameters, section_name);
    section_descr = getfield(handles.list, section_name);

    section = editSection(section, section_descr);
    handles.parameters = setfield(handles.parameters, section_name, section);

    % Update handles structure
    guidata(handles.figure1, handles);
end

% --- Executes on button press in guiLoadXMLPushButton.
function guiLoadXMLPushButton_Callback(hObject, eventdata, handles)
% hObject    handle to guiLoadXMLPushButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

    [xmlFilename, xmlPath] = uigetfile('*.xml', 'Select an XML file');
    disp(['File: ''' xmlFilename ''' and path: ''' xmlPath ''''])

    if xmlFilename
        completePath = fullfile(xmlPath, xmlFilename);
        [handles.parameters, handles.list] = make_parameters(2);
        handles.parameters = gtLoadAcquisitionXML(handles.parameters, handles.list, completePath, false);

        handles.guiEditPath = setAcquisitionPath(handles.guiEditPath, xmlPath);

        % Update handles structure
        guidata(handles.figure1, handles);
    end
end

% --- Executes on button press in guiLaunchButton.
function guiLaunchButton_Callback(hObject, eventdata, handles)
% hObject    handle to guiLaunchButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
end

function updateListbox(listbox, paramfile)
    namesList = fieldnames(paramfile);
    listbox_string = '';
    for k = 1:length(namesList)
        listbox_string = [listbox_string namesList(k)];
    end
    set(listbox, 'String', listbox_string);
end

function sectionOut = editSection(section, section_descr)
    header='Check carefully that the following are correct:';
    sectionOut = gtModifyStructure(section, section_descr, [], header);
end

function guiEditPath_Callback(hObject, eventdata, handles)
% hObject    handle to guiEditPath (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

    % Hints: get(hObject,'String') returns contents of guiEditPath as text
    %        str2double(get(hObject,'String')) returns contents of guiEditPath as a double
    dirname = get(handles.guiEditPath, 'String');
    resetAcquisitionDir(handles, dirname);
end

% --- Executes during object creation, after setting all properties.
function guiEditPath_CreateFcn(hObject, eventdata, handles)
% hObject    handle to guiEditPath (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

    % Hint: edit controls usually have a white background on Windows.
    %       See ISPC and COMPUTER.
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end
end


% --- Executes on button press in guiChangeButton.
function guiChangeButton_Callback(hObject, eventdata, handles)
% hObject    handle to guiChangeButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

    startdir = get(handles.guiEditPath, 'String');
    directoryname = uigetdir(startdir, 'Select acquisition directory...');
    if directoryname
        resetAcquisitionDir(handles, directoryname);
    end
end

function resetAcquisitionDir(handles, directoryname)
    [handles.parameters, handles.list] = make_parameters(2);

    % Let's find a xml file
    paramsFiles = dir(fullfile(directoryname, 'parameters.mat'));
    xmlFiles = dir(fullfile(directoryname, '*.xml'));
    if length(paramsFiles) == 1
        completePath = fullfile(directoryname, paramsFiles(1).name);
        disp(['Opening file: ' completePath])
        handles.parameters = load(completePath);

        handles.guiEditPath = setAcquisitionPath(handles.guiEditPath, directoryname);
    elseif length(xmlFiles) == 1
        completePath = fullfile(directoryname, xmlFiles(1).name);
        disp(['Opening file: ' completePath])
        handles.parameters = gtLoadAcquisitionXML(handles.parameters, handles.list, completePath, false);

        handles.guiEditPath = setAcquisitionPath(handles.guiEditPath, directoryname);
    else
        warnmessage = [ 'Not possible to automatically locate any ' ...
                        'parameters.mat or *.xml file in directory: ' ...
                        directoryname];
        f = warndlg(warnmessage, 'Warning', 'modal');
    end

    % Update handles structure
    guidata(handles.figure1, handles);
end

