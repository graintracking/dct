classdef GtBaseGuiElem < handle
    properties
        conf;

        children_gui;
        ui_timers;
        ui_callbacks;

        timestamp = 0;

        cache = GtSimpleCache();
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Public object API - Interaction with outside
    methods (Access = public)
        function obj = GtBaseGuiElem()
        % GTBASEGUIELEM/GTBASEGUIELEM Constructor
            obj.conf = [];

            % Internal parameters
            obj.children_gui = {};
            obj.ui_timers = {};
            obj.ui_callbacks = {};
            % default parameters
            obj.conf.parent = [];
            obj.conf.f_title = '';
            obj.conf.f_number = [];
            obj.conf.f_size = 'small';
        end

        function delete(obj)
        % GTBASEGUIELEM/DELETE Destructor
            obj.guiQuit();
            clear obj.conf;
            clear obj;
        end

        function updateDisplay(obj)
        % GTBASEGUIELEM/UPDATEDISPLAY Displaying function
            if ((now - obj.timestamp) > 1.157407407407407e-06)
                obj.doUpdateDisplay();
                obj.timestamp = now;
            end
        end
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Internal methods - Addressable from derived classes
    methods (Access = protected)
        function initGtBaseGuiElem(obj, arguments)
        % Base function that initializes the GUI object and shows it.
        % To change or enhance the gui, please modify accordingly the methods
        % called by this.

            % Create the configuration
            obj.initParams(arguments);
            % Set parameters that shouldn't be changed by the user
            obj.setConfigInvariants();
            % Create the gui objects
            obj.initGui();

            % Once we have the structures registered into the guidata, we can set
            % the parameters corresponding to the volume
            obj.resetUiComponents();
            % Now we can add the callbacks
            obj.addUICallbacks();

            % Finally update the display, to visualize the objects
            obj.updateDisplay();

            % protect the window from being accidentall reused
            % Disabled because it prevents update
            %set(obj.conf.h_figure, 'handlevisibility', 'callback')
        end

        function initParams(obj, arguments)
            % parse input
            [obj.conf, ~] = parse_pv_pairs(obj.conf, arguments);
        end

        function setConfigInvariants(obj)
            % Fixed parameters
            obj.conf.close_callback = @delete;
        end

        function initGui(obj)
        % If you overload it, please always call it as first!
            if (~verLessThan('matlab', '8.4.0'))
                uix.tracking('off')
            end
            if (isempty(obj.conf.parent))
                if (~isempty(obj.conf.f_number))
                    figure_title = sprintf('%s: %d', obj.conf.f_title, obj.conf.f_number);
                else
                    figure_title = obj.conf.f_title;
                end
                obj.conf.h_figure = figure('Name', figure_title, ...
                    'NumberTitle', 'off', 'MenuBar', 'none', ...
                    'Toolbar', 'none');
                obj.addUICallback(obj.conf.h_figure, 'CloseRequestFcn', @(src, evt)delete(obj));
                obj.conf.currentParent = obj.conf.h_figure; % Where are incapsulating in

                % Resize
                screen_pixels = GtBaseGuiElem.getPixels(0, 'ScreenSize');
                switch (lower(obj.conf.f_size))
                    case 'wide'
                        f_positioning = [4 4];
                        f_scaling = [2 2];
                    case 'big'
                        f_positioning = [4 6];
                        f_scaling = [2 1.5];
                    case 'small'
                        f_positioning = [3 4];
                        f_scaling = [3 2];
                end
                fig_pix = [ ...
                    (screen_pixels(1:2) + round(screen_pixels(3:4) ./ f_positioning)), ...
                    round(screen_pixels(3:4) ./ f_scaling) ];
                GtBaseGuiElem.setPixels(obj.conf.h_figure, 'Position', fig_pix);
            else
                obj.conf.h_figure = gtGetParentFigure(obj.conf.parent);
                obj.conf.currentParent = obj.conf.parent;
            end

            % Gui objects
            % GUI Callbacks
        end

        function addIncludedGuiElem(obj, elem)
            obj.children_gui{end+1} = elem;
        end

        function addTimer(obj, varargin)
            t = timer(varargin{:});
            obj.ui_timers{end+1} = t;
            start(t);
        end

        function guiQuit(obj)
        % If you overload it, please always call it at some point!
            obj.delUICallbacks();

            % Call also the quit functions of the included objects
            for n = 1:length(obj.children_gui)
                delete(obj.children_gui{n});
            end

            % Call also the quit functions of the timers
            for n = 1:length(obj.ui_timers)
                try
                    stop(obj.ui_timers{n});
                    delete(obj.ui_timers{n});
                catch mexc
                    gtPrintException(mexc)
                end
            end

            % If the figure is our, let's close it
            if isempty(obj.conf.parent)
                try
                    delete(obj.conf.h_figure)
                catch mexc
                    gtPrintException(mexc, 'Problem in closing figure');
                end
            end
        end

        function addUICallback(obj, elem, cb_type, cb_function, is_java)
            if exist('is_java', 'var') && is_java
                elem = findjobj(elem, 'Persist');
            end
            set(elem, cb_type, cb_function);
            obj.ui_callbacks{end+1} = {elem, cb_type};
        end

        function delUICallbacks(obj)
            for n = 1:numel(obj.ui_callbacks)
                cb = obj.ui_callbacks{n};
                set(cb{1}, cb{2}, '');
            end
        end


        % Utility Functions
        function strParam = toString(~, content)
            if (islogical(content))
                if (content)
                    strParam = 'true';
                else
                    strParam = 'false';
                end
            elseif (isnumeric(content))
                if (numel(content) == 1)
                    strParam = num2str(content);
                elseif (isempty(content))
                    strParam = '[]';
                elseif (isinteger(content))
                    strParam = sprintf('[%s]', sprintf('%s;', sprintf(' %d', content)));
                else
                    strParam = sprintf('[%s]', sprintf('%s;', sprintf(' %g', content)));
                end
            else
                strParam = content;
            end
        end
    end

    methods (Static, Access = public)
        function pixels = getPixels(elem, property)
            units = get(elem, 'Units');
            set(elem, 'Units', 'pixel');
            pixels = get(elem, property);
            set(elem, 'Units', units);
        end

        function setPixels(elem, property, pixels)
            units = get(elem, 'Units');
            set(elem, 'Units', 'pixel');
            set(elem, property, pixels);
            set(elem, 'Units', units);
        end
    end

    methods (Abstract, Access = protected)
        resetUiComponents(obj)
        doUpdateDisplay(obj)
        addUICallbacks(obj)
    end
end
