function answer = inputwdefault(question, default, is_numeric)
% INPUTWDEFAULT  User input function from the command line with default value
% ---------------------------------------------------------------------------
%
%   INPUT:
%     question  = <string>  Message printed to tell the user what to enter
%     default   = <string>  Default value for input
%
%   OPTIONAL INPUT:
%     isnumeric = <any>     Specify that the input is a numeric one
%
%   OUTPUT:
%     answer    = <string>  User input (take the default one if empty)
%                 <double>  Returns double if numeric

    if ~ischar(default)
        gtError('inputwdefault:wrong_argument', ...
            ['Default argument should be a string, not a ' class(default)]);
    end

    if (~exist('is_numeric', 'var'))
        is_numeric = false;
    end

    questionAlign = 30;
    % Conform to any size terminals
    termSize = get(0, 'CommandWindowSize');
    realQuestionLength = mod(max(length(question), questionAlign), termSize(1));
    needNewLine = (realQuestionLength + 6 + length(default)) > termSize(1);

    if (needNewLine)
        questionFormat = ['%-' num2str(questionAlign) 's :\n[%s]\n'];
    else
        questionFormat = ['%-' num2str(questionAlign) 's : [%s] '];
    end
     
    answer = input(sprintf(questionFormat, question, default), 's');

    if isempty(answer)
        answer = default;
    end
    if (is_numeric)
        answer = str2num(answer);
    end
end % end of function
