classdef GtGuiSelectInList < GtGuiJoinableElem
% Class GtGuiSelectInList: Selector class for lists of entries.
%
% It expects a cell string array as input, and displays the values in the rows.
% Using the join() method is suggested.
%
% >> objHandle = GtGuiSelectInList({'Item1', 'Item2', 'Item3'});
% >> index = objHandle.join();
%
% index will contain the index of the selected element, in case Ok button was
% pressed, otherwise it will be an empty vector.
%
    properties
        listOfNames;
    end
    methods (Access = public)
        function obj = GtGuiSelectInList(listOfNames, varargin)
            obj = obj@GtGuiJoinableElem();
            if (iscell(listOfNames))
                listOfNames = reshape(listOfNames, [1 numel(listOfNames)]);
            end
            obj.listOfNames = listOfNames;

            obj.conf.f_title = 'List Selection';
            obj.initGtBaseGuiElem(varargin);
        end
    end

    methods (Access = protected)
        function initGui(obj)
            initGui@GtBaseGuiElem(obj)

            dlgBlock = [];
            dlgBlock.hor_boxes = uiextras.HBox('Parent', obj.conf.currentParent);
            dlgBlock.list = uicontrol('Parent', dlgBlock.hor_boxes, ...
                'Style', 'listbox', 'String', obj.listOfNames);
            dlgBlock.vert_boxes = uiextras.VButtonBox('Parent', dlgBlock.hor_boxes);
            dlgBlock.butts(1) = uicontrol('Parent', dlgBlock.vert_boxes, ...
                'Style', 'pushbutton', 'String', 'Ok', ...
                'Callback', @(src, evt)onOkButton(obj));
            dlgBlock.butts(2) = uicontrol('Parent', dlgBlock.vert_boxes, ...
                'Style', 'pushbutton', 'String', 'Cancel', ...
                'Callback', @(src, evt)onCancelButton(obj));

            set(dlgBlock.hor_boxes, 'Sizes', [-1 120]);

            obj.conf.dlgBlock = dlgBlock;
        end

        function resetUiComponents(obj)
        end

        function doUpdateDisplay(obj)
        end

        function addUICallbacks(obj)
        end
    end

    methods (Access = public)
        function onOkButton(obj)
            selected = get(obj.conf.dlgBlock.list, 'Value');
            if (obj.isJoint)
                obj.setJointReturnVal(selected);
            else
                fprintf('Selected value: %d\n', selected);
                delete(obj);
            end
        end
        function onCancelButton(obj)
            if (obj.isJoint)
                obj.setJointReturnVal([]);
            else
                delete(obj);
            end
        end
    end
end
