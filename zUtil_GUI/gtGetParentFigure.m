function hndl = gtGetParentFigure(hndl)
% GTGETPARENTFIGURE Looks for the parent figure of a graphic handle
% 
% Return:
%   if the object is a figure or figure descendent, returns the figure.
%   Otherwise returns [].

    found = false;
    while ~found
        if  isempty(hndl) || strcmp('figure', get(hndl, 'Type'))
            found = true;
        else
            hndl = get(hndl, 'Parent');
        end
    end
end
