function answer = inputwdefaultnumeric(question, default)
% INPUTWDEFAULTNUMERIC  Implicitly imposing numeric type to generic function
% -------------------------------------------------------------------------
%
%   INPUT:
%     question  = <string>  Message printed to tell the user what to enter
%     default   = <string>  Default numeric value for input
%
%   OUTPUT:
%     answer    = <double>  User input (take the default one if empty)

    answer = str2num(inputwdefault(question, default));
end
