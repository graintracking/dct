classdef GtEventLinkerVolView < handle
    properties (SetObservable, AbortSet, Access = private)
        slice;
        perspective;
        horflip;
        verflip;

        zoom_state;
    end

    properties (Access = private)
        handles = [];
        dims = [1 1 1];
    end

    methods (Access = public)
        function self = GtEventLinkerVolView(volDims, slicendx, persp, hflip, vflip, zoom_state, volViewer)
            self.dims = volDims;
            self.slice = slicendx;
            self.perspective = persp;
            self.horflip = hflip;
            self.verflip = vflip;
            self.zoom_state = zoom_state;

            addlistener(self, 'slice', 'PostSet', @(src, evt)self.propagateSlice);
            addlistener(self, 'perspective', 'PostSet', @(src, evt)self.propagatePerspective);
            addlistener(self, 'horflip', 'PostSet', @(src, evt)self.propagateHorFlip);
            addlistener(self, 'verflip', 'PostSet', @(src, evt)self.propagateVerFlip);
            addlistener(self, 'zoom_state', 'PostSet', @(src, evt)self.propagateZoom);

            self.attach(volViewer, volDims)
        end

        function notify(self, slicendx, persp, hflip, vflip, zoom_state)
            self.slice = slicendx;
            self.perspective = persp;
            self.horflip = hflip;
            self.verflip = vflip;
            self.zoom_state = zoom_state;
        end

        function attach(self, volViewer, volDims)
            if (~all(self.dims == volDims))
                gtError('GtEventLinkerVolView:wrong_argument', ...
                        'Volume dimensions mismatch for linkname')
            end
            % Attach listener's callback
            self.handles = [self.handles, volViewer];
        end

        function isNowEmpty = detach(self, volViewer)
            % Detach listener's callback
            self.handles(self.handles == volViewer) = [];
            isNowEmpty = isempty(self.handles);
        end
    end

    methods (Access = private)
        function propagateSlice(self)
            for ii = 1:length(self.handles)
                self.handles(ii).changeSlice(self.slice);
            end
        end

        function propagatePerspective(self)
            for ii = 1:length(self.handles)
                self.handles(ii).changePerspective(self.perspective);
            end
        end

        function propagateHorFlip(self)
            for ii = 1:length(self.handles)
                self.handles(ii).horFlip(self.horflip);
            end
        end

        function propagateVerFlip(self)
            for ii = 1:length(self.handles)
                self.handles(ii).verFlip(self.verflip);
            end
        end

        function propagateZoom(self)
            for ii = 1:length(self.handles)
                self.handles(ii).setZoom(self.zoom_state);
            end
        end
    end
end
