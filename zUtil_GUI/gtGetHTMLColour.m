function colourString = gtGetANSIColour(colour)
% GTGETANSICOLOUR Returns the ANSI colour for the terminal
%   colourString = gtGetANSIColour(colour)
%
% If in Desktop modality or in deployed environment, it doesn't return anything

    if (~exist('colour', 'var'))
        colourString = '#000000';
    else
        switch (lower(colour))
            case 'bold'
                colourString = '#000000';
            case 'green'
                colourString = '#00FF00';
            case 'red'
                colourString = '#FF0000';
            case 'yellow'
                colourString = '#FFFF00';
            case 'blue'
                colourString = '#0000FF';
            case 'magenta'
                colourString = '#FF00FF';
            case 'cyan'
                colourString = '#00FFFF';
            case 'white'
                colourString = '#FFFFFF';
            otherwise
                colourString = '#000000';
        end
    end
end
