function colourString = gtGetANSIColour(colour)
% GTGETANSICOLOUR Returns the ANSI colour for the terminal
%   colourString = gtGetANSIColour(colour)
%
% If in Desktop modality or in deployed environment, it doesn't return anything

    if (isdeployed || usejava('desktop'))
        colourString = '';
    elseif (~exist('colour', 'var'))
        colourString = '\033[0m';
    else
        switch (lower(colour))
            case 'bold'
                colourString = '\033[1m';
            case 'green'
                colourString = '\033[32m';
            case 'red'
                colourString = '\033[31m';
            case 'yellow'
                colourString = '\033[33m';
            case 'blue'
                colourString = '\033[34m';
            case 'magenta'
                colourString = '\033[35m';
            case 'cyan'
                colourString = '\033[36m';
            case 'white'
                colourString = '\033[37m';
            otherwise
                colourString = '\033[0m';
        end
    end
end
