function neighbors = gtFindNeighboringGrains(phaseID, grain_ids, pad, sample, varargin)
% GTFINDNEIGHBORINGGRAINS
% neighbors = gtFindNeighboringGrains(phaseID, grain_ids, pad, sample, varargin)
% INPUTS:
%    phaseID    = <int>  the default is 1
%    grain_ids  = <int>  the array of the IDs of the grains that are
%                        considered. The default is all the grains.
%    pad        = <int>  the number of voxels to pad for some tolerance.
% OPTIONAL INPUTS:
%    'strategy' = <str>  ['dil_vol']: to use dilated volumes.
%                        'bbox': to use the bounding boxes
%    'compact'  = <int>  [0] : No compaction
%                        1   : Only return the columns of indicated grains
%                        2   : Only return the rows and columns of the
%                              indicated grains
% OUTPUT:
%    neighbors  = <bool> logical sparse NxN square neighboring table
    if (~nargin || isempty(phaseID))
        phaseID = 1;
    end

    if (nargin < 3 || isempty(pad))
        pad = 2;
    end
    conf = struct( ...
        'strategy', 'dil_vol', ...
        'compact', false);
    conf = parse_pv_pairs(conf, varargin);
    % load sample
    if (~exist('sample', 'var') || isempty(sample))
        sample = GtSample.loadFromFile();
    end
    tot_grains = sample.phases{phaseID}.getNumberOfGrains;
    if (nargin < 2 || isempty(grain_ids))
        grain_ids = 1:tot_grains;
        grain_ids = grain_ids(sample.phases{phaseID}.selectedGrains);
    else
        invalid_grain_ids = grain_ids > tot_grains | grain_ids < 1;
        if (any(invalid_grain_ids))
            error([mfilename ':wrong_argument'], 'grain_ids list contains %d invalid grain IDs.', sum(invalid_grain_ids))
            disp('Invalid grain IDs:')
            disp(grain_ids(invalid_grain_ids))
        end
    end
    ngrains = numel(grain_ids);

    neighbors = false(tot_grains, tot_grains);
    if  (strcmp(conf.strategy, 'bbox') || isempty(sample.dilVolFile)) % check if there is available dilated volume.
        disp('Using bounding boxes')
        neighbors(:, grain_ids) = sfCalcNeighboringTableByBoundingBoxes(...
            sample.phases{phaseID}.bboxExtremes, ...
            sample.phases{phaseID}.boundingBox(:, 4:6), ...
            grain_ids, pad);
    else
        c = tic();
        fprintf('Loading volume..')
        dil_vol = load(sample.dilVolFile, 'grains');
        dil_vol_grains = dil_vol.grains;
        fprintf('\b\b: Done in %g seconds.\n', toc(c))

        se = sfBallSe(pad);

        padding = [-pad, -pad, -pad, 2*pad, 2*pad, 2*pad];

        gauge = GtGauge(ngrains, 'Processing grains: ');
        for ii = grain_ids
            gauge.incrementAndDisplay()

            full_gr_vol = dil_vol_grains == ii;
            if (sample.phases{phaseID}.getSelected(ii) && any(full_gr_vol(:)))
                bb = zeros(1, 6);

                bb([1,4]) = find_lims_dim(full_gr_vol, 1);
                bb([2,5]) = find_lims_dim(full_gr_vol, 2);
                bb([3,6]) = find_lims_dim(full_gr_vol, 3);

                bb = bb + padding;
                crop_vol = gtCrop(dil_vol_grains, bb);

                gr_vol = crop_vol == ii;
                gr_vol_dil = imdilate(uint8(gr_vol), se);
                inds = unique(crop_vol(logical(gr_vol_dil(:))));

                inds(inds < 1) = []; % removing the 0 and -1 id
                neighbors(inds, ii) = true;
            end
        end
    end
    switch conf.compact
        case 1
            neighbors = neighbors(:, grain_ids);
        case 2
            neighbors = neighbors(grain_ids, grain_ids);
    end
end

function sub_neighboring_table = sfCalcNeighboringTableByBoundingBoxes(bbox_all_grains, bbox_size_all_grains, gr_ids, pad_tol)
% Calculate neighboring table based on the grain bounding boxes. For
% example, if the bboxes [lb1_x,lb1_y,lb1_z,ub1_x,ub1_y,ub1_z] and
% [lb2_x,lb2_y,lb2_z,ub2_x,ub2_y,ub2_z] are neighbors, then
% ((ub1_i-lb1_i+1)+(ub2_i-lb2_i+1)-1)>=max(|ub2_i-lb1_i|,|ub1_i-lb2_i|) for i=x,y,z.
    bbox_indicated_grains = bbox_all_grains(gr_ids, :);

    % calculate the absolute distance between the bounds in same directions.
    distance_between_bounds = abs(bsxfun(@minus, ...
        permute(bbox_all_grains, [1, 3, 2]), ...
        permute(bbox_indicated_grains(:, [4:6, 1:3]), [3, 1, 2])));

    % sum the lengths of bounding boxes in each direction
    bbox_size_indicated_grains = bbox_size_all_grains(gr_ids, :);
    bbox_size_sum = bsxfun(@plus, ...
        permute(bbox_size_all_grains, [1, 3, 2]), ...
        permute(bbox_size_indicated_grains, [3, 1, 2]));

    % the sizes substract 1 and add paddings
    bbox_size_sum = pad_tol - 1 + bbox_size_sum;

    % for a pair of grains, if all the distances is no more than the sum of
    % the lengths in each direction, the grains are neighbors.
    sub_neighboring_table = all(distance_between_bounds(:, :, 1:3) <= bbox_size_sum, 3);
    sub_neighboring_table = sub_neighboring_table & all(distance_between_bounds(:, :, 4:6) <= bbox_size_sum, 3);
end

function se_out = sfBallSe(pad_tol)
    coords = -round(pad_tol):round(pad_tol);
    [X, Y, Z] = ndgrid(coords, coords, coords);
    nhood = ((X .^ 2 + Y .^ 2 + Z .^ 2) <= (pad_tol ^ 2));
    se_out = strel('arbitrary', nhood);
end

function lims = find_lims_dim(vol, dims_sum)
    %  When dims_sum is 1, we sum the vol along dimension 2 and 3;
    %  When dims_sum is 2, we sum the vol along dimension 1 and 3;
    %  When dims_sum is 3, we sum the vol along dimension 1 and 2. (The orders of the dimensions are not important.)
    inds = [1 2 0]; 
    inds(dims_sum) = 3;
    vol = sum(sum(vol, inds(1)), inds(2));
    %  Find the bounds along dimension dims_sum.
    lims = [find(vol, 1, 'first'), find(vol, 1, 'last')];
    %  Return the lower bound and size.
    lims = [lims(1), lims(2) - lims(1) + 1];
end
