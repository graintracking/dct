function grain = gtTwinCalculateVariants(grain, parameters, twin_angle, twin_axis, twin_center, varargin)
% GTTWINCALCULATEVARIANTS  Calulate diffraction vectors and detector intersection points
%
%     grain = gtTwinCalculateVariants(grain, [parameters], [twin_angle], [twin_axis], [twin_center], varargin)
%     --------------------------------------------------------------------------------------------------------
%     INPUT:
%       grain         = <struct>   grain structure
%       parameters    = <struct>   parameters file ({parameters.mat})
%       twin_angle    = <double>   twin rotation angle {0}
%       twin_axis     = <double>   hkl of twin direction (Miller/Miller
%                                  Bravais indexes) {[0 0 1]}
%       twin_center   = <double>   Twin alternative centroid position {[]}
%       varargin      =            options for gtCalculateGrain
%
%     OPTIONAL INPUT (additional p/v pairs):
%       variants      = <logical>  do the forward simulation for all the variants {false}
%       plot_variants = <logical>  show or not the figure {false}
%
%     OUTPUT:
%       grain         = <cell>     updated grains with added fields:
%            .twin_vars(ii).allblobs

idx_grain_model = fullfile(parameters.acq.dir, '4_grains', 'phase_%02d', 'index.mat');

if (~exist('parameters', 'var') || isempty(parameters))
    parameters = gtLoadParameters();
end
if (~exist('twin_angle', 'var') || isempty(twin_angle))
    twin_angle = 0;
end
if (~exist('twin_axis', 'var') || isempty(twin_axis))
    twin_axis = [0 0 1];
end

[hasP, ind] = ismember('plot_variants', varargin(1:2:end));
if (hasP)
    plot_variants = varargin{ind*2};
else
    plot_variants = true;
end

if (~isfield(grain, 'twin_vars') ...
        || (isfield(grain, 'twin_vars') && ~isfield(grain.twin_vars, 'R_vector')) )
    grain.twin_vars = gtTwinOrientations(grain.R_vector, ...
        twin_angle, twin_axis, ...
        parameters.cryst(grain.phaseid).spacegroup, ...
        parameters.cryst(grain.phaseid).latticepar, ...
        plot_variants);
end

if (~exist('twin_center', 'var') || isempty(twin_center))
    twin_center = grain.center;
else
    if (numel(twin_center) == 1 && isnumeric(twin_center))
        disp(['Using twin information from grain ' num2str(twin_center)])
        idx_grains = gtMATVolReader([sprintf(idx_grain_model, grain.phaseid), ':grain']);
        twin_center = idx_grains{twin_center}.center;
    elseif (numel(twin_center) == 3 && isvector(twin_center))
        fprintf('Using twin center (%f %f %f)\n', twin_center)
        fprintf('with parent grain center (%f %f %f)\n', grain.center)
    else
        error('gtTwinCalculateVariants:wrong_argument', ...
            'Twin Center should be either a grain ID or a position')
    end
end
twin_center = reshape(twin_center, 1, 3);

[hasP, ind] = ismember('variants', varargin(1:2:end));
if (hasP && (varargin{ind*2} == true) && isfield(grain, 'twin_vars'))
    for kk = 1:numel(grain.twin_vars)
        gr = grain;
        gr.R_vector = grain.twin_vars(kk).R_vector;
        gr.center = twin_center;

        grain.twin_vars(kk) = gtCalculateGrain(gr, parameters, varargin{:});

        if (plot_variants)
            ax_title = sprintf('Fsim for grain %d in phase %d - variant %d', ...
                grain.id, grain.phaseid, fsimID);
            set(get(gca, 'Title'), 'String', ax_title, 'FontWeight', 'bold');
        end
    end
end

end % end of function


