function [included, difspotID] = gtFwdSimGetIndexterReflections(gr, grain_idx, seg_spots, det_ind)
% [included, difspotID] = gtFwdSimGetIndexterReflections(gr, grain_idx, segmentedSpots)
%   It matches INDEXTER's spots with gtCalculateGrain's allblobs
%   reflections, by returning their indexes in included
%
%   INPUT:
%       gr        : the grain structure
%       grain_idx : the corresponding INDEXTER grain structure
%       seg_spots : the difspot table from DB
%   OUTPUT:
%       included  : the matched indexes
%

    if (~exist('seg_spots', 'var'))
        seg_spots = gtDBLoadTable([parameters.acq.name 'difspot'], 'difspotID');
    end

    if (~exist('det_ind', 'var') || isempty(det_ind))
        det_ind = 1;
    end

    num_ab_entries = numel(gr.allblobs(det_ind).omega);
    included = [];
    difspotID = [];

    for ii_p = 1:grain_idx.nof_pairs
        % Let's do A first
        hklsp = grain_idx.hklsp(:, ii_p)';
        hklsp = hklsp(ones(num_ab_entries, 1), :);

        idA = grain_idx.difspotidA(ii_p);

        cand_indx = find(all(gr.allblobs(det_ind).hklsp == hklsp, 2));
        th_uvws = gr.allblobs(det_ind).detector.uvw(cand_indx, :);
        exp_uvw = [seg_spots.CentroidX(idA), seg_spots.CentroidY(idA), seg_spots.CentroidImage(idA)];
        exp_uvw = exp_uvw(ones(numel(cand_indx), 1), :);

        [~, uvw_dist] = gtMathsNormalizeVectorsList(th_uvws - exp_uvw);
        [~, uvw_indx] = min(uvw_dist);
        included = [included, cand_indx(uvw_indx)];

        % Let's do B now
        hklsp = - grain_idx.hklsp(:, ii_p)';
        hklsp = hklsp(ones(num_ab_entries, 1), :);

        idB = grain_idx.difspotidB(ii_p);

        cand_indx = find(all(gr.allblobs(det_ind).hklsp == hklsp, 2));
        th_uvws = gr.allblobs(det_ind).detector.uvw(cand_indx, :);
        exp_uvw = [seg_spots.CentroidX(idB), seg_spots.CentroidY(idB), seg_spots.CentroidImage(idB)];
        exp_uvw = exp_uvw(ones(numel(cand_indx), 1), :);

        [~, uvw_dist] = gtMathsNormalizeVectorsList(th_uvws - exp_uvw);
        [~, uvw_indx] = min(uvw_dist);
        included = [included, cand_indx(uvw_indx)];

        difspotID = [difspotID, idA, idB];
    end

    [included, s_indx] = sort(included);
    difspotID = difspotID(s_indx);
end