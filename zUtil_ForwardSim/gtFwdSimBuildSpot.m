function [spot, bb] = gtFwdSimBuildSpot(spotid, bb, parameters, det_index, info)
% BUILDSPOT  Builds the spot image starting from the difspotID
%   [spot, bb] = gtFwdSimBuildSpot(spotid, [bb], [parameters], [det_index], [info])
%
% spotid     : difspotID
% parameters : parameters.mat
% volXsize   : volume Xsize for normalization {1}
% volYsize   : volume Ysize for normalization {1}
% bb         : bounding box from DB
% info       : edf image header info

    if (~exist('det_index', 'var') || isempty(det_index))
        det_index = 1;
    end
    if (~exist('parameters', 'var') || isempty(parameters))
        parameters = gtLoadParameters();
    end

    if (~exist('info', 'var') || isempty(info))
        difspotfile = fullfile(parameters.acq(det_index).dir, '2_difspot', '00000', 'difspot00001.edf');
        if exist(difspotfile, 'file');
            info = edf_info(difspotfile);
        else
            info = [];
        end
    end

    if (exist('bb', 'var') && ~isempty(bb))
        info.dim_1 = bb(3);
        info.dim_2 = bb(4);
    end
    [spot, bb] = gtGetSummedDifSpot(spotid, parameters, true, det_index, info);

    % here we force the intensity to be the same in all spots
    spot = spot / sum(spot(:));
end
