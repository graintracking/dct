function verts = gtFwdSimComputeCircumscribingPolyhedron(gr_center, projvecs, omegas, bb, parameters, det_index, criterion, display_figure, gr_shift_lab)
% verts = gtFwdSimComputeCircumscribingPolyhedron(gr_center, d_beam_dirs, omegas, bb, parameters, det_index, display_figure, gr_shift_lab)
%
% bb is [x_origin, y_origin, x_size, y_size]
%
% We can think of spot U/V edges like planes that cut the space in a
% polyhedron when they intersect, having the grain volume on the inside.
%
% Input:
%   gr_center : <1 x 3> vector in sample coordinates
%   projvecs : <n x 3> vector in sample coordinates
%   omegas : <n> vector
%   bb : <n x 4> vector in detector coordinates
%   parameters : dataset parameters
%

    if (~exist('det_index', 'var'))
        det_index = 1;
    end
    if (~exist('criterion', 'var'))
        criterion = 'convhull';
    end
    if (~exist('display_figure', 'var'))
        display_figure = false;
    end
    if (~exist('gr_shift_lab', 'var'))
        gr_shift_lab = zeros(numel(omegas), 3);
    end

    labgeo = parameters.labgeo;
    samgeo = parameters.samgeo;
    recgeo = parameters.recgeo(det_index);
    detgeo = parameters.detgeo(det_index);

    % All functions that use the same omegas, need the same rotation tensors:
    rot_comp_w = gtMathsRotationMatrixComp(labgeo.rotdir', 'col');
    rot_s2l_w = gtMathsRotationTensor(omegas, rot_comp_w);

    gc_pos_rec = gtGeoSam2Sam(gr_center, samgeo, recgeo, true);
    gc_pos_rec = gc_pos_rec(ones(numel(omegas), 1), :);

    % Taking the four corners: ctl = corner top left, cbr = corner bottom
    % right, ...
    % Here we compute their positions in the lab coordinates
    ctl_pos_lab = gtGeoDet2Lab([bb(:, 1), bb(:, 2)], detgeo, false) - gr_shift_lab;
    ctr_pos_lab = gtGeoDet2Lab([(bb(:, 1) + bb(:, 3)), bb(:, 2)], detgeo, false) - gr_shift_lab;
    cbl_pos_lab = gtGeoDet2Lab([bb(:, 1), (bb(:, 2) + bb(:, 4))], detgeo, false) - gr_shift_lab;
    cbr_pos_lab = gtGeoDet2Lab([(bb(:, 1) + bb(:, 3)), (bb(:, 2) + bb(:, 4))], detgeo, false) - gr_shift_lab;

    % positions in the reconstruction coordinates
    ctl_pos_rec = gtGeoLab2Sam(ctl_pos_lab, rot_s2l_w, labgeo, recgeo, false);
    ctr_pos_rec = gtGeoLab2Sam(ctr_pos_lab, rot_s2l_w, labgeo, recgeo, false);
    cbl_pos_rec = gtGeoLab2Sam(cbl_pos_lab, rot_s2l_w, labgeo, recgeo, false);
    cbr_pos_rec = gtGeoLab2Sam(cbr_pos_lab, rot_s2l_w, labgeo, recgeo, false);

    projvecs_rec = gtGeoSam2Sam(projvecs, samgeo, recgeo, true);
    projvecs_rec = gtMathsNormalizeVectorsList(projvecs_rec);

    % computing now the closest points on the projvec lines from the four
    % corners to the grain center
    dot_ctl_gc = sum((gc_pos_rec - ctl_pos_rec) .* projvecs_rec, 2);
    dot_ctr_gc = sum((gc_pos_rec - ctr_pos_rec) .* projvecs_rec, 2);
    dot_cbl_gc = sum((gc_pos_rec - cbl_pos_rec) .* projvecs_rec, 2);
    dot_cbr_gc = sum((gc_pos_rec - cbr_pos_rec) .* projvecs_rec, 2);

    ctl_pos_rec = ctl_pos_rec + projvecs_rec .* dot_ctl_gc(:, [1 1 1]);
    ctr_pos_rec = ctr_pos_rec + projvecs_rec .* dot_ctr_gc(:, [1 1 1]);
    cbl_pos_rec = cbl_pos_rec + projvecs_rec .* dot_cbl_gc(:, [1 1 1]);
    cbr_pos_rec = cbr_pos_rec + projvecs_rec .* dot_cbr_gc(:, [1 1 1]);

    % Recentering to the grain center position
    ctl_pos_rec = ctl_pos_rec - gc_pos_rec;
    ctr_pos_rec = ctr_pos_rec - gc_pos_rec;
    cbl_pos_rec = cbl_pos_rec - gc_pos_rec;
    cbr_pos_rec = cbr_pos_rec - gc_pos_rec;

    % computing plane normals
    pv_t_rec = gtMathsCross(projvecs_rec, ctr_pos_rec - ctl_pos_rec);
    pv_b_rec = gtMathsCross(projvecs_rec, cbr_pos_rec - cbl_pos_rec);
    pu_l_rec = gtMathsCross(projvecs_rec, ctr_pos_rec - cbr_pos_rec);
    pu_r_rec = gtMathsCross(projvecs_rec, ctl_pos_rec - cbl_pos_rec);

    pv_t_rec = gtMathsNormalizeVectorsList(pv_t_rec);
    pv_b_rec = gtMathsNormalizeVectorsList(pv_b_rec);
    pu_l_rec = gtMathsNormalizeVectorsList(pu_l_rec);
    pu_r_rec = gtMathsNormalizeVectorsList(pu_r_rec);

    % Computing displacements
    pv_t_rec_norm = sum(ctr_pos_rec .* pv_t_rec, 2);
    pv_b_rec_norm = sum(cbl_pos_rec .* pv_b_rec, 2);
    pu_l_rec_norm = sum(ctl_pos_rec .* pu_l_rec, 2);
    pu_r_rec_norm = sum(cbr_pos_rec .* pu_r_rec, 2);

    % We now compute the norm of the vector from the grain_center to
    % the closest point on the plane normals
    pv_t_rec = pv_t_rec_norm(:, [1 1 1]) .* pv_t_rec;
    pv_b_rec = pv_b_rec_norm(:, [1 1 1]) .* pv_b_rec;
    pu_l_rec = pu_l_rec_norm(:, [1 1 1]) .* pu_l_rec;
    pu_r_rec = pu_r_rec_norm(:, [1 1 1]) .* pu_r_rec;

    switch (criterion)
        case 'inscribed_polyhedron'
            verts = gtMathsGetPolyhedronVerticesFromPlaneNormals( ...
                [pv_t_rec; pv_b_rec; pu_l_rec; pu_r_rec] );
        case 'circumscribed_polyhedron'
            tot_vecs = [pv_t_rec; pv_b_rec; pu_l_rec; pu_r_rec];
            k = convhull(tot_vecs);
            verts = gtMathsGetPolyhedronVerticesFromPlaneNormals(tot_vecs(k, :));
        case 'convhull'
            tot_points = [ctl_pos_rec; ctr_pos_rec; cbl_pos_rec; cbr_pos_rec];
            k = convhull(tot_points);
            verts = tot_points(k, :);
        case 'box'
            tot_points = [ctl_pos_rec; ctr_pos_rec; cbl_pos_rec; cbr_pos_rec];
            min_points = min(tot_points, [], 1);
            max_points = max(tot_points, [], 1);
            verts = [min_points; ...
                [min_points(1:2), max_points(3)]; ...
                [min_points(1), max_points(2), min_points(3)]; ...
                [min_points(1), max_points(2:3)]; ...
                [max_points(1), min_points(2:3)]; ...
                [max_points(1), min_points(2), max_points(3)]; ...
                [max_points(1:2), min_points(3)]; ...
                max_points ];
    end

    if (display_figure)
        f = figure();
        ax = axes('parent', f);
        hold(ax, 'on');
        grid(ax, 'on');
        scatter3(ax, verts(:, 1), verts(:, 2), verts(:, 3), 30, 'b', 'filled')
        hold(ax, 'off');

        f = figure();
        ax = axes('parent', f);
        hold(ax, 'on');
        grid(ax, 'on');
        scatter3(ax, 0, 0, 0, 30, 'r', 'filled');
        for ii = 1:numel(omegas)
            disp(ii)
            quiver3(ax, 0, 0, 0, projvecs_rec(ii, 1), projvecs_rec(ii, 2), projvecs_rec(ii, 3));

            scatter3(ax, ctl_pos_rec(ii, 1), ctl_pos_rec(ii, 2), ctl_pos_rec(ii, 3), 20, 'b');
            scatter3(ax, ctr_pos_rec(ii, 1), ctr_pos_rec(ii, 2), ctr_pos_rec(ii, 3), 20, 'b');

%            quiver3(ax, 0, 0, 0, pv_t_rec(ii, 1), pv_t_rec(ii, 2), pv_t_rec(ii, 3));
%            pause

            scatter3(ax, cbl_pos_rec(ii, 1), cbl_pos_rec(ii, 2), cbl_pos_rec(ii, 3), 20, 'b');
            scatter3(ax, cbr_pos_rec(ii, 1), cbr_pos_rec(ii, 2), cbr_pos_rec(ii, 3), 20, 'b');

            quiver3(ax, 0, 0, 0, pv_b_rec(ii, 1), pv_b_rec(ii, 2), pv_b_rec(ii, 3));
%            pause

            quiver3(ax, 0, 0, 0, pu_l_rec(ii, 1), pu_l_rec(ii, 2), pu_l_rec(ii, 3));
            quiver3(ax, 0, 0, 0, pu_r_rec(ii, 1), pu_r_rec(ii, 2), pu_r_rec(ii, 3));
            pause
        end
        hold(ax, 'off');
    end
end