function bl = gtFwdSimBuildDifstackBlobs(blobids, parameters, stackUSize, stackVSize, det_index)
% BUILDDIFSTACKSPOTS
%   bl = gtFwdSimBuildDifstackBlobs(spotids, parameters, stackUSize, stackVSize, det_index)
%
% blobids    : spotid from grain
% parameters : parameters.mat
% stackUSize :
% stackVSize :
% det_index  : Detector index
%
% bl         :

    if (~exist('det_index', 'var') || isempty(det_index))
        det_index = 1;
    end

    num_blobs = numel(blobids);

    bl(1:num_blobs) = gtFwdSimBlobDefinition();

    for ii = 1:num_blobs
        [blob_vol, blob_bb, blob_mask, blob_int] = gtFwdSimBuildBlob(blobids(ii), parameters, det_index);

        % Transposing to keep the same convention as spots
        blob_vol = permute(blob_vol, [2 1 3]);
        blob_mask = permute(blob_mask, [2 1 3]);
        blob_bb = blob_bb([2 1 3 5 4 6]);

        bl(ii).mbbsize = blob_bb(4:6);
        bl(ii).mbbu = [blob_bb(1), blob_bb(1) + blob_bb(4) - 1];
        bl(ii).mbbv = [blob_bb(2), blob_bb(2) + blob_bb(5) - 1];
        bl(ii).mbbw = [blob_bb(3), blob_bb(3) + blob_bb(6) - 1];

        % Essentially zero pads the blob to make it fit into ASTRA's
        % diffraction stack
        shifts_blob = gtFwdSimGetStackShifts(stackUSize, stackVSize, blob_bb, false);
        shifts = [shifts_blob.u, shifts_blob.v, 1];

        % We are applying a padding of one slice (per side) on the w
        % diretion
        blob_size_im = [stackUSize, stackVSize, blob_bb(6)+2];
        blob_bb_im = [blob_bb(1:3) - shifts, blob_size_im];

        bl(ii).intm = gtPlaceSubVolume( zeros(blob_size_im, 'single'), ...
            blob_vol, shifts);

        bl(ii).mask = gtPlaceSubVolume( false(blob_size_im), ...
            uint8(blob_mask), shifts);

        bl(ii).bbsize = blob_size_im;

        bl(ii).bbuim = [blob_bb_im(1), blob_bb_im(1) + blob_bb_im(4) - 1];
        bl(ii).bbvim = [blob_bb_im(2), blob_bb_im(2) + blob_bb_im(5) - 1];
        bl(ii).bbwim = [blob_bb_im(3), blob_bb_im(3) + blob_bb_im(6) - 1];

        bl(ii).intensity = blob_int;
    end
end

