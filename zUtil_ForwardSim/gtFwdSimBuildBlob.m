function [blob, bb, mask, bint] = gtFwdSimBuildBlob(candidateID, parameters, det_index)
% FUNCTION  Builds the spot image starting from the difspotID
%   [spot, bb, mask] = gtFwdSimBuildBlob(candidateID, parameters, det_index)
%
% blobid     : candidateID
% parameters : parameters.mat

    if (~exist('det_index', 'var') || isempty(det_index))
        det_index = 1;
    end

    base_dir = fullfile(parameters.acq(det_index).dir, '2_difblob');
    try
        [blob, bb, mask] = gtReadBlobFromHDF5(base_dir, candidateID);
    catch
        [blob, bb] = gtReadBlobFromHDF5(base_dir, candidateID);
        mask = true(size(blob));
    end
    % Let's filter NaNs out
    mask(isnan(blob)) = false;

    blob(blob < 0) = 0;

    % Let's compute blob intensity
    bint = sum(blob(mask));

    % here we force the intensity to be the same in all spots to 1
    blob = blob / bint;

    if (~isfield(parameters.rec.grains, 'use_mask') || parameters.rec.grains.use_mask)
        blob(~mask) = 0;
    end
end
