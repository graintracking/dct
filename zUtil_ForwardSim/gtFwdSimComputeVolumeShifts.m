function shifts = gtFwdSimComputeVolumeShifts(proj, parameters, vol_size, det_ind)
    if (~exist('det_ind', 'var') || isempty(det_ind))
        det_ind = 1;
    end
    if (~exist('vol_size', 'var') || isempty(vol_size))
        vol_size = [proj(det_ind).vol_size_y, proj(det_ind).vol_size_x, proj(det_ind).vol_size_z];
    end
    shifts = round((parameters.acq(det_ind).bb([3 3 4]) - vol_size) / 2 + proj(det_ind).centerpix) + 1;
end
