function [candidateIDs, cands_info] = gtFwdSimFindCandidates(spot_props, gr, spot_index, segmentedSpots, parameters, det_ind)
% FINDCANDIDATES
% [candidateIDs, spotsCands] = gtFwdSimFindCandidates(spot_props, gr, spot_index, segmentedSpots, parameters, det_index)
%
% considers image number, then position, then area
% uvw : spot position
%
% at this stage we want to include spots which may overlap with the spot we are looking for.
% the omega difference of candidate spots must be smaller than max_w_offset_im,
% the size of the candidate spot must be at least half the minimum spot size
% the candidate spot must cover the predicted spot position...

    if (~exist('det_index', 'var') || isempty(det_ind))
        det_ind = 1;
    end
    if isfield(parameters.fsim, 'allowed_dist')
        allowed_dist = parameters.fsim.allowed_dist;
    else
        allowed_dist = 5;
    end
    spots_info = segmentedSpots(det_ind);

    uvw = gr.allblobs(det_ind).detector.uvw(spot_index, :);

    % centroid distances
    dist_u_pix = abs(spots_info.BoundingBoxXorigin + spots_info.BoundingBoxXsize / 2 - uvw(1));
    dist_v_pix = abs(spots_info.BoundingBoxYorigin + spots_info.BoundingBoxYsize / 2 - uvw(2));
    dist_w_im = abs(spots_info.CentroidImage - uvw(3));

    dist_u_ok = dist_u_pix < max(allowed_dist, (spots_info.BoundingBoxXsize + spot_props.XsizeMin / 2) / 2);
    dist_v_ok = dist_v_pix < max(allowed_dist, (spots_info.BoundingBoxYsize + spot_props.YsizeMin / 2) / 2);

    dist_w_ok = dist_w_im <=  spot_props.max_w_offset_im;

    if (isfield(spot_props, 'max_n_offset') && ~isempty(spot_props.max_n_offset))
        labgeo = parameters.labgeo;
        samgeo = parameters.samgeo;

        omstep = gtAcqGetOmegaStep(parameters, det_ind);

        spots_w = spots_info.CentroidImage * omstep;
        gr_center = gr.center(ones(numel(spots_w), 1), :);
        gr_pos = gtGeoSam2Lab(gr_center, spots_w, labgeo, samgeo, false);

        spots_diff_vec = gtMathsNormalizeVectorsList(spots_info.ScatteringVector - gr_pos);

        spots_eta = gtGeoEtaFromDiffVec(spots_diff_vec, labgeo);
        spots_theta = gtGeoThetaFromDiffVec(spots_diff_vec, labgeo);

        n = gr.allblobs(det_ind).eta(spot_index);
        t = gr.allblobs(det_ind).theta(spot_index);

        dist_eta_ok = abs(spots_eta - n) < spot_props.max_n_offset_deg;
        dist_theta_ok = abs(spots_theta - t) < spot_props.max_t_offset_deg;

        position_ok = dist_w_ok & dist_eta_ok & dist_theta_ok;
    else
        position_ok = dist_w_ok & dist_u_ok & dist_v_ok;
    end

    % candidates must have a minimum and maximum size
    size_x_min_ok = spots_info.BoundingBoxXsize >= spot_props.XsizeMin;
    size_x_max_ok = spots_info.BoundingBoxXsize <= spot_props.XsizeMax;
    size_y_min_ok = spots_info.BoundingBoxYsize >= spot_props.YsizeMin;
    size_y_max_ok = spots_info.BoundingBoxYsize <= spot_props.YsizeMax;

    area_min_ok = size_x_min_ok & size_y_min_ok;
    area_max_ok = size_x_max_ok & size_y_max_ok;
    image_area_ok = area_min_ok & area_max_ok;

    candidateIDs = find(position_ok & image_area_ok);

    cands_info = sfGetTableIndexes(segmentedSpots, candidateIDs);
    cands_info.thr.area_min_thr = spot_props.XsizeMin .* spot_props.YsizeMin;
    cands_info.thr.area_max_thr = spot_props.XsizeMax .* spot_props.YsizeMax;
    cands_info.thr.w_dist_im_thr = spot_props.max_w_offset_im;
    cands_info.thr.u_dist_pix_thr = spot_props.XsizeMax / 2;
    cands_info.thr.v_dist_pix_thr = spot_props.YsizeMax / 2;
    cands_info.diff.dist_u_pix    = dist_u_pix;
    cands_info.diff.dist_v_pix    = dist_v_pix;
    cands_info.diff.dist_w_im     = dist_w_im;
    cands_info.diff.dist_u_ok     = dist_u_ok;
    cands_info.diff.dist_v_ok     = dist_v_ok;
    cands_info.diff.dist_w_ok     = dist_w_ok;
    if (isfield(spot_props, 'max_n_offset') && ~isempty(spot_props.max_n_offset))
        cands_info.diff.dist_n_ok = dist_eta_ok;
        cands_info.diff.dist_t_ok = dist_theta_ok;
    else
        cands_info.diff.dist_n_ok = [];
        cands_info.diff.dist_t_ok = [];
    end
    cands_info.diff.position_ok   = position_ok;
    cands_info.diff.area_min_ok   = area_min_ok;
    cands_info.diff.area_max_ok   = area_max_ok;
    cands_info.diff.area_ok       = area_min_ok & area_max_ok;

    % This is for completeness estimation: even if the candidate is not
    % good, but if it has a minimum size, it might just be an overlap
    cands_info.found_intensity = any(position_ok(area_min_ok));
end

function spotsCands = sfGetTableIndexes(segmentedSpots, candidateIDs)
% spotsCands = sfGetTableIndexes(segmentedSpots, candidateIDs)

    if ~isempty(candidateIDs)
        spotsCands = structfun(@(x) x(candidateIDs, :), segmentedSpots, 'UniformOutput', false);
    else
        spotsCands = [];
    end
end % sfGetTableIndexes
