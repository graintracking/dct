function blob = gtFwdSimBlobDefinition(type, num_blobs)
% function blob = gtFwdSimBlobDefinition(type, num_blobs)
%

    if (~exist('type', 'var') || isempty(type))
        type = 'blob';
    end

    switch (type)
        case 'sf_w'
            blob = struct( ...
                'intm',      [], ...          % The normalized shape function
                'bbwim',     zeros(0, 2), ... % Shape function's limits in W
                'bbsize',    zeros(0, 1), ... % Shape function's BoundingBox (includes margins)
                'interp_w',   zeros(0, 1) );   % Size of the W interpolation (num_interp)
        case 'sf_p'
            blob = struct( ...
                'intm',      [], ...          % The normalized shape function
                'bbpim',     zeros(0, 2), ... % Shape function's limits in P
                'bbsize',    zeros(0, 1), ... % Shape function's BoundingBox (includes margins)
                'interp_p',   zeros(0, 1) );   % Size of the P interpolation (num_interp)
        case 'sf_n'
            blob = struct( ...
                'intm',      [], ...          % The normalized shape function
                'bbnim',     zeros(0, 2), ... % Shape function's limits in N
                'bbsize',    zeros(0, 1) );   % Shape function's BoundingBox (includes margins)
        case 'sf_nw'
            blob = struct( ...
                'intm',      [], ...          % The normalized shape function
                'bbnim',     zeros(0, 2), ... % Shape function's limits in eta
                'bbwim',     zeros(0, 2), ... % Shape function's limits in W
                'bbsize',    zeros(0, 2), ... % Shape function's BoundingBox (includes margins)
                'interp_w',   zeros(0, 1) );   % Size of the W interpolation (num_interp)
        case {'blob', 'sf_uvw'}
            blob = struct( ...
                'intm',      [], ...          % The normalized blob
                'mask',      [], ...          % The segmentation mask
                'nvar',      [], ...          % noise variances of the pixels of the blob
                'bbuim',     zeros(0, 2), ... % Image BB on U
                'bbvim',     zeros(0, 2), ... % Image BB on V
                'bbwim',     zeros(0, 2), ... % Image BB on W <- Not strictly w
                'bbsize',    zeros(0, 3), ... % Image BoundingBox (includes margins)
                'mbbu',      zeros(0, 2), ... % Real (Measured) Blob BB on U
                'mbbv',      zeros(0, 2), ... % Real (Measured) Blob BB on V
                'mbbw',      zeros(0, 2), ... % Real (Measured) Blob BB on W <- Not strictly w
                'mbbsize',   zeros(0, 3), ... % Real (Measured) Blob Bounding Box
                'intensity', zeros(0, 1) );   % Blob original intensity
        case {'blob_topo', 'sf_uvp'}
            blob = struct( ...
                'intm',      [], ...          % The normalized blob
                'mask',      [], ...          % The segmentation mask
                'nvar',      [], ...          % noise variances of the pixels of the blob
                'bbuim',     zeros(0, 2), ... % Image BB on U
                'bbvim',     zeros(0, 2), ... % Image BB on V
                'bbpim',     zeros(0, 2), ... % Image BB on P (phi)
                'bbsize',    zeros(0, 3), ... % Image BoundingBox (includes margins)
                'mbbu',      zeros(0, 2), ... % Real (Measured) Blob BB on U
                'mbbv',      zeros(0, 2), ... % Real (Measured) Blob BB on V
                'mbbp',      zeros(0, 2), ... % Real (Measured) Blob BB on P (phi)
                'mbbsize',   zeros(0, 3), ... % Real (Measured) Blob Bounding Box
                'intensity', zeros(0, 1) );   % Blob original intensity
        otherwise
            error('gtFwdSimBlobDefinition:wrong_argument', ...
                'Unnown blob tye: "%s"', type)
    end

    if (exist('num_blobs', 'var') && ~isempty(num_blobs))
        blob(2:num_blobs) = blob;
    end
end
