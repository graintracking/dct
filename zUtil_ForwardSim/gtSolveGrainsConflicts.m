function [sample_mod,grains_mod] = gtSolveGrainsConflicts(thr_num,update_flag)
% GTSOLVEGRAINSCONFLICTS
%     [sample_mod,grains_mod] = gtSolveGrainsConflicts([thr_num],[update_flag])
%     -------------------------------------------------------------------------
%     To be run in the analysis directory after gtAnalyseGrainsConflicts
%     Several flags are used and saved in the analyser:
%       grains_conflicts{phaseid}.analyser{grainid}
%
%     Flag:
%         -2 - completeness < mean - 3*std
%         -1 - threshold = 0
%          0 - no conflict
%          1 - conflict and threshold < 0
%          2 - conflict and boundingBox = [0 0 0 0 0 0]
%          3 - conflict and totspots <= thr_num
%         -3 - conflict and totspots > thr_num
%          4 - conflict and modified spots
%          5 - conflict and remaining
%          6 - conflict
%
%     INPUT:
%       thr_num     = <double>     minimum number of spots to be discarded {5}
%       update_flag = <logical>    flag to save the output in 4_grains/out.mat,
%                                  and update 4_grains/sample.mat,
%                                  4_grains/spot2grain.mat and
%                                  4_grains/grains_conflicts.mat {false}
%
%     OUTPUT:
%       sample_mod  = <struct>     updated sample.mat
%       grains_mod  = <cell>       updated grains_conflicts.mat
%
%
%     Version 004 27-03-2014 by LNervo
%       Correct and add flags 3,-3,4,5,6
%
%     Version 003 15-06-2012 by LNervo

if ~exist('thr_num','var') || isempty(thr_num)
    thr_num = 5;
end
if ~exist('update_flag','var') || isempty(update_flag)
    update_flag = false;
end


out              = [];
sample           = [];
spot2grain       = [];
spot2phase       = [];
grains_conflicts = [];

bkpdir  = fullfile('4_grains','archive');
fname0  = fullfile(bkpdir,'%s');

sample_root = fullfile('4_grains','sample.mat');
sampleFile  = gtLastFileName(sprintf(fname0,['sample_' date() '_']),'new');

spot_root   = fullfile('4_grains','spot2grain.mat');
spotFile    = gtLastFileName(sprintf(fname0,['spot2grain_' date() '_']),'new');

grains_root = fullfile('4_grains','grains_conflicts.mat');
grainsFile  = gtLastFileName(sprintf(fname0,['grains_conflicts_' date() '_']),'new');

out_root    = fullfile('4_grains','out.mat');
outFile     = gtLastFileName(sprintf(fname0,['out_' date() '_']),'new');

logName     = strrep(outFile,'.mat','.log');
disp(['Log filename in: ' logName])
diary(logName)

load(sample_root);
load(spot_root);
load(grains_root);

num_phases = length(sample.phases);

for phaseid = 1 : num_phases

    redo_rec      = false;
    redo_assemble = false;

    phase  = sample.phases{phaseid};
    grains = grains_conflicts{phaseid};

    fprintf('*************************\n')
    fprintf('***     PHASE: %02d     ***\n',phaseid)
    fprintf('*************************\n')

    % Not in sample.mat any more!
%     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     % get grains with threshold < 0 : deselect
%     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     grainIDs_thr = phase.getGrainsWithFieldValueLessThan('threshold',0);
%     if ~isempty(grainIDs_thr)
%         % set phase.selectedGrains = false
%         arrayfun(@(array) phase.setSelected(array,false),grainIDs_thr);
%         fprintf('\n  Deselected from sample.mat %d grains with threshold < 0\n',length(grainIDs_thr))
%         arrayfun(@(array) grains.setFlag(array,1),grainIDs_thr);
%         fprintf('  Flag = 1 for %d grains with threshold < 0\n',length(grainIDs_thr))
% 
%         % store information
%         out{phaseid}.threshold_negat    = grainIDs_thr;
%         disp('    Saved list in out{phaseid}.threshold_negat')
% 
%         disp('  <<< Check the grain segmentation for those grains >>>')
%         redo_rec = false;
%         reco_assemble = true;
%     end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % bbox empty : deselect
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    grainIDs_bbox0  = find(arrayfun(@(num) ~isempty(intersect(phase.getBoundingBox(num),[0 0 0 0 0 0],'rows')),1:length(phase.grains)));
    if ~isempty(grainIDs_bbox0)
        % set phase.selectedGrains = false
        arrayfun(@(array) phase.setSelected(array,false),grainIDs_bbox0);
        fprintf('\n  Deselected %d grains with boundingBox = [0 0 0 0 0 0]\n',length(grainIDs_bbox0))
        arrayfun(@(array) grains.setFlag(array,2),grainIDs_bbox0);
        fprintf('  Flag = 2 for %d grains with boundingBox = [0 0 0 0 0 0]\n',length(grainIDs_bbox0))

        % store information
        out{phaseid}.bbox_empty         = grainIDs_bbox0;
        disp('    Saved list in out{phaseid}.bbox_empty')

        disp('  <<< Check grain segmentation for those grains >>>')
        redo_rec = false;
        reco_assemble = true;
    end

    % Not in sample.mat any more!
%     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     % threshold = 0 : warning
%     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     grainIDs_thr0 = phase.getGrainsWithFieldValueEqualsTo('threshold',0);
%     if ~isempty(grainIDs_thr0)
%         fprintf('\n  There are %d grains with threshold = 0\n',length(grainIDs_thr0))
%         if length(grainIDs_thr0) == length(phase.grains)
%             disp('  <<< Probably you did not run gtSetupReconstruction yet! >>>')
%         else
%             arrayfun(@(array) grains.setFlag(array,-1),grainIDs_thr0);
%             fprintf('  Flag = -1 for %d grains with threshold = 0\n',length(grainIDs_thr0))
% 
%             % store information
%             out{phaseid}.threshold_zero     = grainIDs_thr0;
%             disp('    Saved list in out{phaseid}.threshold_zero')
% 
%             disp('  <<< Remember to run again gtReconstructGrain and gtChangeThreshold3D for those grains >>>')
%             redo_rec = true;
%             redo_assemble = true;
%         end
%     end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % (completeness - mean ) < 3*std : warning
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    thr_com = mean(phase.completeness(:))-3*std(phase.completeness(:));
    grainIDs_com = phase.getGrainsWithCompletenessLessThan(thr_com)';
    if ~isempty(grainIDs_com)
        fprintf('\n  Mean(completeness): %f\n',mean(phase.completeness(:)))
        fprintf('  Found %d grains with completeness < %f\n',length(grainIDs_com),thr_com)
        arrayfun(@(array) grains.setFlag(array,-2),grainIDs_com);
        fprintf('  Flag = -2 for %d grains with completeness < mean - 3*std\n',length(grainIDs_com))

        % store information
        out{phaseid}.completeness_low   = grainIDs_com;
        disp('    Saved list in out{phaseid}.completeness_low')
        disp('  <<< Please check the goodness of those grains manually >>>')
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % get deselected grains
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    grainIDs_desel = phase.getDeselectedGrains();
    if ~isempty(grainIDs_desel)
        fprintf('\n  Found in total deselected %d grains\n',length(grainIDs_desel))
        % don't care about deselected grains??
        arrayfun(@(array) grains.resetGrainId(array),grainIDs_desel);
        fprintf('  Removed conflicts from %d deselected grains\n',length(grainIDs_desel))

        % store information
        out{phaseid}.deselected_grains  = grainIDs_desel';
        disp('    Saved list in out{phaseid}.deselected_grains')
    end


    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %              GRAINS_CONFLICTS   -   ONLY REMAINING CONFLICTS            %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %   handle difspots in conflict from grains{grainid} still in conflicts   %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    grainIDs = grains.getHasConflictsTrue();
    if ~isempty(grainIDs)
        grainIDs_orig = grainIDs;
        fprintf('\n  There are %d grains with conflicts\n',length(grainIDs_orig));

        % store information
        out{phaseid}.conflicts_orig     = grainIDs_orig;
        disp('    Saved list in out{phaseid}.conflicts_orig')

        arrayfun(@(array) grains.buildIdSpot(array),grainIDs);
        totspots = arrayfun(@(array) grains.getTotSpots(array),grainIDs);
        difspotID = arrayfun(@(array) grains.getDifspotID(array),grainIDs,'UniformOutput',false);
        % separate grains by number of spots in common
        grains.setGrainsWithDifspotLessThan(thr_num,true);
        grains.setGrainsWithDifspotMoreThan(thr_num);
        fprintf('\nFound %d grains with less than %d spots in common per grain\n',length(grains.lessthr),thr_num)
        fprintf('Found %d grains with more than %d spots in common per grain\n',length(grains.overthr),thr_num)

        IDs = false(1,length(grainIDs));
        check = repmat({'n'},1,length(grainIDs));

        check(ismember(grainIDs, grains.lessthr)) = {'y'};
        IDs(ismember(grainIDs, grains.lessthr)) = true;

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % totspots <= thr_num : query
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        grainIDs_spots = grainIDs(IDs);
        fprintf('\n  Found %d grains with totspots <= %d\n',length(grainIDs_spots),thr_num)
        arrayfun(@(array) grains.setFlag(array,3),grainIDs_spots);
        fprintf('  Flag = 3 for %d grains with totspots <= thr_num\n',length(grainIDs_spots))

        % store information
        out{phaseid}.grains_lessthr     = grainIDs_spots;
        disp('    Saved list in out{phaseid}.grainIDs_lessthr')

        % query
        question = inputwdefault(sprintf('Do you want to deselect up to %d spots from %d grains automatically? [y/n]',...
            thr_num,length(find(IDs))),'y');
        if ~strcmpi(question,'y')
            % ask the user if ok to deselect spots from grains manually
            if length(find(IDs)) > 10
                fprintf('  Many grains (%d) with less than %d conflicts...\n',length(grainIDs_spots),thr_num)
                question = inputwdefault('    Do you want to keep/discard the spots in conflicts for those grains manually? [y/n]','n');
            else
                question = 'y';
            end
            if strcmpi(question,'y')
                check(IDs) = arrayfun(@(num,y) ...
                    inputwdefault(sprintf('(%2d) Do you want to deselect %3d/%3d spots from grain %4d? [y/n]',...
                        y,totspots(num),length(difspotID{num}),grainIDs(num)),check{num}),...
                  find(IDs),length(find(IDs)):-1:1,'UniformOutput',false);
            else
                check(IDs) = {'n'};
            end
        end

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % totspots > thr_num : query
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        grainIDs_extra = grainIDs(~IDs);
        fprintf('\n  Found %d grains with totspots > %d\n',length(grainIDs_extra),thr_num)
        arrayfun(@(array) grains.setFlag(array,-3),grainIDs_extra);
        fprintf('  Flag = -3 for %d grains with totspots > thr_num\n',length(grainIDs_extra))

        % store information
        out{phaseid}.grains_overthr     = grainIDs_extra;
        disp('    Saved list in out{phaseid}.grainIDs_overthr')

        % query
        check(~IDs) = arrayfun(@(num,y) ...
            inputwdefault(sprintf('(%2d) Do you want to deselect %3d/%3d spots from grain %4d? [y/n]',...
                y,totspots(num),length(difspotID{num}),grainIDs(num)),check{num}),...
          find(~IDs),length(find(~IDs)):-1:1,'UniformOutput',false);
        % convert to logical
        checkLogical = cellfun(@(x) strcmpi(x,'y'),check,'UniformOutput',false);
        checkLogical = [checkLogical{:}];

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % handle grains to modify : deselect spots
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        if any(checkLogical)
            redo_rec = true;
            redo_assemble = true;

            grainIDs_mod = grainIDs(checkLogical);
            fprintf('\n  Selected %d grains to be modified\n',length(grainIDs_mod))
            arrayfun(@(array) grains.setFlag(array,4),grainIDs_mod);
            fprintf('  Flag = 4 for %d modified grains\n',length(grainIDs_mod))

            % get ids for each grain with difspots in conflict
            idspots = arrayfun(@(array) grains.getIdSpotRow(array,1),grainIDs_mod,'UniformOutput',false);
            indexes = arrayfun(@(array) grains.getIdSpotRow(array,2),grainIDs_mod,'UniformOutput',false);

            % store information
            out{phaseid}.modified_grains    = grainIDs_mod;
            out{phaseid}.modified_difspots  = idspots;
            disp('    Saved list in out{phaseid}.modified_grains')

            conflictid = arrayfun(@(num) grains.getIdConflict(grainIDs_mod(num)),1:length(grainIDs_mod),'UniformOutput',false);
            isNotThere = arrayfun(@(num) ~ismember(conflictid{num},grainIDs_mod),1:length(conflictid),'UniformOutput',false);

            % store information
            out{phaseid}.isNotThere         = isNotThere;
            out{phaseid}.conflictid         = conflictid;
            disp('    Saved idconflict list in out{phaseid}.conflictid')
            disp('    Saved list of idconflict not present in modified grains in out{phaseid}.isNotThere')

            grainIDs_rest = grainIDs(~checkLogical);
            fprintf('\n  Remain %d grains to be modified\n',length(grainIDs_rest))
            arrayfun(@(array) grains.setFlag(array,5),grainIDs_rest);
            fprintf('  Flag = 5 for %d remaining grains\n',length(grainIDs_rest))

            % store information
            out{phaseid}.rest_grains        = grainIDs_rest;
            disp('    Saved list in out{phaseid}.rest_grains')

            % update phase in sample.mat
            arrayfun(@(num) ...
                phase.setSelectedDiffspotsIndexes(grainIDs_mod(num),indexes{num},false),...
              1:length(grainIDs_mod));

            % to be checked
            arrayfun(@(num) ...
                grains.deselectDifspotsFromGrain(grainIDs_mod(num),idspots{num},conflictid{num}),...
              1:length(grainIDs_mod));

            arrayfun(@(num) ...
                fprintf('  \nDeselected %d spots for grain %4d -> %s',...
                    length(indexes{num}),grainIDs_mod(num),num2str(idspots{num})),...
              1:length(grainIDs_mod));

            % update spot2grain and spot2phase
            output = gtUpdateSpot2Grain(grainIDs_mod,[idspots{:}],spot2grain,spot2phase);
            spot2grain = output.spot2grain;
            spot2phase = output.spot2phase;
            isNotRight = output.isNotRight;

            % store information
            out{phaseid}.isNotRight         = isNotRight;
            disp('       Saved list of spots in conflict not paired in out{phaseid}.isNotRight')
            disp('     <<< Please check those spots manually >>>')
        end

        grainIDs = grains.getHasConflictsTrue();
        fprintf('\n  There are still %d grains with conflicts\n',length(grainIDs));
        idspots_rest = arrayfun(@(array) grains.getIdSpotRow(array,1),grainIDs,'UniformOutput',false);

        % store information
        out{phaseid}.conflicts_grains   = grainIDs;
        out{phaseid}.conflicts_difspots = idspots_rest;
        disp('    Saved list in out{phaseid}.conflicts_grains')
        disp('    Saved difspot list in out{phaseid}.conflicts_difspots')
        disp(' ')
    end

    % saving
    if (update_flag)
        copyfile(spot_root,spotFile);
        save(spot_root,'spot2grain','spot2phase');
        disp(['Updated ' spot_root])
    else
        disp('spot2grain.mat has not been saved on disk.')
        disp('Remember that spot2grain.mat has not been updated after the analysis.')
    end

    % store information
    out{phaseid}.spot2grain         = {spot2grain,spot2phase};
    disp('    The updated spot2grain.mat is stored in out{phaseid}.spot2grain')
    disp(' ')

    % update sample structure for current phase
    sample.phases{phaseid}         = phase;
    grains_conflicts{phaseid}      = grains;

    if (redo_rec) && ~isempty(out{phaseid}.modified_grains)
        check = inputwdefault('Do you want to run gtReconstructGrain and gtChangeThreshold3D for those grains? [y/n]','n');
        if strcmpi(check,'y')
            disp('    It may take sometimes... Be patient :-)')
            diary('off')
            for jj = 1 : length(out{phaseid}.modified_grains)
                gtReconstructGrains(out{phaseid}.modified_grains(jj),out{phaseid}.modified_grains(jj),pwd(),phaseid);
                gtChangeThreshold3D(out{phaseid}.modified_grains(jj),out{phaseid}.modified_grains(jj),pwd(),phaseid);
            end
            diary('on')
            fprintf('\n  gtReconstructGrain and gtChangeThreshold3D have been executed for %d grains',out{phaseid}.modified_grains)
            redo_rec = false;
        end
    end
    if (redo_rec)
        disp(['  For phase ' num2str(phaseid) ' is recommended to reconstruct'])
        disp('    all the grains listed in out{phaseid}.modified_grains')
    end
    if (redo_assemble)
        disp(['  For phase ' num2str(phaseid) ' is recommended to run again the Assemble.'])
    end
    disp(' ')

end % end for phaseid

% saving
if exist(out_root,'file')
    copyfile(out_root,outFile);
end
save(out_root,'out','-v7.3');
disp(['Saved output in ' out_root])

if (update_flag)
    copyfile(sample_root,sampleFile);
    sample.saveToFile(sample_root);
    disp(['Saved updated sample' sample_root])

    copyfile(grains_root,grainsFile);
    save(grains_root,'grains_conflicts');
    disp(['Saved updated grains_conflicts in ' grains_root])
end

% stop diary
diary('off');

% function output
assignin('base','out',out)
disp('Assigned to base workspace variable ''out''')
if nargout == 2
    sample_mod = sample;
    grains_mod = grains_conflicts;

    if (~update_flag)
        disp('  The updated sample.mat is the output from this function: don''t loose it!')
        disp('  The updated grains_conflicts.mat is the second output from this function')
    end
elseif nargout == 1
    sample_mod = sample;
    if (~update_flag)
        disp('  The updated sample.mat is the output from this function: don''t loose it!')
    end

    assignin('base','grains_conflicts_mod',grains_conflicts);
    disp('Assigned to base workspace variable ''grains_conflicts_mod''')
elseif nargout == 0
    assignin('base','sample_mod',sample);
    assignin('base','grains_conflicts_mod',grains_conflicts);
    disp('Assigned to base workspace variables ''sample_mod'' and ''grains_conflicts_mod''')
end

end % end of function
