function id_error = gtFsimRunMissingGrains(phase_num,ind)
% GTFSIMRUNMISSINGGRAINS
%     id_error = gtFsimRunMissingGrains(phase_num,ind)
%     ------------------------------------------------
%     INPUT:
%       phase_num =
%       ind       =
%
%     OUTPUT:
%       id_error =

id_error=[];

for i=1:length(ind)
    out=gtForwardSimulate_v2(ind(i),ind(i),pwd,phase_num);
    if exist(fullfile( '4_grains',['phase_' sprintf('%02d',phase_num)],['grain_' sprintf('%04d.mat',ind(i))] ),'file')
        disp(['*** Done grain # ' num2str(ind(i)) '***'])
        disp(['    No. of grains left to be processed :    ' num2str(length(ind)-i)])
        %ind(i) = [];
    else
        disp(['Error with grain # ' num2str(ind(i))])
        id_error=[id_error; ind(i)];
    end

end

save id_error id_error
end % end of function
