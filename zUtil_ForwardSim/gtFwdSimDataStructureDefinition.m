function fwd_sim = gtFwdSimDataStructureDefinition(numspots)
    fwd_sim = struct( ...
        'flag', ones(numspots, 1), ... % see above
        'bb', zeros(numspots, 4), ... % difspot BoundingBox
        'cent', zeros(numspots, 1), ... % centroid distance from database
        'spotid', zeros(numspots, 1), ... % Mysql difspottable difspotID
        'difspotID', zeros(0, 1), ... % Mysql difspottable difspotID
        'check', false(numspots, 2), ...
        'intensity', zeros(numspots, 1), ... % Integrated blob intensity
        'avg_pixel_int', zeros(numspots, 1), ... % Average pixel intensity in blob
        'likelihood', zeros(numspots, 1), ... % Blob likelihood
        'cands', struct( ...
            'id', cell(numspots, 1), ...
            'likelihood_tot', cell(numspots, 1), ...
            'likelihood_proj_mask', cell(numspots, 1), ...
            'likelihood_dev_uv', cell(numspots, 1), ...
            'likelihood_dev_w', cell(numspots, 1) ), ...
        'cands_info', struct('info', cell(numspots, 1)), ...
        'uvw', zeros(numspots, 3), ... % CoM positions of difspots from difspottable
        'spot_type', char(zeros(numspots, 1)), ...
        'check_spots', struct('info', cell(numspots, 1)), ...
        'gv_verts', [] );
end
