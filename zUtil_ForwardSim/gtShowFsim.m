function grainOut = gtShowFsim(grain, phaseid, varargin)
% GTSHOWFSIM  Shows the forward simulation result for one grain
%     grainOut = gtShowFsim(grain, phaseid, varargin)
%     -----------------------------------------------
%     Usage:
%       gtShowFsim(grain.id, grain.phaseid)
%       gtShowFsim(grain{grain.id}, [], 'clims', [-200 500])
%           (in this case, phase ID is taken from grain)
%
%     INPUT:
%       grain     = <cell>/<double> grain info
%       phaseid   = <double>   phase number {1}
%
%     OPTIONAL INPUT (as in gtAppFsimDefaultPars()):
%       clims      = <double>   image color limits {0}
%       conflicts  = <logical>  true if check difspots in conflict {true}
%                               (needs 4_grains/grains_conflicts.mat)
%       fsimtype   = <string>   forward simulation structure name {'allblobs'}|'fwdsim'
%       fsimID     = <double>   number of fsim structure in grain
%                               It is related to the number of geometries
%                               you use and you have stored in the
%                               parameters file if needed {1}
%       replace    = <logical>  true to replace grain.full {true}
%       addspot    = <logical>  add new spots to full image {true}
%       viewblob   = <logical>  view difblob after checking raw images
%                               {false}
%       save_grain = <logical>  update 4_grains/phase_##/grain_####.mat
%                               {false}
%       verbose    = <logical>  true if printing comments {true}
%       resize     = <logical>  resize figure {false}
%       f_title    = <string>   Figure title {''}
%
%     OUTPUT:
%       grainOut   = <cell>     grain info updated
%
%     Version 004 12-05-2014 by LNervo

%     Version 003 16-01-2014 by LNervo
%
%     Version 002 13-12-2013 by LNervo
%       Added checkboxes for HKL families, spot flags;
%       Added conflicts box info

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% loading parameters
parameters = gtLoadParameters();

% application options
[app, vars_list] = gtAppFsimDefaultPars();

if ~isempty(varargin)
    [app, ~] = parse_pv_pairs(app, varargin);
else
    app.verbose = parameters.fsim.verbose;
    app = gtModifyStructure(app, vars_list, [], 'Options for displaying the Forward Sim:');
end
% verbosity
output = GtConditionalOutput(app.verbose);

if (~exist('phaseid', 'var') ||isempty(phaseid))
    phaseid = 1;
end

grainDirsModel = fullfile('4_grains', 'phase_%02d', 'grain_%04d.mat');
if isnumeric(grain)
    grain = load(sprintf(grainDirsModel, phaseid, grain));
    output.odisp(['Loaded grain from file ' sprintf(grainDirsModel, phaseid, grain.id)])
end

if (~isfield(grain, 'fwd_sim')) % for older versions of forward simulation
    grain.fwd_sim(app.det_index).spotid = grain.spotid;
    grain.fwd_sim(app.det_index).check = app.check;
    grain.fwd_sim(app.det_index).flag = grain.flag;
    grain.fwd_sim(app.det_index).difspotID = grain.spotid(grain.proj(app.det_index).included);
    grain.fwd_sim(app.det_index).uvw = [grain.uv_exp, grain.om_exp];
    grain.fwd_sim(app.det_index).bb = grain.bb_exp;
elseif (~isfield(grain.fwd_sim, 'difspotID'))
    if (isfield(grain, 'difspotidA') && isfield(grain, 'difspotidB'))
        grain.fwd_sim(app.det_index).difspotID = [grain.difspotidA, grain.difspotidB];
    else
        grain.fwd_sim(app.det_index).difspotID = grain.spotid(grain.proj(app.det_index).included);
    end
elseif (~isfield(grain.fwd_sim, 'difspotID'))
    gtError('', 'Missing field ''difspotID'' in grain... Quitting')
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Getting fields and updating grain
if (~isfield(parameters, 'detgeo'))
    [parameters.detgeo, parameters.labgeo] = gtGeoConvertLegacyLabgeo2Detgeo(parameters.labgeo);
end

spotsCommProps = gtFwdSimGetDiffspotsCommonProperties(grain, parameters, app.det_index);

uvw = grain.allblobs(app.det_index).detector.uvw;

if isfield(grain, 'ondet')
    ondet = grain.proj(app.det_index).ondet';
else
    ondet = gtFwdSimFindSpotsOnDetector(spotsCommProps, uvw(:, 1:2), parameters, app.det_index)';
    grain.proj(app.det_index).ondet = ondet';
end

if (~isfield(grain, 'full') || isempty(grain.full))
    if (isfield(grain, 'fwd_sim'))
        bb = grain.fwd_sim(app.det_index).bb;
    else
        bb = grain.bb_exp;
    end

    grain.full = sfBuildFull(grain.fwd_sim(app.det_index).difspotID, parameters, bb, app.det_index);
end

% taking fsim reflections on detector
uvw       = uvw(ondet, :);
omega     = grain.allblobs(app.det_index).omega(ondet);
hkl       = grain.allblobs(app.det_index).hkl(ondet, :);
thetatype = double(grain.allblobs(app.det_index).thetatype(ondet));

if (~isfield(grain, 'proj') || ~isfield(grain.proj, 'included'))
    grain.proj(app.det_index).included = find(grain.fwd_sim(app.det_index).flag >= 3);
end
% create table field in grain with included spot information
grain = gtFSimCreateGrainTable(grain, ...
    {'spotid', 'hklsp', 'uvw', 'flag', 'thetatype', 'hkl'}, ...
    app.det_index, false, app.verbose);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Check conflicts
if (app.conflicts)
    % look for output.odispots in conflicts
    if (exist(fullfile('4_grains', 'grains_conflicts.mat'), 'file'))
        % set up the conflict table data
        grain = gtCheckGrainConflicts(grain, phaseid, parameters, [], app.save_grain, app.verbose);
    else
        output.odisp('The file grains_conflicts.mat has not been found.')
        output.odisp('Just run gtAnalyseGrainsConflicts(true, false, true, true) to create it.')
    end % end hasConflictInfo
else
    output.odisp('No displaying conflicts info')
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% unique reflections
if (isfield(grain.fsimgui, 'used_fam'))
    used_fam = grain.fsimgui.used_fam;
else
    used_fam = find(parameters.cryst(phaseid).usedfam);
end
used_fam(~ismember(used_fam, unique(thetatype)')) = [];
grain.fsimgui.used_fam = used_fam;

num_used_families = numel(used_fam);

int_hkls = parameters.cryst(phaseid).int;
int_hkls = (int_hkls - min(int_hkls)) / (max(int_hkls) - min(int_hkls));
int_hkls = int_hkls(used_fam);

% added
grain.fsimgui.thtypes  = parameters.cryst(phaseid).thetatype(used_fam)';
grain.fsimgui.u_hkls   = parameters.cryst(phaseid).hkl(:, used_fam)';
grain.fsimgui.int_hkls = int_hkls;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% clustering depending on flag
flags_h_markers_uvw = cell(6, 1);
flags_h_info = cell(6, 6);
enable_vec = cell(6, 1);

leg_str = { ...
  '1: No intensity in raw images', ...
  '2: Intensity in raw images', ...
  '3: Segmented intensity not matching search criteria', ...
  '4: Difspot matching search criteria', ...
  '5: Indexed difspot as Friedel pair', ...
  '6: Spot claimed by another grain'};
for ii = 1 : length(leg_str)
    if ((ii == 6) && isfield(grain, 'conf_table') ...
            && ~isempty(grain.conf_table))
        ind = find(grain.conf_table.flag == 6);
    else
        ind = find(grain.fwd_sim(app.det_index).flag == ii);
    end
    if (~isempty(ind))
        flags_h_markers_uvw{ii} = uvw(ind, :);
        flags_h_info{ii, 1} = omega(ind);
        flags_h_info{ii, 2} = thetatype(ind);
        flags_h_info{ii, 3} = hkl(ind, :);
        flags_h_info{ii, 6} = true(length(ind), 1);
        enable_vec{ii} = 'on';
    else
        flags_h_markers_uvw{ii} = [-1 -1 -1];
        flags_h_info{ii, 1} = 0;
        flags_h_info{ii, 2} = 0;
        flags_h_info{ii, 3} = zeros(1, size(hkl(1, :), 2));
        leg_str{ii} = [num2str(ii) ': None'];
        flags_h_info{ii, 6} = false(length(ind), 1);
        enable_vec{ii} = 'off';
    end
    flags_h_info{ii, 4} = ind;
    flags_h_info{ii, 5} = repmat(ii, numel(ind), 1);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Drawing
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% create figure
h_fig = figure('Units', 'normalized', 'Position', [0.2 0.1 0.65 0.7], ...
    'Resize', 'off', 'Tag', 'h_fig');

% set application data
setappdata(h_fig, 'output', output)

% figure menu
h_fig_menu = uimenu(h_fig, 'Label', 'Forward Sim', 'Tag', 'h_fig_menu', 'ForegroundColor', 'red');
uimenu(h_fig_menu, 'Label', 'Update Image', ...
    'Checked', sfConvertValueToStatus(app.replace, 0, 1), ...
    'Callback', {@sfSetAppData, 'AppData', 'replace'})
uimenu(h_fig_menu, 'Label', 'Add new spot', ...
    'Checked', sfConvertValueToStatus(app.addspot, 0, 1), ...
    'Callback', {@sfSetAppData, 'AppData', 'addspot'})
uimenu(h_fig_menu, 'Label', 'View blob', ...
    'Checked', sfConvertValueToStatus(app.viewblob, 0, 1), ...
    'Callback', {@sfSetAppData, 'AppData', 'viewblob'})
uimenu(h_fig_menu, 'Label', 'Show comments', ...
    'Checked', sfConvertValueToStatus(app.verbose, 0, 1), ...
    'Callback', {@sfSetAppData, 'AppData', 'verbose'})
uimenu(h_fig_menu, 'Label', 'Save grain', ...
    'Checked', sfConvertValueToStatus(app.save_grain, 0, 1), ...
    'Callback', {@sfSetAppData, 'AppData', 'save_grain'})

% buttons
uicontrol('Parent', h_fig, 'Style', 'pushbutton', 'Tag', 'h_montage_button', ...
    'String', 'Show Montage', ...
    'Units', 'normalized', 'FontWeight', 'bold', ...
    'Position', [0.67 0.95 0.15 0.035], ...
    'BackgroundColor', get(h_fig, 'Color'), ...
    'Callback', @sfShowMontage)
uicontrol('Parent', h_fig, 'Style', 'pushbutton', 'Tag', 'h_full_button', ...
    'String', 'Recalculate full image', ...
    'Units', 'normalized', 'FontWeight', 'bold', ...
    'Position', [0.84 0.95 0.15 0.035], ...
    'BackgroundColor', get(h_fig, 'Color'), ...
    'Callback', @sfUpdateFullImage)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% create image axes
full_img_size = [ ...
    0, parameters.detgeo(app.det_index).detsizeu, ...
    0, parameters.detgeo(app.det_index).detsizev ];

h_ax_image = axes('Parent', h_fig, 'Tag', 'h_ax_image', ...
    'Units', 'normalized', 'Position', [0.02 0.1 0.75 0.8]);
set(h_ax_image, 'XLim', full_img_size(1:2), 'YLim', full_img_size(3:4));
ax_pos = get(h_ax_image, 'Position');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% change color limits
app.clims = gtSetColourLimits(grain.full, app.clims);
hold(h_ax_image, 'on');
if (all(isequal(app.clims, [0 0])))
    h_full = imshow(grain.full, []);
else
    app.clims(1) = max(-500, app.clims(1));
    h_full = imshow(grain.full, app.clims);
    h_bc = gtBrightnessContrast(h_ax_image);
end
set(h_full, 'Tag', 'h_full')
colormap(h_ax_image, gray);

if (exist('h_bc', 'var') && ~isempty(h_bc))
    % set tags for h_bc and change positions
    set(h_bc(1), 'Tag', 'h_slider_min')
    set(h_bc(2), 'Tag', 'h_slider_max')
    set(h_bc(3), 'Tag', 'h_slider_label')

    set(h_bc(1), 'Position', [0.035 0.57 0.025 0.4])
    set(h_bc(2), 'Position', [0.08 0.57 0.025 0.4])
    set(h_bc(3), 'Position', [0.035 0.51 0.07 0.042], ...
        'BackgroundColor', get(h_fig, 'Color'))
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% second axes
h_ax_uvw = copyobj(h_ax_image, h_fig);
delete(findobj(h_ax_uvw, 'type', 'image'))
set(h_ax_uvw, 'Visible', 'off');
set(h_ax_uvw, 'Tag', 'h_ax_uvw');

if (~isempty(app.f_title))
    title_fig = app.f_title;
else
    title_fig = sprintf('Dataset name "%s"     Phase number %02d     GrainID %d', ...
        strrep(parameters.acq(1).name, '_', '\_'), grain.phaseid, grain.id);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% figure settings
set(h_fig, 'Name', strrep(title_fig, '\_', '_'))
% set application data
setappdata(h_fig, 'AppData', app)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% plot UVW with colors and markes; creates legend
% it should be improved to use more grains at a time
[h_scattergroup_flag, ~] = gtPlotMultiScatter3DMatrix(flags_h_markers_uvw, ...
    'labels', leg_str, ...
    'markers', {'x', 'o', 'o', 'd', 'h', 'o'}, ...
    'cmap', {[1 0 0];[1 0 1];[0 0.8 0.8];[0 1 0];[0 1 0];[1 1 0]}, ...
    'filled', false, ...
    'h_figure', h_fig, 'fig_replace', false, ...
    'h_axes', h_ax_uvw, 'axes_replace', false, 'rotate3d', false, ...
    'leg_color', [0 0 0], 'leg_textcolor', [1 1 1], ...
    'leg_pos', [0.71 0.5 0.2 0.4], ...
    'debug', false);
h_scattergroup_flag = h_scattergroup_flag';

% sort ascending order children
h_scatter_markers = cellfun(@(x) sort(get(x, 'Children'), 'ascend'), ...
    h_scattergroup_flag, 'UniformOutput', false);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% conflict special marker
if (app.conflicts) && ...
        isfield(grain, 'conf_table') && ~isempty(grain.conf_table) && ...
        all(ishandle(h_scatter_markers{6}))

    % Only in case we have it
    set(h_scatter_markers{6}, 'MarkerSize', 20);
    % build list of old flags
    oldflags   = [];
    oldhandles = [];
    for jj = 1:5
        % use indexes from original list (grain.fwd_sim(app.det_index).flag) to find the
        % corresponding spot position
        if ~isempty(h_scatter_markers{jj})
            [isMember{jj}, position{jj}] = ismember(flags_h_info{6, 4}, flags_h_info{jj, 4});
            if any(isMember{jj})
                check_box_fam_pos = position{jj};
                check_box_fam_pos(~isMember{jj}) = [];
                oldflags = horzcat(oldflags, ...
                    [repmat(jj, 1, length(check_box_fam_pos)); check_box_fam_pos']);
                oldhandles = [oldhandles, h_scatter_markers{jj}(check_box_fam_pos)];
            end
        end
    end
    grain.conf_table.oldflags   = oldflags;
    grain.conf_table.oldhandles = oldhandles;

    indexes = [];
    for ii = 1:length(grain.conf_table.ids)
        newtag = sprintf('data_%d_%d__data_%d_%d', ...
            6, ii, oldflags(1, ii), oldflags(2, ii));
        indexes(ii, :) = sfGetIndexesFromTagname(newtag);
        set(h_scatter_markers{6}(ii), 'Tag', newtag)
        % label with ID for conflicts
        text(grain.conf_table.x(ii)+10, grain.conf_table.y(ii), ...
            num2str(grain.conf_table.ids(ii)), 'Color', 'y', 'Tag', newtag);
    end
    grain.conf_table.indexes = indexes;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% text for spot info
uipanel('Title', 'Candidates Search', ...
    'TitlePosition', 'centertop', ...
    'BorderType', 'none', ...
    'FontSize', 11, 'FontWeight', 'bold', ...
    'Parent', h_fig, 'Tag', 'h_candidates_panel', ...
    'Units', 'normalized', ...
    'BackgroundColor', get(h_fig, 'Color'), ...
    'Position', [0.005 0.16 0.13 0.33]);
uitable('Parent', h_fig, 'Tag', 'h_candidates_table', ...
    'Rowstriping', 'on', ...
    'ColumnWidth', {200}, ...
    'RowName', [], ...
    'ColumnName', {}, ...
    'BackgroundColor', [get(h_fig, 'Color'); 1 1 153/255], ...
    'Units', 'normalized', ...
    'Enable', 'inactive', ...
    'HitTest', 'off', ...
    'Position', [0.005 0.142 0.13 0.32]);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% click on figure's image and get experimental data about difspotID and
% forward simulated data overlapping
tol.u = spotsCommProps.Xsize / 2;
tol.v = spotsCommProps.Ysize / 2;
tol.w = parameters.fsim.omegarange;

uipanel('Title', 'Tolerances', ...
    'FontSize', 11, 'Parent', h_fig, 'Tag', 'h_tol_title', ...
    'FontWeight', 'bold', ...
    'BackgroundColor', get(h_fig, 'Color'), ...
    'Units', 'normalized', ...
    'Position', [0.005 0.01 0.13 0.125]);
uicontrol('Parent', h_fig, 'Style', 'text', 'Tag', 'h_tol_u_label', ...
    'String', 'U', 'FontWeight', 'bold', ...
    'Units', 'normalized', ...
    'Position', [0.012 0.078 0.03 0.022]);
uicontrol('Parent', h_fig, 'Style', 'text', 'Tag', 'h_tol_v_label', ...
    'String', 'V', 'FontWeight', 'bold', ...
    'Units', 'normalized', ...
    'Position', [0.012 0.05 0.03 0.022]);
uicontrol('Parent', h_fig, 'Style', 'text', 'Tag', 'h_tol_w_label', ...
    'String', 'om', 'FontWeight', 'bold', ...
    'Units', 'normalized', ...
    'Position', [0.012 0.02 0.03 0.022]);
uicontrol('Parent', h_fig, 'Style', 'edit', 'Tag', 'h_tol_u_edit', ...
    'String', sprintf('%0.1f', tol.u), 'FontWeight', 'normal', ...
    'BackgroundColor', [1 1 1], ...
    'Units', 'normalized', ...
    'Position', [0.048 0.078 0.06 0.026]);
uicontrol('Parent', h_fig, 'Style', 'edit', 'Tag', 'h_tol_v_edit', ...
    'String', sprintf('%0.1f', tol.v), 'FontWeight', 'normal', ...
    'BackgroundColor', [1 1 1], ...
    'Units', 'normalized', ...
    'Position', [0.048 0.048 0.06 0.026]);
uicontrol('Parent', h_fig, 'Style', 'edit', 'Tag', 'h_tol_w_edit', ...
    'String', sprintf('%0.2f', tol.w), 'FontWeight', 'normal', ...
    'BackgroundColor', [1 1 1], ...
    'Units', 'normalized', ...
    'Position', [0.048 0.018 0.06 0.026]);
uicontrol('Parent', h_fig, 'Style', 'text', 'Tag', 'h_tol_u_unit', ...
    'String', 'px', 'FontWeight', 'normal', ...
    'BackgroundColor', get(h_fig, 'Color'), ...
    'Units', 'normalized', ...
    'Position', [0.11 0.075 0.02 0.026]);
uicontrol('Parent', h_fig, 'Style', 'text', 'Tag', 'h_tol_v_unit', ...
    'String', 'px', 'FontWeight', 'normal', ...
    'BackgroundColor', get(h_fig, 'Color'), ...
    'Units', 'normalized', ...
    'Position', [0.11 0.045 0.02 0.026]);
uicontrol('Parent', h_fig, 'Style', 'text', 'Tag', 'h_tol_w_unit', ...
    'String', 'deg', 'FontWeight', 'normal', ...
    'BackgroundColor', get(h_fig, 'Color'), ...
    'Units', 'normalized', ...
    'Position', [0.11 0.015 0.02 0.026]);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% create list of all the points
all_tt = vertcat(flags_h_info{:, 2});
all_h  = vertcat(h_scatter_markers{:});
% hide 'none' flags
%set(all_h( findStringIntoCell(get(all_h, 'Tag'), {'none'}, 1, true) ), 'Visible', 'off');
for ii = 1:length(h_scatter_markers)
    if (~sfConvertStatusToValue(enable_vec{ii}))
        set(h_scatter_markers{ii}, 'Tag', ['none_' num2str(ii) '_1'], 'Visible', 'off')
    end
end

% set application data
setappdata(h_fig, 'flags_h_info', flags_h_info)
setappdata(h_fig, 'h_scatter_markers', h_scatter_markers)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% text for (UV) point
uipanel('Title', '(U, V) pointer', ...
    'FontSize', 11, 'Parent', h_fig, 'Tag', 'h_uv_title', ...
    'FontWeight', 'bold', ...
    'BackgroundColor', get(h_fig, 'Color'), ...
    'Units', 'normalized', ...
    'Position', [0.14 0.01 0.145 0.08]);
uicontrol('Parent', h_fig, 'Style', 'text', 'Tag', 'h_uv_label', ...
    'String', '', 'FontWeight', 'normal', 'FontSize', 11, ...
    'BackgroundColor', get(h_fig, 'Color'), ...
    'Units', 'normalized', ...
    'Position', [0.142 0.015 0.14 0.042]);

% Callback for displaying coordinate of pixel
set(h_fig, 'WindowButtonMotionFcn', @sfGetPointInfo)
set(h_fig, 'WindowButtonDownFcn', @sfGetCandidates)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% text for spot fsim info
uipanel('Title', 'Forward simulation info', ...
    'FontSize', 11, 'Parent', h_fig, 'Tag', 'h_fsim_panel', ...
    'FontWeight', 'bold', ...
    'BackgroundColor', get(h_fig, 'Color'), ...
    'Units', 'normalized', ...
    'Position', [0.29 0.01 0.36 0.08]);
uicontrol('Parent', h_fig, 'Style', 'text', 'Tag', 'h_fsim_label', ...
    'String', 'No simulated difspot selected', ...
    'FontSize', 11, ...
    'BackgroundColor', get(h_fig, 'Color'), ...
    'Units', 'normalized', ...
    'Position', [0.292 0.0135 0.356 0.051]);

% Callback for all the markers
set(all_h, 'ButtonDownFcn', @sfDisplayUVWinfo)
hcmenu = uicontextmenu;
uimenu(hcmenu, 'Label', 'Check raw images', ...
    'Callback', @sfCheckRawImages);
% uimenu(hcmenu, 'Label', 'Show Bounding Box', ...
%     'Callback', {@sfShowBbox, grain});
uimenu(hcmenu, 'Label', 'Display Spot', ...
    'Callback', @sfDisplaySpot);

for ii = 1:length(all_h)
    set(all_h(ii), 'UIContextMenu', hcmenu)
end
set(all_h, 'HitTest', 'on');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% select / deselect flags to visualize
set(findobj(h_fig, 'Tag', 'legend'), 'Units', 'pixels');
leg_pos_px = get(findobj(h_fig, 'Tag', 'legend'), 'Position');
if (num_used_families <= 10)
    l_spacing = leg_pos_px(4) / (length(h_scatter_markers) + num_used_families);
else
    l_spacing = 20;
end

for ii = 1:length(h_scatter_markers)
    check_box_spot_flag_pos = [ ...
        (leg_pos_px(1) + leg_pos_px(3) + 10), ...
        (leg_pos_px(2) + leg_pos_px(4) - ii * l_spacing), ...
        50, l_spacing];
    if (~isempty(h_scatter_markers{ii}))
        uicontrol('Parent', h_fig, 'Style', 'checkbox', 'Tag', 'h_flag_on', ...
            'String', sprintf('%d', ii), ...
            'Value', sfConvertStatusToValue(enable_vec{ii}), ...
            'Enable', enable_vec{ii}, 'Visible', enable_vec{ii}, ...
            'Units', 'pixels', ...
            'Position', check_box_spot_flag_pos, ...
            'BackgroundColor', get(h_fig, 'Color'), ...
            'Callback', {@sfShowFlag, ii, 'h_flag_on'});
    end
end

check_box_spot_flag_global_pos = [ ...
    (leg_pos_px(1) + leg_pos_px(3) + 10), ...
    (leg_pos_px(2) + leg_pos_px(4) + 5), ...
    50, l_spacing];

% global checkbox for flags
uicontrol('Parent', h_fig, 'Style', 'checkbox', 'Tag', 'h_flag_all', ...
    'String', ' all', 'FontWeight', 'bold', ...
    'Value', true, 'Enable', 'on', 'Visible', 'on', ...
    'Units', 'pixels', ...
    'Position', check_box_spot_flag_global_pos, ...
    'BackgroundColor', get(h_fig, 'Color'), ...
    'Callback', {@sfShowFlag, 0, 'h_flag_on'});

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% select / deselect families to visualize
h_fam     = cell(num_used_families, 1);
label_fam = cell(num_used_families, 1);
for ii = 1:num_used_families
    h_fam{ii} = all_h(all_tt == grain.fsimgui.thtypes(ii));
    label_fam{ii} = sprintf('hkl type %2d:  %s  int norm: %0.3f', ...
        grain.fsimgui.thtypes(ii), ...
        ['\{' num2str(grain.fsimgui.u_hkls(ii, :)) '\}'], int_hkls(ii));
end

setappdata(h_fig, 'uidata_vec', [flags_h_markers_uvw, enable_vec])
setappdata(h_fig, 'h_flag', h_scattergroup_flag)
setappdata(h_fig, 'h_scatter_markers', h_scatter_markers);
setappdata(h_fig, 'h_fam', h_fam)

h_fam_on = cell(num_used_families, 1);

for ii = 1:num_used_families
    if (~isempty(h_fam{ii}))
        check_box_fam_pos = [ ...
            (leg_pos_px(1) + leg_pos_px(3) + 10), ...
            (leg_pos_px(2) + leg_pos_px(4) - 6 * l_spacing - ii * l_spacing), ...
            50, l_spacing];
        h_fam_on{ii} = uicontrol('Parent', h_fig, 'Style', 'checkbox', ...
            'Tag', 'h_fam_on', ...
            'String', sprintf('# %d', grain.fsimgui.thtypes(ii)), ...
            'Value', true, 'Enable', 'off', 'Visible', 'on', ...
            'Units', 'pixels', ...
            'Position', check_box_fam_pos, ...
            'BackgroundColor', get(h_fig, 'Color'));
    end
end

% keep it after so then h_fam is complete (no clue why!)
for ii = 1:num_used_families
    if (~isempty(h_fam{ii}))
        set(h_fam_on{ii}, 'Callback', {@sfShowFlag, ii, 'h_fam_on'})
    end
end

check_box_fam_global_pos = [ ...
    (leg_pos_px(1) + leg_pos_px(3) + 10), ...
    (leg_pos_px(2) + leg_pos_px(4) - 6 * l_spacing - (num_used_families + 1) * l_spacing - 5), ...
    50, ...
    l_spacing ];
% global checkbox for fams
uicontrol('Parent', h_fig, 'Style', 'checkbox', 'Tag', 'h_fam_all', ...
    'String', ' all', 'FontWeight', 'bold', ...
    'Value', true, 'Enable', 'off', 'Visible', 'on', ...
    'Units', 'pixels', ...
    'Position', check_box_fam_global_pos, ...
    'BackgroundColor', get(h_fig, 'Color'), ...
    'Callback', {@sfShowFlag, 0, 'h_fam_on'});

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% update legend
if (~isempty(findobj(h_fig, 'Tag', 'legend')))
    legend_obj = findobj(h_fig, 'Tag', 'legend');

    legend_labels = get(legend_obj, 'String');
    legend_labels = [legend_labels, label_fam'];

    leg_data      = get(legend_obj, 'UserData');
    leg_data.handles  = [leg_data.handles; cellfun(@(x) x(~isempty(x(1))), h_fam)];

    leg_color     = get(findobj(h_fig, 'Tag', 'legend'), 'Color');
    leg_textcolor = get(findobj(h_fig, 'Tag', 'legend'), 'TextColor');

    leg_pos_px(4) = (num_used_families + length(h_scattergroup_flag)) * l_spacing + 5;
    last = get(findobj(h_fig, 'tag', 'h_fam_all'), 'Position');
    leg_pos_px(2) = last(2) + l_spacing + 5;

    legnew = legend(leg_data.handles, legend_labels);
    set(legnew, 'Color', leg_color, 'TextColor', leg_textcolor);
    set(legnew, 'Units', 'pixels', 'Position', leg_pos_px)
end

% legend
legend(h_ax_uvw, 'show')
legend(h_ax_image, 'hide')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% handles in legend
leg_data = get(findobj(h_fig, 'Tag', 'legend'), 'UserData');
h_l = leg_data.LabelHandles;
% remove handles to text
h_l( findStringIntoCell(get(h_l, 'Type'), {'text'}, 1) ) = [];
% flag handles
h_l_flag = findobj(h_l, 'Type', 'hggroup');
% hkl families handles
h_l_fam = h_l( findStringIntoCell(get(h_l, 'Type'), {'patch'}, 1) );

set(h_l_fam, 'Visible', 'off')
set(h_l_flag, 'Visible', 'on')
h_l = {h_l_flag, h_l_fam};


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% "change color map" button
uicontrol('Parent', h_fig, 'Style', 'popupmenu', 'Tag', 'h_cmap', ...
    'String', {' flag colored', ' omega colored', ' hkl family colored'}, ...
    'Value', 1, ...
    'Units', 'normalized', 'FontWeight', 'bold', ...
    'Position', [0.735 0.9 0.15 0.04], ...
    'BackgroundColor', get(h_fig, 'Color'), ...
    'Callback', {@sfChooseMap, h_l});

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% table for conflicts
% TO DO : use separated window?
uipanel('Parent', h_fig, 'Tag', 'h_header', ...
    'Title', 'No conflict found', 'TitlePosition', 'centertop', ...
    'FontWeight', 'bold', 'FontSize', 11, ......
    'Units', 'normalized', ...
    'BorderType', 'none', 'Visible', 'off', ...
    'Position', [0.67 0.1 0.32 0.24], ...
    'BackgroundColor', get(h_fig, 'Color'));
uitable('Parent', h_fig, 'Units', 'normalized', 'Tag', 'h_table', ...
    'Units', 'normalized', ...
    'Position', [0.67 0.1 0.32 0.21], ...
    'BackgroundColor', [get(h_fig, 'Color'); 1 1 153/255], ...
    'Enable', 'inactive', 'Visible', 'off', ...
    'ColumnName', {''}, ...
    'RowStriping', 'on', 'RearrangeableColumns', 'on');
if (app.conflicts) && isfield(grain, 'conf_table') && ~isempty(grain.conf_table)
    set(findobj(h_fig, 'Tag', 'h_header'), ...
        'Title', 'Conflict table', ...
        'Visible', 'on');
    set(findobj(h_fig, 'Tag', 'h_table'), ...
        'Data', grain.conf_table.data, ...
        'ColumnName', grain.conf_table.cnames, ...
        'RowName', grain.conf_table.rnames, ...
        'ColumnWidth', grain.conf_table.cwidths, ...
        'Visible', 'on');
    mtable = findobj(h_fig, 'Tag', 'h_table');
    sfAutoResizeTable(mtable);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % colorbar
h_colorbar = colorbar('peer', h_ax_uvw);
set(h_colorbar, 'Location', 'northoutside')
set(h_colorbar, 'Position', [0.1405 0.92 0.5080 0.02])
set(h_colorbar, 'XAxisLocation', 'bottom')
set(h_colorbar, 'Visible', 'off')
cbfreeze(h_colorbar, 'off')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% reset axes limits
axis(h_ax_image, full_img_size)
axis(h_ax_uvw, full_img_size)
axis(h_ax_uvw, 'square')
set([h_ax_image h_ax_uvw], 'Position', ax_pos)

% take all the handles in figure wth a specified TAG
handles = guihandles(h_fig);

% set application data
setappdata(h_fig, 'handles', handles)
setappdata(h_fig, 'grain', grain)
setappdata(h_fig, 'parameters', parameters)
setappdata(h_fig, 'spotsCommProps', spotsCommProps)

if (app.save_grain)
    grain = sfGetAppData(h_fig, 'grain');
    if ~isempty(grain) && isstruct(grain)
%         grain = rmfield(grain, 'fsimgui');
        save(sprintf(grainDirsModel, phaseid, grain.id), '-struct', 'grain', '-append')
        disp(['grain has been updated in ' sprintf(grainDirsModel, phaseid, grain.id)])
    end
end

if (nargout > 0)
    grainOut = grain;
end

end % end of function

%%%%%%%%%%%%%%%%%
% sub-functions %
%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%
% CALLBACKS %
%%%%%%%%%%%%%

function sfSetAppData(hObj, ~, field, varname)
% GTSHOWFSIM/sfSETAPPDATA
%   sfSetAppData(hObj, ~, field, varname)
%
% hObj     : popmenu h_cmap handle
% event    : ~
% field    :
% varname  :

if ishandle(hObj)
    varvalue = get(hObj, 'Checked');
    if exist('field', 'var') && ~isempty(field) && isappdata(gtGetParentFigure(hObj), field)
        tmp = getappdata(gtGetParentFigure(hObj), field);
        if isfield(tmp, varname) || isprop(tmp, varname)
            tmp.(varname) = ~sfConvertStatusToValue(varvalue);
        end
        setappdata(gtGetParentFigure(hObj), field, tmp)
        set(hObj, 'Checked', sfConvertValueToStatus(tmp.(varname), 0, 1))
    end
end

end % end function sfSetAppData
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function value = sfGetAppData(hObj, field, varname)
% GTSHOWFSIM/sfGETAPPDATA
%   value = sfGetAppData(hObj, ~, field, varname)
%
% hObj     :
% event    : ~
% field    :
% varname  :
%
% value    :

value = [];
if ishandle(hObj)
    if isappdata(gtGetParentFigure(hObj), field)
        tmp = getappdata(gtGetParentFigure(hObj), field);
        if exist('varname', 'var') && ~isempty(varname) && (isfield(tmp, varname) || isprop(tmp, varname))
            value = tmp.(varname);
        else
            value = tmp;
        end
    end
end

end % end function sfGetAppData
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function sfShowFlag(hObj, ~, flagID, tagname)
% GTSHOWFSIM/sfSHOWFLAG
%   sfShowFlag(hObj, ~, flagID, tagname)
%
% hObj    : single checkbox handle
% event   : ~
% h       : markers handles for flags
% flagID  : flag number
% tagname : tag for hObj

% figure
h_fig = gtGetParentFigure(hObj);
% get axis
ax_pos = get(findobj(h_fig, 'Tag', 'h_ax_uvw'), 'Position');

det_index = sfGetAppData(h_fig, 'AppData', 'det_index');

grain  = sfGetAppData(h_fig, 'grain');
h_vec  = sfGetAppData(h_fig, 'h_scatter_markers');
h_fam  = sfGetAppData(h_fig, 'h_fam');

if (strcmpi(tagname, 'h_flag_on'))
    h = h_vec;
elseif (strcmpi(tagname, 'h_fam_on'))
    h = h_fam;
end

% get conflict labels
h_conf = findobj(h_fig, 'Type', 'text');
h_conf = h_conf( findStringIntoCell(get(findobj(h_fig, 'Type', 'text'), 'Tag'), {'data_6'}, 1, true) );

if (flagID == 0)
    % set the status to al the points
    set(vertcat(h{:}), 'Visible', sfGetStatus(hObj))
    % set status to text
    set(h_conf, 'Visible', sfGetStatus(hObj))

    % get all the checkbox relative to the current type 'h_flag_on' |
    % 'h_fam_on' and set them to val (only the one enabled)
    h_tmp = sort(findobj(h_fig, 'Tag', tagname), 'ascend');
    if ~isempty(h_tmp)
        h_tmp(findStringIntoCell(get(h_tmp, 'Visible'), {'off'}, 1))=[]; % was Enable
        set(h_tmp, 'Value', get(hObj, 'Value'))
    end
else
    % set the status to the relative points
    set(h{flagID}, 'Visible', sfGetStatus(hObj))
end

% checkboxes
h_fam_on  = sort(findobj(h_fig, 'Tag', 'h_fam_on'), 'ascend');
h_flag_on = sort(findobj(h_fig, 'Tag', 'h_flag_on'), 'ascend');

% update status basing on other flag value
flags_on = logical(cell2mat(get(h_flag_on, 'Value')))';
fams_on  = logical(cell2mat(get(h_fam_on, 'Value')))';

% get list of active difspots
h_tmp = sort(findobj(h_fig, 'Tag', tagname), 'ascend');
h( ~arrayfun(@(x) sfConvertStatusToValue(x), get(h_tmp, 'Visible')) ) = [];
% children
h_child = cat(1, h{:});
active = sfGetActiveMarkers(grain, det_index, flags_on, fams_on);

% % check other checked flags and get consistency with visible markers on image
sfUpdateActiveMarkers(grain, det_index, active, h_child, tagname)

% keep consistent value of global flag 'all'
h_tmp = sort(findobj(h_fig, 'Tag', tagname), 'ascend');
h_tmp_all = findobj(h_fig, 'Tag', [tagname(1:end-2) 'all']);
if (~isempty(h_tmp))
    h_tmp(findStringIntoCell(get(h_tmp, 'Visible'), {'off'}, 1)) = []; %was "Enable"
end
if (length(unique( cell2mat(get(h_tmp, 'Value')) )) == 2)
    set(h_tmp_all, 'Value', false)
elseif (length(unique( cell2mat(get(h_tmp, 'Value')) )) == 1)
    set(h_tmp_all, 'Value', get(h_tmp(1), 'Value'))
end

set(h_conf, 'Visible', sfGetStatus(h_flag_on(6)))

% reset axis position
set(findobj(h_fig, 'Tag', 'h_ax_uvw'), 'Position', ax_pos);

end % end function sfShowFlag
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function sfChooseMap(hObj, ~, h_l)
% GTSHOWFSIM/sfCHOOSEMAP
%   sfChooseMap(hObj, ~, hp)
%
% hObj     : popmenu h_cmap handle
% event    : ~
% h_l      : markers handles legend {h_l_flag, h_l_fam}

h_fig = gtGetParentFigure(hObj);
output         = sfGetAppData(h_fig, 'output');
output.doPrint = sfGetAppData(h_fig, 'AppData', 'verbose');

% axes in figure
ax_pos = get(findobj(h_fig, 'Tag', 'h_ax_uvw'), 'Position');

flags_h_info = sfGetAppData(h_fig, 'flags_h_info');
h_fam    = sfGetAppData(h_fig, 'h_fam');
h_vec    = sfGetAppData(h_fig, 'h_scatter_markers');

grain      = sfGetAppData(h_fig, 'grain');
used_fam = grain.fsimgui.used_fam;

% markers in legend
h_l_flag = h_l{1};
h_l_fam  = h_l{2};

switch(get(hObj, 'Value'))
    case 1
        output.odisp('flag coloring')

        % visualize checkboxes
        h_flag_on = findobj(h_fig, 'Tag', 'h_flag_on');
        h_flag_on(findStringIntoCell(get(h_flag_on, 'Enable'), {'off'}, 1)) = [];
        set(h_flag_on, 'Visible', 'on')

        % colormap for flags
        flags = sfGetLengthMatrixFromCell(h_vec, [], true);
        flagmap = [1 0 0; 1 0 1; 0 0.8 0.8; 0 1 0; 0 1 0; 1 1 0];

        sfChangeStyle(h_vec, {'x', 'o', 'o', 'd', 'h', 'o'}, flagmap, flags)

        % checkboxes
        set(h_l_fam, 'Visible', 'off')
        set(h_l_flag, 'Visible', 'on')
        cbfreeze('off')
        set(findobj(h_fig, 'Tag', 'Colorbar'), 'Visible', 'off')

        set(findobj(h_fig, 'Tag', 'h_fam_all'), 'Enable', 'off');
        set(findobj(h_fig, 'Tag', 'h_fam_on'), 'Enable', 'off');
        set(findobj(h_fig, 'Tag', 'h_flag_all'), 'Enable', 'on');
        set(findobj(h_fig, 'Tag', 'h_flag_on'), 'Enable', 'on');
    case 2
        output.odisp('omega coloring')

        % colormap for omega values
        omegamap = jet(361);
        omtoINT = @(x) round(x + 1);
        om_int = cellfun(@(num) omtoINT(num), flags_h_info(:, 1), 'UniformOutput', false);

        % colormap for flags
        sfChangeStyle(h_vec, {'o'}, omegamap, om_int)
        colormap(findobj(h_fig, 'Tag', 'h_ax_uvw'), omegamap)

        % colorbar
        if ( strcmpi(get(findobj(h_fig, 'Tag', 'Colorbar'), 'Visible'), 'off') )
            set(findobj(h_fig, 'Tag', 'Colorbar'), 'Location', 'northoutside')
            caxis(findobj(h_fig, 'Tag', 'h_ax_uvw'), [0 360])
            set(findobj(h_fig, 'Tag', 'Colorbar'), 'Position', [0.1405 0.92 0.5080 0.02])
            set(findobj(h_fig, 'Tag', 'Colorbar'), 'XAxisLocation', 'bottom')
            set(findobj(h_fig, 'Tag', 'Colorbar'), 'XTickLabelMode', 'auto')
            set(findobj(h_fig, 'Tag', 'Colorbar'), 'XTickMode', 'manual')
            set(findobj(h_fig, 'Tag', 'Colorbar'), 'XTick', 0:60:360)
            cbfreeze('on')
            set(findobj(h_fig, 'Tag', 'Colorbar'), 'Visible', 'on')
        end

        % checkboxes
        set(h_l_fam, 'Visible', 'off')
        set(h_l_flag, 'Visible', 'off')

        set(findobj(h_fig, 'Tag', 'h_fam_all'), 'Enable', 'off');
        set(findobj(h_fig, 'Tag', 'h_fam_on'), 'Enable', 'off');
        set(findobj(h_fig, 'Tag', 'h_flag_all'), 'Enable', 'off');
        set(findobj(h_fig, 'Tag', 'h_flag_on'), 'Enable', 'off');
    case 3
        output.odisp('hkl family coloring')

        % colormap for families
        % find maximum family number
        maxType = max(vertcat(flags_h_info{:, 2}));
        % colors for families
        hklmap = colorcube(maxType);
        % values for thetatype
        tth = sfGetLengthMatrixFromCell(h_fam, used_fam, true);

        % colormap for flags
        sfChangeStyle(h_fam, {'o'}, hklmap, tth)

        % checkboxes
        set(h_l_flag, 'Visible', 'off')
        set(h_l_fam, 'Visible', 'on')
        cbfreeze('off')
        set(findobj(h_fig, 'Tag', 'Colorbar'), 'Visible', 'off')

        set(findobj(h_fig, 'Tag', 'h_fam_all'), 'Enable', 'on');
        set(findobj(h_fig, 'Tag', 'h_fam_on'), 'Enable', 'on');
        set(findobj(h_fig, 'Tag', 'h_flag_all'), 'Enable', 'off');
        set(findobj(h_fig, 'Tag', 'h_flag_on'), 'Enable', 'off');
end

% colormap image
colormap(findobj(h_fig, 'Tag', 'h_ax_image'), gray)
set(findobj(h_fig, 'Tag', 'h_ax_uvw'), 'Position', ax_pos);

end % end function sfChooseMap
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function sfDisplayUVWinfo(hObj, ~)
% GTSHOWFSIM/sfDISPLAYUVWINFO
%   sfDisplayUVWinfo(hObj, ~)
%
% hObj         : patch single handle
% event        : ~

h_fig = gtGetParentFigure(hObj);

flags_h_info = sfGetAppData(h_fig, 'flags_h_info');

output = sfGetAppData(h_fig, 'output');
output.doPrint = sfGetAppData(h_fig, 'AppData', 'verbose');

% left-click event
if (strcmpi(get(h_fig, 'SelectionType'), 'normal'))
    % get flag and index from Tag
    indexes = sfGetIndexesFromTagname(get(hObj, 'Tag'));
    if (length(indexes) == 2)
        flag  = indexes(1);
        index = indexes(2);
    elseif (length(indexes) == 4)
        if (indexes(3) == 6)
            flag  = indexes(3);
            index = indexes(4);
        else
            flag  = indexes(1);
            index = indexes(2);
        end
    end

    % get current info
    om = flags_h_info{flag, 1}(index, :);
    % get thetatype
    tth = flags_h_info{flag, 2}(index, :);
    % get hklsp
    hklsp = flags_h_info{flag, 3}(index, :);
    % index in grain.thetatype
    ind_g = flags_h_info{flag, 4}(index, :);

    % build string to display info
    text = get(hObj, 'UserData');
    if (~isempty(text))
        extratext = sprintf('flag: %2d \nhkl: {%s}  index: %2d  hklfamily: %2d', flag, num2str(hklsp), ind_g, tth);
        text = sprintf(text, 'U', 'V', 'W', 'omega', om, extratext);

        set(hObj, 'UserData', text)
        % display on screen
        output.odisp('Fsim data:')
        output.odisp(text)
    end

    % update GUI
    set(findobj(h_fig, 'Tag', 'h_fsim_label'), 'String', text)
    drawnow()
end % left-click event

end % end function sfDisplayUVWinfo
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function point = sfGetPointInfo(hObj, ~)
% GTSHOWFSIM/sfGETPOINTINFO
%   point = sfGetPointInfo(hObj, ~)
%
% hObj  : figure handle
% event : ~
%
% point :

h_fig = gtGetParentFigure(hObj);

% get mouse position on axis
mousePos = get(get(h_fig, 'CurrentAxes'), 'CurrentPoint');
UV = mousePos(1, 1:2);
% If it is outside the axis, discard it
if UV(1) < 0 || UV(1) > 2048 || UV(2) < 0 || UV(2) > 2048
    UV = [];
end
% build string to display
if (~isempty(UV))
    value = sprintf('(%5.1f, %5.1f)', UV(1), UV(2));
else
    value = sprintf('(U, V)');
end

% update GUI
set(findobj(h_fig, 'Tag', 'h_uv_label'), 'String', value)
set(findobj(h_fig, 'Tag', 'h_uv_label'), 'UserData', UV)

if (nargout > 0)
    point = UV;
end

end % end function sfGetPointInfo
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function sfGetCandidates(hObj, ~)
% GTSHOWFSIM/sfGETCANDIDATES
%   sfGetCandidates(hObj, ~)
%
% hObj    : figure handle
% event   : ~

% figure handle
h_fig = gtGetParentFigure(hObj);

grain = sfGetAppData(h_fig, 'grain');

% point handles
h_vec = sfGetAppData(h_fig, 'h_scatter_markers');
% uvw info
uidata_vec = sfGetAppData(h_fig, 'uidata_vec');
uvw_vec = uidata_vec(:, 1);
% conditional output
output = sfGetAppData(h_fig, 'output');
output.doPrint = sfGetAppData(h_fig, 'AppData', 'verbose');

det_index = sfGetAppData(h_fig, 'AppData', 'det_index');

parameters = sfGetAppData(h_fig, 'parameters');
omstep = gtAcqGetOmegaStep(parameters, det_index);

% if left-click
if (strcmpi(get(h_fig, 'SelectionType'), 'normal'))

    % read h_uv_label UV position from UserData
    UV = get(findobj(h_fig, 'Tag', 'h_uv_label'), 'UserData');

    % read tolerances from GUI
    tol.u = str2double(get(findobj(h_fig, 'Tag', 'h_tol_u_edit'), 'String'));
    tol.v = str2double(get(findobj(h_fig, 'Tag', 'h_tol_v_edit'), 'String'));
    tol.w = str2double(get(findobj(h_fig, 'Tag', 'h_tol_w_edit'), 'String'));

    uvw_fsim = vertcat(uvw_vec{:});
    h_child = vertcat(h_vec{:});
    % get flag-index from Tag names
    indexes = cellfun(@(x) sfGetIndexesFromTagname(x), get(h_child, 'Tag'), 'UniformOutput', false);
    conflicts = indexes( cellfun(@(x) size(x, 2), indexes) > 2 );
    if (~isempty(conflicts))
        % remove extra columns from indexes (case when there are conflicts)
        indexes( cellfun(@(x) size(x, 2), indexes) > 2) = cellfun(@(x) ...
            x(1:2), conflicts, 'UniformOutput', false);
        isconfl = cellfun(@(x) ismember(6, x), conflicts);
        conflicts = conflicts(isconfl);
        conflicts = vertcat(conflicts{:});
    end
    % convert to matrix
    indexes = vertcat(indexes{:});

    header = {};
    candidateID = [];
    if (~isempty(UV))
        % finds intersection btw fsim results (xy[z]) and current point (UV)
        cand_pos = findIndexesMultiCols(uvw_fsim(:, 1:2), UV, [tol.u tol.v])';
        uvw_fsim = uvw_fsim(cand_pos, :);
        [uvw_fsim, ia2] = unique(uvw_fsim, 'rows', 'stable');
        cand_pos = cand_pos(ia2);
        if (~isempty(cand_pos))
            indexes = indexes(cand_pos, :)';
        else
            indexes = NaN(1, 2);
        end

        % finds in the DB intersection btw centroid XYImage and fsim candidate
        % positions (xyz) using tolerances passed by tol
        if (isfield(grain, 'fwd_sim'))
            uvw_exp = grain.fwd_sim(det_index).uvw;
        else
            uvw_exp = [grain.uv_exp, grain.om_exp];
        end
        uvw_exp(:, 3) = uvw_exp(:, 3) / omstep;

        [candidateID, uvw_exp] = findIndexesMultiCols(uvw_exp, uvw_fsim, [tol.u tol.v tol.w/omstep]);
        [candidateID, ia] = unique(candidateID, 'stable');
        uvw_exp = uvw_exp(ia, :);

        % calculate distances btw exp and fsim(POINT) and error and sort
        % candidateID in ascending order of distance.
        % can be useful to use the sub-function sfFindCandidates in
        % gtINDEXMatchGrains_2
        if (isempty(candidateID))
            candidateID = NaN;
            dexp        = NaN;
            ddist_      = NaN;
            dom_        = NaN;
        else
            if ((size(uvw_exp, 1) ~= size(uvw_fsim, 1)) && ismember(1, indexes(:, 1)))
                output.odisp('Expected spots not found...')

            end

            if (size(uvw_exp, 1) == size(uvw_fsim, 1))
                dxy_    = uvw_exp(:, 1:2) - uvw_fsim(:, 1:2);
                ddist_  = sqrt(sum(dxy_.^2, 2));
                derr_   = ddist_.^2 / (tol.u^2 + tol.v^2);
                dom_    = abs(uvw_exp(:, 3) - uvw_fsim(:, 3))*omstep;
                [ddist_, ~]  = sort(ddist_, 1, 'ascend');
                dom_ = sort(dom_, 1, 'ascend');
                derrom_ = dom_.^2 / (tol.w)^2;
            else
                tol = rmfield(tol, 'w');
                ddist_  = NaN;
                dom_    = NaN;
            end
            dxy         = uvw_exp(:, 1:2) - UV(ones(size(uvw_exp, 1), 1), :);
            dexp        = sqrt(sum(dxy.^2, 2));
            derr        = dexp .^ 2 / (tol.u ^ 2 + tol.v ^ 2);
            [dexp, ind]  = sort(dexp, 1, 'ascend');
            candidateID = candidateID(ind)';
        end
        if ~any(find(isnan(indexes)))
            duv         = uvw_fsim(:, 1:2) - UV(ones(size(uvw_fsim, 1), 1), :);
            dfsim       = sqrt(sum(duv .^ 2, 2));
            derrfsim    = dfsim .^ 2 / (tol.u ^ 2 + tol.v ^ 2);
            [dfsim, ~]  = sort(dfsim, 1, 'ascend');
        else
            dfsim       = NaN;
        end
        % save values in axes 'omega' UserData
        header = {'';...
                  'candidateIDs:';...
                  'flag (index):';...
                  'Exp/Fsim UV [px]:';...
                  'Exp/Fsim omega [deg]:';...
                  'Exp/(U, V) [px]:';...
                  'Fsim/(U, V) [px]:'};
        UV = {UV; candidateID; indexes; ddist_'; dom_'; dexp'; dfsim'};

        if (isnan(candidateID))
            set(findobj(h_fig, 'Tag', 'h_fsim_label'), ...
                'String', 'No simulated difspot selected')
        end

    end

    % save IDs, errors and UV coordinates in the axis 'omega'
    set(findobj(h_fig, 'Tag', 'h_ax_uvw'), 'UserData', UV)

    % update GUI
    sfUpdateText(findobj(h_fig, 'Tag', 'h_candidates_table'), header, UV, conflicts)

    % select row corresponding to the conflict
    if (~isempty(conflicts) && ~isempty(candidateID))
        sfRowSelection(hObj, candidateID)
    end
end % left-click event

end % end function sfGetCandidates
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function sfCheckRawImages(hObj, ~)
% GTSHOWFSIM/sfCHECKRAWIMAGES
%   sfCheckRawImages(hObj, ~)
%
% hObj           : uimenu handle of uicontextmenu parent
% event          : ~
% uvw_vec        : UVW coordinates for fsim flags

    h_fig = gtGetParentFigure(hObj);

    % parameters
    parameters = sfGetAppData(h_fig, 'parameters');

    % uidata
    uidata_vec = sfGetAppData(h_fig, 'uidata_vec');
    uvw_vec = uidata_vec(:, 1);
    % spotsCommProps : difspots common properties
    spotsCommProps = sfGetAppData(h_fig, 'spotsCommProps');

    output = sfGetAppData(h_fig, 'output');
    output.doPrint = sfGetAppData(h_fig, 'AppData', 'verbose');

    % right-click event
    if strcmpi(get(h_fig, 'selectiontype'), 'alt')
        h_point = gco;

        % image data
        oldtag = get(h_point, 'Tag');

        indexes = sfGetIndexesFromTagname(oldtag);
        UVW = uvw_vec{indexes(1)}(indexes(2), :);
        if (~isempty(UVW) && ~any(UVW == [-1 -1 -1], 2))
            % read tolerances from GUI
            tol.u = str2double(get(findobj(h_fig, 'Tag', 'h_tol_u_edit'), 'String'));
            tol.v = str2double(get(findobj(h_fig, 'Tag', 'h_tol_v_edit'), 'String'));
            tol.w = str2double(get(findobj(h_fig, 'Tag', 'h_tol_w_edit'), 'String'));

            % update only omega range
            parameters.fsim.omegarange = tol.w;
            parameters.fsim.bb_size    = 2 * floor([tol.u tol.v]);

            [found_intensity, spotInfo] = gtFwdSimCheckSpotInRawImages(UVW, ...
                spotsCommProps, parameters);

            %set(h_point, 'UserData', {get(h_point, 'UserData'), spotInfo})
            %checkSpots(ii).info = spotInfo;
            if (found_intensity)
                text_out = sprintf(['Found intensity at UVW = ' ...
                    '(%6.1f, %6.1f, %6.1f (img: %d)) for fsim (flag=%d, index=%d)'], ...
                    UVW(1:2), UVW(3)*gtAcqGetOmegaStep(parameters), ...
                    round(UVW(3)), indexes(1), indexes(2));
                output.odisp(text_out)

                hf = figure('Name', text_out, 'Tag', 'h_fig_spot');
                ax = axes('Parent', hf);
                imagesc(spotInfo.spot, 'Parent', ax)
                colormap(ax, gray)
                gtBrightnessContrast(ax)

                if (indexes(1) == 1)
                    indexes(1) = 2;
                    indexes(2) = length(uvw_vec{indexes(1)}) + 1;
                    newtag = ['data_' num2str(indexes(1)) '_' ...
                        num2str(indexes(2)) '__' get(h_point, 'Tag')];
                    set(h_point, 'Tag', newtag)
                    % update all appdata and draw again

                end
                if (sfGetAppData(h_fig, 'AppData', 'addspot'))
                    % update GUI
                    sfAddSpotToFullImage(findobj(h_fig, 'Tag', 'h_full'), ...
                        spotInfo.spot, spotInfo.bb);
                    % update all appdata and again

                end
                if (sfGetAppData(h_fig, 'AppData', 'viewblob'))
                    GtVolView(spotInfo.difblob);
                end
                print_structure(spotInfo, 'spot_info', false, output.doPrint)
            else
                output.odisp('No intensity found')
            end
        end
    end % right-click event
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function sfDisplaySpot(hObj, ~)
% GTSHOWFSIM/sfVIEWSPOTIMAGE
%   sfDisplaySpot(hObj, ~)
%
% hObj  : uimenu handle of uicontextmenu parent
% event : ~

    h_fig = gtGetParentFigure(hObj);
    % grain structure
    grain = sfGetAppData(h_fig, 'grain');
    flags_h_info = sfGetAppData(h_fig, 'flags_h_info');
    det_index = sfGetAppData(h_fig, 'AppData', 'det_index');

    % right-click event
    if strcmpi(get(h_fig, 'selectiontype'), 'alt')
        h_point = gco;
        % ask user if replacing full image

        indexes = sfGetIndexesFromTagname(get(h_point, 'Tag'));
        if (length(indexes) == 2)
            flag = indexes(1);
            index = indexes(2);
        elseif (length(indexes) == 4)
            flag = indexes(3);
            index = indexes(4);
        end
        ind_g = flags_h_info{flag, 4}(index);
        spot_ID = grain.fsimgui.table.spotid(ind_g);
        [hasID, ind] = ismember(spot_ID, grain.fwd_sim(det_index).difspotID);
        if (hasID && spot_ID ~= 0)
            figure('Name', 'Difspot image from grain stack');
            imshow(squeeze(grain.proj(det_index).stack(:, ind, :)), [])
            title(['difspotID : ' num2str(spot_ID) '  om: ' num2str(flags_h_info{flag, 1}(index)) ' flag : ' num2str(flag)])
        else
            warndlg('The selected spot was not included into the stack!')
        end
    end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%
% WITH HANDLES %
%%%%%%%%%%%%%%%%

function sfRowSelection(hObj, spotID)
% GTSHOWFSIM/sfROWSELECTION
%   sfRowSelection(hObj, spotID)

    h_fig = gtGetParentFigure(hObj);

    % get axis
    ax_pos = get(findobj(h_fig, 'Tag', 'h_ax_uvw'), 'Position');
    mtable = findobj(h_fig, 'Tag', 'h_table');

    jtable = sfAutoResizeTable(mtable);

    datatable = get(mtable, 'Data');
    IDs = cell2mat(datatable(:, 1));
    [hasID, num]=ismember(spotID, IDs);
    if (any(hasID) && num ~= 0)
        jtable.setRowSelectionInterval(num-1, num-1)
    else
        jtable.setRowSelectionAllowed(false)
    end

    % reset axis position
    set(findobj(h_fig, 'Tag', 'h_ax_uvw'), 'Position', ax_pos)
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function hJTable = sfAutoResizeTable(hObj)
% GTSHOWFSIM/sfAUTORESIZETABLE
%   hJTable = sfAutoResizeTable(hObj)

    jscrollpane = findjobj(hObj, 'nomenu');
    jtable = jscrollpane.getViewport.getView;

    jtable.setRowSelectionAllowed(true)
    jtable.setNonContiguousCellSelection(false)
    jtable.setColumnSelectionAllowed(false)
    jtable.setColumnResizable(true)
    jtable.setColumnAutoResizable(true)
    jtable.setAutoResizeMode(4)

    if (nargout > 0)
        hJTable = jtable;
    end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function status = sfGetStatus(hObj)
% GTSHOWFSIM/sfGETSTATUS
%   status = sfGetStatus(hObj)

    val   = get(hObj, 'Value');
    minV  = get(hObj, 'Min');
    maxV  = get(hObj, 'Max');

    status = sfConvertValueToStatus(val, minV, maxV);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function sfUpdateText(hObj, header, UV, conflicts)
% GTSHOWFSIM/sfUPDATETEXT
%   sfUpdateText(hObj, header, UV, conflicts)
%
% hObj   : UV coordinate label handle
% header : columns names
% UV     : UVinfo stored in the axis 'omega'
%          UV = {UV;candidateID;indexes;dexp';dfsim';ddist_';ddistom_'};
% conflicts :

    if iscell(UV) && ~any(find(isnan(UV{3, :}))) && ~isempty(conflicts)
        [rows, ia, ~] = findDifferentRowIntoMatrix(conflicts(:, 1:2), UV{3, :}');
        if ~isempty(rows)
            UV{3, :}(:, end+1:end+length(ia)) = conflicts(ia, 3:4);
        end
    end

    text = [];
    if iscell(UV) && ~isempty(UV)
        text{1, 2} = sprintf('(%0.1f, %0.1f)', UV{1, :});
        text{2, 2} = sprintf('%6d  ', UV{2, :});
        text{3, 2} = sprintf('%d (%2d)  ', UV{3, :});
        text{4, 2} = sprintf('%0.2f  ', UV{4, :});
        text{5, 2} = sprintf('%0.2f  ', UV{5, :});
        text{6, 2} = sprintf('%0.2f  ', UV{6, :});
        text{7, 2} = sprintf('%0.2f  ', UV{7, :});

        text(:, 1) = header;
        text(:, 2) = cellfun(@(x)strtrim(x), text(:, 2), 'UniformOutput', false);

        text_label = reshape(text', [], 1);
        text_label(1) = [];

        % update GUI
        set(hObj, 'Data', text_label);
    end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function sfAddSpotToFullImage(hObj, spot, bb)
% GTSHOWFSIM/sfADDSPOTTOFULLIMAGE
%   sfAddSpotToFullImage(hObj, spot, bb)

    im = get(hObj, 'CData');
    if isempty(im)
        im = zeros(size(im));
    end
    im = gtPlaceSubImage2(spot, im, bb(1), bb(2), 'summed');

    set(hObj, 'CData', im);
    drawnow()
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function sfShowMontage(hObj, ~)
% GTSHOWFSIM/sfUPDATEFULLIMAGE
%   sfShowMontage(hObj, ~, varargin)
%
% hObj     :
% event    : ~
% varargin :

    % figure
    h_fig = gtGetParentFigure(hObj);

    % current grain info
    grain = sfGetAppData(h_fig, 'grain');

    % parameters.mat
    parameters = sfGetAppData(h_fig, 'parameters');

    det_index = sfGetAppData(h_fig, 'AppData', 'det_index');

    % checkboxes
    h_fam_on  = sort(findobj(h_fig, 'Tag', 'h_fam_on'), 'ascend');
    h_flag_on = sort(findobj(h_fig, 'Tag', 'h_flag_on'), 'ascend');

    % update status basing on other flag value
    flags_on = logical(cell2mat(get(h_flag_on, 'Value')))';
    fams_on  = logical(cell2mat(get(h_fam_on, 'Value')))';

    % get list of active difspots (ondet)
    active = sfGetActiveMarkers(grain, det_index, flags_on, fams_on);

    % I might be wrong, but it seems to me that included shouldn't be
    % touched, but the seleced should be intersected with the indexx
    % instead
    included = grain.proj(det_index).included;
    selected = grain.proj(det_index).selected;
    selected = selected & active(included);

    show_blobs = strcmpi(parameters.rec.grains.algorithm(1:2), '6D');

    grain.proj(det_index).selected = gtGuiGrainMontage(grain, selected, show_blobs, det_index);

    setappdata(h_fig, 'grain', grain)
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function active = sfGetActiveMarkers(grain, det_index, flags_on, fams_on)

    flags   = grain.fwd_sim(det_index).flag;
    thtypes = grain.allblobs(det_index).thetatype(grain.proj(det_index).ondet);

    ind_flag = find(flags_on);
    ind_fam  = grain.fsimgui.thtypes(fams_on)';

    active = ismember(flags, ind_flag) & ismember(thtypes, ind_fam);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function sfUpdateActiveMarkers(grain, det_index, active, h_child, tagname)

    if (strcmpi(tagname, 'h_flag_on'))
        % This won't work with HKL coloring because not used families
        % are going to be ignored!
        set(h_child(~active), 'Visible', 'off')
        set(h_child(active), 'Visible', 'on')
    elseif (strcmpi(tagname, 'h_fam_on'))
        % So, we only select the markes associated to the used families
        thtypes = grain.allblobs(det_index).thetatype(grain.proj(det_index).ondet);

        used_fams = grain.fsimgui.used_fam;
        inds_used_fams = ismember(thtypes, used_fams);

        to_be_on = active(inds_used_fams);

        set(h_child(~to_be_on), 'Visible', 'off')
        set(h_child(to_be_on), 'Visible', 'on')
    else
        error('gtShowFsim:wrong_argument', ...
            'Unknown type of marker: "%s"', tagname);
    end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function sfUpdateFullImage(hObj, ~)
% GTSHOWFSIM/sfUPDATEFULLIMAGE
%   full = sfUpdateFullImage(hObj, ~)
%
% hObj           :
% event          : ~

    % figure
    h_fig = gtGetParentFigure(hObj);

    % current grain info
    grain = sfGetAppData(h_fig, 'grain');

    % parameters.mat
    parameters = sfGetAppData(h_fig, 'parameters');

    det_index = sfGetAppData(h_fig, 'AppData', 'det_index');

    if (sfGetAppData(h_fig, 'AppData', 'replace'))
        % checkboxes
        h_fam_on  = sort(findobj(h_fig, 'Tag', 'h_fam_on'), 'ascend');
        h_flag_on = sort(findobj(h_fig, 'Tag', 'h_flag_on'), 'ascend');

        % update status basing on other flag value
        flags_on = logical(cell2mat(get(h_flag_on, 'Value')))';
        fams_on  = logical(cell2mat(get(h_fam_on, 'Value')))';

        included = grain.proj(det_index).included;

        % get correspondng bbox from DB
        if (isfield(grain, 'fwd_sim'))
            bb = grain.fwd_sim(det_index).bb;
            ids = grain.fwd_sim(det_index).difspotID;
        else
            bb = grain.bb_exp;
            ids = grain.difspotID;
        end

        % get list of active difspots
        active = sfGetActiveMarkers(grain, det_index, flags_on, fams_on);
        % If some spots will be missing from the full image, it might be
        % that they belong to "not used families"!! Careful if you change
        % this later!!
        bb = bb(active(included), :);
        ids = ids(active(included));

        full = sfBuildFull(ids, parameters, bb, det_index);
        set(findobj(h_fig, 'Tag', 'h_full'), 'CData', full);

        grain.full = full;
        setappdata(h_fig, 'grain', grain)
    else
        warndlg('Flag ''replace'' in figure menu ''Fsim'' is set to false')
    end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function sfChangeStyle(h_scatter_markers, markers, cmap, flags_h_info)
% GTSHOFSIM/sfCHANGESTYLE
%   sfChangeStyle(h_vec, markers, cmap, flags_h_info)
%
% h_scatter_markers : scattergroup handles for flags (NOT TRUE! these are
%                     marker handles, which are not easily available
%                     anymore, starting from matlab 2014b)
% markers  : marker(s) in cell
% cmap     : color map table
% flags_h_info : current extra info values in cell (omega, thetatype, ...)

    if (numel(markers) == 1)
        markers(2:numel(h_scatter_markers)) = markers;
        nofill = false;
    else
        nofill = true;
    end

    % colormap
    for ii = 1:length(h_scatter_markers)
        h_child = h_scatter_markers{ii};
        set(h_child, 'Marker', markers{ii});

        for jj = 1:length(h_child)
            % check Face color and adapt border to better contrast
            if (~isempty(flags_h_info{ii}(jj)) ...
                    && ( isequal(cmap(flags_h_info{ii}(jj), :), [0 0 0]) ...
                    || all(cmap(flags_h_info{ii}(jj), :) < [0.5 0.5 0.5], 2) ) )
                set(h_child(jj), 'MarkerEdgeColor', [1 1 1])
            elseif (~isempty(flags_h_info{ii}(jj)) ...
                    && all(cmap(flags_h_info{ii}(jj), :) > [0.6 0.6 0.6], 2))
                set(h_child(jj), 'MarkerEdgeColor', [0 0 0])
            else
                set(h_child(jj), 'MarkerEdgeColor', cmap(flags_h_info{ii}(jj), :))
            end

            if (~nofill)
                set(h_child(jj), 'MarkerFaceColor', cmap(flags_h_info{ii}(jj), :))
            else
                set(h_child(jj), 'MarkerFaceColor', 'none')
            end
        end

        % conflict case
        tags = get(h_child, 'Tag');
        if (~iscell(tags))
            tags = {tags};
        end
        ind = findStringIntoCell(tags, {'data_6'}, 1, true);
        if (~isempty(ind))
            set(h_child(ind), 'MarkerFaceColor', 'none')
            set(h_child(ind), 'MarkerEdgeColor', [1 1 0])
            set(h_child(ind), 'MarkerSize', 20);
        end
    end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%
% WITHOUT HANDLES %
%%%%%%%%%%%%%%%%%%%

function val = sfConvertStatusToValue(status)
% GTSHOWFSIM/sfCONVERTSTATUSTOVALUE
%   val = sfConvertStatusToValue(status)

    val = strcmpi(status, 'on');
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function status = sfConvertValueToStatus(val, minV, maxV)
% GTSHOWFSIM/sfCONVERTVALUETOSTATUS
% status = sfConvertValueToStatus(val, minV, maxV)

    status = [];
    if (val == minV)
        status = 'off';
    elseif (val == maxV)
        status = 'on';
    end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function full = sfBuildFull(spotid, parameters, bb, det_index)
% GTSHOWFSIM/sfBUILDFULL
%   full = sfBuildFull(spotid, parameters, spotsCommProps, bb)
%
% spotid         : spotid from grain
% parameters     : parameters.mat
% spotsCommProps : spots common properties
% bb             : bounding boxes from DB

    full = zeros(parameters.acq(det_index).ydet, parameters.acq(det_index).xdet, 'single');

    for ii = 1 : length(spotid)
        [spot, ~] = gtFwdSimBuildSpot(spotid(ii), bb(ii, :), parameters, det_index);

        full = gtPlaceSubImage2(single(spot), full, bb(ii, 1), bb(ii, 2), 'summed');
    end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function out = sfGetLengthMatrixFromCell(cellValues, indexes, repeat)
% GTSHOWFSIM/sfGETLENGTHMATRIXFROMCELL

    if ~exist('indexes', 'var') || isempty(indexes)
        indexes = 1:length(cellValues);
    end
    if ~iscell(cellValues)
        cellValues = mat2cell(cellValues);
    end

    if (repeat)
        out = arrayfun(@(x) repmat(indexes(x), length(cellValues{x}), 1), ...
            1:length(cellValues), 'UniformOutput', false);
    else
        out = arrayfun(@(x) 1:x, indexes, 'UniformOutput', false);
    end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function indexes = sfGetIndexesFromTagname(tagname)
% GTSHOWFSIM/sfGETINDEXESFROMTAGNAME

    indexes = str2double(regexp(tagname, '\d*', 'match'));
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
