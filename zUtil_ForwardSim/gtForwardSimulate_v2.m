function out = gtForwardSimulate_v2(first, last, workingdirectory, phaseID, varargin)
% GTFORWARDSIMULATE_V2  Finds missing diffraction spots and updates grain structure
%     out = gtForwardSimulate_v2(first, last, workingdirectory, [phaseID], varargin)
%     ------------------------------------------------------------------------------
%
%     INPUT:
%       first            = <int>     first grainid
%       last             = <int>     last grainid
%       workingdirectory = <string>  working directory
%       phaseID          = <int>     phase ID
%       varargin         =           parameters.fsim plus some additional
%                                    options as in gtAppFsimExtraPars():
%
%     OPTIONAL INPUT (p/v pairs):
%       updateSample     = <logical> true/false to update sample.mat and
%                                    spot2grain.mat {true}
%       updateDBtables   = <logical> true/false to update DB table with
%                                    'grainID', 'phaseID', 'flag' {false}
%       area_factor      = <double>  Weight for area criterion {1}
%       position_factor  = <double>  Weight for UV position criterion {1}
%       image_factor     = <double>  Weight for image criterion {1}
%       int_factor       = <double>  Weight for intensity criterion {0}
%
%     OPTIONAL INPUT for twin variants calculation (p/v pairs):
%       variants         = <logical> Use fsim relative to twin variants
%                                    {false}
%       plot_variants    = <logical> Plot the variants orientation wrt to
%                                    the parent orientation as XYZ crystal
%                                    axes {false}
%       num_variant      = <double>  Variant number to forward simulate
%                                    {[]}
%       twin_angles       = <double>  twin angle {[]}
%       twin_axes        = <double>  twin axis {[]}
%       twin_center      = <double>  alternative center for the twin (mm)
%                                    or the twinID if available from the
%                                    same indexed dataset {[]}
%       twin_grain       = <struct>  twin grain structure for common spot
%                                    properties and full image {[]}
%
%     OUTPUT:
%       out  = <struct>   updated grain structure. Fields:
%          .bl             = <struct N> stack of normalized difblobs, along with metadata
%          .proj.geom      = <double>   input for grain reconstruction
%                                        / forward projection using ROI
%                                        (hsize, vsize) (Nx12)
%          .proj.geom_full = <double>   input for grain reconstruction
%                                        / forward projection (full images) (Nx12)
%          .proj.geom_abs  = <double>   input for grain reconstruction
%                                        / forward projection of extinction spots (Nx12)
%          .proj.difstack  = <double>   stack of normalized difspots (XxYxN)
%          .ondet          = <double>   indices of those spots (from the list in allblobs)
%                                       which are expected on the detector
%          .uvw            = <double>   spot positions on the detector (Nx3)
%          .hklsp          = <double>   signed hk(i)l of reflection (Nx3(4))
%          .hkl            = <double>   hk(i)l family for each reflection (Nx3(4))
%          .flag           = <int>      status flag indicating result of forward simulation
%          .allblobs       = <struct>   forward simulation output produced by gtIndexFwdSimGrains
%          .used_fam       = <double>   List of unique HKL families used
%                                       (theoretical)
%          .used_famsp     = <double>   List of all the HKL families used
%                                       (theoretical)
%
%       We will classify spots with a flag  [1...6]:
%        - 1 : No intensity at expected spot position
%        - 2 : Found intensity in FULL images at predicted position
%        - 3 : Found segmented difspot at expected position - but position
%              / size do not match strict criteria
%        - 4 : Found segmented difspot in database matching the strict
%              search criteria - we will use theoretically predicted
%              angles for reconstruction
%        - 5 : Difspot is part of a pair identified by INDEXTER - use
%              Friedelpair information for reconstruction
%        - 6 : Conflict difspot (if existing)
%
%     Version 010 21-07-2014 by LNervo
%       Extracted the ASTRA geometry and the gtFwdSimFindCandidates routines

%     Version 009 14-05-2014 by LNervo
%       Added check of used hkl families
%
%     Version 008 25-04-2014 by LNervo
%       Adapted to use twin forward simulation
%
%     Version 007 Beginning-2014 by LNervo
%       Modulized, simplified, added application parameters to update
%       sample.mat and DB difspot table
%
%     Version 006 18-12-2013 by LNervo
%       Moduling a bit more...
%
%     Version 005 05/03/2013 by Nicola Vigano (nicola.vigano@esrf.fr)
%       Re-structured completely, to be more clear and easy to debug.
%
%     Version 004 21/06/2012 by AKing
%       Trivial change to make segbb zeros(1, 6)
%
%     Version 003 14-06-2012 by LNervo
%       Use gtFsimDefaultParameters


if (isdeployed)
    global GT_DB %#ok<NUSED,TLEV>
    global GT_MATLAB_HOME %#ok<NUSED,TLEV>
    load('workspaceGlobal.mat');
end

cd(workingdirectory);
gtDBConnect();

parameters = gtLoadParameters();
if (~isfield(parameters, 'detgeo') || isempty(parameters.detgeo))
    parameters.detgeo = gtGeoConvertLegacyLabgeo2Detgeo(parameters.labgeo);
end
detgeo = parameters.detgeo;
if (~isfield(parameters.labgeo, 'omstep') || isempty(parameters.labgeo.omstep))
    parameters.labgeo.omstep = gtAcqGetOmegaStep(parameters);
end
labgeo = parameters.labgeo;
samgeo = parameters.samgeo;

if (~isfield(parameters.seg, 'writehdf5') || ~parameters.seg.writehdf5)
    error('gtForwardSimulate_v2:wrong_parameter', ...
        [ 'Segmentation has to write HDF5 blobs in order for ' ...
          'ForwardSimulation to work properly. You can now create them ' ...
          'using some of the conversion functions like "gtConvertRawImages2Blobs"' ])
end

if (~isfield(parameters.fsim, 'save_grain'))
    parameters.fsim.save_grain = true;
end
if (~exist('phaseID', 'var') || isempty(phaseID))
    phaseID = 1;
end

[parameters.fsim, rej_pars] = parse_pv_pairs(parameters.fsim, varargin)

if (isdeployed)
    first   = str2double(first);
    last    = str2double(last);
    phaseID = str2double(phaseID);
    parameters.fsim.display_figure = false;
end

conf = struct( ...
    'det_index', [], ...
    'debug', false, ...
    'twin_angles', zeros(0, 1), ...
    'twin_axes', zeros(0, 3), ...
    'twin_angular_toll_deg', 0.5, ... Could be raised up to 2.5 or even 5
    'stack', [], ...
    'use_raw_images', 0, ...
    'list', [], ...
    'mode', [], ...
    'newgrains', [] ...
);

conf = parse_pv_pairs(conf, varargin)

if (isempty(conf.det_index))
    conf.det_index = 1:numel(detgeo);
elseif ischar(conf.det_index)
    conf.det_index = sscanf(conf.det_index, '%d');
end

num_dets = numel(detgeo);

fprintf('\nLoading indexing results...\n');
grain = [];
switch parameters.fsim.mode
    case {'indexter', 'addclones'}
        indexgrainModel = fullfile(parameters.acq(1).dir, '4_grains', 'phase_%02d', 'index.mat');
        load(sprintf(indexgrainModel, phaseID), 'grain');
    case 'global_fit'
        indexgrainModel = fullfile(parameters.acq(1).dir, '4_grains', 'phase_%02d', 'index.mat');
        indexter = load(sprintf(indexgrainModel, phaseID), 'grain');
        indexgrainModel = fullfile(parameters.acq(1).dir, '4_grains', 'phase_%02d', 'globalfit.mat');
        load(sprintf(indexgrainModel, phaseID), 'globalfit');
        grain = globalfit.grain;
        parameters.fsim.usestrain = true;
    otherwise
        disp('not recognized option parameters.fsim.mode - can be ''indexter''  or ''global_fit''');
end


filetable   = [parameters.acq(1).name '_filetable'];
samplefile  = fullfile(parameters.acq(1).dir, '4_grains', 'sample.mat');
sample      = GtSample.loadFromLockedFile(filetable, samplefile);
totGrains   = sample.phases{phaseID}.getNumberOfGrains;

if isempty(conf.list)
    grains_list = first : last;
else
    grains_list = conf.list;
    % in case the code is deployed, we have to convert to strings
    if ischar(grains_list)
        grains_list = sscanf(grains_list, '%d');
    end
end

ngrains = numel(grains_list);

% We can either add new grains to the current phase (addclones)
% or import indexing results as a new phase (cloning, farfield)
if strcmpi(parameters.fsim.mode, 'addgrains')
    %     grain(totGrains + 1 : totGrains + ngrains) = conf.newgrains;
    %     totGrains = totGrains + ngrains;
    % taken care of by gtFwdSimAddGrain before starting
    % gtForwardSimulation_v2
elseif strcmpi(parameters.fsim.mode, 'addclones')
    orig_pars = parameters.clone;
%     indexgrainModel = fullfile(orig_pars.acq.dir, '4_grains', 'phase_%02d', 'index.mat');
%     fprintf('\nLoading indexing results for grains to be cloned...\n');
%     orig_grains = load(sprintf(indexgrainModel, orig_pars.phaseID), 'grain');
%     orig_grains = orig_grains.grain;
elseif strcmpi(parameters.fsim.mode, 'cloning')
    orig_pars = parameters.clone;
    indexgrainModel = fullfile(orig_pars.acq.dir, '4_grains', 'phase_%02d', 'index.mat');
    fprintf('\nLoading indexing results for grains to be cloned...\n');
    orig_grains = load(sprintf(indexgrainModel, parameters.clone.phaseID), 'grain');
    num_grains = numel(orig_grains.grain)
    % cl.shift corresponds to dc (translation offset)
    % cl.rot_tensor corresponds to drot (rotation)
    % when running [match,dev,dc,drot,conflicts] = gtINDEXMatchGrains(clone.grain,current.grain,1,dc,drot,tol);
    for n = 1 : num_grains
        grain{n} = orig_grains.grain{grains_list(n)};
        grain{n}.center = (grain{n}.center - orig_pars.shift) * orig_pars.rot_tensor';
        orimat = gtMathsRod2OriMat(grain{n}.R_vector);
        orimat = orimat * orig_pars.rot_tensor;
        R_vector = gtMathsOriMat2Rod(orimat);
        grain{n}.R_vector = gtMathsRod2RodInFundZone(R_vector, orig_pars.cryst(orig_pars.phaseID).symm)';
    end
    indexgrainModel = fullfile(parameters.acq(1).dir, '4_grains', 'phase_%02d', 'index.mat');
    fprintf('\nSaving transformed version of indexing results\n');
    save(sprintf(indexgrainModel, phaseID), 'grain')
    totGrains = ngrains;
elseif strcmpi(parameters.fsim.mode, 'cloning_new')
    orig_pars = parameters.clone;
end

if any(strcmpi(parameters.fsim.mode, {'cloning', 'addclones'}))
    fprintf('\nLoading spots from the other dataset...(cloning...)');
    for ii_d = conf.det_index
        % Initialization of spot table and info about pairs
        originalSpots{ii_d} = gtDBLoadTable([parameters.clone.acq.name 'difspot'], 'difspotID');
        spots_uv = [ ...
            originalSpots{ii_d}.BoundingBoxXorigin, ...
            originalSpots{ii_d}.BoundingBoxYorigin ];
        scatt_vectors = gtGeoDet2Lab(spots_uv, detgeo(ii_d), false);
        originalSpots{ii_d}.ScatteringVector = gtMathsNormalizeVectorsList(scatt_vectors);
        originalPairs{ii_d}     = gtDBLoadTable(parameters.clone.acq.pair_tablename, 'pairID');
    end
    originalSpots = [originalSpots{:}];
    originalPairs = [originalPairs{:}];
    fprintf('(Done).\n')
end

% initialise sample structure
phase = GtPhase(parameters.cryst(phaseID).name, totGrains);
sample.phases{phaseID} = phase;

% if (last > totGrains)
%     warning('gtForwardSimulate_v2:wrong_parameter', ...
%         'Last was bigger than the total number of grains, setting it to max')
%     last = totGrains;
% end

difspotfile = fullfile(parameters.acq(1).dir, '2_difspot', '00000', 'difspot00001.edf');
if (exist(difspotfile, 'file'))
    info = edf_info(difspotfile);
else
    info = [];
end

%%% this section should be replaced with a function dealing with different
% versions of parameters names:  fed, v1, v2...

% !!! using a mean pixel size is uncertain; ideally fwd simulation should
% use mm-s

segmentedSpots = cell(num_dets, 1);
pairsTable = cell(num_dets, 1);

fprintf('\nLoading reflections from database..');
for ii_d = conf.det_index
    % Initialization of spot table and info about pairs
    segmentedSpots{ii_d} = gtDBLoadTable([parameters.acq(ii_d).name 'difspot'], 'difspotID');
    pairsTable{ii_d}     = gtDBLoadTable(parameters.acq(ii_d).pair_tablename, 'pairID');

    % Ehm, the spots' information should should be loaded for each
    % detector each time, but that is not possible yet
    spots_uv = [ ...
        segmentedSpots{ii_d}.BoundingBoxXorigin, ...
        segmentedSpots{ii_d}.BoundingBoxYorigin ];
    scatt_vectors = gtGeoDet2Lab(spots_uv, detgeo(ii_d), false);
    segmentedSpots{ii_d}.ScatteringVector = gtMathsNormalizeVectorsList(scatt_vectors);
end
segmentedSpots = [segmentedSpots{:}];
pairsTable = [pairsTable{:}];
fprintf('(Done).\n')

difspot_num = numel(segmentedSpots.difspotID); % number of difspots in database
spot2grain  = cell(difspot_num, 1); % cell array linking difspots with grainids
spot2phase  = cell(difspot_num, 1);

filetable   = [parameters.acq(ii_d).name '_filetable'];
filename    = fullfile(parameters.acq(ii_d).dir, '4_grains', 'spot2grain.mat');
spotTable   = GtMergeTable(filetable, filename, {'spot2grain', 'spot2phase'});



% initialize output
out = [];

if (parameters.fsim.verbose)
    disp('FwdSim parameters:')
    print_structure(parameters.fsim, 'parameters.fsim', false, true)
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for nn = 1 : ngrains
    n = grains_list(nn);
    fprintf('\nRunning forward simulation for grain %d:\n', n);
    fprintf(' - Computing grain properties..');

    gr = grain{n}; % output from INDEXTER / FITTING

    % Storing information about the grain in sample.mat
    sample.phases{phaseID}.setR_vector(n, gr.R_vector);
    sample.phases{phaseID}.setCenter(n, gr.center);

    %%% Predict spot positions
    gr = gtCalculateGrain(gr, parameters);

    proj = gtFwdSimProjDefinition('fwd_sim', num_dets);
    fwd_sim = repmat(gtFwdSimDataStructureDefinition(0), [num_dets, 1]);

    for ii_d = conf.det_index
        uvw = gr.allblobs(ii_d).detector.uvw;

        %%% Get:
        % - average difspot X and Y sizes for the current spots in the grain
        % - theshold values used during the database search
        % - offset in possible lateral grain orientation
        % - diffstack Vertical and Horizontal sizes
        spotsCommProps = gtFwdSimGetDiffspotsCommonProperties(gr, parameters, ii_d);

        %%% Get the indices of all spots which fully intersect the active
        % region of the detector
        ondet = gtFwdSimFindSpotsOnDetector(spotsCommProps, uvw, parameters);
        fprintf(' (Done).\n - Producing blob masks..')
        switch parameters.fsim.mode
            case {'addclones'}
                orig_gr = gtLoadGrain(orig_pars.phaseID, grain{n}.id, 'parameters', orig_pars);
                proj_bls = predict_spot_masks_from_fwdsimgrain(gr, orig_gr, orig_gr.proj, orig_gr.fwd_sim, ondet, orig_pars, parameters, ii_d)
                %[proj_bls, raw_bls] = predict_spot_masks_from_clone(gr, clone_id, ondet, parameters, ii_d, conf.stack);
            case {'cloning', 'cloning_new'}
                %cl_gr = gtLoadGrain(cl.phaseID, n, 'parameters', cl);
                orig_gr  = gtLoadGrain(gr.phaseid, n, 'parameters', orig_pars);
                %proj_bls = cl_gr.proj_bls;
                proj_bls = predict_spot_masks_from_fwdsimgrain( ...
                gr, orig_gr, orig_gr.proj, orig_gt.fwd_sim, ondet, parameters, ii_d);
            case 'farfield'
                proj_bls = predict_spot_masks_from_farfield(gr, ondet, parameters, ii_d);
            case {'indexter', 'addgrains'}
                proj_bls = predict_spot_masks_from_indexed( ...
                gr, grain, segmentedSpots, spotsCommProps, ondet, parameters, ii_d, conf.debug);
            case {'global_fit'}
                proj_bls = predict_spot_masks_from_indexed( ...
                gr, indexter.grain, segmentedSpots, spotsCommProps, ondet, parameters, ii_d, conf.debug);
                gr.stat = indexter.grain{gr.id}.stat;  %re-use grain statistics from indexter; TODO: update spot statistics in fitting
                gr.pairid = indexter.grain{gr.id}.pairid; % idem...to be updated, it is currently only a copy of initial indexing
                gr.nof_pairs = indexter.grain{gr.id}.nof_pairs;
        end

        fprintf(' (Done).\n - Preallocating structs..')

        if (parameters.fsim.assemble_figure)
            full = zeros(detgeo(ii_d).detsizev, detgeo(ii_d).detsizeu);
            full_raw = zeros(detgeo(ii_d).detsizev, detgeo(ii_d).detsizeu);
        else
            full = [];
            full_raw = [];
        end

        % number of exspected diffraction spots
        numspots = numel(ondet);

        fprintf(' (Done).\n - Matching reflections and spots..');

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%% Forward simulation:
        % We try to match the reflections with the segmented spots, so we
        % examine all hklsp and search corresponding spots / intensity on
        % the detector..
        %%% But also build the diffraction blob stack
        switch parameters.fsim.mode
            case 'to_be_repaired'
                [fwd_sim(ii_d), bl] = forward_simulate_clone(gr, ondet, ...
                    spotsCommProps, segmentedSpots, originalSpots, pairsTable, originalPairs, proj_bls, raw_bls, parameters, conf, ii_d);
            case {'indexter', 'farfield', 'cloning', 'addclones', 'addgrains', 'global_fit'}
                [fwd_sim(ii_d), bl] = forward_simulate_grain(gr, ondet, ...
                    spotsCommProps, segmentedSpots, pairsTable, proj_bls, parameters, conf, ii_d);
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%% Re-check flag == 1, to see if there's intensity at the
        % predicted position, if requested.
        if (parameters.fsim.check_spot)
            fprintf(' (Done).\n -  Checking spots in Raw images..');

            toBeChecked = find(fwd_sim(ii_d).flag == 1);
            toBeChecked = reshape(toBeChecked, 1, []);
            for ii = 1 : numspots
                [found_intensity, spotInfo] = gtFwdSimCheckSpotInRawImages(...
                    uvw(ondet(ii), :), spotsCommProps, parameters);

                if (parameters.fsim.assemble_figure)
                        full_raw = gtPlaceSubImage2(spotInfo.spot, full_raw, ...
                            spotInfo.bb(1), spotInfo.bb(2), 'summed');
                end

                if (found_intensity && ismember(ii, toBeChecked))
                    % Found intensity in FULL images at predicted position
                    fwd_sim(ii_d).flag(ii) = 2;
                    full = gtPlaceSubImage2(spotInfo.spot, full, ...
                            spotInfo.bb(1), spotInfo.bb(2), 'summed');
                end
                fwd_sim(ii_d).check_spots(ii).info = spotInfo;
            end
             % keep check spot info for all these spots in output
            out.checkSpots = fwd_sim(ii_d).check_spots;
        end

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Reflections that have been matched to segmented spots
        included = find(fwd_sim(ii_d).flag >= 3);
        % Removing entries that were not included
        fwd_sim(ii_d) = clean_fwd_sim(fwd_sim(ii_d), included);

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%% We now build the diffraction stack
        fprintf(' (Done).\n - Building difstack..');

        difstack = gtFwdSimBuildDifstackSpots( ...
            fwd_sim(ii_d).difspotID, parameters, ...
            spotsCommProps.stackUSize, spotsCommProps.stackVSize, ...
            fwd_sim(ii_d).bb, ii_d, info);

        if (parameters.fsim.assemble_figure)
            % no padded spots
            full = sfBuildFull(fwd_sim(ii_d).difspotID, parameters, ...
                fwd_sim(ii_d).bb, ii_d);
        end

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%% We now build the Geometry
        fprintf(' (Done).\n - Building geometry..');

        %%% Diffraction geometry
        % diff_beam_dirs: direction of diffracted beam used in ASTRA
        identif_indexed = fwd_sim(ii_d).flag(included) == 5;
        incl_indexed = find(identif_indexed);
        incl_not_indexed = find(~identif_indexed);
        additional_output = true;
        if 0 %isempty(incl_not_indexed) || (strcmpi(parameters.fsim.mode, 'indexter') && isempty(incl_indexed))
            warning('gtForwardSimulate_v2:wrong_grain', ...
                ['Grain "%d" is probably a badly indexed grain.\n' ...
                ' It will be deselected'], n)
            sample.phases{phaseID}.setSelected(n, false);
            additional_output = false;
            continue;  % loop over detectors
        end

        % Diffraction vectors directions
        diff_beam_dirs = zeros(numel(included), 3);

        % we found a segmented spot - can use the theoretically
        % predicted direction or apparent direction (based on CoM of
        % grain and spot) for backprojection

        if (parameters.fsim.use_th)
            % use theoretically predited directions (default)
            fwd_sim(ii_d).uvw(:, 3) = gr.allblobs(ii_d).omega(ondet(included));
            % theoretically predicted beam direction (in sample
            % coordinate system)
            diff_beam_dirs = gr.allblobs(ii_d).dvecsam(ondet(included), :);
        else
            % use experimentally determined omega angles (which we find in: 
            % fwd_sim(ii_d).uvw(incl_not_indx, 3))
            % projection direction as inferred from grain / spot center
            % positions
            % If available, we want to use the angles as determined from
            % the Friedel pairs - interrogate spotpair table
            [diff_beam_dirs(incl_indexed, :), fwd_sim(ii_d).uvw(incl_indexed, 3)] = sfGetFriedelPair( ...
            pairsTable, fwd_sim(ii_d).difspotID(incl_indexed), fwd_sim(ii_d).spot_type(incl_indexed) == 'a');
            diff_beam_dirs(incl_not_indexed, :) = gtGeoDiffVecInSample( ...
                fwd_sim(ii_d).uvw(incl_not_indexed, 1:2), ...
                fwd_sim(ii_d).uvw(incl_not_indexed, 3), ...
                gr.center, detgeo(ii_d), labgeo, samgeo);
        end

        %%% Selection of the spots
        [selected, goodness] = gtFwdSimSelectSpots( ...
            fwd_sim(ii_d).likelihood, fwd_sim(ii_d).intensity, ...
            fwd_sim(ii_d).avg_pixel_int, fwd_sim(ii_d).flag(included), ...
            parameters);

        %%% We now Build ASTRA's geometry
        [proj(ii_d), fwd_sim(ii_d).gv_verts] = gtFwdSimBuildProjGeometry( ...
            diff_beam_dirs, gr.center, fwd_sim(ii_d).uvw(:, 3), ...
            fwd_sim(ii_d).bb, parameters, ...
            spotsCommProps.stackUSize, spotsCommProps.stackVSize, ...
            selected, ii_d);
        % Selections
        proj(ii_d).ondet    = ondet;
        proj(ii_d).included = included;
        % selected: logicals with pre-selected active spots
        proj(ii_d).selected = selected;
        % blob/spot stacks
        proj(ii_d).bl    = bl(included);
        proj(ii_d).stack = difstack;
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%% We now look for possible twins if asked
    if (~isempty(conf.twin_angles))
        fprintf(' (Done).\n - Searching twins..');

        fp_twins = forward_simulate_twin(gr, fwd_sim, proj, conf, ...
            spotsCommProps, segmentedSpots, pairsTable, parameters);

        fprintf(' (Done).\n - Num spots per twin variant per detector:\n')
        for ii_d = 1:num_dets
            fprintf('   + Detector: %d\n', conf.det_index(ii_d))
            for ii_t = 1:numel(fp_twins)
                fprintf('     %d) included: %d, auto-selected: %d\n', ii_t, ...
                    numel(fp_twins(ii_t).proj(ii_d).included), ...
                    numel(find(fp_twins(ii_t).proj(ii_d).selected)) );
            end
        end
        fprintf(' - Assembling final data-structure..')
    else
        fp_twins = [];
        fprintf(' (Done).\n - Assembling final data-structure..')
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%% Now fill in the information for the output
    %  remove reflections for which no segmented spot has been found
    out.id        = n;
    out.phaseid   = phaseID;
    out.R_vector  = gr.R_vector;
    out.center    = gr.center;
    out.allblobs  = gr.allblobs;
    out.fwd_sim   = fwd_sim;

    out.check     = fwd_sim(1).check;
    out.flag      = fwd_sim(1).flag;
    out.pairid    = gr.pairid;
    out.spotid    = fwd_sim(1).spotid;

    % only included spots info
    out.cands     = fwd_sim(1).cands;
    out.likelihood = fwd_sim(1).likelihood;
    out.difspotID = fwd_sim(1).difspotID;
    out.uv_exp    = fwd_sim(1).uvw(:, 1:2);
    out.om_exp    = fwd_sim(1).uvw(:, 3);
    out.bb_exp    = fwd_sim(1).bb;
    out.intensity = fwd_sim(1).intensity;

    % save full image
    out.full     = full;
    out.full_raw = full_raw;
    out.proj_bls = proj_bls;

    % diffraction geometry
    out.proj = proj;

    % Twin variants, if requested
    out.twin_vars = fp_twins;
    % Provide a subset of the output even if grain will be deselected automatically
    if additional_output
        found_reflections = numel(find(fwd_sim(1).flag > 1));
        out.completeness  = found_reflections / numspots;
        out.completeness_incl = numel(find(fwd_sim(1).flag >= 3)) / numspots;

        out.goodness      = goodness;
        out.goodness_sel  = mean(out.likelihood(selected));
        out.confidence    = out.goodness * out.completeness_incl;
    else
        found_reflections = numel(find(fwd_sim(1).flag > 1));
        out.completeness  = NaN;
        out.goodness      = NaN;
        out.goodness_sel  = NaN;
        out.confidence    = NaN;
    end

    if (parameters.fsim.save_grain)
        fprintf(' (Done).\n - Saving ouput..');

        gtSaveGrain(phaseID, n, out);
    end

    if (parameters.fsim.display_figure)
        fprintf(' (Done).\n - Displaying figure..');
        if additional_output
            displayShowFsim(phaseID, out, rej_pars{:})
            %figure; imshow(full_raw,[]);
            %gtBrightnessContrast(gca);
        else
            fprintf('no difspots found for grain %d\n', n)
        end
    end

    fprintf(' (Done).\n\n');

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%% Now update spotTables and sample
    spot2grain(out.difspotID) = {n};
    spot2phase(out.difspotID) = {phaseID};

    sample.phases{phaseID}.setCompleteness(n, out.completeness);


    if (isempty(out.proj(1).selected))
        warning('gtForwardSimulate_v2:bad_value', ...
            'No spot was selected! This will make reconstruction impossible', ...
            'grain has been deselected')
       sample.phases{phaseID}.setSelected(n, false);
    elseif (numel(out.proj(1).selected) < 10)
        warning('gtForwardSimulate_v2:bad_value', ...
            'Too few selected spots! This will give a very bad reconstruction')
    end

    if (out.goodness < 0.25)
        warning('gtForwardSimulate_v2:very_low_goodness', ...
            [ 'Very low goodness (included %f, selected %f) indicates a' ...
              ' probably poor quality of grain reconstruction' ], ...
            out.goodness, out.goodness_sel)
    elseif (out.goodness < 0.5)
        warning('gtForwardSimulate_v2:low_goodness', ...
            [ 'Low goodness (included %f, selected %f) indicates a' ...
              ' possibly poor quality of grain reconstruction' ], ...
            out.goodness, out.goodness_sel)
    end

    if (~parameters.fsim.check_spot)
        fprintf(['Completeness estimates may be too pessimistic - activate ' ...
                 'option ''check_spot'' for more reliable results\n'])
    end

    fprintf('\n Grain number %d:\n', n);

    newspots = length(find(fwd_sim(1).flag == 4));
    fprintf(['Forward simulation found intensity at %d out of %d expected ' ...
             'spot positions \n'], found_reflections, numspots);
    fprintf('Completeness estimate:  %f\n', out.completeness);
    fprintf('Goodness estimate: included %f, selected %f\n', ...
        out.goodness, out.goodness_sel);
    fprintf('%d spots will be used for reconstruction\n', length(out.proj(1).selected));
    fprintf(['%d new spots have been detected in addition to the %d ' ...
        'spotpairs identified by INDEXTER\n'], newspots, gr.nof_pairs);


    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if (parameters.fsim.save_grain)
        % update difspot table in the DB
        % keep list of difspots
        spotID = out.spotid;
        spotID(out.spotid == 0) = [];
        flags = out.flag;
        flags(out.spotid == 0) = [];

        gtDBFillUpdateColumn([parameters.acq(conf.det_index).name 'difspot'], ...
            'grainID', 'int', 'integral', 'difspotID', {out.difspotID}, n)
        gtDBFillUpdateColumn([parameters.acq(conf.det_index).name 'difspot'], ...
            'phaseID', 'int', 'integral', 'difspotID', {out.difspotID}, phaseID)
        gtDBFillUpdateColumn([parameters.acq(conf.det_index).name 'difspot'], ...
            'flag', 'int', 'integral', 'difspotID', {spotID}, {flags});
    end
end

if (parameters.fsim.save_grain)
    % fill spotTables
    spotTable.fillTable(spot2grain, 'spot2grain');
    spotTable.fillTable(spot2phase, 'spot2phase');
    % save sample.mat
%     tmp = GtSample.loadFromFile;
%     while (tmp.phases{phaseID}.grains_number < n)
%         tmp.phases{phaseID}.pushGrain();
%     end
%     tmp.saveToFile
    sample.mergeToFile(filetable, samplefile, 'sample');
end

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% sub-functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Forward Simulation

function [fwd_sim, bl] = forward_simulate_grain(gr, ondet, ...
        spotsCommProps, segmentedSpots, pairsTable, proj_bls, parameters, conf, det_index)

    uvw = gr.allblobs(det_index).detector.uvw;

    % number of exspected diffraction spots
    numspots = numel(ondet);

    % stack of zero padded diffraction spots (projections used for 3D reconstruction)
    bl(1:numspots) = gtFwdSimBlobDefinition();
    fwd_sim = gtFwdSimDataStructureDefinition(numspots);

    % We try to match the reflections with the segmented spots, so we
    % examine all hklsp and search corresponding spots / intensity on the
    % detector..
    %%% But also build the diffraction blob stack
    for ii = 1:numspots
        % Producing predicted spot's properties
        switch parameters.fsim.mode
            case 'to_be_repaired'
                spot_props = predict_spot_properties_clone(spotsCommProps, proj_bls(ii), parameters);
            case 'global_fit'
                spot_props = predict_spot_properties(spotsCommProps, proj_bls(ii), gr.stat, parameters);
            otherwise
                spot_props = predict_spot_properties(spotsCommProps, proj_bls(ii), gr.stat, parameters);
        end

        % Look for difspots appying loose search criteria - is there any intensity at the predicted spot
        % position ?  Choose the one which is closest to the expected size
        [candidateIDs, cands_info] = gtFwdSimFindCandidates(spot_props, gr, ondet(ii), segmentedSpots, parameters, det_index);

        if (conf.debug)
            fwd_sim.cands_info(ii).info = cands_info;
            fwd_sim.cands_info(ii).candidateIDs = candidateIDs;
        end

        if (isempty(candidateIDs))
            % NO CANDIDATES FOUND:
            %   segmentation / overlap / intensity / ... problem ?

            if (cands_info.found_intensity)
                % A bigger spot was found in the region.. it may be an
                % overlap
                fwd_sim.flag(ii) = 2;
            end

            % If requested, we will check into the raw images later.
            continue;
        end

        bls = gtFwdSimBuildDifstackBlobs(candidateIDs, parameters, ...
            spotsCommProps.stackUSize, spotsCommProps.stackVSize, det_index);

        proj_bl = proj_bls(ii * ones(numel(bls), 1));

        % Computing spot area/shape deviation/likelihood
        ls_pb = compute_proj_bbs_partial_likelihoods(bls, proj_bl);

        refl_indx = ondet(ii) * ones(numel(bls), 1);

        % Computing UV deviation/likelihood
        th_uv = uvw(refl_indx, 1:2);
        blobs_uv = [ cands_info.CentroidX, cands_info.CentroidY ];
        ls_uv = compute_uv_partial_likelihoods(blobs_uv, th_uv, proj_bl);

        % Computing W deviation/likelihood
        th_w = gr.allblobs(det_index).omega(refl_indx);
        blob_w = cands_info.CentroidImage * parameters.labgeo.omstep;
        ls_w = compute_w_partial_likelihoods(blob_w, th_w);

        % in case there are several candidates, take the one with best
        % likelihood in terms of: shape, UV position, and W position.
        ls = sqrt(ls_pb) .* ls_uv .* ls_w;  % why sqrt only on first term ?
        % TO DO: add an intensity likelihood criterion here as well !
        [~, cand_inc_index] = max(ls);

        fwd_sim.cands(ii).id = candidateIDs';
        fwd_sim.cands(ii).likelihood_tot = ls';
        fwd_sim.cands(ii).likelihood_proj_mask = ls_pb';
        fwd_sim.cands(ii).likelihood_dev_uv = ls_uv';
        fwd_sim.cands(ii).likelihood_dev_w = ls_w';

        fwd_sim.spotid(ii) = candidateIDs(cand_inc_index);

        bl(ii) = bls(cand_inc_index);

        fwd_sim.uvw(ii, :) = [ ...
            cands_info.CentroidX(cand_inc_index), ...
            cands_info.CentroidY(cand_inc_index), ...
            cands_info.CentroidImage(cand_inc_index) * parameters.labgeo.omstep ];
        fwd_sim.bb(ii, :) = [...
            cands_info.BoundingBoxXorigin(cand_inc_index), ...
            cands_info.BoundingBoxYorigin(cand_inc_index), ...
            cands_info.BoundingBoxXsize(cand_inc_index), ...
            cands_info.BoundingBoxYsize(cand_inc_index) ];
        % The following should be beter than the previous, but it seems to
        % geneterate problems
%         fwd_sim.bb(ii, :) = bls(cand_inc_index).mbbsize([1 2 4 5]);
        fwd_sim.intensity(ii) = bls(cand_inc_index).intensity;
        fwd_sim.avg_pixel_int(ii) = fwd_sim.intensity(ii) / sum(bls(cand_inc_index).mask(:));
        fwd_sim.likelihood(ii) = ls(cand_inc_index);

        % Found a segmented difspot at expected position:
        % we will check if it does match strict criteria
        uv_ok = fwd_sim.cent(ii, :) <= spotsCommProps.Offset; % fwd_sim.cent is not set!
        bb_ok = all( [...
            between(fwd_sim.bb(ii, 3), spotsCommProps.XsizeMin, spotsCommProps.XsizeMax), ...
            between(fwd_sim.bb(ii, 4), spotsCommProps.YsizeMin, spotsCommProps.YsizeMax) ] );
        fwd_sim.check(ii, :)  = [uv_ok, bb_ok];
        if conf.debug
            disp([ii, uv_ok, bb_ok])
        end
        if any(strcmpi(parameters.fsim.mode, {'cloning', 'farfield', 'addclones', 'global_fit'}))
            isSpotA = any(pairsTable.difAID == fwd_sim.spotid(ii));
            isSpotB = any(pairsTable.difBID == fwd_sim.spotid(ii));
        else
            isSpotA = any(gr.difspotidA == fwd_sim.spotid(ii));
            isSpotB = any(gr.difspotidB == fwd_sim.spotid(ii));
        end

        if (isSpotA || isSpotB)
            % This spot is part of a pair and has been used by INDEXTER
            fwd_sim.flag(ii) = 5;
            fwd_sim.spot_type(ii) = char('a' * isSpotA + 'b' * isSpotB);
        elseif all(fwd_sim.check(ii, :))
            fwd_sim.flag(ii) = 4; % This flag is kinda obsolete
        else
            fwd_sim.flag(ii) = 3;
        end
    end
end

function [fwd_sim, bl, proj] = forward_simulate_clone(gr, ondet, ...
        spotsCommProps, segmentedSpots, originalSpots, pairsTable, originalPairs, proj_bls, raw_bls, parameters, conf, det_index)

    uvw = gr.allblobs.detector(det_index).uvw;
    avg_range = ceil(mean((originalSpots.EndImage(gr.difspotidA) - originalSpots.StartImage(gr.difspotidA)) ...
        .* abs(sind(originalPairs.eta(gr.pairid)))));
    om_step = gtGetOmegaStepDeg(parameters, det_index);
    % number of exspected diffraction spots
    numspots = numel(ondet);

    % stack of zero padded diffraction spots (projections used for 3D reconstruction)
    bl(1:numspots) = gtFwdSimBlobDefinition();
    fwd_sim = gtFwdSimDataStructureDefinition(numspots);

    % We try to match the reflections with the segmented spots, so we
    % examine all hklsp and search corresponding spots / intensity on the
    % detector..
    %%% But also build the diffraction blob stack
    num_pix = 1;

    for ii = 1:numspots
        num_im = ceil(min(avg_range ./ abs(sind(gr.allblobs.eta(ondet(ii)))), parameters.fsim.omegarange ./ om_step));
        dil_mask = imdilate(single(proj_bls(ii).mask), ones(num_pix,num_pix,num_im));
        thr = 0.1 * max(raw_bls(ii).intm(:) .* dil_mask(:));
        seg_vol = imreconstruct(logical(dil_mask), raw_bls(ii).intm > thr);
        area_seg  = numel(find(sum(seg_vol, 3) > 0));
        area_proj = numel(find(proj_bls(ii).mask > 0));
        if conf.debug
            figure(1);imshow(sum(seg_vol.*raw_bls(ii).intm,3),[]);figure(2);imshow(sum(proj_bls(ii).intm,3),[])
            figure(3);imshow(sum(raw_bls(ii).intm,3),[])
            drawnow;pause(1)
        end
        if between(area_seg, area_proj/1.1, area_proj*1.1)
            fwd_sim.flag(ii) = 4;
            disp('valid spot')
        else
            fwd_sim.flag(ii) = 2;
            disp('dicarded spot')
        end
  
        bl(ii).intm = raw_bls(ii).intm .* dil_mask;
        difstack(:,ii,:) = sum(bl(ii).intm, 3);
        fwd_sim.bb(ii,:) = [raw_bls(ii).bbuim(1), raw_bls(ii).bbvim(1), raw_bls(ii).bbsize(1:2)];
        fwd_sim.uvw(ii, :) = uvw(ondet(ii),:);
        fwd_sim.difspotID(ii) = 0;
    end

    included = find(fwd_sim.flag >=4);
    selected = true(numel(included), 1);

    if (parameters.fsim.use_th)
        diff_beam_dirs = gr.allblobs.dvecsam(ondet(included), :);
    else
        % use experimentally determined omega angles (which we find in:
        % fwd_sim(ii_d).uvw(incl_not_indx, 3))
        % projection direction as inferred from grain / spot center
        % positions
        diff_beam_dirs = gtGeoDiffVecInSample( ...
            fwd_sim.uvw(included, 1:2), ...
            fwd_sim.uvw(included, 3), ...
            gr.center, detgeo(ii_d), labgeo, samgeo);
    end

   fwd_sim = clean_fwd_sim(fwd_sim, included);
    %%% We now Build ASTRA's geometry
    [proj, fwd_sim.gv_verts] = gtFwdSimBuildProjGeometry( ...
        diff_beam_dirs, gr.center, fwd_sim.uvw(:, 3) * om_step, ...
        fwd_sim.bb, parameters, ...
        fwd_sim.bb(1, 3), fwd_sim.bb(1, 4), ...
        selected);
    % Selections
    proj.ondet    = ondet;
    proj.included = included;
    % selected: logicals with pre-selected active spots
    proj.selected = selected;
    % blob/spot stacks
    proj.bl    = bl(included);
    proj.stack = difstack(:,included,:);
    proj.num_iter = parameters.rec.grains.num_iter;
end
function twin_vars = forward_simulate_twin(gr, gr_fwd_sim, gr_proj, conf, spotsCommProps, segmentedSpots, pairsTable, parameters)
    sg = parameters.cryst(gr.phaseid).spacegroup;
    lp = parameters.cryst(gr.phaseid).latticepar;

    num_dets = numel(conf.det_index);

    % Computing all twin variants for all the given pairs angle-axis
    num_angles = numel(conf.twin_angles);
    twin_vars = cell(num_angles, 1);
    for ii_a = 1:num_angles
        twin_vars{ii_a} = gtTwinOrientations(gr.R_vector, ...
            conf.twin_angles(ii_a), conf.twin_axes(ii_a, :), sg, lp);
    end
    twin_vars = [twin_vars{:}];

    spotsCommProps.XsizeMin = 5;
    spotsCommProps.YsizeMin = 5;
    spotsCommProps.max_n_offset_deg = conf.twin_angular_toll_deg;
    spotsCommProps.max_t_offset_deg = 0.25;

    num_twin_vars = numel(twin_vars);
    fprintf('\b\b, variant: ')
    for ii_t = 1:num_twin_vars
        num_chars = fprintf('%d/%d', ii_t, num_twin_vars);

        twin = struct('id', gr.id, 'phaseid', gr.phaseid, ...
            'R_vector', twin_vars(ii_t).R_vector, 'center', gr.center);
        twin = gtCalculateGrain(twin, parameters, 'ref_omind', gr.allblobs.omind);

        % Determining reflections that are shared with the twin and
        % selected by the parent
        refl_match = gt6DGetMatchingReflections(gr, twin, conf.twin_angular_toll_deg * 2);

        twin.stat = gr.stat;
        %twin.stat.bbxsstd = twin.stat.bbxsstd * 0.1;
        %twin.stat.bbysstd = twin.stat.bbysstd * 0.1;
        twin.difspotidA = [];
        twin.difspotidB = [];

        twin_fwd_sim = repmat(gtFwdSimDataStructureDefinition(0), [num_dets, 1]);
        twin_proj = gtFwdSimProjDefinition('fwd_sim', num_dets);

        for ii_d = conf.det_index
            om_step = gtAcqGetOmegaStep(parameters, ii_d);
            spotsCommProps.max_w_offset_im = conf.twin_angular_toll_deg / om_step;

            uvw = twin.allblobs.detector(ii_d).uvw;
            twin_ondet = gtFwdSimFindSpotsOnDetector(spotsCommProps, uvw, parameters);

            proj_bls = predict_spot_masks_from_fwdsimgrain(twin, gr, ...
                gr_proj(ii_d), gr_fwd_sim(ii_d), twin_ondet, parameters, parameters, ii_d);

            [twin_fwd_sim(ii_d), bl] = forward_simulate_grain(...
                twin, twin_ondet, spotsCommProps, segmentedSpots, pairsTable, proj_bls, parameters, conf, ii_d);

            twin_incl = find(twin_fwd_sim(ii_d).flag >= 3);
            twin_fwd_sim(ii_d) = clean_fwd_sim(twin_fwd_sim(ii_d), twin_incl);

            twin_proj(ii_d).bl = bl(twin_incl);
            if (~isempty(twin_proj(ii_d).bl))
                spots = arrayfun(@(x){sum(x.intm, 3)}, bl(twin_incl));
                twin_proj(ii_d).stack = permute(cat(3, spots{:}), [1 3 2]);
            end

            % We will auto-select the shared reflections with the parent
            % that got also selcted there (if the same blob was used!)
            parent_sel = false(size(gr.allblobs.omega));
            parent_sel(gr_proj.ondet(gr_proj.included(gr_proj.selected))) = true;

            parent_spots = zeros(size(gr.allblobs.omega));
            parent_spots(gr_proj(ii_d).ondet(gr_proj(ii_d).included)) = gr_fwd_sim(ii_d).difspotID;

            twin_proj(ii_d).ondet = twin_ondet;
            twin_proj(ii_d).included = twin_incl;
            twin_proj(ii_d).selected = refl_match(twin_ondet(twin_incl))...
                & parent_sel(twin_ondet(twin_incl)) ...
                & (parent_spots(twin_ondet(twin_incl)) == twin_fwd_sim(ii_d).difspotID);
        end

        twin_vars(ii_t).allblobs = twin.allblobs;
        twin_vars(ii_t).fwd_sim = twin_fwd_sim;
        twin_vars(ii_t).proj = twin_proj;

        % Just to make gtGuiGrainMontage happy
        twin_vars(ii_t).id = gr.id;
        twin_vars(ii_t).phaseid = gr.phaseid;

        fprintf(repmat('\b', [1 num_chars]))
    end

    fprintf('%d/%d', num_twin_vars, num_twin_vars)
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Support Functions

function full = sfBuildFull(spotid, parameters, bb, det_index)
% full = sfBuildFull(spotid, parameters, spotsCommProps, bb)
%
% spotid         : spotid from grain
% parameters     : parameters.mat
% spotsCommProps : spots common properties
% bb             : bounding boxes from DB

    full = zeros(parameters.detgeo(det_index).detsizev, parameters.detgeo(det_index).detsizeu, 'single');

    for ii = 1:numel(spotid)
        [spot, ~] = gtFwdSimBuildSpot(spotid(ii), bb(ii, :), parameters, det_index);

        full = gtPlaceSubImage2(spot, full, bb(ii, 1), bb(ii, 2), 'summed');
    end
end

function [diff_beam_dir, omega, pairID] = sfGetFriedelPair(pairsTable, spotID, isSpotA, isSpotB)
% [diff_beam_dirs, omega] = sfGetFriedelPair(pairsTable, spotID, isSpotA)

    for ii = numel(spotID):-1:1
        if (isSpotA(ii))
            pairID(ii) = find(pairsTable.difAID == spotID(ii));
            omega(ii) = pairsTable.omega(pairID(ii));
            diff_beam_dir(ii, :) = [pairsTable.ldirX(pairID(ii)), pairsTable.ldirY(pairID(ii)), pairsTable.ldirZ(pairID(ii))];
        %elseif (isSpotB(ii))
        else
            pairID(ii) = find(pairsTable.difBID == spotID(ii));
            omega(ii) = pairsTable.omegaB(pairID(ii));
            % in this case we have to reverse the line direction
            diff_beam_dir(ii, :) = -[pairsTable.ldirX(pairID(ii)), pairsTable.ldirY(pairID(ii)), pairsTable.ldirZ(pairID(ii))];
        end
    end
end

function displayShowFsim(phaseID, grain, varargin)
% displayShowFsim(phaseID, grain, varargin)

    if isempty(varargin)
        [vars_pars, vars_list] = gtAppFsimDefaultPars();
        ind = findValueIntoCell(fieldnames(vars_pars), {'fsimtype', 'f_title'});
        vars_list(ind(:, 1), 4) = {2};

        vars_pars = gtModifyStructure(vars_pars, vars_list, 1, 'Options for displaying the Forward Sim:');
        varargin = struct2pv(vars_pars);
    end
    gtShowFsim(grain, phaseID, varargin{:});
end

function [proj_bls, raw_bls] = predict_spot_masks_from_clone(gr, clone_id, ondet, parameters, ii_d, stack)
    clone_rec = gtLoadGrainRec(gr.phaseid, clone_id, 'parameters', parameters.clone);
    clone = gtLoadGrain(gr.phaseid, clone_id, 'parameters', parameters.clone);
    use_vol = 'VOL3D';
    dilate_steps = 1;
    clone.proj.centerpix = clone.proj.centerpix - parameters.clone.shift;
    clone.center = gr.center;
    clone.R_vector = gr.R_vector;
    switch (use_vol)
        case 'ODF6D'
              %rot_rod = gtMathsOriMat2Rod(parameters.clone.rot_tensor);
              [int_vol_masked, dm_vol_masked, mask] = gtCreateMaskedVolumes(clone_rec, dilate_steps, 'type' , use_vol);
              [gvdm_orig, dmvol_size] = gtDefDmvol2Gvdm(dm_vol_masked);
              %rot_rod = gtMathsOriMat2Rod(parameters.clone.rot_tensor);
              active_elements = find(sum(gvdm_orig));
              %gvdm_rotated = gtMathsRodSum(rot_rod, gvdm_orig(:, active_elements));
              %orimat = gtMathsRod2OriMat(gvdm);
              %for i = 1 : size(orimat, 3) orimat(:, :, i) = orimat(:, :, i) * parameters.clone.rot_tensor; end
              %gvdm_rotated = gtMathsOriMat2Rod(orimat
              gvdm_rotated = gvdm_orig;
              disp('...checking if still in Fundamental zone...');drawnow;
              tic;gvdm_rotated = gtMathsRod2RodInFundZone(gvdm_rotated, parameters.cryst.symm);toc
              gvdm = gvdm_orig;
              gvdm(:, active_elements) = gvdm_rotated;
              dmvol_rotated = gtDefGvdm2Dmvol(gvdm, dmvol_size);
              volumes.voxels_avg_R_vectors = dmvol_rotated;
%             internal_shift = clone_rec.SEG.segbb(1:3) - clone_rec.ODF6D.shift + 1;
%             seg_vol_bb = [internal_shift, clone_rec.SEG.segbb(4:6)];
%             dm_vol(:, :, :, 3) = gtCrop(clone_rec.ODF6D.voxels_avg_R_vectors(:, :, :, 3), seg_vol_bb);
%             dm_vol(:, :, :, 2) = gtCrop(clone_rec.ODF6D.voxels_avg_R_vectors(:, :, :, 2), seg_vol_bb);
%             dm_vol(:, :, :, 1) = gtCrop(clone_rec.ODF6D.voxels_avg_R_vectors(:, :, :, 1), seg_vol_bb);
%             grain_vol = gtCrop(clone_rec.ODF6D.intensity, seg_vol_bb);
              %[int_vol_masked, dm_vol_masked, mask] = gtCreateMaskedVolumes(clone_rec, dilate_steps, 'type' , use_vol);
              volumes.intensity = int_vol_masked;
              [volumes, proj] = gtGrainRotateStructure(volumes, clone.proj, use_vol,  parameters, parameters.clone.rot_tensor', [], 'nearest');
              fedpars = gtFedCreateGrainParsMasked(clone, volumes.intensity, volumes.voxels_avg_R_vectors, parameters);
              [clone, gv] = gtDefSyntheticGrainCreate(fedpars, clone, parameters, volumes.voxels_avg_R_vectors, volumes.intensity); 
        case 'SEG'
            grain_vol = single(clone_rec.SEG.seg);
            shift = clone_rec.SEG.segbb(1:3);
        case 'VOL3D'
            internal_shift = clone_rec.SEG.segbb(1:3) - clone_rec.VOL3D.shift + 1;
            seg_vol_bb = [internal_shift, clone_rec.SEG.segbb(4:6)];
            grain_vol = gtCrop(clone_rec.VOL3D.intensity, seg_vol_bb);
    end
    shift = clone_rec.SEG.segbb(1:3) - parameters.clone.shift ./ parameters.detgeo.pixelsizeu;
    sam_vol = zeros(parameters.acq(ii_d).bb(3), parameters.acq(ii_d).bb(3), parameters.acq(ii_d).bb(4));
    sam_vol = gtPlaceSubVolume(sam_vol, grain_vol, round(shift));
    sam_vol = gtRotateVolume(sam_vol, parameters.clone.rot_tensor);
    geom = gtGeoProjForReconstruction(gr.allblobs.dvecsam(ondet,:), gr.allblobs.omega(ondet), [], ...
        repmat([1 1 parameters.detgeo.detsizeu parameters.detgeo.detsizev], numel(find(ondet)), 1), ...
        [], parameters.detgeo, parameters.labgeo, parameters.samgeo, ...
        parameters.recgeo, 'ASTRA_full');

    %geom(clone.proj.included, :) = clone.proj.geom_full;
    %geom_grain(clone.proj.included, :) = clone.proj.geom;
    [proj, int] = gtAstraCreateProjections(sam_vol, geom, [parameters.detgeo.detsizeu, parameters.detgeo.detsizev], 1);
%     geom = gtGeoProjForReconstruction(gr.allblobs.dvecsam(ondet,:), gr.allblobs.omega(ondet), gr.center, ...
%          repmat([1 1 clone.proj.num_cols clone.proj.num_rows], numel(find(ondet)), 1), ...
%          [], parameters.detgeo, parameters.labgeo, parameters.samgeo, ...
%          parameters.recgeo, 'ASTRA_grain');
%     vol_center_rec = gtGeoSam2Sam(gr.center, parameters.samgeo, parameters.recgeo, false, false);
%     bb_center_lab = gtGeoSam2Lab(geom(:, 4:6), gr.allblobs.omega(ondet), ...
%         parameters.labgeo, parameters.recgeo, 0, 0);
%     bb_center_det = gtGeoLab2Det(bb_center_lab, parameters.detgeo, 1)
%     proj = gtAstraCreateProjections(clone_rec.SEG.seg, geom, [clone.proj.numvols, clone.proj.numrows], 1);
    num_blobs = numel(ondet);
    proj_bls = gtFwdSimBlobDefinition('blob', num_blobs);
    raw_bls = gtFwdSimBlobDefinition('blob', num_blobs);
    blob_bb = zeros(num_blobs, 6);

    for ii_b = 1 : num_blobs
        omega_im =  round(gr.allblobs.omega(ondet(ii_b)) ./ gtGetOmegaStepDeg(parameters, ii_d));
        props = regionprops(squeeze(proj(:, :, 1, ii_b)) > 0);
        ind = find([props.Area] == max([props.Area]));  % get rid of noise
        bb = round(props(ind).BoundingBox);
        %bb = bb + geom(ii_b, 4:6);
        blob_bb(ii_b, :) = [bb(1), bb(2), omega_im, bb(3), bb(4), 1];  % to do: handle omega range correctly
        proj_bls(ii_b).mbbsize = [bb(3), bb(4)];
        proj_bls(ii_b).mbbu = [bb(1), bb(1) + bb(3) - 1];
        proj_bls(ii_b).mbbv = [bb(2), bb(2) + bb(4) - 1];
        proj_bls(ii_b).mbbw = [omega_im, omega_im] ;  % to do, see above...
        proj_bls(ii_b).mask = gtCrop(squeeze(proj(:,:,1,ii_b) > 0), bb);
        proj_bls(ii_b).intm = gtCrop(squeeze(proj(:,:,1,ii_b)), bb);
        figure(2);imshow(proj_bls(ii_b).intm,[]);drawnow, pause(0.1)
        proj_bls(ii_b).intensity = sum(proj_bls(ii_b).intm(:));
        proj_bls(ii_b).intm = proj_bls(ii_b).intm ./ proj_bls(ii_b).intensity;
    end

    blobSizes = cat(1, proj_bls.mbbsize);
    %stackUSize = round(max(blobSizes(:,1)) * parameters.fsim.oversize);
    %stackVSize = round(max(blobSizes(:,2)) * parameters.fsim.oversize);
    stackUSize = clone.proj.num_cols;
    stackVSize = clone.proj.num_rows;

    test = zeros(2048,2048);
    c = zeros(num_blobs, 2);

    num_im = 5;
    shift_stack = zeros(size(clone.proj.stack));
    for ii_b = 1 : num_blobs
        shifts_blob = gtFwdSimGetStackShifts(stackUSize, stackVSize, blob_bb(ii_b,:), false);
        shifts = [shifts_blob.u, shifts_blob.v, num_im]

        % We are applying a padding of num_im slices (per side) on the w
        % diretion
        blob_size_im = [stackUSize, stackVSize, blob_bb(ii_b, 6) + 2 * num_im];
        blob_bb_im = [blob_bb(ii_b, 1:3) - shifts, blob_size_im];
        bbpos_det_grain(ii_b, :) = [blob_bb_im(1 : 2), stackUSize, stackVSize];

        proj_bls(ii_b).intm = gtPlaceSubVolume(zeros(blob_size_im, 'single'), ...
            permute(proj_bls(ii_b).intm, [2, 1, 3]), shifts);
        %proj_bls(ii_b).intm = permute(proj_bls(ii_b).intm, [2, 1, 3]);  %re-arrange for ASTRA...
        proj_bls(ii_b).mask = gtPlaceSubVolume(false(blob_size_im), ...
            uint8(permute(proj_bls(ii_b).mask, [2, 1, 3])), shifts);
        %proj_bls(ii_b).mask = permute(proj_bls(ii_b).mask, [2, 1, 3]);  %re-arrange for ASTRA...
        proj_bls(ii_b).bbsize = blob_size_im;

        proj_bls(ii_b).bbuim = [blob_bb_im(1), blob_bb_im(1) + blob_bb_im(4) - 1];
        proj_bls(ii_b).bbvim = [blob_bb_im(2), blob_bb_im(2) + blob_bb_im(5) - 1];
        proj_bls(ii_b).bbwim = [blob_bb_im(3), blob_bb_im(3) + blob_bb_im(6) - 1];
        test = gtPlaceSubImage2(proj_bls(ii_b).intm(:,:,num_im+1)', test, proj_bls(ii_b).bbuim(1), proj_bls(ii_b).bbvim(1), 'summed');
        raw_bls(ii_b) = proj_bls(ii_b);
        raw_bls(ii_b).intm = gtCrop(stack, blob_bb_im([1 2 3 4 5 6]));
        if ~all(size(raw_bls(ii_b).intm) == raw_bls(ii_b).bbsize)
            raw_bls(ii_b) = fixSize(raw_bls(ii_b), parameters);
        end
    end
%     if (1)
%         test2 = zeros(size(test));
%         geom_grain = gtGeoProjForReconstruction(gr.allblobs.dvecsam(ondet,:), gr.allblobs.omega(ondet), ...
%         gr.center, bbpos_det_grain, [], parameters.detgeo, parameters.labgeo, parameters.samgeo, ...
%         parameters.recgeo, 'ASTRA_grain');
%         geom_grain(clone.proj.included, :) = clone.proj.geom;
%         proj_bl = gtAstraCreateProjections(grain_vol, geom_grain, [stackVSize, stackUSize], 1);
%         for ii_b = 1 : num_blobs
%             %test2 = gtPlaceSubImage2(proj_bl(:,:,1, ii_b), test2, bbpos_det_grain(ii_b, 1), bbpos_det_grain(ii_b, 2), 'summed');
%
%             ind = find(clone.proj.included == ii_b);
%             if (~isempty(ind))
%                 %c(ii_b, :) = correlate(squeeze(proj_bl(:,:,1,ii_b))', squeeze(clone.proj.stack(:, ind, :)));
%                 clone_bb = [clone.proj.bl(ind).bbuim(1), clone.proj.bl(ind).bbvim(1), clone.proj.bl(ind).bbsize(1:2)];               
%                 try
%                     cropped_proj(:, :, ind) = gtCrop(proj(:, :, 1, ii_b), clone_bb)';
%                     c(ii_b, :) = correlate(cropped_proj(:,:,ind), squeeze(clone.proj.stack(:, ind, :)));
%                     trans = translate(c(ii_b, 1), c(ii_b, 2), clone.proj.num_cols, clone.proj.num_rows);
%                     shift_stack(:, ind, :) = real(ifft2(fft2(squeeze(clone.proj.stack(:, ind, :))).*trans));
%                     for i = 1 : size(clone.proj.bl(ind).intm, 3)
%                         shift_bl(:,:,i) = real(ifft2(fft2(squeeze(clone.proj.bl(ind).intm(:, :, i))).*trans));
%                         shift_mask(:,:,i) = real(ifft2(fft2(squeeze(clone.proj.bl(ind).mask(:, :, i))).*trans));
%                     end
%                     clone.proj.shift_bl(ind) = clone.proj.bl(ind);
%                     clone.proj.shift_bl(ind).intm = shift_bl;
%                     clone.proj.shift_bl(ind).mask = shift_mask;
%                 catch
%                     disp('wrong size..');
%                 end
%             end
%         end
%     end
    if ~isempty(stack)
          for ii_b = 1 : numel(clone.proj.bl)
            raw_bls(ii_b) = clone.proj.bl(ii_b);
            bb = [raw_bls(ii_b).bbuim(1), raw_bls(ii_b).bbvim(1), raw_bls(ii_b).bbwim(1), raw_bls(ii_b).bbsize];
            raw_bls(ii_b).intm = gtCrop(stack, bb);
            if ~all(size(raw_bls(ii_b).intm) == raw_bls(ii_b).bbsize)
                raw_bls(ii_b) = fixSize(raw_bls(ii_b), parameters);
            end
        end
        keyboard
    else
        raw_bls = 1;
    end
end

function proj_bls = predict_spot_masks_from_farfield(gr, ondet, parameters, det_index)
% A primitive version, assuming spherical grains. Could be enhanced by
% calculating Laguerre tesselation of farfield data...and could be enhanced
% by actually matching / assigning the ff-peaks to hklsp's in dct (like
% this we could use measured omgea intervals instead of our poor estimate)

    gr_radius = round(gr.stat.bbxsmean / 2);
    omstep = gtAcqGetOmegaStep(parameters, det_index);
    num_blobs = numel(find(gr.allblobs.detector.ondet));
    uvw = gr.allblobs.detector.uvw(gr.allblobs.detector.ondet, :);

    for ii_b = 1 : size(ondet)
        proj_bls(ii_b).mbbsize = [2 * gr_radius, 2 * gr_radius];
        proj_bls(ii_b).mbbu = round([uvw(ii_b, 1) - gr_radius, uvw(ii_b, 1) + gr_radius]);
        proj_bls(ii_b).mbbv = round([uvw(ii_b, 2) - gr_radius, uvw(ii_b, 2) + gr_radius]);
        proj_bls(ii_b).mbbw = round([uvw(ii_b, 3), uvw(ii_b, 3)] ./ omstep);
        proj_bls(ii_b).mask = ones(proj_bls(ii_b).mbbsize);

        proj_bls(ii_b).bbsize = [2 * gr_radius, 2 * gr_radius];
        proj_bls(ii_b).bbuim = round([uvw(ii_b, 1) - gr_radius, uvw(ii_b, 1) + gr_radius]);
        proj_bls(ii_b).bbvim = round([uvw(ii_b, 2) - gr_radius, uvw(ii_b, 2) + gr_radius]);
        proj_bls(ii_b).bbwim = round([uvw(ii_b, 3), uvw(ii_b, 3)] ./ omstep);
    end
end

function [proj_bls, verts_rec] = predict_spot_masks_from_indexed(gr, grain_INDX, ...
    segmentedSpots, spotsCommProps, ondet, parameters, det_index, debug)

    samgeo = parameters.samgeo;
    labgeo = parameters.labgeo;
    detgeo = parameters.detgeo(det_index);
    omstep = gtAcqGetOmegaStep(parameters, det_index);

    % First argument: included_INDXTR, might be needed in the future for
    % some reason...
    [~, difspotID_INDXTR] = gtFwdSimGetIndexterReflections(gr, grain_INDX{gr.id}, segmentedSpots(det_index));

    % Producing blobs' boundingboxes (as by dilated segmentation info)
    bls_INDXTR = gtFwdSimBuildDifstackBlobs(difspotID_INDXTR, parameters, ...
        spotsCommProps.stackUSize, spotsCommProps.stackVSize, det_index);
    % INDEXTER Data
    BBus = cat(1, bls_INDXTR(:).mbbu);
    BBvs = cat(1, bls_INDXTR(:).mbbv);
    BBsizes = cat(1, bls_INDXTR(:).mbbsize);
    BBs = [BBus(:, 1), BBvs(:, 1), BBsizes(:, 1:2)];

    Ws = segmentedSpots(det_index).CentroidImage(difspotID_INDXTR) .* omstep;

    gr_center = gr.center(ones(numel(difspotID_INDXTR), 1), :);
    % Account for sample shifts
    if isfield(parameters.acq(det_index), 'correct_sample_shifts')
        if (parameters.acq(det_ind).correct_sample_shifts')
            [gr_shift_lab, gr_shift_sam] = gtMatchGetSampleShifts(parameters, Ws);
        else
            gr_shift_lab = zeros(numel(Ws), 3);
        end
    else
        gr_shift_lab = zeros(numel(Ws), 3);
    end
    projvecs = [ ...
        segmentedSpots(det_index).CentroidX(difspotID_INDXTR), ...
        segmentedSpots(det_index).CentroidY(difspotID_INDXTR)];
    projvecs = gtGeoDet2Lab(projvecs, detgeo, false) - gr_shift_lab;
    projvecs = gtGeoLab2Sam(projvecs, Ws, labgeo, samgeo, 0) - gr_center;

    verts_rec = gtFwdSimComputeCircumscribingPolyhedron(gr.center, ...
        projvecs, Ws, BBs, parameters, det_index, 'convhull', debug, gr_shift_lab);

    proj_bls = gtFwdSimProjectVertices2Det(gr, verts_rec, ondet, parameters, det_index, debug);

    % Producing original blobs' boundingboxes (as by original segmentation
    % info)
    BBs = [...
        segmentedSpots(det_index).BoundingBoxXorigin(difspotID_INDXTR), ...
        segmentedSpots(det_index).BoundingBoxYorigin(difspotID_INDXTR), ...
        segmentedSpots(det_index).BoundingBoxXsize(difspotID_INDXTR), ...
        segmentedSpots(det_index).BoundingBoxYsize(difspotID_INDXTR) ];

    verts_rec = gtFwdSimComputeCircumscribingPolyhedron(gr.center, ...
        projvecs, Ws, BBs, parameters, det_index, 'convhull', debug, gr_shift_lab);

    proj_bls_original = gtFwdSimProjectVertices2Det(gr, verts_rec, ...
        ondet, parameters, det_index);

    for ii_b = 1:numel(proj_bls)
        proj_bls(ii_b).mbbsize = proj_bls_original(ii_b).bbsize;
        proj_bls(ii_b).mbbu = proj_bls_original(ii_b).bbuim;
        proj_bls(ii_b).mbbv = proj_bls_original(ii_b).bbvim;
        proj_bls(ii_b).mbbw = proj_bls_original(ii_b).bbwim;

        proj_bls(ii_b).mask = proj_bls_original(ii_b).mask;
    end
end

function proj_bls = predict_spot_masks_from_fwdsimgrain(new_gr, orig_gr, orig_proj, orig_gr_fwd_sim, new_gr_ondet, orig_pars, new_pars, det_ind)
% We here produce the expected bounding box using the main grain's proj
% datastructure, and then we produce the projected blobs at the predicted
% new grain's positions

    if (isempty(orig_gr_fwd_sim.gv_verts))
        sel_bls = orig_proj.bl(orig_proj.selected);
        centroids_w = zeros(numel(sel_bls), 1);
        for ii_b = 1:numel(sel_bls)
            int_profile = reshape(sum(sum(sel_bls(ii_b).intm, 1), 2), 1, []);
            num_slices = numel(int_profile);
            centroids_w(ii_b) = sum((0:num_slices-1) .* int_profile) / num_slices;
        end
        Ws = cat(1, sel_bls.bbwim);
        Ws = (Ws(:, 1) + centroids_w) .* gtAcqGetOmegaStep(orig_pars, det_ind);

        % Producing image blobs' boundingboxes (as by blob size info)
        BBus = cat(1, sel_bls.bbuim);
        BBvs = cat(1, sel_bls.bbvim);
        BBsizes = cat(1, sel_bls.bbsize);
        BBs = [BBus(:, 1), BBvs(:, 1), BBsizes(:, 1:2)];

        projvecs = orig_gr.allblobs.dvecsam(orig_proj.ondet(orig_proj.included(orig_proj.selected)), :);
        verts_rec = gtFwdSimComputeCircumscribingPolyhedron(orig_gr.center, projvecs, Ws, BBs, orig_pars, det_ind, 'convhull');
    else
        verts_rec = orig_gr_fwd_sim.gv_verts;
    end

    proj_bls = gtFwdSimProjectVertices2Det(new_gr, verts_rec, new_gr_ondet, new_pars, det_ind);

    if (isempty(orig_gr_fwd_sim.gv_verts))
        % Producing original blobs' boundingboxes (as by original segmentation
        % info)
        BBus = cat(1, sel_bls.mbbu);
        BBvs = cat(1, sel_bls.mbbv);
        BBsizes = cat(1, sel_bls.mbbsize);
        BBs = [BBus(:, 1), BBvs(:, 1), BBsizes(:, 1:2)];

        verts_rec = gtFwdSimComputeCircumscribingPolyhedron(orig_gr.center, projvecs, Ws, BBs, orig_pars, det_ind, 'convhull', false);
    end

    proj_bls_original = gtFwdSimProjectVertices2Det(new_gr, verts_rec, new_gr_ondet, new_pars, det_ind);

    for ii_b = 1:numel(proj_bls)
        proj_bls(ii_b).mbbsize = proj_bls_original(ii_b).bbsize;
        proj_bls(ii_b).mbbu = proj_bls_original(ii_b).bbuim;
        proj_bls(ii_b).mbbv = proj_bls_original(ii_b).bbvim;
        proj_bls(ii_b).mbbw = proj_bls_original(ii_b).bbwim;

        proj_bls(ii_b).mask = proj_bls_original(ii_b).mask;
    end
end

function spot_props = predict_spot_properties(spotsCommProps, proj_bl, gr_stats, parameters)
    bb_size_factor = parameters.fsim.bbsize_factor;

    % Recentl we introduced a grain volume bounding box estimation that
    % probably considerably over-estimates the grain volume (moving from
    % inscribed polyhedron to convex-hull), so the new minimum sizes have
    % to be adusted. We chose to divide them by the same factor that then
    % multiplies the standard deviation
    spot_props.XsizeAvg = spotsCommProps.Xsize;
    spot_props.YsizeAvg = spotsCommProps.Ysize;
    spot_props.XsizeMin = min(proj_bl.mbbsize(1) / bb_size_factor, spotsCommProps.XsizeMin);
    spot_props.YsizeMin = min(proj_bl.mbbsize(2) / bb_size_factor, spotsCommProps.YsizeMin);

    % Even if the convex shape of the grain computed from indexter's
    % selection had a limited angle view on the grain volume, we add some
    % reduction of the minimum spot size by user definition
    %spot_props.XsizeMin = max(spot_props.XsizeMin - bb_size_factor * gr_stats.bbxsstd, 5);
    %spot_props.YsizeMin = max(spot_props.YsizeMin - bb_size_factor * gr_stats.bbysstd, 5);

    spot_props.XsizeMax = max(proj_bl.mbbsize(1), spotsCommProps.XsizeMax + bb_size_factor * gr_stats.bbxsstd);
    spot_props.YsizeMax = max(proj_bl.mbbsize(2), spotsCommProps.YsizeMax + bb_size_factor * gr_stats.bbysstd);

    spot_props.max_w_offset_im = spotsCommProps.max_w_offset_im;
    if (isfield(spotsCommProps, 'max_n_offset') && ~isempty(spotsCommProps.max_n_offset))
        spot_props.max_n_offset_deg = spotsCommProps.max_n_offset_deg;
        spot_props.max_t_offset_deg = spotsCommProps.max_t_offset_deg;
    end
end

function spot_props = predict_spot_properties_clone(spotsCommProps, proj_bl, parameters)
    % fsim.bbsize_factor allows for variation of grainsize (e.g. due to
    % grain growth between sucessive time steps)
    bb_size_factor = parameters.fsim.bbsize_factor;
    spot_props.XsizeMin = proj_bl.mbbsize(1) / bb_size_factor;
    spot_props.YsizeMin = proj_bl.mbbsize(2) / bb_size_factor;

    spot_props.XsizeMax = proj_bl.mbbsize(1) * bb_size_factor;
    spot_props.YsizeMax = proj_bl.mbbsize(2) * bb_size_factor;

    spot_props.max_w_offset_im = spotsCommProps.max_w_offset_im;
    if (isfield(spotsCommProps, 'max_n_offset') && ~isempty(spotsCommProps.max_n_offset))
        spot_props.max_n_offset_deg = spotsCommProps.max_n_offset_deg;
        spot_props.max_t_offset_deg = spotsCommProps.max_t_offset_deg;
    end
end
 
function fwd_sim = clean_fwd_sim(fwd_sim, included)
    % These were traditionally excluded!
    % fwd_sim.flag = fwd_sim.flag(included);
    % fwd_sim.spotid = fwd_sim.spotid(included);
    % fwd_sim.check = fwd_sim.check(included, :);
    % fwd_sim.check_spots = fwd_sim.check_spots(included);

    % And the traditionally included:
    fwd_sim.bb = fwd_sim.bb(included, :);
    fwd_sim.cent = fwd_sim.cent(included);
    fwd_sim.intensity = fwd_sim.intensity(included);
    fwd_sim.avg_pixel_int = fwd_sim.avg_pixel_int(included);
    fwd_sim.likelihood = fwd_sim.likelihood(included);
    fwd_sim.cands = fwd_sim.cands(included);
    fwd_sim.uvw = fwd_sim.uvw(included, :);
    fwd_sim.spot_type = fwd_sim.spot_type(included);

    % And a new field:
    fwd_sim.difspotID = fwd_sim.spotid(included);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Likelihood computation

function ls = compute_uv_partial_likelihoods(blob_uv, th_uv, proj_bls)
    proj_bbox(:, [1 3]) = cat(1, proj_bls(:).bbuim);
    proj_bbox(:, [2 4]) = cat(1, proj_bls(:).bbvim);

    blob_expected_sizes = proj_bbox(:, [3 4]) - proj_bbox(:, [1 2]) + 1;

    ls = (th_uv - blob_uv) ./ max(blob_expected_sizes ./ 10, 10);
    ls = exp(- (ls .^ 2) );
    ls = ls(:, 1) .* ls(:, 2);
end

function ls = compute_w_partial_likelihoods(blob_w, th_w)
    ls = 2 .* (th_w - blob_w);
    ls = exp(- (ls .^ 2));
end

function ls = compute_proj_bbs_partial_likelihoods(bls, proj_bls)
    bls_bboxes(:, [1, 3]) = cat(1, bls(:).bbuim);
    bls_bboxes(:, [2, 4]) = cat(1, bls(:).bbvim);

    proj_bls_bboxes(:, [1, 3]) = cat(1, proj_bls(:).bbuim);
    proj_bls_bboxes(:, [2, 4]) = cat(1, proj_bls(:).bbvim);

    cont_bbxes = [ ...
        min(bls_bboxes(:, 1:2), proj_bls_bboxes(:, 1:2)), ...
        max(bls_bboxes(:, 3:4), proj_bls_bboxes(:, 3:4)) ...
        ];

    cont_bbxes = [cont_bbxes(:, 1:2), (cont_bbxes(:, 3:4) - cont_bbxes(:, 1:2) + 1)];

    bls_shifts = bls_bboxes(:, 1:2) - cont_bbxes(:, 1:2);
    bls_shifts(:, 3) = 0;
    proj_bls_shifts = proj_bls_bboxes(:, 1:2) - cont_bbxes(:, 1:2);
    proj_bls_shifts(:, 3) = 0;

    num_bls = numel(proj_bls);
    mismatch_more = zeros(num_bls, 1);
    mismatch_less = zeros(num_bls, 1);
    proj_count = zeros(num_bls, 1);

    for ii = 1:num_bls
        cont_size = cont_bbxes(ii, 3:4);

        proj_count(ii) = sum(proj_bls(ii).mask(:));

        bl_mask = bls(ii).mask;
        bl_mask = sum(bl_mask, 3);
        bl_mask = bl_mask > 0;

        proj_bl_mask = logical(proj_bls(ii).mask);

        bl_mask = gtPlaceSubVolume( ...
            zeros(cont_size, 'uint8'), bl_mask, bls_shifts(ii, :));
        proj_bl_mask = gtPlaceSubVolume( ...
            zeros(cont_size, 'uint8'), proj_bl_mask, proj_bls_shifts(ii, :));

        overlap = bl_mask & proj_bl_mask;

        bl_mask(overlap) = 0;
        proj_bl_mask(overlap) = 0;

        mismatch_more(ii) = sum(bl_mask(:));
        mismatch_less(ii) = sum(proj_bl_mask(:));
    end

    % 3/4 makes up for the recent change to the use of a bigger estimate of
    % the grain volume
    ls = (1 .* mismatch_more + 3 / 4 .* mismatch_less) ./ proj_count;

    ls = exp(- (ls .^ 2));
end

function raw_bl = fixSize(raw_bl, parameters)
    vol = zeros(raw_bl.bbsize);
    shift = [0, 0 , 0];
    if (raw_bl.bbuim(1) < 1);
        shift(1) = -raw_bl.bbuim(1) + 1;
    end
    if (raw_bl.bbvim(1) < 1);
        shift(2) = -raw_bl.bbvim(1) + 1;
    end
    if (raw_bl.bbwim(1) < 1);
        shift(3) = -raw_bl.bbwim(1) + 1;
    end
    raw_bl.intm = gtPlaceSubVolume(vol, raw_bl.intm, shift);
    raw_bl.mask = gtPlaceSubVolume(vol, raw_bl.mask, shift);
end
