function gtSetupForwardSimulation(phaseID, varargin)
% GTSETUPFORWARDSIMULATION  Simple helper function to run gtForwardSimulate_v2
%     gtSetupForwardSimulation()
%     --------------------------

conf = struct( ...
    'recompile_function', [], ...
    'reset_sample', true, ...
    'reset_spot2grain', [], ...
    'reset_difspot_table', [], ...
    'reset_grains', [], ...
    'list', []);
[conf, rej_conf_pars] = parse_pv_pairs(conf, varargin);
parameters = gtLoadParameters();

[parameters.fsim, rej_pars] = parse_pv_pairs(parameters.fsim, rej_conf_pars);
conf.mode = parameters.fsim.mode

% Let's check every field is in, and in case let's update it!
parameters = gtCheckParameters(parameters, 'fsim', 'verbose', true);
parameters = gtCheckParameters(parameters, 'recgeo', 'verbose', true);
if (~isfield(parameters, 'detgeo') || isempty(parameters.detgeo))
    parameters.detgeo = gtGeoConvertLegacyLabgeo2Detgeo(parameters.labgeo);
end
if (~isfield(parameters.labgeo, 'omstep') || isempty(parameters.labgeo.omstep))
    parameters.labgeo.omstep = gtAcqGetOmegaStep(parameters);
end

% Updating recgeo with a consistent set of parameters
% parameters.recgeo = gtGeoRecDefaultParameters(parameters.detgeo(1), parameters.samgeo, parameters.acq(1));
% parameters.recgeo = gtGeoRecDefaultParameters(parameters.detgeo, parameters.samgeo);
gtSaveParameters(parameters);

gtDBConnect();

if (isempty(conf.recompile_function))
    check = inputwdefault('Do you want to recompile the function ''gtForwardSimulate_v2''? [y/n]', 'n');
    conf.recompile_function = strcmpi(check, 'y');
end
if (conf.recompile_function)
    gtExternalCompileFunctions('gtForwardSimulate_v2');
end

nof_phases = length(parameters.cryst);

if (nof_phases > 1)
    phaseID = inputwdefaultnumeric('Enter phaseID', num2str(1));
else
    phaseID = 1;
end

phaseID_str = sprintf('%02d', phaseID);

disp(['Setting up Forward Simulation for phase ' phaseID_str ' ' parameters.cryst(phaseID).name]);

% get the latest version of indexter output
ngrains = zeros(nof_phases, 1);
for ii = 1:nof_phases
    grain = [];
    index_filename = fullfile(parameters.acq(1).dir, '4_grains', sprintf('phase_%02d', ii), 'index.mat');
    load(index_filename, 'grain');
    % how many grains in this dataset?
    ngrains(ii) = length(grain);
end


if (isempty(conf.reset_difspot_table))
    check = inputwdefault('Do you want to reset grainID/flag/phaseID in the difspot table? [y/n]', 'y');
    conf.reset_difspot_table = strcmpi(check, 'y');
end
if (conf.reset_difspot_table)
    [columns, ~, ~, ~, ~, ~] = mym(['SHOW COLUMNS FROM ' parameters.acq(1).name 'difspot']);
    if ismember('grainID', columns)
        mym(['UPDATE ' parameters.acq(1).name 'difspot SET grainID = 0']);
        disp(['Variables grainID has been resetted in table ' parameters.acq(1).name 'difspot'])
    end
    if ismember('phaseID', columns)
        mym(['UPDATE ' parameters.acq(1).name 'difspot SET phaseID = 0']);
        disp(['Variable phaseID has been resetted in table ' parameters.acq(1).name 'difspot'])
    end
    if ismember('flag', columns)
        mym(['UPDATE ' parameters.acq(1).name 'difspot SET flag = 0']);
        disp(['Variable flag has been resetted in table ' parameters.acq(1).name 'difspot'])
    end
else
    fprintf('Skipping reset of grainID/flag/phaseID in the difspot table\n');
end

% reset grain_####.mat
if (isempty(conf.reset_grains))
    check = inputwdefault(sprintf('Do you want to delete the previous grain_####.mat files for phase %d? [y/n]', phaseID), 'y');
    reset_grains = strcmpi(check, 'y');
else
    reset_grains = conf.reset_grains;
end

if (reset_grains)
    delete( fullfile(parameters.acq(1).dir, '4_grains', sprintf('phase_%02d', phaseID), 'grain_*.mat') );
else
    fprintf('Skipping deletion of grain_####.mat files for phase %d. You might have problems in the future.\n', ...
        phaseID);
end

% create / reset sample.mat
if (conf.reset_sample)
    sample_filename = fullfile(parameters.acq(1).dir, '4_grains', 'sample.mat');
    if ( exist(sample_filename, 'file') && nof_phases > 1 )
        % do not create from scratch - we want to keep information related to other phases
        sample = GtSample.loadFromFile(sample_filename);
        sample.phases{phaseID} = GtPhase(parameters.cryst(phaseID).name, ngrains(phaseID));
    else
        sample = GtSample(nof_phases);
        sample.fileTable = [parameters.acq(1).name '_filetable'];
        for ii = 1:nof_phases
            sample.phases{ii} = GtPhase(parameters.cryst(ii).name, ngrains(ii));
        end
    end
    sample.saveToFile(sample_filename);

    fprintf('Reset phase number %02d in the sample.mat:\n  Phase name : %s\n  Nof_grains : %d\n', ...
        phaseID, parameters.cryst(phaseID).name, ngrains(phaseID));
else
    fprintf('Skipping reset of phase number %02d in the sample.mat\n', phaseID)
end

% create / reset spot2grain.mat
filename = fullfile(parameters.acq(1).dir, '4_grains', 'spot2grain.mat');
difspot_num  = mym(['select count(*) from ' parameters.acq(1).name 'difspot']);

if ( exist(filename, 'file') && nof_phases > 1 )
    if (isempty(conf.reset_spot2grain))
        check = inputwdefault('Do you want to reset spot2grain? [y/n]', 'n');
        conf.reset_spot2grain = strcmpi(check, 'y');
    end
    if (conf.reset_spot2grain)
        % cell array linking difspots with grainids
        spot2grain  = cell(difspot_num, 1); %#ok<PREALL>
        % cell array linking difspots with phases
        spot2phase  = cell(difspot_num, 1); %#ok<PREALL>
        save(filename, 'spot2grain', 'spot2phase');
    else
        fprintf('Skipping reset of: %s\n', filename)
    end
else
    % cell array linking difspots with grainids
    spot2grain  = cell(difspot_num, 1); %#ok<NASGU>
    % cell array linking difspots with phases
    spot2phase  = cell(difspot_num, 1); %#ok<NASGU>
    save(filename, 'spot2grain', 'spot2phase');
end

% get reconstruction parameters
check = inputwdefault('Use existing parameters for Forward Simulation? [y/n]', 'y');
if (~strcmpi(check, 'y'))
    list = build_list_v2();
    parameters.fsim = gtModifyStructure(parameters.fsim, list.fsim, 1, 'Forward simulation parameters:');
    save(fullfile(parameters.acq(1).dir, 'parameters.mat'), 'parameters');
end

% If list is provided, the grain_ids are distributed over the cluster
if isempty(conf.list)
    conf.list = 1 : ngrains(phaseID)
end


% launch forward simulation process to OAR - split to two jobs
check = inputwdefault([ ...
    'Launch Forward Simulation with OAR (o), here on the command line ' ...
    '(c) or quit the forward simulation (q)? [o/c/q]'], 'o');
switch(lower(check))
    case 'o'
        disp('Launching several separate OAR jobs');

        oar.njobs    = min(ceil(ngrains(phaseID)/20), 30); % max 30 jobs
        oar.walltime = ngrains(phaseID) / oar.njobs * 5 * 60;  % allow 5 minutes per grain % in seconds
        oar.mem      = 2000; %Mb

        % oar parameters are defined in build_list_v2
        list = build_list_v2();
        OAR_parameters = gtModifyStructure(oar, list.oar, 1, 'OAR parameters:');

        OARtmp = ceil(OAR_parameters.walltime / 60); % minutes
        OARm   = mod(OARtmp, 60); % minutes
        OARh   = (OARtmp - OARm) / 60; % hours

        OAR_parameters.walltime = sprintf('%02d:%02d:00', OARh, OARm);
        OAR_parameters.otherparameters = sprintf('mode %s det_index %d', conf.mode, 1);
        disp('Using OAR parameters:')
        disp(OAR_parameters)
        disp(['Using ' num2str(OAR_parameters.njobs) ' OAR jobs']);

        pause(0.5);

        if (isfield(parameters.labgeo, 'sourcepoint') ...
                && ~isempty(parameters.labgeo.sourcepoint))
            gtOarLaunch('gtForwardSimulatePoly', 1, ngrains(phaseID), OAR_parameters.njobs, ...
                [parameters.acq(1).dir ' ' phaseID_str], true, ...
                'walltime', OAR_parameters.walltime, 'mem', OAR_parameters.mem);
        else
            gtOarLaunch('gtForwardSimulate_v2', 1, ngrains(phaseID), OAR_parameters.njobs, ...
                sprintf('%s %s %s ', parameters.acq(1).dir, phaseID_str, OAR_parameters.otherparameters), true, ...
                'walltime', OAR_parameters.walltime, 'mem', OAR_parameters.mem, 'distribute', true, 'list', conf.list);
        end
    case 'c'
        disp('Launching forward simulation...')
        disp(' ')

        if (isfield(parameters.labgeo, 'sourcepoint') && ~isempty(parameters.labgeo.sourcepoint))
            gtForwardSimulatePoly(1, ngrains(phaseID), parameters.acq(1).dir, phaseID);
        else
            gtForwardSimulate_v2(1, ngrains(phaseID), parameters.acq(1).dir, phaseID, 'display_figure', false, 'list', conf.list);
        end
    otherwise
        disp('Quitting forward simulation...')
        return
end

end

