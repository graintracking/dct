function grainOut = gtCheckGrainConflicts(grain, phaseid, parameters, db_info, saveFlag, debug)
% GTCHECKGRAINCONFLICTS
%     grain = gtCheckGrainConflicts(grain, phaseid, [parameters], [db_info], [saveFlag], [debug])
%     --------------------------------------------------------------------------------------
%     To be run in the analysis directory!
%     Adds 'hasconf' and 'conf_table' fields;
%     Updates 'included' and 'selected' fields;
%
%     INPUT:
%       grain      = <struct/double>  grain/grainid
%       phaseid    = <double>         phaseid
%       parameters = <struct>         parameters
%       db_info    = <struct>         DB difspot table info
%       saveFlag   = <logical>        update grain_####.mat file {false}
%       debug      = <logical>        print comments {false}
%
%     OUTPUT:
%       grainOut   = <struct>         updated grain structure

if (~exist('saveFlag', 'var') || isempty(saveFlag))
    saveFlag = false;
end
if (~exist('debug', 'var') || isempty(debug))
    debug = false;
end
output = GtConditionalOutput(debug);

grainDirsModel = fullfile('4_grains', 'phase_%02d', 'grain_%04d.mat');
if isnumeric(grain)
    grain = load(sprintf(grainDirsModel, phaseid, grain));
    output.odisp(['Loaded grain from file ' grainDirsModel])
end
if (~exist('parameters', 'var') || isempty(parameters))
    parameters = gtLoadParameters();
end

fsim = parameters.fsim;
omstep = gtAcqGetOmegaStep(parameters);

if (isfield(grain.proj, 'ondet'))
    ondet = grain.proj.ondet;
else
    ondet = grain.ondet;
end
hkl = grain.allblobs.hkl(ondet);

if size(hkl, 1) ~= size(grain.flag, 1)
    grain = gtFSimCreateGrainTable(grain, ...
      {'spotid', 'hklsp', 'uvw', 'flag', 'thetatype', 'hkl'}, ...
      1, false, debug);
end

grains_conflicts = [];
if (~exist(fullfile('4_grains', 'grains_conflicts.mat'), 'file'))
    disp('Conflicts file is not existing...Quitting')
    grainOut = grain;
    return
end
load(fullfile('4_grains', 'grains_conflicts.mat'));

idspots = grains_conflicts{phaseid}.getIdSpot(grain.id);
if ~isempty(idspots)
    % We load things from DB only if we need it!
    if (~exist('db_info', 'var') || isempty(db_info))
        db_info = gtDBLoadTable([parameters.acq.name 'difspot'], 'difspotID');
    end

    % get conflict difspotID and index in list (grain.selected)
    ids     = idspots(1, :);
    indexes = idspots(2, :);

    spots_info = sfGetRowIndexes(db_info, ids);
    x = spots_info.CentroidX;
    y = spots_info.CentroidY;
    im = spots_info.CentroidImage;

    flag = grain.flag;
    % recalculate included and selected difspots
    included = find(flag >= 3);
    % 4 doesn't actually mean anything any more, but I just don't want
    % matlab to complain if "fsim.selected" doesn't exist
    selected = flag(included) >= 4;

    % updating included and selected fields
    % it should be that conflicts are checked over all the spot list, also
    % the segmented and matched ones
    flag(included(indexes)) = 6;
    grain.included = included;
    grain.selected = selected;
    grain.hasconf  = flag == 6;

    idconflict = grains_conflicts{phaseid}.getIdConflict(grain.id);
    spot2grain = grains_conflicts{phaseid}.getGrain(grain.id);
    output.fprintf('Found %d spots in common with other %d grain(s):\n', size(idspots, 2), size(idconflict, 2))
    for ii=1:length(idconflict)
        output.fprintf('grainID: %d n.conflict: %d\n', ...
            idconflict(ii), length(grains_conflicts{phaseid}.getDifspotsConflicts(grain.id, idconflict(ii))) )
    end

    % prepare data for conflict table
    [hasAll, ind] = ismember(ids, spot2grain(2, :));
    if ~all(hasAll) ...
            || ~all((grain.uv_exp(flag(included) == 6, 1) - x) < 1e-5) ...
            || ~all((grain.uv_exp(flag(included) == 6, 2) - y) < 1e-5) ...
            || ~all((grain.om_exp(flag(included) == 6) - im*omstep ) < 1e-5)
        grain.conf_table = [];
        output.odisp('Problem with grain conflicts...Not visualizing them')
        grainOut = grain;
        return
    else
        spot2grain = spot2grain(:, ind);
    end

    thetatype = grain.allblobs.thetatype(ondet);
    hkl = grain.allblobs.hkl(ondet, :);

    % hkl, hklsp, thetatype are filled now for ONDET spots, the same for hkl
    % uv_exp om_exp dispotID are filled for INCLUDED spots only
    data = [spot2grain' hkl(flag == 6, :) thetatype(flag == 6) grain.uv_exp(flag(included) == 6, :) grain.om_exp(flag(included) == 6, :)];
    IDs = data(:, 2);
    data(:, 2) = data(:, 1);
    data(:, 1) = IDs;
    data = mat2cell(data, ones(1, length(ids)), [1 1 size(hkl(1, :), 2) 1 1 1 1]);
    % hkl to string
    data(:, 3) = cellfun(@(x) sprintf(' %2d', x), data(:, 3), 'UniformOutput', false);
    data(:, 5:7) = cellfun(@(x) sprintf('%4.1f', x), data(:, 5:7), 'UniformOutput', false);
    if (size(hkl(1, :), 2) == 3)
        tmp_col = 'hkl';
    elseif (size(hkl(1, :), 2) == 4)
        tmp_col = 'hk(i)l';
    end
    cnames = {'spotID', 'grainID', tmp_col, 'fam', 'expU', 'expV', 'expOm'};
    cwidths = {50 50 80 40 50 50 50};
    % saving table info
    grain.conf_table.data    = data;
    grain.conf_table.x       = x;
    grain.conf_table.y       = y;
    grain.conf_table.im      = im;
    grain.conf_table.ids     = ids;
    grain.conf_table.indexes = indexes;
    grain.conf_table.cnames  = cnames;
    grain.conf_table.rnames  = {}; % rnames;
    grain.conf_table.cwidths = cwidths;
    grain.conf_table.flag    = flag;
else
    output.fprintf('No difspots in conflict found for grain %d\n', grain.id)
    grain.conf_table = [];
end % end if ~isempty(idspots)

if (saveFlag)
    save(sprintf(grainDirsModel, phaseid, grain.id), '-struct', 'grain', '-append')
end

if nargout > 0
    grainOut = grain;
end

end

function db_info = sfGetRowIndexes(db_info, ids)
% sfGETROWINDEXES
%   db_info = sfGetRowIndexes(db_info, ids)
%
% db_info : structure with field content of same length
% ids     : rows indexes to get

    if ~isempty(ids)
        db_info = structfun(@(x) x(ids, :), db_info, 'UniformOutput', false);
    end
end
