function [difstack, shifts] = gtFwdSimBuildDifstackSpots(spotids, parameters, stackUSize, stackVSize, bb, det_index, info)
% BUILDDIFSTACKSPOTS
%   [difstack, shifts] = gtFwdSimBuildDifstackSpots(spotids, parameters, stackUSize, stackVSize, bb, det_index, info)
%
% spotids    : spotid from grain
% parameters : parameters.mat
% stackUSize :
% stackVSize :
% det_index  : detector index
% bb         : bounding boxes
% info
%
% difstack   :
% shifts     : shifts (h,v) for each spot to fit into the montage

    num_spots = numel(spotids);

    if (~exist('det_index', 'var') || isempty(det_index))
        det_index = 1;
    end
    if (~exist('parameters', 'var') || isempty(parameters))
        parameters = gtLoadParameters();
    end
    if (~exist('info', 'var') || isempty(info))
        difspotfile = fullfile(parameters.acq(det_index).dir, '2_difspot', '00000', 'difspot00001.edf');
        if (exist(difspotfile, 'file'))
            info = edf_info(difspotfile);
        else
            info = [];
        end
    end

    shifts = gtFwdSimGetStackShifts(stackUSize, stackVSize, bb, true);

    difstack = zeros(stackUSize, num_spots, stackVSize, 'single');

    for ii = 1:num_spots
        spot_id = spotids(ii);
        spot_bb = bb(ii, :);

        [spot, ~] = gtFwdSimBuildSpot(spot_id, spot_bb, parameters, det_index, info);

        % Essentially zero pads the spot to make it fit into ASTRA's
        % diffraction stack
        spot_pad = gtPlaceSubVolume( ...
            zeros(stackUSize, stackVSize, 'single'), spot', ...
            [shifts.u(ii), shifts.v(ii), 0]);

        difstack(:, ii, :) = spot_pad;
    end
end
