function [selected, goodness] = gtFwdSimSelectSpots(likelihood, intensity, avg_pixel_int, flag, parameters)
% Flag has to be filtered by the included vector
    fsim = parameters.fsim;
    if (~isfield(fsim, 'sel_lh_stdev') || isempty(fsim.sel_lh_stdev))
        fsim.sel_lh_stdev = 1;
    end
    if (~isfield(fsim, 'sel_int_stdev') || isempty(fsim.sel_int_stdev))
        fsim.sel_int_stdev = 1;
    end
    if (~isfield(fsim, 'sel_avint_stdev') || isempty(fsim.sel_avint_stdev))
        fsim.sel_avint_stdev = 1;
    end
    % Let's determine the indices of the selected spots in list of included
    % spots, for different categories.
    [ls_selected, goodness] = select_best_likelihoods(likelihood, fsim.sel_lh_stdev);
    int_selected = deselect_low_intensities(intensity, ls_selected, fsim.sel_int_stdev);
    avg_int_selected = deselect_low_intensities(avg_pixel_int, ls_selected, fsim.sel_avint_stdev);

    % We assume that INDEXTER is always right!
    % (we only filter them if they are low intensity)
    indx_selected = flag == 5;
    selected = (ls_selected | indx_selected) & (int_selected & avg_int_selected);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Selection decision taking functions

function [selected, avg_lh] = select_best_likelihoods(likelihood, std_factor, lower_thr)
    if (~exist('lower_thr', 'var'))
        lower_thr = 1 / std_factor;
    end

    avg_lh = mean(likelihood);

    std_dev_thr = avg_lh - std_factor * std(likelihood);
    avg_thr = avg_lh * lower_thr;

    selected = (likelihood > std_dev_thr) & (likelihood > avg_thr);
end

function selected = deselect_low_intensities(intensities, selected, lower_thr)
    if (~exist('selected', 'var') || isempty(selected))
        selected = 1:numel(intensities);
    end
    log_ints = log10(intensities);
    log_avg = mean(log_ints(selected));
    log_dev = std(log_ints(selected));

    if (~exist('lower_thr', 'var'))
        lower_thr = 1;
    end
    selected = log_ints >= (log_avg - log_dev*lower_thr);
end
