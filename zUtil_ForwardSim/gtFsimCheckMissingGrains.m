function missing = gtFsimCheckMissingGrains(phase_num)
% GTFSIMCHECKMISSINGGRAINS  Checks output from Forward Simulation
%     missing = gtFsimCheckMissingGrains(phase_num)
%     ---------------------------------------------
%     To be run in the analysis directory
%
%     INPUT:
%       phase_num = <double>     number of the phase
%
%     OUTPUT:
%       missing   = <struct>     structure with:
%              .ind           = <double>     grainid of missing grain from fsim
%              .is_grain      = <logical>    for each grain true if grain_####.mat
%                                            exists
%              .old           = <double>     grainid of old grains
%              .is_uptodate   = <logical>    true if date(grain_####.mat) >= date(last job
%                                            submitted to OAR)
%              .checkFsim     = <double>     grains to check because id in grain_####.mat
%                                            do not correspond
%              .checkIndexter = <double>     grains to check because id in index.mat:grain
%                                            do not correspond
%
%     Version 002 08-10-2012 by LNervo
%       Check variable 'id' in each grain to correspond to grain_####.mat number


% get last gtForwardSimulate_v2 run date from oar.log file
fid = fopen('oar.log');
output = textscan(fid,'%s %s %d %s %s');
fclose(fid);
fcnames = output{2};
arrays  = output{3};
dates   = output{4};
ind_fsim = cellfun(@(num) strcmpi(num, 'gtForwardSimulate_v2'), fcnames);
dates_fsim = dates(ind_fsim);

if ~isempty(dates_fsim)
    datemax = max(datenum(dates_fsim(:)));


    grain = [];
    load(fullfile( '4_grains',['phase_' sprintf('%02d',phase_num)],'index.mat' ),'grain');
    % total number of grains from INDEXTER
    nof_grains = length(grain);


    % check if grain_####.mat is up to date (only those existing)
    is_uptodate = false(1, length(grain));
    list_grains = dir(fullfile( '4_grains',['phase_' sprintf('%02d',phase_num)],'grain_details_*.mat'));
    for ii = 1 : length(list_grains)
        if datenum(list_grains(ii).date,'dd-mmm-yyyy') >= datemax
            is_uptodate(ii) = true;
        else
            is_uptodate(ii) = false;
        end
    end

    % total number of grains from fsim
    nof_grains_fsim = length(list_grains);
    % check if grain_####.mat exists for all the INDEXED grains
    is_grain    = false(1, length(grain));
    for ii = 1: nof_grains
        if exist(fullfile( '4_grains',['phase_' sprintf('%02d',phase_num)],sprintf('grain_%04d.mat',ii)),'file')
            is_grain(ii) = true;
        else
            disp(['grain # ' num2str(ii) ' does not exist.'])
            is_grain(ii) = false;
        end
    end

    missing.ind         = find(is_grain==false);
    missing.is_grain    = is_grain;
    missing.old         = find(is_uptodate==false);
    missing.is_uptodate = is_uptodate;

    if nof_grains ~= nof_grains_fsim
        disp('Check number of grains from forward simulation: it differs from the total number of grains from INDEXTER')
        disp(['  Indexter : ' num2str(nof_grains)])
        disp(['  Forward Simulation : ' num2str(nof_grains_fsim)])
        disp(['  There should be ' num2str(nof_grains_fsim - nof_grains) ' grains to be removed. Run the following commands:'])
        for ii = missing.old(1) : missing.old(end)
            if ii > nof_grains && ii <= nof_grains_fsim
                disp(['   delete( ''' fullfile( '4_grains',['phase_' sprintf('%02d',phase_num)],sprintf('grain_%04d.mat',ii)) ''' )'])
            end
        end
    end

    % check id in grain_####.mat
    checkId_fsim = zeros(1,0);
    for kk = 1 : nof_grains_fsim
        load(fullfile( '4_grains',['phase_' sprintf('%02d',phase_num)],sprintf('grain_%04d.mat',kk)),'id');
        check = [kk id];
        if kk ~= id
            checkId_fsim{end+1} = check;
        end
    end

    % check id in index.mat:grain
    checkId_indexter = zeros(1,0);
    for ii=1:nof_grains
        check = [ii grain{ii}.id];
        if ii ~= grain{ii}.id
            checkId_indexter{end+1} = check;
        end
    end

    missing.checkFsim     = cell2mat(checkId_fsim');
    missing.checkIndexter = cell2mat(checkId_indexter');

else
    disp('Forward simulation has not been run yet!')
    missing.ind = [];
    missing.old = [];
    missing.checkFsim = [];
    missing.checkIndexter = [];
end

end % end of function
