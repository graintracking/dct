function shifts = gtFwdSimGetStackShifts(stackUSize, stackVSize, bb, use_spots)
    if (~exist('use_spots', 'var') || use_spots)
        shifts.u = round((stackUSize - bb(:, 3)) / 2);
        shifts.v = round((stackVSize - bb(:, 4)) / 2);
    else
        shifts.u = round((stackUSize - bb(:, 4)) / 2);
        shifts.v = round((stackVSize - bb(:, 5)) / 2);
    end
end
