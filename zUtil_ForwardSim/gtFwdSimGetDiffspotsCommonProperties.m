function spotsProps = gtFwdSimGetDiffspotsCommonProperties(grain, parameters, det_index)
% GETDIFFRACTIONSPOTSCOMMONPROPERTIES
%   spotsProps = gtFwdSimGetDiffspotsCommonProperties(grain, parameters, det_ind)
%
% grain         : grain structure
% detrefpos_pix : detector position in pixels
% parameters    : DCT parameters

    if (~exist('det_index', 'var') || isempty(det_index))
        det_index = 1;
    end

    fsim = parameters.fsim;
    omstep = gtAcqGetOmegaStep(parameters);
    detgeo = parameters.detgeo(det_index);
    labgeo = parameters.labgeo;
    recgeo = parameters.recgeo(det_index);

    % detector reference position in pixels
    detrefpos_pix = gtGeoLab2Sam(detgeo.detrefpos, eye(3), labgeo, recgeo, true);

    %%% Get the average difspot X and Y sizes for the current spots in the grain,
    spotsProps = [];
    % if grain is not the correct one, load it from index results
    if (isempty(grain) || ~isfield(grain, 'stat'))
        idx_grain_model = fullfile('4_grains', 'phase_%02d', 'index.mat');
        idx_grains = gtMATVolReader([sprintf(idx_grain_model, grain.phaseid), ':grain']);
        grain = idx_grains{grain.id};
    end
    spotsProps.Xsize = grain.stat.bbxsmean; % average X Size of difspots
    spotsProps.Ysize = grain.stat.bbysmean; % average Y Size of difspots

    % define theshold values which will be used during the database search later
    segMinX = parameters.seg.minblobsize(1);
    segMinY = parameters.seg.minblobsize(2);
    spotsProps.XsizeMin = max(segMinX, spotsProps.Xsize - fsim.bbsize_factor * grain.stat.bbxsstd);
    spotsProps.XsizeMax = spotsProps.Xsize + fsim.bbsize_factor * grain.stat.bbxsstd;
    spotsProps.YsizeMin = max(segMinY, spotsProps.Ysize - fsim.bbsize_factor * grain.stat.bbysstd);
    spotsProps.YsizeMax = spotsProps.Ysize + fsim.bbsize_factor * grain.stat.bbysstd;

    % allow for a lateral deviation of spot postions based on grain orientation statics
    % dif_props.Offset = fsim.Rdist_factor * gr.stat.Rdiststd * norm(detrefpos_pix)
    % !!! detrefpos is not necessarily the distance; depends on definition by user
    spotsProps.Offset = fsim.Rdist_factor * deg2rad(grain.stat.dangstd) * norm(detrefpos_pix)

    grainBBox = [max(grain.bbxs), max(grain.bbys)];
    % take the biggest spot and add a relative extra margin (multiplicative factor oversize)
    spotsProps.stackUSize = round(grainBBox(1) * fsim.oversize);
    % ...same for vertical direction
    spotsProps.stackVSize = round(grainBBox(2) * fsim.oversize);

    % Maximum offset (in number of images) from the predicted omega position
    spotsProps.max_w_offset_im = fsim.MaxOmegaOffset / omstep;
end
