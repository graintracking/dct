function proj_bls = gtFwdSimPredictProjectedGrainBBox(gr, sel_projvecs, sel_bb, sel_omegas, ondet, parameters, det_ind)
    if (~exist('det_ind', 'var') || isempty(det_ind))
        det_ind = 1;
    end

    verts_rec = gtFwdSimComputeCircumscribingPolyhedron(gr.center, sel_projvecs, sel_omegas, sel_bb, parameters, det_ind, 'convhull');

    proj_bls = gtFwdSimProjectVertices2Det(gr, verts_rec, ondet, parameters, det_ind);
end