function output = gtUpdateSpot2Grain(grains_list, difspots_list, spot2grain, spot2phase)
% output = gtUpdateSpot2Grain(grains_list, difspots_list, spot2grain, spot2phase)

difspots_list = unique(difspots_list);
count = zeros(1,length(difspots_list));
disp(' ')
% update spot2grain and spot2phase
for jj = 1 : length(difspots_list)
    fprintf('  Removed conflict in spot2grain.mat -> difspotID: %6d  grainIDs: ', difspots_list(jj))
    for ii = 1 : length(grains_list)
        index = find(spot2grain{difspots_list(jj)}==grains_list(ii));
        if ~isempty(index)
            count(jj) = count(jj) + 1;

            spot2grain{difspots_list(jj)}(index)=[];
            spot2phase{difspots_list(jj)}(index)=[];
            fprintf('%4d ',grains_list(ii))
        end
    end
    fprintf('\n')
end
fprintf('  -> Found %d difspots in conflict with %d grains.\n', length(difspots_list), length(grains_list))
output.spot2grain = spot2grain;
output.spot2phase = spot2phase;
output.isNotRight = difspots_list(count<2);
fprintf('  -> Found %d spots in conflicts not paired (not claimed by two grains!)\n',length(output.isNotRight))

end % end of gtUpdateSpot2Grain
