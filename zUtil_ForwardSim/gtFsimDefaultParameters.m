function par_fsim = gtFsimDefaultParameters()
    % check for intensity in the FULL images in case no difspot can be found in database
    par_fsim.check_spot      = false;
    % look in  +/- n degrees from predicted image position
    par_fsim.omegarange      = 0.5;
    % Maximum omega offset
    par_fsim.MaxOmegaOffset  = 1;
    % default dimensions of ROI, read from the FULL images - if empty the zeropadded projection size will be used
    par_fsim.bb_size         = [];
    % assemble the results of forward simulation into a full image saved in the grain%d_.mat file
    par_fsim.assemble_figure = true;
    % Display gtShowFsim; automatically disabled when launched on OAR
    par_fsim.display_figure  = true;
    % default colorlimits when visualizing diffraction spots
    par_fsim.clims           = [-50 800];
    % allow for multiple times Rdiststd in calulcation of lateral spot offset
    par_fsim.Rdist_factor    = 3;
    % allow for +/- times std difference in horizontal and vertical bb size
    par_fsim.bbsize_factor   = 2;
    % projections are zeropadded to twice their size before reconstruction
    par_fsim.oversize        = 1.6;
    % reconstruction volume is zeropadded by (1.2)^3
    par_fsim.oversizeVol     = 1.2;
    % use theoretically predicted diffraction angles for new detected spots
    par_fsim.use_th          = true;
    % Display comments on screen
    par_fsim.verbose         = false;
    % flag to save grain_####.mat files (and the related entry in sample.mat)
    par_fsim.save_grain      = true;
    % threshold for looking for new spots in full images
    par_fsim.thr_check       = 0.1;
    % Selects spots for reconsruction that are in the top 4 sigma of the likelihood rankings
    par_fsim.sel_lh_stdev    = 1;
    % Selects spots for reconsruction that are in the top 4 sigma of the intensity rankings
    par_fsim.sel_int_stdev   = 1;
    % Selects spots for reconsruction that are in the top 4 sigma of the average intensity per pixel rankings
    par_fsim.sel_avint_stdev = 1;
    % Use output from Indexter (indexter) or from FED global fit
    % (global_fit)
    par_fsim.mode            = 'indexter';
end
