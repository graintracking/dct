function gtFsimUpdate(grain, varargin)
% gtFsimUpdate(grain, varargin)
%
% save grain info into fsim grain_####.mat files
% only some variable is possible

disp(['Reading and updating info from ' num2str(length(grain)) ' grains into 4_grains/phase_##/ folder'])

for ii = 1:length(grain)
    tmpvar = grain{ii};
    filename = fullfile('4_grains',sprintf('phase_%02d',grain{ii}.phaseid),sprintf('grain_%04d.mat', grain{ii}.id));
    if exist(filename, 'file')
        if isempty(varargin)
            save(filename, '-struct', 'tmpvar')
        else
            save(filename, '-struct', 'tmpvar', varargin{:})
        end
    end
end
disp('Updated 4_grains/phase_##/grain_####.mat files from the given grain structure')

end % end of function
