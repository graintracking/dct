 function proj_bls = gtFwdSimProjectVertices2Det(gr, verts_rec, ondet, parameters, det_ind, verbose)
% FUNCTION proj_bls = gtFwdSimProjectVertices2Det(gr, verts_rec, ondet, parameters, det_ind, verbose)
%

    if (~exist('verbose', 'var') || isempty(verbose))
       verbose = false;
    end

    detgeo = parameters.detgeo(det_ind);
    labgeo = parameters.labgeo;
    samgeo = parameters.samgeo;
    recgeo = parameters.recgeo(det_ind);

    omstep = gtAcqGetOmegaStep(parameters, det_ind);

    % Now we project the vertices to the spots and take the convex hull
    dvecs_lab = gr.allblobs(det_ind).dvec(ondet, :);
    omegas = gr.allblobs(det_ind).omega(ondet);
    if (verbose)
        uv = gr.allblobs(det_ind).detector.uvw(ondet, 1:2);
    end

    num_omegas = numel(omegas);
    proj_bls = gtFwdSimBlobDefinition('blob', num_omegas);

    % All functions that use the same omegas, need the same rotation tensors:
    % Notice the sign of W !! This is going to be used for a Sam -> Lab
    rot_l2s = gr.allblobs(det_ind).srot(:, :, ondet);
    rot_s2l = permute(rot_l2s, [2 1 3]);

    ones_verts = ones(size(verts_rec, 1), 1);
    gc_sam = gr.center(ones_verts, :);

    verts_sam = gtGeoSam2Sam(verts_rec, recgeo, samgeo, false) + gc_sam;

    for ii = 1:num_omegas
        rot_s2l_w = rot_s2l(:, :, ii);
        verts_lab = gtGeoSam2Lab(verts_sam, rot_s2l_w, labgeo, samgeo, false);
        dvec_vers = dvecs_lab(ii .* ones_verts, :);

        verts_det_pos = gtFedPredictUVMultiple([], dvec_vers', verts_lab', ...
            detgeo.detrefpos', detgeo.detnorm', detgeo.Qdet, ...
            [detgeo.detrefu, detgeo.detrefv]')';

        if (verbose)
            f = figure();
            ax = axes('parent', f);
            hold(ax, 'on')
            scatter(ax, verts_det_pos(:, 1), verts_det_pos(:, 2));
            scatter(ax, uv(ii, 1), uv(ii, 2), 30, 'r');
            hold(ax, 'off')
        end

        verts_idxs = convhull(verts_det_pos);
        verts_det_pos = verts_det_pos(verts_idxs, :);

        bb_2d = [floor(min(verts_det_pos, [], 1)), ceil(max(verts_det_pos, [], 1))];
        bb_2d = [bb_2d(1:2), (bb_2d(3:4) - bb_2d(1:2) + 1)];

        mask = zeros(bb_2d(3:4));
        verts_mask_pos = round(verts_det_pos) - bb_2d(ones(numel(verts_idxs), 1), 1:2) + 1;
        verts_mask_indx = verts_mask_pos(:, 1) + bb_2d(3) * (verts_mask_pos(:, 2) - 1);
        mask(verts_mask_indx) = 1;
        mask = bwconvhull(mask);

        proj_bls(ii).bbuim = [bb_2d(1), (bb_2d(1) + bb_2d(3) - 1)];
        proj_bls(ii).bbvim = [bb_2d(2), (bb_2d(2) + bb_2d(4) - 1)];
        proj_bls(ii).bbwim = round([omegas(ii), omegas(ii)] / omstep);
        proj_bls(ii).bbsize = [bb_2d(3:4), 1];
        proj_bls(ii).mask = mask;

        if (verbose)
            f = figure();
            ax = axes('parent', f);
            imshow(mask, 'parent', ax)

            pause
        end
    end
end