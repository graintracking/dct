function [spot, m] = gtGetRawRoi(start_image, end_image, acq, bb, det_index)
% GETRAWROI Sums a roi in raw images
%   [spot, m] = gtGetRawRoi(start_image, end_image, acq, bb)

    if (~exist('det_index', 'var') || isempty(det_index))
        det_index = 1;
    end

    p = struct('acq', acq);
    tot_images = gtAcqTotNumberOfImages(p, det_index);

    nimages = end_image - start_image + 1;
    spot    = zeros(bb(4), bb(3), nimages, 'single');
    m       = zeros(nimages, 1);
    % What if we accepted spots smaller than 10 x 10 pixels?
    % Will break down for either bb(3), bb(4) =< 10 
    % Probably not very useful spots though
    if (all(bb(3:4) > 10))
        centeru = round((bb(3)/2 -5) : (bb(3)/2 +5));
        centerv = round((bb(4)/2 -5) : (bb(4)/2 +5));
    else
        centeru = round((bb(3)/2 -2) : (bb(3)/2 +2));
        centerv = round((bb(4)/2 -2) : (bb(4)/2 +2));
    end

    detsizeu = acq(det_index).xdet;
    detsizev = acq(det_index).ydet;
    
    % Let's restrict the loading to "with-in" the limits
    if all([(bb(1) + bb(3)) >= 1, bb(1) <= detsizeu, (bb(2) + bb(4)) >= 1, bb(2) <= detsizev])
        spot_range_u = (max(bb(1), 1) - bb(1) + 1):(min(bb(1)+bb(3)-1, detsizeu) - bb(1) + 1);
        spot_range_v = (max(bb(2), 1) - bb(2) + 1):(min(bb(2)+bb(4)-1, detsizev) - bb(2) + 1);

        real_bb = [max(bb(1:2), 1), ...
            (min(bb(1:2) + bb(3:4) - 1, [detsizeu, detsizev]) - max(bb(1:2), 1) + 1)];

        fullImgsDir = fullfile(acq(det_index).dir, '1_preprocessing', 'full');
        indexes = start_image:end_image;
        indexes = mod(indexes, tot_images);

        info = edf_info( fullfile(fullImgsDir, sprintf('full%04d.edf', indexes(1))) );

        for img_n = 1:nimages
            fullImgName = fullfile(fullImgsDir, sprintf('full%04d.edf', indexes(img_n)));
            slice = edf_read(fullImgName, real_bb, [], info, 'convert', false);
            spot(spot_range_v, spot_range_u, img_n) = single(slice);
            m(img_n) = gtImgMeanValue(spot(centerv, centeru, img_n));
        end
    end
end
