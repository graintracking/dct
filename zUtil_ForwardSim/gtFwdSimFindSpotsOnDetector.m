function ondet = gtFwdSimFindSpotsOnDetector(spotsCommProps, uv, parameters, det_index)
% FINDSPOTSONDETECTOR
%   ondet = gtFwdSimFindSpotsOnDetector(spotsCommProps, uv, parameters, det_index)
%
% spotsCommProps : diffraction spots common properties
% uv             : horizontal and vertical coordinate of spot on detector
% parameters     : the dataset parameters, used to get:
%  - radius         : radius from center for spot on detector
%  - acq            : parameters.acq
%  - segbb          : parameters.seg.bbox

    if (~exist('det_index', 'var'))
        det_index = 1;
    end

    acq = parameters.acq(det_index);
    segbb = parameters.seg.bbox;
    detgeo = parameters.detgeo(det_index);

    % radial distances of spot center from detector center
    % !!! to be corrected: detrefu and detrefv can be anywhere, not
    % necessarily in the center
    radius = sqrt((uv(:, 1) - detgeo.detrefu).^2 + (uv(:, 2) - detgeo.detrefv).^2);

    %%% Get the indices of all spots which fully intersect the active region of the detector
    ondet = find(all([ ...
        uv(:, 1) > spotsCommProps.Xsize/2, uv(:, 1) < (acq.xdet - spotsCommProps.Xsize / 2), ...
        uv(:, 2) > spotsCommProps.Ysize/2, uv(:, 2) < (acq.ydet - spotsCommProps.Ysize / 2), ...
        radius < acq.maxradius], 2));

    % Exclude spots which overlap with segmentation boundingbox
    segOverlap = find(all([...
        (uv(:, 1) + spotsCommProps.Xsize/2 >= segbb(1)), ...
        (uv(:, 1) - spotsCommProps.Xsize/2 <= (segbb(1) + segbb(3) - 1)), ...
        (uv(:, 2) + spotsCommProps.Ysize/2 >= segbb(2)), ...
        (uv(:, 2) - spotsCommProps.Ysize/2 <= (segbb(2) + segbb(4) - 1)) ...
        ], 2));

    % indices of spots intersection active area of detector
    ondet = setdiff(ondet, segOverlap);
end
