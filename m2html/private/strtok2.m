function [token, remainder, quotient] = strtok2(string, delimiters)
% STRTOK2  Returns the first token in the string delimited by white-space 
%          characters. 
%
%    [token, remainder, quotient] = strtok2(string, delimiters)
%    -------------------------------------------------------------------------
%    Any leading white space characters are ignored.
%    Enhanced version of STRTOK to also return the quotient.
%
%    INPUT:
%      string     = <string>
%      delimiters = <string>  All the delimiter characters
%
%    OUTPUT:
%      token      = <string>  First token found
%      remainder  = <string>  Remainder of the original
%      quotient   = <string>  Quotient string

token = [];
remainder = [];
quotient = string;

if nargout == 1
    token = strtok(string, delimiters);
elseif nargout == 2
    [token, remainder] = strtok(string, delimiters);
elseif nargout >= 3
    iniLength = length(string);
    [token, remainder] = strtok(string, delimiters);
    qLength = length(string) - length(remainder) - length(token);
    quotient = string(1:qLength);
end

end % end of function
