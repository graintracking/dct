function xml_conf = gtConfLoadXML(verbose)
% FUNCTION xml_conf = gtConfLoadXML()
% Loads the conf.xml file
%

    if (~usejava('jvm'))
        error('gtConfLoadXML:environment_error', ...
            'Cannot load xml conf if java is not running')
    end

    if (~exist('verbose', 'var') || isempty(verbose))
        verbose = false;
    end

    global GT_MATLAB_HOME

    custom_xml_conf = fullfile(GT_MATLAB_HOME, 'conf.xml');
    if (exist(custom_xml_conf, 'file'))
        if (verbose)
            fprintf('Loading configuration from custom conf.xml (%s)\n', custom_xml_conf);
        end
        xml_conf = xml_read(custom_xml_conf);
    else
        default_xml_conf = fullfile('zUtil_Conf', 'conf.default.xml');
        if (verbose)
            fprintf('Loading configuration from default conf.xml (%s)\n', default_xml_conf);
        end
        xml_conf = xml_read(fullfile(GT_MATLAB_HOME, default_xml_conf));
    end
end
