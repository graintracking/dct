function data = gtConfGetField(xml_conf, fields)
% FUNCTION data = gtConfGetField(xml_conf, fields)
%

    field_chuncks = regexp(fields, '\.', 'split');
    data = xml_conf;
    for ii_f = 1:numel(field_chuncks)
        if (isfield(data, field_chuncks{ii_f}))
            data = data.(field_chuncks{ii_f});
        else
            error('gtConfGetField:wrong_argument', ...
                '"%s" of "%s" is not a valid field', ...
                field_chuncks{ii_f}, fields)
        end
    end
end
