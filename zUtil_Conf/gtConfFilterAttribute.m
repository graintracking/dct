function data_out = gtConfFilterAttribute(data_in, attributes, varargin)
% FUNCTION data_out = gtConfFilterAttribute(data_in, attributes, varargin)
%

    if (isfield(data_in, 'ATTRIBUTE'))
        if (~iscell(attributes))
            attributes = {attributes};
        end
        num_attribs = numel(attributes);
        valid = true(num_attribs, 1);

        for ii_a = 1:num_attribs
            attrib = attributes{ii_a};

            if (isfield(data_in.('ATTRIBUTE'), attrib))
                switch (attrib)
                    case 'hostname'
                        [~, hostname] = system('hostname');
                        hosts = data_in.('ATTRIBUTE').('hostname');
                        is_in_hosts = false;
                        for host = regexp(hosts, ',', 'split')
                            if (~isempty(regexp(hostname, host{1}, 'once')))
                                is_in_hosts = true;
                                break;
                            end
                        end
                        valid(ii_a) = is_in_hosts;
                    otherwise
                        warning('gtConfFilterAttribute:wrong_argument', ...
                            'Attribute "%s" not supported', attrib)
                end
            end
        end

        if (all(valid))
            if (isfield(data_in, 'CONTENT'))
                data_out = data_in.('CONTENT');
            else
                data_out = data_in;
            end
        else
            data_out = [];
        end
    else
        data_out = data_in;
    end
end
