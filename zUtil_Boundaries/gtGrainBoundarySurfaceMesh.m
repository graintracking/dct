
function fv=gtGrainBoundarySurfaceMesh(grains, smoothR, grainID)

%this seems to work reasonably quickly
% procedure:
% smooth grain volume, considering relevent voxels only (faster than smooth3)
% isosurface
% assign points on isosurface to particular boundaries
% returns the face-vertex structure,
% this can then be simplified using reduce patch for further analysis

%nb - this was gtAnalyseBoundaryIndexPlaneMesh.m
%this has been split into this file (generate mesh) and
% gtAnalyseGrainBoundarySurface.

tmp=grains==grainID;

%smooth grain volume before isosurface
%find relevent voxels
smoothD=(2*smoothR)+1;
disp('find target voxels for smoothing')
tic
todo=find(imdilate(tmp, ones(smoothD,smoothD,smoothD))-imerode(tmp, ones(smoothD,smoothD,smoothD)));
toc
tmp2=zeros(size(tmp));

%do smoothing
disp('do smoothing')
tic 
for k=1:length(todo)
    
    [y,x,z]=ind2sub(size(tmp), todo(k));

    x1=max(x-smoothR,1);
    y1=max(y-smoothR,1);
    z1=max(z-smoothR,1);
    x2=min(x+smoothR,size(tmp,2));
    y2=min(y+smoothR,size(tmp,1));
    z2=min(z+smoothR,size(tmp,3));
    
neighbourhood=tmp(y1:y2, x1:x2, z1:z2);
tmp2(y,x,z)=mean(neighbourhood(:));

if mod(k, round(length(todo)/10))==0
    disp(sprintf('%0.1f',k/length(todo)))
end

end
toc

%complete smoothed grain interior
tmp2(find(tmp2==0 & tmp))=1;

%isosurface
fv=isosurface(tmp2, 0.5);
%initial triangle area is about 1
approx_triangle_area=(length(todo)/smoothD)/length(fv.vertices)
%reduce mesh to give triangle area ~10














