function gtReadBoundaryProperties(phaseid, boundaries_structure, vol_out, r_vectors)
% gtReadBoundaryProperties(phaseid, boundaries_structure, vol_out, r_vectors)

% continue to add to boundaries_structure.mat - orientations,
% disorientation, r-vectors etc.
parameters = [];
load('parameters.mat');
latticepar = parameters.cryst(phaseid).latticepar;
crystal_system = parameters.cryst(phaseid).crystal_system;
% Get symmetry operators
symm = gtCrystGetSymmetryOperators(crystal_system);

% find the max grainID
maxgrain = max([boundaries_structure.grain1], [boundaries_structure.grain2]);

% loop through the boundaries
for ii = 1:size(boundaries_structure, 2)

    boundaries_structure(ii).grain1_R_vector      = [];
    boundaries_structure(ii).grain2_R_vector      = [];
    boundaries_structure(ii).misorientation_angle = [];
    boundaries_structure(ii).misorientation_axis  = [];
    boundaries_structure(ii).misorientation_axis_hex_int = [];
    boundaries_structure(ii).gb_norm              = [];
    boundaries_structure(ii).gb_centre            = [];
    boundaries_structure(ii).gb_norm_grain1       = [];
    boundaries_structure(ii).gb_norm_grain2       = [];

    if (boundaries_structure(ii).count < 4) %can still be useful to calc index planes where we have only one of the grains
        continue;
    else
        %Read in the r-vector data for the two grains
        grain1 = boundaries_structure(ii).grain1;
        grain2 = boundaries_structure(ii).grain2;

        if grain1 == 0
            g1 = [];
            g1equiv = [];
        elseif grain1 == -1
            continue;
        else
            R1 = r_vectors(grain1, 2:4);
            boundaries_structure(ii).grain1_R_vector = R1;
            g1 = gtMathsRod2OriMat(R1);
        end

        if grain2 == 0
            g2 = [];
        elseif grain2 == -1
            continue;
        else
            R2 = r_vectors(grain2, 2:4);
            boundaries_structure(ii).grain2_R_vector = R2;
            g2 = gtMathsRod2OriMat(R2); 
        end

        % calculate the misorientation angle and axis between these grains
        if ~isempty(g1) && ~isempty(g2)
            % If we have both grains

            [mis_angle, mis_axis, mis_info] = gtDisorientation(g1, g2, symm, ...
                'input', 'orimat', 'latticepar', latticepar);

            boundaries_structure(ii).misorientation_axis  = mis_axis;
            boundaries_structure(ii).misorientation_angle = mis_angle;
            if isfield(mis_info, 'mis_axis_hex')
                boundaries_structure(ii).misorientation_axis_hex = mis_info.mis_axis_hex;
                boundaries_structure(ii).misorientation_axis_hex_int = mis_info.mis_axis_hex_int;
            else
                [tmp, tmp_int] = gtCrystMiller2FourIndexes(boundaries_structure(ii).misorientation_axis, 'direction');
                boundaries_structure(ii).misorientation_axis_hex = tmp;
                boundaries_structure(ii).misorientation_axis_hex_int = tmp_int;
            end
            
        end

        % even if we have only one grain, can deal with the boundary plane
        test = (vol_out == ii);
        [x,y,z] = ind2sub(size(test), find(test));
        [origin, a] = lsplane([x y z]);
        gb_normal = a;
        %gb_normal(3)=-gb_normal(3);

        boundaries_structure(ii).gb_norm   = gb_normal';
        boundaries_structure(ii).gb_centre = origin';

        if ~isempty(g1equiv)
            plane1 = g1equiv \ gb_normal;
        elseif ~isempty(g1)
            plane1 = g1 * gb_normal;
        else
            plane1 = [];
        end

        if ~isempty(g2)
            plane2 = g2 * gb_normal;
        else
            plane2 = [];
        end

        boundaries_structure(ii).gb_norm_grain1 = plane1';
        boundaries_structure(ii).gb_norm_grain2 = plane2';

        switch crystal_system
          case 'cubic'
            % if cubic then this is the index plane

          case 'hexagonal'
            % if hexagonal, keep cartesian vector, but also
            % convert the cartesian plane normal to hexagonal four index notation
            % ditto for misorientation axis

            if ~isempty(plane1)
                boundaries_structure(ii).gb_norm_hex_grain1 = gtCrystMiller2FourIndexes(plane1', 'plane');
            end
            if ~isempty(plane2)
                boundaries_structure(ii).gb_norm_hex_grain2 = gtCrystMiller2FourIndexes(plane2', 'plane');
            end

          otherwise
            disp('crystal system not supported!');
        end

    end % skip small boundaries
end % end for ii

save(fullfile('8_analysis','boundaries_structure.mat'), 'boundaries_structure');

end % end of function
