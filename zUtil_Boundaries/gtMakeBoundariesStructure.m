

function boundaries_structure = gtMakeBoundariesStructure(boundary_map, grains)

% from the boundaries and grains volume, start to build the
% boundaries_structure.

nbounds = max(boundary_map(:));

% loop through each boundary
for i=1:nbounds
    
    list=find(boundary_map(:)==i);
   
    boundaries_structure(i).count=length(list);
    
    list=unique(grains(list));
    if length(list)==2
        boundaries_structure(i).grain1=list(1);
        boundaries_structure(i).grain2=list(2);
    elseif length(list)==1
        disp('one external boundary - one grain only')
        boundaries_structure(i).grain1=list(1);
        boundaries_structure(i).grain2=0;
    end
    
    if mod(i, 100)==0
        fprintf('done %d percent\n', round(100*i/nbounds))
    end
    
end

save boundaries_structure boundaries_structure