function grain_surface=gtAnalyseBoundaryIndexPlaneMesh(grains, bounds, bounds_dil, r_vectors, boundaries_structure, grainID, bounds_list2)

%this seems to work reasonably quickly
% procedure:
% smooth grain volume, considering relevent voxels only (faster than smooth3)
% isosurface
% assign points on isosurface to particular boundaries
% return grain_surface: this contains the face-vertex structure, together
% with another variable assigning vertices to specific grain boundaries
% this can then be simplified using reduce patch for further analysis


% this has been split into gtGrainBoundarySurfaceMesh and gtAnalyseGrainBoundarySurface
% and as such is redundant...

tmp=grains==grainID;
R = r_vectors(grainID, 2:4);
g = gtMathsRod2OriMat(R);
symm = gtCrystGetSymmetryOperators('cubic');
cmap=gtRandCmap(bounds);

%which boundaries are relevent?
bounds_list=[];
for ii = 1:length(boundaries_structure)
    if boundaries_structure(ii).grain1==grainID || boundaries_structure(ii).grain2==grainID
        bounds_list=[bounds_list ii];
    end
end


%find relevent voxels
jj=9
tic
todo=find(imdilate(tmp, ones(jj,jj,jj))-imerode(tmp, ones(jj,jj,jj)));
toc
tmp2=zeros(size(tmp));

%do smoothing
ii=4;
tic 
for k = 1:length(todo)
    
    [y,x,z]=ind2sub(size(tmp), todo(k));

    x1=max(x-ii,1);
    y1=max(y-ii,1);
    z1=max(z-ii,1);
    x2=min(x+ii,size(tmp,2));
    y2=min(y+ii,size(tmp,1));
    z2=min(z+ii,size(tmp,3));
    
neighbourhood=tmp(y1:y2, x1:x2, z1:z2);
tmp2(y,x,z)=mean(neighbourhood(:));

if mod(k, round(length(todo)/10))==0
    k/length(todo)
end

end
toc

%complete smoothed grain interior
tmp2(find(tmp2==0 & tmp))=1;

%isosurface
fv_orig=isosurface(tmp2, 0.5);
%initial triangle area is about 1
approx_triangle_area=(length(todo)/j)/length(fv_orig.vertices)
%reduce mesh to give triangle area ~10
fv=reducepatch(fv_orig,0.1);

%get those vertices that relate to a certain boundary?
% use a dilated boundary map [ gtDilateGrains(bounds, 10) ] - looks very
% odd!

%pass this in for the mo
%bounds_dil=edf_read('/data/id19/graintracking/data/ss2007/ss2007_A_/5_reconstruction/ss2007_A_bounds_dil10.edf');


%loop through vertices, assigning each to a boundary of the original grain
%may not pick up some very small boundaries, but I guess this doesn't
%matter- for grain 1 these are of size 1-39 voxels volume ie max diameter 4
%voxels
faces_bounds=zeros(size(fv.faces));
for ii = 1:length(fv.vertices)
    a=round(fv.vertices(ii, :));
    boundaryID=bounds_dil(a(2), a(1), a(3));
    faces_bounds(find(fv.faces==ii))=boundaryID;
end

if 0 % to render - causes matlab to struggle with memory
figure
fv2=fv;
for ii = 1:length(bounds_list)
    fv2.faces=fv.faces(all(faces_bounds==bounds_list(ii), 2),:);
    p=patch(fv2);
    set(p, 'facecolor', cmap(bounds_list(ii),:));
end
end
    
%read this data

%record all normals and all triangle area for weighting
normals=zeros(length(fv.faces), 3);
areas=zeros(length(fv.faces), 1);


if ~isempty(bounds_list2)
    bounds_list=bounds_list2;
end



for k = 1:length(bounds_list)
    k
    poles=[];
    poles_areas=[];

    dum=find(all(faces_bounds==bounds_list(k), 2));
    if ~isempty(dum)
        
        for jj = 1:length(dum)
            ii = dum(jj);
            face = fv.faces(ii, :);
            %get the three vertices -
            v1=fv.vertices(face(1), [2 1 3]);
            v2=fv.vertices(face(2),  [2 1 3]);
            v3=fv.vertices(face(3),  [2 1 3]);
            %normal:
            n=cross(v1-v2, v3-v2);
            %fix this to follow lsplane output - change sign of third component
            n(3)=-n(3);
            normals(ii, :) = n/norm(n); % may not need to save this info

            %area
            angle=acos(dot((v1-v2)/norm(v1-v2), (v3-v2)/norm(v3-v2)));
            areas(ii) = norm(v1-v2)*norm(v3-v2)*sin(angle)*0.5;

            % Convert from lab to crystallographic plane/pole
            poles(end+1,:) = gtVectorLab2Cryst(normals(ii, :), g);
            poles_areas(end+1) = areas(ii);

        end


        poles2 = [];
        for ii = 1:length(symm)
            poles2 = [poles2; poles*symm(ii).g3];
        end
        %add this to correspond to line in gtMakePoleFigure
        poles2=[poles2; -poles2];
        poles=poles2;
   
        % this unique doesn't seems to work properly, and not sure if it
        % should even be here!
        %  [poles, ndx]=unique(poles2, 'rows');
        
        
        poles_areas=repmat(poles_areas', 48, 1);
     %   poles_areas=poles_areas(ndx);

        gtMakePoleFigure(poles, poles_areas)
        title(sprintf('boundary %d', bounds_list(k)))

        gtMakePoleFigure(poles)
        title(sprintf('boundary %d (not weighted by mesh areas', bounds_list(k)))

        keyboard
        
    end
end

