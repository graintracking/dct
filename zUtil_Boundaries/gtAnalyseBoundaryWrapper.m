




%wrapper


function gtAnalyseBoundaryWrapper(first, last, workingdirectory)

%standard stuff
  if isdeployed
    global GT_DB
    global GT_MATLAB_HOME
    load('workspaceGlobal.mat');
    first=str2double(first);
    last=str2double(last);
  end % special case for running interactively in current directory

  if ~exist('workingdirectory','var')
    workingdirectory=pwd;
  end

  fprintf('Changing directory to %s\n',workingdirectory)
  cd(workingdirectory)

  gtDBConnect
  
%hard coded filenames - very bad
grains=edf_read('5_reconstruction/ss2007_A_twin_dil10_mask.edf');
bounds=edf_read('5_reconstruction/ss2007_A_boundaries.edf');
bounds_dil=edf_read('5_reconstruction/ss2007_A_bounds_dil10.edf');
a=load('boundaries_structure.mat');
boundaries_structure=a.boundaries_structure;
a=load('r_vectors.mat');
r_vectors=a.r_vectors;
tablename='ss2007_boundary_analysis'; %set this up first

%%>> mym('drop table ss2007_boundary_analysis')
%%>> mym('create table ss2007_boundary_analysis (grainID int unsigned not null auto_increment, primary key (grainID))')

%parameters
smoothR=4
reduce_factor=0.5
max_grainID=1038

%rather than do a sequence first to last, use a table so that the first big
%grains are well shared out

grainID=0;
while grainID<=max_grainID

    mym(sprintf('insert into %s (grainID) values("0")', tablename));
    grainID=mym('select last_insert_id()')
    disp(sprintf('doing grain %d...', grainID))

    if find(grains(:)==grainID, 1, 'first')
    
        %mesh grain
    fv=gtGrainBoundarySurfaceMesh(grains, smoothR, grainID);
    %extract pole data
    [poles, weights, pole_data]=gtAnalyseGrainBoundarySurface(fv, reduce_factor, grainID, [], boundaries_structure, r_vectors, bounds_dil);

    %for each boundary calculate density for pole figure
    pole_figure_density=[];
    for i=1:length(pole_data)
        if ~isempty(pole_data(i).poles)
            disp(sprintf('    doing boundary %d...', i))
        [pole_figure_density(i).density, pole_figure_density(i).valid_poles_weight]=gtMakePoleFigure('poles', pole_data(i).poles, 'weights', pole_data(i).weights, 'plot_figure', 0);
        end
    end
    
    save(sprintf('4_grains/grain%d_/pole_figure_density.mat',grainID), 'pole_figure_density')
    end
end