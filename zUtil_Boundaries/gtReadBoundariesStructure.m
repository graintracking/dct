function [output, output2] = gtReadBoundariesStructure(boundaries_structure, phaseid, vThr, DoSym)
%     [output, output2] = gtReadBoundariesStructure(boundaries_structure, phaseid, vThr, DoSym)
%     -----------------------------------------------------------------------------------------
%     boundaries_structure is a .mat file used to store boundary data. It contains: 
%         grain1
%         grain2
%         count
%         grain1_R_vector
%         grain2_R_vector
%         misorientation_angle
%         misorientation_axis
%         gb_norm
%         gb_centre
%         gb_norm_grain1
%         gb_norm_grain2
%         [gb_norm_hex_grain1]
%         [gb_norm_hex_grain2]
%         [misorientation_axis_hex]
%
%     therefore, can pull out the poles for a pole figure according to different
%     criteria, as well as producing symmetry equivalent (for xtallographic
%     things) if required.
%     add - in a crude way - the stuff to read from the the
%     boundaries_structure_test - the pole figure density for each boundary
%     after dealing with curved boundaries.
%     output is either poles or density - either way then used in gtMakePoleFigure
%     output2 is valid poles weight, not used for poles case

poles   = [];
weights = [];
count   = 1;

parameters =[];
load('parameters.mat');
crystal_system = parameters.cryst(phaseid).crystal_system;
% Get symmetry operators
symm = gtCrystGetSymmetryOperators(crystal_system);

% loop through structure
for ii=1:length(boundaries_structure)
    %criteria
    if  boundaries_structure(ii).grain1~=0 && boundaries_structure(ii).grain2~=0 && ~isempty(boundaries_structure(ii).gb_norm)
        if  boundaries_structure(ii).count > vThr
            poles(end+1,:)=boundaries_structure(ii).gb_norm_grain1;
            poles(end+1,:)=boundaries_structure(ii).gb_norm_grain2;
            weights(end+1)=boundaries_structure(ii).count;
            weights(end+1)=boundaries_structure(ii).count;
        end
    end
end

symm = {symm.g3};
if DoSym
    % Apply the symmetry operators
    poles2 = [];
    for ii=1:length(symm)
        poles2 = [poles2; poles*symm{ii}];
    end
    poles = poles2;
    weights = repmat(weights', length(symm), 1);
end

%add this to correspond to line in gtMakePoleFigure
poles   = [poles; -poles];
weights = [weights; weights];
output  = poles;
output2 = weights;

save(fullfile('8_analysis','poles.mat'),'poles');
save(fullfile('8_analysis','weights.mat'),'weights');

%     %find a non-empty density map to preallocate
%     i=1;
%     density=[];
%     count=0;
%     vpw=0;%valid poles_weight for normalising
%     while isempty(density)
%         density=zeros(size(boundaries_structure_test(i).grain1_pole_density));
%         i=i+1;
%     end
%     
%     %for i=1:length(boundaries_structure_test)
%     for i=bridges
%         %criteria
%         %if  boundaries_structure(i).grain1~=0 && boundaries_structure(i).grain2~=0 && ~isempty(boundaries_structure(i).gb_norm)
%         
%             %    if boundaries_structure(i).crack5_count>100
%             %        if isempty(boundaries_structure(i).sigma) || boundaries_structure(i).sigma~=3
%             %if (isempty(boundaries_structure(i).sigma) || boundaries_structure(i).sigma~=3) && boundaries_structure(i).grain1~=0 && boundaries_structure(i).grain2~=0
%             %  if isempty(boundaries_structure(i).sigma)
%             %  if boundaries_structure(i).sigma==3
%             %     if boundaries_structure(i).sigma==9
%             %  if boundaries_structure(i).misorientation_angle>15
%             %end
%             if ~isempty(boundaries_structure_test(i).grain1_pole_density)
%                 density=density+boundaries_structure_test(i).grain1_pole_density;
%                 vpw=vpw+boundaries_structure_test(i).grain1_valid_poles_weight;
%             end
%             if ~isempty(boundaries_structure_test(i).grain2_pole_density)
%                 density=density+boundaries_structure_test(i).grain2_pole_density;
%                 vpw=vpw+boundaries_structure_test(i).grain2_valid_poles_weight;
%             end
%             count=count+1;
%         end%criteria
%     end%loop
%     output=density;
%     output2=vpw;

end % end of function
