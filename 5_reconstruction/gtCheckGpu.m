function gpu_ok = gtCheckGpu()
    % First get list of GPUs
    [ret_code, msg] = unix('nvidia-smi -L');
    if (ret_code)
        disp('No nVidia driver installed!')
        gpu_ok = false;
        return;
    else
        gpu_ok = true;
    end
    % It could be both UUID and S/N, so ".+?" matches both
%     gpus = regexp(msg, '\(.+?: (?<uuid>.*?)\)', 'names');
% 
%     % Now Let's find a GPU which is not attached to a monitor
%     for ii = 1:numel(gpus)
%         [~, msg] = unix(['nvidia-smi -q -i ' gpus(ii).uuid]);
%         displayMode = regexp(msg, 'Display Mode\s*: (?<mode>.*?)\n', 'tokens');
%         if (strcmp(displayMode{1}, 'Disabled'))
%             gpu_ok = true;
%             return;
%         end
%     end
%     gpu_ok = false;
end
