function abs_rec = gtAstraAbsorptionVolume(parameters, proj, varargin)
% GTASTRAABSORPTIONVOLUME reconstruct absorption volume using 3D SIRT or 2D FBP
%     abs = gtAstraAbsorptionVolume(parameters)
%     -----------------------------------------
%     Checks if absorption_stack.mat exists (will create it otherwise)
%     Checks if absorption_mask.mat exists (will create it otherwise)
%     Outputs reconstructed volume into file absorption.mat
%
%     parameters.rec.method : '3DART' / '2DFBP'
%
%     Version 002 26-09-2012 by LNervo
%       Formatting, cleaning, commenting

    conf = struct('save', false, 'filename', 'volume_absorption', 'use_median_files', false);
    conf = parse_pv_pairs(conf, varargin);

    if ~exist('parameters','var') || isempty(parameters)
        parameters = gtLoadParameters();
    end

    projname = fullfile(parameters.acq(1).dir, 'absorption_stack.mat');

    if (~exist('proj', 'var') || isempty(proj))
        if (exist(projname, 'file'))
            check = inputwdefault('Absorption stack already exists - do you want to (re)-create it? [y/n]', 'n');

            if strcmpi(check, 'n')
                fprintf('Loading absorption stack..')
                c = tic();
                proj = load(projname);
                fprintf('\b\b: Done. (%f seconds)\n', toc(c))
            else
                proj = gtAstraPrepareAbsorptionStack(parameters);
            end
        else
            fprintf('Absorption stack doesn''t exist, yet. Building it..')
            c = tic();
            proj = gtAstraPrepareAbsorptionStack(parameters, 'use_median_files', conf.use_median_files);
            fprintf('\b\b: Done in %g seconds.\n', toc(c));
        end
    end

    if (isfield(parameters.rec, 'absorption'))
        rec_abs = parameters.rec.absorption;
    else
        % Old style parameters
        warning('Old "rec" parameters')
        rec_abs = parameters.rec;
        rec_abs.algorithm = rec_abs.method;
    end
    proj.num_iter = rec_abs.num_iter;

    if (~isfield(rec_abs, 'options') || isempty(rec_abs.options))
        rec_abs_options = {};
    else
        rec_abs_options = [fieldnames(rec_abs.options), struct2cell(rec_abs.options)]';
    end

    fprintf('Reconstructing..')
    c = tic();
    is_cone = isfield(parameters.labgeo, 'sourcepoint') && (~isempty(parameters.labgeo.sourcepoint));
    switch (rec_abs.algorithm)
        case {'3DART', 'SIRT'}
            [vol, res_norm] = gtAstra3D(proj, 'is_cone', is_cone);
        case {'SpotSIRT'}
            proj.num_iter = parameters.rec.absorption.num_iter;
            proj.selected = true(size(proj.stack, 2), 1);
            vol = gtAstraSpotSirt(proj);
            res_norm = 0; % to be implemented...
        case {'3DTV'}
            [vol, res_norm] = gtAstra3DTV(proj, 'is_cone', is_cone, rec_abs_options{:});
        case '2DFBP'
            [vol, res_norm] = gtAstra_FBP2D(parameters, proj);
        otherwise
            error('gtAstraAbsorptionVolume:wrong_algorithm', ...
                'Algorithm for reconstruction not known. Please check parameters file');
    end
    fprintf('\b\b: Done. (res: %g, in %f seconds)\n', res_norm, toc(c))

    abs_rec = struct('abs', vol, 'rot_angles', [], 'rot_axes', []);
    if (isfield(proj, 'rot_angles'))
        abs_rec.rot_angles = proj.rot_angles;
        if (isfield(proj, 'rot_axes') && ~isempty(proj.rot_axes))
            abs_rec.rot_axes = proj.rot_axes;
        else
            abs_rec.rot_axes = [0 0 1];
        end
    end

    if (conf.save)
        absorption_volume = fullfile(parameters.acq(1).dir, '5_reconstruction', [conf.filename '.mat']);
        save(absorption_volume, '-struct', 'abs_rec', '-v7.3');
    end
end




