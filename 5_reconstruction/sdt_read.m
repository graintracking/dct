function vol = sdt_read(fname)
% for reading *.sdt files
% fname: filename without the extension
% Wolfgang and Marcelo on 19/06/2006

name = sprintf('%s.spr', fname);
A = floor(textread(name, '%f', 8));
dimension = A(1);

infoVOL = [];
infoVOL.ftype = 'float32';
infoVOL.fbyteorder = 'l';
infoVOL.volSizeX = A(2);
infoVOL.volSizeY = A(5);

switch dimension
    case 2
        infoVOL.volSizeZ = 1;
    case 3
        infoVOL.volSizeZ = A(8);
    otherwise
        Mexc = MException('SDT:wrong_file', 'unsupported file dimension');
        throw(Mexc);
end

sdtname = sprintf('%s.sdt', fname);
vol = gtVolReadSingle(sdtname, infoVOL, 0);

if ndims(vol) == 3 % 3D volume
    vol = permute(vol, [2 1 3]); % reorganise dimensions
else % 2D image
    vol = transpose(vol);
end



