function grain=gtCreateProjectionGeometry(grain,parameters)
% grain=gtCreateProjectionGeometry(grain,parameters)
%     create Projection Geometry input structure for ASTRA code
%



grainfile=sprintf('%s/4_grainsASTRA/grain%d_/grain%d_.mat',parameters.acq.dir,grain.id,grain.id);

if ~isfield(grain,'index')
   grain.index=ones(length(grain.difspots),1);
end

ind=find(grain.index);    % total number of spots for the grain
total_spots=length(ind);

detucen0=parameters.geo.detucen0;
detvcen0=parameters.geo.detvcen0;
detdiru0=parameters.geo.detdiru0';
detdirv0=parameters.geo.detdirv0';
detpos0=(parameters.geo.detpos0/parameters.acq.pixelsize)';

detposcorner=detpos0-detucen0*detdiru0-detvcen0*detdirv0;  % the (0,0) corner of the detector in Lab system

rotcomp = gtFedRotationMatrixComp(parameters.geo.rotdir0');


V=zeros(total_spots,12);

for n=1:total_spots


    omega=grain.allblobs.omega(ind(n));
    eta=grain.allblobs.eta(ind(n));
    theta=grain.allblobs.theta(ind(n));

    r=[cosd(2*theta);sind(2*theta)*sind(eta);sind(2*theta)*cosd(eta)];


    Rottens = gtFedRotationTensor(-omega,rotcomp);


    detdiru = Rottens*detdiru0;
    detdirv = Rottens*detdirv0;
    detpos  = Rottens*detpos0;
    r       = Rottens*r;

    V(n,:)=[r(1), r(2), r(3), detpos(1), detpos(2), detpos(3), detdiru(1), detdiru(2), detdiru(3), detdirv(1), detdirv(2), detdirv(3)];

end



%% save the projection stack and geometry information into the grain.mat file...

grain.proj_geom_full=V;

save(grainfile,'grain');


end



