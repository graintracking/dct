function detector_weights_param = gtRecGrainsOptionsDetectorWeightsDefaultParameters()
    detector_weights_param = struct(...
        'type', 'linear', ... % Possibilities are: {'linear'} | 'sqrt' | 'log' 
        'coefficient', 10, ...
        'nvar_images', 'raw', ... % {'raw'}: raw images, 'median': blobs + median images, others: only blobs
        'use_dark_image_variance', true, ...
        'add_weights_in_6D_algo', 1, ... % Run function to add weights in gtReconstructGrainOrientation: 1 (add weights if nvar is missing), 2 (add weights anyway)
        'save_grain', true); % whether to save grain automatically after generation of noise variances.
    % 'type' decides what type of function of the noise variances is
    % inversely proportional to the detector weights. 'linear' means
    % detector weights are inversely proportional to noise variances.
    % 'sqrt' and 'log' mean detector weights are inversely proportional to
    % the square roots and logarithms of noise variances respectively.

    % 'coefficient' is an offset adding to noise variances when calculating
    % detector weights. It is used to control the detector weights. The
    % larger the coefficients, the smaller the differences of the weights.
    % If the coefficients are sufficiently large, the weights are all close
    % to 1.

    % 'nvar_images' determines what type of images is used as noise
    % variances. 'raw' means using raw images as noise variances. 'median'
    % means using the sum of 'median' background images and segmented
    % 3D blob images. Others mean using only segmented 3D blob images.

    % 'use_dark_image_variance' determines whether to add the dark image
    % noise variances to the blob noise variances.

    % 'add_weights_in_6D_algo' determines whether to add the noise
    % variances at the beginning of the grain reconstruction function, so
    % that the generation of noise variances can run on computer cluster.

    % 'save_grain' determines whether to save the grain automatically after
    % generation of the noise variances.
end