function [volume, res_norm] = gtAstra3D(proj, varargin)
% GTASTRA3D  3D SIRT reconstruction
%     volume = gtAstra3D(proj, varargin)
%     ---------------------------------
%
%     INPUT: proj structure containing fields:
%                .stack          stack of projections
%                .geom           projection geometry for Astra
%                                geom is a N x 12 matrix, witn N = #projections
%                                Each row is:
%                                ( rayX, rayY, rayZ, dX, dY, dZ, uX, uY, uZ, vX, vY, vZ )
%                                  ray: the ray direction
%                                  d  : the center of the detector
%                                  u  : the vector from detector pixel (0,0) to (0,1)
%                                  v  : the vector from detector pixel (0,0) to (1,0)
%                .num_rows       vertical size of projection images
%                .num_cols       horizontal size of projection images
%                .num_iter       number of iterations
%                .vol_size_x     x size of output volume
%                .vol_size_y     y size of output volume
%                .vol_size_z     z size of output volume
%
%     INPUT (optional): varargin fields:
%                verbose : {false} | true <logical>, NOT active yet
%                is_cone : {false} | true <logical>, for cone beam
%
%     OUTPUT:  volume  <double vol_size_x, vol_size_y, vol_size_z>)
%
%
%     Version 002 26-09-2012 by LNervo
%       Formatting, cleaning, commenting

    conf = struct('verbose', false, 'is_cone', false);
    conf = parse_pv_pairs(conf, varargin);

    if (~gtCheckGpu())
        mexc = MException('ASTRA:no_GPU', ...
            'gtAstra3D: Submit this job on a GPU machine ! Exiting...');
        throw(mexc)
    else
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % projection geometry (type, #rows, #columns, vectors)
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        if (conf.is_cone)
            proj_geom = astra_create_proj_geom('cone_vec', proj.num_rows, proj.num_cols, proj.geom);
        else
            proj_geom = astra_create_proj_geom('parallel3d_vec', proj.num_rows, proj.num_cols, proj.geom);
        end

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % create volume and projection data objects (x, y, z)
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % proj_data should be a 3d "matrix" of size [#columns,#projections,#rows]
        vol_geom = astra_create_vol_geom(proj.vol_size_x, proj.vol_size_y, proj.vol_size_z);
        proj_id  = astra_mex_data3d('create', '-proj3d', proj_geom, proj.stack);

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % allocate and zero memory for reconstruction
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        vol_id = astra_mex_data3d('create', '-vol', vol_geom, 0);

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % prepare reconstruction algorithm
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        cfg = astra_struct('SIRT3D_CUDA');
        cfg.ProjectionDataId = proj_id;
        cfg.ReconstructionDataId = vol_id;
        cfg.option.MinConstraint = 0;
        alg_id = astra_mex_algorithm('create', cfg);

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % reconstruct
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %disp('reconstructing...');
        astra_mex_algorithm('iterate', alg_id, proj.num_iter);
        volume = astra_mex_data3d('get_single', vol_id);
        % vol is now a 3d "matrix" of size [x,y,z]

        res_norm = astra_mex_algorithm('get_res_norm', alg_id) / gtMathsNorm_l2(proj.stack);

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % clean up
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        astra_mex_algorithm('delete', alg_id);
        astra_mex_data3d('delete', vol_id);
        astra_mex_data3d('delete', proj_id);

        if (isfield(proj, 'padding') && ~isfield(proj, 'extra_detector_offset'))
            volume = volume( ...
                proj.padding+1:end-proj.padding, ...
                proj.padding+1:end-proj.padding, ...
                proj.padding+1:end-proj.padding );
        end
        if (isfield(proj, 'padding') && isfield(proj, 'extra_detector_offset')) %no vertical padding in absorption
            volume = volume( ...
                proj.padding+1:end-proj.padding, ...
                proj.padding+1:end-proj.padding, ...
                : );
        end
    end
end
