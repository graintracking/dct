function grains = gtLoadAllGrainVolumes(basedir, grains_num)
% GTLOADALLGRAINVOLUMES Loads all the grains in a list

    grains = {};
    parfor ii = 1: grains_num
        graindir = fullfile(basedir, '4_grains', sprintf('grain%d_', ii));

        vol_filename = fullfile(graindir, sprintf('grain%d_filled.edf',ii) );
        if exist(vol_filename,'file')  %if post-processed file exists
            grains{ii} = edf_read(vol_filename);
        else
            grain_name   = fullfile(graindir, 'grain.edf' );
            grains{ii} = edf_read(grain_name);
        end % if exist vol_filename
    end
end
