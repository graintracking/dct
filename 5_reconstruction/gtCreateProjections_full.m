function proj=gtCreateProjections_full(grainvol,proj_geom_full,parameters)
% proj=gtCreateProjections_full(grainvol,proj_geom_full,parameters)

hsize=parameters.acq.xdet;
vsize=parameters.acq.ydet;

nproj=size(proj_geom_full,1);

xvol=size(grainvol,1);
yvol=size(grainvol,2);
zvol=size(grainvol,3);



%% projection geometry (type, #rows, #columns, vectors)
proj_struct = astra_create_proj_geom('parallel3d_vec', vsize, hsize, proj_geom_full);


%% volume geometry (x, y, z)
vol_geom = astra_create_vol_geom(xvol, yvol, zvol);


%% create projection data of phantom
proj_data_id = astra_create_sino3d_cuda(grainvol, proj_struct, vol_geom);

proj= astra_mex_data3d('get', proj_data_id);


proj=permute(proj,[3 1 2]);

proj=reshape(proj,vsize,hsize,1,nproj);

astra_mex_data3d('delete', proj_data_id);

end
