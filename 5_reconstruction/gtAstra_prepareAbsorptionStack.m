function proj = gtAstra_prepareAbsorptionstack(parameters);
% GTASTRA_PREPAREABSORPTIONSTACK  assembles the 3D projection stack of
% absorption images and saves them to file absorption_stack.mat
%
% INPUTS:  parameters <structure>
%
% OUTPUTS: proj <structure>
%          fields:
%          - stack    <double X,X,Z>  3D stack of absorption images
%          - geom     <double Z,12 >  Vector describing Astra preojection geometry
%          - Omega    <Double Z>      Rotation angles in degrees


acq=parameters.acq;
rec=parameters.rec;
geo=parameters.geo;

if strcmp(acq.type, '360degree')
        totproj=2*acq.nproj;
        increment=2*pi/totproj;
        angle_rad=[0:increment:2*pi-increment];
    elseif strcmp(acq.type, '180degree')
        totproj=acq.nproj;
        increment=pi/totproj;
        angle_rad=[pi/2:increment:3/2*pi-increment];
    else
        error('scan type not recognised!')
end

        % projection geometry... Here we assume that the absorption images are
        % shifted such that the axis of rotation is in the center of the images
        % The origin of the sample coordinate system is on the rotation axis - in
        % the middle of the illuminated area

        detpos0  = [0, 0, 0];

        rotcomp = gtFedRotationMatrixComp(geo.rotdir0');


       % stack = zeros(acq.bb(4),acq.bb(3),totproj/rec.absinterval);
        stack = zeros(acq.bb(3), totproj/rec.absinterval, acq.bb(4));
        geom  = zeros(totproj/rec.absinterval, 12);

        jj=1;

        disp('building sinogram...');

        for ii = 0 : rec.absinterval : totproj-1

          if acq.interlaced_turns == 0
            absname = fullfile(acq.dir,'1_preprocessing','abs',sprintf('abs%04d.edf',ii));
          else
            absname = fullfile(acq.dir,'1_preprocessing','abs',sprintf('abs%04d.edf',ii));
          end

          Omega(jj) = angle_rad(ii+1);
          %stack(:,:,jj) = -log(edf_read(absname));
          stack(:,jj,:)  = -log(edf_read(absname))';
          Rottens = gtFedRotationTensor(-Omega(jj)*180/pi,rotcomp);
          detdiru = Rottens*geo.detdiru0';
          detdirv = Rottens*geo.detdirv0';
          detpos  = Rottens*detpos0';
          r       = Rottens*geo.beamdir0';

          geom(jj,:)=[r(1), r(2), r(3), detpos(1), detpos(2), detpos(3), detdiru(1), detdiru(2), detdiru(3), detdirv(1), detdirv(2), detdirv(3)];

          jj=jj+1;
        end

        proj.stack = stack;
        proj.Omega = Omega;
        proj.geom  = geom;

        disp('writing sinogram');
        name = fullfile(acq.dir,'absorption_stack.mat');
        save(name,'proj');

end
