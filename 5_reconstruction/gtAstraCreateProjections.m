function [proj, int] = gtAstraCreateProjections(volume, proj_geom, proj_size, do_permute, proj_model, psf)
% GTASTRACREATEPROJECTIONS  Calculate oblique angle projections of a volume
%     proj = gtAstraCreateProjections(volume, proj_geom, proj_size, [do_permute], [proj_model])
%     -----------------------------------------------------------------------------------------
%
%     INPUT:
%       volume     = volume to be projected (<double X, Y, Z>)
%       proj_geom  = projection geometry for Astra code
%                    (12 element vector created by "gtAstraCreateProjectionGeometry" (<double Nx12>)
%                    [beam_direction(1:3), detect_corner_pos(1:3), detect_u_dir(1:3), detect_v_dir(1:3)]
%       proj_size  = vertical and horizontal size of projection image (<double 2>)
%
%       permute    = if true, the projection stack will be permuted and
%                    organised as a 4D matrix for visualization (<logical> {false})
%       proj_model = either "parallel3d_vec" (default) or "cone_vec"
%
%     OUTPUT:
%       proj       = 3D matrix of projections images
%                    (proj_size(2), nproj, proj_size(1))
%                    use option permute and "montage(proj)" to display the stack
%       int        = summed intensity for each of the projections

OK = gtCheckGpu();

if (~OK)
    mexc = MException('ASTRA:no_GPU','gtAstraCreateProjections: Submit this job on a GPU machine ! Exiting...');
    throw(mexc)
end

if (~exist('do_permute','var') || isempty(do_permute))
    do_permute = false;
end
if (~exist('proj_model','var') || isempty(proj_model))
   proj_model = 'parallel3d_vec';
   disp(['using ' proj_model ' as projection model'])
end

hsize = proj_size(2);
vsize = proj_size(1);
nproj = size(proj_geom, 1);

xvol  = size(volume, 1);
yvol  = size(volume, 2);
zvol  = size(volume, 3);


% projection geometry (type, #rows, #columns, vectors)
proj_geom = astra_create_proj_geom(proj_model, vsize, hsize, proj_geom);

% volume geometry (x, y, z)
vol_geom = astra_create_vol_geom(yvol, xvol, zvol);

% create projection data of phantom
proj_id = astra_create_sino3d_cuda(volume, proj_geom, vol_geom);
proj    = astra_mex_data3d('get', proj_id);

if (exist('psf','var') && ~isempty(psf))
    psf_obj = GtPSF();
    psf_obj.set_psf_direct(psf, [hsize, vsize]);
    proj = psf_obj.apply_psf_direct(proj);
end
int = sum(sum(proj, 1), 3);
if (do_permute)
    proj = permute(proj, [3 1 2]);
    proj = reshape(proj, vsize, hsize, 1, nproj);
end

astra_mex_data3d('delete', proj_id);

end
