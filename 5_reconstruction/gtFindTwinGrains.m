function [twin_list, combine_list] = gtFindTwinGrains()
% GTFINDTWINGRAINS  Automatically identify twins and combines when assembling volume
%                   as for gtnew_assemble_volX, but if two grains overlap, calc
%                   misorientation angle/axis.
%
%
%
% 4/3/2008 simplify to work with ART bboxes, and un-postprocessed sdt
% volumes

    num_grains=1038; % TODO: THIS IS HARDCODED BUT SHOULD NOT BE
    parameters=[];
    load('parameters.mat');
    symm = gtCrystGetSymmetryOperators(parameters.cryst.crystal_system);

    twin_list=[];
    combine_list=[];
    touching_matrix=zeros(num_grains);
    bboxlist=zeros(num_grains, 6);
    rvectors=zeros(num_grains, 3);

    %sigma s: type, angle, axis {, brandon criteria}
    warning('sigma axis indices must be positive, normalised, and in descending order...');
    pause(1);
    sigmas=[3 60 1 1 1;...
            5 36.86 1 0 0;...
            7 38.21 1 1 1;...
            9 38.94 1 1 0];
    %add brandon criteria
    sigmas(:,6)=15*sigmas(:,1).^(-0.5);
    %normalise axis
    sigmas(:,3:5)=sigmas(:,3:5)./repmat(sqrt(sum(sigmas(:,3:5).*sigmas(:,3:5),2)),1,3);

    if ~exist('r_vectors.mat', 'file')
        % Work from bounding boxes initially
        %%%%%%%%%%%  Read data from .mat files   %%%%%%%%%%%%%%%%%%%
        gauge = GtGauge(num_grains, 'Loading data: ', 'style', 'percentage');
        for ii = 1:num_grains
            gauge.incrementAndDisplay();

            tmp = load(sprintf('4_grains/grain%d_/grain%d_.mat', ii, ii), 'R_vector', 'bad', 'x1','y1','nx','ny','zend','zstart');
            if (isfield(tmp, 'bad') && tmp.bad~=0)
                continue;
            end
            % Work with the ART resconstruction boundingbox
            bboxlist(ii, :)  = [tmp.x1 tmp.y1 tmp.zstart tmp.nx tmp.ny tmp.zend-tmp.zstart];
            rvectors(ii, :)  = tmp.R_vector;
            r_vectors(ii, :) = [ii tmp.R_vector];
        end
        gauge.delete();
        save r_vectors r_vectors;
    else
        load r_vectors;
    end

    if ~exist('touching_matrix.mat', 'file')
        %%%%%%%%%%%%%%   Determine which grains touch %%%%%%%%%%%%%%%%%%%
        gauge = GtGauge(num_grains, [], 'style', 'percentage');
        for ii = 1:num_grains
            gauge.incrementAndDisplay();

            mygrain = bboxlist(ii, :);
            %  myvol=sfGetPartialGrain(i, bboxlist, bboxlist(i,:));

            %find overlapping bboxes
            xtest=find(bboxlist(:,1)<(mygrain(1)+mygrain(4)) & (bboxlist(:,1)+bboxlist(:,4))>mygrain(1));
            ytest=find(bboxlist(:,2)<(mygrain(2)+mygrain(5)) & (bboxlist(:,2)+bboxlist(:,5))>mygrain(2));
            ztest=find(bboxlist(:,3)<(mygrain(3)+mygrain(6)) & (bboxlist(:,3)+bboxlist(:,6))>mygrain(3));

            candidates=intersect(xtest, intersect(ytest, ztest)); %ugly...
            candidates(find(candidates==i))=[];

            for jj = 1:length(candidates)
                if (ii > candidates(jj))
                    continue;
                end

                %try without this test - use something simpler
                %testvol=sfGetPartialGrain(candidates(jj), bboxlist, bboxlist(ii, :));
                %test=length(find(testvol & myvol));
                %if test>0

                testgrain=bboxlist(candidates(jj), :);
                %new test - if more than 50% bbox volume shared,say they are touching
                %more the 50% in any dimension
                xstart=max(mygrain(1), testgrain(1));
                xend=min(mygrain(1)+mygrain(4), testgrain(1)+testgrain(4));
                xoverlap=xend-xstart;
                xlength=min(mygrain(4), testgrain(4));
                xtest=xoverlap/xlength;

                ystart=max(mygrain(2), testgrain(2));
                yend=min(mygrain(2)+mygrain(5), testgrain(2)+testgrain(5));
                yoverlap=yend-ystart;
                ylength=min(mygrain(5), testgrain(5));
                ytest=yoverlap/ylength;

                zstart=max(mygrain(3), testgrain(3));
                zend=min(mygrain(3)+mygrain(6), testgrain(3)+testgrain(6));
                zoverlap=zend-zstart;
                zlength=min(mygrain(6), testgrain(6));
                ztest=zoverlap/zlength;

                if (xtest>0.5 || ytest>0.5 || ztest>0.5)
                    touching_matrix(ii, candidates(jj)) = 1;
                end
            end
        end
        gauge.delete();
        save touching_matrix touching_matrix;
    else
        load touching_matrix;
    end

    %now need to check out the touching grains.  Touching grains can be
    %twins, grains that should be combined, or neither of these.
    gauge = GtGauge(num_grains, 'Checking twins/combines: ', 'style', 'percentage');
    for ii = 1:num_grains
        gauge.incrementAndDisplay();

        for jj = 1:num_grains
            if touching_matrix(ii, jj)==1
                sigma = sfTwinTest(rvectors(ii, :), rvectors(jj, :)); %are these a twin?

                if ~isempty(sigma)
                    if length(sigma==1)
                        %disp('found a twin')
                        twin_list(end+1, :)=[ii, jj, sigma];
                    else
                        fprintf('\nOops, two possible sigmas for i=%d / j=%d     \n', ii, jj);
                        keyboard;
                        gauge.rePrint();
                    end
                end

                if 0 % skip combine test for the moment - slow
                    read_out = sfCombineTest(ii, jj); %should grains be combined?

                    if ~isempty(read_out)
                        combine_list(end+1,:)=[ii jj read_out];
                    end
                end
            end
        end
    end
    gauge.delete();

    keyboard;

    if ~isempty(twin_list)
        twin_list=sfSortList(twin_list(:,1:2));
    end
    if ~isempty(combine_list)
        combine_list=sfSortList(combine_list(:,1:2));
    end

    %%%%%%%%%%    SubFunctions    %%%%%%%%%%

    function sigma = sfTwinTest(R1, R2)

        [mis_angle, mis_axis] = gtDisorientation(R1.', R2.', symm, ...
            'sort', 'descend');
        % In the case of hexagonal crystals
        %[mis_angle, mis_axis] = gtDisorientation(R1.', R2.', symm, ...
        %    'sort', 'descend', 'latticepar', latticepar);

        if ~isempty(mis_axis)
            %determine sigma type - initially check just sigma three
            test_angle=abs(mis_angle-sigmas(:,2))<sigmas(:,6);
            test_axis=(180/pi)*abs(acos(dot(repmat(mis_axis',size(sigmas,1),1), sigmas(:,3:5), 2)))<sigmas(:,6);
            sigma=sigmas(find(test_angle & test_axis), 1);
        end
    end

    function read_out = sfCombineTest(grain1, grain2)
    %call plot_rodrigues_consistancy_grain for the two grains.  If more
    %than 90% of the used projections are consistant, consider this a
    %possible combine.
        read_out=plot_rodrigues_consistancy_grain([grain1 grain2], 0);
        if ~all(read_out>0.9)
            read_out=[];
        end
    end

    function temp_output = sfGetPartialGrain(grainid, bboxlist, outputbbox);
    % ii is the grainid
    % Supply a bounding box, function returns just the part of the grain which
    % extends into this bounding box.
        temp_output = zeros(outputbbox(5), outputbbox(4), outputbbox(6));

        %vol_filename = sprintf('4_grains/grain%d_/grain%d_2.edf',grainid, grainid);
        %vol = edf_read(vol_filename);
        vol = sdt_read(sprintf('4_grains/grain%d_/grain%d__res0_3',grainid,grainid));

        % Limits within outputbbox volume
        min3 = bboxlist(grainid,3) - outputbbox(3)+1;
        max3 = min3 + bboxlist(grainid,6)-1;
        min1 = bboxlist(grainid,2) - outputbbox(2)+1;
        max1 = min1 + bboxlist(grainid,5)-1;
        min2 = bboxlist(grainid,1) - outputbbox(1)+1;
        max2 = min2 + bboxlist(grainid,4)-1;

        % Have to deal with bounding boxs that exceed the slice size
        if min1<1
            tmp=2-min1;%if min of first index==0, crop one row of voxels from vol
            min1=1;
            vol=vol(tmp:end,:,:);
        end
        if min2<1
            tmp=2-min2;%if min of second index==0, crop one row of voxels from vol
            min2=1;
            vol=vol(:,tmp:end,:);
        end
        if min3<1
            tmp=2-min3;%if z1==0, crop one row of voxels from vol
            min3=1;
            vol=vol(:,:,tmp:end);
        end
        if max1>outputbbox(5);
            tmp=max1-outputbbox(5);%if x2 is one too big, crop one row of voxels
            max1=outputbbox(5);
            vol=vol(1:end-tmp,:,:);
        end
        if max2>outputbbox(4);
            tmp=max2-outputbbox(4);%if y2 is one too big, crop one row of voxels
            max2=outputbbox(4);
            vol=vol(:,1:end-tmp,:);
        end
        if max3>outputbbox(6);
            tmp=max3-outputbbox(6);%if z2 is one too big, crop one row of voxels
            max3=outputbbox(6);
            vol=vol(:,:,1:end-tmp);
        end

        %grain volume into output volume
        temp_output(min1:max1, min2:max2, min3:max3)=temp_output(min1:max1, min2:max2, min3:max3)+vol;
    end % end of sfGetPartialGrain

    function outlist = sfSortList(twins)
    % sort out twin list
    % twin_list - pairs of twins
    % want to sort this into groups - set of twins
        outlist=[]
        for ii = 1:length(twins)
            new_group=twins(ii, :);

            if any(any(new_group(1)==outlist)) || any(any(new_group(2)==outlist))
                continue;
            end

            new_length=length(new_group)
            old_length=0;
            group=new_group;
            addtwins=[];

            while new_length>old_length
                old_length=new_length;

                for k=1:length(group)
                    [r,c]=ind2sub(size(twins), find(twins==group(k)));
                    addtwins=twins(r, :);
                    addtwins=addtwins(:)';
                    group=[group addtwins];
                end

                group=unique(group);
                new_length=length(group);
            end
            outlist(end+1,1:length(group))=group;
        end
    end

end % end of main function

