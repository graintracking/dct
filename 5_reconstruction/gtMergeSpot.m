%function [c,ea,eb]=gtMergeSpot(a,b)

  for i=1:14;

  a=sdt_read(sprintf('grain2_/grain2_dif%d',i));
  b=sdt_read(sprintf('grain2_/grain2_dif%d',i+14));


  xs=min([size(a,2),size(b,2)]);
  ys=min([size(a,1),size(b,1)]);

  a=a(1:ys,1:xs);
  b=b(1:ys,1:xs);
  b=fliplr(b);
  a=wiener2(a,[10 10]);
  b=wiener2(b,[10 10]);

  ea=edge(a,'canny');
  eb=edge(b,'canny');


  c=correlate(ea,eb);

  bc=interpolatef(b,c(1),c(2));


  c=a+bc;
  t=graythresh(c);
  proj=im2bw(c,0.28*t);

  name=sprintf('grain2_/grain2_difs%d',i);
  sdt_write(name,proj,'float32')

  %imshow([im2bw(c,0.25*t),1000*c],[])
  findshifts3(a,bc,'clims','block','on',[-0.001 0.001],'climt',[-0.01 0.01])


  end

