function vol = gtAstraSpotSirt(proj)

    vol_geom = astra_create_vol_geom(proj.vol_size_x, proj.vol_size_y, proj.vol_size_z);
    
    proj.geom = proj.geom(proj.selected, :);
    proj.stack = proj.stack(:, proj.selected, :);

    proj_geom = astra_create_proj_geom('parallel3d_vec', proj.num_rows, proj.num_cols, proj.geom);

    W = opTomo('cuda', proj_geom, vol_geom);
    
    vol = sirt3d(W, proj.stack(:), proj.num_iter);

    vol = reshape(vol, [proj.vol_size_x, proj.vol_size_y, proj.vol_size_z]);
    if (isfield(proj, 'padding'))
            vol = vol( ...
                proj.padding+1:end-proj.padding, ...
                proj.padding+1:end-proj.padding, ...
                : );
    end
    clear W    
end
