function [list, empty] = gtFindFailedGrains(phase_ID, varargin)
% function list = gtFindFailedGrains(phase_ID, fieldname)
% INPUTS:   phaseid  <int>
%           varargin  {'ODF6D'}  /  'VOL3D' / 'SEG'

if (~isempty(varargin))
    fieldname = varargin{1};
else
    fieldname = 'ODF6D';
end

disp(sprintf('searching for entry %s',fieldname));

load(sprintf('4_grains/phase_%02d/index.mat', phase_ID)); 
grain_file_name = sprintf('4_grains/phase_%02d/grain_details_', phase_ID);


num_grains = numel(grain);

list = [];
empty = [];

failed_grains = 0;
for i = 1 : num_grains
    num_chars = fprintf('%d/%d (failed: %d)', i, num_grains, failed_grains);
    mf = matfile(sprintf('%s%04d.mat', grain_file_name, i));
    try 
        size(mf, fieldname);
        if strcmp(fieldname, 'SEG')    % check if there is really a volume
            load(sprintf('%s%04d.mat', grain_file_name, i), 'SEG');
            if size(SEG.seg) == 1
               empty = [empty, i];
               failed_grains = failed_grains + 1;
            end
        end
    catch
        list = [list, i];
        failed_grains = failed_grains + 1;
    end
    fprintf(repmat('\b', [1 num_chars]))
end
end
