function stat=sdtwrite(name,vol,type)
% writes 2D or 3D binary files (.sdt) and associated descriptor file (.spr)
% for the moment only 'float32' is implemented as type...
% ATTENTION : the name has to be given without extension (.sdt) !
% IS THIS IMAGE TRANSPOSED??? GJ
sdt=sprintf('%s.sdt',name);
spr=sprintf('%s.spr',name);

switch type
  case 'float32'
    typenum=3;
%    disp(sprintf('writing file %s',sdt));
  otherwise
    disp('datatype not yet supported - use float32')
    return;
end
s=size(vol);
%disp('Writing header')
fid_spr=fopen(spr,'w');
fprintf(fid_spr,'%d\n',length(s));
fprintf(fid_spr,'%d\n',s(2));   % was s(2) before
fprintf(fid_spr,'0\n1\n');
fprintf(fid_spr,'%d\n',s(1));    % was s(1) before
fprintf(fid_spr,'0\n1\n');
if length(s)==3
  fprintf(fid_spr,'%d\n',s(3));
  fprintf(fid_spr,'0\n1\n');
end
fprintf(fid_spr,'%d\n',typenum);
fprintf(fid_spr,'0\n1\n');
fclose(fid_spr);

%disp('Writing data')
% 3D volumes might not be in correct order!
fid_sdt=fopen(sdt,'w');
fwrite(fid_sdt,vol',type);

fclose(fid_sdt);


