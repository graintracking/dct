function grain=gtAutoSelectProjections(grain,parameters)

  grain.index=ones(length(grain.index),1);
  niter1=5;
  niter2=5;

  vol=gtRECONastra_roi(grain,parameters,niter1);

  % Estimate a reasonable threshold from the central region (20%) of the volume

  [xvol,yvol,zvol]=size(vol);
  centerx=[round(xvol*0.4):round(xvol*0.6)];
  centery=[round(yvol*0.4):round(yvol*0.6)];
  centerz=[round(zvol*0.4):round(zvol*0.6)];

  thresh=0.5*gtImgMeanValue(vol(centerx,centery,centerz));

  % Eliminate non-connected parts by image reconstruction

  marker=double(zeros(size(vol)));
  marker(round(xvol/2),round(yvol/2),round(zvol/2))=1;
  voltr=double(vol>thresh);
  voltr=imreconstruct(marker,voltr);

  % Simulate projections

  proj=gtCreateProjections(voltr,grain,parameters);

  for i=1:size(proj,4)
    n1(i)=norm((proj(:,:,1,i)-grain.difstack(:,:,1,i)),1);
  end

  med=median(n1);

  ind=find(n1>parameters.rec.as_tresh*med);
  grain.index(ind)=0;

  [vol2,sample2]=gtRECONastra_roi(grain,parameters,niter2);

  thresh=0.5*gtImgMeanValue(vol2(centerx,centery,centerz));

  % Eliminate non-connected parts by image reconstruction


  voltr2=double(vol2>thresh);
  voltr2=imreconstruct(marker,voltr2);
  grain.seg=voltr2;
  grain.sample=sample2;
end
