function abs=gtAstra_FBP2D(parameters,proj)

acq=parameters.acq;
rec=parameters.rec;
geo=parameters.geo;


% name=sprintf('%s/absorption_stack.mat',acq.dir);
% if exist(name,'file')
%     check=inputwdefault('Absorption stack already exists - do you want to (re)-create it ? [y/n]', 'n');
%     if strcmpi(check,'n')
%         disp('reading projections...');
%         load(name);
%     else
%         proj=gtAstra_prepareAbsorptionstack(parameters);
%     end
%
% else
%
%     proj=gtAstra_prepareAbsorptionstack(parameters);
% end



%% now start FBP reconstruction of absorption volume

proj_data=permute(proj.stack,[3 2 1]);   % transforms stack of projection images to stack of sinograms


vsize=size(proj.stack,1);  % numer of slices to be reconstructed
hsize=size(proj.stack,2);  % number of pixels in projection image
nproj=size(proj.stack,3);  % number of projections


sino=zeros(nproj,hsize);


xvol=hsize;
yvol=hsize;
zvol=vsize;



%% projection geometry (type, #rows, #columns, vectors)
proj_geom = astra_create_proj_geom('parallel', 1.0, hsize, proj.Omega);
vol_geom = astra_create_vol_geom(hsize, hsize);
proj_id = astra_create_projector('linear', proj_geom, vol_geom);
sinogram_id = astra_mex_data2d('create','-sino', proj_geom, 0);

% allocate space to store reconstruction image
recon_id = astra_mex_data2d('create', '-vol', vol_geom, 0);

% create FBP algorithm configuration
cfg = astra_struct('FBP_CUDA');
cfg.ProjectionDataId = sinogram_id;
cfg.ReconstructionDataId = recon_id;
cfg.FilterType = 'Ram-Lak';
cfg.option.GPUindex = 0;

% allocate space for the output volume
abs=zeros(xvol,yvol,zvol);

for i=1:vsize

    alg_id = astra_mex_algorithm('create', cfg);

    sino=proj_data(:,:,i);

    astra_mex_data2d('set',sinogram_id,sino)

    astra_mex_algorithm('run', alg_id);

    abs(:,:,vsize-i+1) = astra_mex_data2d('get', recon_id);

    astra_mex_algorithm('delete', alg_id);
end

% clean up
astra_mex_data2d('delete', sinogram_id);
astra_mex_data2d('delete', recon_id);

astra_mex_projector('delete', proj_id);

%  set voxels outside the central cylinder defined by the projection size to the background value
[x,y]=meshgrid([-(hsize/2-0.5):(hsize/2-0.5)],[-(hsize/2-0.5):(hsize/2-0.5)]);
mask=sqrt(x.*x+y.*y)<(hsize/2-0.5);
mask=repmat(mask,[1,1,vsize]);
abs=abs.*mask;



