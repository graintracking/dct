function par=gtMakeHSTParfile(parameters)

acq=parameters.acq;

%% default values
par.parfilename=sprintf('%s/%s_test.par',acq.dir,acq.name);

par.num_first_image=0;
par.number_length_varies='NO';
par.length_of_numerical_part=num2str(4);
par.file_postfix='.edf';
par.file_interval=1;

par.correct=0;
par.correct_flatfield='NO';
par.subtract_background='NO';

par.take_logarithm='YES';
par.rotation_vertical='YES';
par.output_sino='NO';
par.output_rec='YES';
par.oversampling_factor=num2str(4);
par.angle_offset=num2str(0);
par.cache_kilobytes=num2str(256);
par.sino_megabytes=num2str(400);
par.correct_spikes='YES';
par.ccd_filter='"CCD_FILTER"';
par.correct_spikes_threshold=num2str(0.04);
par.correct_rings='NO';   % all the stuff needed here ?
par.correct_rings_nb=0;
par.correct_axis='NO';
par.correct_axis_file='';
par.padding='E';
par.axis_to_the_center='N';
par.graph_out='NO';

% modif sabine
% if strcmp(acq.scantype,'360degree')
%     scanrange=360;
% else
%     scanrange=180;
% end
if isfield(acq,'scantype')
    if strcmp(acq.scantype,'360degree') || strcmp(acq.type,'360')
        scanrange=360;
    else
        scanrange=180;
    end
elseif isfield(acq,'type')
    if strcmp(acq.type,'360degree') || strcmp(acq.type,'360')
        scanrange=360;
    else
        scanrange=180;
    end
else
    disp('can not read scan range');
    return;
end

% modif sabine

%% scan specific values

par.num_image_1=num2str(acq.bb(3));
par.num_image_2=num2str(acq.bb(4));
par.first=num2str(0);
% modif sabine
% par.last=num2str(acq.nproj-1);
% par.angle_between_projections=num2str(scanrange/acq.nproj);
if ~isfield(acq,'interlaced_turns') || acq.interlaced_turns==0
    par.last=num2str(acq.nproj-1);
    par.prefix=sprintf('%s/1_preprocessing/abs/abs',acq.dir);
    par.angle_between_projections=num2str(scanrange/2/acq.nproj);   %quick fix (2*acq.nproj) wolfgang 04/2008
elseif acq.interlaced_turns==1
    par.last=num2str((acq.interlaced_turns+1)*acq.nproj-1);
    par.prefix=sprintf('%s/1_preprocessing/abs/abs_renumbered',acq.dir);
    par.angle_between_projections=num2str(scanrange/((acq.interlaced_turns+1)*acq.nproj));
else
    disp('to be adjusted');
    return;
end
% fin modif sabine
par.image_pixel_size_1=num2str(acq.pixelsize);
par.image_pixel_size_2=num2str(acq.pixelsize);

par.rotation_axis_position=num2str(acq.bb(3)/2+0.5);
par.start_voxel_1=num2str(1);
par.start_voxel_2=num2str(1);
par.start_voxel_3=num2str(1);
par.end_voxel_1=num2str(acq.bb(3));
par.end_voxel_2=num2str(acq.bb(3));
par.end_voxel_3=num2str(acq.bb(4));
par.output_file=sprintf('%s/%s.vol',acq.dir,acq.name);

par.zeroclipvalue = acq.zeroclipvalue;

gtHSTParamWriter(par);
