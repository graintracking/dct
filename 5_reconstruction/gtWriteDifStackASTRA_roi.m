function grain=gtWriteDifStackASTRA_roi(grain,parameters)
% write full .dat projection STACK for ASTRA reconstruction
% projetions are oversized (zeropadded) by a factor of 1.5

oversize=1.5;


grainfile=sprintf('%s/4_grainsASTRA/grain%d_/grain%d_.mat',parameters.acq.dir,grain.id,grain.id);

if ~isfield(grain,'index')
   grain.index=ones(length(grain.difspots),1);
end


total_spots=length(grain.index);    % total number of spots for the grain

hsize=round(max(grain.stat.bbxs)*oversize);   % take the biggest spot and add a relative extra margin (multiplicative factor oversize)
vsize=round(max(grain.stat.bbys)*oversize);   % ...same for vertical direction

detrefu=parameters.geo.detrefu;
detrefv=parameters.geo.detrefv;

detdiru0=parameters.geo.detdiru';
detdirv0=parameters.geo.detdirv';

detrefpos=(parameters.geo.detrefpos/parameters.acq.pixelsize)';

detposcorner=detrefpos-detrefu*detdiru-detrefv*detdirv;  % the (0,0) corner of the detector in Lab system

rotcomp = gtFedRotationMatrixComp(parameters.geo.rotdir');

gcenter=grain.center';

avgsize=grain.stat.bbxsmean*grain.stat.bbysmean;

% allocate memory for matrices
difstack=zeros(vsize,hsize,1,total_spots);
bb=zeros(total_spots,4);
hoffset=zeros(total_spots,1);
voffset=zeros(total_spots,1);
tmp=zeros(vsize,hsize);

for n=1:total_spots

    [spot,bb(n,:)]=gtGetSummedDifSpot(grain.difspots(n),parameters,1);
    hoffset(n)=round((hsize-bb(n,3))/2);
    voffset(n)=round((vsize-bb(n,4))/2);
    spot=gtPlaceSubImage(spot,tmp,hoffset(n)+1,voffset(n)+1);
    spot= spot/sum(spot(:))*avgsize*grain.stat.bbxsmean;  % here we force the intensity to be the same in all spots -
    difstack(:,:,1,n)=spot;

    omega=grain.omega(n);
    eta=grain.eta(n);
    theta=grain.theta(n);

    r=[cosd(2*theta);sind(2*theta)*sind(eta);sind(2*theta)*cosd(eta)];

     detpos =  detposcorner + (bb(n,1)-hoffset(n)-1+hsize/2)*detdiru0    + (bb(n,2)-voffset(n)-1+vsize/2)*detdirv0;


    % rotate detector coordinates by -omega ...

    Rottens = gtFedRotationTensor(-omega,rotcomp);


    detdiru = Rottens*detdiru0;
    detdirv = Rottens*detdirv0;
    detpos  = Rottens*detpos-gcenter;    % take into account the coordinate shift corresponding to the offset of the reconstructed volume
    r       = Rottens*r;

    V(n,:)=[r(1), r(2), r(3), detpos(1), detpos(2), detpos(3), detdiru(1), detdiru(2), detdiru(3), detdirv(1), detdirv(2), detdirv(3)];
end



%% save the projection stack and geometry information into the grain.mat file...
grain.difstack=difstack;
grain.proj_geom=V;

save(grainfile,'grain');


end



