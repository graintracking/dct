function vol=gtRECONastra(grain,parameters)

ind=find(grain.index);
difstack=squeeze(grain.difstack(:,:,1,ind));

V=grain.proj_geom(ind,:);

proj_data=permute(difstack,[2 3 1]);


% V is a N x 12 matrix, witn N = #projections
% Each row is
%           ( rayX, rayY, rayZ, dX, dY, dZ, uX, uY, uZ, vX, vY, vZ )
%           ray: the ray direction
%           d  : the center of the detector
%           u  : the vector from detector pixel (0,0) to (0,1)
%           v  : the vector from detector pixel (0,0) to (1,0)


vsize=size(difstack,1);
hsize=size(difstack,2);
xvol=parameters.acq.bb(3);
yvol=parameters.acq.bb(3);
zvol=parameters.acq.bb(4);
niter=parameters.rec.num_iter;

%% projection geometry (type, #rows, #columns, vectors)
proj_geom = astra_create_proj_geom('parallel3d_vec', vsize, hsize, V);


%% create volume and projection data objects (x, y, z)
% proj_data should be a 3d "matrix" of size [#columns,#projections,#rows]
vol_geom = astra_create_vol_geom(xvol, yvol, zvol);
proj_id = astra_mex_data3d('create', '-proj3d', proj_geom, proj_data);




%% allocate and zero memory for reconstruction
vol_id = astra_mex_data3d('create', '-vol', vol_geom, 0);

%% prepare reconstruction algorithm
cfg = astra_struct('SIRT3D_CUDA');
cfg.ProjectionDataId = proj_id;
cfg.ReconstructionDataId = vol_id;
cfg.option.GPUIndex = 0;
alg_id = astra_mex_algorithm('create', cfg);

%% reconstruct
astra_mex_algorithm('iterate', alg_id, niter);
vol = astra_mex_data3d('get', vol_id);
% vol is now a 3d "matrix" of size [x,y,z]

%% clean up
astra_mex_algorithm('delete', alg_id);
astra_mex_data3d('delete', vol_id);
astra_mex_data3d('delete', proj_id);
