function vol_out = gtDilateGrains(vol, dilate_length, show_result, use_c_func, verbose)
% dilate grains to fill gaps between them.
% one grain colour must not overgrow another.
%
% look for zero voxels, find the lowest non-zero neighbour, and change voxel
% to that colour

    if (~exist('show_result', 'var'))
        show_result = true;
    end

    if (~exist('use_c_func', 'var'))
        use_c_func = true;
    end

    if (~exist('verbose', 'var'))
        verbose = true;
    end

    vol_out     = vol;
    size_vol    = size(vol);
    voxels_done = zeros(1, dilate_length);

    if (verbose)
        gauge = GtGauge(dilate_length, 'Dilation step: ');
    end
    for step = 1:dilate_length
        if (verbose)
            gauge.incrementAndDisplay();
        end
        % rather than loop though everything, make a shortlist of voxels that
        % need looking at by monochrome dilation of the grains
        mono_vol = vol > 0;
        mono_dilated = imdilate(mono_vol, ones(3, 3, 3) );
        mono_dilated = (mono_dilated - mono_vol) & ~vol;
        todo_voxels    = find(mono_dilated);
        voxels_done(step) = length(todo_voxels);

        [xx, yy, zz] = ind2sub(size_vol, todo_voxels);

        %look at voxel neighbourhood
        xstart = xx - 1;
        xend   = xx + 1;
        ystart = yy - 1;
        yend   = yy + 1;
        zstart = zz - 1;
        zend   = zz + 1;

        xstart = max(xstart, 1);
        ystart = max(ystart, 1);
        zstart = max(zstart, 1);

        xend = min(xend, size_vol(1));
        yend = min(yend, size_vol(2));
        zend = min(zend, size_vol(3));

        if (~isempty(todo_voxels))
            local_colour = vol_out(todo_voxels);
            if (use_c_func)
                lims = [xstart'; xend'; ystart'; yend'; zstart'; zend'];
                local_colour = gtCxxDilateGrainsVoxel(local_colour, vol_out, 0, lims);
            else
                for ii = 1:length(xx)
                    neighbours = vol(xstart(ii):xend(ii), ystart(ii):yend(ii), zstart(ii):zend(ii));
                    if any(neighbours(:))
                        local_colour(ii) = min(neighbours(neighbours ~= 0));
                    end
                end % loop though todo_voxels
            end
            vol_out(todo_voxels) = local_colour;
        end

        vol = vol_out; %ready for next iteration
    end
    if (verbose)
        gauge.delete();
    end
    if (show_result), figure, plot(voxels_done, 'x-'); end
end
