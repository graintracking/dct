function rec_opts = gtReconstruct6DGetParamenters(parameters)
% FUNCTION rec_opts = gtReconstruct6DGetParamenters(parameters)
%

    rec = parameters.rec;
    if (~isfield(rec, 'grains'))
        error([mfilename ':bad_structure'], ...
            'The parameters are too old or malformed: there is no ".grains" in .rec!')
    end

    num_iter = rec.grains.num_iter;

    if (~isfield(rec.grains, 'options') || isempty(rec.grains.options))
        warning('gtReconstruct6DGetParamenters:wrong_parameters', ...
            'The rec.grains structure doesn''t seem to be valid. Falling back to defaults')
        rec.grains = gtRecGrainsDefaultParameters(rec.grains.algorithm);
    end

    def_rec = gtRecGrainsDefaultParameters(rec.grains.algorithm);
    def_opts = def_rec.options;

    rec_opts = rec.grains.options;
    rec_opts.num_iter = num_iter;
    rec_opts.algorithm = rec.grains.algorithm;

    % We impose "det_ind = 1" here for the moment!!!
    % It might be added later to the default options
    rec_opts.det_ind = 1;

    if (~isfield(rec_opts, 'volume_downscaling') ...
            || isempty(rec_opts.volume_downscaling))
        rec_opts.volume_downscaling = def_opts.volume_downscaling;
    end
    if (~isfield(rec_opts, 'ospace_super_sampling') ...
            || isempty(rec_opts.ospace_super_sampling))
        if (~isfield(rec_opts, 'super_sampling') ...
                || isempty(rec_opts.super_sampling))
            rec_opts.ospace_super_sampling = def_opts.ospace_super_sampling;
        else
            rec_opts.ospace_super_sampling = rec_opts.super_sampling;
        end
    end
    if (~isfield(rec_opts, 'rspace_super_sampling') ...
            || isempty(rec_opts.rspace_super_sampling))
        rec_opts.rspace_super_sampling = def_opts.rspace_super_sampling;
    end
    if (~isfield(rec_opts, 'ospace_oversize') ...
            || isempty(rec_opts.ospace_oversize))
        rec_opts.ospace_oversize = def_opts.ospace_oversize;
    end
    if (~isfield(rec_opts, 'rspace_oversize') ...
            || isempty(rec_opts.rspace_oversize))
        rec_opts.rspace_oversize = def_opts.rspace_oversize;
    end
    if (~isfield(rec_opts, 'shape_functions_type') ...
            || isempty(rec_opts.shape_functions_type))
        rec_opts.shape_functions_type = def_opts.shape_functions_type;
    end
    if (~isfield(rec_opts, 'detector_norm') ...
            || isempty(rec_opts.detector_norm))
        rec_opts.detector_norm = def_opts.detector_norm;
    end
    if (~isfield(rec_opts, 'tv_norm') ...
            || isempty(rec_opts.tv_norm))
        rec_opts.tv_norm = def_opts.tv_norm;
    end
    if (~isfield(rec_opts, 'tv_strategy') ...
            || isempty(rec_opts.tv_strategy))
        rec_opts.tv_strategy = def_opts.tv_strategy;
    end
    if (~isfield(rec_opts, 'use_predicted_scatter_ints') ...
            || isempty(rec_opts.use_predicted_scatter_ints))
        rec_opts.use_predicted_scatter_ints = false;
    end
    if (~isfield(rec_opts, 'use_matrix_row_rescaling') ...
            || isempty(rec_opts.use_matrix_row_rescaling))
        rec_opts.use_matrix_row_rescaling = false;
    end
    if (~isfield(rec_opts, 'max_grid_edge_points') ...
            || isempty(rec_opts.max_grid_edge_points))
        if (~isfield(rec_opts, 'grid_edge') ...
                || isempty(rec_opts.grid_edge))
            rec_opts.max_grid_edge_points = def_opts.max_grid_edge_points;
        else
            rec_opts.max_grid_edge_points = rec_opts.grid_edge;
        end
    end
    if (~isfield(rec_opts, 'ospace_resolution'))
        rec_opts.ospace_resolution = [];
    end
    if (~isfield(rec_opts, 'lambda_l1') ...
            || isempty(rec_opts.lambda_l1))
        if (~isfield(rec_opts, 'lambda') ...
                || isempty(rec_opts.lambda))
            rec_opts.lambda_l1 = def_opts.lambda_l1;
        else
            rec_opts.lambda_l1 = rec_opts.lambda;
        end
    end
    if (~isfield(rec_opts, 'lambda_tv') ...
            || isempty(rec_opts.lambda_tv))
        rec_opts.lambda_tv = def_opts.lambda_tv;
    end
end
