function [vol_out,thresh_val] = gtFillGrain(grainid, varargin)

%vol is a greyscale grain volume.
%use Greg's gtSnakeBW to fill holes in th grain, going slice by slice.
%try to remove odd features...

%varargin : saveflag to save an edf in the grain directory
saveflag=0;




tmp = load(sprintf('4_grains/grain%d_/grain%d_.mat',grainid, grainid));

if isfield(tmp, 'bad')  &  &  tmp.bad>0

  disp('grain is already set to bad')
  vol_out=[];
  return

end

vol = sdt_read(sprintf('4_grains/grain%d_/grain%d__res0_3',grainid,grainid));
volorig=vol;
%threshold - this could use the automatic function graythresh

%     a=max(vol(:));
%     val1=mean(vol(find(vol>(a/10))))-std(vol(find(vol>(a/10))));%std dev, ignoring low (background) values
%     %further playing...
%     [n,x]=hist(vol(:),500);
%     n(find(x>val1))=[];
%     x(find(x>val1))=[];
%     val2=min(x(find(n==min(n))));
%     thresh_val=min(val1, val2);
if ~isempty(varargin)
    thresh_val=varargin{1};
else
    thresh_val=graythresh(vol);
end
    %volgrey=vol;%save grey vol
vol=vol>thresh_val;

%volA=vol;

%label vol
vol=bwlabeln(vol, 6);

%discard small bits (smaller than 10% of the largest non-zero bit)
[n,x]=hist(vol(:), max(vol(:)));
%remove zeros
n(1)=n(1)-length(find(vol==0));
x=x+0.5;
%keep only the labels of large things
x(find(n<(max(n)/10)))=[];
vol2=zeros(size(vol));
for i=1:length(x)
vol2=vol2+(vol==x(i));
end
vol=vol2;

figure(2);imshow(volorig(:,:,3)-2*thresh_val*vol(:,:,3),[-3*thresh_val,10*thresh_val])


%volB=vol;

%try to kill spurs without changing shape
%spurs are horizontal plane only, so can use only vertical strel
%pad top and bottom of vol with zeros to help erosion
%vol2=zeros(size(vol)+[0 0 2]);
%vol2(:,:,2:end-1)=vol;

%bigger range can be used on big grains
%a=max(10, ceil(size(vol,3)/10))

%vol2=imerode(vol2, ones(1,1,a));
%vol2=imdilate(vol2, ones(1,1,2*a));
%vol2 should have no spurs, but hopefully the over dilate will prevent
%anything real from disappearing.

%unpad vol2
%vol2(:,:,1)=[];
%vol2(:,:,end)=[];

%now use vol2 to mask vol
%vol=vol.*vol2;

%volC=vol;

%now work slice by slice
for i=3

  if length(find(vol(:,:,i)))>0

  %test on aspect ratio

  %use snake to fill holes
  try
    vol(:,:,i)=gtSnakeBW(vol(:,:,i));%can pass snakeoptions here
  end

  end

end

vol_out=vol(:,:,3);


if saveflag
name=sprintf('4_grains/grain%d_/grain%d_filled.edf',grainid,grainid);
disp(sprintf('writing volume %s',name));
edf_write(vol_out,name,'uint8');
end
