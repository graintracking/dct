function gtFillGrain_wrapper(first, last, workingdirectory)


warning('off','Images:initSize:adjustingMag');

if isdeployed
  global GT_DB
  global GT_MATLAB_HOME
  load('workspaceGlobal.mat');
  first=str2double(first);
  last=str2double(last);
end % special case for running interactively in current directory

if ~exist('workingdirectory','var')
  workingdirectory=pwd;
end

cd(workingdirectory)

gtDBConnect();

parameters=[];
load('parameters.mat');

fill_table=[parameters.acq.name '_fill'];

min_id=1;
max_id=[];
try
  max_id=mym(sprintf('select max(grainid) from %sextspot', parameters.acq.name))
  disp(sprintf('doing grainids up to %d', max_id))
catch
  disp('no extspot table found, will try grainids up to 1000')
  max_id=1000;
end
zA=0;
zB=parameters.acq.bb(4);


for grainid=min_id:max_id

  test=0;
  try
    test=mym(sprintf('insert into %s (grainid) values (%d)', fill_table, grainid));
  end

  if test==1 %no other job is doing this grain...


    tmp=load(sprintf('4_grains/grain%d_/grain%d_.mat',grainid,grainid));

    if isfield(tmp, 'bad')  &  &  tmp.bad>0
      disp(sprintf('%d is a bad grain', grainid))
      continue
    end

    if tmp.zstart>zB || tmp.zend<zA
      disp(sprintf('%d is outside of the ROI', grainid))
      continue
    end

    disp(sprintf('doing grain %d', grainid))
    gtFillGrain(grainid, 1);

  end

  disp(sprintf('another job has taken grain %d', grainid))

end


