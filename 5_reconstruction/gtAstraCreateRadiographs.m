function [proj_stack, geom] = gtAstraCreateRadiographs(abs, parameters)
% GTASTRACREATERADIOGRAPHS forward project the absorption volume
% function [proj_stack, geom] = gtAstraCreateRadiographs(abs, parameters)
%
%     Input and output arguments description, variable type (in <>) and default value (in {}) are written below.
%
%     INPUT:
%          parameters = grain of interest (<struct>)
%                 abs = absorption volume of the sample
%
%     OUTPUT:
%          proj_stack = stack of absorption radiographs
%                geom = Projection geometry vector used by Astra (<double Nproj x 12>)

if ~exist('parameters','var')
    load parameters;
end

acq = parameters.acq;
rec = parameters.rec;
geo = parameters.labgeo;

increment = 180/acq.nproj;

blocks = 10;  % split the calculation of the full projection stack into 10 smaller blocks

if strcmp(acq.type, '360degree')
    totproj = 2 * acq.nproj;
    Omega   = [0 : increment : 360-increment];
elseif strcmp(acq.type, '180degree')
    totproj = acq.nproj;
    Omega   = [90 : increment : 270-increment];    % using pmo and fasttomo scanning macro, a '180 degree'  scan covers from 90->270 degrees
else
    Mexc = MException('ACQ:invalid_parameter', ...
                      'Unknown type of scan. Check parameters.acq.type!');
    throw(Mexc);
end

vsize = size(abs, 3);
hsize = size(abs, 1);
xvol  = hsize;
yvol  = hsize;
zvol  = vsize;

detdiru0 = geo.detdiru';
detdirv0 = geo.detdirv';
beamdir0 = geo.beamdir';

detrefpos = [0, 0, 0]';   % the absorption images are centered on the rotation axis - the distance to the  detector does not matter

rotcomp   = gtFedRotationMatrixComp(geo.rotdir');

%% create ASTRA volume data object (x, y, z)
vol_geom = astra_create_vol_geom(xvol, yvol, zvol);

%% allocate memory for the output stack
proj_stack = zeros(vsize, hsize, totproj);

for j = 1 : blocks
  n     = 1;
  start = (j-1) * totproj/blocks + 1  % number of first image
  finish= j * totproj / blocks       %

  for i = start : finish

    Rottens = gtFedRotationTensor(-Omega(i), rotcomp);

    detdiru = Rottens * detdiru0;
    detdirv = Rottens * detdirv0;
    detpos  = Rottens * detrefpos;
    beamdir = Rottens * beamdir0;

    V(n,:)=[beamdir(1), beamdir(2), beamdir(3), detpos(1), detpos(2), detpos(3), detdiru(1), detdiru(2), detdiru(3), detdirv(1), detdirv(2), detdirv(3)];
    n     = n + 1;

  end

  %% projection geometry (type, #rows, #columns, vectors)
  proj_geom = astra_create_proj_geom('parallel3d_vec', vsize, hsize, V);

  %% create projection data of absorption volume
  proj_id = astra_create_sino3d_cuda(abs, proj_geom, vol_geom);
  proj = astra_mex_data3d('get', proj_id);

  astra_mex_data3d('delete', proj_id);

  proj_stack(:, :, start:finish) = permute(proj, [3,1,2]);
  geom(start:finish,:)           = V;

end

end
