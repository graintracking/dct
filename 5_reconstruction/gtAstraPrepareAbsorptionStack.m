function proj = gtAstraPrepareAbsorptionStack(parameters, varargin)
% GTASTRAPREPAREABSORPTIONSTACK  Assembles the 3D projection stack of absorption
%                                images and saves them to file absorption_stack.mat
%
%     proj = gtAstraPrepareAbsorptionStack(parameters)
%     ------------------------------------------------
%     INPUT:
%       parameters <structure>
%
%     OUTPUT:
%       proj <structure> with fields:
%         - stack    <double X,X,Z>  3D stack of absorption images
%         - geom     <double Z,12 >  Vector describing Astra preojection geometry
%         - Omega    <Double Z>      Rotation angles in degrees
%
%
%     Version 002.1 26-09-2012 by AKing
%       Change sign of omega angles (used by 2DFBP) when axis is RH [0 0 1]
%
%     Version 002 26-09-2012 by LNervo
%       Formatting, cleaning, commenting

conf = struct( ...
    'rot_angles', [], ...
    'rot_axes', [0 0 1], ...
    'psf', [], ...
    'save', true, ...
    'filename', 'absorption_stack', ...
    'use_median_files', false, ...
    'extra_detector_offset', [], ...
    'selected', []);
conf = parse_pv_pairs(conf, varargin);

if isempty(parameters)
    gtAstraCreateDefaultParameters(pwd)
end

acq = parameters.acq(1);

labgeo = parameters.labgeo(1);
samgeo = parameters.samgeo(1);
recgeo = parameters.recgeo(1);
detgeo = parameters.detgeo(1);

if (isfield(parameters.rec, 'absorption'))
    rec_abs = parameters.rec.absorption;
else
    % Old style parameters
    warning('Old "rec" parameters')
    % algorithm can be 'SIRT', '2DFBP' or '3DTV'
    rec_abs = gtRecAbsorptionDefaultParameters(parameters.rec.method);
    rec_abs.num_iter = parameters.rec.abs_num_iter;
    rec_abs.interval = parameters.rec.absinterval;
end
if (~isempty(conf.psf))
    rec_abs.psf = conf.psf;
elseif (~isfield(rec_abs, 'psf'))
    rec_abs.psf = [];
end

ang_incr = pi / acq.nproj;

switch(lower(acq.type))
    case '360degree'
        totproj   = 2 * acq.nproj;
        angle_rad = 0 : ang_incr : 2*pi-ang_incr;
    case '180degree'
        totproj   = acq.nproj;
        angle_rad = pi/2 : ang_incr: 3/2*pi-ang_incr;
    otherwise
        error('Scan type not recognised!')
end

% projection geometry... Here we assume that the absorption images are
% shifted such that the axis of rotation is in the center of the images
% The origin of the sample coordinate system is on the rotation axis - in
% the middle of the illuminated area
fprintf('Building geometry..\n')
imgs_nums = 0 : rec_abs.interval : totproj-1;
Omega_rad = angle_rad(imgs_nums + 1);
Omega_deg = Omega_rad * 180 / pi;

%bb = [acq.bb(1), detgeo.detrefv - acq.bb(4) / 2 + 0.5, acq.bb(3), acq.bb(4)];

bbpos = repmat(acq.bb, [numel(Omega_deg), 1]);
% bbpos = [gtGeoGrainCenterSam2Det(samgeo.orig, 0, parameters) - (acq.bb(3:4) - 1) / 2, acq.bb(3:4)];
% bbpos = repmat(bbpos, [numel(Omega_deg), 1]);
if isfield(parameters.acq, 'correct_sample_shifts')
    if parameters.acq.correct_sample_shifts
        [shifts_lab, shifts_sam] = gtMatchGetSampleShifts(parameters, Omega_deg);
    else
        shifts_sam = zeros(numel(Omega_deg), 3);
        shifts_lab = zeros(numel(Omega_deg), 3);
    end
else
    shifts_sam = zeros(numel(Omega_deg), 3);
    shifts_lab = zeros(numel(Omega_deg), 3);
end
%figure; scatter(shifts_lab(:, 1), shifts_lab(:, 2));
if isempty(conf.extra_detector_offset)
    extra_detector_offset = acq.rotu - (detgeo.detrefu - detgeo.detrefpos(2) / detgeo.pixelsizeu);   %sub-pixel shift for 2D-FBP algorithm
else
    extra_detector_offset = conf.extra_detector_offset;
end
geom = gtGeoProjForReconstruction([], Omega_deg', [], bbpos, [], ...
    detgeo, labgeo, samgeo, recgeo, 'ASTRA_absorption', shifts_sam);
if (~isempty(conf.rot_angles))
    rot_tensor_tot = eye(3);
    for ii_a = 1:numel(conf.rot_angles)
        rot_comp = gtMathsRotationMatrixComp(conf.rot_axes(ii_a, :)', 'col');
        rot_tensor_a = gtMathsRotationTensor(conf.rot_angles(ii_a), rot_comp);

        rot_tensor_tot = rot_tensor_a * rot_tensor_tot;
    end

    geom = reshape(geom, [], 3, 4);
    geom = gtMathsMatrixProduct(geom, rot_tensor_tot);
    geom = reshape(geom, [], 12);
end

fprintf('\b\b: Done.\nBuilding sinogram: \n');
c = tic();
abs_files_dir = fullfile(acq.dir, '1_preprocessing', 'abs');
info = [];
stack_size = [acq.bb(3), totproj/rec_abs.interval, acq.bb(4)];
stack = zeros(stack_size, 'single');

if conf.use_median_files
    prefix = 'med';
else
    prefix = 'abs';
end

for jj = 1:numel(imgs_nums)
    num_chars = fprintf('%03d/%03d', jj, numel(imgs_nums));

    if (acq.interlaced_turns == 0)
        absname = fullfile(abs_files_dir, sprintf('%s%04d.edf', prefix, imgs_nums(jj)));
    else
        absname = fullfile(abs_files_dir, sprintf('abs_renumbered%04d.edf', imgs_nums(jj)));
    end
    [abs_img, info] = edf_read(absname, [], false);

    stack(:, jj, :) = abs_img';

    fprintf(repmat('\b', 1, num_chars))
end
stack = -log(max(0.001, stack));

if isempty(conf.selected)
    selected = true(size(stack, 2), 1);
else
    selected = conf.selected;
end


fprintf('Done in %f seconds.\n', toc(c));


fprintf('...done!\n')

% Dealing with the padding
new_stack_size = stack_size + [2, 0, 0] * rec_abs.padding;
new_stack_shifts = [1, 0, 0] * rec_abs.padding;
stack(stack < 0) = 0;
new_stack = gtPlaceSubVolume(zeros(new_stack_size, 'single'), stack, new_stack_shifts);
new_stack(1:rec_abs.padding, :, :) = repmat(stack(1, :, :), [rec_abs.padding, 1, 1]);
new_stack((end - rec_abs.padding + 1) : end, :, :) = repmat(stack(end, :, :), [rec_abs.padding, 1, 1]);

proj = struct( ...
    'padding', rec_abs.padding, ...
    'psf', rec_abs.psf, ...
    'rot_angles', conf.rot_angles, ... % Additional rotation
    'rot_axes', conf.rot_axes, ...   % Additional rotation
    'extra_detector_offset', extra_detector_offset, ... % Correction for sub-pixel shift of rotation axis in 2D-FBP algorithm
    'stack', new_stack, ...
    'Omega', Omega_rad(selected), ...
    'geom', geom, ...
    'num_rows', new_stack_size(3), ...
    'num_cols', new_stack_size(1), ...
    'num_iter', rec_abs.num_iter, ...
    'vol_size_x', new_stack_size(1), ...
    'vol_size_y', new_stack_size(1), ...
    'vol_size_z', new_stack_size(3), ...
    'selected', selected);
if (conf.save)
    name = fullfile(acq.dir, [conf.filename '.mat']);
    disp(['Writing sinogram in ' name]);
    save(name, '-struct', 'proj', '-v7.3');
end

end
