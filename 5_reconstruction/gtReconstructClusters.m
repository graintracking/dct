function gtReconstructClusters(first, last, workingdirectory, phase_id, parameters, varargin)
% gtReconstructClusters  6D cluster reconstructions on a GPU machine
%     gtReconstructClusters(first, last, workingdirectory, phaseID, [parameters], varargin)
%     -------------------------------------------------------

    if (~gtCheckGpu())
        error([mfilename ':no_GPU'], ...
            'You should submit this job on a GPU machine ! Exiting...');
    end

    currentDir = pwd;
    cd(workingdirectory);

    if (isdeployed)
        global GT_DB %#ok<NUSED,TLEV>
        global GT_MATLAB_HOME %#ok<NUSED,TLEV>
        load('workspaceGlobal.mat');
        first = str2double(first);
        last = str2double(last);
        phase_id = str2double(phase_id);
    end

    if (~exist('parameters','var') || isempty(parameters))
        parameters = gtLoadParameters();
    end

    samplefile = fullfile(parameters.acq.dir, '4_grains', 'sample.mat');
    sample = GtSample.loadFromFile(samplefile);

    conf = struct( ...
        'list', [], ...
        'ospace_resolution', [] );
    conf = parse_pv_pairs(conf, varargin);

    if (isempty(conf.list))
        cluster_list = first:last;
    else
        cluster_list = conf.list;
        % in case the code is deployed, we have to convert to strings
        if (ischar(cluster_list))
            cluster_list = sscanf(cluster_list, '%d');
        end
    end
    
    num_clusters = numel(cluster_list);
    
    for ii_cl = 1:num_clusters
        cl_id = cluster_list(ii_cl);
        cl_info = sample.phases{phase_id}.clusters(cl_id);

        if (~cl_info.active)
            warning([mfilename ':wrong_cluster'], ...
                'Cluster from phase %d, and grain ids:%s, will be reconstructed but it is not active', ...
                phase_id, sprintf(' %d', cl_info.included_ids))
        end

        cl = gtLoadCluster(phase_id, cl_info.included_ids);
        gtReconstructGrainCluster(cl, phase_id, parameters, ...
            'ospace_resolution', conf.ospace_resolution);
    end

    cd(currentDir);
end
