function grain=gtWriteDifStackASTRA_roi(grain,parameters)
% write full .dat projection STACK for ASTRA reconstruction
% projetions are oversized (zeropadded) by a factor of 1.5

oversize=1.5;


grainfile=sprintf('%s/4_grainsASTRA/grain%d_/grain%d_.mat',parameters.acq.dir,grain.id,grain.id);

if ~isfield(grain,'index')
   grain.index=ones(length(grain.difspots),1);
end


total_spots=length(grain.index);    % total number of spots for the grain

hsize=round(max(grain.stat.bbxs)*oversize);   % take the biggest spot and add a relative extra margin (multiplicative factor oversize)
vsize=round(max(grain.stat.bbys)*oversize);   % ...same


detucen0=parameters.acq.detucen0;
detvcen0=parameters.acq.detvcen0;
detdiru0=parameters.acq.detdiru0';
detdirv0=parameters.acq.detdirv0';
detpos0=(parameters.acq.detpos0/parameters.acq.pixelsize)';
gcenter=grain.center';

detposcorner=detpos0-detucen0*detdiru0-detvcen0*detdirv0;  % the (0,0) corner of the detector in Lab system

rotcomp = gtFedRotationMatrixComp(parameters.acq.rotdir0');


avgsize=grain.stat.bbxsmean*grain.stat.bbysmean;

% allocate memory for matrices
difstack=zeros(vsize,hsize,1,total_spots);
bb=zeros(total_spots,4);
hoffset=zeros(total_spots,1);
voffset=zeros(total_spots,1);
tmp=zeros(vsize,hsize);

for n=1:total_spots


    omega=grain.omega(n);
    eta=grain.eta(n);
    theta=grain.theta(n);

    r=[cosd(2*theta);sind(2*theta)*sind(eta);sind(2*theta)*cosd(eta)];

    detpos =  detposcorner + (bb(n,1)-hoffset(n)-1+hsize/2)*detdiru0    + (bb(n,2)-voffset(n)-1+vsize/2)*detdirv0;

    %rotate grain position by omega
    %Rottens = gtFedRotationTensor(omega,rotcomp);
    %gcenter  = Rottens*gcenter;

    % rotate detector coordinates by -omega ...

    Rottens = gtFedRotationTensor(-omega,rotcomp);


    detdiru = Rottens*detdiru0;
    detdirv = Rottens*detdirv0;
    detpos  = Rottens*detpos-gcenter;
    r       = Rottens*r;

    V(n,:)=[r(1), r(2), r(3), detpos(1), detpos(2), detpos(3), detdiru(1), detdiru(2), detdiru(3), detdirv(1), detdirv(2), detdirv(3)];
end



%% save the projection stack and geometry information into the grain.mat file...

grain.proj_geom=V;

save(grainfile,'grain');


end



