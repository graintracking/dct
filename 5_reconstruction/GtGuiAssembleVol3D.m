classdef GtGuiAssembleVol3D < GtBaseGuiElem
    properties
        defaultConf;

        assembler;

        assembleStep = '';
        hasGMgui;
    end
    methods (Access = public)
        function obj = GtGuiAssembleVol3D(varargin)
            obj = obj@GtBaseGuiElem();
            obj.defaultConf = GtAssembleVol3D.generateDefaults();

            obj.assembler = GtAssembleVol3D();
            [hasP, ind] = ismember('f_title', varargin(1:2:end));
            if (hasP)
                obj.conf.f_title = varargin{ind*2};
                varargin(ind*2-1:ind*2)=[];
            end
            [hasP, ind] = ismember('GMgui', varargin(1:2:end));
            if (hasP)
                obj.conf.GMgui = varargin{ind*2};
                varargin(ind*2-1:ind*2)=[];
                obj.assembleStep = obj.conf.GMgui.conf.load_vol;
                obj.hasGMgui = true;
            else
                obj.hasGMgui = false;
            end
            guiArguments = obj.assembler.changeParams(varargin);

            obj.initGtBaseGuiElem(guiArguments);
        end
    end

    methods (Access = protected)
        function initGui(obj)
            initGui@GtBaseGuiElem(obj)

            asm_boxes = [];
            asm_boxes.main = uiextras.VBox('Parent', obj.conf.currentParent);
            asm_boxes.param_boxes = uiextras.VBox('Parent', asm_boxes.main);
            asm_boxes.butt_a_boxes = uiextras.HButtonBox('Parent', asm_boxes.main);
            asm_boxes.butt_d_boxes = uiextras.HButtonBox('Parent', asm_boxes.main);

            set(asm_boxes.main, 'Sizes', [-1 40 40]);

            buttonSize = get(asm_boxes.butt_a_boxes, 'ButtonSize');
            buttonSize(1) = 140;
            set(asm_boxes.butt_a_boxes, 'ButtonSize', buttonSize);
            set(asm_boxes.butt_d_boxes, 'ButtonSize', buttonSize);

            % Conf choice
            asm_boxes = obj.createConfChoices(asm_boxes);

            % Buttons
            asm_boxes.butts = [];
            asm_boxes.butts.edit_phase = uicontrol('Parent', asm_boxes.butt_a_boxes, ...
                                                'Style', 'edit', ...
                                                'String', '1', ...
                                                'Callback', @(src, evt)callbackButtSinglePhase(obj));
            asm_boxes.butts.butt_phase = uicontrol('Parent', asm_boxes.butt_a_boxes, ...
                                                'Style', 'pushbutton', ...
                                                'String', 'Assemble Phase', ...
                                                'Callback', @(src, evt)callbackButtSinglePhase(obj));
            asm_boxes.butts.butt_phase = uicontrol('Parent', asm_boxes.butt_a_boxes, ...
                                                'Style', 'pushbutton', ...
                                                'String', 'Assemble All Phases', ...
                                                'Callback', @(src, evt)callbackButtAllPhases(obj));
            asm_boxes.butts.butt_vol = uicontrol('Parent', asm_boxes.butt_a_boxes, ...
                                                'Style', 'pushbutton', ...
                                                'String', 'Assemble Volume', ...
                                                'Callback', @(src, evt)callbackButtVolume(obj));
            asm_boxes.butts.butt_reset = uicontrol('Parent', asm_boxes.butt_a_boxes, ...
                                                'Style', 'pushbutton', ...
                                                'String', 'Reset', ...
                                                'Callback', @(src, evt)callbackButtReset(obj));

            asm_boxes.butts.edit_dilate = uicontrol('Parent', asm_boxes.butt_d_boxes, ...
                                                'Style', 'edit', 'String', '1');
            asm_boxes.butts.butt_dilate = uicontrol('Parent', asm_boxes.butt_d_boxes, ...
                                                'Style', 'pushbutton', ...
                                                'String', 'Dilate Steps', ...
                                                'Callback', @(src, evt)callbackButtDilate(obj));
            asm_boxes.butts.butt_quit = uicontrol('Parent', asm_boxes.butt_d_boxes, ...
                                                'Style', 'pushbutton', ...
                                                'String', 'Quit', ...
                                                'Callback', @(src, evt)callbackButtQuit(obj));                           

            obj.conf.assemble_boxes = asm_boxes;
        end

        function resetUiComponents(obj)
            params = fieldnames(obj.conf.assemble_boxes.params);
            for ii = 1:length(params)
                param = params{ii};
                value = obj.toString(obj.assembler.localPar.(param));
                set(obj.conf.assemble_boxes.params.(param).edit, 'String', value);
            end
        end

        function doUpdateDisplay(obj)
        end

        function addUICallbacks(obj)
        end

        function conf = collectConf(obj)
            conf = {};
            params = fieldnames(obj.conf.assemble_boxes.params);
            for ii = 1:length(params)
                param = params{ii};
                value = get(obj.conf.assemble_boxes.params.(param).edit, 'String');
                conf(end+1:end+2) = {param, obj.fromString(param, value)};
            end
        end

        function asm_boxes = createConfChoices(obj, asm_boxes)
            params = fieldnames(obj.defaultConf);
            for ii = 1:length(params)
                param = params{ii};
                if (ismember(param, {'autosave', 'cache', 'update_status_cb', ...
                                     'add_to_filename', 'list', 'merge_angle'}))
                    continue;
                end
                asm_boxes.params.(param) = [];
                asm_boxes.params.(param).boxes = uiextras.HBox('Parent', asm_boxes.param_boxes);
                asm_boxes.params.(param).label = uicontrol('Parent', asm_boxes.params.(param).boxes, ...
                                                        'Style', 'text', ...
                                                        'String', param, ...
                                                        'HorizontalAlignment', 'left');
                asm_boxes.params.(param).edit = uicontrol('Parent', asm_boxes.params.(param).boxes, ...
                                                        'Style', 'edit');
                set(asm_boxes.params.(param).boxes, 'Sizes', [-1 60]);
            end

            sizesVec = ones(1, length(asm_boxes.param_boxes.Children)) * 30;
            set(asm_boxes.param_boxes, 'Sizes', sizesVec);
        end

        function value = fromString(~, param, value)
            switch (param)
                case {'convention', 'overlaps'}
                otherwise
                    value = eval(value);
            end
        end
    end

    methods (Access = public)
        function callbackButtSinglePhase(obj)
            conf = obj.collectConf();

            obj.assembler.changeParams(conf);

            value = get(obj.conf.assemble_boxes.butts.edit_phase, 'String');
            value = str2double(value);
            obj.assembler.assemblePhase(value);

            obj.assembleStep = 'phase';
            if (obj.assembler.localPar.autoload)
                obj.autoLoad();
            end
        end

        function callbackButtAllPhases(obj)
            conf = obj.collectConf();

            obj.assembler.changeParams(conf);

            obj.assembler.assembleAllPhases();

            obj.assembleStep = 'phase';
            if (obj.assembler.localPar.autoload)
                obj.autoLoad();
            end
        end

        function callbackButtVolume(obj)
            conf = obj.collectConf();

            obj.assembler.changeParams(conf);

            obj.assembler.assembleCompleteVolume();

            obj.assembleStep = 'complete';
            if (obj.assembler.localPar.autoload)
                obj.autoLoad();
            end
        end

        function callbackButtDilate(obj)
            conf = obj.collectConf();

            obj.assembler.changeParams(conf);

            value = get(obj.conf.assemble_boxes.butts.edit_dilate, 'String');
            value = str2double(value);
            obj.assembler.dilateCompleteVolume(value);

            obj.assembleStep = 'dilated';
            if (obj.assembler.localPar.autoload)
                obj.autoLoad();
            end
        end

        function callbackButtReset(obj)
            obj.resetUiComponents();
        end

        function callbackButtQuit(obj)
            if (obj.hasGMgui)
                obj.conf.GMgui.assembler_params = obj.assembler.localPar;
            end
            obj.delete();
        end

        function assembleStep = getAssembleStep(obj)
            assembleStep = obj.assembleStep;
        end

        function autoLoad(obj)
            if (obj.hasGMgui)
                obj.conf.GMgui.assembler_params = obj.assembler.localPar;
                obj.conf.GMgui.setVolumeToLoad(obj.assembleStep);
                if isfield(obj.conf.GMgui, 'extras')
                    obj.conf.GMgui.extras.twinInfo = obj.assembler.twinInfo;
                end
                drawnow('expose')
            end
        end

    end % end of public methods

end % end class
