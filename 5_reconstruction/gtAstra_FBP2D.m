function [absvol, res_norm] = gtAstra_FBP2D(parameters, proj)
% GTASTRA_FBP2D
%     absvol = gtAstra_FBP2D(parameters, proj)
%     -------------------------------------
%
%
%     Version 002 26-09-2012 by LNervo
%       Formatting, cleaning, commenting

    if (~exist('proj','var'))
        name = fullfile(parameters.acq.dir,'absorption_stack.mat');
        if exist(name,'file')
            check = inputwdefault('Absorption stack already exists - do you want to (re)-create it? [y/n]', 'n');
            if strcmpi(check,'n')
                disp('reading projections...');
                proj = load(name);
            else
                proj = gtAstraPrepareAbsorptionStack(parameters);
            end

        else
            proj = gtAstraPrepareAbsorptionStack(parameters);
        end
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % now start FBP reconstruction of absorption volume
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    %proj_data = permute(proj.stack,[3 2 1]); % transforms stack of projection images to stack of sinograms
    proj_data = proj.stack;

    vsize = size(proj.stack, 3);  % numer of slices to be reconstructed
    hsize = size(proj.stack, 1);  % number of pixels in projection image
    nproj = size(proj.stack, 2);  % number of projections

    sino = zeros(nproj, hsize);

    xvol = hsize;
    yvol = hsize;
    zvol = vsize;

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % projection geometry (type, #rows, #columns, vectors)
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % deal with the sense of the rotation,
    if all(round(parameters.labgeo.rotdir) == [0 0 1])
        % rotation axis is anti-clockwise: -Omega_rad will be used for
        % fbp - astra_create_proj_geom, which does not know about rotdir...
        Omega_fbp = -proj.Omega;
    else
        Omega_fbp = proj.Omega;
    end

    proj_geom = astra_create_proj_geom('parallel', 1.0, hsize, Omega_fbp);
    omegas = abs(proj.Omega) / pi * 180;
    if isfield(parameters.acq, 'correct_sample_shifts')
        if parameters.acq.correct_sample_shifts
            [shifts_lab, shifts_sam] = gtMatchGetSampleShifts(parameters, omegas);
        else
            shifts_sam = zeros(numel(omegas), 3);
            shifts_lab = zeros(numel(omegas), 3);
        end
    else
        shifts_sam = zeros(numel(omegas), 3);
        shifts_lab = zeros(numel(omegas), 3);
    end
    proj_geom.option.ExtraDetectorOffset = shifts_lab(:, 2)' ./ parameters.detgeo(1).pixelsizeu;
    if isfield(proj, 'extra_detector_offset')
        disp('shifting projections');
        proj_geom.option.ExtraDetectorOffset = proj_geom.option.ExtraDetectorOffset + ones(1, nproj) * proj.extra_detector_offset;
    end
    %figure; scatter(omegas, proj_geom.option.ExtraDetectorOffset)
    vol_geom = astra_create_vol_geom(hsize, hsize);
    proj_id = astra_create_projector('linear', proj_geom, vol_geom);
    sinogram_id = astra_mex_data2d('create', '-sino', proj_geom, 0);

    % allocate space to store reconstruction image
    recon_id = astra_mex_data2d('create', '-vol', vol_geom, 0);

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % create FBP algorithm configuration
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    cfg = astra_struct('FBP_CUDA');
    cfg.ProjectionDataId = sinogram_id;
    cfg.ReconstructionDataId = recon_id;
    cfg.FilterType = 'Ram-Lak';
    cfg.option.GPUindex = 0;

    absvol = zeros(xvol, yvol, zvol, 'single');

    for ii = 1:vsize
        % Not sure if we really need to re-create the algorithm each time..
        alg_id = astra_mex_algorithm('create', cfg);

        % 'zeropadding' projections is already dealt with in
        % gtAstraPrepareAbsorptionStack
        %sino = gtPlaceSubImage(squeeze(proj_data(:, :, ii))', sino, offset, 1);
        sino = proj_data(:, :, ii)';
        %sino(:, 1:offset-1) = repmat(sino(:, offset), 1, offset-1);
        %sino(:, offset+hsize:end) = repmat(sino(:, offset+hsize-1), 1, hsize_zp-offset-hsize+1);

        astra_mex_data2d('set', sinogram_id, sino)

        astra_mex_algorithm('run', alg_id);

        % retrieve reconstruction from ASTRA lib
        absvol(:, :, vsize-ii+1) = astra_mex_data2d('get', recon_id);

        astra_mex_algorithm('delete', alg_id);
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % clean up
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    astra_mex_data2d('delete', sinogram_id);
    astra_mex_data2d('delete', recon_id);

    astra_mex_projector('delete', proj_id);

    res_norm = 1;

    if (isfield(proj, 'padding'))
        absvol = absvol( ...
            proj.padding+1:end-proj.padding, ...
            proj.padding+1:end-proj.padding, ...
            : );
    end
end
