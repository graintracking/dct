classdef GtGuiThresholdVolume < GtGuiThresholdGrain
    properties
    end

    methods (Access = public)
        function obj = GtGuiThresholdVolume(vol, varargin)
            obj = obj@GtGuiThresholdGrain(vol, varargin{:});

            obj.setSegmentFunction(@(thr, morph, border)segmentVolume(obj, thr, morph, border));
        end
    end

    methods (Access = protected)
        function segmentVolume(obj, threshold, morpho_seed, mask_border)
            if (exist('morpho_seed', 'var'))
                morpho_seed = [];
            end
            if (exist('mask_border', 'var'))
                mask_border = [];
            end
            filter = {'*.mat', 'MATLAB Files (*.mat)';
                      '*.edf', 'EDF files (ESRF Data Format, *.edf)'};
            [filename, pathname, filerIndex] = uiputfile( ...
                filter, 'Select file...', ...
                fullfile('5_reconstruction', 'volume_mask.mat'));

            if (~isempty(filename) && ~isequal(filename, 0))
                thr = GtThreshold();
                seg_struct = thr.volumeThreshold(obj.conf.vol, threshold, morpho_seed, false, mask_border);
                seg_struct.vol = zeros(size(obj.conf.vol), 'like', seg_struct.seg);
                seg_struct.vol = gtPlaceSubVolume(seg_struct.vol, seg_struct.seg, seg_struct.segbb(1:3));

                if (filerIndex == 1)
                    save(fullfile(pathname, filename), '-struct', 'seg_struct', '-v7.3');
                else
                    edf_write(seg_struct.vol, fullfile(pathname, filename));
                end
            end
        end
    end

    methods (Static)
        function handles = compareVolumes(vols, varargin)
            handles = GtGuiThresholdVolume.classCompareVolumes(vols, varargin{:});
        end
    end
end
