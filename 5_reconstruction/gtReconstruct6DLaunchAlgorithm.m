function algo = gtReconstruct6DLaunchAlgorithm(sampler, rec_opts, parameters, varargin)
% FUNCTION algo = gtReconstruct6DLaunchAlgorithm(sampler, rec_opts, parameters, varargin)
%

    conf = struct('det_index', 1, 'blobs', []);
    conf = parse_pv_pairs(conf, varargin);

    rec_factory = Gt6DReconstructionAlgorithmFactory(parameters, ...
        'volume_downscaling', rec_opts.volume_downscaling, ...
        'rspace_super_sampling', rec_opts.rspace_super_sampling, ...
        'rspace_oversize', rec_opts.rspace_oversize, ...
        'use_predicted_scatter_ints', rec_opts.use_predicted_scatter_ints, ...
        'use_matrix_row_rescaling', rec_opts.use_matrix_row_rescaling, ...
        'shape_functions_type', rec_opts.shape_functions_type, ...
        'det_index', conf.det_index );

    if (isempty(conf.blobs))
        algo = rec_factory.get6DAlgorithmSingleRegion(sampler, rec_opts.num_interp);
    else
        algo = rec_factory.get6DAlgorithmMultiRegion(sampler, rec_opts.num_interp, conf.blobs);
    end

    % Adding extra parameters
    algo.detector_norm = rec_opts.detector_norm;
    algo.tv_norm = rec_opts.tv_norm;
    algo.tv_strategy = rec_opts.tv_strategy;

    algo.lambda_l1 = rec_opts.lambda_l1;
    algo.lambda_tv = rec_opts.lambda_tv;

    % Convert the noise variance of the blobs into detector weights.
    if (isfield(rec_opts, 'detector_weights') && ...
            isfield(rec_opts.detector_weights, 'type') && ~isempty(rec_opts.detector_weights.type))
        algo.calcBlobWeights(rec_opts.detector_weights.type, rec_opts.detector_weights.coefficient);
    else
        fprintf('No cost function weight is remarked in parameters.rec.grains.options.detector_weights.\n')
    end

    geom_memory = sum(arrayfun(@(x)x.get_memory_consumption_geometry(), sampler));
    grain_memory = sum(arrayfun(@(x)x.get_memory_consumption_graindata(), sampler));

    mem_cons = [ ...
        (algo.get_peak_memory_consumption(rec_opts.algorithm) + geom_memory + grain_memory), ...
        algo.get_memory_consumption_volumes(), ...
        algo.get_memory_consumption_blobs(), ...
        algo.get_memory_consumption_sinograms(), ...
        geom_memory, ...
        grain_memory ];
    fprintf(['Estimated memory consumption: %d MB (Volumes %d MB, Blobs %d MB, ' ...
        'Sinograms %d MB, Geometry %d MB, Grain data %d MB)\n'], ...
        round(mem_cons / (2^20)))

    algo.cp(upper(rec_opts.algorithm), rec_opts.num_iter);
end
