classdef GtGuiReconstructAndSegment < GtBaseGuiElem
    properties
        thresh;

        defaults;
    end
    methods (Access = public)
        function obj = GtGuiReconstructAndSegment(varargin)
            obj = obj@GtBaseGuiElem();

            obj.thresh = GtThreshold();
            obj.defaults = obj.thresh.generateDefaults();

            obj.conf.f_title = 'Reconstruct & Segment GUI';
            obj.initGtBaseGuiElem(varargin);
        end
    end

    methods (Access = protected)
        function initGui(obj)
            initGui@GtBaseGuiElem(obj)

            asm_boxes = [];
            asm_boxes.main = uiextras.VBox('Parent', obj.conf.currentParent);
            asm_boxes.param_boxes = uiextras.VBox('Parent', asm_boxes.main);
            asm_boxes.butt_a_boxes = uiextras.HButtonBox('Parent', asm_boxes.main);
            asm_boxes.butt_d_boxes = uiextras.HButtonBox('Parent', asm_boxes.main);

            set(asm_boxes.main, 'Sizes', [-1 40 40]);

            buttonSize = get(asm_boxes.butt_a_boxes, 'ButtonSize');
            buttonSize(1) = 140;
            set(asm_boxes.butt_a_boxes, 'ButtonSize', buttonSize);
            set(asm_boxes.butt_d_boxes, 'ButtonSize', buttonSize);

            % Conf choice
            asm_boxes = obj.createConfChoices(asm_boxes);

            % Buttons
            asm_boxes.butts = [];
            asm_boxes.butts.butt_rec = uicontrol('Parent', asm_boxes.butt_a_boxes, ...
                                                'Style', 'pushbutton', ...
                                                'String', 'Reconstruct', ...
                                                'Callback', @(src, evt)cbButtReconstruct(obj));
            asm_boxes.butts.butt_thr = uicontrol('Parent', asm_boxes.butt_a_boxes, ...
                                                'Style', 'pushbutton', ...
                                                'String', 'Threshold', ...
                                                'Callback', @(src, evt)cbButtSegment(obj));
            asm_boxes.butts.butt_reset = uicontrol('Parent', asm_boxes.butt_a_boxes, ...
                                                'Style', 'pushbutton', ...
                                                'String', 'Reset', ...
                                                'Callback', @(src, evt)cbButtReset(obj));

            obj.conf.assemble_boxes = asm_boxes;
        end

        function resetUiComponents(obj)
        end

        function doUpdateDisplay(obj)
        end

        function addUICallbacks(obj)
        end

        function asm_boxes = createConfChoices(obj, asm_boxes)
            asm_boxes.params.('range') = [];
            asm_boxes.params.('range').boxes = uiextras.HBox('Parent', asm_boxes.param_boxes);
            asm_boxes.params.('range').label = uicontrol('Parent', asm_boxes.params.('range').boxes, ...
                                                    'Style', 'text', ...
                                                    'String', 'Range:', ...
                                                    'HorizontalAlignment', 'left');
            asm_boxes.params.('range').label_from = uicontrol('Parent', asm_boxes.params.('range').boxes, ...
                                                    'Style', 'text', ...
                                                    'String', 'from ', ...
                                                    'HorizontalAlignment', 'right');
            asm_boxes.params.('range').edit_from = uicontrol('Parent', asm_boxes.params.('range').boxes, ...
                                                    'Style', 'edit', ...
                                                    'String', obj.defaults.phases{1}.min_id);
            asm_boxes.params.('range').label_to = uicontrol('Parent', asm_boxes.params.('range').boxes, ...
                                                    'Style', 'text', ...
                                                    'String', 'to ', ...
                                                    'HorizontalAlignment', 'right');
            asm_boxes.params.('range').edit_to = uicontrol('Parent', asm_boxes.params.('range').boxes, ...
                                                    'Style', 'edit', ...
                                                    'String', obj.defaults.phases{1}.max_id);
            set(asm_boxes.params.('range').boxes, 'Sizes', [-1 40 60 40 60]);

            asm_boxes.params.('phase') = [];
            asm_boxes.params.('phase').boxes = uiextras.HBox('Parent', asm_boxes.param_boxes);
            asm_boxes.params.('phase').label = uicontrol('Parent', asm_boxes.params.('phase').boxes, ...
                                                    'Style', 'text', ...
                                                    'String', 'Phase:', ...
                                                    'HorizontalAlignment', 'left');
            asm_boxes.params.('phase').edit = uicontrol('Parent', asm_boxes.params.('phase').boxes, ...
                                                    'Style', 'edit', ...
                                                    'String', '1', ...
                                                    'Callback', @(src, evt)cbChangePhaseID(obj));
            set(asm_boxes.params.('phase').boxes, 'Sizes', [-1 60]);

            asm_boxes.params.('percent') = [];
            asm_boxes.params.('percent').boxes = uiextras.HBox('Parent', asm_boxes.param_boxes);
            asm_boxes.params.('percent').label = uicontrol('Parent', asm_boxes.params.('percent').boxes, ...
                                                    'Style', 'text', ...
                                                    'String', 'Percentile:', ...
                                                    'HorizontalAlignment', 'left');
            asm_boxes.params.('percent').edit = uicontrol('Parent', asm_boxes.params.('percent').boxes, ...
                                                    'Style', 'edit', ...
                                                    'String', obj.toString(obj.defaults.percentile));
            set(asm_boxes.params.('percent').boxes, 'Sizes', [-1 60]);

            sizesVec = ones(1, length(asm_boxes.param_boxes.Children)) * 30;
            set(asm_boxes.param_boxes, 'Sizes', sizesVec);
        end

        function value = fromString(~, value)
            if (isempty(value))
                error('REC_THR_GUI:wrong_argument', ...
                      'You didn''t specify a value for the field');
            end
            value = str2double(value);
        end

        function info = collectInfo(obj)
            info = [];
            tempStr = get(obj.conf.assemble_boxes.params.('phase').edit, 'String');
            info.phaseID = obj.fromString(tempStr);

            tempStr = get(obj.conf.assemble_boxes.params.('range').edit_from, 'String');
            info.fromID = obj.fromString(tempStr);
            tempStr = get(obj.conf.assemble_boxes.params.('range').edit_to, 'String');
            info.toID = obj.fromString(tempStr);

            tempStr = get(obj.conf.assemble_boxes.params.('percent').edit, 'String');
            info.percentile = obj.fromString(tempStr);
        end
    end

    methods (Access = public)
        function cbChangePhaseID(obj)
            try
                phaseID = get(obj.conf.assemble_boxes.params.('phase').edit, 'String');
                phaseID = obj.fromString(phaseID);
                if (phaseID > length(obj.defaults.phases))
                    error('REC_THR_GUI:wrong_argument', ...
                          'Wrong phase ID %d, max number of phases: %d', ...
                          phaseID, length(obj.defaults.phases))
                end

                set(obj.conf.assemble_boxes.params.('range').edit_from, 'String', ...
                    obj.toString(obj.defaults.phases{phaseID}.min_id));
                set(obj.conf.assemble_boxes.params.('range').edit_to, 'String', ...
                    obj.toString(obj.defaults.phases{phaseID}.max_id));
            catch mexc
                errordlg(mexc.message, 'Wrong phase ID');
            end
        end

        function cbButtReset(obj)
            set(obj.conf.assemble_boxes.params.('phase').edit, 'String', '1');
            obj.cbChangePhaseID();

            set(obj.conf.assemble_boxes.params.('percent').edit, 'String', ...
                obj.toString(obj.defaults.percentile));
        end

        function cbButtReconstruct(obj)
            info = obj.collectInfo();
            gtReconstructGrains(info.fromID, info.toID, pwd, info.phaseID);
        end

        function cbButtSegment(obj)
            info = obj.collectInfo();
            obj.thresh.batchThreshold(info.phaseID, info.fromID, info.toID, info.percentile);

            obj.defaults = obj.thresh.generateDefaults();
        end
    end
end
