function gtSetupReconstruction(phase_id, varargin)
% GTSETUPRECONSTRUCTION  Simple helper function to launch reconstruction and segmentation of
%                        absorption volume - as well as reconstruction and segmentation of grain volumes
%
%     gtSetupReconstruction(phaseID, varargin)
%     ------------------------------
%
%     INPUT:
%       phaseID = number of the phase <double> {1}
%
%     OPTIONAL INPUT:
%       grain_ids    = vector of grain_ids, default value is {[1: num_grains]}
%       cluster_ids  = vector of cluster_ids, default value is {[1: num_clusters]}
%       'is_cluster
%     Version 005 12-10-2016 by W. Ludwig
%       Add possibility to launch grain-cluster reconstructions
%     Version 004 25-05-2016 by W. Ludwig
%       Add possibility to specify a list of grainids - which will be
%       handled by OAR
%     Version 003 29-05-2012 by LNervo
%       Add comments in header about functions used
%       Add possibility to recompile 'gtChangeThreshold3D'
%
%     Version 002 25-05-2012 by LNervo
%       Commenting and formatting: fprintf -> disp; y/n -> [y/n];
%
%     Version 001 05-03-2013 by Aking
%       Move gtModifyStructure to before the abs reconstruction is done.

GPU_OK = gtCheckGpu();

conf = struct('grain_ids', [], 'cluster_ids', [], 'det_ind', 1);
conf = parse_pv_pairs(conf, varargin);

if (~GPU_OK)
    disp('Not on a GPU machine, you won''t  be able to reconstruct localy!');
    disp('from rnice: oarsub -I -l "walltime=hh:mm:ss" -q gpu');
    fprintf('\nSubmitting to OAR.\n')
end

if (~exist('phase_id','var') || isempty(phase_id))
    phase_id = 1;
end

check = inputwdefault('Do you want to recompile the function ''gtChangeThreshold3D''? [y/n]', 'n');
if (strcmpi(check,'y'))
    gtExternalCompileFunctions('gtChangeThreshold3D');
end
check = inputwdefault('Do you want to recompile the function ''gtReconstructGrains''? [y/n]', 'n');
if (strcmpi(check,'y'))
    gtExternalCompileFunctions('gtReconstructGrains');
end

parameters = gtLoadParameters();

% Let's check every field is in, and in case let's update it!
parameters = gtCheckParameters(parameters, 'rec', 'verbose', true);
if (~isfield(parameters, 'detgeo') || isempty(parameters.detgeo))
    parameters.detgeo = gtGeoConvertLegacyLabgeo2Detgeo(parameters.labgeo);
end
if (~isfield(parameters.labgeo, 'omstep') || isempty(parameters.labgeo.omstep))
    parameters.labgeo.omstep = gtAcqGetOmegaStep(parameters);
end
gtSaveParameters(parameters);

nof_phases = numel(parameters.cryst);

if (nof_phases > 1) && isempty(phase_id)
    phase_id = inputwdefaultnumeric('Enter phaseID', num2str(1));
end
phaseID_str = sprintf('%02d', phase_id);

disp(['Setting up Reconstruction for phase ' phaseID_str ' (' parameters.cryst(phase_id).name ')']);

samplefile = fullfile(parameters.acq(1).dir, '4_grains', 'sample.mat');
if exist(samplefile, 'file')
   sample = GtSample.loadFromFile(samplefile);
else
   disp('Can not find 4_grains/sample.mat - have you run gtSetupForwardSimulation?');
   return;
end

% Get reconstruction parameters
% !!! to be fixed
%check = inputwdefault('Use existing parameters for grain reconstruction? [y/n]', 'y');
%if ~strcmpi(check, 'y')
%    list = build_list_v2();
%    parameters.rec = gtModifyStructure(parameters.rec, list.rec, 1, 'Reconstruction parameters:');
%    save(parameters_savefile,'parameters');
%end

% add the possibility to redo abs reconstruction, even if existing
absorption_volume = fullfile(parameters.acq(1).dir, '5_reconstruction', 'volume_absorption.mat');
if (~exist(absorption_volume,'file'))
    check = inputwdefault([ ...
        'Can not find absorption volume. Would you like to launch ' ...
        'reconstruction of absorption volume? [y/n]'] , 'y');
    if (strcmpi(check, 'y'))
        abs_struct = gtAstraAbsorptionVolume(parameters, [], 'save', true);
        sample.absVolFile = fullfile('5_reconstruction', 'volume_absorption.mat');
        sample.saveToFile();
    end
else
    check = inputwdefault('Absorption volume found - would you like to overwrite it? [y/n]', 'n');
    if (strcmpi(check, 'y'))
        abs_struct = gtAstraAbsorptionVolume(parameters, [], 'save', true);
        sample.absVolFile = fullfile('5_reconstruction', 'volume_absorption.mat');
        sample.saveToFile();
    end
end

% add the possibility to redo volume threshold, even if existing
absorption_mask = fullfile(parameters.acq(1).dir, '5_reconstruction', 'volume_mask.mat');
if (~exist(absorption_mask, 'file'))
    check = inputwdefault('Can not find absorption mask. Would you like to segment absorption volume? [y/n]', 'y');
    if (strcmpi(check, 'y'))
        if (~exist('abs_struct', 'var'))
            abs_struct = load(absorption_volume);
        end
        disp([ 'Please create file volume_mask.mat from absorption volume ' ...
            '(right click in the volume to select a seed point and then press ''Segment'')' ]);
        GtGuiThresholdVolume(abs_struct.abs);
    end
else
    check = inputwdefault('Absorption mask found - would you like to overwrite it? [y/n]', 'n');
    if (strcmpi(check, 'y'))
        if (~exist('abs_struct', 'var'))
            abs_struct = load(absorption_volume);
        end
        disp('right click in the volume to select a seed point and then press ''Segment''');
        GtGuiThresholdVolume(abs_struct.abs);
    end
end

% If input parameters grain_ids and cluster_ids are both empty, we will
% reconstruct all grains by default
first = 1;
last  = sample.phases{phase_id}.getNumberOfGrains();

if (isempty(conf.grain_ids) && isempty(conf.cluster_ids))
    conf.grain_ids = first:last;
end

if (~isempty(conf.grain_ids))
    check = inputwdefault('Launch grain reconstruction? [y/n]', 'y');
    rec_on_OAR = false;
    if  strcmpi(check, 'y')
        if (GPU_OK)
            check2 = inputwdefault('Launch grain reconstruction via OAR? [y/n]', 'y');
            rec_on_OAR = strcmpi(check2, 'y');
        else
            rec_on_OAR = true;
        end
        if (rec_on_OAR)
            launch_on_OAR('gtReconstructGrains', first, last, phaseID_str, ...
                parameters, 'otherparameters', sprintf('det_ind %d', conf.det_ind), 'njobs', 8, 'walltime', 3600 * 16, ...
                'gpu', true, 'distribute', true, 'list', conf.grain_ids);
        else
            gtReconstructGrains(first, last, parameters.acq(conf.det_ind).dir, phase_id, ...
                parameters, 'det_ind', conf.det_ind, 'list', conf.grain_ids);
        end
    end

    if (~rec_on_OAR)
        check = inputwdefault('Launch grain segmentation? [y/n]', 'y');
        if  strcmpi(check, 'y')
            check = inputwdefault('Use existing parameters for grain segmentation? [y/n]', 'y');
            if ~strcmpi(check, 'y')
                info_list = build_list_v2();
                parameters.rec = gtModifyStructure(parameters.rec, ...
                    info_list.rec, 2, 'Grain segmentation parameters:');
                gtSaveParameters(parameters);
            end

            check = inputwdefault('Launch grain segmentation via OAR? [y/n]', 'y');
            if (strcmpi(check, 'y'))
                launch_on_OAR('gtChangeThreshold3D', first, last, ...
                    phaseID_str, parameters, 'core', 4, 'distribute', true, 'list', conf.grain_ids);
            else
                gtChangeThreshold3D(first, last, [], phase_id, [], 'list', conf.grain_ids);
            end
        end
    end
end

if (~isempty(conf.cluster_ids))
    check = inputwdefault('Launch cluster reconstruction? [y/n]', 'y');
    if  strcmpi(check, 'y')
        if (GPU_OK)
            check2 = inputwdefault('Launch grain reconstruction via OAR? [y/n]', 'y');
            rec_on_OAR = strcmpi(check2, 'y');
        else
            rec_on_OAR = true;
        end
        if (rec_on_OAR)
            launch_on_OAR('gtReconstructClusters', first, last, phaseID_str, ...
                parameters, 'njobs', 8, 'walltime', 3600 * 16, ...
                'gpu', true, 'distribute', true, 'list', conf.cluster_ids);
        else
            gtReconstructClusters(first, last, parameters.acq.dir, ...
                phase_id, parameters, 'list', conf.cluster_ids);
        end
    end
end

end

function launch_on_OAR(func_name, first, last, phaseID_str, parameters, varargin)
    disp('Launching several separate OAR jobs');

    oar.njobs      = max(ceil(last/30), 30); % max 30 jobs
    oar.walltime   = last / oar.njobs * 300; % allow up to 300 seconds per grain
    oar.mem        = 2048; % Mb
    oar.gpu        = true;
    oar.core       = [];
    oar.distribute = true;
    oar.list       = [];
    oar.otherparameters = [];
    oar = parse_pv_pairs(oar, varargin);

    % oar parameters are defined in build_list_v2
    list = build_list_v2();
    OAR_parameters = gtModifyStructure(oar, list.oar, 1, 'OAR parameters:');

    OARtmp = ceil(OAR_parameters.walltime / 60); % minutes
    OARm   = mod(OARtmp, 60); % minutes
    OARh   = (OARtmp - OARm) / 60; % hours

    OAR_parameters.walltime = sprintf('%02d:%02d:00', OARh, OARm);

    disp('Using OAR parameters:');
    disp(OAR_parameters);
    disp(['Using ' num2str(OAR_parameters.njobs) ' OAR jobs']);

    pause(0.5);
    % With cpu = [] an core = [] we hope to force the reservation of full
    % single nodes
    gtOarLaunch(func_name, first, last, OAR_parameters.njobs, ...
        sprintf('%s %s '''' %s ', parameters.acq(1).dir, phaseID_str, OAR_parameters.otherparameters), true, ...
        'walltime', OAR_parameters.walltime, 'mem', OAR_parameters.mem, ...
        'cpu', [], 'core', oar.core, 'gpu', OAR_parameters.gpu, ...
        'distribute', oar.distribute, 'list', oar.list);
end

