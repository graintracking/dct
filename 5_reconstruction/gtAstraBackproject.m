function vol = gtAstraBackproject(proj_stack, geometry, vol_size, oversampling)

    if (~exist('oversampling', 'var') || isempty(oversampling))
        oversampling = 1;
    end

    usize = size(proj_stack, 1);
    vsize = size(proj_stack, 3);

    % projection geometry (type, #rows, #columns, vectors)
    proj_geom = astra_create_proj_geom('parallel3d_vec', vsize, usize, geometry);

    % volume geometry (x, y, z)
    vol_geom = astra_create_vol_geom(vol_size(2), vol_size(1), vol_size(3));

    % Volume Downscaling option and similar
    opts = struct( ...
        'VoxelSuperSampling', oversampling, ...
        'DetectorSuperSampling', 1, ...
        'GPUindex', -1 );

    astra_projector_id = astra_create_projector('cuda3d', proj_geom, vol_geom, opts);

    vol = astra_mex_direct('BP3D', astra_projector_id, proj_stack);

    astra_mex_projector('delete', astra_projector_id);
end
