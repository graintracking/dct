classdef GtAssembleVol3D < handle
% GtAssembleVol3D  A class to assemble volumes
%
% Can be initialized as:
% assem = GtAssembleVol3D(varargin);
%
%   OPTIONAL INPUT (parse_pv_pairs):
%     add_to_filename        =                 .edf volume containing a set of existing grains
%     display_error_messages = <logical>       show error messages (Used?)
%     cache                  = <GtSimpleCache> object, in case you have a filled one, 
%                                              so you can avoid reloading things from
%                                              disk
%     update_status_cb       = <Callback>      update the status of the computation 
%                                              (used for instance in a gui). The
%                                              said callback is executed every time the loop
%                                              terminates the processing of an entry
%     autosave               = <logical>       Saves automatically after assembling
%     overlaps               = <string>        How to handle overlapping grains:
%                                              ('zero' | 'summed' | {'conflict'} | 'adaptive' | 'assign', 'parent')
%     list                   = <double>        if list is 'all', or [], load all grains.  Otherwise, only the list
%     deal_with_twins        = <logical>       Handles twins  true | {false}
%     use_parent_mask        = <logical>       Use parent mask {false}
%     convention             = <string>        Hexagonal axes convention {'X'} | 'Y'
%     merge_angle            = <double>        Merge angle in degrees
%
%   Version 004 10-05-2014 by LNervo
%     Added static method to retrieve assemble output
%     i.e. foutput = GtAssembleVol3D.loadPhaseTwinOutput(workingdir, phaseID)
%
%   Version 003 18-04-2014 by LNervo
%     Added parent mask for twins
%
%   Version 002 09-10-2013 by LNervo
%     Implemented twin detection for hcp materials; added 'convention' parameter
%     to switch to available conventions for crystal reference system for hcp
%     materials

    properties
        parameters = [];
        localPar   = [];

        volumes = [];

        redo = []; % Keeps track of the grains that had an error

        twinInfo       = {};    % In case of twins
        sigmas         = {};    % In case of twins

        symm           = {};
        spacegroup     = {};
        crystal_system = {};

        conflictStrategies = {'summed', 'zero', 'conflict', 'adaptive', 'assign', 'parent'};
    end

    methods (Access = public)
        function obj = GtAssembleVol3D(varargin)
        % GTASSEMBLEVOL3D/GTASSEMBLEVOL3D  Constructor

            % Load Parameters
            obj.parameters = gtLoadParameters();
            
            for ii = 1:obj.parameters.acq(1).nof_phases
                obj.crystal_system{ii} = obj.parameters.cryst(ii).crystal_system;
                obj.spacegroup{ii} = obj.parameters.cryst(ii).spacegroup;
                obj.symm{ii} = gtCrystGetSymmetryOperators(obj.crystal_system{ii}, obj.spacegroup{ii});
                if (isfield(obj.parameters.cryst(ii), 'sigmas'))
                    if (~isfield(obj.parameters.cryst(ii), 'active_sigmas'))
                        obj.parameters.cryst(ii).active_sigmas = true(size(obj.parameters.cryst(ii).sigmas, 1), 1);
                    end
                    obj.sigmas{ii} = obj.parameters.cryst(ii).sigmas(obj.parameters.cryst(ii).active_sigmas);
                end
            end

            % Coping with older parameters
            def_rec = gtRecDefaultParameters();
            def_thr = def_rec.thresholding;
            local_thr(1, :) = fieldnames(obj.parameters.rec.thresholding);
            local_thr(2, :) = struct2cell(obj.parameters.rec.thresholding);
            [def_thr, ~] = parse_pv_pairs(def_thr, local_thr(:));
            obj.parameters.rec.thresholding = def_thr;

            % Array of all the partial phase volumes
            obj.volumes.phases = {};
            % Complete volume: merge of all phases has two fields which really
            % hold the data:
            % - 'phases' which contains the information about the phaseIDs in
            %   all the voxels
            % - 'grains' which contains the information about the grainIDs in
            %   all the voxels
            obj.volumes.complete = [];
            obj.volumes.complete.phases = [];
            obj.volumes.complete.grains = [];

            % handle variables
            obj.localPar = GtAssembleVol3D.generateDefaults();

            obj.changeParams(varargin);
        end

        function rejected = changeParams(obj, arguments)
        % GTASSEMBLEVOL3D/CHANGEPARAMS Changes the local parameters, validates
        % them, and prepares for the correct usage of them

            if (nargout > 0)
                [obj.localPar, rejected] = parse_pv_pairs(obj.localPar, arguments);
            else
                obj.localPar = parse_pv_pairs(obj.localPar, arguments);
            end

            % not that "zeros" is ambiguous in Matlab because it is a function,
            % and in this context we normally use it as a string.  Use "zero"
            % instead, small bodge here in case this is not done.
            if ((~ischar(obj.localPar.overlaps) && (obj.localPar.overlaps == 0)) ...
                    || (ischar(obj.localPar.overlaps) && strcmpi(obj.localPar.overlaps, 'zeros')) )
                obj.localPar.overlaps = 'zero';
                disp('guessing that overlaps should be set to zero')
            end

            if (~ischar(obj.localPar.overlaps) ...
                    || ~ismember(obj.localPar.overlaps, obj.conflictStrategies))
                strats = obj.conflictStrategies;
                strats(2, :) = {'" | "'};
                gtError('ASSEMBLE:wrong_parameter', ...
                      'You should specify a valid "overlap" behavior: "%s" is not among ( "%s" )', ...
                      obj.localPar.overlaps, [strats{1:end-1}] )
            end

            % Initializing cache
            if (isempty(obj.localPar.cache))
                obj.localPar.cache = GtSimpleCache();
            end
            if (~obj.localPar.cache.isModel('sample'))
                grainDirsModel = fullfile('4_grains', 'sample.mat');
                obj.localPar.cache.createModel(grainDirsModel, 'sample');
            end
            if (~obj.localPar.cache.isModel('grain'))
                grainDirsModel = fullfile('4_grains', 'phase_%02d', 'grain_%04d.mat');
                obj.localPar.cache.createModel(grainDirsModel, 'grain');
            end
            if (~obj.localPar.cache.isModel('grain_rec'))
                grainDirsModel = fullfile('4_grains', 'phase_%02d', 'grain_details_%04d.mat');
                obj.localPar.cache.createModel(grainDirsModel, 'grain_rec');
            end
            if (~obj.localPar.cache.isModel('grain_ext_rec'))
                grainDirsModel = fullfile('4_grains', 'phase_%02d', 'grain_extended_details_%04d.mat');
                obj.localPar.cache.createModel(grainDirsModel, 'grain_ext_rec');
            end

            if (obj.localPar.deal_with_twins)
                disp('dealing with twins')
            else
                disp('not dealing with twins')
            end
        end

        function assemblePhase(obj, phase_id)
        % GTASSEMBLEVOL3D/ASSEMBLEPHASE

            if (~exist('phase_id', 'var'))
                phase_id = 1;
            end

            [~, grain_ids] = obj.initialiseAssembling(phase_id);

            if (obj.parameters.acq(1).no_direct_beam)
                disp('WARNING! No direct beam in the scan.')
                disp('  Revise sample bounding box before continuing!')
            end

            if (any(obj.parameters.acq(1).bb(3) == [0 1 NaN]) ...
                    || any(obj.parameters.acq(1).bb(4) == [0 1 NaN]))
                disp('WARNING! Invalid value for ''parameters.acq(1).bb'' .')
            end

            assemble_6D = strcmpi(obj.parameters.rec.grains.algorithm(1:2), '6D');

            if (assemble_6D)
                [volume_ids, dmvol, intvol] = obj.doAssemblePhaseVolume(phase_id, grain_ids);
            else
                volume_ids = obj.doAssemblePhaseVolume(phase_id, grain_ids);
            end
            % store volume
            obj.volumes.phases{phase_id}.grains = volume_ids;
            if (assemble_6D)
                obj.volumes.phases{phase_id}.dmvol = dmvol;
                obj.volumes.phases{phase_id}.intvol = intvol;
            end
            obj.volumes.phases{phase_id}.rotation_axes = obj.localPar.rotation_axes;
            obj.volumes.phases{phase_id}.rotation_angles = obj.localPar.rotation_angles;

            if (obj.localPar.autosave)
                obj.savePhaseVolume(phase_id);

                if (obj.localPar.deal_with_twins)
                    obj.savePhaseTwinOutput(phase_id);
                end
            end
        end

        function assembleAllPhases(obj)
        % GTASSEMBLEVOL3D/ASSEMBLEALLPHASES

            % Now let's load metadata
            sampleInfo = obj.getSample();

            % For all phases:
            for phaseID = 1:numel(sampleInfo.phases)
                obj.assemblePhase(phaseID);
            end
        end

        function assembleCompleteVolume(obj)
        % GTASSEMBLEVOL3D/ASSEMBLEFINALVOLUME

            % Let's load metadata
            sampleInfo = obj.getSample();

            % Let's initialize the phase IDs volumes
            volumePIDs = zeros(obj.parameters.acq(1).bb([3, 3, 4]), 'int16');
            % Let's initialize the grains IDs volumes
            volumeGIDs = zeros(obj.parameters.acq(1).bb([3, 3, 4]), 'int16');

            assemble_6D = strcmpi(obj.parameters.rec.grains.algorithm(1:2), '6D');
            if (assemble_6D)
                volumeDM = zeros([obj.parameters.acq(1).bb([3, 3, 4]), 3]);
                volumeINT = zeros(obj.parameters.acq(1).bb([3, 3, 4]));
            end

            phases_active = cellfun(@(x)x.active, sampleInfo.phases);

            phasesNum = length(sampleInfo.phases);
            gauge = GtGauge(phasesNum, 'Processing phases: ');
            for ii = 1:phasesNum
                gauge.incrementAndDisplay();
                if (~phases_active(ii))
                    continue;
                end
                % no real need to cache phase volumes
                phase_file_name = sampleInfo.phases{ii}.volumeFile;
                phase_file_name = regexp(phase_file_name, ':', 'split');
                phase_file = load(phase_file_name{1});
                phaseVol = phase_file.vol;

                assemble_phase_6D = assemble_6D && isfield(phase_file, 'dmvol');
                if (assemble_phase_6D)
                    phaseDmVol = phase_file.dmvol;
                    phaseIntVol = phase_file.intvol;
                end
                grainMap = phaseVol;
                phaseVol = int16(phaseVol ~= 0) * ii;

                maskFile = fullfile('5_reconstruction', ...
                                    sprintf('phase_%02d_mask.mat', ii));
                if (exist(maskFile, 'file'))
                    maskVol = gtMATVolReader([maskFile ':mask']);
                    phaseVol = phaseVol .* int16(maskVol);
                    grainMap = grainMap .* int16(maskVol);
                    if (assemble_phase_6D)
                        phaseDmVol(~maskVol) = 0;
                        phaseIntVol(~maskVol) = 0;
                    end
                end

                switch (obj.localPar.overlaps)
                    case {'zero', 'conflict', 'adaptive', 'parent'}
                        overlappingPoints = ((volumePIDs ~= 0) & (phaseVol ~= 0));
                        volumePIDs = volumePIDs + phaseVol;
                        volumePIDs(overlappingPoints) = -1;

                        volumeGIDs = volumeGIDs + grainMap;
                        volumeGIDs(overlappingPoints) = -1;

                        if (assemble_phase_6D)
                            volumeDM = volumeDM + phaseDmVol;
                            volumeDM(overlappingPoints) = -1;

                            volumeINT = volumeINT + phaseIntVol;
                            volumeINT(overlappingPoints) = -1;
                        end
                    case {'summed'}
                        volumePIDs = volumePIDs + phaseVol;
                        volumeGIDs = volumeGIDs + grainMap;

                        if (assemble_phase_6D)
                            volumeDM = volumeDM + phaseDmVol;
                            volumeINT = volumeINT + phaseIntVol;
                        end
                end
            end
            gauge.delete();

            % Shouldn't really happen, but it happens :)
            if (strcmpi(obj.localPar.overlaps, 'zero'))
                fprintf('Setting overlaps to 0..');
                numBefore = length(find(volumePIDs ~= 0));

                % set overlapping regions to zero - this could be another label colour
                volumePIDs(volumePIDs == -1) = 0;
                volumeGIDs(volumeGIDs == -1) = 0;

                numAfter = length(find(volumePIDs ~= 0));
                fprintf(' Done. (%d -> %d)\n', numBefore, numAfter);
            end

            obj.volumes.complete.phases = volumePIDs;
            obj.volumes.complete.grains = volumeGIDs;
            if (assemble_6D)
                obj.volumes.complete.dmvol = volumeDM;
                obj.volumes.complete.intvol = volumeINT;
            end

            % Autosaving
            if (obj.localPar.autosave)
                obj.saveCompleteVolume();
            end
        end

        function savePhaseVolume(obj, phaseID, fileName)
        % GTASSEMBLEVOL3D/SAVEPHASEVOLUME
        
            if (~exist('phaseID', 'var'))
                phaseID = 1;
            end
            if (~exist('fileName', 'var'))
                fileName = sprintf('phase_%02d_vol.mat', phaseID);
            end
            name = obj.checkAndSaveExistingFile(fileName);

            % Small trick to keep consistency in field name in the written file.
            outStruct = struct( ...
                'rotation_axes', obj.volumes.phases{phaseID}.rotation_axes, ...
                'rotation_angles', obj.volumes.phases{phaseID}.rotation_angles, ...
                'vol', int16(obj.volumes.phases{phaseID}.grains) );
            if (isfield(obj.volumes.phases{phaseID}, 'dmvol'))
                outStruct.dmvol = obj.volumes.phases{phaseID}.dmvol;
                outStruct.intvol = obj.volumes.phases{phaseID}.intvol; %#ok<STRNU>
            end

            % We lock, to avoid other processes to write to this file:
            % Actually this is commented out because it should happen
            % through the DB, as long as the ESRF's filesystem is not good
            % enough
%             lock = GtLock(fullfile('4_grains', 'sample.mat'));
%             lock.acquire();

            % Now let's load metadata
            sampleInfo = obj.getSample();
            obj.checkSampleInfo(sampleInfo, phaseID);

            % Store info about volume in the metadata
            sampleInfo.phases{phaseID}.volumeFile = [name ':vol'];
            obj.localPar.cache.set('sample', {}, sampleInfo, 'sample');
            % Flush metadata
            fprintf('Flushing sample.mat\n');
            obj.localPar.cache.flushObjectField('sample', {}, 'sample');

            % Let's release the lock
%             lock.release();

            % Saving the data
            fprintf('Writing phase %02d volume to: %s\n', phaseID, name);
            save(name, '-struct', 'outStruct', '-v7.3');
        end

        function saveCompleteVolume(obj, fileName)
        % GTASSEMBLEVOL3D/SAVECOMPLETEVOLUME
        
            if (~exist('fileName', 'var'))
                fileName = 'volume.mat';
            end
            name = obj.checkAndSaveExistingFile(fileName);

            filetable = [obj.parameters.acq(1).name '_filetable'];
            % We lock, to avoid other processes to write to this file:
            lock = GtLockDB(filetable, fullfile('4_grains', 'sample.mat'));
            lock.acquire();

            % Now let's load metadata
            sampleInfo = obj.getSample();

            % Store info about volume in the metadata
            sampleInfo.volumeFile = name;
            obj.localPar.cache.set('sample', {}, sampleInfo, 'sample');
            % Flush metadata
            fprintf('Flushing sample.mat\n');
            obj.localPar.cache.flushObjectField('sample', {}, 'sample');

            % Let's release the lock
            lock.release();

            % Saving the data
            fprintf('Writing complete volume to: %s\n', name);
            outStruct = obj.volumes.complete; %#ok<NASGU>
            save(name, '-struct', 'outStruct', '-v7.3');
        end

        function dilateCompleteVolume(obj, dilate_length, outFileName, inFileName)
        % GTASSEMBLEVOL3D/DILATECOMPLETEVOLUME

            if (~exist('outFileName', 'var'))
                outFileName = 'volume_dilated.mat';
            end
            name = obj.checkAndSaveExistingFile(outFileName);

            if (~exist('inFileName', 'var'))
                inFileName = fullfile('5_reconstruction', 'volume.mat');
            end

            compositeVol = load(inFileName);

            [uidVol, offset] = GtAssembleVol3D.convertCombinedVolumeToUIDs(compositeVol.phases, compositeVol.grains);

            % Try to see if there's a mask, and in case, use it
            maskFile = fullfile('5_reconstruction', 'volume_mask.mat');
            if (exist(maskFile, 'file'))
                maskVol = gtMATVolReader([maskFile ':vol']);

                fprintf('Doing %02d single dilation steps:\n', dilate_length)
                for step = 1:dilate_length
                    fprintf(' - %02d/%02d: ', step, dilate_length)
                    uidVol = gtDilateGrains(uidVol, 1, false);

                    fprintf('          Applying Absorption Mask.\n')
                    % Let's cut of the things out of the mask
                    newUidVol = zeros(size(uidVol), 'like', uidVol);
                    indexes = (maskVol ~= 0);
                    newUidVol(indexes) = uidVol(indexes);
                    uidVol = newUidVol;
                end
                fprintf('Done.\n')
            else
                uidVol = gtDilateGrains(uidVol, dilate_length, false);
            end

            [compositeVol.phases, compositeVol.grains] = GtAssembleVol3D.convertUIDsToCombinedVolume(uidVol, offset);

            assemble_6D = strcmpi(obj.parameters.rec.grains.algorithm(1:2), '6D');
            if (assemble_6D)
                [compositeVol.dmvol, compositeVol.intvol, compositeVol.phases, compositeVol.grains] ...
                    = obj.extractDmVolFromAssembled(compositeVol.phases, compositeVol.grains, dilate_length);
            end

            save(name, '-struct', 'compositeVol', '-v7.3');
        end

        function twinInfo = getTwinInfo(obj)
        % GTASSEMBLEVOL3D/GETTWININFO
             twinInfo = obj.twinInfo;
        end

        function redoIDs = getRedoList(obj)
            redoIDs = obj.redo;
        end
    end

    methods (Access = private)
        function checkSampleInfo(~, sampleInfo, phaseID)
        % GTASSEMBLEVOL3D/CHECKSAMPLEINFO

            if ((length(sampleInfo.phases) < phaseID) ...
                    || isempty(sampleInfo.phases{phaseID}))
                error('ASSEMBLE:wrong_argument', ...
                      'Phase: %02d, is not in the sample structure', phaseID);
            end
        end

        function sample = getSample(obj)
        % GTASSEMBLEVOL3D/getSample(obj)
            sample = obj.localPar.cache.get('sample', {}, 'sample');
        end

        function list = getGrainsList(obj, grainsNum)
        % GTASSEMBLEVOL3D/GETGRAINSLIST

            if isempty(obj.localPar.list) %|| strcmpi(obj.localPar.list, 'all')
                list = 1:grainsNum;
            else
                list = obj.localPar.list;
            end
        end

        function finalName = checkAndSaveExistingFile(~, fileName)
        % GTASSEMBLEVOL3D/CHECKANDSAVEEXISTINGFILE  Checks if the file exists and
        % saves it to the archive directory.

            if (isempty(fileName))
                error('ASSEMBLE:wrong_argument', ...
                      'not saving data - to save, supply a filename')
            end
            finalName = fullfile('5_reconstruction', fileName);

            if (exist(finalName, 'file'))
                [~, tempFilename, tempExt] = fileparts(finalName);
                fileInfo = dir(finalName);
                destName = [tempFilename ...
                    datestr(fileInfo(1).datenum, '_yyyy-mm-dd_HH-MM-SS') ...
                    tempExt];
                if (~exist(fullfile('5_reconstruction', 'archive'), 'dir'))
                    [~, msg] = mkdir(fullfile('5_reconstruction', 'archive'));
                    disp(msg);
                end
                copyfile(finalName, fullfile('5_reconstruction', 'archive', destName));
            end
        end

        function [sampleInfo, list] = initialiseAssembling(obj, phaseID)
        % GTASSEMBLEVOL3D/INITIALISEASSEMBLING  Initialises the support structures 
        % needed for assembly.

            % Now let's load metadata
            sampleInfo = obj.getSample();
            % And check as possible
            obj.checkSampleInfo(sampleInfo, phaseID);
            % Grain Number
            grainsNum = sampleInfo.phases{phaseID}.getNumberOfGrains();

            % handle list
            list = obj.getGrainsList(grainsNum);
        end

        function [p_gids, p_dm_vol, p_int_vol] = doAssemblePhaseVolume(obj, phase_id, grain_ids)
        % GTASSEMBLEVOL3D/DOASSEMBLEPHASEVOLUME  Function that effectively assembles phase volume

            sample = obj.getSample();
            p_gids = zeros(obj.parameters.acq(1).bb([3, 3, 4]), 'int16');

            assemble_6D = nargout >= 2;
            if (assemble_6D)
                p_dm_vol = zeros([size(p_gids), 3], 'single');

                p_int_vol = zeros(size(p_gids), 'single');
            end

            grains_num = numel(grain_ids);

            if (obj.localPar.deal_with_twins)
                obj.sigmas{phase_id} = gtCrystGetSigmas( ...
                    obj.parameters.cryst(phase_id).crystal_system, ...
                    obj.parameters.cryst(phase_id).latticepar, ...
                    obj.localPar.convention, ...
                    obj.parameters.cryst(phase_id).material, ...
                    obj.localPar.tol_angle);
                obj.twinInfo{phase_id}.output     = cell(grains_num, 1);
                obj.twinInfo{phase_id}.twin_list  = zeros(grains_num, 0);
                obj.twinInfo{phase_id}.check_list = zeros(grains_num, 0);
                obj.twinInfo{phase_id}.merge_list = zeros(grains_num, 0);
            end

            rotate_vols = ~isempty(obj.localPar.rotation_angles);
            if (rotate_vols)
                t = GtThreshold();

                rot_angles = obj.localPar.rotation_angles;
                rot_axes = obj.localPar.rotation_axes;
                rot = gtMathsRotationTensorComposite(rot_axes, rot_angles);
            end

            % Sample translation
            sample_shift = obj.localPar.sample_shift;
            if (isempty(sample_shift) || (size(sample_shift, 2) < 3))
                sample_shift = [0, 0, 0];
            end

            % Adding clusters first!
            clusters = sample.phases{phase_id}.getActiveClusters();

            clustersNum = numel(clusters);
            if (assemble_6D && clustersNum > 0 && ~obj.localPar.force_only_indexed)
                gauge = GtGauge(clustersNum, sprintf('Phase %02d Clusters: ', phase_id));
                for ii = 1:clustersNum
                    str_ids = sprintf('_%04d', clusters(ii).included_ids);
                    try
                        cl_rec = gtLoadClusterRec( ...
                            phase_id, clusters(ii).included_ids, ...
                            'fields', {'SEG', 'ODF6D'});

                        if (clusters(ii).cluster_type == 1)
                            cl_rec = obj.mergeTwinClusterRec(cl_rec);
                        end

                        if (rotate_vols)
                            if (clusters(ii).cluster_type == 1)
                                cluster = gtLoadCluster(phase_id, clusters(ii).included_ids);
                                cluster = cluster.samp_ors(1);
                            else
                                cluster = gtLoadCluster( ...
                                    phase_id, clusters(ii).included_ids, ...
                                    'fields', {'proj'});
                            end
                            cl_rec.ODF6D = gtGrainRotateStructure( ...
                                cl_rec.ODF6D, cluster.proj, 'ODF6D', obj.parameters, rot);

                            int_vol = cl_rec.ODF6D.intensity;

                            cluster_rec_center = round(size(int_vol) / 2);
                            % XXX this should be handled better: the border
                            % mask might change from grain to grain, if the
                            % user optimized it
                            cl_rec.SEG = t.volumeThreshold(int_vol, ...
                                cl_rec.SEG.threshold, cluster_rec_center, ...
                                true, obj.parameters.rec.thresholding.mask_border_voxels);
                            cl_rec.SEG.segbb(1:3) = cl_rec.SEG.segbb(1:3) + cl_rec.ODF6D.shift;
                        end

                        if (obj.localPar.solve_conflict_with_intensity)
                            [p_gids, p_int_vol, p_dm_vol] = obj.placeClusterSubVolume( ...
                                clusters(ii).included_ids(1), cl_rec.ODF6D, cl_rec.SEG, p_gids, p_int_vol, p_dm_vol, sample_shift);
                        else
                            seg_vol = int16(cl_rec.SEG.seg);
                            seg_bb = cl_rec.SEG.segbb;

                            int_vol = cl_rec.ODF6D.intensity;

                            crop_bb = seg_bb;
                            crop_bb(1:3) = crop_bb(1:3) - cl_rec.ODF6D.shift + 1;
                            int_vol = gtCrop(int_vol, crop_bb) .* logical(seg_vol);
%                             int_vol = int_vol * numel(find(seg_vol)) / sum(int_vol(:));

                            p_gids = gtPlaceSubVolume( p_gids, seg_vol, ...
                                seg_bb(1:3) + sample_shift, ...
                                clusters(ii).included_ids(1), ...
                                obj.localPar.overlaps);

                            p_int_vol = gtPlaceSubVolume( p_int_vol, ...
                                int_vol, seg_bb(1:3) + sample_shift );

                            seg = struct('vol', seg_vol, 'bb',  seg_bb);
                            p_dm_vol = obj.placeSubDmVol(p_dm_vol, cl_rec.ODF6D, seg, sample_shift);
                        end
                    catch mexc
                        gtPrintException(mexc, sprintf('\nCluster %d (%s) failed!!\n', ii, str_ids));
                        gauge.rePrint();
                    end

                    gauge.incrementAndDisplay();
                end
                gauge.delete();

                grains_blacklist = cat(2, clusters(:).included_ids);
            else
                grains_blacklist = [];
            end

            grain_ids = obj.getGrainsSortedBySize(phase_id, grain_ids, 'descend');

            gauge = GtGauge(grains_num, sprintf('Phase %02d Grains: ', phase_id));

            for ii = 1:grains_num;
                grain_id = grain_ids(ii);

                gauge.setExtraText(sprintf('(grain %04d)', grain_id));

                if (~sample.phases{phase_id}.getSelected(grain_id))
                    fprintf('\nSkipping deselected grain: %04d\n', grain_id);
                    gauge.rePrint();
                    gauge.incrementAndDisplay();
                    continue;
                elseif (ismember(grain_id, grains_blacklist))
                    fprintf('\nSkipping grain: %04d beause it was already included in a cluster\n', grain_id);
                    gauge.rePrint();
                    gauge.incrementAndDisplay();
                    continue;
                end

                try
                    if (assemble_6D)
                        if (sample.phases{phase_id}.getUseExtended(grain_id) && ~obj.localPar.force_only_indexed)
                            gr_rec = obj.localPar.cache.get('grain_ext_rec', {phase_id, grain_id}, 'ODF6D');
                            gr_seg = obj.localPar.cache.get('grain_ext_rec', {phase_id, grain_id}, 'SEG');
                        else
                            gr_rec = obj.localPar.cache.get('grain_rec', {phase_id, grain_id}, 'ODF6D');
                            gr_seg = obj.localPar.cache.get('grain_rec', {phase_id, grain_id}, 'SEG');
                        end
                    else
                        gr_rec = obj.localPar.cache.get('grain_rec', {phase_id, grain_id}, 'VOL3D');
                        gr_seg = obj.localPar.cache.get('grain_rec', {phase_id, grain_id}, 'SEG');
                    end
                    if (rotate_vols)
                        gr_proj = obj.localPar.cache.get('grain', {phase_id, grain_id}, 'proj');

                        if (assemble_6D)
                            gr_rec = gtGrainRotateStructure(gr_rec, gr_proj, 'ODF6D', obj.parameters, rot);
                        else
                            gr_rec = gtGrainRotateStructure(gr_rec, gr_proj, 'VOL3D', obj.parameters, rot);
                        end
                        int_vol = gr_rec.intensity;

                        gr_rec_center = round(size(int_vol) / 2);
                        % XXX this should be handled better: the border
                        % mask might change from grain to grain, if the
                        % user optimized it
                        gr_seg = t.volumeThreshold(int_vol, ...
                            gr_seg.threshold, gr_rec_center, true, ...
                            obj.parameters.rec.thresholding.mask_border_voxels);
                        gr_seg.segbb(1:3) = gr_seg.segbb(1:3) + gr_rec.shift;
                    end

                    seg_vol = int16(gr_seg.seg);
                    seg_bb = gr_seg.segbb;

                catch mexc
                    gtPrintException(mexc, sprintf('\nGrain %d failed!!\n', grain_id));
                    gauge.rePrint();
                    gauge.incrementAndDisplay();
                    obj.redo = [obj.redo grain_id];
                    continue;
                end

                if (isempty(seg_vol))
                    fprintf('\nEmpty segmented volume for grainID %4d...', grain_id);
                    gauge.rePrint();
                    gauge.incrementAndDisplay();
                    continue;
                end

                if (obj.localPar.deal_with_twins)
                    [p_gids, obj.twinInfo{phase_id}.output{ii}] = ...
                        obj.placeTwinSubVolume(gauge, p_gids, phase_id, grain_id, seg_vol, seg_bb, sample_shift);
                elseif (obj.localPar.solve_conflict_with_intensity)
                    if (assemble_6D)
                        [p_gids, p_int_vol, p_dm_vol] = obj.placeClusterSubVolume( ...
                            grain_id, gr_rec, gr_seg, p_gids, p_int_vol, p_dm_vol, sample_shift);
                    else
                        [p_gids, p_int_vol] = obj.placeClusterSubVolume( ...
                            grain_id, gr_rec, gr_seg, p_gids, p_int_vol, sample_shift);
                    end
                else
                    %fprintf(" seg_bb(1:3): ");

                    p_gids = gtPlaceSubVolume(p_gids, seg_vol, seg_bb(1:3) + sample_shift, grain_id, obj.localPar.overlaps);
                end
                if (assemble_6D && ~obj.localPar.solve_conflict_with_intensity)
                    seg = struct('vol', seg_vol, 'bb',  seg_bb);
                    p_dm_vol = obj.placeSubDmVol(p_dm_vol, gr_rec, seg);

                    crop_bb = seg_bb;
                    crop_bb(1:3) = crop_bb(1:3) - gr_rec.shift + 1;
                    int_vol = gtCrop(gr_rec.intensity, crop_bb) .* logical(seg_vol);
                    int_vol = int_vol * numel(find(seg_vol)) / sum(int_vol(:));
                    p_int_vol = gtPlaceSubVolume(p_int_vol, int_vol, seg_bb(1:3) + sample_shift);
                end

                % Trigger callback (usually used in graphical applications)
                gtTriggerUpdateCallback(obj.localPar.update_status_cb);

                gauge.incrementAndDisplay();
            end
            gauge.delete();

            if (strcmpi(obj.localPar.overlaps, 'zero'))
                % set overlapping regions to zero - this could be another label colour
                p_gids(p_gids == -1) = 0;
            end
            if (assemble_6D && ismember(obj.localPar.overlaps, {'zero', 'conflict'}))
                % set overlapping regions to zero - this could be another label colour
                p_dm_vol(p_gids == -1 | p_gids == 0) = 0;
                p_int_vol(p_gids == -1 | p_gids == 0) = 0;
            end
            if (obj.localPar.deal_with_twins)
                obj.twinInfo{phase_id}.fail_list = obj.redo;
            end
        end

        function cluster_rec = mergeTwinClusterRec(~, cluster_rec, dilate_steps)
            %sample_shift not added here...
            if (exist('dilate_steps', 'var') && ~isempty(dilate_steps) ...
                    && (dilate_steps > 0))
                seg_vol = single(cluster_rec.SEG(1).seg);
                for ii = 2:numel(cluster_rec.SEG)
                    seg_vol = seg_vol + ii * cluster_rec.SEG(ii).seg;
                end
                % XXX: we should check for actual intensity volume size
                seg_vol_final_size = size(seg_vol) + 2 * dilate_steps;
                seg_vol_shift = dilate_steps([1 1 1]);
                seg_vol = gtPlaceSubVolume(...
                    zeros(seg_vol_final_size, 'single'), ...
                    seg_vol, seg_vol_shift);
                seg_vol = gtDilateGrains(seg_vol, dilate_steps, false, false);
                for ii = 1:numel(cluster_rec.SEG)
                    cluster_rec.SEG(ii).seg = double(seg_vol == ii);
                    cluster_rec.SEG(ii).segbb(1:3) = cluster_rec.SEG(ii).segbb(1:3) - seg_vol_shift;
                    cluster_rec.SEG(ii).segbb(4:6) = seg_vol_final_size;
                end
            end
            cl_seg = cluster_rec.SEG(1);
            cl_rec = cluster_rec.ODF6D(1);
            for ii = 2:numel(cluster_rec.SEG)
                cl_seg.seg = cl_seg.seg + cluster_rec.SEG(ii).seg;
                cl_rec.intensity = cl_rec.intensity + cluster_rec.ODF6D(ii).intensity;
                mask = gtPlaceSubVolume(zeros(size(cl_rec.intensity), 'single'), ...
                    cluster_rec.SEG(ii).seg, cl_seg.segbb(1:3) - cl_rec.shift);
                mask = logical(mask);
                mask = cat(4, mask, mask, mask);
                cl_rec.voxels_avg_R_vectors(mask) = cluster_rec.ODF6D(ii).voxels_avg_R_vectors(mask);
            end
            cluster_rec = struct('ODF6D', cl_rec, 'SEG', cl_seg);
        end

        function [ph_gids, ph_ints, ph_dms] = placeClusterSubVolume(obj, grain_id, gr_rec, gr_seg, ph_gids, ph_ints, ph_dms, sample_shift)
            % the bbox extremes start from 0 ad not from 1 (matlab coords).
            % seg_bb(1:3) is a shift (argument 3) that starts from zero!
            [lims_vol, lims_gr] = gtGetVolsIntersectLimits(size(ph_gids), size(gr_seg.seg), gr_seg.segbb(1:3) + sample_shift);

            % Getting the subvolumes -> so we operate all the costy
            % operations on smaller volumes
            sub_ph_gids = ph_gids( ...
                lims_vol(1, 1):lims_vol(2, 1), ...
                lims_vol(1, 2):lims_vol(2, 2), ...
                lims_vol(1, 3):lims_vol(2, 3) );
            sub_gr_seg = gr_seg.seg( ...
                lims_gr(1, 1):lims_gr(2, 1), ...
                lims_gr(1, 2):lims_gr(2, 2), ...
                lims_gr(1, 3):lims_gr(2, 3) );
            sub_gr_seg = logical(sub_gr_seg);

            seg_bb = gr_seg.segbb;
            seg_bb(1:3) = seg_bb(1:3) - gr_rec.shift + 1;
            gr_ints = gtCrop(gr_rec.intensity, seg_bb);

            sub_ph_ints = ph_ints( ...
                lims_vol(1, 1):lims_vol(2, 1), ...
                lims_vol(1, 2):lims_vol(2, 2), ...
                lims_vol(1, 3):lims_vol(2, 3) );
            sub_gr_ints = gr_ints( ...
                lims_gr(1, 1):lims_gr(2, 1), ...
                lims_gr(1, 2):lims_gr(2, 2), ...
                lims_gr(1, 3):lims_gr(2, 3) );

            % what grains overlap?
            disputes = sub_gr_seg & (sub_ph_gids ~= 0);

            worse_gr = (sub_gr_ints < sub_ph_ints) & disputes;
            sub_gr_seg(worse_gr) = false;

            sub_ph_gids(sub_gr_seg) = grain_id;
            sub_ph_ints(sub_gr_seg) = sub_gr_ints(sub_gr_seg);

            ph_gids( ...
                lims_vol(1, 1):lims_vol(2, 1), ...
                lims_vol(1, 2):lims_vol(2, 2), ...
                lims_vol(1, 3):lims_vol(2, 3) ) = sub_ph_gids;
            ph_ints( ...
                lims_vol(1, 1):lims_vol(2, 1), ...
                lims_vol(1, 2):lims_vol(2, 2), ...
                lims_vol(1, 3):lims_vol(2, 3) ) = sub_ph_ints;

            is_6D = exist('ph_dms', 'var') && ~isempty(ph_dms);
            if (is_6D)
                sub_ph_dms = ph_dms( ...
                    lims_vol(1, 1):lims_vol(2, 1), ...
                    lims_vol(1, 2):lims_vol(2, 2), ...
                    lims_vol(1, 3):lims_vol(2, 3), : );
                gr_dms = gtCrop(gr_rec.voxels_avg_R_vectors, [seg_bb(1:3), 1, seg_bb(4:6), 3]);
                sub_gr_dms = gr_dms( ...
                    lims_gr(1, 1):lims_gr(2, 1), ...
                    lims_gr(1, 2):lims_gr(2, 2), ...
                    lims_gr(1, 3):lims_gr(2, 3), : );

                sub_gr_seg_inds = find(sub_gr_seg);
                sub_gr_seg_inds = cat(1, sub_gr_seg_inds, sub_gr_seg_inds + numel(sub_gr_ints), sub_gr_seg_inds + 2 * numel(sub_gr_ints));

                sub_ph_dms(sub_gr_seg_inds) = sub_gr_dms(sub_gr_seg_inds);
                ph_dms( ...
                    lims_vol(1, 1):lims_vol(2, 1), ...
                    lims_vol(1, 2):lims_vol(2, 2), ...
                    lims_vol(1, 3):lims_vol(2, 3), : ) = sub_ph_dms;
            end
        end

        function [volume, g_info] = placeTwinSubVolume(obj, gauge, volume, phase_id, grain_id, seg_vol, seg_bb, sample_shift)
            sample = obj.getSample();

            % Sample translation
            if (~exist('sample_shift', 'var') || isempty(sample_shift))
                sample_shift = obj.localPar.sample_shift;
                sample_shift_size = size(sample_shift);
                if ((isempty(sample_shift)) || (sample_shift_size(2) < 3))
                   sample_shift = [0, 0, 0];
                end
            end

            % the bbox extremes start from 0 ad not from 1 (matlab coords).
            % seg_bb(1:3) is a shift (argument 3) that starts from zero!
            [limsVolume, limsGrain] = gtGetVolsIntersectLimits(size(volume), size(seg_vol), seg_bb(1:3) + sample_shift);

            % Getting the subvolumes -> so we operate all the costy
            % operations on smaller volumes
            sub_phase_vol = volume( ...
                limsVolume(1, 1):limsVolume(2, 1), ...
                limsVolume(1, 2):limsVolume(2, 2), ...
                limsVolume(1, 3):limsVolume(2, 3) );
            sub_grain_vol = seg_vol( ...
                limsGrain(1, 1):limsGrain(2, 1), ...
                limsGrain(1, 2):limsGrain(2, 2), ...
                limsGrain(1, 3):limsGrain(2, 3) );

            % what grains overlap?
            disputes = sub_phase_vol(sub_grain_vol ~= 0);
            disputes = unique(disputes);
            disputes(disputes == 0 | disputes == -1) = [];

            % first, assign the non disputed part
            not_shared_inds = find((sub_phase_vol == 0) & sub_grain_vol);

            sub_phase_vol(not_shared_inds) = grain_id;
            % now, go through the disputed grains checking orientations
            if (~isempty(disputes))
                ref_r_vec = sample.phases{phase_id}.getR_vector(grain_id);
                twin_num = 0;
                check_num = 0;

                num_disputed_grains = numel(disputes);
                g_info = cell(1, num_disputed_grains);
                for jj = 1:num_disputed_grains
                    grain_id_interf = disputes(jj);

                    test_r_vec = sample.phases{phase_id}.getR_vector(grain_id_interf);

                    g_info{jj} = gtCrystTwinTest( ...
                        ref_r_vec, test_r_vec, phase_id, ...
                        'symm', obj.symm{phase_id}, ...
                        'sigmas', obj.sigmas{phase_id}, ...
                        'merge_angle', obj.localPar.merge_angle, ...
                        'convention', obj.localPar.convention, ...
                        'tol_angle', obj.localPar.tol_angle, ...
                        'tol_axis', obj.localPar.tol_axis, ...
                        'parameters', obj.parameters );

                    g_info{jj}.grainID_1 = grain_id;
                    g_info{jj}.grainID_2 = grain_id_interf;

                    % Test if it is twin of another grain
                    shared_inds = find((sub_phase_vol == grain_id_interf) & sub_grain_vol);

                    g_info{jj}.indexes_shared = shared_inds;
                    g_info{jj}.size_vol_free  = numel(not_shared_inds);
                    g_info{jj}.size_vol       = numel(not_shared_inds) + numel(shared_inds);
                    g_info{jj}.frac_shared    = numel(shared_inds) / g_info{jj}.size_vol;

                    grains_not_related = isempty(g_info{jj}.sigmaAnd) ...
                            || ((numel(g_info{jj}.sigmaAnd) == 1) && (g_info{jj}.sigmaAnd <= 0));
                    if (grains_not_related)
                        if (g_info{jj}.frac_shared >= 0.5) && (g_info{jj}.sigmaAnd == -1)
                            % merge the two grains together; keep the other
                            % and record this in the merge list
                            g_info{jj}.type = 'merge';
                        else
                            % this is not a twin, so it is disputed
                            g_info{jj}.type = 'independent';
                        end

                        switch (obj.localPar.overlaps)
                            case {'zero', 'conflict', 'adaptive'}
                                sub_phase_vol(shared_inds) = -1;
                            case 'summed'
                                sub_phase_vol(shared_inds) = sub_phase_vol(shared_inds) + grain_id;
                            case 'assign'
                                sub_phase_vol(shared_inds) = grain_id;
                            case 'parent'
                                sub_phase_vol(shared_inds) = grain_id_interf;
                        end
                    else
                        if (g_info{jj}.frac_shared >= 0.5)
                            % this is a twin.  Allow the new grain (smaller)
                            % to overwrite the older grain
                            g_info{jj}.type = 'twin';

                            sub_phase_vol(shared_inds) = grain_id;
                            if (obj.localPar.use_parent_mask)
                                sub_phase_vol(not_shared_inds) = 0;
                            end
                        else
                            % MAYBE this is a twin.  Allow the new grain (smaller)
                            % to overwrite the older grain: PROBLEM
                            % WITH THE PARENT SEGMENTATION
                            g_info{jj}.type = 'possible_twin';

                            sub_phase_vol(shared_inds) = grain_id;
                            if (obj.localPar.use_parent_mask)
                                sub_phase_vol(not_shared_inds) = 0;
                            end
                        end
                    end
                    [twin_num, check_num] = obj.printAssemblePhaseTwinOutput(...
                        gauge, g_info{jj}, phase_id, twin_num, check_num);
                end
                % Now let's get back the volume in the complete one
                % We use the new shifts given by : limsVolume(1, :) - 1,
                % because the might have cropped the volumes, due to grains
                % falling out of the volume
                volume = gtPlaceSubVolume(volume, sub_phase_vol, ...
                    (limsVolume(1, :) - 1) + sample_shift, 0, 'assign');
            else
                g_info = {};
                % Ok, no conflict or no twin case -> let's treat it as
                % normal case
                volume = gtPlaceSubVolume(volume, seg_vol, seg_bb(1:3) + sample_shift, ...
                    grain_id, obj.localPar.overlaps);
            end
        end

        function [dmvol, intvol, vol_ph_ids, vol_gr_ids] = extractDmVolFromAssembled(obj, vol_ph_ids, vol_gr_ids, dilate_steps)
            sample = obj.getSample();

            rotate_vols = ~isempty(obj.localPar.rotation_angles);
            if (rotate_vols)
                rot_vols = eye(3);
                rot_angles = obj.localPar.rotation_angles;
                for ii_cl = 1:numel(rot_angles)
                    rot_axis = obj.localPar.rotation_axes(ii_cl, :)';
                    rotcomp = gtMathsRotationMatrixComp(rot_axis, 'col');
                    rot_vols = gtMathsRotationTensor(rot_angles(ii_cl), rotcomp) * rot_vols;
                end
            end

            dmvol = zeros([size(vol_gr_ids), 3], 'single');
            intvol = zeros(size(vol_gr_ids), 'single');

            ph_ids = unique(vol_ph_ids(vol_ph_ids > 0));

            fprintf('Assigning R-vectors and intensities to assembled voxels:\n- phase: ')
            for ii_ph = 1:numel(ph_ids)
                phase_id = ph_ids(ii_ph);
                ph_mask = vol_ph_ids == phase_id;
                gr_ids = unique(vol_gr_ids((vol_gr_ids > 0) & ph_mask));

                phase_file_name = sample.phases{phase_id}.volumeFile;
                phase_file_name = regexp(phase_file_name, ':', 'split');
                phase_file = matfile(phase_file_name{1});

                % We first check wether the phase has some rotation
                % encoded, otherwise we look at the rotation encoded in the
                % assembling class.
                rotate_phase = ismember('rotation_angles', fieldnames(phase_file)) ...
                    && ~isempty(phase_file.rotation_angles);
                if (rotate_phase)
                    rot_phase = eye(3);
                    rot_angles = phase_file.rotation_angles;
                    for ii_cl = 1:numel(rot_angles)
                        rot_axis = phase_file.rotation_axes(ii_cl, :)';
                        rotcomp = gtMathsRotationMatrixComp(rot_axis, 'col');
                        rot_phase = gtMathsRotationTensor(rot_angles(ii_cl), rotcomp) * rot_phase;
                    end
                elseif (rotate_vols)
                    rotate_phase = rot_vols;
                end

                num_char_ph = fprintf('%02d, cluster: ', phase_id);

                clusters = sample.phases{phase_id}.getActiveClusters();
                clustersNum = numel(clusters);
                for ii_cl = 1:clustersNum
                    str_ids = sprintf(' %04d', clusters(ii_cl).included_ids);
                    num_char_gr = fprintf('%02d/%02d (ids:%s)', ii_cl, clustersNum, str_ids);

                    cluster_rec = gtLoadClusterRec( ...
                        phase_id, clusters(ii_cl).included_ids, ...
                        'fields', {'SEG', 'ODF6D'});

                    if (clusters(ii_cl).cluster_type == 1)
                        cluster_rec = obj.mergeTwinClusterRec(cluster_rec, dilate_steps);
                    end

                    if (rotate_phase || rotate_vols)
                        if (clusters(ii_cl).cluster_type == 1)
                            cluster = gtLoadCluster(phase_id, clusters(ii_cl).included_ids);
                            cluster = cluster.samp_ors(1);
                        else
                            cluster = gtLoadCluster( ...
                                phase_id, clusters(ii_cl).included_ids, ...
                                'fields', {'proj'});
                        end
                        cluster_rec.ODF6D = gtGrainRotateStructure( ...
                            cluster_rec.ODF6D, cluster.proj, 'ODF6D', obj.parameters, rot_phase);
                    end
                    cl_int_vol = cluster_rec.ODF6D.intensity;
                    cl_inds_vol = find(vol_gr_ids == clusters(ii_cl).included_ids(1) & ph_mask);
                    [cl_inds_vol, cl_inds_rec, filter_gr_inds_vol] ...
                        = obj.filter_dilated_voxels(cl_inds_vol, cl_int_vol, vol_gr_ids, cluster_rec.ODF6D.shift); %#ok<FNDSB>

                    vol_gr_ids(filter_gr_inds_vol) = 0;
                    vol_ph_ids(filter_gr_inds_vol) = 0;

                    cl_inds_vol_dm = cat(1, cl_inds_vol, cl_inds_vol + numel(vol_gr_ids), cl_inds_vol + 2 * numel(vol_gr_ids));
                    cl_inds_rec_dm = cat(1, cl_inds_rec, cl_inds_rec + numel(cl_int_vol), cl_inds_rec + 2 * numel(cl_int_vol));

                    dmvol(cl_inds_vol_dm) = cluster_rec.ODF6D.voxels_avg_R_vectors(cl_inds_rec_dm);

                    ints = cluster_rec.ODF6D.intensity(cl_inds_rec);
                    ints = ints ./ mean(ints);
                    intvol(cl_inds_vol) = ints;

                    fprintf(repmat('\b', [1 num_char_gr]));
                    fprintf(repmat(' ', [1 num_char_gr]));
                    fprintf(repmat('\b', [1 num_char_gr]));
                end

                if (clustersNum)
                    grains_blacklist = cat(2, clusters(:).included_ids);
                else
                    grains_blacklist = [];
                end

                fprintf(repmat('\b', [1 num_char_ph]));
                fprintf(repmat(' ', [1 num_char_ph]));
                fprintf(repmat('\b', [1 num_char_ph]));
                num_char_ph = fprintf('%02d, grain: ', phase_id);

                for ii_gr = 1:numel(gr_ids)
                    grain_id = gr_ids(ii_gr);
                    num_char_gr = fprintf('%04d/%04d (id: %04d)', ii_gr, numel(gr_ids), grain_id);
                    if (ismember(grain_id, grains_blacklist))
                        fprintf('\nSkipping grain: %04d beause it was already included in a cluster\n- phase: %02d, grain: ', ...
                            grain_id, phase_id);
                        continue
                    end

                    if (sample.phases{phase_id}.getUseExtended(grain_id) && ~obj.localPar.force_only_indexed)
                        vol6D = obj.localPar.cache.get('grain_ext_rec', {phase_id, grain_id}, 'ODF6D');
                    else
                        vol6D = obj.localPar.cache.get('grain_rec', {phase_id, grain_id}, 'ODF6D');
                    end
                    if (rotate_phase || rotate_vols)
                        gr_proj = obj.localPar.cache.get('grain', {phase_id, grain_id}, 'proj');
                        vol6D = gtGrainRotateStructure(vol6D, gr_proj, 'ODF6D', obj.parameters, rot_phase);
                    end

                    gr_int_vol = vol6D.intensity;
                    gr_inds_vol = find(vol_gr_ids == grain_id & ph_mask);
                    [gr_inds_vol, gr_inds_rec, filter_gr_inds_vol] ...
                        = obj.filter_dilated_voxels(gr_inds_vol, gr_int_vol, vol_gr_ids, vol6D.shift); %#ok<FNDSB>

                    vol_gr_ids(filter_gr_inds_vol) = 0;
                    vol_ph_ids(filter_gr_inds_vol) = 0;

%                     gr_mask = false(size(gr_int_vol));
%                     gr_mask(gr_inds_rec) = true;
%                     GtVolView(gr_int_vol, 'overlay', gr_mask)
%                     pause

                    gr_inds_vol_dm = cat(1, gr_inds_vol, gr_inds_vol + numel(vol_gr_ids), gr_inds_vol + 2 * numel(vol_gr_ids));
                    gr_inds_rec_dm = cat(1, gr_inds_rec, gr_inds_rec + numel(gr_int_vol), gr_inds_rec + 2 * numel(gr_int_vol));

                    dmvol(gr_inds_vol_dm) = vol6D.voxels_avg_R_vectors(gr_inds_rec_dm);

                    ints = vol6D.intensity(gr_inds_rec);
                    ints = ints ./ mean(ints);
                    intvol(gr_inds_vol) = ints;

                    fprintf(repmat('\b', [1 num_char_gr]));
                    fprintf(repmat(' ', [1 num_char_gr]));
                    fprintf(repmat('\b', [1 num_char_gr]));
                end
                fprintf(repmat('\b', [1 num_char_ph]));
                fprintf(repmat(' ', [1 num_char_ph]));
                fprintf(repmat('\b', [1 num_char_ph]));
            end
            fprintf('%02d/%02d, Done.\n', numel(ph_ids), numel(ph_ids));
        end

        function [gr_inds_vol, gr_inds_rec, filter_gr_inds_vol] = filter_dilated_voxels(~, gr_inds_vol, gr_int_vol, vol_gr_ids, gr_shift)
            [gr_inds_rec_x, gr_inds_rec_y, gr_inds_rec_z] = ind2sub(size(vol_gr_ids), gr_inds_vol);

            gr_inds_rec_x = gr_inds_rec_x - gr_shift(1);
            gr_inds_rec_y = gr_inds_rec_y - gr_shift(2);
            gr_inds_rec_z = gr_inds_rec_z - gr_shift(3);

            size_gr_vol = size(gr_int_vol);
            valid_voxels = ...
                (gr_inds_rec_x > 0) & (gr_inds_rec_x <= size_gr_vol(1)) ...
                & (gr_inds_rec_y > 0) & (gr_inds_rec_y <= size_gr_vol(2)) ...
                & (gr_inds_rec_z > 0) & (gr_inds_rec_z <= size_gr_vol(3));

            gr_inds_rec_x = gr_inds_rec_x(valid_voxels);
            gr_inds_rec_y = gr_inds_rec_y(valid_voxels);
            gr_inds_rec_z = gr_inds_rec_z(valid_voxels);
            filter_gr_inds_vol = gr_inds_vol(~valid_voxels);
            gr_inds_vol = gr_inds_vol(valid_voxels);

            gr_inds_rec = sub2ind(size_gr_vol, gr_inds_rec_x, gr_inds_rec_y, gr_inds_rec_z);

            valid_voxels = gr_int_vol(gr_inds_rec) > 0;

            filter_gr_inds_vol = cat(1, filter_gr_inds_vol, gr_inds_vol(~valid_voxels));
            gr_inds_vol = gr_inds_vol(valid_voxels);
            gr_inds_rec = gr_inds_rec(valid_voxels);
        end

        function [twin_num, check_num] = printAssemblePhaseTwinOutput(obj, gauge, g_info, phase_id, twin_num, check_num)
            % Format string for the output
            out_format_string = '\nFound: %36s - with mis_angle %5.2f degrees (shared volume fraction %4.2f)\n';

            grain_id = g_info.grainID_1;
            grain_id_interf = g_info.grainID_2;
            % Producing output
            switch (g_info.type)
                case 'merge'
                    obj.twinInfo{phase_id}.merge_list(grain_id, 1) = grain_id;
                    obj.twinInfo{phase_id}.merge_list(grain_id, 2) = grain_id_interf;

                    fprintf(out_format_string, ...
                        sprintf('Possible MERGE: grains %4d and %4d', grain_id, grain_id_interf), ...
                        g_info.mis_angle, ...
                        g_info.frac_shared);
                    gauge.rePrint();
                case 'independent'
                case 'twin'
                    obj.twinInfo{phase_id}.twin_list(grain_id, 1) = grain_id;
                    obj.twinInfo{phase_id}.twin_list(grain_id, twin_num+2) = grain_id_interf;
                    twin_num = twin_num + 1;

                    fprintf(out_format_string, ...
                        sprintf('TWIN %4d for grainID %4d', grain_id, grain_id_interf), ...
                        g_info.mis_angle, ...
                        g_info.frac_shared);
                    gauge.rePrint();
                case 'possible_twin'
                    obj.twinInfo{phase_id}.check_list(grain_id, 1) = grain_id;
                    obj.twinInfo{phase_id}.check_list(grain_id, check_num+2) = grain_id_interf;
                    check_num = check_num + 1;

                    fprintf(out_format_string, ...
                        sprintf('Possible TWIN %4d for grainID %4d', grain_id, grain_id_interf), ...
                        g_info.mis_angle, ...
                        g_info.frac_shared);
                    gauge.rePrint();
            end
        end

        function savePhaseTwinOutput(obj, phaseID)
        % GTASSEMBLEVOL3D/SAVEPHASETWINOUTPUT  
        % saving twin info on disk
            twinOut = obj.twinInfo{phaseID}.output;
            twinOut = [twinOut{:}];
            twin_list  = obj.twinInfo{phaseID}.twin_list;
            check_list = obj.twinInfo{phaseID}.check_list;
            merge_list = obj.twinInfo{phaseID}.merge_list;
            fail_list  = obj.twinInfo{phaseID}.fail_list;

            phase_dir = fullfile(obj.parameters.acq(1).dir, '4_grains', ...
                sprintf('phase_%02d', phaseID));

            disp(' ')
            if (~isempty(twinOut))
                out_file = fullfile(phase_dir, 'twinOut.mat');
                save(out_file, 'twinOut', '-v7.3');
                disp(['Saved twin output in ' out_file])
            end
            if (~isempty(twin_list))
                % remove third column
                if size(twin_list, 2) == 3
                    tmp = twin_list(twin_list(:, 3) ~= 0, :);
                    if ~isempty(tmp)
                        tmp(:, 1) = tmp(:, 3);
                        tmp(:, 3) = [];
                        twin_list(:, 3) = [];
                        twin_list(end+1:end+size(tmp, 1), :) = tmp;
                    end
                end
                twin_list(twin_list(:, 1) == 0, :) = [];
                twin_list = unique(twin_list, 'rows');

                out_file = fullfile(phase_dir, 'twin_list.mat');
                save(out_file, 'twin_list', '-v7.3');
                disp(['Saved twin list in ' out_file])
            end
            if (~isempty(check_list))
                % remove third column
                if (size(check_list, 2) == 3)
                    tmp = check_list(check_list(:, 3) ~= 0, :);
                    if ~isempty(tmp)
                        tmp(:, 1) = tmp(:, 3);
                        tmp(:, 3) = [];
                        check_list(:, 3) = [];
                        check_list(end+1:end+size(tmp, 1), :) = tmp;
                    end
                end
                check_list(check_list(:, 1) == 0, :) = [];
                check_list = unique(check_list, 'rows');

                out_file = fullfile(phase_dir, 'check_list.mat');
                save(out_file, 'check_list', '-v7.3');
                disp(['Saved check list in ' out_file])
            end
            if (~isempty(merge_list))
                merge_list(merge_list(:, 1) == 0, :) = [];

                out_file = fullfile(phase_dir, 'merge_list.mat');
                save(out_file, 'merge_list', '-v7.3');
                disp(['Saved merge list in ' out_file])
            end
            if ~isempty(fail_list)
                fail_list(fail_list(:, 1)==0, :) = [];

                out_file = fullfile(phase_dir, 'fail_list.mat');
                save(out_file, 'fail_list', '-v7.3');
                disp(['Saved fail list in ' out_file])
            end

            obj.twinInfo{phaseID}.output     = twinOut;
            obj.twinInfo{phaseID}.twin_list  = twin_list;
            obj.twinInfo{phaseID}.check_list = check_list;
            obj.twinInfo{phaseID}.merge_list = merge_list;
            obj.twinInfo{phaseID}.fail_list  = fail_list;
            
            info = obj.twinInfo{phaseID};
            if (~isempty(info))
                out_file = fullfile(phase_dir, 'twinInfo.mat');
                save(out_file, '-struct', 'info', '-v7.3');
                disp(['Saved twin info in ' out_file])
            end
        end

        function grain_ids = getGrainsSortedBySize(obj, phase_id, grain_ids, sortOrder)
        % GTASSEMBLEVOL3D/
            % want to work in reverse size order
            grainsNum = length(grain_ids);
            if (~exist('sortOrder', 'var'))
                sortOrder = 'descend';
            end
            sample=GtSample.loadFromFile;
            grainsize = zeros(grainsNum, 1);
            gauge = GtGauge(grainsNum, sprintf('Loading grains from phase %02d: ', phase_id));
            for ii = 1:grainsNum
                gauge.incrementAndDisplay();
                try
                    if (sample.phases{phase_id}.getUseExtended(grain_ids(ii)) && ~obj.localPar.force_only_indexed)
                        vol = obj.localPar.cache.get('grain_ext_rec', {phase_id, grain_ids(ii)}, 'SEG');
                    else
                        vol = obj.localPar.cache.get('grain_rec', {phase_id, grain_ids(ii)}, 'SEG');
                    end
                    grainsize(ii) = length(find(vol.seg(:)));
                catch
                    % We ignore the error because it will come up again
                    % later
                    grainsize(ii) = 0;
                end
            end
            gauge.delete();
            % sort - to do largest grain first
            [~, index] = sort(grainsize, sortOrder);
            grain_ids = grain_ids(index);
        end
    end

    methods (Static, Access = public)
        function conf = generateDefaults()
        % GTASSEMBLEVOL3D/GENERATEDEFAULTS  Generates default parameters

            % handle variables
            conf.overlaps = 'conflict';         % how to handle overlapping grains:
                                                %   ('zero' | 'summed' | {'conflict'} | 'adaptive' | 'assign' | 'parent')
            conf.add_to_filename = [];          % to add grains to a preexisting volume (saved as edf)
            conf.list = [];                     % if list is 'all', or [], load all grains.  Otherwise, only the list
            conf.display_error_messages = true; % show error messages {true} | false
            conf.update_status_cb = [];         % Callback to update the status of anything
            conf.cache = [];                    % Grain volumes cache (to not reload all of them each time)
            conf.autosave = true;               % Saves automatically the volumes after assembling them {true} | false
            conf.autoload = true;               % Loads automatically the assembled volume in the GM gui {true} | false
            conf.deal_with_twins = false;       % true | {false}
            conf.use_parent_mask = false;       % use a mask before dilating if it is a parent/twin case
            conf.convention = 'X';              % Hexagonal axes convention {'X'} | 'Y' for assembling volume with deal_with_twins=true
            conf.merge_angle = 5;               % misorientation angle for two grains to be merged {5}
            conf.tol_angle = 15;                % constant term in Brandon criterion (twins)
            conf.tol_axis = 5;                  % maximum deviation from expected rotation axis direction (twins)
            conf.force_only_indexed = false;
            conf.solve_conflict_with_intensity = false;
            conf.rotation_axes = zeros(0, 3);
            conf.rotation_angles = zeros(0, 1);

            % add a sample shift after assembling to have it in the field of view
            conf.sample_shift = zeros(0, 3);
        end

        function [newVol, offset] = convertCombinedVolumeToUIDs(phaseIDs, grainIDs, offset)
        % GTASSEMBLEVOL3D/CONVERTCOMBINEDVOLUMETOUIDS  Converts from a twofold matrix, 
        % into a unique ID matrix.

            % The conflicts are automatically set to zero
            maxGrainID_1 = max(grainIDs(:)) +1;
            if (~exist('offset', 'var'))
                offset = maxGrainID_1;
            elseif (offset < maxGrainID_1)
                error('ASSEMBLE:wrong_argument', ...
                      'bad offset, there could be overlaps in IDs');
            end
            indexes = (grainIDs < 0);
            grainIDs(indexes) = 0;
            phaseIDs(indexes) = 0;
            newVol = grainIDs + (phaseIDs -1) * offset;
            % We need to kill the conflict regions if they exist, and the zeros.
            newVol(newVol < 0) = 0;
        end

        function [phaseIDs, grainIDs] = convertUIDsToCombinedVolume(uidVol, offset)
        % GTASSEMBLEVOL3D/CONVERTUIDSTOCOMBINEDVOLUME  Converts back to a twofold
        % matrix, from a unique IDs matrix. The offset parameters is needed in
        % order to distinguish the phases.

            grainIDs = mod(uidVol, offset);
            uidVol = uidVol - grainIDs;
            phaseIDs = (floor(uidVol / offset) +1);
            phaseIDs(grainIDs == 0) = 0;
        end

        function dmvol = placeSubDmVol(dmvol, vol6D, seg, sample_shift)
        % GTASSEMBLEVOL3D/placeSubDmVol

            if nargin < 4
                sample_shift = [0, 0, 0];
            end
            sample_shift_size = size(sample_shift);
            if sample_shift_size(2)<3
                sample_shift = [0, 0, 0];
            end
            seg_bb = [seg.bb(1:3) - vol6D.shift + 1, seg.bb(4:6)];
            for ii_dim = 1:3
                single_dim_dmvol = vol6D.voxels_avg_R_vectors(:, :, :, ii_dim);
                single_dim_dmvol = gtCrop(single_dim_dmvol, seg_bb);
                single_dim_dmvol(~seg.vol) = 0;

                dmvol = gtPlaceSubVolume( dmvol, single_dim_dmvol, ...
                    [seg.bb(1:3), ii_dim-1] + [sample_shift, 0], [], 'assign');
            end
        end

        function info = loadPhaseTwinOutput(wdir, phaseID)
        % GTASSEMBLEVOL3D/LOADPHASETWINOUTPUT  loading twin info output from disk 
            if (~exist('wdir', 'var') || isempty(wdir))
                wdir = pwd();
            end
            twin_info_file = fullfile(wdir, '4_grains',  ....
                sprintf('phase_%02d', phaseID), 'twinInfo.mat');

            if (exist(twin_info_file, 'file'))
                info = load(twin_info_file);
            else
                info = [];
            end
        end
    end
end
