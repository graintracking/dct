function varargout = gtAstraReconstructGrain(grainID, phaseID, parameters)
% GTASTRARECONSTRUCTGRAIN  3D ART (ASTRA) reconstruction on a GPU machine
%     gtAstraReconstructGrain(grainID, phaseID, [parameters])
%     -------------------------------------------------------
%     Needs to be submitted on a GPU machine
%     (varargin for gtOarLaunch: {'gpu',true} or 'oarsub -q gpu ...')
%
%     Reads 4_grains/phase_##/grain_****.mat    : gr.proj,gr.selected
%     Modifies 4_grains/phase_##/grain_****.mat : gr.proj.stack, gr.proj.geom
%     Appends 4_grains/phase_##/grain_****.mat  : vol
%
%     Version 002 29-05-2012 by LNervo
%       Commenting and formatting
%

    if (~exist('parameters','var') || isempty(parameters))
        parameters = gtLoadParameters();
    end

    if (isfield(parameters.rec, 'grains'))
        rec_opts = parameters.rec.grains;
    else
        % Old style parameters
        warning('Old "rec" parameters')
        rec_opts = gtRecGrainsDefaultParameters(parameters.rec.method);
        rec_opts.num_iter = parameters.rec.num_iter;
    end

    gr = gtLoadGrain(phaseID, grainID, 'fields', {'proj'});
    gr.proj = gr.proj(1);
    if (~isfield(gr.proj, 'selected'))
        gr_extra = gtLoadGrain(phaseID, grainID, 'fields', {'selected'});
        gr.proj.selected = gr_extra.selected;
    end
    if isempty(find(gr.proj.selected))
        warning('No projection has been selected - skipping grain')
        return
    end
    gr.proj.num_iter = rec_opts.num_iter;

    gr.proj.stack = gr.proj.stack(:, gr.proj.selected, :);
    gr.proj.geom  = gr.proj.geom(gr.proj.selected, :);

    if (isfield(parameters.rec.grains.options, 'psf') ...
            && ~isempty(parameters.rec.grains.options.psf))
        gr.proj.psf = parameters.rec.grains.options.psf;
    end

    if (~isfield(rec_opts, 'options') || isempty(rec_opts.options))
        rec_opts_args = {};
    else
        rec_opts_args = [fieldnames(rec_opts.options), struct2cell(rec_opts.options)]';
    end

    is_cone = isfield(parameters.labgeo, 'sourcepoint') ...
        && (~isempty(parameters.labgeo.sourcepoint));

    switch (rec_opts.algorithm)
        case {'3DART', 'SIRT'}
            rec_vol = gtAstra3D(gr.proj, 'is_cone', is_cone);
        case {'3DTV'}
            rec_vol = gtAstra3DTV(gr.proj, 'is_cone', is_cone, rec_opts_args{:});
    end
    shift = gtFwdSimComputeVolumeShifts(gr.proj, parameters, size(rec_vol));

    % Saving details to file
    gr.VOL3D = gtReconstructDataStructureDefinition('VOL3D');
    gr.VOL3D.intensity = rec_vol;
    gr.VOL3D.shift = shift;
    gr.VOL3D.options = rec_opts;

    gtSaveGrainRec(phaseID, grainID, gr, 'fields', {'VOL3D'});

    if (nargout > 0)
        varargout{1} = gr.VOL3D;
    end
end
