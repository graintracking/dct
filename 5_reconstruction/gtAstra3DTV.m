function [volume, res_norm] = gtAstra3DTV(proj, varargin)
% GTASTRA3DTV  3D TV-min reconstruction
%     volume = gtAstra3DTV(proj, varargin)
%     ---------------------------------
%
%     INPUT: proj structure containing fields:
%                .stack          stack of projections
%                .geom           projection geometry for Astra
%                                geom is a N x 12 matrix, witn N = #projections
%                                Each row is:
%                                ( rayX, rayY, rayZ, dX, dY, dZ, uX, uY, uZ, vX, vY, vZ )
%                                  ray: the ray direction
%                                  d  : the center of the detector
%                                  u  : the vector from detector pixel (0,0) to (0,1)
%                                  v  : the vector from detector pixel (0,0) to (1,0)
%                .num_rows       vertical size of projection images
%                .num_cols       horizontal size of projection images
%                .num_iter       number of iterations
%                .vol_size_x     x size of output volume
%                .vol_size_y     y size of output volume
%                .vol_size_z     z size of output volume
%
%     INPUT (optional): varargin fields:
%                verbose : {false} | true <logical>
%                is_cone : {false} | true <logical>, for cone beam
%                epsilon : {1e-4} <double>. Tells how much confidence we
%                          have on the projection data. -1 would activate
%                          the standard update of the detector.
%
%     OUTPUT:  volume  <double vol_size_x, vol_size_y, vol_size_z>)
%
%
%     Version 002 26-09-2012 by LNervo
%       Formatting, cleaning, commenting

    conf = struct(...
        'verbose', false, ...
        'is_cone', false, ...
        'detector_norm', 'l2', ...
        'lambda', 1e-2, ...
        'rspace_oversize', 1, ...
        'rspace_super_sampling', 1, ...
        'epsilon', 0);
    conf = parse_pv_pairs(conf, varargin);

    if (~gtCheckGpu())
        error('ASTRA:no_GPU', ...
            'gtAstra3D: Submit this job on a GPU machine ! Exiting...');
    end

    if (isfield(proj, 'padding'))
        padding = proj.padding;
    else
        padding = 0;
    end

    use_astra_mex_direct = (exist('astra_mex_direct_c', 'file') == 3);
    if (~use_astra_mex_direct)
        error([mfilename ':wrong_configuration'], 'astra_mex_direct not available!')
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % projection geometry (type, #rows, #columns, vectors)
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if (conf.is_cone)
        proj_geom = astra_create_proj_geom('cone_vec', proj.num_rows, proj.num_cols, proj.geom);
    else
        proj_geom = astra_create_proj_geom('parallel3d_vec', proj.num_rows, proj.num_cols, proj.geom);
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % create volume and projection data objects (x, y, z)
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % proj_data should be a 3d "matrix" of size [#columns,#projections,#rows]
    vol_size = [proj.vol_size_x, proj.vol_size_y, proj.vol_size_z];
    vol_size = ceil(vol_size .* conf.rspace_oversize);
    vol_geom = astra_create_vol_geom(vol_size(1), vol_size(2), vol_size(3));

    opts = struct( ...
        'VoxelSuperSampling', conf.rspace_super_sampling, ...
        'DetectorSuperSampling', conf.rspace_super_sampling, ...
        'GPUindex', -1 );
    astra_projector_id = astra_create_projector('cuda3d', proj_geom, vol_geom, opts);

    has_psf = isfield(proj, 'psf') && ~isempty(proj.psf);
    if (has_psf)
        psf_obj = GtPSF();
        psf_obj.set_psf_direct(proj.psf, [proj.num_cols, proj.num_rows]);
    end
    fp = @(x)astra_mex_direct_c('FP3D', astra_projector_id, x);
    bp = @(x)astra_mex_direct_c('BP3D', astra_projector_id, x);

    if (has_psf)
        bp_weights = bp(psf_obj.apply_psf_adjoint(ones(size(proj.stack), 'single')));
        fp_weights = psf_obj.apply_psf_direct(fp(ones(size(bp_weights), 'single')));
    else
        bp_weights = bp(ones(size(proj.stack), 'single'));
        fp_weights = fp(ones(size(bp_weights), 'single'));
    end

    volume = zeros(size(bp_weights), class(bp_weights));
    volume_enh = volume;
    dual_tv = cat(4, volume, volume, volume);
    dual_det = zeros(size(fp_weights), class(fp_weights));

    sigma = 1 ./ (fp_weights + (fp_weights == 0));
    sigma_1 = 1 ./ (1 + sigma);
    tau = 1 ./ ((bp_weights + (bp_weights == 0)) + 6);

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % reconstruct
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    stats = GtTasksStatistics();
    stats.add_task('fp', 'Forward Projection')
    stats.add_task('f_psf', 'Direct PSF')
    stats.add_task('bp', 'Back Projection')
    stats.add_task('b_psf', 'Adjoint PSF')
    stats.add_task('dual_det_update', 'Update of Dual Detector')
    stats.add_task('dual_grad_update', 'Update of Dual Gradient')
    stats.add_task('primal_update', 'Update of Primal')
    stats.add_task('grad_comp', 'Computing the Gradient')
    stats.add_task('div_comp', 'Computing the Divergence')

    res_initial = gtMathsNorm_l2(proj.stack);
    residuals = [1; zeros(proj.num_iter, 1)];

    if (strcmpi(conf.detector_norm, 'kl'))
        data_term = 4 .* sigma .* proj.stack;
    else
        data_term = sigma .* proj.stack;
    end

    c = tic();
    fprintf('\b\b: ')
    for ii = 1:proj.num_iter
        if (conf.verbose)
            eta = toc(c) / (ii - 1) * (proj.num_iter - ii + 1) / 24 / 3600;
            if (~isinf(eta))
                eta = datestr(eta, 'dd HH:MM:SS.FFF');
            else
                eta = 'unknown';
            end
            char_num = fprintf('%02d/%02d (res: %f, ETA: %s)', ii-1, proj.num_iter, residuals(ii), eta);
        else
            char_num = fprintf('%02d/%02d (res: %f)', ii-1, proj.num_iter, residuals(ii));
        end

        stats.tic('fp')
        tomo_fp_tmp = fp(volume_enh);
        stats.toc('fp')
        if (has_psf)
            stats.tic('f_psf')
            tomo_fp_tmp = psf_obj.apply_psf_direct(tomo_fp_tmp);
            stats.toc('f_psf')
        end
        residuals(ii+1) = gtMathsNorm_l2(tomo_fp_tmp - proj.stack) / res_initial;

        stats.tic('dual_det_update')
        temp_dual_det = dual_det + sigma .* tomo_fp_tmp;
        switch (lower(conf.detector_norm))
            case 'l2'
                temp_dual_det_l2 = temp_dual_det - data_term;
                if (conf.epsilon > 0)
                    temp_dual_denom = temp_dual_det_l2 + (temp_dual_det_l2 == 0);
                    dual_det = temp_dual_det_l2 .* max(1 - sigma .* conf.epsilon ./ temp_dual_denom, 0);
                else
                    dual_det = temp_dual_det_l2 .* sigma_1;
                end
            case 'l1'
                temp_dual_det_l1 = temp_dual_det - data_term;
                dual_det = temp_dual_det_l1 ./ max(1, abs(temp_dual_det_l1));
            case 'kl'
                dual_det = (1 + temp_dual_det - sqrt((temp_dual_det - 1) .^ 2 + data_term)) ./ 2;
            otherwise
                error([mfilename ':wrong_argument'], ...
                    'Detector divergence term: "%s" not recognized', ...
                    conf.detector_norm)
        end
        stats.toc('dual_det_update')

        stats.tic('grad_comp')
        d = gradient(volume_enh);
        stats.toc('grad_comp')

        stats.tic('dual_grad_update')
        temp_dual_tv = dual_tv + d / 2;
        norm_dual_tv = max(1, sqrt(sum(temp_dual_tv .^ 2, 4)));
        dual_tv = temp_dual_tv ./ norm_dual_tv(:, :, :, [1 1 1]);
        stats.toc('dual_grad_update')

        if (has_psf)
            stats.tic('b_psf')
            dual_det_tmp = psf_obj.apply_psf_adjoint(dual_det);
            stats.toc('b_psf')
        else
            dual_det_tmp = dual_det;
        end
        stats.tic('bp')
        tomo_bp_tmp = bp(dual_det_tmp);
        stats.toc('bp')

        stats.tic('div_comp')
        div_tmp = divergence(dual_tv);
        stats.toc('div_comp')

        stats.tic('primal_update')
        volume_new = volume + (div_tmp * conf.lambda - tomo_bp_tmp) .* tau;
        volume_new(volume_new < 0) = 0;

        volume_enh = volume_new + (volume_new - volume);
        volume = volume_new;
        stats.toc('primal_update')

        fprintf(repmat('\b', [1, char_num]))
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % clean up
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    astra_mex_projector3d('delete', astra_projector_id);

    if (conf.verbose)
        fprintf('\n')
        stats.printStats()
        figure, semilogy(residuals)
    end

    res_norm = residuals(end);

    volume = volume( ...
        padding+1:end-padding, ...
        padding+1:end-padding, ...
        padding+1:end-padding );
end

function d = gradient(vol)
    dx = diff(padarray(vol, [1 0 0], 'post'), 1, 1);
    dy = diff(padarray(vol, [0 1 0], 'post'), 1, 2);
    dz = diff(padarray(vol, [0 0 1], 'post'), 1, 3);
    d = cat(4, dx, dy, dz);
end

function vol = divergence(d)
    dx = d(:, :, :, 1);
    dy = d(:, :, :, 2);
    dz = d(:, :, :, 3);
    ddx = diff(padarray(dx, [1 0 0], 'pre'), 1, 1);
    ddy = diff(padarray(dy, [0 1 0], 'pre'), 1, 2);
    ddz = diff(padarray(dz, [0 0 1], 'pre'), 1, 3);
    vol = ddx + ddy + ddz;
end
