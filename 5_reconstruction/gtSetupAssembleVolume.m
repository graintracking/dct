function [output, redo_list] = gtSetupAssembleVolume( update_cb )
% GTSETUPASSEMBLEVOLUME Helper function to wrap around gtAssembleVol3D.m

% input default parameters
par.savefilename            = 'DCT_vol.edf'; % filename to save volume
par.overlaps                = 'zero';        % how to handle overlapping grains ('zero' | 'summed' | 'conflict' | 'adaptive' | 'assign')
par.add_to_filename         = '';            % to add grains to a preexisting volume (saved as edf)
par.list                    = [];            % if list is [], load all grains.  Otherwise, only the list
par.display_error_messages  = true;          % show error messages

% check with user
list = cell(0,4);
list(end+1,:) = [{'savefilename'},{'filename to save volume (goes into 5_reconstruction/)'},{'char'},{1}];
list(end+1,:) = [{'overlaps'},{'how to treat overlapping grain labels ("zeros" or "summed")'},{'char'},{1}];
list(end+1,:) = [{'add_to_filename'},{'to add grains to an existing reconstruction (filename, path/filename)'},{'char'},{1}];
list(end+1,:) = [{'list'},{'list of grains to add to volume, or empty if "all" grains'},{'double'},{1}];
list(end+1,:) = [{'display_error_messages'},{'display error messages?'},{'logical'},{1}];

par = gtModifyStructure(par, list, [], 'Check with user:');

if ~exist('update_cb', 'var')
    [output, redo_list] = GtAssembleVol3D('savefilename', par.savefilename, ...
        'overlaps', par.overlaps, 'add_to_filename', par.add_to_filename, ...
        'list', par.list, 'display_error_messages', par.display_error_messages);
else
    [output, redo_list] = GtAssembleVol3D('savefilename', par.savefilename, ...
        'overlaps', par.overlaps, 'add_to_filename', par.add_to_filename, ...
        'list', par.list, 'display_error_messages', par.display_error_messages, ...
        'update_status_cb', update_cb);
end

end % end of function
