classdef GtGuiMultiThresholdVolume < GtGuiThresholdGrain
    properties
    end

    methods (Access = public)
        function self = GtGuiMultiThresholdVolume(vol, varargin)
            viewer_args = varargin;
            if ~ismember('f_title', varargin(1:2:end))
                viewer_args(end+1:end+2) = {'f_title', 'Volume Multiple Segmentation'};
            end
            self = self@GtGuiThresholdGrain(vol, viewer_args{:});

            self.setSegmentFunction(@(thr, morph, border)segmentVolumeMultiple(self));
        end

        function changeThreshold(obj, new_thresholds)
            for ii_t = 1:numel(obj.threshold)
                obj.setThresholds(new_thresholds(ii_t), ii_t);
                set(obj.conf.h_thr_slider(ii_t), 'Value', obj.threshold(ii_t));
            end

            set(obj.conf.h_thr_edit, 'String', sprintf(' %g,', obj.threshold));

            obj.updateDisplay();
        end

        function movedSlider(obj)
            new_thresh = get(obj.conf.h_thr_slider(1), 'Value');
            obj.setThresholds(new_thresh, 1);

            new_thresh = get(obj.conf.h_thr_slider(2), 'Value');
            obj.setThresholds(new_thresh, 2);

            set(obj.conf.h_thr_edit, 'String', sprintf(' %g,', obj.threshold));

            obj.updateDisplay();
        end

        function setThresholds(obj, new_threshold, n)
            obj.threshold(n) = new_threshold;
            if (obj.threshold(n) > obj.max_threshold)
                obj.threshold(n) = obj.max_threshold;
            elseif (obj.threshold(n) < obj.min_threshold)
                obj.threshold(n) = obj.min_threshold;
            end
        end
    end

    methods (Access = protected)
        function initParams(self, arguments)
            initParams@GtGuiThresholdGrain(self, arguments)

            self.threshold(2) = self.min_threshold;
        end

        function initGui(self)
            initGui@GtGuiThresholdGrain(self)

            % Let's disable zoom, while configuring
            zoomOp = zoom(self.conf.h_figure);
            zoomState = get(zoomOp, 'Enable');
            zoom(self.conf.h_figure, 'off');

            self.conf.h_thr_slider_label = uicontrol('Style', 'text', ...
                'Parent', self.conf.slider_contr_t_boxes, ...
                'HorizontalAlignment', 'left', 'String', 'N. Thr');
            self.conf.h_thr_slider_edit = uicontrol('Style', 'edit', ...
                'Parent', self.conf.slider_contr_t_boxes);

            self.conf.h_thr_slider(2) = uicontrol('Style', 'slider', ...
                'Parent', self.conf.sliders_t_boxes);

            set(self.conf.slider_main_t_boxes, 'Sizes', [30, -1]);
            set(self.conf.slider_contr_t_boxes, 'Sizes', [-1, -1]);
            set(self.conf.sliders_t_boxes, 'Sizes', [-1, -1]);
            set(self.conf.center_t_boxes, 'Sizes', [40, -1]);

            % Reset back the zoom to the previous state (before the inclusion)
            set(zoomOp, 'Enable', zoomState);
        end

        function resetUiComponents(self)
            resetUiComponents@GtGuiThresholdGrain(self);

            set(self.conf.h_thr_slider(1), 'Min',  self.min_threshold, ...
                'Max', self.max_threshold, 'Value', self.threshold(1), ...
                'SliderStep', [0.005 0.05]);
            set(self.conf.h_thr_slider(2), 'Min',  self.min_threshold, ...
                'Max', self.max_threshold, 'Value', self.threshold(2), ...
                'SliderStep', [0.005 0.05]);
        end

        function addUICallbacks(obj)
            addUICallbacks@GtGuiThresholdGrain(obj)

            obj.addUICallback(obj.conf.h_thr_slider(2), ...
                              'AdjustmentValueChangedCallback', ...
                              @(src, evt)movedSlider(obj), true);
        end

        function setVisualisedData(obj, image, vol, is_overlay)
            if (is_overlay)
                if (~isempty(obj.mask_border_voxels))
                    vol = gtVolumeMaskBorder(vol, 0, obj.mask_border_voxels);
                end
                slice = obj.loadSlice(vol);

                slice_2 = slice > obj.threshold(2);
                slice_1 = slice > obj.threshold(1) & slice <= obj.threshold(2);
                zero_slice = zeros(size(slice));
                slice = cat(3, slice_1, slice_2, zero_slice);
            else
                slice = obj.loadSlice(vol);

                slice_max = get(obj.conf.h_max, 'Value');
                slice_min = get(obj.conf.h_min, 'Value');
                slice = (slice - slice_min)/(slice_max - slice_min);
                slice(slice > 1) = 1;
                slice(slice < 0) = 0;
                slice = repmat(slice, [1,1,3]);
            end
            set(image, 'cdata', slice);
        end
    end

    methods (Access = protected)
        function segmentVolumeMultiple(self)
            filter = {'*.mat', 'MATLAB Files (*.mat)';
                      '*.edf', 'EDF files (ESRF Data Format, *.edf)'};
            [filename, pathname, filerIndex] = uiputfile(filter, 'Select file...', fullfile('5_reconstruction', 'volume_mask_multi.mat'));
            if (isnumeric(filename) && filename == 0)
                return;
            end

            thr = GtThreshold();
            vol = zeros(size(self.conf.vol), class(self.conf.vol));
            fprintf('Applying thresholds: ')
            for ii = 1:numel(self.threshold)
                num_chars = fprintf('%02d/%02d (thr: %g)', ii, numel(self.threshold), self.threshold(ii));

                partial_seg_struct = thr.volumeThreshold(self.conf.vol, self.threshold(ii), [], false, self.mask_border_voxels);

                vol = gtPlaceSubVolume(vol, single(partial_seg_struct.seg), partial_seg_struct.segbb(1:3), ii);

                fprintf(repmat('\b', [1, num_chars]));
            end
            fprintf(repmat(' ', [1, num_chars]));
            fprintf(repmat('\b', [1, num_chars]));
            fprintf('(%02d) Done.\n', numel(self.threshold))

            if (filerIndex == 1)
                final_seg_struct = struct('vol', vol, ...
                    'threshold', self.threshold, ...
                    'mask_border', self.mask_border_voxels); %#ok<NASGU>
                save(fullfile(pathname, filename), '-struct', 'final_seg_struct', '-v7.3');
            else
                edf_write(vol, fullfile(pathname, filename));
            end
        end
    end

    methods (Access = public, Static)
        function handles = compareVolumes(vols, varargin)
            handles = GtGuiMultiThresholdVolume.classCompareVolumes(vols, varargin{:});
        end
    end
end
