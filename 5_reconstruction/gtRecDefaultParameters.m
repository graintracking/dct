function par_rec = gtRecDefaultParameters(varargin)
% FUNCTION par_rec = gtRecDefaultParameters(varargin)
% Arguments can be (default in {}):
%   - absorption_algo   : {'SIRT'} | '2DFBP' | '3DTV'
%   - grains_algo       : {'SIRT'} | '6DL1' | '6DTV' | '6DTVL1'
%

    conf = struct( ...
        'absorption_algo', 'SIRT', ...
        'grains_algo', 'SIRT' );
    conf = parse_pv_pairs(conf, varargin);

    % Absorption volume options
    par_rec.absorption = gtRecAbsorptionDefaultParameters(conf.absorption_algo);

    % Grain volumes options
    par_rec.grains = gtRecGrainsDefaultParameters(conf.grains_algo);

    % Grain segmentation related values
    par_rec.thresholding = struct( ...
        'percentile', 20, 'do_morph_recon', true, 'do_region_prop', true, ...
        'percent_of_peak', 2.5, 'num_iter', 0, 'iter_factor', 1, ...
        'mask_border_voxels', 5, 'use_levelsets', false);
end
