function [varargout] = gtChangeThreshold3D(firstID, lastID, ~, phaseID, ~, varargin)
% GTCHANGETHRESHOLD3D
%     seg = gtChangeThreshold3D(firstID, lastID, ~, phaseID, varargin)
%     ----------------------------------------------------------------
%
%     Reads parameters.mat                     : acq.dir, rec
%     Reads 4_grains/phase_##/grain_****.mat   : vol, shift, id
%     Appends 4_grains/phase_##/grain_****.mat : seg, segbb, threshold, Area
%
%     INPUT:
%       varargin = to modify parameters.rec
%

    args = struct('list', [], 'verbose', false);
    args = parse_pv_pairs(args, varargin);

    if (isdeployed)
        global GT_DB
        global GT_MATLAB_HOME
        load('workspaceGlobal.mat');
        firstID = str2double(firstID);
        lastID  = str2double(lastID);
        if (ischar(phaseID))
            phaseID = str2double(phaseID);
        else
            warning('OAR:unexpected_behavior', ...
                'We should expect only strings from shell environment')
        end

        args.list = str2num(args.list); %#ok<ST2NM>
    end
    % This handles OAR's habbit of passing lists of IDs in a field called
    % 'list'
    args.grain_ids = args.list;
    args = rmfield(args, 'list');

    args = [fieldnames(args), struct2cell(args)]';
    thresh = GtThreshold([], args{:});
    if (nargout >= 1)
        varargout{1} = thresh.batchThreshold(phaseID, firstID, lastID);
    else
        thresh.batchThreshold(phaseID, firstID, lastID);
    end
end

