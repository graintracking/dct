function s = gtMathsSegmentConstrainedLevelset(x, mus, varargin)
    conf = struct( ...
        'lambda_tv', 1, ...
        'lambda_smooth', 1e-2, ...
        'iterations', 100, ...
        'lower_limit', [], ...
        'upper_limit', [] );

    num_vol_dims = numel(size(x));
    mus_size = [ones(1, num_vol_dims), numel(mus)];
    mus_exp = reshape(mus, mus_size);
    W_prime = bsxfun(@minus, x, mus_exp);
    W_prime = W_prime .^ 2;

    W_second = 1 ./ (W_prime + eps .* (W_prime == 0));
    W_mus = bsxfun(@times, W_second, 1 ./ sum(W_second, 4));

    sigma_tv = 0.5;
    sigma_smooth = 1.0 ./ (4.0 .* num_vol_dims);

    tau = 1 / (sum(W_mus + (W_mus == 0), num_vol_dims+1) + (2 .* conf.lambda_tv + 4 .* conf.lambda_smooth) .* num_vol_dims);

    s = gtMathsGetSameSizeZeros(x);
    [~, max_inds] = max(W_mus, [], num_vol_dims+1);
    s(:) = mus(max_inds);
    se = s;

    q_mus = zeros([size(x), numel(mus)]);
    qtv = cell(num_vol_dims, 1);
    for ii_d = 1:num_vol_dims
        qtv{ii_d} = gtMathsGetSameSizeZeros(x);
    end
    ql = gtMathsGetSameSizeZeros(x);

    for ii = 1:conf.iterations
        q_mus = q_mus + bsxfun(@minus, mus_exp, se);
        q_mus = q_mus ./ max(1, abs(q_mus));

        d = gtMathsGradient(se);
        den = gtMathsGetSameSizeZeros(x);
        for ii_d = 1:num_vol_dims
            qtv{ii_d} = qtv{ii_d} + d{ii_d} .* sigma_tv;
            den = den + qtv{ii_d} .^ 2;
        end
        den = sqrt(den);
        for ii_d = 1:num_vol_dims
            qtv{ii_d} = qtv{ii_d} ./ max(1, den);
        end

        ql = ql + gtMathsLaplacian(se) .* sigma_smooth;
        ql = ql ./ max(1, abs(ql));

        sn = s + tau .* (sum(W_mus .* q_mus, num_vol_dims+1) ...
            + conf.lambda_tv .* gtMathsDivergence(qtv) ...
            - conf.lambda_smooth .* gtMathsLaplacian(ql));

        if (~isempty(conf.lower_limit))
            sn = max(sn, conf.lower_limit);
        end
        if (~isempty(conf.upper_limit))
            sn = min(sn, conf.upper_limit);
        end

        se = sn + (sn - s);
        s = sn;
    end
end