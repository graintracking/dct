function rotmatcomp = gtMathsRotationMatrixComp(rotdir, rc)
% GTMATHSROTATIONMATRIXCOMP
%     rotmatcomp = gtMathsRotationMatrixComp(rotdir, rc)
%     ----------------------------------------------
%
%     Given an arbitrary rotation axis in 3D space, returns the three 
%     matrices in Rodrigues's formula (see below). The matrices can be
%     used to compose the rotation matrix for any angle. It considers 
%     right-handed rotation for a positive angle.
%
%     Output to be used with row or column vectors: 
%        1. For ROW VECTORS:     rowvector_rotated = rowvector*Srot
%        2. For COLUMN VECTORS:  colvector_rotated = Srot*colvector
%     NOTE: Using the matrix on the wrong side gives the inverse result!
%
%     Rodrigues's rotation formula describes a linear combination of three
%     matrices with only cos(om) and only sin(om) dependent terms:
%       Srot = Sconst + Scos*cos(om) + Ssin*sin(om)
%         Srot: 3x3 rotation matrix 
%                 for row vectors:     rowvector_rotated = rowvector*Srot
%                 for column vectors:  colvector_rotated = Srot*colvector
%         om  : rotation angle
%
%     To get Srot from the components, use gtMathsRotationTensor.
%
%
%     INPUT
%       rotdir - rotation axis direction in LAB coordinates for right-handed
%                rotation; unit vector (1x3xn for row, 3x1xn for column vectors)
%       rc     - 'row' for row vectors, 'col' for column vectors
%
%     OUTPUT
%       rotmatcomp.const = matrix Sconst (3x3xn)
%       rotmatcomp.cos   = matrix Scos   (3x3xn)
%       rotmatcomp.sin   = matrix Ssin   (3x3xn)

    num_vecs = size(rotdir, 3);

    I = eye(3);
    I = I(:, :, ones(num_vecs, 1));

    z = zeros(1, 1, num_vecs);

    rotdir_perm = permute(rotdir, [2 1 3]);

    if (strcmp(rc, 'row') && (size(rotdir, 1) == 1) ...
            && (size(rotdir, 2) == 3))

        rotmatcomp.const = rotdir_perm(:, [1 1 1], :) .* rotdir([1 1 1], :, :);

        rotmatcomp.cos = I - rotmatcomp.const;
        rotmatcomp.sin = [ ...
                           z,  rotdir(1, 3, :), -rotdir(1, 2, :) ; ...
            -rotdir(1, 3, :),                z,  rotdir(1, 1, :) ; ...
             rotdir(1, 2, :), -rotdir(1, 1, :),                z ];
    elseif (strcmp(rc, 'col') && (size(rotdir, 1) == 3) ...
            && (size(rotdir, 2) == 1))

        rotmatcomp.const = rotdir_perm([1 1 1], :, :) .* rotdir(:, [1 1 1], :);

        rotmatcomp.cos = I - rotmatcomp.const;
        rotmatcomp.sin = [ ...
                           z, -rotdir(3, 1, :),  rotdir(2, 1, :) ; ...
             rotdir(3, 1, :),                z, -rotdir(1, 1, :) ; ...
            -rotdir(2, 1, :),  rotdir(1, 1, :),                z ];
    else
        error('Wrong input format. See function help.')
    end
end
