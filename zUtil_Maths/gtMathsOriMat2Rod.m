function r = gtMathsOriMat2Rod(g)
% GTMATHSROTMAT2EULER  Convert orientation matrices to Rodrigues vectors.
%
%     r = gtMathsOriMat2Rod(g)
%     -------------------------------------------------------------------------
%
%     INPUT:
%       g = <double>  Orientation matrices (3x3xN array).
%                     See Randle & Engler, 2000 for more details about g.
%
%     OUTPUT:
%       r = <double>  Rodrigues vectors, as column vectors (3xN array).
%
%     Version 003 23-02-2021 by Zheheng Liu
%       Discuss the case when rotation angle close to 180
%
%     Version 002 17-10-2012 by YGuilhem
%       Vectorize function to process quickly lots of orientation matrices
%
%     Version 001 15-06-2012 by YGuilhem

% Check size of g matrices
if (~isreal(g))
    gtError('gtMathsOriMat2Rod:wrong_input_type', ...
        'Wrong input type, wait a 3x3xN real array!');
elseif (size(g, 1)~=3 || size(g, 2)~=3)
    gtError('gtMathsOriMat2Rod:wrong_input_size', ...
        'Input array g should be sized 3x3xN!');
end
r = zeros(3, 1, size(g, 3));
% Set epsilon to avoid singular values
epsilon = 1.e-10;

% Compute orientation matrices traces
g_trace_plus1 = g(1, 1, :) + g(2, 2, :) + g(3, 3, :) + 1;

% Find regular cases
ind0 = abs(g_trace_plus1) >= epsilon;
r(:, 1, ind0) = [ g(2, 3, ind0) - g(3, 2, ind0) ; ...
      g(3, 1, ind0) - g(1, 3, ind0) ; ...
      g(1, 2, ind0) - g(2, 1, ind0) ] ./ g_trace_plus1([1, 1, 1], 1, ind0);

%%%%%% special case: the rotation angles are close to 180 %%%%%%
inds_array = 1:size(g, 3);
ind1 = inds_array(~ind0);
if numel(ind1)
    % Use rotation axes divided by epsilon to approximate very large
    % Rodrigues vectors.
    % Calculate the absolute values of rotation axes divided by epsilon
    r(:, 1, ind1) = sqrt(abs([g(1, 1, ind1) - g(2, 2, ind1) - g(3, 3, ind1) + 1; ...
        g(2, 2, ind1) - g(1, 1, ind1) - g(3, 3, ind1) + 1; ...
        g(3, 3, ind1) - g(2, 2, ind1) - g(1, 1, ind1) + 1])) / (2 * epsilon);

    %%%%%% rotation angle is not 180 (sin(w) > 0) %%%%%%
    % Since g23 - g32 = 2sin(w)n1 and sin(w) >= 0
    % if g23 < g32, n1 is negative
    bool_tmp = g(2, 3, ind1) < g(3, 2, ind1);
    r(1, 1, ind1(bool_tmp)) = -r(1, 1, ind1(bool_tmp));
    % g31 - g13 = 2sin(w)n2. If g31 < g13, n2 is negative
    bool_tmp = g(3, 1, ind1) < g(1, 3, ind1);
    r(2, 1, ind1(bool_tmp)) = -r(2, 1, ind1(bool_tmp));
    % g12 - g21 = 2sin(w)n3. If g12 < g21, n3 is negative
    bool_tmp = g(1, 2, ind1) < g(2, 1, ind1);
    r(3, 1, ind1(bool_tmp)) = -r(3, 1, ind1(bool_tmp));

    %%%%%% rotation angle is 180 (sin(w) = 0) %%%%%%
    % If g23 = g32 and g31 = g13, it could be sin(w) = 0 or (n1 and n2 = 0)
    bool_tmp = (g(2, 3, ind1) == g(3, 2, ind1)) & (g(1, 3, ind1) == g(3, 1, ind1));
    ind2 = ind1(bool_tmp);
    % g12 + g21 = 4sin^2(w/2)*n1*n2. If g21 < -g12, then n1, n2 are not
    % zero and rotation angle is 180.
    % Let n1 be positive, so n2 is negative.
    bool_tmp = g(2, 1, ind2) < -g(1, 2, ind2);
    r(2, 1, ind2(bool_tmp)) = -r(2, 1, ind2(bool_tmp));
    % g13 + g31 = 4sin^2(w/2)*n1*n2. If g31 < -g13, then n1, n3 are not
    % zero and rotation angle is 180.
    % Let n1 be positive, so n3 is negative
    bool_tmp = g(3, 1, ind2) < -g(1, 3, ind2);
    r(3, 1, ind2(bool_tmp)) = -r(3, 1, ind2(bool_tmp));

    % If g21 = -g12, then n1 = 0 || n2 = 0
    bool_tmp = g(2, 1, ind2) == -g(1, 2, ind2);
    ind3 = ind2(bool_tmp);
    % g23 + g32 = 4sin^2(w/2)*n2*n3. If n2*n3 < 0, then n2 is nonzero, so
    % n1 must be zero and rotation angle is 180.
    % Let n2 be positive, so n3 is negative
    bool_tmp = (g(2, 3, ind3) + g(3, 2, ind3)) < 0;
    r(3, 1, ind3(bool_tmp)) = -r(3, 1, ind3(bool_tmp));
end
%%%%%% end of the discuss about the special cases

% Reshape resulting matrix 
% r = squeeze(r);
r = reshape(r, 3, []); % Runs faster than squeeze

end % end of function
