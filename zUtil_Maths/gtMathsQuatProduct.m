function qout = gtMathsQuatProduct(q1, q2, flag_conj_q1, flag_conj_q2)
% FUNCTION qout = gtMathsQuatProduct(q1, q2)
% NOTE: Being qc = gtMathsQuatProduct(qa, qb), it is equivalent to:
%   qc = gtMathsOriMat2Quat(gtMathsQuat2OriMat(qa) * gtMathsQuat2OriMat(qb))
% (inspired by function gtMathsRodSum)
% calculate q1*q2 = (w1+i*x1+j*y1+k*z1)*(w2+i*x2+j*y2+k*z2). i^2 = j^2 =
% k^2 = -1 and i*j*k = -1.
% If flag_conj_q1 is true, calculate conj(q1)*... =
% (w1-i*x1-j*y1-k*z1)*...; if flag_conj_q2 is true calculate ...*conj(q2) =
% ...*(w2-i*x2-j*y2-k*z2)
    num_q1 = size(q2, 2);
    num_q2 = size(q1, 2);
    if (num_q1 ~= num_q2)
        if (num_q1 == 1)
            q2 = q2 * ones(1, num_q2);
        elseif (num_q2 == 1);
            q1 = q1 * ones(1, num_q1);
        else
            error('gtMathsQuatProduct:wrong_arguments', ...
                'Dimensions of the input vectors should be consistent')
        end
    end

    if nargin > 2
        if flag_conj_q1
            q1 = sfQuatConj(q1);
        end
        if nargin > 3 && flag_conj_q2
            q2 = sfQuatConj(q2);
        end
    end
    qout = bsxfun(@(x, y) x .* y, q2(1, :), q1) + ...
        (([-1; 1; 1;-1] * q2(2, :)) .* q1([2; 1; 4; 3], :)) + ...
        (([-1;-1; 1; 1] * q2(3, :)) .* q1([3; 4; 1; 2], :)) + ...
        (([-1; 1;-1; 1] * q2(4, :)) .* q1([4; 3; 2; 1], :));
end

% Quaternion conjugate
function q_conj = sfQuatConj(q)
    q_conj = bsxfun(@(x, y) x .* y, [1;-1;-1;-1], q);
end