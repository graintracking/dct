function q = gtMathsRod2Quat(r)
% GTMATHSROD2QUAT  Convert Rodrigues vectors to quaternions.
%
%     q = gtMathsRod2Quat(r)
%     ---------------------------------------------------------------------
%
%     INPUT:
%       r = <double>  Rodrigues vectors, as column vectors (3xN array).
%
%     OUTPUT:
%       q = <double>  Quaternions (4xN array).
%
%     Version 001 06-07-2021 by Zheheng (inspired by Yoann's function gtMathsRod2Euler)

% Check size of r column vectors
if size(r, 1) ~= 3
    gtError('Rodrigues:wrong_input_size', ...
        'Input array r should be sized 3xN!');
end
% |r| = tan(theta/2), so sqrt(1+|r|^2) = 1/cos(theta/2)
cosine_half_theta_reciprocal = sqrt(1 + sum(abs(r) .^ 2, 1));
% q = [cos(theta/2); cos(theta/2)*r] = [1; r] * cos(theta/2)
q = bsxfun(@rdivide, [ones(size(cosine_half_theta_reciprocal)); r], cosine_half_theta_reciprocal);
end % end of function
