function vol = gtMathsUpsampleVolume(vol, scale, method)
% FUNCTION vol = gtMathsUpsampleVolume(vol, scale)

    if (~exist('method', 'var') || isempty(method))
        method = 'linear';
    end

    if (numel(scale) > 1 || scale ~= round(scale))
        vol = upsample_size(vol, scale, method);
    else
        if (iscell(vol))
            for ii = 1:numel(vol)
                vol{ii} = gtMathsUpsampleVolume(vol{ii}, scale, method);
            end
        else
            vol = upsample_interp_interleaving(vol, scale, method);
        end
    end
end

function vol = upsample(vol, scale)
% The fastest!
    vol_in_size = size(vol);
    for ii_dim = numel(vol_in_size):-1:1
        indx{ii_dim} = 1:vol_in_size(ii_dim);
        indx{ii_dim} = indx{ii_dim}(ones(scale, 1), :);
        indx{ii_dim} = reshape(indx{ii_dim}, [], 1);
    end
    vol = vol(indx{:});
end

function vol = upsample_interp_interleaving(vol, scale, method)
% Faster than the custom made, bu still way slower than the simple one
    num_size_4 = size(vol, 4);
    if (num_size_4 == 1)
        vol = interpn(vol, scale-1, method, 0);
    else
        for ii4 = num_size_4:-1:1
            new_vol(:, :, :, ii4) = interpn(vol(:, :, :, ii4), scale-1, method, 0);
        end
        vol = new_vol;
    end
end

function vol = upsample_size(vol, scales, method)
% It does not handle borders correctly but it's good enough for the moment
    vol_in_size = size(vol);
    num_dims = max(numel(vol_in_size), numel(scales));

    scales(numel(scales)+1:num_dims) = 1;
    vol_in_size(numel(vol_in_size)+1:num_dims) = 1;

    vol_out_size = vol_in_size .* scales;
    ranges_in = (vol_in_size - 1) / 2;
    ranges_out = (vol_out_size - 1) ./ (2 .* scales);

    indx_in = cell(1, num_dims);
    indx_out = cell(1, num_dims);
    for ii_d = 1:num_dims
        indx_in{ii_d} = linspace(-ranges_in(ii_d), ranges_in(ii_d), vol_in_size(ii_d));
        indx_out{ii_d} = linspace(-ranges_out(ii_d), ranges_out(ii_d), vol_out_size(ii_d));
    end
    grid_out = cell(1, num_dims);
    [grid_out{:}] = ndgrid(indx_out{:});
    if (~strcmpi(method, 'nearest') && ~ismember(class(vol), {'single', 'double'}))
        vol = single(vol);
    end
    vol = interpn(indx_in{:}, vol, grid_out{:}, method, 0);
end


function vol = upsample_interp(vol, scale, method)
% It does exactly what the interleaving does, but it is here as a tutorial
% for future reerence
    vol_in_size = size(vol);
    num_dims = numel(vol_in_size);
    for ii_dim = num_dims:-1:1
        indx_out{ii_dim} = 1:(1/scale):vol_in_size(ii_dim);
    end
    grid_out = cell(1, num_dims);
    [grid_out{:}] = ndgrid(indx_out{:});
    vol = interpn(vol, grid_out{:}, method, 0);
end
