function x = gtMathsSumNDVol(x)
% FUNCTION x = gtMathsSumNDVol(x)
%

    x = sum(sum(x));
    x = reshape(x, [], 1);
    x = sum(x);
end