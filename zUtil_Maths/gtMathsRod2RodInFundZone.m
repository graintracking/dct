function r_vec = gtMathsRod2RodInFundZone(r_vec, symm, shift)
    if (exist('shift', 'var'))
        r_vec = fast_map(r_vec, symm, shift);
    else
        r_vec = fast_map(r_vec, symm);
    end
end

function r_vec = fast_map(r_vec, symm, shift)
    num_symm_ops = 2 * numel(symm);

    % Expanding the symmetry operators and the matrix to be checked, to
    % parallelize the matrix product
    g3 = cat(3, symm(:).g3);
    g3 = cat(3, g3, permute(g3, [2 1 3]));

    if (exist('shift', 'var'))
        g_shift = gtMathsRod2OriMat(shift);
        for ii = 1:size(g3, 3);
            g3(:, :, ii) = g3(:, :, ii) * g_shift;
        end
    end

    inds = 1:size(g3, 3);
    inds = inds([1 1 1], :);
    inds = reshape(inds, [], 1);
    g3 = g3(:, :, inds);

    g = gtMathsRod2OriMat(r_vec);
    g = reshape(g, 1, [], 1, size(g, 3));
    % Cannot unroll more, because of memory occupation blowing up, resulting in
    % computational efficiency problem!
    unroll = 16;
    g3 = g3(:, :, :, ones(unroll, 1));
    for ii = 1:unroll:size(g, 4)
        iis = ii:min(ii+unroll-1, size(g, 4));
        num_x_time = numel(iis);
        gg = g([1 1 1], :, ones(num_symm_ops, 1), iis);
        gg = reshape(gg, 3, 3, [], num_x_time);

        gg = reshape(sum(gg .* g3(:, :, :, 1:num_x_time), 2), 3, 3, []);
        r_vecs = gtMathsOriMat2Rod(gg);
        r_vecs = reshape(r_vecs, 3, [], num_x_time);

        dists = 2 * atand(sqrt(sum(r_vecs .^ 2, 1)));

        [~, ind_min] = min(dists, [], 2);

        r_vecs = reshape(r_vecs, 3, []);
        ind_min = reshape(ind_min, [], 1) + (0:(num_x_time-1))' .* num_symm_ops;
        r_vec(:, iis) = r_vecs(:, ind_min);
    end
end

function r_vec = slow_map(r_vec, symm)
    g = gtMathsRod2OriMat(r_vec);

    num_symm_ops = numel(symm);

    r_vecs = zeros(2 * num_symm_ops, 3);

    for ii = 1:num_symm_ops
        r_vecs((ii - 1) * 2 + 1, :) = gtMathsOriMat2Rod(symm(ii).g3 * g);
        r_vecs((ii - 1) * 2 + 2, :) = gtMathsOriMat2Rod(symm(ii).g3' * g);
    end

    dists = 2 * atand(sqrt(sum(r_vecs .^ 2, 2)));

    [~, ii] = min(dists);

%     r_vecs

    r_vec = r_vecs(ii, :)';
end
