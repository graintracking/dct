function r = gtMathsQuat2Rod(q)
% GTMATHSQUAT2ROD  Convert quaternions to Rodrigues vectors.
%
%     r = gtMathsQuat2Rod(q)
%     -------------------------------------------------------------------------
%
%     INPUT:
%       q = <double>  Quaternions (4xN array).
%
%     OUTPUT:
%       r = <double>  Rodrigues vectors, as column vectors (3xN array).
%
%     Version 001 06-07-2021 by Zheheng (inspired by Yoann's function gtMathsOriMat2Rod)

% Check size of quaternions
if (~isreal(q))
    gtError('gtMathsQuat2Rod:wrong_input_type', ...
        'Wrong input type, wait a 4xN real array!');
elseif (size(q, 1)~=4)
    gtError('gtMathsQuatRod:wrong_input_size', ...
        'Input array q should be sized 4xN!');
end

% Set epsilon to avoid r infinitely large
epsilon = 1.e-9;

q(1, and(q(1, :) >= 0, q(1, :) < epsilon)) = epsilon;
q(1, and(q(1, :) < 0, q(1, :) > -epsilon)) = -epsilon;
r = bsxfun(@rdivide, q(2:4, :), q(1, :));

end % end of function
