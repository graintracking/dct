function angdeg = gtMathsVectorsAngles(v1, v2)
% GTMATHSVECTORSANGLES Angles in degrees between 3D UNIT vectors.
% Function is out-of-date! Use one of these instead:
%   angrad = gtMathsVectorsAnglesRad(v1,v2,colvec)
%   angdeg = gtMathsVectorsAnglesDeg(v1,v2,colvec)
%

disp('Function is out-of-date! Use one of these instead:')
disp('   angrad = gtMathsVectorsAnglesRad(v1,v2,colvec)')
disp('   angdeg = gtMathsVectorsAnglesDeg(v1,v2,colvec)')

error('Function is out-of-date!')
end
