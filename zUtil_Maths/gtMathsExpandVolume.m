function arr = gtMathsExpandVolume(arr, ssampling, mask, varargin)
    conf = struct(...
        'method', 'linear', ...
        'is_spatial_coords', false, ...
        'padding', 'edge', ...
        'permute_output', false ...
    );
    conf = parse_pv_pairs(conf, varargin);

    arr_size = [size(arr, 1), size(arr, 2), size(arr, 3)];
    has_mask = exist('mask', 'var') && ~isempty(mask);

    if (has_mask)
        mask_size = [size(arr, 1), size(arr, 2), size(arr, 3)];

        if (~strcmpi(conf.method, 'linear'))
            error([mfilename ':wrong_argument'], ...
                'Only linear method is allowed when using a mask. Method %s was passed instead', ...
                method)
        elseif (any(arr_size ~= mask_size))
            error([mfilename ':wrong_argument'], 'Array and mask sizes should match.')
        end
    end

    if (conf.is_spatial_coords)
        % This is not real padding, but rather linear extrapolation
        arr = cat(1, 2 * arr(1, :, :) - arr(2, :, :), arr, 2 * arr(end, :, :) - arr(end-1, :, :));
        arr = cat(2, 2 * arr(:, 1, :) - arr(:, 2, :), arr, 2 * arr(:, end, :) - arr(:, end-1, :));
        arr = cat(3, 2 * arr(:, :, 1) - arr(:, :, 2), arr, 2 * arr(:, :, end) - arr(:, :, end-1));

        if (has_mask && ~isempty(conf.padding))
            mask = pad_array_1(mask, conf.padding);
        end
    else
        arr = pad_array_1(arr, conf.padding);

        if (has_mask)
            mask = pad_array_1(mask, @zeros);
        end
    end

    corner = (1 - (1 / ssampling)) / 2;
    xx_src = linspace(2 - corner, arr_size(1) + 1 + corner, ssampling * arr_size(1));
    yy_src = linspace(2 - corner, arr_size(2) + 1 + corner, ssampling * arr_size(2));
    zz_src = linspace(2 - corner, arr_size(3) + 1 + corner, ssampling * arr_size(3));
    [xx_src, yy_src, zz_src] = ndgrid(xx_src, yy_src, zz_src);

    if (has_mask)
        [inds, ints] = gtMathsGetInterpolationIndices([xx_src(:), yy_src(:), zz_src(:)], [], class(mask), 0);

        xx_dst = 1:ssampling * arr_size(1);
        yy_dst = 1:ssampling * arr_size(2);
        zz_dst = 1:ssampling * arr_size(3);

        [xx_dst, yy_dst, zz_dst] = ndgrid(xx_dst, yy_dst, zz_dst);
        dst_inds = cat(2, xx_dst(:), yy_dst(:), zz_dst(:));
        dst_inds = dst_inds(:, :, ones(8, 1));
        dst_inds = permute(dst_inds, [1, 3, 2]);
        dst_inds = reshape(dst_inds, [], 3);

        valid = ints > eps('single') & all(inds > 0, 2) ...
            & inds(:, 1) <= arr_size(1) & inds(:, 2) <= arr_size(2) & inds(:, 3) <= arr_size(3);
        inds = inds(valid, :);
        ints = ints(valid, :);
        dst_inds = dst_inds(valid, :);

        src_inds = sub2ind(size(arr), inds(:, 1), inds(:, 2), inds(:, 3));
        src_vox_vals = arr(src_inds);
        src_vox_ints = mask(src_inds);

        weight_ints = src_vox_ints .* ints;
        src_ints = src_vox_vals .* weight_ints;

        dst_vox_vals = accumarray(dst_inds, src_ints, size(xx_src));
        dst_vox_weights = accumarray(dst_inds, weight_ints, size(xx_src));
        arr = dst_vox_vals ./ (dst_vox_weights + (dst_vox_weights == 0));
    else
        % Matlab requires X and Y to be swapped
        arr = interp3(arr, yy_src, xx_src, zz_src, conf.method);
    end

    if (conf.permute_output)
        arr = reshape(arr, [ssampling, arr_size(1), ssampling, arr_size(2), ssampling, arr_size(3)]);
        arr = permute(arr, [2 4 6 1 3 5]);
        arr = reshape(arr, 1, arr_size(1) * arr_size(2) * arr_size(3), ssampling ^ 3);
    end
end

function arr = pad_array_1(arr, func)
    numdims = ndims(arr);
    slicing = repmat({':'}, 1, numdims);

    for ii = 1:numdims
        sizes = size(arr);
        sizes(ii) = 1;

        if (ischar(func) && strcmpi(func, 'edge'))
            slicing_s = slicing;
            slicing_s{ii} = 1;
            slicing_e = slicing;
            slicing_e{ii} = size(arr, ii);

            arr = cat(ii, arr(slicing_s{:}), arr, arr(slicing_e{:}));
        elseif isa(func, 'function_handle')
            arr = cat(ii, func(sizes), arr, func(sizes));
        elseif isscalar(func)
            arr = cat(ii, func * ones(sizes), arr, func * ones(sizes));
        else
            error([mfilename ':pad:wrong_argument'], 'Unknown padding argument')
        end
    end
end

