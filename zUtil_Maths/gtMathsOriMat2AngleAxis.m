function [theta, r] = gtMathsOriMat2AngleAxis(g)
% GTMATHSORIMAT2ANGLEAXIS  Convert orientation matrices to angle/axis rotations.
%
%     [theta, r] = gtMathsAngleAxis2OriMat(g)
%     ---------------------------------------------------------------------
%
%     INPUT:
%       g     = <double>  Orientation matrices (3x3xN array).
%
%     OUTPUT:
%       theta = <double>  Rotation angles, in degrees (1xN array).
%       r     = <double>  Rotation axes, as column vectors (3xN array).
%
%     Version 001 27-06-2013 by YGuilhem, yoann.guilhem@esrf.fr

    % Check size of input
    if (size(g, 1) ~= 3 || size(g, 2) ~=3)
        gtError('gtMathsOriMat2AngleAxis:wrong_input_size', ...
             'Input array g should be sized 3x3xN!');
    end

    % Compute (trace-1)/2, check that it really lies between -1 and 1
    tmp = (g(1, 1, :) + g(2, 2, :) + g(3, 3, :) - 1) / 2;
    tmp = max(min(tmp, 1), -1);

    % Compute rotation angle theta
    theta = acosd(tmp);

    if (nargout > 1)
        % Compute rotation axes
        r = [ ...
            g(2, 3, :) - g(3, 2, :) ; ...
            g(3, 1, :) - g(1, 3, :) ; ...
            g(1, 2, :) - g(2, 1, :) ];
        r = reshape(r, 3, []);

        % Normalize rotation axes
        n = sqrt(sum(r.*r, 1));
        r = r ./ n([1 1 1], :);

        % Special case theta = 0: rCase0 = [1 0 0];
        specialCase0 = (theta < 5.e-5);
        nCase0 = sum(specialCase0(:));
        r(:, specialCase0) = [ones(1, nCase0); zeros(2, nCase0)];

        % Special case theta = 180
        specialCase180 = (abs(theta - 180) < 1.e-6);
        special_r_180 = sqrt((1 + [ ...
            g(1, 1, specialCase180); ...
            g(2, 2, specialCase180); ...
            g(3, 3, specialCase180)] ) / 2);
        r(:, specialCase180) = reshape(special_r_180, 3, []);
    end
end
