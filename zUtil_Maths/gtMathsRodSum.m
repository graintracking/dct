function r_vecs_gvdm = gtMathsRodSum(r_vecs_gvdm, shift_r_vec_gvdm)
% FUNCTION r_vecs_gvdm = gtMathsRodSum(r_vecs_gvdm, shift_r_vec_gvdm)
%   from: Frank, F.C. - 1988 - Orientation Mapping
%    and: Kumar, A. and Dawson P.R. - 2000 - Computational modeling of
%           F.C.C. deformation textures over Rodrigues' space
% Because the original formula from Frank1988 seems to get it wrong, but it
% is still a good reference from the understanding point of view.
%
% NOTE: Being Rc = gtMathsRodSum(Ra, Rb), it is equivalent to:
%   Rc = gtMathsOriMat2Rod(gtMathsRod2OriMat(Rb) * gtMathsRod2OriMat(Ra))
% and not to:
%   Rc = gtMathsOriMat2Rod(gtMathsRod2OriMat(Ra) * gtMathsRod2OriMat(Rb))
%

    num_r_vecs = size(r_vecs_gvdm, 2);
    num_shifts = size(shift_r_vec_gvdm, 2);

    if (num_shifts == num_r_vecs)
        Ra = r_vecs_gvdm;
        Rb = shift_r_vec_gvdm;
    elseif (num_r_vecs == 1)
        Ra = r_vecs_gvdm(:, ones(num_shifts, 1));
        Rb = shift_r_vec_gvdm;
    elseif (num_shifts == 1)
        Ra = r_vecs_gvdm;
        Rb = shift_r_vec_gvdm(:, ones(num_r_vecs, 1));
    else
        error('gtMathsRodSum:wrong_arguments', ...
            'Dimensions of the input vectors should be consistent')
    end
    Ra_cross_Rb = cross(Ra, Rb, 1);

    ones_minus_Ra_dot_Rb = 1 - sum(Ra .* Rb, 1);

    r_vecs_gvdm = (Ra + Rb + Ra_cross_Rb) ./ ones_minus_Ra_dot_Rb([1 1 1], :);
end
