function vol = gtMathsSumCellVolumes(vols, dim)
% FUNCTION vol = gtMathsSumCellVolumes(vols, dim)
%

    if (~exist('dim', 'var') || isempty(dim) || (dim < 0))
        vol = vols{1};
        for n = 2:numel(vols)
            vol = vol + vols{n};
        end
    else
        size_vols = size(vols);
        if (numel(size_vols) < dim)
            size_vols(end+1:dim) = 1;
        end
        temp_size = [dim, 1:dim-1, dim+1:numel(size_vols)];

        vols = permute(vols, temp_size);
        vols = reshape(vols, size_vols(dim), []);

        vol = cell(1, size(vols, 2));
        for ii_c = 1:size(vols, 2)
            vol{ii_c} = gtMathsSumCellVolumes(vols(:, ii_c));
        end

        out_size = [size_vols(1:dim-1), 1, size_vols(dim+1:end)];
        vol = reshape(vol, out_size);
    end
end