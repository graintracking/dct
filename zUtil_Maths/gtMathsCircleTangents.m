% FUNCTION tanpoints = gtMathsCircleTangents(point,circcent,circrad,circpl)
%
% Computes the contact points of tangents from an arbitrary point in 3D 
% space to an arbitrary planar circle in 3D space.
% 
% INPUT 
%   point    - point coordinates (1,3)
%   circcent - coordinates of the circle's center (1,3)
%   circrad  - circle radius (scalar)
%   circpl   - circle plane normal (doesn't have to be a unit vector) (1,3) 
%
% OUTPUT
%   tanpoints - the tangent points coordinates (1,3); empty if no solution
%


function tanpoints = gtMathsCircleTangents(point,circcent,circrad,circpl)

% orthogonal projection of the point onto the circle's plane:
pproj = gtMathsProjectPointsOnPlane(point,circpl,circcent,circpl);

% vector from point to circle center:
cvec = circcent - pproj;

lsqr = sum(cvec.^2);
rsqr = circrad^2;

% check if point is inside circle
if lsqr <= rsqr
	tanpoints = [];
	return
end
	
% length of vector 'a'
alength = sqrt(lsqr*circrad^2/(lsqr-circrad^2));

% vector 'a'
avec    = cross(cvec,circpl);
avec    = avec/norm(avec)*alength;

% tangent vector directions
tanvec1 = circcent + avec - pproj;
tanvec2 = circcent - avec - pproj;

% tangent points
tanpoints(1,1:3) = tanvec1/norm(tanvec1)*sqrt(lsqr-rsqr) + pproj;
tanpoints(2,1:3) = tanvec2/norm(tanvec2)*sqrt(lsqr-rsqr) + pproj;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Check results
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Uncomment to plot results for checking:
% 
% figure
% hold on
% axis equal
% 
% alp = 1:360;
% 
% nvec = [-circpl(2) circpl(1) 0];
% nvec = nvec/norm(nvec);
% nnvec = cross(circpl,nvec);
% nnvec = nnvec/norm(nnvec);
% 
% circps = repmat(circcent',1,length(alp)) + nvec'*sind(alp)*circrad + nnvec'*cosd(alp)*circrad;
% 
% plot3(circps(1,:),circps(2,:),circps(3,:))
% plot3(tanpoints(1,1),tanpoints(1,2),tanpoints(1,3),'r*') 
% plot3(tanpoints(2,1),tanpoints(2,2),tanpoints(2,3),'r*')
% plot3([tanpoints(1,1),point(1)],[tanpoints(1,2),point(2)],[tanpoints(1,3),point(3)],'r-')
% plot3([tanpoints(2,1),point(1)],[tanpoints(2,2),point(2)],[tanpoints(2,3),point(3)],'r-')
% plot3([pproj(1),point(1)],[pproj(2),point(2)],[pproj(3),point(3)],'k-')


end
  