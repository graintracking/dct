function pproj = gtMathsProjectPointsOnPlane(points,projvec,plpos,plnorm)
% FUNCTION pproj = gtMathsProjectPointsOnPlane(points,projvec,plpos,plnorm)
% 
% Projects a list of points onto a plane along a projection vector. Result
% is computed even if the projection vector directs away from the plane, and
% independent from which side of the plane the points are located.
% 
% INPUT 
%   points  - array of the point coordinates (n,3)
%   projvec - projection vector (doesn't have to be a unit vector) (1,3) 
%   plpos   - plane position defined by a point in the plane (1,3)
%   plnorm  - plane normal (UNIT vector!) (1,3)
%
% OUTPUT
%   pproj   - array of the points coordinates projected onto the plane (n,3)
% 

replOnes = ones(size(points,1), 1);

% pproj = points + ( ( (repmat(plpos,size(points,1),1)-points)*plnorm') / (projvec*plnorm') ) * projvec ;
pproj = points + ( ((plpos(replOnes, :) - points) * plnorm') / (projvec*plnorm')) * projvec;

end



