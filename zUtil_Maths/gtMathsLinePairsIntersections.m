function cps = gtMathsLinePairsIntersections(bl,cl)

% FUNCTION  cps = gtMathsLinePairsIntersections(bl,cl)
%
% Gives the closest point ('point of intersection') pair-wise between a 
% given base line and other 3D lines.
%
% INPUT:  bl(1x6) - base line coordinates, direction vector normalized
%                     [posX posY posZ norm_dir_X norm_dir_Y norm_dir_Z] 
%         cl(nx6) - other lines coordinates, direction vector normalized
%                     [posX posY posZ norm_dir_X norm_dir_Y norm_dir_Z] =
%
% OUTPUT: cps(nx3) - closest point for each line pair (`intersection point`)
%

% The intersection point is sought as the tip of a triangle made up by the 
% two lines in their mid-plane that is parallel to both lines.
 

% Plane normal: cross product of bl and cl-s
pln = [bl(5)*cl(:,6) - bl(6)*cl(:,5), ...
       bl(6)*cl(:,4) - bl(4)*cl(:,6), ...
       bl(4)*cl(:,5) - bl(5)*cl(:,4)];

% Normalise pln; will be [NaN NaN NaN] for lines parallel with bc
ll  = sqrt(sum(pln.*pln,2));
pln = pln./ll(:,[1 1 1]);

% Vector from B to C
bc = cl(:,1:3) - bl(ones(size(cl,1),1),1:3);

% bc out-of-plane component
dotpln = sum(bc.*pln,2);
bcout  = pln.*dotpln(:,[1 1 1]);

% normalised bc in-plane component and its length before normalisation
bcin = bc - bcout;
ll   = sqrt(sum(bcin.*bcin,2));
llp  = ll > 0;
bcin(llp,:) = bcin(llp,:)./ll(llp,[1 1 1]);  

% Cross product of bl and cl direction with bcin
cr_bl_bcin = [bl(5)*bcin(:,3) - bl(6)*bcin(:,2), ...
              bl(6)*bcin(:,1) - bl(4)*bcin(:,3), ...
              bl(4)*bcin(:,2) - bl(5)*bcin(:,1)];
          
cr_cl_bcin = [cl(:,5).*bcin(:,3) - cl(:,6).*bcin(:,2), ...
              cl(:,6).*bcin(:,1) - cl(:,4).*bcin(:,3), ...
              cl(:,4).*bcin(:,2) - cl(:,5).*bcin(:,1)];
          
          
% Sin of angle between bl and cl and bcin
sinb = sqrt(sum(cr_bl_bcin.*cr_bl_bcin,2));
sinc = sqrt(sum(cr_cl_bcin.*cr_cl_bcin,2));


% Sign of dot product indicate towards which side of bc they point  
bls = sign(sum(cr_bl_bcin.*pln,2));
cls = sign(sum(cr_cl_bcin.*pln,2));

% Find the ones with opposite signs; those cl-s are flipped
fl = (bls.*cls == -1);

cl(fl,4:6) = -cl(fl,4:6);


% Dot product with in-plane bc
bdot = bcin*bl(4:6)';
cdot = sum(bcin.*cl(:,4:6),2);


% Multiplicator for B direction from triangle
mb = ll./(bdot - cdot.*sinb./sinc);
mb(ll==0) = 0;

% Intersection points calculated starting from bl position
cps = bl(ones(size(mb,1),1),1:3) + mb*bl(4:6) + 0.5*bcout;


