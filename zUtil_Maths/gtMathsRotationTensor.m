function Srot = gtMathsRotationTensor(om, rotcomp)
% FUNCTION Srot = gtMathsRotationTensor(om, rotcomp)
%
% Computes the rotation tensor given the 3 matrices in Rodrigues' 
% rotation formula. Can be used for computing rotation around an arbitrary
% axis.
%
% Rodrigues's rotation formula describes a linear combination of
% matrices with cos(om) only and sin(om) only dependent terms:
%   Srot = Sconst + Scos * cos(om) + Ssin * sin(om)
%     Srot: is the right side rotation tensor (for row vector):
%      vec_rotated = vec * Srot
%     om  : rotation angle
%
% INPUT
%   om            - rotation angle (in degrees)
%   rotcomp.const - Sconst matrix
%   rotcomp.cos   - Scos matrix
%   rotcomp.sin   - Ssin matrix
%
% OUTPUT
%   Srot          - rotation matrix

    cosomr = reshape(cosd(om), 1, []);
    sinomr = reshape(sind(om), 1, []);

    constr = reshape(rotcomp.const, [], 1);
    cosr = reshape(rotcomp.cos, [], 1);
    sinr = reshape(rotcomp.sin, [], 1);

    % Multiply using outer product
    Srot = constr(:, ones(numel(om), 1)) + cosr * cosomr + sinr * sinomr;

    % Reshape final output
    Srot = reshape(Srot, 3, 3, []);
end
