function q = gtMathsOriMat2Quat(g)
% GTMATHSROTMAT2QUAT  Convert orientation matrices to quaternions.
%
%     q = gtMathsOriMat2Quat(g)
%     -------------------------------------------------------------------------
%
%     INPUT:
%       g = <double>  Orientation matrices (3x3xN array).
%                     See Randle & Engler, 2000 for more details about g.
%
%     OUTPUT:
%       q = <double>  Quaternions, as column vectors (4xN array).
%
%     Version 001 15-02-2021 by Zheheng (inspired by Yoann's function gtMathsOriMat2Rod)

% Check size of g matrices
if (~isreal(g))
    gtError('gtMathsOriMat2Quat:wrong_input_type', ...
        'Wrong input type, wait a 3x3xN real array!');
elseif (size(g, 1)~=3 || size(g, 2)~=3)
    gtError('gtMathsOriMat2Quat:wrong_input_size', ...
        'Input array g should be sized 3x3xN!');
end

% Set epsilon to avoid singular values
epsilon = 0.1;

q = zeros(4, 1, size(g, 3));

% Compute orientation matrices traces
q0_square = (g(1, 1, :) + g(2, 2, :) + g(3, 3, :) + 1) / 4; % q0^2

% ind0: where rotation angles are not that large (q0^2 > epsilon)
[indices_small_q0, ind0] = sfCreateNewIndices(q0_square > epsilon, 1:size(g, 3));
q(1, 1, ind0) = sqrt(q0_square(1, 1, ind0));% q0 = sqrt(1+g11+g22+g33)/2
% Since g23 - g32 = 4q0q1, g31 - g13 = 4q0q2, g12 - g21 = 4q0q3,
% q1 = (g23 - g32)/4q0, q2 = (g31 - g13)/4q0, q3 = (g12 - g21)/4q0
q(2:4, 1, ind0) = ([ g(2, 3, ind0) - g(3, 2, ind0) ; ...
    g(3, 1, ind0) - g(1, 3, ind0) ; ...
    g(1, 2, ind0) - g(2, 1, ind0) ] ./ ...
    q([1, 1, 1], 1, ind0)) / 4;

%%%%%% special case: q0 is close to 0, i.e., rotation angle is close to 180
if numel(indices_small_q0)
    %%%%%% For the q with q0^2 <= epsilon and q1^2 > epsilon %%%%%%
    q1_square = (g(1, 1, indices_small_q0) - g(2, 2, indices_small_q0) - g(3, 3, indices_small_q0) + 1) / 4;
    bool_indices = q1_square > epsilon;

    % ind1: where x coordinate of rotation axis is not that small (q1^2 > epsilon)
    [indices_small_q0q1, ind1] = sfCreateNewIndices(bool_indices, indices_small_q0);
    q(2, 1, ind1) = sqrt(q1_square(1, 1, bool_indices));% q1 = sqrt(1+g11-g22-g33)/2
    % Since g23 - g32 = 4q1q0, g21 + g12 = 4q1q2, g13 + g31 = 4q1q3
    % q0 = (g23 - g32)/4q1, q2 = (g12 + g21)/4q1, q3 = (g31 + g13)/4q1
    q([1, 3, 4], 1, ind1) = ([ g(2, 3, ind1) - g(3, 2, ind1) ; ...
        g(2, 1, ind1) + g(1, 2, ind1) ; ...
        g(1, 3, ind1) + g(3, 1, ind1) ] ./ ...
        q([2, 2, 2], 1, ind1)) / 4;

    %%%%%%special case: q1 is also close to 0, i.e., x coordinate of rotation axis is close to 0
    if numel(indices_small_q0q1(:))
        %%%%%% For the q with q0^2, q1^2 <= epsilon and q2^2 > epsilon
        q2_square = (g(2, 2, indices_small_q0q1) - g(1, 1, indices_small_q0q1) - g(3, 3, indices_small_q0q1) + 1) / 4;
        bool_indices = q2_square > epsilon;

        % ind2: where y coordinate of rotation axis is not that small (q2^2 > epsilon)
        [ind3, ind2] = sfCreateNewIndices(bool_indices, indices_small_q0q1);
        q(3, 1, ind2) = sqrt(qi_square(1, 1, bool_indices));
        q([1, 2, 4], 1, ind2) = ([ g(3, 1, ind2) - g(1, 3, ind2) ; ...
            g(2, 1, ind2) + g(1, 2, ind2) ; ...
            g(2, 3, ind2) + g(3, 2, ind2) ] ./ ...
            q([3, 3, 3], 1, indices_large_qi)) / 4;

        %%%%%%special case: q2 is also close to 0, i.e., y coordinate of rotation axis is close to 0
        if numel(ind3(:))
            %%%%%% For the q with q0^2, q1^2, q2^2 <= epsilon
            q(4, 1, ind3) = sqrt(g(3, 3, ind3) - g(2, 2, ind3) - g(1, 1, ind3) + 1) / 2;
            q([1, 2, 3], 1, ind3) = ([ g(1, 2, ind3) - g(2, 1, ind3) ; ...
                g(3, 1, ind3) + g(1, 3, ind3) ; ...
                g(2, 3, ind3) + g(3, 2, ind3) ] ./ ...
                q([4, 4, 4], 1, ind3)) / 4;
        end
    end
end
%%%%%% end of the discuss about the special cases.

% Reshape resulting matrix 
% q = squeeze(q);
q = reshape(q, 4, []); % Runs faster than squeeze

% If q0 < 0, then let q0 = -q0
bool_indices = q(1, :) < 0;
q(:, bool_indices) = -q(:, bool_indices);

end % end of function

function [indices_small_qi_out, indices_large_qi_out] = sfCreateNewIndices(bool_qi_larger_than_epsilon_in, indices_small_qi_in)
    indices_large_qi_out = indices_small_qi_in(bool_qi_larger_than_epsilon_in);
    indices_small_qi_out = indices_small_qi_in(~bool_qi_larger_than_epsilon_in);
end