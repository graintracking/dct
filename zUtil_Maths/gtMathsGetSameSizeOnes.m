function ones_vol = gtMathsGetSameSizeOnes(vol, data_type)
% FUNCTION ones_vol = gtMathsGetSameSizeOnes(vol, data_type)
%

    if (~exist('data_type', 'var'))
        data_type = [];
    end

    if (iscell(vol))
        ones_vol = cell(size(vol));
        for n = 1:numel(vol)
            ones_vol{n} = gtMathsGetSameSizeOnes(vol{n}, data_type);
        end
    else
        if (isempty(data_type))
            data_type = class(vol);
        end
        ones_vol = ones(size(vol), data_type);
    end
end