% Tells if a point is inside a convex polygon, defined by its face planes.

function ok=gtIsPointInPolyhedron(p,faces)
% 
% ok = all( (p-f0)*fdir<=0 )
%

ok=all(sum((repmat(p,size(faces,1),1)-faces(:,1:3)).*faces(:,4:6),2)<=1e-10);

