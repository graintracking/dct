function zeros_vol = gtMathsGetSameSizeZeros(vol, data_type)
% FUNCTION zeros_vol = gtMathsGetSameSizeZeros(vol, data_type)
%

    if (~exist('data_type', 'var'))
        data_type = [];
    end

    if (iscell(vol))
        zeros_vol = cell(size(vol));
        for n = 1:numel(vol)
            zeros_vol{n} = gtMathsGetSameSizeZeros(vol{n}, data_type);
        end
    else
        if (isempty(data_type))
            data_type = class(vol);
        end
        zeros_vol = zeros(size(vol), data_type);
    end
end