function [vout,ind] = gtMathsUniqueTol(vin, tol, sorted)

% Returns the unique elements of A. Tol is used as the absolut difference
% for determining uniqueness. If A is sorted, set sorted to 1 to gain speed.

if isempty(vin)
    vout = [];
    ind  = [];
    return
end

if isrow(vin)
    col = false;
elseif iscolumn(vin)
    col = true;
else
    error('Needs a row or column vector as input.')
end

if ~sorted
    [vin, indorig] = sort(vin);
else
    if col
        indorig = (1:length(vin))';
    else
        indorig = 1:length(vin);
    end
end

indsorted = 1;
ilast     = 1;
vout      = vin(1);

for ii = 2:length(vin)
   if abs(vin(ilast) - vin(ii)) >= tol
       
      vout      = [vout vin(ii)];
      indsorted = [indsorted ii];
      ilast     = ii; % if increments are small, this basis A should be kept
      
   end
end

ind = indorig(indsorted);

if col
    vout = vout';
end


