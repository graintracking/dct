function new_vectors = rotateVectors(vectors, varargin)
% ROTATEVECTORS  Rotates list of vectors counterclockwise of 'angle' in degrees 
%                around 'axis' 
%
%     new_vectors = rotateVectors(vectors, varargin)
%     ----------------------------------------------
%     You can give mrot (3x3) or angle/axis pair.
%
%     INPUT:
%       vectors     = <double>      list of row vectors (Nx3)
%
%     OPTIONAL INPUT (varargin):
%       angle       = <double>      rotation angle in degrees (1x1) {[]}
%       axis        = <double>      rotation axis (1x3) {[0 0 1]}
%       mrot        = <double>      rotation matrix (3x3) {eye(3)}
%       cell2mat    = <logical>     transform the cell output into a matrix (Nx3) {true}
%       debug       = <logical>     print comments on screen {false}
%
%     OUTPUT:
%       new_vectors = <double/cell> list of row vectors (Nx3)
%
%
%     Version 001 14-02-2013 by LNervo


app.angle    = [];
app.axis     = [0 0 1];
app.mrot     = eye(3);
app.cell2mat = true;
app.debug    = false;
app = parse_pv_pairs(app, varargin);

out = GtConditionalOutput(app.debug);

print_structure(app, 'options:', false, app.debug)

if ~isequaln(app.mrot, eye(3))
    R = app.mrot;
elseif ~isempty(app.angle)
    if isRowVectorWithLength(app.axis,3) 
        R = rotationmat3D(app.angle, app.axis);
    elseif iscolumn(app.axis) && numel(app.axis)==3
        app.axis = app.axis';
        R = rotationmat3D(app.angle, app.axis);
    else
        gtError('rotateVectors:wrongArgument','The given rotation axis is not recognized...Quitting')
    end
else
    gtError('rotateVectors:missingArgument','The angle/axis information is missing...Quitting')
end


if size(vectors,2) ~=3 && size(vectors,1) == 3
    vectors = vectors';
end
N = size(vectors,1);


new_vectors = arrayfun(@(num) (R * vectors(num,:)')', 1:N, 'UniformOutput', false)';

if app.cell2mat
    new_vectors = reshape([new_vectors{:}],3,N)';
end

if ~isempty(app.angle) && ~isempty(app.axis)
    out.odisp(['Done rotation of ' num2str(app.angle) ' around ' num2str(app.axis) ' axis counterclockwise !'])
elseif ~isequaln(app.mrot, eye(3))
    out.odisp('Equivalent to do a rotation using the matrix R :')
    out.odisp(R)
end

end % end of function

