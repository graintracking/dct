function [cord, phi1, phi2] = gtMathsCordLength(r1, r2, d)
    %% (half) angle and (half) cord length of two intersecting circles 
    cord = sqrt(4.*d.*d.*r1.*r1 - (d*d - r2.*r2 + r1.*r1).^2) / (2*d);
    phi1 = asind(cord ./ r1);
    phi2 = asind(cord ./ r2);
end