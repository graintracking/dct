function square_norm = gtMathsSquareNorm(x)
% FUNCTION square_norm = gtMathsSquareNorm(x)
%

    if (iscell(x))
        square_norm = 0;
        for n = 1:numel(x)
            square_norm = square_norm + gtMathsSquareNorm(x{n});
        end
    else
        square_norm = gtMathsSumNDVol(x .* x);
    end
end