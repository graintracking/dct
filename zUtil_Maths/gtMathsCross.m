function res = gtMathsCross(vec1, vec2, dim)
% FUNCTION res = gtMathsCross(vec1, vec2)
% 
%   Computes the cross product of vectors 1 and 2.
%   Vectors should be ROW vectors.
%   If multiple lines exist, then it computes the cross product of each
%   line.
%   Can be 1 order of magnitude faster than MATLAB's cross built-in function.
%

    if (~exist('dim', 'var') || isempty(dim))
        dim = 2;
    end
    switch (dim)
        case 1
            res = [ vec1(2, :, :) .* vec2(3, :, :) - vec1(3, :, :) .* vec2(2, :, :), ...
                    vec1(3, :, :) .* vec2(1, :, :) - vec1(1, :, :) .* vec2(3, :, :), ...
                    vec1(1, :, :) .* vec2(2, :, :) - vec1(2, :, :) .* vec2(1, :, :) ];
        case 2
            res = [ vec1(:, 2, :) .* vec2(:, 3, :) - vec1(:, 3, :) .* vec2(:, 2, :), ...
                    vec1(:, 3, :) .* vec2(:, 1, :) - vec1(:, 1, :) .* vec2(:, 3, :), ...
                    vec1(:, 1, :) .* vec2(:, 2, :) - vec1(:, 2, :) .* vec2(:, 1, :) ];
        case 3
            res = [ vec1(:, :, 2) .* vec2(:, :, 3) - vec1(:, :, 3) .* vec2(:, :, 2), ...
                    vec1(:, :, 3) .* vec2(:, :, 1) - vec1(:, :, 1) .* vec2(:, :, 3), ...
                    vec1(:, :, 1) .* vec2(:, :, 2) - vec1(:, :, 2) .* vec2(:, :, 1) ];
    end
end
