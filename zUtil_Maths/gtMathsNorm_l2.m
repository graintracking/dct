function l2norm = gtMathsNorm_l2(x)
% FUNCTION l2norm = gtMathsNorm_l2(x)
%

    l2norm = sqrt(gtMathsSquareNorm(x));
end