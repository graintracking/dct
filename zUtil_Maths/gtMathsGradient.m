function d = gtMathsGradient(x)
% FUNCTION d = gtMathsGradient(x)
% Computes the gradient of an n-dimensional object and outputs a cell
% concatenation of the directional gradients.

    dims = size(x);
    num_dims = numel(dims);
    d = cell(num_dims, 1);
    for ii_d = 1:num_dims
        padsize = zeros(1, num_dims);
        padsize(ii_d) = 1;

        try
            pad_x = padarray(x, padsize, 'post');
        catch mexc
            if (strcmpi(mexc.identifier, 'MATLAB:license:checkouterror'))
                pad_x = cat(ii_d, x, zeros(~padsize * (dims(ii_d) - 1) + 1));
            else
                rethrow(mexc)
            end
        end

        d{ii_d} = diff(pad_x, 1, ii_d);
    end
end