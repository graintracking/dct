function g = gtMathsEuler2OriMat(euler, convention)
% GTMATHSEULER2ROTMAT  Convert orientation matrices to Euler angles (degrees).
%                      These follow Bunge convention (ZXZ) [phi1; phi; phi2].
%
%     g = gtMathsEuler2OriMat(euler)
%     -------------------------------------------------------------------------
%
%     INPUT:
%       euler = <double>  Euler angles triplets, as column vectors (3xN array).
%
%     OUTPUT:
%       g     = <double>  Orientation matrices (3x3xN array).
%                         See Randle & Engler, 2000 for more details about g.
%
%     Version 002 17-10-2012 by YGuilhem
%       Vectorize function to process quickly lots of Euler triplets
%
%     Version 001 15-06-2012 by YGuilhem

% Check size of column vectors
if size(euler, 1) ~= 3 
    gtError('gtMathsEuler2OriMat:wrong_input_size', ...
        'Input array euler should be sized 3xN!');
end

if (~exist('convention', 'var'))
    convention = 'ZXZ';
end

% Get number of Euler triplets
N = size(euler, 1);

% Reshape angles components
euler = reshape(euler, 3, 1, []);

switch (convention)
    case 'ZXZ'
        phi1 = euler(1, :, :);
        phi  = euler(2, :, :);
        phi2 = euler(3, :, :);

        % Compute intermediate values
        c1 = cosd(phi1);
        cg = cosd(phi);
        c2 = cosd(phi2);
        s1 = sind(phi1);
        sg = sind(phi);
        s2 = sind(phi2);

        % Compute g matrices components
        g = [  c1.*c2 - s1.*cg.*s2 ,  s1.*c2 + c1.*cg.*s2 ,  sg.*s2 ; ...
              -c1.*s2 - s1.*cg.*c2 ,  c1.*cg.*c2 - s1.*s2 ,  sg.*c2 ; ...
               s1.*sg              , -c1.*sg              ,  cg     ];
    case 'XYZ'
        phi = euler(1, :, :);
        theta = euler(2, :, :);
        psi = euler(3, :, :);

        % Compute intermediate values
        c_phi = cosd(phi);
        c_th = cosd(theta);
        c_psi = cosd(psi);
        s_phi = sind(phi);
        s_th = sind(theta);
        s_psi = sind(psi);

        % Compute g matrices components
        g = [ c_th .* c_psi,                           c_th.*s_psi,                             -s_th; ...
              s_phi .* s_th .* c_psi - c_phi .* s_psi, s_phi .* s_th .* s_psi + c_phi .* c_psi,  c_th .* s_phi; ...
              c_phi .* s_th .* c_psi + s_phi .* s_psi, c_phi .* s_th .* s_psi - s_phi .* c_psi,  c_th .* c_phi ];
end

end % end of function
