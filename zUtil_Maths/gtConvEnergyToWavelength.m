function lambda = gtConvEnergyToWavelength(energy)
% GTCONVENERGYTOWAVELENGTH Converts X-ray energy to wavelength.
%
% wavelength = gtConvEnergyToWavelength(energy)
%
% -----------------------------------------
% 
% INPUT
%   energy  - energy in keV
%
% OUTPUT
%   lambda  - wavelength in angstroms
%

lambda = 12.3984428./energy ;

end
