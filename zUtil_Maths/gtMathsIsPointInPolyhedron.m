function ok = gtMathsIsPointInPolyhedron(p, faces)
% GTMATHSISPOINTINPOLYHEDRON Computes if points 'p' are inside the 
% 3D polyhedron specified in 'faces'.
%
%   ok = gtMathsIsPointInPolyhedron(p, faces)
%
%   INPUT:  p(mx3)     - [X, Y, Z] coordinates of the point
%           faces(nx6) - coordinates of the facet planes of the polyhedron
%                        [p0x p0y p0z pnormx pnormy pnormz]
%                        pnorm vectors point out of the volume
%
%   OUTPUT: ok(m, 1)    - true where the points are inside the polyhedron


    % Tile p and faces vertically size(p, 1) times, so all computation can
    % be done on one matrix

    % p indices
    p1 = 1:size(p, 1);
    p1 = p1(ones(size(faces, 1), 1), :);

    % f indices
    f1 = (1:size(faces, 1))';
    f1 = f1(:, ones(size(p, 1), 1));

    aa = p(p1, :) - faces(f1, [1 2 3]);
    aa = (sum(aa .* faces(f1, [4 5 6]), 2) <= 1e-10);

    ok = all(reshape(aa, size(faces, 1), size(p, 1)), 1)';
end