% Gives outliers in a set of values.

function out=gtOutliers(values,tolfact,meanv,stdv)

if nargin<3
  meanv=mean(values);
  stdv=std(values);
end
  
  out=(values>meanv+tolfact*stdv) | (values<meanv-tolfact*stdv);

end
