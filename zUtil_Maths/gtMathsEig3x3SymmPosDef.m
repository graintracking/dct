function [e, V] = gtMathsEig3x3SymmPosDef(M, toll_deg)
    if (~exist('toll_deg', 'var') || isempty(toll_deg))
        toll_deg = 1e5;
    end
    num_matrs = size(M, 3);
    e = zeros(1, 3, num_matrs, class(M));

    diag_M = gtMathsDiag3x3(M);
    diag_M = reshape(diag_M, 1, 3, []);

    % Trace
    trM = sum(diag_M, 2);

    M23_M13 = M([2 1], 3, :);
    M12_2 = M(1, 2, :) .^ 2;

    p1 = sum(M23_M13 .^ 2, 1) + M12_2;

    trivials = p1 == 0;
    if (any(trivials)) 
        e(1, :, trivials) = diag_M(1, :, trivials);
    end

    others = ~trivials;
    if (any(others))
        q_3 = trM(1, 1, others);
        q = q_3 ./ 3;

        diag_M_minus_q = diag_M(1, :, others) - q(1, [1 1 1], :);

        tr_M_minusqI_2 = sum(diag_M_minus_q .^ 2, 2);
        p2 = tr_M_minusqI_2 + 2 .* p1(1, 1, others);
        p = sqrt(p2 ./ 6);
        p_1 = 1 ./ (p + (p == 0));

        M_minusqI = M(:, :, others);
        M_minusqI = gtMathsDiag3x3(M_minusqI, diag_M_minus_q);
        B = bsxfun(@times, p_1, M_minusqI);

        r = gtMathsDet3x3SymmPosDef(B) / 2;

        phi = acos(r) / 3;
        phi(r <= -1) = pi / 3;
        phi(r >= 1) = 0;

        p_2 = 2 .* p;

        e(1, 1, others) = q + p_2 .* cos(phi);
        e(1, 3, others) = q + p_2 .* cos(phi + (2 * pi / 3));
        e(1, 2, others) = q_3 - e(1, 1, others) - e(1, 3, others);
    end

    e = abs(e);

    V = zeros(3, 3, num_matrs, 'like', M);

    M23M12 = M(2, 3, :) .* M(1, 2, :);
    M12M13 = M(1, 2, :) .* M(1, 3, :);

    M22 = bsxfun(@minus, M(2, 2, :), e);
    M11 = bsxfun(@minus, M(1, 1, :), e);

    V(1, :, :) = bsxfun(@minus, bsxfun(@times, M(1, 3, :), M22), M23M12);
    V(2, :, :) = bsxfun(@minus, bsxfun(@times, M(2, 3, :), M11), M12M13);
    V(3, :, :) = bsxfun(@minus, M12_2, M11 .* M22);

    % Let's take care of the degeneracies
    deg_12 = abs(e(1, 1, :) - e(1, 2, :)) < toll_deg;
    deg_13 = abs(e(1, 1, :) - e(1, 3, :)) < toll_deg;
    deg_23 = abs(e(1, 2, :) - e(1, 3, :)) < toll_deg;

    V(:, 2, deg_12) = gtMathsCross(V(:, 1, deg_12), V(:, 3, deg_12), 1);
    V(:, 3, deg_13) = gtMathsCross(V(:, 1, deg_13), V(:, 2, deg_13), 1);
    V(:, 3, deg_23) = gtMathsCross(V(:, 1, deg_23), V(:, 2, deg_23), 1);

    norm_V = sqrt(sum(V .^ 2, 1));
    norm_V = 1 ./ (norm_V + (norm_V == 0));
    V = bsxfun(@times, V, norm_V);
end
