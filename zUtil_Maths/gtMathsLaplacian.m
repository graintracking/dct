function dd = gtMathsLaplacian(x)
% FUNCTION dd = gtMathsLaplacian(x)
% Computes the divergence of an n-dimensional vector object.

    dims = size(x);
    num_dims = numel(dims);
    dd = cell(num_dims, 1);
    for ii_d = 1:num_dims
        padsize = zeros(1, num_dims);
        padsize(ii_d) = 1;

        dd{ii_d} = diff(padarray(x, padsize, 'both'), 2, ii_d);
    end

    dd = gtMathsSumCellVolumes(dd);
end