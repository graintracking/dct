function distance = gtMathsPointLinesDists(pp,ll)
%
% FUNCTION  distance = gtMathsPointLinesDists(pp,ll)
%
% Gives the Euclidean distances between a point and a list of lines in 
% 3D space.
%
% INPUT:  pp(1x3) - coordinates of the point
%         ll(nx6) - coordinates of lines (direction vector normalised!)
%                   [ posX posY posZ  norm_dir_X norm_dir_Y norm_dir_Z ]
%
% OUTPUT: distance(n,1) - distances
%


% Vectors from the point to the lines
dd = pp(ones(size(ll,1),1),:)-ll(:,1:3);

% Cross product
cr = [dd(:,2).*ll(:,6) - dd(:,3).*ll(:,5), ...
      dd(:,3).*ll(:,4) - dd(:,1).*ll(:,6), ...
      dd(:,1).*ll(:,5) - dd(:,2).*ll(:,4)];

% Length of cr includes a sin factor, therefore it equals to the distance
distance = sqrt(sum(cr.*cr,2));
