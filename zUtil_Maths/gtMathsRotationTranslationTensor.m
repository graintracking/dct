function t = gtMathsRotationTranslationTensor(t_axis, t_angles, t_orig)
% function t = gtMathsRotationTranslationTensor(t_axis, t_angles, t_orig)
%
% It returns a <4 x 4 x n> stack of rotation + translation tensors

    rot_comp = gtMathsRotationMatrixComp(t_axis', 'col');
    t = eye(4);
    t = repmat(t, [1, 1, numel(t_angles)]);
    t(1:3, 1:3, :) = gtMathsRotationTensor(t_angles, rot_comp);

    t_tr_pre = eye(4);
    t_tr_pre(:, 4) = [-t_orig'; 1];

    t_tr_post = eye(4);
    t_tr_post(:, 4) = [t_orig'; 1];

    t = gtMathsMatrixProduct(t_tr_post, gtMathsMatrixProduct(t, t_tr_pre));
end
