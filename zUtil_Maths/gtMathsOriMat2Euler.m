function euler = gtMathsOriMat2Euler(g)
% GTMATHSROTMAT2EULER  Convert orientation matrices to Euler angles (degrees).
%                      These follow Bunge convention (ZXZ) [phi1, phi, phi2].
%
%     euler = gtMathsOriMat2Euler(g)
%     -------------------------------------------------------------------------
%
%     INPUT:
%       g     = <double>  Orientation matrices (3x3xN array).
%                         See Randle & Engler, 2000 for more details about g.
%
%     OUTPUT:
%       euler = <double>  Euler angles triplets, as column vectors (3xN array).
%                         Output angles are in degrees and only positive.
%
%     Version 001 15-06-2012 by YGuilhem
%       Vectorize function to process quickly lots of orientations matrices
%
%     Version 001 15-06-2012 by YGuilhem

% Check size of g matrix
if (~isreal(g))
    gtError('gtMathsOriMat2Euler:wrong_input_type', ...
        'Wrong input type, wait a 3x3xN real array!');
elseif (size(g, 1) ~= 3 || size(g, 2) ~= 3)
    gtError('gtMathsOriMat2Euler:wrong_input_size', ...
        'Input array g should be sized 3x3xN!');
end

% Set epsilon to avoid singular values
epsilon = 1.e-9;

% Find singular and regular cases
singular = abs(g(3, 3, :) - 1) < epsilon;
regular  = ~singular;

phi1 =        double(singular).*atan2(g(1, 2, :),  g(1, 1, :));
phi1 = phi1 + double(regular ).*atan2(g(3, 1, :), -g(3, 2, :));

phi  = double(regular).*acos(g(3, 3, :));

phi2 =        double(singular).*phi1;
phi2 = phi2 + double(regular ).*atan2(g(1, 3, :), g(2, 3, :));

% Gather all Euler angles in an array
euler = [ phi1 ; phi ; phi2 ] * 180 / pi;

% Shift to positive angles range
euler = mod(euler, 360.);

% Reshape to get column vectors
euler = reshape(euler, 3, []);

end % end of function
