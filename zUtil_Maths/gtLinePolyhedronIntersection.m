function isec=gtLinePolyhedronIntersection(l,p)
%
% FUNCTION is=gtLinePolygonIntersection(l,p)
%
% Computes the intersection of a line and the surface of a convex polygon,
%  defined by its face planes.
%
% INPUT:  l:  line parameters   (1x6) [l0x l0y l0z ldirx ldiry ldirz]
%         p:  planes parameters (nx6) [p0x p0y p0z pnormx pnormy pnormz]
%    
%   ,where ldir and pnorm do not need to be normalized.
%
% OUTPUT: isec: the intersection points (2x3) [X1 Y1 Z1; X2 Y2 Z2]
%

isecs_lp=gtLinePlaneIntersection(l,p);

ok=false(size(isecs_lp,1),1);

for i=1:size(isecs_lp,1)
	ok(i)=gtIsPointInPolyhedron(isecs_lp(i,:),p);
end

isec=isecs_lp(ok,:);

end