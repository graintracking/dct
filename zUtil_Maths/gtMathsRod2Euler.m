function euler = gtMathsRod2Euler(r)
% GTMATHSROD2EULER  Convert Rodrigues vectors to Euler angles (in degrees).
%                   These follow Bunge convention (ZXZ) [phi1; phi; phi2].
%
%     euler = gtMathsRod2Euler(r)
%     -------------------------------------------------------------------------
%
%     INPUT:
%       r     = <double>  Rodrigues vectors, as column vectors (3xN array).
%
%     OUTPUT:
%       euler = <double>  Euler angles triplets, as column vectors (3xN array).
%                         Output angles are in degrees and only positive.
%
%     Version 002 17-10-2012 by YGuilhem
%       Vectorize function to process quickly lots of Rogrigues vectors
%
%     Version 001 15-06-2012 by YGuilhem

% Check size of r column vectors
if size(r, 1) ~= 3
    gtError('Rodrigues:wrong_input_size', ...
        'Input array r should be sized 3xN!');
end

% Set epsilon to avoid singular values
epsilon = 1.e-9;

% Compute first intermediate value
c = cos(atan(sqrt(sum(r.^2))));

% Build regular filters
regular = abs(c.*r) >= epsilon;
regular_a = regular(1, :) | regular(2, :);
regular_b = regular(3, :) | c >= epsilon;

% Compute second intermediate values
a = double(~regular_a).*pi/4 + double(regular_a).*atan2(r(2, :), r(1, :));
b = double(~regular_b).*pi/4 + double(regular_b).*atan2(r(3, :), 1.);

% Compute Euler angles in radians
phi1 = a + b;
phi  = 2*atan2(sqrt(r(1, :).^2 + r(2, :).^2), sqrt(1. + r(3, :).^2));
phi2 = b - a;

% Switch to degrees
euler = [phi1 ; phi ; phi2] * 180 / pi;

% Shift to positive angles range
euler = mod(euler, 360.);

end % end of function
