function isecs = gtMathsLinePolyhedronIntersections(l,p)
%
% FUNCTION is = gtMathsLinePolygonIntersections(l,p)
%
% Computes the two intersection points of a line and the surface of a 
% convex polygon defined by its facet planes.
%
% INPUT:  l(1x6) - line coordinates [l0x l0y l0z ldirx ldiry ldirz]
%         p(nx6) - polyhedron planes coordinates [p0x p0y p0z pnormx pnormy
%                  pnormz]; pnorm vectors point out of the volume
%    
%         ldir and pnorm do not need to be normalized.
%
% OUTPUT: isecs(2x3) - the intersection points [X1 Y1 Z1; X2 Y2 Z2]
%                      OR empty if no intersection found
%

isecs_lp = gtMathsLinePlanesIntersections(l,p);

% ok = false(size(isecs_lp,1),1);
% for i = 1:size(isecs_lp,1)
% 	ok(i) = gtMathsIsPointInPolyhedron(isecs_lp(i,:),p);
% end

ok = gtMathsIsPointInPolyhedron(isecs_lp,p);

isecs = isecs_lp(ok,:);

if size(isecs,1) > 2
    isecs = isecs(1:2,:);
end