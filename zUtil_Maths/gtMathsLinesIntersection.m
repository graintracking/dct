function pofi = gtMathsLinesIntersection(l)
%
% FUNCTION  pofi = gtMathsLinesIntersection(l)
%
% Gives the closest point (`point of intersection`) to any number of 
% lines in 3D space. Uses an explicit least squares solution.
%
% INPUT:  l(n,6) - array containing the coordinates of lines; 
%                  direction vectors normalized (!);
%                  [point_X point_Y point_Z norm_dir_X norm_dir_Y norm_dir_Z]
%             
% OUTPUT: pofi(3,1) - `point of intersection` in 3D space
%   


A(1,1) = sum(l(:,6).^2 + l(:,5).^2) ;
A(1,2) = sum(-l(:,5).*l(:,4)) ;      
A(1,3) = sum(-l(:,6).*l(:,4)) ;      

A(2,1) = sum(-l(:,5).*l(:,4)) ;      
A(2,2) = sum(l(:,6).^2 + l(:,4).^2) ;
A(2,3) = sum(-l(:,6).*l(:,5)) ;      

A(3,1) = sum(-l(:,4).*l(:,6)) ;      
A(3,2) = sum(-l(:,5).*l(:,6)) ;       
A(3,3) = sum(l(:,4).^2 + l(:,5).^2) ;

b(1,1) = sum( (l(:,6).^2+l(:,5).^2).*l(:,1) - l(:,4).*(l(:,6).*l(:,3)+l(:,5).*l(:,2)) ) ;
b(2,1) = sum( (l(:,6).^2+l(:,4).^2).*l(:,2) - l(:,5).*(l(:,6).*l(:,3)+l(:,4).*l(:,1)) ) ;
b(3,1) = sum( (l(:,5).^2+l(:,4).^2).*l(:,3) - l(:,6).*(l(:,5).*l(:,2)+l(:,4).*l(:,1)) ) ;

pofi = A\b ;


end % of function
