function range=gtRange(data)
range=max(data(:))-min(data(:));
