function [x, norms] = gtMathsNormalizeVectorsList(x, dim)
% FUNCTION x = gtMathsNormalizeVectorsList(x, dim)
%
%   INPUT:
%       x   : The list of vectors <n x m>
%       dim : Opional, direction of the vectors: [1 | {2} | 3]
%

    if (~exist('dim', 'var') || isempty(dim))
        dim = 2;
    end

    square_norms = sum(x .^ 2, dim);
    norms = sqrt(square_norms) + (square_norms == 0);

    expansion_ones = ones(size(x, dim), 1);
    switch (dim)
        case 1
            x = x ./ norms(expansion_ones, :, :);
        case 2
            x = x ./ norms(:, expansion_ones, :);
        case 3
            x = x ./ norms(:, :, expansion_ones);
    end
end