function d = gtMathsDet3x3SymmPosDef(m)

%     d = m(1, 1, :) .* (m(2, 2, :) .* m(3, 3, :) - m(2, 3, :) .^ 2) ...
%         - m(1, 2, :) .* (m(2, 1, :) .* m(3, 3, :) - m(2, 3, :) .* m(3, 1, :)) ...
%         + m(1, 3, :) .* (m(2, 1, :) .* m(3, 2, :) - m(2, 2, :) .* m(3, 1, :));

    mdiag = gtMathsDiag3x3(m);
    moffdiag = cat(1, m(2, 3, :), m(1, 3, :), m(1, 2, :));
    d = prod(mdiag, 1) + 2 .* prod(moffdiag, 1) - sum(mdiag .* (moffdiag .^ 2), 1);
end