function dot_prod = gtMathsDotProduct(x, y)
% FUNCTION dot_prod = gtMathsDotProduct(x, y)
%

    if (iscell(x))
        dot_prod = 0;
        for n = 1:numel(x)
            dot_prod = dot_prod + gtMathsDotProduct(x, y);
        end
    else
        dot_prod = gtMathsSumNDVol(x .* y);
    end
end