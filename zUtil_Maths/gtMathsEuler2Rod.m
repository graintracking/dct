function r = gtMathsEuler2Rod(euler)
% GTMATHSEULER2ROD  Convert Euler angles (in degrees) to Rodrigues vectors.
%                   These follow Bunge convention (ZXZ) [phi1; phi; phi2].
%
%     r = gtMathsEuler2Rod(euler)
%     -------------------------------------------------------------------------
%
%     INPUT:
%       euler = <double>  Euler angles triplets, as column vectors (3xN array).
%
%     OUTPUT:
%       r     = <double>  Rodrigues vectors, as column vectors (3xN array).
%
%     Version 002 17-10-2012 by YGuilhem
%       Vectorize function to process quickly lots of Euler angles
%
%     Version 001 15-06-2012 by YGuilhem

% Check size of r column vectors
if size(euler, 1) ~= 3
    gtError('Rodrigues:wrong_input_size', ... 
        'Input array euler should be sized 3xN!');
end

% Create Euler angles vectors
phi1 = euler(1, :);
phi  = euler(2, :);
phi2 = euler(3, :);

% Compute intermediate values a & b
a = (phi1 - phi2)/2;
b = (phi1 + phi2)/2;

% Compute intermediate cosine and tangent to speed up
cosb   = cosd(b);
tanphi = tand(phi/2);

% Compute Rodrigues vector components
r = [ tanphi .* cosd(a)./cosb ; ...
      tanphi .* sind(a)./cosb ; ...
      tand(b)                 ];

end % end of function
