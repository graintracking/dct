function energy = gtConvWavelengthToEnergy(lambda)
% GTCONVWAVELENGTHTOENERGY Converts X-ray wavelength to energy.
%
% energy = gtConvWavelengthToEnergy(lambda)
%
% -----------------------------------------
% 
% INPUT
%   lambda  - wavelength in angstroms
%
% OUTPUT
%   energy  - energy in keV
%

energy = 12.3984428./lambda;

end

