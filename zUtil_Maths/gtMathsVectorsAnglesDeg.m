function angdeg = gtMathsVectorsAnglesDeg(v1, v2, colvec)
% GTMATHSVECTORSANGLESDEG Angles in DEGREES between 3D UNIT vectors.
%
% angdeg = gtMathsVectorsAnglesDeg(v1, v2, colvec)
%
% ----------------------------------------------------------------
% 
% Angles in DEGREES between the input 3D UNIT VECTORS v1 and v2.
% The angle is computed with a asin function from the vectorial difference
% between two vectors (0 <= angdeg <= 90deg):
%   angrad = 2 * asin( 0.5*norm(v1-v2) )
%   angdeg = 180/pi*angrad;
%
% This is more accurate and less prone to numerical errors than using acos,
% while the computation time is similar. 
%
% INPUT
%   v1          - one or multiple arbitrary 3D UNIT VECTORS 
%   v2          - a set of arbitrary 3D UNIT VECTORS
%   colvec      - true if column vectors are used, false if row vectors:
%                   true  - size(v1) is 3xn or 3x1; size(v2) is 3xn
%                   false - size(v1) is nx3 or 1x3; size(v2) is nx3
%
% OUTPUT
%   angdeg      - angles between v1 and v2 in DEGREES; 0 <= angdeg <= 90;
%                 (1xn or nx1)
%

angrad = gtMathsVectorsAnglesRad(v1, v2, colvec);
angdeg = (180/pi)*angrad;

end