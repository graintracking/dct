function [theoX, A, cgX] = gtMathsCGSolveCellExample

    numComponents = 5e2;
    verbose = false;
    maxIters = 1600;
    toll = 1e-8;
    cgX = { };

    fprintf('Generating A.. ')
%     c = cos(linspace(1, 10, numComponents) / 20 * pi);
%     A = toeplitz(c);

    A1 = rand(numComponents) + diag(50 * rand(numComponents, 1)) ...
        + diag(30 * rand(numComponents-1, 1),  1) ...
        + diag(30 * rand(numComponents-1, 1), -1);
    A = A1' * A1;
    fprintf('Done.\nConditioning number of A: %e\n\n', cond(A))

    H11 = {A, A};
    b = {rand(numComponents, 1), rand(numComponents, 1)};
    numCells = length(H11);

    onesVols = {ones(numComponents, 1), ones(numComponents, 1)};
    precond = cell(1, numCells);
    for ii = 1:numCells
        precond{ii} = diag(1 ./ (H11{ii} * onesVols{ii}));
    end

    tic();
    theoX = cell(1, numCells);
    for ii = 1:length(theoX)
        theoX{ii} = H11{ii} \ b{ii};
    end
    time_to_compute = toc();
    fprintf('\nMatlab''s Gauss-Pivoting: %f seconds\n\n', time_to_compute);

    x0 = cell(1, numCells);
    for ii = 1:numCells
        x0{ii} = zeros(size(b{ii}));
    end
    tic();
    [cgX{1}, flag, cgres, numIters, srt1] = gtMathsSIRTSolveCell({A1, A1}, [], b, x0, toll, maxIters, verbose, false);
    time_to_compute = toc();
    fprintf('\nSIRT''s implementation: %f seconds (NumIters: %3d, Res: %g, flag: %d)\n\n', time_to_compute, numIters, cgres, flag);

    tic();
    [cgX{2}, flag, cgres, numIters, sr1] = gtMathsCGSolveCell(H11, b, toll, maxIters, [], [], verbose, false);
    time_to_compute = toc();
    fprintf('\nMatlab''s implementation: %f seconds (NumIters: %3d, Res: %g, flag: %d)\n\n', time_to_compute, numIters, cgres, flag);
    tic();
    [cgX{3}, flag, cgres, numIters] = gtMathsCGSolveCell(H11, b, toll, maxIters, [], [], verbose, true);
    time_to_compute = toc();
    fprintf('\nNicola''s implementation: %f seconds (NumIters: %3d, Res: %g, flag: %d)\n\n', time_to_compute, numIters, cgres, flag);

    numIters = zeros(1, numCells);
    cgres = zeros(1, numCells);
    flag = zeros(1, numCells);
    tic();
    cgX{4} = cell(1, numCells);
    for ii = 1:numCells
        [cgX{4}{ii}, flag(ii), cgres(ii), numIters(ii), resv] = pcg(H11{ii}, b{ii}, toll, maxIters);
    end
    time_to_compute = toc();
    fprintf('\nMatlab''s implementation: %f seconds (NumIters:%s, Res:%s, flag:%s)\n\n', ...
        time_to_compute, sprintf(' %3d', numIters), sprintf(' %g', cgres), ...
        sprintf(' %d', flag));

    tic();
    [cgX{5}, flag, cgres, numIters, sr2] = gtMathsCGSolveCell(H11, b, toll, maxIters, diag(1 ./ diag(A)), [], verbose, false);
    time_to_compute = toc();
    fprintf('\nMatlab''s implementation: %f seconds (NumIters: %3d, Res: %g, flag: %d)\n\n', time_to_compute, numIters, cgres, flag);
    tic();
    [cgX{6}, flag, cgres, numIters, sr3] = gtMathsCGSolveCell(H11, b, toll, maxIters, diag(1 ./ diag(A)), [], verbose, true);
    time_to_compute = toc();
    fprintf('\nMatlab''s implementation: %f seconds (NumIters: %3d, Res: %g, flag: %d)\n\n', time_to_compute, numIters, cgres, flag);

    tic();
    [cgX{7}, flag, cgres, numIters, sr4] = gtMathsCGSolveCell(H11, b, toll*1e4, maxIters, diag(1 ./ diag(A)), [], verbose, false);
    time_to_compute = toc();
    fprintf('\nMatlab''s implementation: %f seconds (NumIters: %3d, Res: %g, flag: %d)\n\n', time_to_compute, numIters, cgres, flag);
    tic();
    [cgX{8}, flag, cgres, numIters, sr5] = gtMathsCGSolveCell(H11, b, toll, maxIters, diag(1 ./ diag(A)), cgX{7}, verbose, false);
    time_to_compute = toc();
    fprintf('\nMatlab''s implementation: %f seconds (NumIters: %3d, Res: %g, flag: %d)\n\n', time_to_compute, numIters, cgres, flag);

    tic();
    [cgX{9}, flag, cgres, numIters, sr6] = gtMathsCGSolveCell(H11, b, toll, maxIters, precond, [], verbose, false);
    time_to_compute = toc();
    fprintf('\nMatlab''s implementation: %f seconds (NumIters: %3d, Res: %g, flag: %d)\n\n', time_to_compute, numIters, cgres, flag);

    if (verbose)
        disp([theoX{1}, cgX{1}{1}, cgX{2}{1}])
    end

    fprintf('Max relative errors:\n');
    populatedCells = find(cellfun(@iscell, cgX));
    for ii = 1:numCells
        pattern = sprintf('%e, ', ...
            arrayfun(@(x)max((theoX{ii} - cgX{x}{ii}) ./ theoX{ii}), ...
                        populatedCells) );
        fprintf(['  (' pattern(1:end-2) ')\n']);
    end
    fprintf('\n');

    figure, semilogy(resv)
    hold
    plot(sqrt(sr1), 'r')
    plot(sqrt(sr2), 'g')
    plot(sqrt(sr6), 'y')
    hold

    figure, semilogy(sqrt(sr4))
    hold
    plot([zeros(find(sr4 ~= 0, 1, 'last'), 1); sqrt(sr5)], 'r')
    hold
end
