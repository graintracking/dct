function y=orthonormGS(x1,x2,x3) 
% Gram-Schmidt process

if size(x1,1)~=1 && size(x1,2)~=3
   x1=x1'; 
end
if size(x2,1)~=1 && size(x2,2)~=3
   x2=x2'; 
end
if size(x3,1)~=1 && size(x3,2)~=3
   x3=x3'; 
end

u1=x1;
e1=u1/norm(u1);
u2=x2-dot(x2,u1)*u1/dot(u1,u1);
e2=u2/norm(u2);
u3=x3-dot(x3,u1)*u1/dot(u1,u1)-dot(x3,u2)*u2/dot(u2,u2);
e3=u3/norm(u3);


norm12=dot(e1,e2);
norm23=dot(e2,e3);
norm13=dot(e1,e3);

% assign the three bases to y
y=[e1;e2;e3];




% 
% if norm12==0 && norm23==0 && norm13==0
%     disp('x1.x2=0')
%     disp('x2.x3=0')
%     disp('x1.x3=0')   
%     v1=x1;
%     v2=x2;
%     v3=x3;
%     number=[1 2 3];
% elseif norm12==0 && norm23~=0 && norm13~=0
%     v1=x1;
%     v2=x2;
%     v3=x3;
%     disp('x1.x2=0')
%     number=[1 2 3];
% elseif norm23==0 && norm13~=0 && norm12~=0
%     v1=x2;
%     v2=x3;
%     v3=x1;
%     disp('x2.x3=0')
%     number=[2 3 1];
% elseif norm13==0 && norm23~=0 && norm12~=0
%     v1=x1;
%     v2=x3;
%     v3=x2;
%     disp('x1.x3=0')  
%     number=[1 3 2];
% elseif norm12==0 && norm23==0 && norm13~=0
%     v1=x1;
%     v2=x2;
%     v3=x3;
%     disp('x1.x3~=0') 
%     number=[1 2 3];
% elseif norm23==0 && norm13==0 && norm12~=0
%     v1=x2;
%     v2=x3;
%     v3=x1;
%     disp('x1.x2~=0')  
%     number=[2 3 1];
% elseif norm13==0 && norm12==0 && norm23~=0
%     v1=x1;
%     v2=x3;
%     v3=x2;
%     disp('x2.x3~=0') 
%     number=[1 3 2];
% elseif norm12~=0 && norm23~=0 && norm13~=0
%     v1=x1;
%     v2=x2;
%     v3=x3;
%     disp('x1.x2~=0')    
%     disp('x2.x3~=0') 
%     disp('x1.x3~=0')  
%     number=[1 2 3];
% end       
%     
% % matrix 3x3
% % u
% % v
% % t
% y=orthonormGS(v1,v2,v3);
% 
% ind=find(number==1);
% detdiru=y(ind,:)'
% ind=find(number==2);
% detdirv=y(ind,:)'
% ind=find(number==3);
% tn=y(ind,:)'
