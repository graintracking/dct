function data_size = gtMathsGetArraySize(x)
% FUNCTION data_size = gtMathsGetArraySize(x)
%

    if (iscell(x))
        data_size = cell(size(x));
        for n = 1:numel(x)
            data_size{n} = gtMathsGetArraySize(x{n});
        end
    else
        data_size = size(x);
    end
end