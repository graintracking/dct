function R = rotationmat3D(angle,axis)
% ROTATIONMAT3D  Creates the rotation matrix
%     R = rotationmat3D(angle, axis)
%     ------------------------------
%     creates a rotation matrix such that R * x 
%     operates on x by rotating x around the origin of about "angle" in degrees 
%     counterclockwise around line connecting the origin to the point "axis"
%
%     Usage:
%       to rotate around z axis 45 degrees:
%       Rtest = rotationmat3D(45,[0 0 1])
%
%     Thanks to Bileschi 2009
%
%
% Version 001 14-02-2013 by LNervo


L = norm(axis);
if (L <= 0)
   gtError('rotationmat3D:wrongArgument','axis direction must be non-zero vector');
end

axis = axis / L;
L = 1;
u = axis(1);
v = axis(2);
w = axis(3);
u2 = u^2;
v2 = v^2;
w2 = w^2;

c = cosd(angle);
s = sind(angle);
%storage
R = nan(3);
%fill
R(1,1) =  u2 + (v2 + w2)*c;
R(1,2) = u*v*(1-c) - w*s;
R(1,3) = u*w*(1-c) + v*s;
R(2,1) = u*v*(1-c) + w*s;
R(2,2) = v2 + (u2+w2)*c;
R(2,3) = v*w*(1-c) - u*s;
R(3,1) = u*w*(1-c) - v*s;
R(3,2) = v*w*(1-c)+u*s;
R(3,3) = w2 + (u2+v2)*c;

end

