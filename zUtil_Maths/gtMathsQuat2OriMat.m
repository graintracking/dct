function g = gtMathsQuat2OriMat(q)
% GTMATHSQUAT2ROTMAT  Convert quaternions to orientation matrices.
%
%     g = gtMathsQuat2OriMat(r)
%     ---------------------------------------------------------------------
%
%     INPUT:
%       q = <double>  Quaternions, as column vectors (4xN array).
%
%     OUTPUT:
%       g = <double>  Orientation matrices (3x3xN array).
%                     See Randle & Engler, 2000 for more details about g.
%
%     Version 001 15-02-2021 by Zheheng (inspired by Yoann's function gtMathsRod2OriMat)

% If one single entry
if (numel(q) == 4)
    qs0 = q(1)^2;
    qs1 = q(2)^2;
    qs2 = q(3)^2;
    qs3 = q(4)^2;

    q0q1 = q(1)*q(2);
    q0q2 = q(1)*q(3);
    q0q3 = q(1)*q(4);
    q1q2 = q(2)*q(3);
    q1q3 = q(2)*q(4);
    q2q3 = q(3)*q(4);

    g = [ qs0 + qs1 - qs2 - qs3, 2*(q1q2 + q0q3),       2*(q1q3 - q0q2)        ; ...
          2*(q1q2 - q0q3),       qs0 + qs2 - qs1 - qs3, 2*(q2q3 + q0q1)        ; ...
          2*(q1q3 + q0q2),       2*(q2q3 - q0q1),       qs0 + qs3 - qs1 - qs2] ;
else
    % Otherwise vectorize computation
    if (size(q, 1) ~= 4)
        gtError('gtMathsQuat2OriMat:wrong_input_size', ...
                'Input array q should be sized 4xN!');
    end
    N = size(q, 2);
    g = zeros(9, N, class(q));

    % Off diagonals (1,2) (2,1)
    qq1 = q(2, :).*q(3, :);
    qq2 = q(1, :).*q(4, :);
    g(2, :) = qq1 - qq2;
    g(4, :) = qq1 + qq2;

    % Off diagonals (1,3) (3,1)
    qq1 = q(2, :).*q(4, :);
    qq2 = q(1, :).*q(3, :);
    g(3, :) = qq1 + qq2;
    g(7, :) = qq1 - qq2;

    % Off diagonals (2,3) (3,2)
    qq1 = q(3, :).*q(4, :);
    qq2 = q(1, :).*q(2, :);
    g(6, :) = qq1 - qq2;
    g(8, :) = qq1 + qq2;

    % Multiply all by two
    g = g*2;
    
    % Diagonals (1,1) (2,2) (3,3)
    q2 = q.^2;
    g(1, :) = q2(1, :) + q2(2, :) - q2(3, :) - q2(4, :);
    g(5, :) = q2(1, :) + q2(3, :) - q2(2, :) - q2(4, :);
    g(9, :) = q2(1, :) + q2(4, :) - q2(2, :) - q2(3, :);
    
    % Reshape for output
    g = reshape(g, 3, 3, []);
end

end % end of function
