function im=sumImages
edfFiles=dir('full*.edf');
im=zeros(2048);
for i=1:length(edfFiles)
    filename=sprintf('full%04d.edf',i);
    full=edf_read(filename);
    im=im+full;
end
end