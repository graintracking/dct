function d = gtMathsDivergence(x)
% FUNCTION d = gtMathsDivergence(x)
% Computes the divergence of an n-dimensional vector object.

    num_dims = numel(x);
    d = cell(num_dims, 1);
    for ii_d = 1:num_dims
        padsize = zeros(1, num_dims);
        padsize(ii_d) = 1;

        try
            pad_x = padarray(x{ii_d}, padsize, 'pre');
        catch mexc
            if (strcmpi(mexc.identifier, 'MATLAB:license:checkouterror'))
                dims = size(x{ii_d});
                pad_x = cat(ii_d, zeros(~padsize * (dims(ii_d) - 1) + 1), x{ii_d});
            else
                rethrow(mexc)
            end
        end

        d{ii_d} = diff(pad_x, 1, ii_d);
    end

    d = gtMathsSumCellVolumes(d);
end