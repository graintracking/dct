function [bestSol, flag, bestRes, bestIter, squareResiduals] = ...
    gtMathsCGSolveCell(A, b, toll, maxIter, precond, x0, verbose, use_c_functions)
% GTMATHSCGSOLVECELL Conjugate Gradient solver for Cell arrays
%   [bestSol, flag, res, bestIter, resvec] = gtMathsCGSolveCell(A, b, maxIter[, precond[, x0[, verbose[, use_c_functions]]]])
%
%   Inputs:
%       - A: a matrix, a cell of matrices or a function handle
%       - b: a vector or a cell of vectors
%       - toll: tollerance
%       - maxIter: maximum number of iterations
%       - precond: preconditioner that can be: matrix, cell of matrices or
%               function handle
%       - x0: initial solution (if not spcified, 0 will be the predefined)
%       - verbose: boolean flag to be verbose or not
%       - use_c_functions: boolean flag to activate accelerated C++ functions
%
%   Outputs:
%       - bestSol: solution found by the algorithm
%       - flag: Exit condition (extended from matlab's 'pcg' routine)
%       - bestRes: relative residual of best solution
%       - numIters: number of iterations performed
%       - resvec: vector of square residuals for each iteration
%
%   Flags:
%       - 0: terminated with success
%       - 1: reached max number of iterations
%       - 2: Preconditioner is ill-conditioned
%       - 3: Not returned, yet. (Stagnation of the method)
%       - 4: One of the scalar quantities became either too big or too small
%       - 5: Bad Hessian: non-posite definite!
%

    try
        num_threads = feature('NumCores');
    catch
        num_threads = 1;
    end

    if (~exist('precond', 'var') || isempty(precond))
        precond = [];
    elseif (isnumeric(precond))
        precond = @(x)multiplyMatrix(precond, x);
    elseif (iscell(precond))
        precond = @(x)multiplyCellMatrix(precond, x);
    end

    if (~exist('verbose', 'var'))
        out = GtConditionalOutput(false);
    else
        out = GtConditionalOutput(verbose);
    end

    if (~exist('use_c_functions', 'var'))
        use_c_functions = false;
    end

    if (~iscell(b))
        if (isnumeric(b))
            b = { b };
        else
            error('gtMathsCGSolveCell:wrong_argument', ...
                  'This function works on Cell arrays');
        end
    end
    if (~isa(A, 'function_handle'))
        if (iscell(A))
            if (length(A) == length(b))
                A = @(x)multiplyCellMatrix(A, x);
            else
                error('gtMathsCGSolveCell:wrong_argument', ...
                    'Size of cell Matrix, and cell Vector mismatch!');
            end
        elseif (isnumeric(A))
            warning('gtMathsCGSolveCell:parameter', ...
                    ['I''m assuming that the input A is a square matrix, that' ...
                     ' suits for the vectors in  b']);
            A = @(x)multiplyMatrix(A, x);
        end
    end

    numCells = length(b);

    % To see the trend of residuals
    squareResiduals = zeros(maxIter, 1);
    if (~isempty(precond)), dotRhos = zeros(maxIter, 1); end

    % Solution to the system
    if (~exist('x0', 'var') || isempty(x0))
        z = cell(1, numCells);
        for ii = 1:numCells
            z{ii} = zeros(size(b{ii}));
        end
        % Initial residual which is exactly the b vector, since z = 0
        if (use_c_functions)
            r = gtCxxMathsCellCopy(b, 'threads', num_threads);
        else
            r = b;
        end
    else
        if (use_c_functions)
            z = gtCxxMathsCellCopy(x0, 'threads', num_threads);
        else
            z = x0;
        end
        r = measureResidual(A, b, z, use_c_functions, num_threads);
    end

    % Computing preconditioner
    if (~isempty(precond))
        y = precond(r);

        if (~isFinite(y))
            flag = 2;
            return;
        end
    else
        y = r;
    end

    % First correction is gradient step
    if (use_c_functions)
        d = gtCxxMathsCellTimes(y, -1.0, 'threads', num_threads);
    else
        d = cell(1, numCells);
        for ii = 1:numCells
            d{ii} = -y{ii};
        end
    end
    nextSquareResidual = gtMathsSquareNorm(r);
    if (~isempty(precond)), nextDotRho = gtMathsDotProduct(r, y); end

    % Best solutions in the iterations (just in case we run till the end)
    initialResidualNorm = sqrt(gtMathsSquareNorm(b));
    bestRes = 1;
    bestSol = d;
    bestIter = 0;

    for jj = 1:maxIter
        % Next iteration
        squareResiduals(jj) = nextSquareResidual;
        if (~isempty(precond)), dotRhos(jj) = nextDotRho; end

        % The vector multiplied by the matrix
        q = A(d);

        % d' * A * d
        squareAd = gtMathsDotProduct(d, q);
        if (squareAd < 0)
            out.fprintf('\nExit by Negative defined Hessian\n')
            flag = 5;
            return;
        end

        % Determine Alpha
        if (isempty(precond))
            alpha = squareResiduals(jj) / squareAd;
        else
            alpha = dotRhos(jj) / squareAd;
        end
        if (alpha == 0 || isinf(alpha))
            flag = 4;
            return;
        end
        % Update the solution
        if (use_c_functions)
            z = gtCxxMathsCellMinus(z, d, 'scale', alpha, 'copy', false, 'threads', num_threads);
        else
            for ii = 1:numCells
                z{ii} = z{ii} - alpha * d{ii};
            end
        end

        % Evaluation of the residual
        if (mod(jj+1, 50) == 0) && false % <-- Let's disable for now
            r = measureResidual(A, b, z, use_c_functions, num_threads);
        else
            % Let's update the residual
            if (use_c_functions)
                r = gtCxxMathsCellPlus(r, q, 'scale', alpha, 'copy', false, 'threads', num_threads);
            else
                for ii = 1:numCells
                    r{ii} = r{ii} + alpha * q{ii};
                end
            end
        end

        % let's update y
        if (~isempty(precond))
            y = precond(r);

            if (~isFinite(y))
                flag = 2;
                return;
            end
        else
            y = r;
        end

        % computing beta and the weights
        nextSquareResidual = gtMathsSquareNorm(r);
        if (nextSquareResidual == 0 || isinf(nextSquareResidual))
            flag = 4;
            return;
        end

        if (~isempty(precond))
            nextDotRho = gtMathsDotProduct(r, y);
            if (nextDotRho == 0 || isinf(nextDotRho))
                flag = 4;
                return;
            end
            beta = nextDotRho / dotRhos(jj);
        else
            beta = nextSquareResidual / squareResiduals(jj);
        end
        if (beta == 0 || isinf(beta))
            flag = 4;
            return;
        end

        % Test if it is the best, and save it
        nextResidualNorm = sqrt(nextSquareResidual);
        if (nextResidualNorm < bestRes * initialResidualNorm)
            bestRes = nextResidualNorm / initialResidualNorm;
            bestSol = z;
            bestIter = jj;
        end

        out.fprintf('Iteration %03d/%03d: BestResidualNorm %f, ResidualNorm %5.20f\n', ...
                    jj, maxIter, bestRes, nextResidualNorm);

        % Exit condition
        if (nextResidualNorm < toll * initialResidualNorm)
            out.fprintf('\nExit by tollerance\n');
            flag = 0;
            return;
        end

        % Update the correction to apply at the next iteration
        if (use_c_functions)
            d = gtCxxMathsCellTimes(d, beta, 'copy', false, 'threads', num_threads);
            d = gtCxxMathsCellMinus(d, y, 'copy', false, 'threads', num_threads);
        else
            for ii = 1:numCells
                d{ii} = -y{ii} + beta * d{ii};
            end
        end
    end

    flag = 1;
end

% function toll = getTollerance(residual, squareNormGrad, use_c_functions)
%     if (~exist('squareNormGrad', 'var'))
%         squareNormGrad = gtMathsSquareNorm(residual);
%     end
%
%     if (squareNormGrad < 1)
%         normGrad = sqrt(squareNormGrad);
%         toll = min(0.5, squareNormGrad) * normGrad;
%     else
%         normGrad = sqrt(squareNormGrad);
%         toll = min(0.5, sqrt(normGrad)) * normGrad;
%     end
% end

function prod = multiplyCellMatrix(M, x)
    numCells = length(M);
    prod = cell(1, numCells);
    for ii = 1:numCells
        prod{ii} = M{ii} * x{ii};
    end
end

function prod = multiplyMatrix(M, x)
    numCells = length(x);
    prod = cell(1, numCells);
    for ii = 1:numCells
        prod{ii} = M * x{ii};
    end
end

function is_finite = isFinite(x)
    is_finite = true;
    numCells = length(x);
    for ii = 1:numCells
        is_finite = all(isfinite(x{ii}));
        if (~is_finite), break; end
    end
end

function r = measureResidual(A, b, z, use_c_functions, num_threads)
% Evaluation of the true residual
    numCells = length(b);
    tempAz = A(z);
    r = cell(size(b));
    if (use_c_functions)
        r = gtCxxMathsCellMinus(b, tempAz, 'threads', num_threads);
    else
        for ii = 1:numCells
            r{ii} = b{ii} - tempAz{ii};
        end
    end
end
