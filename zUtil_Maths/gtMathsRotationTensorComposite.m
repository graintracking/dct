function rot = gtMathsRotationTensorComposite(rot_axes, rot_angles)
% rot = gtMathsRotationTensorComposite(rot_axes, rot_angles)
% FUNCTION gtMathsRotationTensorComposite returns the result of the
%          composition of different rotations
% INPUT:
%   - rot_axes : <n x 3>
%   - rot_angles : <n x 1>

    rot = eye(3);
    for ii = 1:numel(rot_angles)
        rotcomp = gtMathsRotationMatrixComp(rot_axes(ii, :)', 'col');
        rot = gtMathsRotationTensor(rot_angles(ii), rotcomp) * rot;
    end
end
