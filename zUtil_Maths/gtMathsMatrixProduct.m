function C = gtMathsMatrixProduct(A, B)
    [size_A(1), size_A(2), size_A(3)] = size(A);
    [size_B(1), size_B(2), size_B(3)] = size(B);

    if (size_A(3) ~= size_B(3))
        if (all([size_A(3), size_B(3)] ~= 1))
            error('gtMathsMatrixProduct:wrong_argument', ...
                'The matrix stacks should have either the same number of matrices, or one should be one')
        elseif (size_A(3) == 1)
            num_out = size_B(3);
        else
            num_out = size_A(3);
        end
    else
        num_out = size_A(3);
    end

    A = reshape(A, size_A(1), size_A(2), 1, size_A(3));
    B = reshape(B, 1, size_B(1), size_B(2), size_B(3));

    C = bsxfun(@times, A, B);
    C = sum(C, 2);
    C = reshape(C, size_A(1), size_B(2), num_out);
end
