function x = gtMathsNonNegative(x)
% FUNCTION x = gtMathsNonNegative(x)
%

    if (iscell(x))
        for n = 1:length(x)
            x{n}(x{n} < 0) = 0;
        end
    else
        x(x < 0) = 0;
    end
end