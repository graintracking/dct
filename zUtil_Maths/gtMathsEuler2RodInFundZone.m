function r_vec = gtMathsEuler2RodInFundZone(e_vec, symm)
    g = gtMathsEuler2OriMat(e_vec);

    num_symm_ops = numel(symm);

    r_vecs = zeros(2 * num_symm_ops, 3);

    for ii = 1:num_symm_ops
        r_vecs((ii - 1) * 2 + 1, :) = gtMathsOriMat2Rod(symm(ii).g3 * g);
        r_vecs((ii - 1) * 2 + 2, :) = gtMathsOriMat2Rod(symm(ii).g3' * g);
    end

    dists = 2 * atand(sqrt(sum(r_vecs .^ 2, 2)));

    [~, ii] = min(dists);

%     r_vecs

    r_vec = r_vecs(ii, :);
end
