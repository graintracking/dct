function d = gtMathsCircularDistance(a, b, limit)
    a = mod(a, limit);
    b = mod(b, limit);

    diff_ab = bsxfun(@minus, a, b);
    d = min(abs(diff_ab), abs(diff_ab + limit));
    d = min(d, abs(diff_ab - limit));
end
