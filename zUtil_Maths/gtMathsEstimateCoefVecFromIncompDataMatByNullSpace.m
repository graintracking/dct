function [col_vec, row_vec] = gtMathsEstimateCoefVecFromIncompDataMatByNullSpace(data_mat, varargin)
    % [row_vec, col_vec] = gtMathsEstimateCoefVecFromIncompDataMatByNullSpace(data_mat, varargin)
    % Matrix data_mat ideally equals the product of a column vector col_vec and a row
    % vector row_vec, but data_mat becomes incomplete and noisy. In this case, this
    % function estimates the direction of col_vec and row_vec by using null spaces of
    % columns in matrix data_mat.
    %
    % input
    % data_mat : the matrix where the missing elements are indicated by nan.
    %
    % optional input
    % 'col_weights' : the weights of columns in data_mat. Default is all ones.
    conf = struct('col_weights', []);
    conf = parse_pv_pairs(conf, varargin);
    data_mat_origin = data_mat;
    usable_col = sum(double(~isnan(data_mat)), 1) > 1;
    data_mat(:, ~usable_col) = [];
    usable_row = any(~isnan(data_mat), 2);
    data_mat(~usable_row, :) = [];
    nof_used_row = size(data_mat, 1);
    nof_used_col = size(data_mat, 2);
    data_nullspace = zeros(nof_used_row, nof_used_row);
    data_nullspace_row_norm = zeros(nof_used_row, 1);
    if numel(conf.col_weights) < nof_used_col
        conf.col_weights = ones(1, nof_used_col);
    end
    for ii_col = 1:nof_used_col
        valid_row_ind = ~isnan(data_mat(:, ii_col));
        data_valid = data_mat(valid_row_ind, ii_col);
        if any(data_valid ~= 0)
            data_valid_sq = data_valid .^ 2;
            tmp_weight = conf.col_weights(ii_col);
            data_valid_nullspace = null(data_valid');
            data_nullspace(valid_row_ind, valid_row_ind) = data_nullspace(valid_row_ind, valid_row_ind) + ...
                data_valid_nullspace * data_valid_nullspace' * (tmp_weight^2);
            data_nullspace_row_norm(valid_row_ind) = data_nullspace_row_norm(valid_row_ind) + ...
                tmp_weight * (1 - data_valid_sq/sum(data_valid_sq));%sum(data_valid_nullspace .^ 2, 2)
        end
    end
    data_nullspace_row_norm = sqrt(data_nullspace_row_norm);
    data_nullspace = data_nullspace ./ (data_nullspace_row_norm * data_nullspace_row_norm');
    [~, ~, data_nullspace] = svd(data_nullspace);
    coef_used = data_nullspace(:, end) ./ data_nullspace_row_norm;
    col_vec = zeros(size(usable_row));
    col_vec(usable_row) = coef_used;
    if sum(col_vec) < 0
        col_vec = -col_vec;
    end
    col_vec(~usable_row) = nan;
    if nargout > 1
        nof_col = size(data_mat_origin, 2);
        row_vec = nan(1, nof_col);
        nonzero_colvecel = (usable_row & (col_vec ~= 0));
        for ii_col = 1:nof_col
            valid_row_ind = ((~isnan(data_mat_origin(:, ii_col))) & nonzero_colvecel);
            if any(valid_row_ind)
                row_vec(ii_col) = median(data_mat_origin(valid_row_ind, ii_col) ./ col_vec(valid_row_ind));
            end
        end
    end
end