function pmirr = gtMathsMirrorPointsOnAxis(points, axispos, axisdir)
% FUNCTION pmirr = gtMathsMirrorPointsOnAxis(points,axispos,axisdir)
% 
% Mirrors a set of points on an arbitrary axis (=rotation by 180deg) in 3D space.
% 
% INPUT 
%   points  - array of the points coordinates (n,3)
%   axispos - axis location defined by a point on the axis (1,3)
%   axisdir - axis direction (UNIT VECTOR!) (1,3) 
%
% OUTPUT
%   pmirr   - array of the mirrored points coordinates (n,3)
% 

replOnes = ones(size(points, 1), 1);

% vectors from points to axis location:
mvec = axispos(replOnes, :) - points;

% mvec components parallel to rot. axis:
mrot  = (mvec*axisdir')*axisdir;

% orthogonal component:
morth = mvec - mrot;

% mirrored points:
pmirr = points + 2*morth;


end
