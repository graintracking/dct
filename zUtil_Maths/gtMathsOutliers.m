% Gives outliers in a set of values.

function out = gtMathsOutliers(values,tolfact,meanv,stdv)

if nargin < 2
	tolfact = 3;
end

if nargin < 3
  meanv = mean(values);
end

if nargin < 4
  stdv  = std(values);
end

out = (values < meanv-tolfact*stdv) | (meanv+tolfact*stdv < values) ;

