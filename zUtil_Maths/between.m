function answer = between(arg, a, b)
% FUNCTION BETWEEN logical operation
% function answer=between(arg, a, b)
%
% returns true if arg <= b && arg >= a
% arg may either be a scalar or a matrix of same size like a and b

    if (~all(size(a) == size(b) | size(a) == 1 | size(b) == 1))
        error('BETWEEN:wrong_input', ...
            'input arguments need to be same size');
    end

    if (~isscalar(arg) && ~all(size(a) == size(arg) | size(a) == 1 | size(arg) == 1))
        error('BETWEEN:wrong_input', ...
            'arg needs to be either a scalar or a matrix of same size as a and b!');
    end

    if (numel(arg) == 1 || all(size(arg) == size(a)))
        answer = arg <= b & arg >= a;
    else
        answer = bsxfun(@and, bsxfun(@le, arg, b), bsxfun(@ge, arg, a));
    end
end