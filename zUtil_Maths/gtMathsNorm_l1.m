function l1norm = gtMathsNorm_l1(x)
% FUNCTION l1norm = gtMathsNorm_l1(x)
%

    if (iscell(x))
        l1norm = 0;
        for n = 1:numel(x)
            l1norm = l1norm + gtMathsSumNDVol(abs(x{n}));
        end
    else
        l1norm = gtMathsSumNDVol(abs(x));
    end
end