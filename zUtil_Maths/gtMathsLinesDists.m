%
% FUNCTION  dist = gtMathsLinesDists(bl,ol)
%
% Gives the spatial distance between a given line and other 3D lines.
% 
% INPUT   bl(1x6)  - base line coordinates
%                      [ point_X point_Y point_Z dir_X dir_Y dir_Z ]
%         ol(nx6)  - other lines coordinates
%                      [ point_X point_Y point_Z dir_X dir_Y dir_Z ]
%
% OUTPUT  dist - spatial distance between bl and ol-s
%        

function dist = gtMathsLinesDists(bl,ol)

% Cross product of line pair directions
cr = [bl(5)*ol(:,6) - bl(6)*ol(:,5), ...
      bl(6)*ol(:,4) - bl(4)*ol(:,6), ...
      bl(4)*ol(:,5) - bl(5)*ol(:,4)];

% Vector between positions
bo  = ol(:,1:3)-bl(ones(size(ol,1),1),1:3);

% Distance
dist = abs(sum(bo.*cr,2)./sqrt(sum(cr.*cr,2))) ;


end
