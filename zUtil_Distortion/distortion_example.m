function distortion_example()
% DISTORTION_EXAMPLE

% To save a distortion map:
%  distortion_write(fname,map_vertical,map_horizontal)

% For undistortion, one must read the CCD distortion file
distortion = distortion_read('shifts_12_sfit56.dat');
% distortion will contain two fields:
%   distortion.x = vertical (!!) displacement field
%   distortion.y = horizontal (!!) displacement field

% read your weird EDFs here
im = edf_read('holeX_corrected_Z0.edf');
im = im';
% correct them like this (image and distortion maps have to be of the same type!)

turns = 40

tic()
for ii = 1:turns
  im_corr = distortion_correct(im, distortion);
end
correction = toc() / turns

% also try this
% [x,y]=meshgrid(1:size(im,1),1:size(im,2));
% x=matrix of coloumn coordinates (horizontal axis) (!!)
% y=matrix of row coordinates (vertical axis) (!!)

% tic
% im_corr2=interp2(im,x-distortion.y,y-distortion.x); % yes, really like this...
%  General usage: interp2(matrix,horizontal axis co-s,vertical axis co-s)
%   where the latter two indicates the locations for which to interpolate
%   from 'matrix'

%%% interp2 does not exactly do the same as distortion_correct !
%
% For interp2, the distortion field (map) should refer to the
% undistorted field, while for distortion correct the displacements should
% be stored on the distorted field.
% However, the difference in output is negligible when distortion is of the
% order of some pixels.
% / PeterR
%
% interpolation = toc()

% then have a look!
figure
imshow(im,[])
axis on
colorbar
title('Distorted image')
shg
figure
imshow(im_corr,[]);
axis on
colorbar
title('Corrected image')
shg
%figure
%imshow(im_corr2,[]);
%axis on
%colorbar
%title('Interp2')
%shg
end
