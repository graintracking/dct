/**
 * Original file by Christian Nemoz from ID17 @ ESRF nemoz@esrf.eu
 * Cleaned and optimized by Nicola Vigano from ID11 @ ESRF vigano@esrf.eu
 */
#include <math.h>
#include "mex.h"

void mexFunction( int nlhs, mxArray * plhs[], int nrhs, const mxArray * prhs[] )
{
  /* Incoming image dimensions */
  const mwSize numDims = mxGetNumberOfDimensions(prhs[0]);
  if (numDims != 2) {
    mexErrMsgIdAndTxt("distortion_correct:wrong_argument",
        "Images should be 2D");
  }
  // We exchange j for i because the images are in FORTRAN convention
  const unsigned int num_rows = mxGetM(prhs[0]), num_cols = mxGetN(prhs[0]);

  /* Pointers to incoming matrices: */
  const double * const image_in = mxGetPr(prhs[0]);
  /* Shift matrices, they may be swapped from external matlab call! */
  const double * const shifty   = mxGetPr(prhs[1]);
  const double * const shiftx   = mxGetPr(prhs[2]);

  /* Create a matrix for the return argument */
  plhs[0] = mxCreateDoubleMatrix(num_rows, num_cols, mxREAL);

  /* Pointers to outgoing matrix: */
  double * const image_out = mxGetPr(plhs[0]);

  unsigned int pt = 0;
  const double * image_in_tmp = image_in;

  // We exchange j for i because the images are in FORTRAN convention
  for(unsigned int i = 0; i < num_cols; i++)
  {
    const unsigned int extraCol = num_rows*( i < (num_cols-1) );

    for(unsigned int j = 0; j < num_rows; j++, pt++, image_in_tmp++)
    {
      /* compute 4 points defining target pixel, a rectangle. Corners are
       * defined by mean values of shifts taken 2 by 2 */
      /* FIXME SLOW pointer arithmetics */
      const unsigned int pt1  = pt + ( j < ( num_rows - 1 ) );
      const unsigned int ptn  = pt + extraCol;
      const unsigned int ptn1 = ptn + ( j < ( num_rows - 1 ) );

      /* target pixel coordinates: the pixel where the original pixel needs to
       * be shifted */
      const double x1 = i   + 0.5*(shiftx[pt ]+shiftx[pt1 ]);
      const double x2 = i+1 + 0.5*(shiftx[ptn]+shiftx[ptn1]);
      const double y1 = j   + 0.5*(shifty[pt ]+shifty[ptn ]);
      const double y2 = j+1 + 0.5*(shifty[pt1]+shifty[ptn1]);

      /* target coordinates within image? */
      const bool is_ok =  ( x1 >= 0. ) && ( x1 < num_cols )  &&
                         (( y1 >= 0. ) && ( y1 < num_rows )) &&
                          ( x2 >= 0. ) && ( x2 < num_cols )  &&
                         (( y2 >= 0. ) && ( y2 < num_rows ));

      if (is_ok)
      {
        /* compute surface of target pixel */
        const double val_sur_surf = (*image_in_tmp) / fabs( (x2-x1)*(y2-y1) );

        /* assume x2>x1 and y2>y1, and round values! */
        const unsigned int x_lower_lim_target = x1;
        const unsigned int x_upper_lim_target = x2;
        const unsigned int y_lower_lim_target = y1;
        const unsigned int y_upper_lim_target = y2;

        const bool is_x_tight = (x_upper_lim_target == x_lower_lim_target);
        const bool is_y_tight = (y_upper_lim_target == y_lower_lim_target);

        /* get the rounding */
        const double i1_x1 = (x_lower_lim_target+1 -                 x1);
        const double x2_i2 = (x2                   - x_upper_lim_target);
        const double j1_y1 = (y_lower_lim_target+1 -                 y1);
        const double y2_j2 = (y2                   - y_upper_lim_target);

        register unsigned int kx;
        for(kx = x_lower_lim_target; kx <= x_upper_lim_target; kx++)
        {
          const double & x_coeff = (kx == x_lower_lim_target)
              ? (is_x_tight ? (x2 - x1) : i1_x1)
              : ((kx == x_upper_lim_target) ? x2_i2 : 1.);

          double * const base_image_out = image_out + kx * num_rows;
          const double base_val = x_coeff * val_sur_surf;

          if (is_y_tight)
          {
            /* It's only one point */
            base_image_out[y_lower_lim_target] += (y2 - y1) * base_val;
          } else {
            /* Treat edge points separately */
            base_image_out[y_lower_lim_target] += j1_y1 * base_val;

            /* Internal points don't have problems with coefficients */
            for(register unsigned int ky = y_lower_lim_target+1;
                ky < y_upper_lim_target; ky++)
            {
              /* shifts the values to the target pixels */
              base_image_out[ky] += base_val;
            }

            /* Treat edge points separately */
            base_image_out[y_upper_lim_target] += y2_j2 * base_val;
          }
        }
      } /* end is_ok */
    } /* end i */
  } /* end j */
}
