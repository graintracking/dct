function distortion_createmap(inp, varargin)
% FUNCTION distortion_createmap(inp, varargin)
%
% Creates nodes and distortion map of an image.
% The input can be either an image (numeric array) or a .mat file containing
% the coordinates of nodes gained by e.g. 'distortion_createnodes.m'. as the
% following: ().
%
% INPUT e.g. distortion_createmap(inp, 'crop', 'manualnodes', 'SplineParMaps', 0.001, 'dojump')
%       e.g. distortion_createmap('nodes.mat', 'savemaps', 'distmap.dm')
%
%   inp: EITHER a numerical array of the source image
%        OR name of a .mat file containing the nodes as follows:
%          -  r, c coloumn vectors of row and coloumn coordinates of nodes
%          -  sx, sy the original image size in pixels
%   varargin:  see 'par' and its default values below; logical values can be
%              changed just by including their names in 'varargin',  other
%              values by including their ('name', value) pairs.
%
%
% Process:
%    -  locate nodes in image (or read them from file)
%    -  number nodes
%    -  determine average horizontal and vertical spacing vector (reference grid
%      for global displacement  =  distortion)
%    -  create spline of local spacing ('strain field'); reject outliers and
%      recreate it
%    -  reject dubious nodes on edges of the field of view
%    -  filter dubious nodes inside the field of view according to local
%      spacing and its pattern
%    -  confirm nodes to be used by manual (de)selection,  if wanted
%    -  compute displacements of nodes and create its spline fit
%    -  reject outliers and recreate distortion spline
%    -  calcaluate spline values for each pixel and save the distortion map,
%      if wanted
%
%
%
% by Peter Reischig,  ESRF,  06.2007
%
%

% x axis: vertical (1st matrix dimension)
% y axis: horizontal (2nd matrix dimension)

    %%% Parameters %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    par = [];
    par.Crop = false;                    % crop original image instead of using the whole
    par.ManualNodes = false;             % (de)select nodes to be considered manually
    par.NodesOnly = false;               % create nodes only,  no evaluation
    par.NoDisplay = false;               % do not display result graphs
    par.SaveNodes = [];                  % filename for saving image and nodes
    par.SaveMat = [];                    % filename for saving matlab variables
    par.SaveMaps = [];                   % filename for saving distortion maps

    par.SelBB = [0.375 0.375 0.25 0.25]; % bounding box for feature selection
    par.FeatureCropBB = [];
    par.OrdFiltSize = 1;

    par.NoFilter = false;                % filter bad nodes
    par.PeakLim = 0.5;                   % threshold applied to the correlation map

    % Search nodes along search vectors for numbering
    par.ManualVectors = false;           % set search vectors manually
    par.TolAngle = 20;                   % angular tolerance when locating neighbouring nodes; should be 10..40 (20)
    par.TolLength = 1.3;                 % distance tolerance when locating neighbouring nodes; should be 1.3..1.5 (1.3)
    par.DoJump = true;                  % ignore missing nodes and continue search for the following
    par.UseThNodes = 1;                  % use every Xth node

    % Locate dubious nodes to be disregarded
    par.TolDiffSpline = 2;               % tolerance factor (*std) for alteration from spline; should be 0..5 (2)
    par.TolDiffMask = 0.5;               % tolerance factor (*std) for diffraction pattern bandwith; should be 0..3 (0.5)

    % Reference grid for displacement (average grid spacing is sought automatically)
    par.GridOptDomain = 0.8;             % percentage of nodes closest to center to be used for determining spacing vectors
    par.GridHor = [];                    % vector of horizontal spacing (pointing appr. to the right)
    par.GridVer = [];                    % vector of vertical spacing (pointing appr. downwards)
    par.GridHorNorm = [];                % length of horizontal vector
    par.GridVerNorm = [];                % length of vertical vector
    par.GridUni = false;                 % length of horizontal and vertical vector is equal
    par.GridOrtho = false;               % grid is orthogonal

    % Distortion splines
    par.SplineParOutliers = 0.0000001;   % spline shape parameter for outlier rejection; should be 0..10e - 4 (10e - 7); 0 -> plane,  1 -> input data
    par.TolMapSpline = 2;                % tolerance factor (*std) for outlier rejection; should be 1..3 (2)
    par.SplineParMaps = 0.0001;          % spline shape parameter; should be 10e - 3..10e - 7 (1*10e - 4); 0 -> plane,  1 -> input data

    par.Description = [];                % description in nodes mat file
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


    par  =  parse_pv_pairs(par, varargin);
    warning('off', 'Images:initSize:adjustingMag');

    %keyboard

    if nargin == 0
        disp('Specify the source image,  please!')
        return
    end
    close all

    r  =  [];
    c  =  [];

    if isnumeric(inp)
        inp  =  double(inp);
        inp(isnan(inp))  =  0;
        % inp(inp == Inf) = 0;
        % inp(inp ==  - Inf) = 0;

        % Find nodes in image
        [r, c, sx, sy, limsh]  =  sfDistortion_Createnodes(par.SelBB,  par.OrdFiltSize,  par.FeatureCropBB,  inp,  par.Crop,  par.SaveNodes,  par.PeakLim,  par.Description);

    elseif (ischar(inp) && strcmp(inp(end - 3:end), '.edf'))
        disp(sprintf('Loading edf data file %s ...', inp))
        inp  =  double(edf_read(inp));

        % Find nodes in image
        [r, c, sx, sy, limsh]  =  sfDistortion_Createnodes(par.SelBB,  par.OrdFiltSize,  par.FeatureCropBB,  inp,  par.Crop,  par.SaveNodes,  par.PeakLim,  par.Description);

    else
        disp(sprintf('Loading Matlab data file %s ...', inp))
        load(inp);
        if exist('im', 'var')
            inp  =  double(im);
        else
            error('Specified Matlab data file does not contain any image (variable "im").')
        end
    end

    if par.NodesOnly == true
        return
    end

    % Color limits for viewing
    limsh  =  gtSetColourLimits(inp, 5);

    % else
    %   disp('First input argument has to be of type: numeric array or character.')
    %   return
    % end

    disp('Parameters used:')
    disp(par)
    tic

    %%% Number the nodes

    ro = r;
    co = c;

    if par.ManualVectors == true
        figure
        if isnumeric(inp)
            imshow(inp, limsh);
        end
        hold on
        plot(co, ro, 'r.')
        xlim([0 sy + 1])
        ylim([0 sx + 1])
        title('Please,  input a spacing vector closer to horizontal axis...')
        xlabel('Top')
        ylabel('Left edge')
        set(gca, 'YDir', 'rev')
        set(gca, 'XAxisLocation', 'top')
        drawnow
        [ci, ri]  =  ginput(2);
        if length(ci) == 2
            dist = (r - ri(1)).^2 + (c - ci(1)).^2;
            [mindist, basei] = min(dist);
            base = [r(basei) c(basei)]
            r(basei) = [];
            c(basei) = [];
            moveright = [ri(2) - ri(1) ci(2) - ci(1)]
            title('Please,  input a spacing vector closer to vertical axis...')
            drawnow
            [ci, ri]  =  ginput(2);
            if length(ci) == 2
                moveup = [ri(2) - ri(1) ci(2) - ci(1)]
            else
                title('Spacings vector have not been set manually.')
                drawnow
                disp('Spacings vector have not been set manually.')
                disp('Automatic selection')
                par.ManualVectors = false;
            end
        else
            disp('Spacings vector have not been set manually.')
            disp('Automatic selection')
            par.ManualVectors = false;
        end
    end

    if par.ManualVectors == false
        % find central node
        dist = (r - sx/2).^2 + (c - sy/2).^2;
        [mindist, basei] = min(dist);
        base = [r(basei) c(basei)];

        r(basei) = [];
        c(basei) = [];

        % find closest node to centre right and up
        dist = (r - base(1, 1)).^2 + (c - base(1, 2)).^2;
        [mindist, closeis] = sort(dist);

        [moveright, rightnode] = max(c(closeis(1:4)));
        rightnode = closeis(rightnode);
        moveright = [r(rightnode), c(rightnode)] - base;

        [moveup, upnode] = min(r(closeis(1:4)));
        upnode = closeis(upnode);
        moveup = [r(upnode), c(upnode)] - base;
    end



    % Create the axes of N(odes) matrix
    %pad = 100;
    pad = floor(min([2*max([sx sy])/min([norm(moveright), norm(moveup)]), 2*max([sx sy])]));

    N(1, 1, 1:2) = base;
    Ncorr(1, 1, 1:2) = NaN;

    move = moveright;
    [newbases, r, c, missingbases] = sfSearch(base, move, r, c, sx, sy, par.TolAngle, par.TolLength, par.DoJump);
    addN = NaN(1, size(newbases, 1) + pad, 2);
    addN(:, 1:size(newbases, 1), 1) = newbases(:, 1)';
    addN(:, 1:size(newbases, 1), 2) = newbases(:, 2)';
    N = [N addN];
    addN = NaN(1, size(newbases, 1) + pad, 2);
    addN(:, 1:size(newbases, 1), 1) = missingbases(:, 1)';
    addN(:, 1:size(newbases, 1), 2) = missingbases(:, 2)';
    Ncorr = [Ncorr addN];

    move =  - moveright;
    [newbases, r, c, missingbases] = sfSearch(base, move, r, c, sx, sy, par.TolAngle, par.TolLength, par.DoJump);
    addN = NaN(1, size(newbases, 1) + pad, 2);
    addN(:, end - size(newbases, 1) + 1:end, 1) = fliplr(newbases(:, 1)');
    addN(:, end - size(newbases, 1) + 1:end, 2) = fliplr(newbases(:, 2)');
    N = [addN N];
    addN = NaN(1, size(newbases, 1) + pad, 2);
    addN(:, end - size(newbases, 1) + 1:end, 1) = fliplr(missingbases(:, 1)');
    addN(:, end - size(newbases, 1) + 1:end, 2) = fliplr(missingbases(:, 2)');
    Ncorr = [addN Ncorr];

    move = moveup;
    [newbases, r, c, missingbases] = sfSearch(base, move, r, c, sx, sy, par.TolAngle, par.TolLength, par.DoJump);
    [baseN(1), baseN(2)] = find((N(:, :, 2) == base(2))&(N(:, :, 1) == base(1)));
    addN = NaN(size(newbases, 1) + pad, size(N, 2), 2);
    addN(end - size(newbases, 1) + 1:end, baseN(2), 1) = flipud(newbases(:, 1));
    addN(end - size(newbases, 1) + 1:end, baseN(2), 2) = flipud(newbases(:, 2));
    N = [addN; N];
    addN = NaN(size(newbases, 1) + pad, size(N, 2), 2);
    addN(end - size(newbases, 1) + 1:end, baseN(2), 1) = flipud(missingbases(:, 1));
    addN(end - size(newbases, 1) + 1:end, baseN(2), 2) = flipud(missingbases(:, 2));
    Ncorr = [addN; Ncorr];

    move =  - moveup;
    [newbases, r, c, missingbases] = sfSearch(base, move, r, c, sx, sy, par.TolAngle, par.TolLength, par.DoJump);
    [baseN(1), baseN(2)] = find((N(:, :, 2) == base(2))&(N(:, :, 1) == base(1)));
    addN = NaN(size(newbases, 1) + pad, size(N, 2), 2);
    addN(1:size(newbases, 1), baseN(2), 1) = newbases(:, 1);
    addN(1:size(newbases, 1), baseN(2), 2) = newbases(:, 2);
    N = [N; addN];
    addN = NaN(size(newbases, 1) + pad, size(N, 2), 2);
    addN(1:size(newbases, 1), baseN(2), 1) = missingbases(:, 1);
    addN(1:size(newbases, 1), baseN(2), 2) = missingbases(:, 2);
    Ncorr = [Ncorr; addN];

    [baseN(1), baseN(2)] = find((N(:, :, 2) == base(2))&(N(:, :, 1) == base(1)));


    % Determine and draw central tilts
    % Horizontal
    dummy = [];
    dummy(:, :, 1:2) = N(baseN(1), ~isnan(N(baseN(1), :, 1)), 1:2);
    p = polyfit(dummy(:, :, 2), dummy(:, :, 1), 1);
    tilthor = p(1);

    % Vertical
    dummy = [];
    dummy(:, :, 1:2) = N(~isnan(N(:, baseN(2), 1)), baseN(2), 1:2);
    p = polyfit(dummy(:, :, 1), dummy(:, :, 2), 1);
    tiltver = p(1);

    dummy = diff(N(baseN(1), :, 2));
    hor = mean(dummy(~isnan(dummy)));
    hor = [hor*tilthor hor];  % one node to the right

    dummy = diff(N(:, baseN(2), 1));
    ver = mean(dummy(~isnan(dummy)));
    ver = [ver ver*tiltver];  % one node downwards

    horo = hor;
    vero = ver;

    % Fill in the rest of N
    r1 = r;
    c1 = c;

    r2 = r;
    c2 = c;

    N1 = N;
    N2 = N;
    N1corr = Ncorr;
    N2corr = Ncorr;

    % 1. Along central row
    for ii = 1:size(N, 2)
        if ii == baseN(2)
            continue
        elseif ~isnan(N(baseN(1), ii, 1))
            base(1:2) = N(baseN(1), ii, 1:2);
        elseif ~isnan(Ncorr(baseN(1), ii, 1))
            base(1:2) = Ncorr(baseN(1), ii, 1:2);
        else
            continue
        end
        move =  - ver;
        [newbases, r1, c1, missingbases] = sfSearch(base, move, r1, c1, sx, sy, par.TolAngle, par.TolLength, par.DoJump);
        N1(baseN(1) - 1: - 1:baseN(1) - size(newbases, 1), ii, 1:2) = newbases;
        N1corr(baseN(1) - 1: - 1:baseN(1) - size(newbases, 1), ii, 1:2) = missingbases;

        move = ver;
        [newbases, r1, c1, missingbases] = sfSearch(base, move, r1, c1, sx, sy, par.TolAngle, par.TolLength, par.DoJump);
        N1(baseN(1) + 1:baseN(1) + size(newbases, 1), ii, 1:2) = newbases;
        N1corr(baseN(1) + 1:baseN(1) + size(newbases, 1), ii, 1:2) = missingbases;
    end

    % 2. Along central column
    for ii = 1:size(N, 1)
        if ii == baseN(1)
            continue
        elseif ~isnan(N(ii, baseN(2), 1))
            base(1:2) = N(ii, baseN(2), 1:2);
        elseif ~isnan(Ncorr(ii, baseN(2), 1))
            base(1:2) = Ncorr(ii, baseN(2), 1:2);
        else
            continue
        end
        move = hor;
        [newbases, r2, c2, missingbases] = sfSearch(base, move, r2, c2, sx, sy, par.TolAngle, par.TolLength, par.DoJump);
        N2(ii, baseN(2) + 1:baseN(2) + size(newbases, 1), 1:2) = newbases;
        N2corr(ii, baseN(2) + 1:baseN(2) + size(newbases, 1), 1:2) = missingbases;

        move =  - hor;
        [newbases, r2, c2, missingbases] = sfSearch(base, move, r2, c2, sx, sy, par.TolAngle, par.TolLength, par.DoJump);
        N2(ii, baseN(2) - 1: - 1:baseN(2) - size(newbases, 1), 1:2) = newbases;
        N2corr(ii, baseN(2) - 1: - 1:baseN(2) - size(newbases, 1), 1:2) = missingbases;
    end

    tokeep = (~isnan(N1) & ~isnan(N2));
    N(tokeep) = N1(tokeep);
    tokeep = (~isnan(N1) & isnan(N2));
    N(tokeep) = N1(tokeep);
    tokeep = (isnan(N1) & ~isnan(N2));
    N(tokeep) = N2(tokeep);

    % Remove padding
    N(1:find(sum(~isnan(N(:, :, 1)), 2), 1, 'first') - 1, :, :) = [];
    N(find(sum(~isnan(N(:, :, 1)), 2), 1, 'last') + 1:end, :, :) = [];
    N(:, 1:find(sum(~isnan(N(:, :, 1)), 1), 1, 'first') - 1, :) = [];
    N(:, find(sum(~isnan(N(:, :, 1)), 1), 1, 'last') + 1:end, :) = [];


    %%% Recreate N in both directions

    Nt = NaN(size(N, 1) + 2*pad, size(N, 2) + 2*pad, 2);
    N1 = Nt;
    N2 = Nt;
    N1corr = Nt;
    N2corr = Nt;
    Ncorr = Nt;

    r1 = ro;
    c1 = co;
    r2 = ro;
    c2 = co;

    % 1. Along N horizontal,  growing Nt vertical
    for ii = 1:size(N, 2)
        dist = abs((1:size(N, 1))' - size(N, 1)/2); % find closest no NaN value to center line
        dist(isnan(N(:, ii, 2))) = Inf;
        [mindist, basei] = min(dist);
        basen1 = pad + basei;
        basen2 = pad + ii;

        base(1:2) = N(basei, ii, 1:2);

        move =  - ver;
        [newbases, r1, c1, missingbases] = sfSearch(base, move, r1, c1, sx, sy, par.TolAngle, par.TolLength, par.DoJump);
        N1(basen1 - 1: - 1:basen1 - size(newbases, 1), basen2, 1:2) = newbases;
        N1corr(basen1 - 1: - 1:basen1 - size(newbases, 1), basen2, 1:2) = missingbases;

        base(1:2) = [N(basei, ii, 1) N(basei, ii, 2)] - ver;

        move = ver;
        [newbases, r1, c1, missingbases] = sfSearch(base, move, r1, c1, sx, sy, par.TolAngle, par.TolLength, par.DoJump);
        N1(basen1:basen1 + size(newbases, 1) - 1, basen2, 1:2) = newbases;
        N1corr(basen1:basen1 + size(newbases, 1) - 1, basen2, 1:2) = missingbases;
    end

    % 2. Along N vertical,  growing vertical
    for ii = 1:size(N, 1)
        dist = abs((1:size(N, 2))' - size(N, 2)/2); % find closest no NaN value to center line
        dist(isnan(N(ii, :, 1))) = Inf;
        [mindist, basei] = min(dist);
        basen1 = pad + ii;
        basen2 = pad + basei;

        base(1:2) = N(ii, basei, 1:2);

        move = hor;
        [newbases, r2, c2, missingbases] = sfSearch(base, move, r2, c2, sx, sy, par.TolAngle, par.TolLength, par.DoJump);
        N2(basen1, basen2 + 1:basen2 + size(newbases, 1), 1:2) = newbases;
        N2corr(basen1, basen2 + 1:basen2 + size(newbases, 1), 1:2) = missingbases;

        base(1:2) = [N(ii, basei, 1) N(ii, basei, 2)] + hor;

        move =  - hor;
        [newbases, r2, c2, missingbases] = sfSearch(base, move, r2, c2, sx, sy, par.TolAngle, par.TolLength, par.DoJump);
        N2(basen1, basen2: - 1:basen2 - size(newbases, 1) + 1, 1:2) = newbases;
        N2corr(basen1, basen2: - 1:basen2 - size(newbases, 1) + 1, 1:2) = missingbases;
    end

    tokeep = (~isnan(N1) & ~isnan(N2));
    Nt(tokeep) = N1(tokeep); % load nodes in Nt which were found in both ways
    if par.DoJump
        tokeep = (~isnan(N1) & isnan(N2)); % also load those found either way
        Nt(tokeep) = N1(tokeep);
        tokeep = (isnan(N1) & ~isnan(N2));
        Nt(tokeep) = N2(tokeep);

        % Ncorr: missing nodes that have a correction available ...
        tocorr = (isnan(N1) & isnan(N2) & (~isnan(N1corr) & ~isnan(N2corr))); % both ways
        Ncorr(tocorr) = (N1corr(tocorr) + N2corr(tocorr))/2;
        tocorr = (isnan(N1) & isnan(N2) & (~isnan(N1corr) & isnan(N2corr))); % from N1corr
        Ncorr(tocorr) = N1corr(tocorr);
        tocorr = (isnan(N1) & isnan(N2) & (isnan(N1corr) & ~isnan(N2corr))); % from N2corr
        Ncorr(tocorr) = N2corr(tocorr);
    end

    % Corrected N for missing nodes
    Nc = Nt;
    Nc(~isnan(Ncorr)) = Ncorr(~isnan(Ncorr));

    % Remove padding
    Nc(1:find(sum(~isnan(Nc(:, :, 1)), 2), 1, 'first') - 1, :, :) = [];
    Nc(find(sum(~isnan(Nc(:, :, 1)), 2), 1, 'last') + 1:end, :, :) = [];
    Nc(:, 1:find(sum(~isnan(Nc(:, :, 1)), 1), 1, 'first') - 1, :) = [];
    Nc(:, find(sum(~isnan(Nc(:, :, 1)), 1), 1, 'last') + 1:end, :) = [];


    %%% Determine average spacing

    dnc212 = diff(Nc(:, :, 2), 1, 2);
    dnc111 = diff(Nc(:, :, 1), 1, 1);
    dnc112 = diff(Nc(:, :, 1), 1, 2);
    dnc211 = diff(Nc(:, :, 2), 1, 1);
    hor = [];
    ver = [];

    if isempty(par.GridHor)
        spacing = dnc212(:);
        [nl2, nl1] = meshgrid(1:size(dnc212, 2), 1:size(dnc212, 1));
        nl1 = nl1(:);
        nl2 = nl2(:);
        nl1 = nl1(~isnan(spacing));
        nl2 = nl2(~isnan(spacing));
        spacing = spacing(~isnan(spacing));
        dist = (nl1 - size(dnc212, 1)/2 - 0.5).^2 + (nl2 - size(dnc212, 2)/2 - 0.5).^2;
        [dist, closest] = sort(dist);
        spacing = spacing(closest(1:ceil(length(closest)*par.GridOptDomain)));
        hor(2) = mean(spacing(spacing < mean(spacing) + 2*std(spacing) & spacing > mean(spacing) - 2*std(spacing)));

        spacing = dnc112(:);
        [nl2, nl1] = meshgrid(1:size(dnc112, 2), 1:size(dnc112, 1));
        nl1 = nl1(:);
        nl2 = nl2(:);
        nl1 = nl1(~isnan(spacing));
        nl2 = nl2(~isnan(spacing));
        spacing = spacing(~isnan(spacing));
        dist = (nl1 - size(dnc112, 1)/2 - 0.5).^2 + (nl2 - size(dnc112, 2)/2 - 0.5).^2;
        [dist, closest] = sort(dist);
        spacing = spacing(closest(1:ceil(length(closest)*par.GridOptDomain)));
        hor(1) = mean(spacing(spacing < mean(spacing) + 2*std(spacing) & spacing > mean(spacing) - 2*std(spacing)));

        %  if ~isempty(par.GridHorNorm)
        %    hor = hor/norm(hor)*par.GridHorNorm;
        %  end
    else
        hor = par.GridHor;
    end

    if isempty(par.GridVer)
        spacing = dnc211(:);
        [nl2, nl1] = meshgrid(1:size(dnc211, 2), 1:size(dnc211, 1));
        nl1 = nl1(:);
        nl2 = nl2(:);
        nl1 = nl1(~isnan(spacing));
        nl2 = nl2(~isnan(spacing));
        spacing = spacing(~isnan(spacing));
        dist = (nl1 - size(dnc211, 1)/2 - 0.5).^2 + (nl2 - size(dnc211, 2)/2 - 0.5).^2;
        [dist, closest] = sort(dist);
        spacing = spacing(closest(1:ceil(length(closest)*par.GridOptDomain)));
        ver(2) = mean(spacing(spacing < mean(spacing) + 2*std(spacing) & spacing > mean(spacing) - 2*std(spacing)));

        spacing = dnc111(:);
        [nl2, nl1] = meshgrid(1:size(dnc111, 2), 1:size(dnc111, 1));
        nl1 = nl1(:);
        nl2 = nl2(:);
        nl1 = nl1(~isnan(spacing));
        nl2 = nl2(~isnan(spacing));
        spacing = spacing(~isnan(spacing));
        dist = (nl1 - size(dnc111, 1)/2 - 0.5).^2 + (nl2 - size(dnc111, 2)/2 - 0.5).^2;
        [dist, closest] = sort(dist);
        spacing = spacing(closest(1:ceil(length(closest)*par.GridOptDomain)));
        ver(1) = mean(spacing(spacing < mean(spacing) + 2*std(spacing) & spacing > mean(spacing) - 2*std(spacing)));

        %  if ~isempty(par.GridVerNorm)
        %    ver = ver/norm(ver)*par.GridVerNorm;
        %  end
    else
        ver = par.GridVer;
    end

    if ~isempty(par.GridHorNorm)
        hor = hor/norm(hor)*par.GridHorNorm;
    end

    if ~isempty(par.GridVerNorm)
        ver = ver/norm(ver)*par.GridVerNorm;
    end

    if par.GridUni
        unilength = (norm(hor) + norm(ver))/2;
        hor = hor/norm(hor)*unilength;
        ver = ver/norm(ver)*unilength;
    end

    if par.GridOrtho
        avvec = (hor/norm(hor) + ver/norm(ver));
        orthovec = [avvec(2), -avvec(1)]; % pointing left down

        newver = avvec + orthovec;
        ver = newver/norm(newver)*norm(ver);

        newhor = avvec - orthovec;
        hor = newhor/norm(newhor)*norm(hor);
    end




    Nc = Nc(1:par.UseThNodes:end, 1:par.UseThNodes:end, 1:2);
    hor = hor*par.UseThNodes;
    ver = ver*par.UseThNodes;

    %%% First creation of splines for outlier rejection (defined by all nodes inside)
    dnc212 = diff(Nc(:, :, 2), 1, 2);
    dnc222 = diff(Nc(:, :, 2), 2, 2);
    dnc111 = diff(Nc(:, :, 1), 1, 1);
    dnc121 = diff(Nc(:, :, 1), 2, 1);

    dnc121e = [NaN(1, size(dnc121, 2));dnc121;NaN(1, size(dnc121, 2))];
    dnc222e = [NaN(size(dnc222, 1), 1), dnc222, NaN(size(dnc222, 1), 1)];


    nc1 = Nc(2:end - 1, 2:end - 1, 1);
    nc1 = nc1(:)';
    nc2 = Nc(2:end - 1, 2:end - 1, 2);
    nc2 = nc2(:)';
    [nl2, nl1] = meshgrid(2:1:size(Nc, 2) - 1, 2:1:size(Nc, 1) - 1);
    nl1 = nl1(:)';
    nl2 = nl2(:)';
    dnc222(1, :) = [];
    dnc222(end, :) = [];
    dnvec222 = dnc222(:)';
    dnc121(:, 1) = [];
    dnc121(:, end) = [];
    dnvec121 = dnc121(:)';

    tocut = (isnan(dnvec121) | isnan(dnvec222));
    nc1(tocut) = [];
    nc2(tocut) = [];
    nl1(tocut) = [];
    nl2(tocut) = [];
    dnvec121(tocut) = [];
    dnvec222(tocut) = [];

    Nkeep = zeros(size(Nc(:, :, 1)));
    for ii = 1:length(nl1)
        Nkeep(nl1(ii), nl2(ii)) = 1;
    end

    if ~par.NoFilter

        disp(' ')
        disp('Applying filter...')
        disp(' ')

        spd121 = tpaps([nc1;nc2], dnvec121, par.SplineParOutliers);
        spd121vals = fnval(spd121, [nc1;nc2]);
        spd121_err = dnvec121 - spd121vals;
        std_spd121 = std(spd121_err);
        tol_spd121 = par.TolDiffSpline*std_spd121;

        spd222 = tpaps([nc1;nc2], dnvec222, par.SplineParOutliers);
        spd222vals = fnval(spd222, [nc1;nc2]);
        spd222_err = dnvec222 - spd222vals;
        std_spd222 = std(spd222_err);
        tol_spd222 = par.TolDiffSpline*std_spd222;

        tocut = (abs(spd222_err) > tol_spd222 | abs(spd121_err) > tol_spd121);
        nc1(tocut) = [];
        nc2(tocut) = [];
        nl1(tocut) = [];
        nl2(tocut) = [];
        dnvec121(tocut) = [];
        dnvec222(tocut) = [];
        spd121_err(tocut) = [];
        spd222_err(tocut) = [];



        %%% Redefine splines and recalculate outliers (bad nodes are left out)

        spd121 = tpaps([nc1;nc2], dnvec121, par.SplineParOutliers);
        spd222 = tpaps([nc1;nc2], dnvec222, par.SplineParOutliers);

        spd121vals = fnval(spd121, [nc1;nc2]);
        spd121_err = dnvec121 - spd121vals;
        std_spd121 = std(spd121_err);
        tol_spd121 = par.TolDiffSpline*std_spd121;

        spd222vals = fnval(spd222, [nc1;nc2]);
        spd222_err = dnvec222 - spd222vals;
        std_spd222 = std(spd222_err);
        tol_spd222 = par.TolDiffSpline*std_spd222;


        %%% Treat rows

        dnc212 = diff(Nc(:, :, 2), 1, 2);
        dnc222 = diff(Nc(:, :, 2), 2, 2);
        dnc232 = diff(Nc(:, :, 2), 3, 2);

        dnc111 = diff(Nc(:, :, 1), 1, 1);
        dnc121 = diff(Nc(:, :, 1), 2, 1);
        dnc131 = diff(Nc(:, :, 1), 3, 1);

        for ii = 1:size(Nc, 1)
            vs = ~isnan(Nc(ii, :, 2));
            first = find(vs, 1, 'first');
            last = find(vs, 1, 'last');
            vsd2 = dnc222(ii, :);

            if (length(find(vs)) >= 3 && ~isnan(vsd2(first)) && ~isnan(vsd2(last - 2)))
                if abs(vsd2(first) - fnval(spd222, [Nc(ii, first + 1, 1);Nc(ii, first + 1, 2)])) < tol_spd222
                    if abs(vsd2(last - 2) - fnval(spd222, [Nc(ii, last - 1, 1);Nc(ii, last - 1, 2)])) < tol_spd222
                        % no corrupted node at edges
                        Nkeep(ii, first) = Nkeep(ii, first) + 1;
                        Nkeep(ii, last) = Nkeep(ii, last) + 1;
                        Nkeep(ii, [first + 1 last - 1]) = Nkeep(ii, [first + 1 last - 1]) + 1;
                    else
                        % only last node corrupted
                        Nkeep(ii, first) = Nkeep(ii, first) + 1;
                        Nkeep(ii, last) = Nkeep(ii, last) - 1;
                        Nkeep(ii, [first + 1 last - 1]) = Nkeep(ii, [first + 1 last - 1]) + 1;
                    end
                else
                    if abs(vsd2(last - 2) - fnval(spd222, [Nc(ii, last - 1, 1);Nc(ii, last - 1, 2)])) < tol_spd222
                        % only first node corrupted
                        Nkeep(ii, first) = Nkeep(ii, first) - 1;
                        Nkeep(ii, last) = Nkeep(ii, last) + 1;
                        Nkeep(ii, [first + 1 last - 1]) = Nkeep(ii, [first + 1 last - 1]) + 1;
                    else
                        % both nodes corrupted
                        Nkeep(ii, first) = Nkeep(ii, first) - 1;
                        Nkeep(ii, last) = Nkeep(ii, last) - 1;
                        Nkeep(ii, [first + 1 last - 1]) = Nkeep(ii, [first + 1 last - 1]) + 1;
                    end
                end
            end
        end % of loop for rows


        %%% Treat columns

        for ii = 1:size(Nc, 2)
            vs = ~isnan(Nc(:, ii, 1));
            first = find(vs, 1, 'first');
            last = find(vs, 1, 'last');
            vsd2 = dnc121(:, ii);

            if (length(find(vs)) >= 3 && ~isnan(vsd2(first)) && ~isnan(vsd2(last - 2)))
                if abs(vsd2(first) - fnval(spd121, [Nc(first + 1, ii, 1);Nc(first + 1, ii, 2)])) < tol_spd121
                    if abs(vsd2(last - 2) - fnval(spd121, [Nc(last - 1, ii, 1);Nc(last - 1, ii, 2)])) < tol_spd121
                        % no corrupted node at edges
                        Nkeep(first, ii) = Nkeep(first, ii) + 1;
                        Nkeep(last, ii) = Nkeep(last, ii) + 1;
                        Nkeep([first + 1 last - 1], ii) = Nkeep([first + 1 last - 1], ii) + 1;
                    else
                        % only last node corrupted
                        Nkeep(first, ii) = Nkeep(first, ii) + 1;
                        Nkeep(last, ii) = Nkeep(last, ii) - 1;
                        Nkeep([first + 1 last - 1], ii) = Nkeep([first + 1 last - 1], ii) + 1;
                    end
                else
                    if abs(vsd2(last - 2) - fnval(spd121, [Nc(last - 1, ii, 1);Nc(last - 1, ii, 2)])) < tol_spd121
                        % only first node corrupted
                        Nkeep(first, ii) = Nkeep(first, ii) - 1;
                        Nkeep(last, ii) = Nkeep(last, ii) + 1;
                        Nkeep([first + 1 last - 1], ii) = Nkeep([first + 1 last - 1], ii) + 1;
                    else
                        % both nodes corrupted
                        Nkeep(first, ii) = Nkeep(first, ii) - 1;
                        Nkeep(last, ii) = Nkeep(last, ii) - 1;
                        Nkeep([first + 1 last - 1], ii) = Nkeep([first + 1 last - 1], ii) + 1;
                    end
                end
            end
        end % of loop for coloumns

        Nkeep(Nkeep >= 1) = 1;
        Nkeep(Nkeep < 1) = 0;


        %%% Treat dubious nodes

        Ndub2 = false(size(Nc(:, :, 1)));
        Ndub1 = false(size(Nc(:, :, 1)));

        nc1 = Nc(2:end - 1, 2:end - 1, 1);
        nc1 = nc1(:)';
        nc2 = Nc(2:end - 1, 2:end - 1, 2);
        nc2 = nc2(:)';
        dnc222(1, :) = [];
        dnc222(end, :) = [];
        dnc121(:, 1) = [];
        dnc121(:, end) = [];

        % mask: diffraction pattern of second derivatives and deviation of value from spline
        mask = zeros(size(dnc222));
        spv_dnc222 = mask; % spline values in vector form
        spv_dnc222(:) = fnval(spd222, [nc1;nc2]);
        mask(dnc222 > spv_dnc222 + par.TolDiffMask*std_spd222) = 1;
        mask(dnc222 < spv_dnc222 - par.TolDiffMask*std_spd222) =  - 1;
        mask = (abs(conv2(mask, [1  - 1 1], 'same')) == 3); % mask 1 acc to diff pattern
        mask = mask&(abs(dnc222 - spv_dnc222) > par.TolDiffSpline*std_spd222); % and deviation of value
        Ndub2(2:end - 1, 2:end - 1) = (mask == true);

        mask = zeros(size(dnc121));
        spv_dnc121 = mask;
        spv_dnc121(:) = fnval(spd121, [nc1;nc2]);
        mask(dnc121 > spv_dnc121 + par.TolDiffMask*std_spd121) = 1;
        mask(dnc121 < spv_dnc121 - par.TolDiffMask*std_spd121) =  - 1;
        mask = (abs(conv2(mask, [1;  - 1; 1], 'same')) == 3);
        mask = mask&(abs(dnc121 - spv_dnc121) > par.TolDiffSpline*std_spd121);
        Ndub1(2:end - 1, 2:end - 1) = (mask == true);

        % diffraction pattern of third derivatives
        mean_dnc232 = dnc232(~isnan(dnc232(:)));
        std_dnc232 = std(mean_dnc232);
        mean_dnc232 = mean(mean_dnc232);
        mask = zeros(size(dnc232));
        mask(dnc232 > mean_dnc232 + par.TolDiffMask*std_dnc232) = 1;
        mask(dnc232 < mean_dnc232 - par.TolDiffMask*std_dnc232) =  - 1;
        mask = (abs(conv2(mask, [1  - 1 1  - 1], 'same')) == 4);
        Ndub2(:, 3:end - 1) = (Ndub2(:, 3:end - 1)&(mask == true));

        mean_dnc131 = dnc131(~isnan(dnc131(:)));
        std_dnc131 = std(mean_dnc131);
        mean_dnc131 = mean(mean_dnc131);
        mask = zeros(size(dnc131));
        mask(dnc131 > mean_dnc131 + par.TolDiffMask*std_dnc131) = 1;
        mask(dnc131 < mean_dnc131 - par.TolDiffMask*std_dnc131) =  - 1;
        mask = (abs(conv2(mask, [1;  - 1; 1;  - 1], 'same')) == 4);
        Ndub1(3:end - 1, :) = (Ndub1(3:end - 1, :)&(mask == true));

        Ndub = (Ndub1|Ndub2);
        Nuse = (~isnan(Nc(:, :, 1)) & Nkeep & ~Ndub);

    else
        Nuse = ~isnan(Nc(:, :, 1));
    end


    %%% Select nodes by hand

    % Remaining nodes
    %rrem = [];
    %crem = [];
    %if length(c1) <  = length(c2)
    %  for i = 1:length(c1)
    %    if any((r2 - r1(i)).^2 + (c2 - c1(i)).^2 == 0)
    %      rrem = [rrem; r1(i)];
    %      crem = [crem; c1(i)];
    %    end
    %  end
    %else
    %  for i = 1:length(c2)
    %    if any((r1 - r2(i)).^2 + (c1 - c2(i)).^2 == 0)
    %      rrem = [rrem; r2(i)];
    %      crem = [crem; c2(i)];
    %    end
    %  end
    %end

    % Nodes
    nc1 = Nc(:, :, 1);
    nc1 = nc1(:)';
    nc2 = Nc(:, :, 2);
    nc2 = nc2(:)';
    dnvec121 = dnc121e(:)';
    dnvec222 = dnc222e(:)';
    [nl2, nl1] = meshgrid(1:size(Nc, 2), 1:size(Nc, 1));
    nl1 = nl1(:)';
    nl2 = nl2(:)';
    nu = Nuse;
    nu = nu(:)';

    tokeep = ~isnan(nc1(:));
    nc1 = nc1(tokeep);
    nc2 = nc2(tokeep);
    nl1 = nl1(tokeep);
    nl2 = nl2(tokeep);
    dnvec121 = dnvec121(tokeep);
    dnvec222 = dnvec222(tokeep);
    nu = nu(tokeep);

    % Grid
    dist = (nc1 - sx/2).^2 + (nc2 - sy/2).^2;
    [mindist, cnode] = min(dist);

    grid1 = repmat([nc1(cnode), nc2(cnode)], length(nc1), 1) + (nl1 - nl1(cnode))'*ver + (nl2 - nl2(cnode))'*hor;
    grid2 = grid1(:, 2)';
    grid1 = grid1(:, 1)';

    if par.NoDisplay == false || par.ManualNodes == true

        f_nc1 = figure('Name', 'Vertical node locations', 'Numbertitle', 'off');
        imagesc(Nc(:, :, 1))
        set(gcf, 'Menubar', 'none')
        set(f_nc1, 'Units', 'normalized')
        set(gca, 'XAxisLocation', 'top')
        set(gcf, 'Position', [0.4788 0.7568 0.2559 0.1948]);
        set(gca, 'Position', [0.2 0.02 0.6 0.9]);
        axis equal

        f_nc2 = figure('Name', 'Horizontal node locations', 'Numbertitle', 'off');
        imagesc(Nc(:, :, 2))
        set(gcf, 'Menubar', 'none')
        set(gca, 'XAxisLocation', 'top')
        set(f_nc2, 'Units', 'normalized')
        set(gcf, 'Position', [0.7425 0.7568 0.2527 0.1948]);
        set(gca, 'Position', [0.2 0.02 0.6 0.9]);
        axis equal

        f_dnc111 = figure('Name', 'Vertical node spacing (`local strain`)', 'Numbertitle', 'off');
        imagesc(dnc111)
        hold on
        f111x = 0;
        f111y = 0;
        f111a = plot(f111x, f111y, 'w + ', 'Markersize', 10);
        f111b = plot(f111x, f111y, 'k.', 'Markersize', 8);
        set(f111a, 'xdatasource', 'f111x')
        set(f111b, 'xdatasource', 'f111x')
        set(f111a, 'ydatasource', 'f111y')
        set(f111b, 'ydatasource', 'f111y')
        set(gca, 'XAxisLocation', 'top')
        set(gcf, 'Menubar', 'none')
        set(gcf, 'Units', 'normalized', 'Position', [0.0039 0.0300 0.2300 0.2600]);
        set(gca, 'Position', [0.01 0.01 0.98 0.98]);
        axis equal
        a_dnc111 = gca;

        f_dnc121 = figure('Name', 'Derivative of vertical node spacing', 'Numbertitle', 'off');
        imagesc(dnc121e)
        hold on
        f121x = 0;
        f121y = 0;
        f121a = plot(f121x, f121y, 'w + ', 'Markersize', 10);
        f121b = plot(f121x, f121y, 'k.', 'Markersize', 8);
        set(f121a, 'xdatasource', 'f121x')
        set(f121b, 'xdatasource', 'f121x')
        set(f121a, 'ydatasource', 'f121y')
        set(f121b, 'ydatasource', 'f121y')
        set(gca, 'XAxisLocation', 'top')
        set(gcf, 'Menubar', 'none')
        set(gcf, 'Units', 'normalized', 'Position', [0.0039 0.3150 0.2300 0.2600]);
        set(gca, 'Position', [0.01 0.01 0.98 0.98]);
        axis equal
        a_dnc121 = gca;

        f_dnc212 = figure('Name', 'Horizontal node spacing (`local strain`)', 'Numbertitle', 'off');
        imagesc(dnc212)
        hold on
        f212x = 0;
        f212y = 0;
        f212a = plot(f212x, f212y, 'w + ', 'Markersize', 10);
        f212b = plot(f212x, f212y, 'k.', 'Markersize', 8);
        set(f212a, 'xdatasource', 'f212x')
        set(f212b, 'xdatasource', 'f212x')
        set(f212a, 'ydatasource', 'f212y')
        set(f212b, 'ydatasource', 'f212y')
        set(gca, 'XAxisLocation', 'top')
        set(gcf, 'Menubar', 'none')
        set(gcf, 'Units', 'normalized', 'Position', [0.2410 0.0300 0.2300 0.2600]);
        set(gca, 'Position', [0.01 0.01 0.98 0.98]);
        axis equal
        a_dnc212 = gca;

        f_dnc222 = figure('Name', 'Derivative of horizontal node spacing', 'Numbertitle', 'off');
        imagesc(dnc222e)
        hold on
        f222a = plot(f121x, f121y, 'w + ', 'Markersize', 10);
        f222b = plot(f121x, f121y, 'k.', 'Markersize', 8);
        set(f222a, 'xdatasource', 'f121x')
        set(f222b, 'xdatasource', 'f121x')
        set(f222a, 'ydatasource', 'f121y')
        set(f222b, 'ydatasource', 'f121y')
        set(gca, 'XAxisLocation', 'top')
        set(gcf, 'Menubar', 'none')
        set(gcf, 'Units', 'normalized', 'Position', [0.2410 0.3150 0.2300 0.2600])
        set(gca, 'Position', [0.01 0.01 0.98 0.98]);
        axis equal
        a_dnc222 = gca;


        f_full = figure('Name', 'Full image', 'Numbertitle', 'off');
        axis equal
        xlim([0 sy + 1])
        ylim([0 sx + 1])
        set(gcf, 'Menubar', 'none')
        set(gcf, 'Toolbar', 'figure')
        set(gcf, 'Units', 'normalized', 'Position', [0.0039 0.6024 0.1609 0.3189]);
        set(gca, 'Position', [0.01 0.01 0.98 0.98]);
        if isnumeric(inp)
            imshow(inp, limsh);
        end

        f_zoom = figure('Name', 'Zoom', 'Numbertitle', 'off');
        set(gcf, 'Menubar', 'none')
        set(gcf, 'Units', 'normalized', 'Position', [0.1727 0.6024 0.2998 0.3491]);
        set(gca, 'Position', [0.01 0.01 0.98 0.98]);
        axis equal
        if isnumeric(inp)
            imshow(inp, limsh);
        end
        hold on
        plot(co, ro, 'r + ', 'MarkerSize', 15)
        plot(nc2(nu), nc1(nu), 'g + ', 'MarkerSize', 15)
        plot(grid2(nu), grid1(nu), 'go', 'MarkerSize', 15)
        plot(grid2(~nu), grid1(~nu), 'ro', 'MarkerSize', 15)
        xlim([sy/2 - 2*norm(hor) sy/2 + 2*norm(hor)]);
        ylim([sx/2 - 2*norm(hor) sx/2 + 2*norm(hor)]);
        a_zoom = gca;

        % Input
        f_nodes = figure('Name', 'Node selection', 'Numbertitle', 'off');
        if isnumeric(inp)
            imshow(inp, limsh);
        end
        hold on
        axis equal
        plot(co, ro, 'r.', 'MarkerSize', 20)
        plot(nc2(nu), nc1(nu), 'g.', 'MarkerSize', 20)
        xlim([0 sy + 1])
        ylim([0 sx + 1])
        set(gcf, 'Menubar', 'none')
        set(gcf, 'Units', 'normalized', 'Position', [0.4788 0.0262 0.5165 0.7033]);
        set(gca, 'Position', [0 0 1 0.95]);
        a_nodes = gca;

        drawnow

        if par.ManualNodes == true
            title('(De)select nodes to be used for splines...')
            drawnow 
            but = 1;
            range = (hor*hor')/9;
            xlimo = get(a_dnc222, 'XLim');
            ylimo = get(a_dnc222, 'YLim');
            xlimax = size(Nc, 2) + 1;
            ylimax = size(Nc, 1) + 1;

            while (but == 1 || but == 3)
                [ci, ri, but]  =  ginput(1);
                if isempty(ci)
                    but = 0;
                else
                    [mindist, nodei] = min((nc2 - ci).^2 + (nc1 - ri).^2);
                    if but == 1
                        if mindist < range
                            nu(nodei) = ~nu(nodei);
                            if nu(nodei) == true
                                plot(a_nodes, nc2(nodei), nc1(nodei), 'g.', 'MarkerSize', 20)
                                plot(a_zoom, nc2(nodei), nc1(nodei), 'g + ', 'MarkerSize', 15)
                                plot(a_zoom, grid2(nodei), grid1(nodei), 'go', 'MarkerSize', 15)
                            else
                                plot(a_nodes, nc2(nodei), nc1(nodei), 'r.', 'MarkerSize', 20)
                                plot(a_zoom, nc2(nodei), nc1(nodei), 'r + ', 'MarkerSize', 15)
                                plot(a_zoom, grid2(nodei), grid1(nodei), 'ro', 'MarkerSize', 15)
                            end
                            f111x = nl2(nu);
                            f111y = nl1(nu) - 0.5;
                            f121x = nl2(nu);
                            f121y = nl1(nu);
                            f212x = nl2(nu) - 0.5;
                            f212y = nl1(nu);
                            refreshdata([f111a f111b f121a f121b f212a f212b f222a f222b], 'caller');
                        end
                    end
                    if (ci < sy && ci > 0 && ri < sx && ri > 0)
                        f111x = nl2(nu);
                        f111y = nl1(nu) - 0.5;
                        f121x = nl2(nu);
                        f121y = nl1(nu);
                        f212x = nl2(nu) - 0.5;
                        f212y = nl1(nu);
                        refreshdata([f111a f111b f121a f121b f212a f212b f222a f222b], 'caller');
                        set(a_zoom, 'xlim', [max(0, ci - 2*norm(hor)) min(sy, ci + 2*norm(hor))])
                        set(a_zoom, 'ylim', [max(0, ri - 2*norm(hor)) min(sx, ri + 2*norm(hor))])
                        set(a_dnc111, 'xlim', [max(0, nl2(nodei) - 10) min(xlimax, nl2(nodei) + 10)])
                        set(a_dnc111, 'ylim', [max(0, nl1(nodei) - 10) min(ylimax, nl1(nodei) + 10)])
                        set(a_dnc121, 'xlim', [max(0, nl2(nodei) - 10) min(xlimax, nl2(nodei) + 10)])
                        set(a_dnc121, 'ylim', [max(0, nl1(nodei) - 10) min(ylimax, nl1(nodei) + 10)])
                        set(a_dnc212, 'xlim', [max(0, nl2(nodei) - 10) min(xlimax, nl2(nodei) + 10)])
                        set(a_dnc212, 'ylim', [max(0, nl1(nodei) - 10) min(ylimax, nl1(nodei) + 10)])
                        set(a_dnc222, 'xlim', [max(0, nl2(nodei) - 10) min(xlimax, nl2(nodei) + 10)])
                        set(a_dnc222, 'ylim', [max(0, nl1(nodei) - 10) min(ylimax, nl1(nodei) + 10)])
                    else
                        f111x = [];
                        f111y = [];
                        f121x = [];
                        f121y = [];
                        f212x = [];
                        f212y = [];
                        refreshdata([f111a f111b f121a f121b f212a f212b f222a f222b], 'caller');
                        set(a_dnc111, 'xlim', xlimo)
                        set(a_dnc111, 'ylim', ylimo)
                        set(a_dnc121, 'xlim', xlimo)
                        set(a_dnc121, 'ylim', ylimo)
                        set(a_dnc212, 'xlim', xlimo)
                        set(a_dnc212, 'ylim', ylimo)
                        set(a_dnc222, 'xlim', xlimo)
                        set(a_dnc222, 'ylim', ylimo)
                    end
                end
            end
            set(a_dnc111, 'xlim', xlimo)
            set(a_dnc111, 'ylim', ylimo)
            set(a_dnc121, 'xlim', xlimo)
            set(a_dnc121, 'ylim', ylimo)
            set(a_dnc212, 'xlim', xlimo)
            set(a_dnc212, 'ylim', ylimo)
            set(a_dnc222, 'xlim', xlimo)
            set(a_dnc222, 'ylim', ylimo)
        end

        figure(f_nodes)
        title('Generating distortion splines...')
        drawnow

    end

    nc1 = nc1(nu);
    nc2 = nc2(nu);
    nl1 = nl1(nu);
    nl2 = nl2(nu);

    %%% Global distortion (displacements)

    % Find central node
    dist = (nc1 - sx/2).^2 + (nc2 - sy/2).^2;
    [mindist, cnode] = min(dist);

    % Compute displacements (vector pointing from where a node is to the location where it
    % should be)
    grid1 = repmat([nc1(cnode), nc2(cnode)], length(nc1), 1) + (nl1 - nl1(cnode))'*ver + (nl2 - nl2(cnode))'*hor;
    disp1 = grid1 - [nc1', nc2'];

    grid2 = grid1(:, 2)'; % grid locations horizontal
    grid1 = grid1(:, 1)'; % grid locations vertical
    disp2 = disp1(:, 2)'; % horizontal displacements
    disp1 = disp1(:, 1)'; % vertical displacements

    %%% Spline for global vertical distortion

    disp(' ')
    disp('Generating distortion splines...')
    disp(' ')

    nc1v = nc1; % vertical node coordinates for treating vertical dist
    nc2v = nc2; % horizotal node coordinates for treating vertical dist
    disp1v = disp1; % vertical displacements for treating vertical dist
    %disp2v = disp2;

    % reject outliers
    spver = tpaps([nc1v;nc2v], disp1v, par.SplineParMaps);
    spvals = fnval(spver, [nc1v;nc2v]);
    spver_err = disp1v - spvals; % spline errors
    stdver = std(spver_err); % standard deviation of errors
    tol_spver = par.TolMapSpline*stdver;

    tocut = abs(spver_err) > tol_spver; % where errors larger than tolerance
    nc1v(tocut) = [];
    nc2v(tocut) = [];
    disp1v(tocut) = [];
    %disp2v(tocut) = [];

    clims_mapver = [min(disp1v), max(disp1v)]; % colour limits

    % spline without outliers
    spver = tpaps([nc1v;nc2v], disp1v, par.SplineParMaps);
    spvals = fnval(spver, [nc1v;nc2v]);
    spver_err = disp1v - spvals;
    stdver = std(spver_err);


    %%% Spline for global horizontal distortion
    nc1h = nc1;
    nc2h = nc2;
    %disp1h = disp1;
    disp2h = disp2;

    sphor = tpaps([nc1h;nc2h], disp2h, par.SplineParMaps);
    spvals = fnval(sphor, [nc1h;nc2h]);
    sphor_err = disp2h - spvals;
    stdhor = std(sphor_err);
    tol_sphor = par.TolMapSpline*stdhor;

    tocut = abs(sphor_err) > tol_sphor;
    nc1h(tocut) = [];
    nc2h(tocut) = [];
    %disp1h(tocut) = [];
    disp2h(tocut) = [];

    clims_maphor = [min(disp2h), max(disp2h)];

    sphor = tpaps([nc1h;nc2h], disp2h, par.SplineParMaps);
    spvals = fnval(sphor, [nc1h;nc2h]);
    sphor_err = disp2h - spvals;
    stdhor = std(sphor_err);

    % Linear interpolation
    [y, x] = meshgrid(1:1:sy, 1:1:sx);

    gdhori = griddata(nc1h, nc2h, disp2h, x, y, 'linear');
    gdhori = single(gdhori);
    gdveri = griddata(nc1v, nc2v, disp1v, x, y, 'linear');
    gdveri = single(gdveri);
    gdtoti = sqrt(gdveri.^2 + gdhori.^2);
    gdtoti = single(gdtoti);

    clear('x', 'y')

    clims_maptot = [0, max(gdtoti(:))];

    %%% Figures

    disp(' ')
    disp('Coordinates of spacing vector closer to horizontal: [vertical horizontal] in pixels')
    disp(mat2str(hor))
    disp('Coordinates of spacing vector closer to vertical: [vertical horizontal] in pixels')
    disp(mat2str(ver))
    disp(sprintf('Standard deviation of spline for horizontal distortion: %s pixels', stdhor))
    disp(sprintf('Standard deviation of spline for vertical distortion: %s pixels', stdver))
    disp(' ')

    if par.NoDisplay == false

        f_vver = figure('Numbertitle', 'off', 'Name', 'Values of vertical distortion');
        plot3(nc2v, nc1v, disp1v, '.')
        xlabel('horizontal axis')
        ylabel('vertical axis')
        zlabel('pixels')
        set(gca, 'XGrid', 'on')
        set(gca, 'YGrid', 'on')
        set(gca, 'ZGrid', 'on')
        xlim([0 sy + 1])
        ylim([0 sx + 1])
        set(gca, 'YDir', 'rev')
        set(gca, 'XAxisLocation', 'top')

        f_errver = figure('Numbertitle', 'off', 'Name', 'Errors of spline for vertical distortion');
        plot3(nc2v, nc1v, spver_err, '.')
        xlabel('horizontal axis')
        ylabel('vertical axis')
        zlabel('pixels')
        set(gca, 'XGrid', 'on')
        set(gca, 'YGrid', 'on')
        set(gca, 'ZGrid', 'on')
        xlim([0 sy + 1])
        ylim([0 sx + 1])
        set(gca, 'YDir', 'rev')
        set(gca, 'XAxisLocation', 'top')

        f_gdveri = figure('Numbertitle', 'off', 'Name', 'Interpolated vertical distortion in pixels');
        imagesc(gdveri, clims_mapver);
        set(gca, 'XAxisLocation', 'top')
        axis equal
        xlim([0 sy + 1])
        ylim([0 sx + 1])
        xlabel('horizontal axis')
        ylabel('vertical axis')
        colormap(jet);
        colorbar
        hold on
        plot(nc2, nc1, 'k.', 'MarkerSize', 5)

        f_spver = figure('Numbertitle', 'off', 'Name', 'Spline of vertical distortion');
        [y, x] = meshgrid(0:sy/50:sy, 0:sx/50:sx);
        surf(y, x, reshape(fnval(spver, [x(:)';y(:)']), size(x)));
        xlabel('horizontal axis')
        ylabel('vertical axis')
        zlabel('pixels')
        set(gca, 'XGrid', 'on')
        set(gca, 'YGrid', 'on')
        set(gca, 'ZGrid', 'on')
        xlim([0 sy + 1])
        ylim([0 sx + 1])
        set(gca, 'YDir', 'rev')
        set(gca, 'XAxisLocation', 'top')
        colorbar

        f_vhor = figure('Numbertitle', 'off', 'Name', 'Values of horizontal distortion');
        plot3(nc2h, nc1h, disp2h, '.')
        xlabel('horizontal axis')
        ylabel('vertical axis')
        zlabel('pixels')
        set(gca, 'XGrid', 'on')
        set(gca, 'YGrid', 'on')
        set(gca, 'ZGrid', 'on')
        xlim([0 sy + 1])
        ylim([0 sx + 1])
        set(gca, 'YDir', 'rev')
        set(gca, 'XAxisLocation', 'top')

        f_errhor = figure('Numbertitle', 'off', 'Name', 'Errors of spline for horizontal distortion');
        plot3(nc2h, nc1h, sphor_err, '.')
        xlabel('horizontal axis')
        ylabel('vertical axis')
        zlabel('pixels')
        set(gca, 'XGrid', 'on')
        set(gca, 'YGrid', 'on')
        set(gca, 'ZGrid', 'on')
        xlim([0 sy + 1])
        ylim([0 sx + 1])
        set(gca, 'YDir', 'rev')
        set(gca, 'XAxisLocation', 'top')

        f_gdhori = figure('Numbertitle', 'off', 'Name', 'Interpolated horizontal distortion in pixels');
        imagesc(gdhori, clims_maphor);
        set(gca, 'XAxisLocation', 'top')
        axis equal
        xlim([0 sy + 1])
        ylim([0 sx + 1])
        xlabel('horizontal axis')
        ylabel('vertical axis')
        colormap(jet);
        colorbar
        hold on
        plot(nc2, nc1, 'k.', 'MarkerSize', 5)

        f_sphor = figure('Numbertitle', 'off', 'Name', 'Spline of horizontal distortion');
        [y, x] = meshgrid(0:sy/50:sy, 0:sx/50:sx);
        surf(y, x, reshape(fnval(sphor, [x(:)';y(:)']), size(x)));
        xlabel('horizontal axis')
        ylabel('vertical axis')
        zlabel('pixels')
        set(gca, 'XGrid', 'on')
        set(gca, 'YGrid', 'on')
        set(gca, 'ZGrid', 'on')
        xlim([0 sy + 1])
        ylim([0 sx + 1])
        set(gca, 'YDir', 'rev')
        set(gca, 'XAxisLocation', 'top')
        colorbar

        f_grid = figure('Numbertitle', 'off', 'Name', 'Nodes and fitted grid points');
        hold on
        plot(co, ro, 'k.')
        plot(grid2, grid1, 'go')
        xlabel('horizontal axis')
        ylabel('vertical axis')
        axis equal
        xlim([0 sy + 1])
        ylim([0 sx + 1])
        set(gca, 'YDir', 'rev')
        set(gca, 'XAxisLocation', 'top')

        f_quiver = figure;
        quiver(nc2, nc1, disp2, disp1)
        set(gcf, 'Name', 'Measured total distortion')
        axis equal
        xlim([0 sy + 1])
        ylim([0 sx + 1])
        xlabel('horizontal axis')
        ylabel('vertical axis')
        set(gca, 'YDir', 'rev')
        set(gca, 'XAxisLocation', 'top')

        f_gdtoti = figure('Numbertitle', 'off', 'Name', 'Interpolated total distortion in pixels');
        imagesc(gdtoti, clims_maptot);
        set(gca, 'XAxisLocation', 'top')
        axis equal
        xlim([0 sy + 1])
        ylim([0 sx + 1])
        xlabel('horizontal axis')
        ylabel('vertical axis')
        colormap(jet);
        colorbar
        hold on
        plot(nc2, nc1, 'k.', 'MarkerSize', 5)

        drawnow
    end

    %%% Save variables in mat file if wanted

    MAPhor = [];
    MAPver = [];
    MAPtot = [];

    if ~isempty(par.SaveMat)
        save(par.SaveMat)
        %   save(par.SaveMat, 'inp', 'par', 'ro', 'co', 'sx', 'sy', 'hor', 'ver', 'Nc', ...
        %     'dnc121e', 'dnc222e', 'spd121', 'spd222', 'Nkeep', 'Ndub', 'Nuse', ...
        %     'nc1', 'nc2', 'nl1', 'nl2', 'nu', 'grid1', 'grid2', 'disp1', 'disp2', ...
        %     'gdhori', 'gdveri', 'gdtoti', 'sphor', 'spver', 'stdhor', 'stdver', 'sphor_err', 'spver_err', ...
        %     'nc1h', 'nc2h', 'disp1h', 'disp2h', 'nc1v', 'nc2v', 'disp1v', 'disp2v', ...
        %     'MAPhor', 'MAPver', 'MAPtot')
        disp(sprintf('Variables saved in: %s',  par.SaveMat))
    end

    toc

    if par.NoDisplay == false
        tilefig([f_vhor, f_errhor, f_gdhori, f_sphor])
        tilefig([f_vver, f_errver, f_gdveri, f_spver])
        tilefig([f_grid, f_quiver, f_gdtoti, f_nodes])
    end

    if isempty(par.SaveMaps)
        if par.NoDisplay == false || par.ManualNodes == true
            figure(f_nodes)
            title('Finished.')
        end
        disp('Finished.')
        %keyboard
        return
    end


    %%% Generate distortion maps from splines
    % A map directly shows the vector for each pixel with which it should be
    % translated to get the undistorted image.

    if par.NoDisplay == false || par.ManualNodes == true
        figure(f_nodes)
        title('Generating distortion maps from splines...')
    end

    disp(' ')
    disp('Generating distortion maps from splines')
    disp('This may take a while...')
    disp(' ')

    [y, x] = meshgrid(1:1:sy, 1:1:sx);

    tic
    MAPhor = fnval(sphor, [x(:)'; y(:)']);
    MAPhor = reshape(MAPhor, sx, sy);
    MAPhor = MAPhor - MAPhor(floor(sx/2), floor(sy/2)); % set central pixel to 0
    if ~isempty(par.SaveMat)
        save(par.SaveMat)
        %   save(par.SaveMat, 'inp', 'par', 'ro', 'co', 'sx', 'sy', 'hor', 'ver', 'Nc', ...
        %     'dnc121e', 'dnc222e', 'spd121', 'spd222', 'Nkeep', 'Ndub', 'Nuse', ...
        %     'nc1', 'nc2', 'nl1', 'nl2', 'nu', 'grid1', 'grid2', 'disp1', 'disp2', ...
        %     'gdhori', 'gdveri', 'gdtoti', 'sphor', 'spver', 'stdhor', 'stdver', 'sphor_err', 'spver_err', ...
        %     'nc1h', 'nc2h', 'disp1h', 'disp2h', 'nc1v', 'nc2v', 'disp1v', 'disp2v', ...
        %     'MAPhor', 'MAPver', 'MAPtot')
        disp(sprintf('Horizontal distortion map was created and added to: %s',  par.SaveMat))
    else
        disp('Horizontal distortion map was created.')
        disp('  displacement of central pixel is set to 0')
    end
    toc

    tic
    MAPver = fnval(spver, [x(:)'; y(:)']);
    MAPver = reshape(MAPver, sx, sy);
    MAPver = MAPver - MAPver(floor(sx/2), floor(sy/2)); % set central pixel to 0

    MAPtot = sqrt(MAPver.^2 + MAPhor.^2);
    clear('x', 'y')

    if ~isempty(par.SaveMat)
        save(par.SaveMat)
        %   save(par.SaveMat, 'inp', 'par', 'ro', 'co', 'sx', 'sy', 'hor', 'ver', 'Nc', ...
        %     'dnc121e', 'dnc222e', 'spd121', 'spd222', 'Nkeep', 'Ndub', 'Nuse', ...
        %     'nc1', 'nc2', 'nl1', 'nl2', 'nu', 'grid1', 'grid2', 'disp1', 'disp2', ...
        %     'gdhori', 'gdveri', 'gdtoti', 'sphor', 'spver', 'stdhor', 'stdver', 'sphor_err', 'spver_err', ...
        %     'nc1h', 'nc2h', 'disp1h', 'disp2h', 'nc1v', 'nc2v', 'disp1v', 'disp2v', ...
        %     'MAPhor', 'MAPver', 'MAPtot')
        disp(sprintf('Vertical distortion map was created and added to: %s',  par.SaveMat))
    else
        disp('Vertical distortion map was created.')
        disp('  displacement of central pixel is set to 0')
    end
    toc

    % Create input file of distortion maps in FIT2D compatible format
    distortion_write(par.SaveMaps, MAPver, MAPhor);
    disp(sprintf('Distortion maps were created and saved into: %s',  par.SaveMaps))

    if par.NoDisplay == false

        f_maphor = figure('Numbertitle', 'off', 'Name', 'Horizontal distortion map in pixels');
        imagesc(MAPhor, clims_maphor);
        set(gca, 'XAxisLocation', 'top')
        axis equal
        xlim([0 sy + 1])
        ylim([0 sx + 1])
        xlabel('horizontal axis')
        ylabel('vertical axis')
        colormap(jet);
        colorbar
        hold on
        plot(nc2, nc1, 'k.', 'MarkerSize', 5)

        f_mapver = figure('Numbertitle', 'off', 'Name', 'Vertical distortion map in pixels');
        imagesc(MAPver, clims_mapver);
        set(gca, 'XAxisLocation', 'top')
        axis equal
        xlim([0 sy + 1])
        ylim([0 sx + 1])
        xlabel('horizontal axis')
        ylabel('vertical axis')
        colormap(jet);
        colorbar
        hold on
        plot(nc2, nc1, 'k.', 'MarkerSize', 5)

        f_maptot = figure('Numbertitle', 'off', 'Name', 'Total distortion map in pixels');
        imagesc(MAPtot, clims_maptot);
        set(gca, 'XAxisLocation', 'top')
        axis equal
        xlim([0 sy + 1])
        ylim([0 sx + 1])
        xlabel('horizontal axis')
        ylabel('vertical axis')
        colormap(jet);
        colorbar
        hold on
        plot(nc2, nc1, 'k.', 'MarkerSize', 5)

        figure(f_nodes)
        title('Finished.')
        tilefig([f_maphor, f_mapver, f_maptot, f_nodes])
    end
end % of function

%%% Sub - functions

%%% For numbering nodes
function [newbases, r, c, missingbases] = sfSearch(base, move, r, c, sx, sy, tol_angle, tol_length, dojump)
    foundnewbase = true;
    jumpagap = false;
    newbases = base;
    searchbase = base + move;
    while (foundnewbase == true || jumpagap == true)
        foundnewbase = false;
        newbasei = [];

        rt = r;
        ct = c;
        ind = (1:length(rt))';

        if ((searchbase(1)  <= sx) && (searchbase(2) <= sy))  % searchbase still in the image

            if (jumpagap == true)
                mindist = (move(1)^2 + move(2)^2)/2;
                dist = (rt - searchbase(1)).^2 + (ct - searchbase(2)).^2;
                tocut = dist > mindist;
                dist(tocut) = [];
                rt(tocut) = [];
                ct(tocut) = [];
                ind(tocut) = [];
                if ~isempty(rt)
                    len = sqrt((rt - newbases(end - 1, 1)).*(rt - newbases(end - 1, 1)) + (ct - newbases(end - 1, 2)).*(ct - newbases(end - 1, 2)));
                    angle = acosd([rt - newbases(end - 1, 1) ct - newbases(end - 1, 2)]*move'./len/norm(move));
                    dist(len < norm(move)/tol_length | len > norm(move)*tol_length) = Inf;
                    dist(angle > tol_angle) = Inf;
                    [mindist, newbasei] = min(dist);
                    newbasei = ind(newbasei);
                    if (~isempty(newbasei) && mindist ~= Inf)
                        foundnewbase = true;
                    end
                end
            else
                mindist = move(1)^2 + move(2)^2;
                dist = (rt - searchbase(1)).^2 + (ct - searchbase(2)).^2;
                tocut = dist > mindist;
                dist(tocut) = [];
                rt(tocut) = [];
                ct(tocut) = [];
                ind(tocut) = [];
                if ~isempty(rt)
                    len = sqrt([(rt - newbases(end, 1)).*(rt - newbases(end, 1)) + (ct - newbases(end, 2)).*(ct - newbases(end, 2))]);
                    angle = acosd([rt - newbases(end, 1) ct - newbases(end, 2)]*move'./len/norm(move));
                    dist(len < norm(move)/tol_length | len > norm(move)*tol_length) = Inf;
                    dist(angle > tol_angle) = Inf;
                    [mindist, newbasei] = min(dist);
                    newbasei = ind(newbasei);
                    if (~isempty(newbasei) && mindist ~= Inf)
                        foundnewbase = true;
                    end
                end
            end

            if (foundnewbase == true)
                newbases = [newbases; r(newbasei), c(newbasei)];
                move = (newbases(end, :) - base)./(size(newbases, 1) - 1);
                searchbase = newbases(end, :) + move;
                r(newbasei) = [];
                c(newbasei) = [];
                jumpagap = false;
            else
                if (jumpagap == false && dojump == true)  % establish a gap and continue searching a new base
                    if size(newbases, 1) > 1
                        move = (newbases(end, :) - base)./(size(newbases, 1) - 1)*2;
                    else
                        move = 2*move;
                    end
                    searchbase = newbases(end, :) + move;
                    newbases = [newbases; NaN, NaN];
                    jumpagap = true;
                elseif jumpagap == true  % no good base found after the gap either
                    newbases(end, :) = [];
                    jumpagap = false;
                end
            end

        else
            if (jumpagap == true)
                newbases(end, :) = [];
            end
            break % searchbase is out of image
        end
    end % of while

    missingbases = NaN(size(newbases));
    dummy = find(isnan(newbases));
    missingbases(dummy) = (newbases(dummy + 1) + newbases(dummy - 1))/2;
    missingbases(1, :) = [];
    newbases(1, :) = [];
end % of sub - function sfSearch


%%% For creating nodes
function [r, c, sx, sy, lim] = sfDistortion_Createnodes(selBB, ordfiltsize, featurecropBB, varargin)
%
% FUNCTION distortion_createnodes(im, crop, fname_mat)
%
% Finds a regular pattern in image 'im' and writes the node coordinates in
% the given 'fname_mat' matlab data file. That can be used in
% distortion_createmap.m for the creation of distortion maps.

    fname_mat = [];

    crop = false;
%   switch nargin
%     case 0
%       disp('Specify the source image,  please!')
%       return
%     case 1
%       im = varargin{1};
%     case 2
%       im = varargin{1};
%       if varargin{2} == 1 | strcmpi(varargin{2}, 'crop')
%         crop = true;
%       end
%     case 3
%       im = varargin{1};
%       if varargin{2} == 1 | strcmpi(varargin{2}, 'crop')
%         crop = true;
%       end
%       fname_mat = varargin{3};
%     case 4
%       im = varargin{1};
%       if varargin{2} == 1 | strcmpi(varargin{2}, 'crop')
%         crop = true;
%       end
%       fname_mat = varargin{3};
%       peaklim = varargin{4};
%     case 5
%       im = varargin{1};
%       if varargin{2} == 1 | strcmpi(varargin{2}, 'crop')
%         crop = true;
%       end
%       fname_mat = varargin{3};
%       peaklim = varargin{4};
%       description = varargin{5};
%     otherwise
%       disp('Too many input arguments!')
%       help mfilename
%       return
%     end

    im = varargin{1};
    if varargin{2} == 1 || strcmpi(varargin{2}, 'crop')
        crop = true;
    end
    fname_mat = varargin{3};
    peaklim = varargin{4};
    description = varargin{5};


    %%% needs revisiting
    % eps doesn't help
    % min2par should check first if the matrix is not singular
    %   if yes,  the peak is crap anyway
    % instead of imcropping a feature,  perhaps just specify the node spacing in pixels,  then search for the feature
    %   in that case the whole process could be fully automatic
    % scaling??

    doscale = false;

    % scale im (.. %) between 0 and 1
    lim = [min(im(:)) max(im(:))];

    im = (im - lim(1))./(lim(2) - lim(1));  %changed for 4M_12mu image
    lim = [0 1];
    %pfSetColourLimits(im, 0)

    figure('Name', 'Create nodes', 'Numbertitle', 'off');
    set(gcf, 'Menubar', 'none')
    imshow(im, lim)
    set(gca, 'Units', 'Normalized', 'Position', [0.05 0.04 0.9 0.9]);
    drawnow

    %keyboard

    if crop == 1
        disp(' ')
        disp('Choose a region completely filled with regular features...')
        disp(' ')
        title('Choose a region completely filled with regular features...')
        im = imcrop;
        sx = size(im, 1);
        sy = size(im, 2);
        %imc = imcrop(im, [sy*0.375 sx*0.375 sy*0.25 sx*0.25]);
        imc = imcrop(im, [sy sx sy sx].*selBB);
    else
        sx = size(im, 1);
        sy = size(im, 2);
        pause(1)
        %imc = imcrop(im, [sy*0.375 sx*0.375 sy*0.375 sx*0.375]);
        imc = imcrop(im, [sy sx sy sx].*selBB);
    end

    %%% Comment: [im, rect] = imcrop(...)
    %%% Rect gives the crop boundaries according to:
    %%% orim =  original full image,  then cut
    %%%  orim(1:round(rect(2)) - 1, :) = [];
    %%%  orim(size(im, 1) + 1:end, :) = [];
    %%%  orim(:, 1:round(rect(1)) - 1) = [];
    %%%  orim(:, size(im, 2) + 1:end) = [];

    if isempty(featurecropBB)
        imshow(imc, lim)
        title('Select a repeated feature...')
        set(gca, 'Position', [0.05 0.04 0.9 0.9]);
        drawnow

        disp(' ')
        disp('Select a repeated feature...')
        disp(' ')
        feature = imcrop;
        %imshow(feature, []);
    else
        feature = imcrop(imc, featurecropBB);
    end

    title('Calculating...')
    drawnow

    %  Since we are using convolution,  prepare the footprint accordingly
    feature  =  rot90(feature, 2);
    %   if doscale  % oversample the image to give better resolution
    %     scale = 1;
    %     im = imresize(im, scale);
    %     feature = imresize(feature, scale);
    %   end

    tic

    %  Convolve
    C = conv2(im,  feature,  'same');
    D = C;
    D(D < peaklim*max(D(:))) = 0;  % get rid of weak peaks

    % do a neighbourhood sort
    %a = round(length(feature)*1);
    a = round(min(size(feature)*ordfiltsize));
    highest = ordfilt2(D, a*a, ones(a));
    mask = (C == highest);
    [r, c, v] = find(mask);

    C = gtPlaceSubImage(C, zeros(size(C) + 2), 2, 2);
    r = r + 1;
    c = c + 1;

    for i = 1:length(r)
        ce = [r(i), c(i)] + min2par(C([r(i) - 1 r(i) r(i) + 1], [c(i) - 1 c(i) c(i) + 1]));
        r(i) = ce(1);
        c(i) = ce(2);
    end

    %   i = 1;
    %   while i <  = length(r)
    %     res = min2par(C([r(i) - 1 r(i) r(i) + 1], [c(i) - 1 c(i) c(i) + 1]));
    %     if isnan(res(1))
    %       r(i) = [];
    %       c(i) = [];
    %     else
    %       res = [r(i), c(i)] + res;
    %       r(i) = res(1);
    %       c(i) = res(2);
    %       i = i + 1;
    %     end
    %   end

    r = r - 1;
    c = c - 1;
    %C = C(2:end - 1, 2:end - 1);

    if doscale  % now revert to the original scale
        r = r./scale;
        c = c./scale;
    end

    nans = (isnan(r) | isnan(c));
    r(nans) = [];
    c(nans) = [];

    toc

    if ~isempty(fname_mat)
        save(fname_mat, 'r', 'c', 'im', 'feature', 'sx', 'sy', 'description');
        disp(sprintf('Nodes and image are saved in: %s', fname_mat))
    end

    imshow(im, lim)
    title('Nodes found:')
    set(gca, 'Position', [0.05 0.04 0.9 0.9]);
    hold on
    plot(c, r, 'r + ')
    drawnow
end % of sfDistortion_Createnodes


%%% Sub - functions taken from correlate.m for peaks at subpixel

function mi = min2par(arg)
    [X, Y] = meshgrid( - 1:1,  - 1:1);
    r = Y(:);
    c = X(:);
    xt = quad2func(r, c);
    a = xt\arg(:);
    mi = [-a(2) -a(3)]/[2*a(5) a(4);a(4) 2*a(6)];
end

% function mi = min2par(arg)
%   [X, Y] = meshgrid( - 1:1,  - 1:1);
%   r = Y(:);
%   c = X(:);
%   xt = quad2func(r, c);
%   a = xt\arg(:);
%   ma = [2*a(5) a(4);a(4) 2*a(6)];
%   if rank(ma) < 2
%     mi = [NaN NaN];
%   else
%     mi = [ - a(2)  - a(3)]/ma;
%   end
% end


function xt = quad2func(x, y)
    xt = [ones(size(x)) x y x.*y x.^2 y.^2];
end

% -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
% function xt = quad2func(x, y)
% quadratic function of 2 parameters x and y
% size(xt) = [n 6] with n the number of observations
% for functions linear in the parameters:
% z  =  xt*a with size(a) = [6 1]
% i.e. z = a(1) + a(2)*x + a(3)*y + a(4)*x.*y + a(5)*x.^2 + a(6)*y.^2
% if one has n observations given by z (size(z) = [n 1])
% the least squares estimation of a is
% a = xt\z;
