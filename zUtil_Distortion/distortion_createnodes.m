function [f_rows, f_cols, sx, sy] = distortion_createnodes(im, varargin)
% [r, c, sx, sy] = distortion_createnodes(im, varargin)
%   INPUTS:
%       im - Image with a regular pattern
%       varargin - Pairs:
%           'crop' - ('true'|{'false'})
%           'scale' - {1}
%           'fname_mat' - {''}: Filename where to write. If empty, nothing is
%                               written.
%
% Finds a regular pattern in image 'im' and writes the node coordinates in
% the given 'fname_mat' matlab data file. That can be used in
% distortion_createmap.m for the creation of distortion maps.
%

    if (~exist('im', 'var'))
        gtError('distortion_createnodes:wrong_argument', ...
            'Specify the source image, please!')
    end

    conf = [];
    conf.crop = false;
    conf.scale = 1;
    conf.fname_mat = '';

    conf = parse_pv_pairs(conf, varargin);

    % scale im (.. %) between 0 and 1
    lim = [min(im(:)) max(im(:))];
    im = (im - lim(1)) ./ (lim(2) - lim(1));

    if (conf.crop)
        fprintf('\nChoose a region completely filled with regular features\n\n')
        figure();
        [im, rect] = imcrop(im, [0 1]); % display between 0 and 1
        sx = rect(4);
        sy = rect(3);
        imc = imcrop(im, [sy*0.375 sx*0.375 sy*0.25 sx*0.25]);
        imagesc(imc)
    else
        sx = size(im, 1);
        sy = size(im, 2);
        imagesc(im)
        pause(1)
        imc = imcrop(im, [sy*0.375 sx*0.375 sy*0.375 sx*0.375]);
        imagesc(imc)
    end

    %%% Comment:
    %%% Rect gives the crop boundaries according to:
    %%% orim= original full image, then cut
    %%% orim(1:round(rect(2))-1,:)=[];
    %%% orim(size(im,1)+1:end,:)=[];
    %%% orim(:,1:round(rect(1))-1)=[];
    %%% orim(:,size(im,2)+1:end)=[];

    disp(' ')
    disp('Select a repeated feature...')
    disp(' ')
    feature = imcrop(imc);
    title('Calculating...')
    xlabel('y')
    ylabel('x')
    set(gca, 'XAxisLocation', 'top')
    drawnow

    %  Since we are using convolution, prepare the footprint
    %  accordingly
    fprintf('Feature size: (%d, %d)\n', size(feature));
    feature = rot90(feature, 2);

    if (conf.scale ~= 1)
        fprintf('Rescaling image with scale: %f..', conf.scale);
        % oversample the image to give better resolution
        im = imresize(im, conf.scale);
        feature = imresize(feature, conf.scale);
        fprintf(' Done.\n');
    end

    % Start measuring time
    tic();

    fprintf('Convolution to get features..');
    % Convolve to get feature positions
    C = conv2(im, feature, 'same');
    fprintf(' Done.\n');

    % Get rid of weak peaks
    D = C;
    half_maximum = max(D(:)) * 0.5;
    D(D < half_maximum) = 0;

    % Pretends the feature to be square
    a = length(feature);

    % do a neighbourhood sort
    fprintf('Doing neighbours sort..');
    highest = ordfilt2(D, a*a, true(a) );
    fprintf(' Done.\n');
    size(highest)

    mask = (C == highest);  % only nearest pixel :(

    [f_rows, f_cols, ~] = find(mask);

    numPoints = length(f_rows);
    gauge = GtGauge(numPoints, 'Doing spline fitting: ');
    % Spline fitting of features' positions
    for ii = 1:numPoints
        gauge.incrementAndDisplay();

        pr = f_rows(ii); % Pixel's row
        pc = f_cols(ii); % Pixel's column
        ce = [pr, pc] + min2par( C(pr-1 : pr+1, pc-1 : pc+1) );
        f_rows(ii) = ce(1);
        f_cols(ii) = ce(2);
    end
    gauge.delete();

    % now revert to the original scale
    if (conf.scale ~= 1)
        fprintf('Rescaling displacements to image''s original scale..');
        f_rows = f_rows ./ conf.scale;
        f_cols = f_cols ./ conf.scale;
        fprintf(' Done.\n');
    end
    toc()

    if ~isempty(conf.fname_mat)
        r = f_rows;
        c = f_cols;
        save(conf.fname_mat, 'r', 'c', 'im', 'feature', 'sx', 'sy');
        disp(['Nodes and image are saved in: "' conf.fname_mat '"']);
    end

    imshow(im, [])
    title('Nodes found:')
    xlabel('y')
    ylabel('x')
    set(gca, 'XAxisLocation', 'top')
    hold(gca, 'on')
    plot(f_cols, f_rows, 'r+')
    drawnow
end

% Subfunctions (taken from correlate.m)

function mi = min2par(arg)
    [X, Y] = meshgrid(-1:1, -1:1);
    r = Y(:);
    c = X(:);

    % quadratic function of 2 parameters x and y
    % size(xt)=[n 6] with n the number of observations
    % for functions linear in the parameters:
    % z = xt*a with size(a)=[6 1]
    % i.e. z=a(1)+a(2)*x+a(3)*y+a(4)*x.*y+a(5)*x.^2+a(6)*y.^2
    % if one has n observations given by z (size(z)=[n 1])
    % the least squares estimation of a is
    % a=xt\z;
    %
    % Nothing else than a Vandermonde matrix in 2D
    xt = [ones(size(r)) r c r.*c r.^2 c.^2];

    a = xt \ arg(:);
    mi = [-a(2) -a(3)] / [2*a(5) a(4); a(4) 2*a(6)];
end
