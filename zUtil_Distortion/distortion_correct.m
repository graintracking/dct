function im_corr = distortion_correct(im,distortion)
% DISTORTION_CORRECT.M
%
%
%
  switch sum(class(im))
    case 504
        data_type='uint8';
    case 551
        data_type='uint16';
    case 387
        data_type='int8';
    case 434
        data_type='int16';
    case 642
        data_type='single';
    case 635
        data_type='double';
  end

  im_corr = distortion_correct_C(im,distortion.x,distortion.y);

  switch data_type
    case 'uint8'
        im_corr=uint8(im_corr);
    case 'uint16'
        im_corr=uint16(im_corr);
    case 'int8'
        im_corr=int8(im_corr);
    case 'int16'
        im_corr=int16(im_corr);
    case 'single'
        im_corr=single(im_corr);
    case 'double'
        im_corr=double(im_corr);
  end
end

