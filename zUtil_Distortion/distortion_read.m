function map = distortion_read(fname, detector_size, ROI)
% DISTORTION_READ.M Read an ID17 CCD distortion map
% Greg Johnson
% April 2006
% modified by Peter R., 27 Oct. 2008
% modified by Nicola Vigano, 30 Nov. 2011
%
% Usage:
% map=distortion_read(fname)
%   Uses the supplied fname to read the image. Default image size would be
%   2048x2048
% map=distortion_read(fname, detector_size)
%   Uses the supplied fname and the specified image size to read the image.
%   detector_size is a structure with two fields called x and y
% map=distortion_read(fname, detector_size, ROI)
%   Uses the supplied fname and the specified image size to read the image,
%   but also crops it to the ROI. ROI is of the form
%   [xmin ymin x_size y_size]
%
% OUTPUT:
%   map.x = vertical (!!) displacement field
%   map.y = horizontal (!!) displacement field
%
% See also DISTORTION_CORRECT.M

    if ~exist('detector_size', 'var')
        detector_size = [ ];
        detector_size.x = 2048;
        detector_size.y = 2048;
    end

    fid = fopen(fname,'rb');
    if (fid == -1)
        gtError('distortion_read:wrong_argument', ...
            'Distortion map "%s" doesn''t exist', fname)
    end

    tmp_img = fread(fid,'float32');
    fclose(fid);

    tmp_img = reshape( tmp_img, [detector_size.y, detector_size.x*2] );

    map.x = tmp_img( :, 1:detector_size.x ); % vertical (!!)
    map.y = tmp_img( :, (detector_size.x+1):end ); % horizontal (!!)

    if exist('ROI', 'var')
        ROI = [ROI(1)+1 (ROI(1)+ROI(3)), ROI(2)+1 (ROI(2)+ROI(4))];

        map.x = map.x( ROI(3):ROI(4), ROI(1):ROI(2));
        map.y = map.y( ROI(3):ROI(4), ROI(1):ROI(2));
    end
end
