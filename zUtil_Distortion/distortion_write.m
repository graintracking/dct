function distortion_write(fname,mapx,mapy)
%
% FUNCTION distorion_write(fname,mapx,mapy)
%
% Writes distortion map in a file format as needed for FIT2D (2048*4096,
% float32).
%
% INPUT  fname - name of the file
%        mapx  - map of x displacements (vertical !!)
%        mapy  - map of y displacements (horizontal !!)
%
% 'mapx' and 'mapy' are the displacements needed to be applied directly to the
% same pixel in the original image (thus the displacement field defined on
% the original - and NOT on the undistorted - domain).
%
% Andy 3/2/2014 - allow non 2048x2048 maps to be written, as we can use
% these

if size(mapx)~=[2048 2048]
    disp('Warning! MAPhor size should be 2048*2048.')
    disp('continuing...')
end

if size(mapy)~=[2048 2048]
    disp('Warning! MAPver size should be 2048*2048.')
    disp('continuing...')
end

fid = fopen(fname,'wb');
fwrite(fid,mapx,'float32'); % vertical !!
fwrite(fid,mapy,'float32'); % horizontal !!
fclose(fid);

end
