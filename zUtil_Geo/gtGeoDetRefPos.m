function d_yz = gtGeoDetRefPos(acq_bb, detgeo)
% Returns an estimate of the lateral (Y) offset of the rotation axis
% with respect to the detector center - this estimate is based on center of acq.bb
% The center of the detector is defined as Z = 0 position of the LAB / SAMPLE system
%   24.03.2020 modified in order to take detdiru and detdirv into account
    det_u = ((acq_bb(1) + acq_bb(3)/2) + 0.5);
    det_v = detgeo.detrefv;
    d_xyz = -1 * gtGeoDet2Lab([det_u, det_v], detgeo, 0);
    d_yz = d_xyz(2:3);
end