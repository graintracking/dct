function par_recgeo = gtGeoRecDefaultParameters(detgeo, samgeo, acq)
% GTGEOSAMDEFAULTPARAMETERS  Stores and returns the default Sample geomtery
%                            parameters
%
%     par_recgeo = gtGeoRecDefaultParameters([detgeo [, samgeo, acq]])
%     ---------------------------------------
%     The default Reconstruction reference is set as the Lab but rotating with the
%     sample. Voxel size is determined from detector pixelsize.

    if (exist('samgeo', 'var') && ~isempty(samgeo))
        par_recgeo.orig = samgeo.orig;
        par_recgeo.dirx = samgeo.dirx;
        par_recgeo.diry = samgeo.diry;
        par_recgeo.dirz = samgeo.dirz;
    else
        par_recgeo.orig = [0 0 0];
        par_recgeo.dirx = [1 0 0];
        par_recgeo.diry = [0 1 0];
        par_recgeo.dirz = [0 0 1];
    end

    if (exist('detgeo', 'var') && ~isempty(detgeo))
        det_pixel_size = mean([detgeo.pixelsizeu, detgeo.pixelsizev]);
    else
        det_pixel_size = acq.pixelsize;
    end
    if (exist('acq', 'var') && ~isempty(acq))
        par_recgeo.orig(3) = sign(detgeo.detdirv(3)) * (acq.bb(2) + acq.bb(4) / 2 - detgeo.detrefv - 0.5) * detgeo.pixelsizev;
    end
    par_recgeo.voxsize = det_pixel_size([1 1 1]);
end
