%
% FUNCTION [diffvec,labA,theta] = gtGeoDiffVecInLabPair(detUVA,detUVB,labgeo)
%
% Given a set of Friedel pairs of spotsA and B with their Detector 
% coordinates U,V, the function returns the corresponding diffraction
% vectors in the Lab reference. The diffraction vectors indicate the 
% direction of the diffracted beam paths.
% It also returns the Bragg angles and the Lab coordinates of points A. 
%
% The beam and the rotation axis are assumed to be perpendicular.
% The diffraction vector is calculated after mirroring B on the rotation
% axis. This is where it would occur on a pseudo detector, when the sample
% is kept fixed and the beam and detector are rotated 180 degrees. The
% diffraction path is then from Bmirrored to A. The output diffraction 
% vector also points from B towards A.
%
% INPUT  detUVA - coloumn vectors of Detector coordinates of points A (n,2)
%        detUVB - coloumn vectors of Detector coordinates of points B (n,2)
%
%        Geometry parameters in LAB coordinates:
%
%        labgeo.rotpos     - rotation axis position (row vector)
%        labgeo.rotdir     - rotation axis direction (unit row vector)
%        labgeo.detorig    - position of detector origin (row vector)
%        labgeo.detdiru    - detector U direction (unit row vector)
%        labgeo.detdirv    - detector V direction (unit row vector)
%	     labgeo.pixelsizeu - pixel size U (same unit as position values)
%	     labgeo.pixelsizev - pixel size V (same unit as position values)
%        shiftA            - shift of sample in LAB reference at rotation
%                            positions corresponding to detUVA (nx3)
%        shiftB            - shift of sample in LAB reference at roatation
%                            postions corresponding to detUVB (nx3)
% OUTPUT diffvec - normalised diffraction vectors in LAB coordinates; 
%                  directions point towards A; size (n,3)
%        labA    - Lab coordinates of points A (n,3)
%        theta   - Bragg angles theta in degrees (n,1)
%                  output values account for possible shifts of the sample

function [diffvec,labA,theta] = gtGeoDiffVecInLabPair(detUVA,detUVB,labgeo, shiftA, shiftB)

% Coordinate transformation from Detector to Lab
labA = gtGeoDet2Lab(detUVA, labgeo, 0) - shiftA;
labB = gtGeoDet2Lab(detUVB, labgeo, 0) - shiftB;

% Mirror B-s on the rotation axis while staying in the same LAB reference:
labBmirr = gtMathsMirrorPointsOnAxis(labB, labgeo.rotpos, labgeo.rotdir);

% Diffraction vectors for spot A are on the path from B mirrored to spot A: 
diffvec = labA - labBmirr;

% Normalise diffvec-s (seems the fastest way for nx3 vectors):
dnorm   = sqrt(diffvec(:,1).^2 + diffvec(:,2).^2 + diffvec(:,3).^2) ;

diffvec(:,1) = diffvec(:,1)./dnorm ; 
diffvec(:,2) = diffvec(:,2)./dnorm ;
diffvec(:,3) = diffvec(:,3)./dnorm ;


if nargout == 3

    % Use the dot products of beamdir and diffvec to get theta
    theta  = 0.5*acosd(diffvec*labgeo.beamdir');

end
