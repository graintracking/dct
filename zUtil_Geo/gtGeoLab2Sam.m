function [samXYZ, lab2sam, rottensors] = gtGeoLab2Sam(labXYZ, omega, labgeo, samgeo, freevec, varargin)
% GTGEOLAB2SAM  LAB -> SAMPLE reference coordinate transformation
%
% [samXYZ, lab2sam] = gtGeoLab2Sam(labXYZ, omega, labgeo, samgeo, varargin)
%
% conf = struct('only_rot', false, 'element_wise', true, ...
%     'diffractometer', [], 'basetilt', [], 'omega', [], 'sample_tilts', []);
%
% ----------------------------------------------------------------------
%
% Given the LAB coordinates of a set of points, transforms them 
% into SAMPLE coordinates according to an omega rotation. Omega is usually
% positive.
% It optionally creates the transformation matrix.
%
% INPUT  
%   labXYZ         - coordinates in the Lab reference in pixels (n,3)
%   omega          - omega angles in degrees (n,1)
%   labgeo.rotpos  - rotation axis position in Lab coordinates (1,3)
%   labgeo.rotdir  - rotation axis direction (right-handed rotation)
%   samgeo.orig    - origin of the sample reference in Lab coordinates (1,3)
%   samgeo.voxsize - [x,y,z] voxel sizes in lab units
%   freevec        - 0 for fixed vectors (e.g. location);
%                    1 for free vectors (e.g. orientation/direction)
% (Optional)
%   only_rot       - if true, only applies rotation so the norm of the 
%                    input vectors will be unchanged; 
%                    i.e. no scaling applied according to the 
%                    voxel sizes (only works if 'samgeo.lab2sam' is undefined)
%
% OUTPUT 
%   samXYZ         - coordinates in the Sample reference in no. of voxels (n,3)
%   lab2sam        - coordinate transformation matrix
%
% OPTIONAL, INPUT:
%   omega          - can be rotation tensors ('col') from omega (3,3,n) or
%                    simply (3,3,1) which would then be used for all the
%                    lab vectors.
%

conf = struct('only_rot', false, 'element_wise', true, ...
    'diffractometer', [], 'basetilt', [], 'omega', [], 'sample_tilts', []);
% The implicit tensor construction might be broken, in the case of a
% reference diffractometer configuration, where shifts and tilts are not 0
conf = parse_pv_pairs(conf, varargin);

[size_omega(1), size_omega(2), size_omega(3)] = size(omega);

use_instr_tensor_4x4 = all(size_omega(1:2) == 4);
use_instr_tensor_3x3 = all(size_omega(1:2) == 3);

use_diffractometer = ~isempty(conf.diffractometer) || use_instr_tensor_4x4;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Change of reference from Lab to Sample
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Transformation matrix from Lab to Sample coordinates:
%   Note that Lab is an orthonormal basis by definition, but Sample may not be !!
%   Therefore needs calculation of the inverse, and not only transpose.
if (~isfield(samgeo,'lab2sam') || (isfield(samgeo,'lab2sam') && isempty(samgeo.lab2sam)))

    if (conf.only_rot)
        % Keep norm of input vectors -> keep unit lengths in the rows of 
        % the transformation matrix 
        sam2lab = [samgeo.dirx; samgeo.diry; samgeo.dirz];
    else
        sam2lab = [ ...
            samgeo.dirx * samgeo.voxsize(1); ...
            samgeo.diry * samgeo.voxsize(2); ...
            samgeo.dirz * samgeo.voxsize(3) ];
    end

    lab2sam = inv(sam2lab);

elseif (conf.only_rot)
    error('Transformation matrix already defined. Set ''only_rot'' to false.')
else
    lab2sam = samgeo.lab2sam;
end

%%%%%%%%%%%%%%%%%%%%%
%%% Rotation in Lab
%%%%%%%%%%%%%%%%%%%%%

ones_in = ones(size(labXYZ, 1), 1);

if (~freevec)
    % Vectors relative to rotation axis position
    labXYZ = labXYZ - labgeo.rotpos(ones_in, :);
end

if (use_diffractometer)
    if (use_instr_tensor_4x4)
        rottensors = omega;
    else
        rottensors = gtGeoDiffractometerTensor(conf.diffractometer, 'sam2lab', ...
            'angles_basetilt', conf.basetilt, ...
            'angles_rotation', conf.omega, ...
            'angles_sam_tilt', conf.sample_tilts );
    end
    if (conf.element_wise)
        labXYZ = permute(labXYZ, [2 3 1]);
        labXYZ(4, 1, :) = ~freevec;
    else
        labXYZ = labXYZ';
        labXYZ(4, :) = ~freevec;
    end

    labXYZ_rot = gtMathsMatrixProduct(rottensors, labXYZ);

    if (conf.element_wise)
        labXYZ_rot = labXYZ_rot(1:3, 1, :);
        labXYZ_rot = permute(labXYZ_rot, [3 1 2]);
    else
        labXYZ_rot = labXYZ_rot(1:3, :, :);
        labXYZ_rot = reshape(labXYZ_rot, 3, [])';
    end
else
    % Tensor which rotates with omega from Lab back to Sample:
    if (use_instr_tensor_3x3)
        % This means that the rotation tensors were passed already!
        if (size_omega(3) == 1)
            rottensors = omega(:, :, ones_in);
        else
            rottensors = omega;
        end
    else
        rotcomp    = gtMathsRotationMatrixComp(labgeo.rotdir', 'col');
        rottensors = gtMathsRotationTensor(omega, rotcomp);
    end

    rottensors_t = reshape(rottensors, 3, []);

    if (conf.element_wise)
        % Multiply element-wise to avoid loop (applying rottensors to labXYZ)
        labXYZ_t = [labXYZ, labXYZ, labXYZ]';
        labXYZ_t = reshape(labXYZ_t, 3, []);
        pro = labXYZ_t .* rottensors_t;

        % Sum, reshape, transpose to get rotated vector
        labXYZ_rot = reshape(sum(pro, 1), 3, [])';
    else
        % output wll be groupped by (all rotations per center) x number centers
        labXYZ_rot = labXYZ * rottensors_t;
        labXYZ_rot = reshape(labXYZ_rot', 3, [])';
    end
end

if (~freevec)
    ones_out = ones(size(labXYZ_rot, 1), 1);
    % Set offset back to get rotated vector
    labXYZ_rot = labXYZ_rot + labgeo.rotpos(ones_out, :) - samgeo.orig(ones_out, :);
end

samXYZ = labXYZ_rot * lab2sam;

end % of function
