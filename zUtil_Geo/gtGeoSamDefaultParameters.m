function par_samgeo = gtGeoSamDefaultParameters()
% GTGEOSAMDEFAULTPARAMETERS  Stores and returns the default Sample geomtery 
%                            parameters
%
%     par_samgeo = gtGeoSamDefaultParameters(acq)
%     ---------------------------------------
%     The default Sample reference is set as the Lab but rotating with the 
%     sample. Voxel size is 1 lab unit.
% 

par_samgeo.orig    = [0 0 0]; 
par_samgeo.dirx    = [1 0 0]; 
par_samgeo.diry    = [0 1 0]; 
par_samgeo.dirz    = [0 0 1];
par_samgeo.voxsize = [1 1 1];

end
