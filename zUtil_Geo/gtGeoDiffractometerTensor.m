function [t, t_sam, t_rot, t_base] = gtGeoDiffractometerTensor(diff, type, varargin)
    conf = struct( ...
        'angles_basetilt', 0, ...
        'angles_rotation', 0, ...
        'angles_sam_tilt', zeros(0, 1), ...
        'reference_diffractometer', [] );
    conf = parse_pv_pairs(conf, varargin);

    num_rotations = numel(conf.angles_rotation);
    num_basetilts = numel(conf.angles_basetilt);
    if (num_basetilts > 1 && num_rotations > 1 && num_basetilts ~= num_rotations)
        error('gtGeoDiffractometerTensor:wrong_argument', ...
            'Only one direction allowed for scanning at a time')
    end
    % Building sample tilt components
    if (iscell(conf.angles_sam_tilt))
        error('gtGeoDiffractometerTensor:not_implemented', ...
            'multiple_sample tilts not implemented yet')
    end

    % Whether we do a SAM->LAB or a LAB->SAM tensor
    is_sam2lab = ismember(lower(type), {'sam2lab', 's2l', 's2l_c'});

%     diff_temp = diff;
%     diff_temp.shifts_sam_stage = diff_temp.shifts_sam_stage - conf.reference_diffractometer.shifts_sam_stage;
%     diff_temp.axes_sam_tilt_offset = diff_temp.axes_sam_tilt_offset - conf.reference_diffractometer.axes_sam_tilt_offset;

    % Here we build the goniometer tensor associated to our diffractometer
    % configuration. This includes sample shifts and tilts.
    t_sam = build_gonio_tensor(diff, conf, is_sam2lab);

    % In case we pass a reference diffractometer (which defines the LAB
    % coordinates) we take it into account.
    % This diffractometer configuration is usually related to the first DCT
    % scan in the dataset.
    if (~isempty(conf.reference_diffractometer))
        % The inversion in is_sam2lab is due to the fact that we are always
        % coming back from the instrument reference system in the opposite
        % direction of is_sam2lab
        conf_ref = conf;
        conf_ref.angles_sam_tilt = [];
        conf_ref.angles_rotation = [];

        t_sam_ref = build_gonio_tensor(conf.reference_diffractometer, conf_ref, ~is_sam2lab);

        if (is_sam2lab)
            t_sam = t_sam * t_sam_ref;
        else
            t_sam = t_sam_ref * t_sam;
        end
    end

    % Building rotation components
    if (isempty(conf.angles_rotation))
        t_rot = eye(4);
    else
        t_rot = build_tensor(diff.axes_rotation, conf.angles_rotation, ...
            diff.origin_rotation, is_sam2lab);
    end

    % Building base tilt components
    if (isempty(conf.angles_basetilt))
        t_base = eye(4);
    else
        t_base = build_tensor(diff.axes_basetilt, conf.angles_basetilt, ...
            diff.origin_basetilt, is_sam2lab);
    end

    if (is_sam2lab)
        t = gtMathsMatrixProduct(t_base, gtMathsMatrixProduct(t_rot, t_sam));
    else
        t = gtMathsMatrixProduct(t_sam, gtMathsMatrixProduct(t_rot, t_base));
    end
end

function t_sam = build_gonio_tensor(diff, conf, is_sam2lab)
    t_shift = eye(4);
    if (is_sam2lab && ~isempty(diff.shifts_sam_stage))
        t_shift(:, 4) = [diff.shifts_sam_stage'; 1];
    end

    % Initialise with the shift tensor
    t_sam = t_shift;

    if (~((isempty(conf.angles_sam_tilt) && isempty(diff.axes_sam_tilt_offset)) ...
            || isempty(diff.axes_sam_tilt)))
        if (~isempty(diff.axes_sam_tilt_offset))
            if (isempty(conf.angles_sam_tilt))
                conf.angles_sam_tilt = diff.axes_sam_tilt_offset;
            else
                conf.angles_sam_tilt = conf.angles_sam_tilt + diff.axes_sam_tilt_offset;
            end
        end

        % Order of application is reversed, depending on the sense of
        % application
        if (is_sam2lab)
            axes_order = size(diff.axes_sam_tilt, 1):-1:1;
        else
            axes_order = 1:size(diff.axes_sam_tilt, 1);
        end

        for ii = axes_order
            sam_tilt_ii = build_tensor(diff.axes_sam_tilt(ii, :), ...
                conf.angles_sam_tilt(ii), diff.origin_sam_tilt(ii, :), ...
                is_sam2lab);
            t_sam = sam_tilt_ii * t_sam;
        end
    end

    if (~is_sam2lab && ~isempty(diff.shifts_sam_stage))
        % if it is lab2sam, the translations go aver the sample tilts
        t_shift(:, 4) = [-diff.shifts_sam_stage'; 1];
        t_sam = t_shift * t_sam;
    end

    if (~isempty(diff.axes_rotation_offset))
        t_rot_off = build_tensor(diff.axes_rotation, diff.axes_rotation_offset, ...
            diff.origin_rotation, is_sam2lab);

        if (is_sam2lab)
            t_sam = t_rot_off * t_sam;
        else
            t_sam = t_sam * t_rot_off;
        end
    end
end

function t = build_tensor(t_axis, t_angles, t_orig, is_sam2lab)
    if (is_sam2lab)
        t = gtMathsRotationTranslationTensor(t_axis, t_angles, t_orig);
    else
        t = gtMathsRotationTranslationTensor(t_axis, -t_angles, t_orig);
    end
end

