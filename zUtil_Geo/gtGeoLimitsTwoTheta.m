function [minangle,maxangle]=gtGeoLimitsTwoTheta(R, detpos, detdir)
% GTGEOLIMITSTWOTHETA  Calculate TwoTheta maximum and minimum
%     [minangle,maxangle]=gtGeoLimitsTwotheta(R, detpos, detdir)
%     ----------------------------------------------------------
%
%     R = acq.maxradius*acq.pixelsize (acq.maxradius in [mm])
%     detpos    = labgeo.detrefpos in [mm]
%     detdir    = labgeo.detnorm 

% Calculate minimum and maximum twotheta angle of the experiment
% Really bad calculation! Needed a check

x=[1 0 0];
y=[0 1 0];
z=[0 0 1];

dotx=abs(dot(detdir,[1 0 0]));
doty=abs(dot(detdir,[0 1 0]));
dotz=abs(dot(detdir,[0 0 1]));

if dotx > doty && dotx > dotz % detector inline -> normal // axis X
    % detector must be perpendicular to the plane x-z
    % case inline detector and centered w.r.t. beam direction
    disp('Inline detector')
    % case inline detector with z-dir offset
    minangle=atand( ((detpos(3) - R)/detpos(1)) );
    maxangle=atand( ((detpos(3) + R)/detpos(1)) );
    if minangle < 0
        minangle=0;
    end
elseif abs(dot(detdir,z))==1 % detector vertical normal // axis Z
    disp('Vertical detector')
    % case vertical detector offset by xd w.r.t. z-axis (LAB ref.)
    % detector must be perpendicular to the plane x-z
    % suppose detpos(2)=0
    % D distance sample center - detector center
    %D=sqrt(detpos(1)+detpos(3)); % ?????????
    maxangle=atand( abs(detpos(3))/(detpos(1)-R) );
    minangle=atand( abs(detpos(3))/(detpos(1)+R) );
    if minangle < 0
        minangle = abs(minangle)+90;
    end
    if maxangle < 0
        maxangle = abs(maxangle)+90;
    end
    
elseif abs(dot(detdir,y))==1 % detector vertical but rotated normal // axis Y
    disp('Vertical detector - rotated 90 deg')
    % case vertical detector
    % suppose detpos(3)=0
    % D distance sample center - detector center
    %D=sqrt(detpos(1)*detpos(1)+detpos(2)*detpos(2));
    maxangle=atand( abs(detpos(2))/(detpos(1)-R) );
    minangle=atand( abs(detpos(2))/(detpos(1)+R) );
    if maxangle < 0
        maxangle = abs(maxangle)+90;
    end
    if minangle < 0
        minangle = abs(minangle)+90;
    end
else
    error('Check if detector direction is unusual. Normally the detector is inline or vertical.')
end


end % end of function