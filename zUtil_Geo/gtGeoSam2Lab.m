function [labXYZ, sam2lab, rottensors] = gtGeoSam2Lab(samXYZ, omega, labgeo, samgeo, freevec, varargin)
% GTGEOSAM2LAB  SAMPLE -> LAB reference coordinate transformation.
%
% [labXYZ, sam2lab] = gtGeoSam2Lab(samXYZ, om, labgeo, samgeo, freevec, varargin)
%
% conf = struct('only_rot', false, 'element_wise', true, ...
%     'diffractometer', [], 'basetilt', [], 'omega', [], 'sample_tilts', []);
%
% ----------------------------------------------------------------------- 
%
% Given the SAMPLE coordinates of a set of points, transforms them 
% into LAB coordinates according to an omega rotation. Omega is usually
% positive.
% It optionally creates the transformation matrix.
%
% INPUT  
%   samXYZ         - coordinates in the Sample reference in no. of voxels (n,3)
%   omega          - omega angles in degrees (n,1)
%   labgeo.rotpos  - rotation axis position in Lab coordinates (1,3)
%   labgeo.rotdir  - rotation axis direction (right-handed rotation)
%   samgeo.orig    - origin of the sample reference in Lab coordinates (1,3)
%   samgeo.voxsize - [x,y,z] voxel sizes in lab units
%   freevec        - 0 for fixed vectors (e.g. location);
%                    1 for free vectors (e.g. orientation)
% (optional)
%   only_rot       - if true, only applies rotation and the norm of the 
%                    input vectors will be unchanged; i.e. no scaling 
%                    applied according to the voxel sizes (only works if 
%                    'samgeo.sam2lab' is undefined)
%
% OUTPUT 
%   labXYZ         - row vector of SAMPLE coordinates (n,3)
%   sam2lab        - coordinate transformation matrix
%
% OPTIONAL, INPUT:
%   omega          - can be rotation tensors ('col') from -omega (3,3,n) or
%                    simply (3,3,1) which would then be used for all the
%                    lab vectors.
%

conf = struct('only_rot', false, 'element_wise', true, ...
    'diffractometer', [], 'basetilt', [], 'omega', [], 'sample_tilts', []);
% The implicit tensor construction might be broken, in the case of a
% reference diffractometer configuration, where shifts and tilts are not 0
conf = parse_pv_pairs(conf, varargin);

[size_omega(1), size_omega(2), size_omega(3)] = size(omega);

use_instr_tensor_4x4 = all(size_omega(1:2) == 4);
use_instr_tensor_3x3 = all(size_omega(1:2) == 3);

use_diffractometer = ~isempty(conf.diffractometer) || use_instr_tensor_4x4;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Change of reference from Sample to Lab
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Transformation matrix from Sample to Lab coordinates:
%   (Lab is an orthonormal basis by definition.)
if (~isfield(samgeo,'sam2lab') || (isfield(samgeo,'sam2lab') && isempty(samgeo.sam2lab)))

    if (conf.only_rot)
        % Keep norm of input vectors -> keep unit lengths in the rows of
        % the transformation matrix
        sam2lab = [samgeo.dirx; samgeo.diry; samgeo.dirz];
    else
        sam2lab = [ ...
            samgeo.dirx * samgeo.voxsize(1); ...
            samgeo.diry * samgeo.voxsize(2); ...
            samgeo.dirz * samgeo.voxsize(3) ];
    end

elseif (conf.only_rot)
    error('Transformation matrix already defined. Set ''only_rot'' to false.')
else
    sam2lab = samgeo.sam2lab;
end

%%%%%%%%%%%%%%%%%%%%%
%%% Rotation in Lab
%%%%%%%%%%%%%%%%%%%%%

% Vectors in Lab reference at the origin before rotation
labXYZprerot = samXYZ * sam2lab;

ones_in = ones(size(samXYZ, 1), 1);

if (~freevec)
    % Vectors relative to rot. axis position in Lab reference
    vv           = samgeo.orig - labgeo.rotpos;
    labXYZprerot = labXYZprerot + vv(ones_in,:);
end

if (use_diffractometer)
    if (use_instr_tensor_4x4)
        rottensors = omega;
    else
        rottensors = gtGeoDiffractometerTensor(conf.diffractometer, 'sam2lab', ...
            'angles_basetilt', conf.basetilt, ...
            'angles_rotation', conf.omega, ...
            'angles_sam_tilt', conf.sample_tilts );
    end

    if (conf.element_wise)
        labXYZprerot = permute(labXYZprerot, [2 3 1]);
        labXYZprerot(4, 1, :) = ~freevec;
    else
        labXYZprerot = labXYZprerot';
        labXYZprerot(4, :) = ~freevec;
    end

    labXYZ = gtMathsMatrixProduct(rottensors, labXYZprerot);

    if (conf.element_wise)
        labXYZ = labXYZ(1:3, 1, :);
        labXYZ = permute(labXYZ, [3 1 2]);
    else
        labXYZ = labXYZ(1:3, :, :);
        labXYZ = reshape(labXYZ, 3, [])';
    end
else

    % Tensor which rotates with omega from Sample to Lab
    if (use_instr_tensor_3x3)
        % This means that the rotation tensors were passed already!
        if (size_omega(3) == 1)
            rottensors = omega(:, :, ones_in);
        else
            rottensors = omega;
        end
    else
        rotcomp    = gtMathsRotationMatrixComp(labgeo.rotdir', 'col');
        rottensors = gtMathsRotationTensor(-omega, rotcomp);
    end

    rottensors_t = reshape(rottensors, 3, []);

    if (conf.element_wise)
        % Multiply element-wise to avoid loop
        labXYZprerot_t = [labXYZprerot, labXYZprerot, labXYZprerot]';
        labXYZprerot_t = reshape(labXYZprerot_t, 3, []);
        pro = labXYZprerot_t .* rottensors_t;

        % Sum, reshape, transpose
        labXYZ = reshape(sum(pro, 1), 3, [])';
    else
        % output wll be groupped by (all rotations per center) x number centers
        labXYZ = labXYZprerot * rottensors_t;
        labXYZ = reshape(labXYZ', 3, [])';
    end
end

if (~freevec)
    ones_out = ones(size(labXYZ, 1), 1);
    % Set offset back
    labXYZ = labXYZ + labgeo.rotpos(ones_out, :);
end

end % of function
