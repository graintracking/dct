function labgeo = gtGeoSamEnvFromAcq(labgeo, detgeo, acq)
% labgeo = gtGeoSamEnv(labgeo, detgeo, acq)
%
% Calculates the sample envelope using acq.bb and acq.pixelsize, and 
% extends it with 5% but with a minimum of 20 pixels extra. 
%
% Veraion 003 25-03-2020 by WLudwig
% Express samenvtop and samenvbot in Lab System (i.e. take vertical offset of
% acq.bb into account
%
% Version 002 18-12-2013 by PReischig
%
% Version 001 09-10-2013 by LNervo

% Add margin around bounding box: 5% of total bbox height but min. 20
% pixels more
% labgeo.samenvtop = max(acq.bb(4)/2 * 1.05, acq.bb(4)/2 + 20) * acq.pixelsize;
% labgeo.samenvbot = -labgeo.samenvtop;
bb_vert_cen  = acq.bb(2) + acq.bb(4) / 2;
bb_vert_size = max(acq.bb(4) / 2 * 1.05, acq.bb(4) / 2 + 20);
det_top     = [0, bb_vert_cen + bb_vert_size];
det_bot     = [0, bb_vert_cen - bb_vert_size];
samenv_vert = gtGeoDet2Lab([det_top; det_bot], detgeo, 0);
labgeo.samenvtop = max(samenv_vert(:, 3));
labgeo.samenvbot = min(samenv_vert(:, 3));
labgeo.samenvrad = max(acq.bb(3)/2 * 1.05, acq.bb(3)/2 + 20) * acq.pixelsize;

end % end of function
