function samenv = gtGeoSamEnvInSampleRef(labgeo, samgeo)
%     samenv = gtSampleVolInSampleRef(labgeo, samgeo)
%     ----------------------------------------------
%     Returns the coordinates of the irradiated sample volume in the Sample 
%     reference.
%     If the Sample ref is not a Cartesian (orthonormal) basis, results might 
%     be inaccurate.
%
%     INPUT:
%       labgeo      = <struct>     as in parameters.labgeo
%       samgeo      = <struct>     as in parameters.samgeo
%
%     OUTPUT:
%       samenv.top  = <double>     the central point on the envelope's top surface
%       samenv.bot  = <double>     the central point on the envelope's bottom surface
%       samenv.rad  = <double>     radian of sample envelope (unit is the maximum voxel
%                                  size of the three)
%       samenv.axis = <double>     rotation axis direction

    % Envelope top and bottom point in Lab
    toppoint = labgeo.rotpos + labgeo.rotdir * labgeo.samenvtop;
    botpoint = labgeo.rotpos + labgeo.rotdir * labgeo.samenvbot;

    % Envelope top and bottom point in Sample
    samenv.top  = gtGeoLab2Sam(toppoint, 0, labgeo, samgeo, false);
    samenv.bot  = gtGeoLab2Sam(botpoint, 0, labgeo, samgeo, false);

    % Radius
    samenv.rad  = labgeo.samenvrad / max(samgeo.voxsize);

    % Normalised rotation axis direction
    samenv.axis = gtGeoLab2Sam(labgeo.rotdir, 0, labgeo, samgeo, true, 'only_rot', true);
    samenv.axis = samenv.axis / sqrt(sum(samenv.axis .* samenv.axis, 2));
end
