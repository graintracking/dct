function proj_geom = gtGeoProjForReconstruction(projvec_sam, omega, vol_center,...
                     bbpos, bboff, detgeo, labgeo, samgeo, recgeo, rectype, vol_center_shift)
% GTGEOPROJFORRECONSTRUCTION Projection coordinates required for
%                            reconstructions.
%
% proj_geom = gtGeoProjForReconstruction(projvec, omega, ...
%             graincenter, bbpos, bboff, labgeo, samgeo, recgeo)
% 
% ----------------------------------------------------------------
%
% Reconstruction geometries
%   'ASTRA_grain' - for a single grain placed in the origin of the 
%                   reconctructed ROI (requires 'graincenter')
%   'ASTRA_full'  - for a reconstruction of the complete sample volume
%                   (does not use 'graincenter')
%   'ASTRA_absorption' - for a reconstruction of the absorption volume
%                   (does not use 'graincenter' and 'projvec')
% 
%
%     INPUT:
%       projvec     = projection vectors (beam direction) in the 
%                     SAMPLE reference (nx3) (Not needed for 'absorption')
%       omega       = omega rotational positions corresponding to 
%                     the projection vectors (in degrees) (nx1)
%       graincenter = grain center in the SAMPLE reference (1x3)
%                     (Only needed for 'grain')
%       bbpos       = bounding box positions on the detector (in pixels)
%                     [corner_U corner_V width height] (nx4)
%       bboff       = additional bounding box U,V offsets relative to
%                     position 'bbpos' (n,2)
%       detgeo      = DETector geometry as in parameters.detgeo
%       labgeo      = LABoratory reference as in parameters.labgeo
%       samgeo      = SAMPLE reference as in parameters.samgeo
%       recgeo      = REConstruction reference as in parameters.recgeo
%       rectype     = <char> reconstruction geometry:
%                     'ASTRA_grain', 'ASTRA_absorption' or 'ASTRA_full'
%       vol_center_shift = shift of grain center in SAMPLE reference (nx3)
%                          accounting for sample movements relative to the
%                          the rotation axis (see also gtMatchGetSampleShifts)
%
%     OUTPUT:
%       proj_geom   = input for the specified reconstruction geometry:
%                       'ASTRA_grain', 'ASTRA_absorption' and 'ASTRA_full':
%                         Parameters defined relative to the sample (nx12):
%                             [projection vector, detector position, ...
%                              detector vector U, detector vector V];
%                       
%

    if isempty(bboff)
        bboff = zeros(size(bbpos, 1), 2);
    end

    if (isempty(detgeo))
        detgeo = gtGeoConvertLegacyLabgeo2Detgeo(labgeo);
    end

    [size_omega(1), size_omega(2), size_omega(3)] = size(omega);

    use_instr_tensor_4x4 = all(size_omega(1:2) == 4);
    use_instr_tensor_3x3 = all(size_omega(1:2) == 3);

    % Bounding box center U,V coordinates
    bbcent_det  = [ ...
        bbpos(:, 1) + (bbpos(:, 3) - 1) / 2 + bboff(:, 1) , ...
        bbpos(:, 2) + (bbpos(:, 4) - 1) / 2 + bboff(:, 2) ];

    if (use_instr_tensor_3x3 || use_instr_tensor_4x4)
        % Rotation tensors were given already
        rot_tensor = omega;
        ones_omega = ones(size(omega, 3), 1);
    else
        % All functions that use the same omegas, need the same rotation tensors:
        rot_comp_w = gtMathsRotationMatrixComp(labgeo.rotdir', 'col');
        rot_tensor = gtMathsRotationTensor(omega, rot_comp_w);
        % NOTE: rot_tensor_w is a 3x3 sam->lab tensor, used in lab->sam
        % functions because they perform the left product! This has to
        % change in the future
        ones_omega = ones(length(omega), 1);
    end

    if (~exist('vol_center_shift', 'var') || isempty(vol_center_shift))
        vol_center_shift_rec = zeros(numel(ones_omega), 3);
    else
        vol_center_shift_rec = gtGeoSam2Sam(vol_center_shift, samgeo, recgeo, false, false);
    end

    % Bounding box center X,Y,Z coordinates in LAB and REC reference
    bbcent_lab  = gtGeoDet2Lab(bbcent_det, detgeo, 0);
    bbcent_rec  = gtGeoLab2Sam(bbcent_lab, rot_tensor, labgeo, recgeo, false);

    % Detector orientation vectors in REC reference
    detdiru = detgeo.detdiru ./ sqrt(sum(detgeo.detdiru .^ 2)) * detgeo.pixelsizeu;
    detdirv = detgeo.detdirv ./ sqrt(sum(detgeo.detdirv .^ 2)) * detgeo.pixelsizev;

    detdiru_rec = gtGeoLab2Sam(detdiru(ones_omega, :), rot_tensor, labgeo, recgeo, true);
    detdirv_rec = gtGeoLab2Sam(detdirv(ones_omega, :), rot_tensor, labgeo, recgeo, true);

    if (strcmpi(rectype, 'ASTRA_absorption'))
        projvec_lab = labgeo.beamdir(ones_omega, :);
        projvec_rec = gtGeoLab2Sam(projvec_lab, rot_tensor, labgeo, recgeo, true);
    else
        % Normalised projection vector in REC reference
        projvec_rec = gtGeoSam2Sam(projvec_sam, samgeo, recgeo, true, false);
    end
    projvec_rec = gtMathsNormalizeVectorsList(projvec_rec);


    switch rectype
        case 'ASTRA_absorption'
            % Volume center in REC reference
            samorig_rec = gtGeoSam2Sam(samgeo.orig, samgeo, recgeo, false, false);
            proj_shift  = samorig_rec(ones_omega, :) - vol_center_shift_rec;
            bbcent_rec  = proj_shift;  %bbcent_rec + proj_shift;

        case 'ASTRA_grain'
            vol_center_rec = gtGeoSam2Sam(vol_center, samgeo, recgeo, false, false);
            proj_shift     = -vol_center_rec(ones_omega, :) - vol_center_shift_rec;
            bbcent_rec     = bbcent_rec + proj_shift;

        case 'ASTRA_full'
            bbcent_rec = bbcent_rec - vol_center_shift_rec;
    end

    proj_geom = [projvec_rec, bbcent_rec, detdiru_rec, detdirv_rec];
end


