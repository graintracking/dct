function [minangle,maxangle, bbdir]=gtGeoTwoThetaLimits(im, parameters)
% GTGEOTWOTHETALIMITS  Calculate initial guess of TwoTheta limits
%  
%     [minangle,maxangle, bbdir]=gtGeoTwoThetaLimits(im, parameters)
%     ----------------------------------------------------------
%
%     im  = projection image used to determine approximate direct beam
%     bounding box
%           
%     
%     

    acq = parameters.acq;
    edges = [1 1; 1 acq.xdet; acq.ydet 1; acq.ydet acq.xdet]';
    
    
    questionMsg = 'Are you satisfied with this bounding box for the direct beam? [y/n]';
    
    redo = 1;
    hfig = figure;
    
    while redo
        disp('Please select (zoom onto) the direct beam area and disable zoom afterwards');
        figure(hfig), clf, imagesc(im), gtBrightnessContrast(gca), h = zoom(hfig); set(h, 'Enable', 'on'); waitfor(h, 'Enable', 'off'), hold on;
        disp('-> Now click top-left and bottom-right corners for final selection');
        bbdir = ginput(2);
        bbdir = round([bbdir(1, 1) bbdir(1, 2) bbdir(2, 1)-bbdir(1, 1)+1 bbdir(2, 2)-bbdir(1, 2)+1]);

        % Make sure the bbox width and height are even numbers (needed for correlation)
        bbdir(3:4) = ceil(bbdir(3:4)/2)*2;

        % Plot the new bounding box
        plot([bbdir(1), bbdir(1)+bbdir(3)-1, bbdir(1)+bbdir(3)-1, bbdir(1), bbdir(1)], [bbdir(2), bbdir(2), bbdir(2)+bbdir(4)-1, bbdir(2)+bbdir(4)-1, bbdir(2)], 'r-');
        bbcenter_abs = ((2*bbdir(1))+bbdir(3)-1)/2;
        plot([acq.xdet/2+0.5 acq.xdet/2+0.5], [1 acq.ydet], 'b-.');
        plot([bbcenter_abs bbcenter_abs], [bbdir(2) bbdir(2)+bbdir(4)-1], 'c-.'); % centre of the direct beam
        
        if ~strcmp(inputwdefault(questionMsg, 'n'), 'n')
           redo = 0;
        end
    end   


    % calculate the distance from the edges of the direct beam to the edges of the
    % detector - take the one with maximum distance for estimation of maximum two-theta angle

    maxdist = zeros(4, 1);
    center  = [bbdir(1) + bbdir(3)/2 bbdir(2) + bbdir(4)/2];
    %maxdist(1) = max(sqrt(sum((edges - repmat([bbdir(1) bbdir(2)]',1, 4)).^2)));
    %maxdist(2) = max(sqrt(sum((edges - repmat([bbdir(1) bbdir(2) + bbdir(4)]',1, 4)).^2)));
    %maxdist(3) = max(sqrt(sum((edges - repmat([bbdir(1) + bbdir(3) bbdir(2)]',1, 4)).^2)));
    %maxdist(4) = max(sqrt(sum((edges - repmat([bbdir(1) + bbdir(3) bbdir(2) + bbdir(4)]',1, 4)).^2)));
   
    maxdist = max(sqrt(sum((edges - repmat(center', 1, 4)).^2)));
    
    maxangle = atand(maxdist * parameters.acq.pixelsize / parameters.acq.dist);

    minangle = 0;


end % end of function