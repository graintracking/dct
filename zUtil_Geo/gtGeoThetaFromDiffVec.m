function thetas = gtGeoThetaFromDiffVec(diffvec, labgeo)
% FUNCTION thetas = gtGeoThetaFromDiffVec(diffvec, labgeo)
%
% Determines the theta Bragg angles for a set of diffraction vectors
% given in the Lab reference.
% Thetas are calculated as: 0.5 * acosd(beamdir * diffvec').
%
% INPUT  
%   diffvec  - Lab coordinates [X, Y, Z] of diffraction vectors; unit
%              vectors (n, 3)
%   beamdir  - Lab coordinates of the beam direction; unit vector (1, 3)
%
% OUTPUT
%   thetas   - coloumn vector of theta angles in degrees (n, 1)
%

    diffvec = gtMathsNormalizeVectorsList(diffvec);

    thetas  = 0.5 * acosd(diffvec * labgeo.beamdir');
end