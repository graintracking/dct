function detgeo = gtGeoComputeExtraDetgeoProperties(detgeo)
    detgeo.detscaleu = 1 / detgeo.pixelsizeu;
    detgeo.detscalev = 1 / detgeo.pixelsizev;

    detgeo.detnorm = gtMathsCross(detgeo.detdiru, detgeo.detdirv); % unit vector

    detgeo.detorig = gtGeoDetOrig(detgeo);

    detgeo.Qdet = gtFedDetectorProjectionTensor( ...
        detgeo.detdiru, detgeo.detdirv, ...
        detgeo.detscaleu, detgeo.detscalev);
end
