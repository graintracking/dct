function par_detgeo = gtGeoDetDefaultParameters(acq)
% GTGEOLABDEFAULTPARAMETERS
%     par_labgeo = gtGeoDetDefaultParameters(acq)
%     -------------------------------------------
%     Use of acq.dist, acq.pixelsize, acq.xdet, acq.ydet,
%     acq.no_direct_beam, acq.bb, acq.rotation_axis, acq.rotation_direction,
%     acq.detector_definition, [acq.rotationname]
%     Added interactive confirmation of default detdiru and detdirv settings

par_detgeo = [];

% Pixel and detector size
par_detgeo.pixelsizeu = acq.pixelsize;
par_detgeo.pixelsizev = acq.pixelsize;
par_detgeo.detsizeu   = acq.xdet; % number of pixels along the u direction
par_detgeo.detsizev   = acq.ydet; % number of pixels along the v direction

par_detgeo.detrefu = (par_detgeo.detsizeu/2) + 0.5;
par_detgeo.detrefv = (par_detgeo.detsizev/2) + 0.5;

% Detector angular coverage limits (2theta) for entire sample volume
par_detgeo.detanglemin = 0;
par_detgeo.detanglemax = 45;

if (acq.no_direct_beam && strcmp(acq.detector_definition, 'vertical'))
    % Assume vertical setup; detector is above sample pointing downwards (-Z)
    disp(['No direct beam case with a vetical detector. ' ...
        'Any flip was applied during the scan... Please check if it is true'])
    par_detgeo.detrefpos = [0 acq.dist 0];
    par_detgeo.detdiru   = inputwdefaultnumeric('Approximate detdiru', '0 0 -1');
    par_detgeo.detdirv   = inputwdefaultnumeric('Approximate detdirv', '1 0 0');

    if (strcmp(acq.rotation_direction, 'counterclockwise'))
        % is this really true?
        par_detgeo.detrefpos = -1 * par_detgeo.detrefpos;
        par_detgeo.detdiru   = -1 * par_detgeo.detdiru;
    end

elseif (~acq.no_direct_beam)
    % inline detector HR camera - suppose it on x-axis
    disp('High resolution direct beam scan...')
    
    par_detgeo.detrefpos = [acq.dist 0 0];
    par_detgeo.detdiru   = inputwdefaultnumeric('Approximate detdiru: ', '0  1  0');
    par_detgeo.detdirv   = inputwdefaultnumeric('Approximate detdirv: ', '0  0 -1');

    if (strcmp(acq.rotation_direction, 'counterclockwise'))
        % is this really true?
        par_detgeo.detdiru = -1 * par_detgeo.detdiru;
        par_detgeo.detdirv = -1 * par_detgeo.detdirv;
    end
    % Compute temporary detorigin
    par_detgeo = gtGeoComputeExtraDetgeoProperties(par_detgeo);
    % Should take sample envelope from sample bbox (parameters.acq.bb) if
    % exists, otherwise in gtPreprocessing
    if (isfield(acq, 'bb') && ~isempty(acq.bb))
        par_detgeo.detrefpos(2:3) = gtGeoDetRefPos(acq.bb, par_detgeo);
    end
	
elseif (acq.no_direct_beam && strcmp(acq.detector_definition, 'inline'))
    % inline detector - suppose it on x-axis
    % taper scan (far field)
    disp('This looks like a taper scan...')
    par_detgeo.detrefpos = [acq.dist 0 0];
    par_detgeo.detdiru   = inputwdefaultnumeric('Approximate detdiru: ', '0  1  0');
    par_detgeo.detdirv   = inputwdefaultnumeric('Approximate detdirv: ', '0  0 -1');
    % Compute temporary detorigin
    par_detgeo = gtGeoComputeExtraDetgeoProperties(par_detgeo);
    % Should take sample envelope from sample bbox (parameters.acq.bb) if
    % exists, otherwise in gtPreprocessing
    if (isfield(acq,'bb') && isempty(acq.bb))
        % guess a sample bounding box
        acq.bb = [acq.xdet/2-50 acq.ydet/2-50 100 100];
    end
    if (isfield(acq,'bb') && ~isempty(acq.bb))
        par_detgeo.detrefpos(2:3) = gtGeoDetRefPos(acq.bb, par_detgeo);
    end
    disp('Choose a squared sample bounding box in the center of the image...')
end

par_detgeo = gtGeoComputeExtraDetgeoProperties(par_detgeo);

end % end of function


%%%%%%%%%%%%%%%%%%%%%
% SUB-FUNCTIONS
%%%%%%%%%%%%%%%%%%%%%
% has been moved to gtGeoDetRefPos
function d_yz = sfDetrefpos(acq_bb, par_labgeo)
% Returns the detector lateral (Y) and vertical (Z) positions which approximately
% account for an offset of the sample bounding box position:
%   Y lateral: It assumes that the projection of the rotation axis is at the
%   center of the sample bounding box.
%   Vertical center of the sample bounding (beam center) corresponds to Z=0.
%   It assumes rotpos = [0 0 0] and view into the beam.

    d_yz(1) = -((acq_bb(1) + acq_bb(3)/2) - par_labgeo.detrefu - 0.5) * par_labgeo.pixelsizeu;
    d_yz(2) =  ((acq_bb(2) + acq_bb(4)/2) - par_labgeo.detrefv - 0.5) * par_labgeo.pixelsizev;
end