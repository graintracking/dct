function [labXYZ, det2lab] = gtGeoDet2Lab(detUV, detgeo, freevec_flag)
% FUNCTION [labXYZ, det2lab] = gtGeoDet2Lab(detUV, detgeo, freevec_flag)
%
% DETECTOR -> LAB coordinate transformation using arbitrary geometry.
%
% Given the Detector coordinates of a set of points, transforms them into
% Lab coordinates.
%
% INPUT  detUV  - detector coordinates in pixels (n,2)
%
%        Geometry parameters in LAB coordinates:
%        detgeo.detorig    - position of detector origin (pixel {0,0});
%                            size (1,3)
%        detgeo.detdiru    - detector U direction (unit row vector)
%        detgeo.detdirv    - detector V direction (unit row vector)
%        detgeo.pixelsizeu - pixel size U (same unit as position values)
%        detgeo.pixelsizev - pixel size V (same unit as position values)
%        detgeo.det2lab (optional) - transformation matrix from Detector to
%                                    Lab coordinates
%
%        freevec_flag      - 1 for free vectors, 0 for bound vectors
%
% OUTPUT labXYZ            - array of the LAB coordinates (n,3)
%        det2lab           - coordinate transformation matrix

    % Coordinate transformation matrix
    if (~isfield(detgeo, 'det2lab'))
        det2lab = [ ...
            detgeo.pixelsizeu * detgeo.detdiru; ...
            detgeo.pixelsizev * detgeo.detdirv ];
    else
        det2lab = detgeo.det2lab;
    end

    % New coordinates
    if (freevec_flag)  % free vectors
        labXYZ = detUV * det2lab;
    else   % bound vectors
        labXYZ = detUV * det2lab + detgeo.detorig(ones(size(detUV, 1), 1), :);
    end
end
