%
% FUNCTION scattvec = gtGeoScattVecFromDiffVec(diffvec,beamdir)
%
% Given a set of diffraction vectors with their Lab coordinates X,Y,Z and 
% the incoming beam direction, the function returns the Lab coordinates 
% of the corresponding unit scattering vectors (plane normals)
% The diffraction vectors indicate the direction of the diffracted beam 
% paths. The scattering vectors are computed as: diffvec - beamdir . 
%
% INPUT  diffvec - Lab coordinates X,Y,Z of a set of diffraction vectors;
%                  unit vectors! (n,3)      
%        beamdir - Lab coordinates X,Y,Z of the incoming beam direction; 
%                  unit vector! (1,3)
%
% OUTPUT scattvec - Lab coordinates of the normalised scattering vectors;
%                   unit vectors (n,3)
%

% !!! it probably doesn't matter if it's the Lab, but has to be the same
% reference for all

function scattvec = gtGeoScattVecFromDiffVec(diffvec,beamdir)

% Scattering vectors 
scattvec = diffvec - beamdir(ones(size(diffvec,1),1),:);

% Normalise scattvec-s:
snorm = sqrt(scattvec(:,1).^2 + scattvec(:,2).^2 + scattvec(:,3).^2) ;

scattvec(:,1) = scattvec(:,1)./snorm ; 
scattvec(:,2) = scattvec(:,2)./snorm ;
scattvec(:,3) = scattvec(:,3)./snorm ;


end