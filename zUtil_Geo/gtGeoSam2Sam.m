function [xyzB, samA2samB] = gtGeoSam2Sam(xyzA, samgeoA, samgeoB, freevec, ...
                             rot_flag)
% GTGEOSAM2SAM SAMPLE -> SAMPLE reference coordinate transformation.
%
% [xyzB, samA2samB] = gtGeoSam2Sam(xyzA, samgeoA, samgeoB, freevec, rot_flag)
%
% --------------------------------------------------------------------
%
% Given the coordinates of a set of points in reference SampleA, transforms
% them into coordinates in reference SampleB.
% It optionally creates the transformation matrix.
%
% INPUT  
%   xyzA           - coordinates in reference SampleA in no. of voxels (n,3)
%   samgeoA.orig   - origin of reference A in Lab coordinates (1,3)
%   samgeoB.orig   - origin of reference B in Lab coordinates (1,3)
%   freevec        - 0 for fixed vectors (e.g. location);
%                    1 for free vectors (e.g. orientation)
%   samgeoA.voxsize - [x,y,z] voxel sizes A in lab units
%                     (required if samgeoA.sam2lab is not defined; )
%   samgeoB.voxsize - [x,y,z] voxel sizes B in lab units
%                     (required if samgeoB.lab2sam is not defined; )
%   rot_flag        - if true, only applies rotation and the norm of the 
%                     input vectors will be unchanged; i.e. no scaling 
%                     applied according to the voxel sizes (only works if 
%                     'samgeoA.sam2lab' and 'samgeoB.lab2sam' are undefined)
%
% OPTIONAL INPUT
%   samgeoA.sam2lab - matrix for transformation from SampleA to Lab coordinates (3,3)
%                     for row vectors
%   samgeoB.lab2sam - matrix for transformation from Lab to SampleB coordinates (3,3)
%                     for row vectors
%
% OUTPUT 
%   xyzB      - coordinates in reference SampleB in no. of voxels (n,3)
%   samA2samB - coordinate transformation matrix from Sample A to B (3,3)
%

    if (~exist('rot_flag', 'var'))
        rot_flag = false;
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%% Change of reference from SampleA to Lab
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % Transformation matrix from Sample to Lab coordinates:
    %   (Lab is an orthonormal basis by definition.)
    if (~isfield(samgeoA,'sam2lab') || isempty(samgeoA.sam2lab))
        if (rot_flag)
            % Keep norm of input vectors -> keep unit lengths in the rows of
            % the transformation matrix
            sam2labA = [samgeoA.dirx; samgeoA.diry; samgeoA.dirz];
        else
            sam2labA = [ ...
                samgeoA.dirx * samgeoA.voxsize(1); ...
                samgeoA.diry * samgeoA.voxsize(2); ...
                samgeoA.dirz * samgeoA.voxsize(3) ];
        end
    elseif (rot_flag)
        error('Transformation matrix already defined. Set ''rot_flag'' to false.')
    else
        sam2labA = samgeoA.sam2lab;
    end

    n = size(xyzA, 1);
    ones_n = ones(n, 1);

    % Vectors in Lab reference without rotation around the rotation axis
    if (freevec)
        % free vectors
        labXYZ = xyzA * sam2labA;
    else
        % offset vectors
        labXYZ = xyzA * sam2labA + samgeoA.orig(ones_n, :);
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%% Change of reference from Lab to SampleB
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % Transformation matrix from Lab to Sample coordinates:
    %   Note that Lab is an orthonormal basis by definition, but Sample may not be !!
    %   Therefore needs calculation of the inverse, and not only transpose.
    if (~isfield(samgeoB, 'lab2sam') || isempty(samgeoB.lab2sam))
        if (rot_flag)
            % Keep norm of input vectors -> keep unit lengths in the rows of
            % the transformation matrix
            sam2labB = [samgeoB.dirx; samgeoB.diry; samgeoB.dirz];
        else
            sam2labB = [ ...
                samgeoB.dirx * samgeoB.voxsize(1); ...
                samgeoB.diry * samgeoB.voxsize(2); ...
                samgeoB.dirz * samgeoB.voxsize(3) ];
        end
        lab2samB = inv(sam2labB);

    elseif (rot_flag)
        error('Transformation matrix already defined. Set ''rot_flag'' to false.')
    else
        lab2samB = samgeoB.lab2sam;
    end

    % Vectors in SampleB reference
    if (freevec)
        % free vectors
        xyzB = labXYZ * lab2samB;
    else
        % offset vectors
        xyzB = (labXYZ - samgeoB.orig(ones_n, :)) * lab2samB;
    end

    samA2samB = sam2labA * lab2samB;
end
