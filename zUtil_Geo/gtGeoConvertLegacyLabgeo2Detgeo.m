function [detgeo, labgeo] = gtGeoConvertLegacyLabgeo2Detgeo(labgeo)
    detgeo.detrefpos = labgeo.detrefpos;

    detgeo.detsizeu = labgeo.detsizeu;
    detgeo.detsizev = labgeo.detsizev;
    detgeo.detrefu = labgeo.detrefu;
    detgeo.detrefv = labgeo.detrefv;

    detgeo.detdiru = labgeo.detdiru;
    detgeo.detdirv = labgeo.detdirv;
    detgeo.pixelsizeu = labgeo.pixelsizeu;
    detgeo.pixelsizev = labgeo.pixelsizev;

    detgeo.detanglemin = labgeo.detanglemin;
    detgeo.detanglemax = labgeo.detanglemax;

    detgeo = gtGeoComputeExtraDetgeoProperties(detgeo);

    % Removing duplicates
    detfields = fieldnames(detgeo);
    for ii_f = 1:numel(detfields)
        field = detfields{ii_f};
        if (isfield(labgeo, field))
            labgeo = rmfield(labgeo, field);
        end
    end
end
