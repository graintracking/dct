function labgeo = gtGeoConvertDetgeo2LegacyLabgeo(detgeo, labgeo)

    % merging the structures
    detfields = fieldnames(detgeo);
    for ii_f = 1:numel(detfields)
        field = detfields{ii_f};
        labgeo.(field) = detgeo.(field);
    end
end
