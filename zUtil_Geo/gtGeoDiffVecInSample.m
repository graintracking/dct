function [diffvec, samAXYZ, theta] = gtGeoDiffVecInSample(detAUV, omega, samBXYZ, detgeo, labgeo, samgeo)
% FUNCTION [diffvec, samAXYZ, theta] = gtGeoDiffVecInSample(detAUV, omega, samBXYZ, detgeo, labgeo, samgeo)
%
% Given a set of points A on the detector with their Detector coordinates
% U, V, omega and another set of points B with their Sample coordinates X, Y, Z, 
% the function returns the corresponding diffraction vectors in the Sample
% reference. The diffraction vectors indicate the direction of the
% diffracted beam paths relative to a fixed sample.
% It also returns the Bragg angles and the Sample coordinates of points A.
%
% INPUT  detAUV  - Detector coordinates U, V of the first set of points (n, 2)
%        omega   - rottaion angle corresponding to U, V; usually positive (n, 1)
%        samBXYZ - Sample coordinates X, Y, Z of the second set of points (n, 3)
%
%        Lab geometry parameters in Lab coordinates:
%
%        labgeo.rotpos   - rotation axis position (row vector)
%        labgeo.rotdir   - rotation axis direction (unit row vector)
%        detgeo.detorig  - position of detector origin (row vector)
%        detgeo.detdiru  - detector U direction (unit row vector)
%        detgeo.detdirv  - detector V direction (unit row vector)
%        detgeo.pixelsizeu - pixel size U (same unit as position values)
%        detgeo.pixelsizev - pixel size V (same unit as position values)
%
%        Geometry parameters in LAB coordinates:
%
%        samgeo.orig - Lab cooridnates of the origin of sample reference (1, 3)
%
%        Either
%        samgeo.lab2sam - coordinate transformation matrix Lab -> Sample (3, 3)
%        Or
%        samgeo.voxsize - Sample voxel sizes X, Y, Z in Lab units (1, 3)
%        samgeo.dirx - Lab cooridnates of the Sample X axis; unit vector (1, 3)
%        samgeo.diry - Lab cooridnates of the Sample Y axis; unit vector (1, 3)
%        samgeo.dirz - Lab cooridnates of the Sample Z axis; unit vector (1, 3)
%
%        For output theta
%        labgeo.beamdir
%
% OUTPUT diffvec - Sample coordinates of the normalised diffraction vectors;
%                  the positive direction of these vectors are such that they
%                  would point towards the detector when turned into the
%                  diffracting position. (n, 3)
%        samAXYZ - Sample coordinates of points A (n, 3)
%        theta   - Bragg angles theta in degrees (n, 1)

    % Coordinate transformation from Detector to Lab
    labA = gtGeoDet2Lab(detAUV, detgeo, 0);

    % Coordinate transformation from Lab to Sample
    samAXYZ = gtGeoLab2Sam(labA, omega, labgeo, samgeo, false);

    % Diffraction vectors from points samXYZ to point samA:
    diffvec = samAXYZ - samBXYZ(ones(size(samAXYZ, 1), 1), :);

    % Normalise diffvec-s (seems the fastest way for nx3 vectors):
    dnorm = sqrt(diffvec(:, 1).^2 + diffvec(:, 2).^2 + diffvec(:, 3).^2) ;

    diffvec(:, 1) = diffvec(:, 1)./dnorm ;
    diffvec(:, 2) = diffvec(:, 2)./dnorm ;
    diffvec(:, 3) = diffvec(:, 3)./dnorm ;

    
    if (nargout == 3)
        % Coordinate transformation from Lab to Sample for beam direction
        % (free vector)
        beamdirlab = labgeo.beamdir(ones(length(omega), 1), :);
        beamdirsam = gtGeoLab2Sam(beamdirlab, omega, labgeo, samgeo, true, 'only_rot', true);

        % Use the dot products of beamdir and diffvec to get theta
        theta = 0.5 * acosd( sum(beamdirsam.*diffvec, 2) );
    end
end
