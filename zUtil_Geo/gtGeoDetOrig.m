function detorig = gtGeoDetOrig(detgeo)
% GTGEODETORIG Calculates the detector origin.
% 
%     detorig = gtGeoDetOrig(detgeo)
%
%     -------------------------------------------------------
%     Calculates the coordinates of the detector origin (pixel 0,0) in the
%     LAB reference.
%
%     INPUT:
%       labgeo - as in the parameters file
%
%     OUTPUT:
%       detorig = detector origin X,Y,Z coordinates (1x3)
%

    detorig = detgeo.detrefpos ...
              - detgeo.detdiru * detgeo.detrefu * detgeo.pixelsizeu ...
              - detgeo.detdirv * detgeo.detrefv * detgeo.pixelsizev;
end
