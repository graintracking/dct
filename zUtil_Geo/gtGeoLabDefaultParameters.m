function par_labgeo = gtGeoLabDefaultParameters(acq)
% GTGEOLABDEFAULTPARAMETERS
%     par_labgeo = gtGeoLabDefaultParameters(acq)
%     -------------------------------------------
%     Use of acq.dist, acq.pixelsize, acq.xdet, acq.ydet,
%     acq.no_direct_beam, acq.bb, acq.rotation_axis, acq.rotation_direction,
%     acq.detector_definition, [acq.rotationname]
% 

par_labgeo = [];

% Beam direction, rot. axis position
par_labgeo.beamdir = [1 0 0];
par_labgeo.rotpos  = [0 0 0];

% Coordinate axes definition and Lab unit (for records only)
par_labgeo.deflabX = 'Along the beam direction.';
par_labgeo.deflabY = 'Right-handed from Y=cross(Z,X).';
par_labgeo.deflabZ = 'Along rotation axis. Positive away from sample stage.';
par_labgeo.labunit = 'mm';

% Rotation axis direction
switch (acq.rotation_name)
    case {'pmo' , 'srot'}
        par_labgeo.rotdir = [0 0 -1];
    case 'diffrz'
        par_labgeo.rotdir = [0 0 1];
    case 'unknown'
        par_labgeo.rotdir = [0 0 -1];
        disp('Verify the parameter ''rotdir'' by yourself')
end

if strcmp(acq.rotation_axis, 'horizontal')
    if strcmp(acq.rotation_direction, 'clockwise')
        % It assumes clockwise rotation for images
        par_labgeo.rotdir = -par_labgeo.rotdir;
    end    
end

if (acq.no_direct_beam && strcmp(acq.detector_definition, 'vertical'))
    % Assume vertical setup; detector is above sample pointing downwards (-Z)
    disp(['No direct beam case with a vetical detector. ' ...
        'Any flip was applied during the scan... Please check if it is true'])
    % Should guess sample envelope
    par_labgeo.samenvtop = [];
    par_labgeo.samenvbot = [];
    par_labgeo.samenvrad = [];
	
elseif (~acq.no_direct_beam)
    % inline detector HR camera - suppose it on x-axis
    disp('High resolution direct beam scan...')

    % Should take sample envelope from sample bbox (parameters.acq.bb) if
    % exists, otherwise in gtPreprocessing
    if (isfield(acq, 'bb') && ~isempty(acq.bb))
        par_labgeo = gtGeoSamEnvFromAcq(par_labgeo, acq);
    else
        par_labgeo.samenvtop = [];
        par_labgeo.samenvbot = [];
        par_labgeo.samenvrad = [];
    end
	
elseif (acq.no_direct_beam && strcmp(acq.detector_definition, 'inline'))
    % inline detector - suppose it on x-axis
    % taper scan (far field)
    disp('This looks like a taper scan...')

    % Should take sample envelope from sample bbox (parameters.acq.bb) if
    % exists, otherwise in gtPreprocessing
    if (isfield(acq,'bb') && isempty(acq.bb))
        % guess a sample bounding box
        acq.bb = [acq.xdet/2-50 acq.ydet/2-50 100 100];
    end
    par_labgeo = gtGeoSamEnvFromAcq(par_labgeo, acq);
    disp('Choose a squared sample bounding box in the center of the image...')
end

end
