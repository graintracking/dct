%
% FUNCTION projUV = gtGeoPointProjectionsOnDetector(labXYZ,projvecs,labgeo)
% 
% Given the Lab coordinates (X,Y,Z) of a point and a set of projection
% directions, it returns the Detector coordinates (U,V) of the projections 
% of that point on the detector plane.
% For perpendicular projections and several points, consider using 
% gtGeoLab2Det.
%
% INPUT  labXYZ   - LAB coordinates of point to be projected (1,3)
%        projvecs - projection vectors (do not have to be unit vectors) (n,3)
%
%        Geometry parameters in LAB coordinates:
%
%        labgeo.detorig    - position of detector origin
%        labgeo.detdiru    - detector U direction (unit vector)
%        labgeo.detdirv    - detector V direction (unit vector)
%        labgeo.detnorm    - detector normal vector (unit vector)
%	     labgeo.pixelsizeu - pixel size U (same unit as position)
%	     labgeo.pixelsizev - pixel size V (same unit as position)
%	     labgeo.detsizeu   - detector size U in pixels
%	     labgeo.detsizev   - detector size V in pixels
%
% OUTPUT projUV  - coloumn vectors of Detector coordinates in pixels (n,2);
%                  NaN for those which fall outside the detector area, or
%                  the projection vector points away from the detector
%                  plane.
% 


function projUV = gtGeoPointProjectionsOnDetector(labXYZ,projvecs,labgeo)

% Multiplication factor for projection vector
mf = (((labgeo.detorig - labXYZ) * labgeo.detnorm') ./ (projvecs * labgeo.detnorm'));

% Projected points in Lab coordinates X,Y,Z
projXYZ = labXYZ(ones(size(projvecs,1),1),:) + [mf mf mf].*projvecs ;

% Projected points in Scintillator coordinates U,V
projUV = (projXYZ - labgeo.detorig(ones(size(projvecs,1),1),:))*...
    [labgeo.detdiru'/labgeo.pixelsizeu, labgeo.detdirv'/labgeo.pixelsizev];

% Check if out of detector U,V range or projection is away from detector
uvout = false(size(projUV,1),5);

uvout(:,1:2) = projUV < 0.5 ;
uvout(:,3)   = projUV(:,1) > labgeo.detsizeu + 0.5 ;
uvout(:,4)   = projUV(:,2) > labgeo.detsizev + 0.5 ;
uvout(:,5)   = mf < 0; % the projection points away from detector plane

projUV(any(uvout,2),:) = NaN;

										
end