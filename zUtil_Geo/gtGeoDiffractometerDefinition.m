function diff = gtGeoDiffractometerDefinition(types, varargin)
    diff = struct( ...
        'axes_basetilt', zeros(1, 3), ...
        'axes_rotation', zeros(1, 3), ...
        'axes_sam_tilt', zeros(0, 3), ... % They are applied in reverse order (last->first) for SAM->LAB transformations in gtGeoDiffractometerTensor
        'origin_basetilt', zeros(1, 3), ...
        'origin_rotation', zeros(1, 3), ...
        'origin_sam_tilt', zeros(0, 3), ...
        'limits_sam_tilt', zeros(0, 2), ...
        'shifts_sam_stage', zeros(0, 3), ...
        'axes_rotation_offset', [], ...
        'axes_sam_tilt_offset', [] );

    diff = parse_pv_pairs(diff, varargin);

    switch (lower(types))
        case 'esrf_id11'
            diff.axes_basetilt = [0, 1, 0];
            diff.axes_rotation = [0, 0, 1];
            diff.axes_sam_tilt = [0, 1, 0; 1, 0, 0]; % In sam2lab: first around x, and then around y
            diff.origin_sam_tilt = [0, 0, 0; 0, 0, 0];
            diff.limits_sam_tilt = [-15, 15; -20 20];
        case 'esrf_id06'
            diff.axes_basetilt = [0, 1, 0];
            diff.axes_rotation = [0, 0, 1];
            diff.axes_sam_tilt = [0, 1, 0; 1, 0, 0];
            diff.origin_sam_tilt = [0, 0, 0; 0, 0, 0];
            diff.limits_sam_tilt = [-8, 8; -10 10];
        otherwise
            error('gtAcqDiffractometerDefinition:wrong_argument', ...
                'Unknown diffractometer: %s', types)
    end

%     % The following code path has been REMOVED, because we prefer to handle
%     % the axes in a more explicit way.
%     % It is only kept in comment form to serve as reminder of an
%     % interesting alternative.

%     % In case there are tilt offsets, we have to compute what the
%     % translations and tilts look like in reality (the offsets change the
%     % alignment of the axes)
%     if (~isempty(diff.axes_sam_tilt_offset))
%         rot = eye(3);
%         % The last tilt does not affect the other tilts
%         for ii = (size(diff.axes_sam_tilt, 1)-1):-1:1
%             rotcomp = gtMathsRotationMatrixComp(diff.axes_sam_tilt(ii, :), 'row');
%             rot_ii = gtMathsRotationTensor(diff.axes_sam_tilt_offset(ii), rotcomp);
%             rot = rot * rot_ii;
%         end
%
%         diff.axes_sam_tilt = diff.axes_sam_tilt * rot;
%
%         % The translations need all the tilt offsets to be recomputed
%         rotcomp = gtMathsRotationMatrixComp(diff.axes_sam_tilt(end, :), 'row');
%         rot_ii = gtMathsRotationTensor(diff.axes_sam_tilt_offset(end), rotcomp);
%         rot = rot_ii * rot;
%
%         diff.shifts_sam_stage = diff.shifts_sam_stage * rot;
%     end
%
%     % Same as for the tilt offsets, if we have a rotation offset...
%     if (~isempty(diff.axes_rotation_offset))
%         rotcomp = gtMathsRotationMatrixComp(diff.axes_rotation, 'row');
%         rot = gtMathsRotationTensor(diff.axes_rotation_offset, rotcomp);
%
%         diff.axes_sam_tilt = diff.axes_sam_tilt * rot;
%         diff.shifts_sam_stage = diff.shifts_sam_stage * rot;
%     end
end
