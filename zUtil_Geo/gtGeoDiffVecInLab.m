function [diffvec, labAXYZ, theta] = gtGeoDiffVecInLab(detAUV, labBXYZ, detgeo, labgeo)
% FUNCTION [diffvec, labAXYZ, theta] = gtGeoDiffVecInLab(detAUV, labBXYZ, detgeo, labgeo)
%
% Given a set of points A on the detector with their Detector coordinates
% U, V, and another set of points B with their Lab coordinates X, Y, Z,
% the function returns the corresponding diffraction vectors in the Lab
% reference. The diffraction vectors indicate the direction of the
% diffracted beam paths.
% It also returns the Bragg angles and the Lab coordinates of points A.
%
% INPUT  detAUV  - Detector coordinates U, V of the points A (n, 2)
%        labBXYZ - Lab coordinates X, Y, Z of points B (n, 3)
%
%        Geometry parameters in Lab coordinates:
%
%        labgeo.rotpos   - rotation axis position (row vector)
%        labgeo.rotdir   - rotation axis direction (unit row vector)
%        detgeo.detorig  - position of detector origin (row vector)
%        detgeo.detdiru  - detector U direction (unit row vector)
%        detgeo.detdirv  - detector V direction (unit row vector)
%        detgeo.pixelsizeu - pixel size U (same unit as position values)
%        detgeo.pixelsizev - pixel size V (same unit as position values)
%
% OUTPUT diffvec - Lab coordinates of the normalised diffraction vectors;
%                  direction points from points B to A (n, 3)
%        labAXYZ - Lab coordinates of points A (n, 3)
%        theta   - Bragg angles theta in degrees (n, 1)
%

    % Coordinate transformation from Detector to Lab
    labAXYZ = gtGeoDet2Lab(detAUV, detgeo, 0);

    % Diffraction vectors from labXYZ to point A:
    diffvec = labAXYZ - labBXYZ;

    % Normalise diffvec-s (seems the fastest way for nx3 vectors):
    dnorm = sqrt(sum(diffvec .^ 2, 2));

    diffvec = diffvec ./ dnorm(:, [1 1 1]);

    if (nargout == 3)
        % Use the dot products of beamdir and diffvec to get theta
        theta  = 0.5 * acosd(diffvec * labgeo.beamdir');
    end
end

