function [detUV, lab2det] = gtGeoLab2Det(labXYZ, detgeo, freevec_flag)
% FUNCTION [detUV, lab2det] = gtGeoLab2Det(labXYZ, detgeo, freevec_flag)
%
% LAB -> DETECTOR coordinate transformation using arbitrary geometry.
%
% Given the Lab coordinates of a set of points, transforms them into 
% Detector coordinates. If labXYZ is not in the detector plane, 
% its perpendicular projection onto the detector plane will be used.
%
% INPUT  labXYZ - array of LAB coordinates (n, 3)
%
%        Geometry parameters given in LAB coordinates:
%        detgeo.detorig    - position of detector origin: pixel {0, 0};
%                            size (1, 3)
%        detgeo.detdiru    - detector U direction (unit row vector)
%        detgeo.detdirv    - detector V direction (unit row vector)
%        detgeo.pixelsizeu - pixel size U (same unit as position)
%        detgeo.pixelsizev - pixel size V (same unit as position)
%
%        freevec_flag      - 1 for free vectors, 0 for bound vectors
%
% OUTPUT detUV             - detector coordinates in pixels (n, 2)
%        lab2det           - transformation matrix from Lab to Detector (3, 2)
%

    if isfield(detgeo, 'lab2det')
        lab2det = detgeo.lab2det;
    else
        % Determine the det2lab transformation matrix from geometry.
        % The u, v basis may not be orthonormal, not like the Lab reference which
        % is an orthonormal basis by definition. Therefore here the decomposition
        % of labXYZ into u and v vectors needs to be done adequately.
        % The transformation matrix lab2det is the same as the pseudo-inverse
        % of det2lab.
        u = detgeo.detdiru;
        v = detgeo.detdirv;
        uv = u*v';

        a = (u - v * uv) / (1 - uv * uv);
        b = v - a * uv;

        lab2det = [a / detgeo.pixelsizeu; b / detgeo.pixelsizev]';
    end

    % New coordinates

    if (freevec_flag)  % free vectors
        detUV = labXYZ * lab2det;
    else  % offset vectors
        detUV = (labXYZ - detgeo.detorig(ones(size(labXYZ, 1), 1), :)) * lab2det;
    end
end
