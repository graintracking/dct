function pl_lab = gtGeoPlLabFromThetaEta(thetas, etas, labgeo)
% FUNCTION pl_lab = gtGeoPlLabFromThetaEta(thetas, etas, labgeo)

%     % Here just for reference!
% 
%     % Vector pointing to eta = 90:
%     eta90 = gtMathsCross(labgeo.beamdir, labgeo.rotdir);
%     eta90 = reshape(eta90, [], 1);
% 
%     % It should be -eta90, but we will need it transosed later for the
%     % vectorization, so we will produce it wth +eta90
%     rot_comp_t = gtMathsRotationMatrixComp(eta90, 'col');
%     rot_comp_n = gtMathsRotationMatrixComp(-labgeo.beamdir', 'col');
% 
%     rot_t = gtMathsRotationTensor(thetas, rot_comp_t);
%     rot_n = gtMathsRotationTensor(etas, rot_comp_n);
% 
%     rot_t = reshape(rot_t, 3, [])';
%     pl_lab = rot_t * (-labgeo.rotdir)';
%     pl_lab = reshape(pl_lab, 1, 3, []);
%     pl_lab = sum(rot_n .* pl_lab([1 1 1], :, :), 2);
%     pl_lab = reshape(pl_lab, 3, [])';

    cos_t = cosd(thetas);
    tan_t = -tand(thetas);

    fact_n = labgeo.rotdir * [0; 0; -1];
     % Omitting the minus for the sin, since eta is here defined as
     % counter clockwise with respect to beamdir
    sin_n = fact_n * sind(etas);
    cos_n = fact_n * cosd(etas);
    pl_lab = cos_t(:, [1 1 1]) .* [tan_t, sin_n, cos_n];
end
