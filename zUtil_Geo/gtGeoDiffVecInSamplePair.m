function [diffvec, samA, theta] = gtGeoDiffVecInSamplePair(detUVA, detUVB, ...
                                omegaA, omegaB, detgeo, labgeo, samgeo)
% FUNCTION [diffvec, samA, theta] = gtGeoDiffVecInSamplePair(detUVA, detUVB, 
%                                 omegaA, omegaB, detgeo, labgeo, samgeo)
%
% Given a set of Friedel pairs of spotsA and B with their Detector 
% coordinates U, V, omega, the function returns the corresponding diffraction
% vectors in the Sample reference. The diffraction vectors indicate the 
% direction of the diffracted beam paths relative to a fixed sample.
% It also returns the Bragg angles and the Sample coordinates of points A. 
%
% The beam and the rotation axis are assumed to be perpendicular.
% The diffraction vector is calculated after mirroring B on the rotation
% axis. This is where it would occur on a pseudo detector, when the sample
% is kept fixed and the beam and detector are rotated 180 degrees. The
% diffraction path is then from Bmirrored to A. The output diffraction 
% vector also points from B towards A.
%
% INPUT  detUVA  - Detector coordinates U, V of points A (n, 2)
%        detUVB  - Detector coordinates U, V of points B (n, 2)
%        omegaA  - rotation angles of A in degrees; usually positive (n, 1)
%        omegaB  - rotation angles of B in degrees; usually positive (n, 1)
%
%        Lab geometry parameters in Lab coordinates:
%
%        labgeo.rotpos     - rotation axis position (row vector)
%        labgeo.rotdir     - rotation axis direction (unit row vector)
%        detgeo.detorig    - position of detector origin (row vector)
%        detgeo.detdiru    - detector U direction (unit row vector)
%        detgeo.detdirv    - detector V direction (unit row vector)
%	     detgeo.pixelsizeu - pixel size U in lab units
%	     detgeo.pixelsizev - pixel size V in lab units
%
%        Geometry parameters in LAB coordinates:
%
%        samgeo.orig - Lab coordinates of the origin of sample reference (1, 3)
%
%        Either 
%        samgeo.lab2sam - coordinate transformation matrix Lab -> Sample (3, 3)
%        Or
%        samgeo.voxsize - Sample voxel sizes X, Y, Z in Lab units (1, 3)
%        samgeo.dirx    - Lab cooridnates of the Sample X axis; unit vector (1, 3)
%        samgeo.diry    - Lab cooridnates of the Sample Y axis; unit vector (1, 3)
%        samgeo.dirz    - Lab cooridnates of the Sample Z axis; unit vector (1, 3)
%
%        For output theta         
%        labgeo.beamdir - beam direction in Lab coordinates; unit vector (1, 3)
%
% OUTPUT diffvec - Sample coordinates of the normalised diffraction vectors;
%                  the positive direction of these vectors are such that they
%                  would point towards points A on the detector when turned
%                  into the diffracting position. (n, 3)
%        samA    - Sample coordinates of points A (n, 3)
%        theta   - Bragg angles theta in degrees (n, 1)
%

    % Coordinate transformation from Detector to Lab
    labA = gtGeoDet2Lab(detUVA, detgeo, 0);
    labB = gtGeoDet2Lab(detUVB, detgeo, 0);

    % Mirror B-s on the rotation axis while staying in the same LAB reference:
    labBmirr = gtMathsMirrorPointsOnAxis(labB, labgeo.rotpos, labgeo.rotdir);

    % Diffraction vectors for spot A are on the path from B mirrored to spot A: 
    dveclab = labA - labBmirr;

    % Normalise diffvec-s (seems the fastest way for nx3 vectors):
    dnorm = sqrt(dveclab(:, 1) .^ 2 + dveclab(:, 2) .^ 2 + dveclab(:, 3) .^ 2);

    dveclab(:, 1) = dveclab(:, 1) ./ dnorm ; 
    dveclab(:, 2) = dveclab(:, 2) ./ dnorm ;
    dveclab(:, 3) = dveclab(:, 3) ./ dnorm ;

    % Coordinate transformation from Lab to Sample (free vector)
    omega   = (omegaA + omegaB) / 2;
    diffvec = gtGeoLab2Sam(dveclab, omega, labgeo, samgeo, true, 'only_rot', true);

    if (nargout > 1)
        % Coordinate transformation from Lab to Sample
        samA = gtGeoLab2Sam(labA, omegaA, labgeo, samgeo, false);

        if (nargout == 3)
            % Use the dot products of beamdir and diffvec to get theta
            theta = 0.5 * acosd(dveclab * labgeo.beamdir');
        end
    end
end


