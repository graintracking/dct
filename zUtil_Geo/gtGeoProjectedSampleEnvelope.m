%
% FUNCTION bbproj = gtGeoProjectedSampleEnvelope(spotu,spotv,labgeo)
%
% Given the location of a diffraction spot, computes the bounding box of
% the projection of a cylindrical sample envelope onto a pseudo detector 
% exactly 180 degrees offset in omega (i.e. mirrored on the rot. axis).
% 
% INPUT
%
%   spotu,spotv      - spot Detector coordinates U and V (scalars in pixels)
%   
%   labgeo.samenvrad - radius of the cylindrical sample envelope
%   labgeo.samenvtop - distance of sample envelope top plane from rotpos 
%                      along rotdir (scalar)
%   labgeo.samenvbot - distance of sample envelope bottom plane from rotpos 
%                      along rotdir (scalar)
%   
%   Other geometry parameters used (defined as usual):
%   labgeo.rotpos
%   labgeo.rotdir
%   labgeo.detdiru
%   labgeo.detdirv
%   labgeo.detnorm
%   labgeo.detorig
%   labgeo.pixelsizeu
%   labgeo.pixelsizev
% 
%
% OUTPUT:
%
%   bbproj = [Umin, Vmin, Umax, Vmax]
%
%
%%
   
    
function bbproj = gtGeoProjectedSampleEnvelope(spotu,spotv,labgeo)  


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Setup coordinates
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% spot A coordinates in Lab system:
spXYZ = gtGeoDet2Lab([spotu,spotv],labgeo,0);

% sample envelop circle centers:
topcenter = labgeo.rotpos + labgeo.rotdir*labgeo.samenvtop ;
botcenter = labgeo.rotpos + labgeo.rotdir*labgeo.samenvbot ;

% detector origin in Lab:
detorig =  gtGeoDet2Lab([0,0],labgeo,0);

% mirror detector origin on rotation axis:
detorigmirr = gtMathsMirrorPointsOnAxis(detorig,labgeo.rotpos,labgeo.rotdir);

% mirror detector u vector (free vector):
detdirumirr = gtMathsMirrorPointsOnAxis(labgeo.detdiru,[0 0 0],labgeo.rotdir);

% mirror detector v vector (free vector):
detdirvmirr = gtMathsMirrorPointsOnAxis(labgeo.detdirv,[0 0 0],labgeo.rotdir);

% mirror detector normal (free vector):
detnormmirr  = gtMathsMirrorPointsOnAxis(labgeo.detnorm,[0 0 0],labgeo.rotdir);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Sample reference points
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% project spot along detector axis u and v onto sample top and bottom plane:
spdirutop = gtMathsProjectPointsOnPlane(spXYZ,labgeo.detdiru,topcenter,labgeo.rotdir) ;
spdirubot = gtMathsProjectPointsOnPlane(spXYZ,labgeo.detdiru,botcenter,labgeo.rotdir) ;
spdirvtop = gtMathsProjectPointsOnPlane(spXYZ,labgeo.detdirv,topcenter,labgeo.rotdir) ;
spdirvbot = gtMathsProjectPointsOnPlane(spXYZ,labgeo.detdirv,botcenter,labgeo.rotdir) ;

% compute where tangents from the points above touch the sample top and
% bottom circles in their own plane:
tanp(1:2,:) = gtMathsCircleTangents(spdirutop,topcenter,labgeo.samenvrad,labgeo.rotdir);
tanp(3:4,:) = gtMathsCircleTangents(spdirubot,botcenter,labgeo.samenvrad,labgeo.rotdir);
tanp(5:6,:) = gtMathsCircleTangents(spdirvtop,topcenter,labgeo.samenvrad,labgeo.rotdir);
tanp(7:8,:) = gtMathsCircleTangents(spdirvbot,botcenter,labgeo.samenvrad,labgeo.rotdir);

% project the tangent points onto the pseudo detector from the spot:
tanpproj(1,:) = gtMathsProjectPointsOnPlane(tanp(1,:), tanp(1,:)-spXYZ, detorigmirr, detnormmirr);
tanpproj(2,:) = gtMathsProjectPointsOnPlane(tanp(2,:), tanp(2,:)-spXYZ, detorigmirr, detnormmirr);
tanpproj(3,:) = gtMathsProjectPointsOnPlane(tanp(3,:), tanp(3,:)-spXYZ, detorigmirr, detnormmirr);
tanpproj(4,:) = gtMathsProjectPointsOnPlane(tanp(4,:), tanp(4,:)-spXYZ, detorigmirr, detnormmirr);
tanpproj(5,:) = gtMathsProjectPointsOnPlane(tanp(5,:), tanp(5,:)-spXYZ, detorigmirr, detnormmirr);
tanpproj(6,:) = gtMathsProjectPointsOnPlane(tanp(6,:), tanp(6,:)-spXYZ, detorigmirr, detnormmirr);
tanpproj(7,:) = gtMathsProjectPointsOnPlane(tanp(7,:), tanp(7,:)-spXYZ, detorigmirr, detnormmirr);
tanpproj(8,:) = gtMathsProjectPointsOnPlane(tanp(8,:), tanp(8,:)-spXYZ, detorigmirr, detnormmirr);

% transform the tangent points Lab coordinates into the pseudo detector coordinates:
tanpprojvec = tanpproj - detorigmirr(ones(8,1),:);
tanpprojU   = (tanpprojvec*detdirumirr')/labgeo.pixelsizeu;
tanpprojV   = (tanpprojvec*detdirvmirr')/labgeo.pixelsizev;

% find the extreme U and V coordinates of the sample envelope on the pseudo detector:
bbproj(1) = min(tanpprojU);
bbproj(2) = min(tanpprojV);
bbproj(3) = max(tanpprojU);
bbproj(4) = max(tanpprojV);


end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % For checking output
%
% keyboard
% 
% %define sample "corners"
% corners(1,:) = [sample_radius sample_bot rot_to_det-sample_radius];
% corners(2,:) = [-sample_radius sample_bot rot_to_det-sample_radius];
% corners(3,:) = [sample_radius sample_top rot_to_det-sample_radius];
% corners(4,:) = [-sample_radius sample_top rot_to_det-sample_radius];
% corners(5,:) = [sample_radius sample_bot rot_to_det+sample_radius];
% corners(6,:) = [-sample_radius sample_bot rot_to_det+sample_radius];
% corners(7,:) = [sample_radius sample_top rot_to_det+sample_radius];
% corners(8,:) = [-sample_radius sample_top rot_to_det+sample_radius];
% 
% %done above
% %spotU=spotU-rotU;
% 
% %relative distance from spot to corner
% deltaX=corners(:,1)-spotU;
% deltaY=corners(:,2)-spotV;
% %relative distance from spot to projection of corner
% deltaX=deltaX.*(2*rot_to_det./corners(:,3));
% deltaY=deltaY.*(2*rot_to_det./corners(:,3));
% %take extremes
% deltaXmin=min(deltaX);
% deltaXmax=max(deltaX);
% deltaYmin=min(deltaY);
% deltaYmax=max(deltaY);
% %assemble output
% % leftX (min X)
% bb(1)=spotU+deltaXmin;
% %allow for rotated detector
% bb(1)=rotU-bb(1);
% % upY (min Y)
% bb(2)=spotV+deltaYmin;
% % rightX (max X)
% bb(3)=spotU+deltaXmax;
% %allow for rotated detector
% bb(3)=rotU-bb(3);
% % botY (max Y)
% bb(4)=spotV+deltaYmax;


