function etas = gtGeoEtaFromDiffVec(diffvec, labgeo)
% FUNCTION etas = gtGeoEtaFromDiffVec(diffvec, labgeo)
%
% Determines the eta diffraction angles for a set of diffraction vectors
% given in the Lab reference.
%
%  ETA DEFINITION
%
% The definition of eta is contained in this function.
% Eta is the azimuth angle (0 <= eta <= 360) on the diffraction cone when 
% looking along the beam direction.It is defined relative to the beam 
% direction (vector 'b') and rotation axis direction (vector 'r'), 
% and it is not dependent on the detector position.
%   When looking along 'b':
%     At Eta=0 the diffracted beam appears aligned but opposite to 'r'.
%     At Eta=180 the diffracted beam appears parallel with 'r'.
%     Eta positive direction is seen by a left-handed rotation of 'r' 
%     around 'b'.
%     This is consistent with the usual DCT setup conventions. Thus when 
%     looking into the beam and the rotation axis is vertical downwards,
%     eta=0 appears at the top of the image and is positive clockwise.
%
% INPUT  
%   diffvec        - Lab coordinates [X,Y,Z] of diffraction vectors (n,3);
%   labgeo.beamdir - Lab coordinates of the beam direction; unit vector (1,3)
%   labgeo.rotdir  - Lab coordinates of the rot. axis direction; unit
%                    vector (1,3)
%
% OUTPUT
%   etas - coloumn vector of eta angles in degrees (n,1)
%

    % The diffraction vectors and the vector of the rotation axis direction 
    % are projected and analysed in a plane perpendicular to the beam.

    % Vector pointing to eta = 0 (opposite to rotation axis direction):
    eta0 = -(labgeo.rotdir - labgeo.beamdir*(labgeo.rotdir*labgeo.beamdir'));
    eta0 = reshape(eta0, [], 1);

    % Vector pointing to eta = 90:
    eta90 = gtMathsCross(labgeo.beamdir, labgeo.rotdir);
    eta90 = reshape(eta90, [], 1);

    % Compute the planar coordinates of the diffraction vectors; calculate
    % etas with 'atan2d':
    etas = atan2d(diffvec * eta90, diffvec * eta0);

    % Set range between 0 and 360deg, after using 'atan2' :
    etas = mod(etas, 360);
end
