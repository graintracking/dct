function maxradius = gtFindActiveArea(parameters)
%%%%%%%%%%%%%%%%%%%%%%
%   function maxradius=gtFindActiveArea(parameters)
%   tries to estimate active area of Detector from images
acq = parameters.acq;

name = fullfile('0_rawdata', acq.name, 'dark.edf');
if exist(name, 'file')
    d = edf_read(name);
else
    name = fullfile('0_rawdata', acq.name, 'darkend0000.edf');
    if exist(name,'file')
       d = edf_read(name) / acq.ndark;
    else
        disp('can not find dark file - use default value');
        d = zeros(acq.ydet, acq.xdet);
    end
end

name1 = fullfile('0_rawdata', acq.name, 'refHST0000.edf');
name2 = fullfile('0_rawdata', acq.name, 'ref0000_0000.edf');
if exist(name1, 'file')
    ref = edf_read(name1)-d;
elseif exist(name2, 'file')
    ref = edf_read(name2)-d;
else
    disp('No initial reference image found!')
    ref = [];
end


% interactive part
colormap gray
imagesc(ref, [-10 100]);
disp('IN THE PRESENCE OF VIGNETTING, PLEASE SELECT A POINT ON THE CIRCUMFERENCE OF THE ACTIVE AREA');
disp('OR ClICK OUTSIDE IMAGE TO ACCEPT FULL DIAGONAL SIZE');

interactive = true;

while interactive
    figure(1);
    hold off
    imagesc(ref, [-10 200])
    title('please select one point and press enter');
    drawnow
    [x,y] = ginput(1);
    if isempty(x)
        maxradius = sqrt((acq.xdet/2)^2+(acq.ydet/2)^2);
        xplot = [1:acq.xdet/20:acq.xdet, acq.xdet*ones(1,20), acq.xdet:-acq.xdet/20:1, ones(1,20)];
        yplot = [ones(1,20), 1:acq.ydet/20:acq.ydet, acq.ydet*ones(1,20), acq.ydet:-acq.ydet/20:1];
        hold on
        plot(xplot, yplot, 'o');
    else
        maxradius = round(sqrt((acq.xdet/2-x)^2+(acq.ydet/2-y)^2));
        angles = 0:10:350;
        xplot = maxradius*sind(angles)+acq.xdet/2;
        yplot = maxradius*cosd(angles)+acq.ydet/2;
        hold on
        plot(xplot, yplot, 'o')

    end
    check = inputwdefault('Are you satisfied with this selection? [y/n]', 'y');
    if strcmpi(check,'y')
      interactive = false;
    end
end







