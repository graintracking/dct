function omega_step = gtAcqGetOmegaStep(parameters, det_ind, unit)
    if (~exist('det_ind', 'var') || isempty(det_ind))
        det_ind = 1;
    end
    if (~exist('unit', 'var') || isempty(unit))
        unit = 'deg';
    end

    num_det = numel(det_ind);
    omega_step = zeros(num_det, 1);
    for ii_d = 1:num_det
        omega_step(ii_d) = get_step(parameters, det_ind(ii_d));
    end

    if (strcmpi(unit, 'rad'))
        omega_step = omega_step * pi / 180;
    end
end

function omega_step = get_step(parameters, det_ind)
    if (isfield(parameters.detgeo(det_ind), 'omstep'))
        omega_step = parameters.detgeo(det_ind).omstep;
    else
        if (numel(parameters.acq) > 1 || det_ind == 1)
            omega_step = 360 / gtAcqTotNumberOfImages(parameters, det_ind);
        else
            % This is the last resort! It should become more verbose in the
            % future, in case it happens!
            omega_step = 360 / gtAcqTotNumberOfImages(parameters, 1);
        end
    end
end