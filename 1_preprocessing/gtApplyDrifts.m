function gtApplyDrifts(~, ~, workingdir)
%
% gtApplyDrifts_raw(first, last, workingdir)
% new version _raw to act on images before Preprocessing.  
% drifts are saved in the parameters file
%
% adds a field to the edf header - istranslated=X
% where X is the number of times that the images has been drifted
% The field prep.correct_drift_iteration keeps track of this

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % get data from parameters
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    cd(workingdir)
    if exist('parameters.mat','file')
        parameters = [];
        load('parameters.mat')
    else
        gtError('Cannot find parameter file!')
    end
    imagedir = fullfile(parameters.acq.dir, '0_rawdata', parameters.acq.name);
    udrift = parameters.prep.udrift;
    vdrift = parameters.prep.vdrift;

    if isfield(parameters.prep, 'drifts_pad')
        pad = parameters.prep.drifts_pad;
    else
        pad = 'av';
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % database connect
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    gtDBConnect
    drifttable = [parameters.acq.name 'drifttable'];

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %   apply drifts routine
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    disp('Applying drifts to images...')
    counter = 0;
    % while loop for function timeout when function is finished
    while (counter < 100)
        found = false;

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %   get the list of files - .edfs only
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % source directory file list
        sfiles = dir(fullfile(imagedir, '*.edf'));
        %sort files by date order to copy in order
        sfdates = {sfiles.('datenum')};
        [~, indx] = sort(sfdates);

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %   try to apply drifts
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        for n = 1:length(sfiles)
            sfname = sfiles(indx(n)).name;

            %check if sfname has already been taken by another condor job
            test = false;
            try
                test = mym(sprintf('insert into %s (filename) values ("%s")', drifttable, sfname));
            catch Mexc
                gtPrintException(Mexc, 'Couldn''t insert in DB')
            end

            if test %no other job is copying this file, do copy
                fprintf('working on %s',sfname)

                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                % do drift here
                fnum = sfname;
                fnum = str2double(fnum(end-7:end-4));
                if ~isempty(fnum)
                    u=udrift(fnum+1);
                    v=vdrift(fnum+1);
                else %dark image
                    u=udrift(1);
                    v=vdrift(1);
                end
                try
                    test = checkAndShift(fullfile(imagedir, sfname), u, v, pad);

                    if test % keep track for time out
                        found = true;
                    end
                catch Mexc
                    gtPrintException(Mexc, 'in checking translation condition')
                end
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            end

        end %for loop over the file list, working in date order

        if (~found)
            counter = counter+1;
            pause(10) %have a rest, wait before looking again
            disp('sleeping 10, then look again')
        else
            counter = 0;
        end
    end
    disp('Timed out while waited for files to appear... quitting')


    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %   subfunction sfOverwrite
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    function was_translated = checkAndShift(fname, ushift, vshift, pad)
        [im, info] = edf_read(fname);
        % check image has not already been shifted
        if (isfield(info,'istranslated'))
            if ischar(info.istranslated)
                translated = str2double(info.istranslated);
            elseif isnumeric(info.istranslated)
                translated = info.istranslated;
            else
                Mexc = MException('TYPE:wrong_type_error', ...
                                  'The field ''istranslated'' should be ehither a string or a number');
                throw(Mexc)
            end
            if (translated >= parameters.prep.correct_drift_iteration)
                fprintf('Image has been shifted %d times already! Not touching this file', translated)
                was_translated = false;
                return
            else
                % increment istranslated in header (value hould be a string)
                info.istranslated = num2str(translated+1);
            end
        else
            info.istranslated='1';
        end
        % shift image
        im = gtShift(im, ushift, vshift, pad);  % gtShift(im,x(horizontal_right),y(vertical_downwards))
        edf_write(im, fname, info);
        was_translated = true;
    end
end % of function
