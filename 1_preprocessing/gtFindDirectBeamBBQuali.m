function bb=gtFindDirectBeamBBQuali(acq,calib)
%
% Gives approximate bounding box of direct beam to be used for
% calibration and to be refined as sample bounding box.
%
% 09/2007 Peter
%

refarea=127;
lim_directbeam=0.6;

disp(' ')
disp('Determining direct beam bounding box of qualis...')
disp(' ')

if strcmpi(acq.type,'180degree')
  totproj=acq.nproj;
elseif strcmpi(acq.type,'360degree');
  totproj=acq.nproj*2;
else
  disp('unknown scantype in gtFindDirectBeamBBQuali');
  return;
end

d=edf_read(sprintf('%s/dark.edf',calib.dir),[],'nodisp');

ref0=edf_read([calib.dir '/refHST0000.edf'],[],'nodisp')-d; % reference at beginning of scan
ref1=edf_read(sprintf('%s/refHST%04d.edf',calib.dir,calib.totproj),[],'nodisp')-d; % reference at end of scan

ref=min(ref0,ref1); % min of ref-s at beginning and end of scan

% calculate mean value m in central part of the image
m=gtImgMeanValue(ref(round(acq.ydet/2-refarea:acq.ydet/2+refarea+1),round(acq.xdet/2-refarea:acq.xdet/2+refarea+1)));
cen=double(ref>(lim_directbeam*m));  % should correspond to direct beam footprint

% find the direct beam region bb automatically (supposes fairly homogeneous
% beam profile - attention with ID11 data here... !

marker=zeros(acq.ydet,acq.xdet);
marker(round(acq.ydet/2),round(acq.xdet/2))=1;
cen=imreconstruct(marker,cen);
tmp=regionprops(cen,'BoundingBox');
bb=tmp.BoundingBox;

% Tighten bb until it's fully filled
bb=[ceil(bb(1:2)),bb(3:4)-1];
goon=true;
while goon
  cenbb=gtCrop(cen,bb);
  goon=false;
  
  if any(~cenbb(:,1)) % tighten bb on the left
    bb(1)=bb(1)+1;
    bb(3)=bb(3)-1;
    goon=true;
  end

  if any(~cenbb(:,end)) % tighten bb on the right
    bb(3)=bb(3)-1;
    goon=true;
  end

  cenbb=gtCrop(cen,bb);
  
  if any(~cenbb(1,:)) % tighten bb at the top
    bb(2)=bb(2)+1;
    bb(4)=bb(4)-1;
    goon=true;
  end

  if any(~cenbb(end,:)) % tighten bb at the bottom
    bb(4)=bb(4)-1;
    goon=true;
  end
end

% Loosen bb if it can be on any side
goon=true;
while goon
  goon=false;
  
  tmpbb=bb+[-1 0 1 0]; % loose bb on the left
  tmpim=gtCrop(cen,tmpbb);
  if all(tmpim(:)) 
    bb=tmpbb;
    goon=true;
  end

  tmpbb=bb+[0 0 1 0]; % loose bb on the right
  tmpim=gtCrop(cen,tmpbb);
  if all(tmpim(:)) 
    bb=tmpbb;
    goon=true;
  end

  tmpbb=bb+[0 -1 0 1]; % loose bb at the top
  tmpim=gtCrop(cen,tmpbb);
  if all(tmpim(:)) 
    bb=tmpbb;
    goon=true;
  end

  tmpbb=bb+[0 0 0 1]; % loose bb at the bottom
  tmpim=gtCrop(cen,tmpbb);
  if all(tmpim(:)) 
    bb=tmpbb;
    goon=true;
  end
end


% make sure the bbox width and height are even numbers (needed for correlate1, so probably not for qualis)
bb(3:4)=ceil(bb(3:4)/2)*2;

bbcenter_abs=(2*bb(1)+bb(3)-1)/2;

% plot bbox found, its center-line and center-line of image
figure('name','Direct beam bounding box')

wdefault=warning;
warning('off','all');

imshow(ref,[])

warning(wdefault);

hold on
plot([bb(1),bb(1)+bb(3)-1,bb(1)+bb(3)-1,bb(1),bb(1)],[bb(2),bb(2),bb(2)+bb(4)-1,bb(2)+bb(4)-1,bb(2)],'r-')
plot([acq.xdet/2+0.5 acq.xdet/2+0.5],[1 acq.ydet],'b-.')
plot([bbcenter_abs bbcenter_abs],[bb(2) bb(2)+bb(4)-1],'c-.')

%bboxmin=min(abs(acq.xdet/2-bbox(1)),abs(acq.xdet/2-(bbox(1)+bbox(3)-1)));
%bboxmin(2)=bbox(2);



end % of function
