function outputdir = gtSetAnalysisDir(inputdir)

dir = [];
strcell = regexp(inputdir,filesep(),'split');
strcell(find(cellfun(@(x) strcmp(x,'0_rawdata'),strcell)))=[];
strcell(find(cellfun(@(x) strcmp(x,'Orig'),strcell)))=[];
    
if any(cellfun(@(x) strcmp(x,'_data_visitor'),strcell))
    % regular id11 visitor experiment
    ind = find(cellfun(@(x) strcmp(x,'_data_visitor'),strcell));
    if any(cellfun(@(x) strcmp(x,'id11'),strcell))
        ind1 = find(cellfun(@(x) strcmp(x,'id11'),strcell));
    elseif any(cellfun(@(x) strcmp(x,'bm05'),strcell))
        ind1 = find(cellfun(@(x) strcmp(x,'bm05'),strcell));   
    elseif any(cellfun(@(x) ~isempty(strfind(x,'id')),strcell))
        ind1 = find(cellfun(@(x) ~isempty(strfind(x,'id')),strcell));
    end
    ind = ind : ind1;
    ind2 = find(cellfun(@(x) ~isempty(strfind(x,'DCT_Analysis')),strcell));
    ind3 = find(cellfun(@(x) ~isempty(strfind(x,gtGetLastDirName(inputdir))),strcell));
elseif any(cellfun(@(x) ~isempty(strfind(x,'graintracking')),strcell)) 
    % graintracking experiment
    ind  = find(cellfun(@(x) ~isempty(strfind(x,'graintracking')),strcell));
    ind2 = find(cellfun(@(x) ~isempty(strfind(x,'DCT_Analysis')),strcell));
    ind3 = find(cellfun(@(x) ~isempty(strfind(x,gtGetLastDirName(inputdir))),strcell));
elseif any(cellfun(@(x) ~isempty(strfind(x,'inhouse')),strcell)) 
    % graintracking experiment
    ind  = find(cellfun(@(x) ~isempty(strfind(x,'inhouse')),strcell));
    ind2 = find(cellfun(@(x) ~isempty(strfind(x,'DCT_Analysis')),strcell));
    ind3 = find(cellfun(@(x) ~isempty(strfind(x,gtGetLastDirName(inputdir))),strcell));
else
    % default to ID11/graintracking
    tmp = fullfile('/data','id11','graintracking','DCT_Analysis',gtGetLastDirName(inputdir));
    dir = tmp;
end

if isempty(dir)
    if ~isempty(ind)
        strcell{ind(1)} = strrep(strcell{ind(1)},'_',filesep());
   
        dir = arrayfun(@(num) [strcell{num} filesep], ind, 'UniformOutput', false);
        dir = [dir{:}];
    end
    tmp=dir;
    if ~isempty(ind2)
        ind = ind : ind2;
    elseif ~isempty(ind3)
        if ind3 ~= length(strcell)
            tmp=fullfile(tmp,'DCT_Analysis');
            ind = [];
        else
            for ii=ind(end)+1:length(strcell)-1;
                tmp=fullfile(tmp,strcell{ii});
            end
            tmp=fullfile(tmp,'DCT_Analysis');
        end
        tmp = fullfile(tmp,strcell{ind3});
    else
        tmp = fullfile(tmp,gtGetLastDirName(inputdir));
    end
    dir=tmp;
end

outputdir = dir;

end
