function par_acq = gtAcqDefaultParameters(par_acq)
% GTACQDEFAULTPARAMETERS  Set the acuisition parameters not read from the
%                         xml file
%     par_acq = gtAcqDefaultParameters(par_acq)
%     -----------------------------------------
%
%     Version 001 15-05-2012 by LNervo
%       Split from gtLoadAcquisitionXML()

%     is_dct = ismember(lower(par_acq.type), {'360degree', '180degree', 'dct'});
    is_dct_360 = ismember(lower(par_acq.type), {'360degree', 'dct'});

    par_acq.true_detsizeu = 2048;
    par_acq.true_detsizev = 2048;

    % interlacing parameters
    % try to guess whether the scan is interlaced
    if (is_dct_360 && exist([par_acq.name '0_0000.edf'], 'file'))
        par_acq.interlaced_turns = 1; % assume one extra turn is reasonable guess
        disp('This looks like an interlaced scan! Doubling nproj...')
        par_acq.nproj            = par_acq.nproj * 2;
    else
        par_acq.interlaced_turns = 0;
    end

    % mono tuned during scan? If yes, put the number of ref groups
    par_acq.mono_tune = 0;
    % if we have a horizontal rotation axis we turn the images during the copying process
    par_acq.rotation_axis = 'vertical';
    par_acq.distortion = 'none';

    %   deal with flips and rotations
    % subsequently, deal with flips and rotations, scans without direct beam
    par_acq.flip_images = false;
    par_acq.no_direct_beam = false;

    % rotate images to the gt coordinate system !!! Help !!!
    par_acq.rotation_direction = 'clockwise';
    par_acq.detector_definition = 'inline';

    % Images will be shifted in gtCreateFullLive to have rotation axis in center...
    par_acq.rotu = par_acq.xdet / 2 + 0.5;
    par_acq.rotx = par_acq.rotu;
    par_acq.maxradius = max(par_acq.xdet, par_acq.ydet) / 2 * sqrt(2);
    par_acq.beamchroma = 'mono';

    % if it is 360degree data, we need a pair table
    if (is_dct_360)
        % pair tablename can be derived from the scan name
        par_acq.pair_tablename  = [par_acq.name 'spotpairs'];
        par_acq.calib_tablename = [par_acq.name 'paircalib'];
    end
end
