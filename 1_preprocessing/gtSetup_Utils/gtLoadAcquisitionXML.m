function acq = gtLoadAcquisitionXML(xmlfname, interactive, verbose)
% FUNCTION gtLoadAcquisitionXML  Try to read the .xml file that may be present
%     params_acq = gtLoadAcquisitionXML(xmlfname, interactive)
%     --------------------------------------------------------
%
%     Version 002 15/05/2012 by LNervo
%       Split acq parameters from parameters read from xml file

    if (~exist('verbose', 'var') || isempty(verbose))
        verbose = true;
    end
    % try to read the .xml file that may be present
    acq = [];
    
    if exist(xmlfname,'file')
        if (verbose)
            disp('Reading parameters from xml file');
        end
        tmpxml = xml_read(xmlfname);
        tmpxml = tmpxml.acquisition;

        acq.name          = tmpxml.scanName; % shouldn't change!
        acq.date          = tmpxml.date;
        acq.xdet          = tmpxml.projectionSize.DIM_1; % pixels
        acq.ydet          = tmpxml.projectionSize.DIM_2; % pixels
        acq.nproj         = tmpxml.tomo_N / 2;
        acq.refon         = tmpxml.ref_On;
        acq.nref          = tmpxml.ref_N;
        acq.ndark         = tmpxml.dark_N;
        acq.pixelsize     = tmpxml.pixelSize / 1000; % mm
        acq.count_time    = tmpxml.ccdtime;
        acq.energy        = tmpxml.energy; % keV
        acq.dist          = tmpxml.distance; % mm
        acq.sensortype    = tmpxml.cameraName;
        if (isfield(tmpxml, 'scanRange'))
            acq.type      = [num2str(tmpxml.scanRange) 'degree'];
        else
            warning('Could not find scanRange info in xml file, assuming it is 360degree...');
            acq.type      = '360degree';
        end
        acq.detroi_v_off  = tmpxml.projectionSize.ROW_BEG;
        acq.detroi_u_off  = tmpxml.projectionSize.COL_BEG;

        motors_xml = tmpxml.listMotors.motor;
        for ii_m = 1:numel(motors_xml)
            m = motors_xml(ii_m);
            val = m.motorPosition.CONTENT;
            unit = m.motorPosition.ATTRIBUTE.unit;
            acq.motors.(m.motorName) = convert_to_mm_or_deg(val, unit);
        end
    else %if .xml not found
        if interactive
            disp('No .xml file found!');
            disp('-> Please check that you are in the acquistion directory.');
            check = inputwdefault('Do you want to proceed without the xml file? [y/n]', 'n');
            if strcmpi(check, 'n')
                gtError('XML:no_xml_file', ...
                    'There is no .xml file and the user chose to quit');
            end
        end
        % guess some default values
        acq.date          = date();
        acq.xdet          = 2048;
        acq.ydet          = 2048;
        acq.nproj         = 7200;
        acq.refon         = 200;
        acq.nref          = 5;
        acq.ndark         = 11;
        acq.pixelsize     = 0.0014;
        acq.count_time    = 2;
        acq.energy        = 20;
        acq.dist          = 5;
        acq.sensortype    = 'frelon';
        acq.type          = '360degree';
        acq.detroi_u_off  = 0;
        acq.detroi_v_off  = 0;
    end
end

function x = convert_to_mm_or_deg(x, unit)
    switch(lower(unit))
        case 'm'
            x = 1e3 * x;
        case {'mm', 'deg'}
        case 'um'
            x = 1e-3 * x;
        case 'nm'
            x = 1e-6 * x;
        case 'rad'
            x = x * 180 / pi;
    end
end

