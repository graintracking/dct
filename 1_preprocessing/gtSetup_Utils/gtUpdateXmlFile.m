function [xml_name, xml] = gtUpdateXmlFile()
% GTUPDATEXMLFILE  Update existing .xml file adding info from images
%                  headers.
%     [xml_name, xml] = gtUpdateXmlFile()
%     -----------------------------------
%     It has to be run in the acquisition directory. 
%     Looks for darkend0000.edf, ref0000_0000.edf, [dataset_name].xml
%
%     OUTPUT:
%       xml_name = name of the new created file .xml
%       xml      = updated xml structure from xml file
%

msg = [];

% get dataset_name and get 
dataset_name = gtGetLastDirName(pwd());

% load header from files
xml_file  = [dataset_name '.xml'];
dark_file = 'darkend0000.edf';
ref_file  = 'ref0000_0000.edf';

if exist(xml_file,'file') 
    xml = xml_read(xml_file);
    xml.acquisition.scanName = dataset_name;
    
    if exist(dark_file,'file')
        info_dark = edf_info(dark_file);
        if all(isfield(info_dark,{'byteorder','suffix'}))
            xml.acquisition.byteorder   = info_dark.byteorder;
            xml.acquisition.imageSuffix = info_dark.suffix;
        else % if don't exist, define them by default
            xml.acquisition.byteorder   = 'l';
            xml.acquisition.imageSuffix = '.edf';
            msg = 'Setting byteorder and imageSuffix by default.';
        end
    else
        msg = 'Image darkend0000.edf not found.';
    end
    xml.acquisition.collection_dir_old = fullfile(xml.acquisition.disk,xml.acquisition.scanName);
    
    if exist(ref_file,'file')
        info_ref  = edf_info(ref_file);
        if all(isfield(info_ref,{'energy','optic_used'}))
            xml.acquisition.energy     = info_ref.energy;
            xml.acquisition.opticsUsed = info_ref.optic_used;
        else
            msg = 'Energy and optic_used information have not been found in the header.';
        end
    else
        msg = 'Image ref0000_0000.edf not found.';
    end
    
    % saving the updated xml file
    newname=strrep(xml_file,'.xml','dct_.xml');
    xml_write(newname,xml,'tomodb2');
    disp(['Saved updated .xml file in ' fullfile(pwd(),newname)])
	disp(' ')
    if nargout > 0
        xml_name = newname;
    end
else
    msg = 'File .xml not found';
    if nargout > 0
        xml_name = [];
        xml      = [];
    end
end
disp(msg);

end % end of function