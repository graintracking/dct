function par_prep = gtPrepDefaultParameters(acq)
% par_prep = gtPrepDefaultParameters(acq)

if (acq.no_direct_beam)
    % if no direct beam, don't normalise images
    par_prep.normalisation = 'none';
else
    par_prep.normalisation = 'none';
% 'margin' may mistakenly change the intensity because the sample
% overlapped with the monitored area
%     par_prep.normalisation = 'margin';
end

% Andy 26/4/2011 - problem with a scan with 1440/720 images when using default
% values.
% Must check that fullint and absint values work.  n x fullint and m x absint
% should equal total number of images.   I think it should also equal the
% number of images per turn for an interlaced scan.
if strcmp(acq.type, '360degree')
    % images per turn in an interlaced scan
    totproj = 2*acq.nproj;
elseif strcmp(acq.type, '180degree')
    totproj = acq.nproj;
else
    error('ACQ:invalid_parameter', ...
        'Unknown type of scan: "%s". Check parameters.acq.type!', acq.type);
end
tproj = totproj / (acq.interlaced_turns+1);

test = mod(tproj, 1:100);
test = find(test == 0); % which numbers work?
% get the working numbers closest to 10 and 50 respectively
test10 = abs(test-10); % difference from 10
[~, ind10] = min(test10);
test50 = abs(test-50); % difference from 50
[~, ind50] = min(test50);

par_prep.absint     = test(ind10);
par_prep.absrange   = 10*par_prep.absint;
par_prep.fullint    = test(ind50);
par_prep.fullrange  = 10*par_prep.fullint;
par_prep.margin     = 5;
par_prep.intensity  = 1800;
par_prep.filtsize   = [3 3];
par_prep.drifts_pad = 'av';
par_prep.renumbered = false;


par_prep.udrift        = zeros(1, totproj);
par_prep.vdrift        = zeros(1, totproj);
par_prep.udriftabs     = zeros(1, totproj);

if (~acq.no_direct_beam)
    par_prep.correct_drift = 'not_required';
end

% by default using same bbox for acq
par_prep.bbox = acq.bb;

% total number of images
par_prep.totproj = totproj;

end
