function gtSetupCalibration(bbdir, parameters)
% GTSETUPCALIBRATION  Sets up calibration data and drifts.
%     gtSetupCalibration(bbdir)
%     -------------------------
%
%     warning limit of inconsistency of the calibration method in pixels:
%     (difference between total horizontal sample drift during scan and
%     rotation axis drift from calibration to the end of scan)
%
%     allow user to input working directory, if data is being collected in one
%     place but will be copied to another.

lim_incon = 0.05;

if (~exist('parameters', 'var') || isempty(parameters))
    parameters = gtLoadParameters();
end

close all

% Calibration scan parameters

disp('Type of calibration scan?');
parameters.acq.calibtype = inputwdefault('''180degree'', ''360degree'' or ''none''','360degree');

if strcmpi(parameters.acq.calibtype,'180degree') || strcmpi(parameters.acq.calibtype,'360degree')
    parameters.acq.caliboffset = inputwdefault('Image number in scan where corresponding calibration image is #0000 (positive or negative offset)','0');
    parameters.acq.caliboffset = str2double(parameters.acq.caliboffset);
    parameters.acq.calibration_path = input('Enter absolute path of calibration scan: ','s');

    % Create dark.edf file of the calibration (no refHST-s for calib)
    disp(' ')
    disp('Creating dark.edf image of calibration scan')

    calibname = parameters.acq.calibration_path(find(parameters.acq.calibration_path=='/',1,'last')+1:end);
    xmlfname_calib = fullfile(parameters.acq.calibration_path, [calibname '.xml']);
    if ~exist(xmlfname_calib,'file')
        Mexc = MException('ACQ:no_xml_file', ...
                          ['Cannot proceed - missing xml file of calibration scan: ' xmlfname_calib]);
        throw(Mexc)
    end

    xmlPrefs = [];
    xmlPrefs.Str2Num = 'never';
    xml_calib = xml_read(xmlfname_calib, xmlPrefs);
    xml_calib = xml_calib.acquisition;
    ndark_calib = str2double(xml_calib.dark_N);

    dark_name = fullfile(parameters.acq.calibration_path, 'dark.edf');
    if (~exist(dark_name, 'file'))
        darkend_name = fullfile(parameters.acq.calibration_path, 'darkend0000.edf');
        [dark, info] = edf_read(darkend_name);
        dark = dark / ndark_calib;
        edf_write(dark, dark_name, info);
        disp(' ')
    end
end

disp('In which dimension would you like to compute sample drifts?')
disp('( vertical=1, horizontal=2, both=0 ) ')
parameters.acq.correlationdim = inputwdefault('dimension of correlation:','0');
parameters.acq.correlationdim = str2double(parameters.acq.correlationdim);



% direct beam bb
%bbdir=gtFindDirectBeamBB(parameters.acq);
%bbdir=parameters.acq.bb;

[ydrift_end,zdrift_end,rotaxisloc_abs_end,rotoff_abs_end,rotoff_rel_end]=gtDriftsEndOfScan(parameters.acq,bbdir);

if (any(strcmpi(parameters.acq.calibtype, {'180degree', '360degree'})))

    [ydrift_pol_calib,zdrift_pol_calib,ydrift_int_calib,zdrift_int_calib,rotaxisloc_abs_calib,rotoff_abs_calib,rotoff_rel_calib,bbs_calib] ...
        = gtEvaluateCalibrationScan(parameters.acq, bbdir);

    totproj = gtAcqTotNumberOfImages(parameters);

    % Inconsistency of calibration: when there is a sample drift during the scan,
    % the rotation axis location is determined from ... ??? ....
    % drifted images at the end of scan, 'rotoff_abs_end' and rotoff_
    incon = abs((ydrift_end(totproj+1)-ydrift_end(1))+(rotoff_abs_end-rotoff_abs_calib));

    if (incon > lim_incon)
        disp('WARNING! Inconsistency of calibration method is above the preset limit.')
    end

    format short g

    disp(' ')
    disp('-----------------------------------------------------------')
    disp(' ')
    disp('IMAGES AT END  and  CALIBRATION  comparison:')
    disp('Maximum absolute value of horizontal sample drift: ')
    disp([max(abs(ydrift_end)) max(abs(ydrift_int_calib))])
    disp('Mean value of horizontal sample drift: ')
    disp([mean(ydrift_end) mean(ydrift_int_calib)])
    disp('Maximum value of vertical sample drift: ')
    disp([max(zdrift_end) max(zdrift_int_calib)])
    disp('Mean value of vertical sample drift: ')
    disp([mean(zdrift_end) mean(zdrift_int_calib)])
    disp('Rotation axis offset relative to center of direct beam bb:')
    disp([rotoff_rel_end rotoff_rel_calib])
    disp('Rotation axis offset relative to center of full images:')
    disp([rotoff_abs_end rotoff_abs_calib])
    disp('Rotation axis location in full images:')
    disp([rotaxisloc_abs_end rotaxisloc_abs_calib])
    disp('Inconsistency of calibration:')
    disp(incon)
    disp(' ')
    disp('-----------------------------------------------------------')
    disp(' ')

    figure('name','Sample drifts according to images at end and calibration')
    subplot(2,1,1)
    title('horizontal drift');
    hold on
    plot(ydrift_end,'b.')
    plot(ydrift_pol_calib,'g.')
    plot(ydrift_int_calib,'k-')

    subplot(2,1,2)
    title('vertical drift');
    hold on
    plot(zdrift_end,'b.')
    plot(zdrift_pol_calib,'g.')
    plot(zdrift_int_calib,'k-')


    disp('On what would you like to base sample drifts computation?')
    disp('    1. polynomial fit from calibration scan')
    disp('    2. interpolated values from calibration scan')
    disp('    3. interpolated values from images at end of scan')
    disp('    4. average 1. and 3. ?')
    parameters.acq.driftsbase = inputwdefault(' Drifts based on','1');
    parameters.acq.driftsbase = str2double(parameters.acq.driftsbase);
else
    disp(' ')
    disp('Sample drifts are computed on the basis of images at end of scan.')
    disp(' ')
    parameters.acq.driftsbase = 3;
end

switch (parameters.acq.driftsbase)
    case 1
        parameters.acq.driftsbase = 'calib_pol';
        ydrift = ydrift_pol_calib;
        zdrift = zdrift_pol_calib;
    case 2
        parameters.acq.driftsbase = 'calib_int';
        ydrift = ydrift_int_calib;
        zdrift = zdrift_int_calib;
    case 3
        parameters.acq.driftsbase = 'images_at_end';
        ydrift = ydrift_end;
        zdrift = zdrift_end;
    case 4
        parameters.acq.driftsbase = 'average';
        ydrift = (ydrift_pol_calib+ydrift_end)/2;
        zdrift = (zdrift_pol_calib+zdrift_end)/2;
end

parameters.prep.ydrift = ydrift;
parameters.prep.zdrift = zdrift;

gtSaveParameters(parameters)

disp('Calibration data and drifts saved in parameters file.')
end





