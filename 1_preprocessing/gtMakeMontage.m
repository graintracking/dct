

function gtMakeMontage

name='ss10j_'
lims=[0.1 1];
bbox=[800 850 450 350];

dark=edf_read('dark.edf',bbox);


im=edf_read(sprintf('%s0000.edf',name),bbox);
ref=edf_read('refHST0000.edf',bbox);
im=(im-dark)./(ref-dark);
subplot(2,4,1);imshow(im,lims);

im=edf_read(sprintf('%s0900.edf',name),bbox);
ref=edf_read('refHST0900.edf',bbox);
im=(im-dark)./(ref-dark);
subplot(2,4,2);imshow(im,lims);

im=edf_read(sprintf('%s1800.edf',name),bbox);
ref=edf_read('refHST1800.edf',bbox);
im=(im-dark)./(ref-dark);
subplot(2,4,3);imshow(im,lims);

im=edf_read(sprintf('%s2700.edf',name),bbox);
ref=edf_read('refHST2700.edf',bbox);
im=(im-dark)./(ref-dark);
subplot(2,4,4);imshow(im,lims);

im=edf_read(sprintf('%s3599.edf',name),bbox);
ref=edf_read('refHST3600.edf',bbox);
im=(im-dark)./(ref-dark);
subplot(2,4,5);imshow(im,lims);


im=edf_read(sprintf('%s3600.edf',name),bbox);
ref=edf_read('refHST3600.edf',bbox);
im=(im-dark)./(ref-dark);
subplot(2,4,6);imshow(im,lims);

im=edf_read(sprintf('%s3601.edf',name),bbox);
ref=edf_read('refHST3600.edf',bbox);
im=(im-dark)./(ref-dark);
subplot(2,4,7);imshow(im,lims);

im=edf_read(sprintf('%s3602.edf',name),bbox);
ref=edf_read('refHST3600.edf',bbox);
im=(im-dark)./(ref-dark);
subplot(2,4,8);imshow(im,lims);