function gtCopyCorrectUndistortWrapper(parameters)
% GTCOPYCORRECTUNDISTORTWRAPPER  Calculates the number of OAR jobs to launch
%     gtCopyCorrectUndistortWrapper(parameters)
%     -----------------------------------------
%       It creates the table used to coordinate the jobs running in parallel
%
%     Version 002 09-11-2011 by LNervo
%       Add comments and clean function
%
%     Version 001 by AKing
%       It also copies certain files needed early in the preprocessing stage, so
%       they are available for live processing
%
%       Modify to use OAR - try to calculate reasonable number of jobs and wall
%       time.
%
%
%
%     FUNCTIONS SUBMITTED WITH OAR:
%[oar]- gtCopyCorrectUndistortCondor

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% setup database table
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
gtDBConnect();
filetable = [parameters.acq.name '_filetable'];
mym(['drop table if exists ' filetable]);
mym(['create table if not exists ' filetable ' (filename text, unique key (filename(40)))']);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% calculate how many jobs to launch
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
source_dir = parameters.acq.collection_dir;
dest_dir   = fullfile(parameters.acq.dir, '0_rawdata', parameters.acq.name);

src_files = dir(fullfile(source_dir, '*'));
copied_files = dir(fullfile(dest_dir, '*'));

% go through the source list, remove directories, parameters.mat, and anything
% else already copied
filter_names = [{copied_files.('name')}, 'parameters.mat'];

files_to_keep = ~ismember({src_files(:).name}, filter_names);
files_to_keep = files_to_keep & ~[src_files(:).isdir];
src_files = src_files(files_to_keep);

%count files in source dir, nfiles
nfiles = length(src_files);

% how many files should this dataset contain?
totproj = gtAcqTotNumberOfImages(parameters);
tot_edf_files = totproj + parameters.acq.ndark ...
    + ( ( (totproj / parameters.acq.refon) + 1) * parameters.acq.nref);
fprintf('Number %d files to be copied...\n', tot_edf_files)

if isfield(parameters.acq, 'count_time')
    tottime = tot_edf_files * (parameters.acq.count_time+0.4) * 1.3; %1.3 safety factor
    % worst case E2V readout time
else
    tottime = tot_edf_files * 5;
end
% for OAR - want either several jobs for a short time, or a few jobs for
% the time taken to acquire the dataset
if (isfield(parameters.acq, 'online') && parameters.acq.online)
    % running - need to run until end of scan
    OARjobs = 4;
    OARtime = max(tottime, 3*3600);
else
    % finished
    OARjobs = ceil(tot_edf_files / 500);
    OARtime = (tot_edf_files / OARjobs) * 3; % guess 3 seconds per job
end
OARtmp   = ceil(OARtime / 60); % minutes
OARm     = mod(OARtmp, 60);  % minutes
OARh     = (OARtmp - OARm) / 60; % hours
walltime = sprintf('%02d:%02d:00', OARh, OARm);
fprintf('Number of OAR jobs: %d, walltime to %s hours...\n', OARjobs, walltime);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% launch OAR jobs
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% gtOarLaunch(executable, first, last, njobs, otherparameters, submitflag, varargin)
gtOarLaunch('gtCopyCorrectUndistortCondor', 0, tot_edf_files, OARjobs, ...
    parameters.acq.dir, true, 'walltime', walltime);

end % end function
