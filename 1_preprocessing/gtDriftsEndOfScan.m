function [ydrift, zdrift, varargout] = gtDriftsEndOfScan(acq, bbdir)
% FUNCTION [ydrift, zdrift, varargout] = gtDriftsEndOfScan(acq, bbdir)
% Treats images taken as references at end of scan (cropped in bbdir),
% determines rotation axis location at sub-pixel and interpolates sample
% drifts (thus provides a rough reference).
%
% Uses ref0000_xxxx-s for flat field correction.
%
% INPUT
% acq=acquisition parameters
% bbdir=direct beam bb according to GT conventions [originx, originy, width, height]
% bbdir doesn't need to be centered in full image
% if acq.correlationdim = 1 bbdir(4) needs to be even number (for correlate1)
% if acq.correlationdim = 2 bbdir(3) needs to be even number (for correlate1)
%
% OUTPUT
% ydrift, zdrift=direct values with which the images need to be translated
% (e.g direct input for interpolatef)
% varargout{1}=rotaxisloc_abs;rot axis horizontal location in full image
% (non-integer in pixels)
% varargout{2}=rotoff_abs;rot axis offset in full image; vector pointing
% from center of image to rotation axis location;
% positive to the right
% varargout{3}=rotoff;rot axis offset in bbdir; vector pointing from center
% of bbdir to rotation axis location in bbdir, positive to the right
%
% 07/2007 Wolfgang, Peter
%

frame = 20; % for bb extension when translating images

if strcmpi(acq.type, '180degree')
    totproj = acq.nproj;
elseif strcmpi(acq.type, '360degree')
    totproj = acq.nproj * 2;
else
    gtError('Unknown type of scan. Check acq.type!');
end

dim = acq.correlationdim;
if (isempty(dim))
    dim = 0;
end

d = edf_read(sprintf('0_rawdata/%s/dark.edf', acq.name), [], 'nodisp');

refend = edf_read(sprintf('0_rawdata/%s/ref%04d_%04d.edf', acq.name, acq.nref-1, totproj), [], 'nodisp')-d;

name = sprintf('0_rawdata/%s/%s%04d.edf', acq.name, acq.name, totproj);
im0 = (edf_read(name, [], 'nodisp')-d)./refend;

name = sprintf('0_rawdata/%s/%s%04d.edf', acq.name, acq.name, totproj+2);
im2 = (edf_read(name, [], 'nodisp')-d)./refend;

a0 = gtCrop(im0, bbdir);
a2 = fliplr(gtCrop(im2, bbdir));

% Calculate the double of rotation axis offset from center of bbdir
rotoff1 = median(correlate1(a0', a2'));

% double check this result and modify if necessary byuser intervention...
a2c = interpolatef(a2, 0, rotoff1);

shift = [];
assignin('base', 'shift', shift);
disp(' ')
disp('Images at end: check result of image correlation with findshifts...')
disp(' ')

gtFindShifts(a0, a2c);
set(gcf, 'name', 'Images at end: check correlation...')
while isempty(shift)
    drawnow;
    shift = evalin('base', 'shift');
end

shift = evalin('base', 'shift');
rotoff1 = shift(2) / 2 + rotoff1 / 2;
rotoff2 = rotoff1;

close(gcf)

if strcmpi(acq.type, '360degree')
    name = sprintf('0_rawdata/%s/%s%04d.edf', acq.name, acq.name, totproj+1);
    im1 = (edf_read(name, [], 'nodisp')-d) ./ refend;
    name = sprintf('0_rawdata/%s/%s%04d.edf', acq.name, acq.name, totproj+3);
    im3 = (edf_read(name, [], 'nodisp')-d) ./ refend;
    
    a1 = gtCrop(im1, bbdir);
    a3 = fliplr(gtCrop(im3, bbdir));
    
    rotoff2 = median(correlate1(a1', a3'));
    a3c = interpolatef(a3, 0, rotoff2);
    
    shift = [];
    assignin('base', 'shift', shift);
    disp(' ')
    disp('Images at end: check result of image correlation with findshifts...')
    disp(' ')
    
    gtFindShifts(a1, a3c);
    set(gcf, 'name', 'Images at end: check correlation...')
    while isempty(shift)
        drawnow;
        shift = evalin('base', 'shift');
    end
    shift = evalin('base', 'shift');
    rotoff2 = shift(2) / 2 + rotoff2 / 2;
    close(gcf)
end

rotoff = (rotoff1 + rotoff2) / 2;

detcenter_abs = acq.xdet / 2 + 0.5;
bbdircenter_abs = (2 * bbdir(1) + bbdir(3) - 1) / 2;

rotoff_abs = bbdircenter_abs - detcenter_abs + rotoff;
rotaxisloc_abs = bbdircenter_abs + rotoff;

% acq.rotx = bbdir(1)+bbdir(3)/2+offset; % check if we should add + 0.5 here
% or not:(does the pixel 1 start at 0 or at 0.5 ?)


% Now check if there was a sample drift during the scan and correct for it
% for the moment we suppose a 360 degree scan with 4 images at the end...


bbread = [bbdir(1)-frame, bbdir(2)-frame, bbdir(3)+2*frame, bbdir(4)+2*frame];
bbor = [frame+1, frame+1, bbdir(3), bbdir(4)];

if (strcmpi(acq.type, '360degree'))
    name = sprintf('0_rawdata/%s/dark.edf', acq.name);
    d = edf_read(name, bbread, 'nodisp');

    name = sprintf('0_rawdata/%s/ref%04d_%04d.edf', acq.name, acq.nref-1, totproj); % last ref at end of scan
    refend = edf_read(name, bbread, 'nodisp')-d;

    name = sprintf('0_rawdata/%s/%s%04d.edf', acq.name, acq.name, totproj);
    im_end_360 = (edf_read(name, bbread, 'nodisp')-d)./refend;
    im_end_360 = interpolatef(im_end_360, 0, -rotoff);
    im_end_360 = gtCrop(im_end_360, bbor);

    name = sprintf('0_rawdata/%s/%s%04d.edf', acq.name, acq.name, totproj+1);
    im_end_270 = (edf_read(name, bbread, 'nodisp')-d)./refend;
    im_end_270 = interpolatef(im_end_270, 0, -rotoff);
    im_end_270 = gtCrop(im_end_270, bbor);

    name = sprintf('0_rawdata/%s/%s%04d.edf', acq.name, acq.name, totproj+2);
    im_end_180 = (edf_read(name, bbread, 'nodisp')-d)./refend;
    im_end_180 = interpolatef(im_end_180, 0, -rotoff);
    im_end_180 = gtCrop(im_end_180, bbor);

    name = sprintf('0_rawdata/%s/%s%04d.edf', acq.name, acq.name, totproj+3);
    im_end_90 = (edf_read(name, bbread, 'nodisp')-d)./refend;
    im_end_90 = interpolatef(im_end_90, 0, -rotoff);
    im_end_90 = gtCrop(im_end_90, bbor);

    name = sprintf('0_rawdata/%s/%s%04d.edf', acq.name, acq.name, totproj+4);
    im_end_0 = (edf_read(name, bbread, 'nodisp')-d)./refend;
    im_end_0 = interpolatef(im_end_0, 0, -rotoff);
    im_end_0 = gtCrop(im_end_0, bbor);


    name = sprintf('0_rawdata/%s/dark.edf', acq.name);
    d = edf_read(name, bbdir, 'nodisp');

    name = sprintf('0_rawdata/%s/ref%04d_%04d.edf', acq.name, acq.nref-1, totproj);
    ref360 = edf_read(name, bbdir, 'nodisp')-d;
    name = sprintf('0_rawdata/%s/%s%04d.edf', acq.name, acq.name, totproj);
    im_360 = (edf_read(name, bbdir, 'nodisp')-d)./ref360;

    name = sprintf('0_rawdata/%s/ref%04d_%04d.edf', acq.name, acq.nref-1, totproj*0.75);
    ref270 = edf_read(name, bbdir, 'nodisp')-d;
    name = sprintf('0_rawdata/%s/%s%04d.edf', acq.name, acq.name, totproj*0.75);
    im_270 = (edf_read(name, bbdir, 'nodisp')-d)./ref270;

    name = sprintf('0_rawdata/%s/ref%04d_%04d.edf', acq.name, acq.nref-1, totproj*0.5);
    ref180 = edf_read(name, bbdir, 'nodisp')-d;
    name = sprintf('0_rawdata/%s/%s%04d.edf', acq.name, acq.name, totproj*0.5);
    im_180 = (edf_read(name, bbdir, 'nodisp')-d)./ref180;

    name = sprintf('0_rawdata/%s/ref%04d_%04d.edf', acq.name, acq.nref-1, totproj*0.25);
    ref90 = edf_read(name, bbdir, 'nodisp')-d;
    name = sprintf('0_rawdata/%s/%s%04d.edf', acq.name, acq.name, totproj*0.25);
    im_90 = (edf_read(name, bbdir, 'nodisp')-d)./ref90;

    name = sprintf('0_rawdata/%s/ref%04d_%04d.edf', acq.name, acq.nref-1, 0);
    ref0 = edf_read(name, bbdir, 'nodisp')-d;
    name = sprintf('0_rawdata/%s/%s%04d.edf', acq.name, acq.name, 0);
    im_0 = (edf_read(name, bbdir, 'nodisp')-d)./ref0;

    switch (dim)
        case 0
            corr_0 = correlate(im_end_0, im_0);
            corr_90 = correlate(im_end_90, im_90);
            corr_180 = correlate(im_end_180, im_180);
            corr_270 = correlate(im_end_270, im_270);
            corr_360 = correlate(im_end_360, im_360);
        case 1
            corr_0 = [median(correlate1(im_end_0, im_0)), 0];
            corr_90 = [median(correlate1(im_end_90, im_90)), 0];
            corr_180 = [median(correlate1(im_end_180, im_180)), 0];
            corr_270 = [median(correlate1(im_end_270, im_270)), 0];
            corr_360 = [median(correlate1(im_end_360, im_360)), 0];
        case 2
            corr_0 = [0, median(correlate1(im_end_0', im_0'))];
            corr_90 = [0, median(correlate1(im_end_90', im_90'))];
            corr_180 = [0, median(correlate1(im_end_180', im_180'))];
            corr_270 = [0, median(correlate1(im_end_270', im_270'))];
            corr_360 = [0, median(correlate1(im_end_360', im_360'))];
        otherwise
            error('gtDriftsEndOfScan:wrong_argument', ...
                'Dimension (second input argument) should be [], 0, 1 or 2!')
    end

    check_drift = true;

    while (check_drift)
        % simple linear interpolation for the moment - not the most
        % accurate - for higher accuracy one should acquire a small
        % calibration scan !

        zdrift = interp1([0, totproj*0.25, totproj*0.5, totproj*0.75, totproj], [corr_0(1), corr_90(1), corr_180(1), corr_270(1), corr_360(1)], 0:totproj);
        ydrift = interp1([0, totproj*0.25, totproj*0.5, totproj*0.75, totproj], [corr_0(2), corr_90(2), corr_180(2), corr_270(2), corr_360(2)], 0:totproj);

        ydrift = ydrift+(detcenter_abs-bbdircenter_abs);

        figure('name', 'Drifts according to images at end')
        subplot(2, 1, 1);plot(ydrift, 'r.');title('horizontal drift');
        subplot(2, 1, 2);plot(zdrift, 'b.');title('vertical drift');
        accept=inputwdefault('Do you accept those values? [y/n]', 'y');

        if (strcmpi(accept, 'y') || strcmpi(accept, 'yes'))
            check_drift = false;
        else
            disp('Run gtFindShifts on pairs: corr_0 = gtFindShifts(im_end_0, im_0), corr_90 = findshifts(im_end_90, im_90), ...');
            %disp('when done set check_drift = 0');% no - we need to go once more through the calculation above...
            disp('when done type return to continue');
            keyboard;
        end
    end
else
    % to be done...
    disp('Correction is not yet implemented for 180degree scans.');
    zdrift = zeros(totproj+1, 1);
    ydrift = zeros(totproj+1, 1);
end

output{1} = rotaxisloc_abs;
output{2} = rotoff_abs;
output{3} = rotoff;

nout = max(nargout, 2) - 2;
for ii = 1:nout
    varargout{ii} = output{ii};
end

end % of function
