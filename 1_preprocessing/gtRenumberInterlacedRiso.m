function gtRenumberInterlacedRiso(nimages, nturns)
%  Function to run live during acquistion of an interlaced scan.
%  As the files are collected, copy them to a new directory, renaming 
%  and reordering.  
%  What is two steps in DCT processing (renaming and then later
%  renumbering) can be done in a single step in this case.
%  References do not need to be treated.
%
%
%  so.. for 80 images collected over two turns
%  nproj=40 in ftomosetup - 
%  nimages=40 in this macro.
%  nturns=2 in this macro.
%  first turn:
%  name_0_0000.edf, name_0_0001.edf... name_0_0039.edf
%  second turn:
%  name_1_0000.edf, name_1_0001.edf... name_1_0039.edf,
%  should become, in the new directory:
%  name_0000.edf, name_0002.edf... name_0078.edf
%  and:
%  name_0001.edf, name_0003.edf... name_0079.edf

%  get the scan name and input directory name
inputdir=pwd;
scanname=inputdir(find(inputdir==filesep,1,'last')+1:end);
%  ouput directory name - suffix "renamed"
outputdir=[inputdir 'renamed'];
mkdir(outputdir);

for i=1:nturns
    for j=1:nimages

        %names for the copy operation
        oldname=sprintf('%s%d_%04d.edf', scanname, i-1, j-1);
        newname=sprintf('%s%04d.edf', scanname, ((j-1)*nturns)+(i-1));

        done=0;
        while done==0
            if exist([inputdir '/' oldname], 'file')
                [s,msg]=copyfile([inputdir '/' oldname],[ outputdir '/' newname]); disp(msg)
                clear s msg
                done=1;
                fprintf('done image %d of %d\n', j+(nimages*(i-1)), nimages*nturns);
            else
                disp('file not found, waiting 10 seconds (crtl-C to quit)')
                pause(10);
            end
        end
    end
end

end

