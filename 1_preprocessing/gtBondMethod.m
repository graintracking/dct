function angle=gtBondMethod(energy)
%given an energy in keV, calculate the rotation required to find the Si
%{111} reflection
%simple helper function


lambda=12.398/energy;
a=5.431;
d=a./sqrt([3 27 75 243]);
theta=asind(lambda./(2*d));

angle=theta;

