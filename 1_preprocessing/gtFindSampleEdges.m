function acqbb = gtFindSampleEdges(acq, prep)
% GTFINDSAMPLEEDGES
%     acqbb = gtFindSampleEdges(acq, prep, bb)
%     ----------------------------------------
%     this is less important for INDEXTER style processing than for Autofind
%     searching of extinction spots.  What is important is to find a margin at
%     the edges of the direct beam (~more or less outside the sample) to be
%     used for normalisation.
%
%     modifications of Sabine incorporated
%     modified to make tolerant of missing files - use what is available, and
%     leave final sample edge position choice to the operator!
%
%     Operates at the start of preprocessing, so images have been undistorted and
%     interlaced images renamed, and placed in the 0_rawdata/scanname/ folder
%
%
%  version 2 - Aking - 25/02/2013 - overwork the final part of the function to fix a
%  bug that can get the rotation axis wrong.


disp('Trying to determine a tight BoundingBox around the sample and a region for intensity normalization');

intervals = 6;

bb = acq.bbdir;

%use bb when reading the files in.
ims = zeros(bb(4),bb(3));
count = 0; %how many images read?

%images per turn
if strcmp(acq.type, '360degree')
    totproj=2*acq.nproj;
elseif strcmp(acq.type, '180degree')
    totproj=acq.nproj;
else
    Mexc = MException('ACQ:invalid_parameter', ...
                      'Unknown type of scan. Check parameters.acq.type!');
    throw(Mexc);
end
im_turn=(totproj/(acq.interlaced_turns+1));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% read images and perform flatfield correction
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% read in as many images as available

acqdir = fullfile(acq.dir, '0_rawdata', acq.name);
darkname = fullfile(acqdir, 'dark.edf');

if exist(darkname,'file')
    d = edf_read(darkname,bb);
else
    d = 100*ones(bb(2),bb(1));
end

for ii = 0:round(im_turn/intervals):im_turn

    imname = fullfile(acqdir, sprintf('%s%04d.edf', acq.name, ii) );
    if exist(imname, 'file');
        im = edf_read(imname,bb) - d;
        % read associated reference - for first turn of interlaced
        % naming is the same as a normal scan
        name = fullfile(acqdir, sprintf('ref0000_%04d.edf', floor(ii/acq.refon)*acq.refon) );
        if ~exist(name, 'file')
            name = fullfile(acqdir, sprintf('refHST%04d.edf', floor(ii/acq.refon)*acq.refon) );
        end
        if exist(name, 'file')
            ref = edf_read(name,bb) - d;
            ims(:,:,count+1)=im./ref;

            count=count+1;
        else
            disp('skipping a missing ref file')
        end
    else
        disp('skipping a missing file')
    end
end

if (count > 0)
    ims = min(ims, [], 3);  % using minimum for better contrast at edges..
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % analyse images
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %intensity profile
    ims_profile = mean(ims);
    %smooth
    a = 10; %filter length
    ims_profile = smooth(ims_profile, a);
    m = abs(diff(ims_profile))'; % sorte de calcul de gradient
    m_signed = diff(ims_profile)';
    %centre of sample
    [~, evt] = min(ims_profile);

    % how easy this is to do depends a lot of the sample/beam profile.
    % for ideal data, should have a flat, high intensity beam, with a lower
    % intensity due to absorption by the sample in the middle.  There should be
    % clear change in gradient between the sample and the unobstructed beam.
    %
    % keep the current logic - I think from Sabine? - just adding smoothing and
    % some tidying.

    zero_crossings = []; %store local maxima
    maxima = []; %store zero crossings
    range = 10;
    %need to pad the profile by this length at each end
    m = [m(1)*ones(1,range) m m(end)*ones(1,range)];
    m_signed = [m_signed(1)*ones(1,range) m_signed m_signed(end)*ones(1,range)];

    for ii = (1:bb(3)-1)+range
        tmp=m(ii-range:ii+range);
        %local maxima
        if m(ii) == max(tmp)
            % should correct for "range" offset, also 0.5 for using "diff"
            maxima = [maxima ii-range+0.5];
        end
        %zero crossings
        if (m_signed(ii)<0 && m_signed(ii+1)>0) || (m_signed(ii)>0 && m_signed(ii+1)<0)
            zero_crossings=[zero_crossings ii-range+0.5];
        end
    end

    %local maxima, hopefully excluding the slits
    %take the local maxima to either side of the centre
    first = maxima(find(maxima<evt, 1, 'last'));
    last = maxima(find(maxima>evt, 1, 'first'));
    %first=maxima(2);
    %last=maxima(end-1);

    %first1=first; last1=last; %

    %zero crossing points outside these local maxima
    %first=max(zero_crossings(find(zero_crossings<first)));
    %last=min(zero_crossings(find(zero_crossings>last)));


    %if (isempty(first));  first=first1; end
    %if (isempty(last)); last=last1; end

    % if nothing found, use direct beam bounding bow - margin
    if (isempty(first))
        disp('default "first" value')
        first = prep.margin;
    end
    if (isempty(last))
        disp('default "last" value')
        last = bb(3)-prep.margin+1;
    end
    disp('centering bb on rotx')
    %rotation axis position 
    bb_rotu = acq.rotu-bb(1)+1;

    % check that enough of a margin is left for normalisation
    min_first = prep.margin + 1;
    max_last = bb(3) - prep.margin;
    % shouldn't exceed either of these
    max_width = min(bb_rotu-min_first, max_last-bb_rotu);

    % we have previously tried to guess first and last
    width = max(bb_rotu-first, last-bb_rotu);
    % apply the limit
    width = min(width, max_width);
    first = floor(bb_rotu-width);
    last = ceil(bb_rotu+width);
else
    disp('%%%%%%% missing images %%%%%%%%')
    disp('no useful images found! - using defaults')
    % as default, use direct beam bounding bow - margin
    first = prep.margin;
    last = bb(3)-prep.margin+1;
    bb_rotu = acq.rotu-bb(1)+1;
    disp('centering bb on rotx')
    disp('%%%%%%% missing images %%%%%%%%')
    disp(' ')
    width = max(bb_rotu-first, last-bb_rotu);
    first = floor(bb_rotu-width);
    last = ceil(bb_rotu+width);
    acqbb = [bb(1)+first-1 bb(2) last-first+1 bb(4)];
    return
end


%pass to operator for final decision
h = figure();

colormap gray
imagesc(ims);
hold on
plot([bb_rotu first first bb_rotu], [1 1 bb(4) bb(4)], 'g')
plot([bb_rotu last last bb_rotu], [1 1 bb(4) bb(4)], 'r')
drawnow

title('use arrow keys to modify the sample BoundingBox - press enter to accept')
set(h, 'KeyPressFcn', @sfKeyPress);


quit = false;
while ~quit
    clf(h)
    imagesc(ims)
    hold on
    plot([bb_rotu first first bb_rotu], [1 1 bb(4) bb(4)], 'g')
    plot([bb_rotu last last bb_rotu], [1 1 bb(4) bb(4)], 'r')
    drawnow
end

acqbb = [bb(1)+first-1 bb(2) last-first+1 bb(4)];
close(h);

%%
% Sub-functions

    function sfKeyPress(~, evt)
        switch evt.Key
            case 'leftarrow' %left arrow
               title('wider - press shift+left arrow to step by 10 pixels, press enter to accept');
               if strcmp(evt.Modifier,'shift')
                 incr = 10;
               else
                 incr = 1;
               end

                if ((first > prep.margin+incr) && (last < bb(3)-prep.margin-incr+1))
                    first = first-incr;
                    last = last+incr;
                else
                    title('maximum width given this margin and direct beam bb!  - press enter to accept')
                end
            case 'rightarrow' % right arrow
                title('narrower - press shift right arrow to step by 10 pixels, press enter to accept');
                if strcmp(evt.Modifier, 'shift')
                 incr = 10;
                else
                 incr = 1;
                end
                first = first+incr;
                last = last-incr;
            case 'return'
                %exit interactive mode and return current shift
                set(h, 'KeyPressFcn', '');
                quit = true;
        end
    end

end % end of function

