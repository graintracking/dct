function gtOneBigMedian
load('parameters.mat');
prep=parameters.prep;
scanname=parameters.acq.name;

d=edf_read(sprintf('0_rawdata/%s/dark.edf',scanname));

k=1;
step=25;
first=6950; last=7200;
medstack=zeros(length(first:step:last),2048,2048);
for n=first:step:last
  k
  fname=sprintf('0_rawdata/%s/%s%04d.edf',scanname,scanname,n);
  im=edf_read(fname)-d;
  
  if 1 %do normalisation?
    
    center=gtCrop(im,prep.bbox);%copy from create full
    switch prep.normalisation
      case 'margin'
        int=gtImgMeanValue(center(:,[1:prep.margin,end-prep.margin+1:end])); % copy from create_full
        medstack(k,:,:)=prep.intensity/int.*im;
      case 'fullbeam'
        int=gtImgMeanValue(gtCrop(im,prep.bbox));
        medstack(k,:,:)=prep.intensity/int.*im;
      case 'none'
        medstack(k,:,:)=im;
    end
  
  else %  bypass normalisation
  medstack(k,:,:)=im;
  end
  k=k+1;
end

med=squeeze(median(medstack,1));
  


edf_write(med,'1_preprocessing/full/big_med0000.edf');

q=1;
step=1;
first=0;last=50;
vol=zeros(length(first:step:last),200,168);
for n=first:step:last
  n
  fname=sprintf('1_preprocessing/abs/abs%04d.edf',n);
  im=edf_read(fname);%-d;
  vol(q,:,:)=im;
  q=q+1;
end

med=squeeze(median(vol,1));
  


edf_write(med,'1_preprocessing/abs/big_med0000.edf');