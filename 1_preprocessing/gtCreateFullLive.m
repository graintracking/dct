function gtCreateFullLive(first_img, last_img, workingdirectory)
% GTCREATEFULLLIVE  Create the full images in the 1_preprocessing/full directory
%     gtCreateFullLive(first_img, last_img, workingdirectory)
%     ------------------------------------------------
%
%     Version 003 06-12-2013 by PReischig
%       Cleaning and rationalising.
%
%     Version 002 10-11-2011 by LNervo
%       Add comments and clean syntax and warnings
%     Sub-functions:
%[sub]- sfPutbb
%
%     Version 001
%       live! will wait for files to appear during acquistion
%
%       add test to wait for images to be translated by gtApplyDrifts before
%       continuing - in pfWaitToRead
%       from prep.correct_drift
%       propagate edf header info to the new images created
%
%       do the renumber of interlaced scans here, as there is no reason not to!
%
%

disp('gtCreateFullLive.m')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Set parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% special case for running interactively in current directory
if ~exist('workingdirectory', 'var')
    workingdirectory = pwd;
end
cd(workingdirectory);

if (isdeployed)
    global GT_DB
    global GT_MATLAB_HOME
    load('workspaceGlobal.mat');
    first_img = str2double(first_img);
    last_img  = str2double(last_img);
    
    % add a small pause to hopefully ease condor crashes
    % only if deployed...
    pausetime = 60 * (first_img / 7200);
    fprintf('Pausing by %0.1f seconds in the hope of easing parameter file conflicts', pausetime)
    pause(pausetime);
end

parameters = gtLoadParameters();
acq         = parameters.acq;
prep        = parameters.prep;
datadir     = fullfile(acq.dir, '0_rawdata', acq.name);
preproc_dir = fullfile(acq.dir, '1_preprocessing');

is_direct_beam = ( ~isfield(acq, 'no_direct_beam') ...
                  || strcmpi(acq.no_direct_beam, 'false') ...
                  || ~acq.no_direct_beam );

if (is_direct_beam)
    disp('Processing mode: direct beam present in images.')
else
    disp('Processing mode: direct beam is NOT present in images.')
end

if (acq.interlaced_turns == 0)
    disp('Processing mode: NON-interlaced scan.')    
else
    disp('Processing mode: interlaced scan.')
end

% We do not wait for images to be shifted by gtApplyDrifts - apply shift at
% the end of the process to full/abs/ext images
waitfortranslated = false;

% how many images in this scan (for renumbering of interlaced scans)
totproj = gtAcqTotNumberOfImages(parameters);
interlacedscan = acq.interlaced_turns + 1;
imageperturn   = totproj / (interlacedscan);

% Read in the dark image
dark_name = fullfile(datadir, 'dark.edf');
if exist(dark_name, 'file')
    dark = edf_wait_read(dark_name, [], waitfortranslated);
else
    darkend_name = fullfile(datadir, 'darkend0000.edf');
    dark = edf_wait_read(darkend_name, [], waitfortranslated) / acq.ndark;
end

% Prepare variables
fullmedianold = '';

first_img = floor(first_img / prep.absint) * prep.absint;
last_img  = ceil(last_img / prep.absint)   * prep.absint;

if (prep.absint == 1)
    last_img = last_img + 1;
end

% Loop through absorption intervals 
for ii = first_img : prep.absint : (last_img - prep.absint)
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Load images
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % Check if we need to update the median file of the full images
    index = floor(ii / prep.fullint) * prep.fullint;
    fullmediannew = fullfile(preproc_dir, 'full', sprintf('med%04d.edf', index) );

    if (~strcmp(fullmediannew, fullmedianold))
        medfull = edf_wait_read(fullmediannew, [], waitfortranslated);
        fullmedianold = fullmediannew;
    end

    % Load absorption median image
    if (is_direct_beam)
        name   = fullfile(preproc_dir, 'abs', sprintf('med%04d.edf', ii) );
        medabs = edf_wait_read(name, [], waitfortranslated);
    end

    % Loop through each image in current absortion interval
    for jj = 0 : (prep.absint - 1)

        % Load full image file, do dark subtraction
        imno = ii + jj;  % image number
        name = fullfile(datadir, sprintf('%s%04d.edf', acq.name, imno));
        [full_vol, info_img] = edf_wait_read(name, [], waitfortranslated);
        full_vol = full_vol - dark;
        
        % XXX Looks quite strange: we didn't cast anything. We just changed the label
        info_img.datatype = 'float32';
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Image intensity normalisation
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if (is_direct_beam)
            switch prep.normalisation
                case 'margin'
                    % Calculate mean intensity in two vertical stripes of 
                    % prep.margin width around the (truncated) direct beam
                    center = gtCrop(full_vol, prep.bbox);
                    margins = [1:prep.margin, (size(center, 2) - prep.margin + 1):size(center, 2)];
                    mean_value(imno + 1) = gtImgMeanValue(center(:, margins));
                    full_vol = prep.intensity / mean_value(imno + 1) * full_vol;
                    
                case 'fullbeam'
                    % Use entire direct beam area for normalisation
                    center = gtCrop(full_vol, prep.bbox);
                    mean_value(imno + 1) = gtImgMeanValue(center);
                    full_vol = prep.intensity / mean_value(imno + 1) * full_vol;
                    
                otherwise
                    % No normalisation
                    mean_value(imno + 1) = prep.intensity;
            end
        end

        % Median subtraction
        full_vol = full_vol - medfull;

        if (is_direct_beam)
            % Load absorption image
            name     = fullfile(preproc_dir, 'abs', sprintf('abs%04d.edf', imno));
            absorp   = edf_wait_read(name, [], waitfortranslated);
            
            % Create, filter and insert direct beam image
            direct   = prep.intensity * (absorp - medabs);
            full_vol = sfPutbb(full_vol, acq.bb, direct);
            full_vol = gtImgMedianFilter2(full_vol, prep.filtsize);
            
            % Create extinction image
            ext      = real(-log(absorp) + log(medabs));
            ext      = gtImgMedianFilter2(ext, prep.filtsize);
        else
            full_vol = gtImgMedianFilter2(full_vol, prep.filtsize);
        end

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Apply sample drifts 
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Move rotation axis into center of images, if drifts are known 
        % from some calibration scan.
        
        % Check status
        if ~isfield(info_img, 'correct_drift_iteration')
            if isfield(prep, 'correct_drift_iteration')
                info_img.correct_drift_iteration = prep.correct_drift_iteration;
            else
                info_img.correct_drift_iteration = 1;
            end
        else
            disp(' ')
            fprintf('Careful! Images have already been shifted %d times. ', prep.correct_drift_iteration)
            disp('This should be handled correctly, but be aware! :-) ')
            disp(' ')
            info_img.correct_drift_iteration = info_img.correct_drift_iteration + 1;
        end

        % Set drifts
        udrift    = prep.udrift;
        vdrift    = prep.vdrift;
        udriftabs = prep.udriftabs;  % absorption images are already centered around rotx - only need drifts here

        % Padding
        if isfield(prep, 'drifts_pad')
            pad = prep.drifts_pad;
        else
            pad = 'av';
        end

        % Apply drifts
        if (udrift(imno + 1) ~= 0) || (vdrift(imno + 1) ~= 0)  
            full_vol = gtShift(full_vol, udrift(imno + 1), vdrift(imno + 1), pad);
            if (is_direct_beam)
                ext    = gtShift(ext, udriftabs(imno + 1), vdrift(imno + 1), pad);
                absorp = gtShift(absorp, udriftabs(imno + 1), vdrift(imno + 1), pad);
            end
        end
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Save images
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        % Final image number and file names
        number_in = imno;
        
        if acq.interlaced_turns == 0
            % not interlaced, no renumbering
            fullname = fullfile(preproc_dir, 'full', sprintf('full%04d.edf', number_in));
            extname  = fullfile(preproc_dir, 'ext', sprintf('ext%04d.edf', number_in));
            absname  = fullfile(preproc_dir, 'abs', sprintf('abs%04d.edf', number_in));
        else
            % renumber - copy from renumber interlaced
            number_out = interlacedscan * mod(number_in, imageperturn) + floor(number_in / imageperturn);
            fullname   = fullfile(preproc_dir, 'full', sprintf('full%04d.edf', number_out) );
            extname    = fullfile(preproc_dir, 'ext', sprintf('ext%04d.edf', number_out) );
            absname    = fullfile(preproc_dir, 'abs', sprintf('abs_renumbered%04d.edf', number_out) );
        end

        % Save full image edf file
        edf_write(full_vol, fullname, info_img, 1);

        % Save extinction and absorption images edf files
        if (is_direct_beam)
            edf_write(ext, extname, info_img, 1);
            edf_write(absorp, absname, info_img, 1);
        end
        
        disp(['Done image ' num2str(imno)])
        
    end % for jj
end %for ii

end % end function



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SUB-FUNCTIONS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function b = sfPutbb(im, gtbb, roi)
    b = im;
    b(gtbb(2):(gtbb(2) + gtbb(4) - 1), gtbb(1):(gtbb(1) + gtbb(3) - 1)) = roi;
end






