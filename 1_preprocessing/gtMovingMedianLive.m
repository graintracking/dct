function gtMovingMedianLive(first, last, workingdirectory)
% GTMOVINGMEDIANLIVE  Calculates the moving median of a series of raw images (first:last)
%     gtMovingMedianLive(first, last, workingdirectory)
%     -------------------------------------------------
%
%     Version 002 10-11-2011 by LNervo
%       Add comments and clean syntax and warnings
%
%     Version 001
%       median is calculated from a stack containing prep.fullrange/interval images
%
%       ATTENTION
%       prep.fullrange must be multiple of prep.fullint and a MULTIPLE OF 2   !!!!!!!!
%       acq.nproj should be a multiple of prep.fullrange
%
%       median is calculated/updated only each interval images
%       the original files are supposed to be in indir/name%04.edf
%       the output is written to directory outdir/med%04d.edf
%       in order to normalize raw images to the same intensity, dark subtraction
%       and a multiplicative corection factor are applied
%
%       live! will wait for files to appear
%
%       add test to wait for images to be translated by gtApplyDrifts before
%       continuing - in pfWaitToRead
%       from prep.correct_drift
%       propagate edf header info to the new images created
%
%       bug - will look for (and use) image 7200 of a scan from 0-7199 images,
%       for example, where 7200 is an "image after the scan".  Change so that
%       7199 would be used, without disturbing output file numbering. ak-10/12/08
%


disp('gtMovingMedianLive.m')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% get parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% special case for running interactively in current directory
if ~exist('workingdirectory', 'var')
    workingdirectory = pwd;
end
cd(workingdirectory);

load('parameters.mat');
acq  = parameters.acq;
prep = parameters.prep;

if isdeployed
    global GT_DB
    global GT_MATLAB_HOME
    load('workspaceGlobal.mat');
    first = str2double(first);
    last  = str2double(last);
end

% should we wait for images to be shifted by gtApplyDrifts?
waitfortranslated = false;
% if strcmp(prep.correct_drift, 'required')
%     waitfortranslated=parameters.prep.correct_drift_iteration;
% end

if strcmp(acq.type, '180degree') || strcmp(acq.type, '180')
    nimages = acq.nproj;
elseif strcmp(acq.type, '360degree') || strcmp(acq.type, '360')
    nimages = acq.nproj*2;
else
    Mexc = MException('ACQ:invalid_parameter', ...
                      'Unknown type of scan. Check parameters.acq.type!');
    throw(Mexc);
end

indir  = fullfile(acq.dir, '0_rawdata', acq.name);
outdir = fullfile(acq.dir, '1_preprocessing', 'full');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% do moving median
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%read in the dark image
if exist(sprintf('0_rawdata/%s/dark.edf', acq.name), 'file')
    fname = fullfile('0_rawdata', acq.name, 'dark.edf');
    dark = edf_wait_read(fname, [], waitfortranslated);
else
    fname = fullfile('0_rawdata', acq.name, 'darkend0000.edf');
    dark = edf_wait_read(fname, [], waitfortranslated) / acq.ndark;
end

if (first - prep.fullrange / 2 < 0)
    start = 0;
else
    start = (first - prep.fullrange / 2);
end

% read the full stack only for the first image - in the following only
% one image is replaced in the stack
% in case we start from image 0, we use the median calculated from [0:interval:prep.fullrange]
% and write the same file for the first  0:interval:prep.fullrange/2 images
medstack_idx = 0;
gtDBConnect
for ii = start:prep.fullint:(start + prep.fullrange)
    medstack_idx = medstack_idx + 1;
    fname = fullfile(indir, sprintf('%s%04d.edf', acq.name, ii));
    [img, info] = edf_wait_read(fname, [], waitfortranslated);
    img = img - dark;
    info.datatype = 'float32';
    center = gtCrop(img, prep.bbox);%copy from create full

    switch prep.normalisation
        case 'margin'
            % e.g.: margin = 5; the first and the last 5 columns of the 
            % prep.bbox are selected
            mean_val = gtImgMeanValue(center(:, [1:prep.margin, (end - prep.margin + 1):end])); 
            medstack(medstack_idx, :, :) = prep.intensity / mean_val .* img;
        case 'fullbeam'
            mean_val = gtImgMeanValue(center);
            medstack(medstack_idx, :, :) = prep.intensity / mean_val .* img;
        case 'none'
            medstack(medstack_idx, :, :) = img;
    end
end

med = squeeze(median(medstack, 1));

%%% XXX - Ugly saving the last EDF header for all of them

% If we are in the special case of starting before prep.fullrange / 2, 
% we write out the same median image prep.fullrange/2/interval times
if start == 0
    for ii = start:prep.fullint:(prep.fullrange / 2)
        fname = fullfile(outdir, sprintf('med%04d.edf', ii) );
        edf_write(med, fname, info);
        start = (prep.fullrange / 2) + prep.fullint;
    end
else
    fname = fullfile(outdir, sprintf('med%04d.edf', first));
    edf_write(med, fname, info);
    start = first + prep.fullint;
end

% check if we are not running out of images
stop = min(last, (nimages - (prep.fullrange / 2)));


% now process images form start:interval:stop

for ii = start:prep.fullint:stop
    medstack_idx = medstack_idx + 1;

    if mod(medstack_idx, (prep.fullrange / prep.fullint) + 1)
        medstack_idx = mod(medstack_idx, (prep.fullrange / prep.fullint) + 1);
    else
        medstack_idx = (prep.fullrange / prep.fullint) + 1;
    end

    if ii + (prep.fullrange / 2) == nimages  % scan runs from 0 to (nimages-1) - see header comment ak-10/12/08
        fname = fullfile(indir, sprintf('%s%04d.edf', acq.name, nimages - 1));
    else
        fname = fullfile(indir, sprintf('%s%04d.edf', acq.name, ii + (prep.fullrange / 2)));
    end
    [img, info] = edf_wait_read(fname, [], waitfortranslated);
    img = img - dark;
    info.datatype = 'float32';
    center = gtCrop(img, prep.bbox);%copy from create full

    switch prep.normalisation
        case 'margin'
            % e.g.: margin = 5; the first and the last 5 columns of the
            % prep.bbox are selected
            mean_val = gtImgMeanValue(center(:, [1:prep.margin, (end - prep.margin + 1):end])); % copy from create_full
            medstack(medstack_idx, :, :) = (prep.intensity / mean_val) .* img;
        case 'fullbeam'
            mean_val = gtImgMeanValue(center);
            medstack(medstack_idx,:,:) = (prep.intensity / mean_val) .* img;
        case 'none'
            medstack(medstack_idx, :, :) = img;
    end

    med = squeeze(median(medstack, 1));
    fname = fullfile(outdir, sprintf('med%04d.edf', ii));
    edf_write(med, fname, info);
end

%%% XXX - Ugly saving the last EDF header for all of them

% some special processing for the last prep.fullrange/2 images: median is no longer
% updated since no images > last available

for ii = (stop + prep.fullint):prep.fullint:last
    fname = fullfile(outdir, sprintf('med%04d.edf', ii) );
    edf_write(med, fname, info);
end

end % end function





