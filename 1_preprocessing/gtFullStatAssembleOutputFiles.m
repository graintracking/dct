%
% FUNCTION gtFullStatAssembleOutputFiles
%
% Looks for the output files of gtFullStatCondor and copies the data in one
% single file named 'fullstat_dataname.mat'.
% The seperate result files can be deleted afterwards.

function gtFullStatAssembleOutputFiles

load('parameters.mat');

rfile=dir('fullstatpart_*_*.mat');

disp(['Number of result files: ' num2str(length(rfile))])

for i=1:length(rfile)
  fname=rfile(i).name;
  data=load(fname);
  startno=str2num(fname(12:15));
  endno=str2num(fname(17:20));
  mean_full_all(startno+1:endno+1)=data.mean_full;
  mean_full_1_all(startno+1:endno+1)=data.mean_full_1;
  med_full_all(startno+1:endno+1)=data.med_full;
  peak_full_all(startno+1:endno+1)=data.peak_full;
  std_full_all(startno+1:endno+1)=data.std_full;
  std_full_1_all(startno+1:endno+1)=data.std_full_1;
end

figure('name','Median of fullimages')
plot(med_full_all,'b+')

figure('name','Peak of fullimages')
plot(peak_full_all,'b+')

figure('name','Mean of fullimages')
plot(mean_full_all,'b+')

figure('name','Mean of fullimages; 1% cut off')
plot(mean_full_1_all,'b+')

figure('name','STD of fullimages')
plot(std_full_all,'b+')

figure('name','STD of fullimages; 1% cut off')
plot(std_full_1_all,'b+')

allfilename=['fullstat_' parameters.acq.name '.mat'];
allfilename=allfilename(1:end-1);

save(allfilename,'mean_full_all','mean_full_1_all',...
     'med_full_all','peak_full_all','std_full_all','std_full_1_all')

end
