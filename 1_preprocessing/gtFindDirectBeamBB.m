function bbdir = gtFindDirectBeamBB(acq)
% GTFINDDIRECTBEAMBB  Gives approximate bounding box of direct beam
%     bbdir = gtFindDirectBeamBB(acq)
%     -------------------------------
%     INPUT:
%       acq = acquisition structure in parameters.mat
%
%     OUTPUT:
%       bbdir  = direct beam bounding box <double 1x4>
%
%     Version 004 26/04/2012 by LNervo
%       Replace sprtinf by fullfile
%       
%     Version 003 25-11-2011 by LNervo
%       *** clean version ***
%
%     Version 001 July-2007 by Wolfgang,Peter
%       to be used for
%       calibration and to be refined as sample bounding box.
%
%     Version 002 October-2008 by AKing
%       to be more tolerant of missing images
%       (incorporating Sabine's modifications for interlaced scans)
%
%     Version 003 March 2020 by W Ludwig
%       now allows checking an existing bounding box.
%
%     Operates at the start of preprocessing, so images have been undistorted and
%     interlaced images renamed, and placed in the 0_rawdata/scanname/ folder
%
%

refarea = 200;
lim_directbeam = 0.4;
ref = get_refimage(acq);

if ~isempty(acq.bbdir)
    bbdir = display_bbdir(acq, acq.bbdir, ref);
else
    disp(' ');
    disp('Determining direct beam bounding box...');
    disp(' ');

    % calculate mean value m in central part of the image
    m = gtImgMeanValue(ref(round(acq.ydet/2-refarea:acq.ydet/2+refarea+1), ...
        round(acq.xdet/2-refarea:acq.xdet/2+refarea+1)));
    cen = double(ref > (lim_directbeam*m));  % should correspond to direct beam footprint

    % find the direct beam region bb automatically (supposes fairly homogeneous
    % beam profile - attention with ID11 data here... !

    marker = zeros(acq.ydet, acq.xdet);
    marker(round(acq.ydet/2), round(acq.xdet/2)) = 1;
    cen = imreconstruct(marker, cen);
    tmp = regionprops(cen, 'BoundingBox');

    if isempty(tmp)
        disp('Please select (zoom onto) the direct beam area and disable zoom afterwards');
        hfig = figure('name', 'Direct beam bounding box');
        colormap gray;
        imagesc(ref);
        drawnow;
        h = zoom(hfig); set(h, 'Enable', 'on'); waitfor(h, 'Enable', 'off'), hold on;
        disp('-> Now click top-left and bottom-right corners for final selection');
        bbdir = ginput(2);
        bbdir = round([bbdir(1, 1) bbdir(1, 2) bbdir(2, 1)-bbdir(1, 1)+1 bbdir(2, 2)-bbdir(1, 2)+1]);

        % Make sure the bbox width and height are even numbers (needed for correlation)
        bbdir(3:4) = ceil(bbdir(3:4)/2)*2;
        cen = gtPlaceSubImage(ones(bbdir(4),bbdir(3)), zeros(acq.ydet, acq.xdet), bbdir(1), bbdir(2));
    else
        bbdir = tmp.BoundingBox;
    end

    % Tighten bb until it's fully filled
    bbdir = [ceil(bbdir(1:2)), bbdir(3:4)-1];
    goon = true;
    while goon
        cenbb = gtCrop(cen, bbdir);
        goon = false;

        if any(~cenbb(:, 1)) % tighten bb on the left
            bbdir(1) = bbdir(1)+1;
            bbdir(3) = bbdir(3)-1;
            goon = true;
        end

        if any(~cenbb(:, end)) % tighten bb on the right
            bbdir(3) = bbdir(3)-1;
            goon = true;
        end

        cenbb = gtCrop(cen, bbdir);

        if any(~cenbb(1, :)) % tighten bb at the top
            bbdir(2) = bbdir(2)+1;
            bbdir(4) = bbdir(4)-1;
            goon = true;
        end

        if any(~cenbb(end, :)) % tighten bb at the bottom
            bbdir(4) = bbdir(4)-1;
            goon = true;
        end
    end

    % Loosen bb if it can be on any side
    goon = true;
    while goon
        goon = false;

        tmpbb = bbdir+[-1 0 1 0]; % loose bb on the left
        tmpim = gtCrop(cen, tmpbb);
        if all(tmpim(:))
            bbdir = tmpbb;
            goon = true;
        end

        tmpbb = bbdir+[0 0 1 0]; % loose bb on the right
        tmpim = gtCrop(cen, tmpbb);
        if all(tmpim(:))
            bbdir = tmpbb;
            goon = true;
        end

        tmpbb = bbdir+[0 -1 0 1]; % loose bb at the top
        tmpim = gtCrop(cen, tmpbb);
        if all(tmpim(:))
            bbdir = tmpbb;
            goon = true;
        end

        tmpbb = bbdir+[0 0 0 1]; % loose bb at the bottom
        tmpim = gtCrop(cen, tmpbb);
        if all(tmpim(:))
            bbdir = tmpbb;
            goon = true;
        end
    end


    % make sure the bbox width and height are even numbers (needed for correlation)
    bbdir(3:4) = ceil(bbdir(3:4)/2)*2;
    bbdir = display_bbdir(acq, bbdir, ref);
end

end % end of function


function bbdir = display_bbdir(acq, bbdir, ref)
    hfig = figure('name', 'Direct beam bounding box');
    % plot bbox found, its center-line and center-line of image
    bbcenter_abs = ((2*bbdir(1))+bbdir(3)-1)/2;

    colormap gray;
    imagesc(ref);
    drawnow;
    hold on;
    plot([bbdir(1), bbdir(1)+bbdir(3)-1, bbdir(1)+bbdir(3)-1, bbdir(1), bbdir(1)], [bbdir(2), bbdir(2), bbdir(2)+bbdir(4)-1, bbdir(2)+bbdir(4)-1, bbdir(2)], 'r-');
    plot([acq.xdet/2+0.5 acq.xdet/2+0.5], [1 acq.ydet], 'b-.'); % centre of the ccd
    plot([bbcenter_abs bbcenter_abs], [bbdir(2) bbdir(2)+bbdir(4)-1], 'c-.'); % centre of the direct beam

    % interactive check
    questionMsg = 'Are you satisfied with this bounding box for the direct beam? [y/n]';
    while (strcmpi(inputwdefault(questionMsg, 'n'), 'n'))
        disp('Please select (zoom onto) the direct beam area and disable zoom afterwards');

        figure(hfig), clf, imagesc(ref), h = zoom(hfig); set(h, 'Enable', 'on'); waitfor(h, 'Enable', 'off'), hold on;
        disp('-> Now click top-left and bottom-right corners for final selection');
        bbdir = ginput(2);
        bbdir = round([bbdir(1, 1) bbdir(1, 2) bbdir(2, 1)-bbdir(1, 1)+1 bbdir(2, 2)-bbdir(1, 2)+1]);

        % Make sure the bbox width and height are even numbers (needed for correlation)
        bbdir(3:4) = ceil(bbdir(3:4)/2)*2;

        % Plot the new bounding box
        plot([bbdir(1), bbdir(1)+bbdir(3)-1, bbdir(1)+bbdir(3)-1, bbdir(1), bbdir(1)], [bbdir(2), bbdir(2), bbdir(2)+bbdir(4)-1, bbdir(2)+bbdir(4)-1, bbdir(2)], 'r-');
        bbcenter_abs = ((2*bbdir(1))+bbdir(3)-1)/2;
        plot([acq.xdet/2+0.5 acq.xdet/2+0.5], [1 acq.ydet], 'b-.');
        plot([bbcenter_abs bbcenter_abs], [bbdir(2) bbdir(2)+bbdir(4)-1], 'c-.'); % centre of the direct beam
    end
    close(hfig);
end

function ref = get_refimage(acq)

    % get dark image
    darkname = fullfile(acq.dir,'0_rawdata',acq.name,'dark.edf');
    if exist(darkname, 'file')
        d = edf_read(darkname);
    end
    % get the first reference image
    name1 = fullfile(acq.dir,'0_rawdata',acq.name,'refHST0000.edf');
    name2 = fullfile(acq.dir,'0_rawdata',acq.name,'ref0000_0000.edf');
    if exist(name1, 'file')
        ref0 = edf_read(name1) - d;
    elseif exist(name2, 'file')
        ref0 = edf_read(name2) - d;
    else
        disp('No initial reference image found!');
        ref0 = [];
    end

    % if possible get the last reference image
    % after renaming of interlaced scans, so no special naming conventions
    if strcmp(acq.type, '360degree')
        totproj = 2*acq.nproj;
    elseif strcmp(acq.type, '180degree')
        totproj = acq.nproj;
    else
        gtError('ACQ:invalid_parameter', ...
            'Unknown type of scan: "%s". Check parameters.acq.type!', acq.type);
    end
    name1 = fullfile(acq.dir, '0_rawdata', acq.name, sprintf('refHST%04d.edf', totproj));
    name2 = fullfile(acq.dir, '0_rawdata', acq.name, sprintf('ref0000_%04d.edf', totproj));
    if exist(name1, 'file')
        ref1 = edf_read(name1) - d;
    elseif exist(name2, 'file')
        ref1 = edf_read(name2) - d;
    else
        disp('No final reference image found...');
        ref1 = [];
    end

    %take the minimum of the two references if available
    if (~isempty(ref0) && ~isempty(ref1))
        ref = min(ref0, ref1); % min of ref-s at beginning and end of scan
    elseif (~isempty(ref0))
        ref = ref0;
    else % no images found!
        disp('%%%%%%% missing images %%%%%%%%');
        disp('no reference images found in this directory!');
        disp('using a default guess');
        disp('%%%%%%% missing images %%%%%%%%');
        disp(' ');
        bbdir = [512 512 1024 1024];
        return
    end
end