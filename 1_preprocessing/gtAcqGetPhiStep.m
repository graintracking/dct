function phi_step = gtAcqGetPhiStep(parameters, det_ind, unit)
    if (~exist('det_ind', 'var') || isempty(det_ind))
        det_ind = 1;
    end
    if (~exist('unit', 'var') || isempty(unit))
        unit = 'deg';
    end

    if (~strcmpi(parameters.acq(det_ind).type, 'topotomo'))
        error('gtAcqGetThetaStep:wrong_argument', ...
            'This is not a topotomo acquisition!')
    end

    num_det = numel(det_ind);
    phi_step = zeros(num_det, 1);
    for ii_d = 1:num_det
        phi_step(ii_d) = get_step(parameters, det_ind(ii_d));
    end

    if (strcmpi(unit, 'rad'))
        phi_step = phi_step * pi / 180;
    end
end

function phi_step = get_step(parameters, det_ind)
    if (isfield(parameters.detgeo(det_ind), 'phstep'))
        phi_step = parameters.detgeo(det_ind).phstep;
    else
        range_basetilt = parameters.acq(det_ind).range_basetilt(2) - parameters.acq(det_ind).range_basetilt(1);
        num_basetilt_imgs = parameters.acq(det_ind).nproj_basetilt;
        % Because it is a continous integration!
        phi_step = range_basetilt / num_basetilt_imgs;
    end
end
