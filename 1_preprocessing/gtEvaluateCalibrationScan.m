function [ydrift_pol,zdrift_pol,varargout]=gtEvaluateCalibrationScan(acq,bbox)
%
% FUNCTION [ydrift_pol,zdrift_pol,varargout]=gtEvaluateCalibrationScan2(acq,bbox)
%
% In bbox, treats the calibration scan, determines rotation axis location
% at sub-pixel (in full image and in bbox), creates master images as references
% to sample drifts, determines accurate sample bounding box for the total scan
% inside bbox.
%
% Sample drifts are relative to constant reference positions, where
% reference positions have been determined the following way:
% Assuming that during calibration scan the sample doesn't move relative to
% the rotation table, the rotation axis is determined from image pairs 180
% degrees offset. Master images are created from the calibration in a way
% that they are translated to have the rotation axis in the very center of
% the full image (even if working in an asymmetric bbox).
% Images of the scan are correlated (horizontally and/or vertically) with
% those masters, yielding 'ydrift' and 'zdrift'.
%
% Uses linear flat correction between ref0000_xxxx-s.
%
% INPUT
%   acq = acquisition parameters of scan
%   bbox = according to GT conventions [originx, originy, width, height]
%            bbox doesn't need to be centered in full image
%            if acq.correlationdim=1 bbox(4) needs to be even number (for correlate1)
%            if acq.correlationdim=2 bbox(3) needs to be even number (for correlate1)
%
% OUTPUT
%   ydrift_pol, zdrift_pol = polynomial values with which the images need to be directly
%                             translated (e.g direct input for interpolatef)
%   varargout{1} = ydrift_int;  interpolated values of ydrifts
%   varargout{2} = zdrift_int;  interpolated values of zdrifts
%   varargout{3} = rotaxisloc_abs;  rot axis x (horizontal) location in full image
%                   (non-integer in pixels)
%   varargout{4} = rotdev_abs;  rot axis offset in full image; vector pointing
%                   from center of image to rotation axis location;
%                   positive to the right
%   varargout{5} = rotdev;  rot axis offset in bbox; vector pointing from center
%                   of bbox to rotation axis location in bbox, positive to the right
%   varargout{6} = bbs_ext;  bounding box of maximum fill by sample  during
%                   rotation extended horizontally by ext_pix, in full image
%   varargout{7} = edges;  bounding box of maximum fill by sample in bbox according to 'lim_samatten'
%   varargout{8} = edges_ext;  'edges' extended by 'ext_pix' (at most up to the edges of bbox)
%
% 07/2007 Wolfgang, Peter
%

%% Parameters
tol_pol=1; % tolerance factor for polynomial outliers (in unit std)
tol_rotdev=0.5; % tolerance for deviation in rotation axis location found by different pairs (used for warning)
lim_samatten=0.92; % limit to describe sample attenuation
ext_pix=2; % sample bounding box extension in pixels
frame=20; % frame extension for reading master files to be able to translate them

clims_substract=[-0.1 0.1]; % color limits for substracted sample image display
clims_sample=[0 1]; % color limits for sample display

%%

disp(' ')
disp('Evaluating calibration scan...')
disp(' ')

dim=acq.correlationdim;

calibname=acq.calibration_path(find(acq.calibration_path=='/',1,'last')+1:end);

scanname=acq.dir(find(acq.dir=='/',1,'last')+1:end);
scanfolder=sprintf('%s/0_rawdata/%s',acq.dir,scanname);

if strcmpi(acq.type,'360degree')
  totproj = acq.nproj * 2;
  if acq.caliboffset < 0 || acq.caliboffset >= totproj
    message = sprintf('Invalid calibration offset value!\nCalibration offset have to be >= 0 and < %d',totproj);
    Mexc = MException('ACQ:invalid_parameter', message);
    throw(Mexc)
  end
elseif strcmpi(acq.type,'180degree')
  totproj=acq.nproj;
  if acq.caliboffset < 0 || acq.caliboffset >= totproj
    message = sprintf('Invalid calibration offset value!\nCalibration offset have to be >= 0 and < %d',2*totproj);
    Mexc = MException('ACQ:invalid_parameter', message);
    throw(Mexc)
  end
else
    Mexc = MException('ACQ:invalid_parameter', 'Unknown type of scan. Check acq.type!');
    throw(Mexc)
end

bbread = [bbox(1)-frame,bbox(2)-frame,bbox(3)+2*frame,bbox(4)+2*frame];

if ~strcmpi(acq.calibtype,'180degree') && ~strcmpi(acq.calibtype,'360degree')
  gtError('Unknown type of calibration scan (see acq.calibtype)!')
end

xmlPrefs = []; xmlPrefs.Str2Num = 'never';
xml_filename = fullfile(acq.calibration_path, [calibname '.xml']);
xml=xml_read(xml_filename, xmlPrefs);

tomo_N = str2double(xml.acquisition.tomo_N);
ref_N  = str2double(xml.acquisition.ref_N);
%ref_On=str2num(xml.acquisition.ref_On);
%dark_N=str2num(xml.acquisition.dark_N);

close all

%% Determine rotation axis offset

detcenter_abs=acq.xdet/2+0.5;
bboxcenter_abs=(2*bbox(1)+bbox(3)-1)/2;

% Darks and ref-s
cdark=edf_read(sprintf('%s/dark.edf',acq.calibration_path),bbread,'nodisp');
cdarkbb=cdark(1+frame:end-frame,1+frame:end-frame);

refstart=edf_read(sprintf('%s/ref0000_0000.edf',acq.calibration_path),bbread,'nodisp');
for k=1:ref_N-1
  refstart=refstart+edf_read(sprintf('%s/ref%0.4d_0000.edf',acq.calibration_path,k),bbread,'nodisp');
end
refstart=refstart/ref_N-cdark;
% refstartbb=refstart(1+frame:end-frame,1+frame:end-frame);

refend=edf_read(sprintf('%s/ref0000_%0.4d.edf',acq.calibration_path,tomo_N),bbread,'nodisp');
for k=1:ref_N-1
  refend=refend+edf_read(sprintf('%s/ref%0.4d_%0.4d.edf',acq.calibration_path,k,tomo_N),bbread,'nodisp');
end
refend=refend/ref_N-cdark;
refendbb=refend(1+frame:end-frame,1+frame:end-frame);

imend180=(edf_read(sprintf('%s/%s%0.4d.edf',acq.calibration_path,calibname,tomo_N+2),bbox,'nodisp')-cdarkbb)./refendbb;
imend0=(edf_read(sprintf('%s/%s%0.4d.edf',acq.calibration_path,calibname,tomo_N),bbox,'nodisp')-cdarkbb)./refendbb;
imend0=fliplr(imend0);

% Determine rotdev: that is a vector of [bbox center to rotation axis
%  location in the bbox], positive to the right.
rotdev=median(correlate1(imend180',imend0'))/2;

% Confirmation with findshifts %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
imforfs=interpolatef(imend0,0,rotdev*2);

shift=[];
assignin('base','shift',shift);
disp(' ')
disp('Calibration scan: check result of image correlation with findshifts...')
disp(' ')

gtFindShifts(imend180,imforfs);
set(gcf,'name','Calibration scan: check correlation...')
while isempty(shift)
  drawnow;
  shift=evalin('base','shift');
end

shift=evalin('base','shift');
rotdev=shift(2)/2+rotdev; %%% check this!!!
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if strcmpi(acq.calibtype,'360degree')
  imend270=(edf_read(sprintf('%s/%s%0.4d.edf',acq.calibration_path,calibname,tomo_N+3),bbox,'nodisp')-cdarkbb)./refendbb;
  imend90=(edf_read(sprintf('%s/%s%0.4d.edf',acq.calibration_path,calibname,tomo_N+1),bbox,'nodisp')-cdarkbb)./refendbb;
  imend90=fliplr(imend90);

  rotdev2=median(correlate1(imend270',imend90'))/2;

  disp('Rotation axis location in bounding box found by first pair:')
  disp(rotdev)
  disp('Rotation axis location in bounding box found by second pair:')
  disp(rotdev2)

  % Confirmation with findshifts %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  imforfs=interpolatef(imend90,0,rotdev2*2);

  shift=[];
  assignin('base','shift',shift);
  disp(' ')
  disp('Calibration scan: check result of image correlation with findshifts...')
  disp(' ')

  gtFindShifts(imend270,imforfs);
  set(gcf,'name','Calibration scan: check correlation...')
  while isempty(shift)
    drawnow;
    shift=evalin('base','shift');
  end

  shift=evalin('base','shift');
  rotdev2=shift(2)/2+rotdev2; %%% check this!!!
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  if abs(rotdev-rotdev2)>tol_rotdev
    disp('WARNING! Incosistent values have been found for rotation axes location in calibration scan.')
    disp('Check images at end in calibration scan!')
    pause(3)
  end
  rotdev=(rotdev+rotdev2)/2;
end

% vector of rotation axis location in full image; [from image center
% to rotation axis location], positive to the right;
rotdev_abs=bboxcenter_abs-detcenter_abs+rotdev;
rotaxisloc_abs=bboxcenter_abs+rotdev;

%% Create masters and correlate scan images

if strcmpi(acq.type,'360degree')
  if strcmpi(acq.calibtype,'360degree')
    sf=totproj/tomo_N;
  elseif strcmpi(acq.calibtype,'180degree')
    sf=totproj/tomo_N/2;
  else
    gtError('Unknown type of calibration scan (see parameter file)!')
  end
elseif strcmpi(acq.type,'180degree')
  if strcmpi(acq.calibtype,'360degree')
    sf=totproj/tomo_N*2;
  elseif strcmpi(acq.calibtype,'180degree')
    sf=totproj/tomo_N;
  else
    gtError('Unknown type of calibration scan (see parameter file)!')
  end
else
  gtError('Unknown type of scan (see parameter file acq.type)!')
end

if mod(sf,1)~=0
  gtError('No. of images in scan and calibration do not correspond!')
end


imdark=edf_read(sprintf('%s/dark.edf',scanfolder),bbox,'nodisp');
corr=NaN(totproj+1,2);
masterblob=[];
imblob=[];

for jj=0:tomo_N
  cref=(tomo_N-jj)/tomo_N*refstart+jj/tomo_N*refend;
  master=(edf_read(sprintf('%s/%s%0.4d.edf',acq.calibration_path,calibname,jj),bbread,'nodisp')-cdark)./cref;
  master=interpolatef(master,0,-rotdev);
  masterblob(:,:,jj+1)=master(1+frame:end-frame,1+frame:end-frame);
  %edf_write(masterbb,sprintf('%s/master_%0.4d.edf',acq.calibration_path,i),'float32');
end


%% Determine correlation

cmimi=[];

if strcmpi(acq.calibtype,'180degree') && strcmpi(acq.type,'180degree')
  for imi=0:sf:totproj

    if abs((imi-acq.caliboffset)/sf)==tomo_N
      mi=tomo_N;
    else
      mi=mod((imi-acq.caliboffset)/sf,tomo_N);
    end

    masterbb=masterblob(:,:,mi+1);

    if (imi>=acq.caliboffset) || (imi<=acq.caliboffset-totproj) || (imi==totproj && mi==0)
      fl=[];
    else
      fl='flipped';
      masterbb=fliplr(masterbb);
    end

    cmimi=[cmimi; imi mi];
    disp(sprintf(' Correlating: image %d with master %d %s',imi,mi,fl))
    [corr,imbb]=sfCorrMaster(corr,scanfolder,scanname,imi,acq,bbox,imdark,masterbb,dim);
    imblob(:,:,end+1)=imbb;
  end
end


if strcmpi(acq.calibtype,'180degree') && strcmpi(acq.type,'360degree')
  for imi=0:sf:totproj

    if abs((imi-acq.caliboffset)/sf)==tomo_N
      mi=tomo_N;
    else
      mi=mod((imi-acq.caliboffset)/sf,tomo_N);
    end

    masterbb=masterblob(:,:,mi+1);

    if (imi>=acq.caliboffset && imi<=acq.caliboffset+totproj/2) || (imi<=acq.caliboffset-totproj/2) || (imi==totproj && mi==0)
      fl=[];
      cmimi=[cmimi; imi mi 0];
    else
      fl='flipped';
      masterbb=fliplr(masterbb);
      cmimi=[cmimi; imi mi 1];
    end

    disp(sprintf(' Correlating: image %d with master %d %s',imi,mi,fl))
    [corr,imbb]=sfCorrMaster(corr,scanfolder,scanname,imi,acq,bbox,imdark,masterbb,dim);
    imblob(:,:,end+1)=imbb;
  end
end

if strcmpi(acq.calibtype,'360degree')
  for imi=0:sf:totproj
    mi=mod((imi-acq.caliboffset)/sf,tomo_N);

    if mi==0 && imi==totproj
      mi=tomo_N;
    end

    masterbb=masterblob(:,:,mi+1);

    cmimi=[cmimi; imi mi 0];
    disp(sprintf(' Correlating: image %d with master %d',imi,mi))
    [corr,imbb]=sfCorrMaster(corr,scanfolder,scanname,imi,acq,bbox,imdark,masterbb,dim);
    imblob(:,:,end+1)=imbb;
  end
end

imblob(:,:,1)=[];


%disp('Master images are saved in calibration folder.')

%corr(isnan(corr))=interp1(find(~isnan(corr)),corr(~isnan(corr)),find(isnan(corr)));

%% Polynomial of vertical drift
corr_ver=corr(:,1);
figure('name','Vertical drift of scan from calibration')
hold on
plot(corr_ver,'r.')

[pol_ver,s_ver,mu_ver]=polyfit(find(~isnan(corr_ver)),corr_ver(~isnan(corr_ver)),5);
corrp_ver=polyval(pol_ver,((1:totproj+1)-mu_ver(1))/mu_ver(2))';
err_ver=corrp_ver-corr_ver;

corr_ver(abs(err_ver)>tol_pol*std(err_ver(~isnan(err_ver))))=NaN;
plot(find(~isnan(corr_ver)),corr_ver(~isnan(corr_ver)),'g.')
[pol_ver,s_ver,mu_ver]=polyfit(find(~isnan(corr_ver)),corr_ver(~isnan(corr_ver)),8);
corrp_ver=polyval(pol_ver,((1:totproj+1)-mu_ver(1))/mu_ver(2))'; % vector of drifts
plot(corrp_ver,'r.','markersize',3)


% Polynomial of horizontal drift
% add a constant drift to all, if bounding bbox is not centered in the
%  original image
corr_hor=corr(:,2)+(detcenter_abs-bboxcenter_abs);
figure('name','Horizontal drift of scan from calibration')
hold on
plot(corr_hor,'r.')

[pol_hor,s_hor,mu_hor]=polyfit(find(~isnan(corr_hor)),corr_hor(~isnan(corr_hor)),5);
corrp_hor=polyval(pol_hor,((1:totproj+1)-mu_hor(1))/mu_hor(2))';
err_hor=corrp_hor-corr_hor;

corr_hor(abs(err_hor)>tol_pol*std(err_hor(~isnan(err_hor))))=NaN;
plot(find(~isnan(corr_hor)),corr_hor(~isnan(corr_hor)),'g.')
[pol_hor,s_hor,mu_hor]=polyfit(find(~isnan(corr_hor)),corr_hor(~isnan(corr_hor)),8);
corrp_hor=polyval(pol_hor,((1:totproj+1)-mu_hor(1))/mu_hor(2))'; % vector of drifts
plot(corrp_hor,'r.','markersize',3)

ydrift_pol=corrp_hor;
zdrift_pol=corrp_ver;


% Interpolated drifts
zdrift_int=corr(:,1);
zdrift_int(isnan(zdrift_int))=interp1(find(~isnan(zdrift_int)),zdrift_int(~isnan(zdrift_int)),find(isnan(zdrift_int)));

ydrift_int=corr(:,2)+(detcenter_abs-bboxcenter_abs);
ydrift_int(isnan(ydrift_int))=interp1(find(~isnan(ydrift_int)),ydrift_int(~isnan(ydrift_int)),find(isnan(ydrift_int)));


%% BBox filled by sample during scan
[imblob_min3,imno_imblobmin]=min(imblob,[],3);
[minrow,imno_minrow]=min(imblob_min3,[],1);
[mincol,imno_mincol]=min(imblob_min3,[],2);

imno_minrow=imno_imblobmin(imno_minrow+(0:size(imno_imblobmin,2)-1)*size(imno_imblobmin,1))-1;
imno_mincol=imno_imblobmin((1:size(imno_imblobmin,1))'+(imno_mincol-1)*size(imno_imblobmin,1))-1;

if strcmpi(acq.calibtype,'180degree') && strcmpi(acq.type,'360degree')
  tmp=imno_minrow;
  imno_minrow=mod(tmp,tomo_N);
  imno_minrow(tmp>0 & imno_minrow==0)=tomo_N;

  tmp=imno_mincol;
  imno_mincol=mod(tmp,tomo_N);
  imno_mincol(tmp>0 & imno_mincol==0)=tomo_N;
end

imno_probl=[];

if max(mincol)>lim_samatten
  disp('WARNING! Sample during scan may not entirely fill the given bounding box!')
  imno_probl=unique(imno_mincol(mincol>lim_samatten));
  disp(' See scan images no. : ');
  disp(imno_probl)
  showims='yes';
end

if max(minrow)<lim_samatten
  disp('WARNING! Sample during scan may have moved out of the given bounding box!')
  imno_probl=unique(imno_minrow(minrow<lim_samatten));
  disp(' See scan images no. : ');
  disp(imno_probl')
  showims='yes';
end


%% Determine and check bbox filled by sample during calibration
[masterblob_min3,mno_masterblobmin]=min(masterblob,[],3);
[minrow,mno_minrow]=min(masterblob_min3,[],1);
[mincol,mno_mincol]=min(masterblob_min3,[],2);

mno_minrow=mno_masterblobmin(mno_minrow+(0:size(mno_masterblobmin,2)-1)*size(mno_masterblobmin,1))-1;
mno_mincol=mno_masterblobmin((1:size(mno_masterblobmin,1))'+(mno_mincol-1)*size(mno_masterblobmin,1))-1;

if strcmpi(acq.calibtype,'180degree') && strcmpi(acq.type,'360degree')
  tmp=mno_minrow;
  mno_minrow=mod(tmp,tomo_N);
  mno_minrow(tmp>0 & mno_minrow==0)=tomo_N;

  tmp=mno_mincol;
  mno_mincol=mod(tmp,tomo_N);
  mno_mincol(tmp>0 & mno_mincol==0)=tomo_N;
end

showims='no';

mno_probl=[];

if max(mincol)>lim_samatten
  disp('WARNING! Sample during calibration may not entirely fill the given bounding box!')
  mno_probl=unique(mno_mincol(mincol>lim_samatten));
  disp(' See master image no. : ');
  disp(mno_probl)
  showims='yes';
end

if max(minrow)<lim_samatten
  disp('WARNING! Sample during calibration may have moved out of the given bounding box!')
  mno_probl=unique(mno_minrow(minrow<lim_samatten));
  disp(' See master image no. : ');
  disp(mno_probl')
  showims='yes';
end


%% Display

sfDisplayImages(cmimi,masterblob,imblob,corr,clims_sample,clims_substract,mno_probl,imno_probl,0)

showims=inputwdefault('Would you like to loop through the scan images?',showims);

if strcmpi(showims,'y') || strcmpi(showims,'yes')
  disp('Check master and scan images...')
  disp('Press enter to loop through images')
  sfDisplayImages(cmimi,masterblob,imblob,corr,clims_sample,clims_substract,mno_probl,imno_probl,1)
end

%% Create sample bounding box

minrow=minrow<lim_samatten;
marker=false(size(minrow));
marker(round(length(marker)/2))=true;
minrowr=imreconstruct(marker,minrow);
edgel=find(minrowr,1,'first');
edger=find(minrowr,1,'last');
edgextl=max(edgel-ext_pix,1);
edgextr=min(edger+ext_pix,bbox(3));
bbs_ext(1)=bbox(1)+edgextl-1;
bbs_ext(3)=edgextr-edgextl+1;

mincol=mincol<lim_samatten;
marker=false(size(mincol));
marker(round(length(marker)/2))=true;
mincolr=imreconstruct(marker,mincol);
edget=find(mincolr,1,'first');
edgeb=find(mincolr,1,'last');
edgextt=max(edget-ext_pix,1);
edgextb=min(edgeb+ext_pix,bbox(4));
bbs_ext(2)=bbox(2)+edgextt-1;
bbs_ext(4)=edgextb-edgextt+1;

edges=[edgel edget edger edgeb];
edges_ext=[edgextl edgextt edgextr edgextb];

figure('name','Bounding box filled by sample during calibration')
imshow(masterblob_min3,[0 1])
hold on
plot([edgel edger edger edgel edgel],[edget edget edgeb edgeb edget],'r-')
plot([edgextl edgextr edgextr edgextl edgextl],[edgextt edgextt edgextb edgextb edgextt],'g-')

output{1}=ydrift_int;
output{2}=zdrift_int;
output{3}=rotaxisloc_abs;
output{4}=rotdev_abs;
output{5}=rotdev;
output{6}=bbs_ext;
output{7}=edges;
output{8}=edges_ext;

nout=max(nargout,2)-2;
for k=1:nout
  varargout{k}=output{k};
end

keyboard


%% Sub-functions

function [corr,im]=sfCorrMaster(corr,scanfolder,scanname,imi,acq,bbox,imdark,masterbb,dim)
  imrefa=edf_read(sprintf('%s/ref0000_%0.4d.edf',scanfolder,floor(imi/acq.refon)*acq.refon),bbox,'nodisp')-imdark;
  imrefb=edf_read(sprintf('%s/ref0000_%0.4d.edf',scanfolder,ceil((imi-eps)/acq.refon)*acq.refon),bbox,'nodisp')-imdark;
  imref=(acq.refon-mod(imi,acq.refon))/acq.refon*imrefa+mod(imi,acq.refon)/acq.refon*imrefb;
  im=(edf_read(sprintf('%s/%s%0.4d.edf',scanfolder,scanname,imi),bbox,'nodisp')-imdark)./imref;

  if dim==0 || isempty(dim)
    %corrvec=correlaterealspace(masterbb,im,20,20);
    %corr(imi+1,1:2)=(corrvec+correlate(masterbb,im))/2;
    corr(imi+1,1:2)=correlate(masterbb,im);
  elseif dim==1
    corrvec=correlate1(masterbb,im);
    corr(imi+1,1:2)=[median(corrvec), 0];
  elseif dim==2
    corrvec=correlate1(masterbb',im');
    corr(imi+1,1:2)=[0, median(corrvec)];
  else
    gtError('Dimension (first input argument) should be [], 0, 1 or 2!')
  end
end


function sfDisplayImages(cmimi,masterblob,imblob,corr,clims_sample,clims_substract,mno_probl,imno_probl,dopause)

figure('name','Evaluation of calibration')
 set(gcf,'Units','normalized','Position',[0.1 0.1 0.8 0.8]);

 subplot(2,2,1)
  set(gca,'Units','normalized','OuterPosition',[0 0.5 0.5 0.5]);

 subplot(2,2,2)
  set(gca,'Units','normalized','OuterPosition',[0.5 0.5 0.5 0.5]);

 subplot(2,2,3)
  set(gca,'Units','normalized','OuterPosition',[0 0 0.5 0.5]);

 subplot(2,2,4)
  set(gca,'Units','normalized','OuterPosition',[0.5 0 0.5 0.5]);

for ii=1:length(cmimi)
  subplot(2,2,1)
   if cmimi(ii,3)==1
     imshow(fliplr(masterblob(:,:,cmimi(ii,2)+1)),clims_sample)
     title(sprintf('Master # %d flipped',cmimi(ii,2)));
   else
     imshow(masterblob(:,:,cmimi(ii,2)+1),clims_sample)
     title(sprintf('Master # %d',cmimi(ii,2)));
   end
   if any(cmimi(ii,2)==mno_probl)
     hold on
     plot([-20 size(masterblob,2)+20 size(masterblob,2)+20 -20 -20],[-20 -20 size(masterblob,1)+20 size(masterblob,1)+20 -20],'r-','linewidth',2);
     hold off
   end
   axis([-25 size(masterblob,2)+25 -25 size(masterblob,1)+25])
   set(gca,'position',[0.05 0.55 0.4 0.4])

  subplot(2,2,2)
   imshow(imblob(:,:,ii),clims_sample)
   if any(cmimi(ii,1)==imno_probl)
     hold on
     plot([-20 size(masterblob,2)+20 size(masterblob,2)+20 -20 -20],[-20 -20 size(masterblob,1)+20 size(masterblob,1)+20 -20],'r-','linewidth',2);
     hold off
   end
   title(sprintf('Image # %d',cmimi(ii,1)));
   axis([-25 size(masterblob,2)+25 -25 size(masterblob,1)+25])
   set(gca,'position',[0.55 0.55 0.4 0.4])

  subplot(2,2,3)
   plot(0:size(corr,1)-1,corr(:,2),'c.')
   hold on
   plot(0:size(corr,1)-1,corr(:,1),'b.')
   plot(cmimi(ii,1),corr(cmimi(ii,1)+1,2),'ms')
   plot(cmimi(ii,1),corr(cmimi(ii,1)+1,1),'ms')
   hold off
   title('Horizontal (cyan) and vertical (blue) drifts');
   xlim([0 size(corr,1)])
   set(gca,'position',[0.05 0.05 0.4 0.4])

  subplot(2,2,4)
   if cmimi(ii,3)==1
     mbb=fliplr(masterblob(:,:,cmimi(ii,2)+1));
   else
     mbb=masterblob(:,:,cmimi(ii,2)+1);
   end
   imbb=imblob(:,:,ii);
   imbb=interpolatef(imbb,corr(cmimi(ii,1)+1,1),corr(cmimi(ii,1)+1,2));
   imshow(mbb-imbb,clims_substract)
   title('Substraction of correlated images')
   set(gca,'position',[0.55 0.05 0.4 0.4])

  drawnow
  if dopause==1
    pause
  end
end

end % of sfDisplayImages

end % of function
