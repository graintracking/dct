
function gtFlipExtImages(first, last, workingdirectory)

if isdeployed
  global GT_DB
  global GT_MATLAB_HOME
  load('workspaceGlobal.mat');
  first=str2double(first);
  last=str2double(last);
end

if ~exist('workingdirectory','var')
  workingdirectory=pwd;
end

cd(workingdirectory)

for i=first:last
  
  disp(sprintf('flipping image:  ext%04d',i))
  
  
  im=edf_read(sprintf('1_preprocessing/ext/ext%04d.edf',i));
%  im=flipud(im);
  im=fliplr(im);
  edf_write(im, sprintf('1_preprocessing/ext/ext%04d.edf',i), 'float32')
  

end
