function gtAbsMedianLive(first, last, workingdirectory)
% GTABSMEDIANLIVE  Create the medians of the abs images as they appear
%     gtAbsMedianLive(first, last, workingdirectory)
%     ----------------------------------------------
%
%     Version 002 10-11-2011 by LNervo
%       Add comments and clean syntax and warnings
%
%     Version 001
%       adapt to the specific case of the abs
%       images (ie already flatfielded so no dark or refs needed, no intensity
%       correction needed)
%
%       add test to wait for images to be translated by gtApplyDrifts before
%       continuing - in pfWaitToRead
%       from prep.correct_drift
%       propagate edf header info to the new images created
%
%       bug - will look for (and use) image 7200 of a scan from 0-7199 images,
%       for example, where 7200 is an "image after the scan".  Change so that
%       7199 would be used, without disturbing output file numbering. ak-10/12/08
%
%

disp('gtAbsMedianLive.m')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% get parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% special case for running interactively in current directory
if (~exist('workingdirectory', 'var'))
    workingdirectory = pwd;
end
cd(workingdirectory);

parameters = gtLoadParameters();
prep = parameters.prep;

if (isdeployed)
    global GT_DB %#ok<TLEV,NUSED>
    global GT_MATLAB_HOME %#ok<TLEV,NUSED>
    load('workspaceGlobal.mat');
    first = str2double(first);
    last  = str2double(last);
end

nimages = gtAcqTotNumberOfImages(parameters);

% should we wait for images to be shifted by gtApplyDrifts?
waitfortranslated = false;
% if strcmp(prep.correct_drift, 'required')
%     waitfortranslated=parameters.prep.correct_drift_iteration;
% end

interval = prep.absint;
range    = prep.absrange;
indir    = fullfile('1_preprocessing', 'abs');
outdir   = fullfile('1_preprocessing', 'abs');
name     = 'abs';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% do abs median
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Problem: "first" should be a multiple of absint
if (mod(first, prep.absint))
    first = first - mod(first, prep.absint);
end

% Problem: some special processing in the beginning and at the end
% required - no symmetric median available
start = max(first - range / 2, 0);

% read the full stack only for the first image - in the following only one
% image is replaced in the stack
% in case we start from image 0, we use the median calculated from
% [0:interval:range] and write the same file for the first
% 0:interval:range/2 images

k = 0;
gtDBConnect();
for ii = start:interval:start+range
    k = k + 1;
    fname = fullfile(indir, [name sprintf('%04d', ii) '.edf']);
    medstack(k, :, :) = edf_wait_read(fname, [], waitfortranslated);
    info = edf_info(fname);
    info.datatype = 'float32';
end

if isfield(prep, 'use_average')
    med = squeeze(mean(medstack, 1));
else
    med = squeeze(median(medstack, 1));
end

% if we are in the special case of starting before range/2, we write out
% the same median image range/2/interval times
if (start == 0)
    for ii = start:interval:range/2
        fname = fullfile(outdir, ['med' sprintf('%04d', ii) '.edf']);    
        edf_write(med, fname, info);
        start = range/2 + interval;
    end
else
    fname = fullfile(outdir, ['med' sprintf('%04d', first) '.edf']);   
    edf_write(med, fname, info);
    start = first + interval;
end

% check if we are not running out of images
stop = min(last, nimages - range / 2);

% now process images form start:interval:stop
for ii = start:interval:stop
    k = k + 1;
    if (mod(k, range / interval + 1))
        k = mod(k, range / interval + 1);
    else
        k = range / interval + 1;
    end

    % scan runs from 0 to (nimages-1) - see header comment ak-10/12/08
    if ((ii + range / 2) == nimages)
        fname = fullfile(indir, [name sprintf('%04d', nimages - 1) '.edf']);   
    else
        fname = fullfile(indir, [name sprintf('%04d', ii + range / 2) '.edf']);           
    end
    medstack(k, :, :) = edf_wait_read(fname, [], waitfortranslated);
    info = edf_info(fname);
    info.datatype = 'float32';

    if isfield(prep, 'use_average')
        med = squeeze(mean(medstack, 1));
    else
        med = squeeze(median(medstack, 1));
    end

    fname = fullfile(outdir, ['med' sprintf('%04d', ii) '.edf']);    
    edf_write(med, fname, info);
end

% some special processing for the last range/2 images: median is no longer
% updated since no images > last available
for ii = stop+interval:interval:last
    fname = fullfile(outdir, ['med' sprintf('%04d', ii) '.edf']);      
    edf_write(med, fname, info);
end

end % end of function




