function gtCopy(fnamein, fnameout, fnameout_orig, parameters, move_data_flag)
% GTCOPY  Copies a file from fnamein to fnameout
%     gtCopy(fnamein, fnameout, fnameout_orig, parameters, move_data_flag, varargin)
%     ------------------------------------------------------------------------------
%     INPUT:
%       fnamein        = input file name
%       fnameout       = output file name
%       fnameout_orig  = output original file name
%       parameters     = parameters file
%       move_data_flag = flag for moving data
%
%     OUTPUT:
%       --none--
%
%     Version 002 28-11-2011 by LNervo
%       *** clean version ***
%
%     Version 001
%       Either move or copy the original file to fnameout_orig
%       Adds to edf header info according to parameters
%
%       Use move_data_flag=0 if the data is already in the 0_rawdata/Orig
%       directory, and shouldn't be moved again.
%
%       varargin - to pass in distortion file if required
%
%

    fprintf('gtCopy of "%s"\n', fnamein);

    [~, ~, extension] = fileparts(fnamein);
    switch (extension)
        case '.edf'
            % image corrections for edf files
            copyEdf(fnamein, fnameout, parameters);
        otherwise
            % non-images files: just copy
            try
                [~, msg] = copyfile(fnamein, fnameout); disp(msg);
            catch mexc
                gtPrintException(mexc);
                mexc1 = GtException('SETUP:copy_error', ...
                    'Not able to copy "%s" to: "%s".', fnamein, fnameout);
                mexc1 = mexc1.addCause(mexc);
                throw(mexc1);
            end
    end

    % move or copy the original file
    if (move_data_flag)
        try
            [~, msg] = movefile(fnamein, fnameout_orig); disp(msg);
        catch mexc
            % if no permission to mv, copy instead
            errorMsg = ['Not able to move "' fnamein '" to: "' fnameout_orig '".'];
            gtPrintException(mexc, errorMsg);
            disp('Trying to just copy...');
            try
                [~, msg] = copyfile(fnamein, fnameout_orig); disp(msg);
            catch mexc2
                gtPrintException(mexc2);
                mexc1 = GtException('SETUP:copy_error', ...
                    'Not able to move or copy "%s" to: "%s".', fnamein, fnameout);
                mexc1 = mexc1.addCause(mexc);
                mexc1 = mexc1.addCause(mexc2);
                throw(mexc1);
            end
        end
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Sub-functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function copyEdf(fnamein, fnameout, parameters)
    [img, info] = edf_wait_read(fnamein);

    % ccd specific corrections
    if (ismember(lower(parameters.acq.sensortype), {'kodak4mv1', 'frelon 4m', 'frelon hd4m'}))
        %add fields to info
        if ( (isfield(info, 'isccdcorrected') && strcmpi(info.isccdcorrected, 'true')) ...
                || (isfield(info, 'israw') && strcmpi(info.israw, 'false')) )
            disp('Kodak corrections already applied');
        else
            disp('applying Kodak chip corrections');
            info.ccdmodel = 'Kodak4Mv1';
            info.isccdcorrected = true;
            % do the kodak chip corrections
            if (parameters.acq.xdet == 2048 && parameters.acq.ydet == 2048)
                midline = round(parameters.acq.ydet/2);
                img = [img(1:midline,:); ...
                    mean([img(midline,:); img(midline+1,:)]); ...
                    img(midline+1:end-1,:) ];
                % im=im.^0.96;
            elseif (parameters.acq.xdet == 1024 && parameters.acq.ydet == 1024)
                disp('assuming 2x2 binning')
                % scale up, add the line, scale down
                img = imresize(img, 2);
                midline = 1024;
                img = [img(1:midline,:); ...
                    mean([img(midline,:); img(midline+1,:)]); ...
                    img(midline+1:end-1,:) ];
                img = imresize(img, 0.5);
            end
        end
    else
        %add fields to info
        info.ccdmodel = parameters.acq.sensortype;
    end

    % distortion correction
    if (strcmp(parameters.acq.distortion, 'none'))
        disp('No correction applied.');
    elseif (isfield(info, 'isundistorted') && strcmpi(info.isundistorted, true))
        disp('Image has already been corrected.');
    else
        try
            % get distortion map - passed in or read
            fprintf('Reading distortion map from file: %s\n', ...
                parameters.acq.distortion);
            detector_size = [];
            detector_size.x = parameters.acq.true_detsizeu;
            detector_size.y = parameters.acq.true_detsizev;
            if ( detector_size.x == parameters.acq.xdet ...
                    && detector_size.y == parameters.acq.ydet )
                fprintf('- Using full detector..');
                distortion = distortion_read(parameters.acq.distortion, detector_size);
            else
                fprintf('- Using a ROI of the detector (%d, %d)..', ...
                    parameters.acq.xdet, parameters.acq.ydet)
                % ROI is of the form [xmin ymin x_size y_size]
                roi = [ parameters.acq.detroi_u_off parameters.acq.detroi_v_off ...
                    parameters.acq.xdet parameters.acq.ydet ];
                distortion = distortion_read(parameters.acq.distortion, detector_size, roi);
            end
            fprintf(' Done.\nApplying distortion correction..');
            img = distortion_correct(img, distortion);
            info.isundistorted = true;
            fprintf(' Done.\n');
        catch mexc
            errorMsg = [ 'Not able to apply correction from file: "' ...
                parameters.acq.distortion '".' ];
            gtPrintException(mexc, errorMsg);
        end
    end

    % turn images for gt coordinate system
    if (isfield(parameters.acq, 'rotation_axis') ...
            && strcmp(parameters.acq.rotation_axis, 'horizontal') )
        if (~isfield(info, 'gtrotated') || strcmp(info.gtrotated, 'false'))
            if strcmpi(parameters.acq.rotation_direction, 'clockwise')
                disp('Rotating 90 degrees counterclockwise (horizontal rotation axis).');
                info.gtrotated = true;
                img = rot90(img, -1); % 90 degrees
            elseif strcmpi(parameters.acq.rotation_direction, 'counterclockwise')
                disp('Rotating 90 degrees clockwise (horizontal rotation axis).');
                info.gtrotated = true;
                img = rot90(img); % -90 degrees
            else
                disp('Options are: "counterclockwise" and "clockwise".');
            end
        end
    end

    % flip images for gt coordinate system
    if (parameters.acq.flip_images)
        if (~isfield(info, 'gtlrflipped') || strcmpi(info.gtlrflipped, 'false') || ~info.gtlrflipped)
            disp('Flipping LR for gt coordinate system.');
            info.gtlrflipped = true;
            img = fliplr(img);
        end
    else
        disp('Any flip has been applied to the images.')
    end

    % write the corrected image and info to the new location
    edf_write(img, fnameout, info);
end
