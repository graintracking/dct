function parameters = gtSetupCalibrationQuali(bbdir, showcorr, parameters)
% GTSETUPCALIBRATIONQUALI  Sets up calibration data and drifts with quali images.
%     parameters = gtSetupCalibrationQuali(bbdir,showcorr)
%     ----------------------------------------------------
%     Version 001
%       warning limit of inconsistency of the calibration method in pixels:
%       (difference between total horizontal sample drift during scan and
%       rotation axis drift from calibration to the end of scan)
%
%       allow user to input working directory, if data is being collected in one
%       place but will be copied to another.
%
%       lim_incon=0.05;
%
%

disp(' ')
disp('CALIBRATION WITH QUALI IMAGES')

if (~exist('bbdir','var'))
    bbdir = [];
end

if (~exist('showcorr', 'var') || (~islogical(showcorr) && isemtpy(showcorr)))
    showcorr = true;
end

if (~exist('parameters', 'var') || isempty(parameters))
    parameters = gtLoadParameters();
end

close all

% Calibration scan parameters
disp(' ')

calibdir_default = fullfile(fileparts(parameters.acq.dir), ...
    [parameters.acq.name 'calib_'], '0_rawdata', ...
    [parameters.acq.name 'calib_']);

disp('Type of calibration scan?');
parameters.calib.type =inputwdefault('180degree, 360degree or none','360degree');

if (any(strcmpi(parameters.calib.type, {'180degree', '360degree'})))
    parameters.calib.offset = inputwdefault('Image number in scan where corresponding calibration image is #0000 (positive or negative offset)','0');
    parameters.calib.offset = str2double(parameters.calib.offset);
    if (isfield(parameters.calib, 'dir'))
        calibdir_default = parameters.calib.dir;
    end
    parameters.calib.dir = inputwdefault('Enter absolute path of calibration scan: ', calibdir_default);
    parameters.calib.imagesource = inputwdefault('Which images shall be used for comparison: [scan / quali]: ', 'quali');
    if (strcmp(parameters.calib.imagesource, 'quali'))
        parameters.calib.images = 'quali';
    else
        parameters.calib.images = parameters.acq.name;
    end

    % Create dark.edf file of the calibration
    disp(' ')
    disp('Creating dark.edf image of calibration scan')
    [~, parameters.calib.name, ~] = fileparts(parameters.calib.dir);
    xmlfname_calib = fullfile(parameters.calib.dir, [parameters.calib.name '.xml']);
    if (~exist(xmlfname_calib,'file'))
        error('ACQ:no_xml_file', ...
            ['Cannot proceed - missing xml file of calibration scan: ' xmlfname_calib]);
    end
    xmlPrefs = []; xmlPrefs.Str2Num= 'never';
    xml_calib = xml_read(xmlfname_calib, xmlPrefs);
    xml_calib = xml_calib.acquisition;
    ndark_calib               = str2double(xml_calib.dark_N);
    parameters.calib.totproj  = str2double(xml_calib.tomo_N);
    parameters.calib.refon    = str2double(xml_calib.ref_On);
    parameters.calib.nref     = str2double(xml_calib.ref_N);
    dark_name = fullfile(parameters.calib.dir, 'dark.edf');
    if (~exist(dark_name, 'file'))
        darkend_name = fullfile(parameters.calib.dir, 'darkend0000.edf');
        [dark, info] = edf_read(darkend_name);
        dark = dark / ndark_calib;
        edf_write(dark, dark_name, info);
        disp(' ')
    end
    %refs=dir([parameters.calib.dir '/refHST*']);

    % Direct beam bb of qualis
    if (isempty(bbdir))
        bbdir = gtFindDirectBeamBBQuali(parameters.acq,parameters.calib);
        pause
    end
end

disp('In which dimension would you like to compute sample drifts?')
disp('( vertical=1, horizontal=2, both=0 ) ')
parameters.calib.correlationdim = inputwdefault('dimension of correlation:','0');
parameters.calib.correlationdim = str2double(parameters.calib.correlationdim);

parameters.calib.bb=bbdir;

if (strcmpi(parameters.calib.type, 'none'))
    % Try to find shift according to images at end of scan
    [ydrift_end,zdrift_end,rotaxisloc_abs_end,rotoff_abs_end,rotoff_rel_end]=gtDriftsEndOfScanQuali(parameters);

    disp(' ')
    disp('Sample drifts are computed on the basis of images at end of scan.')
    disp(' ')
    parameters.calib.driftsbase=3;
    parameters.calib.rotx=rotaxisloc_abs_end;

elseif (any(strcmpi(parameters.calib.type, {'180degree','360degree'})))
    % Evaluate calibration
    [ydrift_pol_calib,zdrift_pol_calib,ydrift_int_calib,zdrift_int_calib,rotaxisloc_abs_calib,...
        rotoff_abs_calib,rotoff_rel_calib,bbs_calib] = gtEvaluateCalibrationQuali(parameters.acq,parameters.calib,bbdir,showcorr);

    % Inconsistency of calibration: when there is a sample drift during the scan,
    % the rotation axis location is determined from ... ??? ....
    % drifted images at the end of scan, 'rotoff_abs_end' and rotoff_

%     totproj = gtAcqTotNumberOfImages(parameters);

%       incon=abs((ydrift_end(totproj+1)-ydrift_end(1))+(rotoff_abs_end-rotoff_abs_calib));
%       if incon>lim_incon
%         disp('WARNING! Inconsistency of calibration method is above the preset limit.')
%       end
%     
%       % Display results
%       format short g
%     
%       disp(' ')
%       disp('-----------------------------------------------------------')
%       disp(' ')
%       disp('IMAGES AT END  and  CALIBRATION  comparison:')
%       disp('Maximum absolute value of horizontal sample drift: ')
%       disp([max(abs(ydrift_end)) max(abs(ydrift_int_calib))])
%       disp('Mean value of horizontal sample drift: ')
%       disp([mean(ydrift_end) mean(ydrift_int_calib)])
%       disp('Maximum value of vertical sample drift: ')
%       disp([max(zdrift_end) max(zdrift_int_calib)])
%       disp('Mean value of vertical sample drift: ')
%       disp([mean(zdrift_end) mean(zdrift_int_calib)])
%       disp('Rotation axis offset relative to center of direct beam bb:')
%       disp([rotoff_rel_end rotoff_rel_calib])
%       disp('Rotation axis offset relative to center of full images:')
%       disp([rotoff_abs_end rotoff_abs_calib])
%       disp('Rotation axis location in full images:')
%       disp([rotaxisloc_abs_end rotaxisloc_abs_calib])
%       disp('Inconsistency of calibration:')
%       disp(incon)
%       disp(' ')
%       disp('-----------------------------------------------------------')
%       disp(' ')
%     
    figure('name','Sample drifts according to images at end and calibration')
    subplot(2,1,1)
    title('horizontal drift');
    hold on
    %plot(ydrift_end,'b.')
    plot(ydrift_pol_calib,'g.')
    plot(ydrift_int_calib,'k-')

    subplot(2,1,2)
    title('vertical drift');
    hold on
    %plot(zdrift_end,'b.')
    plot(zdrift_pol_calib,'g.')
    plot(zdrift_int_calib,'k-')

    disp('On what would you like to base sample drifts computation?')
    disp('    1. polynomial fit from calibration scan')
    disp('    2. interpolated values from calibration scan')
    disp('    3. interpolated values from images at end of scan')
    %disp('    4. average 1. and 3. ?')
    parameters.calib.driftsbase = inputwdefault(' Drifts based on','1');
    parameters.calib.driftsbase = str2double(parameters.calib.driftsbase);
    parameters.calib.rotx = rotaxisloc_abs_calib;
end

% Use one of the methods
switch (parameters.calib.driftsbase)
    case 1
        parameters.calib.driftsbase='calib_pol';
        ydrift=ydrift_pol_calib;
        zdrift=zdrift_pol_calib;
    case 2
        parameters.calib.driftsbase='calib_int';
        ydrift=ydrift_int_calib;
        zdrift=zdrift_int_calib;
    case 3
        parameters.calib.driftsbase='images_at_end';
        ydrift=ydrift_end;
        zdrift=zdrift_end;
    case 4
        parameters.calib.driftsbase='average';
        ydrift=(ydrift_pol_calib+ydrift_end)/2;
        zdrift=(zdrift_pol_calib+zdrift_end)/2;
end

parameters.calib.ydrift = ydrift;
parameters.calib.zdrift = zdrift;

gtSaveParameters(parameters)
disp('Calibration data and drifts saved in parameters file.')

end %end function

