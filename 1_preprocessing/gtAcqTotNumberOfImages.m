function totproj = gtAcqTotNumberOfImages(parameters, det_ind)
    if (~exist('det_ind', 'var') || isempty(det_ind))
        det_ind = 1;
    end

    if (~ismember(lower(parameters.acq(det_ind).type), {'180degree', '360degree', 'dct', 'topotomo'}))
        error('gtAcqTotNumberOfImages:wrong_argument', ...
            'This is not a DCT or Topotomo acquisition!')
    end

    num_det = numel(det_ind);
    totproj = zeros(num_det, 1);
    for ii_d = 1:num_det
        totproj(ii_d) = get_imgs(parameters, det_ind(ii_d));
    end
end

function totproj = get_imgs(parameters, det_index)
    switch (lower(parameters.acq(det_index).type))
        case {'360degree', 'dct', 'topotomo'}
            totproj = 2 * parameters.acq(det_index).nproj;
        case {'180degree'}
            totproj = parameters.acq(det_index).nproj;
        otherwise
            error('ACQ:invalid_parameter', ...
                'Unknown type of scan. Check parameters.acq.type!');
    end
end
