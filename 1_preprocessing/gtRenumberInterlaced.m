function gtRenumberInterlaced(parameters)

if (~exist('parameters', 'var') || isempty(parameters))
    parameters = gtLoadParameters();
end

% initial checks
if (parameters.acq.interlaced_turns == 0)
    disp('Not an interlaced scan!')
    return
end

interlaced_scan = parameters.acq.interlaced_turns+1;

totproj = gtAcqTotNumberOfImages(parameters);
acq_dir = parameters.acq.dir;

imgs_per_turn = totproj / (interlaced_scan);

if (isfield(parameters.prep, 'renumbered') && parameters.prep.renumbered == true)
    disp('The interlaced renumbering has already been done! ');
    return
else
    if length(dir([acq_dir '/1_preprocessing/full/full*edf']))<totproj
        disp('not all of the full images exist!')
    else
        % do renumbering
        parameters.prep.renumbered = true;
        disp('The interlaced renumbering will be processed now! ');
        save([acq_dir '/parameters.mat'], 'parameters');
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% renumber full images
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
cd('1_preprocessing/full');

disp(' moving original full ...');
% intermediate - move to a tmp directory
mkdir('tmp');
% have to do this 1000 images at a time - faster than individually
for ii = 0:floor(totproj/1000)
    orig = sprintf('full%d*.edf', ii);
    [~, msg] = movefile(orig, 'tmp'); disp(msg)
    clear s msg
end

% final renumbering
disp(' renaming full ...');
for ii = 0:totproj-1
    orig = sprintf('tmp/full%04d.edf', ii);
    j = interlaced_scan * mod(ii, imgs_per_turn) + floor(ii / imgs_per_turn);
    dest = sprintf('full%04d.edf', j);
    [~, msg] = movefile(orig, dest); disp(msg)
    clear s msg
end

% clean-up tmp directory
check = dir('tmp/*.edf');
if (isempty(check))
    delete('tmp');
end
cd(acq_dir);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% renumber ext images
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if (exist('1_preprocessing/ext/ext0000.edf', 'file'))
    cd('1_preprocessing/ext');
    disp('copying original ext ...');
    % intermediate - move to a tmp directory
    mkdir('tmp');
    % have to do this 1000 images at a time - faster than individually
    for ii = 0:floor(totproj/1000)
        orig = sprintf('ext%d*.edf', ii);
        [~, msg] = movefile(orig, 'tmp'); disp(msg)
        clear s msg
    end

    % final renumbering
    disp(' renaming ext ...');
    for ii = 0:totproj-1
        orig = sprintf('tmp/ext%04d.edf', ii);
        j = interlaced_scan*mod(ii, imgs_per_turn)+floor(ii/imgs_per_turn);
        dest = sprintf('ext%04d.edf', j);
        [~, msg] = movefile(orig, dest); disp(msg)
        clear s msg
    end

    % clean-up tmp directory
    check = dir('tmp/*.edf');
    if (isempty(check))
        delete('tmp');
    end
    cd(acq_dir);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% renumber abs images
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if (exist('1_preprocessing/abs/abs0000.edf', 'file'))
    cd('1_preprocessing/abs');
    % intermediate - move to a tmp directory
    mkdir('tmp');
    % have to do this 1000 images at a time - faster than individually
    for ii = 0:floor(totproj/1000)
        orig = sprintf('abs%d*.edf', ii);
        [~, msg] = movefile(orig, 'tmp'); disp(msg)
        clear s msg    
    end

    % final renumbering
    disp('renaming abs ...');
    for ii = 0:totproj-1
        orig = sprintf('tmp/abs%04d.edf', ii);
        j = interlaced_scan*mod(ii, imgs_per_turn)+floor(ii/imgs_per_turn);
        dest = sprintf('abs%04d.edf', j);
        [~, msg] = movefile(orig, dest); disp(msg)
        clear s msg    
    end

    % clean-up tmp directory
    check = dir('tmp/*.edf');
    if (isempty(check))
        delete('tmp');
    end
    cd(acq_dir);
end

end %end of function