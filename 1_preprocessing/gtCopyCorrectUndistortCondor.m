function gtCopyCorrectUndistortCondor(~, ~, workingdirectory)
% GTCOPYCORRECTUNDISTORTCONDOR  Handles the parallelisation
%     gtCopyCorrectUndistortCondor(first, last, workingdirectory)
%     -----------------------------------------------------------
%     Version 002 26-11-2011 by LNervo
%       Comments and clean version.
%     Sub-functions:
%[sub]- sfRename
%
%     Version 001 by AKing
%       gtCopy does the business of copying/correcting/undistorting
%       talking to the database to
%       coordinate copying between multiple condor jobs.
%
%       all filenames, directory names, and paths are determined from the
%       parameters file
%
%       first and last are dummy variables - not used by the function.
%

if isdeployed
    global GT_MATLAB_HOME %#ok<TLEV,NUSED>
    global GT_DB %#ok<TLEV,NUSED>
    load('workspaceGlobal.mat')
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   load and read data from parameters.mat
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
cd(workingdirectory)
parameters = gtLoadParameters();

source_dir    = parameters.acq.collection_dir;
dest_dir      = fullfile(parameters.acq.dir, '0_rawdata', parameters.acq.name);
dest_dir_orig = fullfile(parameters.acq.dir, '0_rawdata', 'Orig');

%database connect
gtDBConnect('graindb.esrf.fr', 'gtadmin', 'gtadmin', 'graintracking');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   check directories are okay
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if (~exist(source_dir, 'dir'))
    gtError('Cannot find source directory!')
end
% These two directory creations should be redundant
if (~exist(dest_dir, 'dir')), mkdir(dest_dir); end
if (~exist(dest_dir_orig, 'dir')), mkdir(dest_dir_orig); end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   has the data already been moved to analysis directory?
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% determine if we need to move the data from the collection directory to the analysis location.
% If sourcedir is "xxx/0_rawdata/Orig", and destinationdir is "xxx/0_rawdata/scanname"
% then data has already been moved (ie sourcedir==destdir_orig)
%
% directory formats are
% either
% /data/id19/graintracking/ ..path.. /scanname/
% or
% /mntdirect/_data_id19_graintracking/ ..path../scanname/
%
% therefore need two tests

% are paths the same?
% what is the source dir name?
[path1, source_dirname, ~] = fileparts(source_dir);
[path2,              ~, ~] = fileparts(dest_dir);

move_data_flag = ~(strcmp(path1, path2) || strcmp(source_dirname, 'Orig'));
if (move_data_flag)
    disp('Not moving data - it is already in the analysis location')
else
    disp('Moving (or copying) data to the analysis location')
end

filetable = [parameters.acq.name '_filetable'];
base_query = sprintf('insert into %s (filename) values ("%%s")', filetable);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   copying routine
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp('Copying raw images...')
counter = 0;
should_stop = false;
% while loop for function timeout when copying is finished
while (~should_stop)
    found = false;

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %   get the list of files to try and copy
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % source directory file list
    src_files = dir(fullfile(source_dir, '*'));
    copied_files = dir(fullfile(dest_dir, '*'));

    % go through the source list, remove directories, parameters.mat,
    filter_names = [{copied_files.('name')}, 'parameters.mat'];
    files_to_keep = ~ismember({src_files(:).name}, filter_names);
    files_to_keep = files_to_keep & ~[src_files(:).isdir];
    src_files = src_files(files_to_keep);

    % sort files by date order to copy in order
    [~, indx] = sort( [src_files.('datenum')] );

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %   try to copy files
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    for n = reshape(indx, [1 numel(indx)])
        sfname = src_files(n).name;
        query = sprintf(base_query, sfname);

        %check if sfname has already been taken by another condor job
        try
            test = mym(query);
            % Andy 18/03/2012 - fixed this: job should not die if try fails,
            % but test should be false
        catch mexc
            if (strncmpi(mexc.message, 'duplicate', 9))
                fprintf('%s, skipping...\n', mexc.message)
                test = false;
            else
                % If not a duplicate entry, this is a more serious error!
                rethrow(mexc)
            end
        end

        if (test) % no other job is copying this file, do copy
            disp(['working on ' sfname])
            fname_in = fullfile(source_dir, sfname);
            fname_out_orig = fullfile(dest_dir_orig, sfname);

            if (isfield(parameters.acq, 'interlaced_turns') ...
                    && (parameters.acq.interlaced_turns > 0) )
                %if using interlaced turns, may need some renaming
                try
                    % determine how file should be renamed
                    newfname = edf_rename(sfname, parameters);
                    fnameout = fullfile(dest_dir, newfname);
                catch mexc
                    gtPrintException(mexc)
                    fnameout = fullfile(dest_dir, sfname);
                end
            else % standard routine
                fnameout = fullfile(dest_dir, sfname);
            end

            try
                gtCopy(fname_in, fnameout, fname_out_orig, parameters, move_data_flag);
                found = true;
            catch mexc
                if gtCheckExceptionType(mexc, 'EDF:')
                    message = [ 'Skipping badly formed file: "' fname_in ...
                        '" (' mexc.identifier ')'];
                    gtPrintException(mexc, message);
                else
                    gtPrintException(mexc, 'Error in the code!!');
                    rethrow(mexc)
                end
            end
        end
    end %for loop over the file list, working in date order

    % Checking for stopping conditions
    if (isfield(parameters.acq, 'online') && parameters.acq.online)
        if (~found)
            counter = counter + 1;
            pause(10) % have a rest, wait before looking again
            disp('sleeping 10, then look again')
        else
            counter = 0;
        end

        % Check if we waited more than 16 minutes for new images that didn't
        % come
        should_stop = counter >= 100;
    else
        should_stop = true;
    end
end
disp('Timed out while waited for files to appear... quitting')

end
