function [ydrift_pol,zdrift_pol,varargout]=gtEvaluateCalibrationQuali(acq,calib,bbox,showcorr)
%
% FUNCTION [ydrift_pol,zdrift_pol,varargout]=gtEvaluateCalibrationScan2(acq,bbox)
%
% In bbox, treats the calibration scan, determines rotation axis location
% at sub-pixel (in full image and in bbox), creates master images as references
% to sample drifts, determines accurate sample bounding box for the total scan
% inside bbox.
%
% Sample drifts are relative to constant reference positions, where
% reference positions have been determined the following way:
% Assuming that during calibration scan the sample doesn't move relative to
% the rotation table, the rotation axis is determined from image pairs 180
% degrees offset. Master images are created from the calibration in a way
% that they are translated to have the rotation axis in the very center of
% the full image (even if working in an asymmetric bbox).
% Images of the scan are correlated (horizontally and/or vertically) with
% those masters, yielding 'ydrift' and 'zdrift'.
%
% Uses linear flat correction between ref0000_xxxx-s.
%
% INPUT
%   acq = acquisition parameters of scan
%   bbox = according to GT conventions [originx, originy, width, height]
%            bbox doesn't need to be centered in full image
%            if acq.correlationdim=1 bbox(4) needs to be even number (for correlate1)
%            if acq.correlationdim=2 bbox(3) needs to be even number (for correlate1)
%
% OUTPUT
%   ydrift_pol, zdrift_pol = polynomial values with which the images need to be directly
%                             translated (e.g direct input for interpolatef)
%   varargout{1} = ydrift_int;  interpolated values of ydrifts
%   varargout{2} = zdrift_int;  interpolated values of zdrifts
%   varargout{3} = rotaxisloc_abs;  rot axis x (horizontal) location in full image
%                   (non-integer in pixels)
%   varargout{4} = rotdev_abs;  rot axis offset in full image; vector pointing
%                   from center of image to rotation axis location;
%                   positive to the right
%   varargout{5} = rotdev;  rot axis offset in bbox; vector pointing from center
%                   of bbox to rotation axis location in bbox, positive to the right
%   varargout{6} = bbs_ext;  bounding box of maximum fill by sample  during
%                   rotation extended horizontally by ext_pix, in full image
%   varargout{7} = edges;  bounding box of maximum fill by sample in bbox according to 'lim_samatten'
%   varargout{8} = edges_ext;  'edges' extended by 'ext_pix' (at most up to the edges of bbox)
%
% 07/2007 Wolfgang, Peter
%

%% Parameters
tol_pol=1; % tolerance factor for polynomial outliers (in unit std)
%polyorder_outliers=18; % order of polynomial for rejecting outliers of drift curves
polyorder_driftcurve=18; % order of polynomial of drift curves
tol_rotdev=0.5; % tolerance for deviation in rotation axis location found by different pairs (used for warning)
lim_samatten=0.7; % limit to describe sample attenuation
ext_pix=5; % sample bounding box extension in pixels
frame=20; % frame extension for reading master files to be able to translate them

clims_substract=[-0.05 0.05]; % color limits for substracted sample image display
clims_sample=[0 1]; % color limits for sample display


qualiname = calib.images;

%qualirefname='quali_ref_';   % this is the reference image name when
%expert setting open slits for quali images is active
%for quali-images...
qualirefname='ref0000_';      % option disables - we should modify fasttomo SPEC to always take a quali_ref image in order to avoid this conflict
%% Prepare variables

disp(' ')
disp('Evaluating calibration scan...')
disp(' ')

% Check if acquisition types and calibration offset make sense
if strcmpi(acq.type,'360degree')
  totproj=acq.nproj*2;
  if calib.offset<0 || calib.offset>=totproj
    disp(sprintf('Calibration offset has to be >=0 and <%d',totproj))
    gtError('Invalid calibration offset value!')
  end
elseif strcmpi(acq.type,'180degree')
  totproj=acq.nproj;
  if calib.offset<0 || calib.offset>=totproj
    disp(sprintf('Calibration offset has to be >=0 and <%d',2*totproj))
    gtError('Invalid calibration offset value!')
  end
else
  gtError('Unknown type of scan. Check acq.type!');
end

if ~strcmpi(calib.type,'180degree') && ~strcmpi(calib.type,'360degree')
  gtError('Unknown type of calibration scan (see calib.type)!')
end

close all

if ~exist('showcorr','var')
  showcorr=true;
end

tomo_N=calib.totproj; % =xml.acquisition.tomo_N
ref_N=calib.nref;    % =xml.acquisition.ref_N
dim=calib.correlationdim;
scanname=acq.name;
scanfolder=sprintf('%s/0_rawdata/%s',acq.dir,scanname);

% decrease frame value if bbread would extend over the detector size
frame=min([acq.xdet-(bbox(1)+bbox(3)-1),acq.ydet-(bbox(2)+bbox(4)-1),frame]);
% BB for read-out (wider so that no need for padding after translation)
bbread=[bbox(1)-frame,bbox(2)-frame,bbox(3)+2*frame,bbox(4)+2*frame];
%bbread(1:2)=max([1 1],bbread(1:2));
%bbread(3:4)=min([acq.xdet-bbread(1)+1 acq.ydet-bbread(2)+1],bbread(3:4));

%% Determine rotation axis offset

detcenter_abs=acq.xdet/2+0.5;
bboxcenter_abs=(2*bbox(1)+bbox(3)-1)/2;

% Darks and ref-s
cdark=edf_read(sprintf('%s/dark.edf',calib.dir),bbread,'nodisp');
cdarkbb=cdark(1+frame:end-frame,1+frame:end-frame);

% refstart=edf_read(sprintf('%s/ref0000_0000.edf',calib.dir),bbread,'nodisp');
% for i=1:ref_N-1
%   refstart=refstart+edf_read(sprintf('%s/ref%0.4d_0000.edf',calib.dir,i),bbread,'nodisp');
% end
% refstart=refstart/ref_N-cdark;

%  refstart=[];
%  refstart(:,:,1)=edf_read(sprintf('%s/ref0000_0000.edf',calib.dir),bbread,'nodisp');
%  for i=1:ref_N-1
%    refstart(:,:,i+1)=edf_read(sprintf('%s/ref%0.4d_0000.edf',calib.dir,i),bbread,'nodisp');
%  end
%  refstartmed=median(refstart,3)-cdark;

% refend=edf_read(sprintf('%s/ref0000_%0.4d.edf',calib.dir,tomo_N),bbread,'nodisp');
% for i=1:ref_N-1
%   refend=refend+edf_read(sprintf('%s/ref%0.4d_%0.4d.edf',calib.dir,i,tomo_N),bbread,'nodisp');
% end
% refend=refend/ref_N-cdark;

refstart=edf_read(sprintf('%s/refHST0000.edf',calib.dir),bbread,'nodisp')-cdark;
refstartbb=refstart(1+frame:end-frame,1+frame:end-frame);

refend=edf_read(sprintf('%s/refHST%0.4d.edf',calib.dir,tomo_N),bbread,'nodisp')-cdark;
refendbb=refend(1+frame:end-frame,1+frame:end-frame);

imend180=(edf_read(sprintf('%s/%s%0.4d.edf',calib.dir,calib.name,tomo_N+2),bbox,'nodisp')-cdarkbb)./refendbb;
imend0=(edf_read(sprintf('%s/%s%0.4d.edf',calib.dir,calib.name,tomo_N),bbox,'nodisp')-cdarkbb)./refendbb;
imend0=fliplr(imend0);

% Determine rotdev: that is a vector of [bbox center to rotation axis
%  location in the bbox], positive to the right.
rotdev1=median(correlate1(imend180',imend0'))/2;

% Confirm with Findshifts and add auto and manual results
imforfs=interpolatef(imend0,0,rotdev1*2);
shifts=sfConfirmWithFindshifts(imend180,imforfs);
rotdev1=shifts(2)/2+rotdev1;

if strcmpi(calib.type,'360degree')
  % Deal with other end pair
  imend270=(edf_read(sprintf('%s/%s%0.4d.edf',calib.dir,calib.name,tomo_N+3),bbox,'nodisp')-cdarkbb)./refendbb;
  imend90=(edf_read(sprintf('%s/%s%0.4d.edf',calib.dir,calib.name,tomo_N+1),bbox,'nodisp')-cdarkbb)./refendbb;
  imend90=fliplr(imend90);

  rotdev2=median(correlate1(imend270',imend90'))/2;

  disp('Rotation axis location in bounding box found by first pair:')
  disp(rotdev1)
  disp('Rotation axis location in bounding box found by second pair:')
  disp(rotdev2)

  % Confirm with Findshifts and add auto and manual results
  imforfs=interpolatef(imend90,0,rotdev2*2);
  shifts=sfConfirmWithFindshifts(imend270,imforfs);
  rotdev2=shifts(2)/2+rotdev2;

  if abs(rotdev1-rotdev2)>tol_rotdev
    disp('WARNING! Incosistent values have been found for rotation axis location in calibration scan.')
    disp('Check images at end in calibration scan!')
    pause(3)
  end

  % Compute rotdev from all the calib pairs
  rotdev=nfLocateRotAxis;
end

% vector of rotation axis location in full image; [from image center
% to rotation axis location], positive to the right;
rotdev_abs = bboxcenter_abs-detcenter_abs+rotdev;
rotaxisloc_abs = bboxcenter_abs+rotdev;


%% Compute drifts

% Create masters
[sf,masterblob] = nfCreateMasterblob();

% Depending on scan type and calibration type (3 cases): matching the corresponding
% pairs, flipping master images if needed, and creation of master and image blobs.
[imblob,cmimi,corr] = nfCorrelateMastersAndScan();

% Polynomial and interpolated values of drifts
[ydrift_pol,zdrift_pol,ydrift_int,zdrift_int]=nfComputePolynomials;

% Check bounding box fill
imno_probl=nfScanBBfill;
[mno_probl,minrow,mincol,masterblob_min3]=nfMasterBBfill;


%% Display

if showcorr
  sfDisplayImages(cmimi,masterblob,imblob,corr,clims_sample,clims_substract,mno_probl,imno_probl,0)
end

showims=inputwdefault('Would you like to loop through the scan images?',showims);

if strcmpi(showims,'y') || strcmpi(showims,'yes')
  disp('Check master and scan images...')
  disp('Press enter to loop through images')
  sfDisplayImages(cmimi,masterblob,imblob,corr,clims_sample,clims_substract,mno_probl,imno_probl,1)
end

[bbs_ext,edges,edges_ext] = nfCreateSampleBoundingBox();

output{1}=ydrift_int;
output{2}=zdrift_int;
output{3}=rotaxisloc_abs;
output{4}=rotdev_abs;
output{5}=rotdev;
output{6}=bbs_ext;
output{7}=edges;
output{8}=edges_ext;

nout=max(nargout,2)-2;
for jj=1:nout
  varargout{jj}=output{jj};
end

%disp('End of gtEvaluateCalibratonQuali.')
%keyboard



%% Sub- and nested functions


%% Confirm with findshifts

function shifts=sfConfirmWithFindshifts(baseim,imforfs)

  shift=[];
  assignin('base','shift',shift);
  disp(' ')
  disp('Calibration scan: check result of image correlation with findshifts...')
  disp(' ')

  wdefault=warning;
  warning('off','all');

  gtFindShifts(baseim,imforfs);

  warning(wdefault);

  set(gcf,'name','Calibration scan: check correlation...')
  while isempty(shift)
    drawnow;
    shift=evalin('base','shift');
  end

  shifts=evalin('base','shift');

end % of sfConfirmWithFindshifts


%% Check rotation axis defined by all the pairs

  function [rotdev_mean,rotaxisloc_abs_mean]=nfLocateRotAxis

    nofimpairs=tomo_N/2;
    rotdev_vec=[];

    if nofimpairs==round(nofimpairs)
      disp(' ')
      disp('Analysing location of rotation axis during calibration...')
      disp(' ')
      for ii=0:nofimpairs
        refA=(tomo_N-ii)/tomo_N*refstartbb+ii/tomo_N*refendbb;
        refB=(tomo_N/2-ii)/tomo_N*refstartbb+(ii+tomo_N/2)/tomo_N*refendbb;

        imA=(edf_read(sprintf('%s/%s%0.4d.edf',calib.dir,calib.name,ii),bbox,'nodisp')-cdarkbb)./refA;
        imB=(edf_read(sprintf('%s/%s%0.4d.edf',calib.dir,calib.name,ii+nofimpairs),bbox,'nodisp')-cdarkbb)./refB;
        imB=fliplr(imB);

        rotdev_vec(ii+1)=median(correlate1(imA',imB'))/2;
      end
    else
      disp(' ')
      disp('Odd number of radiographs in calibration.')
      disp('Can''t analyse rotation axis location by image pairs through all the scan.')
      disp(' ')
    end

    % values of rotation axis location in full image;
    % [from image center to rotation axis location], positive to the right;
    rotdev_mean=mean(rotdev_vec);
    %rotdev_abs_vec=bboxcenter_abs-detcenter_abs+rotdev_vec;
    rotaxisloc_abs_vec=bboxcenter_abs+rotdev_vec; % locations of rotation axis in full image
    rotaxisloc_abs_mean=mean(rotaxisloc_abs_vec); % mean value for location of rotation axis in full image

    figure('name','Location of the rotation axis during calibration')
    hold on
    plot(0:tomo_N/2,rotaxisloc_abs_vec,'b.')
    plot(0:tomo_N/2,rotaxisloc_abs_vec,'b-')
    plot([0 tomo_N/2],[rotaxisloc_abs_mean rotaxisloc_abs_mean],'g-')
    drawnow

  end % of nfLocateRotAxis


%% Create masters

  function [sf, masterblob] = nfCreateMasterblob()

    if strcmpi(acq.type,'360degree')
      if strcmpi(calib.type,'360degree')
        sf=totproj/tomo_N;
      elseif strcmpi(calib.type,'180degree')
        sf=totproj/tomo_N/2;
      else
        gtError('Unknown type of calibration scan (see parameter file)!')
      end
    elseif strcmpi(acq.type,'180degree')
      if strcmpi(calib.type,'360degree')
        sf=totproj/tomo_N*2;
      elseif strcmpi(calib.type,'180degree')
        sf=totproj/tomo_N;
      else
        gtError('Unknown type of calibration scan (see parameter file)!')
      end
    else
      gtError('Unknown type of scan (see parameter file acq.type)!')
    end

    if mod(sf,1)~=0
      gtError('No. of images in scan and calibration do not correspond!')
    end

    disp(' ')
    disp('Creating master images...')
    disp(' ')

    masterblob=zeros(bbox(4),bbox(3),tomo_N+1,'single');

    for ii=0:tomo_N
      cref=(tomo_N-ii)/tomo_N*refstart+ii/tomo_N*refend;
      master=(edf_read(sprintf('%s/%s%0.4d.edf',calib.dir,calib.name,ii),bbread,'nodisp')-cdark)./cref;
      master=interpolatef(master,0,-rotdev);
      masterblob(:,:,ii+1)=master(1+frame:end-frame,1+frame:end-frame);
      edf_write(masterblob(:,:,ii+1),sprintf('%s/master_%0.4d.edf',calib.dir,ii),'float32');
    end

  end % of nfCreateMasterblob


%% Correlation of masters and scan; creation of scan image blob
% cmimi=[imi mi isflipped];
%   imi = image index
%   mi = corresponding master index

  function [imblob,cmimi,corr]=nfCorrelateMastersAndScan

    imblob=zeros(bbox(4),bbox(3),tomo_N+1,'single');
    corr=NaN(totproj+1,2);
    cmimi=[];

    imdark=edf_read(sprintf('%s/dark.edf',scanfolder),bbox,'nodisp');

    if strcmpi(calib.type,'180degree') && strcmpi(acq.type,'180degree')
      for imi=0:sf:totproj

        if abs((imi-calib.offset)/sf)==tomo_N
          mi=tomo_N;
        else
          mi=mod((imi-calib.offset)/sf,tomo_N);
        end

        masterbb=masterblob(:,:,mi+1);

        if (imi>=calib.offset) || (imi<=calib.offset-totproj) || (imi==totproj && mi==0)
          fl=[];
          cmimi=[cmimi; imi mi 0];
        else
          fl='flipped';
          cmimi=[cmimi; imi mi 1];
          masterbb=fliplr(masterbb);
        end

        disp(sprintf(' Correlating: image %d with master %d %s',imi,mi,fl))
        [corr,imbb]=sfCorrMaster(corr,scanfolder,qualiname,qualirefname,imi,acq,bbox,imdark,masterbb,dim);
        imblob(:,:,imi/sf+1)=imbb;
      end
    end


    if strcmpi(calib.type,'180degree') && strcmpi(acq.type,'360degree')
      for imi=0:sf:totproj

        if abs((imi-calib.offset)/sf)==tomo_N
          mi=tomo_N;
        else
          mi=mod((imi-calib.offset)/sf,tomo_N);
        end

        masterbb=masterblob(:,:,mi+1);

        if (imi>=calib.offset && imi<=calib.offset+totproj/2) || (imi<=calib.offset-totproj/2) || (imi==totproj && mi==0)
          fl=[];
          cmimi=[cmimi; imi mi 0];
        else
          fl='flipped';
          masterbb=fliplr(masterbb);
          cmimi=[cmimi; imi mi 1];
        end

        disp(sprintf(' Correlating: image %d with master %d %s',imi,mi,fl))
        [corr,imbb]=sfCorrMaster(corr,scanfolder,qualiname,qualirefname,imi,acq,bbox,imdark,masterbb,dim);
        imblob(:,:,imi/sf+1)=imbb;
      end
    end

    if strcmpi(calib.type,'360degree')
      for imi=0:sf:totproj
        mi=mod((imi-calib.offset)/sf,tomo_N);

        if mi==0 && imi==totproj
          mi=tomo_N;
        end

        masterbb=masterblob(:,:,mi+1);

        cmimi=[cmimi; imi mi 0];

        disp(sprintf(' Correlating: image %d with master %d',imi,mi))

        [corr,imbb]=sfCorrMaster(corr,scanfolder,qualiname,qualirefname,imi,acq,bbox,imdark,masterbb,dim);
        imblob(:,:,imi/sf+1)=imbb;
      end
    end

  end % of nfCorrelateMastersAndScan


%% Correlate a given master-scanimage pair

function [corr,im]=sfCorrMaster(corr,scanfolder,scanname,refname,imi,acq,bbox,imdark,masterbb,dim)
  %imrefa=edf_read(sprintf('%s/ref0000_%0.4d.edf',scanfolder,floor(imi/acq.refon)*acq.refon),bbox,'nodisp')-imdark;
  %imrefb=edf_read(sprintf('%s/ref0000_%0.4d.edf',scanfolder,ceil((imi-eps)/acq.refon)*acq.refon),bbox,'nodisp')-imdark;
  %imref=(acq.refon-mod(imi,acq.refon))/acq.refon*imrefa+mod(imi,acq.refon)/acq.refon*imrefb;

  imrefa=edf_read(sprintf('%s/%s%0.4d.edf',scanfolder,refname,floor(imi/acq.refon)*acq.refon),bbox,'nodisp')-imdark;
  imrefb=edf_read(sprintf('%s/%s%0.4d.edf',scanfolder,refname,ceil((imi-eps)/acq.refon)*acq.refon),bbox,'nodisp')-imdark;
  imref=(acq.refon-mod(imi,acq.refon))/acq.refon*imrefa+mod(imi,acq.refon)/acq.refon*imrefb;

  im=(edf_read(sprintf('%s/%s%0.4d.edf',scanfolder,scanname,imi),bbox,'nodisp')-imdark)./imref;

  if dim==0 || isempty(dim)
    %corrvec=correlaterealspace(masterbb,im,20,20);
    %corr(imi+1,1:2)=(corrvec+correlate(masterbb,im))/2;
    corr(imi+1,1:2)=correlate(masterbb,im);
  elseif dim==1
    corrvec=correlate1(masterbb,im);
    corr(imi+1,1:2)=[median(corrvec), 0];
  elseif dim==2
    corrvec=correlate1(masterbb',im');
    corr(imi+1,1:2)=[0, median(corrvec)];
  else
    gtError('Dimension (first input argument) should be [], 0, 1 or 2!')
  end
end

%% Display correlated images in loop

function sfDisplayImages(cmimi,masterblob,imblob,corr,clims_sample,clims_substract,mno_probl,imno_probl,dopause)

  figure('name','Evaluation of calibration')
  set(gcf,'Units','normalized','Position',[0.1 0.1 0.8 0.8]);

  subplot(2,2,1)
  set(gca,'Units','normalized','OuterPosition',[0 0.5 0.5 0.5]);

  subplot(2,2,2)
  set(gca,'Units','normalized','OuterPosition',[0.5 0.5 0.5 0.5]);

  subplot(2,2,3)
  set(gca,'Units','normalized','OuterPosition',[0 0 0.5 0.5]);

  subplot(2,2,4)
  set(gca,'Units','normalized','OuterPosition',[0.5 0 0.5 0.5]);

  for ii = 1:length(cmimi)
    subplot(2,2,1)
    if cmimi(ii, 3) == 1
      imshow(fliplr(masterblob(:,:,cmimi(ii,2)+1)),clims_sample)
      title(sprintf('Master # %d flipped',cmimi(ii,2)));
    else
      imshow(masterblob(:,:,cmimi(ii,2)+1),clims_sample)
      title(sprintf('Master # %d',cmimi(ii,2)));
    end
    if any(cmimi(ii,2) == mno_probl)
      hold on
      plot([-20 size(masterblob,2)+20 size(masterblob,2)+20 -20 -20],[-20 -20 size(masterblob,1)+20 size(masterblob,1)+20 -20],'r-','linewidth',2);
      hold off
    end
    axis([-25 size(masterblob,2)+25 -25 size(masterblob,1)+25])
    set(gca,'position',[0.05 0.55 0.4 0.4])

    subplot(2,2,2)
    imshow(imblob(:,:,ii),clims_sample)
    if any(cmimi(ii, 1) == imno_probl)
      hold on
      plot([-20 size(masterblob,2)+20 size(masterblob,2)+20 -20 -20],[-20 -20 size(masterblob,1)+20 size(masterblob,1)+20 -20],'r-','linewidth',2);
      hold off
    end
    title(sprintf('Image # %d',cmimi(ii,1)));
    axis([-25 size(masterblob,2)+25 -25 size(masterblob,1)+25])
    set(gca,'position',[0.55 0.55 0.4 0.4])

    subplot(2,2,3)
    plot(0:size(corr,1)-1,corr(:,2),'c.')
    hold on
    plot(0:size(corr,1)-1,corr(:,1),'b.')
    plot(cmimi(ii,1),corr(cmimi(ii,1)+1,2),'ms')
    plot(cmimi(ii,1),corr(cmimi(ii,1)+1,1),'ms')
    hold off
    title('Horizontal (cyan) and vertical (blue) drifts');
    xlim([0 size(corr,1)])
    set(gca,'position',[0.05 0.05 0.4 0.4])

    subplot(2,2,4)
    if cmimi(ii,3)==1
      mbb=fliplr(masterblob(:,:,cmimi(ii,2)+1));
    else
      mbb=masterblob(:,:,cmimi(ii,2)+1);
    end
    imbb=imblob(:,:,ii);
    imbb=interpolatef(imbb,corr(cmimi(ii,1)+1,1),corr(cmimi(ii,1)+1,2));
    imshow(mbb-imbb,clims_substract)
    title('Substraction of correlated images')
    set(gca,'position',[0.55 0.05 0.4 0.4])

    drawnow
    if dopause==1
      pause
    end
  end

end % of sfDisplayImages


%% Polynomials of vertical and horizontal drifts

  function [ydrift_pol,zdrift_pol,ydrift_int,zdrift_int]=nfComputePolynomials
    %corr(isnan(corr))=interp1(find(~isnan(corr)),corr(~isnan(corr)),find(isnan(corr)));

    % Polynomial of vertical drift
    corr_ver=corr(:,1);

    figure('name','Vertical drift of scan from calibration')
    hold on
    plot(corr_ver,'r.')

    %[pol_ver,s_ver,mu_ver]=polyfit(find(~isnan(corr_ver)),corr_ver(~isnan(corr_ver)),polyorder_outliers);
    %corrp_ver=polyval(pol_ver,((1:totproj+1)-mu_ver(1))/mu_ver(2))';
    %err_ver=corrp_ver-corr_ver;
    err_ver=0; % no outliers considered

    corr_ver(abs(err_ver)>tol_pol*std(err_ver(~isnan(err_ver))))=NaN;
    plot(find(~isnan(corr_ver)),corr_ver(~isnan(corr_ver)),'g.')
    [pol_ver,s_ver,mu_ver]=polyfit(find(~isnan(corr_ver)),corr_ver(~isnan(corr_ver)),polyorder_driftcurve);
    corrp_ver=polyval(pol_ver,((1:totproj+1)-mu_ver(1))/mu_ver(2))'; % vector of drifts
    plot(corrp_ver,'r.','markersize',3)


    % Polynomial of horizontal drift
    % add a constant drift to all, if bounding bbox is not centered in the
    % original image
    % corr_hor=corr(:,2)+(detcenter_abs-bboxcenter_abs);
    corr_hor=corr(:,2);   % add constant shift later in gtPreprocessing

    figure('name','Horizontal drift of scan from calibration')
    hold on
    plot(corr_hor,'r.')

    %[pol_hor,s_hor,mu_hor]=polyfit(find(~isnan(corr_hor)),corr_hor(~isnan(corr_hor)),polyorder_outliers);
    %corrp_hor=polyval(pol_hor,((1:totproj+1)-mu_hor(1))/mu_hor(2))';
    %err_hor=corrp_hor-corr_hor;
    err_hor=0; % no outliers considered

    corr_hor(abs(err_hor)>tol_pol*std(err_hor(~isnan(err_hor))))=NaN;
    plot(find(~isnan(corr_hor)),corr_hor(~isnan(corr_hor)),'g.')
    [pol_hor,s_hor,mu_hor]=polyfit(find(~isnan(corr_hor)),corr_hor(~isnan(corr_hor)),polyorder_driftcurve);
    corrp_hor=polyval(pol_hor,((1:totproj+1)-mu_hor(1))/mu_hor(2))'; % vector of drifts
    plot(corrp_hor,'r.','markersize',3)

    % the resulting drift polynomials
    ydrift_pol=corrp_hor;
    zdrift_pol=corrp_ver;


    % Interpolated drifts
    zdrift_int=corr(:,1);
    zdrift_int(isnan(zdrift_int))=interp1(find(~isnan(zdrift_int)),zdrift_int(~isnan(zdrift_int)),find(isnan(zdrift_int)));

    ydrift_int=corr(:,2);   %  +(detcenter_abs-bboxcenter_abs);  add this offset later in gtPreprocessing
    ydrift_int(isnan(ydrift_int))=interp1(find(~isnan(ydrift_int)),ydrift_int(~isnan(ydrift_int)),find(isnan(ydrift_int)));

  end % of nfComputePolynomials


%% BBox filled by sample during scan

  function [imno_probl]=nfScanBBfill

    [imblob_min3,imno_imblobmin]=min(imblob,[],3);
    [minrow,imno_minrow]=min(imblob_min3,[],1);
    [mincol,imno_mincol]=min(imblob_min3,[],2);

    % image id-s which give the extreme values
    imno_minrow=imno_imblobmin(imno_minrow+(0:size(imno_imblobmin,2)-1)*size(imno_imblobmin,1))-1;
    imno_mincol=imno_imblobmin((1:size(imno_imblobmin,1))'+(imno_mincol-1)*size(imno_imblobmin,1))-1;

    if strcmpi(calib.type,'180degree') && strcmpi(acq.type,'360degree')
      tmp=imno_minrow;
      imno_minrow=mod(tmp,tomo_N);
      imno_minrow(tmp>0 & imno_minrow==0)=tomo_N;

      tmp=imno_mincol;
      imno_mincol=mod(tmp,tomo_N);
      imno_mincol(tmp>0 & imno_mincol==0)=tomo_N;
    end

    % problematic images (images from which the sample may have moved out)
    imno_probl=[];

    if max(mincol)>lim_samatten
      disp('WARNING! Sample during scan may not entirely fill the given bounding box!')
      imno_probl=unique(imno_mincol(mincol>lim_samatten));
      disp(' See scan images no. : ');
      disp(imno_probl)
      showims='yes';
    end

    if max(minrow)<lim_samatten
      disp('WARNING! Sample during scan may have moved out of the given bounding box!')
      imno_probl=unique(imno_minrow(minrow<lim_samatten));
      disp(' See scan images no. : ');
      disp(imno_probl')
      showims='yes';
    end

  end % of nfSampleBB


%% Determine and check bbox filled by sample during calibration

  function [mno_probl,minrow,mincol,masterblob_min3]=nfMasterBBfill
    [masterblob_min3,mno_masterblobmin]=min(masterblob,[],3);
    [minrow,mno_minrow]=min(masterblob_min3,[],1);
    [mincol,mno_mincol]=min(masterblob_min3,[],2);

    % image id-s which give the extreme values
    mno_minrow=mno_masterblobmin(mno_minrow+(0:size(mno_masterblobmin,2)-1)*size(mno_masterblobmin,1))-1;
    mno_mincol=mno_masterblobmin((1:size(mno_masterblobmin,1))'+(mno_mincol-1)*size(mno_masterblobmin,1))-1;

    if strcmpi(calib.type,'180degree') && strcmpi(acq.type,'360degree')
      tmp=mno_minrow;
      mno_minrow=mod(tmp,tomo_N);
      mno_minrow(tmp>0 & mno_minrow==0)=tomo_N;

      tmp=mno_mincol;
      mno_mincol=mod(tmp,tomo_N);
      mno_mincol(tmp>0 & mno_mincol==0)=tomo_N;
    end

    showims='no';

    % problematic images (images from which the sample may have moved out)
    mno_probl=[];

    if max(mincol)>lim_samatten
      disp('WARNING! Sample during calibration may not entirely fill the given bounding box!')
      mno_probl=unique(mno_mincol(mincol>lim_samatten));
      disp(' See master image no. : ');
      disp(mno_probl)
      showims='yes';
    end

    if max(minrow)<lim_samatten
      disp('WARNING! Sample during calibration may have moved out of the given bounding box!')
      mno_probl=unique(mno_minrow(minrow<lim_samatten));
      disp(' See master image no. : ');
      disp(mno_probl')
      showims='yes';
    end

  end %of nfMasterBBfill


%% Create sample bounding box

  function [bbs_ext, edges, edges_ext] = nfCreateSampleBoundingBox()

    minrow=minrow<lim_samatten;
    marker=false(size(minrow));
    marker(round(length(marker)/2))=true;
    minrowr=imreconstruct(marker,minrow);
    edgel=find(minrowr,1,'first');
    edger=find(minrowr,1,'last');
    edgextl=max(edgel-ext_pix,1);
    edgextr=min(edger+ext_pix,bbox(3));
    bbs_ext(1)=bbox(1)+edgextl-1;
    bbs_ext(3)=edgextr-edgextl+1;

    mincol=mincol<lim_samatten;
    marker=false(size(mincol));
    marker(round(length(marker)/2))=true;
    mincolr=imreconstruct(marker,mincol);
    edget=find(mincolr,1,'first');
    edgeb=find(mincolr,1,'last');
    edgextt=max(edget-ext_pix,1);
    edgextb=min(edgeb+ext_pix,bbox(4));
    bbs_ext(2)=bbox(2)+edgextt-1;
    bbs_ext(4)=edgextb-edgextt+1;

    edges=[edgel edget edger edgeb];
    edges_ext=[edgextl edgextt edgextr edgextb];

    figure('name','Bounding box filled by sample during calibration')

    wdefault=warning;
    warning('off','all');

    imshow(masterblob_min3,[0 1])

    warning(wdefault);

    hold on
    plot([edgel edger edger edgel edgel],[edget edget edgeb edgeb edget],'r-')
    plot([edgextl edgextr edgextr edgextl edgextl],[edgextt edgextt edgextb edgextb edgextt],'g-')

  end % of nfCreateSampleBoundingBox


%%

end % of primary function










% Helpful crap

%plot([bbs_ext(1),bbs_ext(1)+bbs_ext(3)-1,bbs_ext(1)+bbs_ext(3)-1,bbs_ext(1),bbs_ext(1)],[bbs_ext(2),bbs_ext(2),bbs_ext(2)+bbs_ext(4)-1,bbs_ext(2)+bbs_ext(4)-1,bbs_ext(2)],'g-')
%[edgel edger edget edgeb]
%bbs_ext

% For displaying a given image pair

% i=1
%   cref=(tomo_N-i)/tomo_N*refstart+i/tomo_N*refend;
%   master=(edf_read(sprintf('%s/%s%0.4d.edf',calib.dir,calib.name,i),bbread)-cdark)./cref;
%   master=interpolatef(master,0,-rotdev);
%   masterbb=master(1+frame:end-frame,1+frame:end-frame);
%   figure(1)
%   imshow(masterbb,[0 1])
%   shg
%
%   imi=mod(i*sf+calib.offset,totproj);
%   if imi~=i*sf+calib.offset && strcmpi(acq.type,'180degree')
%     masterbb=fliplr(masterbb);
%   end
%
%   imi=mod((i+tomo_N)*sf+calib.offset,totproj)
%
%   imrefa=edf_read(sprintf('%s/ref0000_%0.4d.edf',scanfolder,floor(imi/acq.refon)*acq.refon),bbox)-imdark;
%   imrefb=edf_read(sprintf('%s/ref0000_%0.4d.edf',scanfolder,ceil((imi-eps)/acq.refon)*acq.refon),bbox)-imdark;
%   imref=(acq.refon-mod(imi,acq.refon))/acq.refon*imrefa+mod(imi,acq.refon)/acq.refon*imrefb;
%   im=(edf_read(sprintf('%s/%s%0.4d.edf',scanfolder,scanname,imi),bbox)-imdark)./imref;
%   figure(2)
%   imshow(im,[0 1]),shg


% For displaying the masters:
%
% figure
% for ii=0:36
%   imshow(edf_read(sprintf('master_%0.4d.edf',ii)),[]);shg
%   pause
% end

% For displaying the masters:
%
% figure
% for ii=0:36
%   imshow(masterblob(:,:,ii+1),[0 1]);shg
% end


% figure
% for ii=1:73
%   imshow(imblob{ii},[0 1]);shg
%   disp(ii)
%   pause
% end




