function gtApplyDrifts_raw(first, last, workingdir)
%
% gtApplyDrifts_raw(first, last, workingdir)
% new version _raw to act on images before Preprocessing.  
% drifts are saved in the parameters file
% add a field to the edf header - istranslated='true'
%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% get data from parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
cd(workingdir)
if exist('parameters.mat','file')
  load('parameters.mat')
  acq=parameters.acq;
else
  gtError('Cannot find parameter file!')
end
imagedir=[parameters.acq.dir '/0_rawdata/' parameters.acq.name];
udrift=parameters.prep.udrift;
vdrift=parameters.prep.vdrift;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% database connect
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
gtDBConnect
drifttable=[parameters.acq.name '_drifttable'];


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   apply drifts routine
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp('Applying drifts to images...')
counter=0;
% while loop for function timeout when function is finished
while counter<100
    found=0;
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %   get the list of files - .edfs only
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % source directory file list
    sfiles=dir([imagedir '/*.edf']);
    %sort files by date order to copy in order
    sfdates=[];
    for n=1:length(sfiles)
        sfdates(n)=sfiles(n).datenum;
    end
    [sfdates,i]=sort(sfdates);

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %   try to apply drifts 
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    for n=1:length(sfiles)
        sfname=sfiles(i(n)).name;

        %check if sfname has already been taken by another condor job
        test=0;
        try
            test=mym(sprintf('insert into %s (filename) values ("%s")', drifttable, sfname));
        end
        if test==1 %no other job is copying this file, do copy
            disp(sprintf('working on %s',sfname))

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % do drift here 
            fnum=sfname;
            fnum=str2num(fnum(end-7:end-4));
            test=sfOverwrite(sfname, ushift(fnum+1), vshift(fnum+1), pad);
            if test==1 % keep track for time out
                found=1;
            end
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        end

    end %for loop over the file list, working in date order

    if found==0
        counter=counter+1;
        pause(10) %have a rest, wait before looking again
        disp('sleeping 10, then look again')
    else
        counter=0;
    end
end
disp('Timed out while waited for files to appear... quitting')


end % of function



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   subfunction sfOverwrite
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function test=sfOverwrite(fname, ushift, vshift, pad)
im=edf_read(fname);
info=edf_info(fname);

%check image has not already been shifted
if isfield(info,'istranslated') && strcmp(info.istranslated,'true')
    warning('Image has been shifted earlier!, not touching this file')
    test=0;
    return
end

%update istranslated in header
info.istranslated='true';
im=gtShift(im, ushift, vshift, pad);  % gtShift(im,x(horizontal_right),y(vertical_downwards))
edf_write(im,fname,info);
test=1;
end
