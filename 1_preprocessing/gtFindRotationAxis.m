function rotu = gtFindRotationAxis(acq, bbdir)
%
% modify to make tolerant of missing images
% assume centre of the detector if the neccesary images can't be found
%
% Operates at the start of preprocessing, so images have been undistorted and
% interlaced images renamed, and placed in the 0_rawdata/scanname/ folder
%
% "u" = horizontal coordinate of detector

disp(' finding rotation axis...')

raw_data_dir = fullfile(acq.dir, '0_rawdata', acq.name);

try
    % acq.nproj is the number of images per 180 degrees
    % in an interlaced scan, the number of images per 180 degrees is
    % nproj/(acq.interlaced_turns+1)
    % after renaming the images this is all we need to know

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % read images
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    indx_next = 2*acq.nproj+2;
    name_next = fullfile(raw_data_dir, sprintf('%s%04d.edf', acq.name, indx_next) );

    if exist(name_next,'file')
      sprintf('Using images AFTER scan for determination of rotation axis posistion\n')
        % FIRST CHOICE: IMAGES AFTER SCAN
      indx_curr = 2*acq.nproj;

      name_curr = fullfile(raw_data_dir, sprintf('%s%04d.edf', acq.name, indx_curr) );
      name_ref0 = fullfile(raw_data_dir, sprintf('ref0000_%04d.edf', indx_curr) );
      name_ref1 = name_ref0;
    else
      sprintf('Using images DURING scan for determination of rotation axis posistion\n')
      % SECOND CHOICE: IMAGES DURING SCAN
      % WL May 2009
      % changed to use per default the images at end of scan - should be more accurate;
      indx_next = acq.nproj/(acq.interlaced_turns+1);

      name_curr = fullfile(raw_data_dir, sprintf('%s0000.edf', acq.name) );
      name_next = fullfile(raw_data_dir, sprintf('%s%04d.edf', acq.name, indx_next) );
      name_ref0 = fullfile(raw_data_dir, 'ref0000_0000.edf');
      
      % is there a reference group at 180 degrees?
      if mod(indx_next, acq.refon)==0
      name_ref1 = fullfile(raw_data_dir, sprintf('ref0000_%04d.edf', indx_next) );
      else
          name_ref1=[];
      end
      
    end
    % If there is already a sample bounding box, we will use it
    if ~isempty(acq.bb)
        bbdir = acq.bb;
    end
    im0  = edf_read(name_curr, bbdir, 'nodisp');
    im1  = edf_read(name_next, bbdir, 'nodisp');
    ref0 = edf_read(name_ref0, bbdir, 'nodisp');
    
    if ~isempty(name_ref1)
        ref1 = edf_read(name_ref1, bbdir, 'nodisp');
    else
    % special case if there isn't a reference group
    % at 180 degrees in an interlaced scan
          indx_nextF = floor(indx_next/acq.refon)*acq.refon;
          name_ref1F = fullfile(raw_data_dir, sprintf('ref0000_%04d.edf', indx_nextF) );
          indx_nextC=ceil(indx_next/acq.refon)*acq.refon;
          name_ref1C = fullfile(raw_data_dir, sprintf('ref0000_%04d.edf', indx_nextC) );
          w=mod(indx_next/acq.refon, 1);
          ref1 = (1-w)*edf_read(name_ref1F, bbdir, 'nodisp') + w*edf_read(name_ref1C, bbdir, 'nodisp');  
    end
    
    %get the dark file
    darkname = fullfile(raw_data_dir, 'dark.edf');
    if exist(darkname, 'file')
        d = edf_read(darkname, bbdir);
    else
        disp(['Warning: no file named: ''' darkname ''' found. Cannot apply darkfield']);
        d = 0;
    end

    % apply flatfield
    im0 = (im0-d)./(ref0-d);
    im1 = (im1-d)./(ref1-d);

    shift = gtFindShifts(im0, fliplr(im1), 'block', 'on');

    % working in the direct beam bounding box
    rotu = bbdir(1) - 1 + (bbdir(3)/2) + 0.5 + (shift(2)/2);
catch Mexc
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % default to the centre of the detector if images not found
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    gtPrintException(Mexc);
    rotu = acq.xdet/2 + 0.5;
    disp('%%%%%%% missing images %%%%%%%%')
    fprintf('not all images found! Using default value of %0.1f for centre of rotation!', rotu)
    disp('verify this later - may affect choice of sample bounding box')
    disp('%%%%%%% missing images %%%%%%%%')
    disp(' ')
end

