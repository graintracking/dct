function gtSetup()
% GTSETUP  Sets up the directory structure and parameters file for the 
%          experiment, initialises the processing.
%
%     gtSetup()
%     ---------
%     To be run in the directory where the data has been collected
%
%
%     Version 013 28-03-2014 by PReischig
%       Formatting, cleaning, improved feedback. 
%
%     Version 012 14-06-2012 by LNervo
%       Add functions to set default parameters for seg, index, fsim and rec
%
%     Version 011 14-05-2012 by LNervo
%       Modify gtReadSpaceGroup to give lattice and crystal systems
%
%     Version 010 02-05-2012 by WLudwig
%       Add multi-phases materials
%
%     Version 009 08-03-2012 by PReischig
%       Updated to parameters version 2:
%         Modified fields: match, cryst, xop, acq, seg, rec
%         Added field    : labgeo, samgeo, version, fsim
%         Removed fields : geo, opt, match_calib
%
%     Version 008 14-12-2011 by LNervo
%       Clean code: y/n -> [y/n]
%                   ';' at the end of lines
%                   '()' at the end of functions/methods
%                   add 'clear' statements to free memory
%
%     Version 007 02-03-2012 by LNervo
%       Split crystallographic part into functions
%
%     Version 006 06-01-2012 by LNervo
%       Add fed structure
%
%     Version 005 01-12-2011 by LNervo
%       Adapt to new parameters file template
%       Cryst1 is made of cryst1, xop1, reflections1
%
%     Version 004 22-11-2011 by LNervo
%       Split acquisition structure 'acq' into acq, cryst, xop, geo, opt
%       Add version option for saving mat files
%
%     Version 003 17-11-2011 by LNervo
%       Change 7_FED into 7_fed
%       Add OAR_log folder
%       Add geo structure with detector geometry parameters
%       Use the same folder naming for the source code (see initialize_gt2.m)
%
%     Version 002 08-11-2011 by LNervo
%       *** Cleanest version (first attempt) ***
%       Add 3_pairmatching, 6_rendering, 7_FED and 8_optimization folders
%           in data analysis directory
%       Change 'true' 'false' in 1 - 0 logical values
%       Add arbitrary geometry parameters to acq structure
%       Add crystallography stuff to acq structure
%       Add xop to acq (create folder in /data/id11/graintracking/file_xop)
%
%     Version 001 06-04-2011 by AKing


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Initialise
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp(' ');
disp(' ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~');
disp(' ~      Welcome to Grain Tracking at the ESRF!      ~');
disp(' ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~');
disp(' ')
pause(1);
close('all');

if (isempty(whos('global', 'GT_MATLAB_HOME')))
    gtError('SETUP:no_such_variable', ...
          ['GT_MATLAB_HOME variable doesn''t exist in global workspace, '...
           'your environment is not sane, and you need to either ' ...
           're-initialise or re-run matlab.'])
end
global GT_MATLAB_HOME;

parameters = [];
list       = [];

% Initialise output colors for errors and warnings
defaultColor = sprintf(gtGetANSIColour());
redColor = sprintf(gtGetANSIColour('red'));


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Create parameters structure, move/correct datafiles
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copy parameters from a different acquisition
check = inputwdefault('Would you like to copy an existing parameter file? [y/n]', 'y');
if strcmpi(check, 'y')
    masterpath = uigetdir('../','Select a directory');
    p = gtLoadParameters(masterpath);
    p.acq.dir  = pwd;
    p.acq.name = gtGetLastDirName(p.acq.dir);
    p.acq.pair_tablename  = sprintf('%sspotpairs', p.acq.name);
    p.acq.calib_tablename = sprintf('%spaircalib', p.acq.name);
    p.acq.collection_dir  = pwd;
    p.acq.collection_dir_old = pwd;
    p.acq.masterdir = masterpath;
    gtSaveParameters(p);
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %   Set up the database tables
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    gtDBSetupTables(p.acq);
    % Make directories
    disp(['Setting up directory structure in ' p.acq.dir]);
    [~,msg] = mkdir( fullfile(p.acq.dir, '0_rawdata') );                      disp(msg);
    [~,msg] = mkdir( fullfile(p.acq.dir, '0_rawdata', p.acq.name));           disp(msg);
    [~,msg] = mkdir( fullfile(p.acq.dir, '0_rawdata', 'Orig') );              disp(msg);
    [~,msg] = mkdir( fullfile(p.acq.dir, '1_preprocessing') );                disp(msg);
    [~,msg] = mkdir( fullfile(p.acq.dir, '1_preprocessing', 'full') );        disp(msg);
    [~,msg] = mkdir( fullfile(p.acq.dir, '1_preprocessing', 'abs') );         disp(msg);
    [~,msg] = mkdir( fullfile(p.acq.dir, '1_preprocessing', 'ext') );         disp(msg);
    [~,msg] = mkdir( fullfile(p.acq.dir, '2_difspot') );                      disp(msg);
    [~,msg] = mkdir( fullfile(p.acq.dir, '3_pairmatching') );                 disp(msg);
    [~,msg] = mkdir( fullfile(p.acq.dir, '4_grains') );                       disp(msg);
    [~,msg] = mkdir( fullfile(p.acq.dir, '5_reconstruction') );               disp(msg);
    [~,msg] = mkdir( fullfile(p.acq.dir, '6_rendering') );                    disp(msg);
    [~,msg] = mkdir( fullfile(p.acq.dir, '7_fed') );                          disp(msg);
    [~,msg] = mkdir( fullfile(p.acq.dir, '8_analysis') );                     disp(msg);
    [~,msg] = mkdir( fullfile(p.acq.dir, '8_analysis', 'figures') );          disp(msg);
    [~,msg] = mkdir( fullfile(p.acq.dir, 'OAR_log') );                        disp(msg);

end
% If the parameters are already set up, start moving and correction
if exist(fullfile(pwd,'parameters.mat'), 'file')
    disp('A parameters.mat file already exists in this directory');
    disp('You can recreate this parameters file to enter new parameters');
    disp('or use the existing file and just start the moving and correction')
    disp('process straightaway.');
    load('parameters.mat');
    disp(' ')
    disp('NOTE! The original files from the source folder will be moved!')
    disp('A copy of the original files will be kept in the processing folders.')
    disp(' ')
    
    check = inputwdefault('Launch the moving and correction process without changing parameters? [y/n]', 'n');
    if strcmpi(check, 'y')
        disp(' ');
        % gtCopyCorrectUndistortWrapper handles the moving/copying via oar
        % The files in the original folder may be deleted!
        disp(' Starting data moving and correction using OAR...');
        disp(' ');
        gtCopyCorrectUndistortWrapper(parameters);
        return;
    end
    check = inputwdefault('Do you want to reset all the values in parameters.mat? [y/n]', 'n');
    % build template locally from build_list_v#, where # is the version number
    % get also list which contains info of parameters fields.
    [tmppar, list] = make_parameters(2);
    if strcmpi(check, 'y')
        parameters = tmppar;
        gtSaveParameters(parameters);
    end
    clear check tmppar;
else
    % Create new parameters structure
    [parameters, list] = make_parameters(2);
end


if isempty(parameters.acq.dir)
    parameters.acq.dir = pwd;
end

% Add to xml file fields like 'collection_dir' and updates 'scanName' by
% reading some info from headers of ref0000_0000.edf  and darkend0000.edf to
% update xml file.
disp('Adding additional info to the experiment xml file ...')
[xml_newname, tmpxml] = gtUpdateXmlFile();
if isempty(xml_newname)
    parameters.acq.name               = gtGetLastDirName(pwd());
    parameters.acq.collection_dir_old = pwd();
    parameters.acq.collection_dir     = pwd();
else
    parameters.acq.name               = tmpxml.acquisition.scanName;
    parameters.acq.collection_dir_old = tmpxml.acquisition.collection_dir_old;
    parameters.acq.collection_dir     = pwd();
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Directory structure for analysis
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Set up directory structure.

% For the moment we process in the acquisition directory - this will have to
% change with ESRF data policy
parameters.acq.dir = pwd;  % gtSetAnalysisDir(pwd());

hasChosen = false;
while (~hasChosen)
    dir = inputwdefault('Which directory do you want to use as analysis directory?', parameters.acq.dir);
    jdir = java.io.File(dir);
    if (~jdir.isAbsolute())
        disp([redColor 'Analysis directory should be an absolute path! Try again...' defaultColor]);
    else
        jdir.mkdirs();
        if (jdir.isFile())
            disp([redColor 'This already exists as a file. Choose another one...' defaultColor]);
        elseif (~jdir.canWrite())
            disp([redColor 'You have no writing permission to this directory! Choose another one...' defaultColor]);
        else
            parameters.acq.dir = dir;
            hasChosen = true;
        end
    end
end
freeSpace = round(jdir.getUsableSpace() / 10^6);
fprintf('You have about %d Mb free space\n', freeSpace);
jdir.delete();


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Set up the database tables
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
gtDBSetupTables(parameters.acq);

% Make directories
disp(['Setting up directory structure in ' parameters.acq.dir]);
[~,msg] = mkdir(parameters.acq.dir);                                               disp(msg);
[~,msg] = mkdir( fullfile(parameters.acq.dir, '0_rawdata') );                      disp(msg);
[~,msg] = mkdir( fullfile(parameters.acq.dir, '0_rawdata', parameters.acq.name) ); disp(msg);
[~,msg] = mkdir( fullfile(parameters.acq.dir, '0_rawdata', 'Orig') );              disp(msg);
[~,msg] = mkdir( fullfile(parameters.acq.dir, '1_preprocessing') );                disp(msg);
[~,msg] = mkdir( fullfile(parameters.acq.dir, '1_preprocessing', 'full') );        disp(msg);
[~,msg] = mkdir( fullfile(parameters.acq.dir, '1_preprocessing', 'abs') );         disp(msg);
[~,msg] = mkdir( fullfile(parameters.acq.dir, '1_preprocessing', 'ext') );         disp(msg);
[~,msg] = mkdir( fullfile(parameters.acq.dir, '2_difspot') );                      disp(msg);
[~,msg] = mkdir( fullfile(parameters.acq.dir, '3_pairmatching') );                 disp(msg);
[~,msg] = mkdir( fullfile(parameters.acq.dir, '4_grains') );                       disp(msg);
[~,msg] = mkdir( fullfile(parameters.acq.dir, '5_reconstruction') );               disp(msg);
[~,msg] = mkdir( fullfile(parameters.acq.dir, '6_rendering') );                    disp(msg);
[~,msg] = mkdir( fullfile(parameters.acq.dir, '7_fed') );                          disp(msg);
[~,msg] = mkdir( fullfile(parameters.acq.dir, '8_analysis') );                     disp(msg);
[~,msg] = mkdir( fullfile(parameters.acq.dir, '8_analysis', 'figures') );          disp(msg);
[~,msg] = mkdir( fullfile(parameters.acq.dir, 'OAR_log') );                        disp(msg);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   ACQ - Acquisition parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

parameters_name = fullfile(parameters.acq.dir, 'parameters.mat');
if (exist(parameters_name, 'file'))
    % if a parameters.mat already exists in the analysis directory, use it.
    disp('Using existing parameters file in the analysis directory...');
    disp(parameters_name);
    new_params = gtLoadParameters(parameters.acq.dir);
    new_params.acq.dir = parameters.acq.dir;
    parameters = new_params;
    clear p;
else
    % Otherwise just load stuff from XML and guess default parameters

    % Parameters from .xml if possible
    xmlfname = fullfile(parameters.acq.collection_dir, [parameters.acq.name '.xml']);

    % try to read the .xml file that may be present
    try
        interactive = true;
        params_xml = gtLoadAcquisitionXML(xmlfname, interactive);
    catch mexc
        if gtCheckExceptionType(mexc, 'XML:no_xml_file', @()disp('Quitting..'))
            return;
        end
    end
    % update energy field from updated xml file
    if ~isempty(tmpxml)
        params_xml.energy = tmpxml.acquisition.energy;
        parameters.acq = gtAddMatFile(parameters.acq, params_xml, true, false, false);
    end
    clear params_xml;

    %%% And now let's initialise automatically everything we can.

    % Default ACQUISITION parameters
    params_acq = gtAcqDefaultParameters(parameters.acq);
    parameters.acq = gtAddMatFile(parameters.acq, params_acq, true, false, false);
    clear params_acq;
    
    %parameters = %gtCheckParameters(parameters, 'acq', 'verbose', true);
    gtSaveParameters(parameters);

    % Default SAMPLE REFERENCE
    % Default voxel size is set to 1 lab unit, so samgeo is like labgeo at
    % omega=0.
    parameters.samgeo = gtGeoSamDefaultParameters();
    
    %parameters = %gtCheckParameters(parameters, 'samgeo', 'verbose', true);
    gtSaveParameters(parameters);

    % Default RECONSTRUCTION REFERENCE
    % Default voxel size is set to 1e-3 lab unit, so recgeo is like samgeo
    % but scaled to microns.
    parameters.recgeo = gtGeoRecDefaultParameters(detgeo);

    %parameters = %gtCheckParameters(parameters, 'recgeo', 'verbose', true);
    gtSaveParameters(parameters);
end

% Get or confirm the important parameters: choosing flag = 1 from
% build_list_v2.m
header = 'Check carefully that the following are correct:';
parameters.acq = gtModifyStructure(parameters.acq, list.acq, 1, header);

% based on this, some extra info might be needed
if strcmpi(parameters.acq.rotation_axis, 'horizontal')
    header = 'Images will be rotated for processing:';
    parameters.acq = gtModifyStructure(parameters.acq, list.acq, 2, header);
end

disp('Saving parameters.mat in the analysis directory...');
gtSaveParameters(parameters);
clear xmlfname;

% get edf info structure from first image file
if parameters.acq.interlaced_turns > 0
    [im, info] = edf_read(fullfile(parameters.acq.collection_dir,[parameters.acq.name '0_0000.edf']));
else
    [im, info] = edf_read(fullfile(parameters.acq.collection_dir,[parameters.acq.name '0000.edf']));
end

% try to read rotation (omega) motor positions
if isfield(info, 'motor')
    tmp_motors = fieldnames(info.motor);
    if ismember('pmo', tmp_motors)
        parameters.acq.rotation_name = 'pmo';
    elseif ismember('diffrz', tmp_motors)
        parameters.acq.rotation_name = 'diffrz';
    elseif ismember('srot', tmp_motors)
        parameters.acq.rotation_name = 'srot';
    else
        parameters.acq.rotation_name = 'unknown';
    end
else
    parameters.acq.rotation_name = 'diffrz';
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% LABGEO - setup geometry
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% The parameters.labgeo field contains the geometric parameters of the 
% setup, defined in the Lab reference frame.

% get default values for labgeo
parameters.labgeo = gtGeoLabDefaultParameters(parameters.acq);
parameters.detgeo = gtGeoDetDefaultParameters(parameters.acq);
parameters.acq    = rmfield(parameters.acq, 'rotation_name');
clear tmp_motors info

% Estimate 2 theta range covered by the detector
if ~parameters.acq.no_direct_beam
    if strcmpi(parameters.acq.rotation_axis, 'horizontal')
        if strcmpi(parameters.acq.rotation_direction, 'clockwise')
            im = imrotate(im, -90);
        else
            im = imrotate(im, 90);
        end
    end
    disp('Estimation of 2 theta range covered by the detector')
    [minangle, maxangle, parameters.acq.bbdir] = gtGeoTwoThetaLimits(im, parameters);
else
    angles = inputwdefault('Approximate values for two-theta range covered by detector', num2str([0 45]), true);
    minangle = angles(1);
    maxangle = angles(2);
end

if isempty(minangle)
    parameters.detgeo.detanglemin = 0;
else
    parameters.detgeo.detanglemin = minangle;
end
if isempty(maxangle)
    parameters.detgeo.detanglemax = 45;
else
    parameters.detgeo.detanglemax = maxangle;
end


%parameters = %gtCheckParameters(parameters, 'labgeo', 'verbose', true);
gtSaveParameters(parameters);

header = 'Parameters dealing with arbitrary geometry:';

% Note! Some other fields are calculated later, typically during 
% preprocessing when know the direct beam bounding box.
parameters.labgeo = gtModifyStructure(parameters.labgeo, list.labgeo, 1, header);
% parameters.detgeo = gtModifyStructure(parameters.detgeo, list.detgeo, 1, header);

disp('Saving parameters.mat in the analysis directory...');
gtSaveParameters(parameters);

disp('Saving list.mat in the data collection directory...');
list_name = fullfile(parameters.acq.collection_dir, 'list.mat');
save(list_name, 'list');
clear list list_name


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Set up the database tables
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
gtDBSetupTables(parameters.acq);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PREP - for preprocessing
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
params_prep = gtPrepDefaultParameters(parameters.acq);
parameters.prep = gtAddMatFile(parameters.prep, params_prep, true, false, false);
clear params_prep;

disp('Saving parameters.mat in the analysis directory...');
gtSaveParameters(parameters);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SEG - for segmentation of diffraction blobs / spots
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
parameters.seg = gtSegDefaultParameters();

disp('Saving parameters.mat in the analysis directory...');
gtSaveParameters(parameters);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% MATCH - for Friedel pair matching
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
parameters.match = gtMatchDefaultParametersGUI();
parameters.match.thetalimits = 0.5*[parameters.detgeo.detanglemin, ...
                                    parameters.detgeo.detanglemax];

disp('Saving parameters.mat in the analysis directory...');
gtSaveParameters(parameters);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% INDEX - used by the indexing algorithm INDEXTER
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
parameters.index = gtIndexDefaultParameters();

disp('Saving parameters.mat in the analysis directory...');
gtSaveParameters(parameters);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% FORWARD SIMULATION PARAMETERS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
parameters.fsim = gtFsimDefaultParameters();
disp('Saving parameters.mat in the analysis directory...');
gtSaveParameters(parameters);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% RECONSTRUCTION - parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
parameters.rec = gtRecDefaultParameters();
disp('Saving parameters.mat in the analysis directory...');
gtSaveParameters(parameters);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% FED part
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
parameters.fed.dir           = parameters.acq.dir;
parameters.fed.dct_vol       = fullfile(parameters.acq.dir, 'dct_vol.edf');
parameters.fed.dct_offset    = [0 0 0];
parameters.fed.renumber_list = [];

disp('Saving parameters.mat in the analysis directory...');
gtSaveParameters(parameters);
disp(' ');
close('all');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Crystallography setup -  allow for multiphase materials
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Crystallography parameters; do it last because it's complicated and 
% tends to break. 

nof_phases = 1;
parameters.acq.nof_phases = inputwdefaultnumeric( ...
    'Number of crystallographic phases to be analysed in this sample?', num2str(nof_phases));

if parameters.acq.nof_phases >1
    parameters.xop(parameters.acq.nof_phases)=parameters.xop(1);
    parameters.cryst(parameters.acq.nof_phases)=parameters.cryst(1);
end

% Since the crystallography setup often crashes, include it in try/catch
try
	% get Cryst field information for all phases
	for phase_id = 1 : parameters.acq.nof_phases
		% make a subfolder in 4_grains
		[~, msg] = mkdir( fullfile(parameters.acq.dir, '4_grains', ...
			sprintf('phase_%02d', phase_id)) );
		disp(msg);
		parameters = gtFillCrystFields(parameters, phase_id);
	end % end crystallography
	
	disp('Saving parameters.mat in the analysis directory...');
	gtSaveParameters(parameters);
	disp(' ');
catch
	disp(' ')
	warning('Crystallography setup failed!')
	disp('You may try the crystallography setup later by running:')
	disp(' parameters = gtFillCrystFields(parameters, phase_id)')
	disp(' ')
	disp('Continuing with gtSetup ...')
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Recompile binaries
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% It is not often needed, and usually dealt with outside matlab by running
% the dct_launch.py compilation scripts. 

check = inputwdefault('Recompile data moving and correction function for OAR? Not normally needed! [y/n]', 'n');
if strcmpi(check, 'y')
    disp('recompiling gtCopyCorrectUndistortCondor');
    gtExternalCompileFunctions('gtCopyCorrectUndistortCondor');
end

disp(' ');
disp('Changing to the analysis directory:');
cd(parameters.acq.dir);
pwd
disp(' ');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Start copy/move/undistortion process
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp(' ')
disp('NOTE! The original files from the source/acquisition folder will be moved!')
disp('A copy of the original files will be kept in the processing folders.')
disp(' ')
	
check = inputwdefault('Start moving and correction process now? [y/n]', 'y');
if strcmpi(check, 'n')
    disp('You can start the process later by running:')
    disp(' in parallel using OAR :');
    disp('    load(parameters)');
    disp('    gtCopyCorrectUndistortWrapper(parameters)');
    disp(' OR sequentially:')
    disp('    gtCopyCorrectUndistortCondor([], [], analysis_directory)')
    return;
end

% gtCopyCorrectUndistortWrapper handles moving/correction via OAR
disp('Starting data moving and correction using OAR...');
disp(' ');
gtCopyCorrectUndistortWrapper(parameters);

clear msg check;

end % end of function
