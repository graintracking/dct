
function gtFlipFullImages(first, last, workingdirectory, updown, leftright)
%flip full images.  
%set updown or leftright to true to flip on that axis

workingdirectory

if isdeployed
  global GT_DB
  global GT_MATLAB_HOME
  load('workspaceGlobal.mat');
  first=str2double(first);
  last=str2double(last);
  updown=str2double(updown);
  leftright=str2double(leftright);
end

if ~exist('workingdirectory','var')
  workingdirectory=pwd;
end

cd(workingdirectory)

disp(sprintf('doing images from %d to %d', first, last))
if updown
  disp('flipping up-down')
end
if leftright
  disp('flipping left-right')
end




for i=first:last
  
  disp(sprintf('processing image:  full%04d',i))
  
  %read image
  im=edf_read(sprintf('1_preprocessing/full/full%04d.edf',i));
  %apply flips
  if updown
    im=flipud(im);
  end
  if leftright
    im=fliplr(im);
  end
  %write image
  edf_write(im, sprintf('1_preprocessing/full/full%04d.edf',i), 'float32')
  

end
