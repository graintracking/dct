function [peaksInfo, extraInfo] = gtTaperReadIndexedPeaks(indexedPeaks)
% [peaksInfo, extraInfo] = gtTaperReadIndexedPeaks(indexedPeaks)
% read indexing output from shell command:
% less peaks_t100_s_cleaned_new.indexed | grep " )  " > indexed_peaksHKL.txt

pattern = 'Peak   \(  h       k       l      \)   drlv             x       y    Omega_obs Omega_calc   Eta_obs Eta_calc   tth_obs tth_calc';
%[C] = textread(indexedPeaks,'%s','delimiter','\n')
%index = findStringIntoCell(C,{pattern});

% read indexing output from shell command:
% less peaks_t100_s_cleaned_new.indexed | grep " )  " > indexed_peaksHKL.txt
texto = fileread('indexed_peaksHKL.txt');
Ctrue = regexp(texto,['\n?' pattern '\n'],'split');
Ctrue(1)=[];
tmp = regexp(Ctrue{end},'\nGrain*\n','split');
extraInfo = tmp(2:end);
Ctrue(end) = tmp(1);
clear tmp

save extraInfo.mat extraInfo

% for each grain get lines relative to peaks
Clines = {};
for ii=1:length(Ctrue)
    [C]=regexp(Ctrue{ii},char(10),'split');
    Clines{ii} = C;
end

% for each grain split lines to get peaks info
allPeaks = {};
cols = {};
for ii=1:length(Clines)
    tmp = [];
    for jj=1:length(Clines{ii})
        [D]=regexp(Clines{ii}{jj},'\s+','split');
        tmp{jj} = D;
        clear D
    end
    % get number of columns
    cols{ii} = cellfun(@(num) size(num,2), tmp);
    % save peaks for current grain
    allPeaks{ii} = tmp;
end
clear peaks ii jj kk

nof_peaks = cellfun(@(num) size(num,2), cols);
NOTassigned_peaks     = cellfun(@(num) num>15, cols, 'UniformOutput', false);
nof_NOTassigned_peaks = cellfun(@(num) size(find(num),2), NOTassigned_peaks);
assigned_peaks     = cellfun(@(num) num==15, cols, 'UniformOutput', false);
nof_assigned_peaks = cellfun(@(num) size(find(num),2), assigned_peaks);

save('RD4_15N_taper_newIndexing.mat','Clines','Ctrue','cols','assigned_peaks','NOTassigned_peaks','nof_assigned_peaks','nof_NOTassigned_peaks','nof_peaks')
clear Clines Ctrue cols

% convert to doubles
for ii=1:length(allPeaks)
    tmp = allPeaks{ii};
    for jj=1:length(tmp)
        for kk=1:size(tmp{jj},2)
            tmp{jj}{kk} = str2num(allPeaks{ii}{jj}{kk});
        end
    end
    allPeaks{ii} = tmp;
end
clear peaks ii jj kk

% remove empty entries NOW
for ii=1:length(allPeaks)
    tmp = [allPeaks{ii}{:}];
    isemptycell = cellfun(@(num) isempty(num),tmp);
    tmp(find(isemptycell))=[];

    tmp = reshape(tmp,13,[])';
    allPeaks{ii} = tmp;
end
clear tmp isemptycell ii

save allPeaks.mat allPeaks

% build peaks info
peaksInfo = {};
for ii=1:length(allPeaks)
    tmp = mat2cell(cell2mat(allPeaks{ii}'),[1 3 1 1 1 1 1 1 1 1 1],nof_peaks(ii));

    peaksInfo{ii} = cell2struct(tmp,{'ids','hkl','drlv','x','y','omega_obs','omega_calc','eta_obs','eta_calc','tth_obs','tth_calc'},1);
    peaksInfo{ii}.nof_peaks    = nof_peaks(ii);
    peaksInfo{ii}.assigned     = assigned_peaks{ii};
    peaksInfo{ii}.not_assigned = NOTassigned_peaks{ii};
    peaksInfo{ii}.refind       = 1:nof_peaks(ii);
end
clear tmp ii

save peaksInfo.mat peaksInfo

% leave only assigned peaks
names = fieldnames(peaksInfo{1});
ind = findValueIntoCell(names,{'nof_peaks','assigned','not_assigned'});
names(ind(:,1))=[];

assignedPeaks = {};
notAssignedPeaks = {};
for ii=1:length(peaksInfo)
    tmp = peaksInfo{ii};
    tmp = rmfield(tmp, {'nof_peaks','assigned','not_assigned'});
    tmpnot = peaksInfo{ii};
    tmpnot = rmfield(tmpnot, {'nof_peaks','assigned','not_assigned'});
    for jj=1:length(names)
        tmp.(names{jj})    = tmp.(names{jj})(:,peaksInfo{ii}.assigned);
        tmpnot.(names{jj}) = tmpnot.(names{jj})(:,peaksInfo{ii}.not_assigned);
    end
    tmp.nof_peaks    = peaksInfo{ii}.nof_peaks;
    tmpnot.nof_peaks = tmp.nof_peaks;
    assignedPeaks{ii} = tmp;
    notAssignedPeaks{ii} = tmpnot;
    clear tmp tmpnot
end
clear ii jj tmp tmpnot names ind

save assignedPeaks.mat assignedPeaks
save notAssignedPeaks.mat notAssignedPeaks

end

%     output = cell2struct(struct2cell([grains1{:}]), fieldnames(grains1{1}), 1)
%     [fields,ind] = setdiff(fieldnames(grains1{1}), {'pla','pld','indST','stat','reindexed','pairid','refid','hklind','shklind'}, 'stable');
%     grain = struct2cell(output); OR     grain = struct2cell(grain{:});
%     grain = grain(ind);
%     grain_of_interest = cell2struct(cellfun(@(num) num(keeplist,:), grain, 'UniformOutput', false), fields, 1);
%
% OR
%
%     grain_of_interest = grains1{keeplist};
%     grain_of_interest = rmfield(grain_of_interest,
%         {'pla','pld','indST','stat','reindexed','pairid','refid','hklind','shklind'});
%
% OR
%     fields = setdiff(fieldnames(grains1{1}), {'pla','pld','indST','stat','reindexed','pairid','refid','hklind','shklind'}, 'stable');
%     grain_of_interest = cellfun(@(num) grains1{keeplist}.(num), fields)
%     grain_of_interest = cell2struct(grain_of_interest, fields, 1);


