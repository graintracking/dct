function [gid, Npeaks, drlv] = gtTaperReadDrlvFile(filename)
% Grain: 50   Npeaks=69   <drlv>=0.026469
% '../RD4_15N_taper_/workspace/indexed_Npeaks_drlv.txt'

fid = fopen(filename);
[C] = textscan(fid, 'Grain: %d   Npeaks=%d   <drlv>=%f');

gid = C{1};
Npeaks = C{2};
drlv = C{3};

end
