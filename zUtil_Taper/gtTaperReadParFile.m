function parameters = gtTaperReadParFile(parfile)
% parameters = gtTaperReadParFile(parfile)

fid = fopen(parfile);
C = textscan(fid, '%s %s');
fclose(fid);
index = [];
for ii=1:length(C{2})
    patt=regexp(C{2}{ii},'[0-9.e-]','match');
    C{3}{ii} = patt;
    if length(C{2}{ii})~=length(C{3}{ii})
        index(end+1) = ii;
    end
end

for ii=index
    C{1}(ii) = [];
    C{2}(ii) = [];
    C{3}(ii) = [];
end

for ii=1:length(C{2})
    C{4}{ii} = str2num(C{2}{ii});
end

final = [];

for ii=1:length(C{2})
    final.(C{1}{ii}) = C{4}{ii};
end

parameters = final;

end
