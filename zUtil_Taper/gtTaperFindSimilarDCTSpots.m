function output = gtFindSimilarSpots(dct_grain, taper_name)
% find spots corresponding to a dct grain in the taper data
% for the moment indexing straight from taper seems sketchy

parameters = [];
load('parameters.mat');
output = [];

% Should change from parameters
dataset_dir_taper = fullfile('/data', 'id19', 'graintracking', 'november', 'magnesium_taper_0_');
dataset_dir_dct = fullfile('/data', 'id19', 'graintracking', 'november', 'magnesium_dct_1_');
base_dir_taper = fullfile(dataset_dir_taper, '1_preprocessing', 'full');
base_dir_dct = fullfile(dataset_dir_dct, '1_preprocessing', 'full');

taper_name = [taper_name 'spotpairs'];

%angular resolution for searching (eta, omega)
tol = 2;

for DCT_grainid = 1:5
    %read from grain structure produced by sort grains
    DCT_pairids = dct_grain{DCT_grainid}.pairid;
    DCT_theta = dct_grain{DCT_grainid}.theta;
    DCT_eta = dct_grain{DCT_grainid}.eta;
    DCT_omega = dct_grain{DCT_grainid}.omega;
    DCT_thetatype = dct_grain{DCT_grainid}.thetatype;

    %forward simulate for all diffraction data
    R_vector = dct_grain{DCT_grainid}.R_vector;
    [theta_fs, eta_fs, omega_fs, thetatype_fs] = sfForwardSimulate(R_vector);

    %use / don't use foward simulated data
    if 1
        theta = theta_fs;
        thetatype = thetatype_fs;
        eta = eta_fs;
        omega = omega_fs;
    end

    save_possibles = [];

    for ii_theta = 1:length(theta)
        %searching pairs based on angles...  size/intensity surely not proportional,
        %though could still be useful.
        %should one search spots, based on position, to get unpaired spots?
        %in taper data ~60% paired, so for the moment this should be okay.
        [pairID, eta_shift, omega_shift] = sfFindClosestSpot( ...
            theta(ii_theta), thetatype(ii_theta), eta(ii_theta), omega(ii_theta), tol);

        %collect results
        if isempty(pairID)
            disp('nothing found here')
            %output = [output; DCT_grainid, thetatype(i), NaN, NaN, NaN];
        else
            output = [output; DCT_grainid, thetatype(ii_theta), pairID, ...
                eta_shift, omega_shift];
        end

        %to display spots found / manual search
        if 1%%%%%%%%%%%

            DCT_id = DCT_pairids(find( ...
                DCT_thetatype == thetatype(ii_theta) ...
                & DCT_eta>eta(ii_theta)-tol ...
                & DCT_eta<eta(ii_theta)+tol ...
                & DCT_omega>omega(ii_theta)-tol ...
                & DCT_omega<omega(ii_theta)+tol) );

            if ~isempty(DCT_id)
                %get difspot data (difspot difA) and load image
                query = sprintf([ ...
                    'SELECT maximage, centroidx, centroidy ' ...
                    '   FROM magnesium_dct_1_spotpairs INNER JOIN magnesium_dct_1_difspot ON difAID = difspotID '...
                    '   WHERE pairID = %d'], DCT_id);
                [DCT_maxim, DCT_x, DCT_y] = mym(query);
                filename = fullfile(base_dir_dct, sprintf('full%04d.edf', DCT_maxim));
                DCT_im = edf_read(filename);
                subplot(1, 2, 1); imshow(DCT_im, [-30 30]); hold on
                plot(DCT_x, DCT_y, 'rx')
            else
                %get images around predicted omega
                im = round((omega(ii_theta)/180)*3600);
                first = max(0, im-4);
                last = min(7200, im+4); %should be unnecessary
                DCT_im = zeros(2048);
                for im = first:last
                    filename = fullfile(base_dir_dct, sprintf('full%04d.edf', im));
                    DCT_im = DCT_im+edf_read(filename);
                end
                DCT_im = DCT_im/(last-first+1);
                subplot(1, 2, 1); imshow(DCT_im, [-30 30]); hold on
                %plot a line to show where spot should be....
                plot([1024 1024+500*sind(eta(ii_theta))], [1024 1024-500*cosd(eta(ii_theta))], 'b')
            end

            if ~isempty(pairID)
                %get taper difspot data (difspot difA) and load image
                query = sprintf([...
                    'SELECT maximage, centroidx, centroidy ' ...
                    '   FROM magnesium_taper_0_spotpairs INNER JOIN magnesium_taper_0_difspot ON difAID = difspotID '...
                    '   WHERE pairID = %d'], pairID);
                [tap_maxim, tap_x, tap_y] = mym(query);
                filename = fullfile(base_dir_taper, sprintf('full%04d.edf', tap_maxim));
                tap_im = edf_read(filename);
                subplot(1, 2, 2); imshow(tap_im, [0 150]); hold on
                plot(tap_x, tap_y, 'rx')
            else
                %get images around predicted omega
                im = round((omega(ii_theta)/180)*3600);
                first = max(0, im-4);
                last = min(7200, im+4); %should be unnecessary
                tap_im = zeros(2048);
                for im = first:last
                    filename = fullfile(base_dir_taper, sprintf('full%04d.edf', im));
                    tap_im = tap_im+edf_read(filename);
                end
                tap_im = tap_im/(last-first+1);
                subplot(1, 2, 2); imshow(tap_im, [0 150]); hold on
                %plot a line to show where spot should be....
                plot([1024 1024+500*sind(eta(ii_theta))], [1024 1024-500*cosd(eta(ii_theta))], 'b')
            end

            keyboard
        end %%%%%%%%%%%
    end
end

    function [th, et, om, th_type] = sfForwardSimulate(R_vector)
    % SubFunction for foward simulate, just return a list of angles
        all_hkls = parameters.cryst.hklsp;
        tmp = sqrt(sum((all_hkls.*all_hkls), 2));
        normalised_hkls = all_hkls./(repmat(tmp, 1, 3));

        % Compute orientation matrix g
        g = gtMathsRod2OriMat(R_vector);

        % Express normalised hkl vectors in cartesian SAMPLE CS
        all_normals = gtVectorCryst2Lab(normalised_hkls, g);

        %predict the diffraction angles
        th = [];
        th_type = [];
        et = [];
        om = [];

        for ii_hkl = 1:length(all_hkls)
            try
                [tmp1, tmp2, tmp3] = gtPredictAngles( ...
                    all_normals(ii_hkl, :), all_hkls(ii_hkl, :), parameters);
                %record angles
                th = [th; tmp1'];
                et = [et; tmp2'];
                om = [om; tmp3'];
            end
        end

        th_type = parameters.cryst.thtype;

        %remove thoses outside of 0-180 degrees (not needed to search for pairs)
        dum = find(om > 180);
        th(dum) = [];
        et(dum) = [];
        om(dum) = [];
        th_type(dum) = [];
    end

    function [id, et_shift, om_shift] = sfFindClosestSpot(th, th_type, et, om, tol)

        id = []; et_shift = []; om_shift = [];
        [th et om]
        cmd = sprintf([ ...
            'SELECT pairid, eta, omega, plX, plY, plZ ' ...
            '   FROM %s ' ...
            '   WHERE (omega between %f and %f) ' ...
            '       AND (thetatype = %d) ' ...
            '       AND (eta between %f and %f)' ], ...
            taper_name, om-tol, om+tol, th_type, et-tol, et+tol);

        [possibles, et_pos, om_pos, pl1, pl2, pl3] = mym(cmd);
        tap_pl = [pl1 pl2 pl3];

        if isempty(possibles)
            disp('no corresponding pair found')
            return
        else
            disp('found something')
            if length(possibles) == 1
                id = possibles;
                et_shift = et_pos-et;
                om_shift = om_pos-om;
            else %try using criteria of closest plane normal vector to chose
                DCT_pl(1) = -sind(th)*cosd(om) - cosd(th)*sind(et)*sind(om);
                DCT_pl(2) = cosd(th)*sind(et)*cosd(om) - sind(th)*sind(om);
                DCT_pl(3) = cosd(th)*cosd(et);

                dev = real(acosd(dot(repmat(DCT_pl, length(possibles), 1), tap_pl, 2)));
                [~, dum] = min(dev); % index of the closest vector
                id = possibles(dum);
                et_shift = et_pos(dum)-et;
                om_shift = om_pos(dum)-om;
            end
        end
    end
end


