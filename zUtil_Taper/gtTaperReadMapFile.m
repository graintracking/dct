function grain = gtTaperReadMapFile(pathtofiles, map_file, detector_par)
% grain = gtTaperReadMapFile(pathtofiles,map_file,detector_par)

if ~exist(pathtofiles,'dir') || isempty(pathtofiles)
    disp(['directory ' pathtofiles ' does not exist...'])
    return
end
if ~exist(fullfile(pathtofiles,map_file),'file')
    disp(['file ' map_file ' does not exist...'])
    return
end
if ~exist(fullfile(pathtofiles, detector_par),'file')
    disp(['file ' detector_par ' does not exist...'])
    return
end

output_gff = [map_file(1:end-4) '.gff'];
% to be run in the analsysis directory

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% convert ubi file into gff file
% grainno  x  y  z  rodx  rody  rodz
% ... U11  U12  U13  U21  U22  U23  U31  U32  U33
% ... eps11  eps22  eps33  eps23  eps13  eps12

% somehow unix command doesn't search system folders???
[~, pcmd] = system('which ubi_to_gff.py');
% unix command adds newline char. Why???
pcmd = pcmd(1:end-1);
disp(pcmd)
cdir = pwd;
cd(pathtofiles);

cmd = [pcmd ' ' map_file ' ' detector_par ' ' output_gff];
disp(cmd)
[~,msg] = gtPythonCommand(cmd, true, true);
disp(msg)

fid = fopen(output_gff);
C = textscan(fid, '%*d %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f','Headerlines',1);
fclose(fid);
gff = cell2mat(C);

cen = gff(:,1:3); % x y z (mm)
rod = gff(:,4:6); % rx ry rz
U   = gff(:,7:15); % ... U11  U12  U13  U21  U22  U23  U31  U32  U33
eps = gff(:,16:end); % ... eps11  eps22  eps33  eps23  eps13  eps12

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% generate txt files from ubi file and save all into grain
%bash_script = fullfile('/users',getenv('USER'),'matlabDCT','zUtil_Taper','script_generate_files.sh');
if (isempty(whos('global', 'GT_MATLAB_HOME')))
    gtError('SETUP:no_such_variable', ...
          ['GT_MATLAB_HOME variable doesn''t exist in global workspace, '...
           'your environment is not sane, and you need to either ' ...
           're-initialise or re-run matlab.'])
end
global GT_MATLAB_HOME;
bash_script = fullfile(GT_MATLAB_HOME, 'zUtil_Taper','script_generate_files.sh');
cmd = [bash_script ' ' map_file];
[~,msg]=unix(cmd); disp(msg);

cd(cdir);

fid = fopen(fullfile(pathtofiles,'ids.txt'));
C = textscan(fid, '%*s %d');
fclose(fid);
A2 = cell2mat(C);

fid = fopen(fullfile(pathtofiles,'npeaks.txt'));
C = textscan(fid, '%*s %d');
fclose(fid);
A3 = cell2mat(C);

format longg
fid = fopen(fullfile(pathtofiles,'sum_of_all.txt'));
C = textscan(fid, '%f64');
fclose(fid);
A4 = cell2mat(C);

fid = fopen(fullfile(pathtofiles,'medians.txt'));
C = textscan(fid, '%f64');
fclose(fid);
A5 = cell2mat(C);

fid = fopen(fullfile(pathtofiles,'mins.txt'));
C = textscan(fid, '%f64');
fclose(fid);
A6 = cell2mat(C);

fid = fopen(fullfile(pathtofiles,'maxs.txt'));
C = textscan(fid, '%f64');
fclose(fid);
A7 = cell2mat(C);

fid = fopen(fullfile(pathtofiles,'means.txt'));
C = textscan(fid, '%f64');
fclose(fid);
A8 = cell2mat(C);

fid = fopen(fullfile(pathtofiles,'stds.txt'));
C = textscan(fid, '%f64');
fclose(fid);
A9 = cell2mat(C);

fid = fopen(fullfile(pathtofiles,'ns.txt'));
C = textscan(fid, '%d');
fclose(fid);
A10 = cell2mat(C);

grain = [];

for ii = 1:length(cen)

    grain{ii}.id       = ii;
    grain{ii}.center   = cen(ii,:);
    grain{ii}.R_vector = rod(ii,:);
    grain{ii}.R_onedge = false;
    grain{ii}.index    = A2(ii);
    grain{ii}.strain.strainT  = [eps(ii,1) eps(ii,6) eps(ii,5); ...
                                 eps(ii,6) eps(ii,2) eps(ii,4); ...
                                 eps(ii,5) eps(ii,4) eps(ii,3)];
    grain{ii}.strain.eps      = eps(ii,:);

    grain{ii}.stat.npeaks     = A3(ii);
    grain{ii}.stat.intmedian  = A5(ii);
    grain{ii}.stat.intmin     = A6(ii);
    grain{ii}.stat.intmax     = A7(ii);
    grain{ii}.stat.intmean    = A8(ii);
    grain{ii}.stat.intstd     = A9(ii);
    grain{ii}.stat.n          = A10(ii);
    grain{ii}.stat.sum_of_all = double(A4(ii));

    grain{ii}.stat.bbxsmean = 1;
    grain{ii}.stat.bbysmean = 1;

    grain{ii}.phaseid  = 1;

    grain{ii}.g        = [U(ii,1:3);U(ii,4:6);U(ii,7:9)];

end


end % end of function

