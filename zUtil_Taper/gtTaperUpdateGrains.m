function [grain, peaks_info] = gtTaperUpdateGrains(grain, peaks_info, cryst, saveFlag)
% [grain, peaks_info] = gtTaperUpdateGrains(grain, peaks_info, cryst, saveFlag)

if ~exist('saveFlag','var') || isempty(saveFlag)
    saveFlag = false;
end

if ~isfield(peaks_info, 'theta')
    [~, index] = sortrows([peaks_info.hkl peaks_info.tth/2],4);
    peaks_info.theta = peaks_info.tth/2;
    peaks_info.ind_sortByTheta = index;
end

% keep only families on detector
if ~isfield(peaks_info, 'thetatype')
    cryst.theta(cryst.theta > max(peaks_info.theta)) = [];

    for ii=1:length(peaks_info.theta)
        ind = find(abs(peaks_info.theta(ii) - cryst.theta) == min(abs(peaks_info.theta(ii) - cryst.theta)));
        peaks_info.thetatype(ii) = ind;
    end
end

if strcmp(cryst.crystal_system, 'hexagonal') && ~isfield(peaks_info, 'i')
    peaks_info.i = -(peaks_info.h+peaks_info.k);
    peaks_info.ir = -(peaks_info.hr+peaks_info.kr);
    peaks_info.hkl(:,4) = peaks_info.hkl(:,3);
    peaks_info.hkl(:,3) = -(peaks_info.hkl(:,1)+peaks_info.hkl(:,2));
    peaks_info.hklr(:,4) = peaks_info.hklr(:,3);
    peaks_info.hklr(:,3) = -(peaks_info.hklr(:,1)+peaks_info.hklr(:,2));
end

tocheck =[];
for jj=1:length(grain)
    fprintf('\b\b\b\b\b\b\b\b\b\b\b\b')

    index = jj-1;
    indGrain = find(peaks_info.labels == index);
    if grain{jj}.index ~= index
        tocheck = [tocheck; jj];
    end
    grain{jj}.refind    = indGrain';
    grain{jj}.npeaks    = length(indGrain);
    grain{jj}.difspotID = peaks_info.spot3d_id(indGrain)';
    grain{jj}.theta     = peaks_info.theta(indGrain)';
    grain{jj}.thetatype = peaks_info.thetatype(indGrain)';
    grain{jj}.eta       = peaks_info.eta(indGrain)';
    grain{jj}.hklsp     = peaks_info.hkl(indGrain,:)';
    grain{jj}.fc        = peaks_info.fc(indGrain)';
    grain{jj}.sc        = peaks_info.sc(indGrain)';
    grain{jj}.omega     = peaks_info.omega(indGrain)';
    grain{jj}.deltaf    = (peaks_info.Max_f(indGrain) - peaks_info.Min_f(indGrain))';
    grain{jj}.deltas    = (peaks_info.Max_s(indGrain) - peaks_info.Min_s(indGrain))';
    grain{jj}.deltao    = (peaks_info.Max_o(indGrain) - peaks_info.Min_o(indGrain))';
    grain{jj}.sumint    = peaks_info.sum_intensity(indGrain)';
    grain{jj}.g_vec     = peaks_info.g_vec(indGrain,:)'; %Angstron^-1
    grain{jj}.xyzl      = peaks_info.xyzl(indGrain,:)'; %um
    grain{jj}.drlv2     = peaks_info.drlv2(indGrain)';
    grain{jj}.n_pixels  = peaks_info.Number_of_pixels(indGrain)';


    list = gtCrystFindFamilies(grain{jj}.hklsp, cryst);
    grain{jj} = gtAddMatFile(grain{jj}, list, true, true, false);

    fprintf('grain # %04d',jj)
end

if saveFlag
    save('peaks_info.mat','peaks_info','-v7.3');
    disp('Saved peaks_info.mat')
end

end % end of function
