function [data, data2] = gtTaperReadFltFile(fltfile)
% GTTAPERREADFLTFILE  Reads .flt file output from indexing ImageD11
%     [data, data2] = gtTaperReadFltFile(fltfile)
%     -------------------------------------------
%     INPUT:
%       fltfile  = <string>     name of the .flt file with peaks information
%
%     OUTPUT:
%       data     = <struct>     structure with column values for each field <double Nx1>
%       data2    = <cell Nx1>   cell structure for each peak with fields <struct>
%
%
%     Fields for each peak are:
%       sc                = distortion corrected centre of mass, slow index direction
%       fc                = distortion corrected centre of mass, fast index direction
%       omega             = omega (rotation sequence) centre of mass
%       Number_of_pixels  =  ... in the 3D connected object
%       avg_intensity     = average intensity of the pixels in the object
%       s_raw             = raw position (no spatial), slow direction
%       f_raw             = raw position (no spatial), fast direction
%       sigs              = second moment, slow direction
%       sigf              = second moment, fast direction
%       covsf             = covariance for fast/slow
%       sigo              = second moment, omega direction (out of plane of image)
%       covso             = covariance for slow/omega
%       covfo             = covariance for fast/omega
%       sum_intensity     = total intensity for pixels above threshold
%       sum_intensity^2   = summed intensity squared [not useful?]
%       IMax_int          = maximum pixel
%       IMax_s            = array index slow direction for maximum pixel
%       IMax_f            = array index fast direction for maximum pixel
%       IMax_o            = array index omega direction for maximum pixel
%       Min_s             = minimum pixel position in slow direction
%       Max_s             = maximum pixel position in slow direction
%       Min_f             = minimum pixel position in fast direction
%       Max_f             = maximum pixel position in fast direction
%       Min_o             = minimum pixel position in omega direction
%       Max_o             = maximum pixel position in omega direction
%       dety              = unlikely to be correct flipped direction
%       detz              = unlikely to be correct flipped direction
%       onfirst           = blob is present on first image
%       onlast            = blob is present on last image
%       spot3d_id         = line number - 1 (titles) in the file on creation by peaksearch.py
%       xl                = x-component of scattering vector in the laboratory coordinate system (along the beam) with all angles at zero (units microns)
%       yl                = y-component of scattering vector in the laboratory coordinate system (toward the door) with all angles at zero (units microns)
%       zl                = z-component of scattering vector in the laboratory coordinate system (roughly up) with all angles at zero (units microns)
%       tth               = two theta angle (degrees)
%       eta               = azimuthal angle (degrees)
%       drlv2             =
%       labels            = grainID
%       tth_per_grain     =
%       eta_per_grain     =
%       gx                = x-component of scattering vector (along the beam) with all angles at zero (units 1/Å)
%       gy                = y-component of scattering vector (toward the door) with all angles at zero (units 1/Å)
%       gz                = z-component of scattering vector (roughly up) with all angles at zero (units 1/Å)
%       hr                =
%       kr                =
%       lr                =
%       h                 =
%       k                 =
%       l                 =
%       Lorentz           =
%       Lorentz_per_grain =
%
%
%     Version 001 11-06-2013 by LNervo


% read headers
fid = fopen(fltfile,'r');
titles = fgetl(fid);
if strfind(titles, 'filename') ~= 0
    titles = fgetl(fid);
end
fclose(fid);
C = textscan(titles, '%s');
titles = C{1};
titles(1)=[];

ncols = length(titles);

titles = arrayfun(@(num) strrep(titles{num},'^','_'),1:ncols, 'UniformOutput', false);

% read values
str_format = repmat('%f',1,ncols);
fid = fopen(fltfile,'r');
data_cell = textscan(fid,str_format,'CommentStyle','#','MultipleDelimsAsOne',true);
fclose(fid);

clear C fid
% take only existing columns

tmp = struct(titles{1},data_cell{1});
for ii=2:ncols
    tmp.(titles{ii}) = data_cell{ii};
end

% add some useful columns
tmp.g_vec = [tmp.gx tmp.gy tmp.gz];
tmp.hkl   = [tmp.h  tmp.k  tmp.l];
tmp.hklr  = [tmp.hr tmp.kr tmp.lr];
tmp.xyzl  = [tmp.xl tmp.yl tmp.zl];


tmp.hklsp = tmp.hkl;

data = tmp;

if nargout > 1
    tmp = cell(length(data_cell{1}),1);
    for jj=1:length(data_cell{1})
        for ii=1:ncols
            tmp{jj}.(titles{ii}) = data_cell{ii}(jj);
        end
    end

    data2 = tmp;
end

end % end of function
