#!/bin/bash
# input: fabledir/filename.map

CDIR="$PWD"

# define usage function
usage(){
        echo "Usage: $0 <fabledir>/<filename>.map"
        exit 1
}

# define is_file_exits function
# $f -> store argument passed to the script
exist(){
        local f="$1"
        [[ -f "$f" ]] && return 0 || return 1
}

# invoke  usage
# call usage() function if filename not supplied
[[ $# -eq 0 ]] && usage

# Invoke is_file_exits
if ( exist "$1" ) && [ -f "$2"];
then
  echo "File found"
  DPATH=`dirname $1`
  FILE=`basename $1`

  cd $DPATH

  less $FILE | grep name > names.txt
  less names.txt | awk -F":" '{print $1}' > ids.txt
  less $FILE | grep npks > npeaks.txt
  less $FILE | grep intensity > intensities.txt
  less intensities.txt | awk -F", " '{print $1}' | awk -F"= " '{print $2}' > sum_of_all.txt
  less intensities.txt | awk -F", " '{print $2}' | awk -F"= " '{print $2}' > medians.txt
  less intensities.txt | awk -F", " '{print $3}' | awk -F"= " '{print $2}' > mins.txt
  less intensities.txt | awk -F", " '{print $4}' | awk -F"= " '{print $2}' > maxs.txt
  less intensities.txt | awk -F", " '{print $5}' | awk -F"= " '{print $2}' > means.txt
  less intensities.txt | awk -F", " '{print $6}' | awk -F"= " '{print $2}' > stds.txt
  less intensities.txt | awk -F", " '{print $7}' | awk -F"= " '{print $2}' > ns.txt

  echo "The following txt files have been created in:"
  echo $DPATH
  ls -ltr *.txt
  cd $CDIR

else
  echo "File not found"
fi

