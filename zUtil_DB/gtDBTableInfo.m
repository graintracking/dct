function [info, columns] = gtDBTableInfo(tablename, verbose)
% [info, columns] = gtDBTableInfo(tablename, verbose)
%
% Returns information about a database table and its columns.

gtDBConnect();

if ~exist('verbose','var') || verbose
    mym(['DESCRIBE ' tablename])
end


[info.Name, info.Engine, info.Version, info.Row_format, info.Rows, ...
    info.Avg_row_length, info.Data_length, info.Max_data_length, ...
    info.Index_length, info.Data_free, info.Auto_increment, ...
    info.Create_time, info.Update_time, info.Check_time, info.Collation,...
    info.Checksum, info.Create_options, info.Comment] ...
    = mym(['SHOW table status WHERE Name = "' tablename '"']);

if ~isempty(info.Rows)
    [a,b,c,d,e,f] = mym(['SHOW columns FROM ' tablename]);
    columns = cell2struct({a,b,c,d,e,f},{'Field','Type','Null','Key','Default','Extra'},2);
else
    disp('Table is empty')
    columns = [];
end

end % end of function
