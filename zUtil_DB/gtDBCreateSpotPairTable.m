function check = gtDBCreateSpotPairTable(expname, overwriteflag, calibflag)
% GTDBCREATESPOTPAIRTABLE  Sets up the spot pair table 
%     check = gtDBCreateSpotPairTable(expname, overwriteflag, calibflag)
%     --------------------------------------------------------
%     Sets up a spot pair table for storing Friedel pair data. 
%     If calibflag is true, it also sets up a table for calibration with 
%     equivalent fields.
% 
%     OUTPUT:
%       check = <logical>  - true if there is already an existing table 
%                            with the specified name
%


if ~exist('expname','var')
  expname = [];
end

if ~exist('overwriteflag','var')
  overwriteflag = false;
end
if ~exist('calibflag','var')
  calibflag = false;  
end

check = false;

% if no name supplied, get it from parameters file
if isempty(expname)
    load('parameters.mat');
    %if there is a pair tablename (redundant)
    if isfield(parameters.database, 'pair_tablename')
			tablename = parameters.database.pair_tablename;
    elseif isfield(parameters.acq, 'pair_tablename')
			tablename = parameters.acq.pair_tablename;
    else % what should be standard convention
			tablename = sprintf('%sspotpairs',parameters.acq.name);
    end
    
    % Name of calibration table
    if calibflag
        tablename = sprintf('%spaircalib', parameters.acq.name);
    end

else
	if calibflag
		tablename = sprintf('%spaircalib', expname);
	else
		tablename = sprintf('%sspotpairs', expname);
	end
end


fprintf('Working on table:  %s\n',tablename);

gtDBConnect

if ~overwriteflag
  check0 = mym(sprintf('show tables like "%s"', tablename));
  if ~isempty(check0) 
      lastwarn('Table exists already! Run again using the overwriteflag=true');
      check = true;
      return
  end
end

mym(sprintf('drop table if exists %s', tablename));

%mysqlcmd=sprintf('create table %s (pairID int not null auto_increment, difAID int, difBID int, goodness double, theta double, eta double, omega double, etaB double, omegaB double, avintint double, avbbXsize double, avbbYsize double, samcentXA double, samcentYA double, samcentZA double, samcentXB double, samcentYB double, samcentZB double, ldirX double, ldirY double, ldirZ double, plX double, plY double, plZ double, thetatype int, grainID int, primary key (pairID))',tablename);
mysqlcmd = sprintf(['create table %s (pairID int not null auto_increment, '...
    'difAID int, difBID int, goodness double, meanerror double, ' ...
    'theta double, eta double, omega double, ' ...
    'etaB double, omegaB double, ' ...
    'avintint double, avbbXsize double, avbbYsize double, ' ...
    'samcentXA double, samcentYA double, samcentZA double, ' ...
    'samcentXB double, samcentYB double, samcentZB double, ' ...
    'ldirX double, ldirY double, ldirZ double, ' ...
    'plX double, plY double, plZ double, ' ...
    'phasetype int, thetatype int, grainID int, ' ...
    'primary key (pairID))'],tablename);

mym(mysqlcmd)

end % end of function
