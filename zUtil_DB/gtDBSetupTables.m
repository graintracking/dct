function gtDBSetupTables(acq)
% Sets up database tables for processing.

disp(' ')
disp('Establishing connection to the databse...');
gtDBConnect();

names = gtDBTableInfo(acq.name, false);

disp(' ')
disp(['Setting up database tables for: ' acq.name]);

check(1) = gtDBCreateDifblobTable(acq.name);
check(2) = gtDBCreateDifspotTable(acq.name);
check(3) = gtDBCreateExtspotTable(acq.name);

if strcmpi(acq.type, '360degree')
	check(4) = gtDBCreateSpotPairTable(acq.name, 0, 0);
end

if any(check)
	disp(' ')
	disp('Existing database tables containing the experiment name:')
	disp(names)
	warning('Database tables with the specified experiment name exist already!' )
	disp('Overwriting them may cause data loss for older datasets.')
	disp('Consult a supervisor before proceeding.')
	disp(' ')
    ch = inputwdefault('Would you like to overwrite the existing tables? [y/n]', 'n');
    
	if strcmpi(ch, 'y')
		disp('Overwriting existing database tables...')
		gtDBCreateDifblobTable(acq.name, 1);
        gtDBCreateDifspotTable(acq.name, 1);
        gtDBCreateExtspotTable(acq.name, 1);
        if strcmpi(acq.type, '360degree')
            gtDBCreateSpotPairTable(acq.name, 1, 0);
        end
	end

end
	

% try
%     disp(['Setting up database tables for ' acq.name]);
%     gtDBCreateDifblobTable(acq.name);
%     gtDBCreateDifspotTable(acq.name);
%     gtDBCreateExtspotTable(acq.name);
%     if strcmpi(acq.type, '360degree')
%         gtDBCreateSpotPairTable(acq.name, 0, 0);
%     end
% catch mexc
%     gtPrintException(mexc, 'Failed to set up tables! Try again, forcing overwite of existing tables?')
%     if strcmpi(check,'y')
%         gtDBCreateDifblobTable(acq.name, 1);
%         gtDBCreateDifspotTable(acq.name, 1);
%         gtDBCreateExtspotTable(acq.name, 1);
%         if strcmpi(acq.type, '360degree')
%             gtDBCreateSpotPairTable(acq.name, 1, 0);
%         end
%     end
% end
%
% clear check


end
