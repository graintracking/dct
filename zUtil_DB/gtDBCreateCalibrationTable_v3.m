% CalibrationTable is used for geometry correction (rotation axis to detector distance,
% detector tilts), contains pairs of perfectly matching diffraction spots.
% Should have exactly the same structure as SpotPairTable-s.
%
% 
% Used by:  gtMatchDifspotsForCalibration.m  - loads data
%           gtCorrectGeo.m  - reading data
%

function gtDBCreateCalibrationTable_v3(name)

fprintf('Working on %s\n',name);

gtDBConnect;
%mym('close')
%mym('open','mysql.esrf.fr','gtadmin','gtadmin')
%mym('use graintracking')

tablename_calibration=sprintf('%scalibration',name);

mym(sprintf('drop table if exists %s',tablename_calibration));
mysqlcmd=sprintf('create table %s (pairID int not null auto_increment, difAID int, difBID int, theta double, eta double, omega double, avintint double, avbbXsize double, avbbYsize double, samcentXA double, samcentYA double, samcentZA double, samcentXB double, samcentYB double, samcentZB double, ldirX double, ldirY double, ldirZ double, plX double, plY double, plZ double, thetatype int, grainID int, primary key (pairID))',tablename_calibration);
mym(mysqlcmd)

end
