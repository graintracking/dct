function [db_info, db_nan] = gtDBLoadTable(table, sort_var, varargin)
% GTDBLOADTABLE  Gets content of a table in the database, sorting values
%                according to sort_var in the sort_order
%     [db_info, db_nan] = gtDBLoadTable(table, [sort_var], varargin)
%     ----------------------------------------------------------------------------------------
%     fill_empties flag is used to fill empty key values for all the table
%     columns (useful for difspot table for example, where some blobs are
%     not converted into spots).
%
%     INPUT:
%       table       = <string>   table name
%       sort_var    = <string>   column to sort table {auto_increment column}
%     OPTIONAL INPUT (varargin):
%       sort_order  = <string>   sorting order {'asc'} | 'desc'
%       fill_empty  = <logical>  {true} | false
%       verbose     = <logical>  print output {false}
%
%     OUTPUT:
%
%       db_info     = <struct>   table content
%       db_nan      = <struct>   table NaN-entries indexes for each column
%
%     Version 001 09-01-2014 by LNervo

    conf = struct( ...
        'sort_order', 'asc', ...
        'fill_empty', true, ...
        'verbose', false);
    conf = parse_pv_pairs(conf, varargin);

    out = GtConditionalOutput(conf.verbose);

    gtDBConnect();
    % check if table exists
    check = mym(['SHOW tables LIKE "' table '"']);

    if (isempty(check))
        db_info = [];
        db_nan  = [];
        out.odisp('table does not exist. Quitting..')
        return
    end

    % get column names and values
    [columns, ~, ~, ~, ~, values] = mym(['SHOW columns FROM ' table]);
    if (isempty(columns))
        db_info = [];
        db_nan  = [];
        out.odisp('table is empty. Quitting..')
        return
    end

    % set sorting variable
    if (~exist('sort_var', 'var') || isempty(sort_var))
        index = findStringIntoCell(values, {'auto_increment'});
        if isempty(index)
            index = 1;
        end
        sort_var = columns{index};
    else
        index = findStringIntoCell(columns, {sort_var});
        if isempty(index)
            out.fprintf('wrong sort_var name: "%s". Quitting..', sort_var)
            return
        end
    end

    out.odisp(['sorting by ' sort_var ' ' conf.sort_order])
    % interrogate database
    mysqlcmd = ['SELECT * FROM ' table ' ORDER BY ' sort_var ' ' conf.sort_order];
    [values{:}] = mym(mysqlcmd);
    % get structure
    db_info = cell2struct(values, columns, 1);

    if (conf.fill_empty)
        % create vectors of size (numspots, 1)
        % these will have some empty elements since not all difspotIDs
        % exist (for instance some are filtered out because fo size
        % restrictions)

        num_entries = max(values{index});
        tmpTab = structfun(@(x) NaN(num_entries, 1), db_info, 'UniformOutput', false);

        % indexes of keys
        tmp = values{index};
        for ii = 1:length(columns)
            tmpTab.(columns{ii})(tmp) = values{ii};
        end
        db_info = tmpTab;
        % update values
        values = struct2cell(db_info);
    end

    if (nargout > 1)
        db_nan = cellfun(@(num) find(isnan(num)), values, 'UniformOutput', false);
        db_nan = cell2struct(db_nan, columns, 1);
    end
end

