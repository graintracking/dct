function check = gtDBCreateDifblobTable(expname,overwriteflag)
% GTDBCREATEDIFBLOBTABLE  Creates the difblob table
%     check = gtDBCreateDifblobTable(expname,overwriteflag)
%     ---------------------------------------------
%
%     OUTPUT:
%       check = <logical>  - true if there is already an existing table 
%                            with the specified name
%


if ~exist('expname','var')
    expname = [];
end

if ~exist('overwriteflag','var')
    overwriteflag = false;
end

check = false;

% if no name supplied, get it from parameters file
if isempty(expname)
    load('parameters.mat');
    expname = parameters.acq.name;
end

fprintf('Working on tables of experiment:  %s\n',expname);

gtDBConnect();

tablename_difblob        = sprintf('%sdifblob',expname);
tablename_difblobinfo    = sprintf('%sdifblobinfo',expname);
tablename_difblobdetails = sprintf('%sdifblobdetails',expname);

tablename_cracked_difblob        = sprintf('%scracked_difblob',expname);
tablename_cracked_difblobinfo    = sprintf('%scracked_difblobinfo',expname);
tablename_cracked_difblobdetails = sprintf('%scracked_difblobdetails',expname);



if ~overwriteflag
    %check if any tables exist already
    check1 = mym(sprintf('show tables like "%s"', tablename_difblob));
    check2 = mym(sprintf('show tables like "%s"', tablename_difblobinfo));
    check3 = mym(sprintf('show tables like "%s"', tablename_difblobdetails));

    check4 = mym(sprintf('show tables like "%s"', tablename_cracked_difblob));
    check5 = mym(sprintf('show tables like "%s"', tablename_cracked_difblobinfo));
    check6 = mym(sprintf('show tables like "%s"', tablename_cracked_difblobdetails));


    if (~isempty(check1) || ~isempty(check2) || ~isempty(check3) || ~isempty(check4) || ~isempty(check5) || ~isempty(check6))
        lastwarn('At least one of the tables exists already! Run again using the overwriteflag=true');
        check = true;
        return
    end
end
mym(sprintf('drop table if exists %s', tablename_difblob));
mym(sprintf('drop table if exists %s', tablename_difblobinfo));
mym(sprintf('drop table if exists %s', tablename_difblobdetails));

mym(sprintf('drop table if exists %s', tablename_cracked_difblob));
mym(sprintf('drop table if exists %s', tablename_cracked_difblobinfo));
mym(sprintf('drop table if exists %s', tablename_cracked_difblobdetails));

mysqlcmd = sprintf(...
    'create table %s (difblobID int unsigned not null auto_increment, primary key (difblobID)) ENGINE=MyISAM',...
    tablename_difblob);
mym(mysqlcmd)

mysqlcmd = sprintf(...
    'create table %s (difblobID int unsigned not null, xIndex smallint unsigned not null, yIndex smallint unsigned not null, zIndex smallint unsigned not null, graylevel double, index (difblobID),index (zIndex)) ENGINE=MyISAM',...
    tablename_difblobdetails);
%mysqlcmd=sprintf(...
%  'create table %s (difblobID int unsigned not null, xIndex smallint unsigned not null, yIndex smallint unsigned not null, zIndex smallint unsigned not null, graylevel double) ENGINE=MyISAM',...
%  tablename_difblobdetails);
mym(mysqlcmd)


mysqlcmd = sprintf(...
    'create table %s (chunkID int not null auto_increment, startIndex smallint unsigned not null, endIndex smallint unsigned not null, primary key (chunkID)) ENGINE=MyISAM',...
    tablename_difblobinfo);
mym(mysqlcmd);



mysqlcmd = sprintf(...
    'create table %s (difblobID int unsigned not null auto_increment, primary key (difblobID)) ENGINE=MyISAM',...
    tablename_cracked_difblob);
mym(mysqlcmd)

mysqlcmd = sprintf(...
    'create table %s (difblobID int unsigned not null, xIndex smallint unsigned not null, yIndex smallint unsigned not null, zIndex smallint unsigned not null, graylevel double, index (difblobID),index (zIndex)) ENGINE=MyISAM',...
    tablename_cracked_difblobdetails);
mym(mysqlcmd)

mysqlcmd = sprintf(...
    'create table %s (chunkID int not null auto_increment, startIndex smallint unsigned not null, endIndex smallint unsigned not null, primary key (chunkID)) ENGINE=MyISAM',...
    tablename_cracked_difblobinfo);
mym(mysqlcmd);

fprintf('Made tables: %s, %s, %s and %s, %s, %s\n', tablename_difblob, tablename_difblobdetails, tablename_difblobinfo,...
    tablename_cracked_difblob, tablename_cracked_difblobdetails, tablename_cracked_difblobinfo);

end % end of function
