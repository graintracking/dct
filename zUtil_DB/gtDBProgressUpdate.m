function gtDBProgressUpdate(funcname,scanname,first,last,current)
% gtDBProgressUpdate(funcname,scanname,first,last,current)
%
% makes a mark in the OAR progress table so you can track your jobs...
% Usage:
% Open the database BEFORE the main loop, and call gtDBProgressUpdate
% inside the loop.  The parameters to gtDBProgressUpdate are:
%  mfilename - use this always
%  acq.name  - or some string to identify the dataset
%  first, last - two integers indicating beginning and end of the
%  processing loop
%  i - the index of the current position
%
% Example:
%  gtDBConnect
%  for i=first:last
%      gtDBProgressUpdate(mfilename,acq.name,first,last,i);
%      do_some_work
%  end
%
% Monitoring progress
%   See gtDBProgressMonitor
%
% Version XXX 09-01-2014 by LNervo 


gtDBConnect();
if isempty(mym('show tables like "OARprogress"'))
    mym('CREATE table OARprogress (function varchar(80),scanname varchar(60),first int,last int,current int,ts timestamp default now())');
end

mysqlcmd = sprintf('INSERT into OARprogress (function,scanname,first,last,current) values ("%s","%s",%d,%d,%d)',...
             funcname,scanname,first,last,current);

mym(mysqlcmd)

end % end of function
