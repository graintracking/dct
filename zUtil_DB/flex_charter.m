if isempty(import)
  javaaddpath(which('jdom.jar'))


  import org.jdom.*
  import org.jdom.input.*
  import org.jdom.output.*
  import java.lang.*
  import java.io.File.*
end


% create data
mym('open','mysql','gtadmin','gtadmin');
mym('use graintracking');


products=mym('select distinct product from matlab');

for q=1:length(products)
  fprintf('Graphing %s\n',products{q});

  doc=Document;
  chart=Element('chart');
  doc.setRootElement(chart);


  axis_category=Element('axis_category');
  axis_category.setAttribute('size','12');
  axis_category.setAttribute('orientation','diagonal_down');
%  axis_category.setAttribute('skip','120');% num2str(skipval));

  axis_ticks=Element('axis_ticks');
  axis_ticks.setAttribute('value_ticks','false');
  axis_ticks.setAttribute('category_ticks','false');


  chart_border=Element('chart_border');
  chart_data=Element('chart_data');

  timestamp=mym(sprintf('select distinct timestamp from matlab where product="%s" order by timestamp',products{q}));
  times=1:length(timestamp);
%  skipval=floor((length(times)/10)-1);
  [used,total]=mym(sprintf('select used,total from matlab where product="%s"',products{q}));

  axis_value=Element('axis_value');
  axis_value.setAttribute('min','0');
  axis_value.setAttribute('max',num2str(total(1)));

  values=used';
  %values=used(1:10:end)';

  row=Element('row');
  row.addContent(Element('null'));
  for n=1:size(values,2)
   
    dnum=datenum(timestamp{n},'yyyy-mm-dd HH:MM:SS');
    [y,m,d,h,mi,s]=datevec(dnum);    
    if mi==0 && (mod(h,3)==0)
      tmp=Element('string');
      tmp.setText(datestr(dnum,'HH:MM (dd mmm)'));
      %      datestr(dnum,'HH:MM')
      row.addContent(tmp);

    elseif n==size(values,2)
      tmp=Element('string');
      tmp.setText(datestr(dnum,'HH:MM'));
      row.addContent(tmp);
    else
      tmp=Element('string');
      tmp.setText('');
      row.addContent(tmp);
    end

  end
  chart_data.addContent(row);

  row=Element('row');
  tmp=Element('string');
  tmp.setText(products{q});
  row.addContent(tmp);
  for m=1:size(values,2)
    tmp=Element('number');
    tmp.setText(num2str(values(m)));
    row.addContent(tmp);
  end
  chart_data.addContent(row);



  chart_grid_h=Element('chart_grid_h');
  chart_grid_v=Element('chart_grid_v');
  chart_pref=Element('chart_pref');
  chart_pref.setAttribute('point_shape','none');
  chart_rect=Element('chart_rect');
  chart_rect.setAttribute('x','80');
  chart_rect.setAttribute('y','55');
  chart_rect.setAttribute('width','650');
  chart_rect.setAttribute('height','360');

  
  
  chart_type=Element('chart_type');
  tmp=Element('string');
  tmp.setText('line');
  chart_type.addContent(tmp);

  chart_value=Element('chart_value');
  chart_value.setAttribute('position','cursor');

  chart_transition=Element('chart_transition');
%  chart_transition.setAttribute('type','drop');
%  chart_transition.setAttribute('delay','1');
%  chart_transition.setAttribute('duration','3');
%  chart_transition.setAttribute('order','series');
  draw=Element('draw');

  text=Element('text');
  text.setAttribute('color','ffffff');
  text.setAttribute('alpha','20');
  text.setAttribute('rotation','0');
  text.setAttribute('bold','true');
  text.setAttribute('x','80');
  text.setAttribute('y','375');
  text.setAttribute('height','60');
  text.setAttribute('h_align','left');
  text.setAttribute('v_align','bottom');
  if length(products{q})>10
  text.setAttribute('size','50');
  else
  text.setAttribute('size','80');
  end
  text.setText(sprintf('%s',products{q}));

  draw.addContent(text);

  %  image=Element('image');
  %  image.setAttribute('url','charts.swf?library_path=charts_library&xml_source=temp2.xml');
  %  draw.addContent(image);


  legend_label=Element('legend_label');
  legend_label.setAttribute('alpha','0');
  legend_label.setAttribute('layout','vertical');

  legend_rect=Element('legend_rect');
  legend_rect.setAttribute('x','5000');
  legend_rect.setAttribute('y','5000');
  
  legend_rect.setAttribute('width','0');
  legend_rect.setAttribute('height','0');
  legend_rect.setAttribute('fillalpha','0');
  legend_rect.setAttribute('linealpha','0');
  
  

  series_color=Element('series_color');

  chart.addContent(axis_category);
  chart.addContent(axis_ticks);
  chart.addContent(axis_value);
  chart.addContent(chart_border);
  chart.addContent(chart_data);
  chart.addContent(chart_grid_h);
  chart.addContent(chart_grid_v);
  chart.addContent(chart_pref);
  chart.addContent(chart_rect);
  chart.addContent(chart_type);
  chart.addContent(chart_value);
  chart.addContent(draw);
  chart.addContent(legend_label);
  chart.addContent(legend_rect);
  chart.addContent(series_color);
  chart.addContent(chart_transition);




  stream=java.io.FileOutputStream(sprintf('/data/id19/graintracking/www/matlablicense/flex_%02d.xml',q));

  outputter = XMLOutputter(Format.getPrettyFormat);

  outputter.output(doc,stream)

end

mym('close')



