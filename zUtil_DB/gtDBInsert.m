function cmd = gtDBInsert(table, titles, values)
% cmd = gtDBInsert(table, titles, values)

% modified 10/12/2007 to use mantissa/exponent (%g) for double precision
% modified 13/12/2007 to use mantissa/exponent (%0.20g) for double precision /Peter/
% Modified by Nicola Vigano', 2012, ID11 @ ESRF vigano@esrf.eu

    titles(1, :) = titles(:);
    values = reshape(values, [1 numel(values)]);
    numColumns = length(titles);

    if (length(values) ~= numColumns)
        error('DB:gtDBInsert:wrong_argument', ...
              'Size mismatch between columns (n: %d) and values (n: %d)', ...
              numColumns, length(values));
    end

    tuplePattern(1, 1:numColumns) = {'%0.20g'};
    tuplePattern(2, 1:numColumns) = {','};
    tuplePattern = [tuplePattern{1:end-1}];

    titles(2, :) = {','};
    cmd = [ 'INSERT INTO ' table ' (' titles{1:end-1} ') VALUES ' ...
            '(' sprintf(tuplePattern, values) ')'];
end
