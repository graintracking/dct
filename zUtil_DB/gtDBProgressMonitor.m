function [first,last,current,ts]=gtDBProgressMonitor(varargin)

param.function='';
param.scanname='';
param.first=1;
param=parse_pv_pairs(param,varargin);


gtDBConnect();
mysqlcmd=sprintf('select first,last,current,ts from condorprogress where function="%s" and scanname="%s"',param.function,param.scanname);
%
[first,last,current,ts]=mym(mysqlcmd);

hold off
plot(datenum(ts,'yyyy-mm-dd HH:MM:SS'),[current first last])
title(sprintf('Last point: %d @ %s',current(end),char(ts(end))))
datetick
shg

end

