function gtDBCopyAllTables(old_dataset_name, new_dataset_name)
% GTDBCOPYALLTABLES  Copy all the exiting tables for a dataset to a
%                    different set of tables for a different dataset
%                    including the values
%
%     gtDBCopyAllTables(old_dataset_name, new_dataset_name)
%     -----------------------------------------------------
%     INPUT: 
%       old_dataset_name
%       new_dataset_name
%
gtDBConnect();

names=mym(['SHOW TABLES LIKE "' old_dataset_name '%"']);

% creating tables list names
new_names=cell({});
for i=1:length(names)
    new_names{i}=strrep(names{i},old_dataset_name,new_dataset_name); 
end
% cell column
new_names=new_names';

for i=1:length(new_names)
    % creating tables
    if ~strcmp(new_names{i},[new_dataset_name 'bb'])
        mym(['DROP TABLE IF EXISTS ' new_names{i}])
        mym(['CREATE TABLE ' new_names{i} ' LIKE ' names{i}])
        mym(['INSERT ' new_names{i} ' SELECT * FROM ' names{i}])
        disp(['Created and filled table ' new_names{i}])
    else % creating a view
        mym(['DROP VIEW IF EXISTS ' new_names{i}])
        mym(['CREATE VIEW ' new_names{i} ' AS SELECT * FROM ' names{i}])
        disp(['Created and filled view ' new_names{i}])
    end
    mym(['SHOW COLUMNS FROM ' new_names{i}])
    mym(['SELECT COUNT(*) FROM ' new_names{i}])
end

% show tables names for the two datasets
mym(['SHOW TABLES LIKE "' old_dataset_name '%"'])
mym(['SHOW TABLES LIKE "' new_dataset_name '%"'])

end % end of function