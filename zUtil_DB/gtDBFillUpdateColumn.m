function gtDBFillUpdateColumn(table, variable, type, lastcolumn, queryname, queryvalues, values, varargin)
% GTDBFILLUPDATECOLUMN   Updates table's column with values adding a new column
%     gtDBFillUpdateColumn(table, variable, type, lastcolumn, queryname, queryvalues, values, varargin)
%     -------------------------------------------------------------------------------------------------
%     Adds/updates values to a specific set of rows in a table in the db.
%
%     OPTIONAL INPUT (varargin):
%       reset_all   = <logical>  {false} | true : first reset the specified column
%       verbose     = <logical>  {false} | true : print output
%
%     Usage:
%       for ii=1:length(dir('4_grains/phase_01/grain_*.mat'))
%           gr{ii} = load(sprintf('4_grains/phase_01/grain_%04d.mat', ii), 'difspotID');
%       end
%       difspotID = gtIndexAllGrainValues(gr, 'difspotID', [], [], [], false);
%       gtDBFillUpdateColumn([parameters.acq.name 'difspot'], 'grainID', 'int', 'integral', 'difspotID', difspotID, 1:length(gr))
%
%     Version 002 08-11-2013 by LNervo

    conf = struct( ...
        'reset_all', false, ...
        'verbose', false);
    conf = parse_pv_pairs(conf, varargin);

    out = GtConditionalOutput(conf.verbose);

    % if it doesn't already exist, we may need to add a grainid column to
    % difspot table

    [a, ~, ~, ~, ~, ~] = mym(['SHOW COLUMNS FROM ' table ' LIKE "' variable '"']);
    if isempty(a)
        % add the field
        mym(['ALTER TABLE graintracking.' table ' ADD COLUMN ' variable ' ' type ' AFTER ' lastcolumn]);
        disp(['adding column ' variable ' to the ' table ' table'])
    end

    if (conf.reset_all)
        mym(['UPDATE ' table ' SET ' variable '=null']);
        disp(['Variable ' variable ' has been resetted'])
    end
    % then loop through, updating according to the grain structure information
    gauge = GtGauge(length(queryvalues), 'done %04d', 'style', 'count', ...
        'verbose', true, 'start', false);

    for ii = 1:length(queryvalues)
        if iscell(queryvalues)
            ids = queryvalues{ii};
        else
            ids = queryvalues(ii);
        end
        switch(length(values))
            case length(queryvalues)
                if iscell(values)
                    curr_values = values{ii};
                else
                    curr_values = values(ii);
                end
            case 1
                curr_values = repmat(values, 1, length(ids));
            case length(ids)
                curr_values = values;
            otherwise
                return
        end
        if length(curr_values) == 1
            curr_values = repmat(curr_values, 1, length(ids));
        end
        for jj = 1:length(ids)
            mysqlcmd = gtDBUpdate(table, queryname, ids(jj), variable, curr_values(jj));
            mym(mysqlcmd);
        end
        gauge.incrementAndDisplay();
    end
    gauge.delete();

    out.fprintf('\nupdated column "%s" from "%s" table\n', variable, table)
end
