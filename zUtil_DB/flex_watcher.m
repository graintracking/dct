function flex_watcher()
% adds entrys to the database with matlab flex license vales

% mym('create table matlab (timestamp datetime,product char(40), total
% int,used int)')

if mym('status')
mym('open','mysql.esrf.fr','gtadmin','gtadmin');
mym('use graintracking');
end
timestamp=datestr(now,31);

fname=tempname;
[tmp,msg]=unix(['/sware/com/matlab_2006b/etc/lmstat -a > ' fname]);

pattern='Users\s+of\s+(?<product>\S+):.*\(Total\s+of\s+(?<total>\d+).*Total of (?<used>\d+).*$';
results=[];
fid=fopen(fname,'rt');
while ~feof(fid)
  l=fgetl(fid);
  tmp=regexp(l,pattern,'names');
  if ~isempty(tmp)
    mysqlcmd=sprintf('insert into matlab (timestamp,product,total,used) values ("%s","%s",%d,%d)',timestamp,tmp.product,str2num(tmp.total),str2num(tmp.used));
    mym(mysqlcmd);
    
  end
end

fclose(fid);
mym('close');
flex_charter();



end
