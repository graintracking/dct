 function cmd = gtDBUpdate(table,whereparam,wherevalue,varargin)
 % cmd = gtDBUpdate(table,whereparam,wherevalue,varargin)
 
 beginning = ['UPDATE ' table ' SET '];
 middle = [];
 for n=1:2:nargin-3
     middle = [middle sprintf('%s="%f",',varargin{n},varargin{n+1})];
 end
 middle(end) = ' '; % suppress last comma
 ending = sprintf('WHERE %s="%f"',whereparam,wherevalue);
 cmd = [beginning middle ending];
 
 end % end of function
