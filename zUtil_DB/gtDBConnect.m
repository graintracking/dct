function gtDBConnect(host, user, password, name)
% GTDBCONNECT Connects to the database
%     gtDBConnect(host, user, password, name)
%     ---------------------------------------
%     OPTIONAL INPUT:
%       host
%       user
%       password
%       name
%
% Specifying the parameters will kill the previous connection, and open a new
% one with the new parameters.
%
% NOTE: Specifying only some of the parameters is not supported!
%
% Modified 30/11/2007 GJ
%   No longer accepts gtadmin as a user - gtuser has all rights.
%
% Modified by Nicola Vigano', 2012, nicola.vigano@esrf.fr
%   Using global variable GT_DB, and better documentation/error reporting
%

if (nargin == 4)
    mym('close'); % will fail without a problem if no connection exists.
    mym('open', host, user, password)
    mym(sprintf('use %s', name));
elseif (nargin == 0)
    if (mym('status'))
        % if not already connected
        if (isempty(whos('global', 'GT_DB')))
            error('DB:no_such_variable', ...
                  ['GT_DB variable doesn''t exist in global workspace, '...
                   'your environment is not sane, and you need to either ' ...
                   're-initialise or re-run matlab.'])
        end
        global GT_DB
        if ~(isstruct(GT_DB) && isfield(GT_DB, 'name') ...
                && isfield(GT_DB, 'host') && isfield(GT_DB, 'user') ...
                && isfield(GT_DB, 'password'))
            error('DB:corrupted_variable', ...
                  ['GT_DB variable doesn''t contain the needed fields, '...
                   'your environment is not sane, and you need to either ' ...
                   're-initialise or re-run matlab.'])
        end

        fprintf('Connecting to the DB:\n')
        mym('open', GT_DB.host, GT_DB.user, GT_DB.password)
        mym(sprintf('use %s', GT_DB.name));
        fprintf('Connected.\n')
    else
        fprintf('Using previous connection to the DB.\n')
    end
else
    error('DB:wrong_parameter', ...
          'Specifying only some of the four parameters is not allowed.')
end

end % end of function





