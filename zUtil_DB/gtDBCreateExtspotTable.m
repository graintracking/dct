function check = gtDBCreateExtspotTable(expname, overwriteflag)
% create the extspot (and related) tables
% GTDBCREATEEXTSPOTTABLE  Create the extspot (and related) tables
%     check = gtDBCreateExtspotTable(expname, overwriteflag)
%     ---------------------------------------------
%
%     OUTPUT:
%       check = <logical>  - true if there is already an existing table 
%                            with the specified name
%


if ~exist('expname','var')
  expname = [];
end

if ~exist('overwriteflag','var')
  overwriteflag = false;
end

check = false;

% if no name supplied, get it from parameters file
if isempty(expname)
    load('parameters.mat');
    expname = parameters.acq.name;
end

fprintf('Working on tables of experiment:  %s\n',expname);

gtDBConnect

tablename_extspot = sprintf('%sextspot', expname);
tablename_snakes  = sprintf('%ssnakes', expname);
tablename_bboxes  = sprintf('%sbboxes', expname);
tablename_grainid = sprintf('%sgrainid', expname);
viewname_bb = sprintf('%sbb', expname);


if ~overwriteflag
    %check if any tables exist already
    checkA = mym(sprintf('show tables like "%s"', tablename_extspot));
    checkB = mym(sprintf('show tables like "%s"', tablename_snakes));
    checkC = mym(sprintf('show tables like "%s"', tablename_bboxes));
    checkD = mym(sprintf('show tables like "%s"', tablename_grainid));
    if ~isempty(checkA) || ~isempty(checkB) || ~isempty(checkC) || ~isempty(checkD)
        lastwarn('One of the tables exists already!Run again using the overwriteflag=true');
        check = true;
        return
    end
end


% create all tables necessary

% extspot
mym(sprintf('drop table if exists %s', tablename_extspot));
mysqlcmd = sprintf('create table %s (extspotID int not null,bbID int, searchbbID int, Area int, SnakeID int, GrainID int, primary key (extspotID))',tablename_extspot);
mym(mysqlcmd)

% snakes
mym(sprintf('drop table if exists %s', tablename_snakes));
mysqlcmd = sprintf('create table %s (snakeID int, x double, y double)', tablename_snakes);
mym(mysqlcmd);

% boundingboxes
mym(sprintf('drop table if exists %s', tablename_bboxes));
mysqlcmd = sprintf('create table %s (bboxID int not null auto_increment, extspotID int,Xorigin int, Yorigin int, Xsize int, Ysize int, Xcentroid double, Ycentroid double,primary key (bboxID))',tablename_bboxes);
mym(mysqlcmd);

mym(sprintf('drop view if exists %s', viewname_bb));
mym(sprintf('create view %sbb as select extspotID,grainID,ifnull(bbID,searchbbID) as bbID from %sextspot', expname, expname));



end % end of function
