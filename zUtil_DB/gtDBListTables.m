function names = gtDBListTables(name)
% GTDBLISTTABLES  List of all existing graintracking tables.
%
%     gtDBListTables(name)
%     -----------------------------------------------------
%     It lists all existing graintracking tables in the database.
%
%     Note! Mysql has an inconsistent behaviour and may return one table 
%     name even if there is no table present with that name. 
%
%     INPUT: 
%       name  = <char> string present in table name; '*' can be used as
%                      wildcard
%
%     OUTPUT:
%       names = <cell> table names found in the database
%
%     EXAMPLE
%       names = gtDBListTables('Al*small')
%

gtDBConnect();

name(name=='*') = '%';

names = mym(['SHOW TABLES LIKE "%', name, '%"']);


end