if 1
  disp('First compile - coral')
  mex -I/data/id19/graintracking/code/include -I/data/id19/graintracking/code/mysql/include ...
    -L/data/id19/graintracking/code/lib  ...
    -lmysqlclient -lz ...
    /data/id19/graintracking/matlab/zUtil_DB/mym.cpp
else
  disp('First compile - esrflinux')
  mex -I/data/id19/graintracking/code/include -I/data/id19/graintracking/code/mysql/include ...
    -L/data/id19/graintracking/code/lib64  ...
    -lmysqlclient -lz ...
    /data/id19/graintracking/matlab/zUtil_DB/mym.cpp
end
try
  disp('Then check it runs')
  mym
  disp('Then try a connection')

  mym('open','mysql','gtadmin','gtadmin')
  disp('Seemed to work')
catch
  lasterr
  disp('Did not work - probably the library problem')
  !ldd mym.mexa64 |grep mysql
end
