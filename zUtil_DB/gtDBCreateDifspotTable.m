function check = gtDBCreateDifspotTable(expname, overwriteflag)
% GTDBCREATEDIFSPOTTABLE  Creates the difspot table
%     check = gtDBCreateDifspotTable(expname, overwriteflag)
%     ---------------------------------------------
%
%     OUTPUT:
%       check = <logical>  - true if there is already an existing table 
%                            with the specified name
%



if ~exist('expname', 'var')
    expname = [];
end

if ~exist('overwriteflag', 'var')
    overwriteflag = false;
end

check = false;

% if no name supplied, get it from parameters file
if isempty(expname)
    load('parameters.mat');
    expname = parameters.acq.name;
end

fprintf('Working on table of experiment:  %s\n', expname);

gtDBConnect

tablename_difspot = sprintf('%sdifspot', expname);


if ~overwriteflag
    %check if any tables exist already
    check1 = mym(sprintf('show tables like "%s"', tablename_difspot));
    if ~isempty(check1)
        lastwarn('At least one of the tables exists already! Run again using the overwriteflag=true');
        check = true;
        return
    end
end


% difspot
mym(sprintf('drop table if exists %s', tablename_difspot));
mysqlcmd = sprintf('create table %s (difspotID int not null auto_increment, StartImage int, EndImage int, MaxImage int, ExtStartImage int, ExtEndImage int, Area double, CentroidX double, CentroidY double, CentroidImage double, BoundingBoxXorigin int, BoundingBoxYorigin int, BoundingBoxXsize int, BoundingBoxYsize int, Integral double, Bad int, primary key (difspotID))',tablename_difspot);
mym(mysqlcmd)

end

