function varargout = gtSubFigure(varargin)
% Create a figure within a grid-based layout. It's subplot for figures.
%
% gtSubFigure(m,n,p), or gtSubFigure(mnp), divides the screen into an m-by-n grid of
% tiles and creates a figure within the pth tile. Tiles are counted along the
% top row of the screen, then the second row, etc.
%
% If p is a vector, the figure is sized to cover all the gtSubFigure positions
% listed in p.
%
% gtSubFigure(m,n) creates a figure showing a m-by-n grid layout with tiles
% labeled in the order that they are numbered by gtSubFigure. This is useful for
% planning screen layouts, especially when one or more gtSubFigures will span
% multiple tiles (when p is a vector).
%
% h = gtSubFigure(...) returns a handle to the figure.
%
% Every call to gtSubFigure creates a new figure even if a figure exists at the
% location specified by m, n, and p. The existing figure is not made current or
% reused. Existing figures that are overlapped by new gtSubFigures are not
% deleted. This behavior is dissimilar to subplot.
%
% Example 1: Four non-overlapping figures.
%
% gtSubFigure(2,2,1)
% gtSubFigure(2,2,2)
% gtSubFigure(2,2,3)
% gtSubFigure(2,2,4)
%
% Example 2: Three non-overlapping figures of various sizes.
%
% gtSubFigure(4,4,[1 13])
% gtSubFigure(4,4,[2 4])
% gtSubFigure(4,4,[6 16])
%
% Example 3: Show the grid for a 3 x 5 layout.
%
% gtSubFigure(3,5)

% Padding to allow room for figure borders and menus
hpad = 10;
vpad = 50;

% Process input arguments
switch nargin
  case 0
    error('Not enough input arguments.')
  case 1
    m = floor(varargin{1}/100);
    n = rem(floor(varargin{1}/10),10);
    p = rem(varargin{1},10);
    screen_number = 1;
  case 2
    m = varargin{1};
    n = varargin{2};
    p = [];
    screen_number = 1;
  case 3
    m = varargin{1};
    n = varargin{2};
    p = varargin{3};
    screen_number = 1;
  case 4
    m = varargin{1};
    n = varargin{2};
    p = varargin{3};
    screen_number = varargin{4};
  otherwise
    error('Too many input arguments.')
end

% Error checking
if ~isscalar(m) || ~isscalar(n)
  error('Gird dimensions must be scalar values.')
end
if m < 0 || n < 0
  error('Grid dimensions must be greater than zero.')
end
if any(p > m*n)
  error('Position value exceeds grid size.')
end

if isempty(p)
  % Draw example grid using subplots
  p = m*n;
  f = figure('NumberTitle','Off',...
    'Name',sprintf('gtSubFigure tile numbering for a %i by %i grid',m,n));
  for ii = 1:p
    h(ii) = subplot(m,n,ii);
    set(h(ii),'Box','On',...
      'XTick',[],...
      'YTick',[],...
      'XTickLabel',[],...
      'YTickLabel',[])
    text(0.5,0.5,int2str(ii),...
      'FontSize',16,...
      'HorizontalAlignment','Center')
  end
  % Return handle if needed
  if nargout
    varargout{1} = f;
  end
  return
end

% Calculate tile size and spacing
scrsz = get(0, 'monitorpositions'); % 'ScreenSize');
switch screen_number
  case 0
    scrsz = [scrsz(1, 1:2), scrsz(end, 3:4)];
  case 1
    scrsz = scrsz(1, :);
  case 2
    scrsz = scrsz(2, :);
  otherwise
    scrsz = scrsz(1, :);
end
hstep = floor( (scrsz(3) - scrsz(1) + 1 - hpad) / n );
vstep = floor( (scrsz(4) - scrsz(2) + 1 - vpad) / m );
vsz = vstep - vpad;
hsz = hstep - hpad;

% Row and column positions of each gtSubFigure in p
r = ceil(p/n);
c = rem(p,n);
c(c==0) = n; % Special case

% Position of each gtSubFigure in p in pixels (left, right, bottom, top)
le = scrsz(1) - 1 + hpad + (c-1)*hstep;
ri = le + hsz;
bo = scrsz(2) - 1 + vpad + (m-r)*vstep;
to = bo + vsz;

% Position of a gtSubFigure that covers all gtSubFigures in p
le = min(le); % Leftmost left
ri = max(ri); % Rightmost right
bo = min(bo); % Lowest bottom
to = max(to); % Highest top

% Calculate figure position
pos = [le, bo, ri-le, to-bo]; % [left, bottom, width, height]

% Display figure
f = figure('Position',pos);

% Return handle if needed
if nargout
  varargout{1} = f;
end
