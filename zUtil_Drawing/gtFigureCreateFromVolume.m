function gtFigureCreateFromVolume( vol, varargin )
% gtFigureCreateFromVolume( vol, varargin )
%    conf = struct( ...
%        'cmap', jet, ...
%        'filename', '', ...
%        'plane', 'xy', ...
%        'layer', 1, ...
%        'clims', [], ...
%        'borders', 'no', ...
%        'colorbar', 'no');
% 

    conf = struct( ...
        'plane', 'xy', ...
        'slice', 1, ...
        'sample_shape', []);
    [conf, other_params] = parse_pv_pairs(conf, varargin);

    slice = gtVolumeGetSlice(vol, conf.plane, conf.slice);

    if (~isempty(conf.sample_shape))
        shape = conf.sample_shape;
        if (numel(shape) == 2)
            shape = [round(([size(vol, 1), size(vol, 2)] - shape) / 2), shape];
        end
        shape = [shape(1:2) shape(1:2)+shape(3:4)];
        slice = (slice - min(slice(:))) / (max(slice(:)) - min(slice(:)));
        slice = repmat(slice, [1 1 3]);

        red_vec = reshape([1 0 0], [1 1 3]);
        slice(shape(1):shape(3), shape(2), :) = repmat(red_vec, [(shape(3)-shape(1)+1) 1 1]);
        slice(shape(1):shape(3), shape(4), :) = repmat(red_vec, [(shape(3)-shape(1)+1) 1 1]);
        slice(shape(1), shape(2):shape(4), :) = repmat(red_vec, [1 (shape(4)-shape(2)+1) 1]);
        slice(shape(3), shape(2):shape(4), :) = repmat(red_vec, [1 (shape(4)-shape(2)+1) 1]);
    end

    gtFigureCreateFromPicture(slice, [], other_params{:});
end

