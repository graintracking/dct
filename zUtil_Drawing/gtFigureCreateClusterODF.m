function gtFigureCreateClusterODF(cluster, cluster_rec, grs, varargin)
% function gtFigureCreateClusterODF(cluster, cluster_rec, grs, varargin)

%%% XXX - Change behavior to use functionality from gtFigureCreateGvdmODF in here!!!

    conf = struct( ...
        'parameters', [], ...
        'percent', 0.0025, ...
        'filename', '', ...
        'clims', [], ...
        'pixel_to_cm', 0.5, ...
        'relative', false, ...
        'colorbar', false );
    conf = parse_pv_pairs(conf, varargin);

    if (~exist('cluster_rec', 'var') || isempty(cluster_rec))
        cluster_rec = gtLoadClusterRec(cluster.phaseid, cluster.gids);
    end
    if (~exist('grs', 'var') || isempty(grs))
        grs = gtLoadGrain(cluster.phaseid, cluster.gids);
    end

    internal_shift = cluster_rec.SEG.segbb(1:3) - cluster_rec.ODF6D.shift + 1;
    seg_vol_bb = [internal_shift, cluster_rec.SEG.segbb(4:6)];

    dm_vol(:, :, :, 3) = gtCrop(cluster_rec.ODF6D.voxels_avg_R_vectors(:, :, :, 3), seg_vol_bb);
    dm_vol(:, :, :, 2) = gtCrop(cluster_rec.ODF6D.voxels_avg_R_vectors(:, :, :, 2), seg_vol_bb);
    dm_vol(:, :, :, 1) = gtCrop(cluster_rec.ODF6D.voxels_avg_R_vectors(:, :, :, 1), seg_vol_bb);

    int_vol = gtCrop(cluster_rec.ODF6D.intensity, seg_vol_bb);

    gv_dm = gtDefDmvol2Gvdm(dm_vol);
    gv_mask = logical(cluster_rec.SEG.seg(:));
    gv_int = int_vol(:);

    if (isempty(conf.parameters))
        conf.parameters = gtLoadParameters();
    end
    ospace_res = tand(gtAcqGetOmegaStep(conf.parameters) / 2);

    grid_points = [ ...
        min(cluster_rec.ODF6D.R_vectors{1}, [], 1), ...
        max(cluster_rec.ODF6D.R_vectors{1}, [], 1), ...
        ospace_res([1 1 1])];
    grid_points(1:3) = grid_points(1:3) - mod(grid_points(1:3), ospace_res);
    grid_points(4:6) = grid_points(4:6) + mod(ospace_res - mod(grid_points(4:6), ospace_res), ospace_res);
    odf = gtGetODFFromGvdm(gv_dm(:, gv_mask), grid_points, gv_int(gv_mask));

    if (conf.relative)
        x_pos = 2 * atand(linspace(grid_points(1), grid_points(4), size(odf, 1)) - grs(1).R_vector(1));
        y_pos = 2 * atand(linspace(grid_points(2), grid_points(5), size(odf, 2)) - grs(1).R_vector(2));
        z_pos = 2 * atand(linspace(grid_points(3), grid_points(6), size(odf, 3)) - grs(1).R_vector(3));
    else
        x_pos = linspace(grid_points(1), grid_points(4), size(odf, 1));
        y_pos = linspace(grid_points(2), grid_points(5), size(odf, 2));
        z_pos = linspace(grid_points(3), grid_points(6), size(odf, 3));
    end

    f = figure();
    ax = axes('parent', f);
    thr_value = (max(odf(:)) - min(odf(:))) * conf.percent + min(odf(:));
    p = patch( ...
        isosurface(x_pos, y_pos, z_pos, permute(odf, [2 1 3]), thr_value), ...
        'FaceVertexAlphaData', 0.2, 'FaceAlpha', 'flat', 'parent', ax);
    set(p, 'FaceColor', 'cyan', 'EdgeColor', 'none')
%     set(p, 'FaceColor', [1 0.2 0.5], 'EdgeColor', 'none')
    camlight('right')
    camlight('left')
    lighting('gouraud')
    grid(ax, 'on')
    view(ax, 3);

    xlabel(ax, 'X (degrees)');
    ylabel(ax, 'Y (degrees)');
    zlabel(ax, 'Z (degrees)');

    r_vecs = cat(1, grs(:).R_vector);
    if (conf.relative)
        r_vecs = 2 * atand(r_vecs - repmat(grs(1).R_vector, [numel(grs), 1]));
    end
    hold(ax, 'on')
    scatter3(ax, r_vecs(:, 1), r_vecs(:, 2), r_vecs(:, 3), 60, 'red', 'filled')
    hold(ax, 'off')

    for ii = 1:numel(grs)
        text(r_vecs(ii, 1), r_vecs(ii, 2), r_vecs(ii, 3), ...
            sprintf('  \\leftarrow %d', grs(ii).id), 'parent', ax)
%             sprintf('   Average Orientation'), 'parent', ax)
    end

%     for ii = 1:numel(grs)
%         if (grs(ii).id == 171)
%             text(r_vecs(ii, 1), r_vecs(ii, 2), r_vecs(ii, 3), ...
%                 sprintf('%d \\rightarrow  ', grs(ii).id), 'parent', ax, ...
%                 'HorizontalAlignment', 'right')
%         elseif (grs(ii).id == 3)
%             text(r_vecs(ii, 1), r_vecs(ii, 2), r_vecs(ii, 3), ...
%                 sprintf('%d \\rightarrow            ', grs(ii).id), 'parent', ax, ...
%                 'HorizontalAlignment', 'right')
%         elseif (grs(ii).id == 67)
%             text(r_vecs(ii, 1), r_vecs(ii, 2), r_vecs(ii, 3), ...
%                 sprintf('   \\leftarrow %d', grs(ii).id), 'parent', ax)
%         elseif (grs(ii).id == 200)
%             text(r_vecs(ii, 1), r_vecs(ii, 2), r_vecs(ii, 3), ...
%                 sprintf('   \\leftarrow %d', grs(ii).id), 'parent', ax)
%         else
%             text(r_vecs(ii, 1), r_vecs(ii, 2), r_vecs(ii, 3), ...
%                 sprintf('  \\leftarrow %d', grs(ii).id), 'parent', ax)
%         end
%     end

    img_size = mean(size(odf));
    set_size(f, ax, conf, img_size([1 1]));

    drawnow();

    if (~isempty(conf.filename))
        gtFigureSaveToFile(f, conf.filename);
    end
end

function set_size(f, ax, conf, img_size)
    set(f, 'Units', 'centimeters')
    img_size = img_size * conf.pixel_to_cm;
    img_size = img_size([2 1]);
    set(ax, 'Units', 'centimeters')
    if (~conf.colorbar)
        position = [0, 0, img_size] + [0, 0, 3.5, 1.5];
        set(f, 'Position', position)
        set(f, 'Paperposition', position)
    else
        position = [0, 0, img_size] + [0, 0, 4.5, 2];
        set(f, 'Position', position)
        set(f, 'Paperposition', position)

        cb = colorbar('peer', ax, 'location', 'EastOutside');
        set(cb, 'Units', 'centimeters')
        position = [img_size(1)+2.35, 1, 0.65, img_size(2)];
        set(cb, 'Position', position)
    end
    position = [2, 1, img_size];
    set(ax, 'Position', position)
end
