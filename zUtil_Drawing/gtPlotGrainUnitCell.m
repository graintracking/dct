function [hp, hl] = gtPlotGrainUnitCell(vertices, caxis, faces, cmap, hlight, linecolor, alpha, drawfaces)
% GTPLOTGRAINUNITCELL  Draws the grain unit cell given its properties
%
%     [hp, hl] = gtPlotGrainUnitCell(vertices, caxis, faces, cmap, [hlight], linecolor, alpha, drawfaces)
%     ---------------------------------------------------------------------------------------------------
%     INPUT:
%       vertices    = <double>  vertices coordinates (Nx3)
%       caxis       = <double>  coordinates of the c-axis (2x3)
%       faces       = <double>  faces
%       cmap        = <double>  facecolor property (1x3)
%       hlight      = <logical> flag to highlight the unit cell (thicker edges)
%       linecolor   = linecolor property (edges)
%       alpha       = <double>  facealpha property
%       drawfaces   = <logical> show patch on faces
%
%     OUTPUT:
%       hp          = <handle>  patch handles
%       hl          = <handle>  c-axis handle (if existing)
%
%     Version 002 06-06-2013 by LNervo

if ~exist('hlight','var') ||  isempty(hlight)
    hlight = false;
end
if (~drawfaces)
    linecolor = cmap;
    cmap = 'none';
    alpha = 1;
end
if ~iscell(faces)
    cmap = repmat(cmap,size(faces,1),1);
end

if iscell(faces)
    hp = cellfun(@(num) ...
             patch('Vertices', vertices, 'Faces', num, ...
                   'FaceColor', cmap, ...
                   'EdgeColor', linecolor, 'FaceAlpha', alpha), faces );
else
    hp = patch('Vertices', vertices, 'Faces', faces, ...
               'FaceColor', cmap, ...
               'EdgeColor', linecolor, 'FaceAlpha', alpha);
end
% draw the caxis
if ~isempty(caxis)
    hl = line(caxis(:,1),caxis(:,2),caxis(:,3),'Color',linecolor);
else
    hl = [];
end

if (hlight) || (~drawfaces)
    set(hp,'LineWidth',2)
    set(hl,'LineWidth',2)
else
    set(hp,'LineWidth',1)
    set(hl,'LineWidth',1)
end

if (drawfaces)
    set(hp,'FaceLighting','none')
end

end  % end of function
