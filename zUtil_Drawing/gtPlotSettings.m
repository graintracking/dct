function gtPlotSettings(ha, varargin)
% GTPLOTSETTINGS  Sets plot parameters
%     gtPlotSettings(ha, varargin)
%     ----------------------------
%     Varargin are not passed by pairs.
%
%     ha = axis handle
%
%     OPTIONAL INPUT:
%       XLabel    = <string>
%       YLabel    = <string>
%       ZLabel    = <string>
%       Title     = <string>
%       XLim      = <double>
%       XTickStep = <double>
%       YLim      = <double>
%       YTickStep = <double>
%       Zlim      = <double>
%       ZTickStep = <double>
%       legend    = <cell>
%       legpos    = <string>

if ~exist('ha','var') || isempty(ha)
    ha = gca;
end

if ~isempty(varargin)

    varargin( arrayfun(@(num) isempty(varargin{num}),1:length(varargin)) ) = [];

    if length(varargin) >= 4 && isa(varargin{1},'char') && isa(varargin{2},'char') && isa(varargin{3},'char') && isa(varargin{4},'char')
        set(get(ha,'XLabel'),'String',varargin{1})
        set(get(ha,'YLabel'),'String',varargin{2})
        set(get(ha,'ZLabel'),'String',varargin{3})
        set(get(ha,'Title'),'String',varargin{4})
        varargin(1:4)=[];
    elseif length(varargin) >= 3 && isa(varargin{1},'char') && isa(varargin{2},'char') && isa(varargin{3},'char')
        set(get(ha,'XLabel'),'String',varargin{1})
        set(get(ha,'YLabel'),'String',varargin{2})
        set(get(ha,'Title'),'String',varargin{3})
        varargin(1:3)=[];
    elseif length(varargin) >= 2 && isa(varargin{1},'char') && isa(varargin{2},'char')
        set(get(ha,'XLabel'),'String',varargin{1})
        set(get(ha,'YLabel'),'String',varargin{2})
        varargin(1:2)=[];
    elseif length(varargin) >= 1 && isa(varargin{1},'char')
        set(get(ha,'XLabel'),'String',varargin{1})
        varargin(1)=[];
    end

    if ~isempty(varargin) && isvector(varargin{1}) && size(varargin{1},2)==2 && ~iscell(varargin{1})
        set(ha,'XLim',varargin{1})
        if length(varargin) > 1 && size(varargin{2},2)==1 && isnumeric(varargin{2})
            set(ha,'XTick',varargin{1}(1):varargin{2}:varargin{1}(2))
            varargin(2)=[];
        end
        varargin(1)=[];
    end

    if ~isempty(varargin) && isvector(varargin{1}) && size(varargin{1},2)==2 && ~iscell(varargin{1})
        set(ha,'YLim',varargin{1})
        if length(varargin) > 1 && size(varargin{2},2)==1 && isnumeric(varargin{2})
            set(ha,'YTick',varargin{1}(1):varargin{2}:varargin{1}(2))
            varargin(2)=[];
        end
        varargin(1)=[];
    end

    if ~isempty(varargin) && isvector(varargin{1}) && size(varargin{1},2)==2 && ~iscell(varargin{1})
        set(ha,'ZLim',varargin{1})
        if length(varargin) > 1 && size(varargin{2},2)==1 && isnumeric(varargin{2})
            set(ha,'ZTick',varargin{1}(1):varargin{2}:varargin{1}(2))
            varargin(2)=[];
        end
        varargin(1)=[];
    end

    if ~isempty(varargin) && iscell(varargin{1})
        legend(varargin{1})
        if length(varargin) > 1 && isa(varargin{2},'char')
            set(findobj(gcf,'tag','legend'),'Location',varargin{2})
            varargin(2)=[];
        end
        varargin(1)=[];
    end

end

set(get(ha,'Title'),'Visible','on')

set(gcf,'Position',[1089 583 800 450]);
set(ha,'Position',[0.0775 0.0967 0.9012 0.8500]);
grid(ha,'on');
box(ha,'on');
set(gcf,'Color',[1 1 1])
set(ha,'FontSize',12)
set(get(ha,'Title'),'FontSize',12)
set(get(ha,'XLabel'),'FontSize',12)
set(get(ha,'YLabel'),'FontSize',12)
set(get(ha,'ZLabel'),'FontSize',12)
set(ha,'GridLineStyle',':')
set(ha,'LineWidth',1)
set(ha,'MinorGridLineStyle',':')
set(ha,'LineStyleOrder',{'-o',':s','--*','-.+','-x',':d','--.','p','h'})


end % end of function
