function fig2jpg(fname);
% FIG2JPG.M Converts Matlab figure to a JPEG
% Usage:
%       fig2jpg(filename)
%       filename is every EXCEPT the extension - .fig and .jpg will be
%       appended automatically

% Greg Johnson, September 2006

  hgload([fname '.fig']);
  print('-djpeg',[fname '.jpg']);
  close(gcf)
end
