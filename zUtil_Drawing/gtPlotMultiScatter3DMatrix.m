function [h_scattergroups, conf] = gtPlotMultiScatter3DMatrix(data, varargin)
% GTPLOTMULTISCATTER3DMATRIX  Plots matrices in the same plot
%     [h_obj, data_obj] = gtPlotMultiScatter3DMatrix(data, varargin)
%     --------------------------------------------------------------
%     INPUT:
%       data          = <cell array>   UVW data to plot for each dataset <cell> (Nx3) 
%
%     OPTIONAL INPUT (parse by pairs as varargin):
%       ids           = <cell array>   indexes to consider for each dataset <double> {all}
%       markers       = <cell array>   marker for each dataset <string> (1xP) {'xr','.b','og','xk','sm','db'}
%       cmap          = <cell array>   cmap for each point for each dataset <double> (Nx3) {[]}
%       labels        = <cell array>   entry in the legend for each dataset <string> (1xP) {[]}
%
%       leg_show      = <logical>      display or not the legend {true} | false
%       leg_pos       = <string>       legend position {[0.75 0.8 0.2 0.05]}
%       leg_color     = <double>       legend bkg RGB color {[1 1 1]}
%       leg_textcolor = <double>       legend text RGB color {[0 0 0]}
%       leg_replace   = <logical>      replace existing legend position and colors {true}
%
%       view          = <double>       axes view property {[0 90]}
%       xlim          = <double>       x axis limits {[]}
%       ylim          = <double>       y axis limits {[]}
%       zlim          = <double>       z axis limits {[]}
%       axes_labels   = <cell array>   labels for axes X Y and Z respectively <string> {'X','Y','Z'}
%       axes_pos      = <double>       axes position {[0.05 0.1 0.75 0.8]}
%       axes_tag      = <string>       axes tag {''}
%       axes_replace  = <logical>      reset position, in normalized units and tag {true}
%       axes_title    = <string>       axes title {''}
%       rotate3d      = <logical>      enables axes rotation in 3D {false}
%
%       fig_pos       = <double>       figure position {[0.2 0.1 0.65 0.7]}
%       fig_replace   = <logical>      reset position,in normalized units {true}
%
%       h_figure      = <handle>       figure handle {[]}
%       h_axes        = <handle>       axes handle {[]}
%       h_points      = <handle>       scatter points handle {[]}
%       h_legend      = <handle>       legend handle {[]}
%
%       debug         = <logical>      display comments {true}
%
%     OUTPUT:
%       h_obj         = <handle>       objects handles (1xP)
%       data_obj      = <handle>       data plotted in the figure <double> (Mx3)
%
%
%     Version 003 27-09-2013 by LNervo

    % cell arrays
    conf.ids     = arrayfun(@(num) 1:size(data{num}, 1), 1:length(data), 'UniformOutput', false);
    conf.markers = {'s','o','o','x','.','d'};
    conf.cmap    = {'r','b','g','k','m','b'};
    conf.labels  = cell(size(data));
    % legend
    conf.leg_show      = true;
    conf.leg_pos       = [0.75 0.8 0.2 0.05];
    conf.leg_color     = [1 1 1];
    conf.leg_textcolor = [0 0 0];
    conf.leg_replace   = true;
    % axes
    conf.view         = [0 90];
    conf.xlim         = [];
    conf.ylim         = [];
    conf.zlim         = [];
    conf.axes_labels  = {'X','Y','Z'};
    conf.axes_pos     = [0.05 0.1 0.75 0.8];
    conf.axes_tag     = 'axes';
    conf.axes_replace = true;
    conf.axes_title   = '';
    conf.rotate3d     = false;
    % figure
    conf.fig_pos     = [0.2 0.1 0.65 0.7];
    conf.fig_replace = true;
    % handles
    conf.h_figure = [];
    conf.h_axes   = [];
    conf.h_legend = [];
    % debug
    conf.debug = true;
    conf.filled = true;

    conf = parse_pv_pairs(conf, varargin);

    % pick up only the right number of different markers
    conf.markers = conf.markers(1:numel(data));
    conf.data    = [];

    % create the figure
    if isempty(conf.h_figure)
        f1 = figure();
        conf.h_figure = f1;
    else
        f1 = conf.h_figure;
        conf.fig_replace = false;
    end
    figure(f1)
    if (conf.fig_replace)
        set(f1, 'Units', 'normalized', 'Position', conf.fig_pos)
    end

    % create the axes
    if ~isempty(conf.h_axes)
        axes(conf.h_axes)
    else
        conf.h_axes = gca;
        conf.axes_replace = false;
    end
    if (conf.axes_replace)
        set(conf.h_axes, 'Position', conf.axes_pos, 'Units', 'normalized', 'Tag', conf.axes_tag);
    end
    hold(conf.h_axes, 'on');

    % drawing
    h_scattergroups = cell(numel(data), 1);
    for ii = 1:numel(data)
        % get values
        tmp_var = data{ii};
        % get ids
        tmp_var = tmp_var(conf.ids{ii}, :);
        if ~isempty(tmp_var)
            if (~isempty(conf.cmap{ii}) && isnumeric(conf.cmap{ii}))
                cmap = conf.cmap{ii};
                if (all(cmap(1, :) == [0 0 0]) && (size(cmap, 1) > 1))
                    cmap = cmap(2:end, :);
                end
                if (size(cmap, 1) ~= length(tmp_var))
                    cmap  = repmat(cmap, length(tmp_var), 1);
                end
                cmap = cmap(conf.ids{ii},:);

                conf.cmap{ii} = cmap;
                if (conf.filled)
                    h_scattergroups{ii} = scatter3(tmp_var(:, 1), tmp_var(:, 2), tmp_var(:, 3), 'fill', conf.markers{ii}, 'CData', cmap);
                else
                    h_scattergroups{ii} = scatter3(tmp_var(:, 1), tmp_var(:, 2), tmp_var(:, 3), conf.markers{ii}, 'CData', cmap);
                end
            else
                if (conf.filled)
                    h_scattergroups{ii} = scatter3(tmp_var(:, 1), tmp_var(:, 2), tmp_var(:, 3), 'fill', conf.markers{ii});
                else
                    h_scattergroups{ii} = scatter3(tmp_var(:, 1), tmp_var(:, 2), tmp_var(:, 3), conf.markers{ii});
                end
            end
            % children of handle are stacked in the reverse order : from the
            % last to the first
            h_child = get(h_scattergroups{ii}, 'Children');
            h_child = sort(h_child, 'ascend');
            for jj = 1:length(h_child)
                sfSetInfo(h_child(jj), tmp_var(jj, :));
                set(h_child(jj), 'Tag', sprintf('data_%d_%d', ii, jj))
            end
        end
        conf.data{ii} = tmp_var;
    end

    % avoid warning Painter's mode
    set(gcf, 'Renderer', 'zbuffer');

    % legend
    leg = findobj(conf.h_figure, 'Tag', 'legend');
    if (~isempty(leg))
        leg_data          = get(leg, 'UserData');
        leg_data.lstrings = [leg_data.lstrings; conf.labels];
        leg_data.handles  = [leg_data.handles; [h_scattergroups{:}]];

        conf.leg_pos       = get(leg, 'Position');
        conf.leg_color     = get(leg, 'Color');
        conf.leg_textcolor = get(leg, 'TextColor');
        if (conf.leg_replace)
            set(leg, 'UserData', leg_data)
        end
    end
    if (conf.leg_show)
        if isempty(leg)
            leg = legend(leg, conf.labels', 'Parent', conf.h_figure,...
              'Position', conf.leg_pos, 'Color', conf.leg_color, ...
              'TextColor', conf.leg_textcolor);
        end
        conf.h_legend = leg;
    end

    % axes options
    axis(conf.h_axes,'equal');
    xlabel(conf.axes_labels{1});
    ylabel(conf.axes_labels{2});
    zlabel(conf.axes_labels{3});

    if (~isempty(conf.xlim))
        set(conf.h_axes, 'XLim', conf.xlim)
    end
    if (~isempty(conf.ylim))
        set(conf.h_axes, 'YLim', conf.ylim)
    end
    if (~isempty(conf.zlim))
        set(conf.h_axes, 'ZLim', conf.zlim)
    end
    box(conf.h_axes, 'on');
    grid(conf.h_axes, 'on');
    if ~isempty(conf.axes_title)
        set(get(conf.h_axes, 'Title'), 'String', conf.axes_title);
        set(f1, 'Name', strrep(conf.axes_title, '\_', '_'))
    end
    conf.h_figure = f1;
    if (conf.rotate3d)
        % 3D rotation ON
        h=rotate3d(conf.h_figure);
        set(h,'Enable','on','RotateStyle','Box');
    end
    view(conf.view);

    % save application data
    set(conf.h_figure, 'UserData', conf);
    % show options
    print_structure(conf, 'options', false, conf.debug)
end

function sfSetInfo(hObj, xyz)

    text = sprintf('%%s: %5.1f  %%s: %5.1f  %%s: %5.1f  %%s: %s  %%s',xyz(1),xyz(2),xyz(3),'%5.1f') ;

    set(hObj, 'UserData', text)

end