function [hf, ha] = gtPlotScatter3DMatrix(data, varargin)
%     [hf, ha] = gtPlotScatter3DMatrix(data, varargin)
%     ------------------------------------------------
%     <<<<< DEPRECATED >>>>>
%
%     INPUT:
%       data      = <cell>   data to plot (Nx3)
%
%     OPTIONAL INPUT:
%       ids       = <double>     
%       marker    = <string>     
%       cmap      = <double>     
%       label     = <string>     
%       legend    = <logical>    
%       legendpos = <string>     
%       view      = <double>

% cell arrays
app.ids       = 1:length(data);
app.marker    = 'xr';
app.cmap      = [];
app.label     = 'data1';
% others
app.legend    = true;
app.legendpos = 'NorthEast';
app.view      = [0 90];

app = parse_pv_pairs(app, varargin);

% create the figure
set(gcf,'Visible','off');
hold(gca, 'on');

if ~isempty(app.cmap)
    app.marker = 'h';
else
    app.marker = 'o';
end

data = data(app.ids,:);
if ~isempty(app.cmap)
    cmap = app.cmap;
    if all(cmap(1,:)==[0 0 0])
      cmap = cmap(2:end,:);
    end
    cmap = cmap(app.ids,:);
    app.cmap = cmap;
    scatter3(data(:,1), data(:,2), data(:,3), app.marker, 'CData', cmap);
else
    scatter3(data(:,1), data(:,2), data(:,3), app.marker);
end


print_structure(app,'options:',false,true)

% drawing options
axis equal
xlabel('X');
ylabel('Y');
zlabel('Z');

grid(gca, 'on');
set(gcf, 'Visible', 'on');

if app.legend
    leg = legend('Parent',gcf,'String',app.label,'Location',app.legendpos);
end

hf = gcf;
h=rotate3d(hf);
set(h,'Enable','on','RotateStyle','Box');
view(app.view);

if nargout == 2
    ha = gca;
end

end % end of function
