function gtFigureAddExtras(ax, im_props, extras)
% function gtFigureAddExtras(ax, im_props, extras)

    num_extras = numel(extras);
    original_image = permute(getimage(ax), [2 1 3]);

    if (iscell(extras))
        for ii_ex = 1:num_extras
            apply_extras(ax, im_props, extras{ii_ex}, original_image)
        end
    else
        for ii_ex = 1:num_extras
            apply_extras(ax, im_props, extras(ii_ex), original_image)
        end
    end
end

function apply_extras(ax, im_props, extras, original_image)
    switch (extras.type)
        case 'unit_bar'
            add_unit_bar(ax, im_props, extras);
        case {'line_vertical', 'line_horizontal'}
            add_line(ax, im_props, extras)
        case 'box'
            add_box(ax, extras)
        case 'point'
            add_point(ax, extras)
        case 'text'
            add_text(ax, extras)
        case 'arrow'
            add_arrow(ax, extras)
        case 'segment'
            add_segment(ax, extras)
        case 'inset_zoom'
            add_inset(ax, extras, 'zoom', original_image)
        case 'inset_image'
            add_inset(ax, extras, 'image')
    end
end

function add_unit_bar(ax, im_props, ex_props)
    bar_length = ex_props.line_length / im_props.pixel_size(2);

    switch (ex_props.position)
        case 'north_west'
            xs_l = ex_props.margin([1 1]) + [0 bar_length];
            ys_l = ex_props.margin([2 2]);
            x_t = ex_props.margin(1);
            y_t = ex_props.margin(2) + 1; % + ex_props.line_width
            font_placing_vert = 'top';
            font_placing_horz = 'left';
        case 'south_west'
            xs_l = ex_props.margin([1 1]) + [0 bar_length];
            ys_l = im_props.size([2 2]) - ex_props.margin([2 2]);
            x_t = ex_props.margin(1);
            y_t = im_props.size(2) - ex_props.margin(2) - 1; % - ex_props.line_width
            font_placing_vert = 'bottom';
            font_placing_horz = 'left';
        case 'north_est'
            xs_l = im_props.size([1 1]) - ex_props.margin([1 1]) - [0 bar_length];
            ys_l = ex_props.margin([2 2]);
            x_t = im_props.size(1) - ex_props.margin(1); % - bar_length
            y_t = ex_props.margin(2) + 1; % + ex_props.line_width
            font_placing_vert = 'top';
            font_placing_horz = 'right';
        case 'south_est'
            xs_l = im_props.size([1 1]) - ex_props.margin([1 1]) - [0 bar_length];
            ys_l = im_props.size([2 2]) - ex_props.margin([2 2]);
            x_t = im_props.size(1) - ex_props.margin(1); % - bar_length
            y_t = im_props.size(2) - ex_props.margin(2) - 1; % - ex_props.line_width
            font_placing_vert = 'bottom';
            font_placing_horz = 'right';
    end

    line(xs_l, ys_l, 'parent', ax, 'Color', ex_props.color, 'LineWidth', ex_props.line_width)
    text(x_t, y_t, sprintf('%g %s', ex_props.line_length, ex_props.base_unit), ...
        'parent', ax, 'Color', ex_props.color,  ...
        'FontSize', ex_props.font_size, ...
        'FontAngle', ex_props.font_angle, ...
        'FontWeight', ex_props.font_weight, ...
        'VerticalAlignment', font_placing_vert, ...
        'HorizontalAlignment', font_placing_horz )
end

function add_line(ax, im_props, ex_props)

    switch (ex_props.type)
        case 'line_vertical'
            xs_l = ex_props.position([1 1]);
            ys_l = [1 im_props.size(2)];
        case 'line_horizontal'
            xs_l = [1 im_props.size(1)];
            ys_l = ex_props.position([1 1]);
    end

    line(xs_l, ys_l, 'parent', ax, 'Color', ex_props.color, ...
        'LineWidth', ex_props.line_width, 'LineStyle', ex_props.line_style)
end

function add_box(ax, ex_props)

    x_nw = ex_props.position(1);
    y_nw = ex_props.position(2);

    x_sw = ex_props.position(1);
    y_sw = ex_props.position(2) + ex_props.position(4) - 1;

    x_ne = ex_props.position(1) + ex_props.position(3) - 1;
    y_ne = ex_props.position(2);

    x_se = ex_props.position(1) + ex_props.position(3) - 1;
    y_se = ex_props.position(2) + ex_props.position(4) - 1;

    line([x_nw x_sw], [y_nw y_sw], 'parent', ax, 'Color', ex_props.color, ...
        'LineWidth', ex_props.line_width, 'LineStyle', ex_props.line_style)
    line([x_ne x_se], [y_ne y_se], 'parent', ax, 'Color', ex_props.color, ...
        'LineWidth', ex_props.line_width, 'LineStyle', ex_props.line_style)
    line([x_nw x_ne], [y_nw y_ne], 'parent', ax, 'Color', ex_props.color, ...
        'LineWidth', ex_props.line_width, 'LineStyle', ex_props.line_style)
    line([x_sw x_se], [y_sw y_se], 'parent', ax, 'Color', ex_props.color, ...
        'LineWidth', ex_props.line_width, 'LineStyle', ex_props.line_style)
end

function add_point(ax, ex_props)
    x = ex_props.position(1);
    y = ex_props.position(2);

    hold(ax, 'on')
    scatter(x, y, ex_props.size, ex_props.color, ex_props.style, 'filled', 'parent', ax);
    hold(ax, 'off')
end

function add_text(ax, ex_props)
    text(ex_props.position(1), ex_props.position(2), ex_props.string, ...
        'parent', ax, 'Color', ex_props.color, ...
        'FontSize', ex_props.font_size, ...
        'FontAngle', ex_props.font_angle, ...
        'FontWeight', ex_props.font_weight, ...
        'HorizontalAlignment', ex_props.alignment_horz, ...
        'VerticalAlignment', ex_props.alignment_vert, ...
        'Interpreter', ex_props.interpreter );
end

function add_arrow(ax, ex_props)

    final_dir = ex_props.direction * ex_props.line_length;

    hold(ax, 'on')
    quiver(ex_props.position(1), ex_props.position(2), final_dir(1), final_dir(2), 0, ...
        'parent', ax, 'Color', ex_props.color, ...
        'LineWidth', ex_props.line_width, 'LineStyle', ex_props.line_style)
    hold(ax, 'off')
end

function add_segment(ax, ex_props)

    xs_l = ex_props.position([1 3]);
    ys_l = ex_props.position([2 4]);

    line(xs_l, ys_l, 'parent', ax, 'Color', ex_props.color, ...
        'LineWidth', ex_props.line_width, 'LineStyle', ex_props.line_style)
end

function add_inset(ax, ex_props, type, original_image)

    switch (lower(type))
        case 'zoom'
            cdata = original_image;

            method_type = 'nearest';
            oversampling = [1, 1];

            shifts = min(ex_props.region(3:4) ./ ex_props.position(3:4) - 0.5, 0);
            region_start = ex_props.region(1:2) + shifts;
            region_end = ex_props.region(1:2) + ex_props.region(3:4) - 1 - shifts;
        case 'image'
            cdata = im2double(permute(ex_props.image, [2 1 3]));
            region = [1, 1, size(cdata, 1), size(cdata, 2)];

            method_type = 'nearest';
            oversampling = ceil(region(3:4) ./ ex_props.position(3:4));

            shifts = min(region(3:4) ./ (ex_props.position(3:4) .* oversampling) - 0.5, 0);
            region_start = region(1:2) + shifts;
            region_end = region(1:2) + region(3:4) - 1 - shifts;
        otherwise
            error('gtFigureAddExtras:wrong_argument', ...
                'No known inset type: %s', type)
    end
    yy = linspace(region_start(1), region_end(1), ex_props.position(3) * oversampling(1));
    xx = linspace(region_start(2), region_end(2), ex_props.position(4) * oversampling(2));

    if (numel(size(cdata)) == 3)
        zz = [1 2 3];
        [xx, yy, zz] = ndgrid(xx, yy, zz);

        inset_data = interp3(cdata, xx, yy, zz, method_type, 0);
    else
        [xx, yy] = ndgrid(xx, yy);

        inset_data = interp2(cdata, xx, yy, method_type, 0);
    end
    inset_data = reshape(inset_data, oversampling(2), ex_props.position(4), oversampling(1), ex_props.position(3), []);
    inset_data = squeeze(sum(sum(inset_data, 1), 3)) / prod(oversampling);

    hold(ax, 'on')
    % watch out for the inversion of coordinates xx and yy here
    xx = ex_props.position(1):ex_props.position(1)+ex_props.position(3)-1;
    yy = ex_props.position(2):ex_props.position(2)+ex_props.position(4)-1;
    imagesc(ax, 'XData', xx, 'YData', yy, 'CData', inset_data)
    hold(ax, 'off')

    box_props = ex_props;
    box_props.position(1:2) = box_props.position(1:2) - 0.5;
    box_props.position(3:4) = box_props.position(3:4) + 1;
    add_box(ax, box_props);
    box_props.position = box_props.region;
    box_props.position(1:2) = box_props.position(1:2) - 0.5;
    box_props.position(3:4) = box_props.position(3:4) + 1;
    add_box(ax, box_props);
end

