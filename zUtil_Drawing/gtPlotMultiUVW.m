function gtPlotMultiUVW(grains, grainIDs, flagToPlot, fsimID, startF, varargin)
% GTPLOTMULTIUVW  Plots positions of spots forward simulated or measured,
%                 depending on the flagToPlot, for a set of grains
%
%     gtPlotMultiUVW(grains, grainIDs, flagToPlot, fsimID, startF, varargin)
%     ----------------------------------------------------------------------
%     It displays figures in a grid using gtSubFigure and links the axes
%     property 'View','XLim','YLim'
%
%     INPUT:
%       grains     = <cell>       grain info
%       grainIDs   = <double>     list of grains to consider P = length(grainIDs)
%       flagToPlot = <string>     It can be chosen between those below
%                                   'fsimA' (1),'fsimB' (1),'fsimAB' (2), ...
%                                   'allAB' (5), ...
%                                   'partA' (2),'partB' (2),'partAB' (4), ...
%                                   'expA' (1),'expB' (1),'expAB' (2), ...
%                                   'expU' (1),'fsimU' (1),'partU' (2), ...
%                                   'allU' (3), ...
%                                   'fsimAll' (3),'indexfsim' (1), ...
%                                   'fwd2' (1),'twin-parent' (2), ...
%                                   'indexfsim-fwd2' (2)
%                                 It is possible to pass the markers for each
%                                 plot in 'markers'. Remember the number of the
%                                 plots indicated in ()
%       fsimID     = <double>     ID for the forward simulation result to be used
%                                 (normally from 1 to 4)
%       startF     = <double>     Number of figures to skip in the grid (N,M)
%                                 N : number of rows max(3, P/4)
%                                 M : number of columns min(4, P)
%     OPTIONAL INPUT (parse by pairs):
%       varargin   =              variable list of options for plots using
%                                 gtPlotMultiScatter3DMatrix
%
%
%     Version 001 26-09-2013 by LNervo

if ~exist('fsimID','var') || isempty(fsimID)
    fsimID = 1;
end
if ~exist('startF','var') || isempty(startF)
    startF = 0;
end

if length(grainIDs) > 4
    nrows = max(4,ceil(length(grainIDs)/4));
    ncols = min(4,length(grainIDs));
else
    nrows = 2;
    ncols = max(length(grainIDs),2);
end


index = findValueIntoCell(varargin, {'xlim'});
if isempty(index)
    varargin(end+1:end+2) = {'xlim',[0 2048]};
end
index = findValueIntoCell(varargin, {'ylim'});
if isempty(index)
    varargin(end+1:end+2) = {'ylim',[0 2048]};
end
if isempty(index)
    varargin(end+1:end+2) = {'axes_labels',{'U','V','W'}};
end
index = findValueIntoCell(varargin, {'h_figure'});
if isempty(index)
     varargin(end+1:end+2) = {'h_figure',[]};
end
index = findValueIntoCell(varargin, {'h_axes'});
if isempty(index)
     varargin(end+1:end+2) = {'h_axes',[]};
end
index = findValueIntoCell(varargin, {'leg_pos'});
if isempty(index)
     varargin(end+1:end+2) = {'leg_pos',[0.8137 0.8110 0.1738 0.1384]};
end
index = findValueIntoCell(varargin, {'leg_textcolor'});
if isempty(index)
     varargin(end+1:end+2) = {'leg_textcolor',[1 1 1]};
end
index = findValueIntoCell(varargin, {'leg_color'});
if isempty(index)
     varargin(end+1:end+2) = {'leg_color',[0 0 0]};
end
index = findValueIntoCell(varargin, {'axes_title'});
if isempty(index)
    varargin(end+1:end+2) = {'axes_title',...
      sprintf('Dataset name "%s"     Phase number %02d     GrainID %d',...
               strrep(gtGetLastDirName(pwd),'_','\_'), ...
               grains{grainIDs(1)}.phaseid, ...
               grains{grainIDs(1)}.id)};
end


% loop over grains
for jj=1:length(grainIDs)
     
    index = findValueIntoCell(varargin, {'h_figure'});
    f(jj) = gtSubFigure(nrows,ncols,jj+startF);
    if isempty(varargin{index(2)+1}) 
        varargin{index(2)+1} = f(jj);
    elseif varargin{index(2)+1} ~= f(jj)
        close(f(jj))
        f(jj) = varargin{index(2)+1};
    end
    
    index = findValueIntoCell(varargin, {'h_axes'});
    if isempty(varargin{index(2)+1})
        varargin{index(2)+1} = get(f(jj),'CurrentAxes');
    elseif ~isempty( findobj(f(jj),'Tag','omega') )
        varargin{index(2)+1} = findobj(f(jj),'Tag','omega'); % only for gtShowFsim
    end

    set(f(jj),'Name',['grain # ' num2str(grainIDs(jj))])
    switch flagToPlot
      case 'fsimA'
        gtPlotMultiScatter3DMatrix({grains{grainIDs(jj)}.fsimA.uvw'}, ...
          'labels',{'fsimA'}, varargin{:})
        %gtPlotMultiScatter3D({grains}, 'ids', {grainIDs(jj)}, 'var', {'fsimA'}, 'var2', {'uvw'}, ...
        %  'labels',{'fsimA'}, varargin{:})
      case 'fsimB'
        gtPlotMultiScatter3DMatrix({grains{grainIDs(jj)}.fsimB.uvw'}, ...
          'labels',{'fsimB'}, varargin{:})
        %gtPlotMultiScatter3D({grains}, 'ids', {grainIDs(jj)}, 'var', {'fsimB'}, 'var2', {'uvw'}, ...
        %  'labels',{'fsimB'}, varargin{:})
      case 'fsimAB'
        gtPlotMultiScatter3DMatrix({grains{grainIDs(jj)}.fsimA.uvw',grains{grainIDs(jj)}.fsimB.uvw'}, ...
          'labels',{'fsimA','fsimB'}, varargin{:})
        %gtPlotMultiScatter3D({grains}, 'ids', {grainIDs(jj)}, 'var', {'fsimA','fsimB'}, 'var2', {'uvw'}, ...
        %  'labels',{'fsimA','fsimB'}, varargin{:})
      case 'allAB'
        fsimID = 1;
        gtPlotMultiScatter3DMatrix({grains{grainIDs(jj)}.fsimA.uvw', grains{grainIDs(jj)}.uvwA', ...
          grains{grainIDs(jj)}.fsimB.uvw', grains{grainIDs(jj)}.uvwB', ...
          grains{grainIDs(jj)}.fsim(fsimID).uvw'}, ...
          'labels',{'fsimA','expA','fsimB','expB','fsim1'}, varargin{:}) 
      case 'partA'
        gtPlotMultiScatter3DMatrix({grains{grainIDs(jj)}.fsimA.uvw', grains{grainIDs(jj)}.uvwA'},...
          'labels',{'fsimA','expA'}, varargin{:})
      case 'partB'
        gtPlotMultiScatter3DMatrix({grains{grainIDs(jj)}.fsimB.uvw', grains{grainIDs(jj)}.uvwB'},...
          'labels',{'fsimB','expB'}, varargin{:})
      case 'partAB'
        gtPlotMultiScatter3DMatrix({grains{grainIDs(jj)}.fsimA.uvw', grains{grainIDs(jj)}.uvwA', ...
          grains{grainIDs(jj)}.fsimB.uvw', grains{grainIDs(jj)}.uvwB'},...
          'labels',{'fsimA','expA','fsimB','expB'}, varargin{:}) 
      case 'expA'
        gtPlotMultiScatter3DMatrix({grains{grainIDs(jj)}.uvwA'},...
          'labels',{'expA'}, varargin{:})
      case 'expB'
        gtPlotMultiScatter3DMatrix({grains{grainIDs(jj)}.uvwB'},...
          'labels',{'expB'}, varargin{:})  
      case 'expAB'
        gtPlotMultiScatter3DMatrix({grains{grainIDs(jj)}.uvwA', grains{grainIDs(jj)}.uvwB'},...
          'labels',{'expA','expB'}, varargin{:})
      case 'expU'
        gtPlotMultiScatter3DMatrix({[grains{grainIDs(jj)}.fc; grains{grainIDs(jj)}.sc; grains{grainIDs(jj)}.image]'},...
          'labels',{'expU'}, varargin{:})
      case 'fsimU'
        gtPlotMultiScatter3DMatrix({grains{grainIDs(jj)}.fsimU.uvw'}, ...
          'labels',{'fsimU'}, varargin{:})
        %gtPlotMultiScatter3D({grains}, 'ids', {grainIDs(jj)}, 'var', {'fsimU'}, 'var2', {'uvw'}, ...
        %  'labels',{'fsimU'}, varargin{:})
      case 'partU'
        gtPlotMultiScatter3DMatrix({grains{grainIDs(jj)}.fsimU.uvw', [grains{grainIDs(jj)}.fc; grains{grainIDs(jj)}.sc; grains{grainIDs(jj)}.image]'}, ...
          'labels',{'fsimU','expU'}, varargin{:})
      case 'allU'
        if isempty(fsimID)
            fsimID = 2;
        end
        gtPlotMultiScatter3DMatrix({grains{grainIDs(jj)}.fsimU.uvw', [grains{grainIDs(jj)}.fc; grains{grainIDs(jj)}.sc; grains{grainIDs(jj)}.image]', ...
          grains{grainIDs(jj)}.fsim(fsimID).uvw'},...
          'labels',{'fsimU','expU',['fsim' num2str(fsimID)]}, varargin{:})
      case 'fsimAll'
        gtPlotMultiScatter3DMatrix({grains{grainIDs(jj)}.fsim(2).uvw', grains{grainIDs(jj)}.fsim(3).uvw', grains{grainIDs(jj)}.fsim(4).uvw'},...
          'labels',{'fsim2','fsim3','fsim4'}, varargin{:})
        %gtPlotMultiScatter3D({grains}, 'ids', {grainIDs(jj)}, 'var', {'fsim'}, 'var2', {'uvw'}, 'ind', {2,3,4}, ...
        %  'labels',{'fsim2','fsim3','fsim4'}, varargin{:})
      case 'indexfsim'
        gtPlotMultiScatter3DMatrix({grains{grainIDs(jj)}.fsim(fsimID).uvw'}, ...
          'labels',{['indexfsim' num2str(fsimID)]}, varargin{:})
        %gtPlotMultiScatter3D({grains}, 'ids', {grainIDs(jj)}, 'var', {'fsim'}, 'var2', {'uvw'}, 'ind', {fsimID}, ...
        %  'labels',{['indexfsim' num2str(fsimID)]}, varargin{:})
      case 'fwd2'
        gtPlotMultiScatter3DMatrix({grains{grainIDs(jj)}.fwd2.allblobs(1).detector.uvw}, ...
          'labels',{['fwd2 grain #' num2str(grainIDs(jj))]}, varargin{:})
        %gtPlotMultiScatter3D({grains}, 'ids', {grainIDs(jj)}, 'var', {'allblobs'}, 'var2', {'uvw'}, ...
        %  'labels',{['fwd2 grain #' num2str(grainIDs(jj))]}, varargin{:})
      case 'twin-parent'
        gtPlotMultiScatter3DMatrix({grains{grainIDs(jj)}.fwd2.allblobs(1).detector.uvw, grains{grainIDs(jj)}.twin_vars(fsimID).fwd2.allblobs(1).detector.uvw}, ...
          'labels',{['parent grain #' num2str(grainIDs(jj))],['twin variant ' num2str(fsimID)]}, varargin{:})
        set(f(jj),'Name',['grain #' num2str(grainIDs(jj)) ' twin variant ' num2str(fsimID)])
      case 'indexfsim-fwd2'
        gtPlotMultiScatter3DMatrix({grains{grainIDs(jj)}.fsim(fsimID).uvw_all', grains{grainIDs(jj)}.fwd2.allblobs(1).detector.uvw}, ...
          'labels',{['indexfsim grain #' num2str(grainIDs(jj))],['fwd2 grain #' num2str(grainIDs(jj))]}, varargin{:})
      otherwise
        disp('Wrong option for flagToPlot... Quitting')
        close(f)
        return
    end % flagToPlot
    
end % for loop jj

ha = get(f,'CurrentAxes');
if length(ha) > 1
    ha = [ha{:}];
    hl = linkprop(ha, {'View','XLim','YLim'});
    key = 'graphics_linkprop';
    % Store link object on first subplot axes
    setappdata(ha,key,hl);
end

end % end of function
  
