function [hf,ha] = gtPlotMultiHexagons(rvec1, rvec2, varargin)
%     [hf,ha] = gtPlotMultiHexagons(rvec1, rvec2, varargin)
%     -----------------------------------------------------
%     Makes a figure containing subplots of the unit cells of the selected grains
%     for two different datasets
%
%     INPUT:
%       rvec1     = <cell>     grain structure for dataset 1
%       rvec2     = <cell>     grain structure for dataset 2
%
%     OPTIONAL INPUT (varargin):
%       grainids   = <double>   IDs of grains of interest (Mx1);
%                               if Mx2 is first column first dataset, etc...
%       cmap1      = <double>   colour map for grains (rgb) dataset1 (N+1x3) {[1 0 0]}
%       cmap2      = <double>   colour map for grains (rgb) dataset2 (N+1x3) {[1 0 0]}
%       
%       rottens    = <double>   global rotation tensor to align dataset2 with dataset1
%                               {eye(3x3)}
%       tomo_setup = flag to match tomo reference system: x-->y y-->x z-->-z
%                    {false}
%       reference  = flag to plot the reference unit cell for each grain orientation
%                    {false}
%       section    = flag to draw a section on the yz-plane to follow
%                    gtShowSampleSurface {false}
%       overlap    = flag to overlap the first grainID in the list to all the
%                    other grains {true}
%       view       = azimuth and elevation for viewing in 3D {[0 180]} xz plane
%
%     OUTPUT:
%       hf         = figure handle
%       ha         = axes handle
%
%     Deals with coordinate systems.  R-vector orientation data in the instrument
%     system: x//beam, z upwards, y towards door.
%     In the reconstructed tomography/grain map data, z is downwards, y//x
%     instruments, x//y instrument.
%
%     Reference hexagon coordinates see gtHexagonalUnitCell
%
%
%     Version 003 16-01-2013 by LNervo

app.grainids   = [];
app.cmap1      = [];
app.cmap2      = [];
app.rottens    = eye(3);
app.tomo_setup = false;
app.reference  = false;
app.section    = false;
app.overlap    = false;
app.view       = [-90 0]; % xz plane

app = parse_pv_pairs(app,varargin);

if size(app.grainids,2) == 1 || isempty(rvec2)
    gtError('','Made for multiple datasets...quitting')
end

% keep info on grainID in the first column

if isempty(app.grainids)
    ids1 = 1:size(rvec1,1);
    ids2 = 1:size(rvec2,1);
elseif size(app.grainids,2) == 2
    ids1 = app.grainids(:,1);
    ids2 = app.grainids(:,2);
end
if isempty(app.cmap2)
    app.cmap2 = zeros(size(rvec2,1), 3);
    app.cmap2(:,2) = 1;
    app.showbar = false;
else
    app.showbar = false;
    if size(app.cmap2,1)==1
        app.cmap2 = repmat(app.cmap2,size(rvec1,1),1);
    end
    % remove bkg color
    if all(app.cmap2(1,:) == 0) && size(app.cmap2,1) == size(rvec2,1)+1
        app.cmap2(1,:) = [];
    end
end

if isempty(app.cmap1)
    app.cmap1 = zeros(size(rvec1,1), 3);
    app.cmap1(:,1) = 1;
    app.showbar = false;
else
    app.showbar = false;
    if size(app.cmap1,1)==1
        app.cmap1 = repmat(app.cmap1,size(rvec1,1),1);
    end
    % remove bkg color
    if all(app.cmap1(1,:) == 0) && size(app.cmap1,1) == size(rvec1,1)+1
        app.cmap1(1,:) = [];
    end
end

% total data

r_vectors = [rvec1(ids1,:);...
             rvec2(ids2,:)];
cmap = [app.cmap1(ids1,:);...
        app.cmap2(ids2,:)];
ids = [ids1;ids2];
datanum = ones(numel(ids1),1); 
datanum = [datanum; ones(numel(ids2),1)*2];


disp('Comparison between dataset 1 and dataset 2')
tot1 = numel(ids1);
tot2 = numel(ids2);
fprintf('%20s %20s %20s %30s','dataset','grainID','r_vector')
disp(' ')
arrayfun(@(num) fprintf('%17d %20d           %+6.3f %+6.3f %+6.3f\n',...
                        datanum(num), ids(num), r_vectors(num,:)), 1:numel(ids))

hf = figure();

% find grid size
if numel(ids1) > 5
    xdim = 5;
    ydim = ceil(numel(ids)/xdim);
else
    xdim = numel(ids1);
    ydim = 2;
end
set(hf, 'Position', [1 250*ydim 200*xdim 250*ydim])



% patch object hexagonal prism
data = gtHexagonalUnitCell('ratio',2,'draw',false,'centered',false);
corners3  = data.vertices(1:12,:);
rec_faces = data.faces_sides;
hex_faces = data.faces_end;

vertices = zeros(12,3,size(r_vectors,1));

% apply the R-vector
for ii=1:length(ids)

    grainID = ids(ii);
    datasetID = datanum(ii);
    % takes the R_vector
    R = r_vectors(ii,:);
    % crystal to sample rotation matrix == R = g^-1
    g = gtMathsRod2OriMat(R');
    corners3a = (g'*corners3')';

    % allow for the two coordinate systems.  Transform from instrument to
    % reconstructed tomo coordinates: x-->y y-->x z-->-z
    if (app.tomo_setup)
        corners3a=[corners3a(:,2) corners3a(:,1) -corners3a(:,3)]; 
    end
    % do the rotate to follow the section plane
    if datasetID == 2
        corners3b = (app.rottens*corners3a')';
    else
        corners3b = (eye(3,3)*corners3a')';
    end
    % store info on vertices
    vertices(:,:,ii) = corners3b;
    
    subplot(ydim, xdim, ii);
    
    % reference hex
    if (app.reference) && (~app.overlap)
        patch('Vertices', corners3, 'Faces', hex_faces, 'FaceColor', 'b', 'FaceAlpha', 0.4)
        patch('Vertices', corners3, 'Faces', rec_faces, 'FaceColor', 'b', 'FaceAlpha', 0.4)
    end
    % if overlap the first grain is used as reference for the others
    if (app.overlap) && ids(1) ~= ii
        overlapID = ids(1);
        disp('OverlapID : ')
        disp(overlapID)
        corners3a = (gtMathsRod2OriMat( r_vectors(1,:)' )'*corners3')';
        patch('Vertices', corners3a, 'Faces', hex_faces, 'FaceColor', cmap(1,:), 'FaceAlpha', 0.4)
        patch('Vertices', corners3a, 'Faces', rec_faces, 'FaceColor', cmap(1,:), 'FaceAlpha', 0.4)
    end
    % reference background like the section plane
    if (app.section)
        pos=min(mean(corners3b(hex_faces(1,:), 1)), mean(corners3b(hex_faces(2,:), 1)));
        section_plane_vertices=[pos 1 1; pos 1 -1; pos -1 1; pos -1 -1];
        section_plane_face=[1 2 4 3];
        patch('Vertices', section_plane_vertices, 'Faces', section_plane_face, 'FaceColor', 'k', 'FaceAlpha', 0.4)
    end
    % including the rotation for sectioning
    patch('Vertices', corners3b, 'Faces', hex_faces, 'FaceColor', cmap(ii,:), 'FaceAlpha', 0.6)
    patch('Vertices', corners3b, 'Faces', rec_faces, 'FaceColor', cmap(ii,:), 'FaceAlpha', 0.6)

    % a bit of labeling
    set(get(get(gcf, 'CurrentAxes'), 'Title'), 'String', sprintf('dataset %d grain %d', datasetID, grainID))
    set(get(get(gcf, 'CurrentAxes'), 'xLabel'),'String', 'x axis')
    set(get(get(gcf, 'CurrentAxes'), 'yLabel'),'String', 'y axis')
    set(get(get(gcf, 'CurrentAxes'), 'zLabel'),'String', 'z axis')
    % view
    set(get(gcf, 'CurrentAxes'), 'view', app.view)

    axis tight;
    axis equal;
    axis square;
    axis vis3d;

    % activate rotating option
    h = rotate3d(gcf);
    set(h,'Enable','on','RotateStyle','Box');
    axis equal;
    drawnow;
    
end % end for grains

out.vertices = vertices;

set(gcf, 'UserData', out);

hf = gcf;

if nargout == 2
    ha = gca;
end

end % end of function
