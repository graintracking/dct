function [newFig, newScat,newHist,bothBars,scatPoints] = makeCornerHist(x,y,xe,ye,xtitle,ytitle,nbins,xylimits,ratio,optHistc,addHist)
%     [newFig, newScat,newHist,bothBars,scatPoints] = makeCornerHist(x,y,xe,ye,xtitle,ytitle,nbins,xylimits,ratio,optHistc,addHist)
%     ---------------------------------------------------------------------------------------------------------------------
%
%     INPUT:
%       x             = <double>     vector of x values
%       y             = <double>     vector of y values
%       xe            = <double>     vector of x errors
%       ye            = <double>     vector of y errors
%
%     OPTIONAL INPUT:
%       nbins         = <double>     number of bins used for histogram {10}
%                                    can also use a vector (see help hist). all bins may not
%                                    show up on final plot, depending on how xylimits is set, but
%                                    all data is used
%       xylimits      = <double>     vector of two values, the min and max for the scatter plot,
%                                    (the scatter plot is a square plot, both axis have same limits,
%                                    leave blank, [], to include all data - default), will affect
%                                    the size of the histogram, as we attempt to line up the
%                                    diagonals, and we make the histogram width 2/3 the size of the
%                                    scatterplot.
%       ratio         = <double>     Ratio is optional to change the height of the hist plot
%                                    relative to its width - (ratio = width/height - ie. a ratio of
%                                    3 means that the height is one third of the width). default
%                                    ratio = 2, which usually makes a nice plot
%       optHistc      = <double>     set to 1 to use histc instead of hist so vector in nbins
%                                    will define edges, not center (see help histc)
%       addHist       = <double>     vector same length as x and y, zeros and ones, ones signify
%                                    which data points to include in a second histogram (on same
%                                    graph as first), if desired. (currently not implemented)
%
%     OUTPUT:
%       newScat       =              handle to scatter plot axes
%       newHist       =              handle to hist plot axes
%       bothBars      =              handles to bars in histogram
%       scatPoints(1) =              handle to main diagonal line
%       scatPoints(2) =              handle to symbols
%       scatPoints(3) =              handle to plot of the x error lines
%       scatPoints(4) =              handle to plot of the y error lines
%
%       to change an axis label:
%         set(get(newScat,'XLabel'),'String','2 choice')
%
%       to add a title: 
%         title(newScat,'Example Plot                         ')
%
%       to move it up higher:
%         title(newScat,{'Example Plot                                             ';''})
%
%       need the extra space, because matlab only centers titles, and our
%       histogram will cover up part of the title otherwise...
%       to see how to manipulate the plots, see help cornerHist
%
%       uses additional lab functions cornerHist and errorbar2
%
%       created by Maria Mckinley, May, 2004
%       major update Apr, 2008
%
%
%       Version 001 07-03-2013 by LNervo

if nargin < 11
    addHist = [];
end
if nargin < 10
    optHistc = [];    
end
if nargin < 9
    ratio = 0.6;
end
if nargin < 8
    xylimits = [];
end
if nargin < 7 || isempty(nbins)
    nbins = 10;
end
if isempty(ratio)
    ratio = 0.6;
end

scatFig = figure;
hold on
barcolor = [0.501960784313725 0.501960784313725 0.501960784313725];
[hsym,hxe,hye] = errorbar2(x,y,xe,ye,barcolor);
pause(0.1)
set(hsym,'Color', 'r', 'MarkerFaceColor','r')
if isempty(xylimits)
    xlims = get(gca,'Xlim');
    ylims = get(gca,'Ylim');
    newMin = min(xlims(1),ylims(1));
    newMax = max(xlims(2),ylims(2));
    xylimits = [newMin newMax];
end
pause(0.1)
set(gca,'Xlim',xylimits)
set(gca,'Ylim',xylimits)
axis square
set(gca,'TickDir','in','FontSize',12,'Box','on');
grid(gca,'on')
set(gca,'LineWidth',1)
hl = line(get(gca,'XLim'), get(gca,'XLim'), 'LineWidth',2,'Color',[0 0 1]);
xlabel(xtitle);
ylabel(ytitle);

histFig = figure;
d = x-y;
if optHistc == 1
    nn = histc(d,nbins);
%     if ~isempty(addHist)
%         N = find(addHist);
%         NN = histc(N,dd);
%     end
    histPlot = bar(nbins,nn,'histc');
else
    [nn dd] = hist(d,nbins);
%     if ~isempty(addHist)
%         N = find(addHist);
%         [NN dd] = hist(N,nbins);
%     end
    histPlot = bar(dd,nn,1);
end
set(gca,'TickDir','in','FontSize',12,'Box','on');
set(histPlot,'FaceColor',[0.7 0.7 0.7])
xlimit = max(round(max(abs(d))),25);
xlim([-xlimit xlimit]);
hold on
%shading flat
if ~isempty(addHist)
    histPlot2 = bar(dd,NN,0.9);
    set(histPlot2,'FaceColor',barcolor)
    set(histPlot2,'EdgeColor','none')
end
[~, newScat, newHist] = cornerHist(scatFig,histFig,ratio);
bothBars = get(newHist,'Children');
scatPoints = get(newScat, 'Children');
set(gcf,'Color',[1 1 1])
set(gcf, 'Position', [200 200 800 800], 'Units', 'points')

set(hsym,'DisplayName','Data')
set(hxe,'DisplayName','Error Bar')
%set(hxe,'DisplayName','X Error Bar')
%set(hye,'DisplayName','Y Error Bar')
set(hl,'DisplayName','Line y=x')

%leg=legend(newScat, [hxe,hye,hsym,hl]);
leg=legend(newScat, [hxe,hsym,hl]);
set(leg,'Position',[0.1208    0.8008    0.1747    0.0791])
set(leg,'Units','normalized')

newFig = gcf;

end % end function
