function varargout = gtFigureCreateVolumeSlices(volume, point, varargin)
    conf = struct( ...
        'cmap', jet, ...
        'filename', '', ...
        'clims', [], ...
        'view', [], ...
        'colorbar', false, ...
        'slices', {[]});
    conf = parse_pv_pairs(conf, varargin);

    volume = double(volume);
    [size_vol(1), size_vol(2), size_vol(3), size_vol(4)] = size(volume);

    f = figure('colormap', conf.cmap);
    ax = axes('parent', f);

    if (isempty(conf.slices))
        if (isempty(point))
            point = floor(size_vol / 2) + 1;
        end
        conf.slices{1} = point(1);
        conf.slices{2} = point(2);
        conf.slices{3} = point(3);
    else
        point = [conf.slices{1}(1), conf.slices{2}(1), conf.slices{3}(1)];
    end

    h = slice(ax, 1:size_vol(2), 1:size_vol(1), 1:size_vol(3), volume, conf.slices{2}, conf.slices{1}, conf.slices{3});
    set(h, 'EdgeColor','none');

    for ii_d = 1:3
        sl_list = conf.slices{ii_d};
        for ii_s = 1:numel(sl_list)
            switch (ii_d)
                case 1
                    slice_ver = [
                                  1, sl_list(ii_s),           1; ...
                                  1, sl_list(ii_s), size_vol(3); ...
                        size_vol(2), sl_list(ii_s),           1; ...
                        size_vol(2), sl_list(ii_s), size_vol(3); ...
                        ];
                case 2
                    slice_ver = [
                        sl_list(ii_s),           1,           1; ...
                        sl_list(ii_s),           1, size_vol(3); ...
                        sl_list(ii_s), size_vol(1),           1; ...
                        sl_list(ii_s), size_vol(1), size_vol(3); ...
                        ];
                case 3
                    slice_ver = [
                                  1,           1, sl_list(ii_s); ...
                                  1, size_vol(1), sl_list(ii_s); ...
                        size_vol(2),           1, sl_list(ii_s); ...
                        size_vol(2), size_vol(1), sl_list(ii_s); ...
                        ];
            end
            faces = [1 2 4 3];
            patch('parent', ax, 'Faces', faces, 'Vertices', slice_ver, 'FaceColor', 'none', 'EdgeColor', [0 0 0]);
        end
    end

    sl_list = conf.slices{3};
    for ii_s = 1:numel(sl_list)
        base_matrix = [ ...
                   0, point(1), sl_list(ii_s); ...
            point(2),        0, sl_list(ii_s); ...
            point(2), point(1),        0; ...
            ];
        inter_ver = [ ...
            base_matrix + eye(3); ...
            base_matrix + diag([size_vol(2), size_vol(1), size_vol(3)]); ...
            ];
        faces = [ ... % Intersections
            1 4; 2 5; 3 6; ...
            ];
        patch('parent', ax, 'Faces', faces, 'Vertices', inter_ver, 'FaceColor', 'none', 'EdgeColor', [0 0 0]);
    end

    show_bbox(ax, [1, 1, 1; 50, 50, 50])

    if (~isempty(conf.clims))
        set(ax, 'CLim', conf.clims)
    end

    set(f, 'Units', 'centimeters')
    set(ax, 'Units', 'centimeters')

    if (conf.colorbar)
        set(f, 'Position', [1 2 20 13.5])
        set(f, 'Paperposition', [1 2 20 13.5])

        set(ax, 'Position', [1.25 1 16 12])

        cb = colorbar('peer', ax, 'location', 'EastOutside');
        set(cb, 'Units', 'centimeters')
        set(cb, 'Position', [17.35 1 0.65 12])
    else
        set(f, 'Position', [1 2 16.75 13.25])
        set(f, 'Paperposition', [1 2 16.75 13.25])

        set(ax, 'Position', [2 1.1 14.5 12])
    end

    if (~isempty(conf.view))
        view(ax, conf.view(1), conf.view(2))
    end
    padding = round([size_vol(2), size_vol(1), size_vol(3)] * 5 / 100);
    padding = [1-padding, [size_vol(2), size_vol(1), size_vol(3)]+padding];
    padding = padding([1 4 2 5 3 6]);
    axis(ax, padding)
    xlabel('x')
    ylabel('y')
    zlabel('z')
    set(ax, 'FontSize', 18)

    if (~isempty(conf.filename))
        gtFigureSaveToFile(f, conf.filename);
    end

    if (nargout >= 1)
        varargout{1} = f;
        if (nargout >= 2)
            varargout{2} = ax;
        end
    end
end

function show_bbox(ax, sampled_pos)
    min_sampled_pos = min(sampled_pos, [], 1);
    max_sampled_pos = max(sampled_pos, [], 1);
    bbox_pos = [ ...
        min_sampled_pos(1), min_sampled_pos(2), min_sampled_pos(3); ...
        min_sampled_pos(1), min_sampled_pos(2), max_sampled_pos(3); ...
        min_sampled_pos(1), max_sampled_pos(2), min_sampled_pos(3); ...
        min_sampled_pos(1), max_sampled_pos(2), max_sampled_pos(3); ...

        max_sampled_pos(1), min_sampled_pos(2), min_sampled_pos(3); ...
        max_sampled_pos(1), min_sampled_pos(2), max_sampled_pos(3); ...
        max_sampled_pos(1), max_sampled_pos(2), min_sampled_pos(3); ...
        max_sampled_pos(1), max_sampled_pos(2), max_sampled_pos(3); ...
        ];
    faces = [ ...
        1 5; 2 6; 3 7; 4 8; ...
        1 3; 2 4; 5 7; 6 8; ...
        1 2; 3 4; 5 6; 7 8; ...
        ];

    patch('parent', ax, 'Faces', faces, 'Vertices', bbox_pos, 'FaceColor', 'w', 'LineStyle', ':');
end