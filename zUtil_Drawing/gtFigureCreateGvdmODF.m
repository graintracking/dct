function [fs, axs, p, odf, coords] = gtFigureCreateGvdmODF(dmvol_or_gvdm, intvol, mask, varargin)

    conf = struct( ...
        'parameters', [], ...
        'ospace_res_deg', [], ...
        'percent', 0.0025, ...
        'reference_R_vector', [], ...
        'plane_projections', 'none', ...
        'show_bbox', 'partial' );
    conf = parse_pv_pairs(conf, varargin);

    if (numel(size(dmvol_or_gvdm)) == 4)
        gv_dm = gtDefDmvol2Gvdm(dmvol_or_gvdm);
    elseif (numel(size(dmvol_or_gvdm)) == 4)
        gv_dm = dmvol_or_gvdm;
    else
        error('gtFigureCreateGvdmODF:wrong_argument', ...
            'Unexpected DMVOL or GVDM size:%s', sprintf(' %d', size(dmvol_or_gvdm)))
    end

    gv_mask = logical(mask(:));
    gv_int = intvol(:);

    if (isempty(conf.ospace_res_deg))
        if (isempty(conf.parameters))
            conf.parameters = gtLoadParameters();
        end
        conf.ospace_res_deg = gtAcqGetOmegaStep(conf.parameters);
    end
    ospace_res_rod = tand(conf.ospace_res_deg / 2);

    grid_points = [ ...
        min(gv_dm, [], 2)', ...
        max(gv_dm, [], 2)', ...
        ospace_res_rod([1 1 1])];

    grid_points(1:3) = grid_points(1:3) - mod(grid_points(1:3), ospace_res_rod);
    grid_points(4:6) = grid_points(4:6) + mod(ospace_res_rod - mod(grid_points(4:6), ospace_res_rod), ospace_res_rod);
    fprintf('Bouding box (deg): [%g %g %g] -> [%g %g %g], ospace-resolution (deg): %g\n', 2 * atand(grid_points(1:6)), conf.ospace_res_deg)

    odf = gtGetODFFromGvdm(gv_dm(:, gv_mask), grid_points, gv_int(gv_mask));

    x_pos = linspace(grid_points(1), grid_points(4), size(odf, 1));
    y_pos = linspace(grid_points(2), grid_points(5), size(odf, 2));
    z_pos = linspace(grid_points(3), grid_points(6), size(odf, 3));
    if (~isempty(conf.reference_R_vector))
        x_pos = x_pos - conf.reference_R_vector(1);
        y_pos = y_pos - conf.reference_R_vector(2);
        z_pos = z_pos - conf.reference_R_vector(3);
    end
    x_pos = 2 * atand(x_pos);
    y_pos = 2 * atand(y_pos);
    z_pos = 2 * atand(z_pos);
    coords = {x_pos, y_pos, z_pos};

    fs{1} = gtFigureOpen('name', 'ODF');
    ax = axes('parent', fs{1}, 'Units', 'centimeters', 'FontSize', 14);
    thr_value = (max(odf(:)) - min(odf(:))) * conf.percent + min(odf(:));
    p = patch( ...
        isosurface(x_pos, y_pos, z_pos, permute(odf, [2 1 3]), thr_value), ...
        'FaceVertexAlphaData', 0.5, 'FaceAlpha', 'flat', 'parent', ax);
    set(p, 'FaceColor', 'cyan', 'EdgeColor', 'none')
    camlight('right')
    camlight('left')
    lighting('gouraud')
    grid(ax, 'on')
    view(ax, 3);

    % Change it if you like!
    set(ax, 'position', [2, 1, 12.5, 10])

    xlabel(ax, 'X (degrees)');
    ylabel(ax, 'Y (degrees)');
    zlabel(ax, 'Z (degrees)');

    bbox_deg = [x_pos(1), y_pos(1), z_pos(1), x_pos(end), y_pos(end), z_pos(end)];
    xlim(ax, bbox_deg([1, 4]) * 1.05)
    ylim(ax, bbox_deg([2, 5]) * 1.05)
    zlim(ax, bbox_deg([3, 6]) * 1.05)

    if (ismember(conf.show_bbox, {'full', 'partial'}))
        hold(ax, 'on')
        show_bbox(conf.show_bbox, ax, bbox_deg)
        hold(ax, 'off')
    end
    axs{1} = ax;

    drawnow();

    switch (lower(conf.plane_projections))
        case {'none', 'integrated'}
        case 'combined'
            offset_hv = [1.75, 1.25];
            img_size_hv = [10.25, 10.25];
            spacing = 0.5;

            f_size = [(offset_hv(1) + img_size_hv(1)) * 3 + spacing * 2 + 2, ...
                offset_hv(2) + img_size_hv(2) + 0.1];
            fs{2} = gtFigureOpen('Name', 'ODF projections', 'size', f_size);

            axs(2:4) = {[]};
            for ii = 1:3
                axs{1+ii} = subplot(1, 3, ii, 'parent', fs{2}, 'Units', 'centimeters');

                ax_pos = [...
                    offset_hv(1) + (offset_hv(1) + spacing + img_size_hv(1)) * (ii - 1), ...
                    offset_hv(2), img_size_hv];
                set(axs{1+ii}, 'Position', ax_pos)
            end

            pass_fs = {[], [], []};
        case 'separate'
            fs(2:4) = {[]};
            axs(2:4) = {[]};
            for ii = 1:3
                fs{1+ii} = gtFigureOpen();
                axs{1+ii} = axes('parent', fs{1+ii}, 'Units', 'centimeters');
            end

            pass_fs = fs(2:4);
    end

    if (~strcmpi(conf.plane_projections, 'none'))
        proj_xy = sum(odf, 3);
        proj_xz = squeeze(sum(odf, 2));
        proj_yz = squeeze(sum(odf, 1));

        clim = [0, max([max(proj_xy(:)), max(proj_xz(:)), max(proj_yz(:))])];

        if (strcmpi(conf.plane_projections, 'integrated'))
            hold(ax, 'on')

            [xx, yy, zz] = ndgrid(x_pos, y_pos, z_pos(1));
            surface(xx, yy, zz, proj_xy, ...
                'parent', ax, ...
                'EdgeColor', 'none', ...
                'FaceLighting', 'none');

            [xx, yy, zz] = ndgrid(x_pos, y_pos(end), z_pos);
            surface(squeeze(xx), squeeze(yy), squeeze(zz), proj_xz, ...
                'parent', ax, ...
                'EdgeColor', 'none', ...
                'FaceLighting', 'none');

            [xx, yy, zz] = ndgrid(x_pos(end), y_pos, z_pos);
            surface(squeeze(xx), squeeze(yy), squeeze(zz), proj_yz, ...
                'parent', ax, ...
                'EdgeColor', 'none', ...
                'FaceLighting', 'none');

            hold(ax, 'off')
        else
            gtFigureCreateFromPicture(proj_xy, [], ...
                'figure', pass_fs{1}, 'axes', axs{2}, ...
                'cmap', parula, 'clim', clim, 'coordinates', {x_pos, y_pos});
            xlabel(axs{2}, 'X (degrees)');
            ylabel(axs{2}, 'Y (degrees)');

            gtFigureCreateFromPicture(proj_xz, [], ...
                'figure', pass_fs{2}, 'axes', axs{3}, ...
                'cmap', parula, 'clim', clim, 'coordinates', {x_pos, z_pos});
            xlabel(axs{3}, 'X (degrees)');
            ylabel(axs{3}, 'Z (degrees)');

            gtFigureCreateFromPicture(proj_yz, [], ...
                'figure', pass_fs{3}, 'axes', axs{4}, ...
                'cmap', parula, 'clim', clim, 'coordinates', {y_pos, z_pos});
            xlabel(axs{4}, 'Y (degrees)');
            ylabel(axs{4}, 'Z (degrees)');

            if (strcmpi(conf.plane_projections, 'combined'))
                cb = colorbar('peer', axs{4}, 'location', 'EastOutside');
                set(cb, 'Units', 'centimeters')
                cb_position = [...
                    ax_pos(1) + img_size_hv(1) + 0.35, offset_hv(2), ...
                    0.65, img_size_hv(2)];
                set(cb, 'Position', cb_position)
                set(cb, 'FontSize', 16)
            end
        end
    end
end

function show_bbox(type, ax, bbox)
    min_sampled_R_vecs = bbox(1:3);
    max_sampled_R_vecs = bbox(4:6);
    if (strcmpi(type, 'full'))
        bbox_R_vecs = [ ...
            min_sampled_R_vecs(1), min_sampled_R_vecs(2), min_sampled_R_vecs(3); ...
            min_sampled_R_vecs(1), min_sampled_R_vecs(2), max_sampled_R_vecs(3); ...
            min_sampled_R_vecs(1), max_sampled_R_vecs(2), min_sampled_R_vecs(3); ...
            min_sampled_R_vecs(1), max_sampled_R_vecs(2), max_sampled_R_vecs(3); ...

            max_sampled_R_vecs(1), min_sampled_R_vecs(2), min_sampled_R_vecs(3); ...
            max_sampled_R_vecs(1), min_sampled_R_vecs(2), max_sampled_R_vecs(3); ...
            max_sampled_R_vecs(1), max_sampled_R_vecs(2), min_sampled_R_vecs(3); ...
            max_sampled_R_vecs(1), max_sampled_R_vecs(2), max_sampled_R_vecs(3); ...
            ];
        faces = [ ...
            1 5; 2 6; 3 7; 4 8; ...
            1 3; 2 4; 5 7; 6 8; ...
            1 2; 3 4; 5 6; 7 8; ...
            ];

        line_style = '-';
    else
        bbox_R_vecs = [ ...
            min_sampled_R_vecs(1), min_sampled_R_vecs(2), min_sampled_R_vecs(3); ... (lx, ly, lz) - yes
            min_sampled_R_vecs(1), min_sampled_R_vecs(2), max_sampled_R_vecs(3); ... (lx, ly, hz) - no
            min_sampled_R_vecs(1), max_sampled_R_vecs(2), min_sampled_R_vecs(3); ... (lx, hy, lz) - yes
            min_sampled_R_vecs(1), max_sampled_R_vecs(2), max_sampled_R_vecs(3); ... (lx, hy, hz) - yes

            max_sampled_R_vecs(1), min_sampled_R_vecs(2), min_sampled_R_vecs(3); ...
            max_sampled_R_vecs(1), min_sampled_R_vecs(2), max_sampled_R_vecs(3); ...
            max_sampled_R_vecs(1), max_sampled_R_vecs(2), min_sampled_R_vecs(3); ...
            max_sampled_R_vecs(1), max_sampled_R_vecs(2), max_sampled_R_vecs(3); ... (hx, hy, hz) - yes
            ];
        faces = [ ...
            1 5; 3 7; 4 8; ...
            1 3; 5 7; 6 8; ...
            3 4; 5 6; 7 8; ...
            ];

        line_style = '-';
    end

    patch('parent', ax, 'Faces', faces, 'Vertices', bbox_R_vecs, 'FaceColor', 'w', 'LineStyle', line_style);
end



