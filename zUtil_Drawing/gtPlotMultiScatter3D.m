function [h_obj, data_obj] = gtPlotMultiScatter3D(grains, varargin)
% GTPLOTMULTISCATTER3D  Plots variables from grains in the same plot
%     [h_obj, data_obj] = gtPlotMultiScatter3D(grains, varargin)
%     ----------------------------------------------------------
%     INPUT:
%       grains    = <cell array>     structure(s) with grains information <cell> (1xP)
%
%     OPTIONAL INPUT (parse by pairs as varargin):
%       var       = <cell array>     variables (first level) to plot <string> {'center'}
%       var2      = <cell array>     variables (second level) to plot <string> {[]}
%       ind       = <cell array>     indexes for variable (first level) to plot <double> {[]}
%       ind2      = <cell array>     indexes for variable (second level) to plot <double> {[]}
%       ids       = <cell array>     indexes to consider for each dataset <double> {all}
%       markers   = <cell array>     marker for each dataset <string> (1xP) {'xr','.b','og','xk','sm','db'}
%       cmap      = <cell array>     cmap for each point for each dataset <double> (Nx3) {[]}
%       labels    = <cell array>     entry in the legend for each dataset <string> (1xP) {[]}
%
%       showLegend      = <logical>  display or not {true} | false
%       legendpos       = <string>   legend position {[0.8 0.35 0.2 0.35]}
%       legendcolor     = <double>   legend bkg RGB color {[1 1 1]}
%       legendtextcolor = <double>   legend text RGB color {[0 0 0]}
%
%       view        = <double>       axes view property {[0 90]}
%       xlim        = <double>       x axis limits {[]}
%       ylim        = <double>       y axis limits {[]}
%       zlim        = <double>       z axis limits {[]}
%       axes_labels = <cell array>   labels for axes X Y and Z respectively <string> {'X','Y','Z'}
%
%       h_figure = <handle>          figure handle {[]}
%       h_axes   = <handle>          axes handle {[]}
%       h_points = <handle>          scatter points handle {[]}
%       h_legend = <handle>          legend handle {[]}
%
%     OUTPUT:
%       h_obj    = <handle array>    objects handles (1xP)
%       data_obj = <cell array>      data plotted in the figure <double> (Mx3)
%
%
%     Version 003 02-07-2013 by LNervo


% cell arrays
app.var       = arrayfun(@(num) {'center'}, 1:numel(grains), 'UniformOutput', false);
app.var2      = arrayfun(@(num) [], 1:numel(grains), 'UniformOutput', false);
app.ind       = arrayfun(@(num) [], 1:numel(grains), 'UniformOutput', false);
app.ind2      = arrayfun(@(num) [], 1:numel(grains), 'UniformOutput', false);
app.ids       = arrayfun(@(num) 1:length(grains{num}), 1:numel(grains), 'UniformOutput', false);
app.markers   = {'xr','.b','og','xk','sm','db'};
app.cmap      = arrayfun(@(num) [], 1:numel(grains), 'UniformOutput', false);
app.labels    = arrayfun(@(num) [], 1:numel(grains), 'UniformOutput', false);
app.sizes     = arrayfun(@(num) [], 1:numel(grains), 'UniformOutput', false);

% legend
app.showLegend      = true;
app.legendpos       = [0.8 0.35 0.2 0.35];
app.legendcolor     = [1 1 1];
app.legendtextcolor = [0 0 0];
% axes
app.view        = [0 90];
app.xlim        = [];
app.ylim        = [];
app.zlim        = [];
app.axes_labels = {'X','Y','Z'};
% handles
app.h_figure = [];
app.h_axes   = [];
app.h_points = [];
app.h_legend = [];

app = parse_pv_pairs(app, varargin);
% sets variables
app = sfSetVars(grains, app);

% create the figure
if isempty(app.h_figure)
    f1 = figure('Visible', 'off');
else
    f1 = app.h_figure;
    set(f1, 'Visible', 'off')
end

figure(f1)
if ~isempty(app.h_axes)
    set(f1, 'CurrentAxes', app.h_axes)
else
    app.h_axes = get(f1,'CurrentAxes');
end
hold(gca, 'on');

h_p  = [];

for ii=1:numel(app.data)

    % get values
    data = app.data{ii};
    if length(unique(cellfun(@(num) size(num,2), data))) ~= 1
        gtError('gtPlotMultiScatter3D:wrong_input',...
                ['Variable ''' app.var{ii} ''' saved into grains has not the same size for all the grains... Quitting'])
    end
    ncols = size(data{1},2);
    % get ids
    tmp_var = reshape([data{:}],[],ncols);
    if size(tmp_var,1) == 3 && ~isvector(tmp_var) && size(tmp_var,2) ~= 3
        tmp_var = tmp_var';
    end
    tmp_var = tmp_var(app.ids{ii},:);
    if ~isempty(app.sizes{ii})
        app.sizes{ii} = app.sizes{ii}(app.ids{ii},:);
    end
    if ~isempty(app.cmap{ii})
        cmap = app.cmap{ii};
        if all(cmap(1,:)==[0 0 0])
            cmap = cmap(2:end,:);
        end
        cmap = cmap(app.ids{ii},:);
        app.cmap{ii} = cmap;
        h_p(ii) = scatter3(tmp_var(:,1), tmp_var(:,2), tmp_var(:,3), app.markers{ii}, 'CData', cmap);
    else
        h_p(ii) = scatter3(tmp_var(:,1), tmp_var(:,2), tmp_var(:,3), app.markers{ii});
    end

    app.data{ii} = tmp_var;

end % end for ii

app.h_points = h_p;

% legend
h_leg = findobj(app.h_figure, 'Tag', 'legend');
if ~isempty(h_leg)
    leg_data            = get(h_leg, 'UserData');
    app.labels = [leg_data.lstrings; app.labels'];
    app.legendpos       = get(h_leg, 'Position');
    app.legendcolor     = get(h_leg, 'Color');
    app.legendtextcolor = get(h_leg, 'TextColor');
    delete(h_leg);
end

if app.showLegend
    leg = legend('Parent',f1,'String',app.labels,'Location',app.legendpos,'Color',app.legendcolor,'TextColor',app.legendtextcolor);
    app.h_legend = leg;
end


% axes options
axis equal

xlabel(app.axes_labels{1});
ylabel(app.axes_labels{2});
zlabel(app.axes_labels{3});

if ~isempty(app.xlim)
    xlim(gca, app.xlim)
end
if ~isempty(app.ylim)
    ylim(gca, app.ylim)
end
if ~isempty(app.zlim)
    zlim(gca, app.zlim)
end

box(gca, 'on');
grid(gca, 'on');

app.h_figure = f1;
app.h_axes = gca;

% 3D rotation ON
h=rotate3d(app.h_figure);
set(h,'Enable','on','RotateStyle','Box');
view(app.view);


% save application data
set(gcf,'Visible','on')
set(gcf,'UserData',app);
% show options
print_structure(app,'options',false,true)

if nargout > 0
    h_obj = app.h_points;
    if nargout > 1
        data_obj = app.data;
    end
end

end % end of function

%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% sub-functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function app = sfSetVars(grains, app)
% app = sfSetVars(grains, app)

app.data = [];
if numel(app.ind) > numel(grains)
    grains = arrayfun(@(num) grains{end}, 1:numel(app.ind), 'UniformOutput', false);
elseif numel(app.var) > numel(grains)
    grains = arrayfun(@(num) grains{end}, 1:numel(app.var), 'UniformOutput', false);
end
app.labels2 = arrayfun(@(num) [' ' num2str(num)], 1:numel(grains), 'UniformOutput', false);
if numel(app.markers) > numel(grains)
    app.markers = app.markers(1:numel(grains));
end

for ii=1:numel(grains)
 
    % save variable name
    if isempty(app.labels{ii})
        app.labels{ii} = app.var{ii};
    end
    if ii>1 && numel(app.var)<ii
        app.var{ii} = app.var{ii-1};
    end
    if ii>1 && numel(app.ids)<ii
        app.ids{ii} = app.ids{ii-1};
    end
    if ii>1 && numel(app.var2)<ii
        app.var2{ii} = app.var2{ii-1};
    end
    if ii>1 && numel(app.ind)<ii
        app.ind{ii} = app.ind{ii-1};
    end
    if ii>1 && numel(app.ind2)<ii
        app.ind2{ii} = app.ind2{ii-1};
    end
    if ii>1 && numel(app.cmap)<ii
        app.cmap{ii} = app.cmap{ii-1};
    end
    if ii>1 && numel(app.markers)<ii
        app.markers{ii} = app.markers{ii-1};
    end
    if ii>1 && numel(app.labels)<ii
        app.labels{ii} = app.labels{ii-1};
    end
    if ii>1 && numel(app.sizes)<ii
        app.sizes{ii} = app.sizes{ii-1};
    end
    % get possible second level variable
    if ~isempty(strfind(app.var{ii}, '.'))
        [app.var{ii}, app.var2{ii}] = strtok(app.var{ii}, '.');
        app.var2{ii} = strrep(app.var2{ii}, '.', '');
    end
    if ii > 1 && strcmpi(app.labels{ii-1}, app.labels{ii}) 
        app.labels{ii-1} = strcat(app.labels{ii-1}, app.labels2{ii-1});
        app.labels{ii} = strcat(app.labels{ii}, app.labels2{ii});
    end
    
    app.data{ii} = gtIndexAllGrainValues(grains{ii}, app.var{ii}, app.var2{ii}, app.ind{ii}, app.ind2{ii}, false);
end

print_structure(app, 'app', false, true)

end % end function sfSetVars
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
