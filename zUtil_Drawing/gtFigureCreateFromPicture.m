function [f, ax, img] = gtFigureCreateFromPicture(img, mask, varargin)

    conf = struct( ...
        'filename', '', ...
        'figure_name', '', ...
        'lims_u', [], ...
        'lims_v', [], ...
        'flip_ud', false, ...
        'flip_lr', false, ...
        'transpose', true, ...
        'label', [], ...
        'clims', [], ...
        'cmap', jet, ...
        'pixel_to_cm', 0.05, ...
        'borders', true, ...
        'colorbar', true, ...
        'colorbar_font_size', 22, ...
        'axes_ticks', true, ...
        'pixel_size', [], ...
        'extras', [], ...
        'figure', [], ...
        'axes', [], ...
        'coordinates', [] );
    [conf, rej_conf] = parse_pv_pairs(conf, varargin);
    if (~isempty(rej_conf))
        disp('Unknown parameters:')
        disp(rej_conf)
    end

    use_mask = exist('mask', 'var') && ~isempty(mask);
    if (~use_mask)
        mask = true(size(img, 1), size(img, 2));
    end

    if (~isempty(conf.lims_u))
        mask([1:conf.lims_u(1)-1, conf.lims_u(2)+1:end], :) = 0;
    end
    if (~isempty(conf.lims_v))
        mask(:, [1:conf.lims_v(1)-1, conf.lims_v(2)+1:end]) = 0;
    end

    % cuting the images
    horz_profile = sum(mask, 1);
    vert_profile = sum(mask, 2);
    horz_lims = [find(horz_profile, 1, 'first'), find(horz_profile, 1, 'last')];
    vert_lims = [find(vert_profile, 1, 'first'), find(vert_profile, 1, 'last')];

    img = img(vert_lims(1):vert_lims(2), horz_lims(1):horz_lims(2), :);
    mask = mask(vert_lims(1):vert_lims(2), horz_lims(1):horz_lims(2));

    % Note the inversion between lr/up because of matlab's transpose problem
    if (conf.flip_ud)
        img = fliplr(img);
        mask = fliplr(mask);
    end
    if (conf.flip_lr)
        img = flipud(img);
        mask = flipud(mask);
    end
    if (conf.transpose)
        img = permute(img, [2 1 3]);
        mask = permute(mask, [2 1 3]);
    end

    if (isempty(conf.clims))
        if (size(img, 3) == 3)
            clims_img = [0 1];
        else
            clims_img = [min(img(:)), max(img(:))];
        end
    else
        clims_img = [conf.clims(1), conf.clims(2)];
%         clims_img = [conf.clims(1), min(conf.clims(2), max(img(:)))];
    end

    img_size = [size(img, 2), size(img, 1)];

    if (isempty(conf.axes))
        if (isempty(conf.figure))
            f = gtFigureOpen('name', conf.figure_name);
        else
            f = conf.figure;
        end

        ax = axes('parent', f, 'Units', 'centimeters', 'FontSize', 14);
    else
        ax = conf.axes;
    end

    if (use_mask)
        h = imagesc(img, 'parent', ax, ...
            'AlphaDataMapping', 'scaled', 'AlphaData', mask', ...
            clims_img );
    else
        h = imagesc(img, 'parent', ax, ...
            clims_img );
    end
    colormap(ax, conf.cmap)

    if (~isempty(conf.coordinates))
        set(h, 'xdata', conf.coordinates{1})
        set(h, 'ydata', conf.coordinates{2})
        set(ax, 'xlim', [min(conf.coordinates{1}), max(conf.coordinates{1})])
        set(ax, 'ylim', [min(conf.coordinates{2}), max(conf.coordinates{2})])
    end

    if (~conf.borders || ~conf.axes_ticks)
        set(ax, 'xtick', [])
        set(ax, 'xticklabel', [])
        set(ax, 'ytick', [])
        set(ax, 'yticklabel', [])
    end

    if (~isempty(conf.extras))
        if (isempty(conf.pixel_size) && gtFigureExtrasRequireParameters(conf.extras))
            p = gtLoadParameters();
            conf.pixel_size = p.recgeo.voxsize * 1e3;
        end
        im_props = struct( ...
            'pixel_size', conf.pixel_size, ...
            'size', img_size );
        gtFigureAddExtras(ax, im_props, conf.extras);
    end

    have_control_on_figure = isempty(conf.axes) || ~isempty(conf.figure);

    if (have_control_on_figure)
        gtFigureSetSize(f, ax, conf, img_size);
    end

    drawnow();

    if (have_control_on_figure && ~isempty(conf.filename))
        gtFigureSaveToFile(f, conf.filename);
    end
end
