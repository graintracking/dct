function varargout = gtFigureSetSize(f, ax, conf, img_size)
    if (exist('img_size', 'var') && ~isempty(img_size))
        img_size = img_size .* conf.pixel_to_cm;
    else
        img_size = conf.figure_size;
    end

    set(f, 'Units', 'centimeters')
    if (~conf.borders)
        if (~conf.colorbar)
            figure_position = [1, 1, img_size];
        else
            figure_position = [1, 1, img_size] + [0, 0, 3, 0.65];
        end
        set(f, 'Position', figure_position)
        set(f, 'Paperposition', figure_position)
        set(ax, 'Units', 'centimeters')

        if (conf.colorbar)
            lower_offset = 0.4;

            cb = colorbar('peer', ax, 'location', 'EastOutside');
            set(cb, 'Units', 'centimeters')
            position = [img_size(1)+0.35, lower_offset, 0.65, img_size(2)];
            set(cb, 'Position', position)
            set(cb, 'FontSize', conf.colorbar_font_size)
            set(ax, 'Position', [0, lower_offset, img_size])
        else
            set(ax, 'Position', [0, 0, img_size])
        end
    else
        if (~isempty(conf.label))
            if (conf.axes_ticks)
                size_padding = 2;
                lower_offsets_hv = [1, 1.75];
            else
                size_padding = 1.5;
                lower_offsets_hv = [0.5, 1.25];
            end
        else
            if (conf.axes_ticks)
                size_padding = 1.5;
                lower_offsets_hv = [1.5, 1];
            else
                size_padding = 1;
                lower_offsets_hv = [0.5, 0.5];
            end
        end

        set(ax, 'Units', 'centimeters')
        if (~conf.colorbar)
            figure_position = [1, 1, img_size] + [0, 0, 2, size_padding];
            set(f, 'Position', figure_position)
            set(f, 'Paperposition', figure_position)
        else
            figure_position = [1, 1, img_size] + [0, 0, 3.5, size_padding+0.5];
            set(f, 'Position', figure_position)
            set(f, 'Paperposition', figure_position)

            cb = colorbar('peer', ax, 'location', 'EastOutside');
            set(cb, 'Units', 'centimeters')
            position = [img_size(1)+lower_offsets_hv(1)+0.35, lower_offsets_hv(2), 0.65, img_size(2)];
            set(cb, 'Position', position)
            set(cb, 'FontSize', conf.colorbar_font_size)
        end
        axes_position = [lower_offsets_hv, img_size];
        set(ax, 'Position', axes_position)

        if (~isempty(conf.label))
            l = uicontrol('Style', 'Text', 'parent', f, ...
                'String', conf.label, 'FontWeight', 'bold');
            set(l, 'Units', 'centimeters')
            position = [lower_offsets_hv(1), 0.25, img_size(1), 0.75];
            set(l, 'Position', position)
        end
    end
    box(ax, 'Off')
    grid(ax, 'Off')
    fprintf('Figure size: [%g, %g] cm\n', figure_position(3:4))
    drawnow();

    if (nargout > 0 && conf.colorbar)
        varargout{1} = cb;
    end
end