function h = gtDrawAxis3d(varargin)
% GTDRAWAXIS3D  Draws a coordinate system and an origin
%
%     gtDrawAxis3d(varargin)
%     ----------------------
%     Adds 3 cylinders to the current axis, corresponding to the directions
%     of the 3 basis vectors Ox, Oy and Oz.
%     Ox vector is red, Oy vector is green, and Oz vector is blue.
%     All can be modified by using varargin.
%     Uses the current axes
%
%     OPTIONAL INPUT (varargin):
%       'orig'    = <double>   Axis origin {[0 0 0]}
%       'axispar' = <double>   Length of the three axes and angles between
%                                 them {[1 1 1 90 90 90]}
%       'radius'  = <double>   Radius for cylinders
%       'colors'  = <double>   Colors for the three axes as RGB colors
%                              {[1 0 0;0 1 0;0 0 1]}
%
%     Needs matGeom toolbox
%
%     Version 001 by LNervo


% geometrical data
par.orig = [0 0 0];
par.axispar = [1 1 1 90 90 90];
par.radius = 1/10;
par.colors = [1 0 0;0 1 0;0 0 1];
par = parse_pv_pairs(par,varargin);


a = par.axispar(1)
b = par.axispar(2)
c = par.axispar(3)

% compute coordinates for axes
% [1 0 0] default X axis
v = 0:90:360;
x0 = a*cosd(v)
y0 = a*sind(v)


% draw 3 cylinders and a ball
hold(gca,'on');

h.patch(1) = drawCylinder([par.orig par.orig + par.axispar(1) r], 16, 'facecolor', axispar, 'edgecolor', 'none');
h.patch(2) = drawCylinder([par.orig par.orig + par.axispar(2) r], 16, 'facecolor', axispar, 'edgecolor', 'none');
h.patch(3) = drawCylinder([par.orig par.orig + par.axispar(2) r], 16, 'facecolor', axispar, 'edgecolor', 'none');
h.patch(4) = drawSphere([par.orig 2*par.radius], 'faceColor', [0 0 0]);

end
