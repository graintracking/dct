function h_patch = gtDrawPoly3D(vertices, faces, patchColor)
% GTDRAWPOLY3D  Tool to draw a polyhedron
%     h_patch = gtDrawPoly3D(vertices, faces, [patchColor])
%     -----------------------------------------------------
%     Needs matGeom toolbox

if ~exist('patchColor','var') || isemtpy(patchColor)
    patchColor = false;
end

hf = figure();
drawPolyhedron(vertices,faces)
if (~patchColor)
    set(findobj(hf,'type','patch'),'FaceColor','none')
end

if nargout > 0
    h_patch = findobj(hf,'type','patch');
end

end

