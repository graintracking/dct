function hf = gtPlotUVW(im,uv,geo)
% hf = gtPlotUVW(im,uv,geo)
% <<<<< DEPRECATED >>>>>
% Use instead
%   noiseuv=gtOptimizePlotUVW(im,uv,color,geo,scale,titlename,debug,noise,numnoise)
% or
%   [h_figure, ha] = gtPlotMultiScatter3DMatrix(data, varargin)
%
%
% INPUT:
%   im    = output from gtPlaceDifspotsinfull <double 2048x2048>
%   uv    = u,v list to plot <double Nx2>
%   geo   = fitted geometry possibly
% 
% OUTPUT:
%   hf = figure handle

if ~exist('im','var') || isempty(im)
    im = zeros(geo.detsizeu, geo.detsizev);
end

hf = figure();
if ~isempty(im)
    imshow(im,[])
    hold on
end

detsizeu = geo.detsizeu;
detsizev = geo.detsizev;
beamdir  = geo.beamdir';
detdiru  = geo.detdiru';
detdirv  = geo.detdirv';
detdir   = geo.detnorm;
detpos   = geo.detrefpos; % mm
detrefu  = geo.detrefu;
detrefv  = geo.detrefv;

if isfield(geo,'Xzero')
    olddiru = geo.Xzero(1:3);
    olddirv = geo.Xzero(4:6);
    oldpos  = geo.Xzero(7:9).*((geo.pixelsizeu+geo.pixelsizev)/2); % mm
else
   olddiru = detdiru;
   olddirv = detdirv;
   oldpos  = geo.detrefpos;%.*geo.pixelsize
end


% Image frame
plot([0 detsizeu+1],[0 0],'k')
plot([0 detsizeu+1],[detsizev+1 detsizev+1],'k')
plot([0 0],[0 detsizev+1],'k')
plot([detsizeu+1 detsizeu+1],[0 detsizev+1],'k')

%% Midlines
plot([0 detsizeu+1],[detsizev/2+0.5 detsizev/2+0.5],'-.y','Linewidth',1)
plot([detsizeu/2+0.5 detsizeu/2+0.5],[0 detsizev+1],'-.y','Linewidth',1)

Qdet = gtFedDetectorProjectionTensor(detdiru,detdirv,1,1);

%% Beam direction in image
beamuv = Qdet*beamdir;
% Arrow in figure indicating beam direction

if (dot(beamdir,detdir)-1)<0.001 % // 1
    plot(detrefu,detrefv,'.w','MarkerSize',20)
elseif (dot(beamdir,detdir)+1)<0.001 % // -1
    plot(detrefu,detrefv,'xw','MarkerSize',20)
else
    quiver(detrefu,detrefv,beamuv(1),beamuv(2),1000,'-w','Linewidth',2)
end

%% origin u,v
plot(0,0,'.y','MarkerSize',20)
plot(0,0,'oy','MarkerSize',10)

% unit vectors u,v
quiver(0,0,0,1,150,'-y','Linewidth',2)
quiver(0,0,1,0,150,'-y','Linewidth',2)

%% center displacement
shift=oldpos-detpos;
newcenu=detrefu+dot(shift,olddiru)./geo.pixelsizeu;
newcenv=detrefv+dot(shift,olddirv)./geo.pixelsizev;
plot(newcenu,newcenv,'+y','MarkerSize',20)


for i=1:N
    plot(uv(i,1),uv(i,2),'.r','MarkerSize',30)
end

end %end of function
