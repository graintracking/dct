function gtPlotTwinsSchmidCmap(grain, cmap, pxsize, ifam)

twin_list_out = [];
load 4_grains/phase_01/twin_list_out.mat
gIDs = twin_list_out(twin_list_out(:,1)~=0,2);
twinIDs = twin_list_out(twin_list_out(:,1)~=0,1);

gtDrawGrainUnitCells({grain, grain},'pxsize',{pxsize,pxsize},...
'cmap',{cmap{ifam},cmap{ifam}},'ids',{gIDs,twinIDs},... 
'legend',true,'patch',{false,true},'label',{'parent','twin'},'figpos',[300 300 600 500],'paired',true)

axis auto

end