function [xpoints,ypoints]=ellipsepoints(xc,yc,semiaxisx,semiaxisy,nseg)
% ELLIPSEPOINTS.M Produces points lying on a ellipse
% Usage: [x,y]=ellipsepoints(xc,yc,major,minor)
%        uses a default (40) number of line segments to produces points for
%        an ellipse centred at xc,yc with major axis and minor axis as
%        specified
%        [x,y]=ellipsepoints(xc,yc,major,minor,nseg)
%        as for previous case, with user-specified number of line segments

% Greg Johnson, February 2007

if ~exist('nseg','var')  % if user didn't specify number of line segments
  nseg=40;               % make default
end

theta = 0 : (2 * pi / nseg) : (2 * pi);
xpoints = semiaxisx * cos(theta) + xc;
ypoints = semiaxisy * sin(theta) + yc;

% last point is the same as first point