function hf = gtPlotPoleFigures(grain, crystDir, name, varargin)
%     hf = gtPlotPoleFigures(grain, crystDir, name, varargin)
%
%     INPUT:  
%       grain    = <cell>       indexed grains with 'R_vector' field
%       crystDir = <cell>       {[0 0 0 2]; [1 1 -2 0]; [1 0 -1 0]}
%       name     = <string>     'RD4_15N_'
%
%     OPTIONAL INPUT:
%       varargin for gtMakePoleFigure: i.e. 'rings', 'surf', ...
%
%     OUTPUT:
%       hf       = <double>        array of figure handles


r_vectors = gtIndexAllGrainValues(grain,'R_vector',[],1,1:3);

ind = findValueIntoCell(varargin,{'axesRot'});
if ~isempty(ind)
    ind = ind(:,2) + 1;
    axesRot = varargin{ind};
else
    axesRot = [1 2 3];
end
ind2 = findValueIntoCell(varargin,{'clim'});
if ~isempty(ind2)
    ind2 = ind2(:,2) + 1;
    clim = varargin{ind2};
end
ind3 = findValueIntoCell(varargin,{'tag'});
if ~isempty(ind3)
    ind3 = ind3(:,2) + 1;
    tag = varargin{ind3};
else 
    tag = arrayfun(@(num) {num2str(num)},1:length(crystDir));
end

hf = [];
for ii=1:length(crystDir)
    [tmp, ~] = gtCrystAxisPoles(1,crystDir{ii},'r_vectors',r_vectors,'axesRot',axesRot,'save',false);
    varargin{end+1} = 'poles';
    varargin{end+1} = tmp;
    varargin{end+1} = 'crystDir';
    varargin{end+1} = crystDir{ii};
    varargin{end+1} = 'clim';
    if ~isempty(ind2)
        varargin{end+1} = clim(ii,:);
    else
        varargin{end+1} = [];
    end
    if ~isempty(name)
        title = [strrep(name,'_',' ') ' pole figure (' strrep(num2str(crystDir{ii}),' ','') ')'];
    else
        title = ['(' strrep(num2str(crystDir{ii}),' ','') ') pole figure'];
    end
    varargin{end+1} = 'title';
    varargin{end+1} = title;
    varargin{end+1} = 'tag';
    varargin{end+1} = tag{ii};
    
    hf(ii) = gtMakePoleFigure(varargin{:});

end

end % end of function

