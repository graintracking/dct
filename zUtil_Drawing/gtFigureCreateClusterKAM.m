function gtFigureCreateClusterKAM(cl, cl_rec, grs, varargin)
% function gtFigureCreateClusterKAM(cluster, cluster_rec, grs, varargin)
%
%     conf = struct( ...
%         'percent', 0.0025, ...
%         'filename', '', ...
%         'plane', 'xy', ...
%         'flip_ud', false, ...
%         'flip_lr', false, ...
%         'transpose', true, ...
%         'slice', 1, ...
%         'clims', [0 1], ...
%         'show_labels', false, ...
%         'pixel_to_cm', 0.1, ...
%         'rotation_axes', [0 0 1], ...
%         'rotation_angles', zeros(0, 1), ...
%         'use_slice_R_vector', false, ...
%         'borders', true, ...
%         'colorbar', true, ...
%         'extras', [] );

    conf = struct( ...
        'percent', 0.0025, ...
        'filename', '', ...
        'plane', 'xy', ...
        'flip_ud', false, ...
        'flip_lr', false, ...
        'transpose', true, ...
        'slice', 1, ...
        'clims', [0 1], ...
        'show_labels', false, ...
        'label', [], ...
        'pixel_to_cm', 0.1, ...
        'rotation_axes', [0 0 1], ...
        'rotation_angles', zeros(0, 1), ...
        'use_slice_R_vector', false, ...
        'borders', true, ...
        'colorbar', true, ...
        'tight', [], ...
        'extras', [] );
    conf = parse_pv_pairs(conf, varargin);

    if (~exist('cl_rec', 'var') || isempty(cl_rec))
        cl_rec = gtLoadClusterRec(cl.phaseid, cl.gids);
    end
    if (~exist('grs', 'var') || isempty(grs))
        if (isfield(cl, 'gids'))
            grs = gtLoadGrain(cl.phaseid, cl.gids);
        else
            grs = cl;
        end
    end

    p = gtLoadParameters();

    if (isempty(conf.rotation_angles))
        internal_shift = cl_rec.SEG.segbb(1:3) - cl_rec.ODF6D.shift;
        seg_vol_bb = [internal_shift, cl_rec.SEG.segbb(4:6)];

        seg_vol = cl_rec.SEG.seg;
    else
        cl_rec.ODF6D = gtGrainRotateStructure(cl_rec.ODF6D, ...
                cl.proj, 'ODF6D', p, conf.rotation_angles, conf.rotation_axes);

        rot_tensor = gtMathsRotationTensorComposite(conf.rotation_axes, conf.rotation_angles);

        volume_center = round(size(cl_rec.ODF6D.intensity) / 2);
        % not totally sure about the sese of rotation - to be verified!
        seed = ((cl_rec.SEG.seed - volume_center) * rot_tensor) + volume_center;

        t = GtThreshold();
        seg_struct = t.volumeThreshold(cl_rec.ODF6D.intensity, cl_rec.SEG.threshold, round(seed));

        seg_vol_bb = seg_struct.segbb;
        seg_vol = seg_struct.seg;
    end
    
    dm_vol = cl_rec.ODF6D.voxels_avg_R_vectors;
    int_vol = cl_rec.ODF6D.intensity;

    seg_vol = logical(gtPlaceSubVolume(zeros(size(int_vol), 'like', seg_vol), seg_vol, seg_vol_bb(1:3)));
    comp_seg_vol = imdilate(seg_vol, ones(3, 3, 3));

    comp_dm_seg_vol = cat(4, comp_seg_vol, comp_seg_vol, comp_seg_vol);

    dm_vol(~comp_dm_seg_vol) = 0;
    int_vol(~comp_seg_vol) = 0;

    if (conf.use_slice_R_vector)
        slice_dm_vol = gtVolumeGetSlice(dm_vol, conf.plane, conf.slice);
        slice_int_vol = gtVolumeGetSlice(int_vol, conf.plane, conf.slice);
        ref_r_vec = squeeze(sum(sum(slice_dm_vol .* slice_int_vol(:, :, [1 1 1]), 1), 2)) / sum(sum(slice_int_vol));
    else
        ref_r_vec = grs(1).R_vector;
    end

    [kam, gam] = gtDefComputeKernelAverageMisorientation(dm_vol, int_vol);
    fprintf('Grain Average Misorientation: %.3g°\n', gam)
    [igm, gos] = gtDefComputeIntraGranularMisorientation(dm_vol, int_vol, 'R_vector', ref_r_vec);
    fprintf('Grain Orientation Spread: %.3g°\n', gos)

    slice_kam = gtVolumeGetSlice(kam, conf.plane, conf.slice);
    slice_igm = gtVolumeGetSlice(igm, conf.plane, conf.slice);
    slice_seg = gtVolumeGetSlice(seg_vol, conf.plane, conf.slice);

    %

    % cuting the images
    horz_profile = sum(slice_seg, 1);
    vert_profile = sum(slice_seg, 2);
    horz_lims = [find(horz_profile, 1, 'first'), find(horz_profile, 1, 'last')];
    vert_lims = [find(vert_profile, 1, 'first'), find(vert_profile, 1, 'last')];

    slice_kam = slice_kam(vert_lims(1):vert_lims(2), horz_lims(1):horz_lims(2));
    slice_igm = slice_igm(vert_lims(1):vert_lims(2), horz_lims(1):horz_lims(2));
    slice_seg = slice_seg(vert_lims(1):vert_lims(2), horz_lims(1):horz_lims(2));

    if (conf.flip_lr)
        slice_kam = fliplr(slice_kam);
        slice_igm = fliplr(slice_igm);
        slice_seg = fliplr(slice_seg);
    end
    if (conf.flip_ud)
        slice_kam = flipud(slice_kam);
        slice_igm = flipud(slice_igm);
        slice_seg = flipud(slice_seg);
    end
    if (conf.transpose)
        slice_kam = permute(slice_kam, [2 1 3]);
        slice_igm = permute(slice_igm, [2 1 3]);
        slice_seg = permute(slice_seg, [2 1 3]);
    end

    %

    if (~isempty(conf.tight))
        if (numel(conf.tight) == 1)
            conf.tight = conf.tight([1 1; 1 1]);
        elseif (size(conf.tight, 1) == 1)
            conf.tight = conf.tight([1 1], :);
        elseif (size(conf.tight, 2) == 1)
            conf.tight = conf.tight(:, [1 1]);
        end
        slice_kam = padarray(slice_kam, conf.tight(:, 1), 0, 'pre');
        slice_igm = padarray(slice_igm, conf.tight(:, 1), 0, 'pre');
        slice_seg = padarray(slice_seg, conf.tight(:, 1), false, 'pre');
        slice_kam = padarray(slice_kam, conf.tight(:, 2), 0, 'post');
        slice_igm = padarray(slice_igm, conf.tight(:, 2), 0, 'post');
        slice_seg = padarray(slice_seg, conf.tight(:, 2), false, 'post');
    end

    img_size = [size(slice_seg, 2), size(slice_seg, 1)];

    clims_kam = [conf.clims(1), min(conf.clims(2), max(slice_kam(:)))];

    f = figure();
    ax = axes('parent', f);
    imagesc(slice_kam, 'parent', ax, ...
        'AlphaDataMapping', 'scaled', 'AlphaData', slice_seg, ...
        clims_kam );
    colormap(ax, jet)

    if (~isempty(conf.extras))
        im_props = struct( ...
            'pixel_size', p.recgeo.voxsize * 1e3, ...
            'size', img_size );
        gtFigureAddExtras(ax, im_props, conf.extras);
    end

    if (conf.show_labels)
        conf.label = sprintf('Grain Average Misorientation: %.3g°', gam);
    end
    gtFigureSetSize(f, ax, conf, img_size);

    drawnow();

    if (~isempty(conf.filename))
        gtFigureSaveToFile(f, [conf.filename 'KAM']);
    end

    %

    clims_igm = [conf.clims(1), max(slice_igm(:))];

    f = figure();
    ax = axes('parent', f);
    imagesc(slice_igm, 'parent', ax, ...
        'AlphaDataMapping', 'scaled', 'AlphaData', slice_seg, ...
        clims_igm );
    colormap(ax, jet);

    if (~isempty(conf.extras))
        im_props = struct( ...
            'pixel_size', p.recgeo.voxsize * 1e3, ...
            'size', img_size );
        gtFigureAddExtras(ax, im_props, conf.extras);
    end

    if (conf.show_labels)
        conf.label = sprintf('Grain Orientation Spread: %.3g°', gos);
    end
    gtFigureSetSize(f, ax, conf, img_size);

    drawnow();

    if (~isempty(conf.filename))
        gtFigureSaveToFile(f, [conf.filename 'IGM']);
    end
end
