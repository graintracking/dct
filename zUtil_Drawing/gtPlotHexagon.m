function [h_link, h] = gtPlotHexagon(grains, varargin)
% GTPLOTHEXAGON  Makes a figure containing subplots of the unit cells of the 
%                selected grains
%
%     [h_link, h] = gtPlotHexagon(grains, varargin)
%     ---------------------------------------------
%     It does not take into account the grain size and the elastic strain.
%
%     Deals with coordinate systems.  R-vector orientation data in the instrument
%     system: x//beam, z upwards, y towards door.
%     In the reconstructed tomography/grain map data, z is downwards, y//x
%     instruments, x//y instrument. Use tomo_setup flag to use this setup.
%
%     For the reference hexagonal unit cell coordinates see gtHexagonalUnitCell.
%
%     INPUT:
%       grains     = <cell>       grain cell-structures for all the grains (Nx1)
%
%     OPTIONAL INPUT (varargin):
%       ids        = <double>     IDs of grains of interest (1xM)
%       cmap       = <double>     Colour map for all the grains (RGB) (N+1x3) {[1 0 0]}
%       angle      = <double>     rotation around axis to visualize orientations 
%                                 from the section plane as defined in 
%                                 gtShowSampleSurface {0}
%       axis       = <double>     rotation axis {[0 0 1]}
%       translate  = <logical>    flag to apply grain center {false}
%       tomo_setup = <logical>    flag to match tomo reference system
%                                 x-->y y-->x z-->-z {false}
%       reference  = <logical>    flag to plot the reference unit cell for each 
%                                 grain orientation {false}
%       section    = <logical>    flag to draw a section on the yz-plane to follow
%                                 gtShowSampleSurface {false}
%       overlap    = <logical>    flag to overlap the first grain in the list to all the
%                                 other grains {false}
%       caxis      = <logical>    flag to draw cell c-axis for each grain {false}
%       view       = <double>     azimuth and elevation for viewing in 3D (1x2) {[-90 0]}
%       linkp      = <cell>       list of properties to link {'View','Visible'}
%       ratio      = <double>     c/a ratio {2}
%       linecolor  = <double>     RGB for linecolor {[0 0 0]}
%       alpha      = <double>     transparency 0<=alpha<=1 {1}
%       patch      = <logical>    
%       secpos     = <double>     
%       plane      = <string>     
%
%     OUTPUT:
%       h_link     = <graphics.linkprop>
%       h          = <handle>     handles list
%
%
%     Version 004 05-06-2013 by LNervo
%       Added c-axis line;
%       Linked some axes properties and given output as 'h_link'
%
%     Version 003 16-01-2013 by LNervo

%     TO DO LIST: fix bug with facealpha..... if ~= 1, it changes the face color

app.ids        = [];
app.cmap       = [];
app.angle      = 0;
app.axis       = [0 0 1];
app.translate  = false;
app.tomo_setup = false;
app.reference  = false;
app.section    = false;
app.overlap    = false;
app.caxis      = false;
app.view       = [-90 0]; % yz plane x positive into the screen
app.linkp      = {'View','Visible'};
app.ratio      = 1.5857; % c/a
app.linecolor  = [0 0 0];
app.alpha      = 1;
app.hf         = [];
app.ha         = [];
app.render     = 'OpenGL';
app.patch      = true;
app.secpos     = [];
app.plane      = 'YZ';
app = parse_pv_pairs(app,varargin);


% keep info on grainID in the first column
if isempty(app.ids)
    app.ids = 1:length(grains);
end
if isempty(app.cmap)
    app.cmap = repmat([1 0 0], length(grains), 3);
    app.showbar = false;
end

if size(app.cmap,1)==1 && size(app.cmap,2)>1
    app.cmap = repmat(app.cmap,length(grains),1);
end
    % remove bkg color
if all(app.cmap(1,:) == [0 0 0])
    app.cmap = app.cmap(2:end,:);
end

grains  = grains(app.ids);
cmap    = app.cmap(app.ids,:);

if isempty(app.hf)
    hf = figure('Color',[1 1 1]);
    app.hf = hf;
else
    hf = app.hf;
end
figure(hf)
if isempty(app.ha)
    app.ha = axes('Parent',hf,'Color',[1 1 1]);
end

% find grid size
if length(grains) > 5
    xdim = 5;
    ydim = ceil(length(grains)/xdim);
else
    xdim = length(grains);
    ydim = 1;
end
set(hf, 'Position', [10 10 200*xdim 250*ydim])

% patch object hexagonal prism
data = gtHexagonalUnitCell('ratio',app.ratio,'draw',false,'centered',true,'caxis',app.caxis);

if (app.translate)
    centers = gtIndexAllGrainValues(grains, 'center', [], 1, 1:3);
    sizes   = gtIndexAllGrainValues(grains, 'size_int', [], 1, 1)/1000*0.5; % mm grain equivalent radius
else
    centers = zeros(length(grains), 3);
    sizes   = zeros(length(grains), 1);
end

rvectors = gtIndexAllGrainValues(grains, 'R_vector', [], 1, 1:3);

p = [];
for jj = 1:length(grains)
    c_axis = [];
    grainID = grains{jj}.id;
    
    vertices = gtComputeGrainUnitCell(rvectors(jj,:), data.vertices, ...
        sizes(jj), centers(jj,:), [], []);
    if (app.caxis)
        c_axis = gtComputeGrainUnitCell(rvectors(jj,:), data.c_axis, ...
            sizes(jj), centers(jj,:), [], []);
    end
 
    % allow for the two coordinate systems.  Transform from instrument to
    % reconstructed tomo coordinates: x-->y y-->x z-->-z
    if (app.tomo_setup)
        vertices = [vertices(:,2) vertices(:,1) -vertices(:,3)];
        if (app.caxis)
            c_axis = [c_axis(:,2) c_axis(:,1) -c_axis(:,3)];
        end
    end
    % do the rotate to follow the section plane
    vertices = rotateVectors(vertices, 'angle', app.angle, 'axis', app.axis);
    if (app.caxis)
        c_axis = rotateVectors(c_axis, 'angle', app.angle, 'axis', app.axis);
    end
    
    if (app.overlap) && jj == 1
        vertices_overlap = vertices;
        if (app.caxis)
            caxis_overlap = c_axis;
        else
            caxis_overlap = [];
        end
    end
    
    subplot(ydim, xdim, jj);
    hold on
    % reference hex
    if (app.reference) && (~app.overlap)
        hp_reference = gtPlotGrainUnitCell(data.vertices, data.c_axis, data.faces, ...
                 [0.5 0.5 0.5], [], app.linecolor, 0.5, app.patch);
    end
    % if overlap the first grain is used as reference for the others
    if (app.overlap) && jj ~= 1
        hp_overlap = gtPlotGrainUnitCell(vertices_overlap, caxis_overlap, data.faces, ...
                 cmap(1,:), [], app.linecolor, 0.5, ~app.patch);
    end
    % reference background like the section plane
    if (app.section)
        pos=min(mean(vertices(data.faces_end(1,:), 1)), mean(vertices(data.faces_end(2,:), 1)));
        if ~isempty(app.secpos)
            pos = app.secpos;
        end
        section_plane_vertices=[pos 1 1; pos 1 -1; pos -1 1; pos -1 -1];
        if strcmpi(app.plane, 'XZ')
            section_plane_vertices = section_plane_vertices(:,[2 1 3]);
        end
        if strcmpi(app.plane, 'XY')
            section_plane_vertices = section_plane_vertices(:,[2 3 1]);
        end
        hp_section = gtPlotGrainUnitCell(section_plane_vertices, [], [1 2 4 3], ...
                 [0 0 0], [], app.linecolor, 0.4, app.patch);
    end
    % including the rotation for sectioning
    p{jj} = gtPlotGrainUnitCell(vertices, c_axis, data.faces, ...
             cmap(jj,:), [], app.linecolor, app.alpha, app.patch);
    
    % a bit of labeling
    title(sprintf('grain %04d', grainID));
    xlabel('X');
    ylabel('Y');
    zlabel('Z');
    % view
    view(app.view);

    % activate rotating option
    h_rot3d = rotate3d(hf);
    set(h_rot3d,'RotateStyle','box','Enable','on');

    axis vis3d;
    axis equal;
    
    userdata(jj) = struct('id', grains{jj}.id, 'center', centers(jj,:), 'size', sizes(jj,:), 'R_vector', rvectors(jj,:), ...
                          'cmap', cmap(jj,:), 'caxis', c_axis, 'vertices', vertices);

end % end for grains

app.cmap = cmap;
app.userdata = userdata;

handles.hf = hf;
handles.ha = findobj(hf,'type','axes');
handles.hp = p;
handles.hcaxis = findobj(hf,'type','line');

app.ha = handles.ha;
h_link = linkprop(app.ha,app.linkp);

setappdata(hf,'h_link',h_link);
setappdata(hf,'AppData',app);
setappdata(hf,'handles',handles);

print_structure(app,'gtPlotHexagon settings')

if nargout == 2
    h = handles;
end

end % end of function
