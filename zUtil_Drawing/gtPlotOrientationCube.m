function gtPlotOrientationCube(r_vector, colour)
% plot a cube corresponding to the orientation of a grain
%be careful regarding axes and coordinate systems!
%should be that this cube is plotted in instrument coordinates
% tomo reconstruction coordinates: x tomo//y inst ; y tomo // x inst ; z
% tomo // negative z inst
% AK - 9/2008

corners=[0 0 0; 1 0 0; 0 1 0; 0 0 1; 1 1 0; 1 0 1; 0 1 1; 1 1 1 ];
faces=[1 3 5 2; 1 4 6 2; 1 4 7 3; 4 6 8 7; 3 5 8 7; 2 5 8 6];

g=rod2g(r_vector);
cornersA=(g*corners')';

figure
patch('Vertices', cornersA, 'Faces', faces, 'FaceColor', colour, 'FaceAlpha', 1)

axis equal