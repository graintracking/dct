function requires = gtFigureExtrasRequireParameters(extras)
    if (iscell(extras))
        requires = any(cellfun(@gtFigureExtrasRequireParameters, extras));
    else
        requires = any(strcmpi(extras.type, {'unit_bar'}));
    end
end
