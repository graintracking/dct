function gtFigureCreatePhase6D( phase_mat, phase_id, varargin )
% gtFigureCreatePhase6D( phase_mat, phase_id, varargin )
%
%     conf = struct( ...
%         'lims_u', [], ...
%         'lims_v', [], ...
%         'ipf_axis', [], ...
%         'use_dmvol', true, ...
%         'abs_vol', [], ...
%         'abs_mask', [], ...
%         'filename', '', ...
%         'plane', 'xy', ...
%         'flip_ud', false, ...
%         'flip_lr', false, ...
%         'transpose', true, ...
%         'slice', 1, ...
%         'clims', [], ...
%         'pixel_to_cm', 0.05, ...
%         'borders', false, ...
%         'colorbar', false, ...
%         'extras', [] );

    conf = struct( ...
        'ipf_axis', [], ...
        'use_dmvol', true, ...
        'abs_vol', [], ...
        'abs_mask', [], ...
        'plane', 'xy', ...
        'slice', 1 );
    [conf, extra_pars] = parse_pv_pairs(conf, varargin);
    extra_pars = [extra_pars, {'borders', false, 'colorbar', false}];

    slice_ids = gtVolumeGetSlice(phase_mat.vol, conf.plane, conf.slice);
    slice_size = size(slice_ids);

    slice_R = zeros(slice_size);
    slice_G = zeros(slice_size);
    slice_B = zeros(slice_size);

    if (conf.use_dmvol && isfield(phase_mat, 'dmvol'))
        slice_rod = gtVolumeGetSlice(phase_mat.dmvol, conf.plane, conf.slice);
        indx = find(slice_ids > 0);
        r_vecs = reshape(slice_rod, [], 3);
        r_vecs = r_vecs(indx, :);

        cmap = get_cmap(r_vecs, phase_id, conf.ipf_axis);

        slice_R(indx) = cmap(:, 1);
        slice_G(indx) = cmap(:, 2);
        slice_B(indx) = cmap(:, 3);
    else
        sample = GtSample.loadFromFile();

        gids = sort(unique(slice_ids));
        gids(gids < 1) = [];
        r_vecs = sample.phases{phase_id}.R_vector(gids, :);

        cmap = get_cmap(r_vecs, phase_id, conf.ipf_axis);

        for ii = 1:numel(gids)
            indx = find(slice_ids == gids(ii));
            slice_R(indx) = cmap(ii, 1);
            slice_G(indx) = cmap(ii, 2);
            slice_B(indx) = cmap(ii, 3);
        end
    end

    if (~isempty(conf.abs_vol))
        slice_abs = gtVolumeGetSlice(conf.abs_vol, conf.plane, conf.slice);
        slice_abs = (slice_abs - min(slice_abs(:))) ./ (max(slice_abs(:)) - min(slice_abs(:)));
    end

    if (~isempty(conf.abs_mask))
        slice_mask = gtVolumeGetSlice(conf.abs_mask, conf.plane, conf.slice);
        if (~isempty(conf.abs_vol))
            slice_R(~slice_mask) = slice_abs(~slice_mask);
            slice_G(~slice_mask) = slice_abs(~slice_mask);
            slice_B(~slice_mask) = slice_abs(~slice_mask);
        else
            slice_R(~slice_mask) = 0;
            slice_G(~slice_mask) = 0;
            slice_B(~slice_mask) = 0;
        end
    end

    final_slice_RGB = cat(3, slice_R, slice_G, slice_B);
    gtFigureCreateFromPicture(final_slice_RGB, [], extra_pars{:});
end

function cmap = get_cmap(r_vecs, phase_id, ipf_axis)
    p = gtLoadParameters();
    cryst_system = p.cryst(phase_id).crystal_system;
    cryst_spacegroup = p.cryst(phase_id).spacegroup;
    symm = gtCrystGetSymmetryOperators(cryst_system, cryst_spacegroup);

    [cmap, ~, ~] = gtIPFCmap(phase_id, ipf_axis, ...
        'r_vectors', r_vecs, ...
        'crystal_system', cryst_system, ...
        'background', false, ...
        'symm', symm);
end

