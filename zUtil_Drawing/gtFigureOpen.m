function f = gtFigureOpen(varargin)
    conf = struct('name', [], 'size', []);
    conf = parse_pv_pairs(conf, varargin);

    if (~isempty(conf.size))
        conf.size = [1, 1, conf.size];
        f = figure(...
            'Name', conf.name, ...
            'Units', 'centimeters', ...
            'Position', conf.size);
    else
        f = figure('Name', conf.name, 'Units', 'centimeters');
    end
end
