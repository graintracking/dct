function gtFigureCreateTwinClusterKAM(cl, cl_rec, varargin)
% function gtFigureCreateTwinClusterKAM(cluster, cluster_rec, varargin)
%
%     conf = struct( ...
%         'percent', 0.0025, ...
%         'filename', '', ...
%         'plane', 'xy', ...
%         'flip_ud', false, ...
%         'flip_lr', false, ...
%         'transpose', true, ...
%         'slice', 1, ...
%         'clims', [0 1], ...
%         'show_labels', false, ...
%         'pixel_to_cm', 0.1, ...
%         'rotation_axes', zeros(0, 3), ...
%         'rotation_angles', zeros(0, 1), ...
%         'use_slice_R_vector', false, ...
%         'borders', true, ...
%         'colorbar', true );

    conf = struct( ...
        'percent', 0.0025, ...
        'filename', '', ...
        'plane', 'xy', ...
        'flip_ud', false, ...
        'flip_lr', false, ...
        'transpose', true, ...
        'slice', 1, ...
        'clims', [0 1], ...
        'show_labels', false, ...
        'pixel_to_cm', 0.1, ...
        'rotation_axes', zeros(0, 3), ...
        'rotation_angles', zeros(0, 1), ...
        'use_slice_R_vector', false, ...
        'borders', true, ...
        'colorbar', true );
    conf = parse_pv_pairs(conf, varargin);

    num_subgrains = numel(cl.samp_ors);

    if (~exist('cl_rec', 'var') || isempty(cl_rec))
        cl_rec = gtLoadClusterRec(cl.samp_ors(1).phaseid, cl.gids);
    end

    if (~isempty(conf.rotation_angles))
        p = gtLoadParameters();
        for ii_g = 1:num_subgrains
            cl_rec.ODF6D(ii_g) = gtGrainRotateStructure(cl_rec.ODF6D(ii_g), ...
                cl.samp_ors(ii_g).proj, 'ODF6D', p, conf.rotation_angles, conf.rotation_axes);
        end

        total_int_vol = cl_rec.ODF6D(1).intensity;
        volume_center = size(total_int_vol) / 2;
        rot = gtMathsRotationTensorComposite(conf.rotation_axes, conf.rotation_angles);

        t = GtThreshold();
        for ii_g = 2:num_subgrains
            seed = round((cl_rec.SEG(ii_g).seed - volume_center) * rot + volume_center);
            cl_rec.SEG(ii_g) = t.volumeThreshold(cl_rec.ODF6D(ii_g).intensity, cl_rec.SEG(ii_g).threshold, seed);
            cl_rec.SEG(ii_g).seg = gtPlaceSubVolume( ...
                zeros(size(total_int_vol), 'like', cl_rec.SEG(ii_g).seg), ...
                cl_rec.SEG(ii_g).seg, cl_rec.SEG(ii_g).segbb(1:3));
            cl_rec.SEG(ii_g).seg = logical(cl_rec.SEG(ii_g).seg);
            cl_rec.SEG(ii_g).segbb(1:3) = cl_rec.ODF6D(ii_g).shift;

            total_int_vol = total_int_vol + cl_rec.ODF6D(ii_g).intensity;
        end

        seed = round((cl_rec.SEG(1).seed - volume_center) * rot + volume_center);
        cl_rec.SEG(1) = t.volumeThreshold(total_int_vol, cl_rec.SEG(1).threshold, seed);
        cl_rec.SEG(1).seg = gtPlaceSubVolume( ...
            zeros(size(total_int_vol), 'like', cl_rec.SEG(1).seg), ...
            cl_rec.SEG(1).seg, cl_rec.SEG(1).segbb(1:3));
        cl_rec.SEG(1).seg = logical(cl_rec.SEG(1).seg);
        cl_rec.SEG(1).segbb(1:3) = cl_rec.ODF6D(1).shift;
        for ii_g = 2:num_subgrains
            cl_rec.SEG(ii_g).seg = cl_rec.SEG(ii_g).seg & cl_rec.SEG(1).seg;
            cl_rec.SEG(1).seg = ~cl_rec.SEG(ii_g).seg & cl_rec.SEG(1).seg;
        end
    end

    kam = cell(num_subgrains, 1);
    gam = zeros(num_subgrains, 1);
    igm = cell(num_subgrains, 1);
    gos = zeros(num_subgrains, 1);
    for ii_g = 1:num_subgrains
        dm_vol = cl_rec.ODF6D(ii_g).voxels_avg_R_vectors .* cat(4, cl_rec.SEG([ii_g ii_g ii_g]).seg);
        int_vol = cl_rec.ODF6D(ii_g).intensity .* cl_rec.SEG(ii_g).seg;

        if (conf.use_slice_R_vector)
            slice_dm_vol = squeeze(gtVolumeGetSlice(dm_vol, conf.plane, conf.slice));
            slice_int_vol = gtVolumeGetSlice(int_vol, conf.plane, conf.slice);
            ref_r_vec = squeeze(sum(sum(slice_dm_vol .* slice_int_vol(:, :, [1 1 1]), 1), 2)) / sum(sum(slice_int_vol));
        else
            ref_r_vec = cl.samp_ors(ii_g).R_vector;
        end

        [kam{ii_g}, gam(ii_g)] = gtDefComputeKernelAverageMisorientation(dm_vol, int_vol);
        [igm{ii_g}, gos(ii_g)] = gtDefComputeIntraGranularMisorientation(dm_vol, int_vol, 'R_vector', ref_r_vec);
    end
    tot_kam = kam{1} .* cl_rec.SEG(1).seg;
    tot_igm = igm{1} .* cl_rec.SEG(1).seg;
    seg_vol = cl_rec.SEG(1).seg;

    for ii_g = 2:num_subgrains
        tot_kam = tot_kam + kam{ii_g} .* cl_rec.SEG(ii_g).seg;
        tot_igm = tot_igm + igm{ii_g} .* cl_rec.SEG(ii_g).seg;
        seg_vol = seg_vol | cl_rec.SEG(ii_g).seg;
    end

    slice_kam = gtVolumeGetSlice(tot_kam, conf.plane, conf.slice);
    slice_igm = gtVolumeGetSlice(tot_igm, conf.plane, conf.slice);
    slice_seg = gtVolumeGetSlice(seg_vol, conf.plane, conf.slice);

    %

    % Note the inversion between lr/up because of matlab's transpose problem
    if (conf.flip_ud)
        slice_kam = fliplr(slice_kam);
        slice_igm = fliplr(slice_igm);
        slice_seg = fliplr(slice_seg);
    end
    if (conf.flip_lr)
        slice_kam = flipud(slice_kam);
        slice_igm = flipud(slice_igm);
        slice_seg = flipud(slice_seg);
    end
    if (conf.transpose)
        slice_kam = permute(slice_kam, [2 1 3]);
        slice_igm = permute(slice_igm, [2 1 3]);
        slice_seg = permute(slice_seg, [2 1 3]);
    end

    %

    img_size = [size(slice_seg, 2), size(slice_seg, 1)];

    clims_kam = [conf.clims(1), min(conf.clims(2), max(slice_kam(:)))];

    f = figure();
    ax = axes('parent', f);
    imagesc(slice_kam, 'parent', ax, ...
        'AlphaDataMapping', 'scaled', 'AlphaData', slice_seg, ...
        clims_kam );
    colormap(ax, jet)

    if (~isempty(conf.extras))
        im_props = struct( ...
            'pixel_size', p.recgeo.voxsize * 1e3, ...
            'size', img_size );
        gtFigureAddExtras(ax, im_props, conf.extras);
    end

    if (conf.show_labels)
        conf.label = sprintf('Grain Average Misorientations:%s', sprintf(' %.3g°', gam));
    end
    gtFigureSetSize(f, ax, conf, img_size);

    drawnow();

    if (~isempty(conf.filename))
        gtFigureSaveToFile(f, [conf.filename 'KAM']);
    end

    %

    clims_igm = [conf.clims(1), max(slice_igm(:))];

    f = figure();
    ax = axes('parent', f);
    imagesc(slice_igm, 'parent', ax, ...
        'AlphaDataMapping', 'scaled', 'AlphaData', slice_seg, ...
        clims_igm );
    colormap(ax, jet);

    if (~isempty(conf.extras))
        im_props = struct( ...
            'pixel_size', p.recgeo.voxsize * 1e3, ...
            'size', img_size );
        gtFigureAddExtras(ax, im_props, conf.extras);
    end

    if (conf.show_labels)
        conf.label = sprintf('Grain Orientation Spreads:%s', sprintf(' %.3g°', gos));
    end
    gtFigureSetSize(f, ax, conf, img_size);

    drawnow();

    if (~isempty(conf.filename))
        gtFigureSaveToFile(f, [conf.filename 'IGM']);
    end
end
