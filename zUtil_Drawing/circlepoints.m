function [xpoints,ypoints]=circlepoints(x,y,r,nseg)
% CIRCLEPOINTS.M Produces points lying on a circle
% Usage: [x,y]=circlepoints(xc,yc,radius)
%        uses a default (40) number of line segments to produces points for a circle centred at xc,yc with radius 'radius'
%        [x,y]=circlepoints(xc,yc,radius,nseg)
%        as for previous case, with user-specified number of line segments

% Greg Johnson, September 2006

if ~exist('nseg','var')  % if user didn't specify number of line segments
  nseg=40;               % make default
end

theta = 0 : (2 * pi / nseg) : (2 * pi);
xpoints = r * cos(theta) + x;
ypoints = r * sin(theta) + y;

% last point is the same as first point