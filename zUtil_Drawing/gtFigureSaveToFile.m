function gtFigureSaveToFile(f, filename, extensions)
    if (~exist('extensions', 'var') || isempty(extensions))
        extensions = {'png', 'eps'};
    end

    for ii_ext = 1:numel(extensions)
        if (strcmpi(extensions{ii_ext}, 'eps'))
            saveas(f, [filename '.' extensions{ii_ext}], 'epsc');
        else
            saveas(f, [filename '.' extensions{ii_ext}]);
        end
    end
end
