function gtFigureCreateEBSDMapIPF(EBSD_struct, varargin)
% FUNCTION gtFigureCreateEBSDMapIPF(EBSD_struct, varargin)
%
%     conf = struct( ...
%         'ipf_axis', [], ...
%         'flip_ud', false, ...
%         'flip_lr', false, ...
%         'transpose', true, ...
%         'clims', [], ...
%         'lims_u', [], ...
%         'lims_v', [], ...
%         'scale', [], ...
%         'pixel_to_cm', 0.05, ...
%         'borders', false, ...
%         'colorbar', false, ...
%         'extras', [] );

    conf = struct( ...
        'ipf_axis', [], ...
        'flip_ud', false, ...
        'flip_lr', false, ...
        'transpose', true, ...
        'clims', [], ...
        'lims_u', [], ...
        'lims_v', [], ...
        'scale', [], ...
        'pixel_to_cm', 0.05, ...
        'borders', false, ...
        'colorbar', false, ...
        'filename', [], ...
        'extras', [] );
    conf = parse_pv_pairs(conf, varargin);

    EBSD_r_map = EBSD_struct.map_r;
    if (conf.transpose)
        EBSD_r_map = permute(EBSD_r_map, [2 1 3]);
    end

    if (~isempty(conf.lims_u))
        % Note the inversion between x/y because of matlab's transpose problem
        EBSD_r_map = EBSD_r_map(:, conf.lims_u(1):conf.lims_u(2), :);
    end

    if (~isempty(conf.lims_v))
        % Note the inversion between x/y because of matlab's transpose problem
        EBSD_r_map = EBSD_r_map(conf.lims_v(1):conf.lims_v(2), :, :);
    end


    if (~isempty(conf.scale))
        % Note the inversion between x/y because of matlab's transpose problem
        EBSD_r_map = imresize(EBSD_r_map, 'nearest', 'scale', conf.scale);
    end

    slice_ids = all(EBSD_r_map ~= 0, 3);
    indx = find(slice_ids);
    r_vecs = reshape(EBSD_r_map, [], 3);
    r_vecs = r_vecs(indx, :);

    cmap = get_cmap(r_vecs, EBSD_struct.phase_id, conf.ipf_axis);

    slice_R = zeros(size(slice_ids));
    slice_G = zeros(size(slice_ids));
    slice_B = zeros(size(slice_ids));

    slice_R(indx) = cmap(:, 1);
    slice_G(indx) = cmap(:, 2);
    slice_B(indx) = cmap(:, 3);

    if (~isempty(EBSD_struct.mask))
        slice_mask = EBSD_struct.mask;
        if (conf.transpose)
            slice_mask = permute(slice_mask, [2 1 3]);
        end

        if (~isempty(conf.lims_u))
            % Note the inversion between x/y because of matlab's transpose problem
            slice_mask = slice_mask(:, conf.lims_u(1):conf.lims_u(2), :);
        end

        if (~isempty(conf.lims_v))
            % Note the inversion between x/y because of matlab's transpose problem
            slice_mask = slice_mask(conf.lims_v(1):conf.lims_v(2), :, :);
        end

        if (~isempty(conf.scale))
            % Note the inversion between x/y because of matlab's transpose problem
            slice_mask = imresize(slice_mask, 'nearest', 'scale', conf.scale);
        end
        
        slice_R(~slice_mask) = 0;
        slice_G(~slice_mask) = 0;
        slice_B(~slice_mask) = 0;
    end

    if (conf.flip_lr)
        slice_R = fliplr(slice_R);
        slice_G = fliplr(slice_G);
        slice_B = fliplr(slice_B);
    end
    if (conf.flip_ud)
        slice_R = flipud(slice_R);
        slice_G = flipud(slice_G);
        slice_B = flipud(slice_B);
    end

    final_slice_RGB = cat(3, slice_R, slice_G, slice_B);
    img_size = [size(final_slice_RGB, 2), size(final_slice_RGB, 1)];

    f = figure();
    ax = axes('Parent', f);
    imagesc(final_slice_RGB, 'Parent', ax);

    if (~isempty(conf.extras))
        if (~isempty(conf.scale))
            pix_size = EBSD_struct.pixel_size ./ conf.scale;
        else
            pix_size = EBSD_struct.pixel_size;
        end
        im_props = struct( ...
            'pixel_size', pix_size, ...
            'size', img_size );
        gtFigureAddExtras(ax, im_props, conf.extras);
    end

    set(ax, 'xtick', [])
    set(ax, 'xticklabel', [])
    set(ax, 'ytick', [])
    set(ax, 'yticklabel', [])

    gtFigureSetSize(f, ax, conf, img_size)

    if (~isempty(conf.filename))
        gtFigureSaveToFile(f, conf.filename);
    end
end

function cmap = get_cmap(r_vecs, phase_id, ipf_axis)
    p = gtLoadParameters();
    cryst_system = p.cryst(phase_id).crystal_system;
    cryst_spacegroup = p.cryst(phase_id).spacegroup;
    symm = gtCrystGetSymmetryOperators(cryst_system, cryst_spacegroup);

    [cmap, ~, ~] = gtIPFCmap(phase_id, ipf_axis, ...
        'r_vectors', r_vecs, ...
        'crystal_system', cryst_system, ...
        'background', false, ...
        'symm', symm);
end

