function gtFigureCreateClusterIPF(cluster, cluster_rec, varargin)
% function gtFigureCreateClusterIPF(cluster, cluster_rec, varargin)
%
%     conf = struct( ...
%         'plane_normal', [0 0 1], ...
%         'kam_bound', 0.15, ...
%         'filename', '', ...
%         'plane', 'xy', ...
%         'flip_ud', false, ...
%         'flip_lr', false, ...
%         'transpose', true, ...
%         'slice', 1, ...
%         'clims', [0 1], ...
%         'show_labels', false, ...
%         'pixel_to_cm', 0.1, ...
%         'rotation_axis', zeros(0, 3), ...
%         'rotation_angle', zeros(0, 1), ...
%         'borders', false, ...
%         'colorbar', false, ...
%         'extras', [] );

    conf = struct( ...
        'plane_normal', [0 0 1], ...
        'kam_bound', 0.15, ...
        'filename', '', ...
        'plane', 'xy', ...
        'flip_ud', false, ...
        'flip_lr', false, ...
        'transpose', true, ...
        'slice', 1, ...
        'clims', [0 1], ...
        'show_labels', false, ...
        'pixel_to_cm', 0.1, ...
        'rotation_axis', zeros(0, 3), ...
        'rotation_angle', zeros(0, 1), ...
        'borders', false, ...
        'colorbar', false, ...
        'pixel_size', [], ...
        'tight', [], ...
        'extras', [] );
    conf = parse_pv_pairs(conf, varargin);

    if (~exist('cluster_rec', 'var') || isempty(cluster_rec))
        cluster_rec = gtLoadClusterRec(cluster.phaseid, cluster.gids);
    end

    full_dm_vol = cluster_rec.ODF6D.voxels_avg_R_vectors;

    if (isempty(conf.rotation_angle))
        internal_shift = cluster_rec.SEG.segbb(1:3) - cluster_rec.ODF6D.shift + 1;
        seg_vol_bb = [internal_shift, cluster_rec.SEG.segbb(4:6)];

        seg_vol = logical(cluster_rec.SEG.seg);
    else
        rotcomp = gtMathsRotationMatrixComp(conf.rotation_axis', 'col');
        rot_tensor = gtMathsRotationTensor(conf.rotation_angle, rotcomp);

        volume_center = round(size(cluster_rec.ODF6D.intensity) / 2);
        % not totally sure about the sese of rotation - to be verified!
        seed = ((cluster_rec.SEG.seed - volume_center) * rot_tensor) + volume_center;

        int_vol = gtRotateVolume(cluster_rec.ODF6D.intensity, rot_tensor);

        t = GtThreshold();
        seg_struct = t.volumeThreshold(int_vol, cluster_rec.SEG.threshold, seed);

        seg_vol_bb = seg_struct.segbb + [1 1 1 0 0 0];
        seg_vol = logical(seg_struct.seg);

        full_dm_vol(:, :, :, 3) = gtRotateVolume(full_dm_vol(:, :, :, 3), rot_tensor);
        full_dm_vol(:, :, :, 2) = gtRotateVolume(full_dm_vol(:, :, :, 2), rot_tensor);
        full_dm_vol(:, :, :, 1) = gtRotateVolume(full_dm_vol(:, :, :, 1), rot_tensor);
    end

    dm_vol(:, :, :, 3) = gtCrop(full_dm_vol(:, :, :, 3), seg_vol_bb) .* seg_vol;
    dm_vol(:, :, :, 2) = gtCrop(full_dm_vol(:, :, :, 2), seg_vol_bb) .* seg_vol;
    dm_vol(:, :, :, 1) = gtCrop(full_dm_vol(:, :, :, 1), seg_vol_bb) .* seg_vol;

    slice_rod = gtVolumeGetSlice(dm_vol, conf.plane, conf.slice);
    slice_seg = gtVolumeGetSlice(seg_vol, conf.plane, conf.slice);
    r_vecs = reshape(slice_rod, [], 3);
    r_vecs = r_vecs(slice_seg(:), :);

    [cmap, ~, ~] = gtIPFCmap(cluster.phaseid, conf.plane_normal, ...
        'r_vectors', r_vecs, 'background', false);

    slice_R = zeros(size(slice_seg));
    slice_G = zeros(size(slice_seg));
    slice_B = zeros(size(slice_seg));

    slice_R(slice_seg(:)) = cmap(:, 1);
    slice_G(slice_seg(:)) = cmap(:, 2);
    slice_B(slice_seg(:)) = cmap(:, 3);

    int_vol = gtCrop(cluster_rec.ODF6D.intensity, seg_vol_bb) .* seg_vol;
    slice_int_vol = gtVolumeGetSlice(int_vol, conf.plane, conf.slice, false);
    slice_dm_vol = gtVolumeGetSlice(dm_vol, conf.plane, conf.slice, false);

    % needed for the high anglular boundaries
    [slice_kam, slice_gam] = gtDefComputeKernelAverageMisorientation(slice_dm_vol, slice_int_vol);
    slice_kam = squeeze(slice_kam);
    fprintf('Slice Grain Average Misorientation: %.3g°\n', slice_gam)

    %

    if (conf.flip_lr)
        slice_R = fliplr(slice_R);
        slice_G = fliplr(slice_G);
        slice_B = fliplr(slice_B);

        slice_seg = fliplr(slice_seg);
        slice_kam = fliplr(slice_kam);
    end
    if (conf.flip_ud)
        slice_R = flipud(slice_R);
        slice_G = flipud(slice_G);
        slice_B = flipud(slice_B);

        slice_seg = flipud(slice_seg);
        slice_kam = flipud(slice_kam);
    end
    if (conf.transpose)
        slice_R = permute(slice_R, [2 1 3]);
        slice_G = permute(slice_G, [2 1 3]);
        slice_B = permute(slice_B, [2 1 3]);

        slice_kam = permute(slice_kam, [2 1 3]);
        slice_seg = permute(slice_seg, [2 1 3]);
    end

    slice_img = cat(3, slice_R, slice_G, slice_B);

    slice_kam_skel = bwmorph(slice_kam > conf.kam_bound, 'skel', Inf);
    slice_kam_skel_high = slice_kam_skel & (slice_kam > conf.kam_bound*2);

    slice_skel = ones(size(slice_kam));
    slice_skel(slice_kam_skel) = 0.5;
    slice_skel(slice_kam_skel_high) = 0;

    if (~isempty(conf.tight))
        first_ind_1 = find(sum(slice_seg, 2), 1, 'first');
        last_ind_1 = find(sum(slice_seg, 2), 1, 'last');

        first_ind_2 = find(sum(slice_seg, 1), 1, 'first');
        last_ind_2 = find(sum(slice_seg, 1), 1, 'last');

        slice_img = slice_img(first_ind_1:last_ind_1, first_ind_2:last_ind_2, :);
        slice_skel = slice_skel(first_ind_1:last_ind_1, first_ind_2:last_ind_2);
        slice_seg = slice_seg(first_ind_1:last_ind_1, first_ind_2:last_ind_2);

        if (numel(conf.tight) == 1)
            conf.tight = conf.tight([1 1; 1 1]);
        elseif (size(conf.tight, 1) == 1)
            conf.tight = conf.tight([1 1], :);
        elseif (size(conf.tight, 2) == 1)
            conf.tight = conf.tight(:, [1 1]);
        end

        slice_img = padarray(slice_img, conf.tight(:, 1), 0, 'pre');
        slice_skel = padarray(slice_skel, conf.tight(:, 1), 1, 'pre');
        slice_seg = padarray(slice_seg, conf.tight(:, 1), false, 'pre');
        slice_img = padarray(slice_img, conf.tight(:, 2), 0, 'post');
        slice_skel = padarray(slice_skel, conf.tight(:, 2), 1, 'post');
        slice_seg = padarray(slice_seg, conf.tight(:, 2), false, 'post');
    end

    f = figure();
    ax = axes('parent', f);
    imagesc(slice_img, 'parent', ax, ...
        'AlphaDataMapping', 'scaled', 'AlphaData', slice_seg);
    hold(ax, 'on');
    imagesc(slice_skel(:, :, [1 1 1]), 'parent', ax, ...
        'AlphaDataMapping', 'scaled', 'AlphaData', slice_skel < 1);
    hold(ax, 'off');
    colormap(ax, flipud(gray))

    img_size = [size(slice_seg, 2), size(slice_seg, 1)];

    if (~isempty(conf.extras))
        if (isempty(conf.pixel_size))
            p = gtLoadParameters();
            conf.pixel_size = p.recgeo.voxsize * 1e3;
        end
        im_props = struct( ...
            'pixel_size', conf.pixel_size, ...
            'size', img_size );
        gtFigureAddExtras(ax, im_props, conf.extras);
    end

    gtFigureSetSize(f, ax, conf, img_size);

    drawnow();

    if (~isempty(conf.filename))
        gtFigureSaveToFile(f, conf.filename);
    end
end

