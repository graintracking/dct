function val = gtIndexAllGrainValues(grain, field1, field2, ind1, ind2, matform)
% val = gtIndexAllGrainValues(grain, field1, field2, ind1, ind2, matform)
% ------------------------------------------------------------------------
% Returns a column vector containing the values with indices (ind1,ind2) in 
% grain.field1.field2 from all the grains in 'grain'. 
% 
% INPUT
%   grain   - all grain data (cell vector)
%   field1  - field 1 name (string)
%   field2  - field 2 name (string) or empty if not applicable
%   ind1    - row index 1 of variable (1x1)
%   ind2    - column index 2 of variable or empty if not applicable (1xn)
%   matform - output in matrix form (does not work in all cases)
% 
% OUTPUT
%   val     - cell column array; size(m,n) where m is the number of grains; each
%             element has the format as the original grain data, if ind1 and ind2 are
%             empties
%  
% EXAMPLE
%   gtIndexAllGrainValues(grain, 'nof_pairs', [], 1, [])
%   gtIndexAllGrainValues(grain, 'R_vector', [], 1, 1:3)
%   gtIndexAllGrainValues(grain, 'indST', [], 3, 1:3)
%   gtIndexAllGrainValues(grain, 'stat', 'dangmean', 1, [])
%   gtIndexAllGrainValues(grain, 'fsim', [], 1, []) % for structure and cell arrays also
%                                                   % works
%


nof_grains = length(grain);

if ~exist('field2','var')
    field2 = '';
end
if ~exist('ind1','var')
    ind1 = [];
end
if ~exist('ind2','var')
    ind2 = [];
end

if ~exist('matform','var') || isempty(matform)
    matform = true;
end


if isstruct(grain{1}.(field1)) && ~isempty(ind1)
    tmp = cellfun(@(num) num.(field1)(ind1), grain, 'UniformOutput', false)';
    grain = tmp;
    ind1 = ind2;
    ind2 = [];
    field1 = field2;
    field2 = [];
    clear tmp
end

if isempty(ind1) && isempty(ind2)
    form = 0;   % all values will be concatenated
elseif isempty(ind2)
    form = 1;   % parameter is a one dimensional vector
else
    form = 2;   % parameter is a two dimensional array
end


% If there is no second field 
if isempty(field2)
    
    if (form == 2)
        
        if islogical(grain{1}.(field1))
            val = false(nof_grains, length(ind2));          
        elseif isstruct(grain{1}.(field1))
            val = struct([]);
        elseif iscell(grain{1}.(field1))
            val = cell(nof_grains, length(ind2));
        else
            val = zeros(nof_grains, length(ind2), class(grain{1}.(field1)));
        end
        val = cellfun(@(num) num.(field1)(ind1,ind2), grain, 'UniformOutput', false)';
        
    elseif (form == 1)
        
        if islogical(grain{1}.(field1))
            val = false(nof_grains, 1);
        elseif isstruct(grain{1}.(field1))
            val = struct([]);
        elseif iscell(grain{1}.(field1))
            val = cell(nof_grains, 1);
        else
            val = zeros(nof_grains, 1, class(grain{1}.(field1)));
        end
        val = cellfun(@(num) num.(field1)(ind1), grain, 'UniformOutput', false)';
        
    else
        val = cellfun(@(num) num.(field1), grain, 'UniformOutput', false)';
    end
    
% If there is a second field
else
    
    if (form == 2)

        if islogical(grain{1}.(field1).(field2))
            val = false(nof_grains, length(ind2));          
        elseif isstruct(grain{1}.(field1))
            val = struct([]);
        elseif iscell(grain{1}.(field1))
            val = cell(nof_grains, length(ind2));
        else
            val = zeros(nof_grains, length(ind2), class(grain{1}.(field1).(field2)));
        end
        val = cellfun(@(num) num.(field1).(field2)(ind1,ind2), grain, 'UniformOutput', false)';
       
    elseif (form == 1)
        
        if islogical(grain{1}.(field1).(field2))
            val = false(nof_grains, 1);         
        elseif isstruct(grain{1}.(field1))
            val = struct([]);
        elseif iscell(grain{1}.(field1))
            val = cell(nof_grains, 1);
        else
            val = zeros(nof_grains, 1, class(grain{1}.(field1).(field2)));
        end
        val = cellfun(@(num) num.(field1).(field2)(ind1), grain, 'UniformOutput', false)';
        
    else
      
        val = cellfun(@(num) num.(field1).(field2), grain, 'UniformOutput', false)';

    end
    
end


if matform
    % !!! This does not work for fields with e.g: size 3xn, where n is 
    % different for each val{ii}: 
    val = reshape([val{:}],size(val{1},2),[])';
end




end  % of function
