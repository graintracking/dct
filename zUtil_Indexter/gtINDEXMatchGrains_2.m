function output = gtINDEXMatchGrains_2(grain1, grain2, ph1, dc, drot, tol, px1, px2, printflag)
% GTINDEXMATCHGRAINS  Matches multiple indexed grains between two datasets;
%                     can also be used to find unmerged grains.
%
% output = gtINDEXMatchGrains_2(grain1, grain2, [ph1], [dc], [drot], [tol], px1, px2, [printflag])
% ------------------------------------------------------------------------------------------------
% It needs to be launched from the folder belonging to grain1.
% Crystallography info is loaded from parameters.mat in the current folder
% for phase 'ph1'.
%
% INPUT:
%   grain1    = <cell>       indexed grain data of reference set as output from Indexter
%                            {if undefined, it is loaded for the current dataset}
%   grain2    = <cell>       indexed grain data to be matched as output from Indexter
%                            {if undefined, it is same as grain1}
%   ph1       = <double>     phase ID in grain1 {1}
%   dc        = <double>     global translation (of the origin) between datasets from
%                            a position given in grain2 to the same in grain1 {[0 0 0]}
%   drot      = <double>     global rotation around the origin of grain2; the rotation
%                            matrix that transforms a position vector given in grain2
%                            into grain1 (size 3x3 for column vectors); {eye(3)}
%                            --> p1 = drot*p2 + dc;
%   tol       = <struct>     tolerances for finding matching grains and their default values
%                                tol.distf    = distance between center of mass {0.5}
%                                               mult. factor of smaller
%                                               bbox size of actual grain (gr1)
%                                               toldist = tol.distf * min(bbxs, bbys)
%                                tol.Rdist    = absolute distance in Rodrigues space {0.02}
%                                tol.bbxs     = max. bbox size x ratio {2}
%                                tol.bbys     = max. bbox size y ratio {2}
%                                tol.int      = max. intensity ratio {1e10}
%                                tol.absolute = absolute threshold (logical) {false}
%                                tol.stacked  = true if volumes to compare {false}
%                                               are part of a stack (so the Z should be
%                                               free)
%                                tol.pixels   = true if want centers in {false}
%                                               pixels instead of microns
%                                tol.orig     = true if using original center values {false}
%   px1       = <double>     pixel size for dataset 1 (mm/pixel)
%   px2       = <double>     pixel size for dataset 2 (mm/pixel)
%   printflag = <logical>    flag to print {false}
%
% OUTPUT:
% output = <struct> with fields
%       .gr1         = grain structure with R_vectors moved to the fundamental zone
%       .gr2         = grain structure with R_vectors moved to the fundamental zone
%       .match       = array of indices of matching grains [grain_ind1 grain_ind2]
%       .conflicts   = list of conflicts found (same as printed on command line);
%                      conflicts{:, 1}: grains from set1 for which more than one
%                                      possible matches were found
%                      conflicts{:, 2}: corresponding possible matches from set2
%                      conflicts{:, 3}: true ID for grains
%       .dcm         = updated guess for dc
%       .drotm       = updated guess for drot
%       .dev         = vectors of deviations of grain properties
%       .tol         = tolerances used in the matching
%       .ids         = list of matched ids
%       .missing1    = NOT matched grains from dataset 1
%       .missing2    = NOT matched grains from dataset 2
%
% See gtINDEXMatchGrains for algorithm and more information.
%
%   USAGE:
%     Revise tolerances, then run:
%       output = gtINDEXMatchGrains_2(grain1, grain2, 1, [], [], tol, px1, px2, false);
%     or first do:
%       output = gtINDEXMatchGrains_2(grain1, grain2, 1, [], [], [], px1, px2, false);
%     and then:
%       tol = output.tol;
%       output = gtINDEXMatchGrains_2(grain1, grain2, 1, [], [], tol, px1, px2, false);
%
%
%   Version 004 21-11-2013 by LNervo
%     Use microns for distances, pxsize, bbox
%
%   Version 003 03-06-2013 by LNervo
%     Handled different pixel size between datasets
%     Used bounding boxes in mm and not in pixels


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Initialize
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if (~exist('printflag', 'var') || isempty(printflag))
    printflag = false;
end

out = GtConditionalOutput(printflag);

if (~exist('ph1', 'var') || isempty(ph1))
    ph1 = 1;
end
if (~exist('dc', 'var') || isempty(dc))
    dc = [0 0 0];
end
if (~exist('drot', 'var') || isempty(drot))
    drot = eye(3); % for column vectors ! for row vectors, use transpose!
end

if (~exist('grain1', 'var') || isempty(grain1))
    fname = fullfile('4_grains', sprintf('phase_%02d', ph1), 'index.mat');
    out.fprintf('Loading grain info from %s ...\n', fname)
    grain1 = load(fname);
    grain1 = grain1.grain;
end
if (~exist('grain2', 'var') || isempty(grain2))
    grain2 = grain1;
end


if (~exist('tol', 'var') || isempty(tol))
    tol.distf    = 0.5;          % 0.5 = 50%
    tol.Rdist    = 0.02;         % 0.02 -> 2*atand(0.02) = 2.29 deg
    tol.bbxs     = 2;
    tol.bbys     = 2;
    tol.int      = 1e10;
    tol.absolute = false;
    tol.stacked  = false;
    tol.pixels   = false;
    tol.orig     = false;
end

nof_grains1 = length(grain1);
nof_grains2 = length(grain2);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Prepare grain data
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

out.odisp('Loading crystallographic info from parameters.mat ...')
parameters = gtLoadParameters();
labgeo = parameters.labgeo;
if (~isfield(labgeo, 'omstep') || isempty(labgeo.omstep))
    labgeo.omstep = gtAcqGetOmegaStep(parameters);
end
if (~isfield(parameters, 'detgeo') || isempty(parameters.detgeo))
    parameters.detgeo = gtGeoConvertLegacyLabgeo2Detgeo(labgeo);
end
detgeo = parameters.detgeo;
cryst = parameters.cryst(ph1);

if (~exist('px1', 'var') || isempty(px1))
    px1 = (detgeo.pixelsizeu + detgeo.pixelsizev) / 2;
    disp(['pixelsize loaded from parameters: ' num2str(px1)])
end
if (~exist('px2', 'var') || isempty(px2))
    px2 = px1;
end

match      = NaN(nof_grains1, 2);
match(:, 1) = (1:nof_grains1)';
if (tol.absolute)
    if tol.distf < 1
        tol.distf = tol.distf*1000; % um
    end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Grains centers
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

gr1 = sfGetGrainInfo(grain1, px1, tol, out);

gr2 = sfGetGrainInfo(grain2, px2, tol, out);

% Grain2 grain centers
% Rotate and translate grain centers
gr2.center = gr2.center*drot' + dc(ones(nof_grains2, 1), :);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Grains rodrigues vectors
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fzone_acc = gtCrystRodriguesFundZone(cryst.spacegroup, 0);
fzone_ext = gtCrystRodriguesFundZone(cryst.spacegroup, tol.Rdist);

gr1 = sfGetRodriguesCoordinates(gr1, nof_grains1, drot, cryst, tol, fzone_acc, fzone_ext, out);

gr2 = sfGetRodriguesCoordinates(gr2, nof_grains2, drot, cryst, tol, fzone_acc, fzone_ext, out);


% flip ud Z coordinates if the volumes are part of a stack
if (tol.stacked)
    gr2.center(:, 3) = (parameters.labgeo.samenvtop-parameters.labgeo.samenvbot)/2-gr2.center(:, 3);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Match grains
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

out.odisp('Matching grains ...')

out.odisp('Tolerances used:')
print_structure(tol, 'tol', false, printflag);

conflicts = cell(0, 3);

for ii = 1:nof_grains1

    % Actual grain data
    act = sfSelectGrains(ii, gr1);


    % Get candidate grain indices to be matched with the actual grain
    tomatch = sfCheckMatching(act, gr2, tol, fzone_acc, fzone_ext);

    if ~isempty(tomatch)

        nof_matches = length(tomatch);

        if nof_matches > 1

            if (tol.absolute)
                toldist    = tol.distf;
            else
                toldist    = min(act.bbxs, act.bbys) * tol.distf;
            end
            
            dcenter    = sum((act.center(ones(nof_matches, 1), :)   - gr2.center(tomatch, :)).^2, 2)   / (toldist^2);
            dR_vector  = sum((act.R_vector(ones(nof_matches, 1), :) - gr2.R_vector(tomatch, :)).^2, 2) / (tol.Rdist^2);
            dbbxs      = sum((act.bbxs - gr2.bbxs(tomatch, :)).^2, 2)/(act.bbxs^2) / (tol.bbxs^2);
            dbbys      = sum((act.bbys - gr2.bbys(tomatch, :)).^2, 2)/(act.bbys^2) / (tol.bbys^2);
            dint       = sum((act.int  - gr2.int(tomatch, :)).^2, 2) / (tol.int^2);

            dsum = dcenter + dR_vector + dbbxs + dbbys + dint;

            sortM      = [];
            sortM(:, 1) = dsum;
            sortM(:, 2) = tomatch;
            sortM(:, 3) = gr2.ind(tomatch);

            % Find best candidate by sorting
            sortM = sortrows(sortM, 1);

            tomatch = sortM(:, 2);

            out.odisp(['Multiple matches found for grain #' num2str(ii) ':'])
            out.odisp(tomatch)

            conflicts{end+1, 1} = ii;
            conflicts{end, 2}   = tomatch';% was sort(tomatch)'
            conflicts{end, 3}   = sortM';

        end % if nof_matches > 1

        match(ii, 2) = tomatch(1);

    end % if ~isempty(tomatch)

end % for ii = 1:nof_grains1

% delete match for conflicts - conflicts grains remain into 'conflicts' cell
% structure only - it could be whatever you want
match([conflicts{:, 1}], 2)=NaN;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Evalute grain matching
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

dev.xyz   = NaN(nof_grains1, 3);
dev.Rvec  = NaN(nof_grains1, 3);
dev.dist  = NaN(nof_grains1, 1);
dev.Rdist = NaN(nof_grains1, 1);
dev.int   = NaN(nof_grains1, 1);
dev.bbys  = NaN(nof_grains1, 1);
dev.bbxs  = NaN(nof_grains1, 1);
dev.rot   = NaN(3, 3, nof_grains1);

for ii = 1:size(match, 1)
    jj = match(ii, 2);
    if ~isnan(jj)
        dev.xyz(ii, :)   = gr2.center(jj, :) - gr1.center(ii, :);
        dev.dist(ii, :)  = sqrt(dev.xyz(ii, :) * dev.xyz(ii, :)');
        dev.Rvec(ii, :)  = gr2.R_vector(jj, :) - gr1.R_vector(ii, :);
        dev.Rdist(ii, :) = sqrt(dev.Rvec(ii, :) * dev.Rvec(ii, :)');
        dev.bbxs(ii, 1)  = gr2.bbxs(jj) / gr1.bbxs(ii);
        dev.bbys(ii, 1)  = gr2.bbys(jj) / gr1.bbys(ii);
        dev.int(ii, 1)   = gr2.int(jj)  / gr1.int(ii);
        dev.rot(:, :, ii) = gr1.rot(:, :, ii).' - gr2.rot(:, :, jj); % was all_g1(:, :, ii).' - gr2.rot(:, :, jj);
    end
end

% matching indexes
ok = ~isnan(dev.dist(:, 1));

Rvec_med = median(dev.Rvec(ok, :), 1);
xyz_med  = median(dev.xyz(ok, :));
ddist    = sqrt(sum(dev.dist(ok, :).^2, 1)/sum(ok));
dRdist   = sqrt(sum(dev.Rdist(ok, :).^2, 1)/sum(ok));
dcm      = dc - xyz_med;
out.fprintf('\nRemaining median deviation of Rodrigues vectors:\n  [%g %g %g]', Rvec_med);
out.fprintf('\nRemaining root mean square deviation of Rodrigues vectors:\n  %g', dRdist);
out.fprintf('\nRemaining root mean square deviation of grain center distances:\n  %g', ddist);
out.fprintf('\nRemaining median deviation of grain centers:\n  [%g %g %g]', xyz_med);
out.fprintf('\n\nRecommended total displacement correction (dc):\n  [%g %g %g]\n', dcm);

rot_med = gtMathsRod2OriMat(Rvec_med');
if isempty(drot)
    drotm = rot_med;
else
    drotm = rot_med*drot;
end
out.odisp('Recommended total rotation correction (drot):');
out.odisp(drotm);

totmatched = sum(ok);
out.fprintf('Number of matching grains found:\n  %d \n\n', totmatched);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% build output
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if (tol.absolute)
    if tol.distf > 1
        tol.distf = tol.distf/1000; % mm
    end
end


% flip ud Z coordinates if the volumes are part of a stack
if (tol.stacked)
    gr2.center(:, 3) = (parameters.labgeo.samenvtop-parameters.labgeo.samenvbot)/2-gr2.center(:, 3);
end

if (tol.pixels)
    gr1.center = gr1.center ./ gr1.pixsize;
    gr2.center = gr2.center ./ gr2.pixsize;
end

% store information about the grains for the two datasets (with R-vectors
% moved into the fundamental zone) and extra info

output.match     = match;
output.conflicts = conflicts;
output.dcm       = dcm;
output.drotm     = drotm;
output.dev       = dev;
output.tol       = tol;
output.ids       = match(ok, :);
output.missing1  = setdiff(1:length(grain1), output.ids(:, 1));
output.missing2  = setdiff(1:length(grain2), output.ids(:, 2));
output.gr1       = gr1;
output.gr2       = gr2;
%output.ok        = ok;
%output.tot1      = length(grain1);
%output.tot2      = length(grain2);
output.u_gIDs1   = unique(output.ids(:, 1), 'stable')';
output.u_gIDs2   = unique(output.ids(:, 2), 'stable')';

end % end of function

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SUB-FUNCTIONS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function tomatch = sfCheckMatching(act, cand, tol, fzone_acc, fzone_ext)

    tomatch = (1:length(cand.ind))';  % initial indices of grain candidates to be merged

    % Delete those indices that don't meet the following constraints:

    % Distance of centers close enough?
    if (tol.absolute)
        toldist    = tol.distf;
    else
        toldist    = min(act.bbxs, act.bbys) * tol.distf;
    end

    dvec    = repmat(act.center, length(cand.ind), 1) - cand.center;
    dist    = sqrt(sum(dvec.*dvec, 2));
    cons    = dist < toldist;
    cand    = sfSelectGrains(cons, cand);
    tomatch(~cons) = [];

    % Bounding box X size close enough?
    cons = (cand.bbxs > act.bbxs/tol.bbxs) & ...
           (cand.bbxs < tol.bbxs*act.bbxs);
    cand = sfSelectGrains(cons, cand);
    tomatch(~cons) = [];

    % Bounding box Y size close enough?
    cons = (cand.bbys > act.bbys/tol.bbys) & ...
           (cand.bbys < tol.bbys*act.bbys);
    cand = sfSelectGrains(cons, cand);
    tomatch(~cons) = [];

    % Intensity close enough?
    cons = (cand.int > act.int/tol.int) & ...
           (cand.int < tol.int*act.int);
    cand = sfSelectGrains(cons, cand);
    tomatch(~cons) = [];

    if isempty(tomatch)
        return;
    end

    % Rodriguez vector close enough?

    % If any of them is on edge of fundamental zone
    if any([act.R_onedge; cand.R_onedge])

        % Do Rodrigues test from scratch

        Rlines = vertcat(act.Rlines{:}, cand.Rlines{:});
        Rids   = vertcat(act.Rids{:}, cand.Rids{:});

        candinds = zeros(length(act.Rids{:}), 1);
        for jj = 1:length(tomatch)
            candinds = [candinds; jj(ones(length(cand.Rids{jj}), 1))];
        end

        % Find new Rodrigues vector
        [~, ~, goodlines] = gtCrystRodriguesTestCore(Rlines, Rids, ...
                               fzone_acc, fzone_ext, tol.Rdist);


        % Check which candidate has all of its 3 reflections accepted
        candinds = candinds(goodlines);
        candinds(candinds==0) = [];

        unicandinds = unique(candinds);

        okinds = false(size(tomatch));

        for jj = 1:length(unicandinds)
            if (sum(candinds==unicandinds(jj)) == 3)
                okinds(unicandinds(jj)) = true;
            end
        end

        tomatch = tomatch(okinds);

    else
        dRvec = repmat(act.R_vector, length(cand.ind), 1) - cand.R_vector;
        Rdist = sqrt(sum(dRvec.*dRvec, 2));
        cons  = Rdist < tol.Rdist;
        tomatch(~cons) = [];
    end

end % end function sfCheckMatching
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function grk = sfSelectGrains(keeplist, gr)

    grk.ind      = gr.ind(keeplist);
    grk.center   = gr.center(keeplist, :);
    grk.R_vector = gr.R_vector(keeplist, :);
    grk.R_onedge = gr.R_onedge(keeplist, :);
    grk.bbys     = gr.bbys(keeplist);
    grk.bbxs     = gr.bbxs(keeplist);
    grk.int      = gr.int(keeplist);
    grk.Rlines   = gr.Rlines(keeplist);
    grk.Rids     = gr.Rids(keeplist);

end % end function sfSelectGrains
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function gr = sfGetRodriguesCoordinates(gr, nof_grains, drot, cryst, tol, fzone_acc, fzone_ext, out)

% Extension to Rodrigues fundamental zone
% Rext = 0;
Rext = tol.Rdist;

% Calculate Rodrigues lines inside the fundamental zone
gr.Rlines = cell(nof_grains, 1);
gr.Rids   = cell(nof_grains, 1);
gr.Rvec   = gr.R_vector;

% All orientation matrices in dataset
all_g = gtMathsRod2OriMat(gr.R_vector.');

if (any(gr.R_onedge) || ~isempty(drot))

    Bmat = gtCrystHKL2CartesianMatrix(cryst.latticepar);
   
    if (isfield(cryst, 'hklsp') && ~isempty(cryst.hklsp))

        % Take the first three plane normals
        hklsp = cryst.hklsp(:, 1:3);

        % All signed hkl-s for the given family
        shkl1 = cryst.hklsp(:, cryst.thetatypesp == cryst.thetatypesp(1));
        shkl2 = cryst.hklsp(:, cryst.thetatypesp == cryst.thetatypesp(2));
        shkl3 = cryst.hklsp(:, cryst.thetatypesp == cryst.thetatypesp(3));

        % Both hkl and -h-k-l have to be considered:
        shkl1 = [shkl1, -shkl1];
        shkl2 = [shkl2, -shkl2];
        shkl3 = [shkl3, -shkl3];

    else
        % Take the {1, 0, 0} planes (doesn't matter if reflections really exist)
        if size(cryst.hkl, 1) == 3
            hklsp = [1 0 0; 0 1 0; 0 0 1]';
        elseif size(cryst.hkl, 1) == 4
            hklsp = [1 0 -1 0; 0 1 -1 0; 0 0 0 1]';
        else
            gtError('gtINDEXMatchGrains_2:wrongParametersField', 'Field parameters.cryst.hkl is not set correctly.')
        end
        symm = gtCrystGetSymmetryOperators([], cryst.spacegroup);
        % All signed hkl-s for the given family (both hkl and -h-k-l)
        shkl1 = gtCrystSignedHKLs(hklsp(:, 1)', symm)';
        shkl2 = gtCrystSignedHKLs(hklsp(:, 2)', symm)';
        shkl3 = gtCrystSignedHKLs(hklsp(:, 3)', symm)';

    end

    % Plane normals with Cartesian Crystal coordinates
    pl_cry = gtCrystHKL2Cartesian(hklsp, Bmat);  % takes and returns column vectors

    out.odisp('Calculating Rodrigues coordinates ...');

    % Generate a prime number larger than 3 for each grain to be used to
    % generate distinct reflection id-s later
    pr = primes(1e6);
    pr(1:2) = [];
    pr(nof_grains+1:end) = [];

    % Plane normals in the Sample ref. of dataset1
    all_pl_sam = gtVectorCryst2Lab(pl_cry.', all_g);

    for ii = 1:nof_grains

        % Plane normals in the Sample ref. of dataset1
        pl_sam = all_pl_sam(:, :, ii);

        % Lines in Rodrigues space (some might be empty)
        Rline1 = gtCrystRodriguesVector(pl_sam(1, :), shkl1', Bmat, fzone_ext);
        Rline2 = gtCrystRodriguesVector(pl_sam(2, :), shkl2', Bmat, fzone_ext);
        Rline3 = gtCrystRodriguesVector(pl_sam(3, :), shkl3', Bmat, fzone_ext);

        gr.Rlines{ii} = [Rline1; Rline2; Rline3];

        % Vector of line indices to identify which plane they belong to
        gr.Rids{ii} = [1*ones(size(Rline1, 1), 1); 2*ones(size(Rline2, 1), 1); 3*ones(size(Rline3, 1), 1)];

        % For debugging: find Rodrigues vector
        [Rvec, ~, ~, ~, Ronedge] = gtCrystRodriguesTestCore(gr.Rlines{ii}, gr.Rids{ii}, fzone_acc, fzone_ext, Rext);

        if isempty(Rvec)
            out.odisp('WARNING! Rodrigues vector could not be fitted. Possible bug.')
            gr.R_vector(ii, :) = NaN;
            gr.rot(:, :, ii)  = NaN;
        else
            gr.R_vector(ii, :) = Rvec;
            gr.rot(:, :, ii)  = all_g(:, :, ii);
        end
        gr.R_onedge(ii) = Ronedge;

        % For debugging:
        %   Rvec = gtCrystRodriguesTest(pl_sam1', [shkl1(:, 1), shkl2(:, 1), shkl3(:, 1)]', spacegroup, Bmat, 0, 0, 0);
    end
else
    gr.rot = all_g;
end

end % end function sfGetRodriguesCoordinates 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function gr = sfGetGrainInfo(grain, pxsize, tol, out)

nof_grains = length(grain);
% Load relevant info into the gr1 structure (vectors and matrices):
%gr.id = gtIndexAllGrainValues(grain, 'id', [], 1);
gr.ind = (1:nof_grains)';

if (tol.orig) && isfield(grain, 'orig')
    gr.center   = gtIndexAllGrainValues(grain, 'orig', 'center', 1, 1:3)*1000; %um
    gr.R_vector = gtIndexAllGrainValues(grain, 'orig', 'R_vector', 1, 1:3);
    out.odisp('Using original values for ''center'' and ''R_vector'' for the current grain')
else
    gr.center   = gtIndexAllGrainValues(grain, 'center', [], 1, 1:3)*1000; %um
    gr.R_vector = gtIndexAllGrainValues(grain, 'R_vector', [], 1, 1:3);
end
gr.R_onedge(:, 1) = gtIndexAllGrainValues(grain, 'R_onedge', [], 1, 1);

try
    gr.bbxs(:, 1) = gtIndexAllGrainValues(grain, 'stat', 'size_int', 1, 1); %pixels
    gr.bbys(:, 1) = gr.bbxs(:, 1); %pixels
    out.odisp('Using ''size_int'' values from stat for the current grain')
    out.odisp('size_int = 2*(intmean/sum_intmean*volSample).^(1/3); %mm')
catch mexc
    gtPrintException(mexc)
    gr.bbxs(:, 1) = gtIndexAllGrainValues(grain, 'stat', 'bbxsmean', 1, 1); %pixels
    gr.bbys(:, 1) = gtIndexAllGrainValues(grain, 'stat', 'bbysmean', 1, 1); %pixels
    out.odisp('Field ''size_int'' is not stored in the grain.stat structure.')
end
gr.int(:, 1) = gtIndexAllGrainValues(grain, 'stat', 'intmean', 1, 1);
gr.pixsize = pxsize*1000; %um

% convert grain sizes in microns
gr.bbxs = gr.bbxs .* gr.pixsize; %um
gr.bbys = gr.bbys .* gr.pixsize; %um

end % end function sfGetGrainInfo
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
