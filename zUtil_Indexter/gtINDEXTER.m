function grain = gtINDEXTER(phaseID, flag_update, flag_inputfile, flag_log, ...
                    forcemerge, parameters)
% GTINDEXTER Grain indexing algorithm Indexter main function.
%
% gtINDEXTER(phaseID, flag_update, flag_inputfile, flag_log, ...
%            forcemerge, parameters)
%
% --------------------------------------------------------------
%
% Main user function for the Friedel pair indexing algorithm gtIndexter.
% It loads the Friedel pair data from the Spotpair table in the 
% database or an input file. Results (grain data and statistics) are 
% written in an output file. Indexing results are updated in the Spotpair 
% table. A log file is also saved. 
%
% OPTIONAL INPUT
%
%   Friedel pair data is either in Spotpair table (use flag_inputfile = 0) 
%   or in an input file (use flag_inputfile = 1). An input file containing 
%   processed reflection data for indexing is (re)created when 
%   flag_inputfile=0 is used. The input file may also be created by a 
%   separate function before running indexing. Using an input file gives 
%   a speed up, so its use is recommended once it has been created.
%   The path for the input file is:
%    4_grains/phase_##/index_input.mat
%
%   Indexing and setup parameters are read from the local parameters file.
%   
%   phaseID        - phase ID to be indexed; (default is 1)
%   flag_update    - database spotpair table will be updated with results
%                    and output file 'index.mat' will be saved when true; 
%                    (default is false)
%   flag_inputfile - uses input file instead of database when true; 
%                    (default is false)
%   flag_log       - a log file and output file with current date will be
%                    saved when true (default is true); results are always
%                    logged when flag_update is true;
%   forcemerge     - if true, indexing will not run, only merging of 
%                    grains defined in parameters.index.forcemerge;
%                    this should contain a list of grain ID-s to be merged 
%                    in a cell array, for example:
%                      parameters.index.forcemerge{1} = [5 7];
%                      parameters.index.forcemerge{2} = [20 26 29];
%                      parameters.index.forcemerge{3} = [55 77];
%   parameters     - as in the parameters file
%  
%
% OUTPUT
%  
%   Results are saved under two different names (files are identical):
%     4_grains/phase_##/index.mat                  (if flag_update=1)
%     4_grains/phase_##/index_DD-MMM-YYYY_N.mat    (if flag_log=1)
%   Log file is saved as:
%     4_grains/phase_##/index_DD-MMM-YYYY_N.log    (if flag_log=1)
%   
%   Field 'grainID' in Spotpair table in the database is updated if 
%   flag_update is true.
%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Initialize
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if ~exist('phaseID','var')
    phaseID = 1;
end

if ~exist('flag_update','var')
    flag_update = false;
end

if ~exist('flag_inputfile','var')
    flag_inputfile = false;
end

if ~exist('flag_log','var')
    flag_log = true;
end

if ~exist('forcemerge','var')
    forcemerge = false;
end

if ~exist('grain','var')
    grain = [];
end

if flag_update
    flag_log = true;
end


% File names for output; start log file
fname0        = sprintf('4_grains/phase_%02d/index.mat',phaseID);
[fname1,fnmb] = gtLastFileName(sprintf('4_grains/phase_%02d/index_%s_',...
                phaseID,date),'new');
logname = sprintf('4_grains/phase_%02d/index_%s_%d.log',phaseID,date,fnmb);

% Start recording log text
if flag_log
    diary(logname)
end


disp(' ')
disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
disp('%%  gtINDEXTER  - indexing of Friedel pairs  %%')
disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
fprintf('\nDate:\n %s \n',datestr(now))
fprintf('\nWorking directory:\n %s\n',pwd)
fprintf('\nOutput mat file names:\n %s\n %s\n',fname0,fname1)
fprintf('\nOutput log file name:\n %s\n',logname)


if ~flag_update
	fprintf('\nWarning! Update flag is false.\n')
    fprintf('Pair table and output file will not be updated!\n')
end


% Start time of the indexing process
tStart = tic;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Indexing parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if ~exist('parameters','var')
    parameters = load('parameters.mat');
    parameters = parameters.parameters;
end

if isfield(parameters.index,'plotfigure')
    plotfigure = parameters.index.plotfigure;
else
    plotfigure = true;
end

if isfield(parameters,'index')
    if isfield(parameters.index,'strategy')
        strategy = parameters.index.strategy;
    else
        strategy = gtIndexDefaultStrategy;
        parameters.index.strategy = strategy;
        fprintf('\nCould not find indexing parameters in parameters file.\n')
        fprintf('\nUsing default values.\n')
    end
else
    strategy = gtIndexDefaultStrategy;
    parameters.index.strategy = strategy;
    parameters.index.discard  = [];
    fprintf('\nCould not find indexing parameters in parameters file.\n')
    fprintf('\nUsing default values.\n')
end


% Pair table name
if isfield(parameters.acq, 'pair_tablename')
    pairtable = parameters.acq.pair_tablename;
else % what should be standard convention
    pairtable = sprintf('%sspotpairs',parameters.acq.name);
end


% Irradiated sample volume in the Sample reference
samenv = gtGeoSamEnvInSampleRef(parameters.labgeo, parameters.samgeo);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Input data
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Reflection and crystallography data
if flag_inputfile
    
    % Load 'tot' and 'cryst' variables from input file
    fname = sprintf('4_grains/phase_%02d/index_input.mat', phaseID);
    load(fname)

    fprintf('\nUsing input data file:\n %s\n', fname)
    fprintf('\nName of dataset:\n %s\n', parameters.acq.name)
    fprintf('\nWorking on phase %02d (%s).\n', phaseID, ...
            parameters.cryst(phaseID).name)

else
    % Load pair data from database and create input file 
    [tot, cryst] = gtINDEXCreateInputData(phaseID, parameters);
end



if forcemerge
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Forced merging of grains
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    check = inputwdefault('Are you sure to force the merging of grains as indicated in parameters.index.forcemerge? [y/n]','y');
    
    if strcmpi(check,'y')
        list = parameters.index.forcemerge;

        [grain, restids1, strategy, cryst, tot] = gtIndexForceMergeGrains(fname0, list);
        restids2 = restids1;
        restids3 = restids1;
    end
   
else
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Iterative search based on tolerances
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    fprintf('\nStart indexing ...\n')
    
    fprintf('\nINDEXING PARAMETERS\n\n')
    fprintf(' Number of iterations: %d\n',parameters.index.strategy.iter)
    disp(' Build parameters first iteration:')
    disp(parameters.index.strategy.b.beg)
    disp(' Build parameters last iteration:')
    disp(parameters.index.strategy.b.end)
    disp(' Merge parameters first iteration:')
    disp(parameters.index.strategy.m.beg)
    disp(' Merge parameters last iteration:')
    disp(parameters.index.strategy.m.end)
    disp(' Singles fitting parameters:')
    disp(parameters.index.strategy.s)
    disp(' Parameters for marking outliers:')
    disp(parameters.index.strategy.x)
    disp(' ')
    
    
    % The core of the iterative indexing algorithm. Grains are found and
    % by this function.
    %   'grain' is a cell vector of all the grains found
    %   'restids1' is a vector of unindexed reflection id-s
    [grain, restids1] = gtIndexExecuteStrategy(strategy, tot, samenv, cryst, grain);
    
    if isempty(grain)
        fprintf('\n No grain has been found.\n')
        diary off
        return
    end
    
    
    % Calculate additional grain statistics
    grain = gtIndexGrainStatistics(grain);
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Statistical fitting of unindexed reflections
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    % No new grains will be created but the unindexed reflections will be
    % tested whether they fit well enough to the existing grains.
    
    if ~isempty(strategy.s.stdf) && (strategy.s.stdf > 0) && ~isempty(restids1)
        
        % If no unindexed reflections, then skip
            % The last angular tolerance will be used
            strategy.s.ang = strategy.b.end.ang;
            
            % Fit unindexed reflections to the grains
            [grain, restids2] = gtIndexStatisticalFit(grain, restids1, tot, ...
                strategy.s, cryst);          
        
    else
        restids2 = restids1;
    end
    
    % As a last step, exclude outlier reflections in grains, using grain
    % statistics.
    
    if ~isempty(strategy.x.stdf) && (strategy.x.stdf > 0)
        
        % The last angular tolerance will be used
        strategy.x.ang = strategy.b.end.ang;
        
        % Exclude outliers
        [grain, restids3] = gtIndexStatisticalExclusion(grain, tot, ...
            strategy.x, cryst);       
    else
        restids3 = restids2;
    end

end

% Update grain statistics
grain = gtIndexGrainStatistics(grain);


%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Create output 
%%%%%%%%%%%%%%%%%%%%%%%%%%%

fprintf('\n-----------------------------------')
fprintf('\n CREATING OUTPUT...\n\n')
tic

% Create final grain output: rearrange reflections in grains, write into
% database spotpair table
grain = gtIndexCreateOutput(grain, strategy.x, pairtable, phaseID, ...
                            cryst, flag_update);

% Determine unique planes
%disp(' ')
%disp('Unique plane normals cannot be calculated. Function needs updating...')
%grain = gtIndexUniquePlaneNormals(grain,ACM);
%unipl = gtIndexAllGrainValues(grain,'uni','nof_unipl',1);

% Grain statistics on all the grains found
[allgrainstat, outliers] = gtIndexAllGrainStat(grain, plotfigure);

% Estimate errors
%disp(' ')
%disp('Error estimation cannot be done. Function needs updating...')
%errest = gtEstimateErrors(grain);

% To be activated soon...
% Strain fitting (adds grain.strain field)
% Check available Optimization Toolbox License
% license_ok = license('checkout','Optimization_Toolbox');
% if license_ok
%     disp('Running average strain fitting for each grain...')
%     grain = gtStrainGrainsGeneral(grain, cryst, parameters.acq.energy, [], 3);
% else
%     disp('Not running strain fitting - problem with Otimization Toolbox license.')
% end


%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Print and save results
%%%%%%%%%%%%%%%%%%%%%%%%%%%


if ~flag_update
	fprintf('\nWarning! Update flag is false.\n')
    fprintf('Pair table and output file will not be updated!\n')
end


% Create output text

tt{1} = sprintf('Number of grains found: %5d\n',length(grain)) ;

tt{2} = sprintf('Pairs per grain average:  %0.2f\n', ...
        (length(tot.id)-length(restids3))/length(grain));
    
tt{3} = sprintf('Number of pairs ...\n') ;

tt{4} = sprintf('  indexed in total:                 + %5d\n', ...
        length(tot.id)-length(restids3)) ;

tt{5} = sprintf('    indexed in iterations:            %5d\n', ...
        length(tot.id)-length(restids1)) ;

tt{6} = sprintf('    fitted using grain statistics:    %5d\n', ...
        length(restids1)-length(restids2)) ;
    
tt{7} = sprintf('    excluded using grain statistics:  %5d\n', ...
        length(restids3)-length(restids2)) ;
    
tt{8} = sprintf('  unindexed:                        + %5d\n',length(restids3)) ;

tt{9} = sprintf('  input in total:                   = %5d\n',length(tot.id));

tt{10} = sprintf('Results saved in:  %s\n',fname1);

tt{11} = sprintf('Log file saved as: %s\n',logname);
    
summary = [tt{:}]; 


% Save results

disp(' ')
if flag_log
    save(fname1,'grain','restids1','restids2','restids3','tot','strategy',...
        'cryst','allgrainstat','outliers','summary');
    fprintf('\nFinal results have been saved in:\n %s\n', fname1)
    fprintf('\nLog file saved as:\n %s\n\n', logname)
end

if flag_update
    status = copyfile(fname1,fname0);
    if status
        fprintf('Final results have been saved in:\n %s\n',fname0);
    else
        fprintf('Could not save final results in:\n %s\n',fname0);
    end
end


toc

disp(' ')
disp([tt{1:9}])

    
tEnd = toc(tStart);
fprintf('Total elapsed time: %0.0f sec\n',tEnd)

diary off

end % end of function
