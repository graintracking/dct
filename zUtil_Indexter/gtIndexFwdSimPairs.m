function grain = gtIndexFwdSimPairs(grain, detgeo, labgeo, samgeo, drift, ...
                 latticepar, energy)
% GTINDEXFWDSIMPAIRS  Forward simulates spot positions on the detector
%                     for all spots indexed as Friedel pairs.
%
%     grain = gtIndexFwdSimPairs(grain, detgeo, labgeo, samgeo, drift, latticepar, energy)
%     ------------------------------------------------------------------------------------
%
%     INPUT:
%       grain      = <cell>       all the grains as in index.mat
%       detgeo     = <struct>     as in parameters.detgeo
%       labgeo     = <struct>     as in parameters.labgeo
%       samgeo     = <struct>     as in parameters.samgeo
%       drift      = <double>     sample drifts; if empty, no drifts are considered
%       latticepar = <double>     lattice parameters (1x6)
%       energy     = <double>     beam energy in keV
%
%     OUTPUT:
%       grain      = <cell>       updated grain info
%            .fsimA = <struct>     added field containing fwd simulation data for spots A
%            .fsimB = <struct>     added field containing fwd simulation data for spots B
%     or
%            .fsimU = <struct>     added field containing fwd simulation data for spots without
%                                  Friedel pairs info


%%%%%%%%%%%%%%%%%%%
% Prepare input
%%%%%%%%%%%%%%%%%%%

% B matrix for transformation from (hkl) to Cartesian Crystal reference
Bmat = gtCrystHKL2CartesianMatrix(latticepar);

% Rotation tensor components
rotcomp = gtMathsRotationMatrixComp(labgeo.rotdir', 'col');

% Rotation tensor describing crystal orientation; calculate all grains
% at once for speed-up.
% (gtCrystRodriguesVector and gtFedRod2RotTensor used together
% gives the transformation tensor from CRYSTAL TO SAMPLE coordinates)
Rvec    = gtIndexAllGrainValues(grain,'R_vector',[],1,1:3);
cry2sam = gtFedRod2RotTensor(Rvec');

checklist = [];

for ii = 1:length(grain)

    if ~isfield(grain{ii}, 'strain')
        grain{ii}.strain.strainT = NaN(3,3);
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Theoretical plane normals in deformed state
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % Coordinates of the plane normals in the Cartesian Crystal system
    pl_cry          = gtCrystHKL2Cartesian(grain{ii}.hklsp, Bmat);
    grain{ii}.plref = pl_cry;

    % Plane normals in the undrifted SAMPLE reference
    pl_sam = cry2sam(:,:,ii) * pl_cry;

    % Impose strain on grain
    if any(isnan(grain{ii}.strain.strainT(:)))
        %fprintf('No strain info in grain #%d. Using as measured plane normals.\n',ii)
        pl_samd = pl_sam;
        drel    = ones(1,size(pl_sam,2));
    else
        % New deformed plane normals and relative elongations
        [pl_samd, drel] = gtStrainPlaneNormals(pl_sam, grain{ii}.strain.strainT);
    end

    % D-spacings in the reference crystal for the given reflections
    dsp_ref = gtCrystDSpacing(grain{ii}.hkl, Bmat);

    % d-spacing after applying the strain tensor
    dsp_d = drel .* dsp_ref;

    grain{ii}.drel  = drel;
    grain{ii}.plsam = pl_samd;

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Apply rotational drift
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    if ~isempty(drift)
        % In case of drifts, need to calculate the omega angles for A and
        % B reflections separately.

        if isfield(grain{ii}, 'omegaA')
            grain{ii}.fsimA = gtIndexApplyDrifts(pl_samd, dsp_d, grain{ii}.center',...
                grain{ii}.omegaA, drift, detgeo, labgeo, samgeo, rotcomp,  energy);
        end
        if isfield(grain{ii}, 'omegaB')
            grain{ii}.fsimB = gtIndexApplyDrifts(pl_samd, dsp_d, grain{ii}.center',...
                grain{ii}.omegaB, drift, detgeo, labgeo, samgeo, rotcomp, energy);
        end
        if isfield(grain{ii}, 'omega') && ~isfield(grain{ii}, 'omegaA') && ~isfield(grain{ii}, 'omegaB')
            grain{ii}.fsimU = gtIndexApplyDrifts(pl_samd, dsp_d, grain{ii}.center',...
                grain{ii}.omega, drift, detgeo, labgeo, samgeo, rotcomp, energy);
        end

    else
        % In case of no drifts, the omega angles for reflections A and B
        % can be calculated together.

        % Bragg angles theta in the deformed state
        sinth_d = 0.5*gtConvEnergyToWavelength(energy)./dsp_d;

        % The plane normals need to be brought in the Lab reference where the
        % beam direction and rotation axis are defined.
        % Use the Sample -> Lab orientation transformation assuming omega=0;
        % (vector length preserved for free vectors)
        sam2lab = [samgeo.dirx; samgeo.diry; samgeo.dirz]';
        pld     = sam2lab*pl_samd;

        % Predict omega angles: 4 for each plane normal (the Friedel pairs are
        % om1-om3 and om2-om4)
        % Do it only once, for A and B at the same time.
        [om4, pl_lab4, ~, rot4, omind4] = gtFedPredictOmegaMultiple(...
            pld, sinth_d, labgeo.beamdir', labgeo.rotdir', rotcomp, []);

        % Check if all reflections occur
        noref = any(isnan(om4), 1);
        if any(noref)
            msg = sprintf('No reflection was predicted for some pairs in grain #%d', ii);
            disp(msg)
            checklist = [checklist; ii];
        end

        % Get fwd simulated data for reflections A and B.
        % Find the correct omega index for the pairs where the difference is
        % the smallest (shouldn't matter if grain.omega is the measured one
        % or the one corrected for the pair).
        if isfield(grain{ii}, 'omegaA')
            grain{ii}.fsimA = gtIndexFwdSim(grain{ii}.omegaA, grain{ii}.center', ...
                sinth_d, om4, pl_lab4, rot4, omind4, detgeo, labgeo, samgeo);
        end
        if isfield(grain{ii}, 'omegaB')
            grain{ii}.fsimB = gtIndexFwdSim(grain{ii}.omegaB, grain{ii}.center', ...
                sinth_d, om4, pl_lab4, rot4, omind4, detgeo, labgeo, samgeo);
        end
        if isfield(grain{ii}, 'omega') && ~isfield(grain{ii}, 'omegaA') && ~isfield(grain{ii}, 'omegaB')
            grain{ii}.fsimU = gtIndexFwdSim(grain{ii}.omega, grain{ii}.center', ...
                sinth_d, om4, pl_lab4, rot4, omind4, detgeo, labgeo, samgeo);
        end
    end
end

end % of main function
