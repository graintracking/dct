function grain = gtIndexUniqueReflections(grain, dspref, energy)
% GTINDEXUNIQUEREFLECTIONS Unique list of Friedel pairs for all grains
%                          Data is averaged when two Friedel pairs are indexed for a (hkl) plane
%
%     grain = gtIndexUniqueReflections(grain, dspref, energy)
%     -------------------------------------------------------
%
%     Creates a unique list of the indexed Friedel pairs in which each (hkl) 
%     plane is present maximum once using the pair equivalence info in 
%     grain{ii}.equilist.
%
%     INPUT:
%       grain     = <cell>       all grain data as in index.mat
%       dspref    = <double>     list of reference d-spacings for the {hkl} families as in 
%                                parameters.cryst(phaseid).dspacing
%       energy    = <double>     beam energy in keV
%
%     OUTPUT:
%       grain     = <cell>       updated grain data
%            .uni = <struct>     unique reflections list and (averaged) measured properties:
%                .pl       = <double>     plane normals
%                .plref    = <double>     plane normals in reference crystal (unstrained,
%                                         theoretical)
%                .hklind   = <double>     linear indices of the {hkl} families (=thetaype)
%                .theta    = <double>     theta Bragg angles in degrees
%                .elo      = <double>     elongations (stretch) along 'pl' (dimensionless, around 1)
%                .dang     = <double>     angular deviations in degrees (between two Friedel pairs) 
%                .dth      = <double>     theta deviations in degrees (between two Friedel pairs) 
%                .delo     = <double>     elongation deviations (between two Friedel pairs) 

lambda = gtConvEnergyToWavelength(energy);

for ii = 1:length(grain)
     
    uni.pl     = []; 
    uni.plref  = [];
    uni.hklind = [];
    uni.theta  = [];
    uni.elo    = [];
    uni.dang   = [];
    uni.dth    = [];
    uni.delo   = [];
    
    
    if ~isempty(grain{ii}.equilist)

        i1 = grain{ii}.equilist(:,1);
        i2 = grain{ii}.equilist(:,2);
        
        spl = sign(sum(grain{ii}.pl(:,i1) .* grain{ii}.pl(:,i2), 1));
        pl  = 0.5*(grain{ii}.pl(:,i1) + spl([1 1 1],:).*grain{ii}.pl(:,i2));
        pln = sqrt(sum(pl.^2,1));
        uni.pl = pl./pln([1 1 1],:);
       
        uni.plref  = grain{ii}.plref(:,i1);
        uni.hklind = grain{ii}.hklind(i1);
        
        uni.theta = 0.5*(grain{ii}.theta(i1) + grain{ii}.theta(i2));
       
        uni.elo  = (0.5*lambda./sind(uni.theta))./dspref(grain{ii}.hklind(i1));
              
        uni.dang = gtMathsVectorsAnglesDeg(grain{ii}.pl(:,i1), ...
                                           grain{ii}.pl(:,i2), 1);
        
        %uni.dom  = (grain{ii}.centimB - grain{ii}.centimA)*omstep - 180;
        
        uni.dth  = abs(grain{ii}.theta(i1) - grain{ii}.theta(i2));

        uni.delo = abs(((0.5*lambda./sind(grain{ii}.theta(i1))) - ...
                    (0.5*lambda./sind(grain{ii}.theta(i2))))./dspref(grain{ii}.hklind(i1)));
        
    end
    
    ind = (grain{ii}.equipair == 0);
    
    uni.pl    = [uni.pl, grain{ii}.pl(:,ind)];
    uni.plref = [uni.plref, grain{ii}.plref(:,ind)];
    uni.hklind = [uni.hklind, grain{ii}.hklind(ind)];
    uni.theta = [uni.theta, grain{ii}.theta(ind)];
    uni.elo   = [uni.elo, (0.5*lambda./sind(grain{ii}.theta(ind)))./dspref(grain{ii}.hklind(ind));];

    grain{ii}.uni = uni;
end

end % of function
