function grain = gtIndexGrainOutputFinal(grain, cryst, tol_outl)
%
% Creates the final 'grain' output structure from the input 'grain'.
% Reorders reflections in each grain according to (1) their hkl families 
% and (2) alignment with the lab Z axis. Adds the field 'shklfound' 
% which shows for each signed hkl which reflection was assigned to it. 
% Outlier reflections are identified using the std tolerance 'tol_outl' 
% and are stored in grain.stat.outliers.
%

nof_pairs = length(grain.pairid);
grain.nof_pairs = nof_pairs;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Reorder reflections
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Order reflections by hklind (thetatype) and shklind
sortM(:,1) = grain.hklind;
sortM(:,2) = grain.shklind;
sortM(:,3) = (1:nof_pairs)';
sortM      = sortrows(sortM,[1 -2]);
reind      = sortM(:,3);


% Remove unnecessary fields
% Some are needed to be removed for grains that have been force merged
if isfield(grain,'merged')
    grain = rmfield(grain,'merged');
end

if isfield(grain,'extended')
    grain = rmfield(grain,'extended');
end

if isfield(grain,'hklsp')
    grain = rmfield(grain,'hklsp');
end

if isfield(grain,'pla')
    grain = rmfield(grain,'pla');
end

if isfield(grain,'pld')
    grain = rmfield(grain,'pld');
end

if isfield(grain,'difspotidA')
    grain = rmfield(grain,'difspotidA');
end

if isfield(grain,'difspotidB')
    grain = rmfield(grain,'difspotidB');
end


% Reorder values for all fields
% This fiddling is needed for grains that have been force merged, so 
% their output has been already formatted once.

% These may be row or column; make sure they are now row
grain.pairid = grain.pairid(:)';
grain.pairid = grain.pairid(reind);
grain.refid  = grain.refid(:)';
grain.refid  = grain.refid(reind);

% Rearrange all other fields that needs it
fn = fieldnames(grain);
fn(strcmp(fn,'pairid'))    = []; 
fn(strcmp(fn,'refid'))     = []; 
fn(strcmp(fn,'id'))        = []; 
fn(strcmp(fn,'R_vector'))  = []; 
fn(strcmp(fn,'R_onedge'))  = []; 
fn(strcmp(fn,'nof_pairs')) = []; 
fn(strcmp(fn,'center'))    = []; 
fn(strcmp(fn,'indST'))     = []; 
fn(strcmp(fn,'shklfound')) = []; 
fn(strcmp(fn,'refindexed'))= []; 
fn(strcmp(fn,'stat'))      = []; 
fn(strcmp(fn,'phaseid'))   = []; 
fn(strcmp(fn,'shkldirs'))  = []; 

% Use theta to decide if transpose is needed
if (size(grain.theta,1) == 1)
    tr = false;
else
    tr = true;
end

for ii = 1:length(fn)
    if tr
        grain.(fn{ii}) = grain.(fn{ii})(reind,:)';
    else
        grain.(fn{ii}) = grain.(fn{ii})(:,reind);
    end
end


%%%%%%%%%%%%%%%%%
%% Extra fields
%%%%%%%%%%%%%%%%%

grain.pla = NaN(1,nof_pairs);
grain.pld = NaN(1,nof_pairs);

grain.hklsp = zeros(size(grain.hkl,1),length(grain.hklind));
for ii = 1:length(grain.hklind)
    grain.hklsp(:,ii) = cryst.shklfam{grain.hklind(ii)}(grain.shklind(ii),:)';
end
    

for ii = 1:length(grain.shklfound)
    for jj = 1:length(grain.shklfound{ii})
        for kk = 1:length(grain.shklfound{ii}{jj})
            grain.shklfound{ii}{jj}(kk) = find(reind==grain.shklfound{ii}{jj}(kk));
        end
    end
end


%%%%%%%%%%%%%%%%%
%% Mark outliers
%%%%%%%%%%%%%%%%%

grain.stat.outliers.normint = gtMathsOutliers(grain.normint, tol_outl, grain.stat.normintmean, grain.stat.normintstd);
grain.stat.outliers.int     = gtMathsOutliers(grain.int,  tol_outl, grain.stat.intmean, grain.stat.intstd );
grain.stat.outliers.bbxs    = gtMathsOutliers(grain.bbxs, tol_outl, grain.stat.bbxsmean, grain.stat.bbxsstd);
grain.stat.outliers.bbys    = gtMathsOutliers(grain.bbys, tol_outl, grain.stat.bbysmean, grain.stat.bbysstd);
grain.stat.outliers.dcom    = gtMathsOutliers(grain.dcom, tol_outl, grain.stat.dcommean, grain.stat.dcomstd);
grain.stat.outliers.dang    = gtMathsOutliers(grain.dang, tol_outl, grain.stat.dangmean, grain.stat.dangstd);

grain.stat.outliers.normint = grain.pairid(grain.stat.outliers.normint);
grain.stat.outliers.int     = grain.pairid(grain.stat.outliers.int);
grain.stat.outliers.bbxs    = grain.pairid(grain.stat.outliers.bbxs);
grain.stat.outliers.bbys    = grain.pairid(grain.stat.outliers.bbys);
grain.stat.outliers.dcom    = grain.pairid(grain.stat.outliers.dcom);
grain.stat.outliers.dang    = grain.pairid(grain.stat.outliers.dang);




