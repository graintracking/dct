function [paircons, pofinters] = gtIndexCheckPairCons(act, cand, tol, ...
                                 samenv, ACM, minn, cons)
% GTINDEXCHECKPAIRCONS Pair consistency check.
%
% [paircons, pofinters] = gtIndexCheckPairCons(act, cand, tol, ...
%                                              samenv, ACM, minn, cons)
%
% ---------------------------------------------------------------------
%
% Checks pair consistency of reflection 'act' against a set of others 
% in 'cand'.
%
% INPUT
%   act    - the actual reflection to be tested against
%   cand   - a set of candidate reflections to be checked
%   tol    - tolerances
%   samenv - sample envelope
%   ACM    - angular consistency matrix
%   minn   - minimum number of consistent reflections to find
%   cons   - candidates to be checked (logical vector)
%
% OUTPUT
%   paircons  - a vector of logicals; true where 'cand' is pair consistent
%   pofinters - pair-wise points of 'intersection' between diff. paths of 
%               'act' and the consistent 'cand'-s; empty if found less than 
%               'minn' consistent reflections
%



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Check bbox sizes and intensity
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% bbox y size close enough?
consm(:,4) = (act.bbys/tol.bbys < cand.bbys) & (cand.bbys < act.bbys*tol.bbys);
% bbox x size close enough?
consm(:,3) = (act.bbxs/tol.bbxs < cand.bbxs) & (cand.bbxs < act.bbxs*tol.bbxs);
% integrated intensities close enough?
consm(:,2) = (act.int/tol.int < cand.int) & (cand.int < act.int*tol.int);

consm(:,1) = cons;

cons = all(consm,2);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Check distance and location
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

paircons = find(cons);

% andy - can hack this a bit to treat lines, from which we have an estimate
% of grain position?
% note that I have also changed the call to gtMathsLinesDists to use
% paircons instead of cons
if ~isfield(act, 'g3d')
    % Spatial distance between diffracted beams small enough?
    cons2 = gtMathsLinesDists([act.ca act.dir], ...
                          [cand.ca(paircons,:) cand.dir(paircons,:)]) < tol.dist ;
else
    % Spatial distance between grain positions small enough?
    % sum of cons to get the number required
    cons2 = sqrt(sum((repmat(act.g3d, sum(cons), 1)-cand.g3d(paircons, :)).^2, 2)) < tol.dist ;
end
paircons(~cons2) = [];

if length(paircons) < minn
	pofinters = [];
	return
end

% is intersection in the sample volume?
%
% NOTE: the intersection of 2 diff. paths might actually be far from 
% the grain center if they are near being parallel. This could be taken 
% into account here, however, there should be no parallel diff. paths 
% in a grain set.

% andy - can hack this a bit to treat lines, from which we have an estimate
% of grain position?
if ~isfield(act, 'g3d')
    pofinters = gtMathsLinePairsIntersections([act.ca act.dir], ...
        [cand.ca(paircons,:) cand.dir(paircons,:)]);
else
    % the "intersection" is the mean of the two points
    pofinters = (cand.g3d(paircons,:) + repmat(act.g3d, length(paircons), 1))/2;
end

cons = gtIndexIsPointInSample(pofinters,samenv);
pofinters(~cons,:) = [];
paircons(~cons)    = []; 

if length(paircons) < minn
	pofinters = [];
	return
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Check angle
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

cons  = false(length(paircons),1);

candhklind  = cand.hklind(paircons);
candpl      = cand.pl(paircons,:);

% Angular tolerance in radians 
tol_ang_rad = tol.ang*pi/180;

% Angular consistency matrix for 'act';
% act.hklind{1} may have more than one element 
ACMact = ACM(act.hklind{1},:);

% Angles between 'act' and 'cand' in radians
ang = gtMathsVectorsAnglesRad(act.pl, candpl, 0);

% !!! This could be a parfor loop!
for ii = 1:length(paircons)
  ACV = ACMact(:,candhklind{ii});
  ACV = vertcat(ACV{:});
  cons(ii) = min(abs(ACV - ang(ii))) < tol_ang_rad;
end

pofinters(~cons,:) = [];
paircons(~cons)    = []; 

if length(paircons) < minn
	pofinters = [];
end


end % of function

