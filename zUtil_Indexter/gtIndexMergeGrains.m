function [newgrains, tot] = gtIndexMergeGrains(grains, tot, tol_merge, cryst)
% GTINDEXMERGEGRAINS Merges grains that seem identical (one of the main 
% stages of Indexter).
%
% [newgrains, tot] = gtIndexMergeGrains(grains, tot, tol_merge, cryst)
% 
% --------------------------------------------------------------------
%
% 2nd of the 3 steps of the iterative indexing strategy. It looks for  
% combinations of existing grains and merges them into one grain if they 
% seem to be identical - meaning that the differences in bounding box 
% sizes, intensities, location and orientation are within the specified 
% tolerances.  
% Updates grain parameters and statistics for merged grains.
%
% INPUT
%  grain        - all grain data (cell vector)
%  tot.         - all reflection data (including indexed and unindexed)
%  tol_merge.   - tolerances for merging grains;
%                 (as in paramers.index.strategy.m.beg or m.end)
%  cryst.       - crystallography and other data
%
% OUTPUT
%  grain        - updated grain data including merged and unmerged grains
%  tot.indexed  - updated; true for reflections indexed in newgrains, false
%                 for the rest
%

%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Initialize grain data
%%%%%%%%%%%%%%%%%%%%%%%%%%

fprintf('\n MERGING GRAINS...\n\n')
disp(tol_merge)
disp(' ')

nof_newgrains = 0;
nof_grains    = length(grains);

if (nof_grains == 0)
    newgrains = grains;
    return
end

tic

% Grains indices to be checked
remaining = 1:nof_grains;


% Load relevant info of all grains into gr:

gr.id            = gtIndexAllGrainValues(grains,'id',[],1,[]);
gr.nof_pairs     = gtIndexAllGrainValues(grains,'nof_pairs',[],1,[]);

gr.center   = gtIndexAllGrainValues(grains,'center',[],1,1:3);

gr.R_vector = gtIndexAllGrainValues(grains,'R_vector',[],1,1:3);

gr.R_onedge      = gtIndexAllGrainValues(grains,'R_onedge',[],1,[]);

gr.bbxs(:,1)     = gtIndexAllGrainValues(grains,'stat','bbxsmean',1,[]);
gr.bbys(:,1)     = gtIndexAllGrainValues(grains,'stat','bbysmean',1,[]);
gr.int(:,1)      = gtIndexAllGrainValues(grains,'stat','intmean',1,[]);
gr.dang(:,1)     = gtIndexAllGrainValues(grains,'stat','dangmean',1,[]);

%gr.normint(:,1)  = gtIndexAllGrainValues(grains,'stat','normintmean',1);
% !!! normint should be used in the future

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Loop to check identical grains
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Loop until there are grains in 'remaining' to check

while length(remaining) >= 1
    
    % Actual grain to test against
    act  = sfSelectGrains(remaining(1),gr);
    
    % All other remaining grain candidates for merge
    cand = sfSelectGrains(remaining(2:end),gr);

    
    % Distance tolerance between centers: approximate grain size times 
    % a constant (in lab units, not in  pixels!!)
    tol_merge.dist = min([act.bbxs, act.bbys]) * cryst.pixelsize * ...
                     tol_merge.distf ;
    tol_merge.dist = max(tol_merge.dist, tol_merge.distmin);
    tol_merge.dist = min(tol_merge.dist, tol_merge.distmax);
                

    % Orientation tolerance in Rodrigues space (Rdist) is set depending
    % on the the average angular deviation in the grain.
    % Determination of Rdist:
    %  Std of a normal distribution of angular deviations from the average
    %  absolut deviation:                std_ang = abs_ang_mean/0.8
    %  Take f*std as the upper limit:    lim_ang = f*std_ang
    %  Max. distance in Rodrigues space: lim_Rdist = tan(lim_ang/2)
    tol_merge.ang   = act.dang/0.8*tol_merge.angf;
    tol_merge.ang   = max(tol_merge.ang, tol_merge.angmin);
    tol_merge.ang   = min(tol_merge.ang, tol_merge.angmax);
    
    tol_merge.Rdist = tand(tol_merge.ang/2);     
    
    
    % Check which grains fulfill merge criteria
    % 'tomerge' vector contains the linear indices in 'cand' to be merged
    tomerge = gtIndexCheckMergeGrains(act, cand, grains, tot, ...
                                      tol_merge, cryst);
    
    % Increase number of new grains
    nof_newgrains = nof_newgrains + 1 ;
    
   
    if isempty(tomerge)
        
        % No other identical grains, grain is saved unchanged with new 
        % grain ID
        newgrains{nof_newgrains}        = grains{act.id};
        newgrains{nof_newgrains}.id     = nof_newgrains;
        newgrains{nof_newgrains}.merged = false;
        
    else
        
        % Recreate new grain from scratch with all its reflections
        
        % Collect all reflections for the new grain
        % Make sure we have column vectors even if grains have been reloaded 
        refids  = grains{act.id}.refid(:);
        hklind  = num2cell(grains{act.id}.hklind(:));
        shklind = grains{act.id}.shklind(:);
        
        for ii = 1:length(tomerge)
            refids  = [refids; grains{cand.id(tomerge(ii))}.refid(:)] ;
            hklind  = [hklind; num2cell(grains{cand.id(tomerge(ii))}.hklind(:))] ;
            shklind = [shklind; grains{cand.id(tomerge(ii))}.shklind(:)] ;
        end
        
       
        % Reload original reflections data
        newgrefs = gtIndexSelectRefs(refids,tot);

        % If none of the grains to be merged are on the edge of the 
        % fundamental zone, keep old signed hkl indices for faster
        % Rodrigues search below
        if ~any([act.R_onedge; cand.R_onedge(tomerge)])
            newgrefs.hklind  = hklind;
            newgrefs.shklind = shklind;
        end
        
        
        % Redefine angular tolerance as above now with the maximum dang
        tol_merge.ang = max([act.dang; cand.dang(tomerge)])/0.8*tol_merge.angf;
        tol_merge.ang = max(tol_merge.ang, tol_merge.angmin);
        tol_merge.ang = min(tol_merge.ang, tol_merge.angmax);

        
        % Create new merged grain; orientation test is done, not all
        % reflections may pass the test in the merged grain
        [graintmp, goodind] = gtIndexGrainOutputBasic(newgrefs,...
                              cryst, tol_merge.ang);                
         
        % Only accept merged grain if it has more reflections than original
        % (rarely it may not have)
        if length(goodind) > act.nof_pairs
                        
            newgrains{nof_newgrains}        = graintmp;
            newgrains{nof_newgrains}.id     = nof_newgrains;
            newgrains{nof_newgrains}.merged = true;
            
            fprintf('Grains merged as new grain #%d:   %s\n', ...
                nof_newgrains, num2str([act.id, cand.id(tomerge(:))']));

            % Ref id-s not included in merged grain
            excind          = true(length(newgrefs.id),1);  
            excind(goodind) = false;                     
          
            if any(excind)
               
                fprintf('Pairs excluded from grain #%d :   %s\n', ...
                        nof_newgrains, num2str(newgrefs.pairid(excind)'))
                
                % Add excluded ones to unindexed set
                tot.indexed(newgrefs.id(excind)) = false;

            end
            
        % Do not merge grain
        else
            
            % Original grain is saved unchanged with new grain ID
            newgrains{nof_newgrains}        = grains{act.id};
            newgrains{nof_newgrains}.id     = nof_newgrains;
            newgrains{nof_newgrains}.merged = false;
            tomerge                         = [];
            
        end
    end
    
    % Delete processed grains from list
    remaining(1) = []; 
    
    % After deleting the first in 'remaining', 'tomerge' is the correct index 
    remaining(tomerge) = [];  
    
end


disp(' ')
toc


end % of main function


%%%%%%%%%%%%%%%%%%%%%%%%%%
%% SUB-FUNCTION
%%%%%%%%%%%%%%%%%%%%%%%%%%

function grsel = sfSelectGrains(keeplist,gr)

  grsel.id        = gr.id(keeplist);
  grsel.nof_pairs = gr.nof_pairs(keeplist);
  grsel.int       = gr.int(keeplist);
  % grsel.normint   = gr.normint(keeplist); % !!! normint should be used
  % in the future
  grsel.center    = gr.center(keeplist,:);
  grsel.R_vector  = gr.R_vector(keeplist,:);
  grsel.R_onedge  = gr.R_onedge(keeplist);
  grsel.bbys      = gr.bbys(keeplist);
  grsel.bbxs      = gr.bbxs(keeplist);
  grsel.dang      = gr.dang(keeplist);

end
