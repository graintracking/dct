function [grain, excrefids] = gtIndexModifyGrain(grain, reminds, ...
                              addrefids, tot, tol, cryst)
% FUNCTION [grain, excrefids] = gtIndexModifyGrain(grain, reminds, ...
%                               addrefids, tot, tol, cryst)
%
% Applies changes to a grain if a reflections are to be excluded or added.
% Rejects the worst reflections from the final grain if allowed 
% multiplicity for a plane family is exceeded. Updates basic grain data 
% and grain statistics.
%                        
% INPUT
%  grain     - a single grain data
%  reminds   - reflection linear indices in grain to be removed
%  addrefids - original reflection id-s to be added from 'tot'
%  tot       - all original reflection data 
%  tol.ang   - angular tolerance to be used
%  cryst     - crystallography and other data
%
% OUTPUT
%  grain     - updated grain data; empty if grain is found invalid
%  excrefids - original reflection id-s that have been excluded from grain 
%              due to exceeding multiplicity or if the grain was eventually 
%              found invalid 
%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Recreate grain with additional reflections
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Save some old grain parameters
oldgrain.id       = grain.id;
oldgrain.merged   = grain.merged;
oldgrain.extended = grain.extended;
oldgrain.refid    = grain.refid;

excind1 = [];

% If grain has been modified, recreate grain 
if ~isempty(addrefids) || ~isempty(reminds)
    
    grain.refid(reminds)   = [];
    grain.shklind(reminds) = [];
    grain.hklind(reminds)  = [];
    
    % Get reflection data for the new grain
    allrefs = gtIndexSelectRefs([grain.refid'; addrefids], tot);
    
    % Keep signed hkl indices for already indexed reflections in grain
    allrefs.shklind(1:length(grain.refid)) = grain.shklind;
    allrefs.hklind(1:length(grain.refid))  = num2cell(grain.hklind);
    
    % Recreate grain data
    [grain, goodind] = gtIndexGrainOutputBasic(allrefs, cryst, tol.ang);
    
    % If there are any reflections excluded
    if length(goodind) ~= length(allrefs.id)
    
        % Ref id-s not included in merged grain
        excind1          = 1:length(allrefs.id);
        excind1(goodind) = [];
        
    end
    
    excrefids = allrefs.id(excind1);
    
else
    
    excrefids = [];
    
end
	

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Reject mutiple detections and recreate grain
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Exclude reflections that were indexed too many times
% (returns the indices to be excluded)
excind2 = gtIndexExcludeMultipleRefs(grain);


	
% Recreate grain output if a reflection was rejected
if ~isempty(excind2)
    
     % this should be a column!
    excrefids = [excrefids; grain.refid(excind2)'];

    newrefids = grain.refid;
    newrefids(excind2) = [];
    
    newgrainrefs = gtIndexSelectRefs(newrefids, tot);
    
    % Keep signed hkl indices
    newgrainrefs.shklind = grain.shklind;
    newgrainrefs.hklind  = num2cell(grain.hklind);
    
    newgrainrefs.shklind(excind2) = [];
    newgrainrefs.hklind(excind2)  = [];
    
    % Get new grain data
    grain = gtIndexGrainOutputBasic(newgrainrefs, cryst, tol.ang);
    
end


grain.id       = oldgrain.id;
grain.merged   = oldgrain.merged;
grain.extended = oldgrain.extended;


if isempty(grain.R_vector)
    % The grain is not valid anymore and will be deleted. Exclude all
    % initial reflections in grain.
    % this should also be a column! 
    excrefids = oldgrain.refid';
    grain     = [];
end


if ~isempty(excrefids) && size(excrefids, 2)~=1
    disp('! not a column vector')
    keyboard
end
    
if any(excrefids)
    fprintf('Pairs excluded from grain #%d :   %s\n', oldgrain.id,...
        num2str(tot.pairid(excrefids)'))
end


end % of function

