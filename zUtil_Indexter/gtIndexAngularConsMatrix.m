function ACM = gtIndexAngularConsMatrix(cryst)
%
% FUNCTION  ACM = gtIndexAngularConsMatrix(cryst)
%
% Returns the angular consistency matrix that contains a unique 
% list of theoretical angles between plane normals of any two families in 
% an undeformed lattice.
%
% INPUT
%   cryst     - as in parameters.cryst; 
%
% OUTPUT
%   ACM{i,j}  - theoretical angles in radians for any i,j family indices 
%               in cryst; symmetric cell array of size nxn where 
%               n = length(cryst.thetatype)
%

% No. of families
n = length(cryst.thetatype);

% Loop through all combinations of the families

ACM = cell(n);

for ii = 1:n
    for jj = ii:n
        
        % Get unique list of all possible angles
        uniangles = gtIndexAngularCons(ii,jj,cryst);
        
        ACM{ii,jj}  = uniangles;
        ACM{jj,ii}  = uniangles;
        
    end
end

end % of function
