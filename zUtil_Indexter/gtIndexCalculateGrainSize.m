function grain = gtIndexCalculateGrainSize(grain, latticepar, volSample, svolSample)


bbx = gtIndexAllGrainValues(grain, 'stat', 'bbxsmean', 1, []);
bby = gtIndexAllGrainValues(grain, 'stat', 'bbysmean', 1, []);
sbbx = gtIndexAllGrainValues(grain, 'stat', 'bbxsstd', 1, []);
sbby = gtIndexAllGrainValues(grain, 'stat', 'bbysstd', 1, []);

bbs = (bbx+bby)/2;
size_bb = bbs;
sbbs = sqrt( (sbbx.^2) + (sbby.^2) );
ssize_bb = sbbs;

% intensity based
intmean = gtIndexAllGrainValues(grain, 'stat', 'intmean', 1, []);
sintmean = gtIndexAllGrainValues(grain, 'stat', 'intstd', 1, []);
sum_intmean = sum(intmean);
ssum_intmean = sqrt(sum(sintmean.^2));

caratio = latticepar(3)/latticepar(1); %c/a ratio
grainvolume = (intmean/sum_intmean*volSample); %um^3
% volume of hexagonal prism is:
% V = h * AreaHexagon = h * 6 * AreaTriangle = 6h * sqrt(3)/4 * r^2 = 
% h r^2 * 3/2*sqrt(3) = (h/r) r^3 3/2 sqrt(3) = (c/a) 3/2 sqrt(3) r^3
% V = (c/a) 3/2 sqrt(3) r^3  ---> 
%      r^3 = V / (c/a 3/2 sqrt(3))

radius3 = grainvolume / (caratio*3/2*sqrt(3));

size_int = 2*( radius3.^(1/3) ); %um equivalent diameter of grains (is proportional to 2a of unit cell) %um
ssize_int = 2/ (caratio*3/2*sqrt(3))/3.*size_int.*sqrt( (sintmean./intmean).^2 + (ssum_intmean/sum_intmean)^2 + (svolSample/volSample)^2 ); %um

parfor ii=1:length(grain)
    grain{ii}.stat.size_bb      = size_bb(ii); % pixels
    grain{ii}.stat.err_size_bb  = ssize_bb(ii); % pixels
    grain{ii}.stat.size_int     = size_int(ii);
    grain{ii}.stat.err_size_int = ssize_int(ii);
    grain{ii}.size_int          = size_int(ii); %um
    grain{ii}.err_size_int      = ssize_int(ii); %um
end


end
