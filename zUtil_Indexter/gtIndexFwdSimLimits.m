function lim = gtIndexFwdSimLimits(grain, parameters, pairinfo)
% GTINDEXFWDSIMLIMITS  Sets the limits to run then gtIndexDifspot_2
%
%     lim = gtIndexFwdSimLimits(grain, parameters, pairinfo)
%     ------------------------------------------------------
%
%     INPUT:
%       grain      = <cell>       all the grains as in index.mat
%       parameters = <double>     parameters.mat file
%       pairinfo   = <logical>    true if indexed with gtINDEXTER
%
%     OUTPUT:
%       lim        = <struct>     contains the limits for UVW
%          .u = <double>     Limits in mm for U coordinate
%          .v = <double>     Limits in mm for V coordinate
%          .w = <double>     Limits in degrees for omega coordinate

    labgeo = parameters.labgeo;
    if (~isfield(labgeo, 'omstep') || isempty(labgeo.omstep))
        labgeo.omstep = gtAcqGetOmegaStep(parameters);
    end
    if (~isfield(parameters, 'detgeo') || isempty(parameters.detgeo))
        parameters.detgeo = gtGeoConvertLegacyLabgeo2Detgeo(labgeo);
    end
    detgeo = parameters.detgeo;

    if pairinfo
        uvwA_sim = gtIndexAllGrainValues(grain, 'fsimA', 'uvw', [], [], false); uvwA_sim = [uvwA_sim{:}];
        uvwB_sim = gtIndexAllGrainValues(grain, 'fsimB', 'uvw', [], [], false); uvwB_sim = [uvwB_sim{:}];
        uvwA_exp = gtIndexAllGrainValues(grain, 'uvwA', [], [], [], false); uvwA_exp = [uvwA_exp{:}];
        uvwB_exp = gtIndexAllGrainValues(grain, 'uvwB', [], [], [], false); uvwB_exp = [uvwB_exp{:}];

        uvw_sim = [uvwA_sim, uvwB_sim];
        uvw_exp = [uvwA_exp, uvwB_exp];

         % Deviations between fwd simulation and measured spot positions
        du = uvw_sim(1, :) - uvw_exp(1, :);
        dv = uvw_sim(2, :) - uvw_exp(2, :);
        dw = uvw_sim(3, :) - uvw_exp(3, :);

        lim.u = mean(du) + 3*std(du); % pixel
        lim.v = mean(dv) + 3*std(dv); % pixel
        lim.w = mean(dw) + 3*std(dw); % images

        % convert to mm and degrees
        lim.u = lim.u * detgeo.pixelsizeu; % mm
        lim.v = lim.v * detgeo.pixelsizev; % mm
        lim.w = lim.w * labgeo.omstep; % omega [degrees]
    else
        % fsim only indexed peaks

%         uvw = gtIndexAllGrainValues(grain, 'fsimU', 'uvw', [], [], false);  uvw = [uvw{:}];
%         ind = find(isnan(uvw(3, :)));
%         % experimental positions
%         fc = gtIndexAllGrainValues(grain, 'fc', [], [], [], false);        fc = [fc{:}];
%         sc = gtIndexAllGrainValues(grain, 'sc', [], [], [], false);        sc = [sc{:}];
%         omega = gtIndexAllGrainValues(grain, 'omega', [], [], [], false);  omega = [omega{:}];
%
%         uvw(:, ind) = [];
%
%         fc(ind) = [];
%         sc(ind) = [];
%         omega(ind) = [];
%         
%         cent = [fc; sc];
%         centim = omega/labgeo.omstep;

        lim.u = 2; % pixel
        lim.v = 2; % pixel
        lim.w = 5; % image number
    end

    disp(['current pixelsizeu: ' num2str(detgeo.pixelsizeu)])
    disp(['current pixelsizev: ' num2str(detgeo.pixelsizev)])
    disp(['omega step:         ' num2str(labgeo.omstep)])
end
