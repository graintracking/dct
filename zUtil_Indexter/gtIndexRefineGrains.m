function [grain, tot] = gtIndexRefineGrains(grain, tot, tol_b, cryst)
% GTINDEXREFINEGRAINS Adds unindexed reflections to grains (one of the 
% main stages of Indexter).
%
% [grain, tot] = gtIndexRefineGrains(grain, tot, tol_b, cryst)
% 
% ------------------------------------------------------------
%
% 3rd out of 3 steps of the iterative indexing strategy. It loops through
% all the grains and checks if additional unindexed reflections can be 
% added to them based on tolerances specified for building grains.  
% Grain parameters and statistics for extended grains are updated, 
% reflections are checked for multiplicity and least fitting ones are 
% rejected if multiplicity exceeds the maximum.
%
% INPUT
%  grain.       - all grain data (cell vector)
%  tot.         - all reflection data (including indexed and unindexed)
%  tol_b.       - tolerances for building grains;
%                 (as in paramers.index.strategy.b.beg or b.end)
%  cryst.       - crystallography and other data
%
% OUTPUT
%  grain        - updated grain data
%  tot.indexed  - updated; true for all reflection ID-s indexed in grain,
%                 false for the rest
%

fprintf('\n REFINING GRAINS...\n\n');
disp(tol_b);
disp(' ');

if isempty(grain)
    return;
end

tic;

% Grains to delete
gtodel = [];

% all possible normalized signed hkl vectors in cartesian CRYSTAL coordinates
indshkldirs_cry = gtCrystHKL2Cartesian(vertcat(cryst.shklfam{:})', ...
                                       cryst.Bmat)';

% Extract all the Rodrigues vectors into column vectors
R_vectors = gtIndexAllGrainValues(grain, 'R_vector', [], 1, 1:3);

% Compute all orientation matrices g
all_g = gtMathsRod2OriMat(R_vectors.');

% Express normalised signed hkl vectors in cartesian SAMPLE CS
all_shkldirs = gtVectorCryst2Lab(indshkldirs_cry, all_g); 
    
for ii = 1:length(grain);
    % Store normalised signed hkl vectors in cartesian SAMPLE CS
    grain{ii}.shkldirs = all_shkldirs(:, :, ii); 

    % Set distance tolerance using grain statistics
    tol_b.dist = min(3*grain{ii}.stat.dcomstd, tol_b.distmax/2);

    % load data for all candidate reflections
    tryrefs = gtIndexSelectRefs(~tot.indexed, tot);
    
    % check consistency of bbox sizes, intensity, path distance 
    % and orientation
    goodfits = gtIndexCheckAddRefs(tryrefs, grain{ii}, tol_b, 'tols');
    
    if isempty(goodfits)
        grain{ii}.extended = false;
    else
        grain{ii}.extended = true;
        
        fprintf('Pairs added to grain #%d      :   %s\n', ii,...
                num2str(tot.pairid(tryrefs.id(goodfits))'));
    end
   
    
    % Include good fits in grain, get updated grain data and excluded 
    % reflection indices (if grain is found invalid, excrefids contains all
    % its refid-s)
    [grain{ii}, excrefids] = gtIndexModifyGrain(grain{ii}, [], ...
                             tryrefs.id(goodfits), tot, tol_b, cryst);
   
    
    if isempty(grain{ii})
        gtodel = [gtodel ii];
        fprintf('Grain #%d was found invalid and deleted.\n', ii);
    end
    
    % Delete the reflections from list that were added to the grain and add
    % back the ones that were rejected:
    tot.indexed(tryrefs.id(goodfits)) = true;
    tot.indexed(excrefids)            = false;
    
end


% Delete bad grains
grain(gtodel) = [];


disp(' ');
toc;

end % end of function

