function  grain = gtIndexGrainStatistics(grain)

% Calculates the standard deviations of grain properties for each grain.

parfor ii = 1:length(grain)
	
	% Only the absolute values of angular deviation and real space distance are measured. 
	% These give the absolute mean deviation of the errors which is about 0.8*std in case
	% of a normal distribution. 
	grain{ii}.stat.dangstd = grain{ii}.stat.dangmean/0.8;
	grain{ii}.stat.dcomstd = grain{ii}.stat.dcommean/0.8;

	grain{ii}.stat.bbxsstd    = std(grain{ii}.bbxs);
	grain{ii}.stat.bbysstd    = std(grain{ii}.bbys);
	grain{ii}.stat.intstd     = std(grain{ii}.int);
	grain{ii}.stat.normintstd = std(grain{ii}.normint);

	% Maximum relative values
	grain{ii}.stat.bbxsrat    = max(grain{ii}.bbxs)/min(grain{ii}.bbxs) ;
	grain{ii}.stat.bbysrat    = max(grain{ii}.bbys)/min(grain{ii}.bbys) ;
	grain{ii}.stat.intrat     = max(grain{ii}.int)/min(grain{ii}.int) ;
	grain{ii}.stat.normintrat = max(grain{ii}.normint)/min(grain{ii}.normint) ;
	
end	


end % of function

