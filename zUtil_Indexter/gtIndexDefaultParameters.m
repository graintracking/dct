function par_index = gtIndexDefaultParameters()

% 'strategy' contains all indexing parameters 
par_index.strategy = gtIndexDefaultStrategy();
% 'discard' may define a list of pair id-s to be discarded in indexing 
par_index.discard  = [];
% 'forcemerge' may define a list of grains to be merged "manually" if the 
% indexing results are not satisfactory
par_index.forcemerge = [];

end
