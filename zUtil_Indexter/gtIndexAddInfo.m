function grain = gtIndexAddInfo(grain, phaseid, parameters)
% GTINDEXADDINFO Adds additional diff. spot info to the grain output from
% Indexter to be used in the geometry and strain fitting.
%
% INPUT
%   grain      - grain data structure (as output from gtINDEXTER)
%
% OPTIONAL INPUT
%   phaseid    - crystallographic phase ID; if undefined, taken from grain{1}
%   parameters - as in parameters.mat; if undefined, loaded from
%                parameters.mat
%
% OUTPUT
%   grain      - additional fields are added or overwritten
%    .active
%    .centA
%    .centB
%    .omegaA
%    .omegaB
%    .centimA
%    .centimB
%    .etaA
%    .etaB
%    .fsimA
%    .fsimB
%    .fsim
%    .uni
%    .equipair
%    .equilist
%    .plref
%    .strain
%
%
% Version 001 18-04-2014 by P.Reischig
%


if ~exist('phaseid','var') || isempty(phaseid)
    phaseid = grain{1}.phaseid;
end

fprintf('Processing phase %d\n', phaseid);

if ~exist('parameters','var') || isempty(parameters)
    disp('Loading parameters file...')
    parameters = load('parameters.mat');
    parameters = parameters.parameters;
end

difspottable = [parameters.acq.name 'difspot'];
pairtable    = parameters.acq.pair_tablename;

% B matrix
Bmat = gtCrystHKL2CartesianMatrix(parameters.cryst(phaseid).latticepar);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Difspot info
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
tic

% Get pair ID-s
disp('Connecting to database...')
gtDBConnect

% Get spot and pair data
% /use of mysql multiple rows seems unstable -> done one-by-one/ 
fprintf('Loading diff. spot data from difspottable %s ...\n', difspottable)
[difID_spot, centU, centV, centim] = mym(sprintf('SELECT difspotID,CentroidX,CentroidY,CentroidImage FROM %s',difspottable));
if isempty(difID_spot)
	warning('Difspottable seems empty. No data could be retrieved.')
end

fprintf('Loading Friedel pair data from pairtable %s ...\n', pairtable)
[pairID, omegaA, omegaB, etaA, etaB] = mym(sprintf('SELECT pairID,omega,omegaB,eta,etaB FROM %s',pairtable));
if isempty(pairID)
	warning('Pairtable seems empty. No data could be retrieved.')
end

disp('Adding diff. spot data to grains...')

for ii = 1:length(grain)
% This can be a parfor loop.

    % Coordinates of the plane normals in the Cartesian Crystal system
    grain{ii}.plref = gtCrystHKL2Cartesian(grain{ii}.hklsp, Bmat);
    
    % Strain
    if ~isfield(grain{ii},'strain') || ~isfield(grain{ii}.strain,'strainT')
        grain{ii}.strain.strainT = NaN(3,3);
    end
    
    % Spot centers A and B
    grain{ii}.centA   = zeros(2,length(grain{ii}.difspotidA));
    grain{ii}.centB   = grain{ii}.centA;
    
    grain{ii}.centimA = zeros(1,length(grain{ii}.difspotidA));
    grain{ii}.centimB = grain{ii}.centimA;

    grain{ii}.omegaA  = zeros(1,length(grain{ii}.difspotidA));
    grain{ii}.omegaB  = grain{ii}.omegaA;

    grain{ii}.etaA    = zeros(1,length(grain{ii}.difspotidA));
    grain{ii}.etaB    = grain{ii}.etaA;
   
 
    for jj = 1:length(grain{ii}.difspotidA)
        ind = find(grain{ii}.difspotidA(jj) == difID_spot, 1, 'first');
		if isempty(ind)
			fprintf('Grain ID:   %d \n', grain{ii}.id)
			fprintf('Difspot ID: %d \n', grain{ii}.difspotidA(jj))
			error('The above difspot ID is not present in the difspot table.')
		end
        grain{ii}.centA(:,jj) = [centU(ind) centV(ind)];
        grain{ii}.centimA(jj) = centim(ind);
        
        ind = find(grain{ii}.difspotidB(jj) == difID_spot, 1, 'first');
		if isempty(ind)
			fprintf('Grain ID:   %d \n', grain{ii}.id)
			fprintf('Difspot ID: %d \n', grain{ii}.difspotidB(jj))
			error('The above difspot ID is not present in the difspot table.')
		end
		grain{ii}.centB(:,jj) = [centU(ind) centV(ind)];
		grain{ii}.centimB(jj) = centim(ind);

        ind = find(grain{ii}.pairid(jj) == pairID, 1, 'first');
		if isempty(ind)
			fprintf('Grain ID: %d \n', grain{ii}.id)
			fprintf('Pair ID:  %d \n', grain{ii}.pairid(jj))
			error('The above pair ID is not present in the pair table.')
		end
        grain{ii}.omegaA(1,jj) = omegaA(ind);
        grain{ii}.omegaB(1,jj) = omegaB(ind);
        grain{ii}.etaA(1,jj)   = etaA(ind);
        grain{ii}.etaB(1,jj)   = etaB(ind);
    end
    
    grain{ii}.active = true;
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Other info
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Equivalent pairs in each grain
disp('Searching equivalent pairs...')
grain = gtIndexEquivalentPairs(grain);

% Add unique plane normals    
grain = gtIndexUniqueReflections(grain, parameters.cryst(phaseid).dspacing,...
        parameters.acq.energy);

% Forward simulation
disp('Forward simulating reflections...')
labgeo = parameters.labgeo;
if (~isfield(labgeo, 'omstep') || isempty(labgeo.omstep))
    labgeo.omstep = gtAcqGetOmegaStep(parameters);
end
if (~isfield(parameters, 'detgeo') || isempty(parameters.detgeo))
    parameters.detgeo = gtGeoConvertLegacyLabgeo2Detgeo(labgeo);
end
detgeo = parameters.detgeo;

% Fwd simulate pairs
grain = gtIndexFwdSimPairs(grain, detgeo, labgeo, parameters.samgeo,...
        [], parameters.cryst(phaseid).latticepar, parameters.acq.energy);
      
% Fwd simulate individual spots    
grain = gtFitFwdSimGrains(grain, parameters, 0, 0, []);

toc

end