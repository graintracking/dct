function [tot, cryst] = gtINDEXCreateInputData(phaseID, parameters)
% 
% FUNCTION [tot, cryst] = gtINDEXCreateInputData(phaseID, parameters)
%
% Loads the reflections of the given phase from the database and computes 
% their Rodrigues space coordinates. 
% Assigns a list of possible hkl families to those reflections for which
% it is undefined (thetatype=0). 
% Also loads crystallography information and generates the angular 
% consistency matrix with allowed angles between reflections.
%
% The prepared input data are saved in the file: 
%   4_grains/phase_##/index_input.mat
% This file can be used as input for Indexter to save time when run
% mutiple times.
%
% OPTIONAL INPUT
%  phaseID    - phase ID to be indexed (default is 1)
%  parameters - parameters as from the parameters file; if left empty, the
%               parameters file will be read
%
% OUTPUT
%  tot        - all reflection data
%  cryst      - crystalographic information and other data
%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Set parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if ~exist('phaseID','var')
    phaseID = 1;
end

if ~exist('parameters','var')
    parameters = [];
end

if isempty(parameters)
    load('parameters.mat');
end

fprintf('\nName of dataset:\n %s\n', parameters.acq.name)

fprintf('\nWorking on phase %02d (%s).\n', phaseID, ...
        parameters.cryst(phaseID).name)

tic


cryst = parameters.cryst(phaseID);


% Transformation matrix from HKL to Cartesian coordinates 
cryst.Bmat = gtCrystHKL2CartesianMatrix(cryst.latticepar);


% Angular consistency matrix containing allowed angles between any two plane
% families
cryst.ACM = gtIndexAngularConsMatrix(cryst); % in degrees


% Average pixel size (needed for calculating distance tolerances)
cryst.pixelsize = 0.5*(parameters.detgeo(1).pixelsizeu + parameters.detgeo(1).pixelsizev);
 


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Load data from database
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if isfield(parameters.acq, 'pair_tablename')
    pairtable = parameters.acq.pair_tablename;
else % what should be standard convention
    pairtable = sprintf('%sspotpairs',parameters.acq.name);
end
    
fprintf('\nConnecting to database...\n')
gtDBConnect();

fprintf('\nLoading reflections from database...\n')

% Additional information when using polychromatic indexing  - 
% maybe better to add a flag in parameters file and use that for switching ?
[columns,~,~,~,~,~] = mym(['show columns from ' pairtable]);
if ~ismember('g3dX',columns)
	test = false;
else
	test = true;
end

% All the pair data are loaded in 'tot'
% order list according to parameters.index.ordering  {'avintint', 'bbXSize'...}
if ~isfield(parameters.index,'sort_criterium')
    parameters.index.sort_criterium = 'avintint';
end
if ~isfield(parameters.index,'sort_direction')
    parameters.index.sort_direction = 'desc';
end
if (~test)
    mymcmd = sprintf(['SELECT pairID, theta, '...
        'avintint, avbbXsize, avbbYsize, samcentXA, samcentYA, samcentZA, '...
        'ldirX, ldirY, ldirZ, plX, plY, plZ, thetatype '...
        'FROM %s WHERE phasetype=%d ORDER BY %s %s'],pairtable, phaseID, parameters.index.sort_criterium, parameters.index.sort_direction);

    [tot.pairid, tot.theta, tot.int, tot.bbxs, tot.bbys, ...
    tot.ca(:,1),  tot.ca(:,2),  tot.ca(:,3), ...
    tot.dir(:,1), tot.dir(:,2), tot.dir(:,3), ...
    tot.pl(:,1),  tot.pl(:,2),  tot.pl(:,3), tot.thetatype] = mym(mymcmd);
else
    mymcmd = sprintf(['SELECT pairID, theta, '...
    'avintint, avbbXsize, avbbYsize, samcentXA, samcentYA, samcentZA, '...
    'ldirX, ldirY, ldirZ, plX, plY, plZ, thetatype, g3dX, g3dY, g3dZ '...
    'FROM %s WHERE phasetype=%d ORDER BY avintint desc'],pairtable, phaseID);

    [tot.pairid, tot.theta, tot.int, tot.bbxs, tot.bbys, ...
    tot.ca(:,1),  tot.ca(:,2),  tot.ca(:,3), ...
    tot.dir(:,1), tot.dir(:,2), tot.dir(:,3), ...
    tot.pl(:,1),  tot.pl(:,2),  tot.pl(:,3), tot.thetatype, ...
    tot.g3d(:,1), tot.g3d(:,2), tot.g3d(:,3)] = mym(mymcmd);
end

% Exclude Friedel pairs from indexing which are marked to be discarded
% and enable working on a subset of pairs
keeplist = true(numel(tot.pairid), 1);
if isfield(parameters.index,'discard') && ~isempty(parameters.index.discard)
    keeplist   = ~ismember(tot.pairid, parameters.index.discard);
end
if isfield(parameters.index,'subset') && ~isempty(parameters.index.subset)
    keeplist   = keeplist & ismember(tot.pairid, parameters.index.subset);
end
tot.pairid    = tot.pairid(keeplist);
tot.theta     = tot.theta(keeplist);
tot.int       = tot.int(keeplist);
tot.bbxs      = tot.bbxs(keeplist);
tot.bbys      = tot.bbys(keeplist);
tot.thetatype = tot.thetatype(keeplist);

tot.ca  = tot.ca(keeplist,:);
tot.dir = tot.dir(keeplist,:);
tot.pl  = tot.pl(keeplist,:);

if (test)
   tot.g3d  = tot.g3d(keeplist,:); 
end

tot.id = (1:length(tot.pairid))';


% Number of reflections
nof_refs = length(tot.pairid);

fprintf('\nTotal number of reflections to be processed: %d\n',nof_refs)

% Reflection id-s; they will not correspond to pairID-s !!
tot.id = (1:nof_refs)';

tot.hkl      = NaN(nof_refs,size(cryst.hkl,1));
tot.hklind   = cell(nof_refs,1);
tot.shklind  = NaN(nof_refs,1);
tot.Rline    = cell(nof_refs,1);
tot.Rhklind  = cell(nof_refs,1);
tot.Rshklind = cell(nof_refs,1);
  

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% hkl reflections 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% To minimise the probability of faulty indexing, the cryst.hkl families 
% input list should be restricted to those that are expected to be detected
% with the given setup.

% Make sure plane normals and direction vectors are normalized 
tot.pl  = tot.pl./repmat(sqrt(sum(tot.pl.*tot.pl,2)),1,3);
tot.dir = tot.dir./repmat(sqrt(sum(tot.dir.*tot.dir,2)),1,3);

% Find the linear hkl family index (like thetatype but not using thetatype
% in case it's not known for a reflection or is not a continous series 1..n)
for ii = 1:nof_refs
    
    % If a reflection hkl family is not known
    if tot.thetatype(ii) == 0 
        
        % theta tolerance
        toltheta = parameters.match.thr_theta + ...
            tot.theta(ii)*parameters.match.thr_theta_scale;
        
        % Find hkl families that are close
        tot.hklind{ii} = find(abs(tot.theta(ii) - cryst.theta) <= toltheta);

        if isempty(tot.hklind{ii})
            error('No {hkl} family found for pair #%d',tot.pairid(ii))
        end
        
        if length(tot.hklind{ii})==1
            tot.hkl(ii,:)  = cryst.hkl(:,tot.hklind{ii});
        end
       
    % hkl family is known  
    else
        tot.hklind{ii} = find(tot.thetatype(ii) == cryst.thetatype);
        tot.hkl(ii,:)  = cryst.hkl(:,tot.hklind{ii});
    end
        
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Rodrigues space coordinates
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fprintf('\nGenerating Rodrigues space coordinates ...\n')
tic

cryst.Rfzone_acc = gtCrystRodriguesFundZone(cryst.spacegroup, 0);
cryst.Rfzone_ext = gtCrystRodriguesFundZone(cryst.spacegroup, ...
                   parameters.index.strategy.rfzext);


% List of possible signed hkl-s for each hkl family

cryst.shklfound_template = cell(length(cryst.thetatype),1);
shklfam = cell(1,length(cryst.thetatype));

for ii = 1:length(cryst.thetatype)
    inds = (cryst.thetatypesp == cryst.thetatype(ii));
    shklfam{ii} = cryst.hklsp(:,inds)';
    
    % Cell for storing the local indices of indexed pairs in a grain 
    % according to their signed hkl (cell in a cell)
    cryst.shklfound_template{ii} = cell(sum(inds),1);  
end

cryst.shklfam = shklfam;


% Calculate all possible Rodrigues space lines and intersections with 
% fundamental zone for each reflection 

Rline    = cell(nof_refs,1);  % lines in Rodrigues space
Rhklind  = cell(nof_refs,1);  % hkl linear indices of possible plane families
Rshklind = cell(nof_refs,1);  % signed hkl linear indices

% Copy into non-structure variable (required before using it in a parfor
% loop)
hklind = tot.hklind;
pl     = tot.pl;
Bmat   = cryst.Bmat;
fzone  = cryst.Rfzone_ext;

parfor ii = 1:nof_refs

    shkllist = vertcat(shklfam{hklind{ii}});
   
    % Collect hkl and shkl indices in cell arrays for each line
    for jj = 1:length(hklind{ii})
        Rhklind{ii} = [Rhklind{ii}; ...
            repmat(hklind{ii}(jj), [size(shklfam{hklind{ii}(jj)},1),1])];

        Rshklind{ii} = [Rshklind{ii}; (1:size(shklfam{hklind{ii}(jj)},1))'];       
    end
    
    % Here we need to consider both + and - reflections, because we don't
    % know yet which one is the solution and the negative of the plane 
    % normals is not condisered. 
    % Double it for hkl and -h-k-l
    Rhklind{ii}  = [Rhklind{ii}; Rhklind{ii}];
    Rshklind{ii} = [Rshklind{ii}; Rshklind{ii}];
   
    % Keep only those Rodrigues lines that have an intersection with the 
    % fundamental zone.
    [Rline{ii}, ~, ~, ind] = ...
        gtCrystRodriguesVector(pl(ii,:), [shkllist; -shkllist], ...
        Bmat, fzone); 

    Rhklind{ii}  = Rhklind{ii}(ind);
    Rshklind{ii} = Rshklind{ii}(ind);
    
end

tot.Rline    = Rline;
tot.Rhklind  = Rhklind;
tot.Rshklind = Rshklind;

tot.indexed = false(size(tot.pairid));

%%%%%%%%%%%%%%%%%%%%%%%%
%% Save data
%%%%%%%%%%%%%%%%%%%%%%%%
% File name for saving;
fname = sprintf('4_grains/phase_%02d/index_input.mat',phaseID);
save(fname,'tot','cryst')

fprintf('\nInput data saved in:\n %s\n',fname)

fprintf('\nFinished generating input data.\n')
toc


end % of function