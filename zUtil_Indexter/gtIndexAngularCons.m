function uniangles = gtIndexAngularCons(f1,f2,cryst)
%
% FUNCTION uniangles = gtIndexAngularCons(f1,f2,cryst)
%
% Given two lattice plane families, returns a unique list of
% theoretical angles between plane normals of the two types in real space.
% The lattice is considered undeformed.
%
% INPUT
%   f1                  - first family index to consider in hkltypes
%   f2                  - second family index to consider in hkltypes
%   cryst.hklsp         - as in parameters.cryst()
%   crsyt.thetatype
%   crsyt.thetatypesp
%   cryst.Bmat
%
% OUTPUT
%   uniangles - unique list of angles in radians
%

% Tolerance for uniqueness in degrees
TOL = 1e-12; 

% Thetatypes (not assuming that they go from 1..n)
th1 = cryst.thetatype(f1);
th2 = cryst.thetatype(f2);

% Indices of hklsp which belong to the given families
spind1 = (cryst.thetatypesp == th1);
spind2 = (cryst.thetatypesp == th2);

% Transform hkl-s to cartesian
v1 = gtCrystHKL2Cartesian(cryst.hklsp(:,spind1),cryst.Bmat)';
v2 = gtCrystHKL2Cartesian(cryst.hklsp(:,spind2),cryst.Bmat)';
    

% Loop through all reflection combinations and calculate angles:

n1 = size(v1,1);
n2 = size(v2,1);

angles = zeros(n1*n2,1);

for ii = 1:size(v1,1)
	angles((ii-1)*n2+1:ii*n2) = gtMathsVectorsAnglesRad(v1(ii,:), v2, 0) ;
end

% Unique angles with a tolerance
uniangles = gtMathsUniqueTol(angles, TOL, 0);



