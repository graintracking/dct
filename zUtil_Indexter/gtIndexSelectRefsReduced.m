function refssel = gtIndexSelectRefsReduced(keeplist, refs)
% GTINDEXSELECTREFSREDUCED Helper to load a sub-set of reflections.
%
% refssel = gtIndexSelectRefsReduced(keeplist, refs)
%
% --------------------------------------------------
%
% A helper function to load the sub-set 'keeplist' of reflection data from 
% the complete set 'refs'.
%
% INPUT
%   keeplist  - linear or logical indices of reflections to be selected
%   refs      - reflection data
%
% OUTPUT
%   refssel   - selected sub-set of reflection data
%


refssel.id     = refs.id(keeplist);
refssel.int    = refs.int(keeplist);
refssel.bbxs   = refs.bbxs(keeplist);
refssel.bbys   = refs.bbys(keeplist);

refssel.ca     = refs.ca(keeplist,:);
refssel.dir    = refs.dir(keeplist,:);
refssel.pl     = refs.pl(keeplist,:);

refssel.hklind = refs.hklind(keeplist);   % cell vector

% hack for poly stuff
if isfield(refs, 'g3d')
    refssel.g3d    = refs.g3d(keeplist, :);
end

end