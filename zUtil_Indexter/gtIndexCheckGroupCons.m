function groupcons = gtIndexCheckGroupCons(tryref, baserefs, baseinter, ...
                                           tol, samenv, ACM)
%
% FUNCTION  groupcons = gtIndexCheckGroupCons(tryref, baserefs, ...
%                                             baseinter, tol, samenv, ACM)
%
% Checks group consistency of a given reflection against a group of 
% consistent reflections. Thus it tells if the group remains consistent 
% by adding this one extra reflection.
%
% INPUT
%   tryref.    - the reflection to be tested
%   baserefs.  - a consistent group of reflections
%   baseinter  - intersection point of the diffraction paths of the 
%                baserefs set in real space
%   tol.       - tolerances
%   samenv     - sample envelope
%   ACM        - angular consistency matrix
%
% OUTPUT
%   ok         - true if group consistency is kept, false otherwise
%

groupcons = false;

% check if tryref is pair consistent with all baserefs
if gtIndexCheckAllPairCons(tryref, baserefs, tol, samenv, ACM);
    
    % andy - can hack this a bit to treat lines, from which we have an estimate
    % of grain position?
    if ~isfield(tryref, 'g3d')
        % check if tryref path is close to the base intersection in real space
        if gtMathsPointLinesDists(baseinter, [tryref.ca tryref.dir]) < tol.dist;
            groupcons = true;
        end
    else
        if sqrt(baseinter*tryref.g3d') < tol.dist
            groupcons = true;
        end
    end
    
end


end % of function