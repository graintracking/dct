function ok = gtIndexIsPointInSample(pc, samenv)
%
% FUNCTION  ok = gtIndexIsPointInSample(pc, samenv)
%
% Gives a vector of logicals which is true where the given points fall
% inside the specified sample envelope (that is usually a cylinder 
% irradiated with the beam during part of or the entire scan).
%
% INPUT   pc          - [X,Y,Z] coordinates of the points (nx3)
%         samenv.rad  - sample radius
%         samenv.top  - intersection point of envelope top plane and axis
%         samenv.bot  - intersection point of envelope bottom plane and axis
%         samenv.axis - direction of the axis of the envelope cylinder
%                       (perpendicular to the top and bottom planes)
%
%    All the input parameters must be in the same reference and units.
%
% OUTPUT  ok          - true if the given point falls inside the envelope
%


vectop = [pc(:,1)-samenv.top(1), pc(:,2)-samenv.top(2), pc(:,3)-samenv.top(3)];
vecbot = [pc(:,1)-samenv.bot(1), pc(:,2)-samenv.bot(2), pc(:,3)-samenv.bot(3)];

dotprotop = vectop*samenv.axis';
dotprobot = vecbot*samenv.axis';

% Check if points fall between the top and bottom planes
ok1 = (sign(dotprotop) == -1*sign(dotprobot));


% Check if distance from axis is smaller than the radius
veclat = vectop - dotprotop*samenv.axis;

ok2 = sqrt(sum(veclat.*veclat,2)) <= samenv.rad;


ok = ok1 & ok2;


end % of function
