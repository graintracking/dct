function prep_strategy = gtIndexDefaultStrategy()
% GTINDEXDEFAULTSTRATEGY  Stores and returns the default parameter values 
%                         for the indexing strategy.
%     prep_strategy = gtIndexDefaultStrategy()
%     -----------------------------------


% No. of iterations (linear extension of tolerances from 'beg' to 'end'):
prep_strategy.iter = 5;

% Absolute extension to the Rodrigues fundamental zone
prep_strategy.rfzext = 0.005; 

% Tolerances for building grains in the first iteration:
prep_strategy.b.beg.ang     = 0.05;  % angular difference in degrees
prep_strategy.b.beg.int     = 5;     % max. intensity ratio
prep_strategy.b.beg.bbxs    = 1.3;   % max. bbox x size ratio
prep_strategy.b.beg.bbys    = 1.3;   % max. bbox y size ratio
prep_strategy.b.beg.distf   = 0.1 ;  % size factor for max. distances between diff. paths
prep_strategy.b.beg.distmax = 0.002; % max. absolut limit for distances between diff. paths in lab unit (mm)
prep_strategy.b.beg.ming    = 5;     % min. no. of Friedel pairs in a grain  

% Tolerances for building grains in the last iteration:
prep_strategy.b.end.ang     = 1;
prep_strategy.b.end.int     = 50;
prep_strategy.b.end.bbxs    = 3;
prep_strategy.b.end.bbys    = 2;
prep_strategy.b.end.distf   = 0.3;
prep_strategy.b.end.distmax = 0.02;
prep_strategy.b.end.ming    = 4;

% Tolerances for merging grains in the first iteration:
prep_strategy.m.beg.bbxs    = prep_strategy.b.beg.bbxs; % max. ratio of average bbox x sizes
prep_strategy.m.beg.bbys    = prep_strategy.b.beg.bbys; % max. ratio of average bbox y sizes
prep_strategy.m.beg.int     = prep_strategy.b.beg.int;  % max. ratio of average intensities
prep_strategy.m.beg.distf   = 0.5;  % size factor for maximum distance between grain centers
prep_strategy.m.beg.distmin = 0;    % min. limit for distance tolerance in lab unit (mm)
prep_strategy.m.beg.distmax = Inf;  % max. limit for distance tolerance in lab unit (mm)
prep_strategy.m.beg.angf    = 0;    % max. angular deviation (std)
prep_strategy.m.beg.angmin  = 1;    % min. limit for angular tolerance in angles
prep_strategy.m.beg.angmax  = 1;    % max. limit for angular tolerance in angles

% Tolerances for merging grains in the last iteration:
prep_strategy.m.end.bbxs    = prep_strategy.b.end.bbxs;
prep_strategy.m.end.bbys    = prep_strategy.b.end.bbys;
prep_strategy.m.end.int     = prep_strategy.b.end.int;
prep_strategy.m.end.distf   = 0.5; 
prep_strategy.m.end.distmin = 0;   
prep_strategy.m.end.distmax = Inf; 
prep_strategy.m.end.angf    = 0;
prep_strategy.m.end.angmin  = 1.5; 
prep_strategy.m.end.angmax  = 1.5; 

% Tolerance for statistical fitting of unindexed reflections to grains
prep_strategy.s.stdf = 4;     % limit in std to accept a new reflection

% Tolerance for statistical rejection of indexed reflections from grains
prep_strategy.x.stdf = 4;     % limit in std to consider a reflection as outlier

end % end of function


