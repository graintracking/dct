function gtINDEXDrawGrainUnitCells(grains, parameters, phaseid, type, numbered, hlight, strainscale)
%     gtINDEXDrawGrainUnitCells(grains, parameters, phaseid, type, numbered, hlight, strainscale)
%     -------------------------------------------------------------------------------------------
%
%     Draws a figure representing grain size, location and orientation by  
%     cubes or hexagonal prismes based on the their Rodrigues vectors.
%
%     INPUT:
%       grains      = grain structure from indexing
%       parameters  = as in parameters file (required field: labgeo)
%       phaseid     = phase number <int> {1}
%       type        = cell type {'cubic'} / 'hexagonal'
%       numbered    = if true, the grain id-s for each grain are shown
%       hlight      = grain id-s to be highlighted
%       strainscale = a multiplicative factor for the strain components
%                     for better visibility (if strain data available)
%
%     Version 001 28-06-2012 by LNervo
%       Modified gtINDEXDrawGrainCubes : cubes or hexagons can be drawn

if (~exist('phaseid', 'var') || isempty(phaseid))
    phaseid = 1;
end

if (~exist('grains', 'var') || isempty(grains))
    ind_path = fullfile('4_grains', sprintf('phase_%02d', phaseid), 'index.mat');
    fprintf('Loading grain data from %s ...', ind_path)
    tmp = load(ind_path, 'grain');
    grains = tmp.grain;
end

if (~exist('parameters', 'var') || isempty(parameters))
    disp('Loading parameters from parameters.mat ...')
    parameters = gtLoadParameters();
end

if (~exist('type', 'var') || isempty(type))
    type = parameters.cryst(phaseid).crystal_system;
end

if (~exist('hlight', 'var') || isempty(hlight))
    hlight = [];
end

if (~exist('numbered', 'var') || isempty(numbered))
    numbered = [];
end

if (~exist('strainscale', 'var') || isempty(strainscale))
    strainscale = 50;
end

if (~strcmpi(type, 'cubic') && ~strcmpi(type, 'hexagonal'))
    type = 'cubic';
    disp('Hard coded : cubic unit cells')
end

sample.rad = parameters.labgeo.samenvrad;
sample.bot = parameters.labgeo.samenvbot;
sample.top = parameters.labgeo.samenvtop;

labgeo = parameters.labgeo;
if (~isfield(labgeo, 'omstep') || isempty(labgeo.omstep))
    labgeo.omstep = gtAcqGetOmegaStep(parameters);
end
if (~isfield(parameters, 'detgeo') || isempty(parameters.detgeo))
    parameters.detgeo = gtGeoConvertLegacyLabgeo2Detgeo(labgeo);
end
detgeo = parameters.detgeo;

nof_grains = length(grains);

fs = 20;
samradmargin = 0.08;%0.05
samtopmargin = 0.4;%0.2
pixelsize = (detgeo.pixelsizeu + detgeo.pixelsizev)/2;

if (isfield(parameters.labgeo, 'sourcepoint'))
    pixelsize = pixelsize * parameters.labgeo.sourcepoint(1)/(detgeo.detrefpos(1));
    disp('applying projection magnification')
end

figure();

colormap(jet(1000))
caxis([-0.01 0.01])
cb = colorbar('FontSize', fs, 'ytick', -0.01:0.005:0.01);
%cb=colorbar('ytick', -0.01:0.002:0.01);
%cbpos=get(cb, 'Position');
cbpos = [0.01 0.2 0.025 0.6];
set(cb, 'Position', cbpos);

set(gca, 'xtick', [], 'xcolor', 'w')
set(gca, 'ytick', [], 'ycolor', 'w')
set(gca, 'ztick', [], 'zcolor', 'w')

%set(gca, 'Position', [0.1 0 0.9 1])
set(gca, 'Position', [0 0 1 1])

view(3);

axhor = 0.01*floor((sample.rad*(1+samradmargin))/0.01);
axver = 0.01*floor((max(abs(sample.top), abs(sample.bot))*(1+samtopmargin))/0.01);
%axis([-axhor axhor -axhor axhor -axver axver])

%xlabel('X')
%ylabel('Y')
%zlabel('Z')
hold on

get(gca, 'CameraPosition');
get(gca, 'CameraViewAngle');

t = 1:360;
circx = cosd(t)*sample.rad;
circy = sind(t)*sample.rad;
circbotz(1:360) = sample.bot;
circtopz(1:360) = sample.top;

plot3(circx, circy, circbotz, 'k-', 'Linewidth', 1);
plot3(circx, circy, circtopz, 'k-', 'Linewidth', 1);

plot3([0 0], [0 0], [sample.bot sample.top], 'k.-.', 'Linewidth', 1);

plot3([-sample.rad -sample.rad], [0 0], [sample.bot sample.top], 'k.-', 'Linewidth', 1);
plot3([0 0], [-sample.rad -sample.rad], [sample.bot sample.top], 'k.-', 'Linewidth', 1);
plot3([sample.rad sample.rad], [0 0], [sample.bot sample.top], 'k.-', 'Linewidth', 1);
plot3([0 0], [sample.rad sample.rad], [sample.bot sample.top], 'k.-', 'Linewidth', 1);


% beam direction:
% plot3([-170 170], [0 0], [0 0], 'r-', 'Linewidth', 2)

vertices = [];
faces_sides = [];
faces_end = [];
hkl = [];
if ~strcmpi(type, 'hexagonal')
    vertices  = [0 0 0; 1 0 0; 1 1 0; 0 1 0; 0 0 1; 1 0 1; 1 1 1; 0 1 1] - ...
                 0.5*repmat([1 1 1], 8, 1);
    % Cubed face directions
    %             +yz      -yz      -xz       +xz       +xy      -xy
    %             +x       -x       -y        +y        +z       -z
    faces_sides = [2 3 7 6; 4 1 5 8; 1 2 6 5;  3 4 8 7];
    faces_end   = [5 6 7 8; 1 2 3 4];
    hkl         = [1 0 0; -1 0 0; 0 -1 0; 0 1 0; ...
                   0 0 1; 0 0 -1]';
else
    a = parameters.cryst(phaseid).latticepar(1);
    c = parameters.cryst(phaseid).latticepar(3);
    maxx = max(a, c);
    % normalize the biggest axis to 1
    vertices = gtHexagonalUnitCell(a/maxx, c/maxx, false);
    % hexagon face directions
    %                   +yz      -yz        -xz       +xz                          +xy                -xy
    %                   +x       -x         -y        +y                           +z                 -z
    faces_sides = [1 2 8 7; 4 5 11 10; 5 6 12 11; 2 3 9 8; 3 4 10 9; 6 1 7 12];
    faces_end   = [1 2 3 4 5 6; 7 8 9 10 11 12];
    hkl         = [1 0 -1 0; -1 0 -1 0; 0 -1 1 0; 0 1 -1 0; -1 1 0 0; 1 -1 0 0; ...
                   0 0 0 1; 0 0 0 -1]'; % column vectors
end

Bmat = gtCrystHKL2CartesianMatrix(parameters.cryst(phaseid).latticepar);
pl_cryst = gtCrystHKL2Cartesian(hkl, Bmat); % hkl plane normals in cryst reference


for ii = 1:nof_grains

    if ismember(ii, hlight)
        hl = true;
    else
        hl = false;
    end

    sfPlotGrainCell(grains{ii}, vertices, faces_sides, hl, strainscale, pixelsize);
    sfPlotGrainCell(grains{ii}, vertices, faces_end, hl, strainscale, pixelsize);

    if numbered
        text(grains{ii}.center(1), -axhor, grains{ii}.center(3), num2str(ii), 'Color', 'c');
        text(axhor, grains{ii}.center(2), grains{ii}.center(3), num2str(ii), 'Color', 'c');
    end
end

axis equal
axis([-axhor axhor -axhor axhor -axver axver]);

end % of main function

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SUB-FUNCTIONS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function sfPlotGrainCell(grain, vertices, faces, hl, strainscale, pixelsize)

    ga = 0.5 * pixelsize * (grain.stat.bbxsmean + grain.stat.bbysmean) / 2;

    if (isfield(grain, 'centre'))
        gc = grain.centre;
    else
        gc = grain.center;
    end

    % Compute orientation matrix g (Reminder: Vc = g . Vs)
    g_grain = gtMathsRod2OriMat(grain.R_vector);

    % Express grain vertices in cartesian SAMPLE CS
    grain_vertices = ga * gtVectorCryst2Lab(vertices, g_grain);
    grain_vertices_st = repmat(gc, size(vertices, 1), 1) + (eye(3) * grain_vertices')';

    if isfield(grain, 'strain') && ~any(isnan(grain.strain.strainT(:)))
        grain_vertices_st = grain_vertices_st + (grain.strain.strainT * strainscale * grain_vertices')';
        facecolor         = grain.strain.strainT(3, 3);
    else
        facecolor         = 0;
    end

    facecolor = repmat(facecolor, size(faces, 1), 1);

    if (hl)
        patch('Vertices', grain_vertices_st, 'Faces', faces , ...
              'FaceVertexCData', facecolor, 'FaceColor', 'flat', ...
              'EdgeColor', 'r', 'LineWidth', 2);
    else
        patch('Vertices', grain_vertices_st, 'Faces', faces , ...
              'FaceVertexCData', facecolor, 'FaceColor', 'flat');
    end
end
