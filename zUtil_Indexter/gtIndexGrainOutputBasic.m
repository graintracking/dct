function [grain, goodind] = gtIndexGrainOutputBasic(grainrefs, cryst, tol_ang)
% GTINDEXGRAINOUTPUTBASIC Computes basic grain data from a set of
% reflections.
%
% [grain, goodind] = gtIndexGrainOutputBasic(grainrefs, cryst, tol_ang)
%
% ---------------------------------------------------------------------
%
% Creates the basic 'grain' data structure from reflections 'grainrefs'. 
% Used when a new grain is created or an existing one is modififed.
% It performs an orientation test in Rodrigues space on the reflections,
% identifies the ones which are not compatible with the majority that form
% a grain. The good reflections are listed in 'goodind' and the grain's 
% orientation is then determined. Angular deviations from the expected 
% theoretical directions of the specific reflections are calculated 
% in real space. Average grain parameters and grain statistics are 
% computed. 
%
% INPUT
%  grainrefs.   - grain reflection data
%  cryst.       - crystallography and other data
%  tol_ang      - max. angular orientation deviation between two grains
%                 to be merged (in degrees)
%
% OUTPUT
%  grain        - grain data
%  goodind      - reflections linear indices in input that have been 
%                 accepted in the grain
%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Orientation test in Rodrigues space
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Distance tolerance in Rodrigues space is set as a constant. Although the 
% Rodrigues space is not Eucledean, it is a good approximation corresponding 
% to the angular tolerance in real space (at least when the fundamental 
% zone is confined).
tol_Rdist = tand(tol_ang/2);

% Determine orientation as a Rodrigues vector for grain

rline   = [];
refind  = [];
lineind = [];

for ii = 1:length(grainrefs.id)
    
    % If signed hkl unknown, use all possible lines
    if isnan(grainrefs.shklind(ii))
        
        % rodrigues lines
        rline = [rline; grainrefs.Rline{ii}];
        
        % Reflection indices to identify rlines
        refind  = [refind; ii(ones(size(grainrefs.Rline{ii},1),1))];
        lineind = [lineind; (1:size(grainrefs.Rline{ii},1))'];
        
    % If signed hkl is known
    else
        
        % Find which Rodrigues line to use
        rlineind = find(((grainrefs.Rhklind{ii}==grainrefs.hklind{ii}) & ...
            (grainrefs.Rshklind{ii}==grainrefs.shklind(ii))));
        
        if ~isempty(rlineind)
            rline   = [rline; grainrefs.Rline{ii}(rlineind,:)];
            refind  = [refind; ii(ones(length(rlineind),1))];
            lineind = [lineind; rlineind];
        end
        
    end
end

% Rodrigues test
%   'goodind' will be a unique list of the good reflection indices from
%   'refind'
[Rvec, goodind, linesok, ~, Ronedge] = ...
    gtCrystRodriguesTestCore(rline, refind, cryst.Rfzone_acc, ...
    cryst.Rfzone_ext, tol_Rdist);

if isempty(Rvec)
    grain = [];
    return 
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Check angular deviation in real space
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% hkl-s and signed hkl-s
hklind  = NaN(length(goodind),1);
shklind = NaN(length(goodind),1);
shkl    = NaN(length(goodind),size(cryst.hklsp,1));

for ii = 1:length(goodind)
    hklind(ii)  = grainrefs.Rhklind{goodind(ii)}(lineind(linesok(ii)));
    shklind(ii) = grainrefs.Rshklind{goodind(ii)}(lineind(linesok(ii)));
    shkl(ii,:)  = cryst.shklfam{hklind(ii)}(shklind(ii),:);
end

% Compute orientation matrix g
g = gtMathsRod2OriMat(Rvec);

% Normalized signed hkl vectors in cartesian CRYSTAL coordinates
shkldirs_cry = gtCrystHKL2Cartesian(shkl', cryst.Bmat)';

% Express normalized signed hkl vectors in cartesian SAMPLE CS
shkldirs = gtVectorCryst2Lab(shkldirs_cry, g);

% Compute the dot products (all vectors must be normalized)
dotprod = abs(sum(shkldirs.*grainrefs.pl(goodind,:),2));

% Avoid numerical errors
dotprod(dotprod>1) = 1;

% Angular deviation from expected plane normal direction in degrees
dang = acosd(dotprod);

% Check if angular deviations are within the limit
%angok = (dang <= tol_ang);
% This is not needed, due to the way how the constant tol_Rdist was chosen,
% which is a conservative estimate. The angular deviations in real space 
% are probably smaller than the tol_ang limit.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Create grain output
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

grain.id         = NaN;                      
grain.refid      = grainrefs.id(goodind)';     % reflection id-s used in indexing
grain.pairid     = grainrefs.pairid(goodind)'; % Friedel pair IDs from matching

grain.R_vector   = Rvec;                     % Rodrigues vector
grain.R_onedge   = Ronedge;                  % Rodrigues vector is close to
                                             % edge of fundamental zone

grain.nof_pairs  = length(goodind);          % no. of pairs in grain

grain.pl         = grainrefs.pl(goodind,:);  % plane normals
grain.hkl        = grainrefs.hkl(goodind,:); % hkl families
grain.theta      = grainrefs.theta(goodind); % theta
grain.int        = grainrefs.int(goodind);   % intensity
grain.bbxs       = grainrefs.bbxs(goodind);  % bbox x size
grain.bbys       = grainrefs.bbys(goodind);  % bbox y size

grain.hklind     = hklind;            % unsigned hkl family linear index
grain.shklind    = shklind;           % signed hkl linear index
grain.hklsp      = NaN;

% Angular deviation
grain.dang = dang;

% andy - can hack this a bit to treat lines, from which we have an estimate
% of grain position?
if ~isfield(grainrefs, 'g3d')
% Centre of mass of grain in Sample coordinates
grain.center = gtMathsLinesIntersection(...
               [grainrefs.ca(goodind,:) grainrefs.dir(goodind,:)])';

% Distance between grain center of mass and diff. paths
grain.dcom = gtMathsPointLinesDists(grain.center, ...
             [grainrefs.ca(goodind,:) grainrefs.dir(goodind,:)]);
else
    % apply the "goodind" to g3d 
    grain.g3d       = grainrefs.g3d(goodind,:);
    grain.center = mean(grain.g3d, 1);
    grain.dcom = sqrt(sum((grain.g3d - repmat(grain.center, size(grain.g3d, 1), 1)).^2, 2));
end

grain.indST   = zeros(3);  % strain tensor in indexing
grain.normint = zeros(size(grain.int));

% List of shkl-s that have been indexed
grain.shklfound = cryst.shklfound_template;  % blank template

for ii = 1:length(grain.hklind)
    grain.shklfound{grain.hklind(ii)}{grain.shklind(ii)} = ...
        [grain.shklfound{grain.hklind(ii)}{grain.shklind(ii)}, ii];
    
    grain.normint(ii) = grain.int(ii)/cryst.int(grain.hklind(ii));

end


% Status in indexing
grain.merged     = NaN;
grain.extended   = NaN;
grain.refindexed = NaN;


% Grain statistics

grain.stat.dangmean    = mean(dang);          % mean angular deviation
grain.stat.dcommean    = mean(grain.dcom);    % mean distance between COM and diff. paths
grain.stat.bbxsmean    = mean(grain.bbxs);    % mean bbox x size
grain.stat.bbysmean    = mean(grain.bbys);    % mean bbox y size
grain.stat.intmean     = mean(grain.int);     % mean intensity
grain.stat.normintmean = mean(grain.normint); % mean of normalised intensity

% Angular deviation and real space distance. Only the absolute values are measured. 
% These give the absolute mean deviation of the errors which is about 0.8*std in case
% of a normal distribution. 
grain.stat.dangstd = grain.stat.dangmean/0.8; % std of angular deviations
grain.stat.dcomstd = grain.stat.dcommean/0.8; % std of distances between COM and diff. paths

grain.stat.bbxsstd    = std(grain.bbxs); % std of bbox x sizes
grain.stat.bbysstd    = std(grain.bbys); % std of bbox y sizes
grain.stat.intstd     = std(grain.int);  % std of intensities
grain.stat.normintstd = std(grain.int);  % std of normalised intensities


end % end of function

