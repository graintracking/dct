
function [cons, angles] = gtIndexCheckAddRefs(tryrefs, grain, tol, tolmode)

% Checks if reflections in 'tryrefs' can be added to any of the grains 
% in 'grain' based on the tolerance limits in 'tol'.
% Bounding box sizes, intensity, diffraction path distance and angular 
% deviation are considered. The average values in grain.stat are used, 
% deviations from those values are accpeted within the limits set in 'tol'.
% If 'tolmode' is 'tols', the tolerance values are fixed.
% If 'tolmode' is 'std', the tolerance values are calculated as a multiple 
% of the standard deviations in grain.stat.

% Hint for improvement: could use more grain statistics to revise the actual
% tolerances. Not sure it'd be better.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  Set tolerance limits
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if strcmp(tolmode,'tols')
	% Here reusing tolerance values set for Build Grains. They indicate 
    % the ratio of (upper boundary)/(lower boundary) for the expected normal
	% distribution (ie. if v is the lower limit, v*tol is the upper limit).
	% The mean value of the distribution is at (v+v*tol)/2 .
	% Therefore here, when knowing the mean value of the distribution, we use
	%   mean*2/(1+tol)      to determine the lower boundary
	%   mean*2/(1+tol)*tol  to determine the upper boundary
	
	lim.lowbbxs  = grain.stat.bbxsmean/(tol.bbxs+1)*2 ;
	lim.highbbxs = grain.stat.bbxsmean/(tol.bbxs+1)*2*tol.bbxs ;
	
    lim.lowbbys  = grain.stat.bbysmean/(tol.bbys+1)*2 ;
	lim.highbbys = grain.stat.bbysmean/(tol.bbys+1)*2*tol.bbys ;
	
    lim.lowint   = grain.stat.intmean/(tol.int+1)*2 ;
	lim.highint  = grain.stat.intmean/(tol.int+1)*2*tol.int ;

    % Normalised intensity should be used in future
    %lim.lowint   = grain.stat.normintmean/(tol.int+1)*2 ;
	%lim.highint  = grain.stat.normintmean/(tol.int+1)*2*tol.int ;

	% The mean value of the error distribution in case of the angle and
	% distance, imagining signed values, is zero. The observed values are the
	% absolute values, therefore only one limit is used.
	lim.dcom     = tol.dist/2 ;
	lim.dang     = tol.ang/2 ;
	
elseif strcmp(tolmode,'std')
	% Here a constant times the standard deviations are used to
	% determine the limits of the accepted range.
	
	lim.lowbbxs  = grain.stat.bbxsmean - tol.stdf*grain.stat.bbxsstd ;
	lim.highbbxs = grain.stat.bbxsmean + tol.stdf*grain.stat.bbxsstd ;
	
    lim.lowbbys  = grain.stat.bbysmean - tol.stdf*grain.stat.bbysstd ;
	lim.highbbys = grain.stat.bbysmean + tol.stdf*grain.stat.bbysstd ;
	
    lim.lowint   = grain.stat.intmean  - tol.stdf*grain.stat.intstd ;
	lim.highint  = grain.stat.intmean  + tol.stdf*grain.stat.intstd ;

    % Normalised intensity should be used in future
    %lim.lowint   = grain.stat.normintmean  - tol.stdf*grain.stat.normintstd ;
	%lim.highint  = grain.stat.normintmean  + tol.stdf*grain.stat.normintstd ;
   
    
	% The mean value of the error distribution in case of the angle and
	% distance, imagining signed values, is zero. The observed values are the
	% absolute values, therefore only one limit is used.
	lim.dcom     = tol.stdf*grain.stat.dcomstd ;
	lim.dang     = tol.stdf*grain.stat.dangstd ;
	
else
	error('Tolerance mode not recognized.')
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  Check intensity and bounding box sizes
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

angles = [];


% initial consistency list
cons = (1:length(tryrefs.id))'; 


% Bbox y size close enough?
oks        = (lim.lowbbys < tryrefs.bbys) & (tryrefs.bbys < lim.highbbys);
cons(~oks) = [];
if isempty(cons)
	return
end


% Bbox x size close enough?
oks        = (lim.lowbbxs < tryrefs.bbxs(cons)) & ...
             (tryrefs.bbxs(cons) < lim.highbbxs);
cons(~oks) = [];
if isempty(cons)
	return
end


% Integrated intensities close enough?
oks        = (lim.lowint < tryrefs.int(cons)) & ...
             (tryrefs.int(cons) < lim.highint);
cons(~oks) = [];
if isempty(cons)
	return
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  Check distance
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Diffraction path distance small enough?
oks        = gtMathsPointLinesDists(grain.center, ...
             [tryrefs.ca(cons,:) tryrefs.dir(cons,:)]) < lim.dcom ;
cons(~oks) = [];
if isempty(cons)
	return
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  Check orientation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Compute the dot product of all vector combinations (grain.pl-s must be
% normalized), this shows how collinear they are
dotprods = grain.shkldirs*tryrefs.pl(cons,:)';

% Handle numerical errors
dotprods(dotprods>1) = 1;

% Find maximum collinearity. Max and min are needed to tackle both spots in
% a pair. At this stage we don't care which spot in a pair represents the 
% plus or minus direction of the given reflection.
maxdotprods = max(abs(dotprods),[],1)';

% Check against angular tolerance
% Tolerance value tol_b refers to angle between any two plane normals, thus
% here tol_b/2 is used when checking against a mean orientation
oks = maxdotprods > cosd(lim.dang) ;
cons(~oks) = [];

if nargout==1
	return
end

maxdotprods = maxdotprods(oks);

% angular deviation from expected plane normal direction
angles = acosd(maxdotprods);


