function grain = gtIndexFwdSimGrains(grain, parameters, standardOutput, fsimID, saveFlag)
% GTINDEXFWDSIMGRAINS  Forward simulates the spot positions for all
%                      reflections in all grains
%
%     grain = gtIndexFwdSimGrains(grain, [parameters], [standardOutput], [fsimID], [saveFlag])
%     ----------------------------------------------------------------------------------------
%     To be run after INDEXTER.
%     The output will be depending on the 'standardOutput' value:
%       outname = 'fwdsim' (if false) or 'allblobs' (if true)
%     The updated grain will have the result of forward simulation in
%       grain.(outname)(fsimID)
%
%     INPUT:
%       grain          = <cell>     all the grains as in index.mat
%       parameters     = <struct>   parameters as in the parameters file
%       standardOutput = <logical>  true, if using the standard output name 'allblobs'
%                                   false to use 'fwdsim' {true}
%       fsimID         = <double>   forward simulation ID in case we want to forward simulate
%                                   on more than one detector {1}; it will
%                                   save fsim in grain.(outname)(fsimID)
%       saveFlag       = <logical>  add <outname>/'strain' fields to
%                                   4_grains/phase_##/grain_####.mat files if existing {false}
%
%     OUTPUT:
%       grain          = <cell>     updated grain info
%            .(outname) = <struct>  Added fwd simulation data:
%                      .pl        = <double>   plane normal in SAMPLE reference (strained if occured)
%                      .pllab     = <double>   plane normal in LAB reference (unstrained theoretical)
%                      .hkl       = <double>   unique hkl
%                      .hklsp     = <double>   signed hkl
%                      .thetatype = <double>   thetatype
%                      .theta     = <double>   theta angle
%                      .eta       = <double>   eta diffraction angle in degrees
%                      .omega     = <double>   omega coordinate
%                      .dvec      = <double>   diffraction vectors in LAB ref
%                      .uv        = <double>   UV coordinates on detector
%                      .uvw       = <double>   UVW coordinates on detector
%                      .radiuses  = <double>   radius on detector
%                      .srot      = <double>
%
%                      .omind     = <double>   omega index
%                      .is_nan    = <logical>  true if it is NAN
%                      .sinth     = <double>   sin(theta)/lambda = 0.5*lambda*dsp_d
%                      .dsp_d     = <double>   d-spacing (strained if occured)
%                      .refind    = <double>   linear index of reflection in grain
%                      .used_fam  = <double>   Used HKL families (theoretical)
%            .used_fam            = <double>   Used HKL unique families (theoretical)
%
%     Version 002 22-01-2014 by LNervo
%       Made it equal to gtCalculateGrain, except of proj_geom which is not
%       calculated here. It's calculated inside gtForwardSimulate_v2.

if (~iscell(grain) && length(grain) == 1)
    grain = {grain};
end
if (~exist('parameters', 'var') || isempty(parameters))
    parameters = gtLoadParameters();
end
if (~exist('standardOutput', 'var') || isempty(standardOutput))
    standardOutput = true;
end
if (~exist('fsimID', 'var') || isempty(fsimID))
    fsimID = 1;
end
if (~exist('saveFlag', 'var') || isempty(saveFlag))
    saveFlag = false;
end

if (standardOutput)
    outname = 'allblobs';
    fsimID  = 1; % set to 1 the fsimID if using old fsim structure name
else
    outname = 'fwdsim';
end

labgeo = parameters.labgeo;
if (~isfield(labgeo, 'omstep') || isempty(labgeo.omstep))
    labgeo.omstep = gtAcqGetOmegaStep(parameters);
end
if (~isfield(parameters, 'detgeo') || isempty(parameters.detgeo))
    parameters.detgeo = gtGeoConvertLegacyLabgeo2Detgeo(labgeo);
end
detgeo = parameters.detgeo;

energy = parameters.acq.energy;
samgeo = parameters.samgeo;
cryst  = parameters.cryst(grain{1}.phaseid);

cryst.used_thetatype   = cryst.thetatype;
cryst.used_thetatypesp = cryst.thetatypesp;

cryst.usedfamsp = ismember(cryst.thetatypesp, cryst.used_thetatype);

% Rotation tensor components
rotcomp = gtMathsRotationMatrixComp(labgeo.rotdir', 'col');
% B matrix for transformation from (hkl) to Cartesian Crystal reference
Bmat = gtCrystHKL2CartesianMatrix(cryst.latticepar);

% Coordinates of the plane normals in the Cartesian Crystal system
pl_ref  = gtCrystHKL2Cartesian(double(cryst.hklsp(:, cryst.usedfamsp)), Bmat);
refind0 = 1:size(pl_ref, 2);
% D-spacings in the reference crystal for the given reflections
dsp_ref = cryst.dspacingsp(:, cryst.usedfamsp);

% Rotation tensor describing crystal orientation; calculate all grains
% at once for speed-up.
% (gtCrystRodriguesVector and gtFedRod2RotTensor used together
% gives the transformation tensor from CRYSTAL TO SAMPLE coordinates)
Rvec    = gtIndexAllGrainValues(grain, 'R_vector', [], 1, 1:3);
cry2sam = gtFedRod2RotTensor(Rvec');

% calc radius of u, v coordinate
getradius = @(u, v) sqrt((u - detgeo.detrefu).^2 + (v - detgeo.detrefv).^2);

% loop through grains
for ii = 1:length(grain)

    % Plane normals in the Sample reference
    pl_sam = cry2sam(:, :, ii) * pl_ref;

    % Impose strain on grain
    if (~isfield(grain{ii}, 'strain'))
        grain{ii}.strain.strainT = NaN(3, 3);
    end

    if any(isnan(grain{ii}.strain.strainT(:)))
        %fprintf('No strain info in grain #%d. Using as measured plane normals.\n', ii)
        pl_samd = pl_sam;
        drel    = ones(1, size(pl_sam, 2));
    else
        % New deformed plane normals and relative elongations
        [pl_samd, drel] = gtStrainPlaneNormals(pl_sam, grain{ii}.strain.strainT);
    end

    % d-spacing after applying the strain tensor
    dsp_d = drel .* dsp_ref;

    % Bragg angles theta in the deformed state
    sinth = 0.5 * gtConvEnergyToWavelength(energy) ./ dsp_d;

    % The plane normals need to be brought in the Lab reference where the
    % beam direction and rotation axis are defined.
    % Use the Sample -> Lab orientation transformation assuming omega=0;
    % (vector length preserved for free vectors)
    sam2lab = [samgeo.dirx; samgeo.diry; samgeo.dirz]';
    pl_labd = sam2lab * pl_samd;

    % Predict omega angles: 4 for each (hkl) plane normal (the Friedel pairs are
    % om1-om3 and om2-om4); -(hkl) plane normals are assumed not to be in the list
    [om4, pllab4, plsam4, rot4, omind4] = gtFedPredictOmegaMultiple(...
        pl_labd, sinth, labgeo.beamdir', labgeo.rotdir', rotcomp, []);

    swap13 = find(om4(1, :) > om4(3, :));
    om4(:, swap13) = om4([3 2 1 4], swap13);

    swap24 = find(om4(2, :) > om4(4, :));
    om4(:, swap24) = om4([1 4 3 2], swap24);

    omind4(:, swap13)   = omind4([3 2 1 4], swap13);
    pllab4(:, swap13, :) = pllab4(:, swap13, [3 2 1 4]);
    plsam4(:, swap13, :) = plsam4(:, swap13, [3 2 1 4]);
    rot4(:, :, swap13, :) = rot4(:, :, swap13, [3 2 1 4]);

    omind4(:, swap24)   = omind4([1 4 3 2], swap24);
    pllab4(:, swap24, :) = pllab4(:, swap24, [1 4 3 2]);
    plsam4(:, swap24, :) = plsam4(:, swap24, [1 4 3 2]);
    rot4(:, :, swap24, :) = rot4(:, :, swap24, [1 4 3 2]);

    % Delete those where no reflection occurs
    todel = (isnan(om4(1, :)) | isnan(om4(3, :)));
    om4(:, todel) = [];
    omind4(:, todel) = [];
    sinth(todel) = [];
    pllab4(:, todel, :) = [];
    plsam4(:, todel, :) = [];
    rot4(:, :, todel, :) = [];
    dsp_d(todel) = [];

    refind = refind0(~todel);
    thtype = cryst.thetatypesp(cryst.usedfamsp);
    thtype = thtype(~todel);
    hklsp  = cryst.hklsp(:, cryst.usedfamsp);
    hklsp  = hklsp(:, ~todel);
    hkl    = cryst.hkl(:, thtype);

    % repmat [1 1 1 1 2 2 2 2 3 3 3 3 .... last last last last]
    ind4    = reshape(repmat(1:length(refind), 4, 1), [], 1)';
    sinth4  = sinth(ind4);
    thtype4 = thtype(ind4);
    refind4 = refind(ind4);
    hklsp4  = hklsp(:, ind4);
    hklsp4(:, 2:2:end) = -hklsp4(:, 2:2:end);

    hkl4   = hkl(:, ind4);
    dsp_d4 = dsp_d(ind4);
    theta4 = asind(sinth4);

    % om4 : order is :  [1 3 2 4, 1] [1 3 2 4, 2]. .... [1 3 2 4, last]
    om     = reshape(om4', [], 1)';
    omind  = reshape(omind4', [], 1)';
    rot4   = reshape(rot4, 3, 3, []); % or reshape(rot4, 3, [])
    pllab4 = reshape(pllab4, 3, []);
    plsam4 = reshape(plsam4, 3, []);

    % reordering as 1[1 3 2 4] 2[1 3 2 4] 3[1 3 2 4] .... last[1 3 2 4]
    tmp = [];
    for jj = 1:size(om4, 2)
        tmp(end+1:end+4) = (size(om4, 2) * [0 2 1 3]) + [1 1 1 1] * jj;
    end
    om     = om(tmp);
    omind  = omind(tmp);
    rot4   = rot4(:, :, tmp);
    pllab4 = pllab4(:, tmp);
    plsam4 = plsam4(:, tmp);

    % Diffraction vectors in Lab and Sample
    dveclab = gtFedPredictDiffVecMultiple(pllab4, labgeo.beamdir');
    dvecsam = gtGeoLab2Sam(dveclab', om, labgeo, samgeo, true, 'only_rot', true)';

    % Extend grain center
    gc_sam = grain{ii}.center';
    gc_sam = gc_sam(:, ones(size(dveclab, 2), 1));

    % Absolute detector coordinates U, V in pixels
    uv = gtFedPredictUVMultiple(rot4, dveclab, gc_sam, detgeo.detrefpos', ...
         detgeo.detnorm', detgeo.Qdet, [detgeo.detrefu; detgeo.detrefv]);

    % radius of UV spot on detector from center
    radiuses = getradius(uv(1, :), uv(2, :));

    is_nan = isnan(uv(1, :));

    % Absolute coordinates U, V, W (pixel, pixel, image); to account for
    % measurement errors, do not correct for the image number
    % to be mod(om, 360)/omstep

    % trying to make it similar to gtForwardSimulate_v2
    % prepare output
    grain{ii}.(outname)(fsimID).pl        = plsam4';
    grain{ii}.(outname)(fsimID).pllab     = pllab4';
    grain{ii}.(outname)(fsimID).hkl       = hkl4';
    grain{ii}.(outname)(fsimID).hklsp     = hklsp4';
    grain{ii}.(outname)(fsimID).thetatype = thtype4';
    grain{ii}.(outname)(fsimID).theta     = theta4';
    grain{ii}.(outname)(fsimID).eta       = gtGeoEtaFromDiffVec(dveclab', labgeo);
    grain{ii}.(outname)(fsimID).omega     = om';
    grain{ii}.(outname)(fsimID).dvec      = dveclab';
    grain{ii}.(outname)(fsimID).dvecsam   = dvecsam';
    grain{ii}.(outname)(fsimID).uv        = uv';
    grain{ii}.(outname)(fsimID).uvw       = [uv;(om) / labgeo.omstep]';
    grain{ii}.(outname)(fsimID).radiuses  = radiuses';
    grain{ii}.(outname)(fsimID).srot      = rot4;

    % extra info
    grain{ii}.(outname)(fsimID).omind     = omind';
    grain{ii}.(outname)(fsimID).is_nan    = is_nan';
    grain{ii}.(outname)(fsimID).sintheta  = sinth4';
    grain{ii}.(outname)(fsimID).dsp_d     = dsp_d4';
    grain{ii}.(outname)(fsimID).refind    = refind4';
    grain{ii}.(outname)(fsimID).used_fam  = thtype(ind4)';

    grain{ii}.used_fam                    = unique(thtype);
    grain{ii}.used_famsp                  = thtype;
end % end for loop

% save fsim into grain_####.mat files if wished
if (saveFlag)
    gtFsimUpdate(grain, outname, 'used_fam', 'used_famsp', 'strain', '-append')
end

end
