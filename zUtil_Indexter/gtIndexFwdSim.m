function fwdSim = gtIndexFwdSim(om_mes, gcsam, ~, om4, pllab4, rot4, ...
                omind4, detgeo, labgeo, samgeo)
% fwdSim = gtIndexFwdSim(om_mes, gcsam, sinth, om4, pllab4, rot4, ...
%          omind4, detgeo, labgeo, samgeo, omstep)
% -------------------------------------------------------------------
% Provides the fwd simulation data for a given omega index set.
% /mntdirect/_data_id19_graintracking/DCT_Analysis/RD4_15N_taper_/workspace/grid/
% peaks_t100_s_cleaned_smalls.flt.new

% Absolute differences
dom = om4 - om_mes([1 1 1 1], :);

% Find the indices with the smallest absolute difference; consider
% errors and periodicity of 360deg in omega
[~, ind] = min( abs( [dom; dom - 360; dom + 360] ), [], 1);

% Predicted omega is 360deg more
omhigh = ( (5 <= ind) & (ind <= 8) );

% Predicted omega is 360deg less
omlow  = ( (9 <= ind) & (ind <= 12) );

% Correct indices
ind(omhigh) = ind(omhigh) - 4;
ind(omlow)  = ind(omlow)  - 8;

% Get final omega angles (can be <0) using the corresponding linear
% indices of omind4
linind            = ind + (0:length(ind)-1) * 4;
fwdSim.om         = om4(linind);
fwdSim.om(omhigh) = fwdSim.om(omhigh) - 360;
fwdSim.om(omlow)  = fwdSim.om(omlow)  + 360;

% Omega index
fwdSim.omind = omind4(linind);

% Omegas in a column vector
ind1 = (ind == 1);
ind2 = (ind == 2);
ind3 = (ind == 3);
ind4 = (ind == 4);

% Extract the correct pllab vectors according to omind
fwdSim.pllab         = zeros(3, length(ind));
fwdSim.pllab(:, ind1) = pllab4(:, ind1, 1);
fwdSim.pllab(:, ind2) = pllab4(:, ind2, 2);
fwdSim.pllab(:, ind3) = pllab4(:, ind3, 3);
fwdSim.pllab(:, ind4) = pllab4(:, ind4, 4);

% Extract the correct rotation matrices according to omind
fwdSim.rot           = zeros(3, 3, length(ind));
fwdSim.rot(:, :, ind1) = rot4(:, :, ind1, 1);
fwdSim.rot(:, :, ind2) = rot4(:, :, ind2, 2);
fwdSim.rot(:, :, ind3) = rot4(:, :, ind3, 3);
fwdSim.rot(:, :, ind4) = rot4(:, :, ind4, 4);


% Diffraction vector
fwdSim.dveclab = gtFedPredictDiffVecMultiple(fwdSim.pllab, labgeo.beamdir');
fwdSim.dvecsam = gtGeoLab2Sam(fwdSim.dveclab', fwdSim.om, labgeo, samgeo, true, 'only_rot', true)';

% Make sure grain center is extended
gcsam = gcsam(:, ones(size(fwdSim.dveclab, 2), 1));

% Absolute detector coordinates U, V in pixels
uv = gtFedPredictUVMultiple(fwdSim.rot, fwdSim.dveclab, gcsam, detgeo.detrefpos', ...
     detgeo.detnorm', detgeo.Qdet, [detgeo.detrefu; detgeo.detrefv]);

is_nan = isnan(uv(1, :));

keep = (uv(1, :) >= 1) & (detgeo.detsizeu >= uv(1, :)) & ...
       (uv(2, :) >= 1) & (detgeo.detsizev >= uv(2, :));
% Absolute coordinates U, V, W (pixel, pixel, image); to account for
% measurement errors, do not correct for the image number
% to be mod(om, 360)/omstep
index_original = ismember(om_mes, fwdSim.om);

fwdSim.uvw        = [uv; fwdSim.om/labgeo.omstep];
fwdSim.uvw_nan    = [uv(:, is_nan); fwdSim.om(is_nan)/labgeo.omstep];
fwdSim.keep       = keep;
fwdSim.is_nan     = is_nan;
fwdSim.image      = mod(fwdSim.om, 360)/labgeo.omstep;

end % end of function
