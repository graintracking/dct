function newgrain = gtIndexCreateOutput(grain, strategy_x, pairtable, ...
                    phaseID, cryst, flag_update)
%
% Creates the 'newgrain' output structure after some formatting on 'grain'.
% Assigns the final grain id-s according to approximate size, adds difspot 
% id-s. Reflections in each grain are also reordered.
% It updates the database spotpair table with indexing results.
%
                

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Format grains; get difspot id-s
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                
gtDBConnect();

mysqlcmd = sprintf('SELECT pairid,difAID,difBID FROM %s', pairtable);
[pairid, difAID, difBID] = mym(mysqlcmd);


% Update selected grains
for ii = 1:length(grain); % !!! parfor - this can be a parfor loop
    
    % Format each grain; reorder reflections; find outliers
    grain{ii} = gtIndexGrainOutputFinal(grain{ii}, cryst, strategy_x.stdf);
    
    % Find difspot ID-s for the pairs
    grain{ii}.difspotidA = zeros(1,length(grain{ii}.pairid));
    grain{ii}.difspotidB = zeros(1,length(grain{ii}.pairid));

    for jj = 1:length(grain{ii}.pairid)
        ind = find(grain{ii}.pairid(jj)==pairid, 1);
        
        grain{ii}.difspotidA(jj) = difAID(ind);
        grain{ii}.difspotidB(jj) = difBID(ind);
    end
    
    grain{ii}.phaseid = phaseID;
    
end


% Order grains by approximate volume
bbys = gtIndexAllGrainValues(grain,'stat','bbysmean',1,[]);
bbxs = gtIndexAllGrainValues(grain,'stat','bbxsmean',1,[]);

M(:,1) = 1:length(grain);
M(:,2) = bbys.*bbxs;

M = sortrows(M,-2);

newgrain = cell(1,length(grain));

% Assign new grain id
parfor ii = 1:length(grain)
	newgrain{ii}    = grain{M(ii,1)};
	newgrain{ii}.id = ii;
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Update database
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if flag_update
    
    disp('Deleting previous grainID in spotpair table...')
    mym(sprintf('UPDATE %s SET grainID=null WHERE phasetype=%d', pairtable, phaseID)) ;

    disp('Updating grainID-s in spotpair table...')
    
    for ii = 1:length(newgrain)
        for jj = 1:length(newgrain{ii}.pairid)
            mysqlcmd = sprintf('UPDATE %s SET grainID=%d WHERE pairID=%d',...
                       pairtable,ii,newgrain{ii}.pairid(jj));
            mym(mysqlcmd);
        end
    end
end


end % of function




