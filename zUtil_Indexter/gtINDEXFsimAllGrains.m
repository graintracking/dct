function gr = gtINDEXFsimAllGrains(phaseid, fsimID, saveFlag)
% gr = gtINDEXFsimAllGrains(phaseid, fsimID, saveFlag)

    if (~exist('phaseid', 'var') || isempty(phaseid))
        phaseid = 1;
    end
    if (~exist('fsimID', 'var') || isempty(fsimID))
        fsimID = 1;
    end
    if (~exist('saveFlag', 'var') || isempty(saveFlag))
        saveFlag = false;
    end

    grain = [];
    load(fullfile('4_grains', sprintf('phase_%02d', phaseid), 'index.mat'), 'grain');

    parameters = gtLoadParameters();
    labgeo = parameters.labgeo;
    if (~isfield(labgeo, 'omstep') || isempty(labgeo.omstep))
        labgeo.omstep = gtAcqGetOmegaStep(parameters);
    end
    if (~isfield(parameters, 'detgeo') || isempty(parameters.detgeo))
        parameters.detgeo = gtGeoConvertLegacyLabgeo2Detgeo(labgeo);
    end
    detgeo = parameters.detgeo;

    grain = gtIndexUpdateGrains(grain, parameters.cryst(phaseid).latticepar);
    % add pairs info
    grain = gtIndexAddInfo(grain, parameters.acq, parameters.cryst(phaseid), phaseid);
    % fsim dct indexed spots
    grain = gtIndexFwdSimPairs(grain, detgeo, labgeo, ...
        parameters.samgeo, [], parameters.cryst(phaseid).latticepar, ...
        parameters.acq.energy);

    % load DB and update dct grains
    difspotIDs = gtIndexAllGrainValues(grain, 'difspotID', [], [], [], false);
    gtDBFillUpdateColumn([parameters.acq.name 'difspot'], 'grainID', ...
        'int', 'integral', 'difspotID', difspotIDs, 1:length(grain))

    spots_info = gtDBLoadTable([parameters.acq.name 'difspot'], 'difspotID');
    save('spots_info.mat', 'spots_info', '-v7.3')

    % saveFlag = true to update grain_####.mat with 'fsim' and 'strain'
    grain = gtIndexFwdSimGrains(grain, parameters, fsimID, saveflag);
    % set limits
    lim = gtIndexFwdSimLimits(grain, parameters, true);
    % assign reflections to grains
    [gid, conf, sp] = gtIndexDifSpots_2(grain, spots_info, parameters, fsimID, lim);
    % save difspotIDs
    for ii = 1:length(grain)
        grain{ii}.spotids{fsimID} = sp.id(find(gid == grain{ii}.id))';
    end

    if (saveFlag)
        gtFsimUpdate(grain, 'spotids')
        disp('Saved variable ''spotids'' in fsim grains')
    end

    if (nargout == 1)
        gr = grain;
    end
end

