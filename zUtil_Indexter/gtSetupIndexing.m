function gtSetupIndexing(phase_id)
% GTSETUPINDEXING  Helper function to set the parameters for Indexter.
%     gtSetupIndexing(phase_id)
%     -------------------------

if ~exist('phase_id','var') || isempty(phase_id)
    phase_id = 1;
end

parameters = [];
load('parameters.mat');

check = inputwdefault('Do you want to reset all the parameters for indexing? [y/n]','n');
if strcmpi(check, 'y')
     parameters.index = gtIndexDefaultParameters();
     save('parameters.mat','parameters');
     disp('Indexing parameters have been reset to default values')
end

% Let's check every field is in, and in case let's update it!
parameters = gtCheckParameters(parameters, 'index', 'verbose', true);
save('parameters.mat', 'parameters');

% Use default parameters or review/edit them?
check = inputwdefault(['Use existing parameters for phase #' num2str(phase_id) ' for gtINDEXTER? [y/n]'], 'y');
if strcmpi(check, 'y')

    % Index phase 01 by default
    fname = fullfile('4_grains',sprintf('phase_%02d', phase_id),'index_input.mat');

    if exist(fname,'file') == 2
        disp(['The existing input file for phase #' num2str(phase_id) ' will be used.'])
    else
        disp(['An input file for phase #' num2str(phase_id) ' will be created.'])
    end

    check = inputwdefault('Start gtINDEXTER now? [y/n]', 'y');
    if strcmpi(check, 'y')

        if exist(fname,'file') == 2
            gtINDEXTER(phase_id, true, true); % use input file
        else
            gtINDEXTER(phase_id, true, false); % do not use input file
        end

    end

else

    % Work through the various INDEXTER parameters

    oplist = cell(0,4);
    oplist(end+1,:) = [{'phase_ID'},{'Which phase should be indexed?'},{'double'},{1}];
    oplist(end+1,:) = [{'input_file'},{'Use input file?'},{'logical'},{2}];
    oplist(end+1,:) = [{'flag_update'},{'Update ''parameters.mat'', ''index.mat'', database with the results ?'},{'logical'},{3}];
    oplist(end+1,:) = [{'flag_log'},{'Write log and output file with current date ?'},{'logical'},{3}];
    oplist(end+1,:) = [{'forcemerge'},{'Run INDEXTER only merging grains as written in parameters.index.forcemerge'},{'logical'},{3}];

    
    % Select phase to be indexed
    options = [];
    options.phase_ID    = phase_id;
    options = gtModifyStructure(options, oplist, 1, 'INDEXTER options:');
    % Input file name
    fname = fullfile('4_grains',sprintf('phase_%02d',options.phase_ID),'index_input.mat');

    % Update and log: check existence of file 'fname'
    % TO DO: better handle the existence of input file?
    options.input_file  = exist(fname,'file') == 2;
    options.flag_update = true;
    options.flag_log    = true;
    options.forcemerge  = false;

    if (options.input_file)
        options = gtModifyStructure(options, oplist, [2 3], 'An input file already exists:');
    else
        options = gtModifyStructure(options, oplist, 2, 'The input file does not exist:');
    end

    % Get parameters template
    list = build_list_v2();

    check = inputwdefault(['Use the existing STRATEGY parameters for phase #' num2str(phase_id) '? [y/n]'], 'y');
    if ~strcmpi(check, 'y')

        % Initial parameters
        header = 'General parameters:';
        parameters.index.strategy = gtModifyStructure(parameters.index.strategy, ...
            list.index__strategy, 1, header);

        % Tolerances for building grains in the first iteration:
        header = 'Tolerances for BUILDING grains in the FIRST iteration loop';
        parameters.index.strategy.b.beg = gtModifyStructure(parameters.index.strategy.b.beg,...
            list.index__strategy__b__beg, 1, header);


        % Tolerances for building grains in the last iteration:
        header = 'Tolerances for BUILDING grains in the LAST iteration loop';
        parameters.index.strategy.b.end = gtModifyStructure(parameters.index.strategy.b.end,...
            list.index__strategy__b__end, 1, header);


        % Tolerances for merging grains in the first iteration:
        header = 'Tolerances for MERGING grains in the FIRST iteration loop';
        parameters.index.strategy.m.beg = gtModifyStructure(parameters.index.strategy.m.beg,...
            list.index__strategy__m__beg, 1, header);


        % Tolerances for merging grains in the last iteration:
        header = 'Tolerances for MERGING grains in the LAST iteration loop';
        parameters.index.strategy.m.end = gtModifyStructure(parameters.index.strategy.m.end,...
            list.index__strategy__m__end, 1, header);


        % Tolerances for adding unindexed pairs to grains based on statistics.
        header = 'Tolerances for ADDING unindexed pairs to grains based on statistics';
        parameters.index.strategy.s = gtModifyStructure(parameters.index.strategy.s,...
            list.index__strategy__s, 1, header);


        % Tolerances for excluding indexed pairs from grains based on statistics.
        header = 'Tolerances for EXCLUDING indexed pairs from grains based on statistics';
        parameters.index.strategy.x = gtModifyStructure(parameters.index.strategy.x,...
            list.index__strategy__x, 1, header);

    end
    
    if (options.flag_update)
        disp('Saving updated parameters into parameters file...')
        save('parameters.mat','parameters');
    else
        disp('Indexing parameters will not be saved...')
    end
    
    check = inputwdefault('Start gtINDEXTER now? [y/n]', 'y');
    if strcmpi(check, 'y')
        gtINDEXTER(options.phase_ID, options.flag_update, ...
            options.input_file, options.flag_log, options.forcemerge, parameters);
    end

end % end of function
