function excind = gtIndexExcludeMultipleRefs(grain)
	
% The maximum no. of times a given signed hkl (shkl) is expected to be
% detected in case of full 360deg scan is 4 (that is 2 hkl and 2 -h-k-l) 
% which makes 2 Friedel pairs. Based on the pairs, hkl and -h-k-l cannot be 
% distinguished.


maxrefmult = 2;

excind = [];

indn = 1:length(grain.refid);

% Check for too high multiplicity

for ii = 1:length(grain.hklind);
    
    inds1 = (grain.hklind == grain.hklind(ii));
    
    inds2 = find(grain.shklind(inds1) == grain.shklind(ii));
    
    if length(inds2) > maxrefmult
        
        % Suspicious indices in grain
        inddub = indn(inds1);
        inddub = inddub(inds2);
       
        
        % Choose the best reflections
        
        % Determine how well they fit to grain:
        %   goodness = mean square root of normalized deviations
        
        goodness = zeros(4,length(inddub));
        
        % Angular deviation and center of mass distance
        goodness(1,:) = abs(grain.dang(inddub)/grain.stat.dangstd);
        goodness(2,:) = abs(grain.dcom(inddub)/grain.stat.dcomstd);
        
        % Bbox sizes; note that one or both of them may be affected directly
        % by the aspect ratio of the grain (or twinned sub-grain)
        goodness(3,:) = abs((grain.bbxs(inddub) - grain.stat.bbxsmean)/grain.stat.bbxsstd);
        goodness(4,:) = abs((grain.bbys(inddub) - grain.stat.bbysmean)/grain.stat.bbysstd);
        
        % intensity is ignored until proper corrections are applied
        %goodness(5,:) = abs((grain.int(actrefs)  - grain.stat.intmean)/grain.stat.intstd);
        
        sqrgoodness = sqrt(sum(goodness.*goodness,1));
        
        [~,bestinds] = sort(sqrgoodness);
        
        % Reflections over the maximum number are to be excluded from grain.
        excind = [excind, inddub(bestinds(maxrefmult+1:end))];
        
        
    end
    
end
        
    
excind = unique(excind);
   

end % of function
