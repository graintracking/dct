function [grain, remaining] = gtIndexStatisticalExclusion(grain, tot, ...
                                                        strategy_x, cryst)                       
%
% FUNCTION [grain, remaining] = gtIndexStatisticalExclusion(grain, tot, ...
%                                                       strategy_x, cryst)
%
% Excludes reflections from grains based on grain statistics.
% Those reflections of which intensity, bounding box sizes, 
% distance and angles deviate more than the specified tolerance value 
% (in std) from the average grain value will be deleted from the grain. 
%
% INPUT
%  grain      - all grain data
%  tot        - all original reflection data (indexed and unindexed)
%  strategy_x - parameters for statistical exclusion
%               (as in parameters.index.strategy.x)
%  cryst      - crystallography and other data
%
% OUTPUT 
%  grain      - all grain data updated with added reflections
%  remaining  - all remaining unindedxed reflection id-s after the fit
%               (comprises both previously unindexed and rejected ones)
%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Find and exclude outliers
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fprintf('\n---------------------------------------------')
fprintf('\n REMOVING REFLECTIONS BASED ON STATISTICS...\n\n')
disp(strategy_x)
disp(' ')
tic


grainok = true(size(grain));

parfor ii = 1:length(grain)
    
    % Find outliers
    
    % !!! Normalised intensity should be used in future
    %out_normint = gtMathsOutliers(grain{ii}.normint, strategy_x.stdf, ...
    %              grain{ii}.stat.normintmean, grain{ii}.stat.normintstd);
    
    out_int  = gtMathsOutliers(grain{ii}.int,  strategy_x.stdf, ...
               grain{ii}.stat.intmean,  grain{ii}.stat.intstd );
    
    out_bbxs = gtMathsOutliers(grain{ii}.bbxs, strategy_x.stdf, ...
               grain{ii}.stat.bbxsmean, grain{ii}.stat.bbxsstd);
    
    out_bbys = gtMathsOutliers(grain{ii}.bbys, strategy_x.stdf, ...
               grain{ii}.stat.bbysmean, grain{ii}.stat.bbysstd);
   
    out_dcom = gtMathsOutliers(grain{ii}.dcom, strategy_x.stdf, ...
               grain{ii}.stat.dcommean, grain{ii}.stat.dcomstd);
    
    out_dang = gtMathsOutliers(grain{ii}.dang, strategy_x.stdf, ...
               grain{ii}.stat.dangmean, grain{ii}.stat.dangstd);
        
    % Outlier indices
    outinds = (out_int | out_bbxs | out_bbys | out_dcom | out_dang);
    
    
    % If grain has been modified, recreate grain from good reflections
    if any(outinds)
		
        fprintf('Pairs removed from grain #%d    :   %s\n', ii, ...
                num2str(grain{ii}.pairid(outinds)));

        % Keep existing shkl info
        grain_id      = grain{ii}.id;
        grain_refid   = grain{ii}.refid;
        grain_hklind  = grain{ii}.hklind;
        grain_shklind = grain{ii}.shklind;
        
        grain_refid(outinds)   = [];
        grain_hklind(outinds)  = [];
        grain_shklind(outinds) = []; 
        
        % Get reflection data for the new grain
        newrefs = gtIndexSelectRefs(grain_refid', tot);
        
        % Keep signed hkl indices
        newrefs.shklind = grain_shklind;
        newrefs.hklind  = num2cell(grain_hklind);
       
        % Recreate grain data
        grain{ii} = gtIndexGrainOutputBasic(newrefs, cryst, strategy_x.ang);
        
        grain{ii}.id = grain_id;
        
        if isempty(grain{ii}.R_vector)
            % The grain is not valid anymore and will be deleted.
            grainok(ii) = false;  
            
            fprintf('Grain #%d was found invalid and deleted.\n', ii)

        end  

    end
    
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Update grain data and find unindexed reflections
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Find indexed refid-s
grainrefids = [];

for ii = 1:length(grain)
    if grainok(ii)
        grainrefids = [grainrefids, grain{ii}.refid];
    end
end

% Unindexed reflection id-s
remaining = tot.id(~ismember(tot.id, grainrefids)); 


% Delete wrong grains
grain(~grainok) = [];

% Update grain id-s
for ii = 1:length(grain) 
    grain{ii}.id = ii;
end


disp(' ')
toc


end % of function

