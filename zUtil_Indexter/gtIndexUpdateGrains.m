function grain = gtIndexUpdateGrains(grain, latticepar)
% GTINDEXUPDATEGRAINS  Updates grains after indexter
%
%     grain = gtIndexUpdateGrains(grain, latticepar)
%     ----------------------------------------------
%
%     INPUT:
%       grain      = <cell>       all the grains as in index.mat
%       latticepar = <double>     as in parameters.cryst(phaseid).latticepar
%
%     OUTPUT:
%       grain      = <cell>       updated grain info
%            .plref          = <double>     Coordinates of the plane normals in the Cartesian Crystal system
%            .dspacingsp     = <double>     D-spacings in the reference crystal for the given reflections
%            .strain.strainT = <double>     Strain tensor
%            .difspotID      = <double>     unique list for the indexed spots


Bmat = gtCrystHKL2CartesianMatrix(latticepar);

parfor ii = 1:length(grain)

    % Coordinates of the plane normals in the Cartesian Crystal system
    if ~isfield(grain{ii}, 'plref')
        grain{ii}.plref = gtCrystHKL2Cartesian(grain{ii}.hklsp, Bmat);
    end
    
    % D-spacings in the reference crystal for the given reflections
    if ~isfield(grain{ii}, 'dspacingsp')
        dsp_ref = gtCrystDSpacing(grain{ii}.hklsp, Bmat);
        grain{ii}.dspacingsp = dsp_ref;
    end

    % Strain
    if ~isfield(grain{ii},'strain') || ~isfield(grain{ii}.strain,'strainT')
        grain{ii}.strain.strainT = NaN(3,3);
    end

    if all(isfield(grain{ii}, {'difspotidA','difspotidB'})) && ~isfield(grain{ii}, 'difspotID')
        grain{ii}.difspotID = [grain{ii}.difspotidA, grain{ii}.difspotidB];
    end
    
end

%grain = gtIndexCalculateGrainSize(grain, latticepar, volSample, svolSample);

end % end of function
