% former gtINUniquePlaneNormals

function grainsout=gtIndexUniquePlaneNormals(grainsinp,ACM)

% theta types in grainsinp and ACM sold correspond with each other.

if ~exist('ACM','var')
  load parameters.mat
  %ACM=gtAngConsMat(parameters.cryst.spacegroup);
	ACM = gtIndexAngularConsMatrix(hkltypes,parameters);
end

nof_grains=length(grainsinp);

% Handle cell arrays or single one
if nof_grains==1
  grains{1}=grainsinp;
else
  grains=grainsinp;
end

% Loop through grains
for i=1:nof_grains
  nof_pl=size(grains{i}.pl,1);

  % Load grain info into 'remaining' (these remain to be dealt with)
  remainingl.pl=grains{i}.pl;
  remainingl.thetatype=grains{i}.thetatype;
  remainingl.theta=grains{i}.theta;
  remainingl.eta=grains{i}.eta;
  remainingl.hkl=grains{i}.hkl;
  remainingl.omega=grains{i}.omega;

  remainings=1:nof_pl; % ; modif sabine

  nof_unipl=0;

  grains{i}.uni.nof_unipl=NaN;
  grains{i}.uni.nof_dbpl=NaN;

  % Process all the planes in the grain
  while length(remainings)>=1

    nof_unipl=nof_unipl+1;

    merge=1;

    % Take the first in remaining and compare with all other remaining,
    % look for possible identical planes.
    for j=2:length(remainings)
      if remainingl.thetatype(1)==remainingl.thetatype(j)
        ACV=ACM{remainingl.thetatype(1),remainingl.thetatype(j)};
        %ang=gt2PlanesAngle(remainingl.pl(1,:),remainingl.pl(j,:));
        ang = gtMathsVectorsAngles(remainingl.pl(1,:),remainingl.pl(j,:));
				[minval,minloc]=min(abs(ACV-ang));

        if ACV(minloc)==0 % the same planes
          merge=[merge, j];
        end
      end
    end

    % Merge identical planes, its new direction is the mean of the constituting
    %  planes.

    if length(merge)>1
      % here, it's assumed that merge=2 always
      % in case of wrong indexing merge might be >2
      if length(merge)>2
        disp(sprintf('Warning! Segmentation or indexing error in grain %d.',i))
        disp('  Dubious plane normals are:')
        disp(grains{i}.pairid(remainings(merge)))
      end

      % sign: if spl=-1 they are of opposite direction
     
      spl=sign(remainingl.pl(merge(2),:)*remainingl.pl(merge(1),:)'); % direction same or opposite
      spl=repmat(spl,1,3);

      n1=remainingl.pl(merge(1),:);
      n2=remainingl.pl(merge(2),:).*spl;

      unipl=(n1+n2)/2;
      
      theta1=remainingl.theta(merge(1)); % in degrees
      theta2=remainingl.theta(merge(2));
      
      eta1=remainingl.eta(merge(1)); % in degrees
      eta2=remainingl.eta(merge(2));

      omega1=remainingl.omega(merge(1)); % in degrees
      %omega2=remainingl.omega(merge(2));

      % Average eta,theta and their error
      unieta1=eta1;  
      if spl(1)<0
        unieta2=mod((eta2+180),360);
      else
        unieta2=360-eta2;
      end
      unieta=(unieta1+unieta2)/2;
      deta_deg=unieta1-unieta2;
      deta=deg2rad(deta_deg);
      
      unitheta=(theta1+theta2)/2;
      dtheta_deg=theta1-theta2;
      dtheta=deg2rad(dtheta_deg);
      
      % Error in the plane normal coordinates
      dn=n1-n2;
      nn=n1*n2';
      
      dang_deg=acosd(nn); % angle between the two normals
      dang=acos(nn);
 
      nnhor=n1(1:2)*n2(1:2)'/norm(n1(1:2))/norm(n2(1:2));
      %dazhd_deg=acosd(nnhor); % azimuth
      %dazh=acos(nnhor);    
      
      % Azimuth
      az1=atan2(n1(2),n1(1)); % in rad
      az2=atan2(n2(2),n2(1)); % in rad
      
      if az1<0
        az1=az1+2*pi; % set between 0..2pi
      end
      
      if az2<0
        az2=az2+2*pi; % set between 0..2pi
      end
     
      % Corner case around 0 deg (X axis)
      if az1-az2<-pi
        az1=az1+2*pi;
      elseif az1-az2>pi
        az2=az2+2*pi;
      end
      
      uniaz=(az1+az2)/2;
      daz=az1-az2; % error in azimuth in rad
      daz_deg=rad2deg(daz); % in deg
      
      dy1=sind(2*theta1)*sind(eta1);
      dz1=sind(2*theta1)*cosd(eta1);

      dy2=sind(2*theta2)*sind(eta2); % should be around -dy1
      dz2=sind(2*theta2)*cosd(eta2);

      if dy1*dy2>0
        disp('Warning! dy1*dy2>0 ')
        disp(sprintf('   Possible segmentation or indexing error in grain %d ',i))
        disp('   Dubious plane normals are:')
        disp(grains{i}.pairid(remainings(merge)))
				%keyboard
      end
      
%       azd1=-atan(dy1/2/sind(theta1)^2); % azimuth term from the diffr. condition dy,dz 
%       azd2o=-atan(dy2/2/sind(theta2)^2);
%       azd2=azd2o;
      
      if spl(1)<0
        %azd2=azd2+pi;     % if n2 opposite to n1, n2 should be turned 180 degrees
        dz=(dz1-dz2)/2;   % average dz
        ddz=dz1+dz2;      % error in dz
      else
        dz=(dz1+dz2)/2;
        ddz=dz1-dz2;
      end
      
      dy=(dy1-dy2)/2; % !!!  average dy
      ddy=dy1+dy2;    % error in dy

%       dazd=azd1+azd2o; % !!!
%       
%       azd1_deg=rad2deg(azd1)
%       azd2o_deg=rad2deg(azd2o)
%       azd2_deg=rad2deg(azd2)
%       dazd_deg=rad2deg(dazd)
%       
%       az1c_deg=mod(180+azd1_deg+omega1,360)
%       az2c_deg=mod(180+azd2_deg+omega2,360)
%       dazc_deg=az1c_deg-az2c_deg
%       daz_deg
%             
%       dom_deg=dazc_deg-dazd_deg
%       dom_deg=daz_deg-dazd_deg
      
     
%       % Derivatives      
%       dth_ddy=1/2*sind(unieta)/cosd(2*unitheta);
%       dth_ddz=1/2*cosd(unieta)/cosd(2*unitheta);
%       
%       term1=-1/(1+(dy/2/sind(unitheta)^2)^2);
%       term2=1/2*(1/sind(unitheta)^2-2*dy*cosd(unitheta)/sind(unitheta)^3*dth_ddy);
%       daz_ddy=term1*term2;
%       
%       term1=-1/(1+(dy/2/sind(unitheta)^2)^2);
%       term2=-dy*cosd(unitheta)/sind(unitheta)^3*dth_ddz;
%       daz_ddz=term1*term2;
%       
%       % Error in azimuth from dy,dz:
%       dazd=daz_ddy*ddy+daz_ddz*ddz;
%       dazd_deg=rad2deg(dazd); % in deg
%      
%       % Error in azimuth from omega
%       domega=daz-dazd;
%       domega_deg=rad2deg(domega); % in deg
    

      
      
      errvernorm=abs(ddz/cosd(2*unitheta)/sqrt(2)); % vertical detection error normalized
                                                    % by sp/(2r)     
      errhornorm=abs(ddy/cosd(2*unitheta)/sqrt(2)); % horizontal detection error normalized
                                                    % by sp/(2r)     
     
      %pixerrvernorm=abs(dn(3)*2*sind(unitheta)/cosd(2*unitheta));
      ddfrometa=deta*2*sind(unitheta);  %/cosd(2*unitheta);
      %omfactor=(1/(1+(sind(2*unitheta)*sind(unieta)/(2*sind(unitheta)^2))^2)/...
      %         (2*sind(unitheta)^2))^2*cosd(2*unitheta)^2;
      
    else % if length(merge)<=1
      unipl=remainingl.pl(1,:);
      unitheta=remainingl.theta(1);
      unieta=remainingl.eta(1); % in degrees
      omega1=remainingl.omega(1);
			
      % Azimuth
      uniaz=atan2(unipl(2),unipl(1)); % in rad
      if uniaz<0
        uniaz=uniaz+2*pi; % set between 0..2pi
      end
      
      dy=sind(2*unitheta)*sind(unieta);
      dz=sind(2*unitheta)*cosd(unieta);

      % Discrepancy: not known
      dn=NaN(1,3);
      nn=NaN;
      ddy=NaN;
      ddz=NaN;

      errvernorm=NaN;
      errhornorm=NaN;
      %pixerrvernorm=NaN;
      ddfrometa=NaN;
      dang=NaN;
      dang_deg=NaN;
      daz_deg=NaN;
      dtheta_deg=NaN;
      deta_deg=NaN;
            
      nnhor=NaN;
      daz=NaN;
      dtheta=NaN;
      deta=NaN;
      %omfactor=NaN;
    end


    % Eta in first quadrant (between 0..90deg)
     %  Set eta between 0<eta<180
    if unieta>180
      etaq=360-unieta; % in degrees
    else
      etaq=unieta; % in degrees
    end

     %  Set eta between 0<eta<90
    if etaq>90
      etaq=180-etaq;
    end

   
    % Derivatives
    dth_ddy=1/2*sind(unieta)/cosd(2*unitheta);
    dth_ddz=1/2*cosd(unieta)/cosd(2*unitheta);
 
    deta_ddy=cosd(unieta)/sind(2*unitheta);
    deta_ddz=-sind(unieta)/sind(2*unitheta);

    dnz_ddy=-dz/2*cosd(unitheta)/sind(unitheta)^2*dth_ddy;
    dnz_ddz=1/2/sind(unitheta)-dz*cosd(unitheta)/2/sind(unitheta)^2*dth_ddz;

    % azimuth
    term1=-1/(1+(dy/2/sind(unitheta)^2)^2);
    term2=1/2*(1/sind(unitheta)^2-2*dy*cosd(unitheta)/sind(unitheta)^3*dth_ddy);
    daz_ddy=term1*term2;

    term1=-1/(1+(dy/2/sind(unitheta)^2)^2);
    term2=-dy*cosd(unitheta)/sind(unitheta)^3*dth_ddz;
    daz_ddz=term1*term2;

    % Error in azimuth from dy,dz:
    dazd=daz_ddy*ddy+daz_ddz*ddz;
    dazd_deg=rad2deg(dazd); % in deg

    % Error in azimuth from omega
    dom=daz-dazd;
    dom_deg=rad2deg(dom); % in deg
    
    if isnan(dom_deg)
      uniomega=omega1; % no correction 
    else
      uniomega=omega1-dom_deg/2; % double detection; corrected by half the difference
    end
    
    % Save unique planes under grains.uni 
    grains{i}.uni.plid(nof_unipl,:)=false(1,nof_pl);
    grains{i}.uni.plid(nof_unipl,remainings(merge))=true; % which pair-lines are used
    
    % Coordinates
    grains{i}.uni.pl(nof_unipl,:)=unipl;
    grains{i}.uni.thetatype(nof_unipl)=remainingl.thetatype(1);
    grains{i}.uni.hkl(nof_unipl,:)=remainingl.hkl(1,:);
    %grains{i}.uni.pla(nof_unipl)=NaN;
    
    grains{i}.uni.theta(nof_unipl)=unitheta; % in deg
    grains{i}.uni.eta(nof_unipl)=unieta; % in deg 
    grains{i}.uni.etaq(nof_unipl)=etaq; % in deg
    grains{i}.uni.omega(nof_unipl)=uniomega; % refers to n1; in deg
    
    grains{i}.uni.dy(nof_unipl)=dy;
    grains{i}.uni.dz(nof_unipl)=dz;   
    grains{i}.uni.az(nof_unipl)=uniaz; % azimuth in rad   
    
    % Errors
    grains{i}.uni.ddy(nof_unipl)=ddy;    
    %grains{i}.uni.ddy_az(nof_unipl)=ddy_az;
    grains{i}.uni.ddz(nof_unipl)=ddz;
    grains{i}.uni.dom(nof_unipl)=dom;
    
    grains{i}.uni.ddfrometa(nof_unipl)=ddfrometa;

    grains{i}.uni.errvernorm(nof_unipl)=errvernorm;
    grains{i}.uni.errhornorm(nof_unipl)=errhornorm;
    
    grains{i}.uni.dn(nof_unipl,:)=dn;    
    grains{i}.uni.nn(nof_unipl)=nn;
    grains{i}.uni.nnhor(nof_unipl)=nnhor;    
    
    grains{i}.uni.dang(nof_unipl)=dang;    
    grains{i}.uni.daz(nof_unipl)=daz;    
    grains{i}.uni.dazd(nof_unipl)=dazd;    
    grains{i}.uni.dtheta(nof_unipl)=dtheta;    
    grains{i}.uni.deta(nof_unipl)=deta;    

    grains{i}.uni.dom_deg(nof_unipl)=dom_deg;
    grains{i}.uni.dang_deg(nof_unipl)=dang_deg;    
    grains{i}.uni.daz_deg(nof_unipl)=daz_deg;    
    grains{i}.uni.dazd_deg(nof_unipl)=dazd_deg;    
    grains{i}.uni.dtheta_deg(nof_unipl)=dtheta_deg;
    grains{i}.uni.deta_deg(nof_unipl)=deta_deg;

    % Derivatives
    grains{i}.uni.dth_ddy(nof_unipl)=dth_ddy;
    grains{i}.uni.dth_ddz(nof_unipl)=dth_ddz;
    grains{i}.uni.dth_dom(nof_unipl)=0;
    
    grains{i}.uni.deta_ddy(nof_unipl)=deta_ddy;
    grains{i}.uni.deta_ddz(nof_unipl)=deta_ddz;
    grains{i}.uni.deta_dom(nof_unipl)=0;

    grains{i}.uni.dnz_ddy(nof_unipl)=dnz_ddy;
    grains{i}.uni.dnz_ddz(nof_unipl)=dnz_ddz;
    grains{i}.uni.dnz_dom(nof_unipl)=0;
    
    grains{i}.uni.daz_ddy(nof_unipl)=daz_ddy;
    grains{i}.uni.daz_ddz(nof_unipl)=daz_ddz;
    grains{i}.uni.daz_dom(nof_unipl)=1;
   
    % Other

    %grains{i}.uni.pixerrvernorm(nof_unipl)=pixerrvernorm;
    %grains{i}.uni.omfactor(nof_unipl)=omfactor;    

    
    remainingl.pl(merge,:)=[];
    remainingl.thetatype(merge)=[];
    remainingl.theta(merge)=[];
    remainingl.eta(merge)=[];
    remainingl.hkl(merge,:)=[];
    remainingl.omega(merge)=[];

    remainings(merge)=[];

  end % of planes

  grains{i}.uni.nof_unipl=nof_unipl;
  grains{i}.uni.nof_dbpl=nof_pl-nof_unipl;

end % of grains

if nof_grains==1
  grainsout=grains{1};
else
  grainsout=grains;
end

end % of function
