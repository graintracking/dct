function [grain, unindexed, strategy, cryst, tot] = ...
                                     gtIndexForceMergeGrains(fname0, list)
% GTINDEXFORCEMERGEGRAINS Merges the grains specified in 'list' without 
% further conditions.
% 
% [grain, unindexed, strategy, cryst, tot] = ...
%                                     gtIndexForceMergeGrains(fname0, list)
% 
% Merges the grains specified in 'list' without further conditions.
% This function can be used to test merging grains, but run gtINDEXTER
% with 'forcemerge' option instead to update all indexing output properly
% (it will call this function). 
%
% INPUT
%
%  fname0     = relative path to index.mat file
%  list       = grain ID-s to be merged in a cell array;
%                 example: list{1}=[5 7]; list{2}=[20 26 29];
%
% OUTPUT
%  grain      = updated grain data including merged and unmerged grains
%


%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Initialize grain data
%%%%%%%%%%%%%%%%%%%%%%%%%%

fprintf('\n FORCED MERGING OF GRAINS\n')

% Load indexing data
fprintf('\nLoading grain data and parameters from: \n %s\n\n',fname0)
index = load(fname0);

fprintf('Previous Indexter summary: \n%s\n',index.summary)

grain     = index.grain;
tot       = index.tot;
cryst     = index.cryst;
strategy  = index.strategy;
tol_merge = strategy.m.end;
unindexed = index.restids3;

newgrains{1} = [];

% Loop through merge list
for ii = 1:length(list)

    refids = [];
    dang   = [];
    nofp   = [];
    
    for jj = 1:length(list{ii})
        refids = [refids; grain{list{ii}(jj)}.refid(:)];
        dang   = [dang;   grain{list{ii}(jj)}.stat.dangmean];
        nofp   = [nofp;   grain{list{ii}(jj)}.nof_pairs];
    end
    
    % Reload original reflections data
    newgrefs = gtIndexSelectRefs(refids,tot);
    
    % Redefine angular tolerance as above now with the maximum dang
    tol_ang = max(dang)/0.8*tol_merge.angf;
    tol_ang = max(tol_ang, tol_merge.angmin);
    tol_ang = min(tol_ang, tol_merge.angmax);

    
    % Create new merged grain; orientation test is done, not all
    % reflections may pass the test in the merged grain
    [newgrains{ii}, goodind] = gtIndexGrainOutputBasic(newgrefs,...
        cryst, tol_ang);
    
    % Only accept merged grain if it has at least the no. of reflections 
    % of the originals (rarely it may not have)
    
    fprintf('Grains ID-s merged as a new grain: %s\n', num2str(list{ii}));
    fprintf('  Number of consistent pairs: %d of %d\n', ...
        length(goodind), length(newgrefs.id) );
    
    % Ref id-s not included in merged grain
    excind          = true(length(newgrefs.id),1);
    excind(goodind) = false;
    
    if any(excind)       
        fprintf('  Pairs ID-s excluded from new grain: %s\n', ...
            num2str(newgrefs.pairid(excind)'))
        
        % Add excluded ones to unindexed set
        unindexed = [unindexed; newgrefs.id(excind)];       
    end
    
end

% Delete old grains
grain([list{:}]) = [];

% Add new grains
grain = [grain, newgrains];


fprintf('\nGrain ID-s will be rearranged according to approximate grain size.\n\n')


end % of function