function [goods, grain] = gtIndexFindGroupInCand(cand, isecs, tol_build,...
                                                 samenv, cryst)    
% GTINDEXFINDGROUPINCAND Core function to find new grains among the
% reflections
%
% [goods, grain] = gtIndexFindGroupInCand(cand, isecs, tol_build,...
%                                         samenv, cryst)    
%
% ------------------------------------------------------------------
%
% This is the function that finds possible new grains in the
% indexing process. It searches for a grain in the input set of candidate 
% reflections 'cand'.
% All reflections in 'cand' are assumed to be pair-consistent with the 
% first reflection in 'cand'. By looping through the rest of the set 
% and checking group consistency, a 2nd, 3rd, 4th, etc. valid reflection 
% is potentially found.   
% 
% INPUT 
%  cand      - reflection data; all the Friedel pairs that possibly belong 
%              to the same grain as the first one in cand 
%  isecs     - intersection points of the diffraction paths in 3D space;  
%              size (n,6), where n = length(cand)-1
%  tol_build - indexing tolerances for finding new grains (Build grains)
%  samenv    - coordinates of the sample envelope
%  cryst     - crystallography and other data 
%
% OUTPUT
%  goods     - vector of logicals; true for the reflections that 
%              constitute the new grain; size (n,1)
%  grain     - grain data; empty if no valid grain was found
%


nof_cand    = length(cand.id); 
grain       = [];
goods       = false(nof_cand,1);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Loops through input set
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Loop for finding a 2nd reflection of the grain
for c1 = 2:nof_cand 

	% Get data for actual reflection and the following one
	baserefs = gtIndexSelectRefsReduced([1,c1],cand);

	% Loop for finding a 3rd reflection of the grain
	for c2 = c1+1:nof_cand 
	
		% Check group consistency of this third reflection c2 against the
		% other two
		tryref = gtIndexSelectRefsReduced(c2,cand);

		ok = gtIndexCheckGroupCons(tryref, baserefs, isecs(c1-1,:), ...
             tol_build, samenv, cryst.ACM);

		if ok
			
			% Loop for finding a (4th,5th,6th,...,tol_build.ming-th) 
            % reflection of the grain recursively.
			% 'cs' will contain [1,c1,c2] plus the consistent 
            % candidate indices.
			cs = sfAddRefsToGroup([1,c1,c2], cand, nof_cand, tol_build, ...
                 samenv, cryst.ACM, 1);
			
			% If no. of reflections is sufficent, accept them as a grain
			if length(cs) >= tol_build.ming  
				
				% Check the rest of candidate reflections.
				% 'csf' contains 'cs' plus the additionally found 
                % consistent candidate indices.
				csf = sfCheckRest(cs, cand, nof_cand, tol_build, ...
                                  samenv, cryst.ACM);
				goods(csf) = true;
				
				% Reload data for good reflections only
				grainrefs   = gtIndexSelectRefs(csf, cand);
				
				% Create grain output
				[grain, indok] = gtIndexGrainOutputBasic(grainrefs, ...
                                 cryst, tol_build.ang);

				% Check Rodrigues test results
				if length(indok) >= tol_build.ming
					
                    if length(indok)~=length(csf)  
					
                        goods = false(nof_cand,1);
						
						% Keep only the ones with positive Rodr. test
						goods(csf(indok)) = true;

                    end
                    
                    return
                    
				else  % refuse grain
					goods    = false(nof_cand,1);
					goods(1) = true;
					grain    = [];
				end
				
			end
			
		end
		
	end
	
end


end  % of main function


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% SUB-FUNCTION 1 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Finds the minimum required number of group consistent reflections to 
% make a new grain.
% Calls itself recursively.


function csnew = sfAddRefsToGroup(cs, cand, nof_cand, tol_build, ...
                                  samenv, ACM, mylevel)
  
	baserefs = gtIndexSelectRefs(cs, cand);
    % andy - can hack this a bit to treat lines, from which we have an estimate
    % of grain position? 
    if ~isfield(baserefs, 'g3d')
    	baseisec = gtMathsLinesIntersection([baserefs.ca baserefs.dir])';
    else
        baseisec = mean(baserefs.g3d, 1);
    end
    csnew    = cs;
	
	for ii = cs(end)+1:nof_cand
        
		tryref = gtIndexSelectRefs(ii, cand);
        
		ok = gtIndexCheckGroupCons(tryref, baserefs, baseisec, ...
                                   tol_build, samenv, ACM);

        % If all are group consistent                       
		if ok
			
            % If already found enough reflections to make a new grain  
			if (length(cs)+1) >= tol_build.ming
				csnew = [cs,ii];
                return
            
            % Find more reflections with recursive calls
            else
                
                csext = sfAddRefsToGroup([cs,ii], cand, nof_cand, ...
                        tol_build, samenv, ACM, mylevel+1);
                    
				if length(csext) >= tol_build.ming
					csnew = csext;
					return
                end
                
			end
		
		end
		
	end
	
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% SUB-FUNCTION 2 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Checks each member of a set of reflections if it fits (group-consistent)
% the given base set (a potential grain).


function csf = sfCheckRest(cs, cand, nof_cand, tol_build, samenv, ACM)

	baserefs = gtIndexSelectRefs(cs, cand);
    % andy - can hack this a bit to treat lines, from which we have an estimate
    % of grain position?
    if ~isfield(baserefs, 'g3d')
        baseisec = gtMathsLinesIntersection([baserefs.ca baserefs.dir])';
    else
        baseisec = mean(baserefs.g3d, 1); 
    end
	csf      = cs;
	
    % For all candidates beyond base set
	for ii = max(cs):nof_cand
        
        % Check group consistency
		tryref = gtIndexSelectRefs(ii, cand);
		ok     = gtIndexCheckGroupCons(tryref, baserefs, baseisec, ...
                                       tol_build, samenv, ACM);
		
        % If ii-th reflection is group consistent, add it to the base set;
        % recalculate intersection point.
        if ok
			csf      = [csf, ii];
			baserefs = gtIndexSelectRefs(csf, cand);
            % andy - can hack this a bit to treat lines, from which we have an estimate
            % of grain position?
            if ~isfield(baserefs, 'g3d')
                baseisec = gtMathsLinesIntersection([baserefs.ca baserefs.dir])';
            else
                baseisec = mean(baserefs.g3d, 1);
            end
        end
        
	end
end

		

