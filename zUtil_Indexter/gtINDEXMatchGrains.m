function output = gtINDEXMatchGrains_v3(grain1, grain2, ph1, offset_xyz,...
    offset_rot, tol, volume_lim_low, volume_lim_hi, keep_conflicts, ...
    stacked, pixels_out, use_orig, pix1, pix2, printflag)
% GTINDEXMATCHGRAINS_V3  Matches indexed grains between two datasets;
% it also finds unmerged grains within a single dataset.
%
% output = gtINDEXMatchGrains_v3(grain1, grain2, ph1, offset_xyz,...
%    offset_rot, tol, volume_lim_low, volume_lim_hi, keep_conflicts, ...
%    stacked, pixels_out, use_orig, pix1, pix2, printflag)
% ------------------------------------------------------------------------
%
% Matches indexed grains between two datasets based on grain centroid
% position, orientation, size (mean bounding box sizes) and intensity
% (mean spot intensity).
%
% It can potentially also find unmerged grains within a single dataset.
% Similar criteria are used as in gtIndexter but the results may be 
% slightly different for matches close to the tolerance limits.  
%
% It can account for or find a global translational and rotational offset
% between the two datasets. Optimised values for the offsets can be found
% by using the function iteratively. The first guess by the user has to be 
% close enough to find correct matching grains.
%
% Grains used in the matching can be restricted to a sub-region of the 
% volume.
%
% Misorientation is computed using orientation matrices and 
% the symmetry operators from parameters.cryst.symm.
%
% It needs to be launched from the folder belonging to grain1.
% Crystallography info is loaded from parameters.mat in the current folder
% as for phase 'ph1'.
%
%
% MATCHING ALGORITHM
%
% For each grain in the reference set 'grain1' it tries to find the 
% corresponding grain in the set 'grain2'. It uses tolerances in 'tol' 
% as the criteria to find potentially matching candidates. Candidates for 
% each grain1 are found amongst grain2. In case there are multiple 
% candidates found for a grain1, the best fitting candidate from grain2 is 
% selected based on a figure of merit. The figure of merit combines 
% centroid distance, misorientation, etc. which are weighted by the 
% corresponding tolerance values. Hence, changing the tolerances might 
% affect slightly which candidate is selected.
%
% The match is unique for each grain1, i.e. each grain1 will be present 
% in the list of matched grains once (or zero times). The match is not 
% unique for grain2, i.e. a grain2 may be assigned to more than one grain1.
%
%
% OFFSET COMPUTATION 
%
% Firstly, the updated centers and orientations are calculated for grain2
% according to the input global translational and rotational offset.
% The grains are then matched. Using the matched grain pairs, the median 
% deviations of the grain centroids and Rodrigues vectors are calculated, 
% from which updated values of the offset between the two datasets are 
% given. The grain centroids are not, only the grain orientations are 
% used to determine the rotational offset between the two datasets.
%
%
% INPUT
%   grain1     = <cell>      indexed grain data of reference set as output from Indexter
%                            {if undefined, it is loaded for the current dataset}
%   grain2     = <cell>      indexed grain data to be matched as output from Indexter
%                            {if undefined, it is same as grain1}
%   ph1        = <double>    phase ID in grain1 {1}
%   offset_xyz = <double>    global translation (of the origin) between datasets from
%                            a position given in grain2 to the same in grain1 {[0 0 0]} [um]
%   offset_rot = <double>    global rotation around the origin of grain2; the rotation
%                            matrix that transforms a position vector given in grain2
%                            into grain1 (size 3x3 for column vectors); {eye(3)}
%                            --> p1 = offset_rot*p2 + offset_xyz;
%   tol        = <struct>    tolerances for finding matching grains when comparing two grains
%                            {default values in brackets}:
%                                tol.dist_rel = max relative distance between grain centroids {0.5};
%                                               mult. factor of smaller bbox size of the grain (gr1):
%                                                 toldist_rel = tol.dist_rel * min(bbxs, bbys)
%                                tol.dist_um  = max absolute distance between grain centroids [um] {0};
%                                               final distance tolerance is the less stringent of the two:
%                                                 toldist = max(toldist_rel, tol.dist_um)
%                                tol.misangle_deg = max misorientation angle [deg]
%                                tol.bbxs     = max. bbox size x ratio {2}
%                                tol.bbys     = max. bbox size y ratio {2}
%                                tol.int      = max. intensity ratio {1e10}
%   volume_lim_low =<double> lower limit for matching grains in grain1
%                            from only a central subregion of the grain1 volume; e.g. to exclude
%                            grains at the bottom where centroids are affected {[]};
%                              [X,Y,Z] = [0 0 0.2]: bottom 20% in Z are excluded;
%   volume_lim_hi = <double> higher limit for matching grains in grain1
%                            from only a central subregion of the grain1 volume; e.g. to exclude
%                            grains at the top where centroids are affected {[]};
%                              [X,Y,Z] = [1 1 0.8]: top 20% in Z are excluded;
%   keep_conflicts <logical> if true, the best candidate of conflicting matches are kept;
%                            if false, all conflicting matches are excluded {true}
%   stacked    = <logical>   set true if volumes to compare are part of a stack
%                            (so the centroid Z coordinates should be free) {false}
%   pixels_out = <logical>   if true, grain centers in output will be in pixels 
%                            instead of microns {false}
%   use_orig   = <logical>   true if using original center values 
%                            grain.orig.center, grain.orig.R_vector {false}
%   pix1       = <double>    pixel size for dataset 1 (mm/pixel); if not
%                            specified, it will be loaded from the
%                            parameteres file
%   pix2       = <double>    pixel size for dataset 2 (mm/pixel); if not
%                            specified, it will be equal to pix1
%   printflag  = <logical>   flag to print {false}
%
%
% OUTPUT
%   output = <struct>  with fields:
%       .match       = array of indices of matching grains [grain_ind1
%                      grain_ind2]; size (nof grain1, 2); 2nd column is NaN
%                      where no matches found                      
%       .match_ok    = as .match but excluding rows where no match found
%       .conflicts   = list of conflicts found (same as printed on command line);
%                      conflicts{:, 1}: index in grain1 for which more than one
%                                       possible matches were found
%                      conflicts{:, 2}: indices in grain2 that are the possible matches
%                      conflicts{:, 3}: also indices in grain2 (??)
%       .offset_xyz  = updated guess for offset_xyz [um]
%       .offset_rot  = updated guess for offset_rot (3,3)
%       .offset_deg  = updated guess for offset angle [deg]
%       .offset_axis = updated guess for offset axis
%       .deviations  = vectors of deviations of grain properties of matched
%                      grains; NaN where no match found
%       .tol         = tolerances used in the matching
%       .missing1    = NOT matched grains from dataset 1
%       .missing2    = NOT matched grains from dataset 2
%       .gr1         = vectorised grain data used in matching
%       .gr2         = vectorised grain data used in matching
%
%
% USAGE
%   Modes of application:
%
%   1) Check for indexed grains in a single dataset that should have been 
%      merged. 
%        out = gtINDEXMatchGrains_v3()
%        out = gtINDEXMatchGrains_v3(g1, g1)
%
%   2) Revise tolerances:
%        out = gtINDEXMatchGrains_v3()
%        tol.__ = __
%        out = gtINDEXMatchGrains_v3([], [], [], [], [], tol)
%
%   3) Match grains with default settings:
%      Load input from either:
%        a) indexing: this includes size info:
%             g1 = load('4_grains/phase_01/index.mat');
%        b) global fitting: this excludes size info, but more accurate  
%             g1 = load('4_grains/phase_01/globalfit.mat')
%           NOTE! Because of no size info, absolute distance tolerance 
%           should be used, e.g:
%             tol.dist_um = 20
%      Run matching:
%        out = gtINDEXMatchGrains_v3(g1, g2)
%
%   4) Restrict grain1 grains used in matching to a sub-region of the volume; e.g
%      because centroid grain positions at the top and bottom are usually
%      flawed. It does not affect grain2, all grains in grain2 will be used.
%        lim_low = [0 0 0.2]   % exclude bottom 20% in Z
%        lim_hi  = [1 1 0.8]   % exclude top 20% in Z
%        out = gtINDEXMatchGrains_v3(g1, g2, 1, [], [], [], lim_low, lim_hi)
%
%   5) Find global translational and rotational offsets between the two 
%      volumes. Run matching in an iterative manner. For best results,
%      exclude the top and bottom regions until offsets are refined. 
%      Set tolerances:
%        out = gtINDEXMatchGrains_v3(g1, g2);
%        tol = out.tol;
%        tol.__ = __
%      Run matching iteratively to refine offsets:
%        out = gtINDEXMatchGrains_v3(g1, g2, 1, [], [], tol, lim_low, lim_hi);
%        out = gtINDEXMatchGrains_v3(g1, g2, 1, out.offset_xyz, out.offset_rot, tol, lim_low, lim_hi);
%        out = gtINDEXMatchGrains_v3(g1, g2, 1, out.offset_xyz, out.offset_rot, tol, lim_low, lim_hi);
%        out = gtINDEXMatchGrains_v3(g1, g2, 1, out.offset_xyz, out.offset_rot, tol, lim_low, lim_hi);
%      Once offsets are refined, include the complete volume again:
%        out = gtINDEXMatchGrains_v3(g1, g2, 1, out.offset_xyz, out.offset_rot, tol)
%
%
%   Version 005 (v3) 09-2019 by PReischig
%     revised offset computation; volume sub-region option; visual feedback
%   
%   Version 004 21-11-2013 by LNervo
%     Use microns for distances, pxsize, bbox
%
%   Version 003 03-06-2013 by LNervo
%     Handled different pixel size between datasets
%     Used bounding boxes in mm and not in pixels
%
%   Version 001 by PReischig
%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Initialize
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if (~exist('printflag', 'var') || isempty(printflag))
    printflag = true;
end

out = GtConditionalOutput(printflag);

if (~exist('ph1', 'var') || isempty(ph1))
    ph1 = 1;
end
if (~exist('offset_xyz', 'var') || isempty(offset_xyz))
    offset_xyz = [0 0 0];
end
if (~exist('offset_rot', 'var') || isempty(offset_rot))
    offset_rot = eye(3); % for column vectors ! for row vectors, use transpose!
end

if (~exist('grain1', 'var') || isempty(grain1))
    fname = fullfile('4_grains', sprintf('phase_%02d', ph1), 'index.mat');
    out.fprintf('Loading grain info from %s ...\n', fname)
    grain1 = load(fname);
    grain1 = grain1.grain;
end
if (~exist('grain2', 'var') || isempty(grain2))
    grain2 = grain1;
end

if (~exist('tol', 'var') || isempty(tol))
    tol.dist_rel     = 0.5;    % 0.5 = 50% of bounding box size
    tol.dist_um      = 0;      % [um]
    tol.misangle_deg = 3;      % [deg]
    tol.bbxs         = 2;
    tol.bbys         = 2;
    tol.int          = 1e10;
    % tol.distf    = 0.5;    % 0.5 = 50%
    % tol.Rdist    = 0.02;   % 0.02 -> 2*atand(0.02) = 2.29 deg
    % tol.absolute = false;
end

if (~exist('volume_lim_low', 'var') || isempty(volume_lim_low))
    volume_lim_low = [];
end

if (~exist('volume_lim_hi', 'var') || isempty(volume_lim_hi))
    volume_lim_hi = [];
end

if (~exist('keep_conflicts', 'var') || isempty(keep_conflicts))
    keep_conflicts = true;
end

if (~exist('stacked', 'var') || isempty(stacked))
    stacked = false;
end

if (~exist('pixels_out', 'var') || isempty(pixels_out))
    pixels_out = false;
end

if (~exist('use_orig', 'var') || isempty(use_orig))
    use_orig = false;
end


nof_grains1 = length(grain1);
nof_grains2 = length(grain2);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Prepare grain data
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

out.odisp('Loading crystallographic info from parameters.mat ...')
parameters = gtLoadParameters();
labgeo = parameters.labgeo;
if (~isfield(labgeo, 'omstep') || isempty(labgeo.omstep))
    % !!! new name; change it!
    %labgeo.omstep = gtAcqGetOmegaStep(parameters);
    labgeo.omstep = gtGetOmegaStepDeg(parameters);
end
if (~isfield(parameters, 'detgeo') || isempty(parameters.detgeo))
    parameters.detgeo = gtGeoConvertLegacyLabgeo2Detgeo(labgeo);
end
detgeo = parameters.detgeo(1);
cryst = parameters.cryst(ph1);
symm = cryst.symm;

if (~exist('pix1', 'var') || isempty(pix1))
    pix1 = (detgeo.pixelsizeu + detgeo.pixelsizev) / 2; % [mm]
    out.odisp(['Pixelsize loaded from parameters file [mm]: ' num2str(pix1)])
end
if (~exist('pix2', 'var') || isempty(pix2))
    pix2 = pix1;
end

match = NaN(nof_grains1, 2);
match(:, 1) = (1:nof_grains1)';

% if (tol.absolute)
%     if tol.distf < 1
%         tol.distf = tol.distf*1000; % um
%     end
% end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Grains centers
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% gather grain1 centroid data in compact form; apply restricted volume 
gr1 = sfGetGrainInfo(grain1, pix1, volume_lim_low, volume_lim_hi, use_orig, out);

% gather grain2 centroid data in compact form; ignore restricted volume 
gr2 = sfGetGrainInfo(grain2, pix2, [], [], use_orig, out);

% Rotate and translate grain centers; use transpose of offset_rot for row vectors 
gr2.center = gr2.center * offset_rot' + offset_xyz(ones(nof_grains2, 1), :);

% flip ud Z coordinates if the volumes are part of a stack
if stacked
    gr2.center(:, 3) = (parameters.labgeo.samenvtop - parameters.labgeo.samenvbot)...
        / 2 - gr2.center(:, 3);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Grains rodrigues vectors
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%gr1 = sfGetRodVector(gr1, nof_grains1, [], cryst, tol, fzone_acc, fzone_ext, out);

% Coordinate transformation: Crystal -> Sample
%   cry2sam_row = orimat = gtMathsRod2OriMat(Rvec);
%   v_sam = v_cry * cry2sam_row;

% Get orientation matrices
%   input: 3xN
gr1.orimat = gtMathsRod2OriMat(gr1.R_vector');
gr2.orimat = gtMathsRod2OriMat(gr2.R_vector');

% apply the additional initial rotation
gr2.orimat = gtMathsMatrixProduct(gr2.orimat, offset_rot');
gr2.R_vector = gtMathsOriMat2Rod(gr2.orimat)';

if 0    
    fzone_acc = gtCrystRodriguesFundZone(cryst.spacegroup, 0);
    fzone_ext = gtCrystRodriguesFundZone(cryst.spacegroup, tol.Rdist);

    gr1 = sfGetRodriguesCoordinates(gr1, nof_grains1, [], cryst, tol, fzone_acc, fzone_ext, out);
    
    gr2 = sfGetRodriguesCoordinates(gr2, nof_grains2, offset_rot, cryst, tol, fzone_acc, fzone_ext, out);
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Match grains
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

out.odisp('Matching grains ...')

out.odisp('Tolerances used:')
print_structure(tol, 'tol', false, printflag);

conflicts = cell(0, 3);

for ii = 1:nof_grains1

    if ~gr1.used(ii)
        continue
    end
    
    % Actual grain data
    act = sfSelectGrains(ii, gr1);

    % Get candidate grain indices to be matched with the actual grain
    [tomatch, misang] = sfCheckMatching(act, gr2, tol, [], [], symm);

    if ~isempty(tomatch)

        nof_matches = length(tomatch);

        if nof_matches > 1

            % if (tol.absolute)
            %    toldist = tol.distf;
            % else
            %    toldist = min(act.bbxs, act.bbys) * tol.distf;
            % end
            
            % distance tolerance is the larger of the relative and absolute
            % values
            toldist = max(min(act.bbxs, act.bbys) * tol.dist_rel, tol.dist_um);
            
            dcenter = sum((act.center(ones(nof_matches, 1), :) - ...
                gr2.center(tomatch, :)).^2, 2)   / (toldist^2);
            %dR_vector  = sum((act.R_vector(ones(nof_matches, 1), :) - gr2.R_vector(tomatch, :)).^2, 2) / (tol.Rdist^2);
            %dR_vector  = sum((act.R_vector(ones(nof_matches, 1), :) - gr2.R_vector(tomatch, :)).^2, 2) / (tol.Rdist^2);
            dbbxs = sum((act.bbxs - gr2.bbxs(tomatch, :)).^2, 2)/(act.bbxs^2) / (tol.bbxs^2);
            dbbys = sum((act.bbys - gr2.bbys(tomatch, :)).^2, 2)/(act.bbys^2) / (tol.bbys^2);
            dint  = sum((act.int  - gr2.int(tomatch, :)).^2, 2) / (tol.int^2);
            dang  = misang.^2 / tol.misangle_deg^2;
            
            %dsum = dcenter + dR_vector + dbbxs + dbbys + dint;
            dsum = dcenter + dang + dbbxs + dbbys + dint;

            sortM = [];
            sortM(:, 1) = dsum;
            sortM(:, 2) = tomatch;   % indices of candidiates in gr2
            sortM(:, 3) = gr2.ind(tomatch);

            % Find best candidate by sorting
            sortM = sortrows(sortM, 1);

            tomatch = sortM(:, 2);  % sorted indices of candidiates in gr2

            out.odisp(['Multiple matches found for grain #' num2str(ii) ':'])
            out.odisp(tomatch)

            conflicts{end+1, 1} = ii;  % index in gr1
            conflicts{end, 2}   = tomatch'; % was sort(tomatch)'
            conflicts{end, 3}   = sortM';

        end % if nof_matches > 1

        % store best candidate as a match
        match(ii, 2) = tomatch(1);
    end
end


if keep_conflicts
    out.odisp('Handling multiple matching grains (conflicts): best candidate is kept as a match.')
else
    % conflicting grains remain in 'conflicts' only
    out.odisp('Handling multiple matching grains (conflicts): no candidate is kept as a match.')
    match([conflicts{:, 1}], 2) = NaN;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Evalute grain matching
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

dev.xyz   = NaN(nof_grains1, 3);
dev.dist  = NaN(nof_grains1, 1);
dev.Rvec  = NaN(nof_grains1, 3);
dev.Rdist = NaN(nof_grains1, 1);
%dev.rotmat_row = NaN(3, 3, nof_grains1);
dev.angle_deg  = NaN(nof_grains1, 1);
dev.bbys  = NaN(nof_grains1, 1);
dev.bbxs  = NaN(nof_grains1, 1);
dev.int   = NaN(nof_grains1, 1);

for ii = 1:size(match, 1)
    jj = match(ii, 2);
    if ~isnan(jj)
        dev.xyz(ii, :) = gr2.center(jj, :) - gr1.center(ii, :);
        dev.dist(ii) = sqrt(dev.xyz(ii, :) * dev.xyz(ii, :)');
        dev.Rvec(ii, :) = gr2.R_vector(jj, :) - gr1.R_vector(ii, :);
        %dev.Rdist(ii, :) = sqrt(dev.Rvec(ii, :) * dev.Rvec(ii, :)');
        dev.bbxs(ii) = gr2.bbxs(jj) / gr1.bbxs(ii);
        dev.bbys(ii) = gr2.bbys(jj) / gr1.bbys(ii);
        dev.int(ii) = gr2.int(jj)  / gr1.int(ii);
        %dev.rot(:, :, ii) = gr1.rot(:, :, ii).' - gr2.rot(:, :, jj); % was all_g1(:, :, ii).' - gr2.rot(:, :, jj);
        %dev.rotmat_row(:, :, ii) = gtMathsRod2OriMat(-dev.Rvec(ii, :));
        %dev.angle_deg(ii, 1) = gtMathsOriMat2AngleAxis(dev.rotmat_row(:, :, ii));  % deg
        dev.angle_deg(ii) = gtDisorientation(gr1.orimat(:,:,ii),...
            gr2.orimat(:,:,jj), symm, 'input', 'orimat');  % deg
    end
end

% matching indexes
ok = ~isnan(dev.dist);

Rvec_med = median(dev.Rvec(ok, :), 1);
xyz_med  = median(dev.xyz(ok, :), 1);
%ddist    = sqrt(sum(dev.dist(ok, :).^2, 1)/sum(ok));
dist_std = std(dev.dist(ok), 1);  % std with weight N
dist_med = median(dev.dist(ok));
%dRdist   = sqrt(sum(dev.Rdist(ok, :).^2, 1)/sum(ok));

% total translational correction
offset_xyz_new = offset_xyz - xyz_med;

% Median orientation correction matrix; use matrix transpose, as 
% offset_rot was defined for column vectors 
rot_med = gtMathsRod2OriMat(Rvec_med')';
ang_med = gtMathsOriMat2AngleAxis(rot_med);

% misorientation angle
misang_std = std(abs(dev.angle_deg(ok)), 1);  % std with weight N
misang_med = median(abs(dev.angle_deg(ok)));

% total rotational correction to be applied 
if isempty(offset_rot)
    % use inverse (=transpose) of the current deviations
    offset_rot_new = rot_med';
else
    offset_rot_new = rot_med' * offset_rot;
end

out.fprintf('\nGRAIN MATCHING STATISTICS\n\n')
out.fprintf('Number of matching grains found:\n  %d \n', sum(ok));
out.fprintf('\nRemaining error in grain centroids [um]:\n');
out.fprintf('  std:     %g\n', dist_std)
out.fprintf('  median:  %g\n', dist_med)
out.fprintf('Remaining error in misorientation angle [deg]:\n')
out.fprintf('  std:     %g\n', misang_std)
out.fprintf('  median:  %g\n', misang_med)
out.fprintf('\nRemaining median deviation of grain centroids in X,Y,Z [um]:\n  [%g %g %g]\n', xyz_med);
out.fprintf('Remaining median deviation of Rodrigues vectors in X,Y,Z:\n  [%g %g %g]\n', Rvec_med);
out.fprintf('  which corresponds to rotation angle [deg]:  %g\n', ang_med);
out.fprintf('\nRecommended total displacement correction (offset_xyz) [um]:\n  [%g %g %g]\n', offset_xyz_new);
out.odisp('Recommended total rotation correction as a matrix (offset_rot):');
out.odisp(offset_rot_new);
out.odisp('Recommended total rotation correction angle [deg]:');
[offset_angle_new, offset_axis_new] = gtMathsOriMat2AngleAxis(offset_rot_new);
offset_axis_new = offset_axis_new';
out.odisp(offset_angle_new);
out.odisp('Recommended total rotation correction axis [deg]:');
out.odisp(offset_axis_new);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Build output
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% if (tol.absolute)
%     if tol.distf > 1
%         tol.distf = tol.distf / 1000; % mm
%     end
% end

% flip ud Z coordinates if the volumes are part of a stack
if stacked
    gr2.center(:, 3) = (parameters.labgeo.samenvtop - ...
        parameters.labgeo.samenvbot) / 2 - gr2.center(:, 3);
end

if pixels_out
    % change output from microns to pixels 
    gr1.center = gr1.center ./ gr1.pixsize; % [pixels]
    gr2.center = gr2.center ./ gr2.pixsize; % [pixels]
end

% store information about the grains for the two datasets (with R-vectors
% moved into the fundamental zone) and extra info

output.nof_matches = sum(~isnan(match(:,2)));
output.match      = match;        % unmatched will have NaN in match(:,2)
output.match_ok   = match(ok, :); % only the matched indices
output.conflicts  = conflicts;
output.missing1   = setdiff(1:length(grain1), output.match_ok(:, 1));
output.missing2   = setdiff(1:length(grain2), output.match_ok(:, 2));
output.offset_xyz = offset_xyz_new;
output.offset_rot = offset_rot_new;
output.offset_deg = offset_angle_new;
output.offset_axis= offset_axis_new;
output.deviations = dev;   % for all grain1; inc. NaN-s
output.tol        = tol;
output.gr1        = gr1;
output.gr2        = gr2;
%output.ok        = ok;
%output.tot1      = length(grain1);
%output.tot2      = length(grain2);

% unique grain indices; 'stable' -> keeps their order
% ?? shouldn't these be unique anyway? 
output.unique_inds1 = unique(output.match_ok(:, 1), 'stable')';
output.unique_inds2 = unique(output.match_ok(:, 2), 'stable')';


% Plot quiver 
figure('name','Remaining displacements (g2 --> g1)','Units','normalized','Position',[0 0 0.5 0.9])
ok = ~isnan(match(:,2));
c1 = gr1.center(ok,:);
c2 = gr2.center(match(ok,2),:);
vv = c1 - c2;
quiver3(c2(:,1), c2(:,2), c2(:,3), vv(:,1), vv(:,2), vv(:,3), 'g') 
title('Remaining displacements (g2 --> g1)')
legend({'g2 --> g1 vectors'}, 'Location', 'northeast')

% Plot centroids
figure('name','Matched grains','Units','normalized','Position',[0 0 0.5 0.9])
plot3(gr2.center(:,1), gr2.center(:,2), gr2.center(:,3), 'bo')
axis equal
xlabel X
ylabel Y
zlabel Z
hold on
cmatched = gr1.center(~isnan(match(:,2)),:);
cmissed  = gr1.center(isnan(match(:,2)),:);
plot3(cmatched(:,1), cmatched(:,2), cmatched(:,3), 'g.')
plot3(cmissed(:,1), cmissed(:,2), cmissed(:,3), 'r.')
title('Matched grain centroids')
legend({'g2','g1 matched','g1 unmatched'}, 'Location', 'northeast')

end % end of function


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SUB-FUNCTIONS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function gr = sfGetGrainInfo(grain, pxsize, volume_lim_low, ...
    volume_lim_hi, use_orig, out)
% Loads relevant centroid data of all grains in compact form (vectorised);
% selects grains only from within restricted volume.
%
% gr.
%    ind        grain indices as in input 
%    center     [um]
%    R_vector   [dim less]
%    bbxs,bbys  [um]
%    int
%    pixsize    [um]
%    used

nof_grains = length(grain);
gr.ind = (1:nof_grains)';

if use_orig && isfield(grain, 'orig')
    gr.center   = gtIndexAllGrainValues(grain, 'orig', 'center', 1, 1:3)*1000; %um
    gr.R_vector = gtIndexAllGrainValues(grain, 'orig', 'R_vector', 1, 1:3);
    out.odisp('Using original values for ''center'' and ''R_vector'' for the current grain')
else
    gr.center   = gtIndexAllGrainValues(grain, 'center', [], 1, 1:3)*1000; %um
    gr.R_vector = gtIndexAllGrainValues(grain, 'R_vector', [], 1, 1:3);
end
%gr.R_onedge(:, 1) = gtIndexAllGrainValues(grain, 'R_onedge', [], 1, 1);

try
    gr.bbxs(:, 1) = gtIndexAllGrainValues(grain, 'stat', 'size_int', 1, 1); %pixels
    gr.bbys(:, 1) = gr.bbxs(:, 1); %pixels
    out.odisp('Using ''size_int'' values from stat for the current grain')
    out.odisp('size_int = 2*(intmean/sum_intmean*volSample).^(1/3); %mm')
catch mexc
    %gtPrintException(mexc)
    if isfield(grain, 'stat') && isfield(grain.stat, 'bbxsmean') && ...
        ~isempty(grain.stat.bbxsmean)
        out.odisp('Using grain sizes from grain.stat.bbxsmean and bbysmean.');
        gr.bbxs(:, 1) = gtIndexAllGrainValues(grain, 'stat', 'bbxsmean', 1, 1); %pixels
        gr.bbys(:, 1) = gtIndexAllGrainValues(grain, 'stat', 'bbysmean', 1, 1); %pixels
    else
        out.odisp('Could not use grain sizes from grain.stat.bbxsmean and bbysmean.');
        out.odisp('  Using a fixed grain size of 1 pixel ...') 
        gr.bbxs(:, 1) = ones(nof_grains, 1); % pixels
        gr.bbys(:, 1) = ones(nof_grains, 1); % pixels
    end
    out.odisp('Field ''size_int'' is not stored in the grain.stat structure.')
end

if isfield(grain, 'stat') && isfield(grain.stat, 'intmean') && ...
    ~isempty(grain.stat.intmean)
    gr.int(:, 1) = gtIndexAllGrainValues(grain, 'stat', 'intmean', 1, 1);
else
    out.odisp('Could not use grain spot intensity from grain.stat.intmean.')
    out.odisp('  Using a fixed intensity of 1 ...')
    gr.int = ones(nof_grains, 1);
end

gr.pixsize = pxsize * 1000; %um

% convert grain sizes in microns
gr.bbxs = gr.bbxs .* gr.pixsize; %um
gr.bbys = gr.bbys .* gr.pixsize; %um


% Filter out grains on edges
if isempty(volume_lim_low)
    gr.used = true(nof_grains, 1);
else
    % only use grains that are within the restricted volume fraction;
    % find those grains
    cmin = min(gr.center,[],1);
    cmax = max(gr.center,[],1);
    csize = cmax - cmin;
    limlow = cmin + volume_lim_low .* csize;
    limhi  = cmax - (1 - volume_lim_hi) .* csize;  % avoid numerical errors
    
    % vectorised form of: limlow <= gr.center <= limhi
    ok = bsxfun(@ge, gr.center, limlow) & bsxfun(@le, gr.center, limhi);
    ok = all(ok, 2);
    ok_ratio = sum(ok) / length(ok);
    
    out.fprintf('Fraction of grain centroids within the volume range limits: %f %%', ok_ratio*100)
    
    gr.used = ok;
end

end % end function sfGetGrainInfo


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function grk = sfSelectGrains(keeplist, gr)
% Selects a subset of grains from gr structure
    grk.ind      = gr.ind(keeplist);
    grk.center   = gr.center(keeplist, :);
    grk.R_vector = gr.R_vector(keeplist, :);
    grk.bbys     = gr.bbys(keeplist);
    grk.bbxs     = gr.bbxs(keeplist);
    grk.int      = gr.int(keeplist);
    grk.orimat   = gr.orimat(:, :, keeplist);
    %grk.R_onedge = gr.R_onedge(keeplist, :);
    %grk.Rlines   = gr.Rlines(keeplist);
    %grk.Rids     = gr.Rids(keeplist);
end % end function sfSelectGrains


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [tomatch, misang] = sfCheckMatching(act, cand, tol, fzone_acc,...
    fzone_ext, symm)
% Checks 

    tomatch = (1:length(cand.ind))';  % initial indices of grain candidates to be merged
    misang = [];

    % Delete those indices that don't meet the following constraints:

    % Distance of centers close enough?
    %     if (tol.absolute)
    %         toldist = tol.distf;
    %     else
    %         toldist = min(act.bbxs, act.bbys) * tol.distf;
    %     end
    
    % distance tolerance is the larger of the relative and absolute
    % values
    toldist = max(min(act.bbxs, act.bbys) * tol.dist_rel, tol.dist_um);

    dvec = repmat(act.center, length(cand.ind), 1) - cand.center;
    dist = sqrt(sum(dvec.*dvec, 2));
    cons = dist < toldist;
    cand = sfSelectGrains(cons, cand);
    tomatch(~cons) = [];

    % Bounding box X size close enough?
    cons = (cand.bbxs > act.bbxs/tol.bbxs) & ...
           (cand.bbxs < tol.bbxs*act.bbxs);
    cand = sfSelectGrains(cons, cand);
    tomatch(~cons) = [];

    % Bounding box Y size close enough?
    cons = (cand.bbys > act.bbys/tol.bbys) & ...
           (cand.bbys < tol.bbys*act.bbys);
    cand = sfSelectGrains(cons, cand);
    tomatch(~cons) = [];

    % Intensity close enough?
    cons = (cand.int > act.int/tol.int) & ...
           (cand.int < tol.int*act.int);
    cand = sfSelectGrains(cons, cand);
    tomatch(~cons) = [];

    if isempty(tomatch)
        return;
    end

    % Misorientation small enough?
    nc = size(cand.orimat, 3);
    cons = false(nc, 1);
    misang = Inf(nc, 1);
    for ii = 1 : nc 
        misang(ii) = gtDisorientation(act.orimat, cand.orimat(:,:,ii),...
            symm, 'input', 'orimat'); % [deg]
        cons(ii) = misang(ii) < tol.misangle_deg;
    end
    
    tomatch(~cons) = [];
    misang(~cons)  = [];
    
    if 0
        % Rodriguez vectors close enough?
        
        % If any of them is on edge of fundamental zone
        if any([act.R_onedge; cand.R_onedge])
            
            % Do Rodrigues test from scratch
            
            Rlines = vertcat(act.Rlines{:}, cand.Rlines{:});
            Rids   = vertcat(act.Rids{:}, cand.Rids{:});
            
            candinds = zeros(length(act.Rids{:}), 1);
            for jj = 1:length(tomatch)
                candinds = [candinds; jj(ones(length(cand.Rids{jj}), 1))];
            end
            
            % Find new Rodrigues vector
            [~, ~, goodlines] = gtCrystRodriguesTestCore(Rlines, Rids, ...
                fzone_acc, fzone_ext, tol.Rdist);
            
            
            % Check which candidate has all of its 3 reflections accepted
            candinds = candinds(goodlines);
            candinds(candinds==0) = [];
            
            unicandinds = unique(candinds);
            
            okinds = false(size(tomatch));
            
            for jj = 1:length(unicandinds)
                if (sum(candinds==unicandinds(jj)) == 3)
                    okinds(unicandinds(jj)) = true;
                end
            end
            
            tomatch = tomatch(okinds);
            
        else
            dRvec = repmat(act.R_vector, length(cand.ind), 1) - cand.R_vector;
            Rdist = sqrt(sum(dRvec.*dRvec, 2));
            cons  = Rdist < tol.Rdist;
            tomatch(~cons) = [];
        end
    end
end % end function sfCheckMatching


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% UNUSED SUB-FUNCTIONS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function gr = sfGetRodriguesCoordinates(gr, nof_grains, offset_rot, cryst,...
    tol, fzone_acc, fzone_ext, out)
%!!! Function not used !

% Calculate Rodrigues lines inside the fundamental zone
gr.Rlines = cell(nof_grains, 1);
gr.Rids   = cell(nof_grains, 1);
%gr.Rvec   = gr.R_vector;

% Coordinate transformation: Crystal -> Sample
%   Plane normals in the Sample reference:
%     cry2sam_col = gtFedRod2RotTensor(Rvec);
%     v_sam = cry2sam_col * v_cry;
%   OR
%     cry2sam_row = gtMathsRod2OriMat(Rvec);
%     v_sam = v_cry * cry2sam_row;
%

% All orientation matrices in dataset
all_g = gtMathsRod2OriMat(gr.R_vector.');

%if (any(gr.R_onedge) || ~isempty(offset_rot))
if 1

    % global rotational correction in the Sample ref
    if ~isempty(offset_rot)
        % use transpose of offset_rot for row vectors        
        all_g = gtMathsMatrixProduct(all_g, offset_rot');
    end
    
    Bmat = gtCrystHKL2CartesianMatrix(cryst.latticepar);
   
    if (isfield(cryst, 'hklsp') && ~isempty(cryst.hklsp))

        % Take the first three plane normals
        hklsp = cryst.hklsp(:, 1:3);

        % All signed hkl-s for the given family
        shkl1 = cryst.hklsp(:, cryst.thetatypesp == cryst.thetatypesp(1));
        shkl2 = cryst.hklsp(:, cryst.thetatypesp == cryst.thetatypesp(2));
        shkl3 = cryst.hklsp(:, cryst.thetatypesp == cryst.thetatypesp(3));

        % Both hkl and -h-k-l have to be considered:
        shkl1 = [shkl1, -shkl1];
        shkl2 = [shkl2, -shkl2];
        shkl3 = [shkl3, -shkl3];

    else
        % Take the {1, 0, 0} planes (doesn't matter if reflections really exist)
        if size(cryst.hkl, 1) == 3
            hklsp = [1 0 0; 0 1 0; 0 0 1]';
        elseif size(cryst.hkl, 1) == 4
            hklsp = [1 0 -1 0; 0 1 -1 0; 0 0 0 1]';
        else
            gtError('gtINDEXMatchGrains_2:wrongParametersField', 'Field parameters.cryst.hkl is not set correctly.')
        end
        symm = gtCrystGetSymmetryOperators([], cryst.spacegroup);
        % All signed hkl-s for the given family (both hkl and -h-k-l)
        shkl1 = gtCrystSignedHKLs(hklsp(:, 1)', symm)';
        shkl2 = gtCrystSignedHKLs(hklsp(:, 2)', symm)';
        shkl3 = gtCrystSignedHKLs(hklsp(:, 3)', symm)';

    end

    % Plane normals with Cartesian Crystal coordinates
    pl_cry = gtCrystHKL2Cartesian(hklsp, Bmat);  % takes and returns column vectors

    out.odisp('Calculating Rodrigues coordinates ...');

    % Generate a prime number larger than 3 for each grain to be used to
    % generate distinct reflection id-s later
    pr = primes(1e6);
    pr(1:2) = [];
    pr(nof_grains+1:end) = [];

    % Plane normals in the Sample ref. of dataset1
    %   input: row vectors (Mx3) and matrices (3x3xN)
    %   output: row vectors (Mx3xN)
    %   v_sam(Mx3xN) = v_cry(Mx3) * g(3x3xN)
    all_pl_sam = gtVectorCryst2Lab(pl_cry.', all_g);

    for ii = 1:nof_grains

        % Plane normals in the Sample ref. of dataset1
        pl_sam = all_pl_sam(:, :, ii);

        % Lines in Rodrigues space (some might be empty)
        Rline1 = gtCrystRodriguesVector(pl_sam(1, :), shkl1', Bmat, fzone_ext);
        Rline2 = gtCrystRodriguesVector(pl_sam(2, :), shkl2', Bmat, fzone_ext);
        Rline3 = gtCrystRodriguesVector(pl_sam(3, :), shkl3', Bmat, fzone_ext);

        gr.Rlines{ii} = [Rline1; Rline2; Rline3];

        % Vector of line indices to identify which plane they belong to
        gr.Rids{ii} = [1*ones(size(Rline1, 1), 1); 2*ones(size(Rline2, 1), 1);...
            3*ones(size(Rline3, 1), 1)];

        % For debugging: find Rodrigues vector
        [Rvec, ~, ~, ~, Ronedge] = gtCrystRodriguesTestCore(gr.Rlines{ii},...
            gr.Rids{ii}, fzone_acc, fzone_ext, tol.Rdist);

        if isempty(Rvec)
            out.odisp('WARNING! Rodrigues vector could not be fitted. Possible bug.')
            gr.R_vector(ii, :) = NaN;
            gr.rot(:, :, ii)  = NaN;
        else
            gr.R_vector(ii, :) = Rvec;
            gr.rot(:, :, ii)  = all_g(:, :, ii);
        end
        gr.R_onedge(ii) = Ronedge;

        % For debugging:
        %   Rvec = gtCrystRodriguesTest(pl_sam1', [shkl1(:, 1), shkl2(:, 1), shkl3(:, 1)]', spacegroup, Bmat, 0, 0, 0);
    end
else
    gr.rot = all_g;
end

end % end function sfGetRodriguesCoordinates


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
