function [grain, remaining] = gtIndexExecuteStrategy(strategy, ...
                              tot, samenv, cryst, grain)
% GTINDEXEXECUTESTRATEGY Executes the iterative indexing strategy.
%
% [grain, remaining] = gtIndexExecuteStrategy(strategy, tot, samenv, ...
%                                             cryst, grain)
%
% -----------------------------------------------------------------------
%
% Executes the iterative indexing strategy. Returns the grains that have 
% been found and the unindexed reflections. 
%
% INPUT
%   strategy.    - indexing strategy (as in parameters.index.strategy)
%   tot.         - all reflection data (including indexed and unindexed)
%   samenv.      - sample envelope in the Sample reference
%   cryst.       - crystallography and other data
%   grain.       - initial grain data
%
% OUTPUT
%   grain.       - grain data (cell vector)
%   remaining    - remaining unindexed reflection id-s
%


% Signs of steps of the indexing strategy:
%  b = build grains
%  m = merge identical grains
%  l = loosen parameters
%  r = refine grains - add reflections
%  s = add unindexed reflections to grains (statistical)
%  x = exclude outlier reflections from grains (statistical)


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Define processing steps
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Create the sequence of iterative processing; describe each
% processing step.

if ~isfield(strategy,'proc')
    strategy.proc = [];
end

    
if strategy.iter <= 1
    
    strategy.iter = 1;
    
    tol.b = strategy.b.end;
    tol.r = strategy.b.end;
    tol.m = strategy.m.end; 
else
    
    % Calculate the build and merge tolerances: scale linearly between
    % the beginning and end value.
    
    strfields = fieldnames(strategy.b.beg);
    for ii = 1:length(strfields)
        if ((strategy.b.beg.(strfields{ii}) == Inf) || ...
            (strategy.b.end.(strfields{ii}) == Inf))
            dif = Inf;
        else
            dif = strategy.b.end.(strfields{ii}) - strategy.b.beg.(strfields{ii});
        end
        strategy.l.b.(strfields{ii}) = dif/(strategy.iter-1);
    end
    
    strfields = fieldnames(strategy.m.beg);
    for ii = 1:length(strfields)
        if ((strategy.m.beg.(strfields{ii}) == Inf) || ...
            (strategy.m.end.(strfields{ii}) == Inf))
            dif = Inf;
        else
            dif = strategy.m.end.(strfields{ii}) - strategy.m.beg.(strfields{ii});
        end
        strategy.l.m.(strfields{ii}) = dif/(strategy.iter-1);
    end
    
    tol.b = strategy.b.beg;
    tol.r = strategy.b.beg;
    tol.m = strategy.m.beg;
end
    

if isempty(strategy.proc)
    
    if strategy.iter <= 1
    
        strategy.proc{1,1} = 'b';
        strategy.proc{2,1} = 'm';
        strategy.proc{3,1} = 'r';
    
    else
        sp{1,1} = 'b';
        sp{2,1} = 'm';
        sp{3,1} = 'l';
        sp{4,1} = 'r';
        
        for ii = 1:strategy.iter-1
            strategy.proc = [strategy.proc; sp];
        end
        
        strategy.proc{end+1} = 'b';
        strategy.proc{end+1} = 'm';
        strategy.proc{end+1} = 'r';
    end
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Run processing steps - iteration
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

nof_refs = length(tot.pairid);

for ii = 1:length(strategy.proc)
        
    fprintf('\n------------  STEP %d/%d  ------------\n', ...
            ii, length(strategy.proc))
    
    switch strategy.proc{ii}
        
        case 'l'  % LOOSEN TOLERANCES
            
            % Update the build and merge tolerances.
            
            fprintf('\n LOOSENING TOLERANCES...\n')
            
            strfields = fieldnames(strategy.b.beg);
            
            for jj = 1:length(strfields)
                tol.b.(strfields{jj}) = tol.b.(strfields{jj}) + ...
                                        strategy.l.b.(strfields{jj});
            end
            
            tol.r = tol.b;
            
            strfields = fieldnames(strategy.m.beg);
            
            for jj = 1:length(strfields)
                tol.m.(strfields{jj}) = tol.m.(strfields{jj}) + ...
                                        strategy.l.m.(strfields{jj});
            end
        
            
        case 'b'  % BUILD GRAINS
            
            % Find new grains among unindexed reflections.
            %   grainB      - newly created grains
            %   tot.indexed - updated
            [grainB, tot] = gtIndexBuildGrains(tot, tol.b, cryst, samenv);
            
            grain = [grain, grainB];
            
            for jj = 1:length(grain)
                grain{jj}.id = jj;
            end
         
            cfCheckOutput(grain, sum(~tot.indexed), nof_refs)
            
            
        case 'm'  % MERGE GRAINS
            
            % Find and merge identical grains that have been split.
            %   grain       - all grain data (will be updated)
            %   tot.indexed - updated
            [grain, tot] = gtIndexMergeGrains(grain, tot, tol.m, cryst);
            
            cfCheckOutput(grain, sum(~tot.indexed), nof_refs)
          
            
        case 'r'  % REFINE GRAINS
            
            % Add unindexed reflections to the grains.
            %   grain       - all grain data (will be updated)
            %   tot.indexed - updated
            [grain, tot] = gtIndexRefineGrains(grain, tot, tol.r, cryst);
            
            cfCheckOutput(grain, sum(~tot.indexed), nof_refs)
            
    end
    
    
end

fprintf('\n------------  END OF ITERATION  ------------\n')


remaining = find(~tot.indexed);


end % of main function



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% SUN-FUNCTION - checks output of each step
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Checks if the number of indexed and unindexed reflection is consistent. 

function cfCheckOutput(grain, nref_remain, nref_orig)
        
    nof_grains = length(grain);
    
    %allgrains.nof_pairs = [];
    allgrains.pairid    = [];
    
    for kk = 1:nof_grains
        %allgrains.nof_pairs = [allgrains.nof_pairs, grain{kk}.nof_pairs];
        allgrains.pairid    = [allgrains.pairid, grain{kk}.pairid];
    end
    
    nref_indexed = length(allgrains.pairid);
    %nref_remain  = length(remaining);
    nref_sum     = nref_indexed + nref_remain;
    
    fprintf('\nNumber of ...')
    fprintf('\n grains found         : %d',nof_grains)
    fprintf('\n indexed reflections  : %d',nref_indexed)
    fprintf('\n unindexed reflections: %d',nref_remain)
    fprintf('\n reflections in total : %d',nref_sum)
    fprintf('\n input reflections    : %d\n',nref_orig)
    
    if length(unique(allgrains.pairid)) ~= nref_indexed
        warning('GT:Indexing_error', ['Some reflections have been', ...
                ' assigned to more than one grain. Try to debug!'])
    end
    
    if nref_orig ~= nref_sum
        warning('GT:Indexing_error', ['Inconsistency in number of', ...
                ' indexed or remaining reflections. Try to debug!'])
    end
  
end % end of sub-function



