function grain = gtIndexEquivalentPairs(grain)
% GTINDEXEQUIVALENTPAIRS  Finds the equivalent Friedel pairs (two pairs
%                         that originate from the same plane) in all grains.
%
%     grain = gtIndexEquivalentPairs(grain)
%     -------------------------------------
%
%     INPUT:
%       grain          = <cell>       all grain data as in index.mat
%
%     OUTPUT:
%       grain          = <cell>       updated grain data
%            .equipair = <double>     linear indices of the corresponding Friedel pairs;
%                                     zero if no corresponding pair was found.
%            .equilist = <double>     list of equivalent Friedel pair indices


for ii = 1:length(grain)

    grain{ii}.equipair = zeros(1,length(grain{ii}.hklind));
    grain{ii}.equilist = [];
    
    % Loop through Friedel pairs of the grain
    for jj = 1:length(grain{ii}.hklind);
        
        % Check which linear indices of {hkl} and (hkl) match
        inds1     = (grain{ii}.hklind  == grain{ii}.hklind(jj));
        inds2     = (grain{ii}.shklind == grain{ii}.shklind(jj));
        inds2(jj) = false; 

        % Index of equivalent pair
        equiind = find(inds1 & inds2);
        
        if ~isempty(equiind)   
            
            % Maximum one equivalent pair can exist
            if (length(equiind) > 1)
                warning('gtIndexEquivalentPairs:exceeding_pairs_size','More than two equivalent Friedel pairs found for pair #%d in grain #%d', jj, ii)
                disp('The equivalent pair with the lower index was selected.')
            end
            
            grain{ii}.equipair(jj) = equiind(1);
            
            if (jj < grain{ii}.equipair(jj))
                grain{ii}.equilist(end+1,:) = [jj grain{ii}.equipair(jj)];
            end
        end
        
    end % end for jj
    
end % end for ii

end % end of function
