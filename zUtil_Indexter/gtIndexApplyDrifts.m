function fsim = gtIndexApplyDrifts(pl, dsp, gc, om, drift, detgeo, labgeo, ...
                samgeo, rotcomp, energy)
% pl    - plane normals before drifts
% sinth - sin(theta) in deformed state
% om    - omega-s to interpolate the rotational drift
% gc    - grain center in Sample reference before drift
%
% Use the measured omega position as an approximation to calculate
% the drifts, because the fwd simulated is not yet known. As the
% drifts are assumed smooth and slow, this is a good approximation.

    % Plane normals, grain centers and sin(theta) after drifts
    [pld, gcd, sinthd] = gtFitApplyDrifts(pl, gc, dsp, om, drift, energy);
    
    % The plane normals need to be brought in the Lab reference where the 
    % beam direction and rotation axis are defined.
    % Use the Sample -> Lab rotation assuming omega=0;
    % (samgeo.dir norm is always 1, so sam2lab will be preserve vector norms)
    sam2lab = [samgeo.dirx; samgeo.diry; samgeo.dirz]';
    pld     = sam2lab*pld;
    
    
    % Predict omega angles: 4 for each plane normal (the Friedel pairs are
    % om1-om3 and om2-om4)
    [om4, pllab4, ~, rot4, omind4] = gtFedPredictOmegaMultiple(...
        pld, sinthd, labgeo.beamdir', labgeo.rotdir', rotcomp, []);
    
    % Check if all reflections occur
    noref = any(isnan(om4), 1);
    if any(noref)
        error('No reflection was predicted for some pairs in grain.');
    end
    
    % Find the correct omega index for the pairs where the difference is
    % the smallest (shouldn't matter if grain.omega is the measured one
    % or the one corrected for the pair).
    
    % Get fwd simulated data for the observed reflections
    fsim = gtIndexFwdSim(om, gcd, ...
           sinthd, om4, pllab4, rot4, omind4, ...
           detgeo, labgeo, samgeo);
    
end

