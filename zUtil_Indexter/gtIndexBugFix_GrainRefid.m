function gtIndexBugFix_GrainRefid()

disp('Working on phase 1.')
disp(pwd)


load 4_grains/phase_01/index.mat

if isfield(grain{1},'refid')
    disp('Found field ''refid''. Correcting now...')
else
    disp('No ''refid'' field found. Adding now...')
end


for ii = 1:length(grain)
    for jj = 1:length(grain{ii}.pairid)
        grain{ii}.refid(jj) = find(tot.pairid==grain{ii}.pairid(jj));
    end
end


clear ii
clear jj

disp('Overwriting  4_grains/phase_01/index.mat ...')
save('4_grains/phase_01/index.mat')
    
disp('Done.')