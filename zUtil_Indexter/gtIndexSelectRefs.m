function refssel = gtIndexSelectRefs(keeplist, refs)
% GTINDEXSELECTREFS Helper to load a sub-set of reflections.
%
% refssel = gtIndexSelectRefs(keeplist, refs)
%
% --------------------------------------------------
%
% A helper function to load the sub-set 'keeplist' of reflection data from 
% the complete set 'refs'.
%
% INPUT
%   keeplist  - linear or logical indices of reflections to be selected
%   refs      - reflection data
%
% OUTPUT
%   refssel   - selected sub-set of reflection data
%


refssel.id     = refs.id(keeplist);
refssel.pairid = refs.pairid(keeplist);
refssel.theta  = refs.theta(keeplist);
refssel.int    = refs.int(keeplist);
refssel.bbxs   = refs.bbxs(keeplist);
refssel.bbys   = refs.bbys(keeplist);

refssel.ca     = refs.ca(keeplist,:);
refssel.dir    = refs.dir(keeplist,:);
refssel.pl     = refs.pl(keeplist,:);

refssel.hkl    = refs.hkl(keeplist,:);

refssel.hklind    = refs.hklind(keeplist);      % cell vector
refssel.shklind   = refs.shklind(keeplist);

refssel.Rline     = refs.Rline(keeplist);       % cell vector
refssel.Rhklind   = refs.Rhklind(keeplist);     % cell vector
refssel.Rshklind  = refs.Rshklind(keeplist);    % cell vector

% hack for poly stuff
if isfield(refs, 'g3d')
    refssel.g3d    = refs.g3d(keeplist, :);
end

end
