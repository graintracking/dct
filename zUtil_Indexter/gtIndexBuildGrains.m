function [grain, tot] = gtIndexBuildGrains(tot, tol_build, cryst, samenv)
% GTINDEXBUILDGRAINS Finds new grains (one of the main stages of Indexter).
% 
% [grain, tot] = gtIndexBuildGrains(tot, tol_build, cryst, samenv)
% 
% ----------------------------------------------------------------
%
% 1st step out of 3 of the iterative indexing strategy. Finds grains among
% the reflections, returns the grains with their orientation, position and
% statistical data on the reflections.
% The signed hkl indices of the reflections are also determined.
% Unindexed reflection id-s are returned in 'unindexed'.  
%
% INPUT
%  tot.         - all reflection data (including indexed and unindexed)
%  tol_build.   - tolerances for building grains;
%                 (as in paramers.index.strategy.b.beg or b.end)
%  cryst.       - crystallography and other data
%  samenv.      - sample envelope in the Sample reference
%
% OUTPUT
%  grain        - grain data (cell vector)
%  tot.indexed  - updated; true for the indexed reflection ID-s
%

                      
fprintf('\n BUILDING GRAINS...\n\n')
disp(tol_build)
disp(' ')
tic

nof_grains = 0;
grain      = [];
exclude_already_indexed = true;

% Loop through all reflections

for ii = 1:length(tot.indexed)
    
    if tot.indexed(ii) && exclude_already_indexed
        continue
    end
    
    % Current reflection to be tested
    act = gtIndexSelectRefsReduced(ii, tot);     
    
    % Tolerance for distance between diffraction paths is set equal to the 
    % approximate grain size times a constant, but is limited to a max.
    tol_build.dist = min([act.bbxs, act.bbys]) * cryst.pixelsize * ...
                     tol_build.distf ;
    tol_build.dist = min(tol_build.dist, tol_build.distmax) ;
    
    % Indices to check
    if exclude_already_indexed
        inind       = ~tot.indexed;
    else
        inind       = [1:numel(tot.indexed)];
    end
    inind(1:ii) = false;
    
    % Check pair consistency of reflections (all candidates against
    % actual).
    % 'paircons' is a vector of the linear indices of consistent 
    % reflections in tot.
    [paircons, isecs] = gtIndexCheckPairCons(act, tot, ...
                        tol_build, samenv, cryst.ACM, tol_build.ming-1, inind);
    
    % If there are enough pair consistent candidates
    if length(paircons) >= tol_build.ming-1
        
        % Load reflection data for candidates
        cand_paircons = gtIndexSelectRefs([act.id; tot.id(paircons)], tot);
        
        % Find a consistent group among those candidates (check group 
        % consistency)
        [pgroup, grainfound] = gtIndexFindGroupInCand(cand_paircons, ...
                               isecs, tol_build, samenv, cryst);
        
        % If there is a consistent group found
        if ~isempty(grainfound)
           
            nof_grains           = nof_grains + 1;
            grain{nof_grains}    = grainfound;
            grain{nof_grains}.id = nof_grains;
            
            fprintf('Found new grain #%d:   %s\n', nof_grains, ...
                    num2str(grain{nof_grains}.pairid))
            
            tot.indexed(cand_paircons.id(pgroup)) = true;           
        end
        
    end
   
    
end


disp(' ')
toc


end % of function

