function [gid, conf, sp] = gtIndexDifSpots_2(grain, sp, parameters, fsimID, lim)
% GTINDEXDIFSPOTS  Finds grain ID-s for all diffraction spots
%
%     [gid, conf, sp] = gtIndexDifSpots(grain, sp, parameters, [fsimID], lim)
%     -----------------------------------------------------------------------
%
%     It tries to assign a grain ID to each diffraction spot. Those for which
%     no acceptable grain is found or compatible with more than one grain
%     (conflict) will have zero assigned.
%
%     Run these functions beforehand:
%       grain = gtIndexFwdSimGrains(grain, parameters);
%
%     INPUT:
%       grain      = <cell>    grain data
%       sp         = <struct>  diffraction spot data relative to grain
%                              peaks_info.mat file or equivalent can be 
%                              used, which will be translated to sp
%       parameters = <struct>  parameters.mat (needed samgeo)
%       fsimID     = <double>  fsim arrayID {1}
%                              (for multiple forward simulation results)
%                              i.e. multiple detectors fsim : 
%                                   grain.fsim(1), grain.fsim(2)
%       pairinfo   = <logical> if exists pair information {true}
%       lim        = <struct>  limits in mm and degrees (to be converted back
%                              to pixels and image number)
%
%     OUTPUT:
%       gid        = <double>  list of grain ID-s
%       conf       = <double>  list of conflicts
%       sp         = <struct>  updated sp info (like difspot table)

    %%%%%%%%%%%%%%%%%%
    % Prepare input
    %%%%%%%%%%%%%%%%%%

    if (~exist('grain', 'var') || isempty(grain))
        gtError('gtIndexDifspots:missingArgument', ...
            'Argument ''grain'' must be given...Quitting')
    end
    if (~exist('sp', 'var') || isempty(sp))
        gtError('gtIndexDifspots:missingArgument', ...
            'Argument ''sp'' must be given...Quitting')
    end
    if (~exist('parameters', 'var') || isempty(parameters))
        gtError('gtIndexDifspots:missingArgument', ...
            'Argument ''parameters'' must be given...Quitting')
    end
    if (~exist('lim', 'var') || isempty(lim))
        gtError('gtIndexDifspots:missingArgument', ...
            'Argument ''lim'' must be given...Quitting')
    end

    if (~exist('fsimID', 'var') || isempty(fsimID))
        fsimID = 1;
    end

    labgeo = parameters.labgeo;
    if (~isfield(labgeo, 'omstep') || isempty(labgeo.omstep))
        labgeo.omstep = gtAcqGetOmegaStep(parameters);
    end
    if (~isfield(parameters, 'detgeo') || isempty(parameters.detgeo))
        parameters.detgeo = gtGeoConvertLegacyLabgeo2Detgeo(labgeo);
    end
    detgeo = parameters.detgeo;

    % Get spot and pair data
    if all(isfield(sp, {'difspotID', 'CentroidX', 'CentroidY', 'CentroidImage'}))

        sp.uvw  = [sp.CentroidX sp.CentroidY sp.CentroidImage];
        sp.om   = sp.CentroidImage * labgeo.omstep;
        sp.id   = sp.difspotID;

        pairsinfo = true;
    elseif all(isfield(sp, {'difspotidA', 'difspotidB', 'omega', 'omegaB', ...
            'eta', 'etaB', 'theta', 'grainID', ...
            'samcentXA', 'samcentYA', 'samcentZA', ...
            'samcentXB', 'samcentYB', 'samcentZB'}))

        sp.eta   = [sp.eta, sp.etaB];
        sp.id    = [sp.difspotidA, sp.difspotidB];
        sp.om    = [sp.omega, sp.omegaB];
        sp.uvw(:, 3) = sp.om / labgeo.omstep;

        xyzA  = [sp.samcentXA sp.samcentYA sp.samcentZA];
        xyzB  = [sp.samcentXB sp.samcentYB sp.samcentZB];
        xyz   = [xyzA; xyzB];

        uvwLab = gtGeoSam2Lab(xyz, sp.om, labgeo, parameters.samgeo, false);
        sp.uvw(:, 1:2) = gtGeoLab2Det(uvwLab, detgeo, 0);

        pairsinfo = true;
    % check if it is by chance a taper scan, so then it contains those fields
    elseif all(isfield(sp, {'spot3d_id', 'fc', 'sc', 'omega'}) & ~isfield(sp, 'cu'))

        sp.uvw  = [sp.fc sp.sc sp.omega / labgeo.omstep];
        sp.om   = sp.omega;
        sp.id   = sp.spot3d_id;

        pairsinfo = false;
    else
        gtError('gtIndexDifspots:wrong_input', 'Wrong input for ''sp''... Quitting!')
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Vectorise fwd simulated grain spots
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    uvw = gtIndexAllGrainValues(grain, 'fsim', 'uvw', fsimID, [], false);
    uvw = [uvw{:}];
    gr.u = uvw(1, :);
    gr.v = uvw(2, :);
    gr.w = uvw(3, :);

    gr.om = gtIndexAllGrainValues(grain, 'fsim', 'om', fsimID, [], false);
    gr.om = [gr.om{:}];
    gr.id =[];
    for ii=1:length(grain)
        gr.id = [gr.id; grain{ii}.id(ones(1, length(grain{ii}.fsim(fsimID).om)))'];
    end
    gr.id = gr.id';

    disp(['current pixelsizeu: ' num2str(detgeo.pixelsizeu)])
    disp(['current pixelsizev: ' num2str(detgeo.pixelsizev)])
    disp(['omega step:         ' num2str(labgeo.omstep)])

    % pixelsize of fsim detector
    if (pairsinfo)
        lim.u = lim.u / detgeo.pixelsizeu; % pixels
        lim.v = lim.v / detgeo.pixelsizev; % pixels
        lim.w = lim.w / labgeo.omstep; % image number
    end

    print_structure(lim, 'lim')

    %%%%%%%%%%%%%%%%%%%%%%%%%%
    % Loop to find grain ID-s
    %%%%%%%%%%%%%%%%%%%%%%%%%%

    gid  = zeros(1, length(sp.uvw));
    conf = zeros(1, length(sp.uvw));
    ok   = false(length(gr.id), 3);

    gauge = GtGauge(length(sp.uvw), 'Processing spots: ');

    %tic

    % Loop through spots - could be a parallel parfor loop
    for ii = 1:length(sp.uvw)
        gauge.incrementAndDisplay(100);

        % Check if position is within limits
        ok(:, 1) = abs(gr.u - sp.uvw(ii, 1)) < lim.u;
        ok(:, 2) = abs(gr.v - sp.uvw(ii, 2)) < lim.v;
        ok(:, 3) = abs(gr.w - sp.uvw(ii, 3)) < lim.w;

        oks = all(ok, 2);
        sumoks = sum(oks);

        % Check metadata, if exactly one candidate grain found
        if (sumoks == 1)

            idg = gr.id(find(oks, 1, 'first'));
            ids = gtIndexAllGrainValues(grain, 'id', [], [], []);
            ind = find(ids == idg);

            gid(ii) = idg;

        elseif (sumoks > 1)
            % spot in conflict
            conf(ii) = sum(oks);
        end
    end

    gauge.delete();

    %toc;
    disp(' ')
    fprintf('No. of spots analysed   : %d\n', length(gid));
    fprintf('No. of spots indexed    : %d\n', sum(gid>0));
    fprintf('No. of spots in conflict: %d\n', sum(conf));
end
