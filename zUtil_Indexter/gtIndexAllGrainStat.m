function [allgrainstat, outliers] = gtIndexAllGrainStat(grain, plotfigure)
% Creates grain statistics from all the grains and plots figures.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Collect values for all grains 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

nof_grains = length(grain);

allgrainstat.nof_pairs  = [];
allgrainstat.intrat     = [];
allgrainstat.normintrat = [];
allgrainstat.bbxsrat    = [];
allgrainstat.bbysrat    = [];
allgrainstat.dcom       = [];
allgrainstat.dang       = [];
allgrainstat.pairid     = [];
allgrainstat.grainid    = [];

outliers.dcom    = [];
outliers.dang    = [];
outliers.int     = [];
outliers.normint = [];
outliers.bbxs    = [];
outliers.bbys    = [];


% For all grains

for ii = 1:nof_grains
    
    % Gather data in one vector
    
    allgrainstat.nof_pairs = [allgrainstat.nof_pairs, grain{ii}.nof_pairs];
    allgrainstat.pairid    = [allgrainstat.pairid, grain{ii}.pairid];
    
    allgrainstat.dcom       = [allgrainstat.dcom, grain{ii}.dcom];
    allgrainstat.dang       = [allgrainstat.dang, grain{ii}.dang];
    allgrainstat.intrat     = [allgrainstat.intrat, grain{ii}.stat.intrat];
    allgrainstat.normintrat = [allgrainstat.normintrat, grain{ii}.stat.normintrat];
    allgrainstat.bbxsrat    = [allgrainstat.bbxsrat, grain{ii}.stat.bbxsrat];
    allgrainstat.bbysrat    = [allgrainstat.bbysrat, grain{ii}.stat.bbysrat];
    
    allgrainstat.grainid    = [allgrainstat.grainid, ...
                               repmat(ii,1,length(grain{ii}.pairid))];
    
   
    % Find outliers
    if ~isempty(grain{ii}.stat.outliers.int)
        tmp = [repmat(ii,length(grain{ii}.stat.outliers.int),1) grain{ii}.stat.outliers.int'];
        outliers.int = [outliers.int; tmp];
    end
    
    if ~isempty(grain{ii}.stat.outliers.normint)
        tmp = [repmat(ii,length(grain{ii}.stat.outliers.normint),1) grain{ii}.stat.outliers.normint'];
        outliers.normint = [outliers.normint; tmp];
    end
    
    if ~isempty(grain{ii}.stat.outliers.bbxs)
        tmp = [repmat(ii,length(grain{ii}.stat.outliers.bbxs),1) grain{ii}.stat.outliers.bbxs'];
        outliers.bbxs = [outliers.bbxs; tmp];
    end
    
    if ~isempty(grain{ii}.stat.outliers.bbys)
        tmp = [repmat(ii,length(grain{ii}.stat.outliers.bbys),1) grain{ii}.stat.outliers.bbys'];
        outliers.bbys = [outliers.bbys; tmp];
    end
    
    if ~isempty(grain{ii}.stat.outliers.dcom)
        tmp = [repmat(ii,length(grain{ii}.stat.outliers.dcom),1) grain{ii}.stat.outliers.dcom'];
        outliers.dcom = [outliers.dcom; tmp];
    end
    
    if ~isempty(grain{ii}.stat.outliers.dang)
        tmp = [repmat(ii,length(grain{ii}.stat.outliers.dang),1) grain{ii}.stat.outliers.dang'];
        outliers.dang = [outliers.dang; tmp];
    end
    
end


%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Plot Figures
%%%%%%%%%%%%%%%%%%%%%%%%%%

if plotfigure
    nof_bins = nof_grains;

    hf = figure;

    subplot(2,4,1)
    hold on
    title('No. of pairs in the grains','FontWeight','bold')
    ylabel('frequency')
    xlabel('(no. of pairs)')
    hist(allgrainstat.nof_pairs, 1:max(allgrainstat.nof_pairs))

    subplot(2,4,2)
    hold on
    title('Plane normals angular dev.','FontWeight','bold')
    xlabel('(deg)')
    hist(allgrainstat.dang,nof_bins)

    subplot(2,4,6)
    hold on
    title('Diff. path dist. from center','FontWeight','bold')
    xlabel('(lab unit)')
    hist(allgrainstat.dcom,nof_bins)

    subplot(2,4,3)
    hold on
    title('Max. intensity ratio','FontWeight','bold')
    ylabel('frequency')
    hist(allgrainstat.intrat,round(nof_bins/10))

    subplot(2,4,7)
    hold on
    title('Max. normalised int. ratio','FontWeight','bold')
    ylabel('frequency')
    hist(allgrainstat.normintrat,round(nof_bins/10))

    subplot(2,4,4)
    hold on
    title('Max. bbox U size ratio','FontWeight','bold')
    hist(allgrainstat.bbxsrat,round(nof_bins/10))

    subplot(2,4,8)
    hold on
    title('Max. bbox V size ratio','FontWeight','bold')
    hist(allgrainstat.bbysrat,round(nof_bins/10))


    set(hf,'Units','normalized')
    set(hf,'Position',[0.05 0.05 0.9 0.9])
    set(hf,'Name','Grain statistics')
end


end % of function
