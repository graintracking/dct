function tomerge = gtIndexCheckMergeGrains(act, cand, grain, tot, ...
                                           tol_merge, cryst)
%
% FUNCTION tomerge = gtIndexCheckMergeGrains(act, cand, grain, tot, ...
%                                            tol_merge, cryst)
% 
% Checks which of the candidate grains 'cand' are identical and can be 
% merged with the actual grain 'act'. Merge criteria are that differences 
% in bounding box sizes, intensity, location and orientation are within 
% the specified tolerances 'tol_merge'. 
%
% INPUT
%
%  act.       - reduced grain data of actual grain to be checked against
%  cand.      - reduced grain data of other candidate grains to be checked
%  grain      - all grain data
%  tot.       - all reflection data (including indexed and unindexed)
%  tol_merge. - tolerances for merging grains;
%               (as in paramers.index.strategy.m.beg or m.end)
%  cryst.     - crystallography and other data
%
% OUTPUT
%  tomerge    - linear indices in 'cand' that should be merged with 'act'
%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Check meta-data and distance
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% Initial indices of grain candidates to be merged. Indices that don't 
% meet the constraints below will be deleted step by step.
tomerge = (1:length(cand.id))'; 


% Bounding box Y size close enough?
cons = (cand.bbys >= act.bbys/tol_merge.bbys) & ...
       (cand.bbys <= act.bbys*tol_merge.bbys);
   
cand = sfSelectGrains(cons, cand);
tomerge(~cons) = [];
if isempty(tomerge)
	return
end


% Spatial distance of centers close enough?
dvec = act.center(ones(size(cand.center,1),1),:) - cand.center;
dist = sqrt(sum(dvec.*dvec,2));
cons = dist <= tol_merge.dist;

cand = sfSelectGrains(cons, cand);
tomerge(~cons) = [];
if isempty(tomerge)
	return
end


% Bounding box X size close enough?
cons = (cand.bbxs >= act.bbxs/tol_merge.bbxs) & ...
	   (cand.bbxs <= act.bbxs*tol_merge.bbxs);

cand = sfSelectGrains(cons, cand);
tomerge(~cons) = [];
if isempty(tomerge)
	return
end


% Normalised intensity close enough? 
% % !!! normalized intensities should be used in the future
% cons = (cand.normint > act.normint/tol_merge.int) & ...
% 	     (cand.normint < act.normint*tol_merge.int);
cons = (cand.int >= act.int/tol_merge.int) & ...
	   (cand.int <= act.int*tol_merge.int);

cand = sfSelectGrains(cons, cand);
tomerge(~cons) = [];
if isempty(tomerge)
	return
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Check orientation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Orientation is checked as the difference between Rodrigues vectors. 

% If any Rodrigues vector is close to edge of fundamental zone
if any([act.R_onedge; cand.R_onedge])
	
	% Reflection data from actual grain
	ref1 = gtIndexSelectRefs(grain{act.id}.refid, tot);
	
	% List of consistent candidate grains
	cons = false(length(cand.id),1);
	
	% Loop through remaining candidate grains
	for ii = 1:length(cand.id)
        
        % Difference between the two Rodrigues vectors
        diffRvec = act.R_vector - cand.R_vector(ii,:);
        
        % Distance in Rodrigues space
        Rdist    = sqrt(sum(diffRvec.*diffRvec,2));
        
        % If distance is small enough, orientation is identical
        cons(ii)  = (Rdist <= tol_merge.Rdist);
        
        
        % Double check, in case of other side of fundamental zone

        if ~cons(ii)
            
            % Reflection data from candidate grain
            ref2 = gtIndexSelectRefs(grain{cand.id(ii)}.refid, tot);
            
            
            % Concatenate refids in one vector as many times as many
            % reflections they have
            
            refids = [];
            
            for jj = 1:length(ref1.id)
                refids = [refids; ref1.id(jj,ones(size(ref1.Rline{jj},1),1))'];
            end
            
            for jj = 1:length(ref2.id)
                refids = [refids; ref2.id(jj,ones(size(ref2.Rline{jj},1),1))'];
            end
            
            [~,goodrefids] = gtCrystRodriguesTestCore(...
                vertcat(ref1.Rline{:}, ref2.Rline{:}), ...
                refids, cryst.Rfzone_acc, cryst.Rfzone_ext, tol_merge.Rdist);
            
            % If all plane normals are good, two grains have the same orientation
            cons(ii) = (length(goodrefids) == (length(ref1.id)+length(ref2.id)));
            
        end
        
	end
	
	
% If actual grain Rodrigues vector is not close to edge of fundamental zone
else
    
    % Differences between the Rodrigues vectors
	diffRvec = act.R_vector(ones(size(cand.R_vector,1),1),:) - cand.R_vector;
	
	% Distances in Rodrigues space
	Rdist    = sqrt(sum(diffRvec.*diffRvec,2));
	
	% If distances are small enough, orientations are identical
	cons     = Rdist <= tol_merge.Rdist;

end


% Delete non-consistent grains from merge list
tomerge(~cons) = [];


end % of main function


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% SUB_FUNCTION
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function grsel = sfSelectGrains(keeplist,grin)

  grsel.id        = grin.id(keeplist);
  grsel.nof_pairs = grin.nof_pairs(keeplist);
  grsel.int       = grin.int(keeplist);
  % grsel.normint   = grin.normint(keeplist); % !!!
  grsel.center    = grin.center(keeplist,:);
  grsel.R_vector  = grin.R_vector(keeplist,:);
  grsel.R_onedge  = grin.R_onedge(keeplist);
  grsel.bbxs      = grin.bbxs(keeplist);
  grsel.bbys      = grin.bbys(keeplist);
  
end


