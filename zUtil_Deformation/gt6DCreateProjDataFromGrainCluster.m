function [refor, estim_space_bbox_pix, estim_orient_bbox_rod] = gt6DCreateProjDataFromGrainCluster(grs_list, phase_id, varargin)
% FUNCTION [proj, refor, or] = gt6DCreateProjDataFromGrainCluster(grs_list, phase_id, varargin)
%   proj: is a grain.proj structure
%   refor: is a grain structure for the average orientation in the average
%       point
%   or: is a set of grain structures for the extreeme corners of the
%       orientation space that should be considered

    if (~exist('phase_id', 'var'))
        phase_id = 1;
    end

    conf = struct( ...
        'verbose', false, ...
        'min_eta', 15, ...
        'det_index', 1, ...
        'ospace_oversize', 1.1, ...
        'rspace_oversize', 1.1, ...
        'stack_oversize', 1.1, ...
        'use_volume_mask', false, ...
        'save', true, ...
        'cluster_type', 0, ...
        'active', true);
    conf = parse_pv_pairs(conf, varargin);

    num_grains = numel(grs_list);
    if (~isstruct(grs_list))
        fprintf('Loading grains: ')
        for ii_g = num_grains:-1:1
            num_chars = fprintf('%02d/%02d (%d)', num_grains-ii_g+1, num_grains, grs_list(ii_g));
            grs(ii_g) = gtLoadGrain(phase_id, grs_list(ii_g));
            fprintf(repmat('\b', [1 num_chars]));
            fprintf(repmat(' ', [1 num_chars]));
            fprintf(repmat('\b', [1 num_chars]));
        end
        fprintf('Done.\n')
    else
        grs = grs_list;
        grs_list = cat(2, grs(:).id);
    end
    p = gtLoadParameters();
    symm = gtCrystGetSymmetryOperators(p.cryst(phase_id).crystal_system);

    if (conf.verbose)
        cat(1, grs(:).R_vector)
        fprintf('Reciprocal disorientations:\n')
        for ii_g_1 = 1:num_grains
            grs_to_check = (ii_g_1+1):num_grains;
            if (numel(grs_to_check) > 0)
                fprintf(' - Disorientations from grain: %d\n', grs(ii_g_1).id)
                for ii_g_2 = grs_to_check
                    dis_angle = gtDisorientation(grs(ii_g_1).R_vector', grs(ii_g_2).R_vector', symm);
                    fprintf('   + Grain %d: %f\n', grs(ii_g_2).id, dis_angle);
                end
            end
        end
    end

    refgr = grs(1);
    refgr_omind = refgr.allblobs(conf.det_index).omind;
    for ii_g = num_grains:-1:2
        grs(ii_g) = gtGrainAllblobsFilterOrder(grs(ii_g), refgr_omind);
    end

    ref_ondet = refgr.proj(conf.det_index).ondet;
    ref_included = refgr.proj(conf.det_index).included;
    ref_selected = refgr.proj(conf.det_index).selected;

    fwdsim_sel_refl = ref_ondet(ref_included(ref_selected));

    % Real-space volume size
    [gr_center_pix, bbox_size_pix, estim_space_bbox_pix] = gt6DMergeRealSpaceVolumes(grs, conf.det_index, conf.rspace_oversize);
    gr_center_mm = gtGeoSam2Sam(gr_center_pix, p.recgeo, p.samgeo, false, false);

    estim_space_bbox_mm = [ ...
        gtGeoSam2Sam(estim_space_bbox_pix(1:3), p.recgeo, p.samgeo, false, false), ...
        gtGeoSam2Sam(estim_space_bbox_pix(4:6), p.recgeo, p.samgeo, false, false) ];
    bbox_size_mm = estim_space_bbox_mm(4:6) - estim_space_bbox_mm(1:3);

    % Orientation-space volume size
    for ii_g = num_grains:-1:1
        sampler = GtOrientationSampling(p, grs(ii_g), ...
            'detector_index', conf.det_index, 'verbose', conf.verbose);
        odfw_R_vectors{ii_g} = sampler.guess_ODF_BB()';
    end
    r_vecs = cat(1, odfw_R_vectors{:});
    estim_orient_bbox_rod = [min(r_vecs, [], 1), max(r_vecs, [], 1)];
    bbox_size_rod = estim_orient_bbox_rod(4:6) - estim_orient_bbox_rod(1:3);

    % oversizing the orienation BB a bit
    delta_bbox_size_rod = bbox_size_rod * (conf.ospace_oversize - 1) / 2;
    estim_orient_bbox_rod = estim_orient_bbox_rod + [-delta_bbox_size_rod, delta_bbox_size_rod];
    bbox_size_rod = estim_orient_bbox_rod(4:6) - estim_orient_bbox_rod(1:3);

    gr_center_rod = (estim_orient_bbox_rod(4:6) + estim_orient_bbox_rod(1:3)) / 2;

    bbox_size_deg = 2 * atand(bbox_size_rod);

    if (conf.verbose)
        f = figure();
        ax = axes('parent', f);
        hold(ax, 'on');
        gt6DPlotOrientationBBox(ax, [estim_orient_bbox_rod(1:3); estim_orient_bbox_rod(4:6)]);
        for ii_g = 1:num_grains
            scatter3(ax, grs(ii_g).R_vector(1), grs(ii_g).R_vector(2), grs(ii_g).R_vector(3), 30);
            grain_r_vecs = odfw_R_vectors{ii_g};
            grain_orient_bbox_rod = [min(grain_r_vecs, [], 1), max(grain_r_vecs, [], 1)];
            gt6DPlotOrientationBBox(ax, [grain_orient_bbox_rod(1:3); grain_orient_bbox_rod(4:6)]);
        end
        hold(ax, 'off');

        drawnow();
    end

    fprintf('\n');
    fprintf('Estimated spatial voxel BBox: [%3d, %3d, %3d] -> [%3d, %3d, %3d]\n', estim_space_bbox_pix);
    fprintf('                   BBox size: %3d, %3d, %3d (%f, %f, %f mm)\n', bbox_size_pix, bbox_size_mm);

    fprintf('  Estimated orientation BBox: [%3.3f, %3.3f, %3.3f] -> [%3.3f, %3.3f, %3.3f]\n', estim_orient_bbox_rod);
    fprintf('                   BBox size: %3.3f, %3.3f, %3.3f (deg)\n', bbox_size_deg);
    fprintf('\n');

    % Let's now compute the bb on the images, by computing for each corner
    % of the space bb, the position on the detector of each corner of the
    % orientation bb.
    ors = gt6DCalculateFullSpaceCorners( ...
        estim_space_bbox_mm, bbox_size_mm, ...
        estim_orient_bbox_rod, bbox_size_rod, ...
        refgr, p, conf.det_index);

    or_abs = cat(1, ors(:).allblobs);
    num_ors = numel(ors);
    uvw_tab = zeros(numel(fwdsim_sel_refl), num_ors, 3);
    for ii_g = 1:num_ors
        uvw_tab(:, ii_g, :) = or_abs(ii_g).detector(conf.det_index).uvw(fwdsim_sel_refl, :);
    end

    refor = struct( ...
        'id', refgr.id, 'phaseid', refgr.phaseid, 'gids', grs_list, ...
        'center', gr_center_mm, 'R_vector', gr_center_rod );
    refor = gtCalculateGrain(refor, p, 'ref_omind', refgr_omind);
    refor.bb_ors = ors;

    % Let's treat those blobs at the w edge 360->0
    % (from the sampled orientations perspective)
    refor_ws = refor.allblobs(conf.det_index).omega(fwdsim_sel_refl) / gtAcqGetOmegaStep(p, conf.det_index);
    uvw_tab(:, :, 3) = gtGrainAnglesTabularFix360deg(uvw_tab(:, :, 3), refor_ws, p);

    img_bboxes = [
        squeeze(floor(min(uvw_tab, [], 2))), ...
        squeeze( ceil(max(uvw_tab, [], 2))) ];

    refor_ns = refor.allblobs(conf.det_index).eta(fwdsim_sel_refl);

    % We avoid the vertical spots for convenience
    inconvenient_etas = acosd(abs(cosd(refor_ns))) < conf.min_eta;

    img_sizes = img_bboxes(:, 4:6) - img_bboxes(:, 1:3) + 1;
    if (conf.verbose)
        fprintf('%2d) du %8d, dv %8d, dw %8d, eta: %7.3f <- used: %d, u: [%4d %4d], v: [%4d %4d], w: [%4d %4d]\n', ...
            [(1:numel(fwdsim_sel_refl))', img_sizes, refor_ns, ~inconvenient_etas, img_bboxes(:, [1 4 2 5 3 6]) ]');
        fprintf('\n');
    end

    img_bboxes = img_bboxes(~inconvenient_etas, :);
    img_sizes = img_sizes(~inconvenient_etas, :);

    max_img_sizes = [max(img_sizes(:, 1)), max(img_sizes(:, 2))];
    stackUSize = round(max_img_sizes(1) * conf.stack_oversize);
    stackVSize = round(max_img_sizes(2) * conf.stack_oversize);

    fprintf('               Maximum images size: [%3d, %3d]\n', max_img_sizes);
    fprintf('Stack images size (oversize: %1.2f): [%3d, %3d]\n', conf.stack_oversize, stackUSize, stackVSize);
    fprintf('\n');
    fprintf('Loading raw images: ')
    c = tic();
    num_blobs = numel(ref_included);

    blobs = gtFwdSimBlobDefinition('blob', num_blobs);

    sel_reflections = find(ref_selected);
    sel_reflections = sel_reflections(~inconvenient_etas);

    num_sel_refl = numel(sel_reflections);
    for ii_i = 1:num_sel_refl
        num_chars = fprintf('%02d/%02d', ii_i, num_sel_refl);

        ii_b = sel_reflections(ii_i);

        bb = [img_bboxes(ii_i, 1:2), img_sizes(ii_i, 1:2)];
        blob_vol = gtGetRawRoi(img_bboxes(ii_i, 3), img_bboxes(ii_i, 6), p.acq, bb, conf.det_index);
        blob_vol(blob_vol < 0) = 0;
        blob_bb = [img_bboxes(ii_i, 1:3), img_sizes(ii_i, :)];

        % Transposing to keep the same convention as spots
        blob_vol = permute(blob_vol, [2 1 3]);

        blobs(ii_b).mbbsize = blob_bb(4:6);
        blobs(ii_b).mbbu = [blob_bb(1), blob_bb(1) + blob_bb(4) - 1];
        blobs(ii_b).mbbv = [blob_bb(2), blob_bb(2) + blob_bb(5) - 1];
        blobs(ii_b).mbbw = [blob_bb(3), blob_bb(3) + blob_bb(6) - 1];

        blob_size_im = [stackUSize, stackVSize, blob_bb(6)];

        shifts_blob = gtFwdSimGetStackShifts(stackUSize, stackVSize, blob_bb, false);
        shifts_blob = [shifts_blob.u, shifts_blob.v, 0];

        blobs(ii_b).intm = gtPlaceSubVolume( ...
            zeros(blob_size_im, 'single'), single(blob_vol), shifts_blob);

        blobs(ii_b).bbsize = blob_size_im;

        blob_bb_im = [blob_bb(1:3) - shifts_blob, blob_size_im];

        blobs(ii_b).bbuim = [blob_bb_im(1), blob_bb_im(1) + blob_bb_im(4) - 1];
        blobs(ii_b).bbvim = [blob_bb_im(2), blob_bb_im(2) + blob_bb_im(5) - 1];
        blobs(ii_b).bbwim = [blob_bb_im(3), blob_bb_im(3) + blob_bb_im(6) - 1];

        % For the moment we set it to true
        blobs(ii_b).mask = gtPlaceSubVolume( ...
            false(blob_size_im), true(size(blob_vol)), shifts_blob);

        fprintf(repmat('\b', [1 num_chars]));
        fprintf(repmat(' ', [1 num_chars]));
        fprintf(repmat('\b', [1 num_chars]));
    end
    fprintf('Done in %g seconds.\n', toc(c))

    if (conf.use_volume_mask)
        fprintf('Producing blob masks..')
        c = tic();
        volume_verts = zeros(0, 3);
        for ii_g = 1:num_grains
            center_shift = grs(ii_g).center - gr_center_mm;
            volume_verts = cat(1, volume_verts, ...
                bsxfun(@plus, grs(ii_g).fwd_sim.gv_verts, center_shift));
        end
        k = convhull(volume_verts);
        volume_verts = volume_verts(k, :);
        center_verts = max(volume_verts, [], 1) - min(volume_verts, [], 1);
        volume_verts = bsxfun(@minus, volume_verts, center_verts);
        volume_verts = bsxfun(@times, volume_verts, conf.ospace_oversize);
        volume_verts = bsxfun(@plus, volume_verts, center_verts);

        proj_bl_masks = gt6DSpreadProjectVertices2Det(refor, volume_verts, fwdsim_sel_refl(~inconvenient_etas), p, conf.det_index);
        blobs = assign_masks(blobs, proj_bl_masks, sel_reflections, true);
        fprintf('\b\b: Done in %g seconds.\n', toc(c))
    end
    for ii_i = 1:num_sel_refl
        ii_b = sel_reflections(ii_i);
        blobs(ii_b).intensity = sum(blobs(ii_b).intm(blobs(ii_b).mask));
        blobs(ii_b).intm(~blobs(ii_b).mask) = 0;
        blobs(ii_b).intm = blobs(ii_b).intm / blobs(ii_b).intensity;
    end

    % Now filling the remaining blobs with the blobs from refgr
    % (even if they won't be used)
    % ! this might change in the future ! to maybe include fwd projected
    % regions on the detector
    not_sel_reflections = true(size(ref_included));
    not_sel_reflections(sel_reflections) = false;
    not_sel_reflections = find(not_sel_reflections);
    blobs(not_sel_reflections) = refgr.proj.bl(not_sel_reflections);
    for ii_i = 1:numel(not_sel_reflections)
        ii_b = not_sel_reflections(ii_i);
        blobs(ii_b) = equalize_blob_size(blobs(ii_b), stackUSize, stackVSize);
    end

    spots = arrayfun(@(x){sum(x.intm, 3)}, blobs);
    spots = permute(cat(3, spots{:}), [1 3 2]);

    proj = gtFwdSimProjDefinition();

    proj.centerpix = gr_center_pix;
    proj.bl = blobs;
    proj.stack = spots;
    proj.vol_size_x = bbox_size_pix(2);
    proj.vol_size_y = bbox_size_pix(1);
    proj.vol_size_z = bbox_size_pix(3);

    proj.ondet = ref_ondet;
    proj.included = ref_included;

    finally_selected = false(size(proj.included));
    finally_selected(sel_reflections) = true;
    proj.selected = finally_selected;

    refor.proj(conf.det_index) = proj;
    refor.conf = conf;

    if (conf.save)
        fprintf('Saving the cluster file..')
        str_ids = sprintf('_%04d', grs_list);
        grain_filename = fullfile(p.acq.dir, '4_grains', ...
            sprintf('phase_%02d', phase_id), ...
            sprintf('grain_cluster%s.mat', str_ids));
        save(grain_filename, '-struct', 'refor', '-v7.3');
        fprintf('\b\b: Done.\n')

        fprintf('Saving to sample.mat..')
        sample = GtSample.loadFromFile();
        cl_info = GtPhase.makeCluster(grs_list, refor.R_vector, ...
            estim_space_bbox_pix, estim_orient_bbox_rod, conf.cluster_type, conf.active);
        sample.phases{phase_id}.setClusterInfo(grs_list, cl_info);
        sample.saveToFile();
        fprintf('\b\b: Done.\n')
    end
end

function blob = equalize_blob_size(blob, stackUSize, stackVSize)
    new_bbsize = [stackUSize, stackVSize, blob.bbsize(3)];
    shift = floor((new_bbsize - blob.bbsize) / 2);

    new_bbuim = blob.bbuim - shift(1);
    new_bbuim(2) = new_bbuim(1) + new_bbsize(1) - 1;

    new_bbvim = blob.bbvim - shift(2);
    new_bbvim(2) = new_bbvim(1) + new_bbsize(2) - 1;

    blob.bbuim = new_bbuim;
    blob.bbvim = new_bbvim;
    blob.bbsize = new_bbsize;

    blob.intm = gtPlaceSubVolume(zeros(new_bbsize, 'like', blob.intm), blob.intm, shift);
    blob.mask = logical(gtPlaceSubVolume(zeros(new_bbsize, 'uint8'), blob.mask, shift));
end

function blobs = assign_masks(blobs, proj_mask_blobs, global_pos, reset_masks)
    if (~exist('reset_masks', 'var') || isempty(reset_masks))
        reset_masks = false;
    end
    blobs_u_lims = cat(1, blobs(global_pos).bbuim);
    blobs_v_lims = cat(1, blobs(global_pos).bbvim);

    proj_masks_u_lims = cat(1, proj_mask_blobs(:).bbuim);
    proj_masks_v_lims = cat(1, proj_mask_blobs(:).bbvim);

    shifts = [ ...
        (proj_masks_u_lims(:, 1) - blobs_u_lims(:, 1)), ...
        (proj_masks_v_lims(:, 1) - blobs_v_lims(:, 1)) ];
    shifts(:, 3) = 0;

    for ii = 1:numel(global_pos)
        ii_b = global_pos(ii);
        if (reset_masks)
            blobs(ii_b).mask = gtMathsGetSameSizeZeros(blobs(ii_b).mask);
        end

        mask = uint8(proj_mask_blobs(ii).mask);
        mask = mask(:, :, ones(blobs(ii_b).bbsize(3), 1));

        blobs(ii_b).mask = gtPlaceSubVolume(blobs(ii_b).mask, mask, shifts(ii, :));
    end
end

