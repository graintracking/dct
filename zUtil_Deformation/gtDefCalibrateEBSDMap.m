function [rot_angle, rot_axis, rot_tensor, rot_vec, ebsd_map_calib] = gtDefCalibrateEBSDMap(ebsd, points, gr, symm)

    if (~exist('symm', 'var'))
        symm = gtCrystGetSymmetryOperators();
    end

    num_points = numel(gr);

    % Sometimes the axes definition is not the same
%     ref_r_vec = [-ref_r_vec(2), ref_r_vec(3), -ref_r_vec(1)];

    for ii = num_points:-1:1
        ref_r_vec(:, ii) = extract_from_map(ebsd.map_r, points{ii});
        gr_r_vec(:, ii) = gr(ii).R_vector;
    end

    ref_r_vec = gtMathsRod2RodInFundZone(ref_r_vec, symm);
    gr_r_vec = gtMathsRod2RodInFundZone(gr_r_vec, symm);

    for ii = num_points:-1:1
        rot_vec(ii, :) = get_angle_axis(ref_r_vec(:, ii), gr_r_vec(:, ii), symm);
    end

    rot_vec = sum(rot_vec, 1) / num_points;
    rot_angle = 2 * atand(norm(rot_vec));
    rot_axis = rot_vec / norm(rot_vec);

    rotcomp = gtMathsRotationMatrixComp(rot_axis', 'col');
    rot_tensor = gtMathsRotationTensor(rot_angle, rotcomp);

    if (nargout > 4)
        dmvol_EBSD_r_map = permute(ebsd.map_r, [1 2 4 3]);
        [gvdm_EBSD_r_map, size_dmvol_EBSD_map] = gtDefDmvol2Gvdm(dmvol_EBSD_r_map);
        mask = ebsd.mask(:) & all(gvdm_EBSD_r_map ~= 0, 1)';
        gvdm_EBSD_r_map(:, mask) = gtMathsRodSum(rot_vec', gvdm_EBSD_r_map(:, mask));
        gvdm_EBSD_r_map(:, mask) = gtMathsRod2RodInFundZone(gvdm_EBSD_r_map(:, mask), symm);
        dmvol_EBSD_r_map = gtDefGvdm2Dmvol(gvdm_EBSD_r_map, size_dmvol_EBSD_map);
        ebsd_map_calib = permute(dmvol_EBSD_r_map, [1 2 4 3]);
    end
end

function r_vec_3 = get_angle_axis(r_vec_1, r_vec_2, symm)

    % gtDisorientation should be used, but it seems to be buggy, at least
    % for small rotations
%     [angle, axis] = gtDisorientation(r_vec_1', r_vec_2', symm);

    g1 = gtMathsRod2OriMat(r_vec_1);
    g2 = gtMathsRod2OriMat(r_vec_2);

    M = g1' * g2;

    r_vec_3 = gtMathsOriMat2Rod(M);
    r_vec_3 = gtMathsRod2RodInFundZone(r_vec_3, symm);
end

function r_vec = extract_from_map(EBSD_r_map, points)
    kernel = 7;
    k_dist = (kernel - 1) / 2;

    num_points = size(points, 1);
    r_vecs = zeros(num_points, 3);

    for ii = 1:num_points
        point = points(ii, [2 1]);

        ranges = [point(2)-k_dist, point(2)+k_dist; point(1)-k_dist, point(1)+k_dist];

        temp_r_vecs = EBSD_r_map(ranges(1, 1):ranges(1, 2), ranges(2, 1):ranges(2, 2), :);
        temp_r_vecs = sum(sum(temp_r_vecs, 1), 2) / (size(temp_r_vecs, 1) * size(temp_r_vecs, 2));
        r_vecs(ii, :) = reshape(temp_r_vecs, 1, []);
    end

    r_vec = sum(r_vecs, 1) / num_points;
end
