function [igm, gos] = gtDefComputeIntraGranularMisorientation(dmvol, intvol, varargin)
    conf = struct('R_vector', []);
    conf = parse_pv_pairs(conf, varargin);

    gvdm = gtDefDmvol2Gvdm(dmvol);
    [vol_size(1), vol_size(2), vol_size(3), vol_size(4)] = size(dmvol);
    num_def_comps = size(gvdm, 1);
    tot_voxels = size(gvdm, 2);
    gvint = reshape(intvol, 1, []);
    gvint = gvint(ones(num_def_comps, 1), :);

    % Let's first compute the average orientation
    if (isempty(conf.R_vector))
        avg_R_vec = sum(gvdm .* gvint, 2) ./ sum(gvint, 2);
    else
        avg_R_vec = reshape(conf.R_vector, 3, 1);
    end

    igm = gtMathsRodSum(gvdm, -avg_R_vec(:, ones(tot_voxels, 1)));
    igm = sqrt(sum(igm .^ 2, 1));
    igm(intvol == 0) = 0;
    % so we get all the orientation differences (Intra-Granular Misorientation)
    igm = 2 * atand(igm);
    % Grain Orientation Spread
    gos = mean(igm);
    % We now reshape to the original volume size
    igm = reshape(igm, vol_size(1:3));
end
