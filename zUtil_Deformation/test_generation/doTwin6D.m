
try
    if (exist('testCreator', 'var'))
        delete(testCreator)
        clear Gt6DTestCreator
    end
catch mexc
    gtPrintException(mexc)
end

try
    if (exist('reconstructor', 'var'))
        delete(reconstructor)
        clear Gt6DSpotReconstructor
        clear GtCSHessianApproxFIFO
    end
catch mexc
    gtPrintException(mexc)
end

synthetic = true;
initial_zeros = true;

% cd('/data/id19/graintracking/V246/')
cd('/data/id11/graintracking/DCT_Analysis/testTwin/')
% grainsList = [61, 131];
% grainsList = [46, 104];
grainsList = [7, 104];
numGrains = length(grainsList);
grains = cell(numGrains, 1);
% grains{1} = load('grain046.mat');
% grains{1} = grains{1}.grain046;
% grains{2} = load('grain104.mat');
% grains{2} = grains{2}.grain104;
for ii = 1:numGrains
    grains{ii} = load(fullfile('4_grains', 'phase_01', sprintf('grain_%04d.mat', grainsList(ii))));
end

sample = GtSample.loadFromFile(fullfile('4_grains', 'sample.mat'));
metaInfoGrains = sample.phases{1}.grains(grainsList);

parameters = [];
load('parameters')

if (synthetic)
    maxIndivSpots = 12;

    testCreator = Gt6DTwinTestCreator(2);
    grain = testCreator.generateTwinsGeometry(parameters, sample.phases{1}.getR_vector(grainsList(1)), grains{1}.center);
    geoms = testCreator.matchParentTwinGeometries(grain);

    % Projects the volumes and creates the theoretical detector images.
    [detectorImages, trueDetectorImages] = testCreator.getSyntheticTwinDetectorImages(grain, geoms, maxIndivSpots);
else
    % Tries to guess the geometry for the given images
    [detectorImages, trueDetectorImages] = testCreator.getTrueTwinDetectorImages(metaInfoGrains, grains);
end

% detectorImages = GtCSAlgebraicOps.convertDiffStackToSoftThresh(detectorImages);

% Initialize reconstruction object
if (initial_zeros)
    zeroVols = testCreator.getInitialVols();
else
    twinShift = grains{2}.shift - grains{1}.shift;
    volTwin = zeros(size(grains{1}.vol));
    volTwin = gtPlaceSubVolume(volTwin, grains{2}.vol, twinShift, 0, 'assign');
    zeroVols = {grains{1}.vol, volTwin};
end
reconstructor = Gt6DSpotReconstructor(testCreator.proj.geometries, ...
                                    zeroVols, detectorImages);

% Perform reconstruction
try
%     numIters = 100;
%     reconstructor.performCG(numIters, 1e-4);

    numIters = 100;
    reconstructor.performSIRT(numIters, 1e-5);
end

finalVols = reconstructor.getMergeTwins(0.25);
