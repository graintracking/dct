function result = gt6DAssembleResult(test_data, sampled_orientations, solution, det_index)

    dmvol = test_data.dmvol;

    mag = sqrt(sum(dmvol .^ 2, 4));

    has_voxsize = isfield(test_data.fedpars, 'voxsize') ...
        && ~isempty(test_data.fedpars.voxsize);
    to_resize = (has_voxsize ...
        && any(test_data.fedpars.voxsize ~= test_data.parameters.recgeo(det_index(1)).voxsize) ...
        ) || (det_index(1) > 1 ...
        && any(test_data.parameters.recgeo(det_index(1)).voxsize > test_data.parameters.recgeo(1).voxsize));

    if (to_resize)
        if (has_voxsize)
            theo_voxsize = test_data.fedpars.voxsize;
        else
            theo_voxsize = test_data.parameters.recgeo(1).voxsize;
        end
        rec_voxsize = test_data.parameters.recgeo(det_index(1)).voxsize;
        fprintf('Resampling the theoretical volume from voxsize [%g %g %g] um to [%g %g %g] um..', ...
            theo_voxsize * 1e3, rec_voxsize * 1e3)
        c = tic();

        scaling = test_data.parameters.recgeo(det_index(1)).voxsize ./ theo_voxsize;
        sampling = round(2 * scaling);
        out_vol_size = max(ceil(size(mag) ./ scaling), size(solution{1}));

        half_mag_size = (size(mag) - 1) / 2;
        base_xx = linspace(-half_mag_size(1), half_mag_size(1), size(mag, 1)) * theo_voxsize(1);
        base_yy = linspace(-half_mag_size(2), half_mag_size(2), size(mag, 2)) * theo_voxsize(2);
        base_zz = linspace(-half_mag_size(3), half_mag_size(3), size(mag, 3)) * theo_voxsize(3);

        half_new_mag_size = (out_vol_size - 1) / 2 + 0.5 - 1 ./ (sampling .* 2);
        xx = linspace(-half_new_mag_size(1), half_new_mag_size(1), out_vol_size(1) * sampling(1)) * rec_voxsize(1);
        yy = linspace(-half_new_mag_size(2), half_new_mag_size(2), out_vol_size(2) * sampling(2)) * rec_voxsize(2);
        zz = linspace(-half_new_mag_size(3), half_new_mag_size(3), out_vol_size(3) * sampling(3)) * rec_voxsize(3);
        [xx, yy, zz] = ndgrid(xx, yy, zz);

        mag = interp3(base_yy, base_xx, base_zz, mag, yy, xx, zz, 'linear', 0);

        new_vol_size = [sampling; out_vol_size];
        new_vol_size = (new_vol_size(:))';
        mag = reshape(mag, new_vol_size);
        mag = sum(sum(sum(mag, 1), 3), 5) / prod(sampling) * prod(scaling);
        mag = squeeze(mag);

        exp_dmvol = zeros([out_vol_size, 3], 'single');
        for ii_d = 1:3
            dmvol_comp = interp3(base_yy, base_xx, base_zz, dmvol(:, :, :, ii_d), yy, xx, zz, 'linear', 0);

            dmvol_comp = reshape(dmvol_comp, new_vol_size);
            dmvol_comp = sum(sum(sum(dmvol_comp, 1), 3), 5) / prod(sampling) * prod(scaling);
            dmvol_comp = squeeze(dmvol_comp);
            exp_dmvol(:, :, :, ii_d) = dmvol_comp;
        end

        dmvol_interp_mag = sqrt(sum(exp_dmvol .^ 2, 4));
        exp_dmvol = bsxfun(@times, exp_dmvol, mag ./ (dmvol_interp_mag + (dmvol_interp_mag < eps('single'))));

        exp_intvol = interp3(base_yy, base_xx, base_zz, single(test_data.intvol), yy, xx, zz, 'linear', 0);

        exp_intvol = reshape(exp_intvol, new_vol_size);
        exp_intvol = sum(sum(sum(exp_intvol, 1), 3), 5) / prod(sampling) * prod(scaling);
        exp_intvol = squeeze(exp_intvol);

        exp_dmvol = bsxfun(@times, exp_dmvol, 1 ./ (exp_intvol + (exp_intvol < eps('single'))));
        fprintf('\b\b: Done in %g seconds.\n', toc(c))
    else
        exp_dmvol = dmvol;
        exp_intvol = single(test_data.intvol);
    end

    fprintf('Segmenting the theoretical volume:\n - ')
    t = GtThreshold(test_data.parameters);
    t.param.rec.thresholding.do_region_prop = false;
    t.param.rec.thresholding.use_levelsets = true;
    t.param.rec.thresholding.mask_border_voxels = 1;
    gr_rec = struct('vol', exp_intvol, 'shift', [0 0 0]);
    SEG = t.thresholdAutoSingleGrain(gr_rec);

    vol_size = size(exp_dmvol(:, :, :, 1));

    deviations_dmvol = { ...
        cat(1, zeros(1, vol_size(2), vol_size(3)), abs(diff(mag, 1, 1)), zeros(1, vol_size(2), vol_size(3))), ...
        cat(2, zeros(vol_size(1), 1, vol_size(3)), abs(diff(mag, 1, 2)), zeros(vol_size(1), 1, vol_size(3))), ...
        cat(3, zeros(vol_size(1), vol_size(2), 1), abs(diff(mag, 1, 3)), zeros(vol_size(1), vol_size(2), 1)), ...
        };

    result = struct( ...
        'expected_dmvol', {exp_dmvol}, ...
        'expected_intvol', {exp_intvol}, ...
        'expected_seg', {SEG.seg}, ...
        'deviations_dmvol', {deviations_dmvol}, ...
        'grains', {sampled_orientations}, 'solution', {solution}, ...
        'det_index', {det_index});
end
