classdef Gt6DGeometryBuilder < handle
    properties
        rotMatrices = [];
    end

    methods (Access = public)
        function self = Gt6DGeometryBuilder(rotAxis, numProjs)
            if (exist('rotAxis', 'var') && exist('numProjs', 'var'))
                self.createUniformRotationMatrices(rotAxis, numProjs);
            end
        end

        function createUniformRotationMatrices(self, rotAxis, numProjs)
            angles = (0:numProjs-1)/numProjs * 360;
            rotAxis = rotAxis / norm(rotAxis);

            self.rotMatrices = Gt6DGeometryBuilder.getRotMatrices(rotAxis, angles);
        end

        function createBlurringRotationMatrices(self, rotAxis, angles)
            self.rotMatrices = Gt6DGeometryBuilder.getRotMatrices(rotAxis, angles);
        end

        function dirs = getRotatedDirections(self, detectDir)
            numRots = size(self.rotMatrices, 3);
            dirs = zeros(numRots, 3);
            for ii = 1:numRots
                dirs(ii, :) = (self.rotMatrices(:, :, ii) * detectDir')';
            end
        end

        function detectors_pos_uv = getRotatedDetectorGeometries(self, forward_dir, detect_u, detect_v)
            % directions should be normalized
            numRots = size(self.rotMatrices, 3);
            detectors_pos_uv = zeros(numRots, 9);
            for ii = 1:numRots
                detectors_pos_uv(ii, :) = [ (self.rotMatrices(:,:,ii) * forward_dir')', ...
                                            (self.rotMatrices(:,:,ii) * detect_u')', ...
                                            (self.rotMatrices(:,:,ii) * detect_v')'];
            end
        end

        function arrayOfRotatedDirs = rotateListOfDirections(self, directions)
            numRots = size(self.rotMatrices, 3);
            arrayOfRotatedDirs = cell(numRots, 1);

            for kk = 1:numRots
                arrayOfRotatedDirs{kk} = (self.rotMatrices(:, :, kk) * directions')';
            end
        end
    end

    methods (Static, Access = public)
        function rotMatrices = getRotMatrices(rotAxis, angles)
            sines = sind(angles);
            cosines = cosd(angles);
            sub_1_cos = (1 - cosines);

            sines = permute(sines, [1 3 2]);
            cosines = permute(cosines, [1 3 2]);
            sub_1_cos = permute(sub_1_cos, [1 3 2]);

            sines = sines(ones(3,1),ones(3,1),:);
            cosines = cosines(ones(3,1),ones(3,1),:);
            sub_1_cos = sub_1_cos(ones(3,1),ones(3,1),:);

            % g is from page 33 of Texture Analysis, from Randle
            outerMatrix = ((rotAxis') * rotAxis);
            outerMatrix = outerMatrix(:, :, ones(length(angles), 1));
            I = eye(3);
            I = I(:, :, ones(length(angles), 1));
            restMatrix = [0 (-rotAxis(3)) (-rotAxis(2)); 0 0 (-rotAxis(1)); 0 0 0];
            restMatrix = restMatrix - restMatrix';
            restMatrix = restMatrix(:, :, ones(length(angles), 1));

            rotMatrices = sub_1_cos .* outerMatrix + I .* cosines + restMatrix .* sines;
        end

        function detectors_pos_uv = getRotatedDetectorGeometries_dirs(detect_dir, detect_u, detect_v, directions)
            % angles
            angles = acosd(directions * detect_dir');

            rotDirs = bsxfun(@(x, y) cross(x, y), detect_dir', directions');
            norms = sqrt(sum(rotDirs.*rotDirs, 1)) + (mod(angles', 180) == 0);
            rotDirs = (rotDirs ./ norms(ones(3,1),:))';

            % if angles are 180 or 360 degs, this is going to be a problem. So
            % let's fix it!
            dirsToUpdate = find(sum(abs(rotDirs),2) == 0);
            if ~isempty(dirsToUpdate)
                squareDirs = directions(dirsToUpdate) .* directions(dirsToUpdate);

                summedSquareDirs = sum(squareDirs(:,2:end), 2);
                summedSquareRotDirs = sqrt(summedSquareDirs) + (summedSquareDirs == 0);

                newRotVecs = [zeros(length(dirsToUpdate),1) -directions(dirsToUpdate,3) directions(dirsToUpdate,2)];
                newRotVecs = newRotVecs ./ summedSquareRotDirs(:, [1 1 1]);
                % Vectors aligned along [1 0 0] which need an arbitrary
                % direction, because we cannot compute the thing automatically
                arbitraryVecs = find(sum(abs(newRotVecs),2) == 0);
                upwards = [0 0 1];
                newRotVecs(arbitraryVecs, :) = upwards(length(arbitraryVecs), :);

                rotDirs(dirsToUpdate, :) = newRotVecs;
            end

            matrixes = zeros(3,3,length(angles));
            for ii = 1:length(angles)
                matrixes(:,:,ii) = Gt6DGeometryBuilder.getRotMatrices(rotDirs(ii,:), angles(ii));
            end

            detectors_pos_uv = [directions, self.createProjsOfDetect_rotMatrs(detect_u, detect_v, matrixes)];
        end
    end
end
