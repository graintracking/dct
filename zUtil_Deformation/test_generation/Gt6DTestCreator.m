classdef Gt6DTestCreator < Gt6DReconstructionAlgorithmFactory
    properties
        geom;
        proj;

        fullVol;

        singleVolumes = {};

        ref_detectors_pos_uv;
    end

    methods (Access = public)
        function self = Gt6DTestCreator(volsNum)
            % Prepares the data structures
            xx = 1:104;
            yy = xx';
            zz = permute(yy, [2 3 1]);
            self.fullVol = ( (xx(ones(104,1),:,ones(104,1)) - 51.5).^2 ...
                            + (yy(:,ones(104,1),ones(104,1)) - 51.5).^2 ...
                            + (zz(ones(104,1),ones(104,1),:) - 51.5).^2) < 40^2;

            self.fullVol = cast(self.fullVol, self.data_type);

            switch (volsNum)
                case 1
                case 2
                    self.fullVol(50:75, :, :) = self.fullVol(50:75, :, :) * 2;
                case 3
                    self.fullVol(40:60, :, :) = self.fullVol(40:60, :, :) * 2;
                    self.fullVol(61:end, :, :) = self.fullVol(61:end, :, :) * 3;
                case 4
                    self.fullVol(40:60, :, :) = self.fullVol(40:60, :, :) * 2;
                    self.fullVol(:, 40:60, :) = (self.fullVol(:, 40:60, :) ~= 0) ...
                                                .* (self.fullVol(:, 40:60, :) + 2);
                case 5
                    self.fullVol(40:60, :, :) = self.fullVol(40:60, :, :) * 2;
                    self.fullVol(61:end, :, :) = self.fullVol(61:end, :, :) * 3;
                    self.fullVol(:, 40:60, :) = (self.fullVol(:, 40:60, :) ~= 0) ...
                                                .* (self.fullVol(:, 40:60, :) + 3);
                    self.fullVol(self.fullVol == 5) = 0;
                    self.fullVol(self.fullVol == 6) = 5;
                case 6
                    self.fullVol(40:60, :, :) = self.fullVol(40:60, :, :) * 2;
                    self.fullVol(61:end, :, :) = self.fullVol(61:end, :, :) * 3;
                    self.fullVol(:, 40:60, :) = (self.fullVol(:, 40:60, :) ~= 0) ...
                                                .* (self.fullVol(:, 40:60, :) + 3);
                case 9
                    self.fullVol(40:60, :, :) = self.fullVol(40:60, :, :) * 2;
                    self.fullVol(61:end, :, :) = self.fullVol(61:end, :, :) * 3;
                    self.fullVol(:, 40:60, :) = (self.fullVol(:, 40:60, :) ~= 0) ...
                                                .* (self.fullVol(:, 40:60, :) + 3);
                    self.fullVol(:, 61:end, :) = (self.fullVol(:, 61:end, :) ~= 0) ...
                                                .* (self.fullVol(:, 61:end, :) + 6);
                otherwise
                    warning('DATA:wrong_param', 'Number of volumes not supported')
                    self.fullVol(:) = 0;
            end

            self.singleVolumes = cell(volsNum, 1);
            for ii = 1:volsNum
                self.singleVolumes{ii} = cast(self.fullVol == ii, self.data_type);
            end

            self.geom = Gt6DGeometryBuilder();
            self.proj = Gt6DVolumeProjector(self.singleVolumes);
        end

        function createReferenceGeometry(self, numRots, detectPos, detectUV, rotAxis, otherDirs)
            self.geom.createUniformRotationMatrices(rotAxis, numRots);
            
            detectorDistance = norm(detectPos);
            % Detector position and orientation
            self.ref_detectors_pos_uv = self.geom.getRotatedDetectorGeometries( ...
                detectPos / detectorDistance, ... Direction of detector
                detectUV{2}, detectUV{1} ... Axes of the detector
                );

            directions = self.ref_detectors_pos_uv(:, 1:3);
            self.ref_detectors_pos_uv(:, 1:3) = self.ref_detectors_pos_uv(:, 1:3) * detectorDistance;

            geometry = cell(1+numel(otherDirs), 1);
            % Reference geometry
            geometry{1} = [directions, self.ref_detectors_pos_uv];

            % Other geometries
            for n = 1:numel(otherDirs)
                rotAxis = otherDirs{n}{1};
                initialDir = otherDirs{n}{2};

                numOfRotations = size(self.geom.rotMatrices, 3);
                self.geom.createUniformRotationMatrices(rotAxis, numOfRotations);
                directions = self.geom.getRotatedDirections(initialDir);

                geometry{1+n} = [directions, self.ref_detectors_pos_uv];
            end

            self.proj.geometries = geometry;
        end

        function [detectorImages, trueDetectorImages] = getDetectorImages(self)
            stackOfProjs = self.proj.projectVolume(self.singleVolumes);
            detectorImages = gtMathsSumCellVolumes(stackOfProjs);

            trueDetectorImages = permute(detectorImages, [1 3 2]);
        end

        function initialVols = getInitialVols(self)
            initialVols = cell(size(self.proj.volume_geometries));
            for n = 1:numel(self.proj.volume_geometries)
                initialVols{n} = zeros(self.proj.volume_geometries{n}, self.data_type);
            end
        end

        function createBlurredGeometry(self, refGeom, numOfRotations, rotationAxis, angleStep)
            if ((numOfRotations < 3) || (mod(numOfRotations, 2) == 0))
                error('CS_GEOM:wrong_parameter', ...
                      'Invalid number of blurring steps, they should be odd numbers >= 3');
            end

            % Let's copy the initial volume in all the other volumes
            self.singleVolumes = self.singleVolumes(ones(numOfRotations, 1));
            self.proj = Gt6DVolumeProjector(self.singleVolumes);

            % Create the geometry
            finalAngleStep = double(angleStep) / (numOfRotations -1);
            rotsIn1dir = (numOfRotations-1)/2;
            angles = finalAngleStep * [  linspace(rotsIn1dir, 1, rotsIn1dir) ...
                                        -linspace(1, rotsIn1dir, rotsIn1dir) ];
            self.geom.createBlurringRotationMatrices(rotationAxis, angles);

            % Adding Geometries
            self.proj.addProjectionGeometry( refGeom );
            newDetectorDirs = self.geom.rotateListOfDirections(refGeom(:, 4:6));
            for kk = 1:length(angles)
                geometry = [refGeom(:, 1:3), newDetectorDirs{kk} refGeom(:, 7:end)];
                self.proj.geometries{end} = geometry;
            end
        end

        function indexes = createTripleJunction(self, metaGrains, grains)
            indexes = [];
            for ii = 1:2
                for jj = 2:3
                    if (jj > ii)
                        [indexes.(sprintf('common_id_%02d_%02d', ii, jj)), ...
                         indexes.(sprintf('common_indx%02d_%02d_%02d', ii, ii, jj)), ...
                         indexes.(sprintf('common_indx%02d_%02d_%02d', jj, ii, jj))] ...
                            = intersect(grains{ii}.difspotID(metaGrains{ii}.selectedDiffspots), ...
                                        grains{jj}.difspotID(metaGrains{jj}.selectedDiffspots));

                        [indexes.(sprintf('only_id%02d_%02d_%02d', ii, ii, jj)), ...
                         indexes.(sprintf('only_indx%02d_%02d_%02d', ii, ii, jj))] ...
                            = setdiff(grains{ii}.difspotID, grains{ii}.difspotID);
                        [indexes.(sprintf('only_id%02d_%02d_%02d', jj, ii, jj)), ...
                         indexes.(sprintf('only_indx%02d_%02d_%02d', jj, ii, jj))] ...
                            = setdiff(grains{jj}.difspotID, grains{jj}.difspotID);
                    end
                end
            end
            indexes.('common_all') = intersect(indexes.common_id_01_02, ...
                                                indexes.common_id_01_03);

            % Now that we are convinced there is not overlap in the diffspots
            self.createMultipleJunction(metaGrains, grains);
        end

        function [detectorImages, trueDetectorImages] ...
                            = createMultipleJunction(self, metaGrains, grains)
            numGrains = length(grains);

            self.assembleMultipleGeometries(metaGrains, grains);

            for ii = 1:numGrains
                sizes(ii, :) = size(grains{ii}.proj.stack);
            end
            maxBBox = [max([sizes(:, 1); round(self.proj.proj_size(1) * 0.9)]), ...
                       max([sizes(:, 3); round(self.proj.proj_size(2) * 0.9)])];
            offsets(:, 1) = floor((maxBBox(1) - sizes(:, 1)) / 2);
            offsets(:, 2) = zeros(numGrains, 1);
            offsets(:, 3) = floor((maxBBox(2) - sizes(:, 3)) / 2);

            detectorImages = cell(numGrains, 1);
            for ii = 1:numGrains
                only_ii = grains{ii}.proj.stack(:, find(metaGrains{ii}.selectedDiffspots == 1), :);

                detectorImages{ii} = zeros(maxBBox(1), size(only_ii, 2), maxBBox(2), self.data_type);
                detectorImages{ii} = gtPlaceSubVolume(detectorImages{ii}, ...
                                                  only_ii, ...
                                                  offsets(ii, :), ...
                                                  0, 'assign');
            end

            trueDetectorImages = cellfun(@(x){permute(x, [1 3 2])}, detectorImages);
        end

        function assembleMultipleGeometries(self, metaGrains, grains)
        % ASSEMBLEMULTIPLEGEOMETRIES Puts together the boundingboxes and
        % assigns the spots (fixing the center)
        %   Inputs:
        %    -  metaGrains : metadata info, like from file sample.mat
        %    -  grains : grain info, from file grain_%04d.mat

            numGrains = length(grains);
            finalGeoms = cell(numGrains, 1);
            for ii = 1:numGrains
                finalGeoms{ii} = grains{ii}.proj.geom(find(metaGrains{ii}.selectedDiffspots == 1), :);
            end

            self.fixCenterToGeometriesAndAdd(finalGeoms, grains, 0.8);
        end

        function fixCenterToGeometriesAndAdd(self, finalGeoms, grains, volumeOversize)
        % FIXCENTERTOGEOMETRIESANDADD Fix centers and change volumes and
        % final geometry
        %   Inputs:
        %    -  finalGeoms : the geometry to eventually shift
        %    -  grains : the content of files grain_%04d.mat
        %    -  volumeOversize : multiplier to max volume size

            numGrains = length(grains);
            % Half sizes
            halfSizes = cellfun(@(x){ceil(size(x.vol)/2)}, grains);
            fullVolCenters = cellfun(@(x){x.center}, grains);
            % Find Volumes bounding box!
            minVols = min(cat(1, fullVolCenters{:}) - cat(1, halfSizes{:}), [], 1);
            maxVols = max(cat(1, fullVolCenters{:}) + cat(1, halfSizes{:}), [], 1);
            % Center of subvolume will be the shift to the positions in the
            % full volume
            centerOfSubVolume = (minVols + maxVols) / 2;
            subVolumeSize = maxVols - minVols;

            % We can now usually safely resize the volume, in respect to
            % Wolfgang's decisions about volume bounding box sizes.
            subVolumeSize = floor(subVolumeSize * volumeOversize)+1;

            self.singleVolumes = cellfun(@(x){zeros(subVolumeSize, self.data_type)}, grains);
            self.proj = Gt6DVolumeProjector(self.singleVolumes);

            % Finally add the modified geometries
            for ii = 1:numGrains
                repLines = ones(1, size(finalGeoms{ii}, 1) );
                finalGeoms{ii}(:, 4:6) = finalGeoms{ii}(:, 4:6) ...
                                            + grains{ii}.center(repLines, :) ...
                                            - centerOfSubVolume(repLines, :);
                self.proj.geometries{end} = finalGeoms{ii};
            end
        end

        function [detectorImages, trueDetectorImages] ...
                            = createOrientationSpreadGeometry(self, gr, blobs, indexes, volType)
        %
            blobSize = size(blobs(1).intm);
            trueDetectorImages = zeros([blobSize(1:2) length(indexes)], self.data_type);
            for ii = 1:length(indexes)
                trueDetectorImages(:, :, ii) = cast(sum(blobs(indexes(ii)).intm, 3), self.data_type);
            end
            detectorImages = permute(trueDetectorImages, [1 3 2]);

            volumeSize = Gt6DReconstructionAlgorithmFactory.getVolSize(blobSize, volType);

            self.singleVolumes = cellfun(@(x){zeros(volumeSize, self.data_type)}, gr);

            delete(self.proj);
            self.proj = Gt6DVolumeProjector(self.singleVolumes);

            for ii = 1:length(gr)
                geometry = gr{ii}.allblobs.proj_geom(indexes, :);
                geometry = double(geometry);

                self.proj.geometries{end} = geometry;
            end
        end
    end

    methods (Access = protected)
        function newDirection = findPerpendicularDirection(~, initialDir)
            squareDirs = initialDir .* initialDir;

            norms = sqrt(sum(squareDirs, 2));

            summedSquareDirs = sum(squareDirs(:,2:end), 2);
            summedSquareRotDirs = sqrt(summedSquareDirs) + (summedSquareDirs == 0);

            newDirection = [zeros(size(initialDir, 1),1) -initialDir(:,3) initialDir(:,2)];
            newDirection = newDirection ./ summedSquareRotDirs(:, [1 1 1]);

            % Vectors aligned along [1 0 0] which need an arbitrary
            % direction, because we cannot compute the thing automatically
            arbitraryVecs = find(sum(abs(newDirection), 2) == 0);
            upwards = [0 0 1];
            newDirection(arbitraryVecs, :) = upwards(ones(length(arbitraryVecs), 1), :);

            newDirection = newDirection .* norms(:, [1 1 1]);
        end
    end
end
