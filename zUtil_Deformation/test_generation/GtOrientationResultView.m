classdef GtOrientationResultView < GtBaseGuiElem
    properties
        test_data;
        result;
        post_result;

        distance_viewer = [];
        distance_comp_viewer = [];
        domains_viewer = [];
        profile_viewer = [];
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Public object API - Interaction with outside
    methods (Access = public)
        function self = GtOrientationResultView(test_data, result, post_result, varargin)
            self = self@GtBaseGuiElem();

            self.test_data = test_data;
            self.result = result;
            self.post_result = post_result;

            if (~isfield(self.result.ODF6D, 'options'))
                rec_opts = gtReconstruct6DGetParamenters(self.test_data.parameters);
            else
                rec_opts = self.result.ODF6D.options;
            end
            self.conf.f_title = sprintf( ...
                'Result Browser: algo "%s", det_id:%s, res: %g, sf: %s', ...
                rec_opts.algorithm, sprintf(' %d', result.det_index), ...
                rec_opts.ospace_resolution, ...
                GtOrientationResultView.convert_sf2str(rec_opts.shape_functions_type) );

            self.initGtBaseGuiElem(varargin);
        end

        function display_distance(self)
            if (~isempty(self.distance_viewer) && isvalid(self.distance_viewer))
                figure(self.distance_viewer.conf.h_figure)
            else
                self.distance_viewer = GtVolView(self.post_result.distance_deg_alt .* single(self.result.expected_seg));

                self.distance_viewer.conf.h_menu_items{end+1} = ...
                    uimenu(self.distance_viewer.conf.h_context_menu, ...
                        'Label', 'Make figure...', ...
                        'Callback', @(evt,src)self.make_distance_figure(self.distance_viewer));

                self.distance_viewer.conf.h_menu_items{end+1} = ...
                    uimenu(self.distance_viewer.conf.h_context_menu, ...
                        'Label', 'Analyse Pixel...', ...
                        'Callback', @(evt,src)self.show_pixel_figure(self.distance_viewer));

                self.distance_viewer.conf.h_menu_items{end+1} = ...
                    uimenu(self.distance_viewer.conf.h_context_menu, ...
                        'Label', 'Plot 3D Volume from here...', ...
                        'Callback', @(evt,src)self.make_distance_3dplot());
            end
        end

        function display_distance_comp(self)
            if (~isempty(self.distance_comp_viewer) && isvalid(self.distance_comp_viewer))
                figure(self.distance_comp_viewer.conf.h_figure)
            else
                self.distance_comp_viewer = GtMultiVolView(self.post_result.distance_comp_deg);

                self.distance_comp_viewer.conf.h_menu_items{end+1} = ...
                    uimenu(self.distance_comp_viewer.conf.h_context_menu, ...
                        'Label', 'Make figure...', ...
                        'Callback', @(evt,src)self.make_distance_comp_figure());

                self.distance_comp_viewer.conf.h_menu_items{end+1} = ...
                    uimenu(self.distance_comp_viewer.conf.h_context_menu, ...
                        'Label', 'Analyse Pixel...', ...
                        'Callback', @(evt,src)self.show_pixel_figure(self.distance_comp_viewer));

                self.distance_comp_viewer.conf.h_menu_items{end+1} = ...
                    uimenu(self.distance_comp_viewer.conf.h_context_menu, ...
                        'Label', 'Plot 3D Volume from here...', ...
                        'Callback', @(evt,src)self.make_distance_comp_3dplot());
            end
        end

        function display_domains(self)
            if (~isempty(self.domains_viewer) && all(isvalid(self.domains_viewer)))
                for ii = 1:numel(self.domains_viewer)
                    figure(self.domains_viewer(ii).conf.h_figure)
                end
            else
                if (~isempty(self.domains_viewer))
                    % Let's destroy the valid which is left
                    for ii = find(isvalid(self.domains_viewer))'
                        delete(self.domains_viewer(ii))
                    end
                end

                range = [min(self.post_result.domains_theo(:)), max(self.post_result.domains_theo(:))];
                picture = {self.post_result.domains_theo, self.post_result.domains_recon};

                self.domains_viewer = GtVolView.compareVolumes(picture, 'clim', range, 'cmap', jet);

                for ii = 1:numel(self.domains_viewer)
                    self.domains_viewer(ii).conf.h_menu_items{end+1} = ...
                        uimenu(self.domains_viewer(ii).conf.h_context_menu, ...
                            'Label', 'Analyse Pixel...', ...
                            'Callback', @(evt,src)self.show_pixel_figure(self.domains_viewer(ii)));

                    self.domains_viewer(ii).conf.h_menu_items{end+1} = ...
                        uimenu(self.domains_viewer(ii).conf.h_context_menu, ...
                            'Label', 'Plot 3D Volume from here...', ...
                            'Callback', @(evt,src)self.make_domains_3dplot(ii));
                end
            end
        end

        function display_profile(self)
            if (~isempty(self.profile_viewer) && isvalid(self.profile_viewer))
                figure(self.profile_viewer.conf.h_figure)
            else
                self.profile_viewer = GtVolView(self.result.ODF6D.intensity);

                self.profile_viewer.conf.h_menu_items{end+1} = ...
                    uimenu(self.profile_viewer.conf.h_context_menu, ...
                        'Label', 'Shape figure...', ...
                        'Callback', @(evt,src)self.make_shape_figure(self.profile_viewer));

                self.profile_viewer.conf.h_menu_items{end+1} = ...
                    uimenu(self.profile_viewer.conf.h_context_menu, ...
                        'Label', 'IGM figure...', ...
                        'Callback', @(evt,src)self.make_igm_figure(self.profile_viewer));

                self.profile_viewer.conf.h_menu_items{end+1} = ...
                    uimenu(self.profile_viewer.conf.h_context_menu, ...
                        'Label', 'Orientation distance figure...', ...
                        'Callback', @(evt,src)self.make_distance_figure(self.profile_viewer));

                self.profile_viewer.conf.h_menu_items{end+1} = ...
                    uimenu(self.profile_viewer.conf.h_context_menu, ...
                        'Label', 'Analyse Pixel...', ...
                        'Callback', @(evt,src)self.show_pixel_figure(self.profile_viewer));
            end
        end

        function save_to_file(self)
            filename = self.get_result_filename(self.test_data, self.result);

            filter = {'*.mat', 'MATLAB Files (*.mat)'};
            [filename, pathname, ~] = uiputfile(filter, 'Select file...', filename);

            if (filename)
                result = self.result; %#ok<PROP,NASGU>
                post_result = self.post_result; %#ok<PROP,NASGU>
                save(fullfile(pathname, filename), 'result', 'post_result', '-v7.3');
            end
        end

        function figs = make_shape_figure(self, viewer, varargin)
            if (~exist('viewer', 'var') || isempty(viewer))
                slice_ind = get(self.conf.h_shape_slice_edit, 'String');
                slice_ind = eval(slice_ind);
                slice_plane = lower(get(self.conf.h_plane_slice_edit, 'String'));
            else
                slice_ind = viewer.conf.slicendx;
                slice_plane = viewer.conf.plane;
            end
            conf = struct('filename', [], 'border', 3);
            conf = parse_pv_pairs(conf, varargin);

            pixel2cm = str2double(get(self.conf.h_pixtocm_edit, 'String'));

            pixel2pixel = str2double(get(self.conf.h_pixtopix_edit, 'String'));

            slice_rec = gtVolumeGetSlice(self.result.ODF6D.intensity, slice_plane, slice_ind);
            slice_seg = gtVolumeGetSlice(self.result.SEG.seg, slice_plane, slice_ind);
            slice_theo = gtVolumeGetSlice(self.result.expected_intvol, slice_plane, slice_ind);
            slice_theo_seg = gtVolumeGetSlice(self.result.expected_seg, slice_plane, slice_ind);

            if (pixel2pixel > 1)
                slice_rec = gtMathsUpsampleVolume(slice_rec, pixel2pixel .* [1, 1], 'nearest');
                slice_seg = gtMathsUpsampleVolume(slice_seg, pixel2pixel .* [1, 1], 'nearest');
                slice_theo = gtMathsUpsampleVolume(slice_theo, pixel2pixel .* [1, 1], 'nearest');
                slice_theo_seg = gtMathsUpsampleVolume(slice_theo_seg, pixel2pixel .* [1, 1], 'nearest');
            end

            clim = [0, max(self.result.expected_intvol(:))];

%             pos_box = [(slice_rec_size - slice_size) / 2 + 0.5, slice_size + 1];
            extras = { ...
%                 gtFigureCreateExtras('box', 'position', pos_box, 'color', [1 0 0]), ...
                gtFigureCreateExtras('unit_bar', 'margin', [2 1], ...
                    'line_length', 10, 'line_width', 3, ...
                    'font_size', 20, 'font_weight', 'bold'), ...
                };

            det_ind = self.result.det_index(1);
            if (isfield(self.result, 'voxsize'))
                voxsize = self.result.voxsize;
            else
                voxsize = self.test_data.parameters.recgeo(det_index(det_ind)).voxsize;
            end
            [figs{1}, ax] = gtFigureCreateFromPicture(slice_rec, [], 'cmap', gray, ...
                'colorbar', false, 'borders', false, 'pixel_to_cm', pixel2cm, ...
                'extras', extras, 'clims', clim, ...
                'pixel_size', voxsize * 1e3 / pixel2pixel);

            hold(ax, 'on')
            contour(ax, slice_seg', 'levels', 0.5, 'LineWidth', conf.border, 'Color', 'g')
            contour(ax, slice_theo_seg', 'levels', 0.5, 'LineWidth', conf.border, 'Color', 'r')
            hold(ax, 'off')

            save_ref_file = false;
            if (~isempty(conf.filename))
                if (iscell(conf.filename))
                    save_ref_file = (numel(conf.filename) > 1);
                    gtFigureSaveToFile(figs{1}, conf.filename{1});
                else
                    gtFigureSaveToFile(figs{1}, conf.filename);
                end
            end

            if (get(self.conf.theo_check, 'Value') || save_ref_file)
                [figs{2}, ax] = gtFigureCreateFromPicture(slice_theo, [], 'cmap', gray, ...
                    'colorbar', false, 'borders', false, 'pixel_to_cm', pixel2cm, ...
                    'extras', extras, 'clims', clim, ...
                    'pixel_size', voxsize * 1e3 / pixel2pixel);

                hold(ax, 'on')
                contour(ax, slice_theo_seg', 'levels', 0.5, 'LineWidth', conf.border, 'Color', 'r')
                hold(ax, 'off')

                if (save_ref_file)
                    gtFigureSaveToFile(figs{2}, conf.filename{2});
                end
            end
        end

        function figs = make_igm_figure(self, viewer, varargin)
            if (~exist('viewer', 'var') || isempty(viewer))
                slice_ind = get(self.conf.h_shape_slice_edit, 'String');
                slice_ind = eval(slice_ind);
                slice_plane = lower(get(self.conf.h_plane_slice_edit, 'String'));
            else
                slice_ind = viewer.conf.slicendx;
                slice_plane = viewer.conf.plane;
            end
            conf = struct('filename', [], 'border', 3, 'ax', [], 'map_type', 'igm');
            conf = parse_pv_pairs(conf, varargin);

            pixel_to_cm = str2double(get(self.conf.h_pixtocm_edit, 'String'));

            dmvol = self.result.ODF6D.voxels_avg_R_vectors;
            intvol = self.result.ODF6D.intensity;
            seg = self.result.SEG.seg;

            if (strcmpi(conf.map_type, 'igm'))
                igm_rec = gtDefComputeIntraGranularMisorientation( ...
                    dmvol, intvol .* single(seg), ...
                    'R_vector', self.test_data.grain.R_vector);
                igm_theo = gtDefComputeIntraGranularMisorientation( ...
                    self.result.expected_dmvol, self.result.expected_intvol, ...
                    'R_vector', self.test_data.grain.R_vector);
            else
                igm_rec = gtDefComputeKernelAverageMisorientation( ...
                    dmvol, intvol .* single(seg));
                igm_theo = gtDefComputeKernelAverageMisorientation( ...
                    self.result.expected_dmvol, self.result.expected_intvol);
            end

            slice_rec = gtVolumeGetSlice(igm_rec, slice_plane, slice_ind);
            slice_seg = gtVolumeGetSlice(seg, slice_plane, slice_ind);
            slice_theo_rec = gtVolumeGetSlice(igm_theo, slice_plane, slice_ind);
            slice_theo_seg = gtVolumeGetSlice(self.result.expected_seg, slice_plane, slice_ind);

            extras = { ...
                gtFigureCreateExtras('unit_bar', 'margin', [2 1], ...
                    'line_length', 10, 'line_width', 3, ...
                    'font_size', 20, 'font_weight', 'bold'), ...
                };

            det_ind = self.result.det_index(1);
            [figs{1}, ax] = gtFigureCreateFromPicture(slice_rec, [], 'cmap', colormap('parula'), ...
                'colorbar', true, 'borders', false, 'pixel_to_cm', pixel_to_cm, ...
                'extras', extras, 'clims', [0, max(igm_rec(:))], ...
                'pixel_size', self.test_data.parameters.recgeo(det_ind).voxsize * 1e3);

            hold(ax, 'on')
            imagesc(ax, repmat(slice_seg', [1, 1, 3]), 'AlphaDataMapping', 'scaled', 'AlphaData', ~slice_seg')

            contour(ax, slice_theo_seg', 'levels', 0.5, 'LineWidth', conf.border, 'Color', 'r')
            chs = get(ax, 'Children');
            set(ax, 'Children', [chs(1); chs(3:end-1); chs(2); chs(end)])
            hold(ax, 'off')
            conf.ax = ax;

            save_ref_file = false;
            if (~isempty(conf.filename))
                if (iscell(conf.filename))
                    save_ref_file = (numel(conf.filename) > 1);
                    gtFigureSaveToFile(figs{1}, conf.filename{1});
                else
                    gtFigureSaveToFile(figs{1}, conf.filename);
                end
            end

            if (get(self.conf.theo_check, 'Value') || save_ref_file)
                [figs{2}, ax] = gtFigureCreateFromPicture(slice_theo_rec, [], 'cmap', colormap('parula'), ...
                    'colorbar', true, 'borders', false, 'pixel_to_cm', pixel_to_cm, ...
                    'extras', extras, 'clims', [0, max(igm_rec(:))], ...
                    'pixel_size', self.test_data.parameters.recgeo(det_ind).voxsize * 1e3);

                hold(ax, 'on')
                imagesc(ax, repmat(slice_theo_seg', [1, 1, 3]), 'AlphaDataMapping', 'scaled', 'AlphaData', ~slice_theo_seg')

                contour(ax, slice_theo_seg', 'levels', 0.5, 'LineWidth', conf.border, 'Color', 'r')
                chs = get(ax, 'Children');
                set(ax, 'Children', [chs(1); chs(3:end-1); chs(2); chs(end)])
                hold(ax, 'off')

                if (save_ref_file)
                    gtFigureSaveToFile(figs{2}, conf.filename{2});
                end
            end
        end

        function make_orientation_figure(self)
            pos = get(self.conf.h_orient_pos_edit, 'String');
            pos = eval(pos);
            self.make_domains_3dplot(pos)
        end

        function make_ori_distance_figure(self)
            if (get(self.conf.vol_check, 'Value'))
                pos = get(self.conf.h_orient_pos_edit, 'String');
                pos = eval(pos);
                self.make_distance_3dplot(pos)
            else
                self.make_distance_figure()
            end
        end

        function make_ipf_figure(self)
            ipf_dir = get(self.conf.h_ipf_dir_edit, 'String');
            ipf_dir = eval(ipf_dir);
            pos = get(self.conf.h_orient_pos_edit, 'String');
            pos = eval(pos);
            self.make_ipf_3dplot(pos, ipf_dir)
        end

        function make_vox_odf_figure(self)
            vox_percent = get(self.conf.h_vox_odf_percent_edit, 'String');
            vox_percent = eval(vox_percent);
            pos = get(self.conf.h_orient_pos_edit, 'String');
            pos = eval(pos);
            self.make_vox_odf_3dplot(pos, vox_percent)
        end
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Internal methods - Addressable from derived classes
    methods (Access = protected)
        function initGui(obj)
            initGui@GtBaseGuiElem(obj);
            % Let's disable zoom, while configuring
            % must disable zoom before setting impixelinfoval (R2008A)
            zoomOp = zoom(obj.conf.h_figure);
            zoomState = get(zoomOp, 'Enable');
            zoom(obj.conf.h_figure, 'off');

            obj.conf.main_boxes = uiextras.HBox('Parent', obj.conf.currentParent);

            % Layout for buttons used to open windows
            obj.conf.button_boxes = uiextras.VButtonBox('Parent', obj.conf.main_boxes);
            set(obj.conf.button_boxes, 'ButtonSize', [200, 30])

            uicontrol('Parent', obj.conf.button_boxes, 'Style', 'text', ...
                'String', 'Windows:', 'FontSize', 14);

            obj.conf.h_profile_button = uicontrol( ...
                'Parent', obj.conf.button_boxes, 'Style', 'pushbutton', ...
                'String', 'Grain Profile');
            obj.conf.h_distance_button = uicontrol( ...
                'Parent', obj.conf.button_boxes, 'Style', 'pushbutton', ...
                'String', 'Distance (deg)');
            obj.conf.h_distance_comp_button = uicontrol( ...
                'Parent', obj.conf.button_boxes, 'Style', 'pushbutton', ...
                'String', 'Distance Components (deg)');
            obj.conf.h_domains_button = uicontrol( ...
                'Parent', obj.conf.button_boxes, 'Style', 'pushbutton', ...
                'String', 'Domains');

            % Spacer
            uicontrol('Parent', obj.conf.button_boxes, 'Enable', 'off', ...
                'Visible', 'off', 'Tag', 'spacer');

            uicontrol('Parent', obj.conf.button_boxes, 'Style', 'text', ...
                'String', 'Saving:');

            obj.conf.h_save_button = uicontrol(...
                'Parent', obj.conf.button_boxes, 'Style', 'pushbutton', ...
                'String', 'Save to file');

            % Layout for parameters and figures
            obj.conf.figures_boxes = uiextras.VBox('Parent', obj.conf.main_boxes);

            if (~isfield(obj.result.ODF6D, 'options'))
                rec_opts = gtReconstruct6DGetParamenters(obj.test_data.parameters);
            else
                rec_opts = obj.result.ODF6D.options;
            end
            params = { ...
                'Iterations:', sprintf('%d', rec_opts.num_iter); ...
                'Detector norm:', rec_opts.detector_norm; ...
                'lambda l1:', sprintf('%e', rec_opts.lambda_l1); ...
                'lambda TV:', sprintf('%e', rec_opts.lambda_tv); ...
                'Shape function:', GtOrientationResultView.convert_sf2str(rec_opts.shape_functions_type);
                };
            num_params = size(params, 1);

            % Layout for buttons used to create figures
            uicontrol('Parent', obj.conf.figures_boxes, 'Enable', 'off', ...
                'Visible', 'off', 'Tag', 'spacer');

            uicontrol('Parent', obj.conf.figures_boxes, 'Style', 'text', ...
                'String', 'Parameters:', 'FontSize', 14);

            % Parameters
            for ii_p = 1:num_params
                obj.conf.p_boxes = uiextras.HButtonBox('Parent', obj.conf.figures_boxes);
                set(obj.conf.p_boxes, 'ButtonSize', [120, 30])

                uicontrol('Parent', obj.conf.p_boxes, 'Style', 'text', ...
                    'String', params{ii_p, 1});
                uicontrol('Parent', obj.conf.p_boxes, 'Style', 'text', ...
                    'String', params{ii_p, 2});
            end

            % Spacer
            uicontrol('Parent', obj.conf.figures_boxes, 'Enable', 'off', ...
                'Visible', 'off', 'Tag', 'spacer');

            obj.conf.title_fig_boxes = uiextras.HButtonBox('Parent', obj.conf.figures_boxes);
            set(obj.conf.title_fig_boxes, 'ButtonSize', [160, 25])

            uicontrol('Parent', obj.conf.title_fig_boxes, 'Style', 'text', ...
                'String', 'Figures:', 'FontSize', 14);

            obj.conf.theo_check = uicontrol('Parent', obj.conf.title_fig_boxes, ...
                'Style', 'togglebutton', 'String', 'Create reference');

            % Shape figures
            obj.conf.shape_boxes = uiextras.HButtonBox('Parent', obj.conf.figures_boxes);
            set(obj.conf.shape_boxes, 'ButtonSize', [120, 30])

            obj.conf.h_shape_button = uicontrol( ...
                'Parent', obj.conf.shape_boxes, 'Style', 'pushbutton', ...
                'String', 'Shape figure');

            uicontrol('Parent', obj.conf.shape_boxes, 'Style', 'text', ...
                'String', 'Slice:');

            default_slice = round(0.6 * size(obj.result.expected_intvol, 3));
            obj.conf.h_shape_slice_edit = uicontrol('Style', 'edit', ...
                'Parent', obj.conf.shape_boxes, ...
                'String', sprintf('%d', default_slice));

            obj.conf.plane_boxes = uiextras.HButtonBox('Parent', obj.conf.figures_boxes);
            set(obj.conf.plane_boxes, 'ButtonSize', [120, 30])

            obj.conf.h_igm_button = uicontrol( ...
                'Parent', obj.conf.plane_boxes, 'Style', 'pushbutton', ...
                'String', 'IGM figure');

            uicontrol('Parent', obj.conf.plane_boxes, 'Style', 'text', ...
                'String', 'Plane:');

            obj.conf.h_plane_slice_edit = uicontrol('Style', 'edit', ...
                'Parent', obj.conf.plane_boxes, 'String', 'XY');

            % Orientation figures
            obj.conf.orient_boxes = uiextras.HButtonBox('Parent', obj.conf.figures_boxes);
            set(obj.conf.orient_boxes, 'ButtonSize', [120, 30])

            obj.conf.h_orient_button = uicontrol( ...
                'Parent', obj.conf.orient_boxes, 'Style', 'pushbutton', ...
                'String', 'Orientation figure');

            uicontrol('Parent', obj.conf.orient_boxes, 'Style', 'text', ...
                'String', 'Position:');

            default_point = round(abs([1, 1, 0] - 0.15) .* size(obj.result.expected_intvol));
            obj.conf.h_orient_pos_edit = uicontrol('Style', 'edit', ...
                'Parent', obj.conf.orient_boxes, ...
                'String', ['[' sprintf(' %d', default_point) ']']);

            % Distance figures
            obj.conf.distance_boxes = uiextras.HButtonBox('Parent', obj.conf.figures_boxes);
            set(obj.conf.distance_boxes, 'ButtonSize', [120, 30])

            obj.conf.h_dist_button = uicontrol( ...
                'Parent', obj.conf.distance_boxes, 'Style', 'pushbutton', ...
                'String', 'Distance figure');

            uicontrol('Parent', obj.conf.distance_boxes, 'Style', 'text', ...
                'String', 'Style:');

            obj.conf.vol_check = uicontrol('Parent', obj.conf.distance_boxes, ...
                'Style', 'togglebutton', 'String', 'Volumetric');

            % IPF figures
            obj.conf.ipf_boxes = uiextras.HButtonBox('Parent', obj.conf.figures_boxes);
            set(obj.conf.ipf_boxes, 'ButtonSize', [120, 30])

            obj.conf.h_ipf_button = uicontrol( ...
                'Parent', obj.conf.ipf_boxes, 'Style', 'pushbutton', ...
                'String', 'IPF figure');

            uicontrol('Parent', obj.conf.ipf_boxes, 'Style', 'text', ...
                'String', 'Direction:');

            obj.conf.h_ipf_dir_edit = uicontrol('Style', 'edit', ...
                'Parent', obj.conf.ipf_boxes, 'String', '[0 0 1]');

            % Voxel analysis figures
            obj.conf.vox_odf_boxes = uiextras.HButtonBox('Parent', obj.conf.figures_boxes);
            set(obj.conf.vox_odf_boxes, 'ButtonSize', [120, 30])

            obj.conf.h_vox_odf_button = uicontrol( ...
                'Parent', obj.conf.vox_odf_boxes, 'Style', 'pushbutton', ...
                'String', 'Voxel ODF');

            uicontrol('Parent', obj.conf.vox_odf_boxes, 'Style', 'text', ...
                'String', 'Percentile:');

            obj.conf.h_vox_odf_percent_edit = uicontrol('Style', 'edit', ...
                'Parent', obj.conf.vox_odf_boxes, 'String', '1.0');

            % pixel 2 cm option
            obj.conf.pix2cm_boxes = uiextras.HButtonBox('Parent', obj.conf.figures_boxes);
            set(obj.conf.pix2cm_boxes, 'ButtonSize', [120, 30])

            uicontrol('Parent', obj.conf.pix2cm_boxes, 'Style', 'text', ...
                'String', ' ');

            uicontrol('Parent', obj.conf.pix2cm_boxes, 'Style', 'text', ...
                'String', 'pixel_to_cm:');

            obj.conf.h_pixtocm_edit = uicontrol('Style', 'edit', ...
                'Parent', obj.conf.pix2cm_boxes, 'String', '0.2');

            % pixel 2 pixel option
            obj.conf.pix2pix_boxes = uiextras.HButtonBox('Parent', obj.conf.figures_boxes);
            set(obj.conf.pix2pix_boxes, 'ButtonSize', [120, 30])

            uicontrol('Parent', obj.conf.pix2pix_boxes, 'Style', 'text', ...
                'String', ' ');

            uicontrol('Parent', obj.conf.pix2pix_boxes, 'Style', 'text', ...
                'String', 'pixel_to_pixel:');

            obj.conf.h_pixtopix_edit = uicontrol('Style', 'edit', ...
                'Parent', obj.conf.pix2pix_boxes, 'String', '1');

            % Spacer
            uicontrol('Parent', obj.conf.figures_boxes, 'Enable', 'off', ...
                'Visible', 'off', 'Tag', 'spacer');
            set(obj.conf.figures_boxes, 'Sizes', [-1, 30, 30 * ones(1, num_params), -1, 30, 35 * ones(1, 8), -1])

            % Reset back the zoom to the previous state (before the inclusion)
            set(zoomOp, 'Enable', zoomState);
        end

        function addUICallbacks(self)
            % windows buttons
            self.addUICallback(self.conf.h_profile_button, 'Callback', ...
                @(src, evt)display_profile(self), false);

            self.addUICallback(self.conf.h_distance_button, 'Callback', ...
                @(src, evt)display_distance(self), false);

            self.addUICallback(self.conf.h_distance_comp_button, 'Callback', ...
                @(src, evt)display_distance_comp(self), false);

            self.addUICallback(self.conf.h_domains_button, 'Callback', ...
                @(src, evt)display_domains(self), false);

            % Saving button
            self.addUICallback(self.conf.h_save_button, 'Callback', ...
                @(src, evt)save_to_file(self), false);

            % Figures buttons
            self.addUICallback(self.conf.h_shape_button, 'Callback', ...
                @(src, evt)make_shape_figure(self), false);

            self.addUICallback(self.conf.h_igm_button, 'Callback', ...
                @(src, evt)make_igm_figure(self), false);

            self.addUICallback(self.conf.h_orient_button, 'Callback', ...
                @(src, evt)make_orientation_figure(self), false);

            self.addUICallback(self.conf.h_dist_button, 'Callback', ...
                @(src, evt)make_ori_distance_figure(self), false);

            self.addUICallback(self.conf.h_ipf_button, 'Callback', ...
                @(src, evt)make_ipf_figure(self), false);

            self.addUICallback(self.conf.h_vox_odf_button, 'Callback', ...
                @(src, evt)make_vox_odf_figure(self), false);
        end

        function resetUiComponents(self)
        end

        function doUpdateDisplay(self)
        end
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Public interaction API - Interaction with called viewers
    methods (Access = public)
        function [f, conf] = make_distance_figure(self, viewer, varargin)
            if (~exist('viewer', 'var') || isempty(viewer))
                slice_ind = get(self.conf.h_shape_slice_edit, 'String');
                slice_ind = eval(slice_ind);
                slice_plane = lower(get(self.conf.h_plane_slice_edit, 'String'));
            else
                slice_ind = viewer.conf.slicendx;
                slice_plane = viewer.conf.plane;
            end
            conf = struct('filename', [], 'border', 3, 'clims', [], 'ax', [], 'axes', []);
            conf = parse_pv_pairs(conf, varargin);

            pixel_to_cm = str2double(get(self.conf.h_pixtocm_edit, 'String'));

            pixel2pixel = str2double(get(self.conf.h_pixtopix_edit, 'String'));

            dist_mask = single(self.result.expected_seg);
%             dist_mask = dist_mask .* single(self.result.SEG.seg);

            if (isempty(conf.axes))
                dist_map = self.post_result.distance_deg_alt .* dist_mask;
            else
                dmvol_exp = self.result.expected_dmvol(:, :, :, conf.axes);
                dmvol_rec = self.result.ODF6D.voxels_avg_R_vectors(:, :, :, conf.axes);
                dist_map = sqrt(sum((dmvol_exp - dmvol_rec) .^ 2, 4));
                dist_map = 2 * atand(dist_map) .* dist_mask;
            end
            dists = sort(dist_map(:));
            dists_cum = cumsum(dists) / sum(dists);
            ind = find(dists_cum < 0.98, 1, 'last');
            max_val = dists(ind);
            dist_map = min(dist_map, max_val);
            slice_dist = gtVolumeGetSlice(dist_map, slice_plane, slice_ind);

            slice_mask = gtVolumeGetSlice(dist_mask, slice_plane, slice_ind);
            slice_dist = slice_dist .* slice_mask;

            slice_theo_seg = gtVolumeGetSlice(self.result.expected_seg, slice_plane, slice_ind);
            slice_theo_seg = single(slice_theo_seg);

            if (pixel2pixel > 1)
                slice_dist = gtMathsUpsampleVolume(slice_dist, pixel2pixel .* [1, 1], 'nearest');
                slice_mask = gtMathsUpsampleVolume(slice_mask, pixel2pixel .* [1, 1], 'nearest');
                slice_theo_seg = gtMathsUpsampleVolume(slice_theo_seg, pixel2pixel .* [1, 1], 'nearest');
            end

            if (isempty(conf.clims))
                conf.clims = [0, max(dist_map(:))];
            end

            extras = { ...
                gtFigureCreateExtras('unit_bar', 'margin', [2 1], ...
                    'line_length', 10, 'line_width', 3, ...
                    'font_size', 20, 'font_weight', 'bold'), ...
                };

            det_ind = self.result.det_index(1);
            if (isfield(self.result, 'voxsize'))
                voxsize = self.result.voxsize;
            else
                voxsize = self.test_data.parameters.recgeo(det_index(det_ind)).voxsize;
            end
            [f, ax] = gtFigureCreateFromPicture(slice_dist, [], ...
                'colorbar', true, 'border', false, 'axes_ticks', false, ...
                'pixel_to_cm', pixel_to_cm, 'cmap', colormap('parula'), ...
                'extras', extras, 'clims', conf.clims, ...
                'pixel_size', voxsize * 1e3 / pixel2pixel);

            hold(ax, 'on')
            imagesc(ax, repmat(slice_mask', [1, 1, 3]), 'AlphaDataMapping', 'scaled', 'AlphaData', ~slice_mask')

            contour(ax, slice_theo_seg', 'levels', 0.5, 'LineWidth', conf.border, 'Color', 'r')
            chs = get(ax, 'Children');
            set(ax, 'Children', [chs(1); chs(3:end-1); chs(2); chs(end)])
            hold(ax, 'off')
            conf.ax = ax;

            if (~isempty(conf.filename))
                gtFigureSaveToFile(f, conf.filename);
            end
        end

        function make_distance_comp_figure(self)
            point = self.distance_comp_viewer.conf.clicked_point;
            point = self.distance_comp_viewer.getComplete3DPixelInfo(point);
            plane = self.distance_comp_viewer.conf.plane;
            slice = point(self.distance_comp_viewer.conf.ortDim);
            component = self.distance_comp_viewer.conf.volumeIndex;

            extras = { ...
                gtFigureCreateExtras('unit_bar', 'margin', [2 1], ...
                    'line_length', 10, 'line_width', 3, ...
                    'font_size', 16, 'font_weight', 'bold'), ...
                };
            det_ind = self.result.det_index(1);
            gtFigureCreateFromVolume(self.post_result.distance_comp_deg(:, :, :, component), ...
                'colorbar', true, 'border', true, 'axes_ticks', false, ...
                'slice', slice, 'plane', plane, 'pixel_to_cm', 0.2, ...
                'extras', extras, ...
                'pixel_size', self.test_data.parameters.recgeo(det_ind).voxsize * 1e3)
            fprintf([ ...
                'gtFigureCreateFromVolume(post_result.distance_comp_deg(:, :, :, component), ' ...
                    '''colorbar'', true, ''border'', true, ' ...
                    '''slice'', %d, ''plane'', ''%s'', ''pixel_to_cm'', 0.2)\n'], ...
                slice, plane)
        end

        function make_distance_3dplot(self, indx)
            if (numel(indx) == 1)
                point = self.distance_viewer(indx).conf.clicked_point;
                point = self.distance_viewer(indx).getComplete3DPixelInfo(point);
            else
                point = indx;
            end
            fprintf('3dplot for distance, from point:')
            disp(point)
            internal_vol = self.post_result.distance_deg_alt(2:end-1,2:end-1,2:end-1);
            dist_deg = min(self.post_result.distance_deg_alt, max(internal_vol(:)));
            gtFigureCreateVolumeSlices(dist_deg, point, 'colorbar', true);
        end

        function make_distance_comp_3dplot(self)
            point = self.distance_comp_viewer.conf.clicked_point;
            point = self.distance_comp_viewer.getComplete3DPixelInfo(point);
            component = self.distance_comp_viewer.conf.volumeIndex;
            gtFigureCreateVolumeSlices(self.post_result.distance_comp_deg(:, :, :, component), point, 'colorbar', true);
        end

        function make_domains_3dplot(self, indx)
            if (numel(indx) == 1)
                point = self.domains_viewer(indx).conf.clicked_point;
                point = self.domains_viewer(indx).getComplete3DPixelInfo(point);
            else
                point = indx;
            end

            range = [min(self.post_result.domains_theo(:)), max(self.post_result.domains_theo(:))];

            if (get(self.conf.theo_check, 'Value'))
                gtFigureCreateVolumeSlices(self.post_result.domains_theo, point, 'clim', range)
            end
            gtFigureCreateVolumeSlices(self.post_result.domains_recon, point, 'clim', range)
        end

        function make_ipf_3dplot(self, indx, ipf_dir)
            if (numel(indx) == 1)
                point = self.domains_viewer(indx).conf.clicked_point;
                point = self.domains_viewer(indx).getComplete3DPixelInfo(point);
            else
                point = indx;
            end

            size_vol = size(self.result.expected_dmvol(:, :, :, 1));
            vox_inds = reshape(1:prod(size_vol), size_vol);

            gvdm_theo = gtDefDmvol2Gvdm(self.result.expected_dmvol)';
            gvdm_rec = gtDefDmvol2Gvdm(self.post_result.avg_orientations)';
            
            crystal_system = self.test_data.parameters.cryst.crystal_system;
            spacegroup = self.test_data.parameters.cryst.spacegroup;
            [cmap_theo, ~, ~] = gtIPFCmap([], ipf_dir, ...
                'r_vectors', gvdm_theo, 'background', false, ...
                'crystal_system' , crystal_system, 'spacegroup', spacegroup);
            [cmap_rec, ~, ~] = gtIPFCmap([], ipf_dir, ...
                'r_vectors', gvdm_rec, 'background', false, ...
                'crystal_system' , crystal_system, 'spacegroup', spacegroup);

            if (get(self.conf.theo_check, 'Value'))
                gtFigureCreateVolumeSlices(vox_inds, point, 'cmap', cmap_theo)
            end
            gtFigureCreateVolumeSlices(vox_inds, point, 'cmap', cmap_rec)
        end

        function make_vox_odf_3dplot(self, indx, odf_percent)
            if (numel(indx) == 1)
                point = self.domains_viewer(indx).conf.clicked_point;
                point = self.domains_viewer(indx).getComplete3DPixelInfo(point);
            else
                point = indx;
            end

            theo_size = [size(self.result.expected_dmvol, 1), size(self.result.expected_dmvol, 2), size(self.result.expected_dmvol, 3)];
            rec_size = [size(self.result.solution{1}, 1), size(self.result.solution{1}, 2), size(self.result.solution{1}, 3)];
            offset = floor((rec_size - theo_size) / 2);
            point = point + offset;

            gt6DAnalyseVoxelProperties(self.test_data, self.result, self.post_result, point, [], [], [], odf_percent);
        end

        function show_pixel_figure(self, viewer)
            point = viewer.conf.clicked_point;
            point = viewer.getComplete3DPixelInfo(point);

            % Ad-hoc solution for those volumes where we crop already the
            % margin
            theo_size = [size(self.result.expected_dmvol, 1), size(self.result.expected_dmvol, 2), size(self.result.expected_dmvol, 3)];
            if (all(size(viewer.conf.vol) == theo_size))
                rec_size = [size(self.result.solution{1}, 1), size(self.result.solution{1}, 2), size(self.result.solution{1}, 3)];
                offset = floor((rec_size - theo_size) / 2);
                point = point + offset;
            end

            gt6DAnalyseVoxelProperties(self.test_data, self.result, self.post_result, point);
        end
    end

    methods (Access = public, Static)
        function filename = get_result_filename(test_data, result)
            det_inds = sort(result.det_index);
            detector_str = cell(numel(det_inds), 1);
            for ii = 1:numel(det_inds)
                ii_d = det_inds(ii);

                mode = lower(test_data.parameters.acq(ii_d).type);
                if (ismember(mode, {'360degree', '180degree'}))
                    mode = 'dct';
                end

                detector_str{ii} = sprintf('-%d-%s-%gKeV', ii_d, mode, ...
                    round(test_data.parameters.acq(ii_d).energy));
            end
            detector_str = [detector_str{:}];

            rec_opts = gtReconstruct6DGetParamenters(test_data.parameters);

            if (isfield(result, 'voxsize'))
                voxsize = result.voxsize;
            else
                voxsize = test_data.parameters.recgeo(det_index(det_ind)).voxsize;
            end
            rspace_resolution = mean(voxsize) * 1e3;
            filename = sprintf('rec_algo-%s_ores%s_rres%s_det%s_sf-%s.mat', ...
                rec_opts.algorithm, ...
                sprintf('-%g', rec_opts.ospace_resolution), ...
                sprintf('-%g', rspace_resolution), ...
                detector_str, GtOrientationResultView.convert_sf2str(rec_opts.shape_functions_type));
        end

        function [border_mask, border_img] = get_border(slice_seg, color, thickness)
            if (~exist('color', 'var') || isempty(color))
                color = [1, 0, 0];
            end

            figure, contour(slice_seg, 'levels', 1, 'LineWidth', 4, 'Color', color)

            slice_seg = interp2(single(slice_seg), 1);
            border_mask = abs(slice_seg - 0.5 * max(slice_seg(:))) < (max(slice_seg(:)) * 0.2);
            border_mask = imclose(border_mask, strel('disk', 4))';

            if (exist('thickness', 'var') && ~isempty(thickness) && thickness > 1)
                ker_edge = ceil((thickness - 1) / 2);
                ker_size = 2 * ker_edge + 1;
                xx = linspace(-ker_edge, ker_edge, ker_size);
                yy = linspace(-ker_edge, ker_edge, ker_size);
                [xx, yy] = ndgrid(xx, yy);
                rr = sqrt(xx .^ 2 + yy .^ 2);
                elem = single((rr + eps('single')) < ker_edge) ...
                    + 0.5 * single(abs(rr - ker_edge) <= eps('single')) ...
                    + 0.25 * single(abs(rr - ker_edge) <= 0.5);
                
                border_mask = conv2(border_mask, elem, 'same') > 0.5;
%                 border_mask = imdilate(border_mask, ones(thickness));
            end

            border_img = bsxfun(@times, single(border_mask), reshape(color, 1, 1, 3));
        end

        function sf_str = convert_sf2str(sf_type)
            if (iscell(sf_type))
                sf_str = reshape(sf_type, 1, []);
                sf_str(2, :) = {'-'};
                sf_str = [sf_str{1:end-1}];
            else
                sf_str = sf_type;
            end
        end
    end
end
