function cfg = gtPlotGvdm(gvdm, varargin)
    cfg = struct('figure', {[]}, 'axes', {[]}, 'hold', {false}, 'color', {'b'}, 'size', {30}, 'skip', {1});

    if (nargin >= 2)
        if ((numel(varargin) == 1) && isstruct(varargin{1}))
            cfg = varargin{1};
        else
            cfg = parse_pv_pairs(cfg, varargin);
        end
    end

    if (isempty(cfg.figure) && isempty(cfg.axes))
        cfg.figure = figure();
        cfg.axes = axes('parent', cfg.figure);
    elseif (~isempty(cfg.figure))
        cfg.figure = figure(cfg.figure);
        if (isempty(cfg.axes))
            cfg.axes = axes('parent', cfg.figure);
        else
            try
                axes(cfg.axes);
                set(cfg.figure, 'CurrentAxes', cfg.axes)
            catch % If it returns an error, it's because there's no such axes, or it belongs to another figure
                cfg.axes = axes('parent', cfg.figure);
            end
        end
    end

    if (cfg.hold)
        previously_on_hold = ishold(cfg.axes);
        if (~previously_on_hold)
            hold(cfg.axes, 'on');
        end
    end

    if (isstruct(gvdm))
        grid_gr = gvdm.gr(:);
        grid_gr = [grid_gr{:}];
        r_vecs = cat(1, grid_gr(:).R_vector);
        gvdm = r_vecs(gvdm.seg(:), :);
    end

    [shape_gvdm(1), shape_gvdm(2)] = size(gvdm);

    if (shape_gvdm(2) == 3)
        gvdm = gvdm';
    end

    scatter3(cfg.axes, gvdm(1, 1:cfg.skip:end), gvdm(2, 1:cfg.skip:end), gvdm(3, 1:cfg.skip:end), cfg.size, cfg.color);

    if (cfg.hold && ~previously_on_hold)
        hold(cfg.axes, 'off');
    end
end