function ori_map = gtDefChangeOrientationReferenceSystem(ori_map, from_new_to_old, phase_id)

    if (~exist('phase_id', 'var') || isempty(phase_id))
        phase_id = 1;
    end

    [map_size(1), map_size(2), map_size(3), map_size(4)] = size(ori_map);

    is_EBSD = map_size(3) == 3 && map_size(4) == 1;
    if (is_EBSD)
        ori_map = permute(ori_map, [1 2 4 3]);
    end

    p = gtLoadParameters();
    spacegroup = p.cryst(phase_id).spacegroup;
    crystal_system = p.cryst(phase_id).crystal_system;
    symm = gtCrystGetSymmetryOperators(crystal_system, spacegroup);

    [gvdm_ori_map, size_dmvol_ori_map] = gtDefDmvol2Gvdm(ori_map);

    inds_to_remap = any(gvdm_ori_map, 1);
    from_new_to_old = reshape(from_new_to_old, 3, 1);

    fprintf('Shifting values..')
    c = tic();
    gvdm_ori_map(:, inds_to_remap) = gtMathsRodSum(from_new_to_old, gvdm_ori_map(:, inds_to_remap));
    fprintf('\b\b: Done in %f seconds.\nRe-mapping to Fundamental Zone..', toc(c))
    c = tic();
    gvdm_ori_map(:, inds_to_remap) = gtMathsRod2RodInFundZone(gvdm_ori_map(:, inds_to_remap), symm);
    ori_map = gtDefGvdm2Dmvol(gvdm_ori_map, size_dmvol_ori_map);
    fprintf('\b\b: Done in %f seconds.\n', toc(c))

    if (is_EBSD)
        ori_map = permute(ori_map, [1 2 4 3]);
    end
end
