function gt6DMakeSpotFigureForPaper(gr, spot_idx, varargin)
    conf = struct( ...
        'pixel_to_cm', 0.05 );
    conf = parse_pv_pairs(conf, varargin);

    selected = find(gr.proj.selected);

    spot = gr.proj.stack(:, spot_idx, :);
    spot = squeeze(spot);
    spot = (spot - min(spot(:))) / (max(spot(:)) - min(spot(:)));
    spot = spot(:, :, [1 1 1]);

    included = gr.proj.ondet(gr.proj.included);

    sel_idx = included(selected(spot_idx));
    fprintf('HKL %d %d %d, Theta %g, Eta %g, Omega %g delta(%d, %d), \n', ...
        gr.allblobs.hkl(sel_idx, :), gr.allblobs.theta(sel_idx), ...
        gr.allblobs.eta(sel_idx), gr.allblobs.omega(sel_idx), ...
        gr.proj.bl(selected(spot_idx)).bbwim)

    f = figure();
    ax = axes('parent', f);
    imagesc(permute(spot, [2 1 3]), 'parent', ax);

    set(f, 'Units', 'centimeters')
    img_size = size(spot(:, :, 1)) * conf.pixel_to_cm;
    position = [0, 0, img_size];
    set(f, 'Position', position)
    set(f, 'Paperposition', position)

    set(ax, 'Units', 'normalized')
    set(ax, 'Position', [0, 0, 1, 1])
end
