function gt6DPlotVoxelOrientationSpace(ODF6D, full_ODF6D, position, threshold_min, selected)
    if (~exist('threshold_min', 'var') || isempty(threshold_min))
        threshold_min = 0;
    end
    if (~exist('selected', 'var') || isempty(selected))
        selected = true(size(full_ODF6D.orientation_volumes));
    end

    f = figure();
    ax = axes('parent', f);

    scatter3(ax, [], [], []);
    hold(ax, 'on');

    gvdm_tmp_t = ODF6D.R_vectors{1}(selected, :);
    gvdm_tmp_t = 2 * atand(gvdm_tmp_t);
    show_bbox(ax, gvdm_tmp_t);

    avg_R_vec = ODF6D.voxels_avg_R_vectors(position(1), position(2), position(3), :);
    avg_R_vec = reshape(avg_R_vec, [], 1);
    avg_R_vec = 2 * atand(avg_R_vec);
    scatter3(ax, avg_R_vec(1), avg_R_vec(2), avg_R_vec(3), 30, [0 0 0], 'filled');

    intensities_recon = cellfun(@(x)x(position(1), position(2), position(3)), full_ODF6D.orientation_volumes);
    show_orientation_colormap(ax, gvdm_tmp_t, intensities_recon, jet(), threshold_min);

    hold(ax, 'off');

    set(f, 'Units', 'centimeters')
    set(ax, 'Units', 'centimeters')

    set(f, 'Position', [0 0 20 14])
    set(f, 'Paperposition', [0 0 20 14])

    set(ax, 'Position', [1.25 1 16.25 12])
end

function show_orientation_colormap(ax, sampled_R_vecs, intensities, cmap, thr_min)
    indexes_recon_min = intensities > thr_min;
    voxel_R_vecs = sampled_R_vecs(indexes_recon_min, :);
    voxel_intensities = intensities(indexes_recon_min);

    min_int = min(voxel_intensities);
    max_int = max(voxel_intensities);

    % Rescaling
    voxel_intensities = (voxel_intensities - min_int) / (max_int - min_int);
    voxel_intensities = voxel_intensities * (size(cmap, 1)-1) + 1;
    voxel_intensities = round(voxel_intensities);

    scatter3(ax, voxel_R_vecs(:, 1), voxel_R_vecs(:, 2), voxel_R_vecs(:, 3), 20, cmap(voxel_intensities, :), 'filled')
    cb = colorbar('peer', ax, 'YTickLabel', min_int:((max_int-min_int)/10):max_int, 'location', 'EastOutside');
    set(cb, 'Units', 'centimeters')
    set(cb, 'Position', [17.35 1 0.65 12])
end

function show_bbox(ax, sampled_R_vecs)
    min_sampled_R_vecs = min(sampled_R_vecs, [], 1);
    max_sampled_R_vecs = max(sampled_R_vecs, [], 1);
    bbox_R_vecs = [ ...
        min_sampled_R_vecs(1), min_sampled_R_vecs(2), min_sampled_R_vecs(3); ...
        min_sampled_R_vecs(1), min_sampled_R_vecs(2), max_sampled_R_vecs(3); ...
        min_sampled_R_vecs(1), max_sampled_R_vecs(2), min_sampled_R_vecs(3); ...
        min_sampled_R_vecs(1), max_sampled_R_vecs(2), max_sampled_R_vecs(3); ...

        max_sampled_R_vecs(1), min_sampled_R_vecs(2), min_sampled_R_vecs(3); ...
        max_sampled_R_vecs(1), min_sampled_R_vecs(2), max_sampled_R_vecs(3); ...
        max_sampled_R_vecs(1), max_sampled_R_vecs(2), min_sampled_R_vecs(3); ...
        max_sampled_R_vecs(1), max_sampled_R_vecs(2), max_sampled_R_vecs(3); ...
        ];
    faces = [ ...
        1 5; 2 6; 3 7; 4 8; ...
        1 3; 2 4; 5 7; 6 8; ...
        1 2; 3 4; 5 6; 7 8; ...
        ];

    scatter3(ax, bbox_R_vecs(:, 1), bbox_R_vecs(:, 2), bbox_R_vecs(:, 3), 30, 'y', 'filled');
    patch('parent', ax, 'Faces', faces, 'Vertices', bbox_R_vecs, 'FaceColor', 'w');
end