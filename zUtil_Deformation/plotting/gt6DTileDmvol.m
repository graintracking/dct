function tiling = gt6DTileDmvol(dmvol, tiles)
    distances = cell(size(tiles, 1), 1);
    for ii = 1:numel(distances)
        tile_r_vec = reshape(tiles(ii, :), 1, 1, 1, 3);
        dm_diff_vol = bsxfun(@minus, dmvol, tile_r_vec);
        distances{ii} = sqrt(sum((dm_diff_vol .^ 2), 4));
    end

    tiling = zeros(size(distances{1}));
    curr_tile_dist = Inf(size(tiling));
    for ii = 1:numel(distances)
        indx = distances{ii} < curr_tile_dist;
        curr_tile_dist(indx) = distances{ii}(indx);
        tiling(indx) = ii;
    end
end