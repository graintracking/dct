function gtPlotXYZMapsODF(odf_theo, odf_rec)
    odf_theo = odf_theo / max(odf_theo(:));
    odf_rec = odf_rec / max(odf_rec(:));

    sliceXY_theo = sum(odf_theo, 3);
    sliceXY_rec = sum(odf_rec, 3);

    sliceXZ_theo = squeeze(sum(odf_theo, 2));
    sliceXZ_rec = squeeze(sum(odf_rec, 2));

    sliceYZ_theo = squeeze(sum(odf_theo, 1));
    sliceYZ_rec = squeeze(sum(odf_rec, 1));

%     meshc(sliceXY_theo)
%     axis([1, size(odf_theo, 2), 1, size(odf_theo, 1), -5, max(sliceXY_theo(:))])
    f = figure();
    subplot(1, 2, 1); contourf(sliceXY_theo); title('Theo ODF XY');
    subplot(1, 2, 2); contourf(sliceXY_rec); title('Rec ODF XY');
    set(f, 'Units', 'centimeters');
    set(f, 'Position', [0 0 50 25]);
    f = figure(); title('XZ');
    subplot(1, 2, 1);  contourf(sliceXZ_theo); title('Theo ODF XZ');
    subplot(1, 2, 2);  contourf(sliceXZ_rec); title('Rec ODF XZ');
    set(f, 'Units', 'centimeters');
    set(f, 'Position', [0 0 50 25]);
    f = figure(); title('YZ');
    subplot(1, 2, 1);  contourf(sliceYZ_theo); title('Theo ODF YZ');
    subplot(1, 2, 2);  contourf(sliceYZ_rec); title('Rec ODF YZ');
    set(f, 'Units', 'centimeters');
    set(f, 'Position', [0 0 50 25]);
end