function gt6DAnalyseVoxelProperties(test_data, result, post_result, voxel_rec_pos, threshold_min, threshold_max, show_base_grid, contribution_fraction)
    if (~exist('show_base_grid', 'var') || isempty(show_base_grid))
        show_base_grid = false;
    end

    dmvol_theo = result.expected_dmvol;
    dmvol_rec = result.ODF6D.voxels_avg_R_vectors;

    rec_size = [size(dmvol_rec, 1), size(dmvol_rec, 2), size(dmvol_rec, 3)];
    theo_size = [size(dmvol_theo, 1), size(dmvol_theo, 2), size(dmvol_theo, 3)];
    offset = floor((rec_size - theo_size) / 2);

    voxel_theo_pos = voxel_rec_pos - offset;
    if (any([voxel_theo_pos < 1, voxel_theo_pos > theo_size]))
        error('gt6DAnalyseVoxelProperties:wrong_argument', ...
            'Requested a point outside the theoretical volume: [%s], while the theo volume is between [%s] and [%s]', ...
            sprintf(' %d', voxel_rec_pos), sprintf(' %d', offset), sprintf(' %d', offset+theo_size-1))
    end

    avg_R_vec_rec = reshape(dmvol_rec(voxel_rec_pos(1), voxel_rec_pos(2), voxel_rec_pos(3), :), 1, []);
    avg_R_vec_theo = reshape(dmvol_theo(voxel_theo_pos(1), voxel_theo_pos(2), voxel_theo_pos(3), :), 1, []);
    dist_avg_R_vec = post_result.distance_deg_alt(voxel_theo_pos(1), voxel_theo_pos(2), voxel_theo_pos(3));
    fprintf('Distance of reconstructed from real (at: [%s]): %f (deg)\n- Reconstructed: [%g %g %g]\n- Theoretical:   [%g %g %g]\n- Volume shift:  [%d %d %d]\n', ...
        sprintf(' %d', voxel_theo_pos), dist_avg_R_vec, avg_R_vec_rec, avg_R_vec_theo, offset)

    num_orients = numel(result.grains);
    intensities_recon = zeros(num_orients, 1);
    for ii_o = 1:num_orients
        intensities_recon(ii_o) = result.solution{ii_o}(voxel_rec_pos(1), voxel_rec_pos(2), voxel_rec_pos(3));
    end

    if (~exist('threshold_min', 'var') || isempty(threshold_min))
        threshold_min = 0;
    end

    if (~exist('threshold_max', 'var') || isempty(threshold_max))
        sorted_intensities = sort(intensities_recon, 'descend');
        index_thresh = min(numel(sorted_intensities), 4);
        threshold_max = sorted_intensities(index_thresh);
        threshold_max = threshold_max - eps('single');
        threshold_max = max(threshold_max, 0);
    end

    if (exist('contribution_fraction', 'var') && ~isempty(contribution_fraction))
        sorted_intensities = sort(intensities_recon, 'descend');
        cum_sum_ints = cumsum(sorted_intensities);
        cum_sum_ints = cum_sum_ints ./ sum(intensities_recon);

        contribution_fraction = max(min(contribution_fraction, 1), 0);

        last_ind = find(cum_sum_ints <= contribution_fraction, 1, 'last');
        threshold_min = max(sorted_intensities(last_ind), threshold_min);
    end

    gr = [result.grains{:}];

    lower_lims = min(test_data.gv.dm, [], 2)';
    upper_lims = max(test_data.gv.dm, [], 2)';
    region_size = upper_lims - lower_lims;

    cmap = parula();
    f = figure('Name', sprintf('Orientation-space at: (%d, %d, %d), intensity: %g, distance: %g deg', ...
        voxel_rec_pos, sum(intensities_recon), dist_avg_R_vec), ...
        'Colormap', cmap);
    ax = axes('parent', f);

    set(f, 'Units', 'centimeters')
    set(ax, 'Units', 'centimeters')

    set(f, 'Position', [0 0 25 13.5])
    set(f, 'Paperposition', [0 0 25 13.5])

    set(ax, 'Position', [2.25 1 16.25 12])
    scatter3(ax, [], [], []);

    hold(ax, 'on');

    show_bbox(ax, test_data.gv.dm', show_base_grid);

%     % Show all the lattice
%     scatter3(ax, sampled_R_vecs(:, 1), sampled_R_vecs(:, 2), sampled_R_vecs(:, 3), 30, 'm');

    kam_dmvol = result.ODF6D.kernel_average_misorientation;
    show_theoretical_orientation(ax, voxel_theo_pos, avg_R_vec_theo, kam_dmvol);
    show_average_orientation(ax, avg_R_vec_rec, avg_R_vec_theo);

    sampled_R_vecs = cat(1, gr.R_vector);
%     show_orientations_plain_main(ax1, sampled_R_vecs, intensities_recon, threshold_min, threshold_max);
    show_orientation_colormap(ax, sampled_R_vecs, intensities_recon, cmap, threshold_min);

%     quiver3(ax1, lower_lims(1), lower_lims(2), lower_lims(3), region_size(1)/5, 0, 0, 'color', [1 0 0], 'LineWidth', 3)
%     quiver3(ax1, lower_lims(1), lower_lims(2), lower_lims(3), 0, region_size(2)/5, 0, 'color', [0 1 0], 'LineWidth', 3)
%     quiver3(ax1, lower_lims(1), lower_lims(2), lower_lims(3), 0, 0, region_size(3)/5, 'color', [0 0 1], 'LineWidth', 3)

    padding = region_size * 0.05;
    lower_lims = lower_lims - padding;
    upper_lims = upper_lims + padding;

    set(ax, 'FontSize', 18)
    set(ax, 'XLim', [lower_lims(1), upper_lims(1)])
    set(ax, 'YLim', [lower_lims(2), upper_lims(2)])
    set(ax, 'ZLim', [lower_lims(3), upper_lims(3)])

    hold(ax, 'off');
end

function show_theoretical_orientation(ax, voxel_theo_pos, voxel_theo_Rvec, kam)
    scatter3(ax, voxel_theo_Rvec(1), voxel_theo_Rvec(2), voxel_theo_Rvec(3), 30, 'm', 'filled');

    if (~isempty(kam))
        dev_r_vec = tand(kam(voxel_theo_pos(1), voxel_theo_pos(2), voxel_theo_pos(3)) / 2);
        min_sampled_R_vecs = voxel_theo_Rvec - dev_r_vec;
        max_sampled_R_vecs = voxel_theo_Rvec + dev_r_vec;
        bbox_R_vecs = [ ...
            min_sampled_R_vecs(1), min_sampled_R_vecs(2), min_sampled_R_vecs(3); ...
            min_sampled_R_vecs(1), min_sampled_R_vecs(2), max_sampled_R_vecs(3); ...
            min_sampled_R_vecs(1), max_sampled_R_vecs(2), min_sampled_R_vecs(3); ...
            min_sampled_R_vecs(1), max_sampled_R_vecs(2), max_sampled_R_vecs(3); ...

            max_sampled_R_vecs(1), min_sampled_R_vecs(2), min_sampled_R_vecs(3); ...
            max_sampled_R_vecs(1), min_sampled_R_vecs(2), max_sampled_R_vecs(3); ...
            max_sampled_R_vecs(1), max_sampled_R_vecs(2), min_sampled_R_vecs(3); ...
            max_sampled_R_vecs(1), max_sampled_R_vecs(2), max_sampled_R_vecs(3); ...
            ];
        faces = [ ...
            1 5; 2 6; 3 7; 4 8; ...
            1 3; 2 4; 5 7; 6 8; ...
            1 2; 3 4; 5 6; 7 8; ...
            ];

        patch('parent', ax, 'Faces', faces, 'Vertices', bbox_R_vecs, 'FaceColor', 'w', 'EdgeColor', 'm');
    end
end

function show_average_orientation(ax, avg_R_vec, theo_R_vec)
    scatter3(ax, avg_R_vec(1), avg_R_vec(2), avg_R_vec(3), 30, [0 0 0], 'filled');

    quiv_length = avg_R_vec - theo_R_vec;
    quiver3(ax, theo_R_vec(1), theo_R_vec(2), theo_R_vec(3), quiv_length(1), quiv_length(2), quiv_length(3))
end

function show_orientations_plain_main(ax, sampled_R_vecs, intensities, thr_min, thr_max)
    indexes_recon_min = intensities > thr_min;
    indexes_recon_max = intensities > thr_max;

    voxel_R_vecs = sampled_R_vecs(indexes_recon_min, :);
%     voxel_intensities = intensities(indexes_recon_min);

    main_voxel_R_vecs = sampled_R_vecs(indexes_recon_max, :);

    scatter3(ax, voxel_R_vecs(:, 1), voxel_R_vecs(:, 2), voxel_R_vecs(:, 3), 30, 'b');
    scatter3(ax, main_voxel_R_vecs(:, 1), main_voxel_R_vecs(:, 2), main_voxel_R_vecs(:, 3), 30, 'g', 'filled');
end

function show_orientation_colormap(ax, sampled_R_vecs, intensities, cmap, thr_min)
    indexes_recon_min = intensities > thr_min;
    voxel_R_vecs = sampled_R_vecs(indexes_recon_min, :);
    voxel_intensities = intensities(indexes_recon_min);

    min_int = min(voxel_intensities);
    max_int = max(voxel_intensities);

    % Rescaling
    voxel_intensities = (voxel_intensities - min_int) / (max_int - min_int);
    voxel_intensities = voxel_intensities * (size(cmap, 1)-1) + 1;
    voxel_intensities = round(voxel_intensities);

    scatter3(ax, voxel_R_vecs(:, 1), voxel_R_vecs(:, 2), voxel_R_vecs(:, 3), 20, cmap(voxel_intensities, :), 'filled')
    
    cb = colorbar('peer', ax, 'YTickLabel', min_int:((max_int-min_int)/10):max_int, 'location', 'EastOutside', 'FontSize', 18);
    set(cb, 'Units', 'centimeters')
    set(cb, 'Position', [20.35 1 0.65 12])
end

function show_bbox(ax, sampled_R_vecs, base_grid)
    min_sampled_R_vecs = min(sampled_R_vecs, [], 1);
    max_sampled_R_vecs = max(sampled_R_vecs, [], 1);
    bbox_R_vecs = [ ...
        min_sampled_R_vecs(1), min_sampled_R_vecs(2), min_sampled_R_vecs(3); ...
        min_sampled_R_vecs(1), min_sampled_R_vecs(2), max_sampled_R_vecs(3); ...
        min_sampled_R_vecs(1), max_sampled_R_vecs(2), min_sampled_R_vecs(3); ...
        min_sampled_R_vecs(1), max_sampled_R_vecs(2), max_sampled_R_vecs(3); ...

        max_sampled_R_vecs(1), min_sampled_R_vecs(2), min_sampled_R_vecs(3); ...
        max_sampled_R_vecs(1), min_sampled_R_vecs(2), max_sampled_R_vecs(3); ...
        max_sampled_R_vecs(1), max_sampled_R_vecs(2), min_sampled_R_vecs(3); ...
        max_sampled_R_vecs(1), max_sampled_R_vecs(2), max_sampled_R_vecs(3); ...
        ];
    faces = [ ...
        1 5; 2 6; 3 7; 4 8; ...
        1 3; 2 4; 5 7; 6 8; ...
        1 2; 3 4; 5 6; 7 8; ...
        ];

    scatter3(ax, bbox_R_vecs(:, 1), bbox_R_vecs(:, 2), bbox_R_vecs(:, 3), 10, 'r', 'filled');
    patch('parent', ax, 'Faces', faces, 'Vertices', bbox_R_vecs, 'FaceColor', 'w');

    if (base_grid)
        scatter3(ax, sampled_R_vecs(:, 1), sampled_R_vecs(:, 2), sampled_R_vecs(:, 3), 20, 'r');
    end
end