function [proj_phase_mat, proj_mask] = gt6DMakeSurfaceFigureForPaper(phase_mat, plane, direction, phase_mask)

    if (~isfield(phase_mat, 'vol'))
        phase_mat.vol = phase_mat.grains;
    end

    switch (upper(plane))
        case 'XY'
            permutation = [1 2 3 4];
        case 'YX'
            permutation = [2 1 3 4];
        case 'YZ'
            permutation = [2 3 1 4];
        case 'ZY'
            permutation = [3 2 1 4];
        case 'XZ'
            permutation = [1 3 2 4];
        case 'ZX'
            permutation = [3 1 2 4];
    end

    proj_phase_mat.dmvol = permute(phase_mat.dmvol, permutation);
    proj_phase_mat.intvol = permute(phase_mat.intvol, permutation);
    proj_phase_mat.vol = permute(phase_mat.vol, permutation);

    if (exist('phase_mask', 'var') && ~isempty(phase_mask) && all(size(phase_mask) == size(phase_mat.vol)))
        phase_mask = permute(phase_mask, permutation);
        use_phase_mask = true;
    else
        use_phase_mask = false;
    end

    if (direction < 0)
        slice_nums = size(proj_phase_mat.dmvol, 3):-1:1;
    else
        slice_nums = 1:size(proj_phase_mat.dmvol, 3);
    end

    proj_mask = zeros(size(proj_phase_mat.vol, 1), size(proj_phase_mat.vol, 2));
    slice_dmvol = zeros([size(proj_phase_mat.vol), 3], 'like', proj_phase_mat.dmvol);
    slice_intvol = zeros(size(proj_phase_mat.vol), 'like', proj_phase_mat.intvol);
    slice_vol = zeros(size(proj_phase_mat.vol), 'like', proj_phase_mat.vol);
    if (use_phase_mask)
        for ii = slice_nums
            vol_id = proj_phase_mat.vol(:, :, ii);
            vol_dm = proj_phase_mat.dmvol(:, :, ii, :);
            vol_int = proj_phase_mat.intvol(:, :, ii);

            valid_voxels = (phase_mask(:, :, ii) > 0);
            proj_mask = proj_mask + valid_voxels;

            [vv_r, vv_c] = find(valid_voxels);
            pos_valid_vox = sub2ind(size(proj_phase_mat.vol), vv_r, vv_c, proj_mask(valid_voxels));
            pos_valid_vox_dm = cat(1, pos_valid_vox, ...
                pos_valid_vox + numel(proj_phase_mat.vol), ...
                pos_valid_vox + 2 * numel(proj_phase_mat.vol));

            dm_volid_voxels = cat(4, valid_voxels, valid_voxels, valid_voxels);
            slice_dmvol(pos_valid_vox_dm) = vol_dm(dm_volid_voxels);
            slice_intvol(pos_valid_vox) = vol_int(valid_voxels);
            slice_vol(pos_valid_vox) = vol_id(valid_voxels);
        end
    else
        for ii = slice_nums
            vol_id = proj_phase_mat.vol(:, :, ii);
            vol_dm = proj_phase_mat.dmvol(:, :, ii, :);
            vol_int = proj_phase_mat.intvol(:, :, ii);

            valid_voxels = (vol_id > 0);
            proj_mask = proj_mask + valid_voxels;

            [vv_r, vv_c] = find(valid_voxels);
            pos_valid_vox = sub2ind(size(proj_phase_mat.vol), vv_r, vv_c, proj_mask(valid_voxels));
            pos_valid_vox_dm = cat(1, pos_valid_vox, ...
                pos_valid_vox + numel(proj_phase_mat.vol), ...
                pos_valid_vox + 2 * numel(proj_phase_mat.vol));

            dm_volid_voxels = cat(4, valid_voxels, valid_voxels, valid_voxels);
            slice_dmvol(pos_valid_vox_dm) = vol_dm(dm_volid_voxels);
            slice_intvol(pos_valid_vox) = vol_int(valid_voxels);
            slice_vol(pos_valid_vox) = vol_id(valid_voxels);
        end
    end

    proj_phase_mat.dmvol = slice_dmvol;
    proj_phase_mat.intvol = slice_intvol;
    proj_phase_mat.vol = slice_vol;
end
