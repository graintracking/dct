function gt6DPlotNoiseBlobs(blobs, n_ext, n_white, b, s, compact)
    if (~exist('compact', 'var'))
        compact = false;
    end

    s_blob = squeeze(blobs{b}(:, s, :));
    s_ext = squeeze(n_ext{b}(:, s, :));
    s_white = squeeze(n_white{b}(:, s, :));

    blob_clims = [min(s_blob(:)), max(s_blob(:))];

    plot_slice(s_blob, blob_clims, compact);
    plot_slice(s_ext, [], compact);
    plot_slice(s_white, [], compact);
    plot_slice(s_blob + s_ext + s_white, blob_clims, compact);
end

function plot_slice(slice, clims, compact)
    f = figure('colormap', gray);
    ax = axes('parent', f);

    if (isempty(clims))
        clims = [min(slice(:)), max(slice(:))];
    end
    imagesc(slice, 'parent', ax, clims);
    cb = colorbar('peer', ax, 'location', 'EastOutside');

    set(f, 'Units', 'centimeters')
    set(ax, 'Units', 'centimeters')
    set(cb, 'Units', 'centimeters')

    if (compact)
        set(f, 'Position', [0 0 14.5 12.5])
        set(f, 'Paperposition', [0 0 14.5 12.5])

        set(cb, 'Position', [13.1 0.5 0.65 12])

        set(ax, 'Position', [0.75 0.5 12 12])
    else
        set(f, 'Position', [0 0 15.5 13.5])
        set(f, 'Paperposition', [0 0 15.5 13.5])

        set(cb, 'Position', [13.85 1 0.65 12])

        set(ax, 'Position', [1 1 12 12])
    end
end