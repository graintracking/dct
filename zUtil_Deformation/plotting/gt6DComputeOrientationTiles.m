function tiles = gt6DComputeOrientationTiles(dmvol, num_dim_tiles)
    if (numel(size(dmvol)) == 4)
        gvdm = gtDefDmvol2Gvdm(dmvol);
    else
        gvdm = dmvol;
    end
    extremes = [min(gvdm, [], 2); max(gvdm, [], 2)];

    tilesX = linspace(extremes(1), extremes(4), num_dim_tiles);
    tilesY = linspace(extremes(2), extremes(5), num_dim_tiles);
    tilesZ = linspace(extremes(3), extremes(6), num_dim_tiles);

    [tilesX, tilesY, tilesZ] = ndgrid(tilesX, tilesY, tilesZ);

    tiles = [tilesX(:), tilesY(:), tilesZ(:)];
end
