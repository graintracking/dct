function cfg = gtPlotDmvol(dmvol, varargin)
    cfg = struct('figure', {[]}, 'axes', {[]}, 'hold', {false}, 'color', {'b'}, 'size', {30}, 'skip', {1});

    if (nargin >= 2)
        if ((numel(varargin) == 1) && isstruct(varargin{1}))
            cfg = varargin{1};
        else
            cfg = parse_pv_pairs(cfg, varargin);
        end
    end

    if (isempty(cfg.figure) && isempty(cfg.axes))
        cfg.figure = figure();
        cfg.axes = axes('parent', cfg.figure);
    elseif (~isempty(cfg.figure))
        cfg.figure = figure(cfg.figure);
        if (isempty(cfg.axes))
            cfg.axes = axes('parent', cfg.figure);
        else
            try
                axes(cfg.axes);
                set(cfg.figure, 'CurrentAxes', cfg.axes)
            catch % If it returns an error, it's because there's no such axes, or it belongs to another figure
                cfg.axes = axes('parent', cfg.figure);
            end
        end
    end

    if (cfg.hold)
        previous_hold = ishold(cfg.axes);
        hold(cfg.axes, 'on');
    end

    [initialShape(1), initialShape(2), initialShape(3), initialShape(4)] = size(dmvol);

    if (initialShape(4) == 3)
        dmvol = permute(dmvol, [4 1 2 3]);
        dmvol = reshape(dmvol, [3 prod(initialShape(1:3))]);
    end

    scatter3(cfg.axes, dmvol(1, 1:cfg.skip:end), dmvol(2, 1:cfg.skip:end), dmvol(3, 1:cfg.skip:end), cfg.size, cfg.color);

    if (cfg.hold)
        hold(cfg.axes, previous_hold);
    end
end