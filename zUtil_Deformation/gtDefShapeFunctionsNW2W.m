function sfs_w = gtDefShapeFunctionsNW2W(sfs_nw)
% FUNCTION sfs_w = gtDefShapeFunctionsNW2W(sfs_nw)
%

    sfs_w = cell(size(sfs_nw));

    tot_orientations = numel(sfs_nw);
    for ii_o = 1:tot_orientations
        num_blobs = numel(sfs_nw{ii_o});

        sfs_w{ii_o} = gtFwdSimBlobDefinition('sf_w', num_blobs);

        for ii_b = 1:num_blobs
            sfs_w{ii_o}(ii_b).intm = reshape(sum(sfs_nw{ii_o}(ii_b).intm, 1), [], 1);
            sfs_w{ii_o}(ii_b).bbsize = sfs_nw{ii_o}(ii_b).bbsize(2);
        end
        [sfs_w{ii_o}.bbwim] = deal(sfs_nw{ii_o}.bbwim);
    end
end
