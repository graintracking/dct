classdef Gt6DAlgoBenchmarks < GtBenchmarks
    properties
    end

    methods (Access = public, Static)

        function benchmarkUpdateDualDetector(iterations, num_cells, base_num_elems)
            if (~exist('iterations', 'var') || isempty(iterations))
                iterations = 7;
            end
            if (~exist('num_cells', 'var') || isempty(num_cells))
                num_cells = 10;
            end
            if (~exist('base_num_elems', 'var') || isempty(base_num_elems))
                base_num_elems = 20;
            end

            p = Gt6DAlgoBenchmarks.build_cell(num_cells, base_num_elems, 1);
            b = Gt6DAlgoBenchmarks.build_cell(num_cells, base_num_elems, 1);
            cb = Gt6DAlgoBenchmarks.build_cell(num_cells, base_num_elems, 0.2);
            sigma1 = Gt6DAlgoBenchmarks.build_cell(num_cells, base_num_elems, 0.1);
            sigma1_1 = Gt6DAlgoBenchmarks.build_cell(num_cells, base_num_elems, 1.1);

            bytes = GtBenchmarks.getSizeVariable(p);
            GtBenchmarks.print_header('Compute Dual Detector Update', bytes)

            fprintf(' - Doing %d safe copies: ', iterations)
            copy_p = cell(1, iterations);
            for ii = 1:iterations
                copy_p{ii} = gtCxxMathsCellCopy(p);
            end
            fprintf(' Done.\n')

            fprintf(' - Doing %d iterations: ', iterations)
            c = tic();
            for ii = 1:iterations
                new_p_c = gt6DUpdateDualDetector(copy_p{ii}, b, cb, sigma1, sigma1_1, true);
            end
            GtBenchmarks.print_result('gt6DUpdateDualDetector_c', toc(c)/iterations, bytes);

            fprintf(' - Doing %d iterations: ', iterations)
            c = tic();
            for ii = 1:iterations
                new_p = gt6DUpdateDualDetector(p, b, cb, sigma1, sigma1_1, false);
            end
            GtBenchmarks.print_result('for loop                ', toc(c)/iterations, bytes);

            GtBenchmarks.check_errors(new_p, new_p_c);
        end

        function benchmarkUpdateDualL1(numIters, num_cells, base_num_elems)
            if (~exist('num_cells', 'var') || isempty(num_cells))
                num_cells = 10;
            end
            if (~exist('base_num_elems', 'var') || isempty(base_num_elems))
                base_num_elems = 20;
            end

            q = Gt6DAlgoBenchmarks.build_cell(num_cells, base_num_elems, 1);
            es = Gt6DAlgoBenchmarks.build_cell(num_cells, base_num_elems, 1);
            lambda = 1e-3;

            bytes = GtBenchmarks.getSizeVariable(q);
            GtBenchmarks.print_header('Compute Dual Detector Update', bytes)

            fprintf(' - Doing %d safe copies: ', numIters)
            copy_q = cell(1, numIters);
            for ii = 1:numIters
                copy_q{ii} = gtCxxMathsCellCopy(q);
            end
            fprintf(' Done.\n')

            fprintf(' - Doing %d iterations: ', numIters)
            c = tic();
            for ii = 1:numIters
                new_q_c = gt6DUpdateDualL1(copy_q{ii}, es, lambda, true);
            end
            GtBenchmarks.print_result('gt6DUpdateDualL1_c', toc(c)/numIters, bytes);

            fprintf(' - Doing %d iterations: ', numIters)
            c = tic();
            new_q = cell(size(q));
            for ii = 1:numIters
                new_q = gt6DUpdateDualL1(q, es, lambda, false);
            end
            GtBenchmarks.print_result('for loop          ', toc(c)/numIters, bytes);

            GtBenchmarks.check_errors(new_q, new_q_c);
        end

        function benchmarkUpdatePrimal(numIters, num_cells, base_num_elems)
            if (~exist('num_cells', 'var') || isempty(num_cells))
                num_cells = 50;
            end
            if (~exist('base_num_elems', 'var') || isempty(base_num_elems))
                base_num_elems = 50;
            end

            s = Gt6DAlgoBenchmarks.build_cell(num_cells, base_num_elems, 1);
            t = Gt6DAlgoBenchmarks.build_cell(num_cells, base_num_elems, 0.2);
            q = Gt6DAlgoBenchmarks.build_cell(num_cells, base_num_elems, 1);
            tau = Gt6DAlgoBenchmarks.build_cell(num_cells, 1, 0.1);

            bytes = GtBenchmarks.getSizeVariable(s);
            GtBenchmarks.print_header('Compute Primal Update', bytes)
            fprintf(' - Doing %d iterations: ', numIters)
            c = tic();
            new_s_c = cell(size(s));
            new_es_c = cell(size(s));
            for ii = 1:numIters
                for n = 1:numel(s)
                    [new_s_c{n}, new_es_c{n}] = gt6DUpdatePrimal(s{n}, t{n}, q{n}, tau{n}, true);
                end
            end
            GtBenchmarks.print_result('gt6DUpdatePrimal_c', toc(c)/numIters, bytes);

            fprintf(' - Doing %d iterations: ', numIters)
            c = tic();
            new_s = cell(size(s));
            new_es = cell(size(s));
            for ii = 1:numIters
                for n = 1:numel(s)
                    [new_s{n}, new_es{n}] = gt6DUpdatePrimal(s{n}, t{n}, q{n}, tau{n}, false);
                end
            end
            GtBenchmarks.print_result('for loop          ', toc(c)/numIters, bytes);

            GtBenchmarks.check_errors(new_s, new_s_c);
            GtBenchmarks.check_errors(new_es, new_es_c);
        end
    end

    methods (Access = protected, Static)
        function var = build_cell(num_cells, base_num_elems, factor, type, func)
            if (~exist('type', 'var') || isempty(type))
                type = 'single';
            end
            if (~exist('func', 'var'))
                func = @rand;
%                 func = @ones;
            end

            var = cell(1, num_cells);
            for ii = 1:num_cells
                var{ii} = factor * func(base_num_elems([1 1 1]), type);
            end
        end
    end
end