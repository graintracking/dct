function EBSD_struct = gtEBSDLoadMapEulerTSVFile(filename, varargin)
% function [EBSD_e_map, EBSD_r_map] = gtEBSDLoadMapEulerTSVFile(filename, varargin)
%
% DEPRECATED, in favour of gtEBSDLoadMapEulerCTFFile (if possible!)
%
%   INPUT:
%       axes_perm       : [1:3]   - vector<1x3>
%       stretch_axes    : [1 1]   - vector<1x2>
%       axes_convention : {'a'} | 'b'
%       phase_id        : 1       - scalar
%       rotate_vecs     : [0 0 0] - vector<1x3>
%       permute_maps    : {false} - boolean
%
%   Axes permutation allows to easily deal with different reference
%   systems, like for instance:
%       DCT Axes <-> EBSD Axes ([x y z] <-> [x'y'z'])
%           x     =     -y'
%           y     =      z'
%           z     =     -x'
%   Translates to: axes_perm = [-2 3 -1]
%
%   NOTE: Only the Rodrigues vector map has the axes changed!
%

    conf = struct( ...
        'axes_perm', 1:3, ...
        'stretch_axes', [1 1], ...
        'phase_id', 1, ...
        'axes_convention', 'a', ...
        'rotate_vecs', [0 0 0], ...
        'permute_maps', false, ...
        'euler_in_degrees', true, ...
        'euler_convention', 'ZXZ' );
    conf = parse_pv_pairs(conf, varargin);

    c = tic();
    fprintf('Reading from file: "%s"..', filename);

    scan_string = {'%d', '%d', '%f', '%f', '%f', '%f', '%f', '%f', '%d', '%d', '%d', '%d'};
    scan_string(2, :) = {','};

    fid = fopen(filename);
    fgetl(fid);
    input = fscanf(fid, [scan_string{1:end-1}]);
    fclose(fid);

    fprintf('\b\b, Done in %f seconds.\n', toc(c));
    c = tic();
    fprintf('Converting to Euler and Rodriguez maps..');

    input = reshape(input, size(scan_string, 2), [])'; % <- permuted

    xx = input(:, 3);
    yy = input(:, 4);

    sizes(2) = numel(find(xx == 0));
    sizes(1) = numel(find(yy == 0));

    xx = reshape(xx, sizes);
    yy = reshape(yy, sizes);

    diffs_x = xx(2:end, :) - xx(1:end-1, :);
    diffs_y = yy(:, 2:end) - yy(:, 1:end-1);

    scales = [mean(diffs_x(:)), mean(diffs_y(:))];

    ea1 = input(:, 5);
    ea2 = input(:, 6);
    ea3 = input(:, 7);

    EBSD_e_map = cat(3, reshape(ea1, sizes), reshape(ea2, sizes), reshape(ea3, sizes));

    EBSD_mask = input(:, 2);
    EBSD_mask = reshape(EBSD_mask, sizes);

    if (~conf.euler_in_degrees)
        EBSD_e_map = EBSD_e_map * 180 / pi;
    end

    dmvol_EBSD_e_map = permute(EBSD_e_map, [1 2 4 3]);
    [gvdm_EBSD_e_map, size_dmvol_EBSD_map] = gtDefDmvol2Gvdm(dmvol_EBSD_e_map);
    switch (upper(conf.euler_convention))
        case 'ZXZ'
            gvdm_EBSD_r_map = gtMathsEuler2Rod(gvdm_EBSD_e_map);
        case 'XYZ'
            gvdm_EBSD_r_map = gtMathsOriMat2Rod(gtMathsEuler2OriMat(gvdm_EBSD_e_map, 'XYZ'));
    end

    fprintf('\b\b, Done in %f seconds.\n', toc(c));
    c = tic();
    fprintf('Applying reference transformations..');

    gvdm_EBSD_r_map = gvdm_EBSD_r_map(abs(conf.axes_perm), :);
    negative = conf.axes_perm < 0;
    gvdm_EBSD_r_map(negative, :) = - gvdm_EBSD_r_map(negative, :);

    if (conf.axes_convention == 'b')
        conv_rotate_vecs = [0 0 tand(30/2)]';
        gvdm_EBSD_r_map = gtMathsRodSum(gvdm_EBSD_r_map, conv_rotate_vecs);
    end
    if (any(conf.rotate_vecs ~= [0 0 0]))
        gvdm_EBSD_r_map = gtMathsRodSum(conf.rotate_vecs', gvdm_EBSD_r_map);
    end

    fprintf('\b\b, Done in %f seconds.\n', toc(c));
    c = tic();
    fprintf('Mapping Rodrigues vectors to fundamental zone..');

    p = gtLoadParameters();
    cryst_system = p.cryst(conf.phase_id).crystal_system;
    cryst_spacegroup = p.cryst(conf.phase_id).spacegroup;
    symm = gtCrystGetSymmetryOperators(cryst_system, cryst_spacegroup);

    gvdm_EBSD_r_map = gtMathsRod2RodInFundZone(gvdm_EBSD_r_map, symm);
    dmvol_EBSD_r_map = gtDefGvdm2Dmvol(gvdm_EBSD_r_map, size_dmvol_EBSD_map);
    EBSD_r_map = reshape(dmvol_EBSD_r_map, [sizes 3]);

    fprintf('\b\b, Done in %f seconds.\n', toc(c));
    c = tic();
    fprintf('Applying requested transformations..');

    EBSD_mask_inds = find(~EBSD_mask);
    EBSD_mask_inds = cat(1, EBSD_mask_inds, ...
        EBSD_mask_inds + numel(EBSD_mask), ...
        EBSD_mask_inds + 2 * numel(EBSD_mask));
    EBSD_r_map(EBSD_mask_inds) = 0;

    if (any(conf.stretch_axes ~= 1))
        EBSD_e_map = imresize(EBSD_e_map, 'nearest', 'scale', conf.stretch_axes);
        EBSD_r_map = imresize(EBSD_r_map, 'nearest', 'scale', conf.stretch_axes);
        EBSD_mask = imresize(EBSD_mask, 'nearest', 'scale', conf.stretch_axes);
    end

    EBSD_mask = logical(EBSD_mask);

    if (conf.permute_maps)
        EBSD_e_map = permute(EBSD_e_map, [2 1 3]);
        EBSD_r_map = permute(EBSD_r_map, [2 1 3]);
        EBSD_mask = EBSD_mask';
    end

    EBSD_struct = conf;
    EBSD_struct.map_e = EBSD_e_map;
    EBSD_struct.map_r = EBSD_r_map;
    EBSD_struct.mask = EBSD_mask;
    EBSD_struct.mad = [];
    EBSD_struct.bc = [];
    EBSD_struct.pixel_size = reshape(scales, 1, []);

    fprintf('\b\b, Done in %f seconds.\n', toc(c));
end
