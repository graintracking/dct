function orients = gt6DCalculateFullSpaceCorners(estim_space_bbox_mm, bbox_size_mm, estim_orient_bbox, bbox_size_rod, refgr, p, det_ind)
    if (~exist('det_ind', 'var') || isempty(det_ind))
        det_ind = 1;
    end

    orients = cell(2, 2, 2, 2, 2, 2);
    for ii_x1 = 0:1
        base_x1 = estim_space_bbox_mm(1:3) + [ii_x1 * bbox_size_mm(1), 0, 0];
        for ii_x2 = 0:1
            base_x2 = base_x1 + [0, ii_x2 * bbox_size_mm(2), 0];
            for ii_x3 = 0:1
                base_x3 = base_x2 + [0, 0, ii_x3 * bbox_size_mm(3)];
                for ii_r1 = 0:1
                    base_r1 = estim_orient_bbox(1:3) + [ii_r1 * bbox_size_rod(1), 0, 0];
                    for ii_r2 = 0:1
                        base_r2 = base_r1 + [0, ii_r2 * bbox_size_rod(2), 0];
                        for ii_r3 = 0:1
                            base_r3 = base_r2 + [0, 0, ii_r3 * bbox_size_rod(3)];

%                             fprintf(' center: %f, %f, %f, r_vec: %f, %f, %f\n', base_x3, base_r3)

                            orients{ii_r3+1, ii_r2+1, ii_r1+1, ii_x3+1, ii_x2+1, ii_x1+1} = struct( ...
                                'id', refgr.id, 'phaseid', refgr.phaseid, ...
                                'center', base_x3, 'R_vector', base_r3);
                        end
                    end
                end
            end
        end
    end

    orients = gtCalculateGrain_p(orients, p, 'ref_omind', refgr.allblobs(det_ind).omind, 'det_ind', det_ind);
    orients = [orients{:}];
end