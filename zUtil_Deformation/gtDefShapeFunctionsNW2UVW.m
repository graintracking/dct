function sfs_uvw = gtDefShapeFunctionsNW2UVW(sampler, sfs_nw, varargin)
% FUNCTION sfs_uvw = gtDefShapeFunctionsNW2UVW(sampler, sfs_nw, varargin)
%

    conf = struct( ...
        'recenter_sf', true, ...
        'data_type', [], ...
        'rspace_oversampling', 1, ...
        'rspace_voxel_size', [1 1 1] );
    conf = parse_pv_pairs(conf, varargin);

    det_ind = sampler.detector_index;

    p = sampler.parameters;
    labgeo = p.labgeo;
    samgeo = p.samgeo;
    detgeo = p.detgeo(det_ind);

    ref_gr = sampler.get_reference_grain();
    ors = sampler.get_orientations();

    ref_sel = sampler.selected;

    om_step = gtAcqGetOmegaStep(p, det_ind);

    tot_orientations = numel(sfs_nw);
    if (numel(ors) ~= tot_orientations)
        error('gtDefShapeFunctionsNW2UVW:wrong_argument', ...
            'Mismatching number of orientations and shape functions passed')
    end

    centers = compute_centers(ref_gr.center, conf.rspace_voxel_size, conf.rspace_oversampling);
    tot_centers = size(centers, 1);

    sfs_uvw = cell(tot_orientations, 1);

    rotcomp_w = gtMathsRotationMatrixComp(labgeo.rotdir, 'row');

    fprintf('Rasterizing NW shape functions to UVW: ')
    c = tic();
    chunk_update = 25;
    for ii_o = 1:tot_orientations
        if (mod(ii_o, chunk_update) == 1)
            currtime = toc(c);
            num_chars = fprintf('%05d/%05d (%g s, ETA %g s)', ...
                ii_o, tot_orientations, currtime, ...
                currtime / (ii_o - 1) * tot_orientations - currtime);
        end

        sfs_in = sfs_nw{ii_o};
        tot_blobs = numel(sfs_in);

        lims_n = cat(1, sfs_in(:).bbnim);
        lims_w = cat(1, sfs_in(:).bbwim);
        blobs_nw_sizes = cat(1, sfs_in(:).bbsize);

        or_uv = ors{ii_o}.allblobs(det_ind).detector.uvw(ref_sel, 1:2);
        if (conf.recenter_sf)
            or_uv_shift = or_uv;
        else
            or_uv_shift = round(or_uv);
        end

        %%% Here we have to put all the etas and omegas from all the blobs
        %%% in two long vectors to vectorize their usage
        inds_n = [ [0; cumsum(blobs_nw_sizes(1:end-1, 1))]+1, cumsum(blobs_nw_sizes(:, 1)) ];
        inds_w = [ [0; cumsum(blobs_nw_sizes(1:end-1, 2))]+1, cumsum(blobs_nw_sizes(:, 2)) ];

        tot_ns = sum(blobs_nw_sizes(:, 1));
        tot_ws = sum(blobs_nw_sizes(:, 2));
        all_ns = zeros(tot_ns, 1);
        inds_blob_all_ns = zeros(tot_ns, 1);
        all_ws = zeros(tot_ws, 1);

        for ii_b = 1:tot_blobs
            eta_step = (lims_n(ii_b, 2) - lims_n(ii_b, 1)) / (blobs_nw_sizes(ii_b, 1) - 1);
            all_ns(inds_n(ii_b, 1):inds_n(ii_b, 2)) = lims_n(ii_b, 1):eta_step:lims_n(ii_b, 2);

            inds_blob_all_ns(inds_n(ii_b, 1):inds_n(ii_b, 2)) = ii_b;

            all_ws(inds_w(ii_b, 1):inds_w(ii_b, 2)) = (lims_w(ii_b, 1):lims_w(ii_b, 2)) .* om_step;
        end

        t = ors{ii_o}.allblobs(det_ind).theta(ref_sel);

        % Calculating all the diffraction vectors (plane normals) in lab
        % coordinates for al the etas in each blob
        pl_lab_all = gtGeoPlLabFromThetaEta(t(inds_blob_all_ns), all_ns, labgeo);

        % Computing the resulting scattering vectors in lab coordinates
        dveclab_all = gtFedPredictDiffVecMultiple(pl_lab_all', labgeo.beamdir');

        % Normalize dvecs
        dvec_norms_all = sqrt(sum(dveclab_all .* dveclab_all, 1));
        dveclab_all = dveclab_all ./ dvec_norms_all([1 1 1], :);

        % Calculating all the sample center positions at each omega inside
        % the blobs
        rot_s2l_all = gtMathsRotationTensor(all_ws, rotcomp_w);
        gclab_v_all = gtGeoSam2Lab(centers, rot_s2l_all, labgeo, samgeo, false, 'element_wise', false);
        gclab_v_all = reshape(gclab_v_all', 3, 1, tot_ws, []);

        %%% Here we put the scattering vectors and center positions
        %%% together
        uv = cell(tot_blobs, 1);
        uv_min = zeros(tot_blobs, 2);
        uv_max = zeros(tot_blobs, 2);

        blobs_nw_area_sizes = prod(blobs_nw_sizes, 2) * tot_centers;

        chunk = 16;
        for ii_base_b = 0:chunk:tot_blobs-1
            chunk_size = min(chunk, tot_blobs-ii_base_b);

            blobs_nw_area_sizes_chunk = blobs_nw_area_sizes((ii_base_b+1):(ii_base_b+chunk_size));

            inds_chunk = [ [0; cumsum(blobs_nw_area_sizes_chunk(1:end-1))]+1, cumsum(blobs_nw_area_sizes_chunk) ];

            tot_nws_chunk = sum(blobs_nw_area_sizes_chunk);
            dveclab_chunk = zeros(3, tot_nws_chunk);
            gclab_v_chunk = zeros(3, tot_nws_chunk);

            for ii_var_b = 1:chunk_size
                ii_b = ii_base_b + ii_var_b;

                ones_n = ones(blobs_nw_sizes(ii_b, 1), 1);
                ones_w = ones(blobs_nw_sizes(ii_b, 2) * tot_centers, 1);

                inds_bl_chunk = inds_chunk(ii_var_b, 1):inds_chunk(ii_var_b, 2);

                dveclab = dveclab_all(:, inds_n(ii_b, 1):inds_n(ii_b, 2));
                dveclab = dveclab(:, :, ones_w);
                dveclab_chunk(:, inds_bl_chunk) = reshape(dveclab, 3, []);

                gclab_v = gclab_v_all(:, :, inds_w(ii_b, 1):inds_w(ii_b, 2), :);
                gclab_v = reshape(gclab_v, size(gclab_v, 1), size(gclab_v, 2), []);
                gclab_v = gclab_v(:, ones_n, :);
                gclab_v_chunk(:, inds_bl_chunk) = reshape(gclab_v, 3, []);
            end

            uv_chunk = gtFedPredictUVMultiple([], dveclab_chunk, gclab_v_chunk, ...
                detgeo.detrefpos', detgeo.detnorm', detgeo.Qdet, ...
                [detgeo.detrefu, detgeo.detrefv]')';

            for ii_var_b = 1:chunk_size
                ii_b = ii_base_b + ii_var_b;

                inds_bl_chunk = inds_chunk(ii_var_b, 1):inds_chunk(ii_var_b, 2);

                inds_bl = ii_b * ones(blobs_nw_area_sizes(ii_b), 1);
                uv{ii_b} = uv_chunk(inds_bl_chunk, :) - or_uv_shift(inds_bl, :);

                uv_min(ii_b, :) = min(uv{ii_b}, [], 1);
                uv_max(ii_b, :) = max(uv{ii_b}, [], 1);
            end
        end

        uv_blob_size = max(abs(min(uv_min, [], 1)), abs(max(uv_max, [], 1)));
        uv_blob_size = 2 * ceil(uv_blob_size) + 1;

        uv_shift = (uv_blob_size - 1) / 2 + 1;

        sfs_out = gtFwdSimBlobDefinition('sf_uvw', tot_blobs);

        for ii_b = 1:tot_blobs
            ws = 1:blobs_nw_sizes(ii_b, 2);
            ws = ws(ones(blobs_nw_sizes(ii_b, 1), 1), :);
            ws = reshape(ws, [], 1);
            ws = reshape(ws(:, ones(tot_centers, 1)), [], 1);

            ones_nw = ones(blobs_nw_area_sizes(ii_b), 1);

            uv_bl = uv{ii_b} + uv_shift(ones_nw, :);
            ints = reshape(sfs_in(ii_b).intm, [], 1);

            if (isempty(conf.data_type))
                uv_bl = cast(uv_bl, 'like', sfs_in(ii_b).intm);
            else
                uv_bl = cast(uv_bl, conf.data_type);
                ints = cast(ints, conf.data_type);
            end

            ints = ints / tot_centers;
            ints = reshape(ints(:, ones(tot_centers, 1)), [], 1);

            [inds4, ints4] = gtMathsGetInterpolationIndices(uv_bl, ints);
            inds4(:, 3) = [ws; ws; ws; ws];

            uvw_blob_size = [uv_blob_size, blobs_nw_sizes(ii_b, 2)];
            sfs_out(ii_b).intm = accumarray(inds4, ints4, uvw_blob_size);

            sfs_out(ii_b).bbuim = or_uv_shift(ii_b, [1 1]) + [-1 1] * (uv_shift(1) - 1);
            sfs_out(ii_b).bbvim = or_uv_shift(ii_b, [2 2]) + [-1 1] * (uv_shift(2) - 1);
            sfs_out(ii_b).bbwim = lims_w(ii_b, :);

            sfs_out(ii_b).bbsize = uvw_blob_size;

            sfs_out(ii_b).bbuim = or_uv_shift(ii_b, [1 1]) + [-1 1] * (uv_shift(1) - 1);
            sfs_out(ii_b).bbvim = or_uv_shift(ii_b, [2 2]) + [-1 1] * (uv_shift(2) - 1);
            sfs_out(ii_b).bbwim = lims_w(ii_b, :);

            sfs_out(ii_b).bbsize = uvw_blob_size;

            uproj = sum(sum(abs(sfs_out(ii_b).intm), 2), 3);
            vproj = sum(sum(abs(sfs_out(ii_b).intm), 1), 3);
            wproj = sum(sum(abs(sfs_out(ii_b).intm), 1), 2);

            sfs_out(ii_b).mbbu = [find(uproj, 1, 'first'), find(uproj, 1, 'last')] + sfs_out(ii_b).bbuim(1) - 1;
            sfs_out(ii_b).mbbv = [find(vproj, 1, 'first'), find(vproj, 1, 'last')] + sfs_out(ii_b).bbvim(1) - 1;
            sfs_out(ii_b).mbbw = [find(wproj, 1, 'first'), find(wproj, 1, 'last')] + sfs_out(ii_b).bbwim(1) - 1;

            sfs_out(ii_b).mbbsize = [ ...
                sfs_out(ii_b).mbbu(2) - sfs_out(ii_b).mbbu(1) + 1, ...
                sfs_out(ii_b).mbbv(2) - sfs_out(ii_b).mbbv(1) + 1, ...
                sfs_out(ii_b).mbbw(2) - sfs_out(ii_b).mbbw(1) + 1 ];
        end

        sfs_uvw{ii_o} = sfs_out;

        if (mod(ii_o, chunk_update) == 1)
            fprintf(repmat('\b', [1, num_chars]));
        end
    end
    fprintf(repmat(' ', [1, num_chars]));
    fprintf(repmat('\b', [1, num_chars]));
    fprintf('Done in %g seconds.\n', toc(c))
end

function centers = compute_centers(ref_center, rspace_voxel_size, rspace_oversampling)
    if (numel(rspace_oversampling) == 1)
        rspace_oversampling = rspace_oversampling([1 1 1]);
    end
    max_dists_from_center = rspace_voxel_size .* (1 - 1 ./ rspace_oversampling) / 2;
    tot_voxels = prod(rspace_oversampling);

    centers = zeros(tot_voxels, 3);

    counter = 1;

    for ss_z = linspace(-max_dists_from_center(3), max_dists_from_center(3), rspace_oversampling(3))
        for ss_y = linspace(-max_dists_from_center(2), max_dists_from_center(2), rspace_oversampling(2))
            for ss_x = linspace(-max_dists_from_center(1), max_dists_from_center(1), rspace_oversampling(1))

                centers(counter, :) = ref_center + [ss_x, ss_y, ss_z];

                counter = counter + 1;
            end
        end
    end
end

