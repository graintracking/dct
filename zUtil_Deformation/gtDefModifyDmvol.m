function [dmvol_5t_jumps, dmvol_5times, dmvol_jumps] = gtDefModifyDmvol(dmvol)
    [initialShape(1), initialShape(2), initialShape(3), initialShape(4)] = size(dmvol);

    dmvol = permute(dmvol, [4 1 2 3]);
    dmvol = reshape(dmvol, [3 prod(initialShape(1:3))]);

    [dmvol, center] = get_center(dmvol);
    center
    max(dmvol, [], 2)
    min(dmvol, [], 2)

    gtGetMaxDisorientation(dmvol, [], 'minimal');

    normDvol1 = sqrt(sum(dmvol .^ 2, 1));
    normDvol1 = normDvol1 + (normDvol1 == 0);
    dmvol1Norm = dmvol ./ normDvol1([1 1 1], :);
    angles = atand(normDvol1);
    angles = 5 * angles;
    max(angles)
    min(angles)
    normDvol1 = tand(angles);
    dmvol_5times = dmvol1Norm .* normDvol1([1 1 1], :);

%     figure, scatter3(dmvol(1, :), dmvol(2, :), dmvol(3, :))
%     hold on
%     scatter3(dmvol1(1, :), dmvol1(2, :), dmvol1(3, :), 30, 'r')
%     hold off
%     mean(dmvol, 2)'
%     mean(dmvol1, 2)'
%     std(dmvol, 0, 2)'
%     std(dmvol1, 0, 2)'

    gtGetMaxDisorientation(dmvol_5times, [], 'minimal');

    dmvol_5t_jumps = dmvol_5times;

    [xx, yy, zz] = ndgrid(1:initialShape(1), 1:initialShape(2), 1:initialShape(3));
    valid1 = (xx .* 2 + yy .* 3 + zz .* 0.25 - initialShape(1)/2) < initialShape(1);
    valid2 = ((xx .* 2 - yy .* 3 + zz .* 0.5 + initialShape(1)/2)) > initialShape(1)/2 & ~ valid1;
    valid3 = ((xx .* 2 - yy .* 3 - zz .* 0.75 + initialShape(1)/2)) < -initialShape(1) & ~ (valid1 | valid2);

    valid1 = permute(valid1(:, :, :, [1 1 1]), [4 1 2 3]);
    valid2 = permute(valid2(:, :, :, [1 1 1]), [4 1 2 3]);
    valid3 = permute(valid3(:, :, :, [1 1 1]), [4 1 2 3]);

    valid1 = reshape(valid1, [3 size(dmvol_5t_jumps, 2)]);
    valid2 = reshape(valid2, [3 size(dmvol_5t_jumps, 2)]);
    valid3 = reshape(valid3, [3 size(dmvol_5t_jumps, 2)]);

    displacement = randn(3, 3);
    normDisplacement = sqrt(sum(displacement .^ 2, 1));
    renormDisplacement = tand(0.5) ./ normDisplacement;
    displacement = displacement .* renormDisplacement([1 1 1], :);

    dmvol_5t_jumps = dmvol_5t_jumps + valid1 .* displacement(:, 1 * ones(size(dmvol_5t_jumps, 2), 1));
    dmvol_5t_jumps = dmvol_5t_jumps + valid2 .* displacement(:, 2 * ones(size(dmvol_5t_jumps, 2), 1));
    dmvol_5t_jumps = dmvol_5t_jumps + valid3 .* displacement(:, 3 * ones(size(dmvol_5t_jumps, 2), 1));

    [dmvol_5t_jumps, center2] = get_center(dmvol_5t_jumps);
    max_spread_2 = gtGetMaxDisorientation(dmvol_5t_jumps, [], 'minimal');
    dmvol_5t_jumps = dmvol_5t_jumps .* 5 ./ max_spread_2;
    [dmvol_5t_jumps, center2] = get_center(dmvol_5t_jumps);
    gtGetMaxDisorientation(dmvol_5t_jumps, [], 'minimal');

    dmvol_jumps = dmvol_5t_jumps ./ 5;

    figure, scatter3(dmvol(1, 1:10:end), dmvol(2, 1:10:end), dmvol(3, 1:10:end))
    hold on
    scatter3(dmvol_5times(1, 1:10:end), dmvol_5times(2, 1:10:end), dmvol_5times(3, 1:10:end), 30, 'r')
    scatter3(dmvol_5t_jumps(1, 1:10:end), dmvol_5t_jumps(2, 1:10:end), dmvol_5t_jumps(3, 1:10:end), 40, 'c')
    hold off

    dmvol_5times = reshape(dmvol_5times, [3 initialShape(1:3)]);
    dmvol_5times = permute(dmvol_5times, [2 3 4 1]);

    dmvol_5t_jumps = reshape(dmvol_5t_jumps, [3 initialShape(1:3)]);
    dmvol_5t_jumps = permute(dmvol_5t_jumps, [2 3 4 1]);

    dmvol_jumps = reshape(dmvol_jumps, [3 initialShape(1:3)]);
    dmvol_jumps = permute(dmvol_jumps, [2 3 4 1]);
end

function [dmvol, center] = get_center(dmvol)
    center = mean(dmvol, 2);
    dmvol = dmvol - center(:, ones(size(dmvol, 2), 1));
end

function [dmvol] = set_center(dmvol, center)
    dmvol = dmvol + center(:, ones(size(dmvol, 2), 1));
end


