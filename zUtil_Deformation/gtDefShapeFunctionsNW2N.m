function sfs_n = gtDefShapeFunctionsNW2N(sfs_nw)
% FUNCTION sfs_n = gtDefShapeFunctionsNW2N(sfs_nw)
%

    sfs_n = cell(size(sfs_nw));

    tot_orientations = numel(sfs_nw);
    for ii_o = 1:tot_orientations
        num_blobs = numel(sfs_nw{ii_o});

        sfs_n{ii_o} = gtFwdSimBlobDefinition('sf_n', num_blobs);

        for ii_b = 1:num_blobs
            sfs_n{ii_o}(ii_b).intm = sum(sfs_nw{ii_o}(ii_b).intm, 2);
            sfs_n{ii_o}(ii_b).bbsize = sfs_nw{ii_o}(ii_b).bbsize(1);
        end
        [sfs_n{ii_o}.bbwim] = deal(sfs_nw{ii_o}.bbnim);
    end
end
