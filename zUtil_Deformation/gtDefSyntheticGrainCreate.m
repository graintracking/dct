function [grain, gv] = gtDefSyntheticGrainCreate(fedpars, grain, parameters, dmvol, intvol, det_ind)
% [grain, gv] = gtDefSyntheticGrainCreate(fedpars, grain, parameters, dmvol, intvol)
%
% Sets up the framework, variables, random strain distribution for simulated data.
%  In the grain volume everything is defined in voxels, no dimensioning with pixel size.
%  In the projections and diff. blobs, everything is defined in detector pixels
%  (here comes the pixel size and setup geometry into play).
%    Thus the dimensioning/scaling happens in the forward projection
%    functions and their derivative functions, since they relate the
%    grain voxels to the detector pixels.
%
% INPUT
%   fedpars    - FED parameters
%                Some important ones:
%                  grainsize   - no. of voxels in grain
%                  grainenv    = grainsize
%                  ngv         = grainsize
%                  gvsize      = [1 1 1]
%                  loadbl      - blobs ID-s in grain.allblobs to be generated
%                  dcomps      - for rotations only use [1 1 1 0 0 0 0 0 0]
%                  blobsizeadd - blob volume padding
%                  vox000sam   - (X, Y, Z) coordinates of voxel (0, 0, 0) in the Sample reference frame
%   grain      - grain data as output from Indexter
%   parameters - parameters as in DCT paramaters file
%   dmvol      - all deformation components per voxel for the entire volume
%                (nx, ny, nz, ndef); if empty, random distribution is applied
%                with max limits as in fedpars.dapplim and max gradients
%                as in fedpars.dappgradlim
%   intvol     - Volume instensities

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Load and update geometry parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if (~ismember(fedpars.gvtype, {'single', 'double'}))
    warning([mfilename ':wrong_argument'], 'Unsupported "fedpars.gvtype" = %s. Setting to "single".', fedpars.gvtype)
    fedpars.gvtype = 'single';
end

fprintf('Setting voxels up..')
c = tic();

labgeo = parameters.labgeo;
labgeo.omstep = gtAcqGetOmegaStep(parameters);
% Rotation matrix components:
labgeo.rotcomp = gtMathsRotationMatrixComp(labgeo.rotdir', 'col');

samgeo = parameters.samgeo;
if (~isfield(parameters, 'detgeo') || isempty(parameters.detgeo))
    [parameters.detgeo, labgeo] = gtGeoConvertLegacyLabgeo2Detgeo(labgeo);
end
detgeo = parameters.detgeo;
% Update labgeo
parameters.labgeo = labgeo;

num_dets = numel(detgeo);
if (~exist('det_ind', 'var') || isempty(det_ind))
    det_ind = 1:num_dets;
end

for ii_d = det_ind
    det_pix_size = mean([detgeo(ii_d).pixelsizeu, detgeo(ii_d).pixelsizev]);
    if (any(parameters.recgeo(ii_d).voxsize ~= det_pix_size))
        warning('gtDefSyntheticGrainCreate:inconsistent_voxsize', ...
            'The recgeo(%d).voxsize is inconsistent with the detgeo(%d).pixelsize[u,v]', ...
            ii_d, ii_d)
    end
    parameters.recgeo(ii_d).voxsize = det_pix_size([1 1 1]);

    % Updating acq
    parameters.acq(ii_d).dist = detgeo(ii_d).detrefpos(1);
    parameters.acq(ii_d).pixelsize = det_pix_size;
end
recgeo = parameters.recgeo;

if (isfield(parameters, 'diffractometer'))
    for ii_d = 1:numel(parameters.diffractometer)
        diff = parameters.diffractometer(ii_d);
        if (any(size(diff.axes_rotation) ~= size(labgeo.rotdir)) || ...
                any(diff.axes_rotation ~= labgeo.rotdir))
            warning('gtDefSyntheticGrainCreate:inconsistent_rotation_dir', ...
                'The diffractometer rotation axis differs from the labgeo rotation axis')
            disp(diff.axes_rotation)
            disp(labgeo.rotdir)
            parameters.diffractometer(ii_d).axes_rotation = labgeo.rotdir;
        end
    end
end

recgeo_ph = recgeo(1);
if (isfield(fedpars, 'voxsize') && numel(fedpars.voxsize) == 3)
    recgeo_ph.voxsize = fedpars.voxsize;
end

if (~isfield(parameters.fsim, 'oversizeVol') ...
        || isempty(parameters.fsim.oversizeVol))
    parameters.fsim.oversizeVol = 1.2;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Load grain parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if (~exist('dmvol', 'var') || isempty(dmvol))
    gr_size = fedpars.grainsize; % x, y, z number of voxels in grain
else
    gr_size = [size(dmvol, 1), size(dmvol, 2), size(dmvol, 3)];
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Volume sampling and volume elements
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fprintf('\b\b: Done in %g seconds.\nAllocating memory for elements..', toc(c))
c = tic();

% total number of vol elements
nv = gr_size(1) * gr_size(2) * gr_size(3);
nv_ones = ones(nv, 1);

nd = sum(fedpars.dcomps);

% Element XYZ index
[gv.ind(:, 1), gv.ind(:, 2), gv.ind(:, 3)] = ind2sub(gr_size, 1:nv);
gv.ind = gv.ind';

% Element diffracting power
if (~exist('intvol', 'var') || isempty(intvol))
    intvol = ones(gr_size, fedpars.gvtype);
end

if (numel(intvol) ~= nv)
    error('gtFedTestLoadData_v2:wrong_argument', ...
        'The volume of voxel intensities should have the same size as dmvol''s first 3 coordinates')
end
gv.pow = reshape(intvol, 1, []);
if (strcmp(fedpars.gvtype, 'single'))
    gv.pow = single(gv.pow);
end

gv.used_ind = find(gv.pow > 0);

% Element actual strain state
gv.d = zeros(nd, nv, fedpars.gvtype);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Apply strain distribution to grain
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fprintf('\b\b: Done in %g seconds.\n', toc(c))

disp('Active deformation components:')
disp(fedpars.dcomps)

fprintf('Applying random deformation..')
c = tic();

% Measured (true, real) strain distribution:
if (~exist('dmvol', 'var') || isempty(dmvol))
    [gvdm, dmvol, gradok] = gtFedTestGenerateRandomDef(fedpars, nd);
else
    [gvdm, dmvol_size] = gtDefDmvol2Gvdm(dmvol);
    if (strcmpi(fedpars.defmethod, 'small'))
        if (dmvol_size(4) == 3)
            mean_gvdm = mean(gvdm, 2);
            fprintf('\b\b (Recentering GVDM and DMVOL to the mean orientation: (%f, %f, %f), from: (%f, %f, %f))..', ...
                grain.R_vector, mean_gvdm)
%             gvdm_shift = grain.R_vector' - mean_gvdm;
            gvdm_shift = 0 - mean_gvdm;
            gvdm = gvdm + gvdm_shift(:, ones(size(gvdm, 2), 1));
            dmvol = gtDefGvdm2Dmvol(gvdm, dmvol_size);
        end
    end
    gradok = [];
end
gv.dm = gvdm;

fprintf('\b\b: Done in %g seconds.\nComputing voxel centers..', toc(c))
c = tic();

phantom_voxsize = recgeo_ph.voxsize';
vox000sam = grain.center' - (gr_size' / 2) .* phantom_voxsize;

% Elements' positions in the sample volume in mm!
gv.cs = vox000sam(:, nv_ones) + (gv.ind - 0.5) .* phantom_voxsize(:, nv_ones);

% Producing centers and deformation components for projection
if (isfield(fedpars, 'volume_super_sampling') ...
        && (fedpars.volume_super_sampling > 1))
    if (~isfield(fedpars, 'volume_sampling_method') || isempty(fedpars.volume_sampling_method))
        fedpars.volume_sampling_method = 'linear';
    end
    [gv.pcs, gv.d, gv.pow] = compute_oversampling_voxels(gv, dmvol, ...
        fedpars.volume_super_sampling, fedpars.gvtype, fedpars.volume_sampling_method);
else
    gv.pcs = gv.cs;
    gv.d = gv.dm;
end

if (strcmp(fedpars.gvtype, 'single'))
    fprintf('\b\b: Done in %g seconds.\nConverting type to single..', toc(c))
    c = tic();
    fn = fieldnames(gv);
    for ii = 1:length(fn)
        gv.(fn{ii}) = single(gv.(fn{ii}));
    end
end

fprintf('\b\b: Done in %g seconds.\n', toc(c))

if (~all(gradok))
    disp('Gradients are higher than the limits for components:')
    disp(find(~gradok)')
end

disp('Largest deformation component:')
disp(max(dmvol(:)))
disp('Smallest deformation component:')
disp(min(dmvol(:)))

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Computing allblobs info
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Cleaning grain struct
if (~isfield(grain, 'id') || isempty(grain.id))
    grain.id = 1;
end
grain = struct('id', grain.id, 'phaseid', grain.phaseid, ...
    'center', grain.center, 'R_vector', grain.R_vector, ...
    'allblobs', grain.allblobs );

grain = gtCalculateGrain(grain, parameters, 'det_ind', det_ind);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Iinitial blob parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

grain.proj = gtFwdSimProjDefinition([], num_dets);

for n = det_ind
    if (ismember(parameters.acq(n).type, {'180degree', '360degree', 'dct'}))
        % Checking if we should just project all of them, or wether the user
        % has something to say abou it.
        if (isfield(fedpars, 'detector') ...
                && isfield(fedpars.detector(n), 'ondet') ...
                && ~isempty(fedpars.detector(n).ondet) )
            ondet = fedpars.detector(n).ondet;
            included = fedpars.detector(n).included;
            selected = fedpars.detector(n).selected;
        else
            ondet = find(grain.allblobs(n).detector.ondet);
            included = (1:numel(ondet))';
            included = included(abs(cosd(grain.allblobs(n).eta(ondet))) <= 0.7);
            selected = true(size(included));
        end

        ref_inc = ondet(included);
        nbl = numel(ref_inc);

        fprintf( ['Generating blob data (for detector: %d, blobs on the' ...
            ' detector: %d):\n'], n, nbl);

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%% Simulate "measured" diffraction patterns
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        fprintf(' - Simulating true ("measured") blob intensities..')
        c = tic();
        bl = gtDefFwdProjGvdm2UVW(grain, ref_inc, gv, fedpars, parameters, n);
    elseif (strcmpi(parameters.acq(n).type, 'topotomo'))

        lims_basetilt = parameters.acq(n).range_basetilt;
        within_lims = grain.allblobs(n).phi > lims_basetilt(1) & grain.allblobs(n).phi < lims_basetilt(2);
        phinds = grain.allblobs(n).phind(within_lims);
        phinds_counts = histc(phinds, (1:5)-0.5);
        [~, phind] = max(phinds_counts);

        selectedph = grain.allblobs(n).phind == phind;

        nbl = numel(grain.allblobs(n).omega) / 4;
        fprintf( ['Generating blob data (for detector: %d, blobs on the' ...
            ' detector: %d):\n'], n, nbl);

        if (isfield(fedpars, 'detector') ...
                && isfield(fedpars.detector(n), 'ondet') ...
                && ~isempty(fedpars.detector(n).ondet) )
            ondet = fedpars.detector(n).ondet;
            included = fedpars.detector(n).included;
            selected = fedpars.detector(n).selected;
        else
            ondet = find(selectedph);
            included = (1:nbl)';
            selected = true(size(ondet));
        end

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%% Simulate "measured" diffraction patterns
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        fprintf(' - Simulating true ("measured") blob intensities..')
        c = tic();
        bl = gtDefFwdProjGvdm2UVP(grain, selectedph, gv, fedpars, parameters, n);
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%% Assembling the final proj structure
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    fprintf('\b\b (%f seconds)\n - Producing data-structure..\n', toc(c))
    c = tic();

    stack = arrayfun(@(x){sum(x.intm, 3)}, bl);
    stack = cat(3, stack{:});

    vol_size = gtGeoSam2Sam(gr_size, recgeo_ph, recgeo(n), 1);
    vol_size = ceil(vol_size * parameters.fsim.oversizeVol);
    vol_size(3) = max(vol_size(3), 8);

    proj = gtFwdSimProjDefinition();

    proj.stack = permute(stack, [1 3 2]);
    proj.num_cols = size(proj.stack, 1);
    proj.num_rows = size(proj.stack, 3);
    proj.bl = bl;
    proj.vol_size_x = vol_size(2);
    proj.vol_size_y = vol_size(1);
    proj.vol_size_z = vol_size(3);
    proj.centerpix = gtGeoSam2Sam(grain.center, samgeo, recgeo(n), 0);
    proj.ondet = ondet;
    proj.included = included;
    proj.selected = selected;

    grain.proj(n) = proj;

    fprintf('\b\b(%f seconds)\n - True blob dimensions:\n', toc(c))
    fprintf(' %3d) %4d %4d %4d\n', [(1:nbl)', vertcat(bl(:).mbbsize)]')
end

end

function [gvpcs, gvd, gvpow] = compute_oversampling_voxels(gv, dmvol, vssampling, gvtype, method)
    nv = size(gv.ind, 2);

    num_subvoxels = vssampling ^ 3;
    gvpcs = zeros(3, nv, num_subvoxels, gvtype);
    gvd = zeros(3, nv, num_subvoxels, gvtype);

    dmvol_size = [size(dmvol, 1), size(dmvol, 2), size(dmvol, 3)];
    gvcs = reshape(gv.cs', [dmvol_size, 3]);

    fprintf('\b\b (super-sampling = %d):\n- Centers and dmvol: ', vssampling)

    for ii_c = 1:3
        num_chars = fprintf('%03d/%03d', ii_c, 3);

        gvpcs(ii_c, :, :) = gtMathsExpandVolume(gvcs(:, :, :, ii_c), vssampling, [], 'is_spatial_coords', true, 'permute_output', true);
        gvd(ii_c, :, :) = gtMathsExpandVolume(dmvol(:, :, :, ii_c), vssampling, [], 'method', method, 'permute_output', true);

        fprintf(repmat('\b', [1 num_chars]));
    end

    fprintf('\n- Normalizing R-vectors magnitude..')

    r_vec_magn = sqrt(sum(dmvol .^ 2, 4));
    r_vec_magn = gtMathsExpandVolume(r_vec_magn, vssampling, [], 'method', method, 'permute_output', true);

    r_vec_expand_magn = sqrt(sum(gvd .^ 2, 1));
    r_vec_expand_magn = r_vec_expand_magn + (r_vec_expand_magn < eps('single'));

    renorm_r_vec_magn = r_vec_magn ./ r_vec_expand_magn;
    gvd = bsxfun(@times, gvd, renorm_r_vec_magn);

    fprintf('\b\b: Done.\n- Intensity..')

    gvpow = reshape(gv.pow, dmvol_size);
    gvpow = gtMathsExpandVolume(gvpow, vssampling, [], 'method', method, 'padding', @zeros, 'permute_output', true);

    fprintf('\b\b: Done.\n- Total..')
end


