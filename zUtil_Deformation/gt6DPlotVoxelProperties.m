function gt6DPlotVoxelProperties(orientation_vols, ODF6D, voxel)
    voxel_avg_R_vec = squeeze(ODF6D.voxels_avg_R_vectors(voxel(1), voxel(2), voxel(3), :));

    intensities_recon = cellfun(@(x)x(voxel(1), voxel(2), voxel(3)), orientation_vols);

    sampled_R_vecs = cat(1, ODF6D.R_vectors{:});

    f = figure();
    plot_for_figure = true;
    if (~plot_for_figure)
        ax2 = subplot(1, 4, 1, 'parent', f);
        plot(ax2, intensities_recon);

        ax1 = subplot(1, 4, [2 3 4], 'parent', f);
    else
        ax1 = axes('parent', f);

        set(f, 'Units', 'centimeters')
        set(ax1, 'Units', 'centimeters')

        set(f, 'Position', [0 0 20 14])
        set(f, 'Paperposition', [0 0 20 14])

        set(ax1, 'Position', [1.25 1 16.25 12])
    end
    scatter3(ax1, [], [], []);

    hold(ax1, 'on');

    gt6DPlotOrientationBBox(ax1, sampled_R_vecs);

    show_average_orientation(ax1, voxel_avg_R_vec);

    show_orientation_colormap(ax1, sampled_R_vecs, intensities_recon, jet(), 0);

    hold(ax1, 'off');
end

function show_average_orientation(ax, avg_R_vec)
    scatter3(ax, avg_R_vec(1), avg_R_vec(2), avg_R_vec(3), 30, [0 0 0], 'filled');
end

function show_orientation_colormap(ax, sampled_R_vecs, intensities, cmap, thr_min)
%     cmap = [flipud(cmap); 0 0 0];

    indexes_recon_min = intensities > thr_min;
    voxel_R_vecs = sampled_R_vecs(indexes_recon_min, :);
    voxel_intensities = intensities(indexes_recon_min);

    min_int = min(voxel_intensities);
    max_int = max(voxel_intensities);

    % Rescaling
    voxel_intensities = (voxel_intensities - min_int) / (max_int - min_int);
    voxel_intensities = voxel_intensities * (size(cmap, 1)-1) + 1;
    voxel_intensities = round(voxel_intensities);

    scatter3(ax, voxel_R_vecs(:, 1), voxel_R_vecs(:, 2), voxel_R_vecs(:, 3), 20, cmap(voxel_intensities, :), 'filled')
    cb = colorbar('peer', ax, 'YTickLabel', min_int:((max_int-min_int)/10):max_int, 'location', 'EastOutside');
    set(cb, 'Units', 'centimeters')
    set(cb, 'Position', [17.35 1 0.65 12])
end
