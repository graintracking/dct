function [gr_center_pix, vol_size_pix, bbox_pix] = gt6DMergeRealSpaceVolumes(grs, det_index, oversize)
%     num_ors = numel(grs);
%     vol_sizes = zeros(num_ors, 3);
%     vol_centers = zeros(num_ors, 3);
%     for ii = 1:num_ors
%         vol_sizes(ii, :) = [ ...
%             grs(ii).proj(det_index).vol_size_y, ...
%             grs(ii).proj(det_index).vol_size_x, ...
%             grs(ii).proj(det_index).vol_size_z ];
%         vol_centers(ii, :) = grs(ii).proj(det_index).centerpix;
%     end

    projs = arrayfun(@(x){x.proj(det_index)}, grs);
    projs = [projs{:}];

    vol_sizes = [cat(1, projs.vol_size_y), cat(1, projs.vol_size_x), cat(1, projs.vol_size_z)];
    vol_centers = cat(1, projs.centerpix);

    vol_half_sizes = vol_sizes / 2;

    min_corners = vol_centers - vol_half_sizes;
    max_corners = vol_centers + vol_half_sizes;

    full_vol_min_corners = min(min_corners, [], 1);
    full_vol_max_corners = max(max_corners, [], 1);

    gr_center_pix = (full_vol_max_corners + full_vol_min_corners) / 2;

    vol_size_pix = full_vol_max_corners - full_vol_min_corners;

    if (exist('oversize', 'var'))
        vol_size_pix = vol_size_pix .* oversize;
    end

    vol_size_pix = ceil(vol_size_pix);

    vol_half_size_pix = vol_size_pix / 2;
    bbox_pix = [floor(gr_center_pix - vol_half_size_pix), ceil(gr_center_pix + vol_half_size_pix)];
end
