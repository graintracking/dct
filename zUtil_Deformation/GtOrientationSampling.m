classdef GtOrientationSampling < handle
    properties
        R_vectors;
        lattice = GtOrientationSampling.get_sampling_grid_definition();
        sampling_size = [0, 0, 0];
        sampling_res = [0, 0, 0];
        sampling_box = [0, 0, 0, 0, 0, 0];
        sampling_axes = eye(3);

        lattice_ss = [];
        ss_factor = [1 1 1];

        stats;

        parameters = [];

        ref_gr = [];
        ref_gr_topo = [];

        % redundant info that helps in forward compatibility
        bl;
        ondet;
        included;
        selected;

        detector_index = 1;

        verbose = false;
    end

    methods
        function self = GtOrientationSampling(parameters, ref_gr, varargin)
            self.parameters = parameters;
            self.ref_gr = ref_gr;

            self = parse_pv_pairs(self, varargin);

            self.ondet = ref_gr.proj(self.detector_index).ondet;
            self.included = ref_gr.proj(self.detector_index).included;
            self.selected = ref_gr.proj(self.detector_index).selected;
        end

         function make_grid_numpoints(self, min_edge_points, gvdm, even, varargin)
            if (~exist('gvdm', 'var') || isempty(gvdm))
                gvdm = self.guess_ODF_BB();
            end

            conf = struct( ...
                'oversize', 1, ...
                'det_ind', self.detector_index);
            [conf, ~] = parse_pv_pairs(conf, varargin);

            if (exist('even', 'var') && ~isempty(even) && even)
                [~, ~, dranges] = self.get_deformation_stats(gvdm);

                [~, min_ii] = min(dranges);
                other_iis = [1:min_ii-1 min_ii+1:3];
                min_spacing = dranges(min_ii) .* conf.oversize ./ min_edge_points;
                other_edge_points = ceil(dranges(other_iis) .* conf.oversize ./ min_spacing);
                edge_points(min_ii) = min_edge_points;
                edge_points(other_iis(1)) = other_edge_points(1);
                edge_points(other_iis(2)) = other_edge_points(2);
            elseif (numel(min_edge_points) == 1)
                edge_points = min_edge_points([1 1 1]);
            else
                edge_points = min_edge_points;
            end

            self.make_grid(edge_points, gvdm, varargin{:})
         end

        function make_grid_resolution(self, resolution_deg, max_num_points, gvdm, varargin)
            if (~exist('gvdm', 'var') || isempty(gvdm))
                gvdm = self.guess_ODF_BB();
            end
            [~, ~, dranges] = self.get_deformation_stats(gvdm);

            conf = struct( ...
                'oversize', 1, ...
                'det_ind', self.detector_index);
            [conf, ~] = parse_pv_pairs(conf, varargin);

            bb_size_deg = 2 .* atand(dranges .* conf.oversize);
            edge_points = self.get_points_from_resolution(bb_size_deg, resolution_deg);
            edge_points = max(edge_points, 2);
            if (~isempty(max_num_points))
                edge_points = min(edge_points, max_num_points);
            end

            self.make_grid(edge_points, gvdm, varargin{:})
        end

        function make_grid(self, edge_points, gvdm, varargin)
            conf = struct( ...
                'oversize', 1, ...
                'samp_axes', [], ...
                'compute_geometry', true, ...
                'det_ind', self.detector_index);
            conf = parse_pv_pairs(conf, varargin);

            if (isempty(self.R_vectors))
                [dmean, dcenters, dranges] = self.get_deformation_stats(gvdm);

                self.create_R_vectors(edge_points, dcenters, dranges, conf.oversize, conf.samp_axes);

                if (self.verbose && usejava('jvm'))
                    self.plot_sampling(gvdm, dmean);
                end

                self.lattice.gr = cell(self.sampling_size);
                for ii = 1:numel(self.lattice.gr)
                    self.lattice.gr{ii} = struct( ...
                        'R_vector', self.R_vectors(ii, :), ...
                        'center', self.ref_gr.center, ...
                        'phaseid', self.ref_gr.phaseid, ...
                        'allblobs', self.ref_gr.allblobs(1) );
                    % Could be a problem, if anything doesn't check for the
                    % detector index
                end
            end

            mode = self.parameters.acq(conf.det_ind).type;
            if (ismember(mode, {'180degree', '360degree'}))
                mode = 'dct';
            end

            self.lattice.types{conf.det_ind} = mode;

            self.print_sampling_details(conf.det_ind)

            if (conf.compute_geometry)
                self.produce_geometry(conf.det_ind);

                if (self.verbose && usejava('jvm'))
                    self.compute_blob_coverage_simple_grid(conf.det_ind);
                    self.plot_blob_coverage();
                end
            end
        end

        function make_undeformed_sampling(self, varargin)
            conf = struct( ...
                'det_ind', self.detector_index);
            conf = parse_pv_pairs(conf, varargin);

            if (isempty(self.R_vectors))
                self.create_R_vectors([1 1 1], self.ref_gr.R_vector, [0 0 0], 1);

                self.lattice.gr = { struct( ...
                    'R_vector', self.R_vectors, ...
                    'center', self.ref_gr.center, ...
                    'phaseid', self.ref_gr.phaseid, ...
                    'allblobs', self.ref_gr.allblobs(1) ) };
            end

            mode = self.parameters.acq(conf.det_ind).type;
            if (ismember(mode, {'180degree', '360degree'}))
                mode = 'dct';
            end

            self.lattice.types{conf.det_ind} = mode;

            self.print_sampling_details(conf.det_ind)

            self.produce_geometry(conf.det_ind);

            if (self.verbose && usejava('jvm'))
                self.compute_blob_coverage_simple_grid(conf.det_ind);
                self.plot_blob_coverage();
            end
        end

        function make_supersampling_simple_grid(self, directions, factor, det_ind)
            if (~exist('det_ind', 'var') || isempty(det_ind))
                det_ind = self.detector_index;
            end

            self.ss_factor = [1 1 1];
            self.ss_factor(directions) = factor;

            orient_voxel_size = tand(self.sampling_res ./ 2);
            half_voxel_size = orient_voxel_size ./ 2;
            steps = orient_voxel_size ./ self.ss_factor;
            half_steps = steps ./ 2;

            size_or = size(self.lattice.gr);
            self.lattice_ss(det_ind).gr = cell(size_or);

            lower_lims = - half_voxel_size + half_steps;
            upper_lims = + half_voxel_size - half_steps;

            xx = linspace(lower_lims(1), upper_lims(1), self.ss_factor(1));
            yy = linspace(lower_lims(2), upper_lims(2), self.ss_factor(2));
            zz = linspace(lower_lims(3), upper_lims(3), self.ss_factor(3));

            [xx, yy, zz] = ndgrid(xx, yy, zz);
            diffs = cat(2, xx(:), yy(:), zz(:));

            for ii_g = 1:numel(self.lattice.gr)
                gr = self.lattice.gr{ii_g};
                r_vecs = bsxfun(@plus, gr.R_vector, diffs);

                gr_ss = cell(self.ss_factor);
                for ii_ss = 1:numel(gr_ss)
                    gr_ss{ii_ss} = struct( ...
                        'R_vector', {r_vecs(ii_ss, :)}, ...
                        'center', {gr.center}, ...
                        'phaseid', {gr.phaseid}, ...
                        'allblobs', {self.ref_gr.allblobs} );
                end
                self.lattice_ss(det_ind).gr{ii_g} = gr_ss;
            end

            if (self.verbose && usejava('jvm'))
                self.plot_supersampling();
            end

            self.produce_geometry_supersampling(det_ind);
        end

        function [or, or_ss] = get_orientations(self, det_ind)
            if (~exist('det_ind', 'var') || isempty(det_ind))
                det_ind = self.detector_index;
            end

            or = self.lattice.gr(:);
            for ii_or = 1:numel(or)
                or{ii_or}.allblobs = or{ii_or}.allblobs(det_ind);
            end

            if (~isempty(self.lattice_ss) && numel(self.lattice_ss) >= det_ind)
                or_ss = self.lattice_ss(det_ind).gr(:);
                for ii_or = 1:numel(or_ss)
                    for ii_sub_or = 1:numel(or_ss{ii_or})
                        or_ss{ii_or}{ii_sub_or}.allblobs = or_ss{ii_or}{ii_sub_or}.allblobs(det_ind);
                    end
                end
            else
                or_ss = {};
            end
        end

        function ref_gr = get_reference_grain(self)
            ref_gr = self.ref_gr;
        end

        function ref_gr = get_reference_grain_topotomo(self)
            ref_gr = self.ref_gr_topo;
        end

        function r_vectors = get_R_vectors(self)
            r_vectors = self.R_vectors;
        end

        function or_size = get_orientation_sampling_size(self)
            or_size = self.sampling_size;
        end

        function tot_orient = get_total_num_orientations(self)
            tot_orient = prod(self.sampling_size);
        end

        function [avg_R_vecs, avg_R_vecs_int, stddev_R_vecs] = getAverageOrientations(self, vols)
            [avg_R_vecs, avg_R_vecs_int, stddev_R_vecs] = ...
                GtOrientationSampling.computeAverageOrientations(self.get_orientations(), vols);
        end

        function avg_R_vec = getAverageOrientation(self, vols)
            avg_R_vec = GtOrientationSampling.computeAverageOrientation(self.get_orientations(), vols);
        end

        function odf = getODF(self, vols)
            odf = GtOrientationSampling.computeODF(self.get_orientations(), vols);
        end

        function [gvdm_tmp, verts, all_plane_normals] = guess_ODF_BB(self, use_corners, use_etas, det_ind, verbose)
            if (~exist('use_corners', 'var') || isempty(use_corners))
                use_corners = true;
            end
            if (~exist('use_etas', 'var') || isempty(use_etas))
                use_etas = true;
            end
            if (~exist('det_ind', 'var') || isempty(det_ind))
                det_ind = self.detector_index;
            end
            if (~exist('verbose', 'var') || isempty(verbose))
                verbose = false;
            end

            omega_step = gtAcqGetOmegaStep(self.parameters, det_ind);

            samgeo = self.parameters.samgeo;
            labgeo = self.parameters.labgeo;
            detgeo = self.parameters.detgeo(det_ind);

            ond = self.ref_gr.proj(det_ind).ondet;
            inc = self.ref_gr.proj(det_ind).included;
            sel = self.ref_gr.proj(det_ind).selected;

            ab_sel = ond(inc(sel));

            bls = self.ref_gr.proj(det_ind).bl(sel);
            g = gtMathsRod2OriMat(self.ref_gr.R_vector);

            num_refl = numel(ab_sel);

            if (~num_refl)
                error('GtOrientationSampling:guess_ODF_BB:wrong_argument', ...
                    'No reflections selected! So, no estimation can be performed!')
            end

            % Taking theoretical omegas instead of experimental ones!
            w = self.ref_gr.allblobs(det_ind).omega(ab_sel);
            n = self.ref_gr.allblobs(det_ind).eta(ab_sel);
            t = self.ref_gr.allblobs(det_ind).theta(ab_sel);

            gc_sam = self.ref_gr.center(ones(numel(w), 1), :);

            % can we determine the maximum eta spread?
            uv = self.ref_gr.allblobs(det_ind).detector.uvw(ab_sel, 1:2);
            gc_lab = gtGeoSam2Lab(gc_sam, w, labgeo, samgeo, false);

            % Computing the etas according to the corners of the blobs
            u_min_max = cat(1, bls(:).mbbu);
            v_min_max = cat(1, bls(:).mbbv);

            pos_corner_bl = [u_min_max(:, 1) - 0.5, v_min_max(:, 1) - 0.5];
            pos_corner_br = [u_min_max(:, 1) - 0.5, v_min_max(:, 2) + 0.5];
            pos_corner_tl = [u_min_max(:, 2) + 0.5, v_min_max(:, 1) - 0.5];
            pos_corner_tr = [u_min_max(:, 2) + 0.5, v_min_max(:, 2) + 0.5];

            diffvec_corner_bl = gtGeoDet2Lab(pos_corner_bl, detgeo, false) - gc_lab;
            diffvec_corner_br = gtGeoDet2Lab(pos_corner_br, detgeo, false) - gc_lab;
            diffvec_corner_tl = gtGeoDet2Lab(pos_corner_tl, detgeo, false) - gc_lab;
            diffvec_corner_tr = gtGeoDet2Lab(pos_corner_tr, detgeo, false) - gc_lab;

            n_bl = gtGeoEtaFromDiffVec(diffvec_corner_bl, labgeo);
            n_br = gtGeoEtaFromDiffVec(diffvec_corner_br, labgeo);
            n_tl = gtGeoEtaFromDiffVec(diffvec_corner_tl, labgeo);
            n_tr = gtGeoEtaFromDiffVec(diffvec_corner_tr, labgeo);

            n_m_corners = min([n_bl, n_br, n_tl, n_tr], [], 2);
            n_p_corners = max([n_bl, n_br, n_tl, n_tr], [], 2);

            % Computing the etas according to the size of the blob
            gc_det_pos = gtGeoGrainCenterSam2Det(self.ref_gr.center, w, self.parameters, det_ind);
            cosnc = abs(cosd(n)) >= 0.5;
            dn = zeros(size(n));
            bls_bbm = cat(1, bls(:).mbbsize);
            % 1st component is the horizontal, while the 2nd is the
            % vertical !!
            dn(cosnc) = bls_bbm(cosnc, 1) ./ abs(cosd(n(cosnc)));
            dn(~cosnc) = bls_bbm(~cosnc, 2) ./ abs(sind(n(~cosnc)));
            dn = dn ./ (2 * sqrt(sum((uv - gc_det_pos) .^ 2, 2)));
            dn = atand(dn);

            % Let's take a small deviation in eta, to find the plane that
            % determines the limits in orientation-space
            n_m_symm = n - dn;
            n_p_symm = n + dn;

            if (use_corners)
                n_m = n_m_corners;
                n_p = n_p_corners;
            else
                n_m = n_m_symm;
                n_p = n_p_symm;
            end

            % We retrieve the signed y (instead of the unsigned one):
            % y = self.ref_gr.allblobs(det_ind).plorig(ab_sel, :)';
            y0_exp_lab = gtGeoPlLabFromThetaEta(t, n, labgeo);
            y0_exp_sam = gtGeoLab2Sam(y0_exp_lab, w, labgeo, samgeo, true);
            y = y0_exp_sam';

            if (verbose)
                % Useful to validate the previously introduced flip of eta
                ominds = self.ref_gr.allblobs(det_ind).omind(ab_sel);
                ssp = ((ominds == 1) | (ominds == 2));
                ss12  = ssp - ~ssp;

                y_l = self.ref_gr.allblobs(det_ind).pllab(ab_sel, :);
                y_s = self.ref_gr.allblobs(det_ind).pl(ab_sel, :);
                y_o = self.ref_gr.allblobs(det_ind).plorig(ab_sel, :);

                disp('[y0_exp_lab, y_l, y0_exp_lab-y_l]')
                disp([y0_exp_lab, y_l, y0_exp_lab-y_l])
                disp('[y0_exp_sam, y_s, y0_exp_sam-y_s, ss12(:, [1 1 1]) .* y_o]')
                disp([y0_exp_sam, y_s, y0_exp_sam-y_s, ss12(:, [1 1 1]) .* y_o])
            end

            % Should be identical to pl_cry
            hhs = gtVectorLab2Cryst(y', g);

            ym_exp_lab = gtGeoPlLabFromThetaEta(t, n_m, labgeo);
            yp_exp_lab = gtGeoPlLabFromThetaEta(t, n_p, labgeo);

            % r0s related to the extreeme omegas
            r0ws_ex = zeros(2 * num_refl, 3);
            % r01s related to the extreeme omegas
            r0ns_ex = zeros(2 * num_refl, 3);
            % versors of r (classic) related to the extreeme omegas
            rcs_ex_w = zeros(2 * num_refl, 3);
            % versors of the perpendicular to the rs in the extreeme omegas
            % (~ parallel to eta)
            rns_ex_w = zeros(2 * num_refl, 3);
            % versors of the perpendicular to the rs in the extreeme etas
            % (~ parallel to onegas)
            rws_ex_w = zeros(2 * num_refl, 3);

            sub_ws_abs = cat(1, bls(:).mbbw);
            % sub_w_abs will contain the two bounding omegas for the given
            % reflection.
            sub_ws_abs = (sub_ws_abs + repmat([-0.5, 0.5], [num_refl, 1])) * omega_step;

            for ii_r = 1:num_refl
                sub_w_abs = sub_ws_abs(ii_r, :);

                % the y vectors for the central eta, at the bounding omegas
                y0_exp_sam = gtGeoLab2Sam(y0_exp_lab([ii_r ii_r], :), ...
                    sub_w_abs, labgeo, samgeo, true);
                % the y vectors for the lower eta, at the bounding omegas
                ym_exp_sam = gtGeoLab2Sam(ym_exp_lab([ii_r ii_r], :), ...
                    sub_w_abs, labgeo, samgeo, true);
                % the y vectors for the upper eta, at the bounding omegas
                yp_exp_sam = gtGeoLab2Sam(yp_exp_lab([ii_r ii_r], :), ...
                    sub_w_abs, labgeo, samgeo, true);

                hh = hhs(ii_r, :);

                norms_0c = (1 + hh * y0_exp_sam')';
                norms_0c = norms_0c(:, [1 1 1]);
                norms_0m = (1 + hh * ym_exp_sam')';
                norms_0m = norms_0m(:, [1 1 1]);
                norms_0p = (1 + hh * yp_exp_sam')';
                norms_0p = norms_0p(:, [1 1 1]);

                % r0 is defined = (h x y) / (1 + h . y)
                r0c = gtMathsCross(hh, y0_exp_sam) ./ norms_0c;
                r0m = gtMathsCross(hh, ym_exp_sam) ./ norms_0m;
                r0p = gtMathsCross(hh, yp_exp_sam) ./ norms_0p;

                % r is defined = r0 + t * (h + y) / (1 + h . y)
                r_dir_c = (hh([1 1], :) + y0_exp_sam) ./ norms_0c;
                r_dir_m = (hh([1 1], :) + ym_exp_sam) ./ norms_0m;
                r_dir_p = (hh([1 1], :) + yp_exp_sam) ./ norms_0p;

                r_dir_c = gtMathsNormalizeVectorsList(r_dir_c);
                r_dir_m = gtMathsNormalizeVectorsList(r_dir_m);
                r_dir_p = gtMathsNormalizeVectorsList(r_dir_p);

                % Translating r0s to the closest point to the average R
                % vector, on the line r
                dot_r0c_avgR = sum((self.ref_gr.R_vector([1 1], :) - r0c) .* r_dir_c, 2);
                dot_r0m_avgR = sum((self.ref_gr.R_vector([1 1], :) - r0m) .* r_dir_m, 2);
                dot_r0p_avgR = sum((self.ref_gr.R_vector([1 1], :) - r0p) .* r_dir_p, 2);

                r0c = r0c + r_dir_c .* dot_r0c_avgR(:, [1 1 1]);
                r0m = r0m + r_dir_m .* dot_r0m_avgR(:, [1 1 1]);
                r0p = r0p + r_dir_p .* dot_r0p_avgR(:, [1 1 1]);

                % Delimiters in w
                r0ws_ex(ii_r*2-1:ii_r*2, :) = r0c;
                % Delimiters in n
                r0ns_ex(ii_r*2-1:ii_r*2, :) = [sum(r0m, 1); sum(r0p, 1)] / 2;

                rcs_ex_w(ii_r*2-1:ii_r*2, :) = r_dir_c;

                % It will be used for recentering the r0w, on top of the
                % average point and get a colinear plane norm to the
                % distance of the plane to the average point
                r_dir_n = r0p - r0m;
                r_dir_n_norm = sqrt(sum(r_dir_n .^ 2, 2));
                rns_ex_w(ii_r*2-1:ii_r*2, :) = r_dir_n ./ r_dir_n_norm(:, [1 1 1]);
                % Same as the previous, but for the points r0n
                r_dir_w = [r0m(end, :) - r0m(1, :); r0p(end, :) - r0p(1, :)];
                r_dir_w_norm = sqrt(sum(r_dir_w .^ 2, 2));
                rws_ex_w(ii_r*2-1:ii_r*2, :) = r_dir_w ./ r_dir_w_norm(:, [1 1 1]);
            end

            % Let's find the closest point to the average R vector on the plane
            avg_R_vec_exp = self.ref_gr.R_vector(ones(size(r0ws_ex, 1), 1), :);
            trasl_ex_points_w = avg_R_vec_exp - r0ws_ex;
            trasl_ex_points_w = sum(trasl_ex_points_w .* rns_ex_w, 2) ./ sqrt(sum(rns_ex_w .^ 2, 2));
            trasl_ex_points_w = r0ws_ex + trasl_ex_points_w(:, [1 1 1]) .* rns_ex_w;

            if (use_etas)
                trasl_ex_points_n = avg_R_vec_exp - r0ns_ex;
                trasl_ex_points_n = sum(trasl_ex_points_n .* rws_ex_w, 2) ./ sqrt(sum(rws_ex_w .^ 2, 2));
                trasl_ex_points_n = r0ns_ex + trasl_ex_points_n(:, [1 1 1]) .* rws_ex_w;

                all_plane_normals = [ ...
                    trasl_ex_points_w - avg_R_vec_exp; ...
                    trasl_ex_points_n - avg_R_vec_exp ];
            else
                all_plane_normals = trasl_ex_points_w - avg_R_vec_exp;
            end

            % Computing the circumscribing polyhedron in Orientation-space
            verts = gtMathsGetPolyhedronVerticesFromPlaneNormals(all_plane_normals);
            verts = verts + self.ref_gr.R_vector(ones(size(verts, 1), 1), :);

            if (verbose)
                f = figure();
                ax = axes('parent', f);
                hold(ax, 'on')
                scatter3(ax, trasl_ex_points_w(:, 1), trasl_ex_points_w(:, 2), trasl_ex_points_w(:, 3), 'b')
                if (use_etas)
                    scatter3(ax, trasl_ex_points_n(:, 1), trasl_ex_points_n(:, 2), trasl_ex_points_n(:, 3), 'y')
                end
                scatter3(ax, verts(:, 1), verts(:, 2), verts(:, 3), 'r')
                hold(ax, 'off')
            end

%             bbox_r0s = [min(trasl_ex_points, [], 1), max(trasl_ex_points, [], 1)];
%             bbox_r0s = [bbox_r0s(1:3), bbox_r0s(4:6) - bbox_r0s(1:3)];
%             or_bbox = bbox_r0s;
            bbox_verts = [min(verts, [], 1), max(verts, [], 1)];
            bbox_verts = [bbox_verts(1:3), bbox_verts(4:6) - bbox_verts(1:3)];
            or_bbox = bbox_verts;

            gvdm_tmp = [];
            for cz = 0:1
                for cy = 0:1
                    for cx = 0:1
                        gvdm_tmp = [gvdm_tmp, (or_bbox(1:3)' + [cx; cy; cz] .* or_bbox(4:6)')]; %#ok<AGROW>
                    end
                end
            end
        end

        function resolution = estimate_maximum_resolution(self, det_ind)
            if (~exist('det_ind', 'var') || isempty(det_ind))
                det_ind = self.detector_index;
            end

            if (strcmpi(self.parameters.acq(det_ind).type, 'topotomo'))
                xy_resolution = gtAcqGetPhiStep(self.parameters, det_ind);
                z_resolution = Inf;
            else
                ond = self.ref_gr.proj(det_ind).ondet;
                inc = self.ref_gr.proj(det_ind).included;
                sel = self.ref_gr.proj(det_ind).selected;

                ab_sel = ond(inc(sel));

                n = self.ref_gr.allblobs(det_ind).eta(ab_sel);
                t = self.ref_gr.allblobs(det_ind).theta(ab_sel);

                detgeo = self.parameters.detgeo(det_ind);
                delta_pixel = abs(cosd(n)) .* detgeo.pixelsizeu + abs(sind(n)) .* detgeo.pixelsizev;
                xy_resolution = atand(delta_pixel ./ (norm(detgeo.detrefpos) .* tand(2 .* t)));
                xy_resolution = min(xy_resolution);

                z_resolution = gtAcqGetOmegaStep(self.parameters, det_ind);
            end

            resolution = [xy_resolution, xy_resolution, z_resolution];
        end

        function num_interp = suggest_num_interp(self, det_ind)
            if (~exist('det_ind', 'var') || isempty(det_ind))
                det_ind = self.detector_index;
            end

            % The factor 1.5 allows at least a minimum amount of crosstalk
            % between the orientations
            mode = self.parameters.acq(det_ind).type;
            if (strcmpi(mode, 'topotomo'))
                ph_step = gtAcqGetPhiStep(self.parameters, det_ind);
                xy_res = max(self.sampling_res(1:2));
                num_interp = max(xy_res / ph_step * 1.5, 1);
            else
                om_step = gtAcqGetOmegaStep(self.parameters, det_ind);
                z_res = self.sampling_res(3);
                num_interp = max(z_res / om_step * 1.5, 1); % / self.ss_factor(3)
            end
        end

        function num_interp = minimum_num_interp(self, det_ind)
            if (~exist('det_ind', 'var') || isempty(det_ind))
                det_ind = self.detector_index;
            end

            mode = self.parameters.acq(det_ind).type;
            if (strcmpi(mode, 'topotomo'))
                ph_step = gtAcqGetPhiStep(self.parameters, det_ind);
                xy_res = max(self.sampling_res(1:2));
                num_interp = max(xy_res / ph_step, 1);
            else
                om_step = gtAcqGetOmegaStep(self.parameters, det_ind);
                z_res = self.sampling_res(3);
                num_interp = max(z_res / om_step, 1); % / self.ss_factor(3)
            end
        end

        function sampler = regenerate_displaced(self, new_center, det_ind)
            if (~exist('det_ind', 'var') || isempty(det_ind))
                det_ind = self.detector_index;
            end

            detgeo = self.parameters.detgeo;
            labgeo = self.parameters.labgeo;
            samgeo = self.parameters.samgeo;

            sampler = self;

            % Let's update the reference grain
            sampler.ref_gr.center = new_center;

            rot_s2l = permute(sampler.ref_gr.allblobs(det_ind).srot, [2 1 3]);
            gcsam_v = new_center(ones(1, size(rot_s2l, 3)), :);
            gclab_v = gtGeoSam2Lab(gcsam_v, rot_s2l, labgeo, samgeo, false);

            sampler.ref_gr.allblobs(det_ind).detector.uvw(:, 1:2) = gtFedPredictUVMultiple( ...
                [], sampler.ref_gr.allblobs(det_ind).dvec', gclab_v', ...
                detgeo(det_ind).detrefpos', detgeo(det_ind).detnorm', detgeo(det_ind).Qdet, ...
                [detgeo(det_ind).detrefu, detgeo(det_ind).detrefv]')';

            % And now let's take care of the sampled orientations
            grid_gr = self.get_orientations();
            grid_gr = [grid_gr{:}];
            num_orients = numel(grid_gr);

            ab = [grid_gr(:).allblobs];

            rot_l2s = cat(3, ab(:).srot);
            rot_s2l = permute(rot_l2s, [2 1 3]);

            dveclab = cat(1, ab(:).dvec);

            gcsam_v = new_center(ones(1, size(rot_s2l, 3)), :);
            gclab_v = gtGeoSam2Lab(gcsam_v, rot_s2l, labgeo, samgeo, false);

            uv_tot = gtFedPredictUVMultiple([], dveclab', gclab_v', ...
                detgeo(det_ind).detrefpos', detgeo(det_ind).detnorm', detgeo(det_ind).Qdet, ...
                [detgeo(det_ind).detrefu, detgeo(det_ind).detrefv]');

            uv_tot = reshape(uv_tot, 2, [], num_orients);
            uv_tot = permute(uv_tot, [2 1 3]);

            for ii_g = 1:num_orients
                sampler.lattice.gr{ii_g}.center = new_center;

                sampler.lattice.gr{ii_g}.allblobs(det_ind).detector.uvw(:, 1:2) = uv_tot(:, :, ii_g);
            end
        end

        function [delta_omegas, proj_lims] = get_omega_deviations(self, with_shape_functions)
            if (~exist('with_shape_functions', 'var') || isempty(with_shape_functions))
                with_shape_functions = false;
            end
            if (with_shape_functions)
                half_rspace_sizes = self.sampling_res / 2;
                min_bounds = self.lattice(1).gr{1, 1, 1}.R_vector - half_rspace_sizes;
                max_bounds = self.lattice(1).gr{end, end, end}.R_vector + half_rspace_sizes;

                bounds_x = [min_bounds(1), max_bounds(1)];
                bounds_y = [min_bounds(2), max_bounds(2)];
                bounds_z = [min_bounds(3), max_bounds(3)];

                grs = cell(2, 2, 2);
                for dx = 1:2
                    for dy = 1:2
                        for dz = 1:2
                            r_vec = [bounds_x(dx), bounds_y(dy), bounds_z(dz)];
                            grs{dx, dy, dz} = struct( ...
                                'phaseid', self.ref_gr.phaseid, ...
                                'center', self.ref_gr.center, 'R_vector', r_vec );
                        end
                    end
                end

                grs = gtCalculateGrain_p(grs, self.parameters, ...
                    'ref_omind', self.ref_gr.allblobs(self.detector_index).omind );
                grs = [grs{:}];
            else
                grs = [self.lattice(1).gr{:}];
            end
            ond = self.ref_gr.proj(self.detector_index).ondet;
            inc = self.ref_gr.proj(self.detector_index).included;
            sel = self.ref_gr.proj(self.detector_index).selected;

            ab_sel = ond(inc(sel));

            ab = [grs(:).allblobs];
            w_tab = [ab(:).omega];
            w_tab = w_tab(ab_sel, :) / gtAcqGetOmegaStep(self.parameters, self.detector_index);

            ref_int_ws = self.ref_gr.allblobs(self.detector_index).detector.uvw(ab_sel, 3);

            w_tab = gtGrainAnglesTabularFix360deg(w_tab, ref_int_ws, self.parameters);

            max_ws = ceil(max(w_tab, [], 2));
            min_ws = floor(min(w_tab, [], 2));
            proj_lims = [min_ws, max_ws];
            delta_omegas = max_ws - min_ws + 1;
        end

        function [delta_uvs, proj_uv_lims] = get_uv_deviations(self)
            grs = [self.lattice(1).gr{:}];

            ond = self.ref_gr.proj(self.detector_index).ondet;
            inc = self.ref_gr.proj(self.detector_index).included;
            sel = self.ref_gr.proj(self.detector_index).selected;

            ab_sel = ond(inc(sel));

            num_blobs = numel(find(sel));
            num_oris = numel(grs);
            uv_tab = zeros(num_blobs, 2, num_oris);
            for ii_o = 1:num_oris
                uv_tab(:, :, ii_o) = grs(ii_o).allblobs(self.detector_index).detector.uvw(ab_sel, 1:2);
            end

            min_proj_uv = reshape(min(floor(uv_tab), [], 3), [], 1, 2);
            max_proj_uv = reshape(max(ceil(uv_tab), [], 3), [], 1, 2);
            proj_uv_lims = [min_proj_uv, max_proj_uv];
            delta_uvs = reshape(proj_uv_lims(:, 2, :) - proj_uv_lims(:, 1, :), [], 2);
        end

        function bytes = get_memory_consumption_geometry(self)
            bytes = GtBenchmarks.getSizeVariable(self.lattice) ...
                + GtBenchmarks.getSizeVariable(self.lattice_ss) ...
                + GtBenchmarks.getSizeVariable(self.R_vectors);
        end

        function bytes = get_memory_consumption_graindata(self)
            bytes = GtBenchmarks.getSizeVariable(self.ref_gr);
        end
    end

    methods (Access = protected)
        function create_R_vectors(self, num_sampling_points, dcenters, dranges, oversize, samp_axes)
            dranges = dranges * oversize;

            num_dims = numel(num_sampling_points);
            if (~exist('samp_axes', 'var') || isempty(samp_axes))
                samp_axes = eye(num_dims, 3);
            end

            self.sampling_size = num_sampling_points;
            self.sampling_res = 2 .* atand(self.get_resolution_from_points(dranges, self.sampling_size));
            self.sampling_box = [-dranges, dranges] / 2 + [dcenters, dcenters];
            self.sampling_axes = samp_axes;

            % Sampling points component wise
            spc = cell(1, num_dims);
            for ii = 1:num_dims
                spc{ii} = linspace(-dranges(ii)/2, dranges(ii)/2, num_sampling_points(ii));
            end

            [spc{:}] = ndgrid(spc{:});

            sp = cell(1, num_dims);
            for ii_out = 1:3
                sp{ii_out} = dcenters(ii_out);
                for ii_in = 1:num_dims
                    sp{ii_out} = sp{ii_out} + samp_axes(ii_in, ii_out) * spc{ii_in};
                end
            end

            self.R_vectors = gtDefDmvol2Gvdm(cat(4, sp{:}))';

%             xx = linspace(self.sampling_box(1), self.sampling_box(4), num_sampling_points(1));
%             yy = linspace(self.sampling_box(2), self.sampling_box(5), num_sampling_points(2));
%             if (num_sampling_points(3) == 1)
%                 zz = (self.sampling_box(3) + self.sampling_box(6)) / 2;
%             else
%                 zz = linspace(self.sampling_box(3), self.sampling_box(6), num_sampling_points(3));
%             end
% 
%             [xx, yy, zz] = ndgrid(xx, yy, zz);
% 
%             self.R_vectors = gtDefDmvol2Gvdm(cat(4, xx, yy, zz))';
        end

        function produce_geometry(self, det_ind)
            mode = self.lattice.types{det_ind};

            fprintf('Computing geometries (detector: %d, acq type: %s): ', det_ind, mode)
            c = tic();

            if (strcmpi(mode, 'dct'))
                ref_refl_ind = self.ref_gr.allblobs(det_ind).omind;
            else
                ref_refl_ind = self.ref_gr.allblobs(det_ind).phind;
            end

            num_orients = numel(self.lattice.gr);
            if (num_orients > 1)
                strategy = 2;
            else
                strategy = 1;
            end

            switch(strategy)
                case 3 % All in once
                    num_chars_gr = fprintf('gtCalculateGrain in parallel..');
                    self.lattice.gr = gtCalculateGrain_p( ...
                        self.lattice.gr, self.parameters, ...
                        'ref_omind', ref_refl_ind, 'det_ind', det_ind );
                case 2 % In chunks
                    num_chars_gr = fprintf('gtCalculateGrain in chunks: ');
                    chunk_size = 1009; % 1000 seems to generate frequent conflicts (remaining list with only one element?) 
                    for ii = 1:chunk_size:num_orients
                        end_chunk = min(ii+chunk_size-1, num_orients);
                        if (ii <= end_chunk)
                            num_chars_gr_ii = fprintf('%05d/%05d', ii, num_orients);
                            self.lattice.gr(ii:end_chunk) = gtCalculateGrain_p( ...
                                self.lattice.gr(ii:end_chunk), self.parameters, ...
                                'ref_omind', ref_refl_ind, 'det_ind', det_ind );
                            fprintf(repmat('\b', [1 num_chars_gr_ii]));
                        end
                    end
                    fprintf(repmat(' ', [1 num_chars_gr_ii]));
                    fprintf(repmat('\b', [1 num_chars_gr_ii]));
                case 1 % One by One
                    num_chars_gr = fprintf('gtCalculateGrain: ');
                    for ii = 1:num_orients
                        num_chars_gr_ii = fprintf('%05d/%05d', ii, num_orients);
                        self.lattice.gr{ii} = gtCalculateGrain( ...
                            self.lattice.gr{ii}, self.parameters, ...
                            'ref_omind', ref_refl_ind, 'det_ind', det_ind );
                        fprintf(repmat('\b', [1 num_chars_gr_ii]));
                    end
                    fprintf(repmat(' ', [1 num_chars_gr_ii]));
                    fprintf(repmat('\b', [1 num_chars_gr_ii]));
            end
            fprintf(repmat('\b', [1 num_chars_gr]));
            fprintf(repmat(' ', [1 num_chars_gr]));
            fprintf(repmat('\b', [1 num_chars_gr]));

            total_orient = sum(arrayfun(@(x)numel(x.gr), self.lattice));
            fprintf(' (detector: %d, total orientations: %04d, in %3.1f s) Done.\n', ...
                det_ind, total_orient, toc(c));
        end

        function produce_geometry_supersampling(self, det_ind)
            mode = self.lattice.types{det_ind};
            fprintf('Computing super-sampling geometries (detector: %d, acq type: %s): ', det_ind, mode)
            c = tic();
            num_orients = numel(self.lattice.gr);

            if (strcmpi(mode, 'dct'))
                ref_refl_ind = self.ref_gr.allblobs(det_ind).omind;
            elseif (strcmpi(mode, 'topotomo'))
                ref_refl_ind = self.ref_gr.allblobs(det_ind).phind;
            end

            for g_ii = 1:num_orients
                num_chars_gr_ii = fprintf('%05d/%05d', g_ii, num_orients);

                self.lattice_ss(det_ind).gr{g_ii} = gtCalculateGrain_p( ...
                    self.lattice_ss(det_ind).gr{g_ii}, self.parameters, ...
                    'ref_omind', ref_refl_ind, 'det_ind', det_ind );

                fprintf(repmat('\b', [1 num_chars_gr_ii]));
            end

            total_orient = sum(arrayfun(@(x)numel(x.gr), self.lattice_ss(det_ind)));
            fprintf(' (detector: %d, total orientations: %04d, in %3.1f s) Done.\n', ...
                det_ind, total_orient, toc(c));
        end

        function compute_pixel_distances_simple_grid(self)
            max_dist_u = 0;
            max_dist_v = 0;
            max_angle_u = 0;
            max_angle_v = 0;

            for z_ii = 1:size(self.lattice(1).gr, 3)
                for y_ii = 1:size(self.lattice(1).gr, 2)
                    for x_ii = 1:size(self.lattice(1).gr, 1)-1
                        gr1 = self.lattice(1).gr{x_ii, y_ii, z_ii};
                        gr2 = self.lattice(1).gr{x_ii+1, y_ii, z_ii};
                        [gr12_dist, gr12_angle] = self.compute_max_proj_pixel_dist(gr1, gr2);
                        max_dist_u = max(max_dist_u, gr12_dist);
                        max_angle_u = max(max_angle_u, gr12_angle);
                    end
                end

                for y_ii = 1:size(self.lattice(1).gr, 2)-1
                    for x_ii = 1:size(self.lattice(1).gr, 1)
                        gr1 = self.lattice(1).gr{x_ii, y_ii, z_ii};
                        gr2 = self.lattice(1).gr{x_ii, y_ii+1, z_ii};
                        [gr12_dist, gr12_angle] = self.compute_max_proj_pixel_dist(gr1, gr2);
                        max_dist_v = max(max_dist_v, gr12_dist);
                        max_angle_v = max(max_angle_v, gr12_angle);
                    end
                end
            end

            self.stats.proj.max_pixel_dist_u = max_dist_u;
            self.stats.proj.max_pixel_dist_v = max_dist_v;

            self.stats.proj.max_angle_u = max_angle_u;
            self.stats.proj.max_angle_v = max_angle_v;

            self.stats.sample.avg_dist = self.compute_average_sample_distance();
        end

        function dist = compute_average_sample_distance(self)
            ond = self.ref_gr.proj(self.detector_index).ondet;
            inc = self.ref_gr.proj(self.detector_index).included;
            sel = self.ref_gr.proj(self.detector_index).selected;

            ab_sel = ond(inc(sel));

            dist = 0;
            tot_orient = get_total_num_orientations(self);
            for o_ii = 1:numel(self.lattice)
                for z_ii = 1:size(self.lattice(o_ii).gr, 3)
                    for y_ii = 1:size(self.lattice(o_ii).gr, 2)
                        for x_ii = 1:size(self.lattice(o_ii).gr, 1)
                            ab = self.lattice(o_ii).gr{x_ii, y_ii, z_ii}.allblobs;
                            if (isfield(ab, 'detector'))
                                positions = ab.detector(1).proj_geom(ab_sel, 4:6);
                            else
                                positions = ab.proj_geom(ab_sel, 4:6);
                            end
                            dist = dist + sum(sqrt(sum(positions .^ 2, 2))) / size(positions, 1);
                        end
                    end
                end
            end
            dist = dist / tot_orient;
        end

        function [dist, angle] = compute_max_proj_pixel_dist(self, gr1, gr2)
            ond = self.ref_gr.proj(self.detector_index).ondet;
            inc = self.ref_gr.proj(self.detector_index).included;
            sel = self.ref_gr.proj(self.detector_index).selected;

            ab_sel = ond(inc(sel));

            if (isfield(gr1.allblobs, 'detector'))
                geom1 = gr1.allblobs(self.detector_index).detector(self.detector_index).proj_geom(ab_sel, :);
                geom2 = gr2.allblobs(self.detector_index).detector(self.detector_index).proj_geom(ab_sel, :);
            else
                geom1 = gr1.allblobs(self.detector_index).proj_geom(ab_sel, :);
                geom2 = gr2.allblobs(self.detector_index).proj_geom(ab_sel, :);
            end

            dirs1 = geom1(:, 1:3);
            dirs2 = geom2(:, 1:3);

            det_pos1 = geom1(:, 4:6);
            det_pos2 = geom2(:, 4:6);

            lengths1 = sqrt(sum(det_pos1 .^ 2, 2));
            lengths2 = sqrt(sum(det_pos2 .^ 2, 2));

            disps1 = dirs1 .* lengths1(:, [1 1 1]) - det_pos1;
            disps2 = dirs2 .* lengths2(:, [1 1 1]) - det_pos2;

            % Approximately
            disps = disps1 - disps2;
            [dist, ~] = max(sqrt(sum(disps .^ 2, 2)));

%             rot_dirs = cross(det_pos2, det_pos1);
%             norm_rot_dirs = sqrt(sum(rot_dirs .^ 2, 2));
%             rot_dirs = rot_dirs ./ norm_rot_dirs(:, [1 1 1]);
%
%             renorm_det_pos = lengths1 .* lengths2;
%             dot_det_pos = det_pos1 .* det_pos2;
%             rot_angles = abs(acos(sum(dot_det_pos, 2) ./ renorm_det_pos));
%
%             rotated_dirs2 = zeros(size(dirs2));
%
%             for r_ii = 1:numel(rot_angles)
%                 rot_comp = gtMathsRotationMatrixComp(rot_dirs(r_ii, :), 'row');
%                 rot_tensor = gtMathsRotationTensor(rot_angles(r_ii), rot_comp);
%
%                 rotated_dirs2(r_ii, :) = dirs2(r_ii, :) * rot_tensor;
%             end
%
%             angles = acos(sum(dirs1 .* rotated_dirs2, 2));

            angles = abs(acos(sum(dirs1 .* dirs2, 2)));
            [angle, ~] = max(angles);
        end

        function compute_blob_coverage_simple_grid(self, det_ind)
            if (~exist('det_ind', 'var') || isempty(det_ind))
                det_ind = self.detector_index;
            end
            omega_step = gtAcqGetOmegaStep(self.parameters, det_ind);

            ond = self.ref_gr.proj(det_ind).ondet;
            inc = self.ref_gr.proj(det_ind).included;
            sel = self.ref_gr.proj(det_ind).selected;

            ab_sel = ond(inc(sel));

            bls = self.ref_gr.proj(det_ind).bl(sel);

            num_blobs = numel(bls);
            max_depth_blobs = 0;
            for b_ii = 1:num_blobs
                max_depth_blobs = max(max_depth_blobs, size(bls(b_ii).intm, 3));
            end

            % Coverage of the slices in blobs (by Z layer of the sampling)
            self.stats.pics.blob_coverage_map = cell(self.sampling_size(3));

            % Total intensity of each slice in a blob
            self.stats.pics.blob_intensity = zeros(num_blobs, max_depth_blobs);
            % Coverage of the slices in blobs (generic)
            self.stats.pics.blob_coverage = zeros(num_blobs, max_depth_blobs);

            min_blob_pos = zeros(num_blobs, 1);
            max_blob_pos = zeros(num_blobs, 1);

            for b_ii = 1:num_blobs
                depth_blob = size(bls(b_ii).intm, 3);
                min_blob_pos(b_ii) = floor((max_depth_blobs - depth_blob)/2) + 1;
                max_blob_pos(b_ii) = min_blob_pos(b_ii) + depth_blob - 1;

                self.stats.pics.blob_intensity(b_ii, min_blob_pos(b_ii):max_blob_pos(b_ii)) ...
                    = squeeze(sum(sum(bls(b_ii).intm, 2), 1));
            end

            limits = cat(1, bls(:).bbwim);
            for o_ii = 1:numel(self.lattice)
                for z_ii = 1:size(self.lattice(o_ii).gr, 3)
                    self.stats.pics.blob_coverage_map{z_ii} = zeros(num_blobs, max_depth_blobs);

                    for y_ii = 1:size(self.lattice(o_ii).gr, 2)
                        for x_ii = 1:size(self.lattice(o_ii).gr, 1)
                            ab = self.lattice(o_ii).gr{x_ii, y_ii, z_ii}.allblobs;
                            omegas = ab.omega(ab_sel) / omega_step;
                            within_lims = find(omegas >= limits(:, 1) & omegas <= limits(:, 2));
                            pos_in_blobs = round(omegas(within_lims)) - limits(within_lims, 1) + 1;

                            pos_in_coverage = pos_in_blobs + min_blob_pos(within_lims) - 1;

                            indx = sub2ind(size(self.stats.pics.blob_coverage), within_lims, pos_in_coverage);

                            self.stats.pics.blob_coverage_map{z_ii}(indx) = ...
                                self.stats.pics.blob_coverage_map{z_ii}(indx) + 1;

                            self.stats.pics.blob_coverage(indx) = 1;
                        end
                    end
                end
            end

            % Intensity reached by at least one orientation
            self.stats.pics.blob_covered_intensity = ...
                self.stats.pics.blob_intensity .* self.stats.pics.blob_coverage;
            % Intensity NOT reached by any orientation
            self.stats.pics.blob_uncovered_intensity = ...
                self.stats.pics.blob_intensity .* (1 - self.stats.pics.blob_coverage);
        end

        function plot_blob_coverage(self)
            f1 = figure();
            lims = [min(self.stats.pics.blob_intensity(:)) max(self.stats.pics.blob_intensity(:))];
            ax1 = subplot(1, 4, 1, 'parent', f1);
            imagesc(self.stats.pics.blob_covered_intensity, 'parent', ax1, lims);
            title(ax1, 'Covered Intensity');
            ax2 = subplot(1, 4, 2, 'parent', f1);
            imagesc(self.stats.pics.blob_uncovered_intensity, 'parent', ax2, lims);
            title(ax2, 'Uncovered Intensity');
            ax2 = subplot(1, 4, 3, 'parent', f1);
            imagesc(self.stats.pics.blob_coverage, 'parent', ax2);
            title(ax2, 'Coverage');
            ax2 = subplot(1, 4, 4, 'parent', f1);
            imagesc(self.stats.pics.blob_intensity, 'parent', ax2);
            title(ax2, 'Intensity');

            num_z_comps = self.sampling_size(3);

            % Let's visualize the maps separately
            grid_rows = floor(sqrt(num_z_comps));
            grid_cols = ceil(num_z_comps / grid_rows);
            f2 = figure();
            for ii = 1:num_z_comps
                ax = subplot(grid_rows, grid_cols, ii, 'parent', f2);
                imagesc(self.stats.pics.blob_coverage_map{ii}, 'parent', ax);
                title(ax, sprintf('Coverage of Z Layer %d', ii));
            end

            drawnow();
        end

        function plot_sampling(self, gvdm, dmean)
            points = self.R_vectors;
            %points = cat(1, points{:});

            figure %, scatter3(points(:, 1), points(:, 2), points(:, 3))
            hold on
            scatter3(gvdm(1, :)', gvdm(2, :)', gvdm(3, :)', 20, 'c')
            scatter3(points(:, 1), points(:, 2), points(:, 3), 20, 'r')
            scatter3(mean(points(:, 1)), mean(points(:, 2)), mean(points(:, 3)), 30, 'm', 'filled')
            scatter3(dmean(1), dmean(2), dmean(3), 30, 'g', 'filled')
            hold off

            drawnow();
        end

        function plot_supersampling(self)
            points = [self.lattice.gr{:}];
            points = cat(1, points.R_vector);
            points_os = [self.lattice_ss.gr{:}];
            points_os = [points_os{:}];
            points_os = cat(1, points_os.R_vector);

            figure %, scatter3(points(:, 1), points(:, 2), points(:, 3))
            hold on
            scatter3(points(:, 1), points(:, 2), points(:, 3), 20, 'c')
            scatter3(points_os(:, 1), points_os(:, 2), points_os(:, 3), 20, 'r')
            hold off

            drawnow();
        end

        function print_sampling_details(self, det_ind)
            size_bb = 2 * atand(self.sampling_box(4:6) - self.sampling_box(1:3));
            max_resolution = self.estimate_maximum_resolution(det_ind);
            max_res_sampling_points = self.get_points_from_resolution(size_bb, max_resolution);

            num_interp_range = self.minimum_num_interp(det_ind);
            num_interp_range = [num_interp_range, num_interp_range * 2];

            detected_num_interp = self.parameters.rec.grains.options.num_interp;
            if (numel(detected_num_interp) > 1)
                detected_num_interp = detected_num_interp(det_ind);
            end
            if (detected_num_interp < 0)
                detected_num_interp = abs(detected_num_interp) * self.minimum_num_interp(det_ind);
            elseif (~detected_num_interp)
                detected_num_interp = self.suggest_num_interp(det_ind);
            end

            fprintf('\nOrientation-sapce sampling details:\n')
            fprintf('- Bounding-box size (deg): [%g, %g, %g]\n', size_bb)
            fprintf('- Maximum estimated resolution (deg): [%g, %g, %g] (Sampling points: [%d, %d, %d] => %d)\n', ...
                max_resolution, max_res_sampling_points, prod(max_res_sampling_points));
            fprintf('- Resolution (deg): [%g, %g, %g] (Sampling points: [%d, %d, %d] => %d)\n', ...
                self.sampling_res, self.sampling_size, prod(self.sampling_size));
            fprintf('- Suggested num_interp interval: [%g, %g], detected: %g\n\n', ...
                num_interp_range, detected_num_interp);
        end
    end

    methods (Access = public, Static)
        function l = get_sampling_grid_definition()
            l = struct('gr', {{}}, 'types', {{}});
        end

        function num_points = get_points_from_resolution(bbox_size, res)
            num_points = ceil(bbox_size ./ res) + 1;
        end

        function res = get_resolution_from_points(bbox_size, num_points)
            res = bbox_size ./ (num_points - 1);
        end

        function [avg_R_vecs, avg_R_vecs_int, stddev_R_vecs] = computeAverageOrientations(ors, or_vols, selected)
            if (~exist('selected', 'var') || isempty(selected))
                selected = true(size(or_vols));
            end

            final_vol_size = [size(or_vols{1}, 1), size(or_vols{1}, 2), size(or_vols{1}, 3)];

            avg_R_vecs = zeros([final_vol_size, 3]);
            avg_R_vecs_int = zeros(final_vol_size);
            stddev_R_vecs = zeros(final_vol_size);

            if (iscell(ors))
                ors_sel = [ors{selected}];
                % Converting to gvdm
                gvdm_tmp = cat(1, ors_sel(:).R_vector)';
            else
                gvdm_tmp = ors(:, selected);
            end

            % computing weighted average orientation
            for ii = 1:numel(or_vols)
                weights = or_vols{ii};
                r_vec = cat(4, ...
                    gvdm_tmp(1, ii) * ones(final_vol_size), ...
                    gvdm_tmp(2, ii) * ones(final_vol_size), ...
                    gvdm_tmp(3, ii) * ones(final_vol_size) );

                avg_R_vecs_int = avg_R_vecs_int + weights;
                avg_R_vecs = avg_R_vecs + weights(:, :, :, [1 1 1]) .* r_vec;
            end

            avg_R_vecs_int_norm = avg_R_vecs_int + (avg_R_vecs_int == 0);
            avg_R_vecs = avg_R_vecs ./ avg_R_vecs_int_norm(:, :, :, [1 1 1]);

            % computing weighted standard deviations
            for ii = 1:numel(or_vols)
                r_vec = cat(4, ...
                    gvdm_tmp(1, ii) * ones(final_vol_size), ...
                    gvdm_tmp(2, ii) * ones(final_vol_size), ...
                    gvdm_tmp(3, ii) * ones(final_vol_size) );

                square_dists = sum((r_vec - avg_R_vecs) .^ 2, 4);
                weights = or_vols{ii};
                stddev_R_vecs = stddev_R_vecs + weights .* square_dists;
            end

            stddev_R_vecs = stddev_R_vecs ./ avg_R_vecs_int_norm;
        end

        function avg_R_vec = computeAverageOrientation(gr, vols)
            gr_sel = [gr{:}];
            R_vecs = cat(1, gr_sel(:).R_vector);

            vol_ints = GtOrientationSampling.computeODF(gr, vols);
            vol_ints = reshape(vol_ints, [], 1);
            vol_ints = vol_ints(:, [1 1 1]);

            avg_R_vec = sum(R_vecs .* vol_ints, 1) ./ sum(vol_ints, 1);
        end

        function odf = computeODF(ors, vols)
            odf = zeros(size(ors));
            for ii = 1:numel(vols)
                odf(ii) = gtMathsSumNDVol(vols{ii});
            end
        end

        function [dmean, dcenters, dranges] = get_deformation_stats(gvdm)
            dmean = mean(gvdm, 2)';
            dcenters = (max(gvdm, [], 2)' + min(gvdm, [], 2)') / 2;
            dranges = max(gvdm, [], 2)' - min(gvdm, [], 2)';
        end
    end
end
