function bl = gtDefFwdProjGvdm2W(grain, ref_sel, gv, fedpars, parameters, det_ind, verbose)

    if (~exist('det_ind', 'var'))
        det_ind = 1;
    end
    if (~exist('verbose', 'var'))
        verbose = true;
    end

    o = GtConditionalOutput(verbose);

    o.fprintf('Forward projection (%s):\n', mfilename)

    om_step = gtAcqGetOmegaStep(parameters, det_ind);
    nbl = numel(ref_sel);

    uinds = gv.used_ind;
    nv = numel(uinds);

    use_diffractometer = (isfield(parameters, 'diffractometer') ...
        && numel(parameters.diffractometer) >= det_ind ...
        && det_ind > 1);

    labgeo = parameters.labgeo;
    samgeo = parameters.samgeo;
    if (use_diffractometer)
        diff_acq = parameters.diffractometer(det_ind);
        diff_ref = parameters.diffractometer(1);

        rotcomp = gtMathsRotationMatrixComp(diff_acq.axes_rotation', 'col');
        rotdir = diff_acq.axes_rotation';

        instr_t = gtGeoDiffractometerTensor(diff_acq, 'sam2lab', ...
            'reference_diffractometer', diff_ref);
    else
        rotcomp = gtMathsRotationMatrixComp(labgeo.rotdir', 'col');
        rotdir = labgeo.rotdir';
    end

    if (strcmpi(fedpars.defmethod, 'rod_rightstretch'))
        % In this case, the deformation is relative to [0, 0, 0], so we
        % need crystal plane normals
        if (isfield(grain.allblobs, 'plcry'))
            pls_orig = grain.allblobs(det_ind).plcry(ref_sel, :);
        else
            % We bring the plane normals back to the status of pl_cry
            pls_orig = grain.allblobs(det_ind).plorig(ref_sel, :);

            g = gtMathsRod2OriMat(grain.R_vector);
            pls_orig = gtVectorLab2Cryst(pls_orig, g);
        end
    else
        % Here the deformation is relative to the average oriantion, so we
        % need the undeformed sample plane normals
        pls_orig = grain.allblobs(det_ind).plorig(ref_sel, :);
    end

    if (isfield(fedpars, 'detector') ...
        && isfield(fedpars.detector(det_ind), 'blobs_w_interp') ...
        && ~isempty(fedpars.detector(det_ind).blobs_w_interp))
        blobs_w_interp = fedpars.detector(det_ind).blobs_w_interp;
        if (numel(blobs_w_interp) ~= nbl)
            error([mfilename ':wrong_argument'], ...
                'Number of "blobs_w_interp" (%d) doesn''t match with the number of blobs (%d)', ...
                numel(blobs_w_interp), nbl)
        end
    else
        blobs_w_interp = ones(nbl, 1);
    end

    sinths = grain.allblobs(det_ind).sintheta(ref_sel);
    ominds = grain.allblobs(det_ind).omind(ref_sel);

    if (~use_diffractometer)
        % The plane normals need to be brought in the Lab reference where the
        % beam direction and rotation axis are defined.
        % Use the Sample -> Lab orientation transformation assuming omega=0;
        % (vector length preserved for free vectors)
        pls_orig = gtGeoSam2Lab(pls_orig, eye(3), labgeo, samgeo, true);
    end

    if (isfield(fedpars, 'ref_omega') && ~isempty(fedpars.ref_omega))
        ref_oms = fedpars.ref_omega(ref_sel);
    else
        ref_oms = grain.allblobs(det_ind).omega(ref_sel);
    end

    w_shifts = round(ref_oms ./ om_step ./ blobs_w_interp);

    linear_interp = ~(isfield(fedpars, 'projector') ...
        && isstruct(fedpars.projector) && isfield(fedpars.projector, 'interp') ...
        && strcmpi(fedpars.projector, 'nearest'));

    gvpow = gv.pow(1, uinds);

    gvd = gv.d(:, uinds);

    % Deformation tensor (relative to reference state) from its components
    defT = gtFedDefTensorFromComps(gvd, fedpars.dcomps, fedpars.defmethod, 0);

    %%% Computation of indices
    o.fprintf('   * Computing indices and bbsizes: ')
    t = tic();

    w = cell(nbl, 1);
    w_min = zeros(nbl, 1);
    w_max = zeros(nbl, 1);

    valid_voxels = false(nv, nbl);

    for ii_b = 1:nbl
        num_chars = o.fprintf('%03d/%03d', ii_b, nbl);
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Calculate new detector coordinates
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        % New deformed plane normals and relative elongations (relative to
        % reference state)
        % !!! use plcry and not plsam to keep omega order below !!!
        pl_orig = pls_orig(ii_b, :)';

        % unit column vectors
        [pl_samd, drel] = gtStrainPlaneNormals(pl_orig, defT);

        if (use_diffractometer)
            % The plane norma ls need to be brought in the Lab reference where the
            % beam direction and rotation axis are defined.
            % Use the Sample -> Lab orientation transformation assuming omega=0;
            % (vector length preserved for free vectors)
            pl_samd = gtGeoSam2Lab(pl_samd', instr_t, labgeo, samgeo, true)';
        end

        % New sin(theta)
        sinth = sinths(ii_b) ./ drel;

        % Predict omega angles: 4 for each plane normal
        om = gtFedPredictOmegaMultiple(...
            pl_samd, sinth, labgeo.beamdir', rotdir, rotcomp, ominds(ii_b));

        valid_voxels(:, ii_b) = ~isnan(om);

        % Delete those where no reflection occurs
        if (any(isnan(om)))
            inds_bad = find(isnan(om));
            gvd(:, inds_bad(1:min(10, numel(inds_bad))))
            warning('gtFedFwdProjExact:bad_R_vectors', ...
                'No diffraction from some elements after deformation (%d over %d) for blob %d.', ...
                numel(inds_bad), numel(om), ii_b)
        end

        om = gtGrainAnglesTabularFix360deg(om, ref_oms(ii_b));

        w_bl = om ./ om_step ./ blobs_w_interp(ii_b);
        % Transforming into offsets from average orientation
        w_bl = w_bl - w_shifts(ii_b);

        w{ii_b} = w_bl;

        w_min(ii_b, :) = min(w{ii_b});
        w_max(ii_b, :) = max(w{ii_b});

        if ((min(om) > ref_oms(ii_b)) || (max(om) < ref_oms(ii_b)))
            warning([mfilename ':wrong_result'], ...
                '\nThe average orientation seems to project outside the blob!\n')
        end

        o.fprintf(repmat('\b', [1, num_chars]));
    end
    o.fprintf('Done in %g s\n', toc(t));

    o.fprintf('   * Computing max BBox size and feasibilities:\n')
    if (linear_interp)
        blob_w_sizes = ceil(w_max) - floor(w_min) + 1;
        blob_orig_w_shifts = 1 - floor(w_min);
    else
        blob_w_sizes = round(w_max) - round(w_min) + 1;
        blob_orig_w_shifts = 1 - round(w_min);
    end

    o.fprintf('     Computed Blob W sizes: [%s]\n', sprintf(' %d', blob_w_sizes));

    bl = gtFwdSimBlobDefinition('sf_w', nbl);

    %%% Blob projection
    o.fprintf('   * Projecting volumes: ')
    t = tic();
    for ii_b = 1:nbl
        num_chars = o.fprintf('%03d/%03d', ii_b, nbl);

        % Detector coordinates U,V in blob
        w_bl = w{ii_b} + blob_orig_w_shifts(ii_b * ones(1, nv))';

        % Let's now filter valid voxels
        w_bl = w_bl(:, valid_voxels(:, ii_b))';
        gvpow_v = reshape(gvpow(valid_voxels(:, ii_b)), [], 1);

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Sum intensities
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Distribute value over 8 pixels

        if (linear_interp)
            [inds_w, ints_w] = gtMathsGetInterpolationIndices(w_bl, ...
                gvpow_v, fedpars.bltype);
        else
            inds_w = round(w_bl);
            ints_w = gvpow_v;
        end

        % Accumulate all intensities in the blob voxel-wise
        %   'uvw' needs to be nx1; 'ints' is now 1xnx8
        try
            bl(ii_b).intm = accumarray(inds_w, ints_w, [blob_w_sizes(ii_b), 1]);
        catch mexc
            fprintf(['\n\nERROR\nThe error is probably caused by the' ...
                ' projected intensities falling outside the blob volume.' ...
                '\nTry increasing the blob volume padding:' ...
                ' fedpars.detector(det_ind).blobsizeadd, or FIXING' ...
                ' YOUR CODE!!\n\n'])
            disp('Blob ID:')
            disp(ii_b)
            disp('Blob size:')
            disp(blob_w_sizes(ii_b))
            disp('Min projected W coordinate:')
            fprintf('\t%g \t(rounded: %d )\n', ...
                min(w_bl, [], 1), min(inds_w, [], 1))
            disp('Max projected W coordinate:')
            fprintf('\t%g \t(rounded: %d )\n', ...
                max(w_bl, [], 1), max(inds_w, [], 1))
            new_exc = GtFedExceptionFwdProj(mexc, det_ind, [0 0 0]);
            throw(new_exc)
        end

        bl(ii_b).bbsize = blob_w_sizes(ii_b);

        im_low_lim = w_shifts(ii_b) - blob_orig_w_shifts(ii_b) + 1;
        bl(ii_b).bbwim = [im_low_lim, im_low_lim + bl(ii_b).bbsize - 1] * blobs_w_interp(ii_b);
        bl(ii_b).interp_w = blobs_w_interp(ii_b);

        o.fprintf(repmat('\b', [1, num_chars]));
    end
    o.fprintf('Done in %g s\n', toc(t));
end
