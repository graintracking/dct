function [gvdm, dmvol_size] = gtDefDmvol2Gvdm(dmvol)
    [dmvol_size(1), dmvol_size(2), dmvol_size(3), dmvol_size(4)] = size(dmvol);
    gvdm = permute(dmvol, [4 1 2 3]);
    gvdm = reshape(gvdm, dmvol_size(4), []);
end