function EBSD_mask = gtEBSDLoadMapIndexingTSVFile(filename)

    c = tic();
    fprintf('Reading from file: "%s"..', filename);

    fid = fopen(filename);
    % Skipping the first line
    fgets(fid);

    input = textscan(fid, ['%d' '\t' '%s' ...
        '\t' '%f' '\t' '%f' '\t' '%f' '\t' '%f' '\t' '%f' '\t' '%f' ...
        '\t' '%d' '\t' '%d' '\t' '%d' '\t' '%d']);
    fclose(fid);

    EBSD_mask = cellfun(@(x)strcmpi(x, 'Halite'), input{2});

    fprintf('\b\b, Done in %f seconds.\n', toc(c));
end