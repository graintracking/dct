 function proj_bls = gt6DSpreadProjectVertices2Det(gr, verts_rec, ondet, parameters, det_ind, verbose)
% FUNCTION proj_bls = gt6DSpreadProjectVertices2Det(gr, verts_rec, ondet, parameters, det_ind, verbose)
%

    if (~exist('verbose', 'var') || isempty(verbose))
       verbose = false;
    end

    detgeo = parameters.detgeo(det_ind);
    labgeo = parameters.labgeo;
    samgeo = parameters.samgeo;
    recgeo = parameters.recgeo(det_ind);

    omstep = gtAcqGetOmegaStep(parameters, det_ind);

    r_vecs = cat(1, gr.bb_ors(:).R_vector);
    r_vecs = unique(r_vecs, 'rows');

    if (size(r_vecs, 1) ~= 8)
        error('gt6DSpreadProjectVertices2Det:wrong_argument', ...
            'Incoming bb orientations have %d unique orietations (8 were expected)', ...
            size(r_vecs, 1))
    end

    ors = cell(8, 1);
    for ii_o = 1:8
        ors{ii_o} = struct('id', gr.id, 'phaseid', gr.phaseid, ...
            'center', gr.center, 'R_vector', r_vecs(ii_o, :));
    end
    ors = gtCalculateGrain_p(ors, parameters, 'ref_omind', gr.allblobs(det_ind).omind, 'det_ind', det_ind);
    ors = [ors{:}];

    num_blobs = numel(ondet);
    proj_bls = gtFwdSimBlobDefinition('blob', num_blobs);

    dvecs_lab = zeros(num_blobs, 3, 8);
    omegas = zeros(num_blobs, 8);
    rot_l2s = zeros(3, 3, num_blobs, 8);

    for ii_o = 1:8
        dvecs_lab(:, :, ii_o) = ors(ii_o).allblobs(det_ind).dvec(ondet, :);
        omegas(:, ii_o) = ors(ii_o).allblobs(det_ind).omega(ondet);
        rot_l2s(:, :, :, ii_o) = ors(ii_o).allblobs(det_ind).srot(:, :, ondet);
    end
    omegas = gtGrainAnglesTabularFix360deg(omegas, gr.allblobs(det_ind).omega(ondet));
    rot_s2l = permute(rot_l2s, [2 1 3 4]);

    if (verbose)
        uv = gr.allblobs(det_ind).detector.uvw(ondet, 1:2);
    end

    ones_verts = ones(size(verts_rec, 1), 1);
    gc_sam = gr.center(ones_verts, :);

    verts_sam = repmat(gtGeoSam2Sam(verts_rec, recgeo, samgeo, false) + gc_sam, [8, 1]);

    % Now we project the vertices to the spots and take the convex hull
    for ii_b = 1:num_blobs
        rot_s2l_w = rot_s2l(:, :, ii_b .* ones_verts, :);
        rot_s2l_w = reshape(rot_s2l_w, 3, 3, []);
        verts_lab = gtGeoSam2Lab(verts_sam, rot_s2l_w, labgeo, samgeo, false);
        dvec_vers = dvecs_lab(ii_b .* ones_verts, :, :);
        dvec_vers = reshape(permute(dvec_vers, [1 3 2]), [], 3);

        verts_det_pos = gtFedPredictUVMultiple([], dvec_vers', verts_lab', ...
            detgeo.detrefpos', detgeo.detnorm', detgeo.Qdet, ...
            [detgeo.detrefu, detgeo.detrefv]')';

        if (verbose)
            f = figure();
            ax = axes('parent', f);
            hold(ax, 'on')
            scatter(ax, verts_det_pos(:, 1), verts_det_pos(:, 2));
            scatter(ax, uv(ii_b, 1), uv(ii_b, 2), 30, 'r');
            hold(ax, 'off')
        end

        verts_idxs = convhull(verts_det_pos);
        verts_det_pos = verts_det_pos(verts_idxs, :);

        bb_2d = [floor(min(verts_det_pos, [], 1)), ceil(max(verts_det_pos, [], 1))];
        bb_2d = [bb_2d(1:2), (bb_2d(3:4) - bb_2d(1:2) + 1)];

        mask = zeros(bb_2d(3:4));
        verts_mask_pos = round(verts_det_pos) - bb_2d(ones(numel(verts_idxs), 1), 1:2) + 1;
        verts_mask_indx = verts_mask_pos(:, 1) + bb_2d(3) * (verts_mask_pos(:, 2) - 1);
        mask(verts_mask_indx) = 1;
        mask = bwconvhull(mask);

        proj_bls(ii_b).bbuim = [bb_2d(1), (bb_2d(1) + bb_2d(3) - 1)];
        proj_bls(ii_b).bbvim = [bb_2d(2), (bb_2d(2) + bb_2d(4) - 1)];
        proj_bls(ii_b).bbwim = [ ...
            floor(min(omegas(ii_b, :)) / omstep), ...
            ceil(max(omegas(ii_b, :)) / omstep) ];
        proj_bls(ii_b).bbsize = [bb_2d(3:4), 1];
        proj_bls(ii_b).mask = mask;

        if (verbose)
            f = figure();
            ax = axes('parent', f);
            imshow(mask, 'parent', ax)

            pause
        end
    end
 end
