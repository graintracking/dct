function dmvol = gtDefGvdm2Dmvol(gvdm, dmvol_size)
    dmvol = reshape(gvdm, [dmvol_size(4), dmvol_size(1:3)]);
    dmvol = permute(dmvol, [2 3 4 1]);
end