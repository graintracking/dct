classdef Gt6DVolumeToSpotProjector < Gt6DVolumeProjector
    properties
        % Weights
        fwd_weights = {};
        bwd_weights = {};
    end

    methods (Access = public)
        function self = Gt6DVolumeToSpotProjector(initialVolumes, detectorSize, varargin)
            self = self@Gt6DVolumeProjector(initialVolumes, detectorSize, varargin{:});
        end

        function fwd_weights = getRowsSum(self)
            fwd_weights = gtMathsGetSameSizeZeros(self.sinogram);
            for n = 1:numel(self.geometries);
                fwd_weights = self.fwd_project_single_volumes( ...
                    fwd_weights, 1, n);
            end
        end

        function bwd_weights = getColumnsSum(self)
            num_geoms = numel(self.geometries);
            bwd_weights = cell(1, num_geoms);
            for n = 1:num_geoms
                bwd_weights{n} = self.bwd_project_single_volumes(1, n);
            end
        end
    end

    methods (Access = protected)
        function sinogram = fwd_project_single_volumes(self, sinogram, volume, n)
            sinogram = sinogram + self.fwd_project_single_volume(volume, n);
        end

        function volume = bwd_project_single_volumes(self, sinogram, n)
            volume = self.bwd_project_single_volume(sinogram, n);
        end
    end
end