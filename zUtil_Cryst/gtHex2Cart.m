function UVW = gtHex2Cart(uvtw, latticepar, convention)
% GTHEX2CART  Converts a direction in direct hexagonal space described by 
%             Miller-Bravais indexes (uvtw) to the Crystal Cartesian coordinates in the 
%             real space (UVW) normalising it
%     UVW = gtHex2Cart(uvtw, latticepar, convention)
%     ----------------------------------------------
%     It follows the X-convention using right handed CS
%     From (uvtw) direction in Hexagonal direct space to (UVW) direction 
%     in crystal cartesian space
%     Same as the old function gtCrystHKL2Cart
%
%     INPUT:
%       uvtw       = <double>   Miller-Bravais indexes (N, 4) [row vectors]
%       latticepar = <double>   Lattice parameters (1, 6)
%       convention = <string>   hexagonal unit cell convention {'X'}/'Y'
%
%     OUTPUT:
%       UVW        = <double>   Direction of plane normal in the cartesian
%                               normalised space (N, 3)
%
%     Version 002 25-10-2013 by LNervo

    if (~exist('convention', 'var') || isempty(convention))
        convention = 'X';
    end

    if (size(uvtw, 2) == 3)
        uvtw = gtCrystMiller2FourIndexes(uvtw, 'dir');
    end
    % going to cartesian reciprocal space + normalisation of the cartesian
    % space
    UVW(:, 1) = uvtw(:, 1) + 0.5 * uvtw(:, 2);
    UVW(:, 2) = sqrt(3) / 2 * uvtw(:, 2);
    UVW(:, 3) = uvtw(:, 4);

    % scale with unit cell parameters
    UVW(:, 1) = UVW(:, 1) * 2 / (sqrt(3) * latticepar(1));
    UVW(:, 2) = UVW(:, 2) * 2 / (sqrt(3) * latticepar(1));
    UVW(:, 3) = UVW(:, 3) / latticepar(3);

    % normalise hkls vector
    tmp = sqrt(sum((UVW .* UVW), 2));
    UVW = UVW ./ (repmat(tmp, 1, 3));

    if (strcmpi(convention, 'X'))
        % rotates counterclockwise
        UVW = rotateVectors(UVW, 'angle', 30, 'axis', [0 0 1]);
    end
end
