function [Rvec,isec1,isec2,ind] = gtCrystRodriguesVector(plnorm,shkls,...
                                  Bmat,fzone)

% FUNCTION [Rvec,isec1,isec2,ind] = gtCrystRodriguesVector(plnorm, shkls,...
%                                   Bmat, fzone)
%
% Returns the coordinates of the lines in Rodrigues space corresponding to 
% the vector 'plnorm' and signed (hkl) reflection 'shkls'. If an array
% of 'shkls' is given, multiple lines are returned. 
%
% 'plnorm' is usually a measured plane normal and 'shkls' is a set of (hkl)
% reflections to which it may correspond.
%
% Any point on the returned line defines a Rodrigues vector that represents
% a right-handed rotation in the reference system in which the coordinates 
% of 'plnorm' are specified - usually the Sample reference. The direction
% of the Rodrigues vector is the axis of rotation and its length is 
% tan(rotationangle/2).
%
% A rotation according to the Rodrigues vector brings the base 
% vectors of the Sample reference parallel with the base vectors of the 
% Cartesian Crystal reference that is fixed to the grain or crystal. 
% The Cartesian Crystal reference and its relation to the crystallographic 
% axes is defined by the matrix 'Bmat'.
%
%
% INPUT 
%   plnorm      - plane normal given in SAMPLE/LAB coordinates (1x3)
%   shkls       - list of all possible reflections of plnorm's {hkl} family
%                 given by their signed (hkl) and also (-h-k-l) Miller
%                 indices; (nx3 for cubic, nx4 for hexagonal)
%   Bmat        - transformation matrix from signed HKL to Cartesian
%   fzone       - fundamental zone in Rodrigues space (specific to the
%                 crystal symmetry)
%
% OUTPUT
%   Rvec        - position and direction vectors of those lines in 
%                 Rodrigues space which have an intersection with the 
%                 fundamental zone; [posX posY posZ dirX dirY dirZ] (mx6) 
%   isec1,isec2 - the two intersection points of the lines with the
%                 boundaries of the fundamental zone, if they exist; (mx3)
%   ind         - list of indices of input shkls which give Rvec (mx1)
%

Rvec  = [];
isec1 = [];
isec2 = [];
ind   = [];

% Calculate h vector: h is the unit plane normal of a signed
% hkl family given in real space in a Cartesian basis.
% (Note that for cubic lattices, this h vector equals the normalised Miller
% indices.)


% Loop through signed hkl reflections

for ii = 1:size(shkls,1)
    
    % Get the signed hkl plane normals in Cartesian coordinates
    hh = gtCrystHKL2Cartesian(shkls(ii,:)',Bmat)';
    
    
    % Rodrigues space lines from formulae (3.16), (3.17) in Poulsen's 3DXRD
    % book:
    
    % Cross product of hh and plnorm
    cr = [hh(2)*plnorm(3) - hh(3)*plnorm(2), ...
          hh(3)*plnorm(1) - hh(1)*plnorm(3), ...
          hh(1)*plnorm(2) - hh(2)*plnorm(1)];

    % Rodrigues vector with the smallest rotation angle
    r_zero      = cr./(1+hh*plnorm');
    
    % Straigt line direction from r_zero
    r_direction = (hh + plnorm)./(1+hh*plnorm');
    r_direction = r_direction/sqrt(r_direction*r_direction');
    
    % Rodrigues line
    Rv = [r_zero r_direction];
    
    % Lines intersections with the fundamental zone
    isec = gtMathsLinePolyhedronIntersections(Rv,fzone);
    
    if ~isempty(isec)
        Rvec  = [Rvec; Rv];
        ind   = [ind; ii];
        isec1 = [isec1; isec(1,:)];
        isec2 = [isec2; isec(2,:)];
    end
  
end
  

end % of function
