function [theta, sin_theta] = gtCrystTheta(dsp, energy)
% GTCRYSTTHETA  Calculates theta angle given a list of d-spacing
%     theta = gtCrystTheta(dsp, energy)
%     --------------------------------
%    
%     INPUT:
%       dsp    = d-spacing (Angstron)
%       energy = beam energy in keV 
% 
%     OUTPUT:
%       theta = the Bragg angle of the specified hkl family by given the
%               d-spacing list
%
%
%     Version 001 10-07-2013 by LNervo

    lambda = gtConvEnergyToWavelength(energy);

    % theta from the Bragg's law
    sin_theta = lambda ./ (2 .* dsp);
    theta = asind(sin_theta);
end
