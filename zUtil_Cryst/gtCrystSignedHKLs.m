function [allshkls, allhklinds, mult, hklsp, thtype]  = gtCrystSignedHKLs(hkl, symm)
% GTCRYSTSIGNEDHKLS  Gives a complete list of signed hkl planes, by
%     applying symmetry operators of the specified spacegroup to the input
%     list of plane families.
%     Should be revised for at least other than cubic lattices. Use of
%     'lattice_system' and 'crystal_system' instead of spacegroups should
%     be considered.
%
%     [allshkls, allhklinds, mult, hklsp, thtype] = gtCrystSignedHKLs(hkl, symm)
%     --------------------------------------------------------------------------
%
%     INPUT:
%       hkl        = <double>     plane families (one in a row) as in
%                                 parameters.cryst
%                                 (n, 3) for cubic lattices
%                                 (n, 4) for hexagonal lattices
%       symm       = <struct>     structure containing .g3/.g with symmetry
%                                 operators
%
%     OUTPUT:
%       allshkls   = <double>     complete list of signed hkl planes
%                                 (including Friedel pairs)
%       allhklinds = <double>     indices in allshkls according to input hkl 
%       mult       = <double>     multiplicity of hkl families (1, k)
%       hklsp      = <double>     list of all the hkls (friedel not considered)
%       thtype     = <double>     theta types among the given reflections
%                                 according to input hkl
%
%
%     Version 003 21-08-2013 by LNervo
%       Added output hklsp and thtype to simplify calculation
%
%     Version 002 10-07-2013 by LNervo
%       Added check of class for hkl list

    if (~isa(hkl, 'double'))
        hkl = double(hkl);
    end

    % reflections in rows
    if (~isrow(hkl))
        hkl = hkl';
    end

    if (~exist('symm', 'var') || isempty(symm))
        parameters = gtLoadParameters();
        % Get symmetry operators, this would not work with multi-phase mats
        symm = gtCrystGetSymmetryOperators(...
            parameters.cryst.crystal_system, parameters.cryst.spacegroup);
    end

    num_axes = size(hkl, 1);
    num_indexes = size(hkl, 2);
    num_symm_ops = numel(symm);

    switch (num_indexes)
        case 4
            symm = cat(3, symm(:).g);
        case 3
            symm = cat(3, symm(:).g3);
        otherwise
            error('gtCrystSignedHKLs:wrongSize', ...
                'Something went wrong with the size of hkl list...Quitting')
    end

    allshkls   = [];
    allhklinds = [];
    mult       = [];
    hklsp      = [];
    thtype     = [];

    % Loop through input hkl types
    for ii = 1:num_axes
%         for ii_hkl = 1:num_symm_ops
%             old_shkls(ii_hkl * 2 - 1, :) = hkl(ii, :) * symm(:, :, ii_hkl);
%             old_shkls(ii_hkl * 2,     :) = - old_shkls(ii_hkl * 2 - 1, :);
%         end
        % Apply all symmmetry operators to the hkl type to get signed hkl-s
        shkls = zeros(num_symm_ops * 2, num_indexes);
        tmp_shkl = hkl(ii, :) * reshape(symm, num_indexes, num_indexes * num_symm_ops);
        tmp_shkl = reshape(tmp_shkl', num_indexes, num_symm_ops)';
        shkls(1:2:end, :) =  tmp_shkl;
        shkls(2:2:end, :) = -tmp_shkl;

        % Remove duplicates and add to the complete list
        newshkls = unique(shkls, 'rows');
        % sort according to h > k > l
        newshkls = sortrows(newshkls, [-1 -2 -num_indexes]);
        nhkls = size(newshkls, 1) / 2;

        % This should be changed to be preallocated and more structured
        mult       = [mult; nhkls * 2];
        allshkls   = [allshkls; newshkls];
        allhklinds = [allhklinds; repmat(ii, nhkls * 2, 1)];
        %newshkls   = sortrows(newshkls, [ncols 1 2]);
        hklsp      = [hklsp; newshkls(nhkls+1:end, :)];
        thtype     = [thtype; repmat(ii, nhkls, 1)];
    end
end

