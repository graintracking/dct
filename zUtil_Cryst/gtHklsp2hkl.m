function hklt = gtHklsp2hkl(hklsp,spacegroup)
% GTHKLSP2HKL  Gives the unsigned hkl family corrsponding to a given hklsp
%     hkl = gtHklsp2hkl(hklsp,spacegroup)
%     -----------------------------------
%
%     INPUT:
%       hklsp      = plane families (one in a row)
%                    nx3 for cubic lattices
%                    nx4 for hexagonal lattices
%       spacegroup = space group
%
%     OUTPUT:
%       hklt        = corresponding hkl -or- hkil type
%

% Get symmetry operators depending on spacegoup:
switch spacegroup

    case {225, 227, 229, 221}       % Cubic - 3x3 matrices
        hklt = sort(abs(hklsp)) ;

    case {152, 167, 194, 663}  % Hexagonal - 4x4 matrices
        hklt = [sort(abs(hklsp(1:2))),hklsp(3),abs(hklsp(4))];

    otherwise
        error('Spacegroup not supported')
end

