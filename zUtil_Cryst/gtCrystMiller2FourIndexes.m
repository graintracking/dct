function [hkil, hkil_int] = gtCrystMiller2FourIndexes(hkl, miller_type)
% GTCRYSTMILLER2FOURINDEXES  Converts the Miller notation (hkl) to the
%                            four indexes Miller-Bravais notation
%
%     [hkil, hkil_int] = gtCrystMiller2FourIndexes(hkl, miller_type)
%     --------------------------------------------------------------
%     From 3-index notation to 4-index notation (only hexagonal unit cell)
%     INPUT:
%       hkl         = <double>   plane normal Miller indexes (Nx3)
%       miller_type = <string>   'plane' / 'direction'
%
%     OUTPUT:
%       hkil        = <double>   Miller-Bravais notation for hexagonal
%                                materials (float allowed)
%       hkil_int    = <double>   Miller-Bravais indexes for hexagonal materials
%                                as integers
%
%     Version 001 22-10-2013 by LNervo
%
%     Ref. LaboTex 3.0: Piotr Ozga, "Hexagonal Axes: Conventions & Conversions"


    hkil = zeros(size(hkl, 1), 4);

    switch lower(miller_type)
      case {'direction', 'dir', 'd'}
        hkil(:, 1) = (2 * hkl(:, 1) - hkl(:, 2)) / 3;
        hkil(:, 2) = (2 * hkl(:, 2) - hkl(:, 1)) / 3;
        hkil(:, 3) =   - (hkl(:, 1) + hkl(:, 2)) / 3;
        hkil(:, 4) = hkl(:, 3);

      case {'plane', 'pl', 'p'}
        hkil(:, 1) = hkl(:, 1);
        hkil(:, 2) = hkl(:, 2);
        hkil(:, 3) = - (hkl(:, 1) + hkl(:, 2));
        hkil(:, 4) = hkl(:, 3);
    end

    if (nargout > 1)
        for ii = size(hkil, 1):-1:1
            tmp = hkil(ii, :);
            tmp((tmp > 1e-9 & tmp <= 0.1) | tmp == 0) = [];
            minimum(ii) = max(min(abs(tmp)), 0);
            hkil_int(ii, :) = hkil(ii, :) ./ minimum(ii);
            hkil_int(ii, :) = fix(hkil_int(ii, :));
        end
    end
end
