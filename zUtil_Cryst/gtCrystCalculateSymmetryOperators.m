function [symm_unique, symm_all] = gtCrystCalculateSymmetryOperators(phaseid, sg, crystal_system)
% GTCRYSTCALCULATEREFLECTIONS  Calculate the symmetry operators list using python 
%                              from xfab library in fable (sg.py)
%     [symm_unique, symm_all] = gtCrystCalculateSymmetryOperators(phaseid, [sg], [crystal_system])
%     --------------------------------------------------------------------------------------------
%     If spacegroup and crystal_system are not provided, they are read from
%     parameters.mat.
%
%     INPUT: 
%       phaseid        = <double>   phase number {1}
%       sg             = <double>   spacegroup number
%       crystal_system = <string>   crystal system
%     
%     OUTPUT:
%       symm_unique = <struct 1xN> contains :
%               .perm  = set of indistinguishable lattice permutations <double 3x3>
%               .g3    = set of unitary rotation matrices corresponding to the
%                        indistinguishable lattice permutations <double 3x3>
%       symm_all = <struct 1xM> contains :
%               .g3    = rotations (complete list using symmetry operators) <double 3x3>
%               .trans = translation values from symmetry operators <double 1x3>
%
%     Version 002 26-11-2013 by LNervo
%       Changed output names to 'symm_unique' and 'symm_all'
%       Changed symm_all.rot to symm_all.g3
%       Removed default value for phaseid
%
%     Version 001 10-12-2012 by LNervo


if ~exist('sg','var') || isempty(sg)
    parameters = [];
    load('parameters.mat');
    sg = parameters.cryst(phaseid).spacegroup;
end

if ~exist('crystal_system','var') || isempty(crystal_system)
    parameters = [];
    load('parameters.mat');
    crystal_system = parameters.cryst(phaseid).crystal_system;
end

global GT_MATLAB_HOME;
script_file = fullfile(GT_MATLAB_HOME, 'zUtil_Python', 'symmetry_operators_list.py');
[~, msg] = gtPythonCommand([script_file ' ' num2str(sg) ' ' crystal_system], true);
disp(msg);

% read produced files      
permN     = gtReadTextFile(['perm_' crystal_system '.txt'],'%f %f %f',[3 3],true,'Delimiter',' ','CommentStyle','#');
rotN      = gtReadTextFile(['rot_' crystal_system '.txt'],'%f %f %f',[3 3],true,'Delimiter',' ','CommentStyle','#');
all_rotN   = gtReadTextFile(['sg_rot_' crystal_system '.txt'],'%f %f %f',[3 3],true,'Delimiter',' ','CommentStyle','#');
all_transN = gtReadTextFile(['sg_trans_' crystal_system '.txt'],'%f %f %f',[1 3],true,'Delimiter',' ','CommentStyle','#');

symm_unique = struct('g3', rotN, 'perm', permN);
if nargout == 2
    symm_all = struct('g3', all_rotN, 'trans', all_transN);
end

end % end of function
