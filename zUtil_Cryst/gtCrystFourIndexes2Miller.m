function hkl = gtCrystFourIndexes2Miller(hkil, miller_type)
% GTCRYSTFOURINDEXES2MILLER  Converts the Miller-Bravais notation (hkil) to the
%                            Miller notation
%
%     hkl = gtCrystFourIndexes2Miller(hkil, miller_type)
%     --------------------------------------------------
%     From 4-index notation to 3-index notation (only hexagonal unit cell)
%     INPUT:
%       hkil        = <double>   plane normal Miller-Bravais indexes (Nx4)
%       miller_type = <string>   'plane' / 'direction'
%
%     OUTPUT:
%       hkl         = <double>   Miller notation for hexagonal materials
%
%     Version 001 22-10-2013 by LNervo
%
%     Ref. LaboTex 3.0: Piotr Ozga, "Hexagonal Axes: Conventions & Conversions"


    hkl = zeros(size(hkil, 1), 3);

    switch (lower(miller_type))
        case {'direction', 'dir', 'd'}
            hkl(:, 1) = hkil(:, 1) - hkil(:, 3);
            hkl(:, 2) = hkil(:, 2) - hkil(:, 3);
            hkl(:, 3) = hkil(:, 4);

        case {'plane', 'pl', 'p'}
            hkl(:, 1) = hkil(:, 1);
            hkl(:, 2) = hkil(:, 2);
            hkl(:, 3) = hkil(:, 4);
    end
end
