function rgb = gtCrystSaturateSSTColor(rgb)
% GTRGBSATURATE  Saturate RGB color for a better distribution in the SST
%
%   rgb = gtCrystSaturateSSTColor(rgb)
%   --------------------------------------------------------------------------
%
%     INPUT:
%       rgb = <double>  List of N RGB colors stored in a Nx3 matrix
%
%     OUTPUT:
%       rgb = <double>  List of N RGB colors stored in a Nx3 matrix (saturated)
%
%     Version 001 22-03-2013 by YGuilhem yoann.guilhem@esrf.fr

% Get logical list of non-black colors (i.e. [0 0 0] colors)
ok = any(rgb ~= 0, 2);

% Trick to give better color distribution in the SST
rgb = sqrt(rgb);

% Compute max values for each color
maxValues = max(rgb(ok, :), [], 2);

% Saturate each non-black color
rgb(ok, :) = rgb(ok, :)./maxValues(:, [1 1 1]);

end % end of function
