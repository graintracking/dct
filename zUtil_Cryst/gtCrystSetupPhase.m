function cryst = gtCrystSetupPhase(cryst, energy)
%  GTCRYSTSETUPPHASE Sets and fills the symmetry and energy related fields
%  for a crystallographic phase (field parameters.cryst). 
%  --------------------------------------------------------------
%
%  cryst = gtCrystSetupPhase(cryst, energy)
%
%  A user function intended for manually setting crystallography info, 
%  if the {hkl} reflections and lattice parameters and spacegroup are 
%  known. Manual setting may be needed when gtSetup fails.  
%
%  INPUT:
%    cryst = crystallographic phase info
%              .hkl
%              .latticepar
%              .crystal_system
%              .spacegroup
%              .int
%
%  OUTPUT:
%    Additional fields to cryst.
%
%  Version 001 18-12-2013 by PReischig

sr = gtSymmetricReflections(cryst, energy);

nn = size(cryst.hkl,2);

Bmat = gtCrystHKL2CartesianMatrix(cryst.latticepar);

cryst.symm        = gtCrystGetSymmetryOperators(cryst.crystal_system);
cryst.thetatype   = 1:nn;
cryst.dspacing    = gtCrystDSpacing(cryst.hkl, Bmat);
cryst.theta       = gtCrystTheta(cryst.dspacing, energy);

cryst.hklsp       = sr.hklsp;
cryst.thetasp     = sr.thetasp;
cryst.dspacingsp  = sr.dspacingsp;
cryst.thetatypesp = sr.thetatypesp;
cryst.intsp       = sr.intsp;
cryst.mult        = sr.mult;



end