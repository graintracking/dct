function faces = gtCrystRodriguesFundZone(spacegroup,sm)

% FUNCTION faces = gtCrystRodriguesFundZone(spacegroup,sm)
%
% Gives the fundamental zone in Rodrigues space for a given spacegroup.
% To be revised for other than cubic lattices.
%
% About the fundamental zone in Rodrigues space for various crystal 
% symmetries, see Poulsen's book on 3DXRD Section 3 page 29, and 
% A. Morawiec, D.P. Field: Philos. Mag. A 73, 11131130 (1996).
%
%
% INPUT
%  spacegroup - crystallographic spacegroup
%  sm         - safety margin (tolerance) for fund. zone extension
%
% OUTPUT
%  faces(nx6) - defines each face of the polyhedron by 6 coordinates (first three
%               is x,y,z position relative to origin, second three is plane normal
%               x,y,z orientation pointing out of the volume).
%               [p0x p0y p0z pnormx pnormy pnormz]
%

if ~exist('sm','var')
	sm = 0;
end


switch spacegroup
	
	% Cubic
	case {225, 226, 227, 229, 221}
		
		% Sides of the polyhedron:
		limcube = sqrt(2)-1;  % half the edge of the cube
		cube = [ 1  0  0  1  0  0;
	      		-1  0  0 -1  0  0;
                 0  1  0  0  1  0;
                 0 -1  0  0 -1  0;
                 0  0  1  0  0  1;
                 0  0 -1  0  0 -1]*(limcube+sm);

		% Facets on corners:
		limfacet = 1/sqrt(3); % distance from cube center
		facets = [ 1  1  1  1  1  1;
		       	   1  1 -1  1  1 -1;
                   1 -1  1  1 -1  1;
                   1 -1 -1  1 -1 -1;
                  -1  1  1 -1  1  1;
                  -1  1 -1 -1  1 -1;
                  -1 -1  1 -1 -1  1;
                  -1 -1 -1 -1 -1 -1]/sqrt(3)*(limfacet+sm);
		
		% All faces
		faces = [cube; facets];
        
        
        
		
	% Hexagonal	
	case {663, 194, 167, 152, 154}
		
		% Bottom and top of the polyhedron (basal planes):
		limbase = 2-sqrt(3);  % half thickness along basal plane
		basal = [ 0  0  1  0  0  1;
			      0  0 -1  0  0 -1]*(limbase+sm);

		% Facets on the side:
		limfacet = 1;         % extension laterally
		t = (0:(pi/6):2*pi)';
		t = t(1:end-1);
		facets = [cos(t) sin(t) zeros(12,1)]*(limfacet+sm);
		facets = [facets, facets];

		% All faces
		faces = [basal; facets];
        
    % Tetragonal
	case {123, 141}

		% Bottom and top of the polyhedron (basal planes):
		limbase = sqrt(2)-1;  % half thickness along basal plane
		basal = [ 0  0  1  0  0  1;
			      0  0 -1  0  0 -1]*(limbase+sm);

		% Facets on the side:
		limfacet = 1;         % extension laterally
		t = (0:(pi/4):2*pi)';
		t = t(1:end-1);
		facets = [cos(t) sin(t) zeros(8,1)]*(limfacet+sm);
		facets = [facets, facets];

		% All faces
		faces = [basal; facets];

	% Orthorhombic and unknown (limit to 90deg rotations in all direction)
	case {0, 62}    
		
		% Sides of the cube:
		limcube = 1;  % half the edge of the cube
		faces = [ 1  0  0  1  0  0;
	      	 	 -1  0  0 -1  0  0;
                  0  1  0  0  1  0;
                  0 -1  0  0 -1  0;
                  0  0  1  0  0  1;
                  0  0 -1  0  0 -1]*(limcube+sm);
              
              
	otherwise
		error('The specified spacegroup is not recognized.')

end


% Normalise direction vectors
ll = sqrt(sum(faces(:,4:6).*faces(:,4:6),2));
faces(:,4:6) = faces(:,4:6)./ll(:,[1 1 1]);


% Correct numerical errors
faces(abs(faces)<1e-15) = 0;


end
