function [xop_dir,msg] = gtCrystXopInitializeDirectory(name, material)
% GTCRYSTXOPINITIALIZEDIRECTORY  
% Creates the directory in /data/id11/graintracking/file_xop for this sample
% ------------------------------------------------
%     
%     INPUT:
%       name     = parameters.acq.name
%
%     OUTPUT:
%       xop_dir = the name of the directory created
%

top_xop_dir=fullfile('/data','id11','archive','file_xop');
dataset_xop_dir=fullfile('/data','id11','archive','file_xop', name);
material_xop_dir=fullfile('/data','id11','archive','file_xop', name, material);

% create top level directory if needed
if ~exist(top_xop_dir,'dir')
    [~,msg]=mkdir(top_xop_dir); disp(msg)
end

% now create directory for the dataset    
if ~exist(dataset_xop_dir,'dir')
    % this is the first time - create directory for the dataset
    [~,msg]=mkdir(dataset_xop_dir); disp(msg)
end
 
% now create a sub-directory for this material    
if ~exist(material_xop_dir,'dir')
    % this is the first time - create sub-directory for the material
    [~,msg]=mkdir(material_xop_dir); disp(msg)
    xop_dir=material_xop_dir;
else
    % we already have a directory, so create a new one for this date
    xop_dir=fullfile(material_xop_dir, date());
    [~,msg]=mkdir(xop_dir); disp(msg)
end
 


% for this function, nothing else is needed.  Everything else will go
% inside this folder
%else  
  %  xop_dir=fullfile(xop_dir, name, material, date());
  %  [~,msg]=mkdir(xop_dir); disp(msg)
% elseif ~exist(fullfile(xop_dir,name, material),'dir')
%     xop_dir=fullfile(xop_dir, name, material);
%     [~,msg]=mkdir(xop_dir); disp(msg)


% % read info from xop/diamond
% disp('Please create the .csv or .dat file from Diamond or xop')
% disp('and copy it in this folder:')
% disp(xop_dir)
% disp('using <material>.csv from Diamond or <material>.dat from xop')
% check=inputwdefault('Have you done this? [y/n]','n');
% if strcmpi(check,'y')
%     xop=gtLoadReflections(xop_dir);
%     msg=[];
% else
%     xop=[];
%     msg=['Before running gtPreprocessing, run by yourself this lines:'...
%         '    cd(ANALYSIS_DIR) '...
%         '    load(parameters);'...
%         '    xop_dir=' xop_dir...
%         '    xop=gtLoadReflections(xop_dir);'...
%         '    parameters.xop=xop;'...
%         '    save parameters parameters'];
%     % need more instructions here to complete parameters.cryst
% end
% 
% xop.xop_dir = xop_dir;

end % end of function