function [mis_angle, mis_axis, mis_info] = gtDisorientation(ori1, ori2, symm, varargin)
% GTDISORIENTATION  Easy function to calculate the disorientation angle and axis
%                   between two sets of grains
%
%     [mis_angle, mis_axis, info] = gtDisorientation(ori1, ori2, symm, [varargin])
%     ----------------------------------------------------------------------------
%     INPUT:
%       ori1       = <double>   Orientations for grains set 1
%       ori2       = <double>   Orientations for grains set 2
%       symm       = <struct>   Symmetry operators list from gtCrystGetSymmetryOperators
%
%     OPTIONAL INPUT (varargin as a list of pairs, see parse_pv_pairs.m):
%       input      = <string>   Input orientation ({'rod'}, 'orimat', or 'euler')
%                                 rod:    input array sized 3xN
%                                 orimat: input array sized 3x3xN
%                                 euler:  input array sized 3xN
%                                 gr_id:  grain ids 1xN
%       mode       = <string>   Mode of rotation ({'passive'} or 'active')
%       sort       = <string>   Sort disorientation axis components after taking
%                               absolute values (if empty, do nothing at all)
%                               ({''}, 'ascend' or 'descend')
%       latticepar = <double>   Lattice parameters (1x6 for hexagonal crystals)
%       convention = <string>   Convention used for the unit cell {'Y'}/'X' in
%                               case of hexagonal unit cell
%       constrain_to_sst = <logical>  Constrain the rotation axis to the
%                                     standard stereographic triangle {false}
%
%     OUTPUT:
%       mis_angle  = <double>   Misorientation angles between the 2 grain sets
%       mis_axis   = <double>   Misorientation axes between the 2 grain sets
%       mis_info   = <struct>   Extra info
%
%     Verison 002 24-10-2013 by LNervo, laura.nervo@esrf.fr
%       Added argument 'convention'
%
%     Version 001 27-06-2013 by YGuilhem, yoann.guilhem@esrf.fr

% Set default parameters and parse input parameters from varargin
par.input      = 'rod';
par.mode       = 'passive';
par.sort       = '';
par.latticepar = [];
par.convention = 'X';
par.constrain_to_sst = false;
par.grain      = [];
[par, ~] = parse_pv_pairs(par, varargin);

if (~exist('symm', 'var') || isempty(symm))
    symm = gtCrystGetSymmetryOperators();
end
Os = cat(3, symm.g3);

switch (par.input)
    case 'rod'
        Ngrain1 = size(ori1, 2);
        Ngrain2 = size(ori2, 2);
        oriConversion = @(x) gtMathsRod2OriMat(x);
    case 'orimat'
        Ngrain1 = size(ori1, 3);
        Ngrain2 = size(ori2, 3);
        oriConversion = @(x) x;
    case 'euler'
        Ngrain1 = size(ori1, 2);
        Ngrain2 = size(ori2, 2);
        oriConversion = @(x) gtMathsEuler2OriMat(x);
    case 'gr_id'
        Ngrain1 = size(ori1, 1);
        Ngrain2 = size(ori2, 1);

        if isempty(par.grain)
            grain = load('4_grains/phase_01/index', 'grain');
            par.grain = grain.grain;
        end
        oriConversion = @(x) gtMathsRod2OriMat(par.grain{x}.R_vector');
    case 'quat'
        Ngrain1 = size(ori1, 2);
        Ngrain2 = size(ori2, 2);
        oriConversion = @(x) gtMathsQuat2OriMat(x);
    otherwise
        gtError('gtDisorientation:wrong_input_type', ...
            'Allowed orientation types are ''rod'', ''orimat'', ''quat'' or ''euler''!');
end

if (Ngrain1 < 1 || Ngrain1 ~= Ngrain2)
    gtError('gtDisorientation:wrong_input_size', ...
        'This function needs equal number (N>1) of grains per set!');
end

switch (par.mode)
    case 'passive'
        calcDisorientation = @(x,y) sfPassiveDisorientation(x, y, Os, par.constrain_to_sst);
    case 'active'
        calcDisorientation = @(x,y) sfActiveDisorientation(x.', y.', Os);
    otherwise
        gtError('gtDisorientation:wrong_rotation_mode', ...
            'Allowed rotation modes are only ''passive'' or ''active''!');
end

% Allocate output
mis_angle = zeros(Ngrain1, 1);

% Compute orientation matrices for sets 1 and 2
g1 = oriConversion(ori1);
g1t = permute(g1, [2 1 3]);
g2 = oriConversion(ori2);

% Compute active/passive disorientation angle/axis
if (nargout > 1)
    mis_axis = zeros(Ngrain1, 3);

    tmp_info = sfInfoStructureDefinition(Ngrain1);

    for ii = 1:Ngrain1;
        gAt = g1t(:, :, ii);
        gB = g2(:, :, ii);
        [mis_angle(ii), mis_axis(ii, :), mis_info] = calcDisorientation(gAt, gB);
        
        tmp_info(ii) = gtAddMatFile(tmp_info(ii), mis_info, true);
        tmp_info(ii).mis_axis_sp = mis_axis(ii, :);

        if (~isempty(par.latticepar) && (par.latticepar(6) == 120)) % it must be hexagonal
            mis_axis_hex_cart = gtCart2Hex(mis_axis(ii,:), par.latticepar, par.convention);
            tmp_info(ii).mis_axis_hex_cart = mis_axis_hex_cart;

            if (strcmp(par.mode, 'passive'))
                [mis_axis_hex, mis_axis_hex_int] = gtCrystMiller2FourIndexes(mis_axis(ii,:),'direction');
                tmp_info(ii).mis_axis_hex_int = mis_axis_hex_int;
                tmp_info(ii).mis_axis_hex = mis_axis_hex;
            end
        end
    end
    if (nargout > 2)
        mis_info = tmp_info;
    end
else
    if (strcmp(par.mode, 'passive'))
        for ii = 1:Ngrain1
            gAt = g1t(:, :, ii);
            gB = g2(:, :, ii);
            mis_angle(ii) = calcDisorientation(gAt, gB);
        end
    else
        % This alternative is way faster! Unfortunately it is adhoc, and
        % the whole concept should be changed
        g2t = permute(g2, [2 1 3]);
        num_Os = size(Os, 3);
        mis_angle = zeros(Ngrain1, num_Os);
        for ii = 1:num_Os
            netg = gtMathsMatrixProduct(g2t, gtMathsMatrixProduct(Os(:, :, ii), g1));
            mis_angle(:, ii) = gtMathsOriMat2AngleAxis(netg);
        end
        mis_angle = min(mis_angle, [], 2);
    end
end

if (~isempty(par.sort))
    mis_axis = sort(abs(mis_axis), 2, par.sort);
end

end % end of function

%%% Sub-functions

function info = sfInfoStructureDefinition(num_orientations)
    info = struct( ...
        'g1', [], ...
        'g2', [], ...
        'index_i', [], ...
        'index_j', [], ...
        'index_ij', [], ...
        'symm_i', [], ...
        'symm_j', [], ...
        'netg', [], ...
        'mis_axis_sp', []);

    if (exist('num_orientations', 'var') && ~isempty(num_orientations))
        info(2:num_orientations) = info;
    end
end

function [theta, r, info] = sfPassiveDisorientation(gAt, gB, Os, constrain_to_sst)

    N = size(Os, 3);
    Ost = permute(Os, [2 1 3]);

    % Compute all possible misorientation matrices
    % This part could/should be optimized
    netg = zeros(3, 3, 2 * N ^ 2);
    DgAB = gB * gAt; % equiv. and faster than gB * inv(gA);
    for jj = 1:N
        Oj_DgAB = Os(:, :, jj) * DgAB;
%         for ii = 1:N
%             tmp = Oj_DgAB * Os(:, :, ii)'; % equiv. and faster than Oj_DgAB * inv(Os{ii});
%             netg(:, :, ii + N*(jj-1))   = tmp;
%             netg(:, :, ii + N*(jj-1+N)) = tmp.';
%         end
        tmp = gtMathsMatrixProduct(Oj_DgAB, Ost);
        netg(:, :, N*(jj-1) + (1:N)) = tmp;
        netg(:, :, N*(jj-1+N) + (1:N)) = permute(tmp, [2 1 3]);
    end

    % Convert them into angle/axis rotations
    [all_theta, all_r] = gtMathsOriMat2AngleAxis(netg);

    % Constrain solution to standard stereographic triangle for sst = true
    if (constrain_to_sst)
        % Restrict choice to the fundamental zone
        FZ_indices = find(all_r(1, :) >= all_r(2, :) & all_r(2, :) >= all_r(3, :) & all_r(3, :) >= 0);

        % Get minimal rotation angle and axis
        [theta, ind] = min(all_theta(:, FZ_indices));

        % Convert FZ_index to all_index
        index_ij = FZ_indices(ind);
    else
        [theta, index_ij] = min(all_theta);
    end
    r = all_r(:, index_ij).';

    if (nargout > 2)
        info = sfInfoStructureDefinition();
        info.g1 = gAt';
        info.g2 = gB;
        [info.index_i, jj] = ind2sub([N 2*N], index_ij);
        info.index_j = jj;
        if (info.index_j > N)
            info.index_j = info.index_j - N;
        end
        info.index_ij = index_ij;
        info.symm_i = Os(:, :, info.index_i);
        info.symm_j = Os(:, :, info.index_j);
        info.netg = netg(:, :, index_ij);
    end
end

function [theta, r, info] = sfActiveDisorientation(gA, gBt, Os)

    % Compute all possible misorientation matrices
%     num_symm_ops = size(Os, 3);
%     netg = zeros(3, 3, num_symm_ops);
%     for ii = 1:num_symm_ops
%         netg(:, :, ii) = gB * Os(:, :, ii) * gA'; % equiv. and faster than gB * O{ii} * inv(gA)
%     end
    netg = gtMathsMatrixProduct(gBt, gtMathsMatrixProduct(Os, gA));

    if (nargout == 1)
        all_theta = gtMathsOriMat2AngleAxis(netg);
        theta = min(all_theta(:));
    else
        % Convert them into angle/axis rotations
        [all_theta, all_r] = gtMathsOriMat2AngleAxis(netg);

        % Get minimal rotation angle and axis
        [theta, ind] = min(all_theta(:));
        r = all_r(:, ind).';

        if (nargout > 2)
            info = sfInfoStructureDefinition();
            info.g1 = gA;
            info.g2 = gBt';
            info.index_i  = ind;
            info.index_j  = [];
            info.index_ij = [];
            info.symm_i = Os(:, :, ind);
            info.symm_j = [];
            info.netg = netg(:, :, ind);
        end
    end
end
