function twinInfo = gtCrystDetectTwins(rvectors, ids, phaseID, merge_angle)
% GTDETECTTWINS  
%     twinInfo = gtCrystDetectTwins(rvectors, ids, phaseID, merge_angle)
%     ------------------------------------------------------------------
%     rvectors    = <cell>    R-vectors for datasets (one column for one
%                             dataset)
%     ids         = <cell>    indexes of Rvectors to consider (each column
%                             for each dataset)
%     phaseID     = <double>  phase ID
%     merge_angle = <double>  Minimum angle to consider grains as
%                             different; below this number the two grains 
%                             are the same

if size(rvectors,2) == 2 
    % two columns = two datasets
    refRvectors = rvectors{1};
    testRvectors = rvectors{2};
elseif size(rvectors,2) == 1
    refRvectors = rvectors{1};
    testRvectors = rvectors{1};
end

if ~iscell(ids) && size(ids,2) == 2
    ids = arrayfun(@(num) {num}, ids);
end

%[sampleInfo, list] = obj.initialiseAssembling(phaseID);

twinInfo = [];
for ii=1:size(ids,1);
    refRvector = refRvectors(ids{ii,1},:);
    testids = ids{ii,2};
    for jj = 1:size(testids,2)
        % test for being a twin
        testRvector = testRvectors(testids(jj),:);
        [tmpinfo,~] = gtCrystTwinTest(refRvector, testRvector, phaseID, 'merge_angle', merge_angle);
        if tmpinfo.mis_angle > 90 
            tmpinfo.mis_angle = 180 - tmpinfo.mis_angle;
        end
        tmpinfo.grainID_1 = ids{ii,1};
        tmpinfo.grainID_2 = testids(jj);
        twinInfo{ii,jj} = tmpinfo;         
    end
end

end % end function
