function cif = gtReadCif(name)
% GTREADCIF  Reads info from .dif files
%     cif = gtReadCif(name)
%     ---------------------
%     Version 001 by LNervo
%
%     Sub-functions:
%       sfReadCifLattice
%       sfReadCifSymmetry
%       sfReadCifSpacegroup
%       sfReadCifHM
%       sfReadCifCell


latticepar = sfReadCifLattice(name);
op         = sfReadCifSymmetry(name);
%op=textscan(op,'%s','delimiter',char(10));
%op=op{1};
sg         = sfReadCifSpacegroup(name);
HM         = sfReadCifHM(name);
cellname   = sfReadCifCell(name);


cif.latticepar      = latticepar;
cif.opsym           = op;
cif.spacegroup      = sg;
cif.hermann_mauguin = HM;
cif.crystal_system  = cellname;

%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Sub-functions
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    function latticepar=sfReadCifLattice(name)
      a     = grep('_cell_length_a',name,' ',2);        a     = str2num(strtrim(a));
      b     = grep('_cell_length_b',name,' ',2);        b     = str2num(strtrim(b));
      c     = grep('_cell_length_c',name,' ',2);        c     = str2num(strtrim(c));
      alpha = grep('_cell_angle_alpha',name,' ',2);     alpha = str2num(strtrim(alpha));
      beta  = grep('_cell_angle_beta',name,' ',2);      beta  = str2num(strtrim(beta));
      gamma = grep('_cell_angle_gamma',name,' ',2);     gamma = str2num(strtrim(gamma));

      latticepar = [a b c alpha beta gamma];
    end

    function HM=sfReadCifHM(name)
      HM = grep('_symmetry_space_group_name_H-M',name,' ',2);
      HM = strtrim(HM);
      HM = strrep(HM,'''','');
      HM(2:end) = lower(HM(2:end));
    end

    function cellname = sfReadCifCell(name)
      cellname = grep('_symmetry_cell_setting',name,' ',2);
      cellname = strtrim(cellname);
    end

    function sg = sfReadCifSpacegroup(name)
      sg = grep('_symmetry_Int_Tables_number',name,' ',2);
      sg = str2num(strtrim(sg));
    end

    function op = sfReadCifSymmetry(name)
      op = grep('x',name);
      op = strtrim(op);
      op = op(strfind(op,'_xyz')+4:end);
      op = strtrim(op);
      op = strrep(op,char(32),'');
      op = textscan(op,'%s','delimiter',char(10));
      op = op{1};
    end
end % end of function

