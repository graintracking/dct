function [xop,msg] = gtCrystXopInitialize(name, material)
% GTCRYSTXOPINITIALIZE  Initializes the crystallographic stuff in the
%                       parameters and load xop reflections
%     [xop,msg] = gtCrystXopInitialize(name, material)
%     ------------------------------------------------
%     
%     INPUT:
%       name     = parameters.acq.name
%       material = phase name
%
%     OUTPUT:
%       xop = xop structure with reflections <struct>
%       msg = message if the xop file has not been copied yet <string>


xop_dir=fullfile('/data','id11','graintracking','file_xop');
if ~exist(xop_dir,'dir')
    [~,msg]=mkdir(xop_dir); disp(msg)
elseif ~exist(fullfile(xop_dir,name),'dir')
    xop_dir=fullfile(xop_dir,name);
    [~,msg]=mkdir(xop_dir); disp(msg)
elseif ~exist(fullfile(xop_dir,name, material),'dir')
    xop_dir=fullfile(xop_dir, name, material);
    [~,msg]=mkdir(xop_dir); disp(msg)
else
    xop_dir=fullfile(xop_dir, name, material, date());
    [~,msg]=mkdir(xop_dir); disp(msg)
end


% read info from xop/diamond
disp('Please create the .csv or .dat file from Diamond or xop')
disp('and copy it in this folder:')
disp(xop_dir)
disp('using <material>.csv from Diamond or <material>.dat from xop')
check=inputwdefault('Have you done this? [y/n]','n');
if strcmpi(check,'y')
    xop=gtLoadReflections(xop_dir);
    msg=[];
else
    xop=[];
    msg=['Before running gtPreprocessing, run by yourself this lines:'...
        '    cd(ANALYSIS_DIR) '...
        '    load(parameters);'...
        '    xop_dir=' xop_dir...
        '    xop=gtLoadReflections(xop_dir);'...
        '    parameters.xop=xop;'...
        '    save parameters parameters'];
    % need more instructions here to complete parameters.cryst
end

xop.xop_dir = xop_dir;

end % end of function