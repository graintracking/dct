function [info, sigmaAnd] = gtCrystTwinTest(R1, R2, phaseID, varargin)
% GTCRYSTTWINTEST  Tests if the two orientations are from two twins
%     [info, sigma] = gtCrystTwinTest(R1, R2, [phaseID], varargin)
%     ------------------------------------------------------------
%     INPUT:
%       R1 and R2   = Rodriguez vectors of the two different orientations
%       phaseID     = current phase
%
%     OPTIONAL INPUT (parse by pairs):
%       symm        = symmetry operators from gtCrystGetSymmetryOperators {[]}
%       sigmas      = sigma values from gtCrystGetSigmas {[]}
%       merge_angle = a threshold to decide if the angle is small {1}
%       convention  = hcp crystal axes convention {'X'}
%       tol_angle   = misorientation tolerance angle used in Brandon criterion {15}
%       tol_axis    = tolerance angle for deviation from theoretical CSL rotation axis {5}
%       input       = type of input can be {R_vectors}, gr_id
%     Version 001 13-11-2013 by LNervo

if (~exist('phaseID','var') || isempty(phaseID))
    phaseID = 1;
end

conf.symm   = [];
conf.sigmas = [];
conf.active_sigmas = [];
conf.merge_angle = 1;
conf.convention = 'X';
conf.parameters = [];
conf.constrain_to_sst = true;
conf.tol_angle = 15;
conf.tol_axis = 5;
conf = parse_pv_pairs(conf, varargin);

if (isempty(conf.parameters))
    conf.parameters = gtLoadParameters();
end
if numel(R1) == 1
    indexgrainModel = fullfile(conf.parameters.acq.dir, '4_grains', 'phase_%02d', 'index.mat');
    fprintf('\nLoading indexing results...\n');
    grain = [];
    load(sprintf(indexgrainModel, phaseID), 'grain');
    R1 = grain{R1}.R_vector;
    R2 = grain{R2}.R_vector;
end
crystal_system = conf.parameters.cryst(phaseID).crystal_system;
latticepar = conf.parameters.cryst(phaseID).latticepar;
spacegroup = conf.parameters.cryst(phaseID).spacegroup;
material = conf.parameters.cryst(phaseID).material;

if (isempty(conf.symm))
    conf.symm = gtCrystGetSymmetryOperators(crystal_system, spacegroup);
end
if (isempty(conf.sigmas))
    conf.sigmas = gtCrystGetSigmas(crystal_system, latticepar, conf.convention, material, conf.tol_angle);
end
if (isempty(conf.active_sigmas) && isfield(conf.parameters.cryst(phaseID), 'active_sigmas'))
    conf.active_sigmas = conf.parameters.cryst(phaseID).active_sigmas;
else
    conf.active_sigmas = true(size(conf.sigmas, 1), 1);
end
sigmas = conf.sigmas(conf.active_sigmas, :);

sigmaAnd = []; % default sigma
theo_mis_angle = [];
theo_mis_axis = [];
switch(crystal_system)
    case 'cubic'
        [mis_angle, mis_axis, mis_info] = gtDisorientation(R1.', R2.', conf.symm, ...
            'latticepar', latticepar, 'sort', 'descend', ...
            'constrain_to_sst', conf.constrain_to_sst, varargin{:});
        if (mis_angle > 90)
            mis_angle = 180 - mis_angle;
        end

        sigmaOr = [];
        if (~isempty(mis_axis))
            % determine sigma type
            angle_ok = abs(mis_angle - sigmas(:, 2)) < sigmas(:, 6);
            axis_ok  = abs(acosd(dot(repmat(mis_axis,size(sigmas,1),1), sigmas(:, 3:5), 2))) < conf.tol_axis;
            sigmaOr  = sigmas(angle_ok | axis_ok, :);
            sigmaAnd = sigmas(angle_ok & axis_ok, :);

            theo_mis_angle = sigmaAnd(:, 2);
            theo_mis_axis = sigmaAnd(:, 3:5);
        end
    case 'tetragonal'
        [mis_angle, mis_axis, mis_info] = gtDisorientation(R1.', R2.', conf.symm, ...
            'latticepar', latticepar, 'sort', 'descend', ...
            'constrain_to_sst', conf.constrain_to_sst, varargin{:});
        if (mis_angle > 90)
            mis_angle = 180 - mis_angle;
        end

        sigmaOr = [];
        if (~isempty(mis_axis))
            % determine sigma type
            angle_ok = abs(mis_angle - sigmas(:, 2)) < sigmas(:, 6);
            axis_ok  = abs(acosd(dot(repmat(mis_axis,size(sigmas,1),1), sigmas(:, 3:5), 2))) < conf.tol_axis;
            sigmaOr  = sigmas(angle_ok | axis_ok, :);
            sigmaAnd = sigmas(angle_ok & axis_ok, :);

            theo_mis_angle = sigmaAnd(:, 2);
            theo_mis_axis = sigmaAnd(:, 3:5);
        end
    case 'hexagonal'
        % In the case of hexagonal crystals
        [mis_angle, mis_axis, mis_info] = gtDisorientation(R1.', R2.', conf.symm, ...
            'latticepar', latticepar, 'convention', conf.convention, ...
            'constrain_to_sst', conf.constrain_to_sst, varargin{:});
        if (mis_angle > 90)
            mis_angle = 180 - mis_angle;
        end

        sigmaOr = [];
        if (~isempty(mis_axis))
            % determine sigma type
            angle_ok = abs(mis_angle - sigmas(:, 2)) < sigmas(:, 7);
            axis_ok  = abs(acosd(dot(repmat(mis_axis, size(sigmas,1),1), sigmas(:, 8:10), 2))) < conf.tol_axis;
            sigmaOr  = sigmas(angle_ok | axis_ok, :);
            sigmaAnd = sigmas(angle_ok & axis_ok, :);
            theo_mis_angle = sigmaAnd(:, 2);
            theo_mis_axis = sigmaAnd(:, 8:10);
        end
    otherwise
        disp('Crystal system is not currently supported... Quitting')
        return
end

% add low angle (merge grains) criteria
if (mis_angle < conf.merge_angle)
    sigmaAnd = -1;
elseif isempty(sigmaAnd)
    sigmaAnd = 0;
end

info.R1           = R1;
info.R2           = R2;
info.mis_angle    = mis_angle;
info.mis_axis     = mis_axis;
info.sigmaOr      = sigmaOr;
info.sigmaAnd     = sigmaAnd(:, 1);
info.sigmas       = sigmas;
info.theo_mis_angle = theo_mis_angle;
info.theo_mis_axis = theo_mis_axis;
info.merge_angle  = conf.merge_angle;
info.convention   = conf.convention;
info.tol_axis     = conf.tol_axis;
info = gtAddMatFile(info, mis_info, true, true);

end
