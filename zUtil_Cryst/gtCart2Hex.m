function uvtw = gtCart2Hex(UVW, latticepar, convention)
% GTCART2HEX  Converts a direction in Cartesian crystal coordinates (UVW) to the
%             Miller-Bravais indexes in the direct space (uvtw)
%
%     uvtw = gtCart2Hex(UVW, latticepar, convention)
%     ----------------------------------------------
%     It follows the X-convention using right handed CS.
%     From (UVW) direction in cartesian space to (uvtw) plane normal in hexagonal real space 
%
%     INPUT:
%       UVW        = <double>   direction in cartesian coordinates (N,3)
%       latticepar = <double>   lattice parameters (row vector 1,6)
%       convention = <string>   hexagonal unit cell convention {'X'}/'Y'
%
%     OUTPUT:
%       uvtw       = <double>   plane normal in hexagonal direct space described by 
%                               Miller-Bravais indexes (N,4)
%
%     Version 002 25-10-2013 by LNervo


if ~exist('convention','var') || isempty(convention)
    convention = 'X';
end

if strcmpi(convention, 'X')
% rotates counterclockwise
    UVW = rotateVectors(UVW,'angle',30,'axis',[0 0 1]);
end

uvtw = zeros(size(UVW,1), 4);

% change to hexagonal axes
uvtw(:,1) = UVW(:,1) - (UVW(:,2)/sqrt(3));
uvtw(:,2) = UVW(:,2)*2/sqrt(3);
uvtw(:,3) = -(uvtw(:,1) + uvtw(:,2));
uvtw(:,4) = UVW(:,3);

% allow for unit cell parameters
uvtw(:,1:3) = uvtw(:,1:3)*(latticepar(1)*sqrt(3)/2);
uvtw(:,4)   = uvtw(:,4)*latticepar(3);

end % end of function
