function slipSystems = gtGetSlipSystems(slipFamilies)
% Based on Z-set slip systems definition

slipSystems = struct;

for ifam=1:length(slipFamilies)
    switch slipFamilies{ifam}
        case {'fcc', 'FCC', 'octahedral'}

            fcc(1).n  = [ 1  1  1]; fcc(1).l  = [-1  0  1]; % Bd / B4
            fcc(2).n  = [ 1  1  1]; fcc(2).l  = [ 0 -1  1]; % Ba / B2
            fcc(3).n  = [ 1  1  1]; fcc(3).l  = [-1  1  0]; % Bc / B5

            fcc(4).n  = [ 1 -1  1]; fcc(4).l  = [-1  0  1]; % Db / D4
            fcc(5).n  = [ 1 -1  1]; fcc(5).l  = [ 0  1  1]; % Dc / D1
            fcc(6).n  = [ 1 -1  1]; fcc(6).l  = [ 1  1  0]; % Da / D6

            fcc(7).n  = [-1  1  1]; fcc(7).l  = [ 0 -1  1]; % Ab / A2
            fcc(8).n  = [-1  1  1]; fcc(8).l  = [ 1  1  0]; % Ad / A6
            fcc(9).n  = [-1  1  1]; fcc(9).l  = [ 1  0  1]; % Ac / A3

            fcc(10).n = [ 1  1 -1]; fcc(10).l = [-1  1  0]; % Cb / C5
            fcc(11).n = [ 1  1 -1]; fcc(11).l = [ 1  0  1]; % Ca / C3
            fcc(12).n = [ 1  1 -1]; fcc(12).l = [ 0  1  1]; % Cd / C1

            slipSystems.fcc = fcc;

        case {'bcc', 'BCC'}

            bcc(1).n  = [-1  0  1]; bcc(1).l  = [ 1  1  1]; % Bd / B4
            bcc(2).n  = [ 0 -1  1]; bcc(2).l  = [ 1  1  1]; % Ba / B2
            bcc(3).n  = [-1  1  0]; bcc(3).l  = [ 1  1  1]; % Bc / B5

            bcc(4).n  = [-1  0  1]; bcc(4).l  = [ 1 -1  1]; % Db / D4
            bcc(5).n  = [ 0  1  1]; bcc(5).l  = [ 1 -1  1]; % Dc / D1
            bcc(6).n  = [ 1  1  0]; bcc(6).l  = [ 1 -1  1]; % Da / D6

            bcc(7).n  = [ 0 -1  1]; bcc(7).l  = [-1  1  1]; % Ab / A2
            bcc(8).n  = [ 1  1  0]; bcc(8).l  = [-1  1  1]; % Ad / A6
            bcc(9).n  = [ 1  0  1]; bcc(9).l  = [-1  1  1]; % Ac / A3

            bcc(10).n = [-1  1  0]; bcc(10).l = [ 1  1 -1]; % Cb / C5
            bcc(11).n = [ 1  0  1]; bcc(11).l = [ 1  1 -1]; % Ca / C3
            bcc(12).n = [ 0  1  1]; bcc(12).l = [ 1  1 -1]; % Cd / C1

            slipSystems.bcc = bcc;

        case {'bcc112', 'BCC112'}

            bcc112(1).n  = [ 1  1  2]; bcc112(1).l  = [  1  1 -1];
            bcc112(2).n  = [-1  1  2]; bcc112(2).l  = [  1 -1  1];
            bcc112(3).n  = [ 1 -1  2]; bcc112(3).l  = [ -1  1  1];

            bcc112(4).n  = [ 1  1 -2]; bcc112(4).l  = [  1  1  1];
            bcc112(5).n  = [ 1  2  1]; bcc112(5).l  = [  1 -1  1];
            bcc112(6).n  = [-1  2  1]; bcc112(6).l  = [  1  1 -1];

            bcc112(7).n  = [ 1 -2  1]; bcc112(7).l  = [  1  1  1];
            bcc112(8).n  = [ 1  2 -1]; bcc112(8).l  = [ -1  1  1];
            bcc112(9).n  = [ 2  1  1]; bcc112(9).l  = [ -1  1  1];

            bcc112(10).n = [-2  1  1]; bcc112(10).l = [  1  1  1];
            bcc112(11).n = [ 2 -1  1]; bcc112(11).l = [  1  1 -1];
            bcc112(12).n = [ 2  1 -1]; bcc112(12).l = [  1 -1  1];

            slipSystems.bcc112 = bcc112;

        case {'bcc123', 'BCC123'}

            bcc123(1).n  = [ 1  2  3]; bcc123(1).l  = [ 1  1 -1];
            bcc123(2).n  = [-1  2  3]; bcc123(2).l  = [ 1 -1  1];
            bcc123(3).n  = [ 1 -2  3]; bcc123(3).l  = [-1  1  1];

            bcc123(4).n  = [ 1  2 -3]; bcc123(4).l  = [ 1  1  1];
            bcc123(5).n  = [ 3  1  2]; bcc123(5).l  = [-1  1  1];
            bcc123(6).n  = [-3  1  2]; bcc123(6).l  = [ 1  1  1];

            bcc123(7).n  = [ 3 -1  2]; bcc123(7).l  = [ 1  1 -1];
            bcc123(8).n  = [ 3  1 -2]; bcc123(8).l  = [ 1 -1  1];
            bcc123(9).n  = [ 2  3  1]; bcc123(9).l  = [ 1 -1  1];

            bcc123(10).n = [-2  3  1]; bcc123(10).l = [ 1  1 -1];
            bcc123(11).n = [ 2 -3  1]; bcc123(11).l = [ 1  1  1];
            bcc123(12).n = [ 2  3 -1]; bcc123(12).l = [-1  1  1];

            bcc123(13).n = [ 1  3  2]; bcc123(13).l = [ 1 -1  1];
            bcc123(14).n = [-1  3  2]; bcc123(14).l = [ 1  1 -1];
            bcc123(15).n = [ 1 -3  2]; bcc123(15).l = [ 1  1  1];

            bcc123(16).n = [ 1  3 -2]; bcc123(16).l = [-1  1  1];
            bcc123(17).n = [ 2  1  3]; bcc123(17).l = [ 1  1 -1];
            bcc123(18).n = [-2  1  3]; bcc123(18).l = [ 1 -1  1];

            bcc123(19).n = [ 2 -1  3]; bcc123(19).l = [-1  1  1];
            bcc123(20).n = [ 2  1 -3]; bcc123(20).l = [ 1  1  1];
            bcc123(21).n = [ 3  2  1]; bcc123(21).l = [-1  1  1];

            bcc123(22).n = [-3  2  1]; bcc123(22).l = [ 1  1  1];
            bcc123(23).n = [ 3 -2  1]; bcc123(23).l = [ 1  1 -1];
            bcc123(24).n = [ 3  2 -1]; bcc123(24).l = [ 1 -1  1];

            slipSystems.bcc123 = bcc123;

        % slip / twin systems for HCP crystals (ref. hexaSchmid app)
        case {'hcp001'} % basal <a>
            hcp = [];
            hcp(end+1).n = [ 0  0  0  1]; hcp(end).l = [ 2 -1 -1  0];%1
            hcp(end+1).n = [ 0  0  0  1]; hcp(end).l = [-2  1  1  0];%4
            hcp(end+1).n = [ 0  0  0  1]; hcp(end).l = [ 1  1 -2  0];%2
            hcp(end+1).n = [ 0  0  0  1]; hcp(end).l = [-1 -1  2  0];%5
            hcp(end+1).n = [ 0  0  0  1]; hcp(end).l = [-1  2 -1  0];%3
            hcp(end+1).n = [ 0  0  0  1]; hcp(end).l = [ 1 -2  1  0];%6

            slipSystems.hcp001 = hcp;

        case {'hcp100'} % prismatic planes <a>
            hcp = [];
            hcp(end+1).n = [ 1  0 -1  0]; hcp(end).l = [-1  2 -1  0];
            hcp(end+1).n = [ 1  0 -1  0]; hcp(end).l = [ 1 -2  1  0];
            hcp(end+1).n = [ 0  1 -1  0]; hcp(end).l = [ 2 -1 -1  0];
            hcp(end+1).n = [ 0  1 -1  0]; hcp(end).l = [-2  1  1  0];
            hcp(end+1).n = [-1  1  0  0]; hcp(end).l = [ 1  1 -2  0];
            hcp(end+1).n = [-1  1  0  0]; hcp(end).l = [-1 -1  2  0];
            hcp(end+1).n = [-1  0  1  0]; hcp(end).l = [-1  2 -1  0];
            hcp(end+1).n = [-1  0  1  0]; hcp(end).l = [ 1 -2  1  0];
            hcp(end+1).n = [ 0 -1  1  0]; hcp(end).l = [ 2 -1 -1  0];
            hcp(end+1).n = [ 0 -1  1  0]; hcp(end).l = [-2  1  1  0];
            hcp(end+1).n = [ 1 -1  0  0]; hcp(end).l = [ 1  1 -2  0];
            hcp(end+1).n = [ 1 -1  0  0]; hcp(end).l = [-1 -1  2  0];

            slipSystems.hcp100 = hcp;

        case {'hcp101'} % pyramidal planes 1st, 1st <a>
            hcp = [];
            hcp(end+1).n = [ 1  0 -1  1]; hcp(end).l = [-1  2 -1  0];
            hcp(end+1).n = [ 1  0 -1  1]; hcp(end).l = [ 1 -2  1  0];
            hcp(end+1).n = [ 0  1 -1  1]; hcp(end).l = [ 2 -1 -1  0];
            hcp(end+1).n = [ 0  1 -1  1]; hcp(end).l = [-2  1  1  0];
            hcp(end+1).n = [-1  1  0  1]; hcp(end).l = [ 1  1 -2  0];
            hcp(end+1).n = [-1  1  0  1]; hcp(end).l = [-1 -1  2  0];
            hcp(end+1).n = [-1  0  1  1]; hcp(end).l = [-1  2 -1  0];
            hcp(end+1).n = [-1  0  1  1]; hcp(end).l = [ 1 -2  1  0];
            hcp(end+1).n = [ 0 -1  1  1]; hcp(end).l = [ 2 -1 -1  0];
            hcp(end+1).n = [ 0 -1  1  1]; hcp(end).l = [-2  1  1  0];
            hcp(end+1).n = [ 1 -1  0  1]; hcp(end).l = [ 1  1 -2  0];
            hcp(end+1).n = [ 1 -1  0  1]; hcp(end).l = [-1 -1  2  0];

            slipSystems.hcp101 = hcp;           

        case {'hcp102'} % pyramidal planes 1st, 2nd <a>
            hcp = [];
            hcp(end+1).n = [ 1  0 -1  2]; hcp(end).l = [-1  2 -1  0];
            hcp(end+1).n = [ 1  0 -1  2]; hcp(end).l = [ 1 -2  1  0];
            hcp(end+1).n = [ 0  1 -1  2]; hcp(end).l = [ 2 -1 -1  0];
            hcp(end+1).n = [ 0  1 -1  2]; hcp(end).l = [-2  1  1  0];
            hcp(end+1).n = [-1  1  0  2]; hcp(end).l = [ 1  1 -2  0];
            hcp(end+1).n = [-1  1  0  2]; hcp(end).l = [-1 -1  2  0];
            hcp(end+1).n = [-1  0  1  2]; hcp(end).l = [-1  2 -1  0];
            hcp(end+1).n = [-1  0  1  2]; hcp(end).l = [ 1 -2  1  0];
            hcp(end+1).n = [ 0 -1  1  2]; hcp(end).l = [ 2 -1 -1  0];
            hcp(end+1).n = [ 0 -1  1  2]; hcp(end).l = [-2  1  1  0];
            hcp(end+1).n = [ 1 -1  0  2]; hcp(end).l = [ 1  1 -2  0];
            hcp(end+1).n = [ 1 -1  0  2]; hcp(end).l = [-1 -1  2  0];

            slipSystems.hcp102 = hcp;           

        case {'hcp112'} % pyramidal planes 2nd, 2nd <a>
            hcp = [];
            hcp(end+1).n = [ 1  1 -2  2]; hcp(end).l = [-1  1  0  0];
            hcp(end+1).n = [ 1  1 -2  2]; hcp(end).l = [ 1 -1  0  0];
            hcp(end+1).n = [-1  2 -1  2]; hcp(end).l = [-1  0  1  0];
            hcp(end+1).n = [-1  2 -1  2]; hcp(end).l = [ 1  0 -1  0];
            hcp(end+1).n = [-2  1  1  2]; hcp(end).l = [ 0 -1  1  0];
            hcp(end+1).n = [-2  1  1  2]; hcp(end).l = [ 0  1 -1  0];
            hcp(end+1).n = [-1 -1  2  2]; hcp(end).l = [-1  1  0  0];
            hcp(end+1).n = [-1 -1  2  2]; hcp(end).l = [ 1 -1  0  0];
            hcp(end+1).n = [ 1 -2  1  2]; hcp(end).l = [-1  0  1  0];
            hcp(end+1).n = [ 1 -2  1  2]; hcp(end).l = [ 1  0 -1  0];
            hcp(end+1).n = [ 2 -1 -1  2]; hcp(end).l = [ 0 -1  1  0];
            hcp(end+1).n = [ 2 -1 -1  2]; hcp(end).l = [ 0  1 -1  0];

            slipSystems.hcp112 = hcp;          
            
        case {'hcp100ac'} % prismatic planes <a+c>
            hcp = [];
            hcp(end+1).n = [ 1  0 -1  0]; hcp(end).l = [-1  2 -1  3];
            hcp(end+1).n = [ 1  0 -1  0]; hcp(end).l = [ 1 -2  1  3];
            hcp(end+1).n = [ 1  0 -1  0]; hcp(end).l = [ 1 -2  1 -3];
            hcp(end+1).n = [ 1  0 -1  0]; hcp(end).l = [-1  2 -1 -3];

            hcp(end+1).n = [ 0  1 -1  0]; hcp(end).l = [-2  1  1  3];
            hcp(end+1).n = [ 0  1 -1  0]; hcp(end).l = [ 2 -1 -1  3];
            hcp(end+1).n = [ 0  1 -1  0]; hcp(end).l = [ 2 -1 -1 -3];
            hcp(end+1).n = [ 0  1 -1  0]; hcp(end).l = [-2  1  1 -3];

            hcp(end+1).n = [-1  1  0  0]; hcp(end).l = [-1 -1  2  3];
            hcp(end+1).n = [-1  1  0  0]; hcp(end).l = [ 1  1 -2  3];
            hcp(end+1).n = [-1  1  0  0]; hcp(end).l = [ 1  1 -2 -3];
            hcp(end+1).n = [-1  1  0  0]; hcp(end).l = [-1 -1  2 -3];

            hcp(end+1).n = [-1  0  1  0]; hcp(end).l = [-1  2 -1  3];
            hcp(end+1).n = [-1  0  1  0]; hcp(end).l = [ 1 -2  1  3];
            hcp(end+1).n = [-1  0  1  0]; hcp(end).l = [ 1 -2  1 -3];
            hcp(end+1).n = [-1  0  1  0]; hcp(end).l = [-1  2 -1 -3];

            hcp(end+1).n = [ 0 -1  1  0]; hcp(end).l = [-2  1  1  3];
            hcp(end+1).n = [ 0 -1  1  0]; hcp(end).l = [ 2 -1 -1  3];
            hcp(end+1).n = [ 0 -1  1  0]; hcp(end).l = [ 2 -1 -1 -3];
            hcp(end+1).n = [ 0 -1  1  0]; hcp(end).l = [-2  1  1 -3];

            hcp(end+1).n = [ 1 -1  0  0]; hcp(end).l = [-1 -1  2  3];
            hcp(end+1).n = [ 1 -1  0  0]; hcp(end).l = [ 1  1 -2  3];
            hcp(end+1).n = [ 1 -1  0  0]; hcp(end).l = [ 1  1 -2 -3];
            hcp(end+1).n = [ 1 -1  0  0]; hcp(end).l = [-1 -1  2 -3];

            slipSystems.hcp100ac = hcp;

        case {'hcp101ac'} % pyramidal planes 1st, 1st <a+c>
            hcp = [];
            hcp(end+1).n = [ 1  0 -1  1]; hcp(end).l = [-2  1  1  3];
            hcp(end+1).n = [ 1  0 -1  1]; hcp(end).l = [-1 -1  2  3];
            hcp(end+1).n = [ 1  0 -1  1]; hcp(end).l = [ 2 -1 -1 -3];
            hcp(end+1).n = [ 1  0 -1  1]; hcp(end).l = [ 1  1 -2 -3];

            hcp(end+1).n = [ 0  1 -1  1]; hcp(end).l = [-1 -1  2  3];
            hcp(end+1).n = [ 0  1 -1  1]; hcp(end).l = [ 1 -2  1  3];
            hcp(end+1).n = [ 0  1 -1  1]; hcp(end).l = [ 1  1 -2 -3];
            hcp(end+1).n = [ 0  1 -1  1]; hcp(end).l = [-1  2 -1 -3];

            hcp(end+1).n = [-1  1  0  1]; hcp(end).l = [ 1 -2  1  3];
            hcp(end+1).n = [-1  1  0  1]; hcp(end).l = [ 2 -1 -1  3];
            hcp(end+1).n = [-1  1  0  1]; hcp(end).l = [-1  2 -1 -3];
            hcp(end+1).n = [-1  1  0  1]; hcp(end).l = [-2  1  1 -3];

            hcp(end+1).n = [-1  0  1  1]; hcp(end).l = [ 2 -1 -1  3];
            hcp(end+1).n = [-1  0  1  1]; hcp(end).l = [ 1  1 -2  3];
            hcp(end+1).n = [-1  0  1  1]; hcp(end).l = [-2  1  1 -3];
            hcp(end+1).n = [-1  0  1  1]; hcp(end).l = [-1 -1  2 -3];

            hcp(end+1).n = [ 0 -1  1  1]; hcp(end).l = [-1  2 -1  3];
            hcp(end+1).n = [ 0 -1  1  1]; hcp(end).l = [ 1  1 -2  3];
            hcp(end+1).n = [ 0 -1  1  1]; hcp(end).l = [ 1 -2  1 -3];
            hcp(end+1).n = [ 0 -1  1  1]; hcp(end).l = [-1 -1  2 -3];

            hcp(end+1).n = [ 1 -1  0  1]; hcp(end).l = [-1  2 -1  3];
            hcp(end+1).n = [ 1 -1  0  1]; hcp(end).l = [-2  1  1  3];
            hcp(end+1).n = [ 1 -1  0  1]; hcp(end).l = [ 1 -2  1 -3];
            hcp(end+1).n = [ 1 -1  0  1]; hcp(end).l = [ 2 -1 -1 -3];

            slipSystems.hcp101ac = hcp;

        case {'hcp111ac'} % pyramidal planes 2nd, 1st <a+c>
            hcp = [];
            hcp(end+1).n = [ 1  1 -2  1]; hcp(end).l = [-2  1  1  3];
            hcp(end+1).n = [ 1  1 -2  1]; hcp(end).l = [ 1 -2  1  3];
            hcp(end+1).n = [ 1  1 -2  1]; hcp(end).l = [ 2 -1 -1 -3];
            hcp(end+1).n = [ 1  1 -2  1]; hcp(end).l = [-1  2 -1 -3];

            hcp(end+1).n = [-1  2 -1  1]; hcp(end).l = [-1 -1  2  3];
            hcp(end+1).n = [-1  2 -1  1]; hcp(end).l = [ 2 -1 -1  3];
            hcp(end+1).n = [-1  2 -1  1]; hcp(end).l = [ 1  1 -2 -3];
            hcp(end+1).n = [-1  2 -1  1]; hcp(end).l = [-2  1  1 -3];

            hcp(end+1).n = [-2  1  1  1]; hcp(end).l = [ 1 -2  1  3];
            hcp(end+1).n = [-2  1  1  1]; hcp(end).l = [ 1  1 -2  3];
            hcp(end+1).n = [-2  1  1  1]; hcp(end).l = [-1  2 -1 -3];
            hcp(end+1).n = [-2  1  1  1]; hcp(end).l = [-1 -1  2 -3];

            hcp(end+1).n = [-1 -1  2  1]; hcp(end).l = [-1  2 -1  3];
            hcp(end+1).n = [-1 -1  2  1]; hcp(end).l = [ 2 -1 -1  3];
            hcp(end+1).n = [-1 -1  2  1]; hcp(end).l = [ 1 -2  1 -3];
            hcp(end+1).n = [-1 -1  2  1]; hcp(end).l = [-2  1  1 -3];

            hcp(end+1).n = [ 1 -2  1  1]; hcp(end).l = [-2  1  1  3];
            hcp(end+1).n = [ 1 -2  1  1]; hcp(end).l = [ 1  1 -2  3];
            hcp(end+1).n = [ 1 -2  1  1]; hcp(end).l = [ 2 -1 -1 -3];
            hcp(end+1).n = [ 1 -2  1  1]; hcp(end).l = [-1 -1  2 -3];

            hcp(end+1).n = [ 2 -1 -1  1]; hcp(end).l = [-1  2 -1  3];
            hcp(end+1).n = [ 2 -1 -1  1]; hcp(end).l = [-1 -1  2  3];
            hcp(end+1).n = [ 2 -1 -1  1]; hcp(end).l = [ 1 -2  1 -3];
            hcp(end+1).n = [ 2 -1 -1  1]; hcp(end).l = [ 1  1 -2 -3];

            slipSystems.hcp111ac = hcp;

        case {'hcp112ac'} % pyramidal planes 2nd, 2nd <a+c>
            hcp = [];
            hcp(end+1).n = [ 1  1 -2  2]; hcp(end).l = [-1 -1  2  3];
            hcp(end+1).n = [ 1  1 -2  2]; hcp(end).l = [ 1  1 -2 -3];
            hcp(end+1).n = [-1  2 -1  2]; hcp(end).l = [ 1 -2  1  3];
            hcp(end+1).n = [-1  2 -1  2]; hcp(end).l = [-1  2 -1 -3];
            hcp(end+1).n = [-2  1  1  2]; hcp(end).l = [ 2 -1 -1  3];
            hcp(end+1).n = [-2  1  1  2]; hcp(end).l = [-2  1  1 -3];
            hcp(end+1).n = [-1 -1  2  2]; hcp(end).l = [ 1  1 -2  3];
            hcp(end+1).n = [-1 -1  2  2]; hcp(end).l = [-1 -1  2 -3];
            hcp(end+1).n = [ 1 -2  1  2]; hcp(end).l = [-1  2 -1  3];
            hcp(end+1).n = [ 1 -2  1  2]; hcp(end).l = [ 1 -2  1 -3];
            hcp(end+1).n = [ 2 -1 -1  2]; hcp(end).l = [-2  1  1  3];
            hcp(end+1).n = [ 2 -1 -1  2]; hcp(end).l = [ 2 -1 -1 -3];

            slipSystems.hcp112ac = hcp;          

        case {'hcp102twin'} % TT1
            hcp = [];
            hcp(end+1).n = [ 1  0 -1  2]; hcp(end).l = [-1  0  1  1];
            hcp(end+1).n = [ 0  1 -1  2]; hcp(end).l = [ 0 -1  1  1];
            hcp(end+1).n = [-1  1  0  2]; hcp(end).l = [ 1 -1  0  1];
            hcp(end+1).n = [-1  0  1  2]; hcp(end).l = [ 1  0 -1  1];
            hcp(end+1).n = [ 0 -1  1  2]; hcp(end).l = [ 0  1 -1  1];
            hcp(end+1).n = [ 1 -1  0  2]; hcp(end).l = [-1  1  0  1];

            hcp(end+1).n = [ 1  0 -1  2]; hcp(end).l = [ 1  0 -1 -1];
            hcp(end+1).n = [ 0  1 -1  2]; hcp(end).l = [ 0  1 -1 -1];
            hcp(end+1).n = [-1  1  0  2]; hcp(end).l = [-1  1  0 -1];
            hcp(end+1).n = [-1  0  1  2]; hcp(end).l = [-1  0  1 -1];
            hcp(end+1).n = [ 0 -1  1  2]; hcp(end).l = [ 0 -1  1 -1];
            hcp(end+1).n = [ 1 -1  0  2]; hcp(end).l = [ 1 -1  0 -1];

            slipSystems.hcp102twin = hcp;

        case {'hcp101twin'} % CT1
            hcp = [];
            hcp(end+1).n = [ 1  0 -1  1]; hcp(end).l = [-1  0  1  2];
            hcp(end+1).n = [ 0  1 -1  1]; hcp(end).l = [ 0 -1  1  2];
            hcp(end+1).n = [-1  1  0  1]; hcp(end).l = [ 1 -1  0  2];
            hcp(end+1).n = [-1  0  1  1]; hcp(end).l = [ 1  0 -1  2];
            hcp(end+1).n = [ 0 -1  1  1]; hcp(end).l = [ 0  1 -1  2];
            hcp(end+1).n = [ 1 -1  0  1]; hcp(end).l = [-1  1  0  2];

            slipSystems.hcp101twin = hcp;

        case {'hcp111twin'} % TT2
            hcp = [];
            hcp(end+1).n = [ 1  1 -2  1]; hcp(end).l = [-1 -1  2  6];
            hcp(end+1).n = [-1  2 -1  1]; hcp(end).l = [ 1 -2  1  6];
            hcp(end+1).n = [-2  1  1  1]; hcp(end).l = [ 2 -1 -1  6];
            hcp(end+1).n = [-1 -1  2  1]; hcp(end).l = [ 1  1 -2  6];
            hcp(end+1).n = [ 1 -2  1  1]; hcp(end).l = [-1  2 -1  6];
            hcp(end+1).n = [ 2 -1 -1  1]; hcp(end).l = [-2  1  1  6];

            slipSystems.hcp111twin = hcp;

        case {'hcp112twin'} % CT2
            hcp = [];
            hcp(end+1).n = [ 1  1 -2  2]; hcp(end).l = [ 1  1 -2 -3];
            hcp(end+1).n = [-1  2 -1  2]; hcp(end).l = [-1  2 -1 -3];
            hcp(end+1).n = [-2  1  1  2]; hcp(end).l = [-2  1  1 -3];
            hcp(end+1).n = [-1 -1  2  2]; hcp(end).l = [-1 -1  2 -3];
            hcp(end+1).n = [ 1 -2  1  2]; hcp(end).l = [ 1 -2  1 -3];
            hcp(end+1).n = [ 2 -1 -1  2]; hcp(end).l = [ 2 -1 -1 -3];

            slipSystems.hcp112twin = hcp;

        case {'hcp113twin'} % 
            hcp = [];
            hcp(end+1).n = [ 1  1 -2  3]; hcp(end).l = [-1 -1  2  2];
            hcp(end+1).n = [-1  2 -1  3]; hcp(end).l = [ 1 -2  1  2];
            hcp(end+1).n = [-2  1  1  3]; hcp(end).l = [ 2 -1 -1  2];
            hcp(end+1).n = [-1 -1  2  3]; hcp(end).l = [ 1  1 -2  2];
            hcp(end+1).n = [ 1 -2  1  3]; hcp(end).l = [-1  2 -1  2];
            hcp(end+1).n = [ 2 -1 -1  3]; hcp(end).l = [-2  1  1  2];

            slipSystems.hcp113twin = hcp;

        case {'hcp114twin'} % 
            hcp = [];
            hcp(end+1).n = [ 1  1 -2  4]; hcp(end).l = [-2 -2  4  3];
            hcp(end+1).n = [-1  2 -1  4]; hcp(end).l = [ 2 -4  2  3];
            hcp(end+1).n = [-2  1  1  4]; hcp(end).l = [ 4 -2 -2  3];
            hcp(end+1).n = [-1 -1  2  4]; hcp(end).l = [ 2  2 -4  3];
            hcp(end+1).n = [ 1 -2  1  4]; hcp(end).l = [-2  4 -2  3];
            hcp(end+1).n = [ 2 -1 -1  4]; hcp(end).l = [-4  2  2  3];

            slipSystems.hcp114twin = hcp;

     otherwise
        gtError('gtGetSlipSystems:wrong_input', ...
            'Unsupported slip systems family!');
    end
end
