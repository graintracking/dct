function [int_fact,phi1, phi2, range] = gtCrystGeometricFactor(thetatype, eta, rel_bandwidth)
%% function range = gtCrystGeometricFactor(theta, eta, lambda, rel_bandwidth)
% Calulcate geometric intensity factor for normalization of spot intensities
% Attention: this function assumes that rotation is perpendicular to
% beamdirection
    p = gtLoadParameters;
    lambda   = gtConvEnergyToWavelength(p.acq(1).energy * [1 - rel_bandwidth / 2, 1, 1 + rel_bandwidth / 2]);
    theta = asind(lambda / 2 / p.cryst(1).dspacing(thetatype));
    k =  2 * pi ./ lambda / 1e-10;
    g =  2 * pi / p.cryst(1).dspacing(thetatype) * 1e10;
    r = g .* sqrt(sind(theta(2))^2 + cosd(theta(2))^2 .* cosd(90-eta).^2);
    g_z = g * cosd(theta(2)) .* sind(90 - eta);
    R1 = sqrt(k(1)*k(1) - g_z.*g_z);
    R2 = sqrt(k(3)*k(3) - g_z.*g_z);
    [~, ~, phi1] = gtMathsCordLength(R1, r, k(1));
    [~, ~, phi2] = gtMathsCordLength(R2, r, k(3));
    range = abs(phi1 - phi2);
    int_fact = range./min(range);
end
