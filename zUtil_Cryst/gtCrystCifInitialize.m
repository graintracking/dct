function cif = gtCrystCifInitialize(cifname)

% Asks user to input an element and it returns information from the 
% corresponding .cif file.
%
% FUNCTIONS CALLED
%


if exist(cifname, 'file')
    % adding fields 'latticepar' 'opsym' 'spacegroup' 'hermann_mauguin'
    % 'crystal_system' to the acq structure
    cif = gtReadCif(cifname);
else
    disp('You will have to fill fields ''latticepar'', ''opsym'',  ''spacegroup'',  ''hermann_mauguin'' and ''crystal_system'' to the cryst structure of the current phase')
    disp('This needs tobe done before pair-matching - continuing without for the moment... !');
    % 'crystal_system'
    cif.latticepar      = [];
    cif.opsym           = [];
    cif.spacegroup      = [];
    cif.hermann_mauguin = '';
    cif.crystal_system  = '';
end
    



end