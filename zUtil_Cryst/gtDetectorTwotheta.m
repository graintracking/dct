function results=gtDetectorTwotheta(parameters, phaseID)
% GTDETECTORTWOTHETA  Returns reflections and theta values on detector
%     results=gtDetectorTwotheta(parameters, phaseID)
%     ----------------------------------------------------------
%     INPUT:
%       parameters
%       phaseID
%
%     OUTPUT:
%       results  = structure with hkl, tt, d0, twoth and dsp fields.
%
%
%     Version 007 02-05-2012 by WLudwig
%       Add phase input argument
%
%     Version 006 24/04/2012 by LNervo
%       fix thetatype problem from xop
%       remove twotheta and add theta to results
%
%     Version 005 30-03-2012 by AKing and PReischig
%       mhashtable removed ! 
%
%     Version 004 08-03-2012by PReischig
%       Update to parameters version 2: changed parameters.geo to .labgeo.
%
%     Version 003 25-11-2011 by LNervo
%       Update to new parameters structure
%
%     Version 002 18-05-2011 by LNervo
%       Improved automaticity
%
%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% loading parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

numReflections = size(parameters.xop(phaseID).hkl, 1);
disp(['Saved xop reflections:    ' num2str(numReflections)])
if (~numReflections)
    gtError('gtDetectorTwotheta:wrong_parameter', ...
        'You should load XOP reflection information before proceding with the analysis')
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Find the families among the xop reflections
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
results = [];

hkl  = gtFindFamilies(parameters, 'hkl', phaseID);
intensity  = gtFindFamilies(parameters, 'int', phaseID);
formfactor  = gtFindFamilies(parameters, 'formfactor', phaseID);
mult = gtFindFamilies(parameters, 'mult', phaseID);

disp(['hkl total families:       ' num2str(size(hkl, 1))])

% ensure that the angles we have are correct for the energy and lattice parameters 
% in the parameters file
Bmat = gtCrystHKL2CartesianMatrix(parameters.cryst(phaseID).latticepar);
d0 = gtCrystDSpacing(hkl', Bmat); % hkl as column vector
tt = gtCrystTheta(d0, parameters.acq.energy);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Keep only reflections on the detector
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
minangle = parameters.detgeo.detanglemin;
maxangle = parameters.detgeo.detanglemax;

% remove reflections out of detector
ind = find(tt > maxangle/2 | tt < minangle/2);
% ...remove them
tt(ind)    = [];            
hkl(ind, :) = [];
d0(ind)    = [];
intensity(ind)   = [];
mult(ind)  = [];
formfactor(ind) = [];

% order reflections by increasing twotheta values
[tt,ind_tt] = sort(tt);
hkl = hkl(ind_tt, :);
d0  = d0(ind_tt);
intensity = intensity(ind_tt);
mult   = mult(ind_tt);
formfactor = formfactor(ind_tt);
thtype = (1:numel(mult))';

disp(['Families on the detector: ' num2str(size(hkl,1))])
disp('Now reflections are sorted by increasing theta values')

results.hkl       = hkl';
results.theta     = tt;
results.dspacing  = d0;
results.int       = intensity';
results.thetatype = thtype';
results.mult      = mult';
results.formfactor = formfactor';

end % end of function

