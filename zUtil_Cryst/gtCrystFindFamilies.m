function list = gtCrystFindFamilies(hklsp, cryst)
% GTCRYSTFINDFAMILIES
%     list = gtCrystFindFamilies(hklsp, cryst)
%     ----------------------------------------
%     From the hklsp list, finds the unique families by calculating the d-spacing
%     and then sorting in descending order
%
%     INPUT:
%       hklsp = <double>     hk(i)l of reflections (n,3) or (n,4)
%       cryst = <struct>     parameters.cryst
%
%     OUTPUT:
%       list  = <struct>     information about d-spacing, theta
%           .hkl
%           .hklsp
%           .thetatypesp
%           .dspacingsp
%           .mult
%           .allshkls
%           .allhklinds
%           .allthetatype
%           .alldspacing
%           .ind
%           .hklind
%           .dspacingfam
%           .hklfam

% each row one reflection
if ~isrow(hklsp)
    hklsp = hklsp';
end

% use symmetry operators
Bmat = gtCrystHKL2CartesianMatrix(cryst.latticepar);

symm = gtCrystGetSymmetryOperators(cryst.crystal_system, cryst.spacegroup);
[allshkls, allhklinds, mult] = gtCrystSignedHKLs(hklsp', symm); % column reflections

[hklind,ind,~] = unique(allhklinds,'stable');
hkl = allshkls(ind,:);

% calculate d-spacing
alldspacing = gtCrystDSpacing(allshkls', Bmat)';
alldspacing = single(alldspacing); % needed to do unique

[dspacingfam, ~, allthetatype] = unique(alldspacing, 'stable');

list.hkl          = double(hkl');
list.hklsp        = double(hklsp');
list.thetatypesp  = allthetatype(ind)';
list.dspacingsp   = double(alldspacing(ind)');
list.mult         = mult';
list.allshkls     = allshkls';
list.allhklinds   = allhklinds';
list.allthetatype = allthetatype';
list.alldspacing  = alldspacing';
list.ind          = ind';
list.hklind       = hklind';
list.dspacingfam  = double(dspacingfam');
list.hklfam       = unique(hkl, 'rows', 'stable')';

end % end of function
