function [out,ind]=gtFindFamilies(parameters,name, phaseID)
% GTFINDFAMILIES  Get list called 'name' from xop structure
%     out=gtFindFamilies(parameters,name)
%     -----------------------------------
%     Find duplicates entries and remove them.
%
%     INPUT:
%       name = variable name to get <string>
%
%     OUTPUT:
%       out  = variable from xop structure
%       ind  = indexes of reflections kept from xop list
%
% Andy 27/04/2012 - this didn't understand hexangonal crystals correctly


% get reflections from parameters
hkl=parameters.xop(phaseID).hkl;

% treat different symmetries materials correctly
if any(strcmp(parameters.cryst(phaseID).crystal_system, {'hexagonal', 'trigonal'}))
    if size(hkl, 2)==3 
    % change to four index hkil
    
    hkl=[hkl(:,1) hkl(:,2) -hkl(:,1)-hkl(:,2) hkl(:,3)];
    parameters.xop(phaseID).hkl=hkl;
    end
    % sort, knowing that l is not equivilent to h/k/i
    % abs is required to stop confusions between eg 2 -1 -1 and 1 1 -2
    test=[sort(abs(hkl(:, 1:3)),2) abs(hkl(:,4))];
elseif strcmp(parameters.cryst(phaseID).crystal_system, 'cubic')
    % sort hkl - h,k,l all equivilent
    test=sort(hkl,2);
elseif strcmp(parameters.cryst(phaseID).crystal_system, 'tetragonal')
    % so far only tested for the output delivered from diamond software (already sorted...) 
    test = hkl;
else
    error('Need to add crystal system "%s" to gtFindFamiles.m ... quitting', parameters.cryst(phaseID).crystal_system)
end

% remove duplicates
[~,ind]=unique(test,'rows','first');
%sort indexes
inds=sort(ind);
% get fields names
names=fieldnames(parameters.xop(phaseID));

% for each field, remove the duplicated entries
for j=1:size(names,1)
    % except field chars and structures
    if ~ischar(parameters.xop(phaseID).(names{j})) && ~isstruct(parameters.xop(phaseID).(names{j}))
        value=parameters.xop(phaseID).(names{j});
        if ~isempty(value)
            value=value(inds,:);
        end
        parameters.xop(phaseID).(names{j})=value;
    end
end

out=parameters.xop(phaseID).(name);

if nargout == 2
    ind = inds;
end

end % end of function
