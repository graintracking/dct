function sigmas = gtCrystGetSigmas(crystal_system, latticepar, convention, material, tol_angle)
% GTCRYSTGETSIGMAS  Calculates CSL sigmas values for cubic and hexagonal crystal
%     sigmas = gtCrystGetSigmas(crystal_system, latticepar, [convention], [material])
%     -------------------------------------------------------------------
%     INPUT:
%       crystal_system = 
%       latticepar     = 
%       convention     = <string>   hcp axes convention {'X'}|'Y'
%       material       = <string> material {'Ti'}|'Mg'
%                        sigmas for Be, Ti and Zr are the same
%       tol_angle      = <double> tolerance angle for misorientation used in Brandon criterion {15}
if ~exist('latticepar','var') || isempty(latticepar)
    parameters = [];
    load('parameters.mat');
    latticepar = parameters.cryst.latticepar;
    material = parameters.cryst.material;
    clear parameters
end
if ~exist('convention','var') || isempty(convention)
    convention = 'X';
end
if ~exist('material','var') || isempty(material)
    material = 'Ti';
end
if ~exist('tol_angle', 'var') || isempty(tol_angle)
    tol_angle = 15;
end


if strcmpi(crystal_system, 'cubic')

    %sigma s: type, angle, axis cart, brandon criteria {, rodrigues}
    sigmas = [ 3 60    1 1 1 0 0.333 0.333 0.333; ... % 3   1
               5 36.86 1 0 0 0 0.333 0     0; ...     % 5   2
               7 38.21 1 1 1 0 0.199 0.199 0.199; ... % 7   3
               9 38.94 1 1 0 0 0.25  0.25  0; ...     % 9   4
              11 50.47 1 1 0 0 0.333 0.333 0; ...     %11   5
              13 22.62 1 0 0 0 0.2   0     0; ...     %13a  6
              13 27.79 1 1 1 0 0.143 0.143 0.143; ... %13b  7
              15 48.19 2 1 0 0 0.4   0.2   0; ...     %15   8
              21 44.41 2 1 1 0 0.333 0.167 0.167; ... %21b  9
              27 35.43 2 1 0 0 0.285 0.143 0; ...     %27b  10
              ];
    %add brandon criteria
    sigmas(:, 6) = tol_angle * sigmas(:, 1) .^ (-0.5);
    sigmaNorms = sqrt(sum(sigmas(:, 3:5) .* sigmas(:, 3:5), 2));
    %normalise axis
    sigmas(:, 3:5) = sigmas(:, 3:5) ./ sigmaNorms(:, [1 1 1]);

elseif strcmpi(crystal_system, 'hexagonal')
 
    switch lower(material)
        case  {'be','ti','zr'}
    
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % added on 2013-10-09 by LNervo
            % R. Bonnet, E. Cousineau, D.H. Warrington (1981) "Determination of
            %     near-coincident cells for hexagonal crystals. Related  DSC
            %     lattices" Acta Crystallographica, A37, 184-189
            % Bozzolo et al. (2010) "Misorientations induced by deformation
            %     tiwnning in titanium" J. Appl. Cryst. 43, 596-602
            % CSL values for HCP; cartesian axis in Y-convention originally
            
            % sigma for hcp s: type, angle, axis hex, brandon criteria, axis cart
            sigmas = [...,
                7  21.8   0  0  0  1   0  0 0 1; ... %7a
                7  64.6   1  0 -1  0   0  1 0 0; ... %7b
                11 35.1   1  0 -1  0   0  1 0 0; ... %11a
                11 84.8   2 -1 -1  0   0  0 1 0; ... %11b
                13 27.8   0  0  0  1   0  0 0 1; ... %13a
                13 57.4   2 -1 -1  0   0  0 1 0; ... %13b
                13 76.7   1  0 -1  0   0  1 0 0; ... %13c
                17 40.1   2 -1 -1  0   0  0 1 0; ... %17a NO Ti
                17 79.8   3 -1 -2  0   0  0 0 0; ... %17b NONE
                19 13.2   0  0  0  1   0  0 0 1; ... %19a
                19 65.1  10 -5 -5  3   0  0 0 0; ... %19b
                19 87.0   1  0 -1  0   0  1 0 0; ... %19c
                23 55.6   1  0 -1  0   0  1 0 0; ... %23a NONE
                23 72.3   2 -1 -1  0   0  0 1 0; ... %23b NO Ti
                23 86.3  10  0 -10 3   0  0 0 0; ... %23c NONE
                ];
        case 'mg'
            sigmas = [...,
                7  21.8   0  0  0  1   0  0 0 1; ... %7a
                7  64.6   1  0 -1  0   0  1 0 0; ... %7b
                9  56.25  2 -1 -1  0   0  0 1 0; ... %9  check this 
                10 78.46  1  0 -1  0   0  1 0 0; ... %10
                11 62.96  1  0 -1  0   0  1 0 0; ... %11
                18 63.62  1  0 -1  0   0  1 0 0; ... %18a
                13 27.8   0  0  0  1   0  0 0 1; ... %13a
                13 85.59  1  0 -1  0   0  1 0 0; ... %13b
                14 44.42  1  0 -1  0   0  1 0 0; ... %14
                15 29.93  2 -1 -1  0   0  0 1 0; ... %15a
                15 86.18  2 -1 -1  0   0  0 1 0; ... %15b {1 0 -1 2} twin
                17 86.63  2 -1 -1  0   0  0 1 0; ... %17a    idem
                17 49.68  1  0 -1  0   0  1 0 0; ... %17b  check this
                18 70.53  2 -1 -1  0   0  0 1 0; ... %18b
                19 13.2   0  0  0  1   0  0 0 1; ... %19a
                21 73.4   7  0 -7  2   0  0 0 0; ... %21   add cartesian
                23 55.8   2 -1 -1  0   0  0 1 0; ... %23a 
                23 34.30  2 -1 -1  0   0  0 1 0; ... %23b
                23 77.44  2 -1 -1  0   0  0 1 0; ... %23c
                23 34.30  1  0 -1  0   0  1 0 0; ... %23d
                25 63.90  2 -1 -1  0   0  0 1 0; ... %25a
                25 23.07  1  0 -1  0   0  1 0 0; ... %25b
                ];
        otherwise
            gtError('ASSEMBLE:unknown_sigmas', ...
                      'Unknown material (%s) : CSL & sigma currently only supported for Ti, Be, Zr, Mg');
    end
          

    if (strcmpi(convention, 'X'))
        sigmas(:, 8:10) = gtHex2Cart(sigmas(:, 3:6), latticepar, convention);
    end

    %add brandon criteria: is it also for hcp? (laura)
    sigmas(:, 7) = tol_angle * sigmas(:, 1) .^ (-0.5);
    sigmaNorms = sqrt(sum(sigmas(:, 8:10) .* sigmas(:, 8:10), 2));
    %normalise axis
    sigmas(:, 8:10) = sigmas(:, 8:10) ./ sigmaNorms(:, [1 1 1]);

elseif strcmpi(crystal_system, 'tetragonal')
    switch lower(material)
        case  {'tial'}
        %https://hal.archives-ouvertes.fr/jpa-00230279/document
        %sigma s: type, angle, axis cart, brandon criteria {, rodrigues}
        sigmas =  [ %2 90    1 1 0 0 0.707 0.707 0; ...
                    3 70.53 1 0 0 0 0.707 0     0; ...
                    5 36.87 0 0 1 0 0     0     0.333; ... %5a
                    5 53.13 1 1 0 0 0.353 0.353 0; ...     %5b
                    6 60	2 0 1 0 0.666 0     0.333; ...
                    7 73.4	2 2 1 0 0.667 0.667 0.333; ...
                    9 38.94 1 0 0 0 0.353 0     0; ...     %9a
                    9 90    4 0 1 0 1.789 0     0.447; ... %9b
                   10 36.87 1 1 0 0 0.236 0.236 0; ...     %10a
                   10 95.74 2 0 3 0 0.989 0     1.483; ... %10b
                   11 50.48 1 0 0 0 0.471 0     0; ...     %11a
                   11 50.48 1 1 1 0 0.272 0.272 0.272; ... %11b
                   13 22.62 0 0 1 0 0     0     0.2; ...   %13a
                   13 67.38 1 1 0 0 0.471 0.471 0; ...     %13b
                   13 76.66 2 1 0 0 0.913 0.457 0; ...     %13c
                   14 38.21 2 0 1 0 0.4   0     0.2; ...   %14a
                   14 73.4  3 1 0 0 1.118 0.373 0; ...     %14b
                   15 48.19 2 2 1 0 0.4   0.4   0.2; ...   %15a
                   15 78.46 3 1 1 0 1.095 0.365 0.365; ... %15b
                   17 28.07 1 1 0 0 0.177 0.177 0; ...     %17a
                   17 28.07 0 0 1 0 0     0     0.25; ...  %17b
                   17 61.93 4 0 1 0 1.073 0     0.268; ... %17c
                   17 86.63 1 0 0 0 0.943 0     0; ...     %17d
                   18 38.94 1 1 1 0 0.204 0.204 0.204; ... %18a
                   18 67.11 4 2 1 0 1.003 0.501 0.251; ... %18b
                   19 26.53 1 0 0 0 0.236 0     0; ...     %19a
                   19 46.83 2 0 1 0 0.5   0     0.25; ...  %19b
                   19 93.02 2 1 0 0 1.217 0.609 0; ...     %19c
                   21 44.42 1 0 1 0 0.289 0     0.289; ... %21a
                   21 58.41 3 1 0 0 0.839 0.28  0; ...     %21b
                   21 58.41 2 2 1 0 0.5   0.5   0.25; ...  %21c
                   21 79.02 4 4 1 0 1.1   1.1   0.275; ... %21d
                   ];
        %add brandon criteria
        sigmas(:, 6) = tol_angle * sigmas(:, 1) .^ (-0.5);
        sigmaNorms = sqrt(sum(sigmas(:, 3:5) .* sigmas(:, 3:5), 2));
        %normalise axis
        sigmas(:, 3:5) = sigmas(:, 3:5) ./ sigmaNorms(:, [1 1 1]);
        case {'sn', 'snbi'}
            sigmas =  [
                        1 62.8  1 0 0   0   0.707 0     0; ...
                        2 57.2  1 1 0   0   0.707 0.707 0; ...
                      ];
            sigmas(:, 6) = tol_angle * sigmas(:, 1) .^ (-0.5);
            sigmaNorms = sqrt(sum(sigmas(:, 3:5) .* sigmas(:, 3:5), 2));
            %normalise axis
            sigmas(:, 3:5) = sigmas(:, 3:5) ./ sigmaNorms(:, [1 1 1]);
    end
end

end % end of function
