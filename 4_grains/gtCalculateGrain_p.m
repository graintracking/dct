function grain = gtCalculateGrain_p(grain, parameters, varargin)
% GTCALCULATEGRAIN_P  Calulates diffraction vectors and detector intersection points
%
%     grain = gtCalculateGrain_p(grain, parameters, varargin)
%     -----------------------------------------------------
%       Parallel version of gtCalculateGrain
%
%     INPUT:
%       grain      = <struct>/<cell <struct> > grain(s) of interest
%       parameters = <struct>   parameters file
%
%     OPTIONAL INPUT (as a list of pairs):
%       showfigure = <logical>  show or not the figure {false}
%       color      = <logical>  show or not the prediction spot positions coloured as
%                               omega {false}
%       axis       = <logical>  plot axis, image reference and beam direction in the
%                               image {false}
%       overlay    = <double>   difspots image output from "gtPlaceDifspotinFull" (MxM)
%                               M is the size of the overlay image (typically 2048)
%       markersize = <double>   marker size {10}
%       clims      = <double>   scale of grays {[-300 500]}
%       usestrain  = <logical>  use strain info
%
%     OUTPUT:
%       grain      = <struct>/<cell <struct> > grain(s) of interest
%                               Added fields:
%            .pllab     = <double>
%            .hklsp     = <double>
%            .dvec      = <double>
%            .dvecsam   = <double>
%            .allblobs  = <struct>
%                               Updated fields:
%            .thetatype = <double>
%            .hkl       = <double>
%            .pl        = <double>
%            .theta     = <double>
%            .eta       = <double>
%            .omega     = <double>
%
% Simplified version of gtFedGenerateRandomGrain   15-03-2012  - WLudwig

if (nargin < 1)
    disp('Usage:  grain =  gtCalculateGrain(grain, parameters, varargin)');
    return
end
if (~exist('parameters','var') || isempty(parameters))
    parameters = gtLoadParameters();
end

% set default values for optional arguments
app.color      = false;
app.showfigure = false;
app.axis       = false;
app.markersize = 10;
app.clims      = [-300 500];
app.overlay    = []; % zeros(parameters.acq.ydet, parameters.acq.xdet);
app.usestrain  = isfield(parameters.fsim, 'mode') && strcmpi(parameters.fsim.mode, 'global_fit');
app.included   = [];
app.ref_omind  = [];
app.det_ind    = [];
app.pl_ind     = [];

app = parse_pv_pairs(app,varargin);

% If strain data is not available, disable strain
if (~isfield(grain, 'strain') || ~isfield(grain.strain, 'strainT') ...
        || isnan(grain.strain.strainT(1, 1)))

    app.usestrain = false;
end

if (iscell(grain))
    phase = unique(cellfun(@(x)x.phaseid, grain));
    if (numel(phase) > 1)
        error('gtCalculateGrain:wrong_argument', ...
            'Cannot handle multiple phases!');
    end

    R_vec = cellfun(@(x){x.R_vector}, grain);
    R_vec = cat(1, R_vec{:});

    gcsam = cellfun(@(x){x.center}, grain);
    gcsam = cat(3, gcsam{:});

    num_grains = numel(grain);
else
    % Rodrigues vector (grain orientation)
    R_vec = grain.R_vector;
    phase = grain.phaseid;
    gcsam = grain.center;

    num_grains = 1;
end
ones_grains = ones(1, num_grains);

cryst  = parameters.cryst(phase);
labgeo = parameters.labgeo;
samgeo = parameters.samgeo;

if (~isfield(labgeo, 'omstep') || isempty(labgeo.omstep))
    labgeo.omstep = gtAcqGetOmegaStep(parameters);
end

%%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % Detector geometry

if (~isfield(parameters, 'detgeo') || isempty(parameters.detgeo))
    parameters.detgeo = gtGeoConvertLegacyLabgeo2Detgeo(labgeo);
end
if (isempty(app.det_ind))
    app.det_ind = 1:numel(parameters.detgeo);
end

if (isfield(parameters, 'diffractometer'))
    for ii_d = app.det_ind
        diff = parameters.diffractometer(ii_d);
        if (any(size(diff.axes_rotation) ~= size(labgeo.rotdir)) || ...
                any(diff.axes_rotation ~= labgeo.rotdir))
            warning('gtDefSyntheticGrainCreate:inconsistent_rotation_dir', ...
                'The diffractometer rotation axis differs from the labgeo rotation axis')
            disp(diff.axes_rotation)
            disp(labgeo.rotdir)
        end
    end
end

%%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % Crystallography from parameters file

%disp('Translating Miller indices into normalized cartesian coordinates for plane normals')
Bmat = gtCrystHKL2CartesianMatrix(cryst.latticepar);

% Compute all orientation matrices g (Reminder: Vc = g . Vs)
g = gtMathsRod2OriMat(R_vec');

for det_ind = app.det_ind
    acq = parameters.acq(det_ind);
    detgeo = parameters.detgeo(det_ind);

    mode = acq.type;
    if (ismember(mode, {'360degree', '180degree'}))
        mode = 'dct';
    end

    is_dct = strcmpi(mode, 'dct');
    is_topotomo = strcmpi(mode, 'topotomo');

    use_diffractometer = is_topotomo ...
        || (isfield(parameters, 'diffractometer') ...
            && numel(parameters.diffractometer) >= det_ind ...
            && det_ind > 1);
    if (use_diffractometer)
        diff_acq = parameters.diffractometer(det_ind);
        diff_ref = parameters.diffractometer(1);
    end

    if (~is_dct && ~is_topotomo)
        error('gtCalculateGrain:wrong_argument', ...
            'Acquisition type should be: {"dct"} | "topotomo", but "%s" was given ("360degree" corresponds to "dct")', ...
            mode);
    end
    if (is_topotomo && isempty(app.pl_ind))
        if (~isfield(acq, 'pl_ind') || isempty(acq.pl_ind))
            warning('gtCalculateGrain:wrong_argument', ...
                'When mode is "topotomo", "pl_ind" should also be pased');
            continue
        else
            app.pl_ind = acq.pl_ind;
        end
    end

    omstep = gtAcqGetOmegaStep(parameters, det_ind);

    if (is_dct)
        thetatypesp = cryst.thetatypesp;
        thetatypesp = reshape(thetatypesp, [], 1);

        if (size(cryst.hkl, 1) == 3 || size(cryst.hkl, 1) == 4)
            hkl = cryst.hkl(:, thetatypesp);
            hklsp = cryst.hklsp;
        else
            hkl = cryst.hkl(thetatypesp, :)';
            hklsp = cryst.hklsp';
        end
    elseif (is_topotomo)
        if (iscell(grain))
            hkl = grain{1}.allblobs(1).hkl(app.pl_ind, :)';
            hklsp = grain{1}.allblobs(1).hklsp(app.pl_ind, :)';
            thetatypesp = grain{1}.allblobs(1).thetatype(app.pl_ind);
        else
            hkl = grain.allblobs(1).hkl(app.pl_ind, :)';
            hklsp = grain.allblobs(1).hklsp(app.pl_ind, :)';
            thetatypesp = grain.allblobs(1).thetatype(app.pl_ind);
        end
    end
    num_plane_normals = numel(thetatypesp);

    % D-spacings in the reference crystal for the given hkl reflections
    dsp_ref = gtCrystDSpacing(double(hkl), Bmat);

    % Plane normals in Cartesian CRYSTAL coordinates
    plcry = gtCrystHKL2Cartesian(double(hklsp), Bmat);

    % Express plane normals in cartesian SAMPLE CS
    pl_orig = gtVectorCryst2Lab(plcry', g);

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Compute diffraction angles and detector intersection points

    % Let's merge multiple grains information for vectorization purposes
    pl_orig = permute(pl_orig, [2 1 3]);
    pl_orig = reshape(pl_orig, 3, []);

    dsp_ref = dsp_ref(1, :, ones_grains);
    dsp_ref = reshape(dsp_ref, 1, []);

    % Impose strain on grain
    if (app.usestrain)
        % Deformation tensor (use strain tensor for now)
        defT = grain.strain.strainT;

        % New deformed plane normal and relative elongation (relative d-spacing)
        [pl_samd, drel] = gtStrainPlaneNormals(pl_orig, defT); % unit vector
    else
        pl_samd = pl_orig;
        drel = ones(1, num_plane_normals * num_grains);
    end

    % d-spacing after applying the strain tensor
    dsp_d = drel .* dsp_ref;

    lambda = gtConvEnergyToWavelength(acq.energy);
    % Bragg angles theta in the deformed state
    sinth = 0.5 * lambda ./ dsp_d;

    % New sin(theta) in deformed state is inversely proportional to elongation
    theta = asind(sinth);

    % The plane normals need to be brought in the Lab reference where the
    % beam direction and rotation axis are defined. This only matters
    % if the Sample reference at omega=0 does not coincide with the Lab
    % reference (usually not the case, but we should account for the
    % possibility, as it is provided in 'samgeo').
    % Use the Sample -> Lab orientation transformation assuming omega=0;
    % (vector length preserved for free vectors)
    if (is_dct)
        t = eye(3);
        if (use_diffractometer)
            t = gtGeoDiffractometerTensor(diff_acq, 'sam2lab', ...
                'reference_diffractometer', diff_ref);

            rotcomp = gtMathsRotationMatrixComp(diff_acq.axes_rotation', 'col');
            rotdir = diff_acq.axes_rotation';
        else
            rotcomp = gtMathsRotationMatrixComp(labgeo.rotdir', 'col');
            rotdir = labgeo.rotdir';
        end
        pl_labd = gtGeoSam2Lab(pl_samd', t, labgeo, samgeo, true)';
    elseif (is_topotomo)
        num_imgs = gtAcqTotNumberOfImages(parameters, det_ind);
        ones_imgs = ones(num_imgs, 1);

        ws = omstep * (0:(num_imgs-1));
        t = gtGeoDiffractometerTensor(diff_acq, 'sam2lab', ...
            'reference_diffractometer', diff_ref, 'angles_rotation', ws);

        pl_labd = gtGeoSam2Lab(pl_samd', t, labgeo, samgeo, true, 'element_wise', false);
        pl_labd = reshape(pl_labd, num_grains, [], 3);
        pl_labd = permute(pl_labd, [3 2 1]);
        pl_labd = reshape(pl_labd, 3, []);
        sinth = reshape(sinth(ones_imgs, :), 1, []);

        rotcomp = gtMathsRotationMatrixComp(diff_acq.axes_basetilt', 'col');
        rotdir = diff_acq.axes_basetilt';
    end

    % Four omegas of the plane normal (1-3 and 2-4 are the two Friedel pairs):
    % pls is plus or minus pl, and St is the rotation tensor per each omega
    [om4, pllab4, pls4, rot_l2s_r4, omind4] = gtFedPredictOmegaMultiple(...
        pl_labd, sinth, labgeo.beamdir', rotdir, rotcomp, []);

    num_hkl_indxs = size(hkl, 1);
    if (is_topotomo)
        om4 = mod(om4 + 180, 360) - 180;

        hkl = reshape(hkl(:, ones_imgs, ones_grains), num_hkl_indxs, []);
        hklsp = reshape(hklsp(:, ones_imgs, ones_grains), num_hkl_indxs, []);
        thetatypesp = reshape(thetatypesp(ones_imgs, ones_grains), [], 1);

        plcry = reshape(plcry(:, ones_imgs, ones_grains), 3, []);
        pl_orig = reshape(pl_orig(:, ones_imgs, ones_grains), 3, []);
        pl_samd = reshape(pl_samd(:, ones_imgs, ones_grains), 3, []);

        theta = reshape(theta(:, ones_imgs, ones_grains), 1, []);
        sinth = reshape(sinth(:, ones_imgs, ones_grains), 1, []);
    else
        hkl = reshape(hkl(:, :, ones_grains), num_hkl_indxs, []);
        hklsp = reshape(hklsp(:, :, ones_grains), num_hkl_indxs, []);
        thetatypesp = reshape(thetatypesp(:, ones_grains), [], 1);

        plcry = reshape(plcry(:, :, ones_grains), 3, []);
    end

    % Delete those where no reflection occurs
    no_fwd_diffr = (isnan(om4(1, :)) | isnan(om4(3, :)));
    % todel4 is 4 x (num_pn * num_gr) already!
    no_fwd_diffr4 = no_fwd_diffr([1 1 1 1], :);

    %%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % % Propagating the same common values for all the 4 omegas coming from a
    % % single plane normal.
    plorig4 = pl_orig(:, :, [1 1 1 1]);
    plcry4 = plcry(:, :, [1 1 1 1]);
    plsamd4 = pl_samd(:, :, [1 1 1 1]);

    hkl = hkl';
    hklsp = hklsp';

    thetatypesp4 = thetatypesp(:, [1 1 1 1]);
    hkl4 = hkl(:, :, [1 1 1 1]);
    hklsp4 = hklsp(:, :, [1 1 1 1]);
    hklsp4(:, :, [3 4]) = -hklsp4(:, :, [3 4]);
    sinth4 = sinth([1 1 1 1], :);
    theta4 = theta([1 1 1 1], :);

    if (isempty(app.ref_omind))
        %%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % % Following the convention of the matching output, the omega value
        % % smaller than 180deg (spot "a") is used in the pair data.

        chind4 = om4(1, :) > om4(3, :);

        pllab4(:, chind4, [1, 3]) = pllab4(:, chind4, [3, 1]);
        pls4(:, chind4, [1, 3])   = pls4(:, chind4, [3, 1]);
        om4([1, 3], chind4)       = om4([3, 1], chind4);
        omind4([1, 3], chind4)    = omind4([3, 1], chind4);
        rot_l2s_r4(:, :, chind4, [1, 3]) = rot_l2s_r4(:, :, chind4, [3, 1]);
        hklsp4(chind4, :, [1, 3]) = hklsp4(chind4, :, [3, 1]);

        chind4 = om4(2, :) > om4(4, :);

        pllab4(:, chind4, [2, 4]) = pllab4(:, chind4, [4, 2]);
        pls4(:, chind4, [2, 4])   = pls4(:, chind4, [4, 2]);
        om4([2, 4], chind4)       = om4([4, 2], chind4);
        omind4([2, 4], chind4)    = omind4([4, 2], chind4);
        rot_l2s_r4(:, :, chind4, [2, 4]) = rot_l2s_r4(:, :, chind4, [4, 2]);
        hklsp4(chind4, :, [2, 4]) = hklsp4(chind4, :, [4, 2]);

        %%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Four omegas of the plane normal (1-3 and 2-4 are the two Friedel pairs):
        % % But the two Friedel pairs are going to be 1a-1b and 2a-2b.

        pllab4(:, :, [2, 3]) = pllab4(:, :, [3, 2]);
        pls4(:, :, [2, 3])   = pls4(:, :, [3, 2]);
        om4([2, 3], :)       = om4([3, 2], :);
        omind4([2, 3], :)    = omind4([3, 2], :);
        rot_l2s_r4(:, :, :, [2, 3]) = rot_l2s_r4(:, :, :, [3, 2]);
        hklsp4(:, :, [2, 3]) = hklsp4(:, :, [3, 2]);
    end

    %%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % % Unfolding the 4-fold data structures

    no_fwd_diffr = reshape(no_fwd_diffr4, [], num_grains);

    om = reshape(om4, [], num_grains);
    omind = reshape(omind4, [], num_grains);
    sinth = reshape(sinth4, [], num_grains);
    theta = reshape(theta4, [], num_grains);

    % pllab4, pls4, and plorig4 are: 3 x (num_pn * num_gr) x 4
    plcry = reshape(permute(plcry4, [1 3 2]), 3, [], num_grains);
    plsamd = reshape(permute(plsamd4, [1 3 2]), 3, [], num_grains);
    pllab = reshape(permute(pllab4, [1 3 2]), 3, [], num_grains);
    pls = reshape(permute(pls4, [1 3 2]), 3, [], num_grains);
    plorig = reshape(permute(plorig4, [1 3 2]), 3, [], num_grains);

    rot_l2s = reshape(permute(rot_l2s_r4, [1 2 4 3]), 3, 3, [], num_grains);

    thetatypesp = reshape(thetatypesp4', [], num_grains);
    hkl = permute(hkl4, [2 3 1]);
    hkl = reshape(hkl, num_hkl_indxs, [], num_grains);
    hklsp = permute(hklsp4, [2 3 1]);
    hklsp = reshape(hklsp, num_hkl_indxs, [], num_grains);

    if (~isempty(app.ref_omind))
        num_plane_normals = size(thetatypesp, 1) / 4;

        grs_omind_ind = [ ...
            find(omind == 1)'; find(omind == 2)'; ...
            find(omind == 3)'; find(omind == 4)' ];
        grs_omind_ind = reshape(grs_omind_ind, [], 1);

        ref_omind_ind = [ ...
            find(app.ref_omind == 1)'; find(app.ref_omind == 2)'; ...
            find(app.ref_omind == 3)'; find(app.ref_omind == 4)' ];
        ref_omind_ind = reshape(ref_omind_ind, [], 1);

        ref_omind_ind = repmat(ref_omind_ind, [1 num_grains]) ...
            + repmat((0:num_grains-1) * num_plane_normals*4, [num_plane_normals*4, 1]);
        ref_omind_ind = reshape(ref_omind_ind, [], 1);

        no_fwd_diffr(ref_omind_ind) = no_fwd_diffr(grs_omind_ind);

        om(ref_omind_ind) = om(grs_omind_ind);
        omind(ref_omind_ind) = omind(grs_omind_ind);
        sinth(ref_omind_ind) = sinth(grs_omind_ind);
        theta(ref_omind_ind) = theta(grs_omind_ind);

        plcry(:, ref_omind_ind) = plcry(:, grs_omind_ind);
        plsamd(:, ref_omind_ind) = plsamd(:, grs_omind_ind);
        pllab(:, ref_omind_ind) = pllab(:, grs_omind_ind);
        pls(:, ref_omind_ind) = pls(:, grs_omind_ind);
        plorig(:, ref_omind_ind) = plorig(:, grs_omind_ind);

        rot_l2s(:, :, ref_omind_ind) = rot_l2s(:, :, grs_omind_ind);

        thetatypesp(ref_omind_ind) = thetatypesp(grs_omind_ind);
        hkl(:, ref_omind_ind) = hkl(:, grs_omind_ind);
        hklsp(:, ref_omind_ind) = hklsp(:, grs_omind_ind);
    end

    if (~isempty(app.included))
        no_fwd_diffr = no_fwd_diffr(app.included, :);

        om = om(app.included, :);
        omind = omind(app.included, :);
        sinth = sinth(app.included, :);
        theta = theta(app.included, :);

        plcry = plcry(:, app.included, :);
        plsamd = plsamd(:, app.included, :);
        pllab = pllab(:, app.included, :);
        pls = pls(:, app.included, :);
        plorig = plorig(:, app.included, :);

        rot_l2s = rot_l2s(:, :, app.included, :);

        thetatypesp = thetatypesp(app.included, :);
        hkl = hkl(:, app.included, :);
        hklsp = hklsp(:, app.included, :);
    end

    no_fwd_diffr = reshape(no_fwd_diffr, [], 1);

    om = reshape(om, [], 1);

    plcry = permute(plcry, [2 1 3]);
    plsamd = permute(plsamd, [2 1 3]);
    pllab = reshape(pllab, 3, []);
    plorig = permute(plorig, [2 1 3]);
    pls = permute(pls, [2 1 3]);

    hkl = permute(hkl, [2 1 3]);
    hklsp = permute(hklsp, [2 1 3]);

    if (is_dct)
        ph = zeros(size(om));
        phind = zeros(size(omind));

        if (use_diffractometer)
            rot_s2l = gtGeoDiffractometerTensor(diff_acq, 'sam2lab', ...
                'reference_diffractometer', diff_ref, ...
                'angles_rotation', om);
            rot_l2s = gtGeoDiffractometerTensor(diff_acq, 'lab2sam', ...
                'reference_diffractometer', diff_ref, ...
                'angles_rotation', om);
        else
            rot_s2l = permute(rot_l2s, [2 1 3 4]);
            rot_l2s = reshape(rot_l2s, 3, 3, []);
        end
    elseif (is_topotomo)
        ph = om;
        phind = omind;
        om = reshape(ws([1 1 1 1], :, ones_grains), [], num_grains);
        if (~isempty(app.included))
            om = om(app.included, :);
        end
        if (iscell(grain))
            omind = grain{1}.allblobs(1).omind(app.pl_ind, ones_grains);
        else
            omind = grain.allblobs(1).omind(app.pl_ind, ones_grains);
        end

        rot_s2l = gtGeoDiffractometerTensor(diff_acq, 'sam2lab', ...
            'reference_diffractometer', diff_ref, ...
            'angles_rotation', om, 'angles_basetilt', ph);
        rot_l2s = gtGeoDiffractometerTensor(diff_acq, 'lab2sam', ...
            'reference_diffractometer', diff_ref, ...
            'angles_rotation', om, 'angles_basetilt', ph);
    end

    %%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % % Computing the remaining physical quantities

    % Diffraction vectors % takes coloumn vectors
    dveclab = gtFedPredictDiffVecMultiple(pllab, labgeo.beamdir')';
    dveclab = gtMathsNormalizeVectorsList(dveclab);

    % Eta angles % takes row vector
    eta = gtGeoEtaFromDiffVec(dveclab, labgeo);

    % Diffraction vector in Sample reference
    dvecsam = gtGeoLab2Sam(dveclab, rot_l2s, labgeo, samgeo, true);

    % Let's convert grain center Sam -> Lab
    gcsam_v = permute(gcsam, [1 3 2]);
    gcsam_v = gcsam_v(ones(1, size(rot_l2s, 3)/num_grains), :, :);
    gcsam_v = reshape(gcsam_v, [], 3);

    if ((isfield(parameters.acq, 'correct_sample_shifts')) && ...
            (parameters.acq.correct_sample_shifts))
        % !!! This might not work outof the box! It might need to be
        % expanded by the number of orientations...
        [~, shifts_sam] = gtMatchGetSampleShifts(parameters, om);
        gcsam_v = gcsam_v + shifts_sam;
    end

    gclab_v_t = gtGeoSam2Lab(gcsam_v, rot_s2l, labgeo, samgeo, false)';

    % Lorentz factor for the W-bradening of projections (note that eq. 4.1
    % from page 36 in Henning's book refers to the scattering intensity)
    Lfactor = 1 ./ abs(sind(eta));

    % u,v,w or u,v,phi,w coordinates on the detector
    uv = gtFedPredictUVMultiple([], dveclab', gclab_v_t, ...
        detgeo.detrefpos', detgeo.detnorm', detgeo.Qdet, ...
        [detgeo.detrefu, detgeo.detrefv]')';

    w = om / omstep;

    % Precompute which reflections will actually fall into the detector
    ondet = ~no_fwd_diffr & ...
        (uv(:, 1) >= 1) & (detgeo.detsizeu >= uv(:, 1)) & ...
        (uv(:, 2) >= 1) & (detgeo.detsizev >= uv(:, 2));

    if (num_grains > 1)
        uv = reshape(uv, [], num_grains, 2);
        uv = permute(uv, [1 3 2]);
        w = reshape(w, [], num_grains);
        ondet = reshape(ondet, [], num_grains);

        pllab = reshape(pllab, 3, [], num_grains);
        pllab = permute(pllab, [2 1 3]);

        eta = reshape(eta, [], num_grains);
        Lfactor = reshape(Lfactor, [], num_grains);

        om = reshape(om, [], num_grains);

        ph = reshape(ph, [], num_grains);

        dveclab = reshape(dveclab', 3, [], num_grains);
        dvecsam = reshape(dvecsam', 3, [], num_grains);
        dveclab = permute(dveclab, [2 1 3]);
        dvecsam = permute(dvecsam, [2 1 3]);

        if (is_dct)
            if (use_diffractometer)
                rot_l2s = reshape(rot_l2s, 4, 4, [], num_grains);
                rot_s2l = reshape(rot_s2l, 4, 4, [], num_grains);
            else
                rot_l2s = reshape(rot_l2s, 3, 3, [], num_grains);
                rot_s2l = reshape(rot_s2l, 3, 3, [], num_grains);
            end
        elseif (is_topotomo)
            phstep = gtAcqGetPhiStep(parameters, det_ind);

            p = ph / phstep;
            p = reshape(p, [], num_grains);

            phi_range = acq.range_basetilt / phstep;
            ondet = ondet & p > phi_range(1) & p < phi_range(2);

            rot_l2s = reshape(rot_l2s, 4, 4, [], num_grains);
            rot_s2l = reshape(rot_s2l, 4, 4, [], num_grains);
        end

        for ii_gr = 1:num_grains
            allblobs = gtGrainAllblobsDefinition();

            allblobs.type = mode;

            allblobs.plcry = plcry(:, :, ii_gr);
            allblobs.plorig = plorig(:, :, ii_gr);
            allblobs.plsamdef = plsamd(:, :, ii_gr);
            allblobs.pl = pls(:, :, ii_gr);
            allblobs.pllab = pllab(:, :, ii_gr);

            allblobs.hkl = hkl(:, :, ii_gr);
            allblobs.hklsp = hklsp(:, :, ii_gr);

            allblobs.sintheta = sinth(:, ii_gr);
            allblobs.theta = theta(:, ii_gr);
            allblobs.eta = eta(:, ii_gr);
            allblobs.thetatype = thetatypesp(:, ii_gr);
            allblobs.lorentzfac = Lfactor(:, ii_gr);

            allblobs.omega = om(:, ii_gr);
            allblobs.omind = omind(:, ii_gr);

            allblobs.phi = ph(:, ii_gr);
            allblobs.phind = phind(:, ii_gr);

            allblobs.dvec = dveclab(:, :, ii_gr);
            allblobs.dvecsam = dvecsam(:, :, ii_gr);

            allblobs.srot = rot_l2s(:, :, :, ii_gr);

            allblobs.rot_s2l = rot_s2l(:, :, :, ii_gr);
            allblobs.rot_l2s = rot_l2s(:, :, :, ii_gr);

            if (is_dct)
                allblobs.detector = struct( ...
                    'uvw', {[uv(:, :, ii_gr), w(:, ii_gr)]}, ...
                    'ondet', {ondet(:, ii_gr)} );
            elseif (is_topotomo)
                allblobs.detector = struct( ...
                    'uvpw', {[uv(:, :, ii_gr), p(:, ii_gr), w(:, ii_gr)]}, ...
                    'ondet', {ondet(:, ii_gr)} );
            end

            grain{ii_gr}.allblobs(det_ind) = allblobs;
        end
    else
        allblobs = gtGrainAllblobsDefinition();

        allblobs.type = mode;
        allblobs.plcry = plcry;
        allblobs.plorig = plorig;
        allblobs.plsamdef = plsamd;
        allblobs.pl = pls;
        allblobs.pllab = pllab';
        allblobs.hkl = hkl;
        allblobs.hklsp = hklsp;
        allblobs.sintheta = sinth;
        allblobs.theta = theta;
        allblobs.eta = eta;
        allblobs.thetatype = thetatypesp;
        allblobs.lorentzfac = Lfactor;
        allblobs.omega = om;
        allblobs.omind = omind;
        allblobs.dvec = dveclab;
        allblobs.dvecsam = dvecsam;
        allblobs.srot = rot_l2s;
        allblobs.rot_s2l = rot_s2l;
        allblobs.rot_l2s = rot_l2s;

        % Initialse output variables
        if (is_dct)
            allblobs.detector = struct('uvw', {[uv, w]}, 'ondet', ondet);
        elseif (is_topotomo)
            phstep = gtAcqGetPhiStep(parameters, det_ind);

            p = ph / phstep;
            p = reshape(p, [], num_grains);

            phi_range = acq.range_basetilt / phstep;
            ondet = ondet & p > phi_range(1) & p < phi_range(2);

            allblobs.detector = struct('uvpw', {[uv, p, w]}, 'ondet', {ondet} );
        end

        grain.allblobs(det_ind) = allblobs;
    end
end 
