function gtConvertLegacy3DStruct(phase_id)
    if (~exist('phase_id', 'var'))
        phase_id = 1;
    end
    sample = GtSample.loadFromFile();

    num_grains = sample.phases{phase_id}.getNumberOfGrains();
    c = tic();
    fprintf('Phase %02d, Grain: ', phase_id)
    for ii = 1:num_grains
        num_chars = fprintf('%04d/%04d', ii, num_grains);

        try
            gr = gtLoadGrain(phase_id, ii);
            vol3D = struct('intensity', gr.vol, 'shift', gr.proj.shift);
            seg = struct('seg', gr.seg, 'segbb', gr.segbb, 'Area', gr.Area, ...
                'threshold', gr.threshold);
            gr_rec = struct('VOL3D', vol3D, 'SEG', seg);
            gtSaveGrainRec(phase_id, ii, gr_rec, 'fields', {'VOL3D', 'SEG'});

            fprintf(repmat('\b', [1 num_chars]));
        catch mexc
            fprintf('\n')
            gtPrintException(mexc)
            fprintf('Phase %02d, Grain: ', phase_id)
        end
    end
    fprintf('Done %4d in %f seconds\n', num_grains, toc(c))
end
