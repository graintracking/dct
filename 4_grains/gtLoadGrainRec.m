function grain_det = gtLoadGrainRec(phase_id, grain_id, varargin)
    conf = struct('fields', {{}}, 'parameters', {[]}, 'is_extended', {false}, 'full_details', {false});
    conf = parse_pv_pairs(conf, varargin);

    if (isempty(conf.parameters))
        phase_dir = fullfile('4_grains', sprintf('phase_%02d', phase_id));
    else
        phase_dir = fullfile(conf.parameters.acq.dir, '4_grains', sprintf('phase_%02d', phase_id));
    end

    for ii = numel(grain_id):-1:1
        if (conf.is_extended)
            grain_file = fullfile(phase_dir, sprintf('grain_extended_details_%04d.mat', grain_id(ii)));
        elseif (conf.full_details)
            grain_file = fullfile(phase_dir, sprintf('grain_full_details_%04d.mat', grain_id(ii)));
        else
            grain_file = fullfile(phase_dir, sprintf('grain_details_%04d.mat', grain_id(ii)));
        end

        if (isempty(conf.fields))
            grain_det(ii) = load(grain_file);
        else
            grain_det(ii) = load(grain_file, conf.fields{:});
        end
    end
end
