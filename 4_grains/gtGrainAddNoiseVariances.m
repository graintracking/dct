function gr = gtGrainAddNoiseVariances(gr, phase_id, p, detinds)
    if (~exist('phase_id', 'var')) || isempty(phase_id)
        phase_id = 1;
    end
    if (~exist('detinds', 'var')) || isempty(detinds)
        detinds = 1;
    end
    if (~exist('p', 'var')) || isempty(p)
        p = gtLoadParameters;
    end
    if isnumeric(gr)
        gr = gtLoadGrain(phase_id, gr);
    end
    if isfield(p.rec.grains.options, 'detector_weights') ...
            && isfield(p.rec.grains.options.detector_weights, 'add_weights_in_6D_algo')
        flag_add_weights = p.rec.grains.options.detector_weights.add_weights_in_6D_algo;
        if (flag_add_weights > 0)
            if flag_add_weights == 1 ...
                    && isfield(gr.proj(detinds).bl(1), 'nvar') ...
                    && ~isempty(gr.proj(detinds).bl(1).nvar)
                return
            end
            detwei = GtGrainCalcBlobNoiseVars(gr, phase_id, p, detinds);
            gr = detwei.grain;
        end
    end
end