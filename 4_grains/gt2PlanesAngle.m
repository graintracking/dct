% angle between two plane normals (any vectors)
% Output is between 0 and 90.
% If input is not normalized, use norm_flag=1 !


function ang=gt2PlanesAngle(bpl,opl,norm_flag)

if exist('norm_flag','var')
  if norm_flag
    ang=acosd((opl*bpl')./sqrt(sum(opl.*opl,2))/norm(bpl));
    ang=min(ang,180-ang);
    return
  end
end
    
ang=acosd(opl*bpl');
ang=min(ang,180-ang);

end
