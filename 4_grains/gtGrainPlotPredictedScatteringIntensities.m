function gtGrainPlotPredictedScatteringIntensities(refgr, p, det_ind)
    if (~exist('det_ind', 'var') || isempty(det_ind))
        det_ind = 1;
    end

    proj = refgr.proj(det_ind);
    try
        beam_intensities = gtGrainComputeIncomingBeamIntensity(refgr, p, det_ind);
        beam_ints = beam_intensities(proj.selected);
        beam_ints = beam_ints / mean(beam_ints);

        atts_tot = gtGrainComputeBeamAttenuation(refgr, p, det_ind);

        beam_ints = beam_ints .* atts_tot(proj.selected);
    catch mexc
        gtPrintException(mexc, 'Skipping beam intensity renormalization')
        beam_ints = 1;
    end
    blob_ints = refgr.intensity(proj.selected);

    vis_spots = proj.ondet(proj.included(proj.selected));
    thetatypes = refgr.allblobs(det_ind).thetatype(vis_spots);

    if (isfield(p.cryst(refgr.phaseid), 'int_exp'))
        predicted_ints = p.cryst(refgr.phaseid).int_exp(thetatypes)';

        f = figure();
        ax = axes('parent', f);
        plot(ax, blob_ints)
        hold(ax, 'on')
        plot(ax, predicted_ints / mean(predicted_ints) * mean(blob_ints), 'y')
        predicted_ints = predicted_ints .* refgr.allblobs(det_ind).lorentzfac(vis_spots);
        plot(ax, predicted_ints / mean(predicted_ints) * mean(blob_ints), 'r')
        predicted_ints = predicted_ints .* beam_ints;
        plot(ax, predicted_ints / mean(predicted_ints) * mean(blob_ints), 'g')

%         % This was suggested in Henning's book, chapter 4, but it seems to
%         % be already included in our estimation of the intensities...
%         other_predicted_ints = predicted_ints ./ (sind(2 .* refgr.allblobs(det_ind).theta(vis_spots)));
%         plot(ax, other_predicted_ints / mean(other_predicted_ints) * mean(blob_ints), 'c')
    end

    predicted_ints = p.cryst(refgr.phaseid).int(thetatypes)';

    f = figure();
    ax = axes('parent', f);
    plot(ax, blob_ints)
    hold(ax, 'on')
    plot(ax, predicted_ints / mean(predicted_ints) * mean(blob_ints), 'y')
    predicted_ints = predicted_ints .* refgr.allblobs(det_ind).lorentzfac(vis_spots);
    plot(ax, predicted_ints / mean(predicted_ints) * mean(blob_ints), 'r')
    predicted_ints = predicted_ints .* beam_ints;
    plot(ax, predicted_ints / mean(predicted_ints) * mean(blob_ints), 'g')

%     % This was suggested in Henning's book, chapter 4, but it seems to
%     % be already included in our estimation of the intensities...
%     other_predicted_ints = predicted_ints ./ (sind(2 .* refgr.allblobs(det_ind).theta(vis_spots)));
%     plot(ax, other_predicted_ints / mean(other_predicted_ints) * mean(blob_ints), 'c')

    fprintf('%d) %d, %g\n', ...
        [(1:numel(blob_ints))', refgr.allblobs(det_ind).thetatype(vis_spots), ...
        blob_ints - predicted_ints / mean(predicted_ints) * mean(blob_ints)]')
    hold(ax, 'off')
end
