function [structure, proj] = gtGrainRotateStructure(structure, proj, type,  parameters, rot_tensor_angles, rot_axes, interpolation)
    if (any([size(rot_tensor_angles, 1), size(rot_tensor_angles, 2)] == 1))
        rot_angles = rot_tensor_angles;
        if (~exist('rot_axes', 'var') || isempty(rot_axes))
            rot_axes = [0 0 1];
        end
        rot_tensor = gtMathsRotationTensorComposite(rot_axes, rot_angles);
    else
        rot_tensor = rot_tensor_angles;
    end
    if (~exist('interpolation','var') || isempty(interpolation))
        interpolation = 'linear';
    end

    structure.intensity = gtRotateVolume(structure.intensity, rot_tensor, [], [], interpolation);

    switch (upper(type))
        case 'ODF6D'
            dmvol = structure.voxels_avg_R_vectors;
            dmvol(:, :, :, 1) = gtRotateVolume(dmvol(:, :, :, 1), rot_tensor, [], [], interpolation);
            dmvol(:, :, :, 2) = gtRotateVolume(dmvol(:, :, :, 2), rot_tensor, [], [], interpolation);
            dmvol(:, :, :, 3) = gtRotateVolume(dmvol(:, :, :, 3), rot_tensor, [], [], interpolation);
            structure.voxels_avg_R_vectors = dmvol;
        case 'VOL3D'
    end

    proj.centerpix = proj.centerpix * rot_tensor;

    structure.shift = gtFwdSimComputeVolumeShifts(proj, parameters, size(structure.intensity));
end
