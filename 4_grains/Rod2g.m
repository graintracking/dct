function invg = Rod2g(r)
% function invg = Rod2g(r)
%
% It calculates and returns the INVERSE of the orientation matrix g 
% given a Rodrigues vector 'r'.
% 
% invg: CRYSTAL -> SAMPLE coordinate transformation for COLUMN vectors.
%   v_sample = invg*v_crystal
%
% The Rodrigues vector represents a rotation in the Sample reference frame
% which brings the base axes of the Sample reference parallel with the
% axes of the Crystal reference that is fixed to the crystal. This is the 
% ususal DCT convention and is defined and applied in gtCrystRodriguesVector.
% 
% For the theory, see e.g. equation 3.13 in Three-Dimensional X-Ray 
% Diffraction Microscopy by H.F. Poulsen.
%
%
% Original written by EML Sept. 04.
%
% AK - 13/11/2008 rewritten with the permutation and identity matrices hard
% coded rather than derived each time (2x faster!)
%
% 2012, Modified by Nicola Vigano', nicola.vigano@esrf.fr
%  - Made even faster, by saving partial computation: 15x faster than the latest :)

    % initialise the permutation tensor (do the 3rd as 1st, to allocate it all)
    e(:, :, 3) = [ 0     1     0; ...
                  -1     0     0; ...
                   0     0     0];
    e(:, :, 1) = [ 0     0     0; ...
                   0     0     1; ...
                   0    -1     0];
    e(:, :, 2) = [ 0     0    -1; ...
                   0     0     0; ...
                   1     0     0];

    % Saving partial computation
    rSquareNorm = sum(r .^ 2);

    % Initialise the orientation matrix
    invg = zeros(3, 3);
    % Loop over the elements of the orientation matrix
    for ii = 1:3
        for jj = 1:3
            invg(ii, jj) = (1 - rSquareNorm) * (ii == jj) + 2 * r(ii) * r(jj);
            % Summing over the remaining component
            for k = 1:3
                invg(ii, jj) = invg(ii, jj) - 2 * r(k) * e(ii, jj, k);
            end
        end
    end
    invg = invg / (1 + rSquareNorm);
end
