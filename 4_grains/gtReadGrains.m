function [r_vectors, positions] = gtReadGrains(phaseid)
%     [r_vectors, positions] = gtReadGrains(phaseid)
%     ----------------------------------------------
%     Reads from the 4_grains/phase_## folder
%     r_vectors = [grainid rx ry rz]
%     positions = [grainid grainx grainy grainz grainsize]

    % how many grains in the sample?
    sample = GtSample.loadFromFile();
    num_grains = sample.phases{phaseid}.getNumberOfGrains();

    r_vectors = zeros(num_grains, 4);
    positions = zeros(num_grains, 5);

    for gid = 1:num_grains
        r_vectors(gid, :) = [gid sample.phases{phaseid}.getR_vector(gid)];
        bbox = sample.phases{phaseid}.getBoundingBox(gid);
        grainsize = sum(bbox(gid, 4:6)) / 3; % Is this the real grainsize??
        positions(gid, :) = [gid sample.phases{phaseid}.getCenter(gid) grainsize];
    end

    phase_dir = fullfile('4_grains', sprintf('phase_%02d', phaseid));
    save(fullfile(phase_dir, 'r_vectors.mat'), 'r_vectors', '-v7.3');
    save(fullfile(phase_dir, 'positions.mat'), 'positions', '-v7.3');
end
