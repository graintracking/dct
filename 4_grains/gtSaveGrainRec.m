function gtSaveGrainRec(phase_id, grain_id, grain_rec, varargin)
    details_fields = {'VOL3D', 'ODF6D', 'ODFw', 'ODFuvw', 'SEG'};
    conf = struct( ...
        'fields', {details_fields}, ...
        'parameters', {[]}, ...
        'is_extended', {false}, ...
        'clean', {false} );
    conf = parse_pv_pairs(conf, varargin);

    % Only allow for the possible fields
    conf.fields = intersect(conf.fields, details_fields);
    conf.fields = intersect(conf.fields, fieldnames(grain_rec));

    phase_dir = fullfile('4_grains', sprintf('phase_%02d', phase_id));
    if (conf.is_extended)
        grain_file = fullfile(phase_dir, sprintf('grain_extended_details_%04d.mat', grain_id));
    else
        grain_file = fullfile(phase_dir, sprintf('grain_details_%04d.mat', grain_id));
    end

    if (conf.clean)
        if (exist(grain_file, 'file'))
            gr_rec = load(grain_file);
        else
            gr_rec = struct();
        end
        for ii_f = 1:numel(conf.fields)
            gr_rec.(conf.fields{ii_f}) = grain_rec.(conf.fields{ii_f});
        end
        save(grain_file, '-struct', 'gr_rec', '-v7.3');
    else
        mat_grain_file = matfile(grain_file, 'Writable', true);
        for ii_f = 1:numel(conf.fields)
            mat_grain_file.(conf.fields{ii_f}) = grain_rec.(conf.fields{ii_f});
        end
    end
end
