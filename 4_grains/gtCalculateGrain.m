function grain = gtCalculateGrain(grain, parameters, varargin)
% GTCALCULATEGRAIN  Calulates diffraction vectors and detector intersection points
%
%     grain = gtCalculateGrain(grain, [parameters], varargin)
%     -----------------------------------------------------------------
%
%     INPUT:
%       grain      = <struct>   grain of interest
%       parameters = <struct>   parameters file {parameters.mat}
%
%     OPTIONAL INPUT (p/v pairs):
%       showfigure = <logical>  show or not the figure {false}
%       showmode   = <string> (one of: {'color'} | 'axis')
%                   'color' : show or not the prediction spot positions
%                             coloured as omega
%                   'axis'  : plot axis, image reference and beam direction
%                             in the image
%       difspots   = <boolean>  Shows the difspot images from the stack. It
%                   requires a .proj data structure to have been computed already.
%                   Default: false
%       markersize = <double>   marker size {10}
%       clims      = <double>   scale of grays {[-300 500]}
%       usestrain  = <logical>  use strain info
%       variants   = <logical>  Use twin variants simulated orientation
%                   stored in 'g_twins', if existing {false}
%       det_ind    = <double>   Detector index. Default: 1
%
%     OUTPUT:
%       grain      = <struct>   grain of interest. Added fields:
%            .allblobs(fsimID)  = <struct>   Simulated reflections data
%            .used_fam          = <double>   List of used unique HKL families
%            .used_famsp        = <double>   List of all the used HKL families
%
%     Simplified version of gtFedGenerateRandomGrain   15-03-2012  - WLudwig


if (nargin < 1)
    help(mfile)
    error('gtCalculateGrain:wrong_arguments', 'Not enough arguments')
end
if (~exist('parameters', 'var') || isempty(parameters))
    parameters = gtLoadParameters();
end

% set default values for optional arguments
app.showfigure = false;
app.showmode   = 'color'; % Options: {'color'} | 'axis'
app.markersize = 30;
app.variants   = false;
app.usestrain  = isfield(parameters.fsim, 'mode') && strcmpi(parameters.fsim.mode, 'global_fit');
app.included   = [];
app.ref_omind  = [];
app.det_ind    = 1;
app.pl_ind     = [];
if (isfield(parameters.fsim, 'clims'))
    app.clims = parameters.fsim.clims;
else
    app.clims = parameters.fsim.Grayscale;
end

[app, rej_pars] = parse_pv_pairs(app, varargin);
if (app.clims == 0)
    app.clims = [];
end

% If strain data is not available, disable strain
if ~isfield(grain, 'strain') || ~isfield(grain.strain, 'strainT') || ...
    isnan(grain.strain.strainT(1, 1))

    app.usestrain = false;
end

% Rodrigues vector (grain orientation)
R_vec = grain.R_vector;

cryst  = parameters.cryst(grain.phaseid);
labgeo = parameters.labgeo;
samgeo = parameters.samgeo;

if (~isfield(labgeo, 'omstep') || isempty(labgeo.omstep))
    labgeo.omstep = gtAcqGetOmegaStep(parameters);
end

%%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % Detector geometry

if (~isfield(parameters, 'detgeo') || isempty(parameters.detgeo))
    parameters.detgeo = gtGeoConvertLegacyLabgeo2Detgeo(labgeo);
end
if (isempty(app.det_ind))
    app.det_ind = 1:numel(parameters.detgeo);
end

if (isfield(parameters, 'diffractometer'))
    for ii_d = app.det_ind
        diff = parameters.diffractometer(ii_d);
        if (any(size(diff.axes_rotation) ~= size(labgeo.rotdir)) || ...
                any(diff.axes_rotation ~= labgeo.rotdir))
            warning('gtDefSyntheticGrainCreate:inconsistent_rotation_dir', ...
                'The diffractometer rotation axis differs from the labgeo rotation axis')
            disp(diff.axes_rotation)
            disp(labgeo.rotdir)
        end
    end
end

%%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % Crystallography from parameters

%disp('Translating Miller indices into normalized cartesian coordinates for plane normals')
Bmat = gtCrystHKL2CartesianMatrix(cryst.latticepar);

% Compute all orientation matrices g (Reminder: Vc = g . Vs)
g = gtMathsRod2OriMat(R_vec');

for det_ind = app.det_ind
    acq = parameters.acq(det_ind);
    detgeo = parameters.detgeo(det_ind);

    mode = acq.type;
    if (ismember(mode, {'360degree', '180degree'}))
        mode = 'dct';
    end

    is_dct = strcmpi(mode, 'dct');
    is_topotomo = strcmpi(mode, 'topotomo');

    use_diffractometer = is_topotomo ...
        || (isfield(parameters, 'diffractometer') ...
            && numel(parameters.diffractometer) >= det_ind ...
            && det_ind > 1);
    if (use_diffractometer)
        diff_acq = parameters.diffractometer(det_ind);
        diff_ref = parameters.diffractometer(1);
    end

    if (~is_dct && ~is_topotomo)
        error('gtCalculateGrain:wrong_argument', ...
            'Acquisition type should be: {"dct"} | "topotomo", but "%s" was given ("360degree" corresponds to "dct")', ...
            mode);
    end

    if (is_topotomo && isempty(app.pl_ind))
        if (~isfield(acq, 'pl_ind') || isempty(acq.pl_ind))
            warning('gtCalculateGrain:wrong_argument', ...
                'When mode is "topotomo", "pl_ind" should also be pased');
            pl_ind = app.pl_ind;
            continue
        else
            pl_ind = acq.pl_ind;
        end
    else
        pl_ind = app.pl_ind;
    end

    omstep = gtAcqGetOmegaStep(parameters, det_ind);

    if (is_dct)
        thetatypesp = cryst.thetatypesp;
        thetatypesp = reshape(thetatypesp, [], 1);

        if (size(cryst.hkl, 1) == 3 || size(cryst.hkl, 1) == 4)
            hkl = cryst.hkl(:, thetatypesp);
            hklsp = cryst.hklsp;
        else
            hkl = cryst.hkl(thetatypesp, :)';
            hklsp = cryst.hklsp';
        end
    elseif (is_topotomo)
        hkl = grain.allblobs(1).hkl(pl_ind, :)';
        hklsp = grain.allblobs(1).hklsp(pl_ind, :)';
        thetatypesp = grain.allblobs(1).thetatype(pl_ind);
    end

    % D-spacings in the reference crystal for the given reflections
    dsp_ref = gtCrystDSpacing(double(hkl), Bmat);

    % Plane normals in Cartesian CRYSTAL coordinates
    plcry = gtCrystHKL2Cartesian(double(hklsp), Bmat);

    % Express plane normals in cartesian SAMPLE CS
    pl_orig = gtVectorCryst2Lab(plcry', g)';

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Compute diffraction angles and detector intersection points

    % Impose strain on grain
    if (app.usestrain)
        % Deformation tensor (use strain tensor for now)
        defT = grain.strain.strainT;

        % New deformed plane normal and relative elongation (relative d-spacing)
        [pl_samd, drel] = gtStrainPlaneNormals(pl_orig, defT); % unit vector
    else
        pl_samd = pl_orig;
        num_plane_normals = size(pl_orig, 2);
        drel = ones(1, num_plane_normals);
    end

    % d-spacing after applying the strain tensor
    dsp_d = drel .* dsp_ref;

    % Bragg angles theta and sin(theta) in deformed state is inversely proportional
    % to elongation
    [theta, sinth] = gtCrystTheta(dsp_d, acq.energy);

    % The plane normals need to be brought in the Lab reference where the
    % beam direction and rotation axis are defined. This only matters
    % if the Sample reference at omega=0 does not coincide with the Lab 
    % reference (usually not the case, but we should account for the 
    % possibility, as it is provided in 'samgeo').
    % Use the Sample -> Lab orientation transformation assuming omega=0;
    % (vector length preserved for free vectors)
    if (is_dct)
        t = eye(3);
        if (use_diffractometer)
            t = gtGeoDiffractometerTensor(diff_acq, 'sam2lab', ...
                'reference_diffractometer', diff_ref);

            rotcomp = gtMathsRotationMatrixComp(diff_acq.axes_rotation', 'col');
            rotdir = diff_acq.axes_rotation';
        else
            rotcomp = gtMathsRotationMatrixComp(labgeo.rotdir', 'col');
            rotdir = labgeo.rotdir';
        end
        pl_labd = gtGeoSam2Lab(pl_samd', t, labgeo, samgeo, true)';
    elseif (is_topotomo)
        num_imgs = gtAcqTotNumberOfImages(parameters, det_ind);
        ones_imgs = ones(num_imgs, 1);

        ws = omstep * (0:(num_imgs-1));
        t = gtGeoDiffractometerTensor(diff_acq, 'sam2lab', ...
            'reference_diffractometer', diff_ref, 'angles_rotation', ws);

        pl_labd = gtGeoSam2Lab(pl_samd', t, labgeo, samgeo, true)';

        rotcomp = gtMathsRotationMatrixComp(diff_acq.axes_basetilt', 'col');
        rotdir = diff_acq.axes_basetilt';
    end

    % Four omegas of the plane normal (1-3 and 2-4 are the two Friedel pairs):
    % pls is plus or minus pl, and St is the rotation tensor per each omega
    [om4, pllab4, pls4, rot_l2s_r4, omind4] = gtFedPredictOmegaMultiple( ...
        pl_labd, sinth, labgeo.beamdir', rotdir, rotcomp, []);

    num_hkl_indxs = size(hkl, 1);
    if (is_topotomo)
        om4 = mod(om4 + 180, 360) - 180;

        hkl = hkl(:, ones_imgs);
        hklsp = hklsp(:, ones_imgs);
        thetatypesp = thetatypesp(ones_imgs, :);

        plcry = plcry(:, ones_imgs);
        pl_samd = pl_samd(:, ones_imgs);
        pl_orig = pl_orig(:, ones_imgs);
        theta = theta(:, ones_imgs);
        sinth = sinth(:, ones_imgs);
    end

    % Delete those where no reflection occurs
    no_fwd_diffr = (isnan(om4(1, :)) | isnan(om4(3, :)));
    no_fwd_diffr4 = no_fwd_diffr([1 1 1 1], :);

    %%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % % Propagating the same common values for all the 4 omegas coming from a
    % % single plane normal.
    plorig4 = pl_orig(:, :, [1 1 1 1]);
    plcry4 = plcry(:, :, [1 1 1 1]);
    plsamd4 = pl_samd(:, :, [1 1 1 1]);

    hkl = hkl';
    hklsp = hklsp';

    thetatypesp4 = thetatypesp(:, [1 1 1 1]);
    hkl4 = hkl(:, :, [1 1 1 1]);
    hklsp4 = hklsp(:, :, [1 1 1 1]);
    hklsp4(:, :, [3 4]) = -hklsp4(:, :, [3 4]);
    sinth4 = sinth([1 1 1 1], :);
    theta4 = theta([1 1 1 1], :);

    if (isempty(app.ref_omind))
        %%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % % Following the convention of the matching output, the omega value
        % % smaller than 180deg (spot "a") is used in the pair data.

        chind4 = om4(1, :) > om4(3, :);

        pllab4(:, chind4, [1, 3]) = pllab4(:, chind4, [3, 1]);
        pls4(:, chind4, [1, 3])   = pls4(:, chind4, [3, 1]);
        om4([1, 3], chind4)       = om4([3, 1], chind4);
        omind4([1, 3], chind4)    = omind4([3, 1], chind4);
        rot_l2s_r4(:, :, chind4, [1, 3]) = rot_l2s_r4(:, :, chind4, [3, 1]);
        hklsp4(chind4, :, [1, 3]) = hklsp4(chind4, :, [3, 1]);

        chind4 = om4(2, :) > om4(4, :);

        pllab4(:, chind4, [2, 4]) = pllab4(:, chind4, [4, 2]);
        pls4(:, chind4, [2, 4])   = pls4(:, chind4, [4, 2]);
        om4([2, 4], chind4)       = om4([4, 2], chind4);
        omind4([2, 4], chind4)    = omind4([4, 2], chind4);
        rot_l2s_r4(:, :, chind4, [2, 4]) = rot_l2s_r4(:, :, chind4, [4, 2]);
        hklsp4(chind4, :, [2, 4]) = hklsp4(chind4, :, [4, 2]);

        %%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Four omegas of the plane normal (1-3 and 2-4 are the two Friedel pairs):
        % % But the two Friedel pairs are going to be 1a-1b and 2a-2b.

        pllab4(:, :, [2, 3]) = pllab4(:, :, [3, 2]);
        pls4(:, :, [2, 3])   = pls4(:, :, [3, 2]);
        om4([2, 3], :)       = om4([3, 2], :);
        omind4([2, 3], :)    = omind4([3, 2], :);
        rot_l2s_r4(:, :, :, [2, 3]) = rot_l2s_r4(:, :, :, [3, 2]);
        hklsp4(:, :, [2, 3]) = hklsp4(:, :, [3, 2]);
    end

    %%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % % Unfolding the 4-fold data structures

    no_fwd_diffr = reshape(no_fwd_diffr4, [], 1);

    om = reshape(om4, [], 1);
    omind = reshape(omind4, [], 1);
    sinth = reshape(sinth4, [], 1);
    theta = reshape(theta4, [], 1);

    plcry = reshape(permute(plcry4, [1 3 2]), 3, []);
    plsamd = reshape(permute(plsamd4, [1 3 2]), 3, []);
    pllab = reshape(permute(pllab4, [1 3 2]), 3, []);
    pls = reshape(permute(pls4, [1 3 2]), 3, []);
    plorig = reshape(permute(plorig4, [1 3 2]), 3, []);

    rot_l2s = reshape(permute(rot_l2s_r4, [1 2 4 3]), 3, 3, []);

    thetatypesp = reshape(thetatypesp4', [], 1);
    hkl = reshape(permute(hkl4, [3 1 2]), [], num_hkl_indxs);
    hklsp = reshape(permute(hklsp4, [3 1 2]), [], num_hkl_indxs);

    if (~isempty(app.ref_omind))
        gr_omind_ind = [ ...
            find(omind == 1); find(omind == 2); ...
            find(omind == 3); find(omind == 4) ];
        ref_omind_ind = [ ...
            find(app.ref_omind == 1); find(app.ref_omind == 2); ...
            find(app.ref_omind == 3); find(app.ref_omind == 4) ];

        no_fwd_diffr(ref_omind_ind) = no_fwd_diffr(gr_omind_ind);

        om(ref_omind_ind) = om(gr_omind_ind);
        omind(ref_omind_ind) = omind(gr_omind_ind);
        sinth(ref_omind_ind) = sinth(gr_omind_ind);
        theta(ref_omind_ind) = theta(gr_omind_ind);

        plcry(:, ref_omind_ind) = plcry(:, gr_omind_ind);
        plsamd(:, ref_omind_ind) = plsamd(:, gr_omind_ind);
        pllab(:, ref_omind_ind) = pllab(:, gr_omind_ind);
        pls(:, ref_omind_ind) = pls(:, gr_omind_ind);
        plorig(:, ref_omind_ind) = plorig(:, gr_omind_ind);

        rot_l2s(:, :, ref_omind_ind) = rot_l2s(:, :, gr_omind_ind);

        thetatypesp(ref_omind_ind) = thetatypesp(gr_omind_ind);
        hkl(ref_omind_ind, :) = hkl(gr_omind_ind, :);
        hklsp(ref_omind_ind, :) = hklsp(gr_omind_ind, :);
    end

    if (~isempty(app.included))
        no_fwd_diffr = no_fwd_diffr(app.included);

        om = om(app.included);
        omind = omind(app.included);
        sinth = sinth(app.included);
        theta = theta(app.included);

        plcry = plcry(:, app.included);
        plsamd = plsamd(:, app.included);
        pllab = pllab(:, app.included);
        pls = pls(:, app.included);
        plorig = plorig(:, app.included);

        rot_l2s = rot_l2s(:, :, app.included);

        thetatypesp = thetatypesp(app.included);
        hkl = hkl(app.included, :);
        hklsp = hklsp(app.included, :);
    end

    if (is_dct)
        ph = zeros(size(om));
        phind = zeros(size(omind));

        if (use_diffractometer)
            rot_s2l = gtGeoDiffractometerTensor(diff_acq, 'sam2lab', ...
                'reference_diffractometer', diff_ref, ...
                'angles_rotation', om);
            rot_l2s = gtGeoDiffractometerTensor(diff_acq, 'lab2sam', ...
                'reference_diffractometer', diff_ref, ...
                'angles_rotation', om);
        else
            rot_s2l = permute(rot_l2s, [2 1 3]);
        end
    elseif (is_topotomo)
        ph = om;
        phind = omind;
        om = reshape(ws([1 1 1 1], :), [], 1);
        if (~isempty(app.included))
            om = om(app.included);
        end
        omind = grain.allblobs(1).omind(pl_ind);

        rot_s2l = gtGeoDiffractometerTensor(diff_acq, 'sam2lab', ...
            'reference_diffractometer', diff_ref, ...
            'angles_rotation', om, 'angles_basetilt', ph);
        rot_l2s = gtGeoDiffractometerTensor(diff_acq, 'lab2sam', ...
            'reference_diffractometer', diff_ref, ...
            'angles_rotation', om, 'angles_basetilt', ph);
    end

    %%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % % Computing the remaining physical quantities

    % Diffraction vectors % takes coloumn vectors
    dveclab = gtFedPredictDiffVecMultiple(pllab, labgeo.beamdir')';
    dveclab = gtMathsNormalizeVectorsList(dveclab);

    % Eta angles % takes row vector
    eta = gtGeoEtaFromDiffVec(dveclab, labgeo);

    % Diffraction vector in Sample reference
    dvecsam = gtGeoLab2Sam(dveclab, rot_l2s, labgeo, samgeo, true);

    % Let's convert grain center Sam -> Lab
    gcsam_v = grain.center(ones(1, size(rot_s2l, 3)), :);

    if ((isfield(parameters.acq, 'correct_sample_shifts')) && ...
            (parameters.acq.correct_sample_shifts))
        [~, shifts_sam] = gtMatchGetSampleShifts(parameters, om);
        gcsam_v = gcsam_v + shifts_sam;
    end
 
    gclab_v_t = gtGeoSam2Lab(gcsam_v, rot_s2l, labgeo, samgeo, false)';

    % Lorentz factor for the W-bradening of projections (note that eq. 4.1
    % from page 36 in Henning's book refers to the scattering intensity)
    Lfactor = 1 ./ abs(sind(eta));

    allblobs = gtGrainAllblobsDefinition();
    allblobs.type = mode;
    allblobs.plcry = plcry';      % undeformed plane normals in the Crystal ref
    allblobs.plorig = plorig';    % unsigned undeformed plane normals in Sample ref
    allblobs.plsamdef = plsamd';  % unsigned deformed plane normals in Sample ref
    allblobs.pl = pls';           % signed (!) deformed plane normals in Sample ref
    allblobs.pllab = pllab';      % signed (!) deformed plane normals rotated to diffraction in Lab ref
    allblobs.hkl = hkl;           % {hkl} index
    allblobs.hklsp = hklsp;       % specific/signed (hkl) index
    allblobs.sintheta = sinth;    % sin(Bragg_angle) in deformed state
    allblobs.theta = theta;       % Bragg angle in deformed state (deg)
    allblobs.eta = eta;           % Eta azimuth angle in deformed state (deg)
    allblobs.thetatype = thetatypesp;
    allblobs.lorentzfac = Lfactor;% Lorentz factor
    allblobs.omega = om;          % omega rotation angles (0..360deg) in degrees
    allblobs.omind = omind;       % omega variant index (1..4)
    allblobs.phi = ph;            % phi rotation angles (0..360deg) in degrees (base tilt)
    allblobs.phind = phind;       % phi variant index (1..4)
    allblobs.dvec = dveclab;      % diffracted beam dir in Lab ref
    allblobs.dvecsam = dvecsam;   % diffracted beam dir in Sample ref
    allblobs.srot = rot_l2s;      % Sample->Lab (!) rotation coordinate transform matrix for column (!) vectors
                                  % OR Lab->Sample (!) rotation coordinate transform matrix for row (!) vectors

    allblobs.rot_s2l = rot_s2l;   % 4x4 Sample->Lab (!) rotation coordinate transform matrix for column (!) vectors
    allblobs.rot_l2s = rot_l2s;   % 4x4 Lab->Sample (!) rotation coordinate transform matrix for column (!) vectors

    % u,v,w or u,v,phi,w coordinates on the detector
    uv = gtFedPredictUVMultiple([], dveclab', gclab_v_t, ...
        detgeo.detrefpos', detgeo.detnorm', detgeo.Qdet, ...
        [detgeo.detrefu, detgeo.detrefv]')';

    w = om / omstep;

    % Precompute which reflections will actually fall into the detector
    ondet = ~no_fwd_diffr & ...
        (uv(:, 1) >= 1) & (detgeo.detsizeu >= uv(:, 1)) & ...
        (uv(:, 2) >= 1) & (detgeo.detsizev >= uv(:, 2));

    % Initialse output variables
    if (is_dct)
        allblobs.detector = struct('uvw', {[uv, w]}, 'ondet', {ondet});
    elseif (is_topotomo)
        phstep = gtAcqGetPhiStep(parameters, det_ind);

        p = ph / phstep; % + 0.5

        phi_range = acq.range_basetilt / phstep; % + 0.5
        ondet = ondet & p > phi_range(1) & p < phi_range(2);

        allblobs.detector = struct('uvpw', {[uv, p, w]}, 'ondet', {ondet});
    end

    try
        grain.allblobs(det_ind) = allblobs;
    catch mexc
        if gtCheckExceptionType(mexc, 'MATLAB:heterogeneousStrucAssignment')
            fprintf(' - Updating legacy "allblobs" structure..')
            c = tic();
            % We now handle the fact that proj can be a legacy datastructure
            base_ab = gtGrainAllblobsDefinition();
            for ii_p = numel(grain.allblobs):-1:1
                new_ab(ii_p) = gtAddMatFile(base_ab, grain.allblobs(ii_p), true);
            end
            grain.allblobs = new_ab;

            grain.allblobs(det_ind) = allblobs;
            fprintf('\b\b: Done in %g seconds.\n', toc(c))
        end
    end

    % Visual Feedback
    if (app.showfigure)
        confs(2, :) = fieldnames(app);
        confs(1, :) = struct2cell(app);
        gtGrainShow(grain, parameters, confs{:});
    end
end

grain.full = [];
end
