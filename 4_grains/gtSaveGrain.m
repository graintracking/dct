function gtSaveGrain(phase_id, grain_id, grain, varargin)
% GTSAVEEGRAIN  Saves grain structure in 4_grains/phase_
%
%     gtSaveGrain(phase_id, grain_id, [parameters], varargin)
%     -----------------------------------------------------------------
%     INPUT:
%       phase_id   = <int>      phase_id
%       grain_id   = <int>      grain of interest
%       grain      = <struct>   grain structure
%       parameters = <struct>   parameters file {parameters.mat}
%
%     OPTIONAL INPUT (p/v pairs):
%       is_extended = <logical> {false}:  extended grain structure (6D)
    conf = struct( ...
        'fields', {{}}, ...
        'parameters', {[]}, ...
        'is_extended', {false});
    conf = parse_pv_pairs(conf, varargin);

    if (isempty(conf.parameters))
        phase_dir = fullfile('4_grains', sprintf('phase_%02d', phase_id));
    else
        phase_dir = fullfile(conf.parameters.acq.dir, '4_grains', sprintf('phase_%02d', phase_id));
    end
    if (conf.is_extended)
        grain_file = fullfile(phase_dir, sprintf('grain_extended_%04d.mat', grain_id));
    else
        grain_file = fullfile(phase_dir, sprintf('grain_%04d.mat', grain_id));
    end

    if (isempty(conf.fields))
        save(grain_file, '-struct', 'grain', '-v7.3');
    else
        mat_grain_file = matfile(grain_file, 'Writable', true);
        for ii_f = 1:numel(conf.fields)
            mat_grain_file.(conf.fields{ii_f}) = grain.(conf.fields{ii_f});
        end
    end
end
