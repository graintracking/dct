classdef GtGrainTTBlobBoundBoxesFiltering < handle
    properties (Access = public)
        threshold; % threshold of 3D TT blob
        use_astra = false; % true: use ASTRA to refine 2D spot mask; false: only refine 1D mask vertical to rotation axis.
    end

    properties (Access = protected)
        grain; % grain structure
        det_index;
        parameters;

        phi_int_stack; % spots integrated over u and v of detector resulting a 2D intensity stack about omega and phi.
        phi_rot = []; % estimated rotation center of phi; center exists only if the elastic strain are uniform.
        phi_mask;
        ospace_mask = []; % hull of 2D orientation distribution
        ospace_mask_rot = []; % rotation center of the 2D ODF hull

        spotperprot_stack; % spots integrated over phi and rotation direction projection (ideally, v direction)
        spotperprot_rot; % estimated rotation center which can be used to align spot positions with the rotation axis
        spotperprot_mask;
        grain_rot_dir_hull_mask = []; % hull of the projection of distorted grain volume along rotation axis.
        grain_rot_dir_hull_mask_rot = []; % rotation center of 2D volume hull vertical to rotation axis.

        spot_stack_astra; % 3D stack of the 2D TT spots used for ASTRA
        spot_mask_astra;
    end

    methods (Access = public)
        function self = GtGrainTTBlobBoundBoxesFiltering(gr, phaseID, p, detind, thres, bool_use_astra)
            % ttspotmaskobj = GtGrainTTBlobBoundBoxesFiltering(gr, phaseID, p, detind, thres, bool_use_astra)
            % Algning the TT spot positions with rotation axis and refining
            % the TT spot 2D masks.
            % gr             : <struct> grain structure or grain ID
            % phaseID        : phase ID (default: 1)
            % p              : <struct> parameters' structure
            % detind         : detector index of the TT scan
            % thres          : <double> threshold of 3D TT blobs to produce
            %                           initial masks.
            % bool_use_astra : <logical> true: use ASTRA to refine 2D spot mask;
            %                            false: only refine 1D mask vertical to rotation axis.
            %
            % (choose a suitable threshold of 3D TT blobs. GtGuiThresholdGrain(gr.proj(detind).bl(1).intm*gr.proj(detind).bl(1).intensity) can be used.)
            % obj_TT_mask = GtGrainTTBlobBoundBoxesFiltering(gr, phaseID, p, detind, thres, bool_use_astra)
            % obj_TT_mask.updateGeometry(phaseID, flag_save) fits the
            % rotation axis.
            % (obj_TT_mask.initStacks(thres, verbose) can restart the
            % object.)
            % (obj_TT_mask.filterPhiMask(padding, verbose) refines the mask
            % of phi.)
            % (obj_TT_mask.filterSpotRotDirProjMask(padding, verbose)
            % refines the mask integrated over phi and rotation axis. This
            % mask is applied to the stack used to fit rotation axis.)
            % obj_TT_mask.filterSpotMaskUsingASTRA(padding, verbose)
            % refines the 2D masks of 2D TT spots.
            % obj_TT_mask.updateIntStack(verbose) applies the masks to the
            % stacks and update the stacks.
            % obj_TT_mask.saveGrainAndParameterToFile(phaseID) saves the
            % updated parameters and grain structure to file.
            % 24/05/2024 by Zheheng
            if (~exist('p', 'var')) || isempty(p)
                p = gtLoadParameters();
            end
            if strcmpi(p.acq(detind).type, 'topotomo')
                if (~exist('phaseID', 'var')) || isempty(phaseID)
                    phaseID = 1;
                end
                if isnumeric(gr)
                    gr = gtLoadGrain(phaseID, gr);
                end
                for ii_det = 1:numel(gr.proj)
                    if ii_det ~= detind
                        gr.proj(ii_det).bl = [];
                    end
                end
                if (exist('bool_use_astra', 'var') && (~isempty(bool_use_astra)))
                    self.use_astra = bool_use_astra;
                end
                self.parameters = p;
                self.grain = gr;
                self.det_index = detind;
                self.threshold = thres;
                self.initStacks(); % initialize the stacks by simple thresholding
            else
                error('Failed since the indicated detector is not TT scan !')
            end
        end

        function initStacks(self, thres, verbose)
            % self.initStacks(thres, verbose)
            % initialize the stacks by simple thresholding
            % thres   : threshold used for TT 3D blobs. Default is last value.
            % verbose : show images. Default is false.
            if (exist('thres', 'var') && (~isempty(thres)))
                self.threshold = thres;
            end
            if (~exist('verbose', 'var')) || isempty(verbose)
                verbose = false;
            end
            nof_bl = self.get_number_of_blobs();
            [~, ~, bbpimbounds] = self.getTotalBoundingBoxUVP();

            % To integrate spots along rotation direction projection, the
            % needed parameters are calculated.
            [rotdirprojang, slice_size, datalength_perprot, rot2Dpos, rot1Dpos] = self.getSpotRotDirProjInfo();

            % simple intialization of the masks used to initialize the stacks.
            spotperprotmask = GtGrainTTBlobBoundBoxesFiltering.fwdProjection2D(...
                true(slice_size(1), slice_size(2), nof_bl), rot2Dpos, ...
                rotdirprojang, datalength_perprot, rot1Dpos);
            self.spotperprot_mask = spotperprotmask > 0;
            self.phi_mask = true(bbpimbounds * [-1; 1] + 1, nof_bl);
            self.spot_mask_astra = true(...
                self.grain.proj(self.det_index).num_cols, ...
                self.grain.proj(self.det_index).num_rows, nof_bl);

            % initialize the stacks
            [pmask, pintstack, spmask, spstack, spmask_astra, spstack_astra] = update_stacks(self);
            self.phi_int_stack = pintstack;
            self.calcRotPosOfPhi();
            self.phi_mask = pmask;
            self.spot_stack_astra = spstack_astra;
            self.spot_mask_astra = spmask_astra;
            spotperprotstack = GtGrainTTBlobBoundBoxesFiltering.fwdProjection2D(...
                spstack, rot2Dpos, rotdirprojang, datalength_perprot, rot1Dpos);
            spotperprotstack = reshape(spotperprotstack, datalength_perprot, nof_bl);
            self.spotperprot_stack = spotperprotstack; % spots integrated over phi and rotation direction projection on the detector.
            self.calcRotPosOfSpotRotDirProj(); % calculate the rotation center of the stack spotperprot_stack.

            % initialize the masks
            spotperprotmask = GtGrainTTBlobBoundBoxesFiltering.fwdProjection2D(...
                spmask, rot2Dpos, rotdirprojang, datalength_perprot, rot1Dpos);
            spotperprotmask = reshape(spotperprotmask, datalength_perprot, nof_bl) > 0;
            self.spotperprot_mask = spotperprotmask;
            if verbose
                GtVolView(pintstack / max(pintstack(:)), 'overlay', pmask);
                GtVolView(spstack_astra / max(spstack_astra(:)), 'overlay', spmask_astra);
                GtVolView(spotperprotstack / max(spotperprotstack(:)), 'overlay', spotperprotmask);
            end
        end

        function updateIntStacks(self, verbose)
            % self.updateIntStacks(verbose)
            % apply the masks to the stacks and update the stacks.
            if (~exist('verbose', 'var')) || isempty(verbose)
                verbose = false;
            end

            % apply the masks to the stacks.
            [~, pintstack, ~, spstack, ~, spstack_astra] = update_stacks(self);
            nof_bl = self.get_number_of_blobs();
            [rotdirprojang, ~, datalength_perprot, rot2Dpos, rot1Dpos] = self.getSpotRotDirProjInfo();
            spotperprotstack = GtGrainTTBlobBoundBoxesFiltering.fwdProjection2D(...
                spstack, rot2Dpos, rotdirprojang, datalength_perprot, rot1Dpos);
            spotperprotstack = reshape(spotperprotstack, datalength_perprot, nof_bl);
            if verbose
                GtVolView.compareVolumes({pintstack, self.phi_int_stack})
                GtVolView.compareVolumes({spstack_astra, self.spot_stack_astra})
                GtVolView.compareVolumes({spotperprotstack, self.spotperprot_stack})
            end
            % update the stacks
            self.phi_int_stack = pintstack;
            self.calcRotPosOfPhi();
            self.spot_stack_astra = spstack_astra;
            self.spotperprot_stack = spotperprotstack;
            self.calcRotPosOfSpotRotDirProj();
        end

        function phi_rot_pos = calcRotPosOfPhi(self)
            % self.calcRotPosOfPhi()
            % calculate rotation center of phi-omega stack.
            size_stack = size(self.phi_int_stack);
            tmphalfstack1 = self.phi_int_stack(:, 1:(size_stack(2) / 2));
            tmphalfstack2flip = self.phi_int_stack(...
                size_stack(1):-1:1, ...
                (size_stack(2) / 2 + 1):size_stack(2));
            rotcen_shift2x = GtGrainTTBlobBoundBoxesFiltering.correlate(...
                tmphalfstack1, tmphalfstack2flip, 'renorm', true);
            phi_rot_pos = (1 + size_stack(1) + rotcen_shift2x) / 2;
            self.phi_rot = phi_rot_pos;
        end

        function spotrotdirproj_rot_pos = calcRotPosOfSpotRotDirProj(self)
            % self.calcRotPosOfSpotRotDirProj()
            % calculate rotation center of spots integrated over phi and
            % rotation direction projection
            size_stack = size(self.spotperprot_stack);
            tmphalfstack1 = self.spotperprot_stack(:, 1:(size_stack(2) / 2));
            tmphalfstack2flip = self.spotperprot_stack(...
                size_stack(1):-1:1, ...
                (size_stack(2) / 2 + 1):size_stack(2));
            rotcen_shift2x = GtGrainTTBlobBoundBoxesFiltering.correlate(...
                tmphalfstack1, tmphalfstack2flip, 'renorm', true);
            spotrotdirproj_rot_pos = (1 + size_stack(1) + rotcen_shift2x) / 2;
            self.spotperprot_rot = spotrotdirproj_rot_pos;
        end

        function filterPhiMask(self, padding, verbose)
            % self.filterPhiMask(padding, verbose)
            % refining the mask of phi
            % padding : dilate the masks by padding.
            if (~exist('padding', 'var')) || isempty(padding)
                padding = 0;
            end
            if (~exist('verbose', 'var')) || isempty(verbose)
                verbose = false;
            end
            omega_angles = self.grain.allblobs(self.det_index).omega(self.get_included_blob_indices());
            phirot = self.phi_rot;
            phi_mask_old = self.phi_mask;
            [phi_mask_new, ospacemask, ospacemaskrot] = ...
                GtGrainTTBlobBoundBoxesFiltering.filterMaskStackByRotation(...
                phi_mask_old, phirot, omega_angles, padding);
            if verbose
                GtVolView.compareVolumes({self.phi_int_stack / max(self.phi_int_stack(:)), phi_mask_old}, 'overlay', phi_mask_new)
            end
            self.phi_mask = phi_mask_new;
            self.ospace_mask = ospacemask;
            self.ospace_mask_rot = ospacemaskrot;
        end

        function filterSpotRotDirProjMask(self, padding, verbose)
            % self.filterSpotRotDirProjMask(padding, verbose)
            % refining the detector mask of perpendicular to rotation axis.
            % padding : dilate the masks by padding.
            if (~exist('padding', 'var')) || isempty(padding)
                padding = 0;
            end
            if (~exist('verbose', 'var')) || isempty(verbose)
                verbose = false;
            end
            omega_angles = self.grain.allblobs(self.det_index).omega(self.get_included_blob_indices());
            spotperprotrot = self.spotperprot_rot;
            spotperprotmask_old = self.spotperprot_mask;
            [spotperprotmask_new, grainrotdirhullmask, grainrotdirhullmaskrot] = ...
                GtGrainTTBlobBoundBoxesFiltering.filterMaskStackByRotation(...
                spotperprotmask_old, spotperprotrot, omega_angles, padding);
            if verbose
                GtVolView.compareVolumes({self.spotperprot_stack / max(self.spotperprot_stack(:)), spotperprotmask_old}, 'overlay', spotperprotmask_new)
            end
            self.spotperprot_mask = spotperprotmask_new;
            self.grain_rot_dir_hull_mask = grainrotdirhullmask;
            self.grain_rot_dir_hull_mask_rot = grainrotdirhullmaskrot;
        end

        function filterSpotMaskUsingASTRA(self, padding, verbose)
            % self.filterSpotMaskUsingASTRA(padding, verbose)
            % refining the 2D spot masks using ASTRA.
            % padding : dilate the masks by padding.
            if (~exist('padding', 'var')) || isempty(padding)
                padding = 0;
            end
            hull_thres = 0;
            if (~exist('verbose', 'var')) || isempty(verbose)
                verbose = false;
            end
            vol_geom = astra_create_vol_geom(self.grain.proj(self.det_index).vol_size_x, self.grain.proj(self.det_index).vol_size_y, self.grain.proj(self.det_index).vol_size_z);
            proj_geom = astra_create_proj_geom('parallel3d_vec', self.grain.proj(self.det_index).num_rows, self.grain.proj(self.det_index).num_cols, self.grain.proj(self.det_index).geom);
            opts = struct( ...
                'GPUindex', -1);
            astra_projector_id = astra_create_projector('cuda3d', proj_geom, vol_geom, opts);
            spmask_astra_old = permute(self.spot_mask_astra, [1, 3, 2]);
            bp_ref = astra_mex_direct_c('BP3D', astra_projector_id, single(ones(size(spmask_astra_old))));
            bp_ref = bp_ref > hull_thres;
            bp_hull = astra_mex_direct_c('BP3D', astra_projector_id, single(~spmask_astra_old));
            bp_hull = ~(bp_hull > hull_thres);
            bp_hull = bp_hull & bp_ref;
            if padding > 0
                % dilate the 3D hull for padding the mask.
                permute_vec = {[2, 1, 3], [3, 1, 2], [3, 2, 1]};
                for ii_dim = 1:3
                    for ii_pad = 1:padding
                        bp_hull = convn(bp_hull, ones(3, 1), 'same') > 0.1;
                    end
                    bp_hull = permute(bp_hull, permute_vec{ii_dim});
                end
            end
            bp_hull = single(bp_hull);
            spmask_astra = astra_mex_direct_c('FP3D', astra_projector_id, bp_hull);
            spmask_astra = permute(spmask_astra > 0, [1, 3, 2]);
            if verbose
                GtVolView.compareVolumes({self.spot_stack_astra / max(self.spot_stack_astra(:)), self.spot_mask_astra}, 'overlay', spmask_astra)
            end
            self.spot_mask_astra = spmask_astra;
        end

        function [detshift, detgeo_new] = calcDetectorShiftToFitRotAxis(self)
            % [detshift, detgeo_new] = self.calcDetectorShiftToFitRotAxis()
            % Calculate the shift to align the rotation axis projection on the detector to the
            % rotation center of stack of integrated spots.
            [rotdirprojang, ~, ~, ~, ~, rotaxisrefpos] = self.getSpotRotDirProjInfo;
            detgeo_new = self.parameters.detgeo(self.det_index);
            shift_dir = [detgeo_new.detdiru * detgeo_new.pixelsizeu; detgeo_new.detdirv * detgeo_new.pixelsizev];
            shift_dir = [sind(rotdirprojang), cosd(rotdirprojang)] * shift_dir;
            detshift = (rotaxisrefpos - self.spotperprot_rot) * shift_dir;
            detgeo_new.detrefpos = detgeo_new.detrefpos + detshift;
        end

        function [p, gr] = updateGeometry(self, phaseID, flag_save)
            % [p, gr] = self.updateGeometry(phaseID, flag_save)
            % update geometry in parameters and grain structure. If
            % flag_save is true, self.saveGrainAndParameterToFile(phaseID) runs;
            if (~exist('phaseID', 'var')) || isempty(phaseID)
                phaseID = 1;
            end
            [~, detgeo] = self.calcDetectorShiftToFitRotAxis();
            p = self.parameters;
            detind = self.det_index;
            p.detgeo(detind) = detgeo;
            gr = self.grain;
            gr = gtCalculateGrain(gr, p, 'det_ind', detind);
            tmp_allblobs = gr.allblobs(detind);
            inc_inds = self.get_included_blob_indices();
            dvec_sam = tmp_allblobs.dvecsam(inc_inds, :);
            rot_w_l2s = tmp_allblobs.srot(:, :, inc_inds);
            bbuimbounds = cat(2, gr.proj(detind).bl(:).bbuim);
            bbvimbounds = cat(2, gr.proj(detind).bl(:).bbvim);
            bb_bls_uv = [bbuimbounds(:, 1), bbvimbounds(:, 1)];
            bb_bls_uv = [bb_bls_uv, ([bbuimbounds(:, 2), bbvimbounds(:, 2)] - bb_bls_uv + 1)];
            if (isfield(p.acq, 'correct_sample_shifts'))
                omega_angles = tmp.allblobs.omega(inc_inds);
                [~, shifts_sam] = gtMatchGetSampleShifts(p, omega_angles);
            else
                nof_bl = self.get_number_of_blobs();
                shifts_sam = zeros(nof_bl, 3);
            end
            proj_geom = gtGeoProjForReconstruction(...
                dvec_sam, rot_w_l2s, gr.center, bb_bls_uv, [], ...
                detgeo, ...
                p.labgeo, ...
                p.samgeo, ...
                p.recgeo(detind), ...
                'ASTRA_grain', shifts_sam);
            gr.proj(detind).geom = proj_geom;
            self.parameters = p;
            self.grain = gr;
            if exist('flag_save', 'var') && (~isempty(flag_save)) && flag_save
                self.saveGrainAndParameterToFile(phaseID);
            end
        end

        function saveGrainAndParameterToFile(self, phaseID)
            % self.saveGrainAndParameterToFile(phaseID)
            if (~exist('phaseID', 'var')) || isempty(phaseID)
                phaseID = 1;
            end
            detind = self.det_index;
            gtSaveParameters(self.parameters);
            gr = gtLoadGrain(phaseID, self.grain.id);
            gr.proj(detind) = self.grain.proj(detind);
            gr.allblobs(detind) = self.grain.allblobs(detind);
            gtSaveGrain(phaseID, gr.id, gr);
        end
        function [proj, allblobs, detgeo] = getCurrentProjAllblobsDetgeo(self)
            % [proj, allblobs, detgeo] = self.getCurrentProjAllblobsDetgeo()
            detind = self.det_index;
            allblobs = self.grain.allblobs(detind);
            detgeo = self.parameters.detgeo(detind);
            proj = self.grain.proj(detind);

            nof_bl = self.get_number_of_blobs();
            [bbuimbounds, bbvimbounds, bbpimbounds] = self.getTotalBoundingBoxUVP();
            [projang, slice_size, datalength_perprot, rot2Dpos, rot1Dpos] = self.getSpotRotDirProjInfo();
            tmpprojstack = cell(1, nof_bl);
            if ~self.use_astra
                spmask = GtGrainTTBlobBoundBoxesFiltering.bwdProjection2D(...
                    reshape(self.spotperprot_mask, datalength_perprot, 1, nof_bl), ...
                    rot1Dpos, projang, slice_size, rot2Dpos, 0);
            end
            for ii_b = 1:nof_bl
                tmpbl = proj.bl(ii_b);
                tmppinds = self.bbox2inds(tmpbl.bbpim, bbpimbounds);
                tmpint = tmpbl.intm .* tmpbl.mask * tmpbl.intensity;
                if self.use_astra
                    tmpmask = bsxfun(@and, ...
                        self.spot_mask_astra(:, :, ii_b), ...
                        reshape(self.phi_mask(tmppinds, ii_b), 1, 1, []));
                else
                    tmpuinds = self.bbox2inds(tmpbl.bbuim, bbuimbounds);
                    tmpvinds = self.bbox2inds(tmpbl.bbvim, bbvimbounds);
                    tmpmask = bsxfun(@and, ...
                        spmask(tmpuinds, tmpvinds, ii_b), ...
                        reshape(self.phi_mask(tmppinds, ii_b), 1, 1, []));
                end
                tmpmask = tmpmask & (tmpbl.mask > 0);
                tmpint(~tmpmask) = 0;
                tmpprojstack{ii_b} = sum(tmpint, 3);
                tmpbl.mask = tmpmask;
                tmpbl.intensity = sum(tmpprojstack{ii_b}(:));
                tmpbl.intm = tmpint / tmpbl.intensity;
                proj.bl(ii_b) = tmpbl;
            end
            tmpprojstack = cat(3, tmpprojstack{:});
            proj.stack = permute(tmpprojstack, [1, 3, 2]);
        end

        function [bbuimbounds, bbvimbounds, bbpimbounds] = getTotalBoundingBoxUVP(self)
            % [bbuimbounds, bbvimbounds, bbpimbounds] = self.getTotalBoundingBoxUVP()
            % get bbuim, bbvim and bbpim of included blobs.
            bbpimbounds = cat(2, self.grain.proj(self.det_index).bl(:).bbpim);
            bbpimbounds = [min(bbpimbounds), max(bbpimbounds)];
            bbuimbounds = cat(2, self.grain.proj(self.det_index).bl(:).bbuim);
            bbuimbounds = [min(bbuimbounds), max(bbuimbounds)];
            bbvimbounds = cat(2, self.grain.proj(self.det_index).bl(:).bbvim);
            bbvimbounds = [min(bbvimbounds), max(bbvimbounds)];
        end

        function [rot_dir_digital_ondet, rot_pos_digital_ondet] = getRotAxisOnDigitalDetector(self)
            % [rot_dir_digital_ondet, rot_pos_digital_ondet] = self.getRotAxisOnDigitalDetector()
            % rotation axis projection on detector described by u, v indices.
            p = self.parameters;
            detind = self.det_index;
            detgeo = p.detgeo(detind);
            labgeo = p.labgeo;
            inc_inds = self.get_included_blob_indices();
            tmp_allblobs = self.grain.allblobs(detind);
            omega_angles = tmp_allblobs.omega(inc_inds);

            % calculate average phi from the phi values in allblobs
            % phi = A1*cosw+B1*sinw+phi_average, so phi_average is calulated
            % by LS estimator.
            phi_inds = tmp_allblobs.detector.uvpw(inc_inds, 3);
            avg_basetilt = GtGrainTTBlobBoundBoxesFiltering.getRotCen1D(omega_angles, phi_inds);
            avg_basetilt = avg_basetilt * gtAcqGetPhiStep(p, detind, 'deg');

            axes_basetilt = p.diffractometer(detind).axes_basetilt(:);
            rot_dir_tilt = gtMathsRod2OriMat(...
                tand(avg_basetilt / 2) * ...
                axes_basetilt / norm(axes_basetilt))' * ...
                labgeo.rotdir(:); % RotMatrix_Basetilt * RotDir calculates the rotation direction after base tilting.

            % calculate the direction perpendicular to rotation axis and
            % direct beam direction
            orthogonal_vec = cross(labgeo.beamdir(:), rot_dir_tilt(:), 1);

            % suppose [u; v] are the direction of projection of rotation
            % direction onto detector. Then, orthogonal_vec^T *
            % ([detdiru(:), detdirv(:)] * [u * pixelsizeu; v * pixelsizev])
            % = 0, i.e., (orthogonal_vec^T * [detdiru(:), detdirv(:)]) .*
            % [pixelsizeu, pixelsizev] is orthogonal to [u, v]. So we can
            % calulate u and v by [-v, u] = (orthogonal_vec^T *
            % [detdiru(:), detdirv(:)]) .* [pixelsizeu, pixelsizev]
            orthogonal_vec_ondet = orthogonal_vec' * [detgeo.detdiru(:), detgeo.detdirv(:)];
            orthogonal_vec_ondet = orthogonal_vec_ondet .* [detgeo.pixelsizeu, detgeo.pixelsizev];
            rot_dir_digital_ondet = [-orthogonal_vec_ondet(2); orthogonal_vec_ondet(1)] / norm(orthogonal_vec_ondet);

            % make sure the projected direction and rotation direction are
            % on the same side of beam, for convenience.
            rot_dir_ondet = [detgeo.pixelsizeu; detgeo.pixelsizev] .* rot_dir_digital_ondet;
            rot_dir_ondet = [detgeo.detdiru(:), detgeo.detdirv(:)] * rot_dir_ondet;
            check_dir = orthogonal_vec' * cross(labgeo.beamdir(:), rot_dir_ondet, 1);
            if check_dir < 0
                rot_dir_digital_ondet = -rot_dir_digital_ondet;
            end

            % calculate rotation center u0 from the u values in allblobs
            % u = A2*cosw+B2*sinw+u0, so u0 is calulated by LS estimator.
            u_inds = tmp_allblobs.detector.uvpw(inc_inds, 1);
            rot_pos_u = GtGrainTTBlobBoundBoxesFiltering.getRotCen1D(omega_angles, u_inds);

            % calculate rotation center v0 from the v values in allblobs
            % v = A3*cosw+B3*sinw+v0, so v0 is calulated by LS estimator.
            v_inds = tmp_allblobs.detector.uvpw(inc_inds, 2);
            rot_pos_v = GtGrainTTBlobBoundBoxesFiltering.getRotCen1D(omega_angles, v_inds);

            % rotation center of u and v
            rot_pos_digital_ondet = [rot_pos_u, rot_pos_v];
        end

        function [projang, slice_size, datalength_perprot, rot2Dpos, rot1Dpos, rotaxisrefpos] = getSpotRotDirProjInfo(self)
            % [projang, slice_size, datalength_perprot, rot2Dpos, rot1Dpos, rotaxisrefpos] = self.getSpotRotDirProjInfo()
            % In order to calculate the spots integrated over phi and
            % rotation direction projection on detector, this function
            % gives the needed parameters for the integration.
            [bbuimbounds, bbvimbounds] = self.getTotalBoundingBoxUVP(); % bbuim and bbvim of the spots in size of Nx2
            [rotdir, rotrefpos] = self.getRotAxisOnDigitalDetector();
            rotrefpos = rotrefpos + 1 - [bbuimbounds(1), bbvimbounds(1)];
            projang = atan2d(-rotdir(2), rotdir(1));
            length_uv = [bbuimbounds; bbvimbounds] * [-1; 1];
            slice_size = length_uv' + 1;
            proj_vec = [sind(projang), cosd(projang)];
            datalength_perprot = ceil(proj_vec * length_uv) + 1;
            rot2Dpos = length_uv' / 2 + 1;
            rot1Dpos = (datalength_perprot + 1) / 2;
            rotaxisrefpos = (rotrefpos - rot2Dpos) * (proj_vec') + rot1Dpos;
        end

        function [spotstack, spotmasks] = getSpotStack(self)
            % [spotstack, spotmasks] = self.getSpotStack()
            % get stack of 2D spots
            spotstack = self.spot_stack_astra;
            spotmasks = self.spot_mask_astra;
        end

        function [spotintstack, spotintmask] = getSpotIntOrthoRotAxisStack(self)
            % [spotintstack, spotintmask] = self.getSpotIntOrthoRotAxisStack()
            % get the stack of spots integrated over phi and rotation axis
            spotintstack = self.spotperprot_stack;
            spotintmask = self.spotperprot_mask;
        end
    end

    methods (Access = protected)
        function [pmask, pintstack, spmask, spstack, spmask_astra, spstack_astra] = update_stacks(self)
            % apply the masks to the stacks.
            nof_bl = self.get_number_of_blobs();
            [bbuimbounds, bbvimbounds, bbpimbounds] = self.getTotalBoundingBoxUVP();
            length_bbpim = bbpimbounds * [-1; 1] + 1;
            pmask = false(length_bbpim, nof_bl);
            pintstack = zeros(length_bbpim, nof_bl);
            [projang, slice_size, datalength_perprot, rot2Dpos, rot1Dpos] = self.getSpotRotDirProjInfo();
            if self.use_astra
                spmask = true(slice_size(1), slice_size(2), nof_bl);
            else
                spmask = GtGrainTTBlobBoundBoxesFiltering.bwdProjection2D(reshape(self.spotperprot_mask, datalength_perprot, 1, nof_bl), rot1Dpos, projang, slice_size, rot2Dpos, 0);
            end
            spstack = zeros(slice_size(1), slice_size(2), nof_bl);
            spmask_astra = false(self.grain.proj(self.det_index).num_cols, self.grain.proj(self.det_index).num_rows, nof_bl);
            spstack_astra = zeros(self.grain.proj(self.det_index).num_cols, self.grain.proj(self.det_index).num_rows, nof_bl);
            for ii_b = 1:nof_bl
                tmpbl = self.grain.proj(self.det_index).bl(ii_b);
                tmppinds = self.bbox2inds(tmpbl.bbpim, bbpimbounds);
                tmpuinds = self.bbox2inds(tmpbl.bbuim, bbuimbounds);
                tmpvinds = self.bbox2inds(tmpbl.bbvim, bbvimbounds);
                tmpint = tmpbl.intm .* tmpbl.mask * tmpbl.intensity;
                if self.use_astra
                    env_mask = bsxfun(@and, self.spot_mask_astra(:, :, ii_b), reshape(self.phi_mask(tmppinds, ii_b), 1, 1, []));
                else
                    env_mask = bsxfun(@and, spmask(tmpuinds, tmpvinds, ii_b), reshape(self.phi_mask(tmppinds, ii_b), 1, 1, []));
                end
                tmpint(~env_mask) = 0;
                tmpmask = tmpint > self.threshold;
                tmpphimask = any(any(tmpmask, 1), 2);
                tmpphimask = tmpphimask(:);
                pmask(tmppinds, ii_b) = tmpphimask;
                spmask(tmpuinds, tmpvinds, ii_b) = any(tmpmask, 3);
                spmask_astra(:, :, ii_b) = any(tmpmask, 3);
                tmpphiint = sum(sum(tmpint, 1), 2);
                tmpphiint = tmpphiint(:);
                pintstack(tmppinds, ii_b) = tmpphiint;
                spstack(tmpuinds, tmpvinds, ii_b) = sum(tmpint, 3);
                spstack_astra(:, :, ii_b) = sum(tmpint, 3);
            end
        end

        function nof_bl = get_number_of_blobs(self)
            nof_bl = numel(self.grain.proj(self.det_index).bl);
        end

        function inc_inds = get_included_blob_indices(self)
            inc_inds = self.grain.proj(self.det_index).ondet(self.grain.proj(self.det_index).included);
        end
        
        function inds_out = bbox2inds(~, bbox_in, start_ind)
            inds_out = bbox_in - (start_ind(1) - 1);
            inds_out = inds_out(1) : inds_out(2);
        end
    end

    methods (Access = public, Static)
        function rot_cen = getRotCen1D(w_input, val_input)
            % if val_input = Acos(w_input)+Bsin(w_input)+rot_cen, rot_cen
            % can be calculated by LS estimator as
            % (C^T*C)^(-1)*C^T*val_input, where C = [cos(w_input) sin(w_input) 1]
            % inputs:
            % w_inputs  : 1D rotation angles array in degree
            % val_input : 1D values array in the same length as w_inputs
            w_input = w_input(:);
            val_input = val_input(:);
            sin_w = sind(w_input);
            cos_w = cosd(w_input);
            matched_filter = [val_input' * [sin_w, cos_w], sum(val_input)]';
            sum_cosxsin = sin_w' * cos_w;
            sum_cos = sum(cos_w);
            sum_sin = sum(sin_w);
            matTxmat = [...
                sin_w' * sin_w, sum_cosxsin, sum_sin; ...
                sum_cosxsin, cos_w' * cos_w, sum_cos; ...
                sum_sin, sum_cos, numel(w_input)];
            rot_cen = ([0, 0, 1] / matTxmat) * matched_filter;
        end

        function [fp, rot_coord] = fwdProjection2D(slice_in, rot_coord2D, rot_angles, data_length, rot_coord)
            % [fp, rot_coord] = GtGrainTTBlobBoundBoxesFiltering.fwdProjection2D(slice_in, rot_coord2D, rot_angles, data_length, rot_coord)
            % 2D forward projection of slice_in in size of [slice_size1, slice_size2, nof_slices]
            % inputs:
            % slice_in    : input slices in size of [nof_row, nof_column, nof_slices]
            % rot_coord2D : 2D position of rotation center in slice.
            %               Default is (2D_slice_size+1)/2.
            % rot_angles  : rotation angles in degree
            % data_length : length of forward projection of slice. Default
            %               is largest distance between slice pixels and rotation center.
            % rot_coord   : rotation center of forward projection.
            %               Default is (data_length+1)/2.
            %
            % outputs:
            % fp : forward projection in size of [data_length, nof_rotation_angles, nof_slice]
            size_slice = size(slice_in);
            if (~exist('rot_coord2D', 'var')) || isempty(rot_coord2D)
                rot_coord2D = (size_slice + 1) / 2;
            end
            if (~exist('data_length', 'var')) || isempty(data_length)
                data_length = ceil(...
                    2 * sqrt(...
                    max(abs([1, size_slice(1)] - rot_coord2D(1))) ^ 2 + ...
                    max(abs([1, size_slice(2)] - rot_coord2D(2))) ^ 2)) + 1;
            end
            if (~exist('rot_coord', 'var')) || isempty(rot_coord)
                rot_coord = (data_length + 1) / 2;
            end
            nof_pixels = size_slice(1) * size_slice(2);
            nof_slice = size(slice_in, 3);
            rot_angles = rot_angles(:)';
            coords = cat(3, ...
                (1:size_slice(1))' * ones(1, size_slice(2)), ...
                ones(size_slice(1), 1) * (1:size_slice(2)));
            coords = reshape(coords, [], 2);
            coords = bsxfun(@minus, coords, rot_coord2D);
            nof_angs = numel(rot_angles);
            triangfunc_angs = [sind(rot_angles); cosd(rot_angles)];
            fp = zeros(data_length, nof_angs, nof_slice);
            slice_vec = reshape(slice_in, nof_pixels, 1, nof_slice);
            slice_vec = [slice_vec; slice_vec];
            for ii_ang = 1:nof_angs
                inds = coords * triangfunc_angs(:, ii_ang) + rot_coord;
                inds_bound = bsxfun(@plus, floor(inds(:)), [0, 1]);
                coef_bound = 1 - abs(bsxfun(@minus, inds_bound, inds(:)));
                for ii_data = 1:data_length
                    tmpmask = (inds_bound(:) == ii_data);
                    if any(tmpmask, 1)
                        tmppixelints = bsxfun(@times, coef_bound(tmpmask), slice_vec(tmpmask, 1, :));
                        fp(ii_data, ii_ang, :) = sum(tmppixelints, 1);
                    end
                end
            end
        end
        
        function [bp, rot_coord2D] = bwdProjection2D(raw_data, rot_coord, rot_angles, slice_size, rot_coord2D, fill_val)
            % [bp, rot_coord2D] = GtGrainTTBlobBoundBoxesFiltering.bwdProjection2D(raw_data, rot_coord, rot_angles, slice_size, rot_coord2D, fill_val)
            % 2D backward projection of raw_data in size of [nof_1D_data, nof_rotation_angles, nof_slices]
            % inputs:
            % raw_data    : in size of [nof_1D_data, nof_rotation_angles, nof_slices]
            % rot_coord   : 1D position of rotation center in 1D data.
            %               Default is (nof_1D_data+1)/2.
            % rot_angles  : rotation angles in degree
            % slice_size  : 2D size of backward projection slice. Default is
            %               [1,1]*ceil(2*max(abs([1,nof_1D_data]-rot_coord)))
            % rot_coord2D : rotation center of 2D backward projection.
            %               Default is (slice_size+1)/2.
            % fill_val    : the value to fill the region of backward
            %               projection outside the data. Default is 0.
            %
            % outputs:
            % bp : backward projection in size of [slice_size, nof_slice]
            data_length = size(raw_data, 1);
            if (~exist('rot_coord', 'var')) || isempty(rot_coord)
                rot_coord = (data_length + 1) / 2;
            end
            nof_angs = size(raw_data, 2);
            if (~exist('rot_angles', 'var')) || isempty(rot_angles)
                rot_angles = (0:(nof_angs)) * (360 / nof_angs);
            end
            rot_angles = rot_angles(:)';
            if (~exist('slice_size', 'var')) || isempty(slice_size)
                slice_size = [1, 1] * ceil(2 * max(abs([1, data_length] - rot_coord)));
            end
            if (~exist('rot_coord2D', 'var')) || isempty(rot_coord2D)
                rot_coord2D = (slice_size + 1) / 2;
            end
            if (~exist('fill_val', 'var')) || isempty(fill_val)
                fill_val = 0;
            end
            rot_coord2D = rot_coord2D(:)';
            nof_slice = size(raw_data, 3);
            bp = zeros(slice_size(1), slice_size(2), nof_slice);
            coords = (1:slice_size(1))' * ones(1, slice_size(2));
            coords = cat(3, coords, ones(slice_size(1), 1) * (1:slice_size(2)));
            coords = reshape(coords, [], 2);
            coords = bsxfun(@minus, coords, rot_coord2D);
            triangfunc_angs = [sind(rot_angles); cosd(rot_angles)];
            nof_pixels = slice_size(1) * slice_size(2);
            tmp_boundspxval = zeros(2 * nof_pixels, nof_slice);
            inds_bound = zeros(nof_pixels, 2);
            for ii_ang = 1:nof_angs
                inds = coords * triangfunc_angs(:, ii_ang) + rot_coord;
                inds = inds(:);
                inds_bound(:, 1) = floor(inds);
                inds_bound(:, 2) = inds_bound(:, 1) + 1;
                coef_bound = 1 - abs(bsxfun(@minus, inds_bound, inds));
                ondet = ((inds_bound >= 1) & (inds_bound <= data_length));
                ondet = ondet(:);
                tmp_boundspxval(~ondet, :) = fill_val;
                tmp_boundspxval(ondet, :) = raw_data(inds_bound(ondet), ii_ang, :);
                tmp_bwdproj = reshape(tmp_boundspxval, nof_pixels, 2, nof_slice);
                tmp_bwdproj = bsxfun(@times, coef_bound, tmp_bwdproj);
                tmp_bwdproj = sum(tmp_bwdproj, 2);
                tmp_bwdproj = reshape(tmp_bwdproj, slice_size(1), slice_size(2), nof_slice);
                bp = tmp_bwdproj + bp;
            end
        end

        function c = correlate(z1, z2, varargin)
            % c = GtGrainTTBlobBoundBoxesFiltering.correlate(z1, z2, varargin)
            % correlate columns between matrices z1 and z2.
            % optional input:
            % 'renorm' : true - renormalize columns of inputs.
            % original version by Peter
            conf.renorm = false;
            conf = parse_pv_pairs(conf, varargin);
            if conf.renorm
                z1 = bsxfun(@rdivide, z1, sum(abs(z1), 1));
                z2 = bsxfun(@rdivide, z2, sum(abs(z2), 1));
            end
            cc = sum(fft(z1, [], 1) .* conj(fft(z2, [], 1)), 2);
            cc = abs(ifft(cc));
            [a, c] = max(cc);
            size1 = size(z1, 1);
            if c == 1
                inds_3 = [-1, 1, 2];
                corr_val_3 = [cc(size1), a, cc(c + 1)];
            elseif c == size1
                inds_3 = [size1 - 1, size1, size1 + 1];
                corr_val_3 = [cc(c - 1), a, cc(1)];
            else
                inds_3 = [c - 1, c, c + 1];
                corr_val_3 = [cc(c - 1), a, cc(c + 1)];
            end
            c = GtGrainTTBlobBoundBoxesFiltering.parabolicFit(inds_3, corr_val_3);
            c = c - 1;
            if (c > (size1 / 2))
                c = c - size1;
            end
        end

        function c = parabolicFit(var1, var2, var3)
            % c = GtGrainTTBlobBoundBoxesFiltering.parabolicFit(var1, var2, var3)
            % give 3 x values and 3 y values to calculate the extremum
            % point of fitted parabolic.
            % 3 ways of input:
            % 1. var1 = [x1,y1;x2,y2;x3,y3] or its transpose; no var2 or var3
            % 2. var1 = [x1,x2,x3], var2=[y1,y2,y3]; no var3.
            % 3. var1=[x1,y1],var2=[x2,y2],var3=[x3,y3]
            if nargin < 2
                if size(var1, 1) > 2
                    x_3x1 = var1(1, 1:3);
                    y_3x1 = var1(2, 1:3);
                else
                    x_3x1 = var1(1:3, 1)';
                    y_3x1 = var1(1:3, 2)';
                end
            elseif nargin < 3
                x_3x1 = var1(:);
                y_3x1 = var2(:);
            else
                x_3x1 = [var1(1), var2(1), var3(1)];
                y_3x1 = [var1(2), var2(2), var3(2)];
            end

            tmp_polynom_2x1 = [...
                (y_3x1(1) - y_3x1(2)) * (x_3x1(1) - x_3x1(3)); ...
                (y_3x1(3) - y_3x1(1)) * (x_3x1(1) - x_3x1(2))];

            c = [x_3x1(1) + x_3x1(3), x_3x1(1) + x_3x1(2)] * tmp_polynom_2x1;
            c = c / sum(tmp_polynom_2x1) / 2;
        end

        function [maskstack_filtered, hull_mask, hull_mask_rot] = filterMaskStackByRotation(maskstack, rot1Dpos, rot_angles, padding)
            % [maskstack_filtered, hull_mask, hull_mask_rot] = GtGrainTTBlobBoundBoxesFiltering.filterMaskStackByRotation(maskstack, rot1Dpos, rot_angles, padding)
            % If a stack of 1D masks in size of [mask_dim, rotation_dim,
            % slice_dim] has one single rotation center, the masks can be
            % refined by backward projection of its negation and forward
            % projection of negation of the negation (hull).
            % inputs:
            % maskstack  : in size of [nof_mask_elements, nof_rotation_angles, nof_slices]
            % rot1Dpos   : <double> position of rotation center in mask
            %              array. For example, if length of mask is N and
            %              rotation center at middle point, rot1Dpos = (N+1)/2.
            % rot_angles : array of rotation angles in degree.
            % padding    : integer. padding of hull for larger masks.
            %
            % outputs:
            % maskstack_filtered : refined mask stack in the same size as maskstack
            % hull_mask          : 2D hull created by backward projection.
            % hull_mask_rot      : 2D rotation center of the hull.
            if (nargin < 4) || isempty(padding)
                padding = 0;
            end
            [bp, hull_mask_rot] = GtGrainTTBlobBoundBoxesFiltering.bwdProjection2D(single(~maskstack), rot1Dpos, rot_angles, [], [], 1);
            mask_threshold = 0;
            hull_mask = ~(bp > mask_threshold);
            if padding > 0
                % dilate the hull for padding the mask.
                permute_vec = {[2, 1, 3], [2, 1, 3]};
                for ii_dim = 1:2
                    for ii_pad = 1:padding
                        hull_mask = convn(hull_mask, ones(3, 1), 'same') > 0.1;
                    end
                    hull_mask = permute(hull_mask, permute_vec{ii_dim});
                end
            end
            maskstack_filtered = GtGrainTTBlobBoundBoxesFiltering.fwdProjection2D(single(hull_mask), hull_mask_rot, rot_angles, size(maskstack, 1), rot1Dpos);
            maskstack_filtered = maskstack_filtered > 0;
        end
    end
end