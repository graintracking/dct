function w_tab = gtGrainAnglesTabularFix360deg(w_tab, ref_int_ws, parameters, det_index)
    if (exist('parameters', 'var') && ~isempty(parameters))
        if (~exist('det_index', 'var') || isempty(det_index))
            det_index = 1;
        end
        num_images = gtAcqTotNumberOfImages(parameters, det_index);

        % Let's treat those blobs at the w edge 360->0
        % (from the sampled orientations perspective)
        opposite_int_ws = mod(ref_int_ws + num_images / 2, num_images);
        gt_ref_int = opposite_int_ws > num_images / 2;
        lt_ref_int = ~gt_ref_int;
        opposite_int_ws_repmat = opposite_int_ws(:, ones(1, size(w_tab, 2)));

        w_tab(gt_ref_int, :) = w_tab(gt_ref_int, :) ...
            - num_images .* (w_tab(gt_ref_int, :) > opposite_int_ws_repmat(gt_ref_int, :));
        w_tab(lt_ref_int, :) = w_tab(lt_ref_int, :) ...
            + num_images .* (w_tab(lt_ref_int, :) < opposite_int_ws_repmat(lt_ref_int, :));
    else
        % Let's treat those angles at the edge 360->0
        % (from the sampled orientations perspective)
        opposite_angles = mod(ref_int_ws + 180, 360);
        gt_ref = opposite_angles > 180;
        lt_ref = ~gt_ref;
        opposite_angles_repmat = opposite_angles(:, ones(1, size(w_tab, 2)));

        w_tab(gt_ref, :) = w_tab(gt_ref, :) ...
            - 360 .* (w_tab(gt_ref, :) > opposite_angles_repmat(gt_ref, :));
        w_tab(lt_ref, :) = w_tab(lt_ref, :) ...
            + 360 .* (w_tab(lt_ref, :) < opposite_angles_repmat(lt_ref, :));
    end
end