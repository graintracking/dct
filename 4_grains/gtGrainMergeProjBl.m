function bl_merge = gtGrainMergeProjBl(bl_array, difspotID_array)
% Function bl_merge = gtGrainMergeProjBl(bl_array)
% To merge the proj.bl for the forward projection of 6D twin cluster
% recosntruction.
% --------------------------------------------------------------
% INPUT:   bl_array  <struct> An array of proj.bl to be merged
% OUTPUT:  bl_merge  <struct> Merged proj.bl
% 2021-07-01 by Zheheng

    % remove the invalid bl
    for i_bl = numel(bl_array):-1:1
        if ~isfield(bl_array(i_bl), 'intm') || isempty(bl_array(i_bl).intm)
            bl_array(i_bl) = [];
        end
    end
    % Initialize the merged bl
    bl_merge = bl_array(1);

    if numel(bl_array(:)) > 1
        % Merge bounds and sizes
        bound = [cat(3, bl_array(:).bbuim); ...
                 cat(3, bl_array(:).bbvim); ...
                 cat(3, bl_array(:).bbwim)];
        bound_merge = sfMergeBound(bound);
        bl_merge.bbuim = bound_merge(1, :);
        bl_merge.bbvim = bound_merge(2, :);
        bl_merge.bbwim = bound_merge(3, :);
        bl_merge.bbsize = sfBound2Size(bound_merge);
        bound_merge_mbb = sfMergeBound([cat(3, bl_array(:).mbbu); ...
                                        cat(3, bl_array(:).mbbv); ...
                                        cat(3, bl_array(:).mbbw)]);
        bl_merge.mbbu = bound_merge_mbb(1, :);
        bl_merge.mbbv = bound_merge_mbb(2, :);
        bl_merge.mbbw = bound_merge_mbb(3, :);
        bl_merge.mbbsize = sfBound2Size(bound_merge_mbb);

        % Remove the blobs that have the same difspotID with any previous
        % blob.
        if exist('difspotID_array','var') && numel(difspotID_array(:)) == numel(bl_array(:))
            [~, tmp_ind] = unique(difspotID_array(:));
            bl_array = bl_array(tmp_ind);

            % Merge intensity
            intensity_array = cat(1, bl_array(:).intensity);
            tmp_intensity = sum(intensity_array);
            intensity_array = intensity_array/tmp_intensity;

            % Merge mask and intm
            tmp_mask = false(bl_merge.bbsize);
            tmp_intm = single(zeros(bl_merge.bbsize));
            for i_bl = 1:numel(bl_array(:))
                tmp_vol_ind = sfVolInd(bound(:, :, i_bl), bound_merge, bl_merge.bbsize);
                tmp_mask(tmp_vol_ind) = tmp_mask(tmp_vol_ind) + bl_array(i_bl).mask(:);
                tmp_intm(tmp_vol_ind) = tmp_intm(tmp_vol_ind) + intensity_array(i_bl) * bl_array(i_bl).intm(:);
            end
        else
            % Merge mask, intm and intensity.
            intensity_array = cat(1, bl_array(:).intensity);
            tmp_mask = false(bl_merge.bbsize);
            tmp_intm = single(zeros(bl_merge.bbsize));
            for i_bl = 1:numel(bl_array(:))
                tmp_vol_ind = sfVolInd(bound(:, :, i_bl), bound_merge, bl_merge.bbsize);
                % Ignore the bl whose mask overlaps the ones of previous bls
                if any(bl_array(i_bl).mask(:) & tmp_mask(tmp_vol_ind))
                    intensity_array(i_bl) = 0;
                else
                    tmp_mask(tmp_vol_ind) = tmp_mask(tmp_vol_ind) + bl_array(i_bl).mask(:);
                    tmp_intm(tmp_vol_ind) = tmp_intm(tmp_vol_ind) + intensity_array(i_bl) * bl_array(i_bl).intm(:);
                end
            end
            tmp_intensity = sum(intensity_array(:));
            tmp_intm = tmp_intm/tmp_intensity;
        end

        bl_merge.intensity = tmp_intensity;
        bl_merge.mask = tmp_mask;
        bl_merge.intm = tmp_intm;
    end
end

function bound_merge = sfMergeBound(bound)
    bound_merge = [min(bound(:, 1, :), [], 3), ...
                   max(bound(:, 2, :), [], 3)];
end

function size = sfBound2Size(bound)
    size = (bound(:, 2) - bound(:, 1) + 1)';
end

function vol_ind = sfVolInd(bound_vol_embed, bound_vol_merge, size_merge)
    vol_ind = false(size_merge);
    corner1 = bound_vol_embed(:, 1) - bound_vol_merge(:, 1) + 1;
    corner2 = bound_vol_embed(:, 2) - bound_vol_merge(:, 1) + 1;
    vol_ind(corner1(1):corner2(1), ...
            corner1(2):corner2(2), ...
            corner1(3):corner2(3)) = true;
end