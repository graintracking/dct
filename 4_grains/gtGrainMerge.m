
function grn = gtGrainMerge(gr1, gr2, p, mode, flag_merge_bl)
    if (gr1.phaseid ~= gr2.phaseid)
        error('gtGrainMerge:wrong_argument', ...
            'Grains should be from the same phase')
    end

    if (~exist('mode', 'var') || isempty(mode))
        mode = 'partial';
    end
    if (~strcmpi(mode, 'partial'))
        error('gtGrainMerge:not_implemented', 'Mode: %s not implemented!', mode)
    end


    % flag to merge the grain.proj.bl
    if (~exist('flag_merge_bl', 'var') || isempty(flag_merge_bl))
        flag_merge_bl = false;
    end

    refgr_omind = gr1.allblobs.omind;
    if (any(refgr_omind ~= gr2.allblobs.omind))
        gr2 = gtGrainAllblobsFilterOrder(gr2, refgr_omind);
    end

    if (~exist('p', 'var') || isempty(p))
        p = gtLoadParameters();
    end

    gr_center = (gr1.center + gr2.center) / 2;
    gr_R_vector = (gr1.R_vector + gr2.R_vector) / 2;

    grn = gr1;
    grn.id = [gr1.id, gr2.id];
    grn.center = gr_center;
    grn.R_vector = gr_R_vector;
    grn = gtCalculateGrain(grn, p, 'ref_omind', refgr_omind);

    grn.proj = gtFwdSimProjDefinition([], numel(p.detgeo));

    have_fwd_sim = isfield(gr1, 'fwd_sim') && isfield(gr2, 'fwd_sim');
    if (have_fwd_sim)
        grn.fwd_sim = gtFwdSimDataStructureDefinition(0);
    end

    for ii_d = 1:numel(grn.proj)
        gr1_ondet_bool = false(size(refgr_omind));
        gr1_ondet_bool(gr1.proj(ii_d).ondet) = true;
        gr1_inc_bool = false(size(refgr_omind));
        gr1_inc_bool(gr1.proj(ii_d).ondet(gr1.proj(ii_d).included)) = true;
        gr1_sel_bool = false(size(refgr_omind));
        gr1_sel_bool(gr1.proj(ii_d).ondet(gr1.proj(ii_d).included(gr1.proj(ii_d).selected))) = true;

        gr2_ondet_bool = false(size(refgr_omind));
        gr2_ondet_bool(gr2.proj(ii_d).ondet) = true;
        gr2_inc_bool = false(size(refgr_omind));
        gr2_inc_bool(gr2.proj(ii_d).ondet(gr2.proj(ii_d).included)) = true;
        gr2_sel_bool = false(size(refgr_omind));
        gr2_sel_bool(gr2.proj(ii_d).ondet(gr2.proj(ii_d).included(gr2.proj(ii_d).selected))) = true;

        grn_ondet_bool = gr1_ondet_bool & gr2_ondet_bool;
        grn_inc_bool = grn_ondet_bool & (gr1_inc_bool | gr2_inc_bool);
        grn_sel_bool = grn_ondet_bool & (gr1_sel_bool | gr2_sel_bool);

        grn.proj(ii_d).ondet = find(grn_ondet_bool);
        grn.proj(ii_d).included = find(grn_inc_bool(grn.proj(ii_d).ondet));
        grn.proj(ii_d).selected = grn_sel_bool(grn.proj(ii_d).ondet(grn.proj(ii_d).included));

        % merge grain.proj.bl
        if flag_merge_bl
            grn.proj(ii_d).bl = gtFwdSimBlobDefinition('blob', numel(grn.proj(ii_d).included));
            for ii_merge_bl = 1:numel(grn.proj(ii_d).included)
                % find the indices
                merge_blob_ind_on_blob_list = grn.proj(ii_d).ondet(grn.proj(ii_d).included(ii_merge_bl));
                merge_blob_ind_on_gr1_bl_list = find(gr1.proj(ii_d).ondet(gr1.proj(ii_d).included) == merge_blob_ind_on_blob_list);
                merge_blob_ind_on_gr2_bl_list = find(gr2.proj(ii_d).ondet(gr2.proj(ii_d).included) == merge_blob_ind_on_blob_list);
                % merge the bl
                gr1_bl_to_merge = gr1.proj(ii_d).bl(merge_blob_ind_on_gr1_bl_list);
                gr2_bl_to_merge = gr2.proj(ii_d).bl(merge_blob_ind_on_gr2_bl_list);
                if ~isempty(gr1_bl_to_merge) && ~isempty(gr2_bl_to_merge)
                    tmp_bl_merge = gtGrainMergeProjBl([gr1_bl_to_merge, gr2_bl_to_merge]);
                elseif ~isempty(gr1_bl_to_merge)
                    tmp_bl_merge = gr1_bl_to_merge;
                elseif ~isempty(gr2_bl_to_merge)
                    tmp_bl_merge = gr2_bl_to_merge;
                else
                    error('gtGrainMerge:missing_blob', ...
                        'Both gr1.proj(%d).bl(%d) and gr2.proj(%d).bl(%d) are empty', ii_d, merge_blob_ind_on_gr1_bl_list, ii_d, merge_blob_ind_on_gr2_bl_list)
                end
                grn.proj(ii_d).bl(ii_merge_bl) = tmp_bl_merge;
            end
        end

        if (have_fwd_sim)
            spot_ids_1_bool = zeros(size(refgr_omind));
            spot_ids_1_bool(gr1.proj(ii_d).ondet) = gr1.fwd_sim(ii_d).spotid;
            spot_ids_1 = spot_ids_1_bool(grn_ondet_bool);

            spot_ids_2_bool = zeros(size(refgr_omind));
            spot_ids_2_bool(gr2.proj(ii_d).ondet) = gr2.fwd_sim(ii_d).spotid;
            spot_ids_2 = spot_ids_2_bool(grn_ondet_bool);

            spot_ids = zeros(size(grn.proj(ii_d).ondet));

            equal_ids = spot_ids_1 == spot_ids_2;
            spot_ids(equal_ids) = spot_ids_1(equal_ids);

            conflict_ids = spot_ids_1 ~= spot_ids_2 & spot_ids_1 & spot_ids_2;
            spot_ids(conflict_ids) = -1;

            only_gr1_ids = spot_ids_1 ~= spot_ids_2 & spot_ids_1 & ~spot_ids_2;
            spot_ids(only_gr1_ids) = spot_ids_1(only_gr1_ids);

            only_gr2_ids = spot_ids_1 ~= spot_ids_2 & ~spot_ids_1 & spot_ids_2;
            spot_ids(only_gr2_ids) = spot_ids_2(only_gr2_ids);

            grn.fwd_sim(ii_d).spotid = spot_ids;
        end
    end
end
