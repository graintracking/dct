function gr = gtGrainAllblobsFilterOrder(gr, refgr_omind, det_ind)
% FUNCTION gr = gtGrainAllblobsFilterOrder(gr, refgr_omind, det_ind)

    if (~exist('det_ind', 'var') || isempty(det_ind))
        det_ind = 1:numel(gr.allblobs);
    end

    for ii_d = det_ind
        gr_omind = gr.allblobs(ii_d).omind;
        gr_omind_ind = [ ...
            find(gr_omind == 1)'; find(gr_omind == 2)'; ...
            find(gr_omind == 3)'; find(gr_omind == 4)' ];
        gr_omind_ind = reshape(gr_omind_ind, [], 1);
        refgr_omind_ind = [ ...
            find(refgr_omind == 1)'; find(refgr_omind == 2)'; ...
            find(refgr_omind == 3)'; find(refgr_omind == 4)' ];
        refgr_omind_ind = reshape(refgr_omind_ind, [], 1);

        fnames = fieldnames(gr.allblobs(ii_d));
        for ii_f = 1:numel(fnames)
            fname = fnames{ii_f};
            switch(fname)
                case {'srot', 'rot_s2l', 'rot_l2s'}
                    srot = gr.allblobs(ii_d).(fname);
                    srot(:, :, refgr_omind_ind) = srot(:, :, gr_omind_ind);
                    gr.allblobs(ii_d).(fname) = srot;
                case 'detector'
                    subfnames = fieldnames(gr.allblobs(ii_d).detector);
                    for ii_sf = 1:numel(subfnames)
                        subfname = subfnames{ii_sf};
                        prop = gr.allblobs(ii_d).detector.(subfname);
                        prop(refgr_omind_ind, :) = prop(gr_omind_ind, :);
                        gr.allblobs(ii_d).detector.(subfname) = prop;
                    end
                case 'type'
                    % Do nothing
                otherwise
                    prop = gr.allblobs(ii_d).(fname);
                    prop(refgr_omind_ind, :) = prop(gr_omind_ind, :);
                    gr.allblobs(ii_d).(fname) = prop;
            end
        end

        has_fwd_sim = isfield(gr, 'fwd_sim');

        % Taking care of proj/fwd_sim field
        if (isfield(gr, 'proj'))
            ondet_logical = false(numel(gr_omind), 1);
            ondet_logical(gr.proj(ii_d).ondet) = true;
            included_logical = false(numel(gr_omind), 1);
            included_logical(gr.proj(ii_d).ondet(gr.proj(ii_d).included)) = true;
            selected_logical = false(numel(gr_omind), 1);
            selected_logical(gr.proj(ii_d).ondet(gr.proj(ii_d).included(gr.proj(ii_d).selected))) = true;

            ondet_pos = zeros(numel(gr_omind), 1);
            ondet_pos(ondet_logical) = 1:numel(gr.proj(ii_d).ondet);

            included_pos = zeros(numel(gr_omind), 1);
            included_pos(included_logical) = 1:numel(gr.proj(ii_d).included);

            ondet_logical(refgr_omind_ind) = ondet_logical(gr_omind_ind);
            included_logical(refgr_omind_ind) = included_logical(gr_omind_ind);
            selected_logical(refgr_omind_ind) = selected_logical(gr_omind_ind);

            ondet_pos(refgr_omind_ind) = ondet_pos(gr_omind_ind);
            included_pos(refgr_omind_ind) = included_pos(gr_omind_ind);

            gr.proj(ii_d).ondet = find(ondet_logical);
            gr.proj(ii_d).included = find(included_logical(gr.proj(ii_d).ondet));
            gr.proj(ii_d).selected = selected_logical(gr.proj(ii_d).ondet(gr.proj(ii_d).included));

            ondet_pos = ondet_pos(gr.proj(ii_d).ondet);
            included_pos = included_pos(gr.proj(ii_d).ondet(gr.proj(ii_d).included));

            gr.proj(ii_d).bl = gr.proj(ii_d).bl(included_pos);
            gr.proj(ii_d).stack = gr.proj(ii_d).stack(:, included_pos, :);

            if (has_fwd_sim)
                gr.fwd_sim(ii_d).flag = gr.fwd_sim(ii_d).flag(ondet_pos);
                gr.fwd_sim(ii_d).spotid = gr.fwd_sim(ii_d).spotid(ondet_pos);
                gr.fwd_sim(ii_d).check = gr.fwd_sim(ii_d).check(ondet_pos);

                gr.fwd_sim(ii_d).bb = gr.fwd_sim(ii_d).bb(included_pos);
                gr.fwd_sim(ii_d).cent = gr.fwd_sim(ii_d).cent(included_pos);
                gr.fwd_sim(ii_d).intensity = gr.fwd_sim(ii_d).intensity(included_pos);
                gr.fwd_sim(ii_d).avg_pixel_int = gr.fwd_sim(ii_d).avg_pixel_int(included_pos);
                gr.fwd_sim(ii_d).likelihood = gr.fwd_sim(ii_d).likelihood(included_pos);
                gr.fwd_sim(ii_d).cands = gr.fwd_sim(ii_d).cands(included_pos);
                gr.fwd_sim(ii_d).uvw = gr.fwd_sim(ii_d).uvw(included_pos);
                gr.fwd_sim(ii_d).spot_type = gr.fwd_sim(ii_d).spot_type(included_pos);
                gr.fwd_sim(ii_d).check_spots = gr.fwd_sim(ii_d).check_spots(included_pos);

                gr.fwd_sim(ii_d).difspotID = gr.fwd_sim(ii_d).difspotID(included_pos);
            end
        end
    end
end
