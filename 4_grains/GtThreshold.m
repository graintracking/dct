classdef GtThreshold < handle
    properties
        conf;

        % Reconstruction parameters from parameters.mat
        param;
    end

    properties (Constant)
        backspace = '\b';
    end

    methods
        function obj = GtThreshold(p, varargin)
            obj.conf = [];
            obj.conf.verbose = true;
            obj.conf.grain_ids = [];
            if (~exist('p', 'var') || isempty(p))
                p = gtLoadParameters();
            end
            obj.param = p;

            par_thr = obj.getThrParams();
            [par_thr, rej_params] = parse_pv_pairs(par_thr, varargin);
            if (~isfield(par_thr, 'do_morph_recon'))
                par_thr.do_morph_recon = false;
            end
            obj.param.rec.thresholding = par_thr;

            obj.conf = parse_pv_pairs(obj.conf, rej_params);
        end

        function [defaults, sample] = generateDefaults(obj, fileTable)
            samplefile  = fullfile(obj.param.acq(1).dir, '4_grains', 'sample.mat');
            if (exist('fileTable', 'var'))
                sample = GtSample.loadFromLockedFile(fileTable, samplefile);
            else
                sample = GtSample.loadFromFile(samplefile);
            end

            defaults = [];
            defaults.percentile = obj.param.rec.thresholding.percentile;

            defaults.phases = {};
            for phaseID = 1:length(sample.phases)
                defaults.phases{phaseID}.min_id = 1;
                defaults.phases{phaseID}.max_id = sample.phases{phaseID}.getNumberOfGrains();
            end
        end

        function [varargout] = batchThreshold(obj, phaseID, firstID, lastID, percentile)
        % GTTHRESHOLD/BATCHTHRESHOLD
        % [varargout] = batchThreshold(obj, phaseID, firstID, lastID, percentile)
            global GT_DB
            gtDBConnect(GT_DB.host, GT_DB.user, GT_DB.password, GT_DB.name);
            filetable = [obj.param.acq(1).name '_filetable'];

            [defaults, sample] = obj.generateDefaults(filetable);

            if (exist('percentile', 'var'))
                obj.param.rec.thresholding.percentile = percentile;
                gtSaveParameters(obj.param);
            end

            if ischar(phaseID)
                phaseID = str2double(phaseID);
            end

            % Here is for the lazy who wants to launch from CMD line OR for OAR.
            if (ischar(firstID))
                if (strcmpi(firstID, 'first'))
                    firstID = defaults.phases{phaseID}.min_id;
                else
                    firstID = str2double(firstID);
                end
            end
            if (ischar(lastID))
                if (strcmpi(lastID, 'last'))
                    lastID = defaults.phases{phaseID}.max_id;
                else
                    lastID = str2double(lastID);
                end
            end

            if ((firstID < defaults.phases{phaseID}.min_id) ...
                    || (firstID > lastID) ...
                    || (lastID > defaults.phases{phaseID}.max_id))
                error('SEGMENT:wrong_argument', ...
                      ['The range indexes are not correct: (%d, %d).\n' ...
                      'Largest bounds (%d, %d)'], ...
                      firstID, lastID, defaults.phases{phaseID}.min_id, ...
                      defaults.phases{phaseID}.max_id);
            end
            if ~isempty(obj.conf.grain_ids)
                grain_ids = obj.conf.grain_ids
            else
                grain_ids = firstID : lastID;
            end
            gauge = GtGauge([firstID lastID], ...
                sprintf('Phase %02d: ', phaseID), ...
                'verbose', obj.conf.verbose);

            for ii = 1 : numel(grain_ids)
                gauge.incrementAndDisplay();
                grainID = grain_ids(ii);
                try
                    grain_rec = obj.loadGrain(phaseID, grainID, sample);

                    grain_seg = obj.thresholdAutoSingleGrain(grain_rec);

                    % Check size and update metadata
                    if (isempty(grain_seg.seg))
                        fprintf('\n\nWarning! Empty volume in grain %d.\n\n',grainID);
                        gauge.rePrint();
                        grain_seg.seg = ones(1, 1, 1, 'uint8');
                        grain_seg.segbb = [round(size(grain_rec.vol)/2), 1, 1, 1];
                    end
                    sample.phases{phaseID}.setBoundingBox(grainID, grain_seg.segbb);

                    % Write to file
                    obj.saveGrain(phaseID, grainID, grain_seg, sample);
                catch mexc
                    gtPrintException(mexc, ...
                        'Segmenting grain %d in phase %d failed', grainID, phaseID);
                    gauge.rePrint();
                end

            end
            gauge.delete();

            disp('Now writing to sample.mat')
            samplefile = fullfile(obj.param.acq(1).dir, '4_grains', 'sample.mat');
            sample.mergeToFile(filetable, samplefile, 'sample');

            if (nargout >= 1)
                varargout{1} = grain_seg.seg;
            end
        end

        function grain_seg = singleGrainThresholdInFile(obj, phaseID, grainID, threshold, seed, border_mask)
        % GTTHRESHOLD/singleGrainThresholdInFile
        % grain = singleGrainThresholdInFile(obj, phaseID, grainID, threshold, center)

            samplefile  = fullfile(obj.param.acq(1).dir, '4_grains', 'sample.mat');
            sample = GtSample.loadFromFile(samplefile);

            fprintf('Segmenting Grain %d, from phase %d.. ', grainID, phaseID);
            grain_rec = obj.loadGrain(phaseID, grainID, sample);

            if (~exist('border_mask', 'var'))
                border_mask = [];
            end

            if (exist('seed', 'var') && ~isempty(seed))
                do_morpho = true;
            else
                seed = [];
                do_morpho = false;
                warning('THRESHOLD:less_capability', ...
                        'Morphological reconstruction is disabled')
            end

            if (obj.checkIsIterative())
                % iterative search
                grain_seg = obj.thresholdAutoSingleGrainIter(grain_rec, threshold, seed);
            else
                % normal way
                grain_seg = obj.segmentAndDoMorph(grain_rec, threshold, do_morpho, seed, border_mask);
            end

            if isempty(grain_seg.seg)
                fprintf('\n\nWarning! Empty volume in grain %d.\n\n', grainID);
                grain_seg.seg = ones(1, 1, 1, 'uint8');
                grain_seg.segbb = round([(size(grain_rec.vol) -1), (size(grain_rec.vol) +1)] /2);
            end

            obj.saveGrain(phaseID, grainID, grain_seg, sample);
            fprintf('Done.\n');
        end

        function grain_seg = singleGrainAutoThreshold(obj, phaseID, grainID)
        % GTTHRESHOLD/singleGrainAutoThreshold
        % grain = singleGrainAutoThreshold(obj, phaseID, grainID)

            samplefile  = fullfile(obj.param.acq(1).dir, '4_grains', 'sample.mat');
            sample = GtSample.loadFromFile(samplefile);

            fprintf('Segmenting Grain %d, from phase %d..', grainID, phaseID);
            grain_rec = obj.loadGrain(phaseID, grainID, sample);

            grain_seg = obj.thresholdAutoSingleGrain(grain_rec);

            if isempty(grain_seg.seg)
                fprintf('\n\nWarning! Empty volume in grain %d.\n\n',grainID);
                grain_seg.seg = ones(1, 1, 1, 'uint8');
                grain_seg.segbb = round([(size(grain_rec.vol) -1), (size(grain_rec.vol) +1)] /2);
            end
            sample.phases{phaseID}.setBoundingBox(grainID, grain_seg.segbb);

            obj.saveGrain(phaseID, grainID, grain_seg, sample);
            fprintf('\b\b: Done.\n');

            fprintf('Writing to sample.mat..')
            sample.saveToFile(samplefile);
            fprintf('\b\b: Done.\n');
        end

        function cl_seg = singleClusterAutoThreshold(obj, phaseID, grainIDs, thresholds)
        % GTTHRESHOLD/singleClusterAutoThreshold
        % cl_seg = singleClusterAutoThreshold(obj, phaseID, grainID, thresholds)

            samplefile  = fullfile(obj.param.acq(1).dir, '4_grains', 'sample.mat');
            sample = GtSample.loadFromFile(samplefile);

            if (~exist('thresholds', 'var'))
                thresholds = [];
            end

            % Determining the cluster type
            cl_info = sample.phases{phaseID}.getClusterInfo(grainIDs);
            if (isempty(cl_info))
                error('GtThreshold:wrong_argument', ...
                    'Grain IDs: "%s" don''t correspond to any known cluster', ...
                    sprintf(' %d', grainIDs))
            end

            str_ids = sprintf(' %04d', grainIDs);
            fprintf('Segmenting Cluster (%s), from phase %d..', str_ids, phaseID);
            [cl_rec, cl_sub_recs] = obj.loadCluster(phaseID, grainIDs, cl_info.cluster_type);

            switch (cl_info.cluster_type)
                case 0
                    if (~isempty(thresholds))
                        cl_seg = obj.segmentAndDoMorph(cl_rec, thresholds(1), false, []);
                    else
                        cl_seg = obj.thresholdAutoSingleGrain(cl_rec);
                    end
                case 1
                    if (~isempty(thresholds))
                        cl_seg = obj.segmentAndDoMorph(cl_rec, thresholds(1), false, []);
                    else
                        cl_seg = obj.thresholdAutoSingleGrainSimpleSearch(cl_rec, 0.1); %0.04
                    end

                    for ii_g = 2:numel(cl_sub_recs)
                        if (~isempty(thresholds))
                            tmp_seg = obj.segmentAndDoMorph(cl_sub_recs(ii_g), thresholds(ii_g), false, []);
                        else
                            tmp_seg = obj.thresholdAutoSingleGrainSimpleSearch(cl_sub_recs(ii_g), 0.02);
                        end

                        tmp_seg.seg = gtPlaceSubVolume( ...
                            zeros(cl_seg(1).segbb(4:6), 'like', uint8(cl_seg(1).seg)), ...
                            tmp_seg.seg, ...
                            tmp_seg.segbb(1:3) - cl_seg(1).segbb(1:3) );
                        tmp_seg.segbb = cl_seg(1).segbb;

                        tmp_seg.seg = cl_seg(1).seg & tmp_seg.seg;
                        cl_seg(1).seg = cl_seg(1).seg & ~tmp_seg.seg;

                        cl_seg(ii_g) = tmp_seg;
                    end
                case 2
                    error('GtThreshold:not_implemented', ...
                        'Multi-grain thresholding not implemented yet!')
            end

            if isempty(cl_seg(1).seg)
                fprintf('\n\nWarning! Empty volume in cluster (%s).\n\n', str_ids);
                cl_seg(1).seg = ones(1, 1, 1, 'uint8');
                cl_seg(1).segbb = round([(size(cl_rec.vol) -1), (size(cl_rec.vol) +1)] /2);
            end

            obj.saveCluster(phaseID, grainIDs, cl_seg, cl_info.cluster_type);
            fprintf('\b\b: Done.\n');
        end

        function cl_seg = singleGrainInClusterAutoThreshold(obj, phaseID, clusterID, subvol, subvolID, threshold, seeds )
        % GTTHRESHOLD/singleGrainInClusterAutoThreshold
        % grain = singleGrainAutoThreshold(obj, phaseID, gra grainID)
            if ~exist('seeds', 'var')
                seeds = [];
            end

            samplefile  = fullfile(obj.param.acq(1).dir, '4_grains', 'sample.mat');
            sample = GtSample.loadFromFile(samplefile);

            grainIDs = sample.phases{phaseID}.clusters(clusterID).included_ids;
            str_ids = sprintf(' %04d', grainIDs);
            fprintf('Segmenting Cluster (%s), from phase %d..', str_ids, phaseID);
            % [cl_rec, cl_sub_recs] = obj.loadCluster(phaseID, grainIDs, cl_info.cluster_type);
            if isempty(seeds)
                tmp_seg = obj.segmentAndDoMorph(sub_vol, threshold, false);
            else
                tmp_seg = obj.segmentAndDoMorph(sub_vol, threshold, true, seeds);
            end

            tmp_seg.seg = gtPlaceSubVolume( ...
                zeros(cl_seg(1).segbb(4:6), 'like', uint8(cl_seg(1).seg)), ...
                tmp_seg.seg, ...
                tmp_seg.segbb(1:3) - cl_seg(1).segbb(1:3) );
            tmp_seg.segbb = cl_seg(1).segbb;

            tmp_seg.seg = cl_seg(1).seg & tmp_seg.seg;
            cl_seg(1).seg = cl_seg(1).seg & ~tmp_seg.seg;

            cl_seg(ii_g) = tmp_seg;

            if isempty(cl_seg(1).seg)
                fprintf('\n\nWarning! Empty volume in cluster (%s).\n\n', str_ids);
                cl_seg(1).seg = ones(1, 1, 1, 'uint8');
                cl_seg(1).segbb = round([(size(cl_rec.vol) -1), (size(cl_rec.vol) +1)] /2);
            end

            obj.saveCluster(phaseID, grainIDs, cl_seg, cl_info.cluster_type);
            fprintf('\b\b: Done.\n');
        end

        function seg_struct = volumeThreshold(obj, volume, threshold, seed, do_region_prop, border_mask)
        % GTTHRESHOLD/volumeThreshold
        % segvol = volumeThreshold(obj, volume, threshold, center)

            if (~exist('do_region_prop', 'var'))
                do_region_prop = true;
            end

            if (~exist('border_mask', 'var'))
                border_mask = [];
            end

            if (isempty(volume))
                gtError('GtThreshold:wrong_argument', ...
                    'Volume to segment is empty!')
            end

            if (exist('seed', 'var') && ~isempty(seed) && all(seed > 0))
                do_morpho = true;
            else
                seed = [];
                do_morpho = false;
                warning('THRESHOLD:less_capability', ...
                        'Morphological reconstruction is disabled')
            end

            seg_struct = struct('vol', volume, 'shift', [0 0 0]);
            seg_struct = obj.segmentAndDoMorph(seg_struct, threshold, do_morpho, seed, do_region_prop, border_mask);
        end

        function grain_seg = thresholdAutoSingleGrain(obj, grain_rec)
            if (obj.checkIsIterative())
                % iterative search
                grain_seg = obj.thresholdAutoSingleGrainIter(grain_rec);
            else
                % normal way
                grain_seg = obj.thresholdAutoSingleGrainSimple(grain_rec);
            end
        end
    end

    methods (Access = protected)
        function grain_out = loadGrain(obj, phaseID, grainID, sample)
        % GTTHRESHOLD/LOADGRAIN
        % grain = loadGrain(obj, phaseID, grainID)
            is_6D = isfield(obj.param.rec, 'grains') ...
                && strcmpi(obj.param.rec.grains.algorithm(1:2), '6D');
            is_extended = sample.phases{phaseID}.getUseExtended(grainID);
            grain_out = gtLoadGrain(phaseID, grainID, 'is_extended', is_6D && is_extended, 'fields', {'id'});
            grain_rec = gtLoadGrainRec(phaseID, grainID, ...
                'is_extended', is_6D && is_extended);

            if (is_6D)
                grain_out.vol = grain_rec.ODF6D.intensity;
                grain_out.shift = grain_rec.ODF6D.shift;
            else
                grain_out.vol = grain_rec.VOL3D.intensity;
                grain_out.shift = grain_rec.VOL3D.shift;
            end
        end

        function saveGrain(obj, phaseID, grainID, grain_seg, sample)
        % GTTHRESHOLD/SAVEGRAIN
        % saveGrain(obj, phaseID, grainID, grain)

            is_6D = isfield(obj.param.rec, 'grains') ...
                && strcmpi(obj.param.rec.grains.algorithm(1:2), '6D');
            is_extended = sample.phases{phaseID}.getUseExtended(grainID);

            grain_rec = struct('SEG', grain_seg);
            gtSaveGrainRec(phaseID, grainID, grain_rec, 'fields', {'SEG'}, ...
                'is_extended', is_6D && is_extended);
        end

        function [cl_rec, cl_sub_recs] = loadCluster(~, phase_id, grain_ids, cluster_type)
            cluster_rec = gtLoadClusterRec(phase_id, grain_ids);
            switch (cluster_type)
                case 0
                    cl_rec = struct( ...
                        'id', grain_ids(1), ...
                        'vol', cluster_rec.ODF6D.intensity, ...
                        'shift', cluster_rec.ODF6D.shift );
                    cl_sub_recs = [];
                case 1
                    for ii_v = numel(cluster_rec.ODF6D):-1:1
                        cl_sub_recs(ii_v) = struct( ...
                            'id', grain_ids(ii_v), ...
                            'vol', cluster_rec.ODF6D(ii_v).intensity, ...
                            'shift', cluster_rec.ODF6D(ii_v).shift );
                    end
                    cl_rec = struct( ...
                        'id', grain_ids(1), ...
                        'vol', gtMathsSumCellVolumes({cl_sub_recs(:).vol}), ...
                        'shift', cl_sub_recs(1).shift );
                case 2
                    error('GtThreshold:not_implemented', ...
                        'Multi-grain thresholding not implemented yet!')
            end
        end

        function saveCluster(~, phase_id, grain_ids, cluster_seg, cluster_type)
            cluster_rec = struct('SEG', cluster_seg);
            gtSaveClusterRec(phase_id, grain_ids, cluster_rec, 'fields', {'SEG'});
        end

        function is_iterative = checkIsIterative(obj)
            thr_params = obj.getThrParams();
            is_iterative = thr_params.num_iter > 0;
        end

        function thr_params = getThrParams(obj)
        % GTTHRESHOLD/getThrParams
        %   FUNCTION that deals with parameters versions

            if (isfield(obj.param.rec, 'thresholding'))
                thr_params = obj.param.rec.thresholding;

                def_rec = gtRecDefaultParameters();
                def_thr = def_rec.thresholding;
                local_thr(1, :) = fieldnames(thr_params);
                local_thr(2, :) = struct2cell(thr_params);
                [thr_params, ~] = parse_pv_pairs(def_thr, local_thr(:));
            else
                warning('GtThreshold:old_params', ...
                    'Old "rec" parameters. You should produce a new version.')

                def_rec = gtRecDefaultParameters();
                thr_params = def_rec.thresholding;
                thr_params = parse_pv_pairs(thr_params, ...
                    {'percentile', obj.param.rec.percentile2, ...
                    'do_morph_recon', obj.param.rec.do_morph_recon});
            end

            if (~isfield(thr_params, 'do_extended'))
                thr_params.do_extended = false;
            end
        end

        function grain_seg = thresholdAutoSingleGrainSimple(obj, grain_rec)
        % GTTHRESHOLD/THRESHOLDAUTOSINGLEGRAIN Segment 3D grain volume
        %
        % function grain = thresh.thresholdAutoSingleGrainSimple(grain, rec)
        %
        % INPUTS:  grain.vol          graylevel reconstruction of grain volume
        %          rec                contents of parameters.rec structure:
        %             .percentile    adaptive threshold see below
        %             .do_morph_recon remove not connected areas by morphological
        %                             reconstruction (<logical> {true})
        % OUTPUTS:
        %          grain.thresh Threshold value
        %          grain.seg    Segmented grain volume
        %          grain.Area   Area of grain
        %          grain.segbb  BoundingBox (gt convention) for insertion of seg
        %                       volume into sample volume
        %          grain.seed   Seed for morphological reconstruction
        %
        % explanation - we expect a histogram with a peak corresponding to the
        % grain, and a peak a low values corresponding to the background of zeros.
        % We want a threshold value below the peak corresponding to the grain, but
        % above the background peak.

            par_thr = obj.getThrParams();

            % This removes the borders in the computation of the percentile
            % Useful for extended grains
            vol = gtVolumeMaskBorder(grain_rec.vol, 0, par_thr.mask_border_voxels);

            % calulate intensity in the central region of the grain
            centerx = round(size(vol, 1) * 0.25) : round(size(vol, 1) * 0.75);
            centery = round(size(vol, 2) * 0.25) : round(size(vol, 2) * 0.75);
            centerz = round(size(vol, 3) * 0.25) : round(size(vol, 3) * 0.75);

            background = gtImgMeanValue(vol(centerx, centery, centerz)) / 10;
            % Small performance boost
            indexes = vol > background;
            % define threshold as percentile/100 x standard dev below the mean,
            % excluding low (10 percentile background) values
            stdvol  = std( vol(indexes) );
            meanvol = mean( vol(indexes) );
            maxvol  = max( vol(indexes) );

            threshold_val = meanvol - par_thr.percentile/100 * stdvol;

            seed = round([size(vol, 1), size(vol, 2), size(vol, 3)] / 2);
            grain_seg = obj.segmentAndDoMorph(grain_rec, threshold_val, ...
                par_thr.do_morph_recon, seed);
            % sometimes the seed point is outside the grain volume - so try
            % to seed with high intensity areas
            if (isempty(grain_seg.seg))
                seed = vol > maxvol * 0.7;
                grain_seg = obj.segmentAndDoMorph(grain_rec, threshold_val, ...
                    par_thr.do_morph_recon, seed);
            end
        end

        function grain_seg = thresholdAutoSingleGrainSimpleSearch(obj, grain_rec, peak_region_fact)
        % GTTHRESHOLD/thresholdAutoSingleGrainSimpleSearch Segment 3D grain volume
        %
        % function grain = thresh.thresholdAutoSingleGrainSimpleSearch(grain_rec, peak_region_fact)
        %
        % INPUTS:  grain.vol          graylevel reconstruction of grain volume
        %          rec                contents of parameters.rec structure:
        %             .percentile    adaptive threshold see below
        %             .do_morph_recon remove not connected areas by morphological
        %                             reconstruction (<logical> {true})
        % OUTPUTS:
        %          grain.thresh Threshold value
        %          grain.seg    Segmented grain volume
        %          grain.Area   Area of grain
        %          grain.segbb  BoundingBox (gt convention) for insertion of seg
        %                       volume into sample volume
        %          grain.seed   Seed for morphological reconstruction
        %
        % explanation - we expect a histogram with a peak corresponding to the
        % grain, and a peak a low values corresponding to the background of zeros.
        % We want a threshold value below the peak corresponding to the grain, but
        % above the background peak.

            vol = grain_rec.vol;

            par_thr = obj.getThrParams();

            % calulate intensity in the central region of the grain
            centerx = round(size(vol, 1) * 0.25) : round(size(vol, 1) * 0.75);
            centery = round(size(vol, 2) * 0.25) : round(size(vol, 2) * 0.75);
            centerz = round(size(vol, 3) * 0.25) : round(size(vol, 3) * 0.75);

            central_vol = vol(centerx, centery, centerz);
            [~, pos] = max(central_vol(:));
            [ix, iy, iz] = ind2sub(size(central_vol), pos);
            seed = [centerx(1) + ix, centery(1) + iy, centerz(1) + iz] - 1;

            % calulate intensity in the central region of the grain
            centerx = round(seed(1) - size(vol, 1) * peak_region_fact) : round(seed(1) + size(vol, 1) * peak_region_fact);
            centery = round(seed(2) - size(vol, 2) * peak_region_fact) : round(seed(2) + size(vol, 2) * peak_region_fact);
            centerz = round(seed(3) - size(vol, 3) * peak_region_fact) : round(seed(3) + size(vol, 3) * peak_region_fact);

            threshold_val = gtImgMeanValue(vol(centerx, centery, centerz)) / par_thr.percent_of_peak

            grain_seg = obj.segmentAndDoMorph(grain_rec, threshold_val, ...
                par_thr.do_morph_recon, seed);
        end

        function grain_seg = thresholdAutoSingleGrainIter(obj, grain_rec, threshold_val, seed)
        % GTTHRESHOLD/THRESHOLDAUTOSINGLEGRAINITER Segment 3D grain volume
        % iteratively
        %
        % grain = thresh.thresholdAutoSingleGrainIter(grain, threshold_val, center)
        %
        % INPUTS:  grain.vol          graylevel reconstruction of grain volume
        %
        % OUTPUTS:
        %          grain.thresh Threshold value
        %          grain.seg    Segmented grain volume
        %          grain.Area   Area of grain
        %          grain.segbb  BoundingBox (gt convention) for insertion of seg
        %                       volume into sample volume
        %          grain.seed   Seed for morphological reconstruction
        %
        % explanation - we want to end up with a grain that has a volume
        % which is consistent with the difspot sizes.  Adjust the threshold
        % until we achieve this.
        % We need other fields:
        %   parameters.rec.thresholding.iter_factor
        %   parameters.rec.thresholding.num_iter

            vol = grain_rec.vol;

            par_thr = obj.getThrParams();

            % slow and stupid for the moment

            % get bounding box sizes from pair table
            query = sprintf([ ...
                'SELECT avbbXsize, avbbYsize ' ...
                '   FROM %s ' ...
                '   WHERE grainid = %d' ], ...
                obj.param.acq.pair_tablename, grain_rec.id);
            [bbx, bby] = mym(query);
            bbx = median(bbx);
            bby = median(bby);
            target = bbx * bbx * bby * par_thr.iter_factor;

            if (~exist('threshold_val', 'var') || isempty(threshold_val))
                % calulate intensity in the central region of the grain
                centerx = round(size(vol, 1) * 0.45) : round(size(vol, 1) * 0.55);
                centery = round(size(vol, 2) * 0.45) : round(size(vol, 2) * 0.55);
                centerz = round(size(vol, 3) * 0.45) : round(size(vol, 3) * 0.55);
                % use this as a first guess
                threshold_val = gtImgMeanValue(vol(centerx, centery, centerz)) ;
            end
            if (~exist('seed', 'var') || isempty(seed) || any(seed <= 0))
                % centre point for morphological
                seed = round([size(vol, 1), size(vol, 2), size(vol, 3)] / 2);
            end

            happy = false; %:-(
            count = 0;
            while (~happy && count < par_thr.num_iter)
                % segment, test size
                grain_seg = obj.segmentAndDoMorph( grain_rec, threshold_val, ...
                    par_thr.do_morph_recon, seed);

                test = double(sum(grain_seg.seg(:)));
                delta = 100 * (test - target) / target;

                if (abs(delta) > 1)
                    delta_sign = sign(delta);
                    delta_mag = min(threshold_val * abs(delta) / 100, 0.2);

                    threshold_val = threshold_val + (delta_sign * delta_mag);
                else
                    happy = true; %:-)
                end
                count = count + 1;
            end
            fprintf('finished grain %d after %d iterations, error %0.2f\n', ...
                grain_rec.id, count, delta)
        end

        function grain_seg = segmentAndDoMorph(obj, grain_rec, threshold, do_morpho, seed, do_region_prop, border_mask, use_levelsets)
        % GTTHRESHOLD/SEGMENTANDDOMORPH

            if (~exist('do_region_prop', 'var') || isempty(do_region_prop))
                params = obj.getThrParams();
                do_region_prop = params.do_region_prop;
            end
            if (~exist('border_mask', 'var') || isempty(border_mask))
                params = obj.getThrParams();
                border_mask = params.mask_border_voxels;
            end
            if (~exist('use_levelsets', 'var') || isempty(use_levelsets))
                params = obj.getThrParams();
                use_levelsets = params.use_levelsets;
            end

            message = 'morphological reconstruction failed';
            if (isfield(grain_rec, 'id'))
                message = [message ...
                    sprintf(' for grain %d - continue without...', grain_rec.id)];
            end

            seg_vol = gtVolumeMaskBorder(grain_rec.vol, 0, border_mask);
            if (use_levelsets)
                % Let's estimate the mu
                mus = [0, mean(seg_vol(seg_vol > threshold))]
                seg_vol = gtMathsSegmentConstrainedLevelset(seg_vol, mus, 'lower_limits', 0);
                if (obj.conf.verbose)
                    fprintf('GtSegmentation:levelset initial threshold: %g, levels:%s, final threshold: %g\n', ...
                        threshold, sprintf('% g', mus), mean(mus))
                end
                threshold = mean(mus);
            end
            seg_vol = seg_vol > threshold;

            % morphological reconstruction removes not connected areas
            if (do_morpho && ~isempty(seed))
                try
                    marker = false(size(seg_vol));
                    if isequal(size(seg_vol),size(seed))
                        marker = logical(seed);
                    else
                        for ii = 1 : size(seed, 1)
                            marker(seed(ii,1), seed(ii,2), seed(ii,3)) = true;
                        end
                    end
                    marker = marker & seg_vol;
                    seg_vol = uint8(imreconstruct(marker, seg_vol));
                catch mexc
                    gtPrintException(mexc, message);
                end
            else
                seg_vol = uint8(seg_vol);
                seed = zeros(0, 3);
            end

            grain_seg = GtThreshold.getDataStructureDefinition();

            if (do_region_prop)
                regprop = regionprops(seg_vol, 'BoundingBox', 'Area');
                if (length(regprop) > 1)
                    % let's take the biggest! (usually happens without morpho)
                    ind     = find([regprop.Area] == max([regprop.Area]), 1);
                    regprop = regprop(ind);
                end

                if (~isempty(regprop))
                    seg_bb = regprop.BoundingBox([2 1 3 5 4 6]);

                    % ceil because Matlab regionprops yields "0.5" (instead of 1)
                    % for bb start values
                    grain_seg.seg   = gtCrop(seg_vol, ceil(seg_bb));
                    grain_seg.Area  = regprop.Area;
                    seg_bb(1:3)     = grain_rec.shift + ceil(seg_bb(1:3)) - 1;
                    grain_seg.segbb = seg_bb;
                end
            else
                grain_seg.seg   = seg_vol;
                grain_seg.Area  = [];
                grain_seg.segbb = [grain_rec.shift, size(seg_vol)];
            end
            grain_seg.threshold = threshold;
            grain_seg.seed = seed;
        end
    end

    methods (Static, Access = public)
        function seg = getDataStructureDefinition()
            seg = struct('seg', [], 'Area', [], 'segbb', zeros(0, 6), ...
                'threshold', 0, 'seed', zeros(0, 3));
        end
    end
end
