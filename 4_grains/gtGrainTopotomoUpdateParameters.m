function p = gtGrainTopotomoUpdateParameters(scan_xml, range_basetilt, nproj_basetilt, p, detgeo_tt, pl_ind, gr, varargin)
    conf = struct('check_flips', false, ...
        'verbose', true, ...
        'beamline', 'esrf_id11', ...
        'det_roi_offset_uv', [], ...
        'det_ind', [] );
    conf = parse_pv_pairs(conf, varargin);

    if (~exist('p', 'var') || isempty(p))
        p = gtLoadParameters();
    end

    o = GtConditionalOutput(conf.verbose);

    [tt_scan_dir, ~, ~] = fileparts(scan_xml);
    o.fprintf('Importing Topotomo acquisition into current DCT parameters:\n')
    o.fprintf(' - Path of Topotomo scan: %s\n', tt_scan_dir)
    o.fprintf(' - Path of DCT scan: %s\n', p.acq(1).dir)
    if (~isfield(p.acq(1), 'motors') || isempty(fieldnames(p.acq(1).motors)))
        o.fprintf(' - Reading DCT motor positions..')
        c = tic();
        xmlfname = fullfile(p.acq.dir, '0_rawdata', 'Orig', [p.acq.name '.xml']);
        acq_xml = gtLoadAcquisitionXML(xmlfname, false, false);

        % We now have to handle the fact that it is a legacy datastructure
        p.acq(1).motors = acq_xml.motors;
        base_acq = make_parameters(2);
        base_acq = base_acq.acq;
        for ii_a = numel(p.acq):-1:1
            new_acq(ii_a) = gtAddMatFile(base_acq, p.acq(ii_a), true);
        end
        p.acq = new_acq;
        o.fprintf('\b\b: Done in %g seconds.\n', toc(c))
    end

    acq_tt = p.acq(1);

    o.fprintf(' - Reading Topotomo acquisition parameters..')
    c = tic();
    topotomo_xml = gtLoadAcquisitionXML(scan_xml, false, false);
    acq_tt = gtAddMatFile(acq_tt, topotomo_xml, true, false, false);
    acq_tt.type = 'topotomo'; % The XML tends to set it to 360degree
    acq_tt = gtAcqDefaultParameters(acq_tt);
    acq_tt.collection_dir = tt_scan_dir;
    o.fprintf('\b\b: Done in %g seconds.\n', toc(c))

    if (acq_tt.energy < 0)
        disp(' ')
        warning('Energy reported by the XML file is: %d, parsing info file...', acq_tt.energy)
        info_file = fullfile(tt_scan_dir, [acq_tt.name '.info']);
        fid = fopen(info_file);
        info_line = fgetl(fid);
        fclose(fid);
        energy = regexp(info_line, 'Energy=\s*(?<value>.+)', 'names', 'once');
        acq_tt.energy = str2double(energy.value);
        disp(' ')
    end

    % Automatically checking for flips is switched off in config
    % To be revised - depends on optical configuration and physical mounting
    % of tt-detector & camera
    if (conf.check_flips)
        o.fprintf(' - Checking topotomo image flips..')
        c = tic();
        cfg_file = fullfile(tt_scan_dir, [acq_tt.name '.cfg']);
        fid = fopen(cfg_file);
        valid = true;
        file_content = {};
        while (valid)
            file_content{end+1} = fgets(fid); %#ok<AGROW>
            valid = file_content{end} ~= -1;
        end
        fclose(fid);
        file_content = [file_content{:}];
        horz_flip = regexp(file_content, 'ccd_flip_horz\s*(?<value>\d+)', 'names', 'once');
        vert_flip = regexp(file_content, 'ccd_flip_vert\s*(?<value>\d+)', 'names', 'once');
        if (str2double(horz_flip.value))
            acq_tt.detroi_u_off = acq_tt.true_detsizeu - acq_tt.detroi_u_off;
        end
        if (str2double(vert_flip.value))
            acq_tt.detroi_v_off = acq_tt.true_detsizev - acq_tt.detroi_v_off;
        end
        o.fprintf('\b\b: Done in %g seconds.\n', toc(c))
    end

    switch (conf.beamline)
        case 'esrf_id06'
            dct_samt = [0 0 0];
            tt_samt  = [0 0 0];
            dct_samr = [p.acq(1).motors.phi, p.acq(1).motors.chi];
            tt_samr  = [acq_tt.motors.phi, acq_tt.motors.chi];
        case 'esrf_id11'
            dct_samt = [p.acq(1).motors.samtx, p.acq(1).motors.samty, p.acq(1).motors.samtz];
            tt_samt  = [acq_tt.motors.samtx, acq_tt.motors.samty, acq_tt.motors.samtz];
            % samr X and Y reversed, because the first on the right is the one
            % applied first when doing SAM->LAB
            dct_samr = [p.acq(1).motors.samry, p.acq(1).motors.samrx];
            tt_samr  = [acq_tt.motors.samry, acq_tt.motors.samrx];
    end

    % we first update the first acquisition diffractometer
    p.acq(1).sample_shifts = dct_samt;
    p.acq(1).sample_tilts = dct_samr;

    p.diffractometer(1) = gtGeoDiffractometerDefinition(conf.beamline, ...
        'axes_rotation_offset', p.acq(1).motors.diffrz, ...
        'axes_sam_tilt_offset', p.acq(1).sample_tilts, ...
        'shifts_sam_stage', p.acq(1).sample_shifts );

    acq_tt.sample_shifts = tt_samt;
    acq_tt.sample_tilts = tt_samr;

    acq_tt.range_basetilt = range_basetilt;
    acq_tt.nproj_basetilt = nproj_basetilt;

    acq_tt.dist = detgeo_tt.detrefpos(1);

    if (isempty(conf.det_ind))
        conf.det_ind = numel(p.detgeo) + 1;
    end
    p.acq(conf.det_ind) = acq_tt;

    % added sample_tilt_offsets here
    p.diffractometer(conf.det_ind) = gtGeoDiffractometerDefinition(conf.beamline, ...
        'axes_rotation_offset', acq_tt.motors.diffrz, ...
        'axes_sam_tilt_offset', acq_tt.sample_tilts, ...
        'shifts_sam_stage', acq_tt.sample_shifts );

    p.detgeo(conf.det_ind) = detgeo_tt;
    if (~isempty(conf.det_roi_offset_uv))
        det_dir = [p.detgeo(conf.det_ind).detdiru; p.detgeo(conf.det_ind).detdirv]';
        det_pix_size = [p.detgeo(conf.det_ind).pixelsizeu, p.detgeo(conf.det_ind).pixelsizev];
        det_shift =  det_dir * (det_pix_size .* conf.det_roi_offset_uv)';

        p.detgeo(conf.det_ind).detrefpos = p.detgeo(conf.det_ind).detrefpos + det_shift;
        p.detgeo(conf.det_ind).detorig = p.detgeo(conf.det_ind).detorig + det_shift;
    end
    % It might be that the Detector definition ha to be shifted as well!!
%     p.detgeo(conf.det_ind).detrefpos = p.detgeo(conf.det_ind).detrefpos + p.diffractometer(conf.det_ind).shifts_sam_stage;
%     p.detgeo(conf.det_ind).detorig   = p.detgeo(conf.det_ind).detorig + p.diffractometer(conf.det_ind).shifts_sam_stage;

    p.recgeo(conf.det_ind) = p.recgeo(1);
    p.recgeo(conf.det_ind).voxsize = mean([detgeo_tt.pixelsizeu, detgeo_tt.pixelsizev]) * [1, 1, 1];

    if (isempty(pl_ind))
        o.fprintf(' - Guessing Topotomo pl_ind, with respect to DCT (for the range: [%g, %g])..', range_basetilt)
        c = tic();
        % Trying to detect the tilts
        diff_tt = p.diffractometer(conf.det_ind);
        diff_dct = p.diffractometer(1);
        t = gtGeoDiffractometerTensor(diff_tt, 'sam2lab', 'reference_diffractometer', diff_dct);

        pl_samd_sign = gr.allblobs(1).pl;
        pl_labd = gtGeoSam2Lab(pl_samd_sign, t, p.labgeo, p.samgeo, true);

        angle = gtMathsVectorsAnglesDeg(diff_tt.axes_rotation, pl_labd, false);
        o.fprintf('\b\b: Done in %g seconds.\n', toc(c))
        [~, pl_ind] = min(angle);
        o.fprintf('   + Chosen pl_ind: %d (%g deg)\n', pl_ind, angle(pl_ind))
    end
    p.acq(conf.det_ind).pl_ind = pl_ind;
end