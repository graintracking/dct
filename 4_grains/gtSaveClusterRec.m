function gtSaveClusterRec(phase_id, grain_ids, cl_rec_in, varargin)
    details_fields = {'VOL3D', 'ODF6D', 'ODFw', 'ODFuvw', 'SEG'};
    conf = struct( ...
        'fields', {details_fields}, ...
        'parameters', {[]}, ...
        'clean', {false} );
    conf = parse_pv_pairs(conf, varargin);

    % Only allow for the possible fields
    conf.fields = intersect(conf.fields, details_fields);
    conf.fields = intersect(conf.fields, fieldnames(cl_rec_in));

    grain_ids_sorted = sort(grain_ids);

    if (any(grain_ids_sorted ~= grain_ids))
        warning([mfilename ':wrong_argumet'], ...
            'Some grain IDs were no sorted: %s', sprintf(' %d', grain_ids));
    end

    if (isempty(conf.parameters))
        phase_dir = fullfile('4_grains', sprintf('phase_%02d', phase_id));
    else
        phase_dir = fullfile(conf.parameters.acq.dir, '4_grains', sprintf('phase_%02d', phase_id));
    end

    str_ids = sprintf('_%04d', grain_ids_sorted);
    cl_file = fullfile(phase_dir, sprintf('grain_cluster_details%s.mat', str_ids));

    if (conf.clean)
        if (exist(cl_file, 'file'))
            cl_rec_out = load(cl_file);
        else
            cl_rec_out = struct();
        end
        for ii_f = 1:numel(conf.fields)
            cl_rec_out.(conf.fields{ii_f}) = cl_rec_in.(conf.fields{ii_f});
        end
        save(cl_file, '-struct', 'cl_rec_out', '-v7.3');
    else
        mat_cl_rec_file = matfile(cl_file, 'Writable', true);
        for ii_f = 1:numel(conf.fields)
            mat_cl_rec_file.(conf.fields{ii_f}) = cl_rec_in.(conf.fields{ii_f});
        end
    end
end
