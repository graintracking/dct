function cluster = gtLoadCluster(phase_id, grain_ids, varargin)
% GTLOADCLUSTER load the projection input of a grain cluster
%     cl = function cluster = gtLoadCluster(phase_id, cl_or_gr_ids, varargin)
%     INPUT:
%       phaseID     = <int>     phase ID
%       grain_ids   = <int>     either the cluster_id (->sample) or
%                                    list of grain_ids composing the
%                                    cluster
%
%     OPTIONAL INPUT (p/v pairs):
%       fields      = <string>  only load specific  fields from
%                               cluster structure ('ODF6D', 'SEG')
%       parameters  = <struct>  for security (make sure to load from right directory)
%
    conf = struct('fields', {{}}, 'parameters', {[]});
    conf = parse_pv_pairs(conf, varargin);

    if (isempty(conf.parameters))
        phase_dir = fullfile('4_grains', sprintf('phase_%02d', phase_id));
    else
        phase_dir = fullfile(conf.parameters.acq.dir, '4_grains', sprintf('phase_%02d', phase_id));
    end
    if (numel(grain_ids) == 1)
        sample = GtSample.loadFromFile;
        grain_ids_sorted = sample.phases{phase_id}.clusters(grain_ids).included_ids;
    else
        grain_ids_sorted = grain_ids;  %sort(grain_ids);
        if (any(grain_ids_sorted ~= grain_ids))
        warning([mfilename ':wrong_argumet'], ...
            'Some grain IDs were no sorted: %s', sprintf(' %d', grain_ids));
        end
    end

    str_ids = sprintf('_%04d', grain_ids_sorted);
    cluster_file = fullfile(phase_dir, sprintf('grain_cluster%s.mat', str_ids));

    if (isempty(conf.fields))
        cluster = load(cluster_file);
    else
        cluster = load(cluster_file, conf.fields{:});
    end
end
