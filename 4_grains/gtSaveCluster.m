function gtSaveCluster(phase_id, grain_ids, cluster, varargin)
    conf = struct('fields', {{}}, 'parameters', {[]});
    conf = parse_pv_pairs(conf, varargin);

    if (isempty(conf.parameters))
        phase_dir = fullfile('4_grains', sprintf('phase_%02d', phase_id));
    else
        phase_dir = fullfile(conf.parameters.acq.dir, '4_grains', sprintf('phase_%02d', phase_id));
    end

    grain_ids_sorted = sort(grain_ids);

    if (any(grain_ids_sorted ~= grain_ids))
        warning([mfilename ':wrong_argumet'], ...
            'Some grain IDs were no sorted: %s', sprintf(' %d', grain_ids));
    end

    str_ids = sprintf('_%04d', grain_ids_sorted);
    cluster_file = fullfile(phase_dir, sprintf('grain_cluster%s.mat', str_ids));

    if (isempty(conf.fields))
        save(cluster_file, '-struct', 'cluster', '-v7.3');
    else
        mat_grain_file = matfile(cluster_file, 'Writable', true);
        for ii_f = 1:numel(conf.fields)
            mat_grain_file.(conf.fields{ii_f}) = cluster.(conf.fields{ii_f});
        end
    end
end
