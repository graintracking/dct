function grain = gtLoadGrain(phase_id, grain_id, varargin)
    conf = struct('fields', {{}}, 'parameters', {[]}, 'is_extended', {false});
    conf = parse_pv_pairs(conf, varargin);

    if (isempty(conf.parameters))
        phase_dir = fullfile('4_grains', sprintf('phase_%02d', phase_id));
    else
        phase_dir = fullfile(conf.parameters.acq.dir, '4_grains', sprintf('phase_%02d', phase_id));
    end

    for ii = numel(grain_id):-1:1
        if (conf.is_extended)
            grain_file = fullfile(phase_dir, sprintf('grain_extended_%04d.mat', grain_id(ii)));
        else
            grain_file = fullfile(phase_dir, sprintf('grain_%04d.mat', grain_id(ii)));
        end

        if (isempty(conf.fields))
            grain(ii) = load(grain_file);
        else
            grain(ii) = load(grain_file, conf.fields{:});
        end
    end
end
