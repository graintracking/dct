function sample=gtSampleGeoInSampleSystem(parameters)
% sample=gtSampleGeoInSampleSystem(parameters)
% 
% Defines the origin of Z axis of the sample coordinate system, and gives 
%  sample radius, top, bottom and volume in the sample system, in pixels.
%
% Sample is fixed in the lab reference at omega=0 (right-handed):
%   X axis: along the beam, origin on the rotation axis
%   Y axis: horizontal
%   Z axis: vertical, positive upwards
%

if ~exist('parameters','var')
  load('parameters.mat');
end

% Sample coordinate system
% origin X = on the rotation axis
% origin Y = on the rotation axis

% origin Z in image coordinates:
sorZim = floor(parameters.acq.ydet/2); % central pixel in images (the one higher)   
sample.sorZim=sorZim;

% Radius, top, bottom, volume:
sample.rad= parameters.acq.bb(3)/2;   
sample.top= sorZim-parameters.acq.bb(2);   
sample.bot= sorZim-(parameters.acq.bb(2)+parameters.acq.bb(4));
sample.vol= (sample.top-sample.bot)*sample.rad^2*pi ;

end % end of function