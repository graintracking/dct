function allblobs = gtGrainAllblobsDefinition()
    allblobs = struct( ...
        'type', [], ...
        'plcry', [], ...       % undeformed plane normals in the Crystal ref
        'plorig', [], ...      % unsigned undeformed plane normals in Sample ref
        'plsamdef', [], ...    % unsigned deformed plane normals in Sample ref
        'pl', [], ...          % signed (!) deformed plane normals in Sample ref
        'pllab', [], ...       % signed (!) deformed plane normals rotated to diffraction in Lab ref
        'hkl', [], ...         % {hkl} index
        'hklsp', [], ...       % specific/signed (hkl) index
        'sintheta', [], ...    % sin(Bragg_angle) in deformed state
        'theta', [], ...       % Bragg angle in deformed state (deg)
        'eta', [], ...         % Eta azimuth angle in deformed state (deg)
        'thetatype', [], ...
        'lorentzfac', [], ...  % Lorentz factor
        'omega', [], ...       % omega rotation angles (0..360deg) in degrees
        'omind', [], ...       % omega variant index (1..4)
        'phi', [], ...         % phi rotation angles (0..360deg) in degrees (base tilt)
        'phind', [], ...       % phi variant index (1..4)
        'dvec', [], ...        % diffracted beam dir in Lab ref
        'dvecsam', [], ...     % diffracted beam dir in Sample ref
        'srot', [], ...        % Sample->Lab (!) rotation coordinate transform matrix for column (!) vectors
        ...                    % OR Lab->Sample (!) rotation coordinate transform matrix for row (!) vectors
        'detector', [], ...
        'rot_s2l', [], ...     % 4x4 Sample->Lab (!) rotation coordinate transform matrix for column (!) vectors
        'rot_l2s', [] );       % 4x4 Lab->Sample (!) rotation coordinate transform matrix for column (!) vectors
end