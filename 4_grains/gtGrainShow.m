function gtGrainShow(grain, parameters, varargin)
% GTGRAINSHOW  Shows the calulated diffraction vectors and detector intersection points
%
%     gtGrainShow(grain, [parameters], varargin)
%     -----------------------------------------------------------------
%
%     INPUT:
%       grain      = <struct>   grain of interest
%       parameters = <struct>   parameters file {parameters.mat}
%
%     OPTIONAL INPUT (p/v pairs):
%       showmode   = <string> (one of: {'color'} | 'axis')
%                   'color' : show or not the prediction spot positions
%                             coloured as omega
%                   'axis'  : plot axis, image reference and beam direction
%                             in the image
%       difspots   = <boolean>  Shows the difspot images from the stack. It
%                   requires a .proj data structure to have been computed already.
%                   Default: false
%       markersize = <double>   Marker size. Default: 30
%       clims      = <double>   Scale of grays. Default: []
%       det_ind    = <double>   Detector index. Default: 1

    conf = struct( ...
        'showmode', 'color', ...
        'difspots', false, ...
        'clims', [], ...
        'markersize', 30, ...
        'det_ind', 1);
    [conf, ~] = parse_pv_pairs(conf, varargin);

    if (~exist('parameters', 'var') || isempty(parameters))
        parameters = gtLoadParameters();
    end

    detgeo = parameters.detgeo(conf.det_ind);
    labgeo = parameters.labgeo;

    detusize = detgeo.detsizeu;
    detvsize = detgeo.detsizev;

    acq = parameters.acq(conf.det_ind);
    mode = acq.type;
    if (ismember(mode, {'360degree', '180degree'}))
        mode = 'dct';
    end
    is_dct = strcmpi(mode, 'dct');
    is_topotomo = strcmpi(mode, 'topotomo');

    f = figure();
    ax = axes('Parent', f);
    if (conf.difspots && isfield(grain, 'proj') && ~isempty(grain.proj) ...
            && numel(grain.proj) >= conf.det_ind)
        overlay = ones(detvsize, detusize);
        proj = grain.proj(conf.det_ind);
        shifts_u = cat(1, proj.bl(:).bbuim) - 1;
        shifts_v = cat(1, proj.bl(:).bbvim) - 1;
        for ii_b = 1:numel(proj.bl)
            difspot = sum(proj.bl(ii_b).intm, 3)';
            overlay = gtPlaceSubVolume(overlay, difspot, [shifts_v(ii_b), shifts_u(ii_b), 0]);
        end
        overlay = (overlay - min(overlay(:))) / (max(overlay(:)) - min(overlay(:))) * 0.7 + 0.3;
        overlay = overlay(:, :, [1, 1, 1]);
    else
        overlay = 0.3 * ones(detvsize, detusize, 3);
    end
    imshow(overlay, conf.clims, 'Parent', ax);
    hold(ax, 'on');

    % Image frame
    plot([0, detusize+1], [0, 0], 'k')
    plot([0, detusize+1], [detvsize+1, detvsize+1], 'k')
    plot([0, 0], [0, detvsize+1], 'k')
    plot([detusize+1, detusize+1], [0, detvsize+1], 'k')

    % Midlines
    plot([0, detusize+1], [detvsize, detvsize] / 2 + 0.5, '-.k', 'Linewidth', 0.25)
    plot([detusize, detusize] / 2 + 0.5, [0, detvsize+1], '-.k', 'Linewidth', 0.25)

    gc_uv = gtGeoGrainCenterSam2Det(grain.center, 1:360, parameters, conf.det_ind);
    plot(ax, gc_uv(:, 1), gc_uv(:, 2), '-b')
    mean_gc_uv = [mean(gc_uv(:, 1)), mean(gc_uv(:, 2))];
    plot(ax, mean_gc_uv(1), mean_gc_uv(2), 'xb')
%     plot(ax, [0, detusize+1] + mean_gc_uv(1) - detusize / 2, [0, detvsize+1] + mean_gc_uv(2) - detvsize / 2, '--r', 'Linewidth', 0.25)
%     plot(ax, [detusize+1, 0] + mean_gc_uv(1) - detusize / 2, [0, detvsize+1] + mean_gc_uv(2) - detvsize / 2, '--r', 'Linewidth', 0.25)

    switch (lower(conf.showmode))
        case 'axis'
            % Set figure propeties
            axis equal
            set(ax, 'Position', [0.05 0.05 0.95 0.9])
            xlabel(ax, 'U direction')
            ylabel(ax, 'V direction')

            % Beam direction in image
            beamuv = detgeo.Qdet * labgeo.beamdir';
            % Arrow in figure indicating beam direction
            quiver(ax, detgeo.detrefu, detgeo.detrefv, beamuv(1), beamuv(2), ...
                1000, 'r', 'Linewidth', 3)

            % Rotation axis direction in image
            rotuv = detgeo.Qdet * labgeo.rotdir';
            % Arrow in figure indicating rotation axis direction
            quiver(ax, detgeo.detrefu, detgeo.detrefv, rotuv(1), rotuv(2), ...
                1000, 'b', 'Linewidth', 3)

            colormap(ax, gray);
        case 'color'
            ondet = grain.allblobs(conf.det_ind).detector.ondet;
            if (is_dct)
                uvw = grain.allblobs(conf.det_ind).detector.uvw(ondet, :);
            elseif (is_topotomo)
                uvw = grain.allblobs(conf.det_ind).detector.uvpw(ondet, [1, 2, 4]);
            end
            cm = inputwdefault(['Choose the type of colormap: omega '...
                '(o), eta (e), or hkl (h) coloring: [o]'], 'o');

            if (strcmpi(cm, 'h'))
                disp('Using hkl colormap ...')
                thetatype = grain.allblobs(conf.det_ind).thetatype(ondet);
                theta = grain.allblobs(conf.det_ind).theta(ondet);
                thetatype = thetatype(theta <= detgeo.detanglemax/2 ...
                                    & theta >= detgeo.detanglemin/2);
                uthetatypes = unique(thetatype);
                cmap_hkl = jet(numel(uthetatypes));
                freezeColors(ax)
                colormap(ax, cmap_hkl);
                c = colorbar('peer', ax);
                xlabel(c, 'Theta (deg)')
                caxis(ax, [min(theta), max(theta)])

                for it = 1:numel(uthetatypes)
                    indx = (thetatype == uthetatypes(it));
                    h = scatter3(ax, uvw(indx, 1), uvw(indx, 2), uvw(indx, 3), ...
                        conf.markersize, cmap_hkl(it, :), 'fill', 'o');
                    set(h, 'MarkerEdgeColor', [0 0 0])
                end
            else
                if (strcmpi(cm, 'e'))
                    disp('Using eta colormap ...')
                    angles = grain.allblobs(conf.det_ind).eta(ondet);
                    angle_label = 'Eta (deg)';
                else
                    disp('Using omega colormap ...')
                    angles = grain.allblobs(conf.det_ind).omega(ondet);
                    angle_label = 'Omega (deg)';
                end

                caxis(ax, [0, 360])
                scatter(ax, uvw(:, 1), uvw(:, 2), conf.markersize, angles, 'fill', 'o')
                colormap(ax, jet(3601))
                c = colorbar('peer', ax);
                xlabel(c, angle_label)
            end

            try
                impixelinfo
            catch mexc
                disp(mexc)
            end
    end

    view(ax, 0, 90)
    xlim(ax, [0, detusize])
    ylim(ax, [0, detvsize])

    hold(ax, 'off');

    ax_title = sprintf('Fsim for grain %d in phase %d', grain.id(1), grain.phaseid);
    title(ax, ax_title, 'FontWeight', 'bold');
end
