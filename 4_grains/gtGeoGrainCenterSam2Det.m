function gc_det_pos = gtGeoGrainCenterSam2Det(gc, omegas, parameters, det_index)
    if (~exist('det_index', 'var'))
        det_index = 1;
    end
    labgeo = parameters.labgeo;
    samgeo = parameters.samgeo;
    detgeo = parameters.detgeo(det_index);

    num_omegas = numel(omegas);
    ones_omegas = ones(1, num_omegas);
    gc_sam_pos = gc(ones_omegas, :);
    gc_lab_pos = gtGeoSam2Lab(gc_sam_pos, omegas, labgeo, samgeo, false);

    beam_dir = labgeo.beamdir(ones_omegas, :);
    gc_det_pos = gtFedPredictUVMultiple([], beam_dir', gc_lab_pos', ...
        detgeo.detrefpos', detgeo.detnorm', detgeo.Qdet, ...
        [detgeo.detrefu, detgeo.detrefv]')';
end