function gtVTKMeshWriter(mesh, filename, varargin)
% GTVTKMESHWRITER  Write a mesh to VTK file (polydata) with fields or color maps.
%
%   gtVTKMeshWriter(mesh, filename, varargin)
%   --------------------------------------------------------------------------
%
%     INPUT:
%       mesh     = <struct>  Mesh in a structure, composed of following fields:
%                            - vertices: Nv x 3 array -> vertices coordinates
%                            - faces:    Nf x 3 array -> face vertices indices
%                            - edges:    Ne x 2 array -> edge vertices indices
%       filename = <string>  Path to the output VTK file
%
%     OPTIONAL INPUT:
%       'binary'    = <bool>   Write output in binary format (true by default)
%       'coordType' = <string> Output type for coordinates ('float', 'double')
%       'cellField' = <struct> Cell field struct, composed of following fields:
%                              - name    = <string> Name of the field
%                              - data    = <any>    Data array
%                              - cmap    = <cmap>   Colormap         (optional)
%                              - opacity = <double> Color opacity    (optional)
%                              - type    = <string> Field type   (only scalars)
%       'cellColor' = <struct> Cell color struct, composed of following fields:
%                              - name    = <string> Name of the field
%                              - data    = <any>    Color array
%                              - opacity = <double> Color opacity    (optional)
%
%     Version 001 20-10-2012 by YGuilhem yoann.guilhem@esrf.fr

par.header = 'VTK mesh created by DCT (http://sourceforge.net/p/dct)';
par.verbose = false;
par.binary = true;
par.coordType = 'float';
par.cellField = [];
par.cellColor = [];
par = parse_pv_pairs(par, varargin);

%% Check coordinate data type
switch par.coordType
case {'float', 'double'}
    coordFMT = '%f %f %f\n';
otherwise
    gtError('gtVTKMeshWriter:wrong_coord_type', ...
        ['Unsupported coordinates datatype: ' par.coordType]);
end

%% Get file parts and check extension
[fpath, fname, fext] = fileparts(filename);
if isempty(fext)
    fext = '.vtk';
    filename = fullfile(fpath, [fname fext]);
elseif ~strcmp(fext, '.vtk')
    gtError('gtVTKMeshWriter:wrong_file_extension', ...
        'Output file extension should be ''.vtk''!');
end

%% Open file for writing
fid = fopen(filename, 'w');
if fid ==-1
    gtError('gtVTKMeshWriter:bad_output_file', 'Can''t open the file.');
end

%% Header
fprintf(fid, '# vtk DataFile Version 3.0\n');
fprintf(fid, '%s\n', par.header);
if par.binary
    fprintf(fid, 'BINARY\n');
else
    fprintf(fid, 'ASCII\n');
end
fprintf(fid, 'DATASET POLYDATA\n');

%% Points / Vertices
fprintf(fid, 'POINTS %d %s\n', size(mesh.vertices, 1), par.coordType);
if par.binary
    count = fwrite(fid, mesh.vertices', par.coordType, 0, 'ieee-be');
    if count ~= numel(mesh.vertices)
        warning('gtVTKMeshWriter:vertice_warning', ...
            'Problem in writing vertices.');
    end
    fprintf(fid, '\n');
else
    fprintf(fid, coordFMT, mesh.vertices');
end

%% Lines / Edges
nEdges = 0;
if (isfield(mesh, 'edges') || isprop(mesh, 'edges'))
    nEdges = size(mesh.edges, 1);
    nEdgeValues = 3 * nEdges;
    fprintf(fid, 'LINES %d %d\n', nEdges, nEdgeValues);
    output = [2*ones(1, nEdges) ; mesh.edges'-1];
    if par.binary
        count = fwrite(fid, output, 'uint32', 0, 'ieee-be');
        if count ~= nEdgeValues
            warning('gtVTKMeshWriter:edge_warning', ...
                'Problem in writing edges.');
        end
        fprintf(fid, '\n');
    else
        fprintf(fid, '%d %d %d\n', output);
    end
end

%% Polygons / Faces / Cells
nFaces = 0;
if (isfield(mesh, 'faces') || isprop(mesh, 'faces'))
    nFaces = size(mesh.faces, 1);
    nFaceValues = 4 * nFaces;
    fprintf(fid, 'POLYGONS %d %d\n', nFaces, nFaceValues);
    output = [3*ones(1, nFaces) ; mesh.faces'-1];
    if par.binary
        count = fwrite(fid, output, 'uint32', 0, 'ieee-be');
        if count ~= nFaceValues
            warning('gtVTKMeshWriter:face_warning', ...
                'Problem in writing faces.');
        end
        fprintf(fid, '\n');
    else
        fprintf(fid, '%d %d %d %d\n', output);
    end
end
nCells = nEdges + nFaces;

%% Cell fields
if ~isempty(par.cellField) || ~isempty(par.cellColor)
    fprintf(fid, 'CELL_DATA %d\n', nCells);
end

if ~isempty(par.cellField)
    for ifield=1:length(par.cellField)
        cField = par.cellField(ifield);
        % Check opacity
        if ~isfield(cField, 'opacity') || isempty(cField.opacity)
            cField.opacity = 1;
        end
        % Check field type
        if ~isfield(cField, 'type') || isempty(cField.type)
            cField.type = 'scalars';
        elseif ~strcmpi(cField.type, 'scalars')
            gtError('gtVTKMeshWriter:wrong_field_type', ...
                'Sorry, only scalar cellfields are implemented right now');
        end

        inputType = class(cField.data);
        [vtkType, outType, outFMT] = gtVTKConvertTypeMatlab2VTK(inputType);
        fprintf(fid, '%s %s %s 1\n', upper(cField.type), cField.name, ...
            vtkType);
        % Link to tookup table
        if ~isfield(cField, 'cmap') || isempty(cField.cmap)
            fprintf(fid, 'LOOKUP_TABLE default\n');
        else
            fprintf(fid, 'LOOKUP_TABLE cellFieldTable%d\n', ifield);
        end
        % Field values
        if par.binary
            count = fwrite(fid, cField.data, outType, 0, 'ieee-be');
            if count ~= numel(cField.data)
                warning('gtVTKMeshWriter:cellfield_warning', ...
                    ['Problem writing cell field "' cField.name '"']);
            end
            fprintf(fid, '\n');
        else
            fprintf(fid, [outFMT '\n'], cField.data);
        end
        % Lookup table
        if isfield(cField, 'cmap') && ~isempty(cField.cmap)
            nSteps = size(cField.cmap, 1);
            fprintf(fid, 'LOOKUP_TABLE cellFieldTable%d %d\n', ifield, nSteps);
            output = [cField.cmap' ; cField.opacity*ones(1, nSteps)];
            if par.binary
                count = fwrite(fid, uint8(255*output), 'uchar', 0, 'ieee-be');
                if count ~= numel(output)
                    warning('gtVTKMeshWriter:lookup_table_warning', ...
                        ['Problem writing lookup table for cell field "' ...
                        cField.name '"']);
                end
                fprintf(fid, '\n');
            else
                fprintf(fid, '%f %f %f %f\n', output);
            end
        end
    end
end

%% Cell color fields
if ~isempty(par.cellColor)
    for ifield=1:length(par.cellColor)
        cField = par.cellColor(ifield);
        % Check opacity
        if ~isfield(cField, 'opacity') || isempty(cField.opacity)
            output = cField.data';
            outFMT = '%f %f %f\n';
            nColorComp = 3;
        else
            %output = [cField.data' ; cField.opacity*ones(1, size(cField.data, 1))];
            output = [cField.data' ; cField.opacity*ones(1, nCells)];
            outFMT = '%f %f %f %f\n';
            nColorComp = 4;
        end
        % Field values
        fprintf(fid, 'COLOR_SCALARS %s %d\n', cField.name, nColorComp);
        if par.binary
            count = fwrite(fid, uint8(255*output), 'uchar', 0, 'ieee-be');
            if count ~= numel(output)
                warning('gtVTKMeshWriter:cellcolor_warning', ...
                    ['Problem writing cell color field "' cField.name '"']);
            end
            fprintf(fid, '\n');
        else
            fprintf(fid, outFMT, output);
        end
    end
end

%% Close the file end terminate
fclose(fid);

end % end of function
