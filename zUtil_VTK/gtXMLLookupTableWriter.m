function  gtXMLLookupTableWriter(cmap, filename, name, colorspace)

    if ~exist('name', 'var') || isempty(name)
        %name = inputwdefault('Please give a name to your color map', 'DCT color map');
        disp('Seeting color map name to default: ''DCT color map''');
        name = 'DCT color map';
    end
    
    if ~exist('colorspace', 'var') || isempty(colorspace)
        colorspace = 'RGB';
    end

    [fpath, fname, fext] = fileparts(filename);
    if isempty(fext)
        filename = fullfile(fpath, [fname '.xml']);
    end

    % Prepare data to output
    N = size(cmap, 1);
    %x = (0:N-1) / (N-1);
    x = (0:N-1);

    if (size(cmap, 2) == 4)
        output = [x ; cmap'];
    else
        output = [x ; ones([1 N]) ; cmap'];
    end

    % Write XML file
    fid = fopen(filename, 'w');
    fprintf(fid, ['<ColorMap name="' name '" space="%s">\n'], colorspace);
    fprintf(fid, '  <Point x="%f" o="%f" r="%f" g="%f" b="%f"/>\n', output);
    fwrite(fid, '</ColorMap>');
    fclose(fid);

end % end of function

