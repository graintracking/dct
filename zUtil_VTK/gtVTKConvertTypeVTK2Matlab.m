function [mType, inFMT, inType] = gtVTKConvertTypeVTK2Matlab(vtkType)
% GTVTKCONVERTTYPEVTK2MATLAB  Convert VTK datatype to Matlab datatype.

switch vtkType
case 'unsigned_char'
    mType  = 'uint8';
    inFMT  = '%d';
    inType = 'uchar';
case 'unsigned_short'
    mType  = 'uint16';
    inFMT  = '%d';
    inType = 'ushort';
case 'unsigned_int'
    mType  = 'uint32';
    inFMT  = '%d';
    inType = 'uint';
case 'unsigned_long'
    mType  = 'uint64';
    inFMT  = '%d';
    inType = 'uint64';
case 'char'
    mType  = 'int8';
    inFMT  = '%d';
    inType = 'schar';
case 'short'
    mType  = 'int16';
    inFMT  = '%d';
    inType = 'short';
case 'int'
    mType  = 'int32';
    inFMT  = '%d';
    inType = 'int';
case 'long'
    mType  = 'int64';
    inFMT  = '%d';
    inType = 'int64';
case 'float'
    mType  = 'single';
    inFMT  = '%f';
    inType = 'float32';
case 'double'
    mType  = 'double';
    inFMT  = '%f';
    inType = 'double';
    otherwise
        gtError('gtVTKConvertTypeVTK2Matlab:wrong_datatype', ...
            ['Unimplemented datatype "', vtkType, '", sorry...']);
end

end % end of function
