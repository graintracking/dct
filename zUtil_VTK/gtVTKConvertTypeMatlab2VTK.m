function [vtkType, outType, outFMT] = gtVTKConvertTypeMatlab2VTK(mType)
% GTVTKCONVERTTYPEMATLAB2VTK  Convert Matlab datatype to VTK datatype.

switch mType
    case 'uint8'
        outType = 'uchar';
        outFMT  = '%d';
        vtkType = 'unsigned_char';
    case 'uint16'
        outType = 'ushort';
        outFMT  = '%d';
        vtkType = 'unsigned_short';
    case 'uint32'
        outType = 'uint';
        outFMT  = '%d';
        vtkType = 'unsigned_int';
    case 'uint64'
        outType = 'uint64';
        outFMT  = '%d';
        vtkType = 'unsigned_long';
    case 'int8'
        outType = 'schar';
        outFMT  = '%d';
        vtkType = 'char';
    case 'int16'
        outType = 'short';
        outFMT  = '%d';
        vtkType = 'short';
    case 'int32'
        outType = 'int';
        outFMT  = '%d';
        vtkType = 'int';
    case 'int64'
        outType = 'int64';
        outFMT  = '%d';
        vtkType = 'long';
    case {'float', 'float32', 'single'}
        outType = 'float32';
        outFMT  = '%f';
        vtkType = 'float';
    case {'double', 'float64'}
        outType = 'double';
        outFMT  = '%f';
        vtkType = 'double';
    otherwise
        gtError('gtVTKConvertTypeMatlab2VTK:wrong_datatype', ...
            ['Unimplemented datatype "', mType, '", sorry...']);
end

end % end of function
