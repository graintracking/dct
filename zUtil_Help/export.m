function export(name,hf,paper_fig,ws_path,dotFig)
% export(name,hf,paper_fig,ws_path,dotFig)
%
% Version 002 06-12-2013 by LNervo


if isempty(hf)
    hf = gcf;
end
fcolor = get(hf,'Color');
set(hf,'Color',[1 1 1]);

if ~exist('paper_fig','var') || isempty(paper_fig)
    paper_fig = false;
end
if ~exist('ws_path','var') || isempty(ws_path)
    ws_path = false;
end
if ~exist('dotFig','var') || isempty(dotFig)
    dotFig = false;
end

if (ws_path)
    filename = fullfile(getenv('HOME'),'workspace',name);
else
    filename = fullfile('8_analysis','figures',name);
end
if (paper_fig)
    export_fig(filename,'-tif','-r600',hf)
    disp(['Saved figure as ' filename '.tif'])
else
    export_fig(filename,'-png','-r300','-transparent',hf)
    disp(['Saved figure as ' filename '.png'])
end
if (dotFig)
    saveas(hf, filename, 'fig')
    disp(['Saved figure as ' filename '.fig'])
end
set(hf,'Color',fcolor);

end % end of function
