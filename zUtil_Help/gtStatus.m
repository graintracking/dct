function gtStatus(completeCheck, checkOAR)
% GTSTATUS  Checks the status of a dataset, show progress where possible
%           and tell a user where one might have a problem.
%     gtStatus(completeCheck)
%     -----------------------
%
%     Version 004 10-05-2014 by LNervo
%       Improved output and speed
%
%     Version 003 05-10-2012 by LNervo
%       Add feedback from rec
%
%     Version 002 23/08/2012 by LNervo
%       Code formatting
%
%     Version 001 by AKing


if ~exist('completeCheck','var') || isempty(completeCheck)
    completeCheck = true;
end
if ~exist('checkOAR','var') || isempty(checkOAR)
    checkOAR = false;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% FIRST THINGS FIRST - PARAMETERS FILE
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% is there a parameters file?
parameters = [];
disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
if exist('parameters.mat','file')
    load('parameters.mat');
    fprintf('Found a parameters file...\n')
    fprintf(['  Dataset name is ' parameters.acq.name '\n'])
else
    fprintf('No parameters.mat file found! Are you in the analysis directory?\n')
    fprintf(['  Current directory is:  ' pwd() '\n'])
    return
end

problem_list{1}='List of possible problems:';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% WHERE ARE WE REGARDING OAR JOBS?
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if checkOAR
    if exist('oar.log','file')
        fid = fopen('oar.log');
    elseif exist('OAR_log/oar.log','file')
        fid = fopen(fullfile('OAR_log','oar.log'));
    end
    if ~isempty(fid)
        oarlog=textscan(fid,'%s %s %d %s %s');
        fclose(fid);
        % most recent function name
        names=oarlog{2};
        lastoar_name=names{end};
        arrays=oarlog{3};
        lastoar_array=arrays(end);

        [~,status]=unix(sprintf('oarstat --array %d',lastoar_array));
        % read job numbers from this text - assume format doesn't change!
        % get exit code with gtOarGetStats
        ndx=310+160;
        ndx=312;
        job_status='';
        count=0;
        query{1}='exit_code'; query{2}='state';
        %this will be skipped,if status is an empty array
        while ndx<length(status)
            count=count+1;
    %        fprintf(['!!!' status(ndx:ndx+6) '!!!'])
            job=str2double(status(ndx:ndx+6));
            jobstat=gtOarGetStats(job,query);
            exit_code=jobstat.exit_code;
            %exit_code=jobstat{1}.exit_code;
            state=jobstat.state;
            if strcmp(state,'Waiting')
                job_status(count)='W';
            elseif strcmp(state,'Running')
                job_status(count)='R';
            elseif strcmp(state,'Terminated') && strcmpi(exit_code,'0 (0,0,0)')
                job_status(count)='T';
            elseif strcmp(state,'Terminated')
                job_status(count)='E';
            else
                disp('could read this oar job...')
            end
            ndx=ndx+136;
        end
        %status variable can be empty, concanating from oarlog
        launch_time=char(strcat(oarlog{4}(end),{' '},oarlog{5}(end)));
        disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
        fprintf('Last OAR job was %s\n  (array %d launched at %s)\n',lastoar_name,lastoar_array,launch_time)

        % finished?
        if all(job_status=='T' | job_status=='E')
            fprintf('  All these jobs have finished\n')
        end
        % errors?
        if all(job_status=='E')
            fprintf('  All of these jobs show errors\n')
        elseif any(job_status=='E')
            fprintf('  Some of these jobs show errors\n')
        end
        % still running?
        if all(job_status=='R')
            fprintf('  All of these jobs are still running\n')
        elseif any(job_status=='R')
            fprintf('  Some of these jobs are still running\n')
        end
        % still waiting?
        if all(job_status=='W')
            fprintf('  All of these jobs are still waiting\n')
        elseif any(job_status=='W')
            fprintf('  Some of these jobs are still waiting\n')
        end
    else
        disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
        fprintf('No oar.log file found...  \n')
        fprintf('  Possible OAR problem?  \n')
    end
else
   fprintf('OAR jobs bot checked!\n') 
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% WHAT IMAGES ARE PRESENT?
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% If we found a parameters file, how many raw images should there be?
if strcmpi(parameters.acq.type,'360degree')
    totproj = 2*parameters.acq.nproj;
elseif strcmpi(parameters.acq.type,'180degree')
    totproj = parameters.acq.nproj;
else
    Mexc = MException('ACQ:invalid_parameter',...
        'Unknown type of scan. Check parameters.acq.type!');
    throw(Mexc);
end

if (parameters.acq.mono_tune ~= 0)
    extra_refs = ceil(totproj / (parameters.acq.refon * parameters.acq.mono_tune)) * parameters.acq.nref;
else
    extra_refs = 0;
end
if (parameters.acq.interlaced_turns == 0)
    filename = sprintf('%s%d0*.edf',parameters.acq.name,totproj/100);
elseif (parameters.acq.interlaced_turns > 0)
    filename = sprintf('%s*_%d0*.edf',parameters.acq.name,totproj/100);
end
if ~isempty( dir( fullfile(parameters.acq.dir,'0_rawdata','Orig',filename ) ) )
    extra_proj = length( dir( fullfile(parameters.acq.dir,'0_rawdata','Orig',filename) ) );
else
    extra_proj = 0;
end
nrefgroups = (totproj/parameters.acq.refon)+1;
totref     = nrefgroups*parameters.acq.nref + extra_refs;
% how many preprocessed images do we expect?
if (parameters.acq.mono_tune == 0)
    totrefHST = totproj/parameters.acq.refon + 1;
else
    totrefHST = (totproj/parameters.acq.refon) + 1 + (totproj/(parameters.acq.refon*parameters.acq.mono_tune));
end
totabsmed  = (totproj/parameters.prep.absint) + 1;
totfullmed = (totproj/parameters.prep.fullint) + 1;

disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
disp('Checking what images are present')

% make the first check on the full images.  If these are not all present,
% look into the other images to see what is missing:

d=dir(fullfile(parameters.acq.dir,'1_preprocessing','full','full*.edf'));
nfull=length(d);
fprintf('  Found %d full images out of %d expected\n',nfull,totproj)
if (nfull~=totproj)
    problem_list{end+1}='missing full images : possible gtCreateFullLive problem';
    preprocessing_ok=false;
else
    preprocessing_ok=true;
end

% was preprocessing successful?
if (completeCheck || ~preprocessing_ok)
    fprintf('  Checking raw and copied corrected images:\n')
    % Orig images
    d=dir(fullfile(parameters.acq.dir,'0_rawdata','Orig',[parameters.acq.name '*.edf']));
    n_orig=length(d);
    fprintf('    Found %d raw images out of %d expected\n',n_orig,totproj+extra_proj)
    if (n_orig<totproj)
        problem_list{end+1}='missing raw images: possible gtCopyCorrectUndistortCondor problem';
    end
    d=dir(fullfile(parameters.acq.dir,'0_rawdata','Orig','ref*_*.edf'));
    nref_orig=length(d);
    fprintf('    Found %d raw references out of %d expected\n',nref_orig,totref)
    if (nref_orig~=totref)
        problem_list{end+1}='missing raw reference images: possible gtCopyCorrectUndistortCondor problem';
    end
    d=dir(fullfile(parameters.acq.dir,'0_rawdata','Orig','dark*.edf'));
    if ~isempty(d)
        fprintf('    Found at least one dark images\n')
    else
        fprintf('    No raw dark image found\n')
        problem_list{end+1}='missing raw dark image: possible gtCopyCorrectUndistortCondor problem';
    end

    % Treated images
    d=dir(fullfile(parameters.acq.dir,'0_rawdata',parameters.acq.name,[parameters.acq.name '*.edf']));
    n_copy=length(d);
    fprintf('    Found %d copied images out of %d expected\n',n_copy,totproj+extra_proj)
    if (n_copy<totproj+extra_proj)
        problem_list{end+1}='missing copied images: possible gtCopyCorrectUndistortCondor problem';
    end

    d=dir(fullfile(parameters.acq.dir,'0_rawdata',parameters.acq.name,'ref*_*.edf'));
    nref_copy=length(d);
    fprintf('    Found %d copied references out of %d expected\n',nref_copy,totref)
    if (nref_copy~=totref)
        problem_list{end+1}='missing copied reference images: possible gtCopyCorrectUndistortCondor problem';
    end

    d=dir(fullfile(parameters.acq.dir,'0_rawdata',parameters.acq.name,'dark*.edf'));
    ndark_copy=length(d);
    if (ndark_copy>0)
        fprintf('    Found at least one copied dark images\n')
    end

    % was copying successful?
    if (n_copy==totproj+extra_proj && nref_copy==totref && ndark_copy>0)
        copy_ok=true;
    else
        copy_ok=false;
    end
    if (~copy_ok)
        problem_list{end+1}='possible problem during image copying';
    end

    % Preprocessed images
    disp('  Checking for preprocessed images...')
    d=dir(fullfile(parameters.acq.dir,'0_rawdata',parameters.acq.name,'refHST*.edf'));
    nrefHST=length(d);
    fprintf('    Found %d refHST images out of %d expected\n',nrefHST,totrefHST)
    if (nrefHST~=totrefHST)
        problem_list{end+1}='missing refHST images : possible gtSequenceMedianRefs problem';
    end

    d=dir(fullfile(parameters.acq.dir,'1_preprocessing/abs','abs*.edf'));
    nabs=length(d);
    if (parameters.acq.interlaced_turns > 0)
        d2 = dir(fullfile(parameters.acq.dir,'1_preprocessing/abs','abs_renumbered*.edf'));
        disp('    abs_renumbered images have already been created')
    else
        d2 = [];
    end
    fprintf('    Found %d abs images out of %d expected\n',nabs,totproj+length(d2))
    if (nabs~=totproj+length(d2))
        problem_list{end+1}='missing abs images : possible gtCreateAbsLive problem';
    end

    d=dir(fullfile(parameters.acq.dir,'1_preprocessing/abs','med*.edf'));
    nabsmed=length(d);
    fprintf('    Found %d abs median images out of %d expected\n',nabsmed,totabsmed)
    if (nabsmed~=totabsmed)
        problem_list{end+1}='missing abs median images : possible gtAbsMedianLive problem';
    end

    d=dir(fullfile(parameters.acq.dir,'1_preprocessing/full','med*.edf'));
    nfullmed=length(d);
    fprintf('    Found %d full median images out of %d expected\n',nfullmed,totfullmed)
    if (nfullmed~=totfullmed)
        problem_list{end+1}='missing full median images : possible gtMovingMedianLive problem';
    end
    % was preprocessing successful?
    if (nrefHST==totrefHST && nabs==totproj+length(d2) && nabsmed==totabsmed && nfullmed==totfullmed)
        preprocessing_ok=true;
    else
        preprocessing_ok=false;
    end
    if (~preprocessing_ok)
        problem_list{end+1}='possible problem during image preprocessing';
    end
end % if full images present

if (~preprocessing_ok)
    sfQuit(problem_list);
    return
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SEGMENTATION STATUS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
gtDBConnect();
disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
seg_ok = false;
if strcmpi(parameters.seg.method,'doublethr')
    fprintf('Segmentation method: Double threshold \n')

    n_fmv=mym(['select count(*) from ' parameters.acq.name 'fullmedianvals']);
    n_fmv_finished=mym(['select count(*) from ' parameters.acq.name 'fullmedianvals where !isnull(val)']);
    fprintf('  Segmentation completed %d images out of %d\n',n_fmv_finished,n_fmv)
    if (n_fmv~=n_fmv_finished)
        problem_list{end+1}='gtSeedSegmentation_doublethr has not finished';
    end

    % seed segmentation progress
    tot_seeds=mym(['select count(*) from ' parameters.acq.name 'seeds']);
    done_seeds=mym(['select count(*) from ' parameters.acq.name 'seeds_tmp']);
    if (done_seeds~=tot_seeds)
        problem_list{end+1}='gtSegmentDiffractionBlobs_doublethr has not finished';
    end
    fprintf('  Segmentation has treated %d out of %d seeds\n',done_seeds,tot_seeds)
    % was segmentation successful?
    if (n_fmv==n_fmv_finished && done_seeds==tot_seeds)
        seg_ok=true;
    else
        seg_ok=false;
    end
    if (~seg_ok)
        problem_list{end+1}='possible problem during image segmentation';
    end
    
elseif strcmpi(parameters.seg.method,'doublethr_new')
    fprintf('Segmentation method: Double threshold NEW GUI\n')

    % segmentation status
    n_fmv=mym(['select count(*) from ' parameters.acq.name 'fullmedianvals']);
    n_fmv_finished=mym(['select count(*) from ' parameters.acq.name 'fullmedianvals where !isnull(val)']);
    if (parameters.seg.writeblobs)
        fprintf('  Segmentation completed %d images out of %d\n',n_fmv_finished,n_fmv)
        if (n_fmv~=n_fmv_finished)
            problem_list{end+1}='gtSegmentationDoubleThreshold has not finished';
        end
        % was segmentation successful?
        if (n_fmv==n_fmv_finished)
            seg_ok=true;
        else
            seg_ok=false;
        end
        if (~seg_ok)
            problem_list{end+1}='possible problem during image segmentation';
        end
    else
        seg_ok = true;
    end
elseif strcmpi(parameters.seg.method,'singlethr')
    fprintf('Segmentation method: Single threshold \n')
else
    fprintf('Segmentation method not recognised! \n')
end
% blob segmentation progress
n_blobs=mym(['select count(*) from ' parameters.acq.name 'difblob']);
fprintf('  Segmentation has found %d blobs\n',n_blobs')
if (n_blobs<=1)
    problem_list{end+1}='possible problem during segmentation of blobs';
    seg_ok = false;
end
% spot segmentation progress
n_spots=mym(['select count(*) from ' parameters.acq.name 'difspot']);
fprintf('  Database has stored %d difspots from %d blobs\n',n_spots,n_blobs)
if (n_spots<=1)
    problem_list{end+1}='possible problem during segmentation of spots';
    seg_ok = false;
end
if (~seg_ok)
    sfQuit(problem_list);
    return
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PAIRMATCHING STATUS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
nphases = length(parameters.cryst);
disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
n_pairs = zeros(1,nphases);
match_ok = false(1,nphases);
for ii = 1 : nphases
    fprintf('Checking pair matching for phase %d:\n',ii)
    cmd=sprintf('select count(*) from %s where  phasetype=%d',parameters.acq.pair_tablename,ii);
    n_pairs(ii)=mym(cmd);
    if (n_pairs(ii)==0)
        pair_percentage=0;
        problem_list{end+1}=sprintf('possible problem during pair matching for phase %d',ii);
        match_ok(ii) = false;
    else
        pair_percentage=100*(2*n_pairs(ii))/n_spots;
        match_ok(ii) = true;
    end
    fprintf('  Database has %d spot pairs for phase %d from %d difspots (%0.1f%% matching)\n',...
        n_pairs(ii),ii,n_spots,pair_percentage)
    disp(' ')
end

if any(~match_ok)
    sfQuit(problem_list);
    return
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% INDEXING STATUS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% is there an input file?
disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
n_grains = zeros(1,nphases);
n_pairs_inp = zeros(1,nphases);
n_pairs_index = zeros(1,nphases);
index_ok = false(1,nphases);
for ii = 1 : nphases
    fprintf('Checking indexing for phase %d:\n',ii)
    d=dir(sprintf('4_grains/phase_%02d/index_input.mat',ii));
    if ~isempty(d)
        fprintf('  Indexing input file found for phase %02d:\n',ii)
        inp=load(sprintf('4_grains/phase_%02d/index_input.mat',ii));
        fprintf('    This file was created %s \n',d.date)
        n_pairs_inp(ii)=length(inp.tot.pairid);
        if (n_pairs_inp(ii)~=n_pairs(ii))
            fprintf('    !!! Strong warning !!! \n')
            fprintf('    Number of pairs in the indexing input file does not match the spotpair table\n')
            problem_list{end+1}='If you have update the pair matching, un-check "use input file" in gtSetupIndexing';
            problem_list{end+1}=sprintf('possible problem during indexing for phase %d',ii);
            index_ok(ii) = false;
        else
            fprintf('    Indexing input looks consistant with the spot pairs table\n')
            index_ok(ii) = true;
        end

        % is there an output file?
        d2=dir(sprintf('4_grains/phase_%02d/index.mat',ii));
        if ~isempty(d2)
            fprintf('  Indexing results found for phase %02d:\n',ii)
            fprintf('    Latest output file date: %s\n',d2.date)
            a=load(sprintf('4_grains/phase_%02d/index.mat',ii));
            n_grains(ii)=length(a.grain);
            fprintf('    Latest output contains %d indexed grains\n',n_grains(ii))
            n_pairs_index(ii) = sum(gtIndexAllGrainValues(a.grain,'nof_pairs'));
            fprintf('    Latest output contains %d indexed pairs in total\n',n_pairs_index(ii))
            if (d.datenum>d2.datenum)
                fprintf('    !!! Strong warning !!! \n')
                fprintf('    Input file is newer than the output file! Indexing has started but not finished\n')
            end
        else
            fprintf('  No indexing output found for phase %02d\n',ii)
            index_ok(ii) = false;
        end
    else
        fprintf('  No indexing input file for phase %d \n',ii)
        index_ok(ii) = false;
    end
    disp(' ')
end

if any(~index_ok)
    sfQuit(problem_list);
    return
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% FORWARD SIMULATION STATUS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
n_grains_mat = zeros(1,nphases);
fsim_ok = true(1,nphases);
sample=[];
if exist('4_grains/sample.mat','file')
    load('4_grains/sample.mat');
else
    fprintf('No 4_grains/sample.mat file found\n')
    return
end
grains_conflicts = [];
if exist('4_grains/grains_conflicts.mat','file')
    load('4_grains/grains_conflicts.mat');
    conflicts_ok = true(1,nphases);
else
    conflicts_ok = false(1,nphases);
end
disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
for ii = 1 : nphases
    pxsize = (parameters.labgeo.pixelsizeu+parameters.labgeo.pixelsizev)/2;
    dd=dir(sprintf('4_grains/phase_%02d/grain_*.mat',ii));
    d={};
    for jj=1:length(dd)
        if length(strfind(dd(jj).name, 'details'))<1
            d{end+1}=dd(jj).name;
        end
    end
    n_grains_mat(ii)=length(d);
    if (n_grains(ii)==n_grains_mat(ii))
        fprintf('Correct number (%d) of grain_%%04d.mat files found\n',n_grains_mat(ii));
    elseif (n_grains_mat(ii)==0)
        fprintf('No grain_%%04d.mat file found\n')
    else
        fprintf('Some grain_%%04d.mat files missing!\n')
    end
    if (conflicts_ok(ii)) &&  n_grains_mat(ii) ~= length(grains_conflicts{ii}.grain)
        fprintf('Grains_conflicts.mat exists !\n')
        fprintf('  Wrong size for grains_conflicts for phase %d\n',ii)
        conflicts_ok(ii) = false;
    end
    phase = sample.phases{ii};
    check = false(4,length(d));
    title = sprintf('Checking grain_####.mat files for phase %02d: ',ii);
    gauge = GtGauge(n_grains(ii),title);
    for jj = 1 : length(d)
        %b = load(sprintf('4_grains/phase_%02d/grain_%04d.mat',ii,jj),'center','completeness','difspotID','selected');
        b = load(sprintf('4_grains/phase_%02d/grain_%04d.mat',ii,jj),'center','completeness','difspotID');
        if all( single(b.center*pxsize) == single(phase.center(jj,:)*pxsize) )
            check(1,jj) = true;
        end
        if (b.completeness == phase.completeness(jj) )
            check(2,jj) = true;
        end
%         if all(ismember(b.selected, phase.grains{jj}.selectedDiffspots))
            check(3,jj) = true;
%         end
        if (conflicts_ok(ii))
            if all(ismember(b.difspotID',grains_conflicts{ii}.analyser{jj}.difspotID))
                check(4,jj) = true;
            end
        end
        n_spots_fsim(jj) = length(b.difspotID);
        gauge.incrementAndDisplay();
    end
    gauge.delete();
    if any(~check(1,:))
        problem_list{end+1}='Sample.mat and grain_####.mat have different center values for grains: ';
        problem_list{end+1}=sprintf('%d ',find(~check(1,:)));
    end
    if any(~check(2,:))
        problem_list{end+1}='Sample.mat and grain_####.mat have different completeness values for grains: ';
        problem_list{end+1}=sprintf('%d ',find(~check(2,:)));
    end
%     if any(~check(3,:))
%         problem_list{end+1}='Sample.mat and grain_####.mat have different selectedDiffspots values for grains: ';
%         problem_list{end+1}=sprintf('%d ',find(~check(3,:)));
%     end
    if any(~check(4,:))
        problem_list{end+1}='grains_conflicts.mat and grain_####.mat have different difspotID values for grains: ';
        problem_list{end+1}=sprintf('%d ',find(~check(4,:)));
    end
    if any(~check)
        fsim_ok(ii) = false;
    end
    if (~conflicts_ok(ii) && exist('4_grains/grains_conflicts.mat','file'))
        problem_list{end+1}=sprintf('grains_conflicts.mat has different length wrt the number of indexed grains: %d vs %d',...
            length(grains_conflicts{ii}.grain),n_grains_mat(ii));
        problem_list{end+1}='  Suggestion: run again gtAnalyseGrainsConflicts(true,true)';
        fsim_ok(ii) = false;
    end
    fprintf('  Total number of fsim spots is %d\n',sum(n_spots_fsim(:)))
    fprintf('  Average number of fsim spots per grain is %0.1f\n',mean(n_spots_fsim(:)))
    fprintf('  Min (max) number of fsim spots per grain is %d (%d)\n',min(n_spots_fsim(:)),max(n_spots_fsim(:)))
    disp(' ')
end

%if any(~fsim_ok)
%    sfQuit(problem_list);
%    return
%end

for ii = 1 : nphases
    fprintf('Check forward simulation using gtFsimCheckMissingGrains for phase %02d:\n',ii)
    list = gtFsimCheckMissingGrains(ii);
    if isempty(list.ind) && isempty(list.old)
        fprintf('  Forward simulation seems ok\n')
    elseif ~isempty(list.ind)
        fprintf('  Forward simulation failed for %d grains:\n',length(list.ind))
        fprintf('  grain_####.mat does not exist for grains:\n')
        disp(list.ind)
    elseif ~isempty(list.old)
        fprintf('  Forward simulation has to be checked for %d grains:\n',length(list.old))
        fprintf('  grain_####.mat is old for grains:\n')
        disp(list.old)
    end
    if ~isempty(list.checkFsim)
        fprintf('  Wrong grainid for %d grains from Forward Simulation:\n',length(list.checkFsim))
        fprintf('  grain_####.mat has wrong grainid for grains:\n')
        disp(list.checkFsim)
    end
    if ~isempty(list.checkIndexter)
        fprintf('  Wrong grainid for %d grains from Indexter:\n',length(list.checkIndexter))
        fprintf('  index.mat:grain has wrong grainid for grains:\n')
        disp(list.checkIndexter)
    end
    disp(' ')
end % end for phaseid

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% RECONSTRUCTION STATUS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

gtCheckAllReconstructedGrains;

% for ii = 1 : nphases
%     title = sprintf('Check grain reconstructions for phase %02d: ',ii);
%     gauge = GtGauge(n_grains(ii),title);
%     check = true(n_grains(ii),3);
%     warning('off','all') % hide warning messages
%     d=dir(sprintf('4_grains/phase_%02d/grain_details_*.mat',ii));
%     for jj = 1 : n_grains(ii)
%         gauge.incrementAndDisplay();
%         b=load(sprintf('4_grains/phase_%02d/%s',ii,d(jj).name),'VOL3D','SEG','ODF6D');
%         if ~isfield(b,'VOL3D') || isempty(b.VOL3D) || all(isequal(size(b.VOL3D.intensity), [1 1]))
%             check(jj,1)=false;
%         end
%         if ~isfield(b,'SEG') || isempty(b.SEG) || all(isequal(size(b.SEG.seg), [1 1]))
%             check(jj,2)=false;
%         end
%         if ~isfield(b,'ODF6D') || isempty(b.ODF6D) || all(isequal(size(b.ODF6D.intensity), [1 1]))
%             check(jj,3)=false;
%         end
%     end
%     gauge.delete();
%     fprintf('  Reconstructed VOL3D %d grains out of %d\n',sum(check(:,1)),n_grains(ii))
%     fprintf('  Reconstructed ODF6D %d grains out of %d\n',sum(check(:,1)),n_grains(ii))
%     fprintf('  Segmented %d grains out of %d\n',sum(check(:,2)),n_grains(ii))
%     if any(~check(:,1))
%         disp('  Missing grain_####.mat:VOL3D for ID(s):')
%         disp(find(~check(:,1))')
%         if length(find(~check(:,1))) == n_grains(ii)
%             problem_list{end+1}='Maybe reconstruction was not run yet...';
%         end
%     end
%     if any(~check(:,3))
%         disp('  Missing grain_####.mat:ODF6D for ID(s):')
%         disp(find(~check(:,3))')
%         if length(find(~check(:,1))) == n_grains(ii)
%             problem_list{end+1}='Maybe reconstruction was not run yet...';
%         end
%     end
%     if any(~check(:,2))
%         disp('  Missing grain_####.mat:SEG for ID(s):')
%         disp(find(~check(:,2))')
%         if length(find(~check(:,2))) == n_grains(ii)
%             problem_list{end+1}='Maybe grain segmentation was not run yet...';
%         end
%     end
%     if any(~check(:,[1 2 3]))
%         problem_list{end+1}='  Suggestion: run again gtSetupReconstruction() on a GPU machine';
%         rec_ok(ii) = false;
%     else
%         rec_ok(ii) = true;
%     end
%     
%     % dates on these files
%     filedates=zeros(1,length(d));
%     for jj = 1 : length(d)
%         filedates(jj)=d(jj).datenum;
%     end
%     [~,min_ndx]=min(filedates);
%     [~,max_ndx]=max(filedates);
%     fprintf('  Earliest grain.mat file dates from %s\n',d(min_ndx).date)
%     fprintf('  Latest grain.mat file dates from %s\n',d(max_ndx).date)
% 
%     warning('on','all') % hide warning messages
% end
disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')

%if any(~rec_ok)
%    sfQuit(problem_list);
%    return
%end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ASSEMBLED VOLUMES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fprintf('Check reconstructed/assembled volumes:\n')

d=dir('5_reconstruction/volume_absorption.mat');
if ~isempty(d)
    fprintf('  Found absorption reconstruction: volume_absorption.mat\n')
    fprintf('    File date is %s\n',d.date)
else
    fprintf('  No absorption reconstruction found\n')
end
d=dir('5_reconstruction/volume_mask.mat');
if ~isempty(d)
    fprintf('  Found absorption mask: volume_mask.mat\n')
    fprintf('    File date is %s\n',d.date)
else
    fprintf('  No absorption mask found\n')
end
for ii = 1 : nphases
    d=dir(sprintf('5_reconstruction/phase_%02d_vol.mat',ii));
    if ~isempty(d)
        fprintf('  Found DCT reconstruction for phase %d: phase_%02d_vol.mat\n',ii,ii)
        fprintf('    File date is %s\n',d.date)
    else
        fprintf('  No DCT reconstruction found for phase %d\n',ii)
    end
end
d=dir('5_reconstruction/volume.mat');
if ~isempty(d)
    fprintf('  Found DCT reconstruction: volume.mat\n')
    fprintf('    File date is %s\n',d.date)
else
    fprintf('  No DCT reconstruction found\n')
end
d=dir('5_reconstruction/volume_dilated.mat');
if ~isempty(d)
    fprintf('  Found postprocessed DCT reconstruction: volume_dilated.mat\n')
    fprintf('    File date is %s\n',d.date)
else
    fprintf('  No postprocessed DCT reconstruction found\n')
end
disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
disp(' ')
disp(' ')

sfQuit(problem_list);

end % end of gtStatus

%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% sub-functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function sfQuit(problem_list)
    disp(' ')
    if length(problem_list) > 1
        for ii=1:length(problem_list)
            fprintf('%s\n',problem_list{ii})
        end
    end
end


function [reconstructed] = gtReconstructedGrains(algo, phase)

	if (~exist('algo', 'var'))
        	algo = 'ODF6D';
    	end
	if (~exist('phase', 'var'))
        	phase = 1;
    	end

	if (~strcmp(algo,'ODF6D') && ~strcmp(algo,'VOL3D') && ~strcmp(algo,'SEG'))
        	algo = 'ODF6D';
	end
	
	sample = GtSample.loadFromFile();
	num_grains = sample.phases{1}.getNumberOfGrains();

	reconstructed = false(num_grains, 1);
    
    title = sprintf(' - check: phase %02d, reconstructions (% 5s) -> ', phase, algo);
    gauge = GtGauge(num_grains, title);

	for ii = 1:num_grains
        gauge.incrementAndDisplay();
	    fid = matfile(sprintf('4_grains/phase_%02d/grain_details_%04d.mat', phase, ii));
	    reconstructed(ii) = length( who(fid, algo));
    end
    gauge.delete();
end


function gtCheckAllReconstructedGrains(wd)

	currentDir = pwd;

	if (exist('wd', 'var'))
        	cd(wd)
    	end

	pluriel = {'s', ''};

	sample = GtSample.loadFromFile();
	parameters = gtLoadParameters();
	
	ech_name = parameters.acq.name;
	num_phases = length(sample.phases);
	for i=1:num_phases
		phases_names{i} = sample.phases{i}.phaseName;
		phases_nbgrains{i} = sample.phases{i}.getNumberOfGrains();
	end
	
	fprintf('DCT sample %s ', ech_name)
	fprintf('(%d phase%s)\n', num_phases, pluriel{2-(num_phases>1)});
	for i=1:num_phases
		phases_names{i} = sample.phases{i}.phaseName;
		phases_nbgrains{i} = sample.phases{i}.getNumberOfGrains();
		phases_reconstructed_VOL3D{i} = sum(gtReconstructedGrains('VOL3D', i));
		phases_reconstructed_ODF6D{i} = sum(gtReconstructedGrains('ODF6D', i));
		phases_reconstructed_SEG{i} = sum(gtReconstructedGrains('SEG', i));

		fprintf('    * %s : %d grains \n', phases_names{i}, phases_nbgrains{i})
		fprintf('        reconstruction\n')
		fprintf('           3D  % 4d/% 4d grains \n', phases_reconstructed_VOL3D{i}, phases_nbgrains{i})
		fprintf('           6D  % 4d/% 4d grains \n', phases_reconstructed_ODF6D{i}, phases_nbgrains{i})
		fprintf('           SEG % 4d/% 4d grains \n', phases_reconstructed_SEG{i}, phases_nbgrains{i})


        if any(~phases_reconstructed_VOL3D{i})
            disp('  Missing grain_####.mat:VOL3D for ID(s):')
            disp(find(~phases_reconstructed_VOL3D{i})')
            if length(find(~phases_reconstructed_VOL3D{i})) == phase_nbgrains{i}
                disp('Maybe reconstruction was not run yet...');
            end
        end
        if any(~phases_reconstructed_ODF6D{i})
            disp('  Missing grain_####.mat:ODF6D for ID(s):')
            disp(find(~phases_reconstructed_ODF6D{i})')
            if length(find(~phases_reconstructed_ODF6D{i})) == phase_nbgrains{i}
                disp('Maybe reconstruction was not run yet...');
            end
        end
        if any(~phases_reconstructed_SEG{i})
            disp('  Missing grain_####.mat:SEG for ID(s):')
            disp(find(~phases_reconstructed_SEG{i})')
            if length(find(~phases_reconstructed_SEG{i})) == phase_nbgrains{i}
                disp('Maybe reconstruction was not run yet...');
            end
        end

	end

	cd(currentDir);
	
end	