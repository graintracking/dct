function infopc()
% INFOPC display info of the ssh connection

    [~, hostname] = system('hostname');
    hostname = strtrim(hostname);

    disp('******************************************************');
    fprintf(' %s = %s\n', '  matlab', version);
    fprintf(' %s = %s\n', 'hostname', hostname);
    fprintf(' %s = %s\n', 'username', getenv('USER'));
    fprintf(' %s = %s\n', '     pwd', strtrim(pwd));
    disp('******************************************************');
end % end of function

