function [ output_args ] = gtResetAnalysis( datasetname )
% GTRESETANALYSIS  Resets the analysis directory leaving only the data
%   gtResetAnalysis()
%   -----------------

disp('You are in the directory: ')
disp(pwd)

output = dir(pwd());
currentDir = pwd();

tmp = gtGetLastDirName(pwd());
if ~exist('datasetname','var') || isempty(datasetname)
  datasetname = tmp;
end
if ~strcmpi(tmp, datasetname)
  disp('Error : you are trying to reset a directory which is not consistent with the dataset name you have given!')
  return;
end

listDirs = {...
  fullfile(currentDir, '0_rawdata', datasetname),...
  fullfile(currentDir, '1_preprocessing'), ...
  fullfile(currentDir, '2_difspot'),...
  fullfile(currentDir, '3_pairmatching'),...
  fullfile(currentDir, '4_grains'),...
  fullfile(currentDir, '5_reconstruction'),...
  fullfile(currentDir, '6_rendering'),...
  fullfile(currentDir, '7_fed'),...
  fullfile(currentDir, '8_optimization'),...
  fullfile(currentDir, 'OAR_log')};

check = inputwdefault('Are you sure to reset this directory? [y/n]','y');
if strcmp(check,'y')
  % in 0_rawdata/Orig
  if exist('parameters.mat','file')
    copyfile('parameters.mat', fullfile(currentDir, '0_rawdata','Orig',['parameters_' date() '.mat']) );
    disp('Parameters.mat has been copied to ')
    disp( fullfile('0_rawdata','Orig',['parameters_' date() '.mat']) )
  end
  if exist( fullfile(currentDir, '0_rawdata', 'Orig', 'list.mat'), 'file')
    delete( fullfile(currentDir, '0_rawdata', 'Orig', 'list.mat') );
    disp('Deleted list.mat file')
  end
  if exist( fullfile(currentDir, '0_rawdata', 'Orig', [datasetname 'dct_.xml']), 'file')
    delete( fullfile(currentDir, '0_rawdata', 'Orig', [datasetname 'dct_.xml']) );
    disp(['Deleted ' datasetname 'dct_.xml' ' file'])
  end
  
  % in the analysis directory
  for ii=1:length(listDirs)
    if exist(listDirs{ii},'dir')
      fprintf(['Directory ' listDirs{ii} ' is going to be deleted...'])
      rmdir(listDirs{ii},'s')
      disp('...done!')
    end
  end
  delete( fullfile(currentDir, '*.*') )
  disp('All the files have been deleted')
  disp(' ')
  disp('The directory has been resetted')
end % end if

if nargout == 1
  output_args = output;
end

end % end of function
