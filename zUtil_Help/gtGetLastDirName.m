function name = gtGetLastDirName(path)

str = regexp(path,filesep(),'split');
bad = true;
while bad
    if strcmp(str{end},'Orig') || strcmp(str{end},'0_rawdata')
        str(end)=[];
        bad = true;
    else
        bad = false;
    end
end
name = str{end};

end