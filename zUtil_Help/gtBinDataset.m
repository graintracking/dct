function gtBinDataset(oldDir, oldName, newDir, newName, binSliceNumber, binOmegaNumber)
% GTBINDATASET  Compress a datset by binning process (pixel size + omega)
%
%    gtBinDataset(oldDir, oldName, newDir, newName, binSliceNumber, binOmegaNumber)
%    --------------------------------------------------------------------------
%
%    INPUT:
%      oldDir         = <string>  Directory where the old dataset xml file is located
%      oldName        = <string>  Old dataset name
%      newDir         = <string>  Directory where the dataset will be located
%      newName        = <string>  New dataset name
%      binSliceNumber = <int>     Binning factor for slice images
%                                 (e.g with 2, 2048x2048 -> 1024x1024)
%      binOmegaNumber = <int>     Binning factor for omega
%                                 (e.g with 4, 7200 slices -> 1800 slices)
%
%    Version 001 by YGuilhem, yoann.guilhem@esrf.fr

%% Check input parameters
if (~exist('binSliceNumber', 'var') || isempty(binSliceNumber))
    binSliceNumber = 1;
elseif (mod(binSliceNumber, 2))
    gtError('gtBinDataset:wrong_input', ...
        'Parameter binSliceNumber should be even!');
end
if (~exist('binOmegaNumber', 'var') || isempty(binOmegaNumber))
    binOmegaNumber = 1;
elseif (mod(binOmegaNumber, 2))
    gtError('gtBinDataset:wrong_input', ...
        'Parameter binOmegaNumber should be even!');
end

% Create new directory
if (~exist(newDir, 'dir'))
    [~, msg] = mkdir(newDir);
    disp(msg);
end

%% XML file

% Read xml file
try
    xmlold = fullfile(oldDir, [oldName '.xml']);
catch Mexc
    gtPrintException(Mexc, 'Dataset xml file does not exist!');
end
xml = xml_read(xmlold);

% Store old parameters
oldPixelSize = xml.acquisition.pixelSize;
oldDim1 = xml.acquisition.projectionSize.DIM_1;
oldDim2 = xml.acquisition.projectionSize.DIM_2;
%oldRowEnd = xml.acquisition.projectionSize.row_end;
%oldColEnd = xml.acquisition.projectionSize.col_end;
oldTomoN = xml.acquisition.tomo_N;
oldRefOn = xml.acquisition.ref_On;

% Set new parameters
newPixelSize = oldPixelSize * binSliceNumber;
newDim1 = oldDim1 / binSliceNumber;
newDim2 = oldDim2 / binSliceNumber;
newRowEnd = newDim1 - 1; % Have to check whether it is DIM1 or DIM2
newColEnd = newDim2 - 1; % Have to check whether it is DIM1 or DIM2
newTomoN = oldTomoN / binOmegaNumber;
newRefOn = oldRefOn / binOmegaNumber;

% Rectify relevant fields
xml.acquisition.scanName = newName;
xml.acquisition.disk = newDir;
xml.acquisition.pixelSize = newPixelSize;
xml.acquisition.projectionSize.DIM_1 = newDim1;
xml.acquisition.projectionSize.DIM_2 = newDim2;
xml.acquisition.projectionSize.ROW_END = newRowEnd;
xml.acquisition.projectionSize.COL_END = newColEnd;
xml.acquisition.tomo_N = newTomoN;
xml.acquisition.ref_On = newRefOn;

% Write new xml file
xmlnew = fullfile(newDir, [newName '.xml']);
xml_write(xmlnew, xml);

%% Dark images
darkImageFileNames = {'dark.edf', 'darkend0000.edf'};
gauge = GtGauge(numel(darkImageFileNames), ...
    'Writing new dark images: ');
for ii = 1:numel(darkImageFileNames)
    gauge.incrementAndDisplay();
    darkImageFileName = darkImageFileNames{ii};
    oldFile = fullfile(oldDir, darkImageFileName);
    newFile = fullfile(newDir, darkImageFileName);
    if ~exist(oldFile, 'file')
        warning('gtBinDataset:missing_dark_file', ...
            ['Dark file ' oldFile ' could not be found!']);
        continue;
    end

    % Write out new dark image
    newFile = fullfile(newDir, darkImageFileName);
    sfResizeAndWriteSingleImage(oldFile, newFile, binSliceNumber, []);
end
gauge.delete();

%% Take care of interlaced scan dataset
filename = fullfile(oldDir, sprintf('%s1_%04d.edf', oldName, 0));
if exist(filename,'file')
    interlaced = 1;
    listing    = dir(sprintf('%s/%s1_*.edf',oldDir, oldName));
    newImageFileNameRoot = fullfile(newDir, sprintf('%s1_', newName));
else
    interlaced = 0;
    listing    = dir(sprintf('%s/%s*.edf',oldDir, oldName));
    newImageFileNameRoot = fullfile(newDir, sprintf('%s%04d.edf', newName));
end




%% Reference files

suffixes = {'.edf','_1.edf'};

for ii = 0 : interlaced
    
    suffix = suffixes{ii+1};
    
    numberOfReferenceImagesPerGroup = floor(newTomoN/newRefOn + 1);
    if (numberOfReferenceImagesPerGroup ~= floor(oldTomoN/oldRefOn + 1))
        gtError('gtBinDataSet:wrong_ref_number', ...
            'Problem with computing of the number of reference images!');
    end

    gauge = GtGauge(numberOfReferenceImagesPerGroup*xml.acquisition.ref_N, ...
        'Writing new reference images: ');
    for nref = 1:xml.acquisition.ref_N
        oldPrefix = fullfile(oldDir, sprintf('ref%04d', nref - 1));
        newPrefix = fullfile(newDir, sprintf('ref%04d', nref - 1));
        for index = 1:numberOfReferenceImagesPerGroup
            gauge.incrementAndDisplay();

            oldFile = [oldPrefix sprintf('_%04d%s', (index - 1) * oldRefOn), suffix];
            newFile = [newPrefix sprintf('_%04d%s', (index - 1) * newRefOn), suffix];
            if exist(oldFile, 'file')
                newSuffix = sprintf('_%04d%s', (index - 1) * newRefOn, suffix);
                sfResizeAndWriteSingleImage(oldFile, newFile, binSliceNumber, newSuffix);
            else
                sprintf('gtBinDataset:  Reference file %s is missing', oldFile);
            end

        end
    end
    gauge.delete();
end
    
    
    
% Still need to take care of monochronator tuning reference images
% Not obvious to do without any parameters stored in the xml file


%% Write files at end of scan
jj = 0;
for ii = oldTomoN : length(listing)-1
    oldImageFileName = fullfile(oldDir, listing(ii+1).name);
    newImageFileName = fullfile(sprintf('%s%04d.edf', newImageFileNameRoot, newTomoN+jj))
    sfResizeAndWriteSingleImage(oldImageFileName, newImageFileName, binSliceNumber, []);
    jj=jj+1;
end



%% Write Image files
% Force data type to be uint16
datatype = 'uint16';

% Allocate stack for images
stack = zeros([oldDim1, oldDim2, binOmegaNumber], 'uint16');


oldNameOrig = oldName;
newNameOrig = newName;

for interlaced_turn = 0 : interlaced
    
    gauge = GtGauge(newTomoN, 'Writing new images: ');
    
    if interlaced
        parts = {'first', 'second'};
        sprintf('Treating %s turn of interlaced scan', parts{interlaced_turn+1})
        oldName = sprintf('%s%d_', oldNameOrig, interlaced_turn);
        newName = sprintf('%s%d_', newNameOrig, interlaced_turn);   
    end

    for index = 0:newTomoN-1
        gauge.incrementAndDisplay();
        offset = index * binOmegaNumber;

        % Read binOmegaNumber images
        [stack(:, :, 1), info] = edf_read(fullfile(oldDir, sprintf('%s%04d.edf', oldName,  offset)));
        for ii = 2:binOmegaNumber
            stack(:, :, ii) = edf_read(fullfile(oldDir, sprintf('%s%04d.edf', oldName,  offset + ii - 1)));
        end

        % Sum images into a single one
        newImage = sum(stack, 3) / binOmegaNumber;

        % Bin resulting image
        newImage = cast(imresize(newImage, [newDim1 newDim2]), datatype);
        %newImage = sfBinImage(newImage, binSliceNumber);

        % Rectify info
        info = rmfield(info, {'dim_1', 'dim_2', 'dim_3'});
        info.col_end = newColEnd;
        info.row_end = newRowEnd;
        info.col_bin = binSliceNumber;
        info.row_bin = binSliceNumber;
        info.dir = newDir;
        info.prefix = newName;

        % Write out new image
        newImageName = fullfile(newDir, sprintf('%s%04d.edf', newName, index));
        edf_write(newImage, newImageName, info, true);
    end
    gauge.delete();
end



function sfResizeAndWriteSingleImage(oldImageFileName, newImageFileName, factor, suffix)
    % Read old image edf file
    [oldImg, inf] = edf_read(oldImageFileName);

    % Resize image
    newDim = [inf.dim_1 inf.dim_2] / factor;
    newImg = imresize(oldImg, newDim);
    if ~ismember(inf.datatype, {'float32', 'float64'})
        newImg = cast(newImg, inf.datatype);
    end

    % Rectify reference image info
    inf = rmfield(inf, {'dim_1', 'dim_2', 'dim_3'});
    inf.col_end = newDim(1) - 1;
    inf.row_end = newDim(2) - 1;
    inf.col_bin = factor;
    inf.row_bin = factor;
    inf.dir = java.io.File(newImageFileName).getParent();
    if exist('suffix', 'var') && ~isempty(suffix)
        inf.suffix = suffix;
    end

    % Write out new image
    edf_write(newImg, newImageFileName, inf, true);
end

%function im = sfBinImage(im, binFactor)
%    [m, n] = size(im);
%    [M, N] = size(im) / binFactor;
%    im = sum(reshape(im, binFactor, []), 1);
%    im = reshape(im), M, []).';
%    im = sum(reshape(im, binFactor, []), 1);
%    im = reshape(im, N, []).';
%    im = im / binFactor / binFactor;
%end

end %end of function
