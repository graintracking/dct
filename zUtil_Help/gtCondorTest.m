function gtCondorTest(first, last, workingdirectory)

if ~exist('workingdirectory','var')
    workingdirectory=pwd;
end

cd(workingdirectory);

if isdeployed
    global GT_DB
    global GT_MATLAB_HOME
    load('workspaceGlobal.mat');
    first=str2double(first);
    last=str2double(last);
end


fname=sprintf('gtCondorTestOutput_%d_%d.txt', first, last);
fid=fopen(fname, 'w')

for i=first:last
   
    disp('gtCondorTest...')
    i
    pwd
    
    fprintf(fid, 'gtCondorTest\n')
    fprintf(fid, '%d\n', i)
    pwd_string=pwd;
    fprintf(fid, '%s\n', pwd_string); 
    
    
end


disp('Thankyou... goodbye')
fprintf(fid, 'Thankyou... goodbye')
fclose(fid)



