function info = gtVersion()
% GTVERSION  Gives DCT version used with git information
%     -------------------------------------------------------------------------
%     info = gtVersion()
%
%     OUTPUT:
%       info  = <struct> GIT infos about the DCT code currently used
%
%     Version 002 31-01-2013 by YGuilhem
%       Now using git
%
%     Version 001 22-08-2012 by YGuilhem

% Initalise values
gitInfo = struct();

% Get GT_MATLAB_HOME (which is set in initialise_gt) variable and display it
global GT_MATLAB_HOME;

% Guess git branch
[status, result] = system(['cd ' GT_MATLAB_HOME '; git branch --no-color']);
if status
    warning('gtVersion:git_branch', 'Couldn''t get git branch!');
else
    tmp = strrep(regexp(result, '* \w+', 'match'), '* ', '');
    gitInfo.LocalBranch = tmp{1};
    disp(['GIT local branch:     ' gitInfo.LocalBranch]);
end
gitInfo.LocalRepository = GT_MATLAB_HOME;
disp(['GIT local repository: ' GT_MATLAB_HOME]);

% Guess git bare repository
[status, result] = system(['cd ' GT_MATLAB_HOME ...
    '; git config --get remote.origin.url']);
if status
    warning('gtVersion:git_repo', 'Couldn''t get git repository!');
else
    gitInfo.BareRepository = strtrim(result);
    disp(['GIT bare repository:  ' gitInfo.BareRepository]);
end

% Guess git info related to GT_MATLAB_HOME (DCT_DIR)
%[status, result] = system(['svn info --non-interactive ' GT_MATLAB_HOME]);
gitFMT = ['"' ...
    'Commit: %H%n' ...
    'Author: %an%n' ...
    'AuthorEmail: %ae%n' ...
    'AuthorDate: %ai%n'...
    'Committer: %cn%n' ...
    'CommitterEmail: %ce%n' ...
    'CommitterDate: %ci' ...
    '"'];
gitCmd = ['git log --max-count=1 --pretty=format:' gitFMT];
[status, result] = system(['cd ' GT_MATLAB_HOME '; ' gitCmd]);

% Set back LANG env variable
%if ~isempty(lang)
%    setenv('LANG',lang);
%end

if status
    warning('gtVersion:git_problem', 'Couldn''t get git info!');
else
    % Parse output from svn info 
    res = regexp(result, '(?<par>[\w ]+): (?<value>[\S ]+)[\n\r\f]', 'names');
    if ~isempty(res)
        for ii=1:length(res)
            field = strrep(res(ii).par, ' ', '_');
            gitInfo.(field) = res(ii).value;
        end
    else
        warning('gtVersion:git_info', 'Couldn''t get git info!');
    end
    if nargout
        info = gitInfo;
    end
end % end of function
