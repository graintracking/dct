function [Ccell,Cmat] = gtReadTextFile(filename, formatS, minsize, iscell2mat, varargin)
% GTREADTEXTFILE  Reads textfiles into matrices
%
%     [Ccell, Cmat] = gtReadTextFile(filename, formatS, minsize, iscell2mat, varargin)
%     ------------------------------------------------=-------------------------------
%
%     INPUT:
%       filename  = text file to be read <string>
%       formatS   = format pattern as in fprintf <string> i.e. '%f %f %f'
%       minsize   = minimum size of element to be consider as object (like
%                   matrix 3x3 or vector 1x3)
%       iscell2mat = transform to matrix <logical>
%       varargin  = optional argument for textscan
%
%     OUTPUT:
%       Ccell = content of the text file (read as columns) <cell> 
%       Cmat  = read matrix <double>
%
%     Example:
%       [Ccell,Cmat] = gtReadTextFile('reflections_all.txt','%f %f %f %f',[1 3],'Delimiter','\n','CommentStyle','#');
%
%     Version 001 03-12-2012 by LNervo

if ~exist('iscell2mat','var') || isempty(iscell2mat)
    iscell2mat = false;
end

fid = fopen(filename,'r');
C = textscan(fid, formatS, varargin{:});
fclose(fid);

if iscell2mat
    Cmat = cell2mat(C);

    if minsize(1)>0 && minsize(2)>0
        for ii = 1 : size(Cmat,1)/minsize(1)
            C{ii} = Cmat((ii-1)*minsize(1)+1:minsize(1)*ii,1:minsize(2));
        end
    end
else
    Cmat = [];
end

Ccell = C;

end % end of function
