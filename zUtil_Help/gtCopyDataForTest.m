function gtCopyDataForTest(old_pattern, new_pattern, workdir, newdir, alreadySetup)
% GTCOPYDATAFORTEST  Copies data folder to be able to test part of the code
%
%     gtCopyDataForTest(old_pattern, new_pattern, workdir, newdir, [alreadySetup])
%     ----------------------------------------------------------------------------
%       Copy ONLY the 0_rawdata/Orig folder with images if directory exists, if not only the images.
%       Copies also the xml file and rename images.
%       The old data is in:      /workdir/old_pattern/[0_rawdata/Orig/]
%       The new data will be in: /newdir/new_pattern/[0_rawdata/Orig/]
%
%     INPUT: 
%       old_pattern  = <string>     old dataset name
%       new_pattern  = <string>     new dataset name
%       workdir      = <string>     path to where the old dataset directory is located
%       newdir       = <string>     path to where the new dataset directory will be
%       alreadySetup = <logical>    true, if gtSetup has been run at least once
%
%
%     Version 003 03-09-2013 by LNervo
%
%     Version 002 20-09-2012 by YGuilhem
%       Now using GT_MATLAB_HOME to determine path to tomo_utils.py


if ~exist(fullfile(workdir, old_pattern), 'dir')
    gtError('gtCopyDataForTest:wrong_directory', ...
        'Old dataset directory doesn''t exist');
end
if ~exist('alreadySetup','var') || isempty(alreadySetup)
    if exist(fullfile(workdir, old_pattern, '0_rawdata'), 'dir')
        alreadySetup = true;
    else
        alreadySetup = false;
    end
end
if ~exist('newdir', 'var') || isempty(newdir)
    newdir = workdir;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Directories
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp('Creating the new dataset directories...');
if exist(fullfile(newdir, new_pattern), 'dir')
    gtError('gtCopyDataForTest:wrong_directory', ...
        'New directory already exists. Please check the directory name.');
elseif ~exist(newdir, 'dir')
    [~, msg] = mkdir(newdir); disp(msg);
end

[~, msg] = mkdir(fullfile(newdir, new_pattern)); disp(msg);

if (alreadySetup)
    [~, msg] = mkdir(fullfile(newdir, new_pattern, '0_rawdata')); disp(msg);
    [~, msg] = mkdir(fullfile(newdir, new_pattern, '0_rawdata', 'Orig')); disp(msg);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Images
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Copying data
disp('Copying and the images to the new directory...');
if (alreadySetup)
    copyfile(fullfile(workdir, old_pattern, '0_rawdata', 'Orig', '*.edf'), ...
         fullfile(newdir,  new_pattern, '0_rawdata', 'Orig'), 'f');

    disp('Copying the info file...');
    copyfile(fullfile(workdir, old_pattern, '0_rawdata', 'Orig', '*.info'), ...
         fullfile(newdir,  new_pattern, '0_rawdata', 'Orig'), 'f');
else
    copyfile(fullfile(workdir, old_pattern, '*.edf'), ...
         fullfile(newdir,  new_pattern), 'f');

    disp('Copying the info file...');
    copyfile(fullfile(workdir, old_pattern, '*.info'), ...
         fullfile(newdir,  new_pattern), 'f');
end
olddir = pwd;

% Set path to tomo_utils.py script
global GT_MATLAB_HOME;
tomoUtilsScript = fullfile( GT_MATLAB_HOME, 'zUtil_Python', 'tomo_utils.py');

% Enter old dataset Orig directopy
if (alreadySetup)
    cd(fullfile( newdir, new_pattern, '0_rawdata', 'Orig'));
else
    cd(fullfile( newdir, new_pattern));
end

disp('Checking if it is an interlaced scan...');
is_interlaced = exist([old_pattern '0_0000.edf'], 'file');

disp('Renaming the images...');
filenames = [old_pattern '*.edf'];
if (is_interlaced)
    image_old = [old_pattern '0_'];
    image_new = [new_pattern '0_'];
    cmd = [tomoUtilsScript ...
         ' -rename ''' filenames ''' ''' image_old ''' ''' image_new ''''];
    [~, msg] = gtPythonCommand(cmd); disp(msg);
    image_old = [old_pattern '1_'];
    image_new = [new_pattern '1_'];
    cmd = [tomoUtilsScript ...
         ' -rename ''' filenames ''' ''' image_old ''' ''' image_new ''''];
    [~, msg] = gtPythonCommand(cmd); disp(msg);
else
    cmd = [tomoUtilsScript ...
         ' -rename ''' filenames ''' ''' old_pattern ''' ''' new_pattern ''''];
    [~, msg] = gtPythonCommand(cmd); disp(msg);
end

cd(olddir);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copying xml file
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp('Copying the xml file...');
if (alreadySetup)
    %xmlold = fullfile(workdir, old_pattern, '0_rawdata', old_pattern, [old_pattern '.xml']);
    xmlold = fullfile(workdir, old_pattern, '0_rawdata', 'Orig', [old_pattern '.xml']);
    xmlnew = fullfile(newdir, new_pattern, '0_rawdata', 'Orig', [new_pattern '.xml']);
else
    %xmlold = fullfile(workdir, old_pattern, old_pattern, [old_pattern '.xml']);
    xmlold = fullfile(workdir, old_pattern, [old_pattern '.xml']);
    xmlnew = fullfile(newdir, new_pattern, [new_pattern '.xml']);
end
copyfile(xmlold, xmlnew, 'f');

disp('Renaming scan name field in the xml file...');
cmd = ['sed -i -e "s/' old_pattern '/' new_pattern '/g " ' xmlnew];
[~, msg] = unix(cmd); disp(msg);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Header information
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copy *.xml file to *dct_.xml adding some information from headers of
% ref0000_0000.edf and darkend0000.edf images

wd = pwd;
if (alreadySetup)
    cd(fullfile(newdir, new_pattern, '0_rawdata', 'Orig'));
else
    cd(fullfile(newdir, new_pattern));
end

gtUpdateXmlFile();

cd(wd);

% copy DB tables and values 
gtDBCopyAllTables(old_pattern, new_pattern);
gtDBCreateSpotPairTable(new_pattern, 1);

end % end of function
