function output = grep(varargin)
% GREP  The same as the unix command grep
%     grep
%     ----
%     INPUT:
%       text      = text to search
%       directory = directory or file in which to look at
%       sep       = separator character for the command 'awk'
%       column    = which column has to be chosen from 'awk'
%         DEFAULT: sep = ':', column = '1'

text=varargin{1};
directory=varargin{2};
if nargin >= 3
    sep=varargin{3};
else
    sep=':';
end
if nargin == 4
    column=varargin{4};
else
    column='1';
end
if isdeployed
   global GT_DB
   global GT_MATLAB_HOME
   load('workspaceGlobal.mat');
   column=num2str(column);
end

cmd=sprintf('grep ''%s'' %s | awk -F''%s'' ''{print $%s}'' | sort -busi',text,directory,sep,num2str(column));
%disp('Executing this command: ')
%disp(cmd)
%disp(' ')
[s,o]=unix(cmd);
output = o;

clear s o cmd text directory sep column

end