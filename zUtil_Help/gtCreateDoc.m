function gtCreateDoc(outDir, src)
% FUNCTION GTCREATEDOC  Create documentation for matlab functions
%
%     gtCreateDoc(outDir, src)
%     -------------------------------------------------------------------------
%     Create the documentation in the DCT folder (by default) by
%     producing the index.html file, which can be opened with a browser.
%
%     INPUT:
%       outDir = <string>  Path to the output directory, from DCT root
%                          directory (produces ~/GT_MATLAB_HOME/outDir).
%
%     OPTIONAL INPUT:
%       src    = <string>  Path to the source directories or files, from 
%                          DCT root directory, it can be:
%                          - 'zUtil_OAR/gtOarLaunch.m'     : only gtOarLaunch.m
%                          - 'zUtil_OAR'                : only zUtil_OAR dir 
%                          - {'zUtil_GUI', 'zUtil_OAR'} : multiple directories
%                          - '.' (default)              : all the DCT code
%
%     Version 003 27-08-2012 by YGuilhem
%       Now can focus on one or more source directories
%
%     Version 002 13-08-2012 by YGuilhem
%       Change options and syntax
%       Add ignored directories
%
%     Version 001 15-12-2011 by LNervo

    % Default parameters
    if ~exist('src', 'var')
        src = '.';
    end

    % Get global variable GT_MATLAB_HOME to set source directory
    if isempty(whos('global', 'GT_MATLAB_HOME'))
        error('DOC:no_such_variable', ...
              ['GT_MATLAB_HOME variable doesn''t exist in global workspace, '...
               'your environment is not sane, and you need to either ' ...
               're-initialise or re-run matlab.'])
    end
    global GT_MATLAB_HOME
    sourceDir = fullfile(GT_MATLAB_HOME);

    % Check if DCT source directory exists
    if ~exist(sourceDir, 'dir')
        error('DOC:no_DCT_directory', ...
        'The DCT root folder does not exist');
    end

    % Check if documentation output directory exists
    absOutDir = fullfile(GT_MATLAB_HOME, outDir);
    if exist(absOutDir, 'dir')
        error('DOC:wrong_input', ...
        ['directory ''' absOutDir ''' already exists, ',  ...
        'delete it before running gtCreateDoc!']);
    end

    % List of directories to ignore in the documentation process
    ignoredDirs = {'.svn' ,'m2html', 'bin'};

    % Store current directory
    currentDir = pwd;

    % Go to source directory
    cd(sourceDir);

    % Launch m2html procedure
    m2html(...
    'mFiles'               , src         , ...
    'htmlDir'              , outDir      , ...
    'ignoredDir'           , ignoredDirs , ...
    'recursive'            , 'on'        , ...
    'global'               , 'on'        , ...
    'graph'                , 'on'        , ...
    'globalHypertextLinks' , 'on'        , ...
    'todo'                 , 'on'        , ...
    'verbose'              , 'on'        , ...
    'save'                 , 'off'       , ...
    'source'               , 'off'       );

    disp(' ');
    disp(['Documentation for ' sourceDir ' has been generated in ' absOutDir]);
    if ~strcmp(src, '.')
        if ischar(src)
            disp(['Using the source directory: ' src]);
        elseif iscell(src)
            disp('Using the following source directories:')
            for i=1:length(src)
                disp(['- ' src(i)]);
            end
        end
    end

    % Move back to the initial directory
    cd(currentDir);

end % end of function
