function [lname, indout] = gtLastFileName(fname,w,ext)
%     [lname, indout] = gtLastFileName(fname,w,ext)
%     ---------------------------------------------
%
%     Used before saving an output file. It gives a filename (.ext) ending with
%     an integer after browsing the current folder or the given path.
%     If w is 'existing', it outputs an existing filename with the largest 
%     integer at the end. 
%     If w is 'new', the filename will have the largest+1 integer at the end.
%
%     USAGE:
%       gtLastFileName('myfiles','new','mat') will give 'myfiles1.mat' or, if it
%       already exists, then 'myfiles2.mat'.
%
%
%     Version 002 02-09-2013 by LNervo
%       Added freedom of file extension; default is 'mat'
%       Default mode (w) is 'existing'


if ~exist('w','var') || isempty(w)
    w = 'existing';
end
if ~exist('ext','var') || isempty(ext)
    ext = 'mat';
end

% Decompose path and fname
[pathstring, name] = fileparts(fname);

% Search for .mat files 
allfnames = dir([fname '*.' ext]);

% If nothing found
if isempty(allfnames)
    if strcmp(w,'new')
        lname  = [fname '1.' ext];
        indout = 1;
        return
    else
        lname  = [];
        indout = []; 
        return
    end
end


% Continue if files exist

allnum = [];

% Loop through filenames; it may be a series with different numbers 
for ii = 1:length(allfnames)
    
    % Get file number 
    num = str2double(allfnames(ii).name(length(name)+1:end-4));
    
    if isnan(num)
        allnum(ii) = 0;
    else
        allnum(ii) = num;
    end
end

% Find the one with the highest number
[maxnum, maxind] = max(allnum);

% Return existing or new name
switch w
    case 'existing'
        lname  = fullfile(pathstring, allfnames(maxind).name);
        indout = maxnum;
        
    case 'new'
        lname  = sprintf('%s%d.%s',fname,maxnum+1,ext);
        indout = maxnum + 1;
        
    otherwise
        lname  = [];
        indout = [];
        disp('Input option should be ''existing'' or ''new'' !')
end
	

end % of function
