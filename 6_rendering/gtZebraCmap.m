function cmap = gtZebraCmap(maxval, bgcolor)
% GTZEBRACMAP  Creates a zebra color map
%
%   cmap = gtZebraCmap(maxval[, bgcolor])
%   --------------------------------------------------------------------------
%
%     INPUT:
%       maxval  = <uint>    Maximal value of the color map
%       bgcolor = <double>  RGB color code for background color {[0 0 0]}
%                 <string>  Can also be 'black' or 'white'
%
%     OUTPUT:
%       cmap    = <double>  Color map 2D matrix [maxval+1 by 3]
%                           (first line is background color)

    if ~exist('bgcolor', 'var') || isempty(bgcolor)
        bg = [0 0 0]; % black background
    elseif ischar(bgcolor)
        if strcmpi(bgcolor, 'black')
            bg = [0 0 0];
        elseif strcmpi(bgcolor, 'white')
            bg = [1 1 1];
        else
            gtError('gtZebraCmap:wrong_background_color_value', ...
                ['Unsupported background color value: ' bgcolor]);
        end
    elseif (isfloat(bgcolor) && size(bgcolor, 1) == 1 && size(bgcolor, 2) == 3)
        if between(bgcolor, [0 0 0], [1 1 1])
            bg = bgcolor;
        else
            gtError('gtZebraCmap:wrong_background_color_value', ...
                'Background RGB values should be set in [0 , 1]!');
        end
    else
        gtError('gtZebraCmap:wrong_background_color_value', ...
            ['Unsupported background color value: ' bgcolor]);
    end

    baseColors = [ 1 0 0 ; ...
                   0 1 0 ; ...
                   0 0 1 ; ...
                   1 1 0 ; ...
                   1 0 1 ; ...
                   0 1 1];
    N = size(baseColors, 1);
    M = ceil(maxval / N);

    % Replicate colors and select only the proper size
    colors = repmat(baseColors, [M 1]);
    cmap = [bg ; colors(1:maxval, :)];

end % end of function
