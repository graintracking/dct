function stlwrite(pv,filename,varargin)
% STLWRITE.M A simple STL (stereolithography) file writer
% Greg Johnson, August 2002
% greg.johnson@ieee.org
%
% Usage: stlwrite(stl,filename,{'ascii'/'binary'}
% where stl is the variable containing the mesh
%       filename is the output filename
% The file will be a binary STL unless the optional third argument is
% 'ascii'
%
% The stl variable has the following components:
% stl.verticesX (Y,Z) - the locations of the vertices - three per triangle
%
% See also: stlread

if nargin==2
  filetype='binary';
elseif nargin==3
  if strcmp(varargin{1},'ascii')
    filetype='ascii';
  else
    filetype='binary';
  end
else
  disp('Not quite sure what you''re asking...');
  return
end

if strcmp(filetype,'ascii')
  disp('Writing ASCII STL file');  
  fid=fopen(filename,'wt');
  fprintf(fid,'solid matlab\n');
  for ndx_facet=1:size(pv.faces,1)
    points=pv.vertices(pv.faces(ndx_facet,:),:);
    normal = cross([points(2,:)-points(1,:)],[points(3,:)-points(1,:)]);
    normal=normal./norm(normal);
    fprintf(fid,'  facet normal %f %f %f\n',-normal);
    fprintf(fid,'    outer loop\n');
    for ndx_vertex=1:3
      v1=pv.vertices(pv.faces(ndx_facet,ndx_vertex),1)/10;
      v2=pv.vertices(pv.faces(ndx_facet,ndx_vertex),2)/10;
      v3=pv.vertices(pv.faces(ndx_facet,ndx_vertex),3)/10;
      fprintf(fid,'      vertex %1.2f %1.2f %1.2f\n',v1,v2,v3);
    end
    fprintf(fid,'    endloop\n');
    fprintf(fid,'  endfacet\n');
  end
  fprintf(fid,'endsolid %s\n','MATLAB');
  fclose(fid);
else
  disp('Writing binary STL file');
  disp('NOT YET!');
end

