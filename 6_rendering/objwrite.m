function objwrite(pv,filename)
% OBJWRITE.M A simple OBJ (Alias Wavefront) file writer (pv is the form of
% output from isosurface)
% Greg Johnson, August 2002 (November 2006)
%
%pv.faces=pv.faces(1:100:end,:);


fid=fopen(filename,'w');

for group=1:length(pv)
  nvertices=size(pv{group}.vertices,1);
  nfaces=size(pv{group}.faces,1);

  verticesperface=size(pv{group}.faces,2);

  for n=1:nvertices
    fprintf(fid,'v %3.3f %3.3f %3.3f\n',pv{group}.vertices(n,:));
  end

  fprintf(fid,'g foo%d\n',group);

  for n=1:nfaces
    fprintf(fid,'f %d %d %d\n',pv{group}.faces(n,:));
  end
end

fprintf(fid,'g\n');



% 0 0 0                      { start of vertex list }
% 0 0 1
% 0 1 1
% 0 1 0
% 1 0 0
% 1 0 1
% 1 1 1
% 1 1 0
% 4 0 1 2 3                  { start of face list }
% 4 7 6 5 4
% 4 0 4 5 1
% 4 1 5 6 2
% 4 2 6 7 3
% 4 3 7 4 0
fclose(fid);
