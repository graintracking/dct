function h = gtIPFCmapHexagonalKey(varargin)
% GTIPFCMAPHEXAGONALKEY  Show inverse pole figure color space in the hexagonal SST
%
% <<< DEPRECATED >>>
% Use gtIPFCmapKey instead
%
%     h = gtIPFCmapHexagonalKey(varargin)
%     -----------------------------------
%
%     OPTIONAL INPUT (varargin):
%       crystal_system  = <string>
%       N         = <int>       Number of angular steps (quality of the plot) {30}
%       saturate  = <logical>   Saturate or not the color in the SST {true}
%       label     = <logical>   Draw poles label {false}
%       fontsize  = <int>       Font size for labels {18}
%       drawing   = <logical>   Draw the patch with RGB colors {true}
%       hf        = <handle>    figure handle {[]}
%       ha        = <handle>    axis handle {[]}
%       figcolor  = <dobule>    figure color {[1 1 1]}
%       sampleDir = <string>    IPF sample direction {'001'}
%
%     OUTPUT:
%       h        = <handle>    Handles in figure

app.crystal_system  = '';
app.N        = 30;
app.saturate = true;
app.label    = false;
app.fontsize = 12;
app.drawing  = true;
app.hf       = [];
app.ha       = [];
app.figcolor = [1 1 1];
app.sampleDir   = '001';
app = parse_pv_pairs(app, varargin);

if isempty(app.hf)
    hf = figure();
else
    hf = app.hf;
end
if isempty(app.ha)
    ha = axes();
else
    ha = app.ha;
end
hold(ha,'on');

if strcmpi(app.crystal_system, 'hexagonal')
    % Work in radians
    phimin =    0;
    phimax = pi/6;
    inc = phimax/app.N;

    psimin =    0;
    psimax = pi/2;
    chimax = 0;
elseif strcmpi(app.crystal_system, 'cubic')
    % Work in radians
    phimin =    0;
    phimax = pi/4;
    inc = phimax/app.N;

    psimin =    0;
    psimax = atan(sqrt(2))+inc;
    chimax = pi/4;
end

h_p = [];
for phi=phimin:inc:(phimax-inc)     % Rotation around z-axis
    for psi=psimin:inc:(psimax-inc) % Out from z-axis to vector

        % For hexagonal colour map, we need two angles:
        % - phi (around z-axis)
        % - psi (from z-axis to vector)

        ratioR = 1 - psi/psimax;
        ratioB =     phi/phimax;

        red   = ratioR;
        green = (1 - ratioR) * (1 - ratioB);
        blue  = (1 - ratioR) * ratioB;        
        rgb = [red green blue];

        % Ensure that we stay in [0 1]
        rgb(rgb < 0) = 0;
        rgb(rgb > 1) = 1;

        if (app.saturate)
            rgb = gtCrystSaturateSSTColor(rgb);
        end

        % Radius distorted for stereographic proj
        r_patch = tan([psi/2 (psi+inc)/2]);
        [phi_patch, r_patch] = meshgrid([phi phi+inc]', r_patch);
        [x_patch, y_patch] = pol2cart(phi_patch, r_patch);

        if (app.drawing)
            h_p(end+1) = patch(x_patch, y_patch, rgb, 'EdgeColor', 'none', 'Faces', [1 2 4 3]);
        end
    end
end
set(h_p,'Tag','h_patch');

% Add poles and tart up like gtMakePoleFigure
set(ha, 'XTickLabel', '');
set(ha, 'YTickLabel', '');
set(ha, 'GridLineStyle', 'none');
set(ha, 'Ycolor', 'w');
set(ha, 'Xcolor', 'w');
set(ha, 'Tag','h_axes');
axis(ha,'tight')
axis(ha,'equal');
set(ha, 'xlim', [-0.05 1.05]);
set(ha, 'ylim', [-0.06 0.56]);

% Add a black border around triangle
% For hexagonal:
% - line from [0 0 0 1] to [2 -1 -1 0] (phi=0)
% - arc from [2 -1 -1 0] to [1 0 -1 0] (phi=[0 pi/6], psi=pi/2)
% - line from [0 0 0 1] to [1 0 -1 0] (phi=pi/6)
line_phi = (phimin:inc:phimax)';
line_r   = tan(psimax/2 * ones(size(line_phi)));

% Stereographic projection x-y
[line_x, line_y] = pol2cart(line_phi, line_r);
xp = [line_x; 0;  line_x(1)];
yp = [line_y; 0;  line_y(1)];
plot(xp, yp, 'k-','Tag','h_line_plot');

% Add labels for the 3 corners poles
poles_psi = [psimin psimax psimax];
poles_phi = [phimin phimax phimin];
poles_r = tan(poles_psi/2);
[poles_x,poles_y] = pol2cart(poles_phi, poles_r);
plot(poles_x, poles_y, 'k*','Tag','h_points_plot');

text(poles_x(1), poles_y(1), '[0 0 0 1]', 'FontSize', app.fontsize, ...
    'VerticalAlignment','top','HorizontalAlignment','left','Tag','h_poles');
text(poles_x(2), poles_y(2), '[1 0 -1 0]', 'FontSize', app.fontsize, ...
    'VerticalAlignment','bottom','HorizontalAlignment','right','Tag','h_poles');
text(poles_x(3), poles_y(3), '[2 -1 -1 0]', 'FontSize', app.fontsize, ...
    'VerticalAlignment','top','HorizontalAlignment','right','Tag','h_poles');

if (app.label)

end

set(hf, 'Tag', 'h_figure')
set(hf, 'Color', app.figcolor)
set(ha, 'Visible', 'off');

% label and sample direction to plot
text(0,0.5,0,'IPF', 'FontSize', app.fontsize, ...
    'VerticalAlignment','top','HorizontalAlignment','left','Tag','h_IPF');

text(0,0.42,0,app.sampleDir, 'FontSize', app.fontsize, ...
    'VerticalAlignment','top','HorizontalAlignment','left','Tag','h_IPFdir');

h = guihandles(hf);

end % end of function
