function hf = gtMakePoleFigure(varargin)
% GTMAKEPOLEFIGURE  Makes a pole figure given a list of poles
%     hf = gtMakePoleFigure(varargin)
%     -------------------------------
%
%     the poles should cover a larger angular area than required, to avoid
%     depleted pole density at the edges.  Any un-needed poles will be crops to
%     improve speed.
%
%     OPTIONAL INPUT (parse by pairs):
%       'poles'       = <double>     poles to plot
%       'densityXYZ'  = <cell>       x,y,z values from previous polefigure calculation
%       'weights'     = <double>     can supply a vector the same length as poles to 
%                                    weight the poles
%       'range'       = <string>     'sst'/'hex'/'phi90'/{'phi360'}
%                                    standard stereographic triangle, quarter or whole
%                                    hemisphere or hexagonal sst
%       'label_poles' = <string>     'all'/'bcc'/'main'/{'none'}
%                                    all hkl poles or just the corners of the sst.
%       'mark_poles'  = <string>     'none'/'main'/'bcc'/{'all'}
%       'N'           = <int>        data point density in pole figure {100}
%       'searchR'     = <double>     smoothing radius in pole figure {0.1 rad}
%       'plot_figure' = <logical>    can choose not to plot figure {true}
%       'surf'        = <logical>    surface plot option {false}
%       'scatter'     = <logical>    scatter plot option {false}
%       'rings'       = <logical>    plot the psi rings {false}
%       'cmap'        = <double>     cmap to color grainds/poles {[]}
%       'grainids'    = <double>     grainIDs to display only some poles {[]}
%       'colorbar'    = <logical>    display the colorbar {true}
%       'hf'          =              figure handle (if existing) {[]}
%       'logscale'    = <logical>    plot log scale for axis-Z {false}
%       'phaseid'     = <int>        phase number {1}
%       'crystDir'    = <double>     crystallographic direction plotted in the
%                                    pole figure used in the title and figure name {[0 0 0 1]}
%       'title'       = <string>     figure title {''}
%       'export'      = <logical>    flag to export the figure or not {false}
%       'maxring'     = <double>     maximum psi value for ring {90}
%       'linecolor'   = <double>     RGB Color for rings, poles and labels {[1 1 1]}
%       'contourline' = <double>     RGB Color or 'none' for contour lines coloured {'none'}
%       'levels'      = <double>     number of levels for contour and surf {10}
%       'colormap'    = <string>     colormap for the density plot {'jet'}
%       'clims'        = <double>     colour limits for density plot {[]}
%       'axesRot'     = <double>     transform axes {[1 2 3]}
%       'view'        = <double>     axes view {[0 90]}
%       'debug'       = <logical>    print comments {false}
%       'tag'         = <string>     tag for axes {''}
%       'fontsize'    = <double>     text size {10}
%       'figname'     = <string>     figure name for exporting {''}
%
%     OUTPUT:
%       hf            =              figure handle
%
%
%     Version 004 18-09-2013 by LNervo
%       Added options
%       'linecolor','contourline','levels','clims','axesRot','view','debug','tag','fontsize','figname'
%
%     Version 003 17-01-2013 by LNervo
%       Improved speed, formatted in a clearer way, splitted with other
%       functions
%
%     Version 002 26-06-2012 by LNervo
%       Add phaseid as input argument for multiple phases materials
%
%     Version 001 09-2009 by AKing
%       could be improved to pass in a list of which hkl poles to plot and to
%       label.


% read in the specs
app.poles       = [];
app.densityXYZ  = [];
app.weights     = [];
app.range       = 'phi360'; % 'phi360' | 'phi90' | 'hex' | 'sst'
app.N           = 100;
app.searchR     = 0.1;

% if app.poles is empty, run gtCrystAxisPoles
app.phaseid     = [];
app.crystDir    = [];
% gtCrystAxisPoles arguments
app.crystal_system = '';
app.latticepar     = [];
app.r_vectors      = [];
app.axesRot        = [1 2 3]; % trasform axes 
app.background     = true;
app.save           = false;
app.convention     = 'X';
% drawing
app.plot_figure = true;     % true | false
app.surf        = false;    % true | false
app.scatter     = false;    % true | false
app.rings       = true;     % true | false
app.cmap        = [];       % for scatter plots
app.grainids    = [];       % for scatter plots
app.colorbar    = true;     % true | false
app.label_poles = 'none';   % 'all' | 'main' | 'none' | 'bcc' ( only for cubic )
app.mark_poles  = 'main';   % 'all' | 'main' | 'none' | 'bcc' ( only for cubic )
app.maxring     = 90;       % degrees for psi angle
app.linecolor   = [1 1 1];  % color for rings, labels
app.contourline = 'none';   % can be RGB color or 'none'
app.levels      = 10;       % number of levels for contour and surf
app.colormap    = 'jet';    % 
app.clims       = [];       % colour limits
app.fontsize    = 10;       % text size

app.hf          = [];
app.logscale    = false;    % true | false
app.title       = '';
app.view        = [0 90];   % view of the XY plane RD right and TD top -> 0 90;
                            % RD bottom TD right -> 90 90
app.debug       = false;    % true | false
app.tag         = '';       % tag for axes and cb

app.figname     = '';
app.export      = false;    % true | false
app.paper_fig   = false;
app.ws_path     = true;
app.dotFig      = false;

[app, rej_pars] = parse_pv_pairs(app, varargin);

out = GtConditionalOutput(app.debug);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% analyse the pole data
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% before building figure, take only those poles that fall in the range 0-phimax,
% 0-psimax (allowing for search radius)
if isempty(app.densityXYZ)

    if isempty(app.poles)
        vargs_in = struct2pv(app);
        [app.poles, app.grainCaxisColorMap, ~, vargs_out] = gtCrystAxisPoles(app.phaseid, app.crystDir, vargs_in{:});
        [app, rej_pars] = parse_pv_pairs(app, vargs_out);
    end
    [poles, weights, phimax, psimax] = gtFindPolesInRange(app.poles, app.weights, app.range, []);
    if (~app.scatter)
        [x, y, z] = gtCalculateDensityPhiPsi(poles, weights, [0 phimax], [0 psimax], app.searchR, app.N);
        [phi,rho,z] = cart2pol(x,y,z);
        psi = 2*atan(rho);
    else
        phi = atan2(poles(:,2),poles(:,1));
        psi = acos(poles(:,3));
        rho = tan(psi/2);
        [x,y] = pol2cart(phi,rho);
        z = [];
        app.colorbar = false;
    end
else
    x = app.densityXYZ{1};
    y = app.densityXYZ{2};
    z = app.densityXYZ{3};
    [phi,rho,z] = cart2pol(x,y,z);
    psi = 2*atan(rho);
end

if (app.logscale) && ~isempty(z)
    z = log10(z);
    out.odisp('Using log scale for axis Z')
end

print_structure(app, 'Options', false, app.debug)

% figure name/title
figname = [gtGetLastDirName(pwd) '_polefigure_' strrep(num2str(app.crystDir),' ','') '__' app.range];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% build the figure
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if (app.plot_figure)

    if ~isempty(app.hf)
        hf = app.hf;
    else
        hf = figure();
    end
    hold(gca,'on');
    % plot the poles
    if (app.surf)
        surf(x,y,z)
        shading('interp')
        out.odisp('Using colour interpolation for the surface plot')
        figname = [figname '_surf'];
    elseif (app.scatter)
        if ~isempty(app.cmap)

            figname = [figname '_cmap'];
            if all(app.cmap(1,:) == [0 0 0])
                app.cmap = app.cmap(2:end,:);
            end
            if length(app.cmap) ~= length(x)
                tmp2 = app.cmap;
                ids = app.poles(:,1);
    
            end
            if ~isempty(app.grainids)
              app.cmap = app.cmap(app.grainids,:);
              x = x(app.grainids);
              y = y(app.grainids);
            end
            scatter(gca, x,y,10,app.cmap,'filled')
        else
            scatter(gca,x,y,10,'ob','filled')
        end
        out.odisp('Scatter plot with markersize 10 is used')
        figname = [figname '_scatter'];
    else
        contourf(x,y,z,round(app.levels),'LineColor',app.contourline);
        out.odisp(['Using ' num2str(round(app.levels)) ' levels for the contour plot'])
        figname = [figname '_contour'];
    end

    hold(gca,'on')
    if ~isempty(app.clims)
        set(gca, 'CLim', app.clims)
    end
    tmp = app.colormap;
    if strcmpi(tmp, 'gray')
        tmp = ['1-' tmp];
        isCbGray = true;
    else
        isCbGray = false;
    end
    tmp = sprintf('%s(%d)',tmp,app.levels);
    app.cmap = eval(tmp);

    if (app.colorbar)
        hcb = colorbar('Location', 'manual','Position',[0.9351 0.1089 0.0424 0.7840],'YAxisLocation','left');
        set(hcb,'UserData',['CB_' app.tag])
    end
    if ~strcmp(app.colormap, 'IPF')
        colormap(app.cmap)
    elseif (app.scatter)
        hf2 = gtIPFCmapKey('crystal_system',app.crystal_system,...
            'N', 50, 'label', true, 'drawing', false);
        ha = get(hf2,'CurrentAxes');
        set(ha,'Position',[0.75 0.1 0.2 0.2]);
        copyobj(ha,hf);
        figname = [figname 'IPF'];
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % tidy the figure
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    set(gca, 'XLim', [-1.05 1.2]);
    set(gca, 'YLim', [-1.05 1.2]);
    set(gca,'DataAspectRatio',[.2 .2 1]);
    set(gca,'Position',[-0.01 0.025 0.9122 0.934]);
    set(gca,'Tag',app.tag);
    set(gca,'View',app.view);
    set(gca,'Visible','off');
    set(gca,'FontSize',app.fontsize)
    set(get(gca,'Title'),'FontSize',app.fontsize)
    set(get(gca,'XLabel'),'FontSize',app.fontsize)
    set(get(gca,'YLabel'),'FontSize',app.fontsize)
    set(get(gca,'ZLabel'),'FontSize',app.fontsize)
    set(gca,'LineWidth',1)

    set(hf,'Position',[1259 470 600 550]);
    set(hf,'Color',[1 1 1]);
    set(hf,'UserData',{x,y,z;phi,psi,rho});
    set(hf,'Visible','on');
    
    if (~app.scatter)
        label_z=(max(z(:))+1);
    else
        label_z=1;
    end
    
    if ~isempty(app.title)
        set(get(gca,'Title'),'String',app.title);
        set(get(gca,'Title'),'Visible','on')
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % sample reference
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    labels = {'(Xs)','(Ys)','(Zs)'};
    labels = labels(abs(app.axesRot));
    labels = vertcat({'RD ','TD ','ND  '},labels);

    if all(isequal(app.view, [90 90]))
        pos = [[1.1627   -0.0829  label_z];...
               [0.0004    1.0480  label_z];...
               [-0.005   -0.1159  label_z]];
    elseif all(isequal(app.view, [0 90]))
        pos = [[1.1276    0.0004  label_z];...
               [-0.0741   1.1145  label_z];...
               [-0.0917   0.0004  label_z]];
    end
    text(pos(1,1), pos(1,2), pos(1,3),[labels{:,1}],'Color',app.linecolor,'FontSize',app.fontsize);
    text(pos(2,1), pos(2,2), pos(2,3),[labels{:,2}],'Color',app.linecolor,'FontSize',app.fontsize);
    text(pos(3,1), pos(3,2), pos(3,3),[labels{:,3}],'Color',app.linecolor,'FontSize',app.fontsize);

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % psi rings
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if (app.rings)

        if all(isequal(app.view, [90 90]))
            pos = [[0.1715   -0.0259  label_z];...
                   [0.3031   -0.0215  label_z];...
                   [0.4522   -0.0259  label_z];...
                   [0.6232   -0.0215  label_z];...
                   [0.8075   -0.0259  label_z];...
                   [1.0487   -0.0303  label_z]];
        end
        htr = []; count = 0;
        for ring=15:15:app.maxring
            ring_phi=deg2rad([0:3:360 0]);
            ring_psi=deg2rad(ring)*ones(size(ring_phi));
            ring_r=tan(ring_psi/2);
            ring_z=repmat(label_z,size(ring_phi));
            [ring_x,ring_y]=pol2cart(ring_phi,ring_r);
            plot3(ring_x, ring_y, ring_z, '-', 'Color', app.linecolor);
            count = count + 1;
            htr(count) = text(ring_x(1)+0.02, 0, label_z, num2str(ring), 'Color', app.linecolor,'FontSize',app.fontsize);
            if all(isequal(app.view, [90 90]))
                set(htr(count),'Position',pos(count,:))
            end
        end

    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % mark poles
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if ~strcmpi(app.mark_poles, 'none')

        % finds poles in the psi/phi range to be plotted
        [uvw_poles,~,~,~] = gtFindPolesInRange([], [], app.range, app.mark_poles);
        % converts poles from cartesian to spherical coordinates
        [phi_uvw,psi_uvw,r_uvw]=cart2sph(uvw_poles(:,1),uvw_poles(:,2),uvw_poles(:,3));
        % bring phi in [0,2pi] range from [-pi,+pi] range
        phi_uvw=phi_uvw+pi;
        % bring psi in [0,pi] range from [-pi/2,pi/2] range
        psi_uvw=pi/2-psi_uvw;
        % project poles on the plane
        rho_uvw=r_uvw.*tan(psi_uvw/2);
        % go from polar to cartesian space
        [x_uvw,y_uvw]=pol2cart(phi_uvw,rho_uvw);
        % drawing the poles
        plot3(x_uvw, y_uvw, ones(size(x_uvw))*label_z, '*', 'Color', app.linecolor)

    end % end mark_poles

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % range 'sst'
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if strcmpi(app.range, 'sst')
        %can be replaced by gtIPFCmapCubicKey to get the figure without the patch

        %line from 101 to 111 is z=x
        line_phi=[0:0.01:pi/4 pi/4];
        linex=cos(line_phi);
        linez=linex;
        liney=sin(line_phi);
        line_points=[linex' liney' linez'];
        %normalise
        line_points=line_points./repmat(sqrt(sum(line_points.*line_points, 2)), 1, 3);
        %convert to stereographic proj.
        line_psi=acos(line_points(:,3));
        line_r=tan(line_psi/2);
        [line_x,line_y]=pol2cart(line_phi', line_r);

        %white patch over unwanted bit of figure
        mask_x=[line_x ; sqrt(2)-1+0.045 ; sqrt(2)-1+0.045 ; line_x(1)];
        mask_y=[line_y ; line_y(end) ; 0 ; 0];
        mask_z=ones(size(mask_y))*(max(z(:))+0.5); %between the pole points and the surface
        p=patch(mask_x, mask_y, mask_z, 'w');
        set(p, 'edgecolor', 'w')
 
        %black outline on polefigure
        mask_x=[line_x ; 0 ; line_x(1)];
        mask_y=[line_y ; 0 ; 0];
        mask_z=ones(size(mask_y))*(max(z(:))+1);
        plot3(mask_x, mask_y, mask_z, 'k')

        %tweak axis limits and therefore adjust camera position
        set(gca, 'xlim', [-0.02 sqrt(2)-1+0.045])
        set(gca, 'ylim', [-0.025 0.3769])
        set(gca, 'CameraUpVector', [0 1 0]);

    end % end range

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % label poles
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if ~strcmpi(app.label_poles, 'none')
        %label the poles son the corners of the sst
        text(0, 0-0.015, label_z, '(001)')
        text(0.366+0.01, 0.366-0.005, label_z, '(111)')
        text(0.4142-0.015, -0.015, label_z, '(101)')
    end
    if strcmpi(app.label_poles, 'all')
        text(0.1213-0.04, 0.1213, label_z, '(114)')
        text(0.1583-0.04, 0.1583, label_z, '(113)')
        text(0.2247-0.04, 0.2247, label_z, '(112)')
        text(0.2808-0.04, 0.2808, label_z, '(223)')
        text(0.3052-0.045, 0.3052, label_z, '(334)')

        text(0.3845+0.01, 0.2884, label_z, '(434)')
        text(0.3901+0.01, 0.2601, label_z, '(323)')
        text(0.4000+0.01, 0.2000, label_z, '(212)')
        text(0.4077+0.01, 0.1359, label_z, '(313)')
        text(0.4105+0.01, 0.1026, label_z, '(414)')

        text(0.1231-0.02, -0.015, label_z, '(104)')
        text(0.1623-0.01, -0.015, label_z, '(103)')
        text(0.2361-0.015, -0.015, label_z, '(102)')
        text(0.3028-0.02, -0.015, label_z, '(203)')
        text(0.3333-0.01, -0.015, label_z, '(304)')

        text(0.2967+0.01, 0.1483, label_z, '(213)')
        text(0.2330+0.01, 0.1165, label_z, '(214)')
        text(0.3297+0.01, 0.1099, label_z, '(314)')
        text(0.3197+0.01, 0.2131, label_z, '(324)')
    elseif strcmpi(app.label_poles, 'bcc')
        text(0.2247-0.04, 0.2247, label_z, '(112)')
        text(0.3, 0.153, label_z, '(213)')
    end % end label_poles

    if (isCbGray)
        set(findobj(hf,'type','line'),'Color',[0 0 0]);
        set(findobj(hf,'type','text'),'Color',[0 0 0]);
    end

    app.figname = figname;
    setappdata(hf, 'AppData', app);

    if (app.export)
        export(figname,hf,app.paper_fig,app.ws_path,app.dotFig);
    else
        out.odisp('Figure name with description to export it:')
        out.odisp(figname)
        % set title of the figure using figname
        out.odisp('To change the title do:')
        out.odisp('    gtPlotSettings([],[],strrep(<figname>,''_'','' ''))');
    end
else
    hf.x = x;
    hf.y = y;
    hf.z = z;
    out.odisp('hf contains x,y,z values of the pole figure')
end % end plot_figure

end % end of function
