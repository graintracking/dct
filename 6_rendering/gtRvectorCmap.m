function rmap = gtRvectorCmap(phase_id, r_vectors)
% rmap = gtRvectorCmap(phase_id, varargin)
%
% make a colormap from the r_vectors.  Requires tha the r_vectors.mat
% variable has been created - use gtReadGrains or it is possible to pass
% the r_vectors as the second argument.

if ~exist('phase_id', 'var') || isempty(phase_id)
    phase_id = 1;
end

if exist('r_vectors','var') && ~isempty(r_vectors) && size(r_vectors,2) == 3
    rmap = r_vectors;
else
    if exist(fullfile('4_grains', sprintf('phase_%02d', phase_id),'r_vectors.mat'), 'file')
        load(fullfile('4_grains', sprintf('phase_%02d', phase_id),'r_vectors.mat'));
    else
        disp('Loading most recent indexing output: index.mat');
        indexFile = fullfile('4_grains', sprintf('phase_%02d', phase_id), ...
            'index.mat');
        try
        load(indexFile);
        r_vectors = gtIndexAllGrainValues(grain, 'R_vector', [], 1, 1:3);
        r_vectors = [zeros(size(r_vectors, 1), 1) r_vectors];
        catch mexc
            gtPrintException(mexc);
            rmap=[];
            return;
        end
    end

    % get the three components of the r-vector
    rmap=r_vectors(:,2:4);
end
% scale to 0-1
rmap=rmap-min(rmap(:));
rmap=rmap/max(rmap(:));
% black background
rmap=[0 0 0; rmap];

end % end of function
