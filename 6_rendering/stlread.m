function stl=stlread(varargin)
% STLREAD.M A simple STL (stereolithography) file reader
% Greg Johnson, August 2002
% greg.johnson@ieee.org
%
% Usage: stl=stlread            You will be asked to pick an STL file
%        stl=stlread(filename)  The file specified will be used
% The returned structure, stl, contains the name/comment (as taken from the
% file header) along with three vertex lists.  These can be visualised
% with:  
%  
%   hp=patch(stl.verticesX,stl.verticesY,stl.verticesZ,zeros(size(stl.verticesX)));
% and the following, for additional coolness factor:
%   axis image;axis vis3d; axis off
%   cameratoolbar('setmode','orbit')
%   camorbit(20,20)
%   set(gcf,'color',[0 0 0])
%   set(hp,'facecolor',[1 0 0])
%   set(hp,'edgecolor',[0 0 1])
%
% and yes, stlread does automagically read both the ASCII and binary
% variants of the file format.

debug=0;

if nargin==0
  [filename,pathname]=uigetfile('*.stl','Pick an STL file');
  filename=fullfile(pathname,filename);
else
  filename=varargin{1};
end
  
%tic
% test for ASCII or BINARYness

fid=fopen(filename,'rb');
tmp=fread(fid,80,'char');
tmp_numfacets=fread(fid,1,'uint32');
tmp_size=50*tmp_numfacets;
tmp_current=ftell(fid);
fseek(fid,0,'eof');
tmp_end=ftell(fid);
fclose(fid);

if (tmp_end~=tmp_current+tmp_size)
  % ASCII STL FILE
  disp('Reading ASCII STL file');
  fid=fopen(filename,'rt');
  filelocation='started';
  
  stl.name=[];
  stl.verticesX=[];
  stl.verticesY=[];
  stl.verticesZ=[];
  
  while (~feof(fid))
    
    l=upper(fgetl(fid));
    [t,r]=strtok(l);
    switch t
      case 'SOLID'
        if debug disp('SOLID'),end
        if strcmp(filelocation,'started')
          filelocation='solid';
          stl.name=r;
        else
          disp('Somethings wrong...')
          return
        end
        
        
      case 'FACET'
        if debug disp('FACET'),end
        if strcmp(filelocation,'solid')
          filelocation='facet';
          %disp('Not using STL Normal values')
          currentfacetX=[];
          currentfacetY=[];
          currentfacetZ=[];
        else
          disp('Something wrong...')
          return
        end
        
      case 'OUTER'
        if debug disp('OUTER'),end
        if strcmp(filelocation,'facet')
          filelocation='loop';
          
        else
          disp('Something wrong...')
          return
        end
        
      case 'VERTEX'
        if debug disp('VERTEX'),end
        if strcmp(filelocation,'loop')
          filelocation='loop';
          [tmpX,r]=strtok(r);
          [tmpY,r]=strtok(r);
          [tmpZ,r]=strtok(r);
          
          currentfacetX=cat(1,currentfacetX,str2num(tmpX));
          currentfacetY=cat(1,currentfacetY,str2num(tmpY));
          currentfacetZ=cat(1,currentfacetZ,str2num(tmpZ));
          
        else
          disp('Something wrong...')
          return
        end
        
      case 'ENDLOOP'
        if debug disp('ENDLOOP'),end
        filelocation='facet';
      case 'ENDFACET'
        if debug disp('ENDFACET'),end
        filelocation='solid';
        stl.verticesX=cat(2,stl.verticesX,currentfacetX);
        stl.verticesY=cat(2,stl.verticesY,currentfacetY);
        stl.verticesZ=cat(2,stl.verticesZ,currentfacetZ);
        
        
      case 'ENDSOLID'
        if debug disp('ENDSOLID'),end
        filelocation='done';
    end
    
    
    
  end
  
  fclose(fid);
  
  
 
else
  % BINARY STL FILE
  fid=fopen(filename,'rb');
  disp('Reading binary STL file');
  stl.name=[];
  
  
  stl.name=char(fread(fid,80,'char')');
  
  num_facets=fread(fid,1,'uint32');
  stl.verticesX=zeros(3,num_facets);
  stl.verticesY=zeros(3,num_facets);
  stl.verticesZ=zeros(3,num_facets);
  currentfacetX=zeros(3,1);
  currentfacetY=zeros(3,1);
  currentfacetZ=zeros(3,1);
  
  data=fread(fid,12*num_facets,'12*float',2);
  for ndx=1:num_facets
    data_facet=data(((ndx-1)*12)+1:((ndx-1)*12)+12);
    normals=data_facet([1 2 3]);
    
  
    
    for vertex=1:3
      currentfacetX(vertex)=data_facet((vertex*3)+1);
      currentfacetY(vertex)=data_facet((vertex*3)+2);
      currentfacetZ(vertex)=data_facet((vertex*3)+3);
    end
    
    
    stl.verticesX(:,ndx)=currentfacetX';
    stl.verticesY(:,ndx)=currentfacetY';
    stl.verticesZ(:,ndx)=currentfacetZ';
  end
  
  fclose(fid);
  
end

disp(sprintf('Number of facets: %d',size(stl.verticesX,2)))
%toc;


% An example of a simple ASCII STL file:
% SOLID TRI 
%    FACET NORMAL 0.0 0.0 -1.0 
%      OUTER LOOP 
%        VERTEX -1.5 -1.5 1.4 
%        VERTEX 0.0 1.7 1.4 
%        VERTEX 1.5 -1.5 1.4 
%      ENDLOOP 
%    ENDFACET 
%    FACET NORMAL 0.0 0.88148 0.472221 
%      OUTER LOOP 
%        VERTEX -1.5 -1.5 1.4 
%        VERTEX 1.5 -1.5 1.4 
%        VERTEX 0.0 0.0 -1.4 
%      ENDLOOP 
%    ENDFACET 
%    FACET NORMAL -0.876814 -0.411007 0.24954 
%      OUTER LOOP 
%        VERTEX 1.5 -1.5 1.4 
%        VERTEX 0.0 1.7 1.4 
%        VERTEX 0.0 0.0 -1.4 
%      ENDLOOP 
%    ENDFACET 
%    FACET NORMAL 0.876814 -0.411007 0.24954 
%      OUTER LOOP 
%        VERTEX 0.0 1.7 1.4 
%        VERTEX -1.5 -1.5 1.4 
%        VERTEX 0.0 0.0 -1.4 
%      ENDLOOP 
%    ENDFACET 
% ENDSOLID TRI 