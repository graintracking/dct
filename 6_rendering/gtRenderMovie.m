function [mov, wobj] = gtRenderMovie(hf, fname, varargin)
% GTRENDERGRAINSMOVIE creates a video and a video file from 3D plots.
%
%     [mov, wobj] = gtRenderMovie(hf, fname, varargin)
%     ------------------------------------------------
%   
%    The movie is created from a 3D plot open on the screen by specifying 
%    a set of azimuth and elevation angles. The camera path will be 
%    interpolated between the consecutive stages. 
%
%    Note that a screensaver or changing virtual desktop, etc. may 
%    disturb the frame capturing. 
%
%    It uses the getframe function and VideoWriter object. For more info,
%    see Matlab help. 
% 
% 
%    EXAMPLE OF USAGE 
%      Rendering a movie of a 3D grain map.
%        Go to dataset folder.
%        vol = load('5_reconstruction/volume_dilated.mat');
%        [hf] = gtRenderGrains(vol.grains, 'xkeep', 1:50);
%        Choose a set of azimuth and elevation angles for the movie or 
%        run with default settings:
%        [mov, wobj] = gtRenderMovie(hf, 'grains.avi');
%        You can view the Matlab movie this way:
%        hm = figure;
%        movie(hm, mov, 1, wobj.FrameRate, [0 0 0 0]);
%
%
%    INPUT:
%      hf     = handle of figure showing the 3D grain plot
%      fname  = relative path and name of video file to be written
%    
%    OPTIONAL INPUT:
%      viewaz       = list of azimuth angles used for motion interpolation
%                     (1xn) {[0 180 360]}
%      viewel       = list of elevation angles used for motion interpolation
%                     (1xn) {[30 0 30]}
%      nstep        = number of view steps to interpolate for each stage 
%                     (scalar) {30}
%      showaxes     = if false, axes of the figure will be invisible {false}
%      showmov      = if false, it does not show the movie at the end {false}
%      framerate    = frame rate of movie {10}
%      quality      = compression quality of movie {75}
%      videoprofile = video profile for VideoWriter Matlab object 
%                     {Motion JPEG AQI}
%
%    OUTPUT:
%      mov  = Matlab movie object
%      wobj = VideoWriter object handle
%      Video file is written with specified name.
%
%  
%    Version 001 20-09-2012 by PReischig
%
%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Set parameters 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

par.viewaz       = [0 180 360];
par.viewel       = [30 0 30];
par.nstep        = 30;
par.showaxes     = false;
par.showmov      = false;
par.framerate    = 10;
par.quality      = 75;
par.videoprofile = 'Motion JPEG AVI';

[hasP, ind] = ismember('view', varargin(1:2:end));
if (hasP)
    varargin(ind*2-1:ind*2) = [];
end
[par, rej_pars] = parse_pv_pairs(par, varargin);



% Create VideoWriter object
wobj = VideoWriter(fname, par.videoprofile);

wobj.FrameRate = par.framerate;
wobj.Quality   = par.quality;

% fn = fieldnames(par);
% for ii = 1:length(fn)
%     if isprop(wobj, fn{ii})
%         wobj.(fn{ii}) = par.(fn{ii});
%     end
% end

open(wobj);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Interpolate view positions 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Make coloumn vectors
az = par.viewaz(:);
el = par.viewel(:);

totnf = (length(az)-1)*par.nstep + 1;

vaz(1) = az(1);
vel(1) = el(1);

for ii = 1:length(az)-1
    azr = (az(ii+1) - az(ii)) / par.nstep;
    elr = (el(ii+1) - el(ii)) / par.nstep;
    
    vaz = [vaz; (az(ii)+azr : azr : az(ii+1))'];
    vel = [vel; (el(ii)+elr : elr : el(ii+1))'];
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Render frames 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

figure(hf);

if (~par.showaxes)
    axis off
    set(gca,'Position',[0 0 1 1])
end

disp('File will be saved as:');
disp(fname);
fprintf('Working on frame:      ');

gauge = GtGauge(totnf, 'Working on frame: ');
for ii = 1:totnf
    gauge.incrementAndDisplay();
    view(vaz(ii), vel(ii));
    frame = getframe(hf);
    drawnow();
    writeVideo(wobj, frame);
    mov(ii) = frame;
end
gauge.delete();

if (par.showmov)
    hm = figure();
    movie(hm, mov, 1, wobj.FrameRate, [0 0 0 0]);
end

close(wobj);

setappdata(hf,'AppData',par)

end % end of function

