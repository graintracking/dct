function gtVGStudioLUT(map, filename)
% GTVGSTUDIOLUT  Function to write a colour look up table for VGStudio
%
%     gtVGStudioLUT(map, varargin)
%     ---------------------------------------------------------------------
%
%     INPUT:
%       map      = <double>  Color map to write
%
%     OPTIONAL INPUT:
%       filename = <string>  Path to the output file {'VGStudio.lut'}
%
%     Note:
%       VGStudio sometimes seems to do funny things with the colour limits
%       / colour range.  So be careful, what works in matlab might not work
%       correctly in VGS...  Consider yourselves warned!
%
% Andy King, 11/2009

if ~exits('filename', 'var') || isempty(filename)
    filename = 'VGStudio.lut';
else
    [~, ~, fext] = fileparts(filename);
    if ~strcmp(fext, '.lut')
        warning('gtVGStudioLUT:wrong_file_extension', ...
            'VGStudio lookuptable file extension should be ''.lut''!');
    end
end

fid = fopen(filename, 'wt');

fprintf(fid,'; Color look up table\n\n');

N = size(map, 1);
index = ([1:N] - 0.5)/10
amap = ((255*map) + 0.5)';

output = [index ; amap];

fprintf(fid, '%f %f %f %f\n', output);

fclose(fid);

end % end of function
