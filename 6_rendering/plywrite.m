function plywrite(pv,filename,isascii)
% PLYWRITE.M A simple PLY (Stanford polygon) file writer (pv is the form of
% output from isosurface)
% Greg Johnson, August 2002 (November 2006)
%
%pv.faces=pv.faces(1:100:end,:);
  
nvertices=size(pv.vertices,1);
nfaces=size(pv.faces,1);

verticesperface=size(pv.faces,2);

fid=fopen(filename,'w');
fprintf(fid,'ply\n');
if isascii
  fprintf(fid,'format ascii 1.0\n');
else
  fprintf(fid,'format binary_little_endian\n');
end
fprintf(fid,'comment made by matlab\n');
fprintf(fid,'element vertex %d\n',nvertices);
fprintf(fid,'property float x\n');
fprintf(fid,'property float y\n');
fprintf(fid,'property float z\n');
fprintf(fid,'element face %d\n',nfaces);
fprintf(fid,'property list uint32 uint32 vertex_index\n');
fprintf(fid,'end_header\n');

if isascii % ascii format file
  disp('ascii')
  for n=1:nvertices
    fprintf(fid,'%3.3f %3.3f %3.3f\n',pv.vertices(n,:));
  end
  for n=1:nfaces
    fprintf(fid,'%d %d %d %d\n',[size(pv.vertices,2) pv.faces(n,:)-1]);
  end
else  % binary format file
  disp('binary')
  for n=1:nvertices
    fwrite(fid,pv.vertices(n,:),'float32');
  end
  for n=1:nfaces
    fwrite(fid,[size(pv.vertices,2) pv.faces(n,:)-1],'uint32');
  end
end



% 0 0 0                      { start of vertex list }
% 0 0 1
% 0 1 1
% 0 1 0
% 1 0 0
% 1 0 1
% 1 1 1
% 1 1 0
% 4 0 1 2 3                  { start of face list }
% 4 7 6 5 4
% 4 0 4 5 1
% 4 1 5 6 2
% 4 2 6 7 3
% 4 3 7 4 0
fclose(fid);
