function rgb = gtVectorOrientationColor(vectors)
% GTVECTORORIENTATIONCOLOR Convert a list of unit vectors into RGB color values
%
%     rgb = gtVectorOrientationColor(vectors)
%     -------------------------------------------------------------------------
%     The output RGB color values describe orientation with HSL code which use:
%     - psi to give brightness
%     - phi to give color
%
%     INPUT:
%       vectors = <double>  M row vectors (Mx3 matrix)
%
%     OUTPUT:
%       rgb     = <double>  Red-Green-Blue color values 
%
%     Version 001 16-10-2012 by YGuilhem

    psi = acos(vectors(:, 3));
    phi = atan2(vectors(:, 2), vectors(:, 1));

    % Get everything in the right interval
    dummy = find(psi > pi/2);
    psi(dummy) = pi - psi(dummy);

    % If we change psi, should also change phi
    phi(dummy) = phi(dummy) + pi;
    phi = mod(phi, 2*pi);

    h = phi/(2*pi);               % Hue
    s = ones(size(h));            % Saturation
    l = 0.9 - (0.8*(psi/(pi/2))); % Luminance (white //z, black in x-y plane)

    rgb = hsl2rgb([h s l]);   % Turn HSL into RGB

end % end of function
