function [app_pars, app_list] = gtAppDrawGrainPars(grains)
% GTAPPDRAWGRAINPARS  
% [app_pars, app_list] = gtAppDrawGrainPars(grains)
%
%       ids         = <int>          list of grain IDs of interest; by default all
%                                    the grains are drawn
%       cmap        = <double>       can be a component of the strain tensor
%                                    {grain.strain.strainT(3,3)} or a color map
%       hlight      = <int>          grain id-s to be highlighted {[]}
%       linecolor   = <double>       color for edges (1x3) {[0 0 0],[1 0 0],...}
%       strain      = <double>       scale factor for the strain values, if wanted. {0}
%                                    (zero to use the unstrained unit cell)
%       alpha       = <double>       transparency value for patches {1}
%       phaseid     = <int>          phase number {1}
%       patch       = <logical>      true if drawing the patch of the unit cell <true>
%       pxsize      = <double>       pixel size (mm/px) {0.001}
%       ratio       = <double>       c/a ratio; if empty, taken from parameters.cryst {[]}
%       type        = <string>       unit cell type; if empty, taken from parameters.cryst {''}
%       label       = <string>       name for entry in the legend {''}
%
%       orig        = <double>       sample reference (arrows) origin (mm)
%       caxis       = <logical>      flag to draw the c-axis {false}
%       pixels      = <logical>      flag to switch to pixels {false}
%       draw        = <logical>      Draw grain patches {true}
%       scale       = <logical>      Scale grain size {true}
%       size        = <double>       Factor to scale all the grains equally
%                                    {[]}
%       translate   = <logical>      Translate grain centers {true}
%
%       section     = <logical>      Draw a section for each grain {false}
%       secpos      = <double>       Section position {[]}
%       plane       = <string>       Section plane {'xy'}
%       paired      = <logical>      paired grains drawing useful for twins
%                                    One figure for each pair {false}
%
%       figure      = <logical>      Display or not the figure {true}
%       figpos      = <double>       figure position in pixels (1x4) {[100 100 700 500]}
%       figcolor    = <double>       figure background color {[0 0 0]}
%       legend      = <logical>      display or not the legend {false}
%       legendpos   = <double>       location of the legend box {'NorthEast'}
%       showaxes    = <string>       Shows axes or not {'on'}
%       sampleaxes  = <logical>      Draw sample axes {true}
%       sampleenv   = <logical>      Draw sample envelope {true}
%       zoom        = <double>       zoom in of this quantity {[]}
%       view        = <double>       3D angular view (1x2) {[-45 20]}
%       debug       = <logical>      print comments {false}
%
%       hf          = <handle>       existing figure handle {[]}
%       ha          = <handle>       existing axes handle {[]}


app_pars.ids        = arrayfun(@(num) 1:numel(grains{num}), 1:numel(grains), 'UniformOutput', false);
app_pars.cmap{1}    = [];
app_pars.hlight{1}  = [];
app_pars.linecolor  = {[0 0 0],[1 0 0],[0 1 0],[0 0 1],[1 1 0],[1 0 1],[0 1 1],[1 1 1]};
app_pars.strain     = cellfun(@(num) 0, grains, 'UniformOutput', false);
app_pars.alpha      = cellfun(@(num) 1, grains, 'UniformOutput', false);
app_pars.phaseid    = cellfun(@(num) 1, grains, 'UniformOutput', false);
app_pars.patch      = cellfun(@(num) true, grains, 'UniformOutput', false);
app_pars.pxsize{1}  = 0.001; % mm
app_pars.ratio{1}   = [];
app_pars.type{1}    = '';
app_pars.label{1}   = '';

app_pars.orig       = []; % sample ref(arrows) origin
app_pars.caxis      = false;
app_pars.pixels     = false;
app_pars.draw       = true;
app_pars.scale      = true;
app_pars.size       = 20;
app_pars.translate  = true;

app_pars.section    = false;
app_pars.secpos     = [];
app_pars.plane      = 'xy';
app_pars.paired     = false;

app_pars.figure     = true;
app_pars.figpos     = [100 200 900 500];
app_pars.figcolor   = [1 1 1];
app_pars.legend     = false;
app_pars.legendpos  = 'NorthEastOutside';
app_pars.showaxes   = 'on';
app_pars.sampleaxes = true;
app_pars.sampleenv  = true;
app_pars.zoom       = [];
app_pars.view       = [-45 20];
app_pars.debug      = false;

app_pars.hf         = [];
app_pars.ha         = [];

app_list = get_structure(app_pars,[],false,[],[]);
app_list(:,2) = {'list of grain IDs of interest; by default all the grains are drawn',...
    'can be a component of the strain tensor or a color map',...
    'grain IDs to be highlighted',...
    'color for edges (1x3)',...
    'scale factor for the strain values, zero to use the unstrained unit cell',...
    'transparency value for patches',...
    'phase number',...
    'true if drawing the patch of the unit cell',...
    'pixel size (mm/px)',...
    'c/a ratio',...
    'unit cell crystal type',...
    'names for entries in the legend',...
    'sample reference (arrows) origin (mm)',...
    'flag to draw the c-axis',...
    'flag to switch to pixels',...
    'flag to draw grain patches',...
    'flag to scale grain size',...
    'Factor to scale all the grains equally',...
    'flag to translate grain centers',...
    'Draw a section for each grain',...
    'Section position',...
    'Section plane',...
    'paired grains drawing useful for twins One figure for each pair',...
    'Display or not the figure',...
    'figure position in pixels (1x4)',...
    'figure background color',...
    'display or not the legend',...
    'location of the legend box',...
    'Shows axes or not',...
    'Draw sample axes',...
    'Draw sample envelope',...
    'zoom in of this quantity',...
    '3D angular view (1x2)',...
    'print comments',...
    'existing figure handle',...
    'existing axes handle'};

end
