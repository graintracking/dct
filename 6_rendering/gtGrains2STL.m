%gtGrains2STL

% converts all grains into STL polygon meshes for rendering

d=dir('4_grains/grain*');  % get all grain directories

for n=1:length(d)
  
  d2=dir(['4_grains/' d(n).name '/*.edf']);
  if isempty(d2)
    continue
  end
  fprintf('Looking at %s\n',d2.name);
  matname=sprintf('4_grains/%s/%s.mat',d(n).name,d(n).name);
  load(matname)

  edfname=sprintf('4_grains/%s/%s',d(n).name,d2.name);
  
  vol=edf_read(edfname);
  imagesc(vol(:,:,round(size(vol,3)/2)))
  title(num2str(n))
  drawnow,shg
  fv=isosurface(vol,0.5);
  fprintf('Moving to %.2f,%.2f,%.2f\n',vol_x_origin,vol_y_origin,vol_z_origin);
  fv.vertices=fv.vertices+repmat([vol_x_origin vol_y_origin vol_z_origin],length(fv.vertices),1);
  fname=sprintf('6_render/%s.stl',d(n).name);
  stlwrite2(fv,fname,'ascii')
end
 

