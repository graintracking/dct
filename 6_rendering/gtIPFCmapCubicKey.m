function hf = gtIPFCmapCubicKey(N, saturate, label, fontsize, drawing)
% GTIPFCMAPCUBICKEY  Show inverse pole figure color space in the cubic SST
%
%     hf = gtIPFCmapCubicKey([N], [saturate], [label], [fontsize], [drawing])
%     -----------------------------------------------------------------------
%
%     INPUT:
%       N        = <int>       Number of angular steps (quality of the plot) {30}
%       saturate = <logical>   Saturate or not the color in the SST {true}
%       label    = <logical>   Draw poles label {false}
%       fontsize = <int>       Font size for labels {18}
%       drawing  = <logical>   Draw the patch with RGB colors {true}
%
%     OUTPUT:
%       hf       = <handle>    Resulting figure handle


if ~exist('N', 'var') || isempty(N)
    N = 30;
elseif N < 15
    warning('gtIPFCmapCubicKey:wrong_parameters', ...
        'Cannot use less than 15 angular steps, so using 15 steps.');
    N = 15;
end
if ~exist('saturate', 'var') || isempty(saturate)
    saturate = true;
end
if ~exist('label', 'var') || isempty(label)
    label = false;
end
if ~exist('fontsize', 'var') || isempty(fontsize)
    fontsize = 18;
end
if ~exist('drawing', 'var') || isempty(drawing)
    drawing = true;
end

hf = figure();
hold on;

% Work in radians
phimin =    0;
phimax = pi/4;
inc = phimax/N;

psimin = 0;
psimax = atan(sqrt(2))+inc;

chimax = pi/4;

for phi=phimin:inc:(phimax-inc)     % Rotation around z-axis
    for psi=psimin:inc:(psimax-inc) % Out from z-axis to vector

        % For cubic colour map, we need two angles:
        % - phi (around z-axis)
        % - chi (around y-axis)
        z = cos(psi);
        x = sin(psi)*cos(phi);
        chi = atan2(x, z); % in sst chi 0->pi/4
        if chi > chimax + inc/2
            continue;
        end

        ratioR = 1 - chi/chimax;
        ratioB =     phi/phimax;

        red   = ratioR;
        green = (1 - ratioR) * (1 - ratioB);
        blue  = (1 - ratioR) * ratioB;
        rgb = [red green blue];

        % Ensure that we stay in [0 1]
        rgb(rgb < 0) = 0;
        rgb(rgb > 1) = 1;

        if saturate
            rgb = gtCrystSaturateSSTColor(rgb);
        end

        % Radius distorted for stereographic proj
        r_patch = tan([psi/2 (psi+inc)/2]);
        [phi_patch, r_patch] = meshgrid([phi phi+inc]', r_patch);
        [x_patch, y_patch] = pol2cart(phi_patch, r_patch);

        if drawing
            patch(x_patch, y_patch, rgb, 'EdgeColor', 'none', 'Faces', [1 2 4 3]);
        end
    end
end

% Add poles and tart up like gtMakePoleFigure
set(gca, 'XTickLabel', '');
set(gca, 'YTickLabel', '');
set(gca, 'GridLineStyle', 'none');
set(gca, 'Ycolor', 'w');
set(gca, 'Xcolor', 'w');

axis tight
axis equal;
set(gca, 'xlim', [-0.01 0.45]);
set(gca, 'ylim', [-0.03 0.39]);

% Add a black border around triangle
% For cubic:
% - line from [0 0 1] to [1 0 1] (phi=0)
% - arc  from [1 0 1] to [1 1 1] (phi=[0 pi/4], chi=pi/4) i.e z=x
% - line from [1 1 1] to [0 0 1] (phi=pi/4)
line_phi = [0:inc:phimax phimax]';
line_points = [cos(line_phi) sin(line_phi) cos(line_phi)];

% Normalize
line_points = line_points./repmat(sqrt(sum(line_points.*line_points, 2)), 1, 3);

% Convert to polar coordinates
line_psi = acos(line_points(:, 3));
line_r   = tan(line_psi/2);

% Stereographic projection x-y
[line_x, line_y] = pol2cart(line_phi, line_r);
xp = [line_x; 0;  line_x(1)];
yp = [line_y; 0;  line_y(1)];
plot(xp, yp, 'k-');

% Add a white patch to mask the rough outer edge of the triangle
xp = [line_x ; line_x(end) ; 0.45  ; 0.45 ; line_x(1)];
yp = [line_y ; 0.39        ; 0.39  ; 0    ; 0        ];
patch(xp, yp, 'w', 'EdgeColor', 'none');

% Add labels for the 3 corners poles
plot(0, 0, 'k*');
text(0,      0,     '[001]', 'fontsize', fontsize, ...
    'HorizontalAlignment', 'left',  'VerticalAlignment',' top');
plot(0.4142, 0, 'k*');
text(0.4142, 0,     '[101]', 'fontsize', fontsize, ...
    'HorizontalAlignment', 'right', 'VerticalAlignment', 'top');
plot(0.366, 0.366, 'k*');
text(0.366,  0.366, '[111]', 'fontsize', fontsize, ...
    'HorizontalAlignment', 'right',  'VerticalAlignment', 'bottom');

if label
    uvw_poles(1).n = '[102]'; uvw_poles(1).x = 0.236;  uvw_poles(1).y = 0;      % 102
    uvw_poles(2).n = '[112]'; uvw_poles(2).x = 0.2247; uvw_poles(2).y = 0.2247; % 112
    uvw_poles(3).n = '[213]'; uvw_poles(3).x = 0.3;    uvw_poles(3).y = 0.153;  % 213
    uvw_poles(4).n = '[212]'; uvw_poles(4).x = 0.4;    uvw_poles(4).y = 0.2;    % 212
    %uvw_poles(5).n = '[159]'; uvw_poles(5).x = 0.2584; uvw_poles(5).y = 0.0516; % 159

    plot([uvw_poles.x], [uvw_poles.y], 'k*');
    for ii=1:length(uvw_poles)
        text(uvw_poles(ii).x, uvw_poles(ii).y, uvw_poles(ii).n, ...
            'fontsize', fontsize, ...
            'HorizontalAlignment','left', 'VerticalAlignment','top');
    end
end

set(gcf, 'Color', [1 1 1])
set(gca, 'Visible', 'off');

% label and sample direction to plot
text(0,0.36,0,'IPF', 'fontsize', fontsize, ...
    'VerticalAlignment','top','HorizontalAlignment','left');

end % end of function
