function stlwrite(stl,filename,varargin)
% STLWRITE.M A simple STL (stereolithography) file writer
% Greg Johnson, August 2002
% greg.johnson@ieee.org
%
% Usage: stlwrite(stl,filename,{'ascii'/'binary'}
% where stl is the variable containing the mesh
%       filename is the output filename
% The file will be a binary STL unless the optional third argument is
% 'ascii'
%
% The stl variable has the following components:
% stl.verticesX (Y,Z) - the locations of the vertices - three per triangle
%
% See also: stlread

if nargin==2
  filetype='binary';
elseif nargin==3
  if strcmp(varargin{1},'ascii')
    filetype='ascii';
  else
    filetype='binary';
  end
else
  disp('Not quite sure what you''re asking...');
  return
end

if strcmp(filetype,'ascii')
  disp('Writing ASCII STL file');  
  fid=fopen(filename,'wt');
  fprintf(fid,'SOLID %s\n',stl.name);
  for ndx_facet=1:length(stl.verticesX)
    mat=[diff(stl.verticesX(:,ndx_facet)) diff(stl.verticesY(:,ndx_facet)) diff(stl.verticesZ(:,ndx_facet))];
    normal=[det(mat(:,[2 3])) det(mat(:,[3 1])) det(mat(:,[1 2]))];
    normal=normal/(norm(normal)+eps);
    fprintf(fid,'  FACET NORMAL %1.1f %1.1f %1.1f\n',normal(1),normal(2),normal(3));
    fprintf(fid,'    OUTER LOOP\n');
    for ndx_vertex=1:3
      fprintf(fid,'      VERTEX %1.1f %1.1f %1.1f\n',...
        stl.verticesX(ndx_vertex,ndx_facet),...
        stl.verticesY(ndx_vertex,ndx_facet),...
        stl.verticesZ(ndx_vertex,ndx_facet));
    end
    fprintf(fid,'    ENDLOOP\n');
    fprintf(fid,'  ENDFACET\n');
  end
  fprintf(fid,'ENDSOLID %s\n',stl.name);
  fclose(fid);
else
  disp('Writing binary STL file');
  disp('NOT YET!');
end

