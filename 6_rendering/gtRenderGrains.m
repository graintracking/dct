function [h, gids] = gtRenderGrains(grains, varargin)
% GTRENDERGRAINS  Plots a 3D rendered image from a grain map
%     [h, gids] = gtRenderGrains(grains, varargin)
%     --------------------------------------------
%
%     Attention! It can take very long if the number of grains is large.
%     Note that displayed volume is shifted with one pixel in Z.
%
%     If more than one connected grains are rendered, the fastest way is to 
%     do it on a dilated volume (the more space filling, the faster) with the
%     default patchmode='free_interp' option.
%     
%     After rendering you can use the mouse with the "Rotate 3D' tool or 
%     'view' command to change the view (e.g. view(20, 60)), and the
%     'camlight' command to add illumination (e.g. camlight('headlight') or 
%     camlight(-45, -45)), if some grains are poorly visible.
%
%     For more information on rendering, see Lighting Overview in the
%     Matlab help.
%
%     Uses 'imdilate' from the Matlab Image Toolbox. If not available,
%     rendering quality may be bad.
%
%     Other useful commands:
%       change view:
%           view(45, 15)
%       add light:
%           hl = camlight(45, 15)
%           hl = camlight('headlight')
%       change light strength: 
%           set(h.patch, 'AmbientStrength',  0.1)
%           set(h.patch, 'DiffuseStrength',  0.1)
%           set(h.patch, 'SpecularStrength', 0.1)
%       inspect and change object properties:
%           inspect(h.light)
%           get(h.light, 'Position')
%           set(h.light, 'Position', [100 0 200])
%           delete(h.light)
%           ud = get(h.patch, 'UserData')
%
%     INPUT:
%       grains = labelled grain map (e.g. as in volume_dilated.mat);
%                3D matlab array with coordinates 1st-2nd-3rd = X-Y-Z
%
%     OPTIONAL INPUT - varargin:
%       grainids  = grain ID-s to be shown; if empty, all are considered
%       zmark     = marks a given Z layer {empty}
%       xkeep     = X region to keep; grains that touch this region will 
%                   be displayed {1:size(grains, 2)}
%       ykeep     = Y region to keep {1:size(grains, 1)}
%       zkeep     = Z region to keep {1:size(grains, 3)}
%       xcut      = X region to cut out; grains that touch this region will
%                   be excluded completely (e.g. xcut=1:100); {empty}
%       ycut      = Y region to cut out {empty}
%       zcut      = Z region to cut out {empty}
%       patchmode = patch mode:
%                     'all'          - the surface of each grain will be rendered; 
%                     'free_flat'    - only visible free surfaces will be 
%                                      rendered (faster); flat face colouring;
%                                      grain boundaries might look worse
%                     {'free_interp'}- only visible free surfaces will be
%                                      rendered; RGB colours are
%                                      interpolated on the grain boundaries
%       cmap      = colormap (nx3); if empty, gtRandCmap is used; cmap(1, :)
%                   is used for grainID=1, etc.
%       showaxes  = show axes in figure {true}
%       showlabel = show axes labels {true}
%       grid      = show grid in figure {true}
%       box       = show box {'on'} | 'off'
%       viewaz    = azimuth angle of view {30}
%       viewel    = elevation angle of view {40}
%       lighting  = light rendering method ({'phong'}/'gouraud'/'flat'/'none')
%       lightaz   = light direction azimuth angle {45}
%       lightel   = light direction elevation angle {45}
%       ambient   = strength of ambient light {0.5}
%       diffuse   = strength of diffuse reflected light {0.5}
%       specular  = strength of specular (directional) reflected light {0.3}
%
%
%     OUTPUT:
%       h.fig   = handle of figure
%       h.axes  = handle of axes containing the plot
%       h.patch = handle(s) of the plotted isosurface patches
%       h.light = handle of light object
%       gids    = final grain ID-s displayed
%       ID-s of plotted grains and the colormap are stored in the patch
%       userdata: ud = get(h.patch, 'UserData')
%
%
%   Version 003 13-01-2013 by PReischig
%     Improved functionality. 
%   Version 002 19-09-2012 by PReischig
%     Cleaned and pimped.
%   Version 001 by gtTeam
%

par.grainids  = [];
par.patchmode = 'free_interp';
par.xcut      = [];
par.ycut      = [];
par.zcut      = [];
par.xkeep     = 1:size(grains, 1);  % volume is transposed
par.ykeep     = 1:size(grains, 2);  % volume is transposed
par.zkeep     = 1:size(grains, 3);
par.cmap      = [];
par.grain_alpha = [];
par.zmark     = [];
par.showaxes  = true;
par.showlabel = true;
par.grid      = true;
par.box       = 'on';
par.reverse_x = false;
par.reverse_y = false;
par.reverse_z = false;
par.viewaz    = 30;
par.viewel    = 40; 
par.lighting  = 'phong';
par.lightaz   = 45;
par.lightel   = 45;
par.ambient   = 0.5;
par.diffuse   = 0.5;
par.specular  = 0.3;

par.phase_id  = 1;
par.r_vectors = [];
par.ipf_axis  = [];

par.font_size = 10;
par.font_weight = 'normal';

par.sampleaxes = false;
par.sampleenv  = true;
par.pxsize     = [];
par.pixels     = false;
par.hf         = [];
par.ha         = [];
par.labels     = {'X', 'Y', 'Z'};
par.permute_axes = 1:3;
par.orig       = [];
par.parameters = [];
[par, rej_pars] = parse_pv_pairs(par, varargin);
if (~isempty(rej_pars))
    fprintf('Rejected parameters:\n')
    disp(rej_pars)
end

tic();

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Grain ID-s to be rendered
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

grains = permute(grains, par.permute_axes);

keep_slices = {par.xkeep, par.ykeep, par.zkeep};
keep_slices = keep_slices(par.permute_axes);

% Grains ID-s to be shown
% (X-Y are exchanged !)
grainids = unique( grains(keep_slices{1}, keep_slices{2}, keep_slices{3}) );
if ~isempty(par.grainids)
    grainids = intersect(grainids, par.grainids, 'stable');
end

cut_slices = {par.xcut, par.ycut, par.zcut};
cut_slices = cut_slices(par.permute_axes);

% Grains ID-s to be cut
% (X-Y are exchanged !)
cutids   = unique( grains(cut_slices{1}, cut_slices{2}, cut_slices{3}) );
grainids = setdiff(grainids, cutids);

% Remove background and conflicting grains
grainids = setdiff(grainids, [-1 0]);
gids     = grainids'; 

if isempty(grainids)
    disp('No grains to display.')
    h.fig   = [];
    h.axes  = [];
    h.patch = [];
    h.light = [];
    return
end

% Transpose X-Y for Matlab's orientation
grains = permute(grains, [2 1 3]);

fprintf('No. of grains to render: %d\n', length(grainids))

% Pad grain map to avoid missing patches on the edge
% Image Processing Toolbox license test before callling padarray.
% This is to avoid crashing when no license is available.
if license('checkout', 'Image_Toolbox')
    grains = padarray(grains, [1 1 1], 0);
else
    grains0 = zeros(size(grains) + [2 2 2], class(grains));
    grains0(2:end-1, 2:end-1, 2:end-1) = grains;
    grains = grains0;
    clear grains0;
end


%%%%%%%%%%%%%%%%%%%%%%
%%% Create patches
%%%%%%%%%%%%%%%%%%%%%%
%h.fig = figure();
%h.axes = axes('parent', h.fig);

if (isempty(par.parameters))
    parameters = gtLoadParameters();
else
    parameters = par.parameters;
end
labgeo  = parameters.labgeo;
if (isempty(par.pxsize))
    par.pxsize = mean(parameters.recgeo(1).voxsize);
end

if isempty(par.hf)
    if (par.pixels)
        rad = labgeo.samenvrad;
        bot = labgeo.samenvbot;
        top = labgeo.samenvtop;
%         dims = size(grains);
%         par.orig = dims / 2;
        h_tmp = gtDrawSampleGeometry(rad, bot, top, 'centered', false, ...
            'orig', par.orig, 'pxsize', par.pxsize, 'max', 100, ...
            'axes', par.sampleaxes, 'sample', par.sampleenv);
    else
        h_tmp = gtDrawSampleGeometry(labgeo, 'centered', true, ...
            'orig', par.orig, 'axes', par.sampleaxes, 'sample', par.sampleenv);
    end
    h.fig = h_tmp.fig;
    par.hf = h_tmp.fig;
else
    h.fig = par.hf;
end

par.rot3d = h_tmp.rot3d;
if (par.sampleenv)
    par.senv  = h_tmp.senv;
end
if (par.sampleaxes)
    par.saxes = h_tmp.axis;
end

if isempty(par.ha)
    if ~isempty(gca)
        h.axes = get(h.fig, 'CurrentAxes');
    else
        h.axes = axes('Parent', h.fig);
    end
else
    h.axes = par.ha;
end

if (isempty(par.cmap))
    if (~isempty(par.ipf_axis))
        cryst_system = parameters.cryst(par.phase_id).crystal_system;
        cryst_spacegroup = parameters.cryst(par.phase_id).spacegroup;
        symm = gtCrystGetSymmetryOperators(cryst_system, cryst_spacegroup);

        if (isempty(par.r_vectors))
            sample = GtSample.loadFromFile();
            par.r_vectors = sample.phases{par.phase_id}.R_vector;
        end

        [par.cmap, ~, ~] = gtIPFCmap(par.phase_id, par.ipf_axis, ...
            'r_vectors', par.r_vectors, ...
            'crystal_system', cryst_system, ...
            'background', false, ...
            'symm', symm);
    else
        par.cmap = gtRandCmap(max(grainids)+1);
        par.cmap(1, :) = [];
    end
elseif (ischar(par.cmap))
    par.cmap = eval(sprintf('%s(%d)', par.cmap, max(grainids)+1));
end

set(h.fig, 'Colormap', par.cmap);

switch (par.patchmode)
    
    case 'all'
        % Loop through all active grains and create patch
        gauge = GtGauge(length(grainids), 'No. of grains processed: ');
        for ii = 1:length(grainids)
            gauge.incrementAndDisplay();

            vol_bin    = (grains == grainids(ii));
            vol_smooth = smooth3(vol_bin, 'box', 3);

            xx = (1:size(vol_smooth, 2)) * par.pxsize;
            yy = (1:size(vol_smooth, 1)) * par.pxsize;
            zz = (1:size(vol_smooth, 3)) * par.pxsize;

            iso_surf = isosurface(xx, yy, zz, vol_smooth, 0.5);

            h.patch(ii) = patch(iso_surf, 'EdgeColor', 'none', ...
                'FaceColor', par.cmap(grainids(ii), :), 'Parent', h.axes);

            if (~isempty(par.grain_alpha))
                if (iscell(par.grain_alpha))
                    verts_pix = get(h.patch(ii), 'vertices') / par.pxsize;
                    cdata_alpha = interp3(par.grain_alpha{ii}, ...
                        verts_pix(:, 1), verts_pix(:, 2), verts_pix(:, 3), 'linear');

                    set(h.patch(ii), 'FaceAlpha', 'interp', ...
                        'FaceVertexAlphaData', cdata_alpha);
                else
                    set(h.patch(ii), 'FaceAlpha', par.grain_alpha(ii));
                end
            end
        end
        gauge.delete();

    case 'free_flat'
        % Patches are created from free surface, then coloured according 
        % to nearest grain ID.

        vol_bin = false(size(grains));
        gauge = GtGauge(length(grainids), 'No. of grains processed: ');
        for ii = 1:length(grainids)
            gauge.incrementAndDisplay();
            vol_bin(grains==grainids(ii)) = true;
        end
        gauge.delete();
        
        disp('Creating isosurface...')
        vol_smooth = smooth3(vol_bin, 'box', 3);

        xx = (1:size(vol_smooth, 2)) * par.pxsize;
        yy = (1:size(vol_smooth, 1)) * par.pxsize;
        zz = (1:size(vol_smooth, 3)) * par.pxsize;

        iso_surf = isosurface(xx, yy, zz, vol_smooth, 0.5);

        h.patch = patch(iso_surf, 'EdgeColor', 'none', 'Parent', h.axes);
        verts_pix = get(h.patch, 'vertices') / par.pxsize;
   
        % Dilation of active grain ID-s is needed for later interpolation;
        % dilation changes original voxels, thus they need to be reset.
        gr_merged          = zeros(size(grains), 'single');
        gr_merged(vol_bin) = grains(vol_bin);
        gr_merged          = imdilate(gr_merged, ones(3, 3, 3));
        gr_merged(vol_bin) = grains(vol_bin);
        
        % Get vertex colour data from interpolation
        cdata = interp3(gr_merged, ...
            verts_pix(:, 1), verts_pix(:, 2), verts_pix(:, 3), 'nearest');
        
        set(h.patch, 'FaceColor', 'flat', 'FaceVertexCData', cdata, ...
            'CDataMapping', 'direct');

    case 'free_interp'
        % Patches are created from free surface. For improved grain boundary
        % rendering, RGB interpolation is used.

        vol_bin = (grains == grainids(1));
        gauge = GtGauge(length(grainids), 'No. of grains processed: ');
        for ii = 2:length(grainids)
            gauge.incrementAndDisplay();
            vol_bin = vol_bin | (grains == grainids(ii));
        end
        gauge.delete();

        disp('Creating isosurface...')
        vol_smooth = smooth3(vol_bin, 'box', 3);

        xx = (1:size(vol_smooth, 2)) * par.pxsize;
        yy = (1:size(vol_smooth, 1)) * par.pxsize;
        zz = (1:size(vol_smooth, 3)) * par.pxsize;

        iso_surf = isosurface(xx, yy, zz, vol_smooth, 0.5);

        h.patch = patch(iso_surf, 'EdgeColor', 'none', 'Parent', h.axes);
        verts_pix = get(h.patch, 'vertices') / par.pxsize;

        % Dilation needed before interpolation.
        grainsRegion = int16(grains) .* int16(vol_bin);
        
        % Image Processing Toolbox license test before callling imdilate.
        % This is to avoid crashing when no license is available.
        if (license('checkout', 'Image_Toolbox'))
%             dilate_pattern = ones(3, 3, 3);
%             dilate_pattern([1 3 7 9 19 21 25 27]) = 0;
            dilate_pattern = zeros(3, 3, 3);
            dilate_pattern([5 11 13 14 15 17 23]) = 1;
            % maybe: strel('disk', 1) or ones(3, 3, 3)
            dilatedGrains = imdilate(grainsRegion, dilate_pattern);
        else
            warning(['No Matlab Image Toolbox license available.', ...
                    ' Rendering quality may be bad.'])
            dilatedGrains = grainsRegion;
        end
        
        dilatedGrains(vol_bin) = grainsRegion(vol_bin);
        vol_bin = logical(dilatedGrains);

        gr_R = zeros(size(grains), 'single');
        gr_G = gr_R;
        gr_B = gr_R;

        gr_R(vol_bin) = par.cmap(dilatedGrains(vol_bin), 1);
        gr_G(vol_bin) = par.cmap(dilatedGrains(vol_bin), 2);
        gr_B(vol_bin) = par.cmap(dilatedGrains(vol_bin), 3);

        % Colour data of the vetrices
        cdata_R = interp3(gr_R, ...
            verts_pix(:, 1), verts_pix(:, 2), verts_pix(:, 3), 'linear');
        cdata_G = interp3(gr_G, ...
            verts_pix(:, 1), verts_pix(:, 2), verts_pix(:, 3), 'linear');
        cdata_B = interp3(gr_B, ...
            verts_pix(:, 1), verts_pix(:, 2), verts_pix(:, 3), 'linear');

        set(h.patch, 'FaceColor', 'interp', 'FaceVertexCData', [cdata_R, cdata_G, cdata_B]);

        if (~isempty(par.grain_alpha))
            gr_alpha = zeros(size(grains), 'single');
            gr_alpha(vol_bin) = par.grain_alpha(dilatedGrains(vol_bin));
            cdata_alpha = interp3(gr_alpha, ...
                verts_pix(:, 1), verts_pix(:, 2), verts_pix(:, 3), 'linear');
            set(h.patch, 'FaceAlpha', 'interp', ...
                'FaceVertexAlphaData', cdata_alpha);
        end
end

% Save grain ID-s and colormap into patch userdata
userdata.grainids = grainids;
userdata.cmap     = par.cmap;
set(h.patch, 'UserData', userdata);

%%%%%%%%%%%%%%%%%%%%%%
%%% Set light and axes
%%%%%%%%%%%%%%%%%%%%%%
set(h.patch, 'AmbientStrength', par.ambient)
set(h.patch, 'DiffuseStrength', par.diffuse)
set(h.patch, 'SpecularStrength', par.specular)

view(par.viewaz, par.viewel)
set(h.axes, 'Box', par.box)

h.light = camlight(par.lightaz, par.lightel);

axes_names = {'Xdir', 'Ydir', 'Zdir'};

% Reverse axes
if (par.reverse_z)
    set(h.axes, axes_names{par.permute_axes(3)}, 'reverse')
end
if (par.reverse_y)
    set(h.axes, axes_names{par.permute_axes(2)}, 'reverse')
end
if (par.reverse_x)
    set(h.axes, axes_names{par.permute_axes(1)}, 'reverse')
end

axis(h.axes, 'equal')
axis(h.axes, 'vis3d')

camlight(par.lightaz, par.lightel);
lighting(par.lighting)

set(h.axes, 'FontSize', par.font_size, 'FontWeight', par.font_weight);


if (~par.showaxes)
    set(h.axes, 'xTick', []);
    set(h.axes, 'yTick', []);
    set(h.axes, 'zTick', []);
end

if (par.showlabel)
    par.xtext = xlabel(h.axes, par.labels{par.permute_axes(1)}, ...
        'FontSize', par.font_size, 'FontWeight', 'bold');
    par.ytext = ylabel(h.axes, par.labels{par.permute_axes(2)}, ...
        'FontSize', par.font_size, 'FontWeight', 'bold');
    par.ztext = zlabel(h.axes, par.labels{par.permute_axes(3)}, ...
        'FontSize', par.font_size, 'FontWeight', 'bold');
end

if (par.grid)
    grid(h.axes, 'on')
else
    grid(h.axes, 'off')
end

h.volsize = size(grains);

% Draw rectangle at a specified layer
if ~isempty(par.zmark)
    ll = par.zmark + 1;  % original volume was padded
    sx = xlim;
    sy = ylim; 
    line([sx(1) sx(1) sx(2) sx(2) sx(1)]', ...
        [sy(1) sy(2) sy(2) sy(1) sy(1)]', ...
        [ll ll ll ll ll]', 'Color', 'r', 'LineWidth', 1);
end

hr = rotate3d(h.fig);
set(hr, 'RotateStyle', 'box', 'Enable', 'on');

drawnow()

toc()

h.appdata = par;

end
