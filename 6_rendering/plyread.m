% PLYREAD.M A simple PLY (Stanford polygon) file reader
% Greg Johnson, August 2002
%

clear ply
[filename,pathname]=uigetfile('*.ply','Pick a PLY file');


fid=fopen(filename,'rt');

l=upper(fgetl(fid));
if ~strcmp(l,'PLY')
  disp('I don''t think this is a PLY file...');
  return
end

l=upper(fgetl(fid));
[t,r]=strtok(l);
if ~strcmp(t,'FORMAT')
  disp('The file is not formatted correctly');
  return
else
  [ply.filetype,r]=strtok(r);
  [ply.fileversion,r]=strtok(r);
end

ply.comments=[];
ply.elements=[];

while (1)  % read header
  l=upper(fgetl(fid));
  [t,r]=strtok(l);
  switch (t)
    case 'COMMENT'
      disp(['Comment: ' r])
      ply.comments{end+1}=r;
    case 'ELEMENT'
      [t,r]=strtok(r);
      ply.elements{end+1}.name=t;
      ply.elements{end}.quantity=str2num(r);
      ply.elements{end}.property=[];  % initialise properties to empty matrix
      status=t;

    case 'PROPERTY'
      [t,r]=strtok(r);
      if ~strcmp(t,'LIST')
        ply.elements{end}.property{end+1}.name = r(2:end);
        ply.elements{end}.property{end}.datatype=t;
        ply.elements{end}.property{end}.dataset=[];
      else

        [t,r]=strtok(r);
        ply.elements{end}.property
        disp('Don''t know how to handle LIST properties yet...');
      end

    case 'END_HEADER'
      break;
  end
end



% ply
% format ascii 1.0           { ascii/binary, format version number }
% comment made by anonymous  { comments keyword specified, like all lines }
% comment this file is a cube
% element vertex 8           { define "vertex" element, 8 of them in file }
% property float32 x         { vertex contains float "x" coordinate }
% property float32 y         { y coordinate is also a vertex property }
% property float32 z         { z coordinate, too }
% element face 6             { there are 6 "face" elements in the file }
% property list uint8 int32 vertex_index { "vertex_indices" is a list of ints }
% end_header                 { delimits the end of the header }
% 0 0 0                      { start of vertex list }
% 0 0 1
% 0 1 1
% 0 1 0
% 1 0 0
% 1 0 1
% 1 1 1
% 1 1 0
% 4 0 1 2 3                  { start of face list }
% 4 7 6 5 4
% 4 0 4 5 1
% 4 1 5 6 2
% 4 2 6 7 3
% 4 3 7 4 0

if strcmp(ply.filetype,'ASCII')
  disp('Reading ASCII PLY file');
  for element_ndx=1:length(ply.elements)
    for item_ndx=1:ply.elements{element_ndx}.quantity
      l=fgetl(fid);
      values=strread(l,'%f');
      for property_ndx=1:length(ply.elements{element_ndx}.property)
        ply.elements{element_ndx}.property{property_ndx}.dataset(end+1)=...
          values(property_ndx);
      end
    end
  end
else
  disp('Reading binary PLY file')

end
fclose(fid);

