classdef GtColormap < handle
% GTCOLORMAP  Performs advaced colormap operations
%
% cmap = GtColormap;
%       Returns current colormap, much like COLORMAP does,
%       but new figure is not created
%
% GtColormap('SetControls')
% h = GtColormap('SetControls')
%       Use this commands to add GTCOLORMAP controls
%       to current figure.
%
% GtColormap('HideControls')
% h = GtColormap('HideControls')
%       Use this commands to hide GTCOLORMAP controls
%       on current figure (controls are not deleted for later use).
%
% GtColormap('ShowControls')
% h = GtColormap('ShowControls')
%       Use this commands to show GTCOLORMAP controls
%       on current figure which were previously hidden by
%       HIDECONTROLS command.
%
% GtColormap('ToggleControls')
% h = GtColormap('ToggleControls')
%       Use this commands to toggle visibility of
%       GTCOLORMAP controls on current figure.
%
% GtColormap('DeleteControls')
% h = GtColormap('DeleteControls')
%       Use this commands to permanently delete
%       GTCOLORMAP controls from current figure.
%
% cmaps = GtColormap('getMatlabCmaps')
%       Returns cell array of strings of all predefined colormaps.
%
% GtColormap('ShowSupportedColormaps')
% GtColormap('ShowSupportedColormaps', 'OnBlack')
% GtColormap('ShowSupportedColormaps', 'OnWhite')
%       Displays all predefined colormaps as a color+grayscale image
%       or white (default) or black background.
% img = GtColormap('ShowSupportedColormaps', ...)
%       The image is returned if output parameter is specified.
%
% str = GtColormap('GetCurrentColormapString')
%       Returns current colormap string.
%       May not work properly if colormap was modified with commands below.
%
% GtColormap('Reverse')
% GtColormap('Invert')
% GtColormap('ShiftUp')
% GtColormap('ShiftDown')
% GtColormap('RGB>RBG')
% GtColormap('ShiftRight')
% GtColormap('RGB>GRB')
% GtColormap('Mirror')
% GtColormap('ShiftLeft')
% GtColormap('Brighten')
% GtColormap('Brighten', Amount)
% GtColormap('Darken')
% GtColormap('Darken', Amount)
%       Performs GT command on current colormap.
%
% GtColormap('GtColormap.char2RGB(cGroups{kk}(p), obj.colorTable, obj.colorCodes)Value')
%       Tries to adjust current colormap so its gray values grow linearly.
%
% GtColormap(MatLabColorMapName)
% GtColormap(MatLabColorMapName, Length)
%       This is equivalent to MatLab
%           colormap(cmap)
%       and
%           colormap(cmap, n)
%       respectively.
%
% GtColormap(ColorMapString)
% GtColormap(ColorMapString, Length)
% GtColormap(ColorMapString, Length, ColorPositions)
% GtColormap(ColorMapString, Length, ColorPositions, InterpolationMode)
%       ColorMapString      String representation of colormap
%                           Each character represents one color
%                           and must belong to the following set
%
%                               char    color           R-  G-  B- values
%                               k       black           0.0 0.0 0.0
%                               r       red             1.0 0.0 0.0
%                               p       pink            1.0 0.0 0.5
%                               o       orange          1.0 0.5 0.0
%                               g       green           0.0 1.0 0.0
%                               l       lime            0.5 1.0 0.0
%                               a       aquamarine      0.0 1.0 0.5
%                               b       blue            0.0 0.0 1.0
%                               s       sky blue        0.0 0.5 1.0
%                               v       violet          0.5 0.0 1.0
%                               c       cyan            0.0 1.0 1.0
%                               m       magenta         1.0 0.0 1.0
%                               y       yellow          1.0 1.0 0.0
%                               w       white           1.0 1.0 1.0
%
%                           If the string does not contain spaces, each
%                           character represents one color in colormap.
%                           For example, 'kryw' defines black-red-yellow-white (hot-like) colormap
%                           If string contains spaces then each group of space-delimited characters
%                           define one color. RGB components of the color are formed
%                           by averagin of RGB components of each color from the group.
%                           For example, 'bw k rw' defines lightblue-black-lightred colormap
%
%                           Colors in the generated colormap are evenly spaced
%                           covering values from 0 to 1
%       Length              Length of generated colormap
%       ColorPositions      Vector of color positions
%                           If colors from the ColorMapString should be
%                           placed at custom data values, provide their positions in this vector.
%                           Length of the ColorPositions should be equal
%                           to length of the ColorMapString if ColorMapString does not contain spaces
%                           or to number of color groups in ColorMapString
%       InterpolationMode   'nearest'   {'linear'}    'spline'    'cubic'
%                           Interpolation mode for colormap
%                           See INTERP2 for details
%
% cmap = GtColormap(ColorMapString, ...)
%       GTCOLORMAP returns applied colormap to caller
%
% ctable = GtColormap(ColorMapString, 0);
% [ctable, cpos] = GtColormap(ColorMapString, 0);
%       When requested length of colormap is zero, GTCOLORMAP returns
%       original table of colors used to generate the colormap in CTABLE.
%       This can be applied only for predefined or generated-on-the-fly colormaps.
%       GTCOLORMAP also returns vector CPOS of positions of the colors returned in CTABLE
%
% Examples
%       By inverting and reversing standard BONE colormap one can obtain nice sepia colormap
%
%       figure;
%       imagesc(peaks(256));
%       GtColormap('bone');
%       GtColormap('invertCmap');
%       GtColormap('reverseCmap');
%
%       figure;
%       imagesc(peaks(256));
%       GtColormap('kryw');
%
%   Original version copyright 2009-2010 A. Nych
%
%
%   Modified and translated to class 05-02-2014 by LNervo

properties
    h_fig = [];

    type = 'default';
    defname = 'Rand';
    name = 'cool';

    bkg = [0 0 0];
    conflict = [1 1 1];
    hasBkg = true;
    hasConflict = false;

    cbar = [];
    cmapdef = []; % for colorbar
    ind = [];
    func = [];

    maxV = [];
    nsteps = [];
    maxSystem = true;
    scale = false;

    clims = [];
    values = [];
    par_values = [];

    colormap = [];
    origCmap = [];
    origLength = [];
    editcmap = [];

    hideColor = [0.5 0.5 0.5];
    hideIDs = [];
    maskCmap = [];
    activeIDs = [];
    IDs = [];

    matlab_cmaps = {};
    autogen_cmaps = {};
    data_cmaps = {};
    suppCmaps = {};

    % GUI buttons info
    controls = struct();
end

methods(Access = public)

    function obj = GtColormap(varargin)
    % GTCOLORMAP/GTCOLORMAP  Constructor
    % obj = GtColormap(varargin)

        obj.resetCmap();

        % change parameters
        obj.changeParams(varargin{:});

        if ~ismember('h_fig', varargin(1:2:end))
            % get current figure
            obj.getExistingFigure();
        end

        % get current colormap
        obj.getCurrentFigCmap();

        % get colormap features
        obj.getCmapExtra();

        obj.getCurrentCmapName();

        obj.origCmap   = obj.colormap;
        obj.origLength = size(obj.origCmap, 1);

        % cmap definition
        obj.cmapdef = GtColormap.getCmapDef(obj.name, obj.nsteps);
        obj.cbar = obj.cmapdef;

        % all the IDs
        if isempty(obj.IDs)
            obj.IDs = 1:size(obj.colormap, 1);
        end
        if isempty(obj.activeIDs)
            obj.activeIDs = true(1, obj.origLength);
        end
        if isempty(obj.values)
            obj.values = obj.IDs;
        end
        if isempty(obj.maxV)
            obj.maxV = max(obj.values(:));
        end
    end

    function changeParams(obj, varargin)
    % GTCOLORMAP/CHANGEPARAMS

        if ~isempty(varargin)
            for ii=1:length(varargin)/2
                prop{ii} = varargin{ii * 2 - 1};
                val{ii} = varargin{ii * 2};
                if isprop(obj, prop{ii}) && ...
                        ~ismember(prop{ii}, {'colormap', 'editcmap', ...
                        'controls', 'func', 'ind', 'type', ...
                        'autogen_cmaps', 'data_cmaps', 'matlab_cmaps', 'suppCmaps'})
                    obj.(prop{ii}) = val{ii};
                end
            end
        end
    end

    function resetCmap(obj)
    % GTCOLORMAP/RESETCMAP

        def_maps = GtColormap.initialize();
        obj.matlab_cmaps  = def_maps.matlab;
        obj.data_cmaps    = def_maps.data;
        obj.autogen_cmaps = def_maps.autogen;
        obj.suppCmaps     = obj.getSupportedCmaps();

        obj.colormap   = obj.origCmap;
        obj.origLength = size(obj.origCmap, 1);
        obj.nsteps     = size(eval(obj.name), 1) - 1;

        % should those fields be static?
        obj.ind        = @(steps) 0+(1/steps):1/steps:1;
        obj.func       = @(fcn) eval(['@(x) ' fcn '(x)']);

        % cmap definition
        obj.cbar       = obj.cmapdef;

        % all the IDs
        obj.IDs = 1:size(obj.colormap, 1);
        obj.activeIDs = true(1, obj.origLength);
        obj.values = [];
        obj.par_values = [];
        obj.maxV = [];
        obj.maxSystem = false;
        obj.scale     = false;

        % buttons on GUI
        obj.controls.handles = [];
        obj.controls.settings = struct();

        obj.getCurrentCmapName();
    end

    function output = exportProperties(obj, excludeFields)
    % GTCOLORMAP/EXPORTPROPERTIES
    % output = exportProperties(obj, excludeFields)

        if (~exist('excludeFields', 'var'))
            excludeFields = [];
        end
        if (~iscell(excludeFields) && ischar(excludeFields))
            excludeFields = {excludeFields};
        end

        fields = fieldnames(obj);
        fields( findStringIntoCell(fields, excludeFields, 1, false) ) = [];

        output = [];
        for ii = 1:numel(fields)
            output.(fields{ii}) = obj.(fields{ii});
        end
    end

    function importProperties(obj, structure, excludeFields)
    % GTCOLORMAP/IMPORTPROPERTIES
    % importProperties(obj, structure, excludeFields)

        if (~exist('structure', 'var') || isempty(structure) || ~isstruct(structure))
            return
        end
        if (~exist('excludeFields', 'var'))
            excludeFields = [];
        end
        if (~iscell(excludeFields) && ischar(excludeFields))
            excludeFields = {excludeFields};
        end

        fields = fieldnames(structure);
        fields( findStringIntoCell(fields, excludeFields, 1, false) ) = [];

        for ii = 1:numel(fields)
            if isprop(obj, fields{ii})
                obj.(fields{ii}) = structure.(fields{ii});
            end
        end
    end

    function addFigure(obj, hf)
    % GTCOLORMAP/ADDFIGURE  Adds the figure hf as current figure
    % addFigure(obj, hf)

        if (ishandle(hf) && exist('hf', 'var'))
            obj.h_fig = hf;
        end
    end

    function setControls(obj, varargin)
    % GTCOLORMAP/SETCONTROLS
    % setControls(obj, varargin)

        obj.deleteControls();
        obj.createCmapButtons(varargin{:});
    end

    function showControls(obj)
    % GTCOLORMAP/SHOWCONTROLS
    % showControls(obj)

        if (~isempty(obj.controls.handles) && all(ishandle(obj.controls.handles)))
            hh = obj.controls.handles;
            strings = get(hh, 'String');
            set(hh, 'Visible', 'on');
            index = findStringIntoCell(strings, {'Show Controls'}, 1, false);
            set(hh(index), 'String', 'Hide Controls', 'Callback', @(scr, evt)hideControls(obj));
        end
    end

    function hideControls(obj)
    % GTCOLORMAP/HIDECONTROLS
    % hideControls(obj)

        if (~isempty(obj.controls.handles) && all(ishandle(obj.controls.handles)))
            hh = obj.controls.handles;
            strings = get(hh, 'String');
            set(hh, 'Visible', 'off');
            index = findStringIntoCell(strings, {'Hide Controls'}, 1, false);
            set(hh(index), 'Visible', 'on', 'String', 'Show Controls', 'Callback', @(scr, evt)showControls(obj));
        end
    end

    function deleteControls(obj)
    % GTCOLORMAP/DELETECONTROLS
    % deleteControls(obj)

        if (~isempty(obj.controls.handles))
            hh = obj.controls.handles;
            if all(ishandle(hh))
                delete(hh);
            end
            obj.controls.handles = [];
        end
    end

    function setDataCmaps(obj, maps)
        obj.data_cmaps = maps;
    end

    function setMatlabCmaps(obj, maps)
        obj.matlab_cmaps = maps;
    end

    function setAutogenCmaps(obj, maps)
        obj.autogen_cmaps = maps;
    end

    function maps = getCurrentDataCmaps(obj)
    % GTCOLORMAP/
    % map = getCurrentDataCmaps(obj)
        maps = obj.data_cmaps;
    end

    function maps = getSupportedCmaps(obj)
    % GTCOLORMAP/GETSUPPORTEDCMAPS
    % maps = getSupportedCmaps(obj)

        maps = [ obj.matlab_cmaps; obj.data_cmaps(:, 1); obj.autogen_cmaps ];
    end

    function name = getCurrentCmapName(obj)
    % GTCOLORMAP/GETCURRENTCMAPNAME
    % name = getCurrentCmapName(obj)

        name = obj.name;
        if ~isempty(name)
            [hasP, index] = ismember(name, obj.suppCmaps);
            if (~isempty(obj.controls.handles) && all(ishandle(obj.controls.handles)) && hasP)
                hh = obj.controls.handles;
                set(hh( ismember('popupmenu', get(hh, 'Style')) ), 'String', obj.suppCmaps, 'Value', index);
            end
        elseif ismember(obj.defname, {'Caxis', 'IPF', 'Rvec', 'Rand'})
            if ~isempty(obj.controls.handles) && all(ishandle(obj.controls.handles))
                [str, val] = obj.chooseCmap();
                if ~isempty(str)
                    name = str{val};
                    obj.name = name;
                    obj.cmapdef = GtColormap.getCmapDef(obj.name, obj.nsteps);
                else
                    name = '';
                end
            end
        else
            name = 'cool';
            disp('Default colormap is ''cool''')
        end
    end

    function addCmapToSupported(obj)
    % GTCOLORMAP/ADDCMAPTOSUPPORTED
    % addCmapToSupported(obj)
        CountNewMap = 0;
        mapTable  = obj.colormap;
        mapName   = obj.defname;

        if ~isempty(obj.h_fig) && isappdata(obj.h_fig, 'CountNewMap')
            CountNewMap = getappdata(obj.h_fig, 'CountNewMap');
        end

        Length = size(obj.colormap, 1);
        if size(mapTable, 1) > Length
            if (~obj.hasBkg && ~obj.hasConflict)
            mapTable(Length+1:end, :) = [];
            elseif (obj.hasConflict) && size(mapTable, 1) == Length+2
                mapTable(1:2, :) = [];
            elseif (~obj.hasConflict && obj.hasBkg) && size(mapTable, 1) == Length+1
                mapTable(1, :) = [];
            end
        end
        if size(mapTable, 1) < Length
            mapTable = GtColormap.interpolateCTable(mapTable, Length, 'linear');
        end
        Length = size(mapTable, 1);
        if size(mapTable, 2) == 3
            mapTable(:, 2:4) = mapTable;
        end
        mapTable(:, 1) = (0+1/Length:1/Length:1)';

        if isempty(mapName) || ismember(mapName, suppcmaps)
            mapName = obj.name;
        end
        if ~ismember(mapName, obj.suppCmaps)
            if isempty(mapName)
                mapName = sprintf('newmap%d', CountNewMap);
            end

            CountNewMap = CountNewMap + 1;
            setappdata(obj.h_fig, 'CountNewMap', CountNewMap)

            obj.data_cmaps{end+1, 1} = mapName;
            obj.data_cmaps{end, 2}   = mapTable;
            disp('Colormap has been added to the colormap data')
            disp(['  Name  : ' mapName])
            disp(['  Count : ' num2str(CountNewMap)])

            if ~isempty(obj.controls.handles) && all(ishandle(obj.controls.handles))
                hh = obj.controls.handles;
                set(hh( ismember('popupmenu', get(hh, 'Style')) ), 'String', obj.suppCmaps, 'Value', size(obj.suppCmaps, 1));
                drawnow()
            end
            obj.suppCmaps = obj.getSupportedCmaps();
        end
        CMap = mapTable(:, 2:4);
        CBar = obj.cbar;
        obj.updateCmap(CMap, CBar)
    end

    function hideColorbar(obj)
    % GTCOLORBAR/HIDECOLORBAR
    % hideColorbar(obj)

        delete(findobj(obj.h_fig, 'Tag', 'Colorbar'));
        % change callback and text
        obj.changeButtonCbkAndText('Hide Cbar', 'Show Cbar', @(scr, evt)displayColorbar(obj), 0)
    end

    function displayColorbar(obj, CMap, CBar, h_fig, h_cmap_ax, h_cbar_ax)
    % GTCOLORMAP/DISPLAYCOLORBAR
    % displayColorbar(obj, CMap, CBar, h_fig, h_cmap_ax, h_cbar_ax)
    % CMap :
    % CBar :
    % h_fig :
    % h_cmap_ax :
    % h_cbar_ax :

        if ~exist('CMap', 'var') || isempty(CMap)
            CMap = obj.colormap;
            CMap = obj.setCmapExtra(CMap);
        end
        if ~exist('CBar', 'var') || isempty(CBar)
            CBar = obj.cbar;
        end
        % take axes
        if ~exist('h_fig', 'var') || isempty(h_fig)
            h_fig = obj.h_fig;
        end
        if ~exist('h_cmap_ax', 'var') || isempty(h_cmap_ax)
            if isempty(findobj(h_fig, 'Tag', 'h_cmap_ax'))
                h_cmap_ax = get(h_fig, 'CurrentAxes');
                set(h_cmap_ax, 'Tag', 'h_cmap_ax');
            else
                h_cmap_ax = findobj(h_fig, 'Tag', 'h_cmap_ax');
            end
        end
        if ~exist('h_cbar_ax', 'var') || isempty(h_cbar_ax)
            if isempty(findobj(h_fig, 'Tag', 'h_cbar_ax'))
                h_cbar_ax = copyobj(h_cmap_ax, h_fig);
                set(h_cbar_ax, 'Tag', 'h_cbar_ax');
            else
                h_cbar_ax = findobj(h_fig, 'Tag', 'h_cbar_ax');
            end
        end

        % take image
        if isempty(findobj(h_fig, 'Tag', 'h_cmap_im'))
            h_cmap_im = findobj(h_cmap_ax, 'Type', 'image');
            set(h_cmap_im, 'Tag', 'h_cmap_im');
        else
            h_cmap_im = findobj(h_fig, 'Tag', 'h_cmap_im');
        end
        obj.setClims(h_cmap_ax);

        % use copied image for colorbar
        if isempty(findobj(h_fig, 'Tag', 'h_cbar_im'))
            h_cbar_im = findobj(h_cbar_ax, 'Type', 'image');
            set(h_cbar_im, 'Tag', 'h_cbar_im');
        else
            h_cbar_im = findobj(h_fig, 'Tag', 'h_cbar_im');
        end
        freezeColors(h_cmap_ax);

        hold(h_cbar_ax, 'on');
        set(h_cbar_im, 'CData', get(h_cmap_im, 'CData'));

        % freeze colors
        unfreezeColors(h_cbar_ax);
        %freezeColors(h_cmap_ax);
        h_tmp = findobj(h_fig, 'type', 'axes');
        delete( h_tmp( findStringIntoCell(get(h_tmp, 'Tag'), {''}, 1) ) );
        % update colormap
        set(h_fig, 'Colormap', CBar);
        set(h_cbar_ax, 'CLim', [0 obj.maxV]);
        % unfreeze cbar
        if ~isempty(findobj(h_fig, 'Tag', 'Colorbar'))
            cbfreeze(findobj(h_fig, 'Tag', 'Colorbar'), 'off');
        end
        % set cbar to axes fake and set position
        x_ = lower(get(get(h_cmap_ax, 'XLabel'), 'String'));
        y_ = lower(get(get(h_cmap_ax, 'YLabel'), 'String'));
        if isempty(findobj(h_fig, 'Tag', 'Colorbar'))
            colorbar('peer', h_cbar_ax);
            h_cbar = findobj(h_fig, 'Tag', 'Colorbar');
        else
            h_cbar = findobj(h_fig, 'Tag', 'Colorbar');
        end
        set(h_cbar, 'Position', [0 0.1 0.3 0.82]);

        h_cbar = findobj(h_fig, 'Tag', 'Colorbar');
        % freeze cbar
        cbfreeze(findobj(h_fig, 'Tag', 'Colorbar'), 'on');
        h_cbar = findobj(h_fig, 'Tag', 'Colorbar');

        set(h_cbar, 'DeleteFcn', '');
        set(h_cbar, 'ButtonDownFcn', '');
        % hide fake image and axes
        set(h_cbar_im, 'Visible', 'off');
        set(h_cbar_ax, 'Visible', 'off');
        % unfreeze real axes colors
        unfreezeColors(h_cmap_ax);
        % restore real colors for image
        set(h_fig, 'Colormap', CMap);
        set(h_cmap_ax, 'CLim', obj.clims);

        % change callback and text
        obj.changeButtonCbkAndText('Show Cbar', 'Hide Cbar', @(scr, evt)hideColorbar(obj), 1)
    end

    function updateCmap(obj, CMap, CBar, Name)
    % GTCOLORMAP/UPDATECMAP
    % updateCmap(obj, CMap, CBar, Name)
    % CMap :
    % CBar :
    % Name :

        obj.origLength = size(obj.origCmap, 1);
        if exist('CMap', 'var') && ~isempty(CMap)
            if ~isempty(obj.hideIDs)
                obj.activeIDs = true(1, obj.origLength);
                obj.activeIDs(obj.hideIDs) = false;
                obj.editcmap = CMap;

                obj.maskCmap = NaN(obj.origLength, 4);
                obj.maskCmap(:, 1) = 1:size(CMap, 1);
                obj.maskCmap(obj.hideIDs, 2:4) = repmat(obj.hideColor, length(obj.hideIDs), 1);
                CMap(obj.hideIDs, :) = obj.maskCmap(obj.hideIDs, 2:4);
            end
            CMap = obj.setCmapExtra(CMap);
            if ~isempty(obj.h_fig)
                set(obj.h_fig, 'Colormap', CMap);
                obj.colormap = CMap;
                obj.getCmapExtra();
            end
        end
        if exist('CBar', 'var') && ~isempty(CBar)
            obj.cbar = CBar;
            if ~isempty(obj.h_fig)
                % check cbar
                [~, index, ~] = obj.getButtonHandleFromText('Hide Cbar');
                if ~isempty(index)
                    obj.displayColorbar(CMap, CBar, obj.h_fig);
                end
            end
        end
        if exist('Name', 'var') && ~isempty(Name)
            if strcmpi(Name, 'restore')
                obj.type = 'default';
            else
                obj.type = [Name ':' obj.type];
            end
            % update callbacks
            [hh, ~, strings] = obj.getButtonHandleFromText();
            if ~isempty(hh)
                if strcmp(Name, 'restore')
                    h_button_settings = obj.controls.h_button_settings;
                    h_button_settings(obj.controls.settings.hide, :) = [];
                    index = findValueIntoCell(strings, {'Hide Cbar', 'Show Cbar'});
                    %h_button_settings(index(:, 1), :) = [];
                    for ii=1:length(hh)
                        if ~ismember(ii, index(:, 1)')
                            set(hh(ii), 'String', h_button_settings{ii, 1}, ...
                                'Callback', h_button_settings{ii, 2}, 'Value', 0);
                        end
                    end
                    obj.controls.handles = hh;
                elseif strcmp(Name, 'rgbToHsv')
                    obj.changeButtonCbkAndText('RGB > HSV', 'HSV > RGB', @(scr, evt)hsvToRgb(obj, obj.colormap, obj.cbar), 1);
                elseif strcmp(Name, 'rgbToHsl')
                    obj.changeButtonCbkAndText('RGB > HSL', 'HSL > RGB', @(scr, evt)hslToRgb(obj, obj.colormap, obj.cbar), 1);
                elseif strcmp(Name, 'hsvToRgb')
                    obj.changeButtonCbkAndText('HSV > RGB', 'RGB > HSV', @(scr, evt)rgbToHsv(obj, obj.colormap, obj.cbar), 0);
                elseif strcmp(Name, 'hslToRgb')
                    obj.changeButtonCbkAndText('HSL > RGB', 'RGB > HSL', @(scr, evt)rgbToHsl(obj, obj.colormap, obj.cbar), 0);
                end
            end

            disp('Actions performed: ')
            actions = strrep(obj.type, ':', ' < ');
            disp(actions)
        end
    end

    function createCmapButtons(obj, varargin)
    % GTCOLORMAP/CREATECMAPBUTTONS  Creates buttons on the current figure
    % can configure button position using varargin property
    % and giving p/v pairs for button position
    % properties : {'s', 'w', 'h', 'mx', 'my', 'DX', 'DY', 'hide', 'show', 'toggle'}

        h_button_settings = {
            obj.suppCmaps,     @(src, evt)chooseCmap(obj),                              'popupmenu';
            'Reverse',         @(src, evt)reverseCmap(obj, obj.colormap, obj.cbar),       'togglebutton';
            'Invert',          @(src, evt)invertCmap(obj, obj.colormap, obj.cbar),        'togglebutton';
            'Shift Down',      @(src, evt)shiftDown(obj, obj.colormap, obj.cbar, false),   'pushbutton';
            'Shift Up',        @(src, evt)shiftUp(obj, obj.colormap, obj.cbar, false),     'pushbutton'; %5
            'Shift Right',     @(src, evt)shiftRight(obj, obj.colormap, obj.cbar),        'pushbutton';
            'Shift Left',      @(src, evt)shiftLeft(obj, obj.colormap, obj.cbar),         'pushbutton';
            'Mirror',          @(src, evt)mirrorCmap(obj, obj.colormap, obj.cbar),        'togglebutton';
            'RGB > RBG',       @(src, evt)rgbToRbg(obj, obj.colormap, obj.cbar),          'pushbutton';
            'RGB > GRB',       @(src, evt)rgbToGrb(obj, obj.colormap, obj.cbar),          'pushbutton'; %10
            'RGB > HSV',       @(src, evt)rgbToHsv(obj, obj.colormap, obj.cbar),          'togglebutton';
            'RGB > HSL',       @(src, evt)rgbToHsl(obj, obj.colormap, obj.cbar),          'togglebutton';
            'Darken ++',       @(src, evt)brightenCmap(obj, obj.colormap, obj.cbar, -0.5), 'pushbutton';
            'Darken +',        @(src, evt)brightenCmap(obj, obj.colormap, obj.cbar, -0.1), 'pushbutton';
            'Brighten +',      @(src, evt)brightenCmap(obj, obj.colormap, obj.cbar, 0.1),  'pushbutton'; %15
            'Brighten ++',     @(src, evt)brightenCmap(obj, obj.colormap, obj.cbar, 0.5),  'pushbutton';
            'Animate Down',    @(src, evt)animateDown(obj, obj.colormap, obj.cbar),       'pushbutton';
            'Animate Up',      @(src, evt)animateUp(obj, obj.colormap, obj.cbar),         'pushbutton';
            'UniGray',         @(src, evt)makeUniformBygray(obj),                       'pushbutton';
            'Restore',         @(src, evt)restoreCmap(obj),                             'pushbutton'; %20
            'Plot Cmap',       @(src, evt)GtColormap.plotCmap(...
                                   obj.colormap, obj.name),                             'pushbutton';
            'Show All Cmaps',  @(src, evt)GtColormap.showSupportedCmaps(...
                                   src, evt, false),                                     'pushbutton';
            'Export Cmap',     @(src, evt)exportCmap(obj),                              'pushbutton';
            'Show Cbar',       @(src, evt)displayColorbar(obj),                         'togglebutton';
            'Plot Cbar',       @(src, evt)GtColormap.plotCbar(...
                                   obj.cbar, obj.name, ...
                                   obj.defname, obj.nsteps, ...
                                   obj.maxV),                                          'pushbutton'; %25
            'Hide Controls',   @(src, evt)hideControls(obj),                            'togglebutton';
            'Delete Controls', @(src, evt)deleteControls(obj),                          'pushbutton';
        };

        Button.h      = 20; % height
        Button.w      = 120; % width
        Button.s      = 1;  % relative spacer between buttons
        Button.mx     = 10; % margin x
        Button.my     = 10; % margin y
        Button.DX     = 0;  % dx between buttons
        Button.DY     = Button.h + Button.s;
        Button.hide   = [];
        Button.show   = 1:length(h_button_settings);
        Button.toggle = [];
        if ~isempty(varargin)
            [Button, rej_pars] = parse_pv_pairs(Button, varargin);
        end
        % set minimum space between buttons
        Button.DX     = min((Button.w + Button.s), Button.DX);
        Button.DY     = max((Button.h + Button.s), Button.DY);
        Button.pos    = @(x) [Button.mx+Button.DX*x Button.my+Button.DY*x Button.w Button.h]';

        obj.controls.h_button_settings = h_button_settings;

        if isempty(Button.hide)
            Button.hide = setdiff(1:length(h_button_settings), Button.show);
        end
        Button.show(ismember(Button.hide, Button.show)) = [];
        h_button_settings(Button.hide, :) = [];

        par.parent = obj.h_fig;
        par = parse_pv_pairs(par, rej_pars);

        % build buttons
        hh = zeros(1, size(h_button_settings, 1));
        for bi = size(h_button_settings, 1)-1:-1:0
            hh(bi+1) = uicontrol('Parent', par.parent, ...
                'String', h_button_settings{bi+1, 1}, ...
                'Callback', h_button_settings{bi+1, 2}, ...
                'Style', h_button_settings{bi+1, 3}, ...
                'Tag', num2str(bi));
            if par.parent == obj.h_fig
                set(hh(bi+1), 'Units', 'pixels', 'Position', Button.pos(bi));
            end
            if strcmpi(h_button_settings{bi+1, 3}, 'togglebutton')
                set(hh(bi+1), 'Value', false);
                Button.toggle(end+1) = bi+1;
            end
        end

        % save settings
        obj.controls.settings     = Button;
        % active button handles
        obj.controls.handles      = hh;
        obj.controls.parent       = par.parent;
    end

    function getExistingFigure(obj)
    % GTCOLORMAP/GETEXISTINGFIGURE  Sets the current figure handle
    % Checks whether we have at least one open figure or not

        obj.h_fig = get(0, 'currentfigure');
    end

    function getCurrentFigCmap(obj)
    % GTCOLORMAP/GETCURRENTFIGCMAP  Sets the current figure colormap
    % If no figures are open so far it returns default colormap without opening new one.

        if isempty(obj.h_fig)
            obj.getExistingFigure();
        end

        if ~isempty(obj.h_fig)
            % there is an open figure
            obj.colormap = get(obj.h_fig, 'colormap');
        else
            % no figures are open so farw
            obj.colormap = get(0, 'defaultfigurecolormap');
        end
    end

    function getCmapExtra(obj)
    % GTCOLORMAP/GETCMAPEXTRA  Gets optional bkg and conflict colors in the
    %                          current colormap
    % getCmapExtra(obj)

        if (~isempty(obj.colormap))
            if all(isequal(obj.colormap(1, :), obj.bkg))
                obj.hasBkg = true;
            elseif all(isequal(obj.colormap(1, :), obj.conflict))
                obj.hasConflict = true;
                if all(isequal(obj.colormap(2, :), obj.bkg))
                    obj.hasBkg = true;
                end
            end
            if (obj.hasConflict)
                obj.colormap(1, :) = [];
            end
            if (obj.hasBkg)
                obj.colormap(1, :) = [];
            end
        end
    end

    function CMap = setCmapExtra(obj, CMap, fakeCmap)
    % GTCOLORMAP/SETCMAPEXTRA  Sets optional bkg and conflict colors in the
    %                          current colormap
    % CMap = setCmapExtra(obj, CMap, fakeCmap)
        obj.origLength = size(obj.origCmap, 1);
        if (~exist('fakeCmap', 'var') || isempty(fakeCmap))
            fakeCmap = false;
        end
        if (~fakeCmap)
            if (size(CMap, 1) ~= obj.origLength)
                disp('Modified colormap has wrong size...Back to the original one.')
                CMap = obj.origCmap;
            end
        end
        if (obj.hasBkg)
            CMap(2:end+1, :) = CMap;
            CMap(1, :) = obj.bkg;
        end
        if (obj.hasConflict)
            CMap(2:end+1, :) = CMap;
            CMap(1, :) = obj.conflict;
        end
    end

    function setClims(obj, h_cmap_ax)
    % GTCOLORMAP/SETCLIMS
    % setClims(obj, h_cmap_ax)

        obj.clims = get(h_cmap_ax, 'CLim');
    end

    function [hh, index, strings] = getButtonHandleFromText(obj, currText)
    % GTCOLORMAP/GETBUTTONHANDLEFROMTEXT
    % [hh, index, strings] = getButtonHandleFromText(obj, currText)

        hh = [];
        index = [];
        strings = [];
        if ~isempty(obj.controls.handles) && all(ishandle(obj.controls.handles))
            h_all = obj.controls.handles;
            strings = get(h_all, 'String');
            if strcmpi(get(h_all(1), 'Style'), 'popupmenu')
                strings(1) = [];
                h_all(1) = [];
            end
            if (~exist('currText', 'var') || isempty(currText))
                hh = h_all;
            else
                index = findStringIntoCell(strings, {currText}, 1, false);
                if (~isempty(index))
                    hh = h_all(index);
                end
            end
        end
    end

    function changeButtonCbkAndText(obj, currText, newText, newCbk, newVal)
    % GTCOLORMAP/CHANGEBUTTONCBKANDTEXT
    % changeButtonCbkAndText(obj, currText, newText, newCbk, newVal)

        hh = obj.getButtonHandleFromText(currText);
        if ~isempty(hh)
            set(hh, 'String', newText, 'Callback', newCbk, 'Value', newVal);
            drawnow('expose');
        end
    end

end

methods(Access = private)
    function [str, val] = chooseCmap(obj)
    % GTCOLORMAP/CHOOSEMAP  Chooses a default colormap from the supported
    %                       colormap list
    % [str, val] = chooseCmap(obj)
    % str : colormap names
    % val : colormap value

        str = '';
        val = [];
        if ~isempty(obj.controls.handles) && all(ishandle(obj.controls.handles))
            h = obj.controls.handles;
            if strcmpi(get(h(1), 'Style'), 'popupmenu')
                str = get(h(1), 'String');
                val = get(h(1), 'Value');
            end
            if ~isempty(str)
                Name = str{val};
                obj.name = Name;
                disp(['Changed to ' Name ' default colormap'])
                obj.type = 'default';
            end
            if nargout == 0
                [CMap, ~] = GtColormap.getSelectedCmap(obj.origLength, Name, obj.origLength);
                CBar = GtColormap.getCmapDef(obj.name, obj.nsteps);
                obj.updateCmap(CMap, CBar);
            end
        end
    end

    function makeUniformBygray(obj)
    % GTCOLORMAP/MAKEUNIFORMBYGRAY
    % makeUniformBygray(obj)

        if ismember(obj.defname, {'Caxis', 'IPF', 'Rvec', 'Rand'})
            obj.name = 'user';
        end
        LastColormapName    = obj.name;

        ColormapName        = [];
        if isempty(obj.name)
            % Colormap was modified by one of the GTCOLORMAP commands
            if isempty(LastColormapName)
                % User performed one of the GTCOLORMAP commands more than once, so
                % previous colormap is lost.
                % If there are at least one figure and it contains GTCOLORMAP controls,
                % then we can check popupmenu for last used colormap
                if ~isempty(obj.h_fig)
                    LastColormapName = obj.getCurrentCmapName();
                end
            end
            if isempty(LastColormapName)
                uiwait(warndlg({'Can not determine current colormap name!', ...
                    'Usually this happens after performing commands like BRIGHTEN or DARKEN.', ...
                    'Select proper colormap and try again.'}, mfilename, 'modal'));
            else
                check = questdlg({'Can not determine current colormap name!', ...
                    'Usually this happens after performing commands like BRIGHTEN or DARKEN.', ...
                    sprintf('But last used colormap was "%s"', LastColormapName), ...
                    'Should I use it?'}, mfilename, 'Yes', 'No', 'Yes');
                if strcmpi(check, 'yes')
                    ColormapName = LastColormapName;
                end
            end
        else
            ColormapName = obj.name;
        end
        if (ismember(ColormapName, obj.getMatlabCmaps()))
            uiwait(warndlg({'Can not unify default matlab colormap!', ''}, mfilename, 'modal'));
            ColormapName = '';
        end
        if ~isempty(ColormapName) && ~strcmpi(ColormapName, 'user')
            CurrentColorMapLen  = size(obj.colormap, 1);
            [cTab, cPos]        = GtColormap.getSelectedCmap(CurrentColorMapLen, ColormapName, CurrentColorMapLen);
            cTabLen             = length(cPos);
            cTabGray            = rgb2gray(cTab);
            GrayVal             = cTabGray(:, 1);
            Coeff               = ones(size(GrayVal));
            GrayVal1            = cPos;
            for kk=1:cTabLen
                if abs(GrayVal1(kk)-GrayVal(kk))<eps
                    Coeff(kk) = 1;
                else
                    Coeff(kk) = GrayVal1(kk) / GrayVal(kk);
                end
            end
            cTabNew = cTab .* repmat(Coeff, 1, 3);
            cTable = [ cPos cTabNew ];
            vars = {CurrentColorMapLen, cPos, 'linear'};
            [Length, ~, InterpMode] = GtColormap.parseParameters(vars, cTabLen);
            CMap = GtColormap.interpolateCTable(cTable, Length, InterpMode);
            CBar = cTabGray;
            obj.updateCmap(CMap, CBar, 'makeUniformBygray')
        else
            warning('GtColormap:ColorMapDataLost', 'Can not determine last colormap source!');
        end
        if ismember(obj.defname, {'Caxis', 'IPF', 'Rvec', 'Rand'})
            obj.name = '';
        end
    end

    function animateDown(obj, CMap, CBar)
    % GTCOLORMAP/ANIMATEDOWN
    % animateDown(obj, CMap, CBar)

        [tmp_r, ~] = size(CBar);
        for tmp_k = 1:tmp_r
            obj.shiftDown(CMap, CBar, true);
            drawnow();
        end;
        obj.updateCmap(CMap, CBar, 'animateDown');
    end

    function animateUp(obj, CMap, CBar)
    % GTCOLORMAP/ANIMATEUP
    % animateUp(obj, CMap, CBar)

        [tmp_r, ~] = size(CBar);
        for tmp_k = 1:tmp_r
            obj.shiftUp(CMap, CBar, true);
            drawnow();
        end;
        obj.updateCmap(CMap, CBar, 'animateUp');
    end

    function exportCmap(obj)
    % GTCOLORMAP/EXPORTCMAP
    % exportCmap(obj)

        if ~isempty(obj.h_fig) && ishandle(obj.h_fig)
            setappdata(obj.h_fig, 'colormap', obj.colormap);
            setappdata(obj.h_fig, 'cbar', obj.cbar);
            disp('Current colormap and colorbar have been save to figure appdata')
        end
    end

    function restoreCmap(obj)
    % GTCOLORMAP/RESTORECMAP
    % restoreCmap(obj)

        CMap = obj.origCmap;
        CBar = obj.cmapdef;
        obj.updateCmap(CMap, CBar, 'restore');

        if isappdata(obj.h_fig, 'colormap')
            rmappdata(obj.h_fig, 'colormap');
        end
        if isappdata(obj.h_fig, 'cbar')
            rmappdata(obj.h_fig, 'cbar');
        end
        obj.activeIDs = true(1, obj.origLength);
        obj.activeIDs(obj.hideIDs) = false;
    end

    function reverseCmap(obj, CMap, CBar)
    % GTCOLORMAP/REVERSECMAP
    % reverseCmap(obj, CMap, CBar)

        CMap(~obj.activeIDs, :) = obj.origCmap(~obj.activeIDs, :);
        CMap = flipud(CMap);
        %CBar(~obj.activeIDs, :) = obj.cmapdef(~obj.activeIDs, :);
        CBar = flipud(CBar);

        obj.activeIDs = fliplr(obj.activeIDs);
        obj.updateCmap(CMap, CBar, 'reverse');
    end

    function invertCmap(obj, CMap, CBar)
    % GTCOLORMAP/INVERTCMAP
    % invertCmap(obj, CMap, CBar)

        CMap = 1-CMap;
        CBar = 1-CBar;
        obj.updateCmap(CMap, CBar, 'invert');
    end

    function mirrorCmap(obj, CMap, CBar)
    % GTCOLORMAP/MIRRORCMAP
    % mirrorCmap(obj, CMap, CBar)

        CMap = CMap(:, [3 2 1]);
        CBar = CBar(:, [3 2 1]);
        obj.updateCmap(CMap, CBar, 'mirror');
    end

    function shiftUp(obj, CMap, CBar, animating)
    % GTCOLORMAP/SHIFTUP
    % shiftUp(obj, CMap, CBar, animating)

        n_map = size(CMap, 1);
        n_bar = size(CBar, 1);

        CMap = [CMap(end, :); CMap(1:end-1, :)];
        CBar = [CBar(end, :); CBar(1:end-1, :)];
        if (~animating)
            obj.updateCmap(CMap, CBar, 'shiftUp');
        else
            obj.updateCmap(CMap, CBar);
        end
    end

    function shiftDown(obj, CMap, CBar, animating)
    % GTCOLORMAP/SHIFTDOWN
    % shiftDown(obj, CMap, CBar, animating)

        CMap = [CMap(2:end, :); CMap(1, :)];
        CBar = [CBar(2:end, :); CBar(1, :)];
        if (~animating)
            obj.updateCmap(CMap, CBar, 'shiftDown');
        else
            obj.updateCmap(CMap, CBar);
        end
    end

    function shiftRight(obj, CMap, CBar)
    % GTCOLORMAP/SHIFTRIGHT
    % shiftRight(obj, CMap, CBar)

        CMap = CMap(:, [3 1 2]);
        CBar = CBar(:, [3 1 2]);
        obj.updateCmap(CMap, CBar, 'shiftRight');
      end

    function shiftLeft(obj, CMap, CBar)
    % GTCOLORMAP/SHIFTLEFT
    % shiftLeft(obj, CMap, CBar)

        CMap = CMap(:, [2 3 1]);
        CBar = CBar(:, [2 3 1]);
        obj.updateCmap(CMap, CBar, 'shiftLeft');
    end

    function rgbToRbg(obj, CMap, CBar)
    % GTCOLORMAP/RGBTORBG
    % rgbToRbg(obj, CMap, CBar)

        CMap = CMap(:, [1 3 2]);
        CBar = CBar(:, [1 3 2]);
        obj.updateCmap(CMap, CBar, 'rgbToRbg');
    end

    function rgbToGrb(obj, CMap, CBar)
    % GTCOLORMAP/RGBTOGRB
    % rgbToGrb(obj, CMap, CBar)

        CMap = CMap(:, [2 1 3]);
        CBar = CBar(:, [2 1 3]);
        obj.updateCmap(CMap, CBar, 'rgbToGrb');
    end

    function rgbToHsv(obj, CMap, CBar)
    % GTCOLORMAP/RGBTOHSV
    % rgbToHsv(obj, CMap, CBar)

        CMap = rgb2hsv(CMap);
        CBar = rgb2hsv(CBar);
        obj.updateCmap(CMap, CBar, 'rgbToHsv');
    end

    function hsvToRgb(obj, CMap, CBar)
    % GTCOLORMAP/HSVTORGB
    % hsvToRgb(obj, CMap, CBar)

        CMap = hsv2rgb(CMap);
        CBar = hsv2rgb(CBar);
        obj.updateCmap(CMap, CBar, 'hsvToRgb');
    end

    function rgbToHsl(obj, CMap, CBar)
    % GTCOLORMAP/RGBTOHSL
    % rgbToHsl(obj, CMap, CBar)

        CMap = rgb2hsl(CMap);
        CBar = rgb2hsl(CBar);
        obj.updateCmap(CMap, CBar, 'rgbToHsl');
    end

    function hslToRgb(obj, CMap, CBar)
    % GTCOLORMAP/HSLTORGB
    % hslToRgb(obj, CMap, CBar)

        CMap = hsl2rgb(CMap);
        CBar = hsl2rgb(CBar);
        obj.updateCmap(CMap, CBar, 'hslToRgb');
    end

    function brightenCmap(obj, CMap, CBar, Amount)
    % GTCOLORMAP/BRIGHTENCMAP
    % brightenCmap(obj, CMap, CBar, Amount)

        Amount_str = '';
        if isempty(Amount), Amount = 0.1; end
        if Amount<-1
            Amount = -1;
            Amount_str = 'darken(-1)';
        end;
        if Amount>1
            Amount = 1;
            Amount_str = 'brighten(1)';
        end;
        if Amount > 0 && Amount < 1 && Amount ~= 0.5
            Amount_str = ['brighten(' num2str(Amount) ')'];
        elseif Amount < 0 && Amount > -1 && Amount ~= -0.5
            Amount_str = ['darken(' num2str(Amount) ')'];
        end
        if Amount == 0.5
            Amount_str = 'brighten(0.5)';
        elseif Amount == -0.5
            Amount_str = 'darken(0.5)';
        end
        CMap = brighten(CMap, Amount);
        CBar = brighten(CBar, Amount);
        obj.updateCmap(CMap, CBar, Amount_str);
    end

end

methods(Static, Access = public)
% methods(Static, Access = public)

    function def_maps = initialize()
    %
    %
        % These are built-in MatLab colormaps
        def_maps.matlab = {
            'autumn'    'bone'      'colorcube' ...
            'cool'      'copper'    'flag'      ...
            'gray'      'hot'       'hsv'       ...
            'jet'       'lines'     'pink'      ...
            'prism'     'spring'    'summer'    ...
            'white'     'winter' }';


        % Examples of generated colormaps
        % These colormaps (and many others) can be generated programmatically
        % These colormaps are added in case one needs a list of supported colormaps
        def_maps.autogen = {
            'kr'        'kg'        'kb'        ...
            'kc'        'km'        'ky'        ...
            'wr'        'wg'        'wb'        ...
            'wc'        'wm'        'wy'        ...
            'krk'       'kryrk'     'krwrk'     ...
            'rkr'       'yrkry'     'wrkrw'     ...
            'brg'       'yrb'       'bgr'       ...
            'rgb'       'krkgkbk'   'krgbcmyw'  ...
            'cmy'       'kckmkyk'   'rygcb'     ...
            'rygcbmr'   'krywyrk'   ...
            'krpoglabsvcmyw' }';


        % Definitions of new colormaps
        def_maps.data = { ...
            %Colormap name      Value   Red     Green   Blue
            'jet2'          [   0.00    0.0000  0.0000  0.0000;
                                0.20    0.0000  0.0000  0.5000;
                                0.40    0.0000  1.0000  1.0000;
                                0.60    1.0000  1.0000  0.0000;
                                0.80    1.0000  0.0000  0.0000;
                                1.00    0.5000  0.0000  0.0000 ]; ...
            'jet3'          [   0.00    0.0000  0.0000  0.5000;
                                0.20    0.0000  0.0000  0.8000;
                                0.40    0.0000  0.9000  0.9000;
                                0.60    1.0000  1.0000  0.0000;
                                0.80    1.0000  0.0000  0.0000;
                                1.00    0.5000  0.0000  0.0000 ]; ...
            'jet4'          [   0.00    0.0000  0.0000  0.5000;
                                0.30    0.0000  0.0000  0.8000;
                                0.50    0.0000  0.9000  0.9000;
                                0.70    1.0000  1.0000  0.0000;
                                0.90    1.0000  0.0000  0.0000;
                                1.00    0.5000  0.0000  0.0000 ]; ...
            'jet5'          [   0.00    0.0000  0.0000  0.5000;
                                0.20    0.0000  0.0000  0.8000;
                                0.40    0.0000  0.9000  0.9000;
                                0.70    1.0000  1.0000  0.0000;
                                0.90    1.0000  0.0000  0.0000;
                                1.00    0.5000  0.0000  0.0000 ]; ...
            'jet6'          [   0.00    0.0000  0.0000  0.5000;
                                0.10    0.0000  0.0000  0.8000;
                                0.30    0.0000  0.9000  0.9000;
                                0.70    1.0000  1.0000  0.0000;
                                0.90    1.0000  0.0000  0.0000;
                                1.00    0.5000  0.0000  0.0000 ]; ...
            'jet7'          [   0.00    0.0000  0.0000  0.5000;
                                0.10    0.0000  0.0000  0.8000;
                                0.40    0.0000  0.9000  0.9000;
                                0.70    1.0000  1.0000  0.0000;
                                0.90    1.0000  0.0000  0.0000;
                                1.00    0.5000  0.0000  0.0000 ]; ...
            'jet8'          [   0.00    0.0000  0.0000  0.5000;
                                0.10    0.0000  0.0000  0.8000;
                                0.45    0.0000  0.9000  0.9000;
                                0.57    0.0000  1.0000  0.5000;
                                0.70    1.0000  1.0000  0.0000;
                                0.90    1.0000  0.0000  0.0000;
                                1.00    0.5000  0.0000  0.0000 ]; ...
            'thermal'       [   0.0000  0.0000  0.0000  0.0000; % FEX#23342
                                0.2500  0.3000  0.0000  0.7000;
                                0.5000  1.0000  0.2000  0.0000;
                                0.7500  1.0000  1.0000  0.0000;
                                1.0000  1.0000  1.0000  1.0000 ]; ...
            'thermal2'      [   0.0000  0.0000  0.0000  0.0000; % FEX#23342
                                0.2500  0.5000  0.0000  0.0000;
                                0.5000  1.0000  0.2000  0.0000;
                                0.7500  1.0000  1.0000  0.0000;
                                1.0000  1.0000  1.0000  1.0000 ]; ...
            'thermal3'      [   0.0000  0.0000  0.0000  0.0000; % FEX#23342
                                0.2500  0.5000  0.0000  0.0000;
                                0.5000  1.0000  0.2000  0.0000;
                                0.7500  1.0000  1.0000  0.0000;
                                1.0000  1.0000  1.0000  0.8000 ]; ...
            'bled'          [   0.0000  0.0000  0.0000  0.0000; % FEX#23342
                                0.1667  0.1667  0.1667  0.0000;
                                0.3333  0.0000  0.3333  0.0000;
                                0.5000  0.0000  0.5000  0.5000;
                                0.6667  0.0000  0.0000  0.6667;
                                0.8333  0.8333  0.0000  0.8333;
                                1.0000  1.0000  0.0000  0.0000 ]; ...
            'bright'        [   0.0000  0.0000  0.0000  0.0000; % FEX#23342
                                0.1429  0.3071  0.0107  0.3925;
                                0.2857  0.0070  0.2890  1.0000;
                                0.4286  1.0000  0.0832  0.7084;
                                0.5714  1.0000  0.4447  0.1001;
                                0.7143  0.5776  0.8360  0.4458;
                                0.8571  0.9035  1.0000  0.0000;
                                1.0000  1.0000  1.0000  1.0000 ]; ...
            'copper2'       [   0.0000  0.0000  0.0000  0.0000; % FEX#23342
                                0.2500  0.2651  0.2426  0.2485;
                                0.5000  0.6660  0.4399  0.3738;
                                0.7500  0.8118  0.7590  0.5417;
                                1.0000  1.0000  1.0000  1.0000 ]; ...
            'dusk'          [   0.0000  0.0000  0.0000  0.0000; % FEX#23342
                                0.0570  0.0000  0.0000  0.5000;
                                0.3505  0.0000  0.5000  0.5000;
                                0.5000  0.5000  0.5000  0.5000;
                                0.6495  1.0000  0.5000  0.5000;
                                0.9430  1.0000  1.0000  0.5000;
                                1.0000  1.0000  1.0000  1.0000 ]; ...
            'earth'         [   0.0000  0.0000  0.0000  0.0000; % FEX#23342
                                0.0714  0.0000  0.1104  0.0583;
                                0.1429  0.1661  0.1540  0.0248;
                                0.2143  0.1085  0.2848  0.1286;
                                0.2857  0.2643  0.3339  0.0939;
                                0.3571  0.2653  0.4381  0.1808;
                                0.4286  0.3178  0.5053  0.3239;
                                0.5000  0.4858  0.5380  0.3413;
                                0.5714  0.6005  0.5748  0.4776;
                                0.6429  0.5698  0.6803  0.6415;
                                0.7143  0.5639  0.7929  0.7040;
                                0.7857  0.6700  0.8626  0.6931;
                                0.8571  0.8552  0.8967  0.6585;
                                0.9286  1.0000  0.9210  0.7803;
                                1.0000  1.0000  1.0000  1.0000 ]; ...
            'hicontrast'    [   0.0000  0.0000  0.0000  0.0000; % FEX#23342
                                0.1140  0.0000  0.0000  1.0000;
                                0.2990  1.0000  0.0000  0.0000;
                                0.4130  1.0000  0.0000  1.0000;
                                0.5870  0.0000  1.0000  0.0000;
                                0.7010  0.0000  1.0000  1.0000;
                                0.8860  1.0000  1.0000  0.0000;
                                1.0000  1.0000  1.0000  1.0000 ]; ...
            'hsv2'          [   0.0000  0.0000  0.0000  0.0000; % FEX#23342
                                0.1667  0.5000  0.0000  0.5000;
                                0.3333  0.0000  0.0000  0.9000;
                                0.5000  0.0000  1.0000  1.0000;
                                0.6667  0.0000  1.0000  0.0000;
                                0.8333  1.0000  1.0000  0.0000;
                                1.0000  1.0000  0.0000  0.0000 ]; ...
            'pastel'        [   0.0000  0.0000  0.0000  0.0000; % FEX#23342
                                0.1429  0.4709  0.0000  0.0180;
                                0.2857  0.0000  0.3557  0.6747;
                                0.4286  0.8422  0.1356  0.8525;
                                0.5714  0.4688  0.6753  0.3057;
                                0.7143  1.0000  0.6893  0.0934;
                                0.8571  0.9035  1.0000  0.0000;
                                1.0000  1.0000  1.0000  1.0000 ]; ...
            'pink2'         [   0.0000  0.0000  0.0000  0.0000; % FEX#23342
                                0.0714  0.0455  0.0635  0.1801;
                                0.1429  0.2425  0.0873  0.1677;
                                0.2143  0.2089  0.2092  0.2546;
                                0.2857  0.3111  0.2841  0.2274;
                                0.3571  0.4785  0.3137  0.2624;
                                0.4286  0.5781  0.3580  0.3997;
                                0.5000  0.5778  0.4510  0.5483;
                                0.5714  0.5650  0.5682  0.6047;
                                0.6429  0.6803  0.6375  0.5722;
                                0.7143  0.8454  0.6725  0.5855;
                                0.7857  0.9801  0.7032  0.7007;
                                0.8571  1.0000  0.7777  0.8915;
                                0.9286  0.9645  0.8964  1.0000;
                                1.0000  1.0000  1.0000  1.0000 ]; ...
            'sepia'         [   0.0000  0.0000  0.0000  0.0000; % FEX#23342
                                0.0500  0.1000  0.0500  0.0000;
                                0.9000  1.0000  0.9000  0.8000;
                                1.0000  1.0000  1.0000  1.0000 ]; ...
            'temp'          [   0.0000  0.1420  0.0000  0.8500; % FEX#23342
                                0.0588  0.0970  0.1120  0.9700;
                                0.1176  0.1600  0.3420  1.0000;
                                0.1765  0.2400  0.5310  1.0000;
                                0.2353  0.3400  0.6920  1.0000;
                                0.2941  0.4600  0.8290  1.0000;
                                0.3529  0.6000  0.9200  1.0000;
                                0.4118  0.7400  0.9780  1.0000;
                                0.4706  0.9200  1.0000  1.0000;
                                0.5294  1.0000  1.0000  0.9200;
                                0.5882  1.0000  0.9480  0.7400;
                                0.6471  1.0000  0.8400  0.6000;
                                0.7059  1.0000  0.6760  0.4600;
                                0.7647  1.0000  0.4720  0.3400;
                                0.8235  1.0000  0.2400  0.2400;
                                0.8824  0.9700  0.1550  0.2100;
                                0.9412  0.8500  0.0850  0.1870;
                                1.0000  0.6500  0.0000  0.1300 ]; ...
            %Colormap name      Value   Red     Green   Blue
            'cold'          [   0.0000  0.0000  0.0000  0.0000; % FEX#23865
                                0.2500  0.0000  0.0000  1.0000;
                                0.6500  0.0000  1.0000  1.0000;
                                1.0000  1.0000  1.0000  1.0000 ]; ...
            'fireice'       [   0.0000  0.5000  1.0000  1.0000; % FEX#24870
                                0.1400  0.0000  1.0000  1.0000;
                                0.3200  0.0000  0.0000  1.0000;
                                0.5000  0.0000  0.0000  0.0000;
                                0.6800  1.0000  0.0000  0.0000;
                                0.8600  1.0000  1.0000  0.0000;
                                1.0000  1.0000  1.0000  0.5000 ]; ...
            'fireicedark'   [   0.0000  0.3500  1.0000  0.8700; % FEX#24870-based
                                0.0588  0.1500  0.9150  0.8130;
                                0.1176  0.0300  0.8450  0.7900;
                                0.1765  0.0000  0.7600  0.7600;
                                0.2353  0.0000  0.5280  0.6600;
                                0.2941  0.0000  0.3240  0.5400;
                                0.3529  0.0000  0.1600  0.4000;
                                0.4118  0.0000  0.0520  0.2600;
                                0.4706  0.0000  0.0000  0.0800;
                                0.5294  0.0800  0.0000  0.0000;
                                0.5882  0.2600  0.0220  0.0000;
                                0.6471  0.4000  0.0800  0.0000;
                                0.7059  0.5400  0.1710  0.0000;
                                0.7647  0.6600  0.3080  0.0000;
                                0.8235  0.7600  0.4690  0.0000;
                                0.8824  0.8400  0.6580  0.0000;
                                0.9412  0.9030  0.8880  0.0300;
                                1.0000  0.8580  1.0000  0.1500 ]; ...
            'Topographic'   [   0.0000  0.0000  0.0000  0.2000;
                                0.0500  0.0000  0.0000  0.6600;
                                0.1700  0.3300  0.6600  1.0000;
                                0.2200  0.1500  0.5000  0.0000;
                                0.3500  0.2500  0.6000  0.1000;
                                0.5700  0.5000  0.9000  0.4000;
                                0.7100  0.9500  0.9000  0.4000;
                                0.7400  0.9500  0.9000  0.3500;
                                0.8600  0.9500  0.7000  0.2000;
                                0.9500  0.6500  0.5000  0.0500;
                                1.0000  1.0000  1.0000  1.0000 ]; ...
            'Royal'         [   0.0000  0.0000  0.0000  0.0000;
                                0.0100  0.2500  0.0000  0.3200;
                                0.2500  0.4700  0.0000  0.6600;
                                0.5000  1.0000  1.0000  1.0000;
                                0.7500  1.0000  1.0000  0.0000;
                                1.0000  0.2500  0.0000  0.3200 ]; ...
            'RoyalGold'     [   0.0000  0.0000  0.0000  0.0000;
                                0.0100  0.2500  0.0000  0.3200;
                                0.2500  0.4700  0.0000  0.6600;
                                0.5000  1.0000  1.0000  0.0000;
                                1.0000  1.0000  1.0000  1.0000 ]; ...
            'RoyalGoldDark' [   0.0000  0.0000  0.0000  0.0000;
                                0.0100  0.2500  0.0000  0.3200;
                                0.2500  0.4700  0.0000  0.6600;
                                0.5000  0.7500  0.7500  0.0000;
                                0.7500  1.0000  1.0000  0.0000;
                                1.0000  1.0000  1.0000  1.0000 ]; ...
            'FCPM_001'      [   0.0000  0.0000  0.0000  0.0000;
                                0.2000  0.0000  0.0000  1.0000;
                                0.5000  0.0000  1.0000  0.0000;
                                0.8000  1.0000  1.0000  0.0000;
                                1.0000  1.0000  1.0000  1.0000 ]; ...
            'FCPM_002'      [   0.0000  0.0000  0.5000  0.0000;
                                0.1000  0.0000  0.0000  1.0000;
                                0.3500  1.0000  0.0000  1.0000;
                                0.6000  1.0000  0.0000  0.0000;
                                0.8000  1.0000  1.0000  0.0000;
                                1.0000  1.0000  1.0000  1.0000 ]; ...
            'FCPM_LCI'      [   0.0000  0.0000  0.0000  0.0000;
                                0.2000  0.0000  1.0000  0.0000;
                                0.4000  0.0000  0.0000  1.0000;
                                0.6000  1.0000  0.0000  0.0000;
                                0.8000  1.0000  1.0000  0.0000;
                                1.0000  1.0000  1.0000  1.0000 ]; ...
            'krywyrk2'      [   0.0000  0.0000  0.0000  0.0000;
                                0.2000  1.0000  0.0000  0.0000;
                                0.4000  1.0000  1.0000  0.0000;
                                0.5000  1.0000  1.0000  1.0000;
                                0.6000  1.0000  1.0000  0.0000;
                                0.8000  1.0000  0.0000  0.0000;
                                1.0000  0.0000  0.0000  0.0000 ]; ...
            'AFM'           [   0.0000    0.2314    0.0000    0.0000;
                                0.0039    0.2314    0.0000    0.0000;
                                0.0078    0.2314    0.0000    0.0000;
                                0.0118    0.2314    0.0000    0.0000;
                                0.0157    0.2314    0.0000    0.0000;
                                0.0196    0.2314    0.0000    0.0000;
                                0.0235    0.2314    0.0000    0.0000;
                                0.0275    0.2314    0.0000    0.0000;
                                0.0314    0.2314    0.0000    0.0000;
                                0.0353    0.2314    0.0000    0.0000;
                                0.0392    0.2314    0.0000    0.0000;
                                0.0431    0.2314    0.0000    0.0000;
                                0.0471    0.2314    0.0000    0.0000;
                                0.0510    0.2314    0.0000    0.0000;
                                0.0549    0.2314    0.0000    0.0000;
                                0.0588    0.2314    0.0000    0.0000;
                                0.0627    0.2314    0.0000    0.0000;
                                0.0667    0.2314    0.0000    0.0000;
                                0.0706    0.2314    0.0000    0.0000;
                                0.0745    0.2314    0.0000    0.0000;
                                0.0784    0.2314    0.0000    0.0000;
                                0.0824    0.2314    0.0000    0.0000;
                                0.0863    0.2314    0.0000    0.0000;
                                0.0902    0.2314    0.0000    0.0000;
                                0.0941    0.2314    0.0000    0.0000;
                                0.0980    0.2314    0.0000    0.0000;
                                0.1020    0.2314    0.0000    0.0000;
                                0.1059    0.2314    0.0000    0.0000;
                                0.1098    0.2314    0.0000    0.0000;
                                0.1137    0.2314    0.0000    0.0000;
                                0.1176    0.2353    0.0000    0.0000;
                                0.1216    0.2392    0.0000    0.0000;
                                0.1255    0.2431    0.0000    0.0000;
                                0.1294    0.2471    0.0000    0.0000;
                                0.1333    0.2471    0.0000    0.0000;
                                0.1373    0.2549    0.0000    0.0000;
                                0.1412    0.2588    0.0000    0.0000;
                                0.1451    0.2588    0.0000    0.0000;
                                0.1490    0.2627    0.0000    0.0000;
                                0.1529    0.2667    0.0000    0.0000;
                                0.1569    0.2706    0.0000    0.0000;
                                0.1608    0.2745    0.0000    0.0000;
                                0.1647    0.2784    0.0000    0.0000;
                                0.1686    0.2824    0.0000    0.0000;
                                0.1725    0.2863    0.0000    0.0000;
                                0.1765    0.2902    0.0000    0.0000;
                                0.1804    0.2941    0.0000    0.0000;
                                0.1843    0.2941    0.0000    0.0000;
                                0.1882    0.2980    0.0000    0.0000;
                                0.1922    0.3020    0.0000    0.0000;
                                0.1961    0.3059    0.0000    0.0000;
                                0.2000    0.3098    0.0000    0.0000;
                                0.2039    0.3137    0.0000    0.0000;
                                0.2078    0.3176    0.0000    0.0000;
                                0.2118    0.3216    0.0000    0.0000;
                                0.2157    0.3255    0.0000    0.0000;
                                0.2196    0.3294    0.0000    0.0000;
                                0.2235    0.3294    0.0000    0.0000;
                                0.2275    0.3333    0.0000    0.0000;
                                0.2314    0.3412    0.0000    0.0000;
                                0.2353    0.3412    0.0000    0.0000;
                                0.2392    0.3451    0.0000    0.0000;
                                0.2431    0.3490    0.0000    0.0000;
                                0.2471    0.3529    0.0000    0.0000;
                                0.2510    0.3569    0.0000    0.0000;
                                0.2549    0.3569    0.0000    0.0000;
                                0.2588    0.3647    0.0000    0.0000;
                                0.2627    0.3686    0.0000    0.0000;
                                0.2667    0.3686    0.0000    0.0000;
                                0.2706    0.3765    0.0000    0.0000;
                                0.2745    0.3765    0.0000    0.0000;
                                0.2784    0.3804    0.0000    0.0000;
                                0.2824    0.3843    0.0000    0.0000;
                                0.2863    0.3882    0.0000    0.0000;
                                0.2902    0.3922    0.0000    0.0000;
                                0.2941    0.3961    0.0000    0.0000;
                                0.2980    0.4000    0.0000    0.0000;
                                0.3020    0.4039    0.0000    0.0000;
                                0.3059    0.4039    0.0000    0.0000;
                                0.3098    0.4118    0.0000    0.0000;
                                0.3137    0.4118    0.0000    0.0000;
                                0.3176    0.4157    0.0000    0.0000;
                                0.3216    0.4235    0.0000    0.0000;
                                0.3255    0.4235    0.0000    0.0000;
                                0.3294    0.4275    0.0000    0.0000;
                                0.3333    0.4314    0.0000    0.0000;
                                0.3373    0.4353    0.0000    0.0000;
                                0.3412    0.4392    0.0000    0.0000;
                                0.3451    0.4392    0.0000    0.0000;
                                0.3490    0.4471    0.0000    0.0000;
                                0.3529    0.4510    0.0000    0.0000;
                                0.3569    0.4510    0.0078    0.0000;
                                0.3608    0.4588    0.0196    0.0000;
                                0.3647    0.4588    0.0275    0.0000;
                                0.3686    0.4627    0.0431    0.0000;
                                0.3725    0.4667    0.0510    0.0000;
                                0.3765    0.4706    0.0627    0.0000;
                                0.3804    0.4745    0.0745    0.0000;
                                0.3843    0.4784    0.0863    0.0000;
                                0.3882    0.4824    0.0980    0.0000;
                                0.3922    0.4863    0.1059    0.0000;
                                0.3961    0.4863    0.1216    0.0000;
                                0.4000    0.4902    0.1294    0.0000;
                                0.4039    0.4941    0.1412    0.0000;
                                0.4078    0.4980    0.1529    0.0000;
                                0.4118    0.5020    0.1647    0.0000;
                                0.4157    0.5059    0.1765    0.0000;
                                0.4196    0.5098    0.1843    0.0000;
                                0.4235    0.5137    0.1961    0.0000;
                                0.4275    0.5176    0.2118    0.0000;
                                0.4314    0.5216    0.2196    0.0000;
                                0.4353    0.5216    0.2314    0.0000;
                                0.4392    0.5255    0.2392    0.0000;
                                0.4431    0.5333    0.2549    0.0000;
                                0.4471    0.5333    0.2667    0.0000;
                                0.4510    0.5373    0.2745    0.0000;
                                0.4549    0.5412    0.2863    0.0000;
                                0.4588    0.5451    0.2980    0.0000;
                                0.4627    0.5490    0.3098    0.0000;
                                0.4667    0.5529    0.3216    0.0000;
                                0.4706    0.5569    0.3294    0.0000;
                                0.4745    0.5608    0.3451    0.0000;
                                0.4784    0.5608    0.3529    0.0000;
                                0.4824    0.5686    0.3647    0.0000;
                                0.4863    0.5686    0.3765    0.0000;
                                0.4902    0.5725    0.3882    0.0000;
                                0.4941    0.5765    0.4000    0.0000;
                                0.4980    0.5804    0.4078    0.0000;
                                0.5020    0.5843    0.4235    0.0000;
                                0.5059    0.5882    0.4314    0.0000;
                                0.5098    0.5922    0.4431    0.0000;
                                0.5137    0.5961    0.4549    0.0000;
                                0.5176    0.5961    0.4667    0.0000;
                                0.5216    0.6039    0.4784    0.0000;
                                0.5255    0.6039    0.4863    0.0000;
                                0.5294    0.6078    0.4980    0.0000;
                                0.5333    0.6157    0.5137    0.0000;
                                0.5373    0.6157    0.5216    0.0000;
                                0.5412    0.6196    0.5333    0.0000;
                                0.5451    0.6235    0.5412    0.0000;
                                0.5490    0.6275    0.5569    0.0000;
                                0.5529    0.6314    0.5686    0.0000;
                                0.5569    0.6314    0.5765    0.0000;
                                0.5608    0.6392    0.5882    0.0000;
                                0.5647    0.6431    0.6000    0.0000;
                                0.5686    0.6431    0.6118    0.0000;
                                0.5725    0.6510    0.6235    0.0000;
                                0.5765    0.6510    0.6314    0.0000;
                                0.5804    0.6549    0.6471    0.0000;
                                0.5843    0.6588    0.6549    0.0000;
                                0.5882    0.6627    0.6667    0.0000;
                                0.5922    0.6667    0.6745    0.0000;
                                0.5961    0.6706    0.6745    0.0000;
                                0.6000    0.6745    0.6745    0.0118;
                                0.6039    0.6784    0.6745    0.0196;
                                0.6078    0.6784    0.6745    0.0353;
                                0.6118    0.6863    0.6745    0.0471;
                                0.6157    0.6863    0.6745    0.0549;
                                0.6196    0.6902    0.6745    0.0667;
                                0.6235    0.6941    0.6745    0.0784;
                                0.6275    0.6980    0.6745    0.0902;
                                0.6314    0.7020    0.6745    0.0980;
                                0.6353    0.7059    0.6745    0.1098;
                                0.6392    0.7098    0.6745    0.1255;
                                0.6431    0.7137    0.6745    0.1333;
                                0.6471    0.7137    0.6745    0.1451;
                                0.6510    0.7176    0.6745    0.1529;
                                0.6549    0.7255    0.6745    0.1686;
                                0.6588    0.7255    0.6745    0.1804;
                                0.6627    0.7294    0.6745    0.1882;
                                0.6667    0.7333    0.6745    0.2000;
                                0.6706    0.7373    0.6745    0.2118;
                                0.6745    0.7412    0.6745    0.2235;
                                0.6784    0.7451    0.6745    0.2353;
                                0.6824    0.7490    0.6745    0.2471;
                                0.6863    0.7529    0.6745    0.2588;
                                0.6902    0.7529    0.6745    0.2667;
                                0.6941    0.7608    0.6745    0.2784;
                                0.6980    0.7608    0.6745    0.2902;
                                0.7020    0.7647    0.6745    0.3020;
                                0.7059    0.7686    0.6745    0.3137;
                                0.7098    0.7725    0.6745    0.3216;
                                0.7137    0.7765    0.6745    0.3373;
                                0.7176    0.7804    0.6745    0.3490;
                                0.7216    0.7843    0.6745    0.3569;
                                0.7255    0.7882    0.6745    0.3686;
                                0.7294    0.7882    0.6745    0.3804;
                                0.7333    0.7961    0.6745    0.3922;
                                0.7373    0.7961    0.6745    0.4039;
                                0.7412    0.8000    0.6745    0.4118;
                                0.7451    0.8078    0.6745    0.4275;
                                0.7490    0.8078    0.6745    0.4353;
                                0.7529    0.8118    0.6745    0.4471;
                                0.7569    0.8157    0.6745    0.4549;
                                0.7608    0.8196    0.6745    0.4706;
                                0.7647    0.8235    0.6745    0.4824;
                                0.7686    0.8235    0.6745    0.4902;
                                0.7725    0.8314    0.6745    0.5020;
                                0.7765    0.8353    0.6745    0.5137;
                                0.7804    0.8353    0.6745    0.5255;
                                0.7843    0.8431    0.6745    0.5373;
                                0.7882    0.8431    0.6745    0.5490;
                                0.7922    0.8471    0.6745    0.5608;
                                0.7961    0.8510    0.6745    0.5686;
                                0.8000    0.8549    0.6745    0.5804;
                                0.8039    0.8588    0.6745    0.5922;
                                0.8078    0.8627    0.6745    0.6039;
                                0.8118    0.8667    0.6745    0.6157;
                                0.8157    0.8706    0.6745    0.6235;
                                0.8196    0.8706    0.6745    0.6392;
                                0.8235    0.8784    0.6745    0.6510;
                                0.8275    0.8784    0.6745    0.6588;
                                0.8314    0.8863    0.6745    0.6745;
                                0.8353    0.8863    0.6745    0.6745;
                                0.8392    0.8863    0.6745    0.6745;
                                0.8431    0.8863    0.6745    0.6745;
                                0.8471    0.8863    0.6745    0.6745;
                                0.8510    0.8863    0.6745    0.6745;
                                0.8549    0.8863    0.6745    0.6745;
                                0.8588    0.8863    0.6745    0.6745;
                                0.8627    0.8863    0.6745    0.6745;
                                0.8667    0.8863    0.6745    0.6745;
                                0.8706    0.8863    0.6745    0.6745;
                                0.8745    0.8863    0.6745    0.6745;
                                0.8784    0.8863    0.6745    0.6745;
                                0.8824    0.8863    0.6745    0.6745;
                                0.8863    0.8863    0.6745    0.6745;
                                0.8902    0.8863    0.6745    0.6745;
                                0.8941    0.8863    0.6745    0.6745;
                                0.8980    0.8863    0.6745    0.6745;
                                0.9020    0.8863    0.6745    0.6745;
                                0.9059    0.8863    0.6745    0.6745;
                                0.9098    0.8863    0.6745    0.6745;
                                0.9137    0.8863    0.6745    0.6745;
                                0.9176    0.8863    0.6745    0.6745;
                                0.9216    0.8863    0.6745    0.6745;
                                0.9255    0.8863    0.6745    0.6745;
                                0.9294    0.8863    0.6745    0.6745;
                                0.9333    0.8863    0.6745    0.6745;
                                0.9373    0.8863    0.6745    0.6745;
                                0.9412    0.9961    0.9961    0.9961;
                                0.9451    0.9961    0.0000    0.0000;
                                0.9490    0.0000    0.9961    0.0000;
                                0.9529    0.0000    0.0000    0.0000;
                                0.9569    0.4588    0.8314    0.0431;
                                0.9608    0.5176    0.7216    0.7922;
                                0.9647    0.3647    0.3569    0.6275;
                                0.9686    0.5137    0.8510    0.9020;
                                0.9725    0.9961    0.0000    0.0000;
                                0.9765    0.0000    0.9961    0.9961;
                                0.9804    0.0000    0.9961    0.0000;
                                0.9843    0.9961    0.0000    0.9961;
                                0.9882    0.0000    0.0000    0.9961;
                                0.9922    0.9961    0.9961    0.0000;
                                0.9961    0.9961    0.9961    0.9961;
                                1.0000    0.0000    0.0000    0.0000 ];
            };
    end

    function maps = getMatlabCmaps()
    % GTCOLORMAP/GETMATLABCMAPS
    % maps = getMatlabCmaps()
        def_maps = GtColormap.initialize();
        maps = def_maps.matlab;
    end

    function maps = getDataCmaps()
    % GTCOLORMAP/GETDATACMAPS
    % maps = getDataCmaps()
        def_maps = GtColormap.initialize();
        maps = def_maps.data;
    end

    function maps = getAutogenCmaps()
    % GTCOLORMAP/GETAUTOGENCMAPS
    % maps = getAutogenCmaps()
        def_maps = GtColormap.initialize();
        maps = def_maps.autogen;
    end

    function mapdef = getCmapDef(mapname, steps)
    % GTCOLORMAP/GETCMAPDEF
    % mapdef = getCmapDef(mapname, steps)
        mapdef = eval([mapname '(' num2str(steps+1) ')']);
    end

    function rgb = getRGBValues(x, mapdef, steps)
    % GTCOLORMAP/GETRGBVALUES
    % rgb = getRGBValues(x, mapdef, steps)
        x = reshape(x, [], 1);
        rgb = mapdef(round(abs(x)*steps+1), :);
    end

    function [CMap, Pos] = getSelectedCmap(DefLength, MapName, MapLength)
    % GTCOLORMAP/GETSELECTEDCMAP
    % [CMap, Pos] = getSelectedCmap(DefLength, MapName, MapLength)

        if ismember(MapName, GtColormap.getMatlabCmaps())
            CMap = GtColormap.getCmapDef(MapName, MapLength);
            Pos = [];
        else
            [CMap, Pos] = GtColormap.setNewCmap(DefLength, MapName, MapLength+1);
        end
    end

    function [CMap, pos] = setNewCmap(DefLength, MapName, varargin)
    % GTCOLORMAP/SETNEWCMAP
    % [CMap, pos] = setNewCmap(DefLength, MapName, varargin)
    % apply or retrieve either predefined or generated colormap

        % default parameters
        [Length, ColorPos, InterpMode] = GtColormap.parseParameters(varargin, DefLength);

        DataMaps = GtColormap.getDataCmaps();
        [~, Idx] = ismember(MapName, DataMaps(:, 1));
        if Idx>0
            % apply or retrieve predefined colormap
            CTable = DataMaps{Idx, 2};
        else
            % apply or retrieve generated colormap
            if length(MapName)>1
                if (GtColormap.isColormapNameValid(MapName))
                    CTable = GtColormap.string2CmapData(MapName, 'ColorPositions', ColorPos);
                else
                    gtError('GtColormap:setNewCmap:InvalidColormapString', 'Invalid colormap string: "%s"', MapName);
                end
            else
                gtError('GtColormap:setNewCmap:ColormapStringTooShort', 'Colormap string too short: "%s"', MapName);
            end
        end

        [CMap, pos] = GtColormap.interpolateCTable(CTable, Length, InterpMode);
    end

    function map = showSupportedCmaps(hObj, ~, onBlack)
    % GTCOLORMAP/SHOWSUPPORTEDCMAPS
    % map = showSupportedCmaps(onBlack)

        if ~exist('onBlack', 'var') || isempty(onBlack)
            onBlack = false;
        end
        maps        = GtColormap.initialize();
        maps        = vertcat(maps.matlab, maps.autogen, maps.data(:, 1));
        NMaps       = length(maps);
        RowsPerMap  = 20;
        Img     = ones(NMaps*RowsPerMap, 256, 3);
        ImgSpc  = ones(size(Img).*[1 0 1]+[0 15 0]);
        if (onBlack)
            Img     = zeros(NMaps*RowsPerMap, 256, 3);
            ImgSpc  = zeros(size(Img).*[1 0 1]+[0 15 0]);
        end
        keyMap = {};
        for kk=1:NMaps
            m   = GtColormap.getSelectedCmap(256-1, maps{kk}, 256-1);
            r0  = (kk-1)*RowsPerMap+1;
            Img(r0, :, 1) = m(:, 1);
            Img(r0, :, 2) = m(:, 2);
            Img(r0, :, 3) = m(:, 3);
            for r=1:RowsPerMap-2
                Img(r0+r, :, :) = Img(r0, :, :);
            end
            keyMap{kk} = m;
        end
        ImgGray(:, :, 1)  = rgb2gray(Img);
        ImgGray(:, :, 2)  = ImgGray(:, :, 1);
        ImgGray(:, :, 3)  = ImgGray(:, :, 1);
        Img = [ ImgGray ImgSpc Img ];

        if nargout > 0
            map = Img;
        else
            hf = figure('Tag', 'h_show_fig');
            h_show_im = imshow(permute(Img, [2 1 3]));
            set(gca, 'Position', [0.05 0.05 0.9 0.9])
            title(['RGB' repmat(' ', 1, 48) 'gray']);
            set(get(get(hf, 'CurrentAxes'), 'Title'), 'VerticalAlignment', 'middle')
            set(get(get(hf, 'CurrentAxes'), 'Title'), 'Position', [-20 265 1])
            set(get(get(hf, 'CurrentAxes'), 'Title'), 'Rotation', 90)
            NewImg = permute(Img, [2 1 3]);
            set(hf, 'SelectionType', 'alt')
            cbk = @(src, evt)GtColormap.getCurrentPosMapName(src, evt, size(NewImg), maps, RowsPerMap);
            set(hf, 'WindowButtonMotionFcn', cbk)
            cbk2 = @(src, evt)GtColormap.copyCurrentPosMapName(src, evt, keyMap, hObj);
            set(hf, 'WindowButtonDownFcn', cbk2)
        end
    end

    function plotCbar(ha, cs, CBar, Name, defname, nsteps, maxV)
    % GTCOLORMAP/PLOTCBAR
    % plotCbar(ha, cs, CBar, Name, defname, nsteps, maxV)

        if exist('ha', 'var') && ~isempty(ha)
            hf = gtGetParentFigure(ha);
        else
            hf = obj.h_fig;
            ha = get(hf, 'CurrentAxes');
        end
        if ~exist('cs', 'var') || isempty(cs)
            cs = '';
        end

        switch (defname)
            case 'Rvec'
                [h.fig, h.hc] = gtColorspace(1, {'Rx', 'Ry', 'Rz'}, 'FontSize', 12, 'LineWidth', 1.5);
                h.ax = get(h.fig, 'CurrentAxes');
                curr_view = get(h.ax, 'View');
                copyobj(get(h.ax, 'Children'), ha);
                set(ha, 'Visible', 'off')
                set(ha, 'View', curr_view);
                close(h.fig)
            case 'IPF'
                h = gtIPFCmapKey('hf', hf, 'ha', ha, 'crystal_system', cs, 'figcolor', get(hf, 'Color'));
            case 'Caxis'
                [h.fig, h.ax, h.patch] = gtCrystAxisCmap_key([], {'100', '010', '001'}, false, 12);
                copyobj(get(h.ax, 'Children'), ha)
                axis(ha, 'square')
                close(h.fig)
            case 'Rand'
                disp('Random colormap... No colorbar definition in this case')
            otherwise
                [tmp_r, ~] = size(CBar);
                figure('Color', 'w', 'Name', Name, ...
                       'Units', 'Normalized', 'Position', [0.2 0.3 0.5 0.2], ...
                       'NumberTitle', 'off', 'WindowStyle', 'normal', 'ToolBar', 'none');
                if isempty(maxV)
                    maxValue = 1;
                else
                    maxValue = maxV;
                end
                tmp_k = (0:(tmp_r-1))/(tmp_r-1)*maxValue;
                for ii = 1:tmp_r
                    line([tmp_k(ii) tmp_k(ii)], [0 1], 'LineStyle', '-', 'Color', CBar(ii, :), 'LineWidth', nsteps+1);
                end
                xlim([0 maxValue]);
                ylim([0 1]);
                set(gca, 'YTick', []);
                title(Name, 'Interpreter', 'none');
        end
    end

    function plotCmap(CMap, Name)
    % GTCOLORMAP/PLOTCMAP
    % plotCmap(obj, Name)

        [tmp_r, ~] = size(CMap);
        tmp_gm = rgb2gray(CMap);
        figure('Color', 'w', 'Name', Name, 'Tag', 'h_fig_cmap', ...
               'Units', 'Normalized', 'Position', [0.2 0.3 0.5 0.2], ...
               'NumberTitle', 'off', 'WindowStyle', 'normal', 'ToolBar', 'none');
        tmp_k = (0:(tmp_r-1))/(tmp_r-1);
        plot(tmp_k, CMap(:, 1), 'r-', ...
             tmp_k, CMap(:, 2), 'g-', ...
             tmp_k, CMap(:, 3), 'b-', ...
             tmp_k, tmp_gm(:, 1), 'k-', ...
             'LineWidth', 2);
        xlim([0 1]);
        ylim([0 1]);
        title(Name, 'Interpreter', 'none');
    end

    function copyCurrentPosMapName(h_fig, ~, keyMap, hObj)
        index = getappdata(h_fig, 'index');
        mapName = getappdata(h_fig, 'mapName');
        if ~isempty(index)
            fprintf('Colormap: %s ID: %d\n', mapName, index)

            if isprop(hObj, 'UserData')
                set(hObj, 'UserData', {mapName, index, keyMap{index}})
            end
            if ishandle(gtGetParentFigure(hObj))
                h_fig = gtGetParentFigure(hObj);
            end
            setappdata(h_fig, 'map', keyMap{index})
            setappdata(h_fig, 'mapName', mapName)
            setappdata(h_fig, 'index', index)
        end
    end

    function getCurrentPosMapName(h_fig, ~, img_size, maps, rowspermap)

        % get mouse position on axis
        mousePos = get(get(h_fig, 'CurrentAxes'), 'CurrentPoint');
        UV = mousePos(1, 1:2);
        set(get(h_fig, 'CurrentAxes'), 'Units', 'pixels')
        ax_pos = int16(get(get(h_fig, 'CurrentAxes'), 'Position'));
        % If it is outside the image, discard it
        if UV(1) < 0 || UV(1) > img_size(2) || ...
                UV(2) < 0 || UV(2) > img_size(1)
            UV = [];
        end
        % build string to display
        if ~isempty(UV)
            value = sprintf('(%5.1f, %5.1f)', UV(1), UV(2));
            index = ceil(UV(1)/(rowspermap));
            mapName = maps{index};
            title_fig = sprintf('Colormap: %s ID: %d', mapName, index);

            setappdata(h_fig, 'mapName', mapName)
            setappdata(h_fig, 'index', index)
        else
            title_fig = 'No colormap selected';
        end

        set(h_fig, 'Name', title_fig)
    end

    function res = char2RGB(c)
    % GTCOLORMAP/CHAR2RGB
    % res = char2RGB(obj, c)
    % Converts COLORSPEC character into RGB triplet

        colorTable  = [
            0.0 0.0 0.0 % k     black
            1.0 0.0 0.0 % r     red
            1.0 0.0 0.5 % p     pink
            1.0 0.5 0.0 % o     orange
            0.0 1.0 0.0 % g     green
            0.5 1.0 0.0 % l     lime green
            0.0 1.0 0.5 % a     aquamarine
            0.0 0.0 1.0 % b     blue
            0.0 0.5 1.0 % s     sky blue
            0.5 0.0 1.0 % v     violet
            0.0 1.0 1.0 % c     cyan
            1.0 0.0 1.0 % m     magenta
            1.0 1.0 0.0 % y     yellow
            1.0 1.0 1.0 % w     white
            ];
        colorCodes = 'krpoglabsvcmyw';

        idx = find(double(colorCodes)==double(c), 1, 'first');
        if idx
            res = colorTable(idx, :);
        else
            gtError('GtColormap:char2RGB:UnknownColor', 'Color character MUST be one of the following characters: "%s"! (character "%s" is not)', ColorCodes, c);
        end
    end

    function Result = isColormapNameValid(s)
    % GTCOLORMAP/ISCOLORMAPNAMEVALID
    % Result = isColormapNameValid(s)
    % Checks wether string is a valid colormap name

        Result = all(ismember(s, 'krpoglabsvcmyw '));
    end

    function res = string2CmapData(s, varargin)
    % GTCOLORMAP/STRING2CMAPDATA
    % res = string2CmapData(s, varargin)
    % Converts string of characters into valid colormap data

        if ~isempty(regexp(s, '\s+', 'once'))
            cGroups = regexp(s, '\s+', 'split');
            LL          = length(cGroups);
            pos0        = (0:(LL-1)) / (LL-1);
            pos         = GtColormap.getParameter('ColorPositions', varargin, pos0(:));
            if isempty(pos)
                pos = pos0;
            end
            if LL~=length(pos)
                gtError('GtColormap:InvalidColorPositionsLength', 'Lengths of ColorPos vector (%d) does not match number of groups (%d) in colormap string "%s"', length(pos), LL, s);
            end
            res         = zeros(LL, 4);
            res(:, 1)    = pos(:);
            for kk=1:LL
                LGroup      = length(cGroups{kk});
                for p=1:LGroup
                    res(kk, 2:4)  = res(kk, 2:4) + GtColormap.char2RGB(cGroups{kk}(p));
                end
                res(kk, 2:4)  = res(kk, 2:4) / LGroup;
            end
        else
            LL          = length(s);
            pos0        = (0:(LL-1)) / (LL-1);
            pos         = GtColormap.getParameter('ColorPositions', varargin, pos0(:));
            if isempty(pos)
                pos = pos0;
            end
            if LL~=length(pos)
                gtError('GtColormap:InvalidColorPositionsLength', 'Lengths of ColorPos vector (%d) does not match length (%d) of colormap string "%s"', length(pos), LL, s);
            end
            res         = zeros(LL, 4);
            res(:, 1)    = pos;
            for kk=1:LL
                res(kk, 2:4) = GtColormap.char2RGB(s(kk));
            end
        end
    end

    function [CMap, pos] = interpolateCTable(color_tab, color_len, interp_mode)
    % GTCOLORMAP/INTERPOLATECTABLE
    % [CMap, pos] = interpolateCTable(CTable, Length, InterpMode)

        [CR, ~] = size(color_tab);
        if (color_len < 0)
            gtError('GtColormap:NegativeColormapLength', 'Colormap length can not be negative!');
        elseif (color_len == 0)
            CMap = color_tab(:, 2:4);
        else
            if (color_len < CR)
                color_len = CR;
            end
            %ii0 = 0:(CR-1);
            tt0 = color_tab(:, 1);
            iin = 0:(color_len-1);
            ttn = iin/(color_len-1);
            [x0, y0] = meshgrid(1:3, tt0);
            [xn, yn] = meshgrid(1:3, ttn);
            CMap = interp2(x0, y0, color_tab(:, 2:4), xn, yn, interp_mode); % 'nearest'   'linear'    'spline'    'cubic'
            CMap(CMap < 0) = 0;
            CMap(CMap > 1) = 1;

            if (nargout == 2)
                pos = tt0;
            end
        end
    end

    function [color_length, color_pos, interp_mode] = parseParameters(varin, tL)
    % GTCOLORMAP/PARSEPARAMETERS
    % [Length, ColorPos, InterpMode] = parseParameters(varin, tL)

        color_length = [];
        color_pos = [];
        interp_mode = 'linear';

        if ~isempty(varin)
            if (isreal(varin{1}) && isscalar(varin{1}))
                color_length = varin{1};
            end
            if length(varin)>1
                if (isreal(varin{2}) && isvector(varin{2}) && (numel(varin{2})==tL))
                    color_pos = varin{2};
                end
                if length(varin)>2
                    if (ischar(varin{3}) && isvector(varin{3}))
                        interp_mode = varin{3};
                    end
                end
            end
        end
    end

    function res = getParameter(par_name, name_value_array, def_val)
    % GTCOLORMAP/GETPARAMETER
    % res = getParameter(PName, NameValueArray, DefaultValue)
    % Looks for parameter named PNAME in NAMEVALUEARRAY cell array of name-value pairs
    % and returns its vaule.
    % If name is not found, DEFAULTVALUE is returned

        res = def_val;   % in case PName is not present in PNameValArray
        for ii = 1:numel(name_value_array)
            if (strcmpi(name_value_array{ii}, par_name))
                if (numel(name_value_array) > ii)
                    res = name_value_array{ii+1};
                    break;
                else
                    gtError('%s: Parameter "%s" present, but its value absent', mfilename, par_name);
                end
            end
        end
    end
end % end of methods static, public

end % end of class
