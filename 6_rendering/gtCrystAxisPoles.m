function [poles, cmap, valids, vargs_out] = gtCrystAxisPoles(phaseid, pl_norm, varargin)
% GTCRYSTAXISPOLES  Makes a colourmap according to a crystallographic axis
%                   orientation and gives the colormap
%     [poles, cmap, valids, vargs_out] = gtCrystAxisPoles([phaseid], [pl_norm], varargin)
%     -----------------------------------------------------------------------------------
%     Calculates poles wrt a crystallographic direction using r_vectors.
%     Loads parameters.mat:cryst(phaseid):latticepar, crystal_system
%
%     INPUT:
%       phaseid          = <int>        phase number {1}
%       pl_norm          = <double>     hkil of the plane normal wrt calculate the poles (1,3(4)) 
%                                       {[0 0 0 2]},[2 -1 -1 0] for hexagonal, [0 0 1],[0 1 0] for cubic...
%
%     OPTIONAL INPUT (varargin as a list of pairs, see parse_pv_pairs.m)
%       'crystal_system' = <string>     Legacy to set (force) the crystal system
%                                       for old datasets
%       'latticepar'     = <double>     Lattice parameters to convert from hex to
%                                       cartesian base
%       'r_vectors'      = <double>     Rodrigues vectors as N x 3 array
%       'axesRot'        = <double>     vector to trasform sample reference. It rotates
%                                       poles basing on component order in this vector {[1 2 3]}
%       'background'     = <logical>    Keep or remove the background color in first row {true}
%       'save'           = <logical>    flag to save file {false}
%       'convention'     = <string>     Hexagonal axis convention {'X'}
%
%     OUTPUT:
%       poles            = <double>     list of poles direction of all the grains (M,4)
%                                       We keep the index of the reference r_vector (first column)
%                                       NB. M is different from N because of the crystal symmetry.
%       cmap             = <double>     poles colourmap (N,4)
%       valids           = <double>     poles list (only one per orientation) (N,3)
%       vargs_out        = <struct>     application data modified
%
%     Version 005 18-09-2013 by LNervo
%       Added sample axes rotation option; use of gtCrystSignedHKLs
%
%     Version 004 10-01-2013 by LNervo
%       Bug fix with choice of positive poles; choice of a pole for the grain
%       colour
%
%     Version 003 19-10-2012 by YGuilhem
%       Factorized using gtVectorOrientationColor
%
%     Version 002 28-06-2012 by LNervo
%       Update to version 2 of parameters; cleaning formatting

% Default parameters
app.crystal_system = '';
app.latticepar     = [];
app.r_vectors      = [];
app.axesRot        = [1 2 3];
app.background     = true;
app.save           = false;
app.convention     = 'X';
[app, rej_pars] = parse_pv_pairs(app, varargin);

% Set default phaseid
if ~exist('phaseid','var') || isempty(phaseid)
    phaseid = 1;
end

% Set default crystal direction if pl_norm empty
if ~exist('pl_norm','var') || isempty(pl_norm)
    pl_norm = [0 0 1];
end

r_vectors = [];
phaseDir = fullfile('4_grains', sprintf('phase_%02d', phaseid));
if ~isempty(app.r_vectors)
    r_vectors = app.r_vectors;
elseif exist(fullfile(phaseDir, 'r_vectors.mat'), 'file')
    load(fullfile(phaseDir, 'r_vectors.mat'));
elseif exist(fullfile(phaseDir, 'index.mat'), 'file')
    grain = [];
    load(fullfile(phaseDir, 'index.mat'), 'grain');
    r_vectors = gtIndexAllGrainValues(grain, 'R_vector', [], 1, 1:3);
else
    gtError('gtCrystAxisPoles:missing_file', 'Could not find r_vectors.mat or index.mat file!');
end

% Resize r_vectors if needed
if size(r_vectors,2) == 4
    r_vectors = r_vectors(:,2:4);
end

% Get the crystal system
if isempty(app.crystal_system)
    parameters = load('parameters.mat');
    app.crystal_system = parameters.parameters.cryst(phaseid).crystal_system;
end
% Get the lattice parameters
if isempty(app.latticepar)
    parameters = load('parameters.mat');
    app.latticepar = parameters.parameters.cryst(phaseid).latticepar;
end

% convert to Miller-Bravais notation if the case
if strcmpi(app.crystal_system, 'hexagonal') && size(pl_norm,2) == 3
    pl_norm = gtCrystMiller2FourIndexes(pl_norm,'plane');
end

% Compute all orientation matrices g
%   Gcry = g Gsam   --> Gsam = Gcry inv(g)
%   g = U = gtMathsRod2OriMat(r_vectors.')
all_g = gtMathsRod2OriMat(r_vectors.');


% take all the symmetric equivalents
pl_norm_symm = gtCrystSignedHKLs(pl_norm, gtCrystGetSymmetryOperators(app.crystal_system));
% finds the original pl_norm among the list of its symmetries
[~,ind,~] = findDifferentRowIntoMatrix(pl_norm_symm, pl_norm);

% going to cartesian reciprocal space + normalisation of the cartesian space
% PLnorm = pl_norm normalised and symmetrised

% Temporary fix by PR due to bug in gtHex2Cart
% In addition to bug, note this when expressing normalised hkls (PLs) in the cartesian 
% SAMPLE CS from the Rodrigues vectors: the GT processing upstream used a 
% certain convention, so using gtHex2Cart function here may result 
% in a different definition of the Cartesian crystal system!
% Instead, gtCrystHKL2CartesianMatrix does the job for any structure, even 
% hexagonal!
try
    PLnorm = gtHex2Cart(pl_norm_symm, app.latticepar, app.convention);
	% Express normalised hkls (PLs) in cartesian SAMPLE CS
    PLs = gtVectorCryst2Lab(PLnorm, all_g);
catch
	warning('Problem with gtHex2Cart. Using standard DCT convention for the Cartesian crystal reference!')
	Bmat   = gtCrystHKL2CartesianMatrix(par.latticepar);
	PLnorm = gtCrystHKL2Cartesian(pl_norm_symm', Bmat);
	PLs    = gtVectorCryst2Lab(PLnorm', all_g);
end


poles = zeros(0,4);
valid_poles = zeros(0,4);
for ii = 1:length(all_g)
    normals = PLs(:, :, ii);

    n_bkp = normals;
    % rotate
    if app.axesRot(1) < 0
        normals(:,1) = -n_bkp(:,app.axesRot(1));
    else
        normals(:,1) = n_bkp(:,app.axesRot(1));
    end
    if app.axesRot(2) < 0
        normals(:,2) = -n_bkp(:,app.axesRot(2));
    else
        normals(:,2) = n_bkp(:,app.axesRot(2));
    end
    if app.axesRot(3) < 0
        normals(:,3) = -n_bkp(:,app.axesRot(3));
    else
        normals(:,3) = n_bkp(:,app.axesRot(3));
    end

    for jj = 1:size(PLs, 1)
        % takes the positive poles and saves the grainid which it refers to
        if (normals(jj, 3) >= 0)
            poles(end+1, 1) = ii;
            poles(end, 2:4) = normals(jj, :);
        else
            poles(end+1, 1) = ii;
            poles(end, 2:4) = -normals(jj, :);
        end
        % hard coded: among the symmetries takes the original pl_norm
        if (ind == jj)
            valid_poles(end+1, :) = poles(end, :);
        end
    end
end

% sorts the poles by rows and removes the duplicates
tmp = sortrows(poles,[1 2]);
poles = unique(tmp,'rows');
clear tmp

if nargout > 1
    cmap = gtCrystAxisCmap(valid_poles);
    if (~app.background)
        cmap(1,:) = [];
    end
    if nargout > 2
        valids = valid_poles;
        if nargout > 3
            app.r_vectors = r_vectors;
            app.phaseid = phaseid;
            app.crystDir = pl_norm;
            vargs_out = struct2pv(app);
        end
    end
end


if (app.save)
    pl_norm_str = num2str(pl_norm);
    pl_norm_str = strrep(pl_norm_str,' ','');

    % saves the list of poles to be used in the pole figure
    fname = fullfile('6_rendering',['poles_' pl_norm_str '.mat']);
    save(fname,'poles'); % list of the poles
    disp(['Saved poles list in ' fname])

    % Save the colourmap
    fname = fullfile('6_rendering',['poles_cmap_' pl_norm_str '.mat']);
    save(fname,'cmap'); % list of the poles
    disp(['Saved colormap according to the poles list in ' fname])
end


end % end of function
