
function volout = gtThinCrack2(vol)

%label only the centres of a crack
% given a thresholded crack




%%%%%%  Close the crack   %%%%%%%
volout=zeros(size(vol));
vol=uint8(vol>0); %crack=1, material=0

% vol(y,x,z)

for y=1:size(vol,1)
  for x=1:size(vol,2)

       
    tmp=squeeze(vol(y, x, :));
    
    if max(tmp(:))>0
    
    tmp=bwlabel(tmp);
    
    for i=1:max(tmp(:))
        
        z=find(tmp==i);
        z=round(mean(z));
        
        volout(y,x,z)=255;
        
    end
    
    %test if we have some crack
    end

    %loop through the volume
  end
end
