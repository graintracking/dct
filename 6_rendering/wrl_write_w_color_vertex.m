function wrl_write_w_color_vertex(fv, name)
%fv is face vertex structure with
% fv.vertices specified
% fv.faces specified
% fv.FaceVertexCData specified
% name is the file name (string), either name or name.wrl
lnm = length(name);
if name((lnm-3):(lnm-1))=='wrl'
  label = name(1:lnm-3);
else
  label = name;
  name = sprintf('%s.wrl', name);
end
fid = fopen(name,'w');
fprintf(fid,'#VRML V2.0 utf8 \n');
fprintf(fid,'Viewpoint { description "Initial view" position 0.0 0.0 9.0} \n');
fprintf(fid,'NavigationInfo { type "EXAMINE" }\n');
fprintf(fid,'Transform {\n');
fprintf(fid,' translation -1.5 0.0 0.0 \n');
fprintf(fid,' children Shape {\n');
fprintf(fid,' appearance DEF A Appearance { material Material { } } \n');
fprintf(fid,' geometry DEF IFS IndexedFaceSet {\n');
fprintf(fid,' coord Coordinate { \n');
fprintf(fid,' point [ \n');

%fprintf ... vertices 1 2 3, next line
for k = 1:length(fv.vertices)
  fprintf(fid,' %0.4f %0.4f %0.4f,\n',fv.vertices(k,1),fv.vertices(k,2),fv.vertices(k,3));
end
fprintf(fid,'] \n');
fprintf(fid,'} \n');
fprintf(fid,'coordIndex [ \n');
%fprintf ... vert 1, 2, 3, -1, next line, if you need more then add
%another vertex 1, 2, 3 ,4, 5, etc, -1
for k = 1:length(fv.faces)
  fprintf(fid,' %0.0f, %0.0f, %0.0f, -1,\n',fv.faces(k,1)-1,fv.faces(k,2)-1,fv.faces(k,3)-1);
end
fprintf(fid,'] \n');
fprintf(fid,' colorPerVertex TRUE \n');
fprintf(fid,' color Color {\n');
fprintf(fid,' color [ \n');
%fprintf ... color R G B, next line
if size(fv.facevertexcdata,2)==3
  disp('Colour')
  for k = 1:length(fv.facevertexcdata)
    fprintf(fid,' %0.4f %0.4f %0.4f,\n',fv.facevertexcdata(k,1),fv.facevertexcdata(k,2),fv.facevertexcdata(k,3));
  end
else
  disp('grayscale')
  for k = 1:length(fv.facevertexcdata)
    fprintf(fid,' %0.4f %0.4f %0.4f,\n',fv.facevertexcdata(k,1),fv.facevertexcdata(k,1),fv.facevertexcdata(k,1));
  end
end
fprintf(fid,'] \n');
fprintf(fid,'} \n');
fprintf(fid,'} \n');
fprintf(fid,'} \n');
fprintf(fid,'} \n');
fclose(fid);


