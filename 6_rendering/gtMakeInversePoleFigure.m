function hf = gtMakeInversePoleFigure(varargin)
% GTMAKEINVERSEPOLEFIGURE  
%     hf = gtMakeInversePoleFigure(varargin)
%     --------------------------------------
%

app.phaseid    = 1;
app.poles      = [];
app.r_vectors  = [];
app.sampleDir  = [0 0 1];   % sample direction
app.ind        = [];
app.weights    = [];
app.N          = 100;
app.searchR    = 0.15;

app.plot_figure = true;     % true | false
app.surface     = false;    % true | false
app.cmap        = [];       % for scatter plots
app.grainids    = [];       % for scatter plots
app.colorbar    = true;     % true | false
app.label_poles = 'main';   % 'all' | 'main' | 'none' | 'bcc' ( only for cubic )
app.mark_poles  = 'main';   % 'all' | 'main' | 'none' | 'bcc' ( only for cubic )
app.markersize  = 10;
app.markertype  = 'o';
app.label       = '';

app.logscale    = false;    % true | false
app.linecolor   = [0 0 0];  % color for rings, labels
app.contourline = 'none';   % can be RGB color or 'none'
app.levels      = 10;       % number of levels for contour and surf
app.colormap    = 'jet';    % 
app.figcolor    = [1 1 1];  % figure color
app.clim        = [];       % colour limits
app.view        = [0 90];   % view of the XY plane RD right and TD top -> 0 90;
                            % RD bottom TD right -> 90 90

app.title       = '';
app.export      = false;    % true | false
app.paper_fig   = false;
app.ws_path     = true;
app.dotFig      = false;

app.debug       = false;    % true | false
app.tag         = '';       % tag for axes and cb
app.fontsize    = 10;       % text size
app.figname     = '';
app.hf          = [];
app.ha          = [];
[app, rej_pars] = parse_pv_pairs(app, varargin);

app.grainIPFColorMap = [];

out = GtConditionalOutput(app.debug);

if all(isequal(app.figcolor, [0 0 0])) || all(app.figcolor <= [0.5 0.5 0.5])
    app.linecolor = [1 1 1];
end

parameters = [];
load('parameters.mat');
if strcmpi(parameters.cryst(app.phaseid).crystal_system, 'hexagonal')
    app.range = 'hex';
else
    app.range = 'sst';
end

print_structure(app, 'Options', false, app.debug)

% figure name/title
figname = [gtGetLastDirName(pwd) '_inversepolefigure_' strrep(num2str(app.sampleDir),' ','') '__' app.range];

if isempty(app.poles)
    vargs_in = struct2pv(app);
    [cmap, app.poles, ~, vargs_out] = gtIPFCmap(app.phaseid, app.sampleDir, vargs_in{:});
    if isempty(app.cmap)
        app.cmap = cmap;
        app.grainIPFColorMap = app.cmap;
    end
    [app, rej_pars] = parse_pv_pairs(app, vargs_out);
end
if isempty(app.grainids)
    app.grainids = 1:length(app.poles);
end
if isempty(app.weights)
    app.weights = ones(size(app.poles,1),1);
end
if ~isempty(app.cmap)
    if all(app.cmap(1,:) == [0 0 0]) && size(app.cmap,1) == length(app.grainids)
        app.cmap(1,:) = [];
    end
end

if ~isempty(app.grainids)
    app.poles   = app.poles(app.grainids,:);
    if ~isempty(app.cmap) && size(app.cmap,1) >= max(app.grainids)
        app.cmap    = app.cmap(app.grainids,:);
    end
    app.weights = app.weights(app.grainids);
end

% before building figure, calculate phimax and psimax (allowing for search radius)
[~, ~, phimax, psimax] = gtFindPolesInRange(app.poles, [], app.range, app.mark_poles);
% analyse the pole data
[x,y,z] = gtCalculateDensityPhiPsi(app.poles, app.weights, [0 phimax], [0 psimax], app.searchR, app.N);

if isempty(app.hf)
    ipf_app = gtIPFCmapKey('crystal_system',parameters.cryst(app.phaseid).crystal_system,...
        'N',app.N,'fontsize',app.fontsize,'drawing',false);

    figure(ipf_app.h_figure)
    hf = ipf_app.h_figure;
    app.ha = ipf_app.h_axes;
    set(ipf_app.h_ipf, 'Color', app.linecolor, 'HandleVisibility', 'off')
    set(ipf_app.h_ipf_dir, 'Color', app.linecolor, 'HandleVisibility', 'off')
    set(ipf_app.h_poles, 'Color', app.linecolor, 'HandleVisibility', 'off')
    set(ipf_app.h_points_plot, 'Color', app.linecolor, 'HandleVisibility', 'off')
    set(ipf_app.h_line_plot, 'Color', app.linecolor, 'HandleVisibility', 'off')
elseif isempty(app.ha) || isempty(get(app.hf,'CurrentAxes'))
    ipf_app = gtIPFCmapKey('hf',app.hf,'crystal_system',parameters.cryst(app.phaseid).crystal_system,...
        'N',app.N,'fontsize',app.fontsize,'drawing',false);

    figure(ipf_app.h_figure)
    hf = ipf_app.h_figure;
    app.ha = ipf_app.h_axes;
    set(ipf_app.h_ipf, 'Color', app.linecolor, 'HandleVisibility', 'off')
    set(ipf_app.h_ipf_dir, 'Color', app.linecolor, 'HandleVisibility', 'off')
    set(ipf_app.h_poles, 'Color', app.linecolor, 'HandleVisibility', 'off')
    set(ipf_app.h_points_plot, 'Color', app.linecolor, 'HandleVisibility', 'off')
    set(ipf_app.h_line_plot, 'Color', app.linecolor, 'HandleVisibility', 'off')
else
    hf = app.hf;
    figure(hf)
    set(findobj(hf, 'type', 'line'), 'HandleVisibility', 'off');
    set(findobj(hf, 'type', 'hggroup'), 'HandleVisibility', 'off');
end
if isempty(app.ha)
    if isempty(get(hf,'CurrentAxes'))
        app.ha = axes('Parent',hf);
    else
        app.ha = get(hf,'CurrentAxes');
    end
end
set(hf,'Units','normalized','Position',[0.1996 0.0865 0.7153 0.7968]);
% update legend
if ~isempty(findobj(hf,'Tag','legend'))
    leg_data      = get(findobj(hf,'Tag','legend'),'UserData');
end

hold(app.ha,'on');
if isempty(app.cmap) 
    if (app.surface)
        surfc(x,y,z);
        shading('interp')
    else
        contourc(x,y,z)
    end
    colorbar;
    colormap( eval([app.colormap '(' num2str(app.N) ')']) )
else
    phi = atan2(app.poles(:,2),app.poles(:,1));
    psi = acos(app.poles(:,3));
    rho = tan(psi/2);
    [x,y] = pol2cart(phi,rho);
    app.colorbar = false;
    h_p = [];
    if size(app.cmap,1) == 1
        app.cmap = repmat(app.cmap, length(x), 1);
    end
    for kk=1:length(x)
        h_p(kk) = plot(app.ha,x(kk),y(kk),app.markertype,'MarkerFaceColor',app.cmap(kk,:),'MarkerEdgeColor',app.cmap(kk,:),'MarkerSize',app.markersize);
        set(h_p(kk),'ButtonDownFcn',@(src,evt)displayGrainID(src,evt,app.grainids(kk),app.poles(kk,:)))
    end
    if exist('leg_data','var') && ~isempty(leg_data)
        leg_data.lstrings = [leg_data.lstrings; app.label];
        leg_data.handles  = [leg_data.handles; h_p(1)];
        legend(app.ha,leg_data.handles,leg_data.lstrings);
    else
    
    % handle if existing already
    legend(app.ha,'show')
    set(findobj(hf,'tag','legend'),...
        'Color',app.figcolor,'TextColor',app.linecolor,...
        'Position',[0.7981 0.7 0.1527 0.1363])
    end
end
if strcmp(app.colormap, 'gray')
    app.colormap = ['1-' app.colormap];
end
app.figname = figname;
    
if ~isempty(app.title)
    set(get(app.ha,'Title'),'String',app.title);
    set(get(app.ha,'Title'),'Visible','on')
end
set(app.ha,'Position',[0.05 0.05 0.85 0.85]);

set(hf, 'ToolBar','none');
set(hf, 'Color', app.figcolor);
app.hf = hf;
setappdata(hf, 'AppData', app);

if (app.export)
    export(app.figname,hf,app.paper_fig,app.ws_path,app.dotFig);
else
    out.odisp('Figure name with description to export it:')
    out.odisp(app.figname)
    % set title of the figure using figname
    out.odisp('To change the title do:')
    out.odisp('    gtPlotSettings([],[],strrep(<figname>,''_'','' ''))');
end

end

%%
% sub-function
function displayGrainID(src,~,id,pole)
    fprintf('grainID : %d pole : %0.3f %0.3f %0.3f\n',id,pole)
    set(src,'UserData',[id pole])
end

