function [h, ax] = gtIPFCmapKey(varargin)
% GTIPFCMAPKEY  Show inverse pole figure color space in the hexagonal SST
%
%     h = gtIPFCmapKey(varargin)
%     --------------------------
%
%     OPTIONAL INPUT (varargin):
%       crystal_system = <string>
%       N              = <int>       Number of angular steps (quality of the plot) {30}
%       saturate       = <logical>   Saturate or not the color in the SST {true}
%       ipf_label      = <logical>   Draw IPF label {false}
%       poles_label    = <logical>   Draw poles label {false}
%       font_size      = <int>       Font size for labels {18}
%       font_color     = <double>    Font color {[0 0 0]}
%       drawing        = <logical>   Draw the patch with RGB colors {true}
%       hf             = <handle>    figure handle {[]}
%       ha             = <handle>    axis handle {[]}
%       fig_color      = <dobule>    figure color {[1 1 1]}
%       sample_dir     = <string>    sample direction for IPF map {''}
%
%     OUTPUT:
%       h              = <handle>    Handles in figure

app = struct( ...
    'crystal_system', '', ...
    'N', 45, ...
    'poles_label', true, ...
    'ipf_label', true, ...
    'font_size', 20, ...
    'font_color', [0 0 0], ...
    'drawing', true, ...
    'hf', [], ...
    'ha', [], ...
    'fig_color', [1 1 1], ...
    'sample_dir', '', ...
    'phase_id', 1, ...
    'saturate', true );

[app, rej_pars] = parse_pv_pairs(app, varargin);

if (isempty(app.crystal_system))
    parameters = gtLoadParameters();
    app.crystal_system = parameters.cryst(app.phase_id).crystal_system;
end

if (isempty(app.hf))
    hf = figure();
else
    hf = app.hf;
end
if (isempty(app.ha))
    ha = axes('Parent', hf);
else
    ha = app.ha;
end
hold(ha, 'on');

switch (lower(app.crystal_system))
    case {'hexagonal', 'hex', 'hcp', 'h'}
        % For hexagonal colour map, we need two angles:
        % - phi (around z-axis)
        % - psi (from z-axis to vector)
        % Work in radians
        phimin =    0;
        phimax = pi/6;
        inc = phimax/app.N;

        psimin =    0;
        psimax = pi/2;
    case {'cubic', 'cube', 'c'}
        % For cubic colour map, we need two angles:
        % - phi (around z-axis)
        % - chi (around y-axis)
        % Work in radians
        phimin =    0;
        phimax = pi/4;
        inc = phimax/app.N;

        psimin =    0;
        psimax = atan(sqrt(2))+inc;
        chimax = pi/4;
end

h_p = [];
for phi = phimin:inc:(phimax-inc) % Rotation around z-axis
    psis = (psimin:inc:(psimax-inc))';

    if (ismember(app.crystal_system, {'hexagonal', 'hex', 'hcp', 'h'}))
        ratios_R = 1 - psis ./ psimax;
    elseif (ismember(app.crystal_system, {'cubic', 'cube', 'c'}))
        zs = cos(psis);
        xs = sin(psis) * cos(phi);

        chis = atan2(xs, zs);
        valid_chis = chis <= (chimax + inc / 2);
        ratios_R = 1 - chis(valid_chis) / chimax;
        psis = psis(valid_chis);
    end
    ratioB = phi / phimax;

    red   = ratios_R;
    green = (1 - ratios_R) .* (1 - ratioB);
    blue  = (1 - ratios_R) .* ratioB;
    rgb = [red, green, blue];

    % Ensure that we stay in [0 1]
    rgb = max(rgb, 0);
    rgb = min(rgb, 1);

    if (app.saturate)
        rgb = gtCrystSaturateSSTColor(rgb);
    end

    if (app.drawing)
        for ii = 1:numel(psis)
            % Radius distorted for stereographic proj
            r_patch = tan([psis(ii)/2 (psis(ii)+inc)/2]);
            [phi_patch, r_patch] = meshgrid([phi phi+inc]', r_patch);
            [x_patch, y_patch] = pol2cart(phi_patch, r_patch);

            h_p(end+1) = patch(x_patch, y_patch, rgb(ii, :), 'EdgeColor', 'none', 'Faces', [1 2 4 3], 'Parent', ha);
        end
    end
end
set(h_p, 'Tag', 'h_patch');

% Add poles and tart up like gtMakePoleFigure
set(ha, 'XTickLabel', '');
set(ha, 'YTickLabel', '');
set(ha, 'GridLineStyle', 'none');
set(ha, 'Ycolor', 'w');
set(ha, 'Xcolor', 'w');
if isempty(get(ha, 'Tag'))
    set(ha, 'Tag', 'h_axes');
end
axis(ha, 'tight')
axis(ha, 'equal');

if (ismember(app.crystal_system, {'hexagonal', 'hex', 'hcp', 'h'}))
    set(ha, 'xlim', [-0.05 1.05]);
    set(ha, 'ylim', [-0.06 0.56]);

    % Add a black border around triangle
    % For hexagonal:
    % - line from [0 0 0 1] to [2 -1 -1 0] (phi=0)
    % - arc from [2 -1 -1 0] to [1 0 -1 0] (phi=[0 pi/6], psi=pi/2)
    % - line from [0 0 0 1] to [1 0 -1 0] (phi=pi/6)
    line_phi = (phimin:inc:phimax)';
    line_r   = tan(psimax/2 * ones(size(line_phi)));

elseif (ismember(app.crystal_system, {'cubic', 'cube', 'c'}))
    set(gca, 'xlim', [-0.01 0.45]);
    set(gca, 'ylim', [-0.03 0.39]);

    % Add a black border around triangle
    % For cubic:
    % - line from [0 0 1] to [1 0 1] (phi=0)
    % - arc  from [1 0 1] to [1 1 1] (phi=[0 pi/4], chi=pi/4) i.e z=x
    % - line from [1 1 1] to [0 0 1] (phi=pi/4)
    line_phi = [0:inc:phimax phimax]';
    line_points = [cos(line_phi) sin(line_phi) cos(line_phi)];

    % Normalize
    line_points = line_points./repmat(sqrt(sum(line_points.*line_points, 2)), 1, 3);

    % Convert to polar coordinates
    line_psi = acos(line_points(:, 3));
    line_r   = tan(line_psi/2);
end

% Stereographic projection x-y
[line_x, line_y] = pol2cart(line_phi, line_r);
xp = [line_x; 0;  line_x(1)];
yp = [line_y; 0;  line_y(1)];
h_line_plot = plot(xp, yp, '-', ...
    'Tag', 'h_line_plot', 'Color', app.font_color, 'Parent', ha);

if (ismember(app.crystal_system, {'cubic', 'cube', 'c'}))
    % Add a white patch to mask the rough outer edge of the triangle
    xp = [line_x ; line_x(end) ; 0.45  ; 0.45 ; line_x(1)];
    yp = [line_y ; 0.39        ; 0.39  ; 0    ; 0        ];
    patch(xp, yp, app.fig_color, 'EdgeColor', 'none', 'Parent', ha);
end


if (ismember(app.crystal_system, {'hexagonal', 'hex', 'hcp', 'h'}))
    % Add labels for the 3 corners poles
    poles_psi = [psimin psimax psimax];
    poles_phi = [phimin phimax phimin];
    poles_r = tan(poles_psi/2);
    [poles_x, poles_y] = pol2cart(poles_phi, poles_r);
    uvw_poles_l = {'[0 0 0 1]', '[1 0 -1 0]', '[2 -1 -1 0]'};
    uvw_poles_x = poles_x;
    uvw_poles_y = poles_y;
elseif (ismember(app.crystal_system, {'cubic', 'cube', 'c'}))
    uvw_poles_x = [0 0.366 0.4142 0.236 0.2247 0.3 0.4];
    uvw_poles_y = [0 0.366 0 0 0.2247 0.153 0.2];
    uvw_poles_l = {'[0 0 1]', '[1 1 1]', '   [1 0 1]', '[1 0 2] ', '[1 1 2] ', '[2 1 3]', '[2 1 2]'};
    %uvw_poles_l(end+1) = {'[159]'}; 
    %uvw_poles_x(end+1) = 0.2584; 
    %uvw_poles_y(end+1) = 0.0516; % 159
end

h_t = [];
if (app.poles_label)
    for ii = 1:length(uvw_poles_x)
        plot(uvw_poles_x(ii), uvw_poles_y(ii), 'o', 'LineWidth', 5, ...
            'Tag', 'h_points_plot', 'Color', app.font_color, 'Parent', ha);

        h_t{ii} =  text(uvw_poles_x(ii), uvw_poles_y(ii), uvw_poles_l{ii}, ...
            'FontSize', app.font_size, 'Color', app.font_color, ...
            'VerticalAlignment', 'top', 'HorizontalAlignment', 'center', ...
            'Tag', 'h_poles', 'Parent', ha);

        if (ii == 2)
            set(h_t{ii}, 'VerticalAlignment', 'bottom')
        end
        if (ii == 7)
            set(h_t{ii}, 'VerticalAlignment', 'bottom')
        end
        if (ii == 5)
            set(h_t{ii}, 'VerticalAlignment', 'middle', 'HorizontalAlignment', 'right')
        end
    end
end

if (isempty(get(hf, 'Tag')))
    set(hf, 'Tag', 'h_figure')
end
set(hf, 'Color', app.fig_color)
set(ha, 'Visible', 'off');

if (app.ipf_label)
    % label and sample direction to plot
    h_ipf = text(0, 0.6, 0, 'IPF', ...
        'FontSize', app.font_size, 'Color', app.font_color, ...
        'VerticalAlignment', 'top', 'HorizontalAlignment', 'left', ...
        'Tag', 'h_ipf', 'Parent', ha);
end

h_ipf_dir = text(0, 0.52, 0, app.sample_dir, ...
    'FontSize', app.font_size, 'Color', app.font_color, ...
    'VerticalAlignment', 'top', 'HorizontalAlignment', 'left', ...
    'Tag', 'h_ipf_dir', 'Parent', ha);

if (ismember(app.crystal_system, {'cubic', 'cube', 'c'}))
    if (app.ipf_label)
        set(h_ipf, 'Position', [-0.1, 0.40, 0])
    end
    set(h_ipf_dir, 'Position', [-0.1, 0.35, 0])
end

if (nargout > 0)
    h = guihandles(hf);
    ax = ha;
end

end % end of function
