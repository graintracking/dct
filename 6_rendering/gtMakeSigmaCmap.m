function sigma_map = gtMakeSigmaCmap(vol_bounds, boundaries_structure)
% gtMakeSigmaCmap  Creates a colormap to give the right colors to boundaries.
%
%     sigma_map = gtMakeSigmaCmap(vol_bounds, boundaries_structure)
%     --------------------------------------------------------------
%
%     Note:
%       rather than make a volume labelled by sigma, make a single (large)
%       colormap to give the right colours to boundaries.  
%       Save the output, so that it only has to be run once.

sigma_map = zeros(max(vol_bounds(:))+1, 3);

%  non-special boundaries are grey
sigma_map(2:end, :) = 0.7;

%  prepare the key for colouring the rest
colour_key(1,:) =[1     1 1 0]; % sigma 1,  yellow
colour_key(2,:) =[3     1 0 0]; % sigma 3,  red
colour_key(3,:) =[5     1 0 1]; % sigma 5,  pink/purple
colour_key(4,:) =[7     1 0 1]; % sigma 7,  pink/purple
colour_key(5,:) =[9     0 0 1]; % sigma 9,  blue
colour_key(6,:) =[11    1 0 1]; % sigma 11  pink/purple
colour_key(7,:) =[13.1  1 0 1]; % sigma 13a pink/purple
colour_key(8,:) =[13.2  1 0 1]; % sigma 13b pink/purple
colour_key(9,:) =[15    1 0 1]; % sigma 15  pink/purple
colour_key(10,:)=[17.1  1 0 1]; % sigma 17a pink/purple
colour_key(11,:)=[17.2  1 0 1]; % sigma 17b pink/purple
colour_key(12,:)=[19.1  1 0 1]; % sigma 19a pink/purple
colour_key(13,:)=[19.2  1 0 1]; % sigma 19b pink/purple
colour_key(14,:)=[21.1  1 0 1]; % sigma 21a pink/purple
colour_key(15,:)=[21.2  1 0 1]; % sigma 21b pink/purple
colour_key(16,:)=[23    1 0 1]; % sigma 23 pink/purple
colour_key(17,:)=[25.1  1 0 1]; % sigma 25a pink/purple
colour_key(18,:)=[25.2  1 0 1]; % sigma 25b pink/purple
colour_key(19,:)=[27.1  0 1 0]; % sigma 27a green
colour_key(20,:)=[27.2  0 1 0]; % sigma 27b green
colour_key(21,:)=[29.1  1 0 1]; % sigma 29a pink/purple
colour_key(22,:)=[29.2  1 0 1]; % sigma 29b pink/purple

%loop through boundaries structure
for ii = 1:length(boundaries_structure)
    if ~isempty(boundaries_structure(ii).sigma)
        sigma_map(ii+1, :) = colour_key(find(colour_key(:,1)==boundaries_structure(ii).sigma), 2:4);
    end
end

save sigma_map sigma_map;

end % end of function
