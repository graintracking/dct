function data = gtCubicUnitCell(varargin)
% GTCUBICUNITCELL  Draws the cubic unit cell with crystallographic axes
%                  for Euler angles (0,0,0)
%
%     data = gtCubicUnitCell(varargin)
%     --------------------------------
%     Right-handed reference systems for sample and crystal.
%     Draws the unit cell using this convention:
%     Xs <100> // to <100> crystallographic axis (a-axis)
%     Ys <010> // to <010> crystallographic axis (b-axis)
%     Zs <001> // to <001> crystallographic axis (c-axis)
%
%
%     OPTIONAL INPUT (parse by pairs):
%       facecolors   = <double>   RGB color {[]}
%       centered     = <logical>  center the unit cell in the sample ref {false}
%       draw         = <logical>  draw it {true}
%       ratio        = <double>   c/a ratio {1}
%       ind          = <double>   index of face for drawing the patch {0: for all}
%       vertices     = <logical>  draw the vertices points {false}
%       numbers      = <logical>  draw the vertices numbers {false}
%       patch        = <logical>  draw the patch {false}
%       sampleaxes   = <logical>  draw sample axes {true}
%       crystaxes    = <logical>  draw crystal axes {true}
%       %caxis        = <logical>  draw caxis {false}
%       view         = <double>   axes view {[103 20]}
%       add_vertices = <logical>  add some extra vertices (c/3 c/4 position) {false}
%       fontsize     = <double>   font size {14}
%       linewidth    = <double>   line width {2}
%       scale        = <double>   scale for unit cell {1}
%       size         = <double>   real size of unit cell {[]} to be written in
%                                 the middle of the cell
%       hf           = <handle>   figure handle {[]}
%       ha           = <handle>   axis handle {[]}
%
%     OUTPUT:
%       data         = <struct>   drawing options
%
%     Please refer to the DCTdoc_Crystal_orientation_descriptions.pdf 
%     in the DCT_Documentation folder for the graintracking code.
%
%     Version 002 26-10-2013 by LNervo


app.facecolors   = [];
app.centered     = false;
app.draw         = true;
app.ratio        = 1;
app.ind          = 0;
app.showVertices = false;
app.numbers      = false;
app.patch        = false;
app.sampleaxes   = true;
app.crystaxes    = true;
%app.caxis        = false;
app.view         = [103 20];
app.add_vertices = false;
app.fontsize     = 14;
app.linewidth    = 3;
app.scale        = 1;
app.size         = [];
app.hf           = [];
app.ha           = [];

app = parse_pv_pairs(app, varargin);

app.scale = max(app.scale, 1);

a=1*app.scale;
c=app.ratio*app.scale;

% calculation of vertices: first 4 are for 00-1 and 
% then counterclockwise

x0 = [0 1 1 0 0]*a;
y0 = [0 0 1 1 0]*a;

if app.centered
    z0 = repmat(-c/2,1,length(x0));
    z1 = repmat(+c/2,1,length(x0));
else
    z0 = zeros(1,length(x0));
    z1 = repmat(c,1,length(x0));
end
vertices = [[x0;y0;z0],[x0;y0;z1]]';
vertices(5,:)=[];
vertices(end,:)=[];

vertices(end+1,:) = [a/2 a/2 z0(1)];
vertices(end+1,:) = [a/2 a/2 z1(1)];
    
if app.add_vertices

    vertices(end+1,:) = [a/2 a/2 (z1(1)-z0(1))/3];
    vertices(end+1,:) = [a/2 a/2 (z1(1)-z0(1))/3*2];

    vertices(end+1,:) = [a/2 a/2 (z1(1)-z0(1))/4];
    vertices(end+1,:) = [a/2 a/2 (z1(1)-z0(1))/4*2];
    vertices(end+1,:) = [a/2 a/2 (z1(1)-z0(1))/4*3];
end

%c_axis(1,:) = [a/2 a/2 z0(1)-c/4];
%c_axis(2,:) = [a/2 a/2 z1(1)+c/4];

faces = {[1 2 6 5]; [2 3 7 6]; [3 4 8 7]; [4 1 5 8]; ...
         [1 2 3 4]; [5 6 7 8]};
hkl   = [0 -1 0; 1 0 0; 0 1 0; -1 0 0; ...
         0 0 -1; 0 0 1]';
faces_sides = [1 2 6 5; 2 3 7 6; 3 4 8 7; 4 1 5 8];
faces_end   = [1 2 3 4; 5 6 7 8];
faceColors = [ ...
     1 1 0; ... 
     1 0 0; ... 
     0 1 0; ...
     1 0 1; ... 
     0 1 1; ... 
     0 0 1];

faceOrder = [4 1 2 3 5 6];

if ~isempty(app.facecolors)
    faceColors = app.facecolors;
end
if size(faceColors,1)==1
    faceColors = repmat(faceColors, size(faces,1),1);
end

% for crystallographic axes and labels
orig=repmat([0 0 z0(1)],3,1);
xcrys=[x0(2);x0(4);0];
ycrys=[y0(2);y0(4);0];
zcrys=[z0(2);z0(4);z1(1)];
lcrys={'a [100]';...
       'b [010]';...
       'c [001]'};

if app.draw
    
    if isempty(app.hf)
        hf = figure();
        app.hf = hf;
    else
        hf = app.hf;
    end
    if isempty(app.ha)
        app.ha = axes('parent',hf);
    end
    % basal planes
    line(x0,y0,z0,'Color',[0 0 0],'LineWidth',app.linewidth);
    line(x0,y0,z1,'Color',[0 0 0],'LineWidth',app.linewidth);
    hold(app.ha,'on');
    % prismatic planes
    for ii=1:length(x0)
        plot3([x0(ii) x0(ii)],[y0(ii) y0(ii)],[z0(ii) z1(ii)], '-','Color',[0 0 0], 'LineWidth', app.linewidth)
    end
    %if app.caxis
    %    line([0 0],[0 0],[z0(1)-c/4 z1(1)+c/4],'LineWidth',app.linewidth)
    %end
    if app.sampleaxes
        % sample reference system : Xs, Ys, Zs vectors
        gtDrawArrow3([-0.5 0 0],[1.5 0 0],'color','red','stemWidth',0.01);
        gtDrawArrow3([0 -0.5 0],[0 1.5 0],'color','green','stemWidth',0.01);
        gtDrawArrow3([0 0 z0(1)-c/2],[0 0 z1(1)+c/2],'color','blue','stemWidth',0.01);
        
        text(1.5+0.1,0,0,'Xs','Color',[1 0 0],'FontSize',app.fontsize)
        text(0,1.5+0.1,0,'Ys','Color',[0 1 0],'FontSize',app.fontsize)
        text(0,0,z1(1)+c/2+0.1,'Zs','Color',[0 0 1],'FontSize',app.fontsize)
    end
    % cryst axes
    if app.crystaxes
        gtDrawArrow3([0 0 z0(2)],vertices(2,:),'stemWidth',0.015);
        gtDrawArrow3([0 0 z0(4)],vertices(4,:),'stemWidth',0.015);
        gtDrawArrow3([0 0 z0(2)],[0 0 z1(2)],'stemWidth',0.015);
        
        text(xcrys-0.1,ycrys+0.1,zcrys,lcrys,'Color',[0 0 0],'FontSize',app.fontsize)
    end
    view(app.view)
    
    if app.patch || app.ind ~= 0
        if app.ind==0
            for ind=1:6
                p(ind) = gtPlotGrainUnitCell(vertices, [], faces(ind,:), faceColors(ind,:), false, [0 0 0], 0.4);
            end
        else
            ind = find(faceOrder==app.ind);
            p = gtPlotGrainUnitCell(vertices, [], faces(ind,:), faceColors(ind,:), false, [0 0 0], 0.4);
        end
    end
    axis(app.ha,'equal')
    axis(app.ha,'vis3d')
    xlim([-0.5*app.scale 1.5*app.scale])
    ylim([-0.5*app.scale 1.5*app.scale])
    set(app.ha,'Visible','off')

    zoom(1.3)

    if app.numbers
        text(vertices(:,1),vertices(:,2),vertices(:,3)+0.1,num2str((1:length(vertices))'),'Color',[0 0 0],'FontSize',app.fontsize)
    end
    if app.showVertices
        plot3(vertices(:,1),vertices(:,2),vertices(:,3),'.','Color',[0 0 0])
    end
    if ~isempty(app.size) && app.size > 0
        text(app.scale/2,app.scale/2,app.scale/2,num2str(app.size),'Color',[0 0 0],'FontSize',app.fontsize)
    end
    h = rotate3d(app.hf);
    set(h,'Enable','on','RotateStyle','Box')
end % end draw

if nargout == 1
    data.vertices    = vertices;
    %data.c_axis      = c_axis;
    data.faces       = faces;
    data.faces_sides = faces_sides;
    data.faces_end   = faces_end;
    data.facecolor   = faceColors;
    data.hkl         = hkl;    
    data.orig        = orig;
    data.xcrys       = xcrys;
    data.ycrys       = ycrys;
    data.zcrys       = zcrys;
    data.lcrys       = lcrys;
    data             = gtAddMatFile(data, app, true, true);
    data.hf          = hf;
    if app.draw && (app.patch || app.ind ~= 0)
        data.hp = p;
    end
end

end % end of function
