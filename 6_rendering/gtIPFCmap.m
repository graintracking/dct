function [cmap, Vsst, Vc_symm, vargs_out] = gtIPFCmap(phaseid, LD, varargin)
% GTIPFCMAP  Returns a colour map for the specified phase and lab/sample direction.
%
%     [cmap, Vsst, Vc_symm] = gtIPFCmap([phaseid], [LD], varargin)
%     ------------------------------------------------------------
%     Makes an IPF colourmap of the lab_ direction (LD) in the crystal 
%     coordinates. This can be specified as a second argument, otherwise we
%     assume the z-axis [0 0 1]
%
%     INPUT:
%       phaseid          = <int>        ID of the specific phase {1}
%       LD               = <double>     Lab direction to study   {[0 0 1]}
%
%     OPTIONAL INPUT (varargin as a list of pairs, see parse_pv_pairs.m)
%       'saturate'       = <logical>    Saturate colors in the SST         {true}
%       'save'           = <logical>    Save color map to cmap_phase<phaseid>.mat
%                                                                       {false}
%       'crystal_system' = <string>     Legacy to set (force) the crystal system
%                                       for old datasets
%       'r_vectors'      = <double>     Rodrigues vectors as N x 3 array;
%                                       if given, phaseid is ignored;
%       'background'     = <logical>    Add a background color in first row {true}
%   
%     OUTPUT:
%       cmap             = <double>     RGB color map, color code is the inverse
%                                       pole figure in the SST
%       Vsst             = <double>     Row vectors expressed in crystal frame
%                                       moved to the SST triangle
%       Vc_symm          = <double>     Row vectors expressed in crystal frame
%
%     Version 002 08-02-2013 by YGuilhem
%       Add r_vectors as optional input
%
%     Version 003 08-01-2013 by LNervo
%       Added second output argument: Vsst
%
%     Version 002 03-01-2013 by YGuilhem
%       Renamed gtIVPCmap to gtIPFCmap, changed colour saturation
%
%     Version 001 15-11-2012 by YGuilhem
%       Built by gathering old gtIVPcubCmap and gtIVPhexCmap functions

% Default parameters
par.saturate       = true;
par.save           = false;
par.crystal_system = '';
par.spacegroup     = '';
par.r_vectors      = [];
par.background     = true;
par.symm           = [];
[par, rej_pars] = parse_pv_pairs(par, varargin);

if ~exist('phaseid', 'var') || isempty(phaseid)
    phaseid = 1;
end
if ~exist('LD', 'var') || isempty(LD)
    LD = [0 0 1];
end

% Get the r-vectors
r_vectors = [];
phaseDir = fullfile('4_grains', sprintf('phase_%02d', phaseid));
if ~isempty(par.r_vectors)
    r_vectors = par.r_vectors;
elseif exist(fullfile(phaseDir, 'r_vectors.mat'), 'file')
    load(fullfile(phaseDir, 'r_vectors.mat'));
elseif exist(fullfile(phaseDir, 'index.mat'), 'file')
    grain = [];
    load(fullfile(phaseDir, 'index.mat'), 'grain');
    r_vectors = gtIndexAllGrainValues(grain, 'R_vector', [], 1, 1:3);
else
    gtError('gtIPFCmap:missing_file', 'Could not find r_vectors.mat or index.mat file!');
end

% Resize r_vectors if needed
if size(r_vectors, 2) == 4
    r_vectors = r_vectors(:, 2:4);
end

% Get the crystal system
if (isempty(par.crystal_system) || isempty(par.spacegroup))
    parameters = gtLoadParameters();
    par.crystal_system = parameters.cryst(phaseid).crystal_system;
    par.spacegroup = parameters.cryst(phaseid).spacegroup;
end
if (isempty(par.symm))
    par.symm = gtCrystGetSymmetryOperators(par.crystal_system, par.spacegroup);
end

% Compute all orientation matrices g
%   Gcry = g Gsam
%   g = U = gtMathsRod2OriMat(r_vectors.')
all_g = gtMathsRod2OriMat(r_vectors.');

% Compute LDc = LD expressed in the crystal CS
LDc = gtVectorLab2Cryst(LD, all_g);

% Get the color and vector in the SST triangle
[cmap, Vsst, ~, Vc_symm] = gtCrystVector2SST(LDc, par.crystal_system, par.symm, par.saturate);

if (par.background)
    cmap = [ 0 0 0 ; cmap ];
end

if (par.save)
    LD_str = num2str(LD);
    LD_str = strrep(LD_str,' ','');

    fileName = fullfile('6_rendering', sprintf('cmap_phase%02d_%s.mat', phaseid, LD_str));
    save(fileName, 'cmap');
    disp(['IPF cmap saved in ' fileName])
end

if (nargout > 3)
    par.r_vectors      = r_vectors;
    par.phaseid        = phaseid;
    par.samDir         = LD;
    vargs_out = struct2pv(par);
end

end % end of function
