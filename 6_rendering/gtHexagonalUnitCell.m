function data = gtHexagonalUnitCell(varargin)
% GTHEXAGONALUNITCELL  Draws the hexagonal unit cell with crystallographic axes
%                      for Euler angles (0,0,0)
%
%     data = gtHexagonalUnitCell(varargin)
%     ------------------------------------
%     Right-handed reference systems for sample and crystal.
%     Draws the unit cell using the X-convention by default:
%     Xs <100> // to <2-1-10> crystallographic axis (a-axis)
%     Ys <010> // to <01-10> crystallographic axis
%     Zs <001> // to <0001> crystallographic axis (c-axis)
%     It can be rotates to match the Y-convention in which:
%     Xs // <10-10> and Ys // <-12-10> crystallographic axes
%
%     OPTIONAL INPUT (parse by pairs):
%       facecolors   = <double>   RGB color {[]}
%       centered     = <logical>  center the unit cell in the sample ref {false}
%       draw         = <logical>  draw it {true}
%       ratio        = <double>   c/a ratio {1.5857} %titanium c/a
%       ind          = <double>   index of face for drawing the patch {0: for all}
%       showvertices = <logical>  draw the vertices points {false}
%       marker       = <string>   Marker type {'o'}
%       markersize   = <double>   Marker size {25}
%       shownumbers  = <logical>  draw the vertices numbers {false}
%       patch        = <logical>  draw the patch {false}
%       sampleaxes   = <logical>  draw sample axes {true}
%       crystaxes    = <logical>  draw crystal axes {true}
%       caxis        = <logical>  draw caxis {false}
%       view         = <double>   axes view {[103 20]}
%       add_vertices = <logical>  add some extra vertices (c/3 c/4 position) {false}
%       fontsize     = <double>   font size {14}
%       linewidth    = <double>   line width {2}
%       scale        = <double>   scale for unit cell {1}
%       size         = <double>   real size of unit cell {[]} to be written in
%                                 the middle of the cell
%       hf           = <handle>   figure handle {[]}
%       ha           = <handle>   axis handle {[]}
%       zoom
%       convention   = <string>   hexagonal axes {'X'},'Y'
%       stemwidth
%       linecolor
%
%     OUTPUT:
%       data         = <struct>   drawing options
%
%     Please refer to the DCTdoc_Crystal_orientation_descriptions.pdf 
%     in the DCT_Documentation folder for the graintracking code.
%
%     Version 002 23-10-2013 by LNervo

app.facecolors   = [];
app.centered     = false;
app.draw         = true;
app.lp           = [];
app.ratio        = 1.5857; %titanium c/a
app.ind          = 0;
app.showvertices = false;
app.marker       = 'o';
app.markersize   = 25;
app.shownumbers  = false;
app.patch        = false;
app.direct       = false;
app.reciprocal   = false;
app.sampleaxes   = true;
app.crystaxes    = true;
app.crystlabel   = true;
app.caxis        = false;
app.view         = [0 90];
app.add_vertices = false;
app.fontsize     = 14;
app.linewidth    = 2;
app.scale        = 1;
app.size         = [];
app.hf           = [];
app.ha           = [];
app.zoom         = 1;
app.convention   = 'X';
app.stemwidth    = 0.01;
app.linecolor    = [0 0 0];
app = parse_pv_pairs(app, varargin);

app.scale = max(app.scale, 1);

if (app.direct) || (app.reciprocal)
    app.caxis = false;
    app.sampleaxes = false;
end
a=1*app.scale;
c=app.ratio*app.scale;

if isempty(app.lp)
    app.lp = [a a c 90 90 120];
else
    app.ratio = app.lp(3)/app.lp(1);
    a = app.lp(1)*app.scale;
    c = app.lp(3)*app.scale;
end
% calculation of vertices: first 6 are for 000-1 plane and first is at X=0
% then counterclockwise rotation direction
v=0:60:360;

[Bmat,Amat] = gtCrystHKL2CartesianMatrix(app.lp);
% if Y convention, rotates all the lattice vectors of 30 degrees clockwise
% around [0 0 1]
% rotates also angle 'v' for the computaion of unit cell vertices
if strcmpi(app.convention, 'Y')
    v = -30:60:330;
    Bmat = rotationmat3D(-30,[0 0 1]) * Bmat;
    Amat = rotationmat3D(-30,[0 0 1]) * Amat;
end

x0 = a*cosd(v);
y0 = a*sind(v);
if (app.centered)
    z0 = repmat(-c/2,1,length(x0));
    z1 = repmat(+c/2,1,length(x0));
else
    z0 = zeros(1,length(x0));
    z1 = repmat(c,1,length(x0));
end
% building vertices
vertices = [[x0;y0;z0],[x0;y0;z1]]';
vertices(7,:)=[];
vertices(end,:)=[];

vertices(end+1,:) = [0 0 z0(1)];
vertices(end+1,:) = [0 0 z1(1)];

app.n_vertices = size(vertices,1);
if (app.add_vertices)
    vertices(end+1,:) = [0 0 (z1(1)-z0(1))/3];
    vertices(end+1,:) = [0 0 (z1(1)-z0(1))/3*2];

    vertices(end+1,:) = [0 0 (z1(1)-z0(1))/4];
    vertices(end+1,:) = [0 0 (z1(1)-z0(1))/4*2];
    vertices(end+1,:) = [0 0 (z1(1)-z0(1))/4*3];
end

% c-axis longer to be visible
c_axis(1,:) = [0 0 z0(1)-c/4];
c_axis(2,:) = [0 0 z1(1)+c/4];

% sample CS: bigger than unit cell size of 2
lsam  = {'Xs','Ys','Zs'};
xsam = [a;0;0]*2.5;
ysam = [0;a;0]*2.5;
zsam = [0;0;c]*1.2;
csam = [0 0 0];

% crystal CS: bigger than unit cell size of factor 1.5
 %this hsoudl be the Cartesian crystal basis
lcrysn = {'Xc','Yc','Zc'};
xcrysn = [x0(1);0;z0(1)]*1.5;
ycrysn = [0;x0(1);z0(1)]*1.5;
zcrysn = [0;0;z1(1)]*1.5;
ccrysn = [0 0 1];

% direct lattice vectors
lcrysdir = {'a','b','c'};
xcrysdir = (Amat(1,:))';
ycrysdir = (Amat(2,:))';
zcrysdir = (Amat(3,:))';
ccrysdir = [0.8 0 0];

% reciprocal lattice vectors
lcrysstar = {'a*','b*','c*'};
xcrysstar = (Bmat(1,:))';
ycrysstar = (Bmat(2,:))';
zcrysstar = (Bmat(3,:))';
ccrysstar = [0.5 0 0];

if strcmpi(app.convention, 'Y')
    xcrysn = rotateVectors(xcrysn,'angle',30,'axis',[0 0 1]);
    ycrysn = rotateVectors(ycrysn,'angle',30,'axis',[0 0 1]);
    zcrysn = rotateVectors(zcrysn,'angle',30,'axis',[0 0 1]);
end

if (app.direct || app.reciprocal)
    vertices(end+1:end+3,:) = [Amat(:,1)';Amat(:,2)';Amat(:,3)'];
    if (app.reciprocal)
        vertices(end+1:end+3,:) = [Bmat(:,1)';Bmat(:,2)';Bmat(:,3)'];
    end
    if (app.centered)
        vertices(app.n_vertices+1:size(vertices,1),:) = ...
            vertices(app.n_vertices+1:size(vertices,1),:) + repmat(vertices(13,:),size(vertices,1)-app.n_vertices,1);
    end
end

% for patching
faces = {[1 2 8 7]; [4 5 11 10]; [5 6 12 11]; [2 3 9 8]; [3 4 10 9]; [6 1 7 12];...
         [1 2 3 4 5 6]; [7 8 9 10 11 12];}; %
hkil   = [1 0 -1 0; -1 0 1 0; 0 -1 1 0; 0 1 -1 0; -1 1 0 0; 1 -1 0 0; ...
         0 0 0 -1; 0 0 0 1]'; % column vectors
faces_sides = vertcat(faces{1:6});
faces_end   = vertcat(faces{7:8}); %
faceHexColors = [1 1 0; 1 1 0; 1 0 0; 1 0 0; 0 0 1; 0 0 1; 0 1 0; 0 1 0];

faceOrder = [1 4 5 2 3 6 7 8];

if ~isempty(app.facecolors)
    faceHexColors = app.facecolors;
elseif (~app.patch)
    faceHexColors = [0.7 0.7 0.7];
end
if size(faceHexColors,1)==1
    faceHexColors = repmat(faceHexColors, size(faces,1),1);
end

app.facecolors = faceHexColors;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% drawing
if (app.draw)
    % figure
    if isempty(app.hf)
        hf = figure();
        app.hf = hf;
    else
        hf = app.hf;
        figure(app.hf);
    end
    
    % axis
    if isempty(app.ha)
        if isempty(get(hf,'CurrentAxes'))
            app.ha = axes('parent',hf);
        else
            app.ha = get(hf,'CurrentAxes');
        end
    end
    hold(app.ha,'on');

    % caxis
    if (app.caxis && ~app.crystaxes)
        app.h_caxis = line([0 0],[0 0],[z0(1)-c/4 z1(1)+c/4],'LineWidth',app.linewidth,'Color',ccrysdir);
    end
    if (app.sampleaxes)
        % sample reference system : Xs, Ys, Zs vectors
        app.h_sample(1) = gtDrawArrow3(-xsam,xsam,'stemWidth',app.stemwidth*app.scale*1.5,'Color',csam);
        app.h_sample(2) = gtDrawArrow3(-ysam,ysam,'stemWidth',app.stemwidth*app.scale*1.5,'Color',csam);
        app.h_sample(3) = gtDrawArrow3(-zsam,zsam,'stemWidth',app.stemwidth*app.scale*1.5,'Color',csam);
        
        app.h_t_sample = text(xsam,ysam,zsam+[0;0;0.5],lsam,'FontSize',app.fontsize,'Color',csam);
    end
    % crystal orthogonal axes
    if (app.crystaxes)
        app.h_cryst(1) = gtDrawArrow3(vertices(13,:),vertices(13,:)+[xcrysn(1),ycrysn(1),zcrysn(1)],'stemWidth',app.stemwidth*app.scale,'Color', ccrysn);
        app.h_cryst(2) = gtDrawArrow3(vertices(13,:),vertices(13,:)+[xcrysn(2),ycrysn(2),zcrysn(2)],'stemWidth',app.stemwidth*app.scale,'Color', ccrysn);
        app.h_cryst(3) = gtDrawArrow3(vertices(13,:),vertices(13,:)+[xcrysn(3),ycrysn(3),zcrysn(3)],'stemWidth',app.stemwidth*app.scale,'Color', ccrysn);
        
        app.h_t_cryst = text(xcrysn-0.1,ycrysn+0.1,zcrysn,lcrysn,'FontSize',app.fontsize,'Color', ccrysn);
    end
    if (app.direct) 
        if (app.crystlabel)
            app.h_t_crystdir = text(xcrysdir-0.1,ycrysdir+0.1,zcrysdir,lcrysdir,'FontSize',app.fontsize,'Color', ccrysdir);
        end
        for ii=1:3
            app.h_direct(ii) = gtDrawArrow3(vertices(13,:),vertices(13,:)+Amat(:,ii)','color',ccrysdir,'stemWidth',app.stemwidth*app.scale);
        end
    end
    if (app.reciprocal)
        if (app.crystlabel)
            app.h_t_cryststar = text(xcrysstar-0.1,ycrysstar+0.1,zcrysstar,lcrysstar,'FontSize',app.fontsize, 'Color', ccrysstar);
        end
        for ii=1:3
            app.h_reciprocal(ii) = gtDrawArrow3(vertices(13,:),vertices(13,:)+Bmat(:,ii)','color',ccrysstar,'stemWidth',app.stemwidth*app.scale);
        end
    end
    if (~app.reciprocal && ~app.direct)
        app.linecolor = ccrysn;
    end
    view(app.view)

    if app.ind==0
        for ind=1:8
            app.h_patch(ind) = gtPlotGrainUnitCell(vertices, [], faces(ind,:), faceHexColors(ind,:), false, app.linecolor, 1, app.patch);
        end
    else
        ind = find(faceOrder==app.ind);
        app.h_patch = gtPlotGrainUnitCell(vertices, [], faces(ind,:), faceHexColors(ind,:), false, app.linecolor, 1, app.patch);
    end
 
    axis(app.ha,'equal')
    axis(app.ha,'vis3d')
    set(app.ha,'Visible','off')

    if ~isempty(app.zoom)
        zoom('out')
        zoom(app.zoom)
        zoom('off')
    end
    
    if (app.shownumbers)
        app.h_t_vertices = text(vertices(:,1),vertices(:,2),vertices(:,3)+0.1,num2str((1:length(vertices))'),'Color',[0 0 0],'FontSize',app.fontsize);
    end
    if (app.showvertices)
        app.h_vertices = scatter3(vertices(:,1),vertices(:,2),vertices(:,3),app.markersize,'fill',app.marker,'CData',[0 0 0]);
    end
    if ~isempty(app.size) && app.size > 0
        app.h_t_size = text(app.scale/2,app.scale/2,app.scale/2,num2str(app.size),'Color',[0 0 0],'FontSize',app.fontsize);
    end
    app.h_rotate = rotate3d(hf);
    set(app.h_rotate,'Enable','on','RotateStyle','Box')
end % end draw

if nargout == 1
    data.vertices    = vertices;
    data.vertices_norm = data.vertices / a;
    data.c_axis      = c_axis;
    data.faces       = faces;
    data.faces_sides = faces_sides;
    data.faces_end   = faces_end;
    data.faceHexColors = faceHexColors;
    data.faceOrder   = faceOrder;
    data.hkl         = hkil;
    data.xcrysn      = xcrysn;
    data.ycrysn      = ycrysn;
    data.zcrysn      = zcrysn;
    data.lcrysn      = lcrysn';
    data.ccrysn      = ccrysn;
    data.xcrysstar   = xcrysstar;
    data.ycrysstar   = ycrysstar;
    data.zcrysstar   = zcrysstar;
    data.lcrysstar   = lcrysstar;
    data.csrysstar   = ccrysstar;
    data.xcrysdir    = xcrysdir;
    data.ycrysdir    = ycrysdir;
    data.zcrysdir    = zcrysdir;
    data.lcrysdir    = lcrysdir;
    data.csrysdir    = ccrysdir;
    data.xsam        = xsam;
    data.ysam        = ysam;
    data.zsam        = zsam;
    data.lsam        = lsam;
    data.csam        = csam;
    data             = gtAddMatFile(data, app, true, true);
end

end % end of function
