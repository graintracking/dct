function gtDCT2EBSD(grain_volume, r_vectors)
%export grain map to ebsd data format:
% x ; y ; euler1 ; euler2 ; euler3 ;
%euler angles seems to be between 0-2pi, precision to 4 dp
% e1 0-2pi
% e2 0-0.9ish
% e3 0-1.5ish

% convert r_vector to euler angles using x2t, t2x
% r_vectors format is [grainid, r1 r2 r3]
for i=1:size(r_vectors, 1)
    x=[0 0 0 1 r_vectors(i,2:4) 0]';
    t=x2t(x, 'erp');
    xo=t2x(t,'rpm'); %euler z-x-z
    euler_zxz(i,:)=xo(5:7)';
    xo=t2x(t,'rpy'); %euler x-y-z
    euler_xyz(i,:)=xo(5:7)';
end


%put all angles in positive range
euler_zxz(find(euler_zxz<0))=euler_zxz(find(euler_zxz<0))+(2*pi);
euler_xyz(find(euler_xyz<0))=euler_xyz(find(euler_xyz<0))+(2*pi);


grain_slice=grain_volume; % single slice export
%make a file for zxz euler convention
% file open
fid=fopen('EBSD_test_export_zxz.txt', 'w');

% header line
fprintf(fid, 'X ; Y ; Euler1 ; Euler2 ; Euler3 ; \n');

% export the slice
[sizey, sizex]=size(grain_slice);
for x=1:sizex
    for y=1:sizey
        grainid=grain_slice(y,x);
        if grainid~=0
            e=euler_zxz(grainid,:);
        else
            e=[0 0 0];
        end
        fprintf(fid, '%d ; %d ; %0.4f ; %0.4f ; %0.4f ; \n', x-1,y-1,e(1),e(2),e(3));
    end
end

fclose(fid);

%repeat for other convention

%make a file for zxz euler convention
% file open
fid=fopen('EBSD_test_export_xyz.txt', 'w');

% header line
fprintf(fid, 'X ; Y ; Euler1 ; Euler2 ; Euler3 ; \n');

% export the slice
[sizey, sizex]=size(grain_slice);
for x=1:sizex
    for y=1:sizey
        grainid=grain_slice(y,x);
        if grainid~=0
            e=euler_xyz(grainid,:);
        else
            e=[0 0 0];
        end
        fprintf(fid, '%d ; %d ; %0.4f ; %0.4f ; %0.4f ; \n', x-1,y-1,e(1),e(2),e(3));
    end
end

fclose(fid);


% 
% % export the volume
% [sizey, sizex, sizez]=size(grain_volume);
% 
% for z=1:sizez
%     
% % file open
% disp(sprintf('slice %d ...', z))
% fid=fopen(sprintf('/tmp_14_days/andy/EBSD_test/EBSD_export_slice_%d.txt', z), 'w');
% 
% % header line
% fprintf(fid, 'X ; Y ; Euler1 ; Euler2 ; Euler3 ; \n');
% 
% for x=1:sizex
%     for y=1:sizey
%         grainid=grain_volume(y,x,z);
%         if grainid~=0
%             e=euler_xyz(grainid,:);
%         else
%             e=[0 0 0];
%         end
%         fprintf(fid, '%d ; %d ; %0.4f ; %0.4f ; %0.4f ; \n', x-1,y-1,e(1),e(2),e(3));
%     end
% end
% 
% fclose(fid);

end