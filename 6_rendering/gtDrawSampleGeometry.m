function h = gtDrawSampleGeometry(varargin)
% GTSAMPLEGEOMETRY  Draws sample envelope and axis
%     h = gtDrawSampleGeometry(varargin)
%     ----------------------------------
%
%     First input can be:
%     - List of three doubles as:
%        rad = parameters.labgeo.samenvrad
%        bot = parameters.labgeo.samenvbot
%        top = parameters.labgeo.samenvtop
%     - labgeo structure as:
%        parameters.labgeo
%     - vector with the three values:
%        [rad bot top]
%     - property/value pair as:
%        ''labgeo'', parameters.labgeo
%
%     OPTIONAL INPUT (parse by pairs):
%       pxsize    = <double>   mm/px if using pixel unit {[]}
%       color     = <double>   RGB color for axes and labels {[0.5 0.5 0.5]}
%       fontsize  = <double>   font size {12}
%       linestyle = <string>   '-'
%       linecolor = <double>   [0 0 0]
%       linewidth = <double>   1
%
%       sample    = <logical>  true
%       axes      = <logical>  true
%       caxis     = <logical>  false
%       max       = <double>   []
%       scalef    = <double>   1.2
%       centered  = <logical>  false
%       orig      = <double>   []
%
%       hf        = <handle>   figure handle {[]}
%       ha        = <handle>   axes handle {[]}
%       grid      = <string>   'off'
%       minorgrid = <string>   'off'
%       visible   = <string>   'on'
%       box       = <string>   'off'
%       labels    = <cell>     {'X','Y','Z'}
%       view      = <double>   [30 -20]
%
%     OUTPUT:
%       h         = <struct>   handles in figure
%
%     Version 002 26-02-2013 by LNervo
%       Added varargin choice for sample geometry
%
%     Version 001 15-11-2012 by LNervo

% take first arguments
if ( ~isempty(varargin) )
    if ( isstruct(varargin{1}) && all(isfield(varargin{1}, {'samenvrad','samenvbot','samenvtop'})))
        rad = varargin{1}.samenvrad;
        bot = varargin{1}.samenvbot;
        top = varargin{1}.samenvtop;
        varargin(1) = [];
    elseif ( isnumeric(varargin{1}) && isvector(varargin{1}) && length(varargin{1}) == 3 )
        rad = varargin{1}(1);
        bot = varargin{1}(2);
        top = varargin{1}(3);
        varargin(1) = [];
    elseif ( length(varargin) >= 3 ) &&...
             all(cellfun(@isnumeric, varargin(1:3))) && ...
             all(cellfun(@length,varargin(1:3))==1)
        rad = varargin{1};
        bot = varargin{2};
        top = varargin{3};
        varargin(1:3) = [];
    else
        [hasLabgeo,ind] = ismember('labgeo', varargin(1:2:end));
        if (hasLabgeo)
            labgeo = varargin{ind*2};
            varargin(ind*2-1:ind*2) = [];
            rad = labgeo.samenvrad;
            bot = labgeo.samenvbot;
            top = labgeo.samenvtop;
        else
            error_msg = {'gtDrawSampleGeometry:wrong_argument'...
             '  Sample envelope size is missing...'...
             '  It can be:'...
             '    - List of three doubles as:'...
             '        rad = parameters.labgeo.samenvrad'...
             '        bot = parameters.labgeo.samenvbot'...
             '        top = parameters.labgeo.samenvtop'...
             '    - labgeo structure as:'...
             '        parameters.labgeo'...
             '    - vector with the three values:'...
             '        [rad bot top]'...
             '    - property/value pair as:'...
             '        ''labgeo'', parameters.labgeo'};
            cellfun(@disp,error_msg)
            return
        end
    end
else
    disp('No input are given...Quitting')
    h = [];
    return
end

app.pxsize    = [];
app.color     = [0.5 0.5 0.5];
app.fontsize  = 12;
app.linestyle = '-';
app.linecolor = [0 0 0];
app.linewidth = 1;

app.sample    = true;
app.axes      = true;
app.caxis     = false;
app.max       = [];
app.scalef    = 1.2;
app.centered  = false;
app.orig      = [];

app.hf        = [];
app.ha        = [];
app.grid      = 'off';
app.minorgrid = 'off';
app.visible   = 'on';
app.box       = 'off';
app.labels    = {'X','Y','Z'};
app.view      = [30 -20];

[app,rej_pars] = parse_pv_pairs(app, varargin);


if ~isempty(app.orig) && length(app.orig) == 3
    app.orig = [app.orig(1) 0 0; 0 app.orig(2) 0; 0 0 app.orig(3)];
end

if ~isempty(app.pxsize) && app.pxsize ~= 0
    rad = rad/app.pxsize;
    bot = bot/app.pxsize;
    top = top/app.pxsize;
end
if isempty(app.max)
    app.max = rad/4;
end
if isempty(app.hf)
    h.fig = figure('Color',[1 1 1],'Position',[250 250 450 450]);
else
    h.fig = app.hf;
end
if isempty(app.ha)
    if isempty(gca)
        h.ax = axes('Parent',h.fig,'Position',[0.16 0.16 0.78 0.78],'Color',[1 1 1]);
    else
        h.ax = gca;
    end
else
    h.ax = app.ha;
end
hold(h.ax,'on');

% tick
negat = false(1,3);
bot2 = bot;
top2 = top;
if bot2 < 0
    bot2 = abs(bot2);
    negat(2) = true;
elseif top2 < 0
    top2 = abs(top2);
    negat(3) = true;
end
n = floor(log(abs([rad bot2 top2]))./log(10));
step = 10.^n;

limits = ceil([rad bot2 top2]./step).*step;
limits(2:3) = max([bot2 top2], limits(2:3));
limits(negat) = -limits(negat);

limits = limits *app.scalef;
% % axis limits
% xlim(h.ax,[-limits(1) limits(1)])
% ylim(h.ax,[-limits(1) limits(1)])
% zlim(h.ax,[limits(2) limits(3)])

if (app.axes)
    % lab system : x y z vectors
    if (~app.centered)
        if isempty(app.orig)
            app.orig = repmat([-limits(1) -limits(1) limits(2)],3,1);
        end
        dest = app.orig+[[1 0 0];[0 1 0];[0 0 1]]*app.max;
        destT = dest + [[0 1 1];[1 0 1];[1 1 0]]*app.max/4;
        % Create label
        h.text(1) = text(destT(1,1),destT(1,2),destT(1,3),app.labels{1},'FontSize',app.fontsize,'Parent',h.ax,'Color',app.color);
        h.text(2) = text(destT(2,1),destT(2,2),destT(2,3),app.labels{2},'FontSize',app.fontsize,'Parent',h.ax,'Color',app.color);
        h.text(3) = text(destT(3,1),destT(3,2),destT(3,3),app.labels{3},'FontSize',app.fontsize,'Parent',h.ax,'Color',app.color);   
    else
        % axis limits
        xlim(h.ax,[-limits(1) limits(1)])
        ylim(h.ax,[-limits(1) limits(1)])
        zlim(h.ax,[limits(2) limits(3)])

        if isempty(app.orig)
            app.orig = [[-limits(1) 0 0];[0 -limits(1) 0];[0 0 limits(2)]];
        end
        dest = [[limits(1) 0 0];[0 limits(1) 0];[0 0 limits(3)]];

        % Create label
        h.text(1) = xlabel(h.ax,app.labels{1},'FontSize',app.fontsize,'Color',app.color);
        h.text(2) = ylabel(h.ax,app.labels{2},'FontSize',app.fontsize,'Color',app.color);
        h.text(3) = zlabel(h.ax,app.labels{3},'FontSize',app.fontsize,'Color',app.color);
    end
    set(h.text,'Tag','sampletext');

    if isempty(app.pxsize)
        stemWidth = 0.004;
        tipWidth  = 0.01;
     else
        stemWidth = 0.005/app.pxsize;
        tipWidth  = 0.01/app.pxsize;
    end

    h.axis(1) = gtDrawArrow3(app.orig(1,:),dest(1,:),'color','red','stemWidth',stemWidth,'tipWidth',tipWidth,'FaceAlpha', 0.5);
    h.axis(2) = gtDrawArrow3(app.orig(2,:),dest(2,:),'color','green','stemWidth',stemWidth,'tipWidth',tipWidth,'FaceAlpha', 0.5);
    h.axis(3) = gtDrawArrow3(app.orig(3,:),dest(3,:),'color','blue','stemWidth',stemWidth,'tipWidth',tipWidth,'FaceAlpha', 0.5);
    set(h.axis,'Tag','sampleaxes');
end

t = 1:360;
circx = cosd(t)*rad;
circy = sind(t)*rad;
circbotz(1:360) = bot;
circtopz(1:360) = top;

if (app.sample)
    % sample envelope : circles
    h.senv(1) = plot3(circx, circy, circbotz, 'LineStyle', app.linestyle, 'Color', app.linecolor, 'Linewidth',app.linewidth,'Parent',h.ax);
    h.senv(2) = plot3(circx, circy, circtopz, 'LineStyle', app.linestyle, 'Color', app.linecolor, 'Linewidth',app.linewidth,'Parent',h.ax);
    % sample envelope : axis
    if (app.caxis)
        plot3([0 0],[0 0],[bot top],'LineStyle', app.linestyle, 'Color', app.linecolor,'Linewidth',app.linewidth);
    end
    % sample envelope : lateral bars
    h.senv(3) = plot3([-rad -rad],[0 0],[bot top],'LineStyle', app.linestyle, 'Color', app.linecolor, 'Linewidth',app.linewidth,'Parent',h.ax);
    h.senv(4) = plot3([0 0],[-rad -rad],[bot top],'LineStyle', app.linestyle, 'Color', app.linecolor, 'Linewidth',app.linewidth,'Parent',h.ax);
    h.senv(5) = plot3([rad rad],[0 0],[bot top],'LineStyle', app.linestyle, 'Color', app.linecolor, 'Linewidth',app.linewidth,'Parent',h.ax);
    h.senv(6) = plot3([0 0],[rad rad],[bot top],'LineStyle', app.linestyle, 'Color', app.linecolor, 'Linewidth',app.linewidth,'Parent',h.ax);
    set(h.senv,'Tag','sampleenv');
end

set(h.ax,'XMinorGrid',app.minorgrid);
set(h.ax,'YMinorGrid',app.minorgrid);
set(h.ax,'ZMinorGrid',app.minorgrid);
% color
set(h.ax,'XColor',app.color);
set(h.ax,'YColor',app.color);
set(h.ax,'ZColor',app.color);
% grid
set(h.ax,'XGrid',app.grid);
set(h.ax,'YGrid',app.grid);
set(h.ax,'ZGrid',app.grid);

set(h.ax,'Visible',app.visible);
set(h.ax,'Box',app.box);

h.rot3d = rotate3d(h.fig);
set(h.rot3d,'RotateStyle','box','Enable','on');

axis(h.ax,'square')
axis(h.ax,'equal')
axis(h.ax,'vis3d')

set(h.ax,'tag','h_axes_sample')

end % end of function
