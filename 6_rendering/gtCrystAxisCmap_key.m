function [hf, ha, hp] = gtCrystAxisCmap_key(N, XYZlabels, cardinal, fontsize)
% GTCRYSTAXISCMAP_KEY  Makes a figure showing the CaxisCmap present in
%                      stereographic projection
%
%     [hf, ha, hp] = gtCrystAxisCmap_key([N], [XYZlabel], [cardinal], [fontsize])
%     ---------------------------------------------------------------------------
%
%     OPTIONAL INPUT:
%       N         = <int>     Number of angular intervals in [0 pi/2] {90}
%       XYZlabels = <cell>    Cell containing the 3 XYZ poles labels
%                             {'Xs', 'Ys', 'Zs'}
%       cardinal  = <logical> Plot cardinal direction labels or not {false}
%       fontsize  = <double>  Font size {12}
%
%     OPTIONAL OUTPUT:
%       hf        = <handle>  Handle to the key figure
%       ha        = <handle>  Handle to the key axes
%       hp        = <handle>  Handle to the key patch
%
%     NOTE:
%       Colours - whether from gtCrystAxisCmap or from gtAnalyseCrackPlane - are
%       calculated from vectors in instrument coordinate system.
%       red (phi = 0)    -> green (phi = 2*pi/3) -> blue (phi = 4*pi/3)
%           (phi = 0deg)          (phi = 120deg)         (phi = 240deg)
%       Therefore red is at right of the screen, green is uppper left, blue is
%       lower left.
%
%       Tomo:
%       - x: left to right
%       - y: top to bottom
%       - z: positive is into page/screen
%       Instrument:
%       - x: // y tomo;
%       - y: // x tomo;
%       - z: // negative z tomo
%
%     Version 002 16-01-2013 by YGuilhem
%       Factorized and vectorized function, add some optional arguments
%
%     Version 001 10-01-2013 by LNervo

Nmin = 10;
if ~exist('N', 'var') || isempty(N)
    N = 90;
elseif N < Nmin
    warning('gtCrystAxisCmap_key:wrong_argument', ...
        ['You should use at least ' num2str(Nmin) ...
        ' increments, setting it so !']);
    N = Nmin;
end

if ~exist('XYZlabels', 'var') || isempty(XYZlabels)
    XYZlabels = {'Xs', 'Ys', 'Zs'};
end

if ~exist('cardinal', 'var') || isempty(cardinal)
    cardinal = false;
end

if ~exist('fontsize', 'var') || isempty(fontsize)
    fontsize = 12;
end

hf = figure();
pos = get(hf, 'Position');
pos(3:4) = [550 500];
set(hf, 'Position', pos);

inc = pi/2/N;
Nphi = 4*N+2;
Npsi = N;
Ndiv = Nphi*Npsi;

phi = zeros(Ndiv, 1);
psi = zeros(Ndiv, 1);
for iphi=1:Nphi; % around z-axis
    tmp = (iphi-2)*inc;
    for ipsi=1:Npsi % away from z axis
        ii = (iphi-1)*Npsi + ipsi;
        phi(ii) = tmp;
        psi(ii) = (ipsi-1)*inc;
    end
end

r1 = tan(psi/2);
r2 = tan((psi+inc)/2);

x = [(r1.*cos(phi))'; (r1.*cos(phi+inc))'; (r2.*cos(phi+inc))'; (r2.*cos(phi))';];
y = [(r1.*sin(phi))'; (r1.*sin(phi+inc))'; (r2.*sin(phi+inc))'; (r2.*sin(phi))';];

% Centre of patch to give mean colour
mphi = max(min(phi + inc/2, 2*pi), 0.);
mpsi = max(min(pi/2 - psi - inc/2, pi/2), 0.);

[X, Y, Z] = sph2cart(mphi, mpsi, ones(size(mphi)));

rgb = gtVectorOrientationColor([X Y Z]);

ha = axes('Parent',hf);

% Draw patch
hp = patch(x, y, reshape(rgb, [1 size(rgb, 1) 3]), 'edgecolor', 'none', 'Parent', ha);

d1 = 0.04;
text(1+d1, 0,    XYZlabels(1), 'Color', [0 0 0], 'fontsize', fontsize, 'fontweight', 'bold', ...
    'VerticalAlignment', 'middle', 'HorizontalAlignment', 'left', 'Parent', ha);
text(0,    1+d1, XYZlabels(2), 'Color', [0 0 0], 'fontsize', fontsize, 'fontweight', 'bold', ...
    'VerticalAlignment', 'bottom', 'HorizontalAlignment', 'center', 'Parent', ha);
text(0,    0,    XYZlabels(3), 'Color', [0 0 0], 'fontsize', fontsize, 'fontweight', 'bold', ...
    'VerticalAlignment', 'middle', 'HorizontalAlignment', 'center', 'Parent', ha);

if (cardinal)
    d2 = 0.175;
    text(1+d2,  0,     'E', 'Color', [0 0 0], 'fontsize', fontsize, ...
        'VerticalAlignment', 'middle', 'HorizontalAlignment', 'center', 'Parent', ha);
    text(0,    1+d2,   'N', 'Color', [0 0 0], 'fontsize', fontsize, ...
        'VerticalAlignment', 'top', 'HorizontalAlignment', 'center', 'Parent', ha);
    text(-1-d2, 0,     'W', 'Color', [0 0 0], 'fontsize', fontsize, ...
        'VerticalAlignment', 'middle', 'HorizontalAlignment', 'center', 'Parent', ha);
    text(0,     -1-d2, 'S', 'Color', [0 0 0], 'fontsize', fontsize, ...
        'VerticalAlignment', 'bottom', 'HorizontalAlignment', 'center', 'Parent', ha);
end

%axis equal;
axis(ha, 'tight');
axis(ha, 'square');
set(ha, 'xticklabel', '');
set(ha, 'yticklabel', '');
set(ha, 'xcolor', 'w');
set(ha, 'ycolor', 'w');
set(ha, 'Visible','off');
%set(ha, 'xlim', [-1.2 1.2]);
%set(ha, 'ylim', [-1.2 1.2]);
%set(ha, 'Position', get(ha, 'OuterPosition'));

end % end of function
