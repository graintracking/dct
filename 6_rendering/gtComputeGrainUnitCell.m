function vertices = gtComputeGrainUnitCell(rvec, vertices, ga, gc, strainT, scale)
% GTCOMPUTEGRAINUNITCELL  
%     vertices = gtComputeGrainUnitCell(rvec, vertices, [ga], [gc], [strainT], [scale])
%     ---------------------------------------------------------------------------------
%     Trasforms reference unti cell in grain unit cell according to the grain center,
%     grain orientation, grain size, strain tensor and strain scaling factor.
%
%     INPUT:
%       rvec     = <double>   Rodriguez vector (1x3)
%       vertices = <double>   original vertices of the unit cell (Nx3) [mm]
%       ga       = <double>   size of the grain (equivalent radius) (1x1)
%       gc       = <double>   grain center (1x3) [mm]
%       strainT  = <double>   strain tensor (3x3)
%       scale    = <double>   scale factor for strain (1x1)
%
%     OUTPUT:
%       vertices = <double>   modified vertices of the unit cell (Nx3) [mm]
%
%     Version 002 06-06-2013 by LNervo

% compute orientation matrix
g_grain = gtMathsRod2OriMat(rvec');
% compute positions in the Lab ref system
vertices = gtVectorCryst2Lab(vertices, g_grain);
% apply grain size
if exist('ga','var') && ~isempty(ga) && ga ~= 0
    vertices = ga*vertices;
end
% apply grain center
if exist('gc','var') && ~isempty(gc)
    vertices = repmat(gc,size(vertices,1),1) + (eye(3)*vertices')';
end
% apply grain strain
if exist('strainT','var') && ~isempty(strainT) && ~any(isnan(strainT(:)))
    if exist('scale','var') && ~isempty(scale) && scale ~= 0
        strainT = strainT*scale;
        vertices = vertices + (strainT*vertices')';
    end
end

end % end of function
