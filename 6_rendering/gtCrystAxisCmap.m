function poles_cmap = gtCrystAxisCmap(valid_poles)
% GTCRYSTAXISCMAP  Make a colourmap according to poles orientation
%     poles_cmap = gtCrystAxisCmap(valid_poles)
%     -----------------------------------------
%
%     INPUT:
%       valid_poles = valid poles calculated with gtCrystAxisPoles <double Nx4>
%
%     OUTPUT:
%       poles_cmap = colourmap according to poles <double N+1x3>
%
%     poles -> phi/psi -> hsl -> rgb colormap
%     use psi to give brightness; phi to give colour
%
%
%     Version 003 19-10-2012 by YGuilhem
%       Factorized using gtVectorOrientationColor
%
%     Version 002 28-06-2012 by LNervo
%       Update to version 2 of parameters; cleaning formatting


if ~exist('valid_poles','var') || isempty(valid_poles)
    gtError('gtCrystAxisCmap:missingArgument','Missing argument: valid_poles...Quitting')
end
if size(valid_poles,2) ~= 4
    gtError('gtCrystAxisCmap:wrongArgumentSize','Size of valid_poles must be (nof_grains, 4)...Quitting')
end

% Get the orientation color defined by HSL color code, translated to RGB
rgb = gtVectorOrientationColor(valid_poles(:, 2:4));

% Add black background and expand list length of max(grainid)+1
poles_cmap = zeros(max(valid_poles(:, 1))+1, 3);
% uses the grainid of the first column to saves the corresponding rgb colour
for ii=1:length(valid_poles)
    poles_cmap(valid_poles(ii,1)+1, :) = rgb(ii, :);
end

end % end of function
