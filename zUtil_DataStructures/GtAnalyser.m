classdef GtAnalyser
    
    properties
        totspots   = [];
        idspot     = [];
        totgrains  = [];
        idconflict = [];
        flag       = 0;
        difspotID  = [];
    end

end
