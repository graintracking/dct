classdef GtGauge < GtConditionalOutput
% GTGAUGE  Class to display a progress gauge.
%
%     gauge = GtGauge(limits, title, varargin)
%     ------------------------------------------------------------------------
%
%     INPUT:
%       limits  = <int>      Limits of the gauge, can be sized 1 or 2
%                            size = 1: go from 1 to limits
%                            size = 2: go from limits(1) to limits(2)
%       title   = <string>   Title assigned to the gauge
%
%     OPTIONAL INPUT (varargin as a list of pairs, see parse_pv_pairs.m):
%       style   = <string>   Gauge style, can be {'count'} or 'percentage'
%       verbose = <logical>  Display or not the gauge
%       start   = <logical>  Start to display the gauge right now or not
%
%     Version 001 02-05-2013 by YGuilhem

    properties
        title;
        args_to_title;
        style;

        first;
        last;
        steps;
        index;

        format;
        nDel;
        extraText;

        updateFunc;
        delFunc;
        progressFunc;
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%% PUBLIC METHODS
    methods (Access = public)
        function obj = GtGauge(limits, title, varargin)
            par.style = 'count';
            par.start = true;
            par.verbose = true;
            par.args_to_title = {};
            par = parse_pv_pairs(par, varargin);

            obj = obj@GtConditionalOutput(par.verbose);

            obj.steps = 0;
            obj.index = 0;
            obj.nDel = 0;
            obj.extraText = [];
            obj.args_to_title = par.args_to_title;

            switch numel(limits(:))
            case 1
                obj.first = 1;
                obj.last  = limits(1);
                obj.steps = limits(1);
            case 2
                obj.first = limits(1);
                obj.last  = limits(2);
                obj.steps = limits(2) - limits(1) + 1;

            otherwise
                gtError('GtGauge:wrong_gauge_limits', ...
                    'Limits are sized 1 (0 to limit) or 2 (limits(1) to limits(2)!');
            end

            % Style
            obj.progressFunc = @(x)round(x);%handle([]);
            obj.style = par.style;
            obj.setStyle(par.style);

            obj.updateFunc = @()obj.doUpdate();

            if isdeployed
                obj.delFunc = @(x)obj.fprintf('\n');
            else
                obj.delFunc = @(x)obj.fprintf(repmat('\b', [1 x]));
            end

            % Title
            if (~exist('title', 'var') || isempty(title))
                obj.title = 'Progress: ';
            else
                obj.title = title;
            end

            % Start right now or not
            if (logical(par.start))
                obj.start();
            end

            obj.updateFunc();
        end

        function delete(obj)
            delete@handle(obj);
            clear obj;
        end

        function rePrint(obj)
            obj.start();
            obj.nDel = 0;
            obj.updateFunc();
        end

        function setTitle(obj, title)
            obj.title = title;
        end

        function incrementAndDisplay(obj, interval)
            obj.index = obj.index + 1;
            if (~exist('interval', 'var') ...
                    || rem(obj.index, interval) == rem(obj.last-1, interval))
                obj.updateFunc();
            end
        end

        function setExtraText(obj, str)
            obj.extraText = [' ' str];
        end
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%% PROTECTED METHODS
    methods (Access = protected)

        function start(obj)
            if ~isempty(obj.args_to_title)
                obj.fprintf(obj.title, obj.args_to_title{:});
            else
                obj.fprintf(obj.title);
            end
        end

        function setStyle(obj, style)
            obj.style = lower(style);
            switch lower(obj.style)
            case 'percentage'
                obj.setPercentage();
            case 'count'
                obj.setCount();
            otherwise
                gtError('GtGauge:wrong_gauge_style', ['Unknown style: ' style]);
            end
        end

        function setPercentage(obj)
            obj.style = 'percentage';
            obj.format = '%3d%%';
            progress = 100 / obj.steps;
            obj.progressFunc = @(x)round(x * progress);
        end

        function setCount(obj)
            obj.style = 'count';
            nDigits = uint64(round(log10(double(obj.steps))) + 1);
            digitsStr  = ['%' num2str(nDigits) 'd'];
            if (obj.first == 1)
                stepsStr   = num2str(obj.steps);
                obj.format = [digitsStr '/' stepsStr];
                obj.progressFunc = @(x)round(x);
            else
                firstStr   = sprintf(['%0' num2str(nDigits) 'd'], obj.first);
                lastStr    = sprintf(['%0' num2str(nDigits) 'd'], obj.last);
                %firstStr   = num2str(obj.first);
                %lastStr    = num2str(obj.last);
                obj.format = [digitsStr '/(' firstStr '..' lastStr ')'];
                obj.progressFunc = @(x)round(x + obj.first - 1);
            end
        end

        function doUpdate(obj)
            obj.delFunc(obj.nDel);
            output = [obj.format obj.extraText];
            obj.nDel = obj.fprintf(output, obj.progressFunc(obj.index));
            %obj.fprintf([obj.format obj.extraText], obj.progressFunc(obj.index));
            if (obj.index >= obj.steps)
                obj.doFinish();
            end
        end

        function doFinish(obj)
            obj.fprintf(' Done!\n');
            obj.updateFunc = @() (1);
            %obj.delete(); % causes problems if loops go beyond limits
        end
    end
end
