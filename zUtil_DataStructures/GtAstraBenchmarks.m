classdef GtAstraBenchmarks < GtBenchmarks
    methods (Access = public, Static)
        %%% SINO
        function benchmark3Dcreate(numIters, sirtDetectImages)
            [proj, proj_geom] = GtAstraBenchmarks.createProjAndGeom(sirtDetectImages);

            bytes = GtAstraBenchmarks.getSizeVariable(sirtDetectImages);
            GtBenchmarks.print_header('Create Mex Data 3D', bytes)
            fprintf(' - Doing %d iterations: ', numIters)

            tic();
            for ii = 1:numIters
                proj_id  = astra_mex_data3d('create', '-proj3d', proj_geom, proj.stack);
                astra_mex_data3d('delete', proj_id);
            end
            GtBenchmarks.print_result('astra_mex_data3d(''create'', ''-proj3d'')', toc()/numIters, bytes);

            % Let's assume 'get' is correct!
            proj_id = astra_mex_data3d('create', '-proj3d', proj_geom, proj.stack);
            copied = astra_mex_data3d('get', proj_id);
            astra_mex_data3d('delete', proj_id);

            GtBenchmarks.check_errors(sirtDetectImages, copied);
        end

        function benchmark3Dstore(numIters, sirtDetectImages)
            [proj, proj_geom] = GtAstraBenchmarks.createProjAndGeom(sirtDetectImages);

            bytes = GtAstraBenchmarks.getSizeVariable(sirtDetectImages);
            GtBenchmarks.print_header('Store Mex Data 3D', bytes)
            fprintf(' - Doing %d iterations: ', numIters)

            proj_id = astra_mex_data3d('create', '-proj3d', proj_geom, proj.stack);
            tic();
            for ii = 1:numIters
                astra_mex_data3d('store', proj_id, proj.stack);
            end
            GtBenchmarks.print_result('astra_mex_data3d(''store'', ''-proj3d'')', toc()/numIters, bytes);

            % Let's assume 'get' is correct!
            copied = astra_mex_data3d('get', proj_id);
            astra_mex_data3d('delete', proj_id);

            GtBenchmarks.check_errors(sirtDetectImages, copied);
        end

        function benchmark3Dget(numIters, sirtDetectImages)
            [proj, proj_geom] = GtAstraBenchmarks.createProjAndGeom(sirtDetectImages);

            bytes = GtAstraBenchmarks.getSizeVariable(sirtDetectImages);
            GtBenchmarks.print_header('Get Mex Data 3D', bytes)
            fprintf(' - Doing %d iterations: ', numIters)
            proj_id = astra_mex_data3d('create', '-proj3d', proj_geom, proj.stack);

            tic();
            for ii = 1:numIters
                proj = astra_mex_data3d('get', proj_id);
            end
            astra_mex_data3d('delete', proj_id);
            GtBenchmarks.print_result('astra_mex_data3d(''get'', proj_id)', toc()/numIters, bytes);
        end

        %%% VOL
        function benchmarkVolCreate(numIters, sirtDetectImages)
            vol_geom = GtAstraBenchmarks.createVolAndGeom(sirtDetectImages);

            bytes = GtAstraBenchmarks.getSizeVariable(sirtDetectImages);
            GtBenchmarks.print_header('Create Mex Data 3D', bytes)
            fprintf(' - Doing %d iterations: ', numIters)

            tic();
            for ii = 1:numIters
                proj_id  = astra_mex_data3d('create', '-vol', vol_geom, sirtDetectImages);
                astra_mex_data3d('delete', proj_id);
            end
            GtBenchmarks.print_result('astra_mex_data3d(''create'', ''-vol'')', toc()/numIters, bytes);

            % Let's assume 'get' is correct!
            proj_id = astra_mex_data3d('create', '-vol', vol_geom, sirtDetectImages);
            copied = astra_mex_data3d('get', proj_id);
            astra_mex_data3d('delete', proj_id);

            GtBenchmarks.check_errors(sirtDetectImages, copied);
        end

        function benchmarkVolStore(numIters, sirtDetectImages)
            vol_geom = GtAstraBenchmarks.createVolAndGeom(sirtDetectImages);

            bytes = GtAstraBenchmarks.getSizeVariable(sirtDetectImages);
            GtBenchmarks.print_header('Store Mex Data 3D', bytes)
            fprintf(' - Doing %d iterations: ', numIters)

            proj_id = astra_mex_data3d('create', '-vol', vol_geom);
            tic();
            for ii = 1:numIters
                astra_mex_data3d('store', proj_id, sirtDetectImages);
            end
            GtBenchmarks.print_result('astra_mex_data3d(''store'', ''-vol'')', toc()/numIters, bytes);

            % Let's assume 'get' is correct!
            copied = astra_mex_data3d('get', proj_id);
            astra_mex_data3d('delete', proj_id);

            GtBenchmarks.check_errors(sirtDetectImages, copied);
        end

        function benchmarkVolGet(numIters, sirtDetectImages)
            vol_geom = GtAstraBenchmarks.createVolAndGeom(sirtDetectImages);

            bytes = GtAstraBenchmarks.getSizeVariable(sirtDetectImages);
            GtBenchmarks.print_header('Get Mex Data 3D', bytes)
            fprintf(' - Doing %d iterations: ', numIters)
            proj_id = astra_mex_data3d('create', '-vol', vol_geom, sirtDetectImages);

            tic();
            for ii = 1:numIters
                proj = astra_mex_data3d('get', proj_id);
            end
            astra_mex_data3d('delete', proj_id);
            GtBenchmarks.print_result('astra_mex_data3d(''get'', proj_id)', toc()/numIters, bytes);
        end

        %%% CELL SINO
        function benchmarkCellCreate(numIters, detectImages, numCells)
            if (iscell(detectImages))
                [proj, proj_geom] = GtAstraBenchmarks.createProjAndGeom(detectImages);
            else
                proj = cell(1, numCells);
                proj_geom = cell(1, numCells);
                for n = 1:numCells
                    [proj{n}, proj_geom{n}] = GtAstraBenchmarks.createProjAndGeom(detectImages);
                end
            end
            proj_stack = cell(size(proj));
            for n = 1:numel(proj)
                proj_stack{n} = proj{n}.stack;
            end

            bytes = numCells * GtAstraBenchmarks.getSizeVariable(detectImages);
            GtBenchmarks.print_header('Create Mex Data Cell', bytes)
            fprintf(' - Doing %d iterations: ', numIters)

            tic();
            for ii = 1:numIters
                proj_id  = astra_mex_data3d('create', '-proj3d', proj_geom, proj_stack);
                astra_mex_data3d('delete', proj_id{:});
            end
            GtBenchmarks.print_result('astra_mex_data3d(''create'', ''-proj3d'')', toc()/numIters, bytes);

            % Let's assume 'get' is correct!
            proj_id = astra_mex_data3d('create', '-proj3d', proj_geom, proj_stack);
            copied = astra_mex_data3d('get', proj_id);
            astra_mex_data3d('delete', proj_id{:});

            GtBenchmarks.check_errors(proj_stack, copied);
        end

        function benchmarkCellStore(numIters, detectImages, numCells)
            if (iscell(detectImages))
                [proj, proj_geom] = GtAstraBenchmarks.createProjAndGeom(detectImages);
            else
                proj = cell(1, numCells);
                proj_geom = cell(1, numCells);
                for n = 1:numCells
                    [proj{n}, proj_geom{n}] = GtAstraBenchmarks.createProjAndGeom(detectImages);
                end
            end
            proj_stack = cell(size(proj));
            for n = 1:numel(proj)
                proj_stack{n} = proj{n}.stack;
            end

            bytes = numCells * GtAstraBenchmarks.getSizeVariable(detectImages);
            GtBenchmarks.print_header('Store Mex Data Cell', bytes)
            fprintf(' - Doing %d iterations: ', numIters)

            proj_id = astra_mex_data3d('create', '-proj3d', proj_geom, proj_stack);
            tic();
            for ii = 1:numIters
                astra_mex_data3d('store', proj_id, proj_stack);
            end
            GtBenchmarks.print_result('astra_mex_data3d(''store'', ''-proj3d'')', toc()/numIters, bytes);

            % Let's assume 'get' is correct!
            copied = astra_mex_data3d('get', proj_id);
            astra_mex_data3d('delete', proj_id{:});

            GtBenchmarks.check_errors(proj_stack, copied);
        end

        function benchmarkCellGet(numIters, detectImages, numCells)
            if (iscell(detectImages))
                [proj, proj_geom] = GtAstraBenchmarks.createProjAndGeom(detectImages);
            else
                proj = cell(1, numCells);
                proj_geom = cell(1, numCells);
                for n = 1:numCells
                    [proj{n}, proj_geom{n}] = GtAstraBenchmarks.createProjAndGeom(detectImages);
                end
            end
            proj_stack = cell(size(proj));
            for n = 1:numel(proj)
                proj_stack{n} = proj{n}.stack;
            end

            bytes = numCells * GtAstraBenchmarks.getSizeVariable(detectImages);
            GtBenchmarks.print_header('Get Mex Data Cell', bytes)
            fprintf(' - Doing %d iterations: ', numIters)
            proj_id = astra_mex_data3d('create', '-proj3d', proj_geom, proj_stack);

            tic();
            for ii = 1:numIters
                proj = astra_mex_data3d('get', proj_id);
            end
            GtBenchmarks.print_result('astra_mex_data3d(''get'', proj_id)', toc()/numIters, bytes);
            astra_mex_data3d('delete', proj_id{:});
        end

        %%% CELL VOL
        function benchmarkCellVolCreate(numIters, detectImages, numCells)
            vol_geom = cell(1, numCells);
            for n = 1:numCells
                vol_geom{n} = GtAstraBenchmarks.createVolAndGeom(detectImages);
            end
            vol_stack = cell(1, numCells);
            for n = 1:numCells
                vol_stack{n} = detectImages;
            end

            bytes = numCells * GtAstraBenchmarks.getSizeVariable(detectImages);
            GtBenchmarks.print_header('Create Mex Data Cell', bytes)
            fprintf(' - Doing %d iterations: ', numIters)

            tic();
            for ii = 1:numIters
                proj_id = astra_mex_data3d('create', '-vol', vol_geom, vol_stack);
                astra_mex_data3d('delete', proj_id{:});
            end
            GtBenchmarks.print_result('astra_mex_data3d(''create'', ''-vol'')', toc()/numIters, bytes);

            % Let's assume 'get' is correct!
            proj_id = astra_mex_data3d('create', '-vol', vol_geom, vol_stack);
            copied = astra_mex_data3d('get', proj_id);
            astra_mex_data3d('delete', proj_id{:});

            GtBenchmarks.check_errors(vol_stack, copied);
        end

        function benchmarkCellVolStore(numIters, detectImages, numCells)
            vol_geom = cell(1, numCells);
            for n = 1:numCells
                vol_geom{n} = GtAstraBenchmarks.createVolAndGeom(detectImages);
            end
            vol_stack = cell(1, numCells);
            for n = 1:numCells
                vol_stack{n} = detectImages;
            end

            bytes = numCells * GtAstraBenchmarks.getSizeVariable(detectImages);
            GtBenchmarks.print_header('Store Mex Data Cell', bytes)
            fprintf(' - Doing %d iterations: ', numIters)

            proj_id = astra_mex_data3d('create', '-vol', vol_geom);
            tic();
            for ii = 1:numIters
                astra_mex_data3d('store', proj_id, vol_stack);
            end
            GtBenchmarks.print_result('astra_mex_data3d(''store'', ''-vol'')', toc()/numIters, bytes);

            % Let's assume 'get' is correct!
            copied = astra_mex_data3d('get', proj_id);
            astra_mex_data3d('delete', proj_id{:});

            GtBenchmarks.check_errors(vol_stack, copied);
        end

        function benchmarkCellVolGet(numIters, detectImages, numCells)
            vol_geom = cell(1, numCells);
            for n = 1:numCells
                vol_geom{n} = GtAstraBenchmarks.createVolAndGeom(detectImages);
            end
            vol_stack = cell(1, numCells);
            for n = 1:numCells
                vol_stack{n} = detectImages;
            end

            bytes = numCells * GtAstraBenchmarks.getSizeVariable(detectImages);
            GtBenchmarks.print_header('Get Mex Data Cell', bytes)
            fprintf(' - Doing %d iterations: ', numIters)
            proj_id = astra_mex_data3d('create', '-vol', vol_geom, vol_stack);

            tic();
            for ii = 1:numIters
                proj = astra_mex_data3d('get', proj_id);
            end
            GtBenchmarks.print_result('astra_mex_data3d(''get'', proj_id)', toc()/numIters, bytes);
            astra_mex_data3d('delete', proj_id{:});
        end

        function allBenchmarks()
%             imgs = rand(1e3, 1e3, 1e2);
% 
%             GtAstraBenchmarks.benchmark3Dcreate(5, imgs)
%             GtAstraBenchmarks.benchmark3Dstore(5, imgs)
%             GtAstraBenchmarks.benchmark3Dget(5, imgs)
% 
%             GtAstraBenchmarks.benchmarkVolCreate(5, imgs)
%             GtAstraBenchmarks.benchmarkVolStore(5, imgs)
%             GtAstraBenchmarks.benchmarkVolGet(5, imgs)

            imgs = rand(1e3, 1e3, 2e2);
            cellImgs = arrayfun(@(x){imgs}, 1:5);
            p = randperm(size(imgs, 2), round(size(imgs, 2)/2));
            cellImgs{2} = cellImgs{2}(:, p, :);
            p = randperm(size(imgs, 2), round(size(imgs, 2)/3));
            cellImgs{4} = cellImgs{4}(:, p, :);

            GtAstraBenchmarks.benchmarkCellCreate(5, cellImgs, 1);
            GtAstraBenchmarks.benchmarkCellStore(5, cellImgs, 1);
            GtAstraBenchmarks.benchmarkCellGet(5, cellImgs, 1);

%             GtAstraBenchmarks.benchmarkCellVolCreate(5, imgs, 5);
%             GtAstraBenchmarks.benchmarkCellVolStore(5, imgs, 5);
%             GtAstraBenchmarks.benchmarkCellVolGet(5, imgs, 5);

            % Just in case
            astra_mex_data3d('clear');
        end

        function bytes = getSizeVariable(var)
            if (iscell(var))
                bytes = sum(cellfun(@GtBenchmarks.getSizeVariable, var));
            else
                bytes = GtBenchmarks.getSizeVariable(var);
            end
        end
    end

    methods (Access = protected, Static)
        function [proj, proj_geom] = createProjAndGeom(detectImages)
            if (iscell(detectImages))
                proj = cell(size(detectImages));
                proj_geom = cell(size(detectImages));
                for ii = 1:numel(detectImages)
                    [proj{ii}, proj_geom{ii}] = GtAstraBenchmarks.createProjAndGeom(detectImages{ii});
                end
            else
                proj = [];
                proj.stack = detectImages;
                proj.num_cols = size(detectImages, 1);
                proj.num_rows = size(detectImages, 3);
                proj.geom = ones(size(detectImages, 2), 12);
                proj_geom = astra_create_proj_geom('parallel3d_vec', proj.num_rows, proj.num_cols, proj.geom);
            end
        end

        function vol_geom = createVolAndGeom(detectImages)
            vol_geom = astra_create_vol_geom(size(detectImages, 1), size(detectImages, 2), size(detectImages, 3));
        end
    end
end
