classdef GtConflict
    
    properties
        difspots   = [];
        maxnum     = false;
        merge      = [];
        allequals  = false;
        delete     = [];
    end
end
