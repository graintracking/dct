classdef GtVolumesDiskArray < GtVolumesArray
    methods (Access = public)
        function self = GtVolumesDiskArray(volumes_num, volumes_size, disk_path, varargin)
            self = self@GtVolumesArray(0, volumes_size, varargin{:});
            self.volumes_num = volumes_num;

            self.volumes = disk_path;
            if (exist(disk_path, 'file'))
                delete(disk_path)
            end
            h5create(disk_path, '/volumes', ...
                [volumes_size, volumes_num], 'Datatype', self.data_type, ...
                'ChunkSize', [volumes_size, 1], 'Deflate', 3);

            args = struct('type', '{}', 'subs', {{}});
            for n = 1:volumes_num
                args.subs{1} = n;
                if (self.init_val == 0)
                    subsasgn(self, args, zeros(self.volumes_size, self.data_type));
                else
                    subsasgn(self, args, self.init_val * ones(self.volumes_size, self.data_type));
                end
            end
        end

        function out = subsref(self, args)
            switch (args(1).type)
                case '()'
                    num_cells = numel(args.subs);
                    out = cell(num_cells);
                    for n = 1:num_cells
                        out{n} = h5read(self.volumes, '/volumes', ...
                            [1, 1, 1, args.subs{1}(n)], [self.volumes_size, 1]);
                    end
                case '{}'
                    out = h5read(self.volumes, '/volumes', ...
                        [1, 1, 1, args.subs{1}], [self.volumes_size, 1]);
                case '.'
                    out = builtin('subsref', self, args);
            end
        end

        function self = subsasgn(self, args, in)
            switch (args(1).type)
                case '()'
                    num_cells = numel(args.subs);
                    for n = 1:num_cells
                        h5write(self.volumes, '/volumes', in{n}, ...
                            [1, 1, 1, args.subs{1}(n)], [self.volumes_size, 1]);
                    end
                case '{}'
                    h5write(self.volumes, '/volumes', in, ...
                        [1, 1, 1, args.subs{1}], [self.volumes_size, 1]);
                case '.'
                    self = builtin('subsasgn', self, args, in);
            end
        end

        function out = getSameSizeZeros(self, disk_path, num_vols)
            if (exist('num_vols', 'var'))
                out = GtVolumesDiskArray(num_vols, self.volumes_size, ...
                    disk_path, 'init_val', 0, 'data_type', self.data_type);
            else
                out = GtVolumesDiskArray(length(self), self.volumes_size, ...
                    disk_path, 'init_val', 0, 'data_type', self.data_type);
            end
        end

        function out = getSameSizeOnes(self, disk_path, num_vols)
            if (exist('num_vols', 'var'))
                out = GtVolumesDiskArray(num_vols, self.volumes_size, ...
                    disk_path, 'init_val', 1, 'data_type', self.data_type);
            else
                out = GtVolumesDiskArray(length(self), self.volumes_size, ...
                    disk_path, 'init_val', 1, 'data_type', self.data_type);
            end
        end
    end
end
