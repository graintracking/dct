classdef GtBenchmarks
    properties
        num_threads = feature('NumCores');
    end

    methods (Static, Access = public)
        function print_header(func_name, data_size)
            fprintf('\n%- 40s (Data size: %f GB)\n', func_name, data_size / 2^30);
        end

        function print_result(message, result, data)
            fprintf([message '\t']);
            if (exist('data', 'var'))
                fprintf('Throughput: %f GB/s (Time: %f s)\n', data / 2^30 / result, result);
            end
        end

        function bytes = getSizeVariable(var)
            if (iscell(var))
                bytes = 0;
                for ii_c = 1:numel(var)
                    bytes = bytes + GtBenchmarks.getSizeVariable(var{ii_c});
                end
            elseif (isobject(var) || isstruct(var))
                bytes = 0;
                for ii = 1:numel(var)
                    fnames = fieldnames(var(ii));
                    for ii_fn = 1:numel(fnames)
                        bytes = bytes + GtBenchmarks.getSizeVariable(var(ii).(fnames{ii_fn}));
                    end
                end
            else
                info = whos('var');
                bytes = info.bytes;
            end
        end

        function varargout = check_errors(reference, computed, verbose, threshold)
            if (~exist('verbose', 'var') || isempty(verbose))
                verbose = false;
            end
            if (~exist('threshold', 'var') || isempty(threshold))
                threshold = 2 * eps('single');
            end
            fprintf('Maximum error in computation:\n')
            if (iscell(reference))
                reference = reshape(reference, [], 1);
                computed = reshape(computed, [], 1);
                errors = cellfun(@(x, y)max(max(max(abs(x - y)))), ...
                    reference, computed);
                [max_error, pos_max] = max(errors);
                fprintf('\t%g (at position: %d)\n', max_error, pos_max);
                if (verbose)
                    if (max_error > threshold)
                        disp(errors')
                        varargout{1} = true;
                    else
                        varargout{1} = false;
                    end
                end
            elseif (isscalar(reference))
                fprintf('\t%g (%g relative)\n', abs(reference - computed), abs((reference - computed)/reference) );
            else
                fprintf('\t%g\n', max(max(max(abs(reference - computed)))) );
            end
        end
    end
end
