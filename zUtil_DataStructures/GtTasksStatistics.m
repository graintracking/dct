classdef GtTasksStatistics < handle
    properties
        entries = struct();

        host_name = '';
        print_empty = false;
    end

    methods
        function self = GtTasksStatistics(varargin)
            [~, self.host_name] = system('hostname');
            self.host_name = regexprep(self.host_name, '\\n', '');

            self = parse_pv_pairs(self, varargin);
        end

        function clear(self)
            e_names = fieldnames(self.entries);
            for ii_e = 1:numel(e_names)
                self.entries.(e_names{ii_e}).timestamps = [];
                ep_names = fieldnames(self.entries.(e_names{ii_e}).partials);
                for ii_ep = 1:numel(ep_names)
                    self.entries.(e_names{ii_e}).partials.(ep_names{ii_ep}).timestamps = [];
                end
            end
        end

        function add_task(self, name, description)
            self.entries.(name) = struct('description', description, ...
                'timestamps', [], 'timer_tic', 0, 'partials', struct());
        end

        function add_task_partial(self, name, partial_name, description)
            if (~isfield(self.entries, name))
                error('GtTasksStatistics:wrong_argument', ...
                    'Task name "%s" does not exist!', name)
            end
            self.entries.(name).partials.(partial_name) ...
                = struct('description', description, 'timestamps', [], 'timer_tic', 0);
        end

        function tic(self, name, partial_name)
            if (exist('partial_name', 'var'))
                self.entries.(name).partials.(partial_name).timer_tic = tic();
            else
                self.entries.(name).timer_tic = tic();
            end
        end

        function toc(self, name, partial_name)
            if (exist('partial_name', 'var'))
                self.entries.(name).partials.(partial_name).timestamps = ...
                    [self.entries.(name).partials.(partial_name).timestamps, ...
                    toc(self.entries.(name).partials.(partial_name).timer_tic)];
            else
                self.entries.(name).timestamps = ...
                    [self.entries.(name).timestamps, toc(self.entries.(name).timer_tic)];
            end
        end

        function add_timestamp(self, t_stamp, name, partial_name)
            if (~isempty(t_stamp))
                if (exist('partial_name', 'var'))
                    self.entries.(name).partials.(partial_name).timestamps = ...
                        [self.entries.(name).partials.(partial_name).timestamps, t_stamp];
                else
                    self.entries.(name).timestamps = ...
                            [self.entries.(name).timestamps, t_stamp];
                end
            end
        end
        
        function printStats(self, order)
            if (~exist('order', 'var'))
                order = 'no-order';
            end
            fields = fieldnames(self.entries);
            switch (order)
                case 'no-order'
                case 'time'
                    times = cellfun(@(x)sum(self.entries.(x).timestamps), fields);
                    [~, idx] = sort(times, 'descend');
                    fields = fields(idx);
            end

            max_descr_length = max(cellfun(@(x)length(self.entries.(x).description), fields));
            max_descr_length = max(mod(4 - mod(max_descr_length, 4), 4) + 2 + max_descr_length, 4);
            format_str = ['%' sprintf('%d', max_descr_length) ...
                's: %10s (s) [mean %10s (s), std %f (s), %d invocations]\n'];
            format_str_alternative = ['%' sprintf('%d', max_descr_length) 's: No invocations\n'];

            format_str_partial = ['%' sprintf('%d', max_descr_length+2) ...
                's: %10s (s) [mean %10s (s), std %f (s), %d invocations]\n'];

            sep_str = [repmat('-', [1 max_descr_length]) '+' ...
                repmat('-', [1 min(max(2*numel(self.host_name), 20), 80-max_descr_length-1)]) '\n'];
            fprintf(sep_str);
            fprintf(['%' sprintf('%d', max_descr_length) 's: %s\n'], 'Hostname', self.host_name);
            fprintf(sep_str);

            for ii = 1:numel(fields)
                entry = self.entries.(fields{ii});
                if (~isempty(entry.timestamps))
                    fprintf(format_str, ...
                        entry.description, ...
                        sprintf('%f', sum(entry.timestamps)), ...
                        sprintf('%f', mean(entry.timestamps)), ...
                        std(entry.timestamps), numel(entry.timestamps));
                    partial_names = fieldnames(entry.partials);
                    for ii_p = 1:numel(partial_names)
                        partial_entry = entry.partials.(partial_names{ii_p});
                        if (~isempty(partial_entry.timestamps))
                            fprintf(format_str_partial, ...
                                partial_entry.description, ...
                                sprintf('%f', sum(partial_entry.timestamps)), ...
                                sprintf('%f', mean(partial_entry.timestamps)), ...
                                std(partial_entry.timestamps), ...
                                numel(partial_entry.timestamps));
                        end
                    end
                elseif (self.print_empty)
                    fprintf(format_str_alternative, entry.description);
                end
            end
            fprintf(sep_str);
        end

        function res = get_mean(self, name)
            res = mean(self.entries.(name).timestamps);
        end

        function res = get_total(self, name)
            res = sum(self.entries.(name).timestamps);
        end

        function res = get_formatted_mean(self, name)
            res = datestr(self.get_mean(name) / 24 / 3600, 'HH:MM:SS.FFF');
        end

        function res = get_formatted_total(self, name)
            res = datestr(self.get_total(name) / 24 / 3600, 'HH:MM:SS.FFF');
        end
    end
end