classdef GtSample < handle
    properties
        phases = { };

        volumeFile = '';
        dilVolFile = '';
        absVolFile = '';

        fileTable = '';

        version;
    end

    methods (Access = public)
        function obj = GtSample(phases_num, volume_file, dilvol_file, absvol_file, file_table)
        % GTSAMPLE/GTSAMPLE Constructor

            if (~exist('phases_num', 'var'))
                phases_num = 1;
            end
            if (exist('volume_file', 'var'))
                obj.volumeFile = volume_file;
            end
            if (exist('dilvol_file', 'var'))
                obj.dilVolFile = dilvol_file;
            end
            if (exist('absvol_file', 'var'))
                obj.absVolFile = absvol_file;
            end
            obj.phases = cell(1, phases_num);
            for ii = 1: phases_num
                obj.phases{ii} = GtPhase();
            end
            if (exist('file_table', 'var'))
                obj.fileTable = file_table;
            end
            obj.version = GtSample.getCurrentVersion();
        end

        function delete(obj)
            for ii = 1:numel(obj.phases)
                delete(obj.phases{ii});
            end
        end

        function copy = getCopy(obj)
            num_phases = numel(obj.phases);
            copy = GtSample(num_phases, obj.volumeFile, ...
                obj.dilVolFile, obj.absVolFile, obj.fileTable);
            for ii_ph = 1:num_phases
                copy.phases{ii_ph} = obj.phases{ii_ph}.getCopy();
            end
        end

        function merge(obj, other)
        % GTSAMPLE/MERGE Merges an other instance of this class to the current
        % object.
        % It will overwrite just the values from the other class that are not
        % the default values: So be careful to really know what you are doing
        % when you merge two instances.
        % Moreover, it assumes complete coherence between the two objects
        % (number of grains in the phases, and number of phases)

            obj.checkObjectCoherence(other);

            for phaseNum = 1:length(other.phases)
                % Merge selectedGrains
                obj.mergePhaseField(other, phaseNum, 'selectedGrains', true);
                % Merge completeness
                obj.mergePhaseField(other, phaseNum, 'completeness', 1);
                % Merge center
                obj.mergePhaseField(other, phaseNum, 'center', 0);
                % Merge R_vector
                obj.mergePhaseField(other, phaseNum, 'R_vector', 0);
                % Merge boundingBox
                obj.mergePhaseField(other, phaseNum, 'boundingBox', 0);
                % Merge bboxExtremes
                obj.mergePhaseField(other, phaseNum, 'bboxExtremes', 0);

                % Merge bboxExtremes
                obj.mergePhaseField(other, phaseNum, 'use_extended', 0);
            end
        end

        function mergeToFile(obj, fileTable, fileName, fieldName)
        % GTSAMPLE/MERGETOFILE This merges the current object into an existing
        % object, which is loaded from a file.
        %
        % It is synchronized using a GtLock. So it is quite safe in a parallel
        % environment, but still is limited by the intrinsic limitations of the
        % GtLock class

            if (~exist('fieldName', 'var'))
                fieldName = 'sample';
            end

            lock = GtLockDB(fileTable, fileName);
            lock.acquire();

            sample = load(fileName, fieldName);
            sample = sample.(fieldName);
            sample.merge(obj);

            save(fileName, 'sample', '-v7.3');

            lock.release();
        end

        function mergeToFileUnsafe(obj, fileName, fieldName)
        % GTSAMPLE/MERGETOFILE This merges the current object into an existing
        % object, which is loaded from a file.
        %
        % It is synchronized using a GtLock. So it is quite safe in a parallel
        % environment, but still is limited by the intrinsic limitations of the
        % GtLock class

            if (~exist('fieldName', 'var'))
                fieldName = 'sample';
            end

            sample = load(fileName, fieldName);
            sample = sample.(fieldName);
            sample.merge(obj);

            save(fileName, 'sample', '-v7.3');
        end

        function saveToFile(obj, fileName)
        % GTSAMPLE/SAVETOFILE This saves the current object into a new file. If
        % the file already exists, it is overwritten.

            if (~exist('fileName', 'var'))
                fileName = fullfile('4_grains', 'sample.mat');
            end

            sample = obj; %#ok<NASGU>
            save(fileName, 'sample', '-v7.3');
        end

        function grainIDs = getGrainsWithCompletenessLessThan(obj, value)
        % GTSAMPLE/GETGRAINSWITHCOMPLETENESSLESSTHAN Queries the GtPhase
        % instances to find the grains which have a lower completeness than the
        % value

            num_phases = numel(obj.phases);
            grainIDs = cell(num_phases, 1);
            for phaseID = 1:num_phases
                grainIDs{phaseID} = ...
                    obj.phases{phaseID}.getGrainsWithCompletenessLessThan(value);
            end
        end
    end

    methods (Access = private)
        function checkObjectCoherence(obj, other)
        % GTSAMPLE/CHECKOBJECTCOHERENCE It checks complete coherence between the
        % two objects (number of grains in the phases, and number of phases)

            if (~isa(other, 'GtSample'))
                error('GT_SAMPLE:wrong_argument', ...
                      'It is possible to merge just instances of the GtSample class');
            elseif (numel(other.phases) ~= numel(obj.phases))
                error('GT_SAMPLE:wrong_argument', ...
                      'Samples should have the same number of phases');
            else
                for numPhase = 1:numel(other.phases)
                    if (other.phases{numPhase}.getNumberOfGrains() ...
                            ~= obj.phases{numPhase}.getNumberOfGrains())
                        error('GT_SAMPLE:wrong_argument', ...
                              'Phases should have the same number of grains');
                    end
                end
            end
        end

        function mergePhaseField(obj, other, phaseNum, field, defaultValue)
        % GTSAMPLE/MERGEPHASEFIELD

            localField = obj.phases{phaseNum}.(field);
            otherField = other.phases{phaseNum}.(field);
            if (any(size(localField) == 1))
                indexes = find(otherField ~= defaultValue);
                localField(indexes) = otherField(indexes);
            else
                indexes = find(sum(abs(otherField), 2) ~= defaultValue);
                localField(indexes, :) = otherField(indexes, :);
            end
            obj.phases{phaseNum}.(field) = localField;
        end
    end

    methods (Static, Access = public)
        function sample = loadFromLockedFile(fileTable, fileName, fieldName)
        % GTSAMPLE/LOADFROMFILE Synchronized loading from a file

            if (~exist('fileName', 'var'))
                fileName = fullfile('4_grains', 'sample.mat');
            end

            if (~exist('fieldName', 'var'))
                fieldName = 'sample';
            end

            lock = GtLockDB(fileTable, fileName);
            lock.acquire();

            sample = load(fileName, fieldName);
            sample = sample.(fieldName);

            lock.release();
        end

        function sample = loadFromFile(fileName, fieldName)
        % GTSAMPLE/LOADFROMFILE Not synchronized loading from a file

            if (~exist('fileName', 'var'))
                fileName = fullfile('4_grains', 'sample.mat');
            end

            if (~exist('fieldName', 'var'))
                fieldName = 'sample';
            end

            sample = load(fileName, fieldName);
            sample = sample.(fieldName);
        end

        function convert(fileName, fieldName)
        % GTSAMPLE/CONVERT Not synchronized conversion

            if (~exist('fileName', 'var'))
                fileName = fullfile('4_grains', 'sample.mat');
            end

            if (~exist('fieldName', 'var'))
                fieldName = 'sample';
            end

            sample = load(fileName, fieldName);
            sample = sample.(fieldName);

            if (~isfield(sample, 'version') || isempty(sample.version))
                sample.version = 0;
            end
            save([fileName sprintf('.ver-%02d', sample.version)], 'sample', '-v7.3');

            fprintf('Converting from version %d, phase: ', sample.version);
            switch (sample.version)
                case 0
                    num_phases = numel(sample.phases);
                    for ii_ph = 1:num_phases
                        num_chars_ph = fprintf('%02d/%02d, grain: ', ii_ph, num_phases);

                        phase = sample.phases{ii_ph};
                        num_grains = numel(phase.grains);
                        phase.grains_number = num_grains;
                        for ii_gr = 1:num_grains
                            num_chars_gr = fprintf('%03d/%03d', ii_gr, num_grains);

                            gr = struct( ...
                                'threshold', phase.grains{ii_gr}.threshold, ...
                                'selected', logical(phase.grains{ii_gr}.selectedDiffspots));
                            gtSaveGrain(ii_ph, ii_gr, gr, 'fields', {'threshold', 'selected'});

                            fprintf(repmat('\b', [1, num_chars_gr]));
                        end
                        sample.phases{ii_ph} = phase;

                        fprintf(repmat('\b', [1, num_chars_ph]));
                    end
                otherwise
                    error('GtSample:wrong_version', ...
                        'Cannot convert from version: %d', sample.version)
            end
            fprintf('\n')

            sample.version = GtSample.getCurrentVersion();
            save(fileName, 'sample', '-v7.3');
        end

        function ver = getCurrentVersion()
            ver = 1;
        end
    end
end
