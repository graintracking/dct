classdef GtConditionalOutput < handle
    % GTCONDITIONALOUTPUT is a class for conditional output messages
    % Methods are:
    %   GtConditionalOutput(do_print)
    %   fprintf(obj, varargin)
    %   odisp(obj, varargin)

    properties
        doPrint;
    end

    methods (Access = public)
        function obj = GtConditionalOutput(do_print)
            obj.doPrint = do_print;
        end

        function count = fprintf(obj, varargin)

            cnt = 0;
            if (obj.doPrint)
                cnt = fprintf(varargin{:});
            end
            if nargout > 0, count = cnt; end
        end

        function odisp(obj, varargin)
            if (obj.doPrint)
                disp(varargin{:});
            end
        end
    end
end
