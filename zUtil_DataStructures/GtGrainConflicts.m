classdef GtGrainConflicts < handle
% GTGRAINCONFLICTS  Class that contains all the information about conflicts of a given phase.
% All the grains are organized in a performance oriented structure
% get/set methods are provided, to aid the consistent use of the class.

    properties
        grain     = {};       % List of grains conflict info
        analyser  = {};       % List of analyser for each grain (see class GtAnalyser)
        conflicts = {};       % List of conflicts for each grain (see class GtConflict)
        lessthr   = [];       % List of grains with numspots in conflicts less than thr_num
        overthr   = [];       % List of grains with numspots in conflicts more than thr_num
        thr_num   = [];
    end

    methods(Access = public)
        function obj = GtGrainConflicts(numberOfGrains)
        % GTGRAINCONFLICTS/GTGRAINCONFLICTS
        %     Initializes the GtGrainConflicts with 'numberOfGrains'
            if (~exist('numberOfGrains', 'var'))
                numberOfGrains = 0;
            end
            obj.initGrains(numberOfGrains);
        end

        function initGrains(obj, number)
        % GTGRAINCONFLICTS/INITGRAINS
        %     Initializes the GtGrainConflicts to 'number' of grains
            if number<0
                gtError('GtGrainConflicts:wrongArgument','The number of grains must be positive...Quitting')
            end
            obj.grain     = cell(number,1);
            obj.analyser  = cell(number,1);
            obj.conflicts = cell(number,1);
            
            for ii=1:number
                obj.resetGrainId(ii);
            end
        end

        function initConflict(obj, grainid)
        % GTGRAINCONFLICTS/INITCONFLICT
        %     Initializes analyser and conflicts for grainid
            obj.resetAnalyser(grainid);
            obj.resetConflicts(grainid);
        end

        function pushGrainId(obj)
        % GTGRAINCONFLICTS/PUSHGRAINID
        %     Adds a new empty object at the end of each property
            obj.grain{end+1}     = [];
            obj.analyser{end+1}  = GtAnalyser();
            obj.conflicts{end+1} = repmat(GtConflict(),0,1);
        end

        function deleteGrainId(obj, grainid)
        % GTGRAINCONFLICTS/DELETEGRAINID
        %     Deletes objects number 'grainid' from each property
            obj.grain(grainid)     = [];
            obj.analyser(grainid)  = [];
            obj.conflicts(grainid) = [];
        end

        function resetGrainId(obj, grainid)
        % GTGRAINCONFLICTS/RESETGRAINID
        %     Resets to zero all the properties for grainid
            obj.resetGrain(grainid);
            obj.resetAnalyser(grainid);
            obj.resetConflicts(grainid);
        end

        function setAnalyserVars(obj, grainid, varargin)
        % GTGRAINCONFLICTS/SETANALYSERVARS
            tmp = obj.getAnalyser(grainid);
            
        end

        function setConflictsIndexVars(obj, grainid, index, varargin)
        % GTGRAINCONFLICTS/SETCONFLICTSINDEXVARS
            tmp = obj.getConflicts(grainid, index);
            
        end

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Setter Functions

        % grain 
        function setGrain(obj, grainid, grain)
        % GTGRAINCONFLICTS/SETGRAIN
            obj.grain{grainid} = grain;  
        end

        function resetGrain(obj, grainid)
        % GTGRAINCONFLICTS/RESETGRAIN
        %     Reset property 'grain' for grainid
            obj.grain{grainid} = [];
        end

        % analyser fields
        function setTotSpots(obj, grainid, tot)
        % GTGRAINCONFLICTS/SETTOTSPOTS
            obj.analyser{grainid}.totspots = tot;
        end

        function setIdSpot(obj, grainid, id)
        % GTGRAINCONFLICTS/SETIDSPOT
            obj.analyser{grainid}.idspot = id;
        end

        function setIdSpotRow(obj, grainid, row, ind)
        % GTGRAINCONFLICTS/SETIDSPOTROW
            obj.analyser{grainid}.idspot(row,:) = ind; 
        end

        function pushSpots(obj, grainid, idspot)
        % GTGRAINCONFLICTS/PUSHSPOTS
            num = length(idspot);
            obj.analyser{grainid}.idspot(1,end+1:end+num) = idspot;
            
        end

        function setTotGrains(obj, grainid, num)
        % GTGRAINCONFLICTS/SETTOTGRAINS
            obj.analyser{grainid}.totgrains = num;
        end

        function setIdConflict(obj, grainid, id)
        % GTGRAINCONFLICTS/SETIDCONFLICT
            obj.analyser{grainid}.idconflict = id;
        end 

        function setFlag(obj, grainid, f)
        % GTGRAINCONFLICTS/SETFLAG
            obj.analyser{grainid}.flag = f;
        end

        function setDifspotID(obj, grainid, spots)
        % GTGRAINCONFLICTS/SETDIFSPOTID
            obj.analyser{grainid}.difspotID = spots;
        end

        function setAnalyser(obj, grainid, analyser)
        % GTGRAINCONFLICTS/SETANALYSER
            obj.analyser{grainid} = analyser;
        end

        function resetAnalyser(obj, grainid)
        % GTGRAINCONFLICTS/RESETANALYSER
        %     Reset property 'analyser' for grainid
            obj.analyser{grainid} = GtAnalyser();
        end

        % conflicts fields
        function setDifspots(obj, grainid, index, spots)
        % GTGRAINCONFLICTS/SETDIFSPOTS
            obj.conflicts{grainid}(index).difspots = spots;
        end

        function setMaxNum(obj, grainid, index, max)
        % GTGRAINCONFLICTS/SETMAXNUM
            obj.conflicts{grainid}(index).maxnum = max;
        end

        function setMerge(obj, grainid, index, m)
        % GTGRAINCONFLICTS/SETMERGE
            obj.conflicts{grainid}(index).merge = m;
        end

        function setAllEquals(obj, grainid, index, alleq)
        % GTGRAINCONFLICTS/SETALLEQUALS       
            obj.conflicts{grainid}(index).allequals = alleq;
        end

        function setDelete(obj, grainid, index, del)
        % GTGRAINCONFLICTS/SETDELETE
            obj.conflicts{grainid}(index).delete = del;
        end

        function setConflictsIndex(obj, grainid, index, conflict)
        % GTGRAINCONFLICTS/SETCONFLICTID
            obj.conflicts{grainid}(index) = conflict;
        end

        function resetConflicts(obj, grainid)
        % GTGRAINCONFLICTS/RESETCONFLICTS
        %     Reset property 'conflicts' for grainid
            obj.conflicts{grainid} = repmat(GtConflict(),0,1);
        end

        function resetConflictsIndex(obj, grainid, index)
        % GTGRAINCONFLICTS/RESETCONFLICTSINDEX
        %     Reset property 'conflicts' for grainid
            obj.conflicts{grainid}(index) = [];
        end

        function resetConflictsToN(obj, grainid, N)
        % GTGRAINCONFLICTS/RESETCONFLICTSTON
        %     Reset property 'conflicts' to N-array for grainid
            obj.conflicts{grainid} = repmat(GtConflict(),N,1);
        end

        function setThrNum(obj, thr_num)
            obj.thr_num = thr_num;
        end

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Getter Functions

        function grain = getGrain(obj, grainid)
        % GTGRAINCONFLICTS/GETGRAIN
            grain = obj.grain{grainid};
        end

        function out = getDifspotsConflicts(obj, grainid, conflictid)
        % GTGRAINCONFLICTS/GETDIFSPOTSCONFLICTS
        %     output is 'difspots'
            if ~exist('conflictid','var')
                conflictid = [];
            end
            tmp = obj.getGrain(grainid);
            if isempty(conflictid)
                out = unique(tmp(2,:));
            else
                out = tmp(2,tmp(1,:)==conflictid);
            end
        end

        % analyser fields
        function tot = getTotSpots(obj, grainid)
        % GTGRAINCONFLICTS/GETTOTSPOTS
            tot = obj.analyser{grainid}.totspots;
        end

        function idspot = getIdSpot(obj, grainid)
        % GTGRAINCONFLICTS/GETIDSPOT
            idspot = obj.analyser{grainid}.idspot;
        end

        function indexes = getIdSpotRow(obj, grainid, row)
        % GTGRAINCONFLICTS/GETIDSPOTROW
            if ~isempty(obj.analyser{grainid}.idspot)
                indexes = obj.analyser{grainid}.idspot(row,:);
            else
                indexes = [];
            end
        end

        function num = getTotGrains(obj, grainid)
        % GTGRAINCONFLICTS/GETTOTGRAINS
            num = obj.analyser{grainid}.totgrains;
        end

        function id = getIdConflict(obj, grainid)
        % GTGRAINCONFLICTS/GETIDCONFLICT
            id = obj.analyser{grainid}.idconflict;
        end

        function f = getFlag(obj, grainid)
        % GTGRAINCONFLICTS/GETFLAG
            f = obj.analyser{grainid}.flag;
        end

        function ids = getDifspotID(obj, grainid)
        % GTGRAINCONFLICTS/GETDIFSPOTID
            ids = obj.analyser{grainid}.difspotID;
        end

        function analyser = getAnalyser(obj, grainid)
        % GTGRAINCONFLICTS/GETANALYSER
            analyser = obj.analyser{grainid};
        end

        % conflicts fields
        function out = getDifspots(obj, grainid, index)
        % GTGRAINCONFLICTS/GETDIFSPOTS
            out = obj.getConflictsField(grainid, 'difspots');
            if exist('index','var') && ~isempty(index) && index <= size(out,1)
                out = out(index,:);
            end
        end

        function out = getDifspotConflictAllGrains(obj, difspotid)
        % GTGRAINCONFLICTS/GETDIFSPOTCONFCLITALLGRAINS
            tmp = obj.getAllValuesFromCellstrField('conflicts','difspots');
            tmp = cat(1,tmp{:});
            [hasP,index]=cellfun(@(x) ismember(difspotid,x),tmp(:,2));
            if any(hasP)
                tmp_out = tmp(hasP, :);
                index_out = index(hasP,:);
                out = [tmp_out arrayfun(@(x) {x},index_out)];
            else
                fprintf('No conflicts found for difspotd number %d\n',difspotid)
                out = [];
            end
        end

        function out = getGrainAllConflicts(obj, grainid, varargin)
        % GTGRAINCONFLICTS/GETGRAINALLCONFLICT
            out = obj.getDifspots(grainid);
            [hasP,ind]=ismember('sortnum',varargin(1:2:end));
            if (hasP)
                numdiff = cellfun(@length, out(:,2));
                [~,index] = sort(numdiff, varargin{ind*2});
                out = out(index,:);
                return
            end
            [hasP,ind]=ismember('sortid',varargin(1:2:end));
            if (hasP)
                [~,index] = sort(out{:,1}, varargin{ind*2});
                out = out(index,:);
                return
            end
            [hasP,ind]=ismember('maxnum',varargin(1:2:end));
            if (hasP)
                numdiff = cellfun(@length, out(:,2));
                maxid = find(numdiff == max(numdiff(:)));
                out = out(maxid,:);
                return
            end
            [hasP,ind]=ismember('minnum',varargin(1:2:end));
            if (hasP)
                numdiff = cellfun(@length, out(:,2));
                maxid = find(numdiff == min(numdiff(:)));
                out = out(maxid,:);
                return
            end
            [hasP,ind]=ismember('maxid',varargin(1:2:end));
            if (hasP)
                ids = [out{:,1}]';
                maxid = find(ids == max(ids(:)));
                out = out(maxid,:);
                return
            end
            [hasP,ind]=ismember('minid',varargin(1:2:end));
            if (hasP)
                ids = [out{:,1}]';
                maxid = find(ids == min(ids(:)));
                out = out(maxid,:);
                return
            end
        end

        function out = getGrainDifspotsConflict(obj, grainid, conflictid)
        % GTGRAINCONFLICTS/GETGRAINDIFSPOTSCONFLICT
            if ~exist('conflictid','var')
                conflictid = [];
            end
            if isempty(conflictid)
                tmp = obj.getDifspots(grainid);
                out = tmp(:,2);
            else
                list = obj.getIdConflict(grainid);
                [hasP,ind]=ismember(conflictid, list);
                if (hasP)
                    tmp = obj.getDifspots(grainid, ind);
                    out = tmp{2};
                else
                    out = [];
                end
            end
        end

        function out = getGrainDifspotsConflictNum(obj, grainid, conflictid)
        % GTGRAINCONFLICTS/GETGRAINDIFSPOTSCONFLICTNUM
            if ~exist('conflictid','var')
                conflictid = [];
            end
            list = obj.getGrainDifspotsConflict(grainid,conflictid);
            if iscell(list)
                out = cellfun(@length, list);
            else
                out = length(list);
            end
        end

        function out = getMaxNum(obj, grainid, index)
        % GTGRAINCONFLICTS/GETMAXNUM
            out = obj.getConflictsField(grainid, 'maxnum');
            if exist('index','var') && ~isempty(index) && index <= size(out,1)
                out = out(index,:);
            end
        end

        function out = getMerge(obj, grainid, index)
        % GTGRAINCONFLICTS/GETMERGE
            out = obj.getConflictsField(grainid, 'merge');
            if exist('index','var') && ~isempty(index) && index <= size(out,1)
                out = out(index,:);
            end
        end

        function out = getAllEquals(obj, grainid, index)
        % GTGRAINCONFLICTS/GETALLEQUALS
            out = obj.getConflictsField(grainid, 'allequals');
            if exist('index','var') && ~isempty(index) && index <= size(out,1)
                out = out(index,:);
            end
        end

        function out = getDelete(obj, grainid, index)
        % GTGRAINCONFLICTS/GETDELETE
            out = obj.getConflictsField(grainid, 'delete');
            if exist('index','var') && ~isempty(index) && index <= size(out,1)
                out = out(index,:);
            end
        end

        function out = getConflictsField(obj, grainid, field)
        % GTGRAINCONFLICTS/GETCONFLICTSFIELD
            conflict = obj.getConflicts(grainid);
            if ~isempty(conflict) && isprop(GtConflict(), field)
                if strcmpi(field, 'delete')
                    for kk=1:length(conflict)
                        out{kk,1} = conflict(kk).(field);
                    end
                else
                    out = vertcat(conflict.(field));
                end
            else
                out = [];
            end
        end

        function out = getConflicts(obj, grainid, index)
        % GTGRAINCONFLICTS/GETCONFLICTS
            conflict = obj.conflicts{grainid};
            if exist('index','var') && ~isempty(index) && index <= length(conflict)
                out = conflict(index);
            else
                out = conflict;
            end
        end

        % others
        function out = getThrNum(obj)
            out = obj.thr_num;
        end

        function grainIDs = getOverThr(obj)
            grainIDs = obj.overthr;
        end

        function grainIDs = getLessThr(obj)
            grainIDs = obj.lessthr;
        end

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Analysis Functions

        function [allvalues, grainid] = getAllValuesFromCellstrField(obj, cellstr, field)
        % GTGRAINCONFLICTS/GETALLVALUESFROMCELLSTRFIELD
        %     output are 'allvalues' and 'grainid'
        %     cellstr can be 'grain'/'analyser'/'conflicts'

            if ~any(strcmp(cellstr,'conflicts') || strcmp(cellstr,'analyser') || strcmp(cellstr,'grain'))
                gtError('GtGrainConflicts:wrongArgument','Cellstr is wrong...Quitting')
            end
            cellArray = obj.(cellstr);
            if strcmpi(cellstr,'analyser')
                cellArray = [cellArray{:}];
            elseif strcmpi(cellstr,'grain')
                allvalues = [cellArray{:}];
                grainid = unique(allvalues(1,:));
                return
            end
            allvalues = [];
            grainid   = [];
            count = 0;
            sizeA = [];
            for ii = 1 : length(cellArray)
              if iscell(cellArray)
                array = cellArray{ii};
              elseif isa(cellArray,'GtAnalyser')
                array = cellArray(ii);
              end
                sizeA = [sizeA, length(array)];
                for jj = 1 : length(array)
                    current = array(jj);
                    if ~isprop(current, field)
                        gtError('GtGrainConflicts:wrongArgument','Wrong property for the given cell structure...Quitting')
                    end
                    if isempty(current.(field))
                        count = count + 1;
                    else
                        grainid = [grainid; ii];
                        allvalues = [allvalues; {current.(field)}];
                    end
                    %fprintf('grainid: %04d size: %2d currentID: %2d isempty: %4d total: %4d length: %4d \n', ...
                    %    ii, size(array,1) , jj, count, size(allvalues, 1), size(grainid,1))
                end
            end
            sizeA = cumsum(sizeA);
           % disp(['empty: ' num2str(count)])
        end

        function [grainIDs, allvalues] = getAllFieldsValueWithQuery(obj, cellstr, field, value, query)
        % GTGRAINCONFLICTS/GETALLFIELDSVALUEWITHQUERY
        % [grainIDs, allvalues] = getAllFieldsValueWithQuery(obj, cellstr, field, value, query)
        %     cellstr can be 'grain'/'analyser'/'conflicts'
        %     output is 'grainIDs'

            [allvalues, ids] = getAllValuesFromCellstrField(obj, cellstr, field);
            if isempty(allvalues)
                grainIDs = [];
                return
            end
            allvalues = [allvalues{:}];
            if ~exist('query','var') || isempty(query)
                grainIDs = ids;
                return
            end
            if query == 0
                grainIDs = ids(allvalues == value);
            elseif query == -1
                grainIDs = ids(allvalues < value);
            elseif query == 1
                grainIDs = ids(allvalues > value);
            else
                gtError('GtGrainConflicts:wrongArgument','The query is not recnognized...Quitting')
            end
        end

        function inconflict = getHasConflictsTrue(obj)
        % GTGRAINCONFLICTS/GETHASCONFLICTSTRUE
        %     Gets only selected grains with conflicts
        %     output is 'inconflict'
            grainIDs = [obj.grain{:}];
            if ~isempty(grainIDs)
                inconflict = unique(grainIDs(1,:));
            else
                disp('No grains with conflicts')
                inconflict = [];
            end
        end

        function buildIdSpot(obj, grainid)
        % GTGRAINCONfLICTS/BUILDIDSPOT
        
            difspotID = obj.getDifspotID(grainid); % row vector
            if isempty(difspotID)
                disp(['Warning: difspotID field is empty for grain #' grainid])
            end
            idspot = obj.getIdSpotRow(grainid,1); % row vector
            obj.setIdSpot(grainid, []);
            if ~isempty(idspot)
                if size(idspot,1) < size(idspot,2)
                    idspot = idspot(1,:);
                else
                    idspot = idspot(:,1);
                end
                if ~isempty(difspotID)
%                     if size(difspotID,1) ~= size(idspot,1)
%                         idspot = idspot';
%                     end
                    [spots, ind] = intersect(difspotID, idspot, 'stable');
                    obj.setIdSpotRow(grainid, 1, spots);
                    obj.setIdSpotRow(grainid, 2, ind);
                end
            else
                obj.setIdSpot(grainid, []);
            end
        end

        function compareGrains(obj, grainid, conflictids, thr_num)
        % GTGRAINCONFLICTS/COMPAREGRAINS

            if ~exist('thr_num','var')
                thr_num = [];
            end

            for kk=1:numel(conflictids)
                spots  = obj.getDifspotsConflicts(grainid, conflictids(kk));
                if ~isempty(spots)
                    [current_spots,current_inds,~] = intersect(obj.getDifspotID(grainid),spots,'stable');
                    obj.setDifspots(grainid, kk, {conflictids(kk), current_inds'});
                    obj.pushSpots(grainid, spots);

                    orig_spots = obj.getDifspotsConflicts(conflictids(kk), grainid);
                    if isempty(setdiff(orig_spots, spots))
                        obj.setAllEquals(grainid, kk, true);
                        obj.setDelete(grainid, kk, current_spots);
                    end

                    if ~isempty(thr_num)
                        if length(spots) > thr_num && obj.getAllEquals(grainid, kk)
                            obj.setMerge(grainid, kk, conflictids(kk));
                        else
                            obj.setMaxNum(grainid, kk, true);
                        end
                    end
                else
                    obj.resetConflicts(grainid);
                end
            end

        end

        function deselectDifspotsFromGrain(obj, grainIDs_mod, idspots, conflictIDs)
        % GTGRAINCONFLICTS/DESELECTDIFSPOTSFROMGRAIN  Given a list of modified
        % grains and a list of difspots to be deselected, update .grain,
        % .analyser and .conflicts, basing on conflictIDs list
            for ii=1:length(conflictIDs)
                % remove difspots from grain{} for grainIDs_mod and conflictIDs
                tmp = obj.getGrain(grainIDs_mod);
                tmp(:, ismember(tmp(1,:),conflictIDs(ii)) & ismember(tmp(2,:),idspots) )=[];
                obj.setGrain(grainIDs_mod, tmp);

                tmp2 = obj.getGrain(conflictIDs(ii));
                tmp2(:, ismember(tmp2(1,:),grainIDs_mod) & ismember(tmp2(2,:),idspots))=[];
                obj.setGrain(conflictIDs(ii), tmp2);

                obj.compareGrains(grainIDs_mod, conflictIDs(ii), []);

                if isempty(obj.getGrain(grainIDs_mod))
                    obj.resetAnalyser(grainIDs_mod);
                    obj.resetConflicts(grainIDs_mod);
                else
                    obj.buildIdSpot(grainIDs_mod);
                    obj.setTotSpots(grainIDs_mod, size(tmp,2));
                    obj.setIdConflict(grainIDs_mod, unique(tmp(1,:)));
                    obj.setTotGrains(grainIDs_mod, length(obj.getIdConflict(grainIDs_mod)));
                end
                if isempty(obj.getGrain(conflictIDs(ii)))
                    obj.resetAnalyser(conflictIDs(ii));
                    obj.resetConflicts(conflictIDs(ii));
                else
                    obj.buildIdSpot(conflictIDs(ii));
                    obj.setTotSpots(conflictIDs(ii), size(tmp2,2));
                    obj.setIdConflict(conflictIDs(ii), unique(tmp2(1,:)));
                    obj.setTotGrains(conflictIDs(ii), length(obj.getIdConflict(conflictIDs(ii))));
                end
            end
        end

        function setGrainsWithDifspotLessThan(obj, thr_num, le)
        % GTGRAINCONFLICTS/
            if ~exist('le','var') || isempty(le)
                le = false;
            end
            obj.setThrNum(thr_num);
            if (le)
                thr = obj.thr_num + 1;
            else
                thr = obj.thr_num;
            end
            [grainIDs, ~] = obj.getAllFieldsValueWithQuery('analyser', 'totspots', thr, -1);
            if ~isempty(obj.overthr)
                grainIDs = setdiff(grainIDs, obj.overthr);
            end
            restIDs = setdiff(obj.getHasConflictsTrue(),grainIDs);

            maxnum = arrayfun(@(x) obj.getMaxNum(x), restIDs, 'UniformOutput', false);
            maxnum = cellfun(@(x) all(x), maxnum);
            grainIDs = [grainIDs', restIDs(maxnum)];
            
            obj.lessthr = grainIDs;
        end

        function setGrainsWithDifspotMoreThan(obj, thr_num, ge)
        % GTGRAINCONFLICTS/
            if ~exist('ge','var') || isempty(ge)
                ge = false;
            end
            obj.setThrNum(thr_num);
            if (ge)
                thr = obj.thr_num + 1;
            else
                thr = obj.thr_num;
            end
            [grainIDs, ~] = obj.getAllFieldsValueWithQuery('analyser', 'totspots', thr, 1);
            if ~isempty(obj.lessthr)
                grainIDs = setdiff(grainIDs, obj.lessthr);
            end
            restIDs = setdiff(obj.getHasConflictsTrue(),grainIDs);

            merge = arrayfun(@(x) obj.getMerge(x), restIDs, 'UniformOutput', false);
            merge = cellfun(@(x) ~isempty(x), merge);
            grainIDs = [grainIDs', restIDs(merge)];
                        
            obj.overthr = grainIDs;
        end

    end % end public methods
end % end classdef
