classdef GtVolumesArray < handle
    properties (Access = public)
        volumes = {};

        volumes_size = [0 0 0];
        volumes_num = 0;

        data_type = 'single';

        init_val = 0;
    end

    methods (Access = public)
        function self = GtVolumesArray(volumes_num, volumes_size, varargin)
            self.volumes_size = volumes_size;
            self.volumes_num = volumes_num;
            self = parse_pv_pairs(self, varargin);

            if (isempty(self.volumes))
                self.volumes = cell(volumes_num, 1);
                for n = 1:volumes_num
                    if (self.init_val == 0)
                        self.volumes{n} = zeros(self.volumes_size, self.data_type);
                    else
                        self.volumes{n} = self.init_val * ones(self.volumes_size, self.data_type);
                    end
                end
            else
                if (length(self) ~= volumes_num)
                    error('GtVolumesArray:wrong_argument', ...
                        'Wrong numer of volumes')
                end
                for n = 1:volumes_num
                    size_vol = size(self.volumes{n});
                    type_vol = class(self.volumes{n});
                    if (numel(self.volumes_size) ~= numel(size_vol))
                        error('GtVolumesArray:wrong_argument', ...
                            'Wrong volume size')
                    elseif (any(self.volumes_size ~= size_vol))
                        error('GtVolumesArray:wrong_argument', ...
                            'Wrong volume size')
                    elseif (~strcmpi(self.data_type, type_vol))
                        error('GtVolumesArray:wrong_argument', ...
                            'Wrong type of volumes')
                    end
                end
            end
        end

        function out = subsref(self, args)
            switch (args(1).type)
                case '()'
                    out = self.volumes(args.subs{:});
                case '{}'
                    out = self.volumes{args.subs{:}};
                case '.'
                    out = builtin('subsref', self, args);
            end
        end

        function self = subsasgn(self, args, in)
            switch (args(1).type)
                case '()'
                    self.volumes(args.subs{:}) = in;
                case '{}'
                    self.volumes{args.subs{:}} = in;
                case '.'
                    self = builtin('subsasgn', self, args, in);
            end
        end

        function num_vols = length(self)
            num_vols = self.volumes_num;
        end

        function out = getSameSizeZeros(self, num_vols)
            if (exist('num_vols', 'var'))
                out = GtVolumesArray(num_vols, self.volumes_size, ...
                    'init_val', 0, 'data_type', self.data_type);
            else
                out = GtVolumesArray(length(self), self.volumes_size, ...
                    'init_val', 0, 'data_type', self.data_type);
            end
        end

        function out = getSameSizeOnes(self, num_vols)
            if (exist('num_vols', 'var'))
                out = GtVolumesArray(num_vols, self.volumes_size, ...
                    'init_val', 1, 'data_type', self.data_type);
            else
                out = GtVolumesArray(length(self), self.volumes_size, ...
                    'init_val', 1, 'data_type', self.data_type);
            end
        end

        function out = sum(self, dim)
            args = struct('type', '{}', 'subs', {{}});
            switch(dim)
                case 1
                    for n = length(self):-1:1
                        args.subs{1} = n;
                        out(1, :, :, n) = sum(subsref(self, args), 1);
                    end
                case 2
                    for n = length(self):-1:1
                        args.subs{1} = n;
                        out(:, 1, :, n) = sum(subsref(self, args), 2);
                    end
                case 3
                    for n = length(self):-1:1
                        args.subs{1} = n;
                        out(:, :, 1, n) = sum(subsref(self, args), 3);
                    end
                case 4
                    args.subs{1} = 1;
                    out = subsref(self, args);
                    for n = 2:length(self)
                        args.subs{1} = n;
                        out = out + subsref(self, args);
                    end
                otherwise
                    error('GtVolumesArray:wrong_argument', ...
                        'Wrong dimention: %d (only [1-4])', dim)
            end
        end
    end
end