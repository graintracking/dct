classdef GtLock < handle
% Simple lock function to be used in multi-processing environment
% Uses file locks in temp_14_days directory.
    properties
        filename;
        lockName;

        hasLock;
    end

    methods (Access = public)
        function obj = GtLock(filename)
            if ( (~exist('filename', 'var')) || (isempty(filename)) )
                error('LOCK:no_file_name', ...
                      'You didn''t specify a filename to access concurrently');
            end
            obj.filename = filename;

            filename(filename == ':') = [];
            filename(filename == '/') = '_';
            filename(filename == '\') = '_';
            filename(filename == '.') = '_';

            obj.lockName = fullfile('/tmp_14_days', 'graintracking', 'locks', [filename '.lock']);
            [tempPath, ~, ~] = fileparts(obj.lockName);
            if (~exist(tempPath, 'dir'))
                [~, msg] = mkdir(tempPath);
                disp(msg)
            end
            obj.hasLock = false;
        end

        function acquire(obj, timeout)
        % GTLOCK/ACQUIRE Acquires the lock. There's also an optional timeout.
            if (~exist('timeout', 'var'))
                timeout = 0;
            end
            counter = 0;
            while (~obj.hasLock)
                if (~exist(obj.lockName, 'file'))
                    try
                        fid = fopen(obj.lockName, 'w');
                        if (fid == -1)
                            error('LOCK:could_not_create', ...
                                  'Creating the lock file gave an error');
                        end
                        fclose(fid);
                        obj.hasLock = true;
                    catch mexc
                        gtPrintException(mexc)
                        continue;
                    end
                else
                    if (timeout && ( (counter * 0.0156) > timeout))
                        error('LOCK:timeout_exceded', ...
                              'The lock was not released in the specified timeout');
                    end
                    pause(0.0156);
                    counter = counter + 1;
                end
            end
        end

        function release(obj)
        % GTLOCK/RELEASE Releases the lock
            if (obj.hasLock)
                delete(obj.lockName)
                obj.hasLock = false;
            end
        end

        function delete(obj)
        % GTLOCK/DELETE Destructor, which takes care of removing also possible
        % locks
            obj.release();
            delete@handle(obj);
        end
    end
end
