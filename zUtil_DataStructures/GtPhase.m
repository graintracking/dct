classdef GtPhase < handle
% GTPHASE Is a class that contains all the information of a given phase. All the
% grains are organized in a performance oriented structure and get/set methods
% are provided, to aid the consistent use of the class.
    properties
        selectedGrains   = true(0, 1);  % Booleans that enable/disable grains
        completeness     = zeros(0, 1); % Index of completeness of a grain [0, 1]
        center           = zeros(0, 3); % Center of mass of the grain
        R_vector         = zeros(0, 3); % Orientation (in Rodrigues space) of the grain
        boundingBox      = zeros(0, 6); % Bounding Box: [x y z lx ly lz] ?
        volume           = zeros(0, 1); % Volume of the segmented grain
        active           = true;        % Boolean that enables/disables the phase
        phaseName        = '';          % Recognisable name for the phase
        volumeFile       = '';          % Path of the mat file which contains the data.
                                        % The syntax is: 'filename:field' where field is
                                        % the field in the mat file which actually contains
                                        % the volume.
        bboxExtremes     = zeros(0, 6);
        orderedPositions = {};

        use_extended     = false(0, 1);
        extended_params  = GtPhase.makeExtendedField(cell(0, 1), cell(0, 1));

        clusters         = GtPhase.makeCluster( ...
            cell(0, 1), cell(0, 1), cell(0, 1), cell(0, 1), cell(0, 1), cell(0, 1), cell(0, 1));
    end

    properties (Access = public, Hidden = true)
        grains_number    = 0;      % Number of grains
    end

    methods
        function obj = GtPhase(name, numberOfGrains)
        % GTPHASE/GTPHASE( name, numberOfGrains )
        % Initializes the phase 'name', to the 'numberOfGrains'
            if (~exist('name', 'var'))
                name = '';
            end
            if (~exist('numberOfGrains', 'var'))
                numberOfGrains = 0;
            end
            obj.phaseName = name;
            obj.initGrains(numberOfGrains);
        end

        function initGrains(obj, number)
        % GTPHASE/INITGRAINS( number ) Initializes the phase to the 'number' of
        % grains
            obj.grains_number    = number;

            obj.selectedGrains   = true(number, 1);
            obj.completeness     = ones(number, 1);
            obj.center           = zeros(number, 3);
            obj.R_vector         = zeros(number, 3);
            obj.boundingBox      = zeros(number, 6);
            obj.bboxExtremes     = zeros(number, 6);
            obj.volume           = zeros(number, 1);
            obj.orderedPositions = {};

            obj.use_extended     = false(number, 1);
            temp_init_cell       = cell(number, 1);
            obj.extended_params  = GtPhase.makeExtendedField(temp_init_cell, temp_init_cell);
            obj.clusters         = GtPhase.makeCluster( ...
                cell(0, 1), cell(0, 1), cell(0, 1), cell(0, 1), cell(0, 1), cell(0, 1), cell(0, 1));
        end

        function dropGrain(obj)
            obj.grains_number    = obj.grains_number - 1;

            obj.selectedGrains   = obj.selectedGrains(1 : end-1);
            obj.completeness     = obj.completeness(1 : end-1);
            obj.center           = obj.center(1 : end-1, :);
            obj.R_vector         = obj.R_vector(1: end-1, :);
            obj.boundingBox      = obj.boundingBox(1 : end-1, :);
            obj.bboxExtremes     = obj.bboxExtremes(1: end-1, :);
            obj.orderedPositions = {};

            obj.use_extended     = obj.use_extended(1:end-1);
            obj.extended_params  = obj.extended_params(1:end-1);
        end

        function pushGrain(obj)
        % GTPHASE/PUSHGRAIN Adds a new empty grain at the end of the list
            obj.grains_number    = obj.grains_number + 1;

            obj.selectedGrains   = [obj.selectedGrains; true];
            obj.completeness     = [obj.completeness; 1];
            obj.center           = [obj.center; 0 0 0];
            obj.R_vector         = [obj.R_vector; 0 0 0];
            obj.boundingBox      = [obj.boundingBox; 0 0 0 0 0 0];
            obj.bboxExtremes     = [obj.bboxExtremes; 0 0 0 0 0 0];
            obj.orderedPositions = {};

            obj.use_extended     = [obj.use_extended; false];
            obj.extended_params  = [obj.extended_params; ...
                GtPhase.makeExtendedField([], [])];
        end

        function copy = getCopy(obj)
            copy = GtPhase(obj.phaseName, obj.grains_number);

            copy.active           = obj.active;
            copy.volumeFile       = obj.volumeFile;

            copy.selectedGrains   = obj.selectedGrains;
            copy.completeness     = obj.completeness;
            copy.center           = obj.center;
            copy.R_vector         = obj.R_vector;
            copy.boundingBox      = obj.boundingBox;

            copy.bboxExtremes     = obj.bboxExtremes;
            copy.orderedPositions = obj.orderedPositions;

            copy.use_extended     = obj.use_extended;
            copy.extended_params  = obj.extended_params;
            copy.clusters         = obj.clusters;
        end

        function fillGrain(obj, grain)
            gr_id                       = grain.id;
            obj.selectedGrains(gr_id)   = true;
            obj.completeness(gr_id)     = 0;
            obj.center(gr_id, :)        = grain.center;
            obj.R_vector(gr_id, :)      = grain.R_vector;
            if ~isfield(grain, 'boundingBox')
                grain.boundingBox       = zeros(1, 6);
                grain.bboxExtremes      = zeros(1, 6);
            end
            obj.boundingBox(gr_id, :)   = grain.boundingBox;
            obj.bboxExtremes(gr_id, :)  = grain.bboxExtremes;
            obj.use_extended(gr_id)     = false;
            obj.extended_params(gr_id)  = GtPhase.makeExtendedField([], []);
        end

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Setter Functions

        % phase fields
        function setSelected(obj, grainid, sel)
            if ((~exist('grainid', 'var') || isempty(grainid)))
                grainid = 1:obj.getNumberOfGrains();
            end
            obj.selectedGrains(grainid, 1) = sel;
        end

        function setCompleteness(obj, grainid, compl)
            if ((~exist('grainid', 'var') || isempty(grainid)))
                grainid = 1:obj.getNumberOfGrains();
            end
            if ((compl < 0) || (compl > 1))
                error('DATA:wrong_parameter', ...
                      ['Completeness should be between [0, 1], but is: ' num2str(compl)]);
            end
            obj.completeness(grainid, 1) = compl;
        end

        function setCenter(obj, grainid, center)
            if ((~exist('grainid', 'var') || isempty(grainid)))
                grainid = 1:obj.getNumberOfGrains();
            end
            obj.center(grainid, :) = center;
        end

        function setR_vector(obj, grainid, R_vec)
            if ((~exist('grainid', 'var') || isempty(grainid)))
                grainid = 1:obj.getNumberOfGrains();
            end
            obj.R_vector(grainid, :) = R_vec;
        end

        function setBoundingBox(obj, grainid, bbox)
            if ((~exist('grainid', 'var') || isempty(grainid)))
                grainid = 1:obj.getNumberOfGrains();
            end
            obj.boundingBox(grainid, :) = bbox;
            obj.bboxExtremes(grainid, :) = [bbox(1:3) (bbox(1:3) + bbox(4:6) -1) ];
        end

        % grains fields
%         function setMergeIDs(obj, grainid, ids)
%             obj.grains{grainid}.mergeIDs = ids;
%         end

        function setUseExtended(obj, grainid, use_ext)
            if ((~exist('grainid', 'var') || isempty(grainid)))
                grainid = 1:obj.getNumberOfGrains();
            end
            obj.use_extended(grainid, 1) = use_ext;
        end

        function setClusterInfo(obj, gr_ids, cl_info)
            cl_pos = numel(obj.clusters) + 1;
            for ii = 1:numel(obj.clusters)
                if (numel(gr_ids) == numel(obj.clusters(ii).included_ids) ...
                        && all(gr_ids == obj.clusters(ii).included_ids))
                    cl_pos = ii;
                    break;
                end
            end
            obj.clusters(cl_pos) = cl_info;
        end

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Getter Functions

        % phase fields
        function num_grains = getNumberOfGrains(obj)
            num_grains = obj.grains_number;
        end

        function sel = getSelected(obj, grainid)
            if ((~exist('grainid', 'var') || isempty(grainid)))
                grainid = 1:obj.getNumberOfGrains();
            end
            sel = obj.selectedGrains(grainid, 1);
        end

        function compl = getCompleteness(obj, grainid)
            if ((~exist('grainid', 'var') || isempty(grainid)))
                grainid = 1:obj.getNumberOfGrains();
            end
            compl = obj.completeness(grainid, 1);
        end

        function center = getCenter(obj, grainid)
            if ((~exist('grainid', 'var') || isempty(grainid)))
                grainid = 1:obj.getNumberOfGrains();
            end
            center = obj.center(grainid, :);
        end

        function center = getCenterPixels(obj, grainid, pixelsize)
            if ((~exist('grainid', 'var') || isempty(grainid)))
                grainid = 1:obj.getNumberOfGrains();
            end
            if exist('pixelsize','var') && isempty(pixelsize) && pixelsize ~= 0
                center = obj.center(grainid, :) * pixelsize;
            else
                center = obj.center(grainid, :);
                disp('Pixel size was not provided')
            end
        end

        function R_vec = getR_vector(obj, grainid)
            if ((~exist('grainid', 'var') || isempty(grainid)))
                grainid = 1:obj.getNumberOfGrains();
            end
            R_vec = obj.R_vector(grainid, :);
        end

        function bbox = getBoundingBox(obj, grainid)
            if ((~exist('grainid', 'var') || isempty(grainid)))
                grainid = 1:obj.getNumberOfGrains();
            end
            bbox = obj.boundingBox(grainid, :);
        end

        function bbox = getBoundingBoxExtremes(obj, grainid)
            if ((~exist('grainid', 'var') || isempty(grainid)))
                grainid = 1:obj.getNumberOfGrains();
            end
            bbox = obj.bboxExtremes(grainid, :);
        end

        function volume = getGrainVolume(obj, grainid)
            if ((~exist('grainid', 'var') || isempty(grainid)))
                grainid = 1:obj.getNumberOfGrains();
            end
            volume = prod(obj.boundingBox(grainid, 4:6),2);
        end

        function shift = getBoundingBoxShift(obj, grainid)
            if ((~exist('grainid', 'var') || isempty(grainid)))
                grainid = 1:obj.getNumberOfGrains();
            end
            shift = obj.boundingBox(grainid, 1:3);
        end

        function use_ext = getUseExtended(obj, grainid)
            if ((~exist('grainid', 'var') || isempty(grainid)))
                grainid = 1:obj.getNumberOfGrains();
            end
            use_ext = obj.use_extended(grainid, 1);
        end

        function cl_info = getClusterInfo(obj, gr_ids)
            for ii = 1:numel(obj.clusters)
                if (numel(gr_ids) == numel(obj.clusters(ii).included_ids) ...
                        && all(gr_ids == obj.clusters(ii).included_ids))
                    cl_info = obj.clusters(ii);
                    return;
                end
            end
            cl_info = [];
        end

        function a_cls = getActiveClusters(obj)
            a_cls = obj.clusters(logical([obj.clusters(:).active]));
        end

        function setClusterActive(obj, clusterID, state)
            obj.clusters(clusterID).active = logical(state);
        end

        function clusterIDs = getActiveClusterIDs(obj)
            clusterIDs = find([obj.clusters.active]);
        end

        function cluster_table = getClusterTable(obj)
            a_cls = obj.getActiveClusters();
            grainIDs = [a_cls.included_ids]';

            clusterIDs = [];
            active_clusters = obj.getActiveClusterIDs;
            for ii = 1:numel(active_clusters)
                cl_ii = active_clusters(ii);
                n_ids_cl = numel(obj.clusters(active_clusters(ii)).included_ids);
                clusterIDs = [clusterIDs; cl_ii * ones(n_ids_cl, 1)]; %#ok<AGROW>
            end

            cluster_table = cat(2, clusterIDs, grainIDs);
        end

        function clusterID = getClusterID(obj, grainID)
            cluster_table = obj.getClusterTable();
            if (~isempty(cluster_table))
                clusterID = cluster_table(cluster_table(:, 2) == grainID, 1);
            else
                clusterID = [];
            end
        end
        % grains fields
%         function ids = getMergeIDs(obj, grainid)
%             ids = obj.grains{grainid}.mergeIDs;
%         end

        function grainIDs = getGrainsWithCompletenessLessThan(obj, value)
        % GTPHASE/GETGRAINSWITHCOMPLETENESSLESSTHAN  Gets grains with
        % (0 < completeness <= value)
            grainIDs = find(obj.completeness <= value & obj.completeness > 0);
        end

        function deselected = getDeselectedGrains(obj)
        % GTPHASE/GETDESELECTEDGRAINS  Gets deselected grains
            deselected = find(obj.selectedGrains == false);
        end

        function selected = getSelectedGrains(obj)
        % GTPHASE/GETSELECTEDGRAINS  Gets selected grains
            selected = find(obj.selectedGrains == true);
        end


        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Analysis Functions

        function buildBBSearchList(obj)
        % buildBBSearchList(obj)
            for ii = 1:size(obj.bboxExtremes, 2)
                [obj.orderedPositions{ii,1:2}] = sort(obj.bboxExtremes(:,ii));
            end
        end

        function matchList = simpleSearchBBtouched(obj, point)
        % matchList = simpleSearchBBtouched(obj, point)

            matchList = find(  obj.bboxExtremes(:,1) <= point(1) ...
                             & obj.bboxExtremes(:,2) <= point(2) ...
                             & obj.bboxExtremes(:,3) <= point(3) ...
                             ...
                             & obj.bboxExtremes(:,4) >= point(1) ...
                             & obj.bboxExtremes(:,5) >= point(2) ...
                             & obj.bboxExtremes(:,6) >= point(3) ...
                            );
        end

        function IDs = getNeighbours(obj, grainID, oversize)
        % IDs = getNeighbours(obj, grainID, oversize)

            if (~exist('oversize', 'var'))
                oversize = 1;
            end
            numGrains = obj.getNumberOfGrains();
            replOnes = ones(numGrains, 1);

            halfLengths = obj.boundingBox(:, 4:6) / 2;
            grainHalfLengths = halfLengths(grainID, :);

            maxDistances = halfLengths + grainHalfLengths(replOnes, :);

            centersInPixels = obj.boundingBox(:, 1:3) + halfLengths;
            grainCenter = centersInPixels(grainID, :);
            relativeCenters = abs(centersInPixels - grainCenter(replOnes, :));

            IDs = setdiff(find(all(relativeCenters < (maxDistances * oversize), 2)), grainID);
        end

        function merge = checkOverlapConflict(obj, id1, id2, pixelsize)
        % function merge = checkOverlapConflict(obj, id1, id2, pixelsize)

            merge_value1 = false;
            merge_value2 = false;

            % centers in pixels
            c1 = obj.center(id1, :) / pixelsize;
            c2 = obj.center(id2, :) / pixelsize;
            dist = norm(c1 - c2);
            b1 = obj.boundingBox(id1, :);
            b2 = obj.boundingBox(id2, :);
            % mean of two volumes : get radius equivalent and get 10% of it
            % as threshold
            thr_dist = mean(prod(b2(4:6)), prod(b1(4:6))) ^ (1/3) / 10;
            if (dist < thr_dist)
                merge_value1 = true;
            end

            % R_vector
            r1 = obj.R_vector(id1, :);
            r2 = obj.R_vector(id2, :);
            angle = atan2(norm(cross(r1,r2)), dot(r1,r2));
            angle = rad2deg(angle);
            thr_angle = 5;

            if (angle < thr_angle)
                merge_value2 = true;
            end

            merge.grainID_1 = id1;
            merge.grainID_2 = id2;
            merge.center_1 = c1;
            merge.center_2 = c2;
            merge.distance = dist;
            merge.bbox_1 = b1;
            merge.bbox_2 = b2;
            merge.rvector_1 = r1;
            merge.rvector_2 = r2;
            merge.mis_angle = angle;
            merge.checkDistance = merge_value1;
            merge.checkAngle = merge_value2;
            merge.tobemerged = merge_value1 & merge_value2;
        end
    end

    methods (Access = public, Static)
        function extended_grain = makeExtendedField(bb_r_space, bb_o_space)
            extended_grain = struct( ...
                'bb_r_space', bb_r_space, 'bb_o_space', bb_o_space);
        end

        function cluster = makeCluster(included_ids, ref_r_vec, bb_r_space, bb_o_space, cluster_type, twin_paths, active)
        % FUNCTION cluster = GtPhase.makeCluster(included_ids, ref_r_vec, bb_r_space, bb_o_space, cluster_type, active)
        %   Cluster types:
        %       0 - Regular cluster with grains that are close both in real
        %           and orientation-space
        %       1 - Twinned cluster
        %       2 - Neighbouring grains, which are not close in
        %           orientation-space

            if (~exist('cluster_type', 'var') || isempty(cluster_type))
                cluster_type = 0;
            end
            if (~exist('active', 'var') || isempty(active))
                active = false;
            end
            cluster = struct( ...
                'included_ids', included_ids, 'ref_r_vec', ref_r_vec, ...
                'bb_r_space', bb_r_space, 'bb_o_space', bb_o_space, ...
                'cluster_type', cluster_type, 'twin_paths', twin_paths, 'active', logical(active));
        end

        function obj = loadobj(obj)
            % Function to keep some sort of backwards compatibility
            if (numel(obj.use_extended) == 0)
                obj.use_extended = false(obj.grains_number, 1);
                temp_init_cell = cell(obj.grains_number, 1);
                obj.extended_params = GtPhase.makeExtendedField(temp_init_cell, temp_init_cell);
            end
            if (~isfield(obj.clusters, 'cluster_type') || ~isfield(obj.clusters, 'twin_paths'))
                if (numel(obj.clusters) > 0)
                    if (~isfield(obj.clusters, 'cluster_type'))
                        cluster_type_cellarray = 0;
                    else
                        cluster_type_cellarray = {obj.clusters(:).cluster_type};
                    end
                    if (~isfield(obj.clusters, 'twin_paths'))
                        twin_paths_cellarray = [];
                    else
                        twin_paths_cellarray = {obj.clusters(:).twin_paths};
                    end
                    obj.clusters = GtPhase.makeCluster( ...
                        {obj.clusters(:).included_ids}, ...
                        {obj.clusters(:).ref_r_vec}, ...
                        {obj.clusters(:).bb_r_space}, ...
                        {obj.clusters(:).bb_o_space}, ...
                        cluster_type_cellarray, ...
                        twin_paths_cellarray, ...
                        {obj.clusters(:).active});
                else
                    obj.clusters = GtPhase.makeCluster( ...
                        cell(0, 1), cell(0, 1), cell(0, 1), cell(0, 1), cell(0, 1), cell(0, 1), cell(0, 1));
                end
            end
        end
    end
end
