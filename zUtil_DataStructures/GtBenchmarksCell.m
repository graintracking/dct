classdef GtBenchmarksCell < GtBenchmarks
    properties
        num_repetitions = 7;
        num_cells = 100;
        num_points_x_cell = 50;

        data_type = 'single';
    end

    methods (Access = public)
        function self = GtBenchmarksCell(varargin)
            self = self@GtBenchmarks();
            self = parse_pv_pairs(self, varargin);
        end

        function var = init_cell(self, factor, func)
            if (~exist('factor', 'var') || isempty(factor))
                factor = 1;
            end
            if (~exist('func', 'var'))
                func = @rand;
%                 func = @ones;
            end

            var = cell(1, self.num_cells);
            for ii = 1:self.num_cells
                var{ii} = factor .* func(self.num_points_x_cell([1 1 1]), self.data_type);
            end
        end

        function benchmark_function_copy(self, desc, func_m, func_c, varargin)
            bytes = GtBenchmarks.getSizeVariable(varargin);
            GtBenchmarks.print_header(['Test: ' desc], bytes)

            max_chars = max(numel(func2str(func_m)), numel(func2str(func_c)));
            frmt_str = [' - Testing "%' sprintf('%d', max_chars) 's" (x%d)..'];

            fprintf(frmt_str, func2str(func_m), self.num_repetitions);
            c = tic();
            for ii = 1:self.num_repetitions
                data_m = func_m(varargin{:});
            end
            GtBenchmarks.print_result('\b\b: Done.', toc(c)/self.num_repetitions, bytes);

            fprintf(frmt_str, func2str(func_c), self.num_repetitions);
            c = tic();
            for ii = 1:self.num_repetitions
                data_c = func_c(varargin{:}, 'threads', self.num_threads);
            end
            GtBenchmarks.print_result('\b\b: Done.', toc(c)/self.num_repetitions, bytes);

            if (GtBenchmarks.check_errors(data_m, data_c, true))
                disp('WARNING: detected an error in the computation.')
            end
        end

        function benchmark_plus(self)
            function o = cell_plus(a, b)
                o = cell(size(a));
                for ii = 1:numel(a)
                    o{ii} = a{ii} + b{ii};
                end
            end
            a = self.init_cell();
            b = self.init_cell();
            self.benchmark_function_copy('cell plus (wo/ scale)', @cell_plus, @gtCxxMathsCellPlus, a, b);
        end

        function benchmark_minus(self)
            function o = cell_minus(a, b)
                o = cell(size(a));
                for ii = 1:numel(a)
                    o{ii} = a{ii} - b{ii};
                end
            end
            a = self.init_cell();
            b = self.init_cell();
            self.benchmark_function_copy('cell minus (wo/ scale)', @cell_minus, @gtCxxMathsCellMinus, a, b);
        end

        function benchmark_plus_scale(self)
            function o = cell_plus(a, b, ~, c)
                o = cell(size(a));
                for ii = 1:numel(a)
                    o{ii} = a{ii} + c .* b{ii};
                end
            end
            a = self.init_cell();
            b = self.init_cell();
            self.benchmark_function_copy('cell plus (w/ scale)', @cell_plus, @gtCxxMathsCellPlus, a, b, 'scale', 0.5);
        end

        function benchmark_times(self)
            function o = cell_times(a, b)
                o = cell(size(a));
                for ii = 1:numel(a)
                    o{ii} = a{ii} .* b{ii};
                end
            end
            a = self.init_cell();
            b = self.init_cell();
            self.benchmark_function_copy('cell times', @cell_times, @gtCxxMathsCellTimes, a, b);
        end

        function benchmark_divide(self)
            function o = cell_divide(a, b)
                o = cell(size(a));
                for ii = 1:numel(a)
                    o{ii} = a{ii} ./ b{ii};
                end
            end
            a = self.init_cell();
            b = self.init_cell();
            self.benchmark_function_copy('cell divide', @cell_divide, @gtCxxMathsCellDivide, a, b);
        end

        function benchmark_min(self)
            function o = cell_min(a, b)
                o = cell(size(a));
                for ii = 1:numel(a)
                    o{ii} = min(a{ii}, b{ii});
                end
            end
            a = self.init_cell();
            b = self.init_cell();
            self.benchmark_function_copy('cell divide', @cell_min, @gtCxxMathsCellMin, a, b);
        end

        function benchmark_max(self)
            function o = cell_max(a, b)
                o = cell(size(a));
                for ii = 1:numel(a)
                    o{ii} = max(a{ii}, b{ii});
                end
            end
            a = self.init_cell();
            b = self.init_cell();
            self.benchmark_function_copy('cell divide', @cell_max, @gtCxxMathsCellMax, a, b);
        end

        function benchmark_sqrt(self)
            function o = cell_sqrt(a)
                o = cell(size(a));
                for ii = 1:numel(a)
                    o{ii} = sqrt(a{ii});
                end
            end
            a = self.init_cell();
            self.benchmark_function_copy('cell sqrt', @cell_sqrt, @gtCxxMathsCellSqrt, a);
        end

        function benchmark_abs(self)
            function o = cell_abs(a)
                o = cell(size(a));
                for ii = 1:numel(a)
                    o{ii} = abs(a{ii});
                end
            end
            a = self.init_cell();
            self.benchmark_function_copy('cell abs', @cell_abs, @gtCxxMathsCellAbs, a);
        end

        function benchmark_all(self)
            func_list = {
                @self.benchmark_plus;
                @self.benchmark_plus_scale;
                @self.benchmark_minus;
                @self.benchmark_times;
                @self.benchmark_divide;
                @self.benchmark_min;
                @self.benchmark_max;
                @self.benchmark_sqrt;
                @self.benchmark_abs;
            };

            for ii = 1:numel(func_list)
                func = func_list{ii};
                func();
            end
        end
    end
end