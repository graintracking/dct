classdef GtLockDB < handle
% Simple lock function to be used in multi-processing environment
% creates and release entries in dataname_filelocks table
    properties
        filename;
        filetable;

        hasLock;
    end

    methods (Access = public)
        function obj = GtLockDB(filetable, filename)
            if ( (~exist('filename', 'var')) || (isempty(filename)) )
                error('LOCK:no_file_name', ...
                      'You didn''t specify a filename to access concurrently');
            end
            if ( (~exist('filetable', 'var')) || (isempty(filetable)) )
                error('LOCK:no_file_name', ...
                      'You didn''t specify the name of the database filetable');
            end
            obj.filename  = filename;
            obj.filetable = filetable;
            obj.hasLock   = false;

            gtDBConnect();
        end

        function acquire(obj, timeout)
        % GTLOCK/ACQUIRE Acquires the lock. There's also an optional timeout.
            if (~exist('timeout', 'var'))
                timeout = 0;
            end
            query = sprintf('INSERT INTO %s (filename) VALUES ("%s")', obj.filetable, obj.filename);
            first_time = true;
            if ~obj.hasLock
                counter = 0;
                test = 0;
                while (~test)

                    try
                        test = mym(query);
                    catch mexc
                        %gtPrintException(mexc)
                        obj.hasLock = false;
                        test = false;
                        if (first_time)
                            first_time = false;
                            fprintf('Locked file: %s (from table %s)', obj.filename, obj.filetable)
                        end
                    end

                    if test
                        obj.hasLock = true;    
                    else
                        if (timeout && ( (counter * 0.0156) > timeout))
                            error('LOCK:timeout_exceded', ...
                                  'The lock was not released in the specified timeout');
                        end
                        pause(0.0156);
                        counter = counter + 1;
                    end
                end
            else 
                disp('you have already acquired a lock for the current file');
            end
        end

        function release(obj)
        % GTLOCK/RELEASE Releases the lock
            if (obj.hasLock)
                test = mym(sprintf('DELETE FROM %s WHERE (filename) = ("%s")', obj.filetable, obj.filename));
                if test
                    %disp('release file lock')
                    obj.hasLock = false;
                else
                    disp('releasing file lock failed for some reason !!?');
                end
            end
        end

        function delete(obj)
        % GTLOCK/DELETE Destructor, which takes care of removing also possible
        % locks
            obj.release();
            delete@handle(obj);
        end
    end

    methods (Access = public, Static)
        function clear(filetable, filename)
            test = mym(sprintf('DELETE FROM %s WHERE (filename) = ("%s")', filetable, filename));
            if test
                %disp('release file lock')
%                 obj.hasLock = false;
                disp('released');
            else
                disp('releasing file lock failed for some reason !!?');
            end
        end
    end
end
