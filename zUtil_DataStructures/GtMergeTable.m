classdef GtMergeTable < handle
    properties
        fileLock;
        fieldNames;
    end

    methods (Access = public)
        function obj = GtMergeTable(fileTable, fileName, fieldNames)
            obj.fileLock = GtLockDB(fileTable, fileName);
            obj.fieldNames  = fieldNames;
        end

        function fillTable(obj, tmp_array, fieldName)
            % number of rows to be updated / inserted
            num_rows = length(tmp_array);

            % Acquire Lock and load the most recent version of the file
            obj.fileLock.acquire();

            table = load(obj.fileLock.filename, sprintf('%s', fieldName));

            if (length(table.(fieldName)) < num_rows)
                table.(fieldName){num_rows} = [];
            end

            % now fill the table with the new information
            for ii = 1 : num_rows
                if ~isempty(tmp_array{ii})
                    table.(fieldName){ii} = [table.(fieldName){ii}, tmp_array{ii}];
                    %table.(fieldName){ii} = unique([table.(fieldName){ii}]);
                    %fprintf('filling row %d\n',ii)
                end
            end

            % save the file and release Lock
            save(obj.fileLock.filename, '-append', '-struct', 'table');
            obj.fileLock.release();
        end
    end
end
