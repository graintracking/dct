\chapter{Friedel Pair Matching GUI}
\label{ch:matching}
by Peter Reischig \\
updated on 05 September 2012 \\

\section{Description}
Friedel pairs are sought among the segmented diffraction spots and are used later 
as an input for the grain indexing stage. Input for the pair matching are the 
segmented diffraction spots from the difspottable and parameters describing the 
setup geometry and the crystal lattice of the phases. Output is the Friedel pairs 
stored in the pairtable in the database.\\

The processing stages prior to pair matching only apply image processing steps; it 
is the matching stage which first uses the setup geometry and crystallographic 
information and assigns positions and angles to the diffraction events.\\

The matching algorithm considers the centroids of the diffraction spots (spatial 
information) and a list of possible reflections and Bragg angles (crystallographic 
information) to find the best Friedel pair combinations. The user can influence the 
search by setting a set of tolerances using a GUI. The GUI also enables the 
parameter refinement of the setup geometry or the crystal lattice parameters.\\

The underlying symmetry of the Friedel pairs provide a relatively robust and 
accurate way to determine the diffraction vectors (the diffraction angles eta, 
theta, omega) and scattering vectors of the specific lattice plane along with some 
incomplete location information about the grain center.\\

For a detailed description of the use of Friedel pairs, see:
\begin{itemize}
	\item paper by Ludwig et al. in Review of Scientific Instruments 80, 033905 (2009)
	\item book chapter on DCT by Ludwig, King, Reischig (2012)
\end{itemize}


\section{Input-Output}
Parameters file; fields refined and updated by the GUI:
\begin{itemize}
	\item \file|parameters.cryst|
	\item \file|parameters.labgeo|
	\item \file|parameters.match|
\end{itemize}
Following folders and files are created:
\begin{itemize}
	\item \file|3_pairmatching/match_DD-MM-YYYY_X.mat| - matching results saved as 
backup
	\item \file|3_pairmatching/parameters_old_DD-MM-YYYY_X.mat| - parameters file 
backup before
Database table modified:
	\item pairtable - Friedel pair results are stored here; it is next read in 
the indexing stage
\end{itemize}

\section{Function Dependence}
Various functions in:
\begin{itemize}
	\item \file|matlabDCT/3_pairmatchingGUI/*.m| - all functions of the GUI
	\item \file|matlabDCT/3_pairmatchingGUI/*.fig| - GUI layout editable in GUIDE
	\item \file|matlabDCT/zUtil_Cryst| - crystallographic functions
	\item \file|matlabDCT/zUtil_Geo| - geometry functions
\end{itemize}

\section{Other Dependencies}
\begin{itemize}
	\item database connection to access difspottable and pairtable
	\item Matlab Optimization Toolbox License used for the parameters fitting
	\item Matlab GUI editor GUIDE for editing the GUI layout
\end{itemize}

\section{How to use the GUI}

\subsection{Layout of the GUI}
The GUI can be launched with the \mCom|gtMATCHGUI| command. It has four windows, they can be selected in the Plot list:
\begin{itemize}
	\item \textbf{Main}: contains the status display and control features.
	\item \textbf{Eta vs. Theta plot}: this plot is the principal visual feedback on the matching and refinement results. 
It shows the Debye-Sherrer rings opened and straightened. Theta is the Bragg angle. Thus the angle between the incoming and 
diffracted beams is two theta, and the total opening angle of the diffraction cone is four theta. Eta is the azimuth angle of 
diffraction around the incident beam (like the dial of a clock) and is between 0 and 360 degrees.
	\item \textbf{Statistics}: plots for statistical feedback for the tolerance selection. It shows histograms for all the 
accepted pairs independent from the Error filter.
	\item \textbf{Pair figure} images of individual diffraction spots, paired spots, full images and a 3D plot of the matching 
geometry. This figure may be slow when running over the network, so is not launched until the user selects it.
\end{itemize}
The last 3 windows can be relaunched by clicking on the Reset figures button (this does not influence any computation).
\begin{figure}[htbp]
\centering
	\includegraphics[width=0.8\textwidth]{photos/MatchingGUI_base9.png}
	\caption{Main window}
	\label{fig:MatchMain}
\end{figure}

\begin{figure}[htbp]
\centering
	\includegraphics[width=\textwidth]{photos/MatchingGUI_base10.png}
	\caption{Eta versus Theta plot. Reflections should be aligned with the theoretical lines.}
	\label{fig:MatchThetaEtaPlot}
\end{figure}

\begin{figure}[htbp]
\centering
	\includegraphics[width=\textwidth]{photos/MatchingGUI_base11.png}
	\caption{Statistical information about the pair matching.}
	\label{fig:MatchStatistics}
\end{figure}

\begin{figure}[htbp]
\centering
	\includegraphics[width=\textwidth]{photos/MatchingGUI_base8.png}
	\caption{Plot showing individual diffraction spots and geometry.}
	\label{fig:MatchPairs}
\end{figure}

\subsection{Before using the GUI}
Make sure the setup geometry is properly set in parameters.labgeo.\\

Make sure the list of (hkl) reflections, angles and d-spacings are correct in parameters.cryst for all phases, both the families and the specific ones. 
Incomplete or wrongly formatted fields are a common cause of crash.

\subsection{Launching the GUI}
Launch \textcolor{blue}{gtMATCHGUI} in the dataset folder where the parameters file is. If there are already data in the pairtable, 
that previous result will be displayed. For any further action, you should click on Reset and redo the matching. 
If the windows extend beyond the screen, you can move them in a linux system by alt + left mouse click. It may occurs that, on the \textit{Eta vs Theta plot}, you could not see any reflection. This is due to an unknow parameter in \file|parameters.mat|. To solve this problem, type:
\begin{mCode}
>> p90=gtLoadParameters
>> p90.cryst
\end{mCode}
If you see \mCom+usedfam=0+, you have to change it. To do so:
\begin{mCode}
p90.cryst.usedfam=[1:N]
\end{mCode}
Where N is the number of \{hkl\} families you have. You will find N in \mCom+p90.cryst.hkl+, N being the number of columns of the matrix. Doing this, you assume that N \{hkl\} families will be present on the detector. Then, save your parameters:
\begin{mCode}
>> gtSaveParameters(p90)
\end{mCode}
 You can now relaunch \mCom+gtMATCHGUI+.

\subsection{Check setup geometry}
Open the \textcolor{blue}{Pair figure}; check if the geometry is as you expect it to be: detector orientation vectors, 
beam direction, rotation axis orientation and direction (sign of vector!). You can view the setup in 
3D with the \textcolor{blue}{Rotate 3D} icon in the toolbar. The scaling of this 3D view may be distorted, as the angle 
between the detector orientation vectors U and V is always displayed as right angle. Nevertheless, 
the signs and orientation of the setup components can be checked.

\subsection{Check Segmentation}
Check the quality of the segmentation in the Pair figure on randomly selected diffraction spots. 
Good segmentation would typically not show sharp edges or steps in intensity inside or on the outline 
of the spots (except if the grains constitute very low deformation or are on the boundary of 
the irradiated volume) and would not include background noise or multiple spots that do not overlap.

\subsection{Set tolerances and reflections}
Use the default tolerances for pair matching or modify them in the table. The most important one is 
\textcolor{blue}{thr\_theta} which is the absolute tolerance for the Bragg angle. In case of strained materials, 
larger tolerance should be used. The theta acceptance windows around the theoretical values are shown 
and updated in the Eta-Theta plot. This tolerance can be scaled linearly with the angle 
(\textcolor{blue}{thr\_teta\_scale}) but it is usually not needed.\\

Activate all the phases, the algorithm will choose the nearest phase and \{hkl\} family for each 
pair or (at precise) overlaps will leave it undefined and it will be determined at the indexing stage. 
You might want to disable and ignore the really weak reflections. The phase selection here is only 
considered in the matching, but not in later stages (for that, modify the parameter file).\\

Set the Error filter to infinite (Inf) to see all the pairs found.

\begin{figure}[htb]
\centering
\includegraphics[width=0.8\textwidth]{photos/MatchingGUI_base3.png}
\caption{}
\label{fig:matchGeobase3}
\end{figure}

\begin{figure}[htb]
\centering
\includegraphics[width=0.8\textwidth]{photos/MatchingGUI_base4.png}
\caption{}
\label{fig:matchGeobase4}
\end{figure}

\subsection{Pre-Matching}
Click on the \textcolor{blue}{Pre-Matching} button then on \textcolor{blue}{Pause} when there are enough pairs found, typically when the progress is around 15\%. 
Click \textcolor{blue}{Continue} to have more pairs. Each pair appears as a point in the \textcolor{blue}{Eta-Theta plot}. You should 
see clouds of points aligned along the theoretical theta values, although they may be shifted 
and distorted - meaning that the initial parameters are wrong, which is normally the case. In those 
clouds, you should generally see somewhat distinct lines formed by the best pair matches that 
have the smallest deviation from the mean. In the regular forward geometry most of the pairs 
appear at Eta=90\textdegree~and Eta=270\textdegree~for the full rings and at Eta=45,135,225,315\textdegree~for the 
partial rings due to the rectangular shape of the detector. There will be no pairs close to the 
pole at Eta=0,180\textdegree~that is along the rotation axis.\\

The pre-matching accepts pairs outside the allowed theta bands and excludes the theta values 
from the mean error of the pairs. It is important not to start with the normal matching even if 
the pattern looks good because it might be shifted  leading to a confusion of the \{hkl\} families, 
especially if there are not many of them.\\

Modify the detector parameters manually to bring the pattern aligned with the theoretical one. 
If needed, decrease the Error filter (>0) to see the best matches only. The parameters worth fitting are:
\begin{itemize}
\item 'X' adjusts the distance between the rotation axis and the detector. Playing with X will move your vertical lines horizontally.
\item 'Y' adjusts the rotation axis position. If your lines are curved, playing with Y should straighten them.
\item 'tilt in plane' corrects the roation of the detector around the direct beam axis.
\item 'Max error' compares the caracteristics of both spots of a pair, such as their area, their intensity, or their vertical over horizontal size ratio. A reasonable value is 0.5.
\end{itemize}
 You can be more precise by hitting the + and - keys. Use the /store/restore/save keys when you are satisfied or if you want to return to the previous value. \textit{thr\_theta} changes the acceptance band around each reflection (you don't want them overlapping). Changing the \textit{Max. error} value will also help 'tighten up' the fitting and reduce the number of spots considered. Usually we assume that 
the lattice parameters are accurate and fit the detector parameters. In other cases you may trust 
the setup parameters and want to modify the lattice parameters of some phases. The lattice parameters 
are displayed and modified for the selected active phase only  one at a time, but the changes are 
stored for all the phases, so you can switch between the phases.

Then pre-match 100\% and re-do the manual fitting. You can save your parameters after this.\\

Matlab sometimes takes a long time to load when (first) clicked on a GUI component, so be patient 
and wait for it to finish before clicking again on something.

\subsection{Matching}
When the pattern is close to the theoretical one, click \textcolor{blue}{Reset} and \textcolor{blue}{Match}. This will now only accept 
pairs in the allowed theta regions, and will include the theta deviation in the mean error of the pairs. 
Note that strained samples show not straight but curved lines in the Eta-Theta plot according to 
residual strains or external load. If the curve cannot be explained by strain and corrected by fitting 
the parameters, it is probably the effect of distortion in the detector image.\\

Check some random pairs in the Pair figure, both with small and large mean errors. It may help to decide 
whether the tolerances are too strict or too loose. Check the pair statistics which will show you which 
are the most binding constraints. Revise the matching tolerances if necessary. You will always find more 
pairs if the tolerances are loosened. They should be limited such that the majority of the worst pairs 
still seem to be a correct match. A certain amount of bad matches or overlaps is acceptable, as those 
pairs will probably not be indexed.

\subsection{Parameter refinement}
The beam direction, the rotation axis position and orientation is kept fixed. The origin of the LAB 
reference is usually chosen to be on the Z axis, which is parallel to the rotation axis. The location 
of the LAB origin along the rotation axis is a free parameter at this stage;  it only has to be consistent 
with the sample envelope. What is fitted are the detectors coordinates in the LAB reference, by
which its position relative to the rotation axis is also defined. In the end, what matters is the 
relative position of the setup components and the beam.\\

Try to refine the parameters by automatic fitting. Keep in mind that the \{hkl\} family and 
phase assignment or the mean errors are NOT updated when the parameters are modified! Their update requires 
the matching to be restarted.\\

Select the parameters to be fitted - the defaults are initially ticked. Only the active phases and \{hkl\} 
families will be used for fitting  as shown by a flash of red markers. Click on the \textcolor{blue}{Fit button} to run the 
optimisation and check the improvement by eye in the Eta-Theta plot. The fit attempts to minimise the mean 
squared error of the deviation of the true X-ray wavelength from the ones computed from each pair. 
The feedback light in the status display changes to green when the Fit has been run, but it does not mean 
that the fitted results are correct.\\

Fitted detector parameters in a common case:
\begin{itemize} 
\item detector position X,Y,Z: two of them are fitted and the 
one parallel to the rotation axis (usually Z) cannot be fitted, as it is a free parameter and does not 
affect the Friedel pair geometry. 
\item in-plane and two out-of-plane tilts: the oblique geometry of the 
diffraction can be badly affected by even small tilts of the detector plane.
\item pixel size ratio should be considered (usually small): best practice is probably doing the 
undistortion correction of the full images by not enforcing a relative scaling in the X-Y direction and 
then fit the relative pixel size ratio.
\end{itemize}

\paragraph{NOTE!}
The distance of the detector (usually X), the mean pixel size and a scaling factor of the three lattice 
dimensions (hydrostatic strain) have a very similar scaling effect on the calculated Eta-Theta plot. 
One can imagine that a detector with larger pixel size placed at a larger distance will detect the same 
spot centroid positions. Therefore, the regular forward geometry is not sensitive enough to fit those 
three parameters simultaneously. Best practice is to measure the lattice parameters independently 
(e.g. powder diffraction) and to measure or enforce the pixel size based on the image of a grid in the 
undistortion correction of the full images. Probably an appropriate mean pixel size over detector distance 
ratio gives good results, even if the two values are not set very accurately. If the Eta-Theta plot fits 
well to the theoretical one, then the actual set of parameters are probably adequate for grain reconstruction.\\
 
Even for strain analysis, somewhat inaccurate lattice parameters are probably acceptable; just keep in mind that 
the resolved strain will be relative to that reference undeformed unit cell. The strain can easily be recalculated 
without going through the processing again in case one wants to modify the reference lattice parameters later on.\\

Do the automatic fitting multiple times until you are satisfied with the result. Use as many pairs as you can. 
You can use the error filter to exclude the outliers, the phase and family selector and the theta limits to exclude 
entire phases, families or theta ranges. As the data typically has considerable errors, it is up to the user what is 
considered as an outlier, but excluding too many pairs may distort the fit. As a guideline, at least hundreds or thousands 
of pairs should be used, and the width of the Eta-Theta distribution should probably be reduced to an extent where the 
mean curvature of the lines, if any, is visible. Pairs with higher theta angles provide higher accuracy unless they are 
subject to distortion in the image.\\

The optimisation routine used for the fitting might give a local minimum instead of a global solution. If the fit is 
done on a subset before the matching has been finished, restarting the matching will give a new random set on which 
the fit can be tested again.\\

You can use the \textcolor{blue}{Store} button to keep a solution in memory and the \textcolor{blue}{Restore} button to recall it. When you are satisfied 
with the fit, save the actual parameters in the parameters file by clicking on the \textcolor{blue}{Save parameters} button. The feedback 
light in the status display changes to green when the results are saved, and changes back to red if the parameters are 
modified in the GUI but have not yet been saved.

\subsection{Final Matching}
When the right set of parameters has been found and refined, rerun the matching from beginning to the end and 
save the results by clicking on the \textcolor{blue}{Save results} button at the top without any further modification to the parameters. 
This way all pair data will be updated and the confidence in the \{hkl\} family assignment will be the highest.

\subsection{Export}
For debugging or analysis, all the GUI data can be accessed by clicking on the \textcolor{blue}{Export} button at the top. This copies the actual 
Main GUI data called handles into the base workspace. All graphics handles of the GUI figures and the data being processed 
is in this structure. The current latest values of the most important parameters are in:
\begin{center}
\parbox{0cm}{
\begin{tabbing}
%first line is for defining the tabstops
handles.LattParsCorrActxxx \= latest applied corrections to detector geometry \kill
handles.labgeo \> -- detector geometry (as in parameters.labgeo)\\
handles.matchpars \> -- matching tolerances (as in parameters.match)\\ 
handles.DetParsCorrAct  \> -- latest applied corrections to detector geometry\\ 
handles.LattParsCorrAct  \> -- crystal lattice parameters\\ 
handles.pairs \> -- all found pairs before applying the Error filter\\
handles.pairsfilt  \> -- pairs that passed the Error filter and are displayed\\
\end{tabbing}
}
\end{center}

\section{Note!}
\subsubsection{1. GUI behaviour}
GUIs in Matlab do not seem to be very robust. The handles to the graphics objects may be lost if 
an error or interruption occurs. Graphics objects may not be cleared properly from the memory and older features might reappear 
unexpectedly at initiation. Relaunching the GUI often helps. Do not click too fast on features; Matlab often needs time to 
load after first clicking on a button.

\subsubsection{2. Ambiguity in the geometry  mirrored grain map} 
Any DCT dataset can be processed and result in a good but mirrored 
grain map reconstruction of the sample. One should imagine a virtual setup that is mirrored on the detector plane, including the 
incoming beam and the rotation direction. It is then easy to see that the diffraction conditions for the grains would be fulfilled 
the exact same way as in the true setup, and one would obtain an identical diffraction pattern on the virtual setup. Therefore if 
the parameters are set such that the detector images are mirrored (flipped) along the rotation axis, and the rotation direction is 
opposite to the true one, one can obtain a grain map reconstruction that corresponds to the virtual setup and is equal to the true 
one mirrored on some plane. This will not be obvious in any of the processing phase (matching, indexing, reconstruction) until one 
tries to overlap the grain map with a true reconstruction (e.g. the absorption CT or EBSD map) of the sample.

\subsubsection{3. Ambiguity in geometry  successful matching but no grains} 
Following the arguments above, it can happen that one may 
succeed in the matching, but find no grains in the indexing stage. This probably means that the specified rotation direction is not 
consistent with the detector orientation. Check both, and alternatively find the good combination by trial and error until you can 
index properly.

\subsubsection{4. Flipping or rotation of full images} 
The code allows for describing any setup geometry, so there is no need to 
flip/rotate the full images and resegment them. Instead, choose (if unsure then by trial and error) the right parameters in labgeo 
for the detector orientation, rotation axis orientation AND direction (sign matters!).

\subsubsection{5. Reloading existing results} 
When previous matching results are loaded from the database and the parameters in 
the parameters file have been changed in the meantime, the display of the results may be inconsistent or misleading. Best is 
to delete the table and redo the matching.

\subsubsection{6. The Error filter} 
The Error filter and the geometry parameters may be changed any time and are applied 
instantaneously. Note, however, that the mean errors and \{hkl\} families of the pairs are not recalculated when the geometry 
parameters are changed! You have to restart the matching to update them.

\subsubsection{7. Effect of distortion in detector image} 
Distortion in the detector image can introduce a distortion in the 
Eta-Theta lines. If the curve cannot be explained by strain in the sample or corrected by fitting the parameters, it is 
probably the effect of distortion in the detector image.

\subsubsection{8. Saving the refined lattice parameters} 
The crystallographic information is not saved in the parameters file 
when clicking on the \textcolor{blue}{Save results} button. This has to be saved independently with the 
\textcolor{blue}{Save parameters} button.

\subsubsection{9. Saving, overwriting} 
Nothing is saved or gets overwritten until the user clicks on the \textcolor{blue}{Save parameters} or 
\textcolor{blue}{Save results} button. So the GUI can be launched any time and the matching can be tested without corrupting already existing data.

\section{Detailed description of the algorithm}
\subsection{GUI arrangement} 
The GUI consists of four figures that have their own layout stored in a fig file and callback 
functions stored in an m file. The opening function of the Main GUI launches the other GUIs and they store 
some of each others handles to be able to communicate. Most of the variables, including all the input, output 
and intermediate data are stored in the \emph{handles} of the Main GUI using the guidata functionality.\\

Figures of the GUI:
%\begin{center}
%\parbox{0cm}{
\begin{tabbing}
%first line is for defining the tabstops
xxx \= Etaxvs.xThetaxplot:x \= xxxxgtMatchGUIPairfigure.figx \= xgtMatchGUIPairFigure.m \kill
\> Main: \> $\rightarrow$ gtMATCHGUI.fig \> $\rightarrow$ gtMATCHGUI.m\\
\> Eta vs. Theta plot: \> $\rightarrow$ gtMatchGUIEtaVsTheta.fig \> $\rightarrow$ gtMatchGUIEtaVsTheta.m\\
\> Statistics: \> $\rightarrow$ gtMatchGUIStatistics.fig \> $\rightarrow$ gtMatchGUIStatistics.m\\
\> Pair figure: \> $\rightarrow$ gtMatchGUIPairFigure.fig \> $\rightarrow$ gtMatchGUIPairFigure.m\\
\end{tabbing}
%}
%\end{center}
%Main: → gtMATCHGUI.fig → gtMATCHGUI.m  
%Eta vs. Theta plot: → gtMatchGUIEtaVsTheta.fig → gtMatchGUIEtaVsTheta.m  
%Statistics: → gtMatchGUIStatistics.fig → gtMatchGUIStatistics.m  
%Pair figure: → gtMatchGUIPairFigure.fig → gtMatchGUIPairFigure.m
\subsubsection{1. Loading data from database} 
When the GUI is launched, the diffraction spot data are loaded from the database and all further operations 
happen in local memory. Some missing pair information is recalculated.
\indent
 $\rightarrow$  gtMatchLoadDifspotData

\subsubsection{2. Sorting spots into subsets}
When the \textcolor{blue}{(Pre-)Match} button is pushed, the spots are split into set A (the first half of the scan, 
omega \textless 180deg + tolerance) and set B (the second half of the scan, omega \textgreater 180deg - tolerance). 
In addition, for faster processing, they are also split into omega subranges according to the omega tolerance, 
so that later pairs are only sought in corresponding subranges and not in the entire set B.

 $\rightarrow$ gtMatchLaunchMatching\\ 
 $\rightarrow$ gtMatchSplitSpotData

\subsubsection{3. Pair properties; finding candidate pairs}
The algorithm starts stepping through set A in a random order and finds all possible Friedel pair 
candidates in the corresponding subset B for each spot in A. The diffraction angles, the \{hkl\} family 
and the mean error for each pair candidate are computed on the fly. The \{hkl\} family is chosen based 
on the nearest active theoretical Bragg angle. The mean error comprises deviations from the expected theta, 
and the difference between intensity, bounding box sizes and area of the two spots. Their contribution to 
the mean error is weighted with the inverse of the corresponding tolerances. Therefore, if the theta 
tolerance is very small, it will dominate the mean error and may distort the results. Only those pair 
candidates are accepted and stored that fulfil the Matching tolerances, the Theta limits and belong to 
one of the active reflections and phases as set in the GUI. The progress is displayed in the Status display. 
The diffraction angles and the Friedel pairs are found by mirroring a given spot A on the rotation axis and 
searching its pair B in the images approximately 180 degrees offset. The intersections of the detector plane 
with diffraction cones originating from the mirrored point A are determined. The Friedel pair B is searched 
along those intersections and inside the sample envelope projected from the mirrored point A onto the detector plane.

$\rightarrow$ gtMatchFindPairCandidates

\subsubsection{4. Ranking candidates; selection of best candidate combination}
When the Pause button is pushed or all the spots have been processed, the best combinations of all the so far 
found candidates are selected in a way that none of the spots are present in more than one pair. The ranking 
and selection is based on the mean error. Histograms based on the found pairs are computed and displayed.

 $\rightarrow$ gtMatchRankCandidates\\ 
 $\rightarrow$ gtMatchUpdateStatistics

\subsubsection{5. Applying the Error filter}
The Error filter may be changed by the user any time and is applied instantaneously to the list of pairs as the 
final step. Only pairs below this error limit are displayed in the Eta-Theta plot and will eventually be saved. 
The displayed statistics will not be influenced by the Error filter.

 $\rightarrow$ gtMatchFilterPairs

\subsubsection{6. Refinement of detector and lattice parameters}
When the matching is paused and the setup geometry or lattice parameters are changed manually or by clicking on 
the Fit button, it is immediately applied to the pairs and displayed in the Eta-Theta plot. Note, however, 
that the mean error and the {hkl} family of the pairs are not recalculated when the geometry is changed! 
There are 3 reasons for this:
(1) a change in the mean errors could mean that the initial selection of best 
candidate list is not anymore valid; 
(2) faster processing, especially for the automatic fitting; 
(3) to avoid discontinuities or strong non-linearity in the automatic fitting.

 $\rightarrow$ gtMatchUpdateLabgeo\\
 $\rightarrow$ gtMatchUpdatePairs\\
 $\rightarrow$ gtMatchUpdateThetas\\
 $\rightarrow$ gtMatchModifyDetPars\\
 $\rightarrow$ gtMatchModifyLattPars\\
 $\rightarrow$ gtMatchLatticeParDependencies\\
 $\rightarrow$ gtCrystDSpacing\\

\subsubsection{7. Automatic fitting of geometry and lattice parameters (optimization)}
When the matching is paused and the user clicks on the Fit button, a fitting of the best combination of 
all the selected parameters is attempted using a built-in Matlab non-linear least squares optimisation 
routine (lsqnonlin). The target function to be minimised is the sum of the square of deviations between 
the true known incoming X-ray wavelength and the ones calculated from each pair. If the true wavelength 
is not known, minimising the standard deviation of all the pairs can be considered (implemented in the 
same macro but not accessible), but it is probably less robust with outliers. The detector parameters 
are not fitted directly but their corrections listed in the GUI that are better suited for the 
optimisation and for practical usage. These corrections are then immediately applied to the actual 
values of the detector parameters.

 $\rightarrow$ gtMatchFitParameters

\subsubsection{8. Saving updated parameters}
When the matching is paused or finished and the user clicks on the Save parameters button, the parameter 
file is overwritten with the actual geometry (parameters.labgeo) and lattice parameters (parameters.cryst) 
for each phase. The d-spacings and Bragg angles are also updated.

 $\rightarrow$ gtMatchSaveFitParameters

\subsubsection{9. Saving final matching results}
When the matching is paused or finished and the user clicks on the Save results button, the displayed 
pairs are saved into the database, and the actual geometry (parameters.labgeo) is saved into the 
parameters file. Note that changes of the lattice parameters are not saved! Warnings appear in a
pop-up window if the fitting or matching process has not been completed.

 $\rightarrow$ gtMatchSaveResults
      
\section{Known Issues}
Old or strange data might appear in the Pair figure when the GUI is launched. It is a strange Matlab behaviour, 
graphics object are sometimes not deleted properly. Probably does not cause problems, just carry on.

\section{Suggestions for improvement}
Get rid of database pairtable, use the match results file instead; it can store all relevant information better. 
Reloading the data is more straightforward, and different results can be saved separately when the matching is rerun.\\

Implement a thorough check of the input; prevent crashing if input is incomplete.\\

The difference in theta to distinguish two \{hkl\} families or phases is hard coded 
(handles.ThetaUniqueTol); should be controlled by the user in the future.\\

Normalise intensities correctly; some of it is already implemented. Challenge: \{hkl\} family (and therefore the predicted 
intensity) is not known until a pair is created but it is still worth normalising after finding a candidate pair and use 
the deviation in the normalised intensity instead for the mean error.\\

Calculation of mean error: weighting the deviations with the inverse of the tolerances is reasonable but not the best. 
Consider a better way; e.g. for the comparison of pair candidates, consider each deviation independently and give points 
to the smallest 3 in that deviation, then sum these points; this considers each deviation equally and avoids the problem 
of comparing different units.\\

Mean errors and the \{hkl\} families are not recalculated when the geometry is changed; this might take too long in real 
time but could be done with an additional button in the GUI.\\

Control the GUI elements size and resizable behaviour better. Fixed size is easier and nicer on a big screen but may 
be problematic on a laptop. Matlab seems to have bugs or inconsistencies when elements are resizable.\\

The candidate search is now sequential but could be parallelised. Matlab's parfor feature cannot be used straight with 
the pause functionality of the search, though it could be solved with an extra wrap around.\\

A more careful clearing or initiation of the GUI figures may be required to avoid old and strange data appearing at initialisation.\\

The 3D plot in the pair figure is all relative to a rectangular detector plane. A (independent) plotting tool to 
visualise the setup geometry in 3D without distortions could be helpful, and could be utilised in other parts of the code as well.\\

The crystallographic information is not saved in the parameters file when clicking on the Save results button. This should also be saved.\\

Add a figure with plots of Theta vs. Omega, Eta vs. Omega, no. of pairs vs. Omega, etc. These are handy to quickly check if there 
was a problem during the scan, such as beam loss or larger sample drift.

\newpage

\section{Geometry}
\begin{figure}[htbp]
\centering
\includegraphics[width=0.6\textwidth]{photos/DCT_Pair_Matching_Geometry_1.png}
\caption{}
\label{fig:pair1}
\end{figure}

\begin{figure}[htbp]
\centering
\includegraphics[width=0.6\textwidth]{photos/DCT_Pair_Matching_Geometry_2.png}
\caption{}
\label{fig:pair2}
\end{figure}

\begin{figure}[htbp]
\centering
\includegraphics[width=0.7\textwidth]{photos/DCT_Pair_Matching_Geometry_3.png}
\caption{}
\label{fig:pair3}
\end{figure}
