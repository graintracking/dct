\chapter{Reconstruction}
\label{ch:rec}
\section{3D Reconstruction}
\subsection{Reconstruction algorithm}
The reconstruction of the shape of the grains is performed by an \ac{SIRT} algorithm. This type of programme is well know to reconstruct tomographic data. It uses summed diffraction spots, and treats them as parallel projections of the grain. The advantages of using diffraction spots instead of extinction spots are that they have a better contrast on the detector, and they are spread over a large area of the detector, so they are less likely to overlap.
\subsection{Using the function}
You will need a gpu machine for this section. Exit Matlab and type in the Shell:
\begin{shCode}
oarsub -I -l ``walltime=12:00:00'' -p ``gpu='YES'''
\end{shCode}

Then, launch the reconstruction function:
\begin{mCode}
>> gtSetupReconstruction

Do you want to recompile the function 'gtChangeThreshold3D'? [y/n] : [n] 
Do you want to recompile the function 'gtReconstructGrains'? [y/n] : [n] 
 
Getting the field list of parameters......Done!
 *** Checking default fields... *** 
  All the default fields are saved in the file.
Setting up Reconstruction for phase 01 (Zr)

Can not find absorption volume. Would you like to launch reconstruction of absorption volume? [y/n] : [y] 
Building geometry: Done.
Building sinogram: Done in 8.059861 seconds.
...done!
Writing sinogram in /data/id11/external/ma2274/id11/DCT_Analysis/sample_10_down850um_/absorption_stack.mat
Reconstructing: Done. (7.583633 seconds)
>> Can not find absorption mask. Would you like to segment absorption volume? [y/n] :
[y]

\end{mCode}

The absorption volume has to be defined on GUI.


Firstly, move the \textit{Slide through the slices} slider until you are able to see an image. After that, play with the \textit{Threshold} and  \textit{Overlay transparency} sliders to obtain an image similar to the figure above. Click right on the image and select \emph{Set as seed (morpho)}, then hit the \emph{Segment} button. you can finally launch the reconstruction. As usual, check the state of play with the batch gui.

\section{6D Reconstruction}

The 6D reconstruction algorithm takes into account the misorientation inside the grain itself. How does it work in principle? Each voxel present in the grain is given a position in the real space, but also a specific orientation in the orientation space. That is why it is called 6D. It allows a better recognition of the intragranular misorientation, due to the presence of clusters or twins for instance.\\

In order to use this algorithm, you firstly need to enable it in the parameters. To do so, type: 
\begin{mCode}
>> p=gtLoadParameters 
>> p.rec.grains=gtRecGrainsDefaultParameters('6DTV')
>> gtSaveParameters(p)
\end{mCode}
This will create a standard bounding box in the orientation space for each grain.\\
There are several variants available to use this algorithm, depending on what your grains look like.

\subsection{Default function}

This is the variant you have to use first when you try the 6D approach. It gives you an idea of what to do next. The use of this function is basically the same as for the 3D approach. However, you will have to modify some algorithm options before launching the reconstruction itself:
\begin{mCode}
>> p.rec.grains.options.num_interp=2
>> gtSaveParameters(p)
>> gtSetupReconstruction(phase_id, first_id, last_id)
\end{mCode}
You have the possibility to reconstruct only a part of your grains, whose IDs are between \emph{first\_id} and \emph{last\_id}. Don't worry if it takes a long time, it is normal.\\

 The information about the grains are located in the \file|4_grains/phase_01/grain_details_000X.mat| file, which contains:
\begin{itemize} 
\item \file|ODF6D|: orientation distribution function.
\item \file|SEG|: contains .seg and .segbb fields. These are useful for the Thresholding step.
\item \file|VOL3D|: contains .intensity and .shift fields. The shift indicates where to place the grain in the volume.
\end{itemize}

Once your reconstruction has finished, you need to have a look at the result, to decide which function you should use subsequently. To do so, go to the visualization chapter (\autoref{sec:6Drec}). If you are not really happy with the quality of the reconstruction, (e.g parts of some grains are missing), you have the possibility to try to change other algorithm options:

\begin{mCode}
>> p.rec.grains.options.num_interp=4
>> p.rec.grains.options.grid_edge=9
>> gtSaveParameters(p)
\end{mCode}
The \emph{grid\_edge} parameter is the number of points you have on each side of the cubic orientation bounding box. That means that with \emph{grid\_edge}=9, you will have 9*9*9=729 available orientations for each voxel in the real space.



\subsection{Extended function}
\label{subsec:extended}

Even if you tried your best with the default function, you may find some grains badly reconstructed or overlapping with other grains, when you look at the results in the GrainsAnalysis gui. This might be due to the presence of subgrains inside your grain. The orientation bounding box used for the reconstruction would thus be too small to contain the spread of orientations inside the grain. The Extended function is here to enlarge this bounding box, in order to reconstruct the subgrains all at once. Note that this function ignore what you did during the segmentation step, since it works directly with raw data.\\

You have too choose an orientation bounding box oversize, and a real space bounding box oversize. It is 1.1 by default, but you can set it as something between 1.1 and 2. You are now ready to create projection data which will be used by the reconstruction function:

\begin{mCode}
gt6DCreateProjDataFromExtendedGrain(g_id, p_id,'save', true,'ospace_oversize',2,'rspace_oversize',1.5)
\end{mCode}

Here, \textit{g\_id} is the id of the grain you want to extend and \textit{p\_id} is its phase id.
The information created are stored in \file|4_grains/phase_01/grain_extended_XXXX.mat|, which you can load with:

\begin{mCode}
gr=gtLoadGrain(phase_id, grain_id)
\end{mCode}

 When you have created extended data for all the grains you want, you can finally launch the extended 6D reconstruction, but you have to enable it for your grain first, so that the function will use the right orientation bounding box:

\begin{mCode}
>> sample = GtSample.loadFromFile()
>> sample.phases{phase_id}.setUseExtended(grain_id, true)
>> sample.saveToFile()
>> gtReconstructGrainExtended(grain_id, phase_id)
\end{mCode}
 This will create a new file \file|4_grains/phase_01/grain_extended_details_XXXX.mat| that you can load with:

\begin{mCode}
gr=gtLoadGrainRec(phase_id, grain_id)
\end{mCode}

\subsection{Clusters functions}
\label{subsec:clusters}

In the case you suspect the presence of clusters, you have the choice to use two different functions for your reconstruction:
\begin{itemize}
\item \textbf{subgrains cluster}: You were able to recognise clusters, but some subgrains are missing inside these clusters. You need to extend the real and orientation space bounding boxes of the cluster to segment the subgrains all at once. Here again, the function uses raw data. Its name is \mCom+gt6DCreateProjDataFromGrainCluster([gr_list], p_id)+, where \textit{gr\_list} is the list of the grains present in the cluster. Once you have created your clusters, you can reconstruct them with \mCom+gtReconstructGrainCluster([gr_list],p_id)+. The \file|sample.mat| file, located in the \file|4_grains| folder contains meta-data about the sample. To get information about the sample, type:
\begin{mCode}
>> sample=GtSample.loadFromFile()
>> sample.phases{1}.clusters
ans = 
1x1 struct array with fields:
    included_ids
    ref_r_vec
    bb_r_space
    bb_o_space
    cluster_type
    active
\end{mCode}

\begin{itemize}
\item included\_ids: List of grain ids present in the cluster
\item cluster\_type: It is a subgrains cluster here, so the value is 0
\item active: This should be 'true' to allow assembling
\end{itemize}

In case you have modified these fields, you need to save them:
\begin{mCode}
sample.saveToFile()
\end{mCode}

\item \textbf{Twins}: There are basically two types of reconstruction for twins:

\begin{itemize}

\item If both parent and twins were indexed in the previous steps, type:
 \begin{mCode}
>> samp=gt6DCreateProjDataFromTwinnedGrainFwdProj([parent_id, twin_ids], p_id,varargin)
\end{mCode}
where varargin can be one of these (with defaults):
\begin{itemize}
 \item  'verbose' : false
 \item  'min\_eta' : 15
 \item  'rspace\_oversize' : 1.1
 \item  'check\_spots' : false
 \item  'save' : false
\end{itemize}  
'save' has to become true in order to save the produced structure on disk. The syntax used to do this is:
\begin{mCode}
 samp=gt6DCreateProjDataFromTwinnedGrainFwdProj([parent_id, twin_ids], p_id, 'save', true)
\end{mCode}
This will build one single space orientation bounding box, but several separated space orientation bounding boxes for the parent and each of the twins. 

\item If the twins were not found during the indexing part, type:
\begin{mCode}
>> gr_str = gtForwardSimulate_v2(parent_id, parent_id, pwd, phase_id, 'twin_axes', <[the twin axis]>, 'twin_angles', <the twin angle>)
>> gr_str.g_twins(variant_num).proj.selected=gtGuiGrainMontage(gr_str.g_twins(variant_num), [], true);
>> samp_ors = gt6DCreateProjDataFromTwinnedGrainTheoFwdProj(gr_str, variant_num, phase_id, varargin)
\end{mCode}
varargin is the same as before, and \emph{variant\_num} is the twin variant present in you grain.

\end{itemize}
\end{itemize}
These two possibilities work on the segmented spots, not on the raw data. For the reconstruction step, type:
\begin{mCode}
>> gtReconstructGrainTwinCluster(grain_ids, phase_id)
\end{mCode}
where \emph{grain\_ids} is going to be \emph{[parent\_id, twin\_id]} for the first case (where the twins were indexed), while \emph{[parent\_id, parent\_id]} for the second case (where the twins were not indexed).


\section{6D Thresholding}

It is during this step that the volume of your grains will be segmented, in order to reconstruct their shape properly. Firstly, initialize the thresholding class:

\begin{mCode}
t=GtThreshold
\end{mCode}

You can now launch your volume segmentation:

\begin{mCode}
t.batchThreshold(phase_id, first_id, last_id)
\end{mCode}


\section{6D Assembling in case of clusters}

Prior to being able to assemble, you need to enable the assembling process. To do so, type:

\begin{mCode}
>> sample = GtSample.loadFromFile();
>> sample.phases{phase_id}.clusters(num_cluster).active = true;
>> sample.saveToFile();
\end{mCode}

You can check if \emph{num\_cluster} correspond to the cluster you want to assemble by having a look at the ids of the grains it contains:

\begin{mCode}
>> sample.phases{phase_id}.clusters(num_cluster)
\end{mCode}

For the assembling itself, go to the Visualization chapter (\autoref{sec:6Drec}).
