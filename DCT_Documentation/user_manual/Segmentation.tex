\chapter{Segmentation}
\label{ch:seg}

Segmentation is the processing step in which diffraction blobs are separated from the preprocessed images.  We use the term \emph{blob} to 
refer to a 3D entity with intensity extending in plane of the detector (u, v) and in the stack of images (omega).  These 3D regions are 
eventually summed in the stack direction to create a 2D diffraction spot. 

The function that we use to make the segmentation is \mCom+gtSegmentationGUI+.

\section{Double threshold segmentation}

The principle of the algorithm is to work in two phases. The first phase is seed segmentation. This works in 2D, one image at a time.
The image is thresholded with a simple single threshold. This identifies bright regions in the image. The single brightest pixel within 
each region is a seed. The position and intensities of these points are recorded in a database table. These seeds are points from which 
blobs can be grown.  Each blob is grown from a seed.  However, not every seed becomes a blob.

The second phase is to grow blobs from seeds.  The seeds are treated in descending order of intensity, so the brightest seeds are treated first.  
A region around the seed point is selected.  A threshold value is calculated from the seed intensity, and applied to the region.  
Those voxels above the threshold, and connected to the seed point, are selected.  If the thresholded voxels touch the edge of the region, 
the region is extended.  If the region is found to contain another, brighter seed point, or a seed point which already belongs to a grain, 
the process is abandoned.  If the segmented intensity is entirely contained within the region, without reaching the edges, the segmented voxels 
are assigned to a diffraction blob and written to the database.

The reason to work in this way is so that the threshold used can be adjusted as a function of the brightness of the diffraction spot.  
This is because the same grain may have diffraction spots that vary in intensity because of the structure factor of the reflection, or absorption 
in the sample, or the distribution of intensity in the direct beam.  We would like to use all of these as projections of the grain.  
Therefore, they should all be of similar size.  This implies that the threshold must vary, otherwise bright spots appear bigger than dim spots.

\section{Using the function}

Start the function \mCom|gtSegmentationGUI|, in the analysis directory of the dataset.  You will be asked some questions before the main GUI is opened.
\begin{itemize}
  \item Do you want to reset all the parameters for segmentation? Answer no, as you don't want to erase what you have just done during the set up.
    \item Recompile functions for OAR? You don't have to do that.
	\item Check intensity in the full images?  This samples the intensity in the direct beam region of every 20th image.  This should show you if the beam was 
	lost at any time during the scan.  Some variation from image to image in to be expected, due to the decaying current in the storage ring, 
	and the varying projected thickness of the sample.
	\item The function will check that all full images are present.  Because of the way the algorithm works, you should wait until all images have 
	been acquired before you start segmentation.
	\item You are asked if you want to subtract the median intensity from the images.  Normally, this is a good idea, because sometimes the 
	preprocessing leaves a fixed offset to the background intensity, and this can interfere if you want to threshold blobs close to the noise level.\\
\end{itemize}
The GUI will then open. Firstly, focus on the left side.
\begin{figure}[!h]
\centering
\includegraphics[scale=0.6]{photos/SegmentationGUI_left_side.png}
\caption{ROI and Bounding box selection.}
\label{fig:ROI&BB}
\end{figure}
\begin{itemize}
\item Choose a \acs{ROI} (Region of interest) by changing \emph{u start}, \emph{v start}, \emph{u end} and \emph{v end}. You can go from 1 to 2048, which is the number of pixels in each row and each column.
\item Select the number of images with \emph{image start} and \emph{image end}. Usually, taking 30 images is not bad.
\item The \emph{segmentation bounding box} defines a region around the direct beam.  Nothing within this region will be segmented.  
	This can be adjusted by clicking and dragging on the image.
\end{itemize}
After that, you can take care of the right part of the GUI. In essence, the idea is to get your grains to appear in each of the windows in the GUI, working from left to right. You can see the grains better by hitting the \emph{Jet} button in the \emph{Colour map} section, on the right of the GUI. Let's begin by the \textit{Full image data} window.
\begin{figure}[!h]
\centering
\includegraphics[scale=0.8]{photos/Full_image_data.png}
\caption{Full image data window.}
\label{fig:Full_image_data}
\end{figure}
\begin{itemize}
\item your only job here is to move the vertical sliders, in order to see all the grains.
\end{itemize}
Then, go on with the \emph{seed threshold} window.
\begin{figure}[!h]
\centering
\includegraphics[scale=0.6]{photos/Seed_threshold.png}
\caption{Seed threshold window.}
\label{fig:Seed_threshold}
\end{figure}

\begin{itemize}
	\item First is the \emph{seed threshold}.  This is the first threshold that is applied to the images to identify the seed points.
	\item Second is the \emph{seed minimum area}. A seed must have a certain number of pixels to be considered.  This is to remove spikes or hot pixels.
\end{itemize}
Now, move to the \emph{grow threshold} window.
\begin{figure}[!h]
\centering
\includegraphics[scale=0.6]{photos/Grow_threshold.png}
\caption{Grow threshold window.}
\label{fig:Grow_threshold}
\end{figure}

\begin{itemize}
	\item \emph{Thr\_grow\_high} and \emph{thr\_grow\_low} are upper and lower limits to the threshold value.  It is useful to set \emph{thr\_grow\_low} 
	somewhat above the noise floor to stop weak blobs from becoming too large.  \emph{Thr\_grow\_high} can be used to control the upper 
	limit of the threshold value.
	
	\item Note: used together, these can change the behavior from an adaptive threshold to a fixed threshold, marker-mask segmentation.
\end{itemize}
The last window concerns the 3D adaptive double threshold segmentation.
\begin{figure}[!h]
\centering
\includegraphics[scale=0.6]{photos/3D_adaptive.png}
\caption{3D adaptive double threshold segmentation window.}
\label{fig:3D_adaptive}
\end{figure}

	  \begin{itemize}
\item \emph{thr\_grow\_ratio} defines the blob's limits. The pixels with an intensity inferior to the seed intensity multiplied by this ratio are excluded.
	\item \emph{Blob min size} and \emph{max size}:  if a segmented blob is outside of these limits, it is assumed to be bad  
	either that the background noise, or intensity around the direct beam, or spot overlap, has led to the blob becoming too big.  
	Alternatively, that the blob is too small to be useful.  These should be set considering the grain sizes present in the sample. The parameters with which you can play are: (\textit{number of pixels along u, number of pixels along v, number of stacked images}).
		
\end{itemize}

Finally, you should be able to see even the smallest spots on every window. You can move the horizontal slider on the bottom of the GUI just to make sure that the spots are well segmented on other images. 

\begin{figure}[htb]
\centering
	\includegraphics[width=\textwidth]{plots/SegmentationThresholding.png}
	\label{fig:segthres}
	\caption{Illustration of the most important thresholding parameters for Segmentation.}
\end{figure}

Once you have parameters with which you are happy, you can either save these parameters to the parameters.mat file and quit the 
function, or you can save the parameters and launch the segmentation to OAR. You can still check the progress with the \sCom+dct_launch.py batch gui+ command.

This may take one hour or two. If the jobs terminate quickly, that means that the segmentation parameters were not good enough, so you must improve them.

