\NeedsTeXFormat{LaTeX2e}
 
\ProvidesClass{DCTDoc}[2013/06/27 v1.0 DCT documentation class]

% Passes and class options to the underlying article class
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{scrreprt}}
\ProcessOptions

\LoadClass[a4paper,12pt,twoside,openany,final,
           titlepage,
           pagesize,
           headsepline,
           nofootsepline,
           %onelinecaption,
           %tablecaptionabove,
           %liststotoc,
           %idxtotoc,
           %bibtotoc,
           %tocindent,
           %nochapterprefix,
           version=first,
           captions=oneline,
           captions=tableheading,
           listof=totoc,
           index=totoc,
           bibliography=totoc,
           toc=graduated,
           chapterprefix=false,
           appendixprefix]{scrreprt}[2009/07/24]

% \@ifclasslater{scrreprt}{2009/07/24}{
% % With koma-script >= 3.0
% \LoadClass[a4paper,12pt,twoside,openright,final]{scrreprt}[2009/07/24]
% %\LoadClass[a4paper,12pt,twoside,openright,final]{scrreprt}
% 
% \KOMAoption{titlepage}{true}
% \KOMAoptions{pagesize}
% %\KOMAoption{pagesize}{true}
% \KOMAoption{headsepline}{true}
% \KOMAoption{footsepline}{false}
% \KOMAoption{captions}{oneline,tableabove}
% \KOMAoption{toc}{listof,bibliography,indent,index}
% %\KOMAoptions{DIV13}{true}             % type area
% %\KOMAoptions{bigheadings}{true}
% %\KOMAoption{pointlessnumbers}{true}
% \KOMAoption{chapterprefix}{false}
% \KOMAoption{appendixprefix}{true}
% %\KOMAoption{BCOR}{15mm}               % binding correction
% %\KOMAoption{cleardoublepage}{empty}
% %\KOMAoption{parskip}{}
% }{
% \LoadClass[a4paper,12pt,twoside,openright,final,
%            titlepage,
%            pagesize,
%            headsepline,
%            footsepline,
%            onelinecaption,
%            tablecaptionabove,
%            liststotoc,
%            idxtotoc,
%            bibtotoc,
%            tocindent,
%            chapterprefix=false,
%            appendixprefix]{scrreprt}
% }
% 
% %\KOMAoptions{DIV13=true}             % type area
% %\KOMAoptions{bigheadings=true}
% %\KOMAoptions{pointlessnumbers==true}
% %\KOMAoptions{BCOR=15mm}               % binding correction
% %\KOMAoptions{cleardoublepage=empty}
% %\KOMAoptions{parskip=}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                   (1) PACKAGES                       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\RequirePackage[latin1]{inputenc} % use applemac if you are on an apple laptop, ansinew for Windows, but latin1 should be fine for most systems
\RequirePackage[T1]{fontenc}
\RequirePackage{lmodern}
\RequirePackage{textcomp}
\RequirePackage[pdftex]{graphicx}
\RequirePackage[pdftex]{color}
\RequirePackage{a4}
\RequirePackage{acronym} % texlive-latex-extra should be installed to compile with acronym style
\RequirePackage{makeidx}
\RequirePackage[numbers,sort&compress]{natbib} % for bibliography
\RequirePackage{appendix}

\RequirePackage{subfigure} % almost OBSOLETE (but not for tetex-3.0) -> switch to subfig or subcaption

\RequirePackage{amsmath,amssymb,amsbsy} % match package, eg align statt eqnarray
\RequirePackage{amsfonts}     % Set of miscellaneous TeX fonts that augment the standard Computer Modern set normally distributed with TeX
\RequirePackage{xspace}       % Extra spaces and lengths
\RequirePackage{enumerate}    % Enhanced enumerations
\RequirePackage{multirow}     % multi-row cells in the tables
\RequirePackage{multicol}     % multi-column layout
\RequirePackage{longtable}    % Long tables over multiple pages
\RequirePackage[cm]{fullpage} % Shorter margins, wider text layout
\RequirePackage{fancyhdr}     % For headers and footers
\RequirePackage{listings}     % To insert code
%\RequirePackage[2007/09/29]{kvsetkeys}
%\RequirePackage{svn-multi}  % To interact with svn
\RequirePackage[pdftex, colorlinks=true, pdfstartview=FitV, linkcolor=red, citecolor=green, urlcolor=blue]{hyperref} % hyperlinks
\RequirePackage[all]{hypcap}
\RequirePackage[english]{babel}
\RequirePackage{caption}     % Latest caption package
\RequirePackage{float}
%\RequirePackage{subcaption}  % Too recent for teTeX-3.0

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                   (2) SETTINGS                       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% If you put this into your preamble, floats on pages of their own will appear at the top of the page.
% If it's too high just take a different value than 0pt.  
% fig or tab alone on page no longer in center
\makeatletter
\setlength{\@fptop}{0pt}
\makeatother

% Use \index{word} to tag special expressions \printindex will then give you word-page-index
\makeindex

\setcounter{tocdepth}{1}
\setcounter{secnumdepth}{1}
\uchyph=0 % stop hypenation of upper case words
% Distance to figures in floatin environment
%\setlength{\intextsep}{3mm plus2mm minus2mm}
%\setlength{\abovecaptionskip}{0mm}
%\setlength{\belowcaptionskip}{0mm}

% Fix header layout and space
\pagestyle{headings}
\headsep=20pt
\headheight=14.2pt

% Remove all indentation
\setlength{\parindent}{0pt}

% autoref names, you can change here the default behaviour link names
\renewcommand{\figureautorefname}{fig.}
\newcommand{\subfigureautorefname}{fig.}
\renewcommand{\tableautorefname}{tab.}
\renewcommand{\chapterautorefname}{chap.}
\renewcommand{\sectionautorefname}{sect.}
\renewcommand{\subsectionautorefname}{subsect.}
\renewcommand{\equationautorefname}{eq.}

% Cosmetics
\renewcommand{\setthesection}{\Alph{section}}
\renewcommand{\labelitemi}{$\bullet$}
\renewcommand{\labelitemii}{$\diamond$}
\renewcommand{\labelitemiii}{$\ast$}
\renewcommand{\labelitemiv}{$\cdot$}

% Colors
\definecolor{codeBG}{gray}{0.9}
\definecolor{outBG}{gray}{0.75}

% Code listing settings
\lstset{
basicstyle=\ttfamily,
breaklines=true
}
\lstdefinestyle{intext}{
basicstyle=\ttfamily
}
\lstdefinestyle{outoftext}{
basicstyle=\ttfamily\small,
backgroundcolor=\color{codeBG},
prebreak=...,
postbreak=...
}
\lstdefinestyle{outoftext2}{
basicstyle=\ttfamily\small,
backgroundcolor=\color{outBG},
breakautoindent=false,
breakindent=0pt,
prebreak=,
postbreak=
}
\lstloadlanguages{sh,bash,Matlab,Python,Octave,C,C++}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                  (3) COMMANDS                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% To get and print git revision info
\AtBeginDocument{\newcommand{\RevisionInfo}{\protect\input"|./scripts/getGitRevisionInfo.sh"}}

% define here useful and often used commands to shorten writing unless you define it in acronyms
\newcommand{\dct}{Diffraction Contrast Tomography}

%%%%%%%%%%%%%%%%%%%%%%%%%%
% LaTeX commands to print:
%%%%%%%%%%%%%%%%%%%%%%%%%%

% files, directories, pathes in text
\def\file{\lstinline[language=,basicstyle=\sl]}

% Matlab and shell commands in text
\def\mCom{\lstinline[language=Matlab,basicstyle=\tt]}
\def\sCom{\lstinline[language=sh,basicstyle=\tt]}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Environments to print out-of-text multiline code:
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Matlab code
\lstnewenvironment{mCode}
{\lstset{language=Matlab,style=outoftext}}
{}

% C code
\lstnewenvironment{cCode}
{\lstset{language=C,style=outoftext}}
{}

% C++ code
\lstnewenvironment{cppCode}
{\lstset{language=C++,style=outoftext}}
{}

% sh code
\lstnewenvironment{shCode}
{\lstset{language=sh,style=outoftext}}
{}

% any code
\lstnewenvironment{Code}
{\lstset{language=,style=outoftext}}
{}

% output
\lstnewenvironment{mOut}
{\lstset{language=,style=outoftext2}}
{}

\endinput
