##############################################################################
##        Guidelines for building LaTeX sources for DCT Documentation       ##
##############################################################################

To run the compilation, type:

$ make user_manual        # user documentation 

$ make main               # devel documentation for the graintracking team

$ make help               # to display the other make commands

##############################################################################
##  Guidelines for creating or editing LaTeX sources for DCT Documentation  ##
##############################################################################

1) Never put \\ at the end of a paragraph, it is useless. Just type:
blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah
blah blah blah blah blah blah blah blah blah blah blah blah

blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah 
blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah
blah blah blah blah


2) When referring to a shell, OAR or matlab command, please use:
\mCom+...+  % for Matlab
\sCom+...+  % for shell


3) When referring to a file, directory or path, please use:
\file|...|


4) To insert an out-of-text block for matlab/OAR/shell commands, please use:
\begin{mCode}
...
...
...
\end{mCode}

Note:
others environments are also created:
- cCode for C
- cppCode for C++
- shCode for shell
...


5) Another environment is provided for Matlab output:
\begin{mOut}
ans = 1   2   3   4
\end{mOut}
