# With full hash number
#git log -1 --date=short --format=format:'Revision %H on %ad by %an'
# With short hash number
git log -1 --date=short --format=format:'Revision %h on %ad by %an'
