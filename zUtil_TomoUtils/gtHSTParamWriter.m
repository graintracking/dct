function gtHSTParamWriter(par)
% GTHSTPARAMWRITER  Writes a HST parameter file from HST parameter structure.
%     -------------------------------------------------------------------------
%     gtHSTParamWriter(par)
%
%     The output file name is defined inside the given HST parameter structure.
%
%     INPUT:
%       par = <struct>  HST parameter structure e.g. got from gtHSTParamReader
%
%     Version 002 14-03-2012 by YGuilhem, yoann.guilhem@esrf.fr
%       Import par_write.m in DCT with some modifications
%
%     Version 001 01-10-2007 by WLudwig, wolfgang.ludwig@esrf.fr
%       par_write.m

fid = fopen(par.parfilename, 'w');

if fid==-1
    disp('Problems writing hst-parameterfile # Check write permissions');
    return
end

not_available = 'N.A.';

fprintf(fid, '# HST_SLAVE PARAMETER FILE\n\n');
fprintf(fid, '# Parameters defining the projection file series\n');

if isfield(par, 'prefix')
    % This first part of the code will use fieldname conventions used in stiching...
    fprintf(fid, 'FILE_PREFIX = %s\n', par.prefix);
    fprintf(fid, 'NUM_FIRST_IMAGE = %s # No. of first projection file\n', par.first);
    fprintf(fid, 'NUM_LAST_IMAGE = %s # No. of last projection file\n', par.last);
    fprintf(fid, 'NUMBER_LENGTH_VARIES = %s\n', par.number_length_varies);
    fprintf(fid, 'LENGTH_OF_NUMERICAL_PART = %s # No. of characters\n', par.length_of_numerical_part);
    fprintf(fid, 'FILE_POSTFIX = %s\n', par.file_postfix);
    fprintf(fid, 'FILE_INTERVAL = %s # Interval between input files\n', num2str(par.file_interval));

    fprintf(fid, '\n# Parameters defining the projection file format\n');
    fprintf(fid, 'NUM_IMAGE_1 = %s # Number of pixels horizontally\n', par.num_image_1);
    fprintf(fid, 'NUM_IMAGE_2 = %s # Number of pixels vertically\n', par.num_image_2);
    fprintf(fid, 'IMAGE_PIXEL_SIZE_1 = %s # Pixel size horizontally (microns)\n', par.image_pixel_size_1);
    fprintf(fid, 'IMAGE_PIXEL_SIZE_2 = %s # Pixel size vertically\n', par.image_pixel_size_2);

    fprintf(fid, '\n# Parameters defining background treatment\n');

    if par.correct
        fprintf(fid, 'SUBTRACT_BACKGROUND = %s # Subtract background from data\n', par.subtract_background);
        fprintf(fid, 'BACKGROUND_FILE = %s\n', par.background_file);
    else
        fprintf(fid, 'SUBTRACT_BACKGROUND = %s # No background subtraction\n', par.subtract_background);
        fprintf(fid, 'BACKGROUND_FILE = %s\n', not_available);
    end

    fprintf(fid, '\n# Parameters defining flat-field treatment\n');
    if par.correct
        fprintf(fid, 'CORRECT_FLATFIELD = %s # Divide by flat-field image\n', par.correct_flatfield);
        fprintf(fid, 'FLATFIELD_CHANGING = %s # Series of flat-field files\n', par.flatfield_changing);
        fprintf(fid, 'FLATFIELD_FILE = %s\n', par.flatfield_file);
        fprintf(fid, 'FF_PREFIX = %s\n', par.ff_prefix);
        fprintf(fid, 'FF_NUM_FIRST_IMAGE = %s # No. of first flat-field file\n', par.ff_num_first_image);
        fprintf(fid, 'FF_NUM_LAST_IMAGE = %s # No. of last flat-field file\n', par.ff_num_last_image);
        fprintf(fid, 'FF_NUMBER_LENGTH_VARIES = %s\n', par.number_length_varies);
        fprintf(fid, 'FF_LENGTH_OF_NUMERICAL_PART = %s # No. of characters\n', par.length_of_numerical_part);
        fprintf(fid, 'FF_POSTFIX = %s\n', par.file_postfix);
        fprintf(fid, 'FF_FILE_INTERVAL = %s # Interval between flat-field files\n', par.ff_file_interval);
    else
        fprintf(fid, 'CORRECT_FLATFIELD = %s # No flat-field correction\n', par.correct_flatfield);
        fprintf(fid, 'FLATFIELD_CHANGING = %s\n', not_available);
        fprintf(fid, 'FLATFIELD_FILE = %s\n', not_available);
        fprintf(fid, 'FF_PREFIX = %s\n', not_available);
        fprintf(fid, 'FF_NUM_FIRST_IMAGE = %s\n', not_available);
        fprintf(fid, 'FF_NUM_LAST_IMAGE = %s\n', not_available);
        fprintf(fid, 'FF_NUMBER_LENGTH_VARIES = %s\n', not_available);
        fprintf(fid, 'FF_LENGTH_OF_NUMERICAL_PART = %s\n', not_available);
        fprintf(fid, 'FF_POSTFIX = %s\n', not_available);
        fprintf(fid, 'FF_FILE_INTERVAL = %s\n', not_available);
    end

    fprintf(fid, '\nTAKE_LOGARITHM = %s # Take log of projection values\n', par.take_logarithm);

    fprintf(fid, '\n# Parameters defining experiment\n');
    fprintf(fid, 'ANGLE_BETWEEN_PROJECTIONS = %s # Increment angle in degrees\n', par.angle_between_projections);
    fprintf(fid, 'ROTATION_VERTICAL = %s\n', par.rotation_vertical);
    fprintf(fid, 'ROTATION_AXIS_POSITION = %s # Position in pixels\n', par.rotation_axis_position);

    fprintf(fid, '\n# Parameters defining reconstruction\n');
    fprintf(fid, 'OUTPUT_SINOGRAMS = %s # Output sinograms to files or not\n', par.output_sino);
    fprintf(fid, 'OUTPUT_RECONSTRUCTION = %s # Reconstruct and save or not\n', par.output_rec);
    fprintf(fid, 'START_VOXEL_1 =   %4s # X-start of reconstruction volume\n', par.start_voxel_1);
    fprintf(fid, 'START_VOXEL_2 =   %4s # Y-start of reconstruction volume\n', par.start_voxel_2);
    fprintf(fid, 'START_VOXEL_3 =   %4s # Z-start of reconstruction volume\n', par.start_voxel_3);
    fprintf(fid, 'END_VOXEL_1 =   %4s # X-end of reconstruction volume\n', par.end_voxel_1);
    fprintf(fid, 'END_VOXEL_2 =   %4s # Y-end of reconstruction volume\n', par.end_voxel_2);
    fprintf(fid, 'END_VOXEL_3 =   %4s # Z-end of reconstruction volume\n', par.end_voxel_3);
    fprintf(fid, 'OVERSAMPLING_FACTOR = %s # 0 = Linear, 1 = Nearest pixel\n', par.oversampling_factor);
    fprintf(fid, 'ANGLE_OFFSET = %s # Reconstruction rotation offset angle in degrees\n', par.angle_offset);
    fprintf(fid, 'CACHE_KILOBYTES = %s # Size of processor cache (L2) per processor (KBytes)\n', par.cache_kilobytes);
    fprintf(fid, 'SINOGRAM_MEGABYTES = %s # Maximum size of sinogram storage (megabytes)\n', par.sino_megabytes);

    % Adding extra features for PyHSTmpi
    fprintf(fid, '\n# Parameters extra features PyHST\n');
    fprintf(fid, 'DO_CCD_FILTER = %s # CCD filter (spikes)\n', par.correct_spikes);
    fprintf(fid, 'CCD_FILTER = "%s"\n','CCD_Filter');
    fprintf(fid, 'CCD_FILTER_PARA = {"threshold": %s }\n', par.correct_spikes_threshold);
    fprintf(fid, 'DO_SINO_FILTER = %s # Sinogram filter (rings)\n', par.correct_rings);
    fprintf(fid, 'SINO_FILTER = "%s"\n','SINO_Filter');
    fprintf(fid, 'ar = Numeric.ones(%s,''f'')\n', par.num_image_1);
    fprintf(fid, 'ar[0]=0.0\n');

    if (par.correct_rings_nb > 0)
        fprintf(fid, 'ar[2:%d]=0.0\n',2*(par.correct_rings_nb+1));
    end

    fprintf(fid, 'SINO_FILTER_PARA = {"FILTER": ar }\n');
    fprintf(fid, 'DO_AXIS_CORRECTION = %s # Axis correction\n', par.correct_axis);
    fprintf(fid, 'AXIS_CORRECTION_FILE = %s\n', par.correct_axis_file);
    fprintf(fid, 'OPTIONS= { ''padding'':''%s'' , ''axis_to_the_center'':''%s''} # Padding and position axis\n', par.padding, par.axis_to_the_center);
    if isfield(par, 'zeroclipvalue')
        fprintf(fid, 'ZEROCLIPVALUE = %s # Minimum value of radiographs after flat / before log\n', par.zeroclipvalue);
    end

    fprintf(fid, '\n# Parameters defining output file / format\n');
    fprintf(fid, 'OUTPUT_FILE = %s\n', par.output_file);

    fprintf(fid, '\n# Reconstruction program options\n');
    fprintf(fid, 'DISPLAY_GRAPHICS = %s # No images\n', par.graph_out);

else
    % This part of the code will work in conjunction with gtHSTParamReader (different fieldnames)
    fprintf(fid, 'FILE_PREFIX = %s\n', par.file_prefix);
    fprintf(fid, 'NUM_FIRST_IMAGE = %d # No. of first projection file\n', par.num_first_image);
    fprintf(fid, 'NUM_LAST_IMAGE = %d # No. of last projection file\n', par.num_last_image);
    fprintf(fid, 'NUMBER_LENGTH_VARIES = %s\n', par.number_length_varies);
    fprintf(fid, 'LENGTH_OF_NUMERICAL_PART = %d # No. of characters\n', par.length_of_numerical_part);
    fprintf(fid, 'FILE_POSTFIX = %s\n', par.file_postfix);
    fprintf(fid, 'FILE_INTERVAL = %d # Interval between input files\n', par.file_interval);

    fprintf(fid, '\n# Parameters defining the projection file format\n');
    fprintf(fid, 'NUM_IMAGE_1 = %d # Number of pixels horizontally\n', par.num_image_1);
    fprintf(fid, 'NUM_IMAGE_2 = %d # Number of pixels vertically\n', par.num_image_2);
    fprintf(fid, 'IMAGE_PIXEL_SIZE_1 = %f # Pixel size horizontally (microns)\n', par.image_pixel_size_1);
    fprintf(fid, 'IMAGE_PIXEL_SIZE_2 = %f # Pixel size vertically\n', par.image_pixel_size_2);

    fprintf(fid, '\n# Parameters defining background treatment\n');

    if par.subtract_background
        fprintf(fid, 'SUBTRACT_BACKGROUND = %s # Subtract background from data\n', par.subtract_background);
        fprintf(fid, 'BACKGROUND_FILE = %s\n', par.background_file);
    else
        fprintf(fid, 'SUBTRACT_BACKGROUND = %s # No background subtraction\n', par.subtract_background);
        fprintf(fid, 'BACKGROUND_FILE = %s\n', not_available);
    end

    fprintf(fid, '\n# Parameters defining flat-field treatment\n');
    if par.correct_flatfield
        fprintf(fid, 'CORRECT_FLATFIELD = %s # Divide by flat-field image\n', par.correct_flatfield);
        fprintf(fid, 'FLATFIELD_CHANGING = %s # Series of flat-field files\n', par.flatfield_changing);
        fprintf(fid, 'FLATFIELD_FILE = %s\n', par.flatfield_file);
        fprintf(fid, 'FF_PREFIX = %s\n', par.ff_prefix);
        fprintf(fid, 'FF_NUM_FIRST_IMAGE = %d # No. of first flat-field file\n', par.ff_num_first_image);
        fprintf(fid, 'FF_NUM_LAST_IMAGE = %d # No. of last flat-field file\n', par.ff_num_last_image);
        fprintf(fid, 'FF_NUMBER_LENGTH_VARIES = %s\n', par.ff_number_length_varies);
        fprintf(fid, 'FF_LENGTH_OF_NUMERICAL_PART = %d # No. of characters\n', par.ff_length_of_numerical_part);
        fprintf(fid, 'FF_POSTFIX = %s\n', par.ff_postfix);
        fprintf(fid, 'FF_FILE_INTERVAL = %d # Interval between flat-field files\n', par.ff_file_interval);
    else
        fprintf(fid, 'CORRECT_FLATFIELD = %s # No flat-field correction\n', par.correct_flatfield);
        fprintf(fid, 'FLATFIELD_CHANGING = %s\n', not_available);
        fprintf(fid, 'FLATFIELD_FILE = %s\n', not_available);
        fprintf(fid, 'FF_PREFIX = %s\n', not_available);
        fprintf(fid, 'FF_NUM_FIRST_IMAGE = %s\n', not_available);
        fprintf(fid, 'FF_NUM_LAST_IMAGE = %s\n', not_available);
        fprintf(fid, 'FF_NUMBER_LENGTH_VARIES = %s\n', not_available);
        fprintf(fid, 'FF_LENGTH_OF_NUMERICAL_PART = %s\n', not_available);
        fprintf(fid, 'FF_POSTFIX = %s\n', not_available);
        fprintf(fid, 'FF_FILE_INTERVAL = %s\n', not_available);
    end

    fprintf(fid, '\nTAKE_LOGARITHM = %s # Take log of projection values\n', par.take_logarithm);

    fprintf(fid, '\n# Parameters defining experiment\n');
    fprintf(fid, 'ANGLE_BETWEEN_PROJECTIONS = %f # Increment angle in degrees\n', par.angle_between_projections);
    fprintf(fid, 'ROTATION_VERTICAL = %s\n', par.rotation_vertical);
    fprintf(fid, 'ROTATION_AXIS_POSITION = %f # Position in pixels\n', par.rotation_axis_position);

    fprintf(fid, '\n# Parameters defining reconstruction\n');
    fprintf(fid, 'OUTPUT_SINOGRAMS = %s # Output sinograms to files or not\n', par.output_sinograms);
    fprintf(fid, 'OUTPUT_RECONSTRUCTION = %s # Reconstruct and save or not\n', par.output_reconstruction);
    fprintf(fid, 'START_VOXEL_1 =   %4d # X-start of reconstruction volume\n', par.start_voxel_1);
    fprintf(fid, 'START_VOXEL_2 =   %4d # Y-start of reconstruction volume\n', par.start_voxel_2);
    fprintf(fid, 'START_VOXEL_3 =   %4d # Z-start of reconstruction volume\n', par.start_voxel_3);
    fprintf(fid, 'END_VOXEL_1 =   %4d # X-end of reconstruction volume\n', par.end_voxel_1);
    fprintf(fid, 'END_VOXEL_2 =   %4d # Y-end of reconstruction volume\n', par.end_voxel_2);
    fprintf(fid, 'END_VOXEL_3 =   %4d # Z-end of reconstruction volume\n', par.end_voxel_3);
    fprintf(fid, 'OVERSAMPLING_FACTOR = %d # 0 = Linear, 1 = Nearest pixel\n', par.oversampling_factor);
    fprintf(fid, 'ANGLE_OFFSET = %f # Reconstruction rotation offset angle in degrees\n', par.angle_offset);
    fprintf(fid, 'CACHE_KILOBYTES = %d # Size of processor cache (L2) per processor (KBytes)\n', par.cache_kilobytes);
    fprintf(fid, 'SINOGRAM_MEGABYTES = %d # Maximum size of sinogram storage (megabytes)\n', par.sinogram_megabytes);

    % Adding extra features for PyHSTmpi
    fprintf(fid, '\n# Parameters extra features PyHST\n');
    fprintf(fid, 'DO_CCD_FILTER = %s # CCD filter (spikes)\n', par.do_ccd_filter);
    fprintf(fid, 'CCD_FILTER = "%s"\n','CCD_Filter');
    fprintf(fid, 'CCD_FILTER_PARA = {"threshold": %f }\n', par.ccd_filter_para);
    fprintf(fid, 'DO_SINO_FILTER = %s # Sinogram filter (rings)\n', par.do_sino_filter);
    fprintf(fid, 'SINO_FILTER = "%s"\n','SINO_Filter');
    fprintf(fid, 'ar = Numeric.ones(%d,''f'')\n', par.num_image_1);
    fprintf(fid, 'ar[0]=0.0\n');

    if (par.correct_rings_nb > 0)
    	fprintf(fid, 'ar[2:%d]=0.0\n',2*(par.correct_rings_nb+1));
    end

    fprintf(fid, 'SINO_FILTER_PARA = {"FILTER": ar }\n');
    fprintf(fid, 'DO_AXIS_CORRECTION = %s # Axis correction\n', par.do_axis_correction);
    fprintf(fid, 'AXIS_CORRECTION_FILE = %s\n', par.axis_correction_file);

    if isfield(par, 'options_avoidhalftomo')
        fprintf(fid, 'OPTIONS= { ''padding'':''%s'' , ''axis_to_the_center'':''%s'' , ''avoidhalftomo'':''%s''} # Padding and position axis\n', par.options_padding, par.options_axis, par.options_avoidhalftomo);
    else
        fprintf(fid, 'OPTIONS= { ''padding'':''%s'' , ''axis_to_the_center'':''%s''} # Padding and position axis\n', par.options_padding, par.options_axis);
    end

    if isfield(par, 'zeroclipvalue')
        fprintf(fid, 'ZEROCLIPVALUE = %s # Minimum value of radiographs after flat / before log\n', par.zeroclipvalue);
    end

    fprintf(fid, '\n# Parameters defining output file / format\n');
    fprintf(fid, 'OUTPUT_FILE = %s\n', par.output_file);

    fprintf(fid, '\n# Reconstruction program options\n');
    fprintf(fid, 'DISPLAY_GRAPHICS = %s # No images\n', par.display_graphics);
    
    if isfield(par, 'doubleffcorrection')
        fprintf(fid, 'DOUBLEFFCORRECTION = %s # correction by filtered mean of the projections\n', par.doubleffcorrection);
    end

    % Adding binning (Andy King, 13/1/2011)
    if isfield(par, 'binning') && par.binning~=1
        fprintf(fid, '\nBINNING = %d\n', par.binning);
    end
end

% Close file
fclose(fid);

% Set file permissions
try
  fileattrib(par.parfilename,'+w','u g o');
catch
end
    
end % end of function
