classdef GtOrientationVolView < GtVolView
    properties
        int_vol = [];
        dmvol = [];
        mask = [];

        values = [];

        flat = true;
        mode = 'ipf';
    end

    methods (Access = public)
        function self = GtOrientationVolView(gr, gr_rec, varargin)
            dmvol = gr_rec.ODF6D.voxels_avg_R_vectors;
            int_vol = gr_rec.ODF6D.intensity;

            conf = struct('flat', true, 'threshold', -1, 'mode', 'igm', ...
                'phase_id', 1, 'plane_normal', [0 0 1]);
            [conf, other_pars] = parse_pv_pairs(conf, varargin);

            % bringing average to 0, so to avoid things that were 0 in the
            % first place
%             mask = intensities > (max(intensities(:)) / 100);
            if (conf.threshold >= 0)
                mask = int_vol > conf.threshold;
            elseif (isfield(gr_rec, 'SEG'))
                mask = zeros(size(int_vol), 'uint8');
                shift = gr_rec.SEG.segbb(1:3) - gr_rec.ODF6D.shift;
                mask = gtPlaceSubVolume(mask, uint8(gr_rec.SEG.seg), shift);
                mask = logical(mask);
            else
                mask = true(size(int_vol));
            end

            gvdm = reshape(dmvol, [], 3);

            switch (conf.mode)
                case 'ipf'
                    [cmap, ~, ~] = gtIPFCmap(conf.phase_id, conf.plane_normal, ...
                        'r_vectors', gvdm(mask(:), :), 'background', false);

                    values = zeros(size(gvdm), 'like', dmvol);
                    values(mask, :) = cmap;
                    values = reshape(values, size(dmvol));

                    init_vals = int_vol;
                case 'igm'
                    values = gtDefComputeIntraGranularMisorientation(...
                        dmvol, int_vol .* mask, 'R_vector', gr.R_vector);

                    init_vals = values;
                    other_pars(end+1:end+2) = {'cmap', jet};
                case 'kam'
                    values = gtDefComputeKernelAverageMisorientation(...
                        dmvol, int_vol .* mask);

                    init_vals = values;
                    other_pars(end+1:end+2) = {'cmap', jet};
                case 'int'
                    values = int_vol;
                    init_vals = int_vol;
                    other_pars(end+1:end+2) = {'overlay', mask};
            end

            self = self@GtVolView(init_vals, other_pars{:});

            self.flat = conf.flat;
            self.mode = lower(conf.mode);

            self.values = values;
            self.mask = mask;

            switch (self.mode)
                case 'ipf'
                    if (~self.flat)
                        self.conf.h_im = mesh('Parent', self.conf.h_ax, [0 0; 0 0], ...
                            'EdgeColor', [0.2 0.2 0.2], 'FaceColor', 'interp', ...
                            'EdgeAlpha', 0.5);
                    end
                case {'igm', 'kam', 'int'}
                    self.conf.h_cb = colorbar(self.conf.h_ax, 'Parent', self.conf.ax_boxes);

                    uicontrol('Parent', self.conf.ax_boxes, ...
                        'Enable', 'off', 'Visible', 'off', 'Tag', 'spacer');

                    set(self.conf.ax_boxes, 'Sizes', [-1, 20, 40]);

                    if (ismember(self.mode, {'igm', 'kam'}))
                        hold(self.conf.h_ax, 'on');
                        self.conf.h_mask = imagesc(0, 'Parent', self.conf.h_ax, 'AlphaDataMapping', 'scaled');
                        hold(self.conf.h_ax, 'off');
                    end
            end

            self.int_vol = int_vol;
            self.dmvol = dmvol;
        end
    end

    methods (Access = protected)
        function slice = loadSlice(self, vol)
        % GTVOLVIEW/LOADSLICE Loads a slice from the volume (given the plane and
        % slice index in obj.conf.*) and permutes it to be visualised correctly
        % by matlab, which always flips X and Y dimensions in visualisation.

            if (~isempty(self.values))
                switch(lower(self.mode))
                    case 'ipf'
                        slice(:, :, 1) = self.loadSlice@GtVolView(self.values(:, :, :, 1));
                        slice(:, :, 2) = self.loadSlice@GtVolView(self.values(:, :, :, 2));
                        slice(:, :, 3) = self.loadSlice@GtVolView(self.values(:, :, :, 3));
                    case {'igm', 'kam'}
                        slice(:, :, 1) = self.loadSlice@GtVolView(self.values);
                        slice(:, :, 2) = self.loadSlice@GtVolView(self.mask);
                    case 'int'
                        slice = self.loadSlice@GtVolView(vol);
                end
            else
                slice = repmat(self.loadSlice@GtVolView(vol), [1, 1, 3]);
                slice = zeros(size(slice), 'like', slice);
            end
        end

        function setVisualisedData(self, image, vol, is_overlay)
        % GTVOLVIEW/SETVISUALISEDDATA Loads a slice and then operates
        % transformations on them, to adapt to the requested visualization needs

            slice = self.loadSlice(vol);
            slice = double(slice);

            if (is_overlay && ~isa(vol,'integer'))
                sliceMin = min(slice(:));
                sliceMax = max(slice(:));
                sliceRange = sliceMax - sliceMin;
                slice = repmat(slice-sliceMin, [1,1,3])/sliceRange;
            end

            switch(lower(self.mode))
                case {'ipf', 'int'}
                    set(image, 'CData', slice);
                case {'igm', 'kam'}
                    set(image, 'CData', slice(:, :, 1));
                    set(self.conf.h_mask, 'CData', repmat(slice(:, :, 2), [1, 1, 3]), ...
                        'AlphaDataMapping', 'scaled', 'AlphaData', ~slice(:, :, 2))
            end
        end
    end
end

%         function slice = loadSlice(self, vol)
%         % GTVOLVIEW/LOADSLICE Loads a slice from the volume (given the plane and
%         % slice index in obj.conf.*) and permutes it to be visualised correctly
%         % by matlab, which always flips X and Y dimensions in visualisation.
% 
% %             slice(:, :, 4) = self.loadSlice@GtVolView(vol);
% %             if (self.smooth)
% %                 slice(:, :, 3:4) = smooth3(slice(:, :, 3:4), 'box', [5 5 1]);
% %             end
% % 
% %             if (~isempty(self.dmvol))
% %                 slice(:, :, 1) = self.loadSlice@GtVolView(self.dmvol(:, :, :, 1));
% %                 slice(:, :, 2) = self.loadSlice@GtVolView(self.dmvol(:, :, :, 2));
% %                 slice(:, :, 3) = self.loadSlice@GtVolView(self.dmvol(:, :, :, 3));
% %             end
% 
%             if (~isempty(self.colors))
%                 slice(:, :, 1) = self.loadSlice@GtVolView(self.colors(:, :, :, 1));
%                 slice(:, :, 2) = self.loadSlice@GtVolView(self.colors(:, :, :, 2));
%                 slice(:, :, 3) = self.loadSlice@GtVolView(self.colors(:, :, :, 3));
%             else
%                 slice = repmat(self.loadSlice@GtVolView(vol), [1, 1, 3]);
%             end
%         end
% 
%         function setVisualisedData(self, image, vol, is_overlay)
%         % GTVOLVIEW/SETVISUALISEDDATA Loads a slice and then operates
%         % transformations on them, to adapt to the requested visualization needs
% 
%             slice = self.loadSlice(vol);
%             slice = double(slice);
% 
% %             if (is_overlay && ~isa(vol,'integer'))
% %                 sliceMin = min(slice(:));
% %                 sliceMax = max(slice(:));
% %                 sliceRange = sliceMax - sliceMin;
% %                 slice = repmat(slice-sliceMin, [1,1,3])/sliceRange;
% %             end
% 
% %             avg_R_vecs = slice(:, :, 1:3);
% %             avg_ints = slice(:, :, 4);
% % 
% %             % Rescaling
% %             avg_ints = avg_ints / max(self.conf.vol(:)) ...
% %                 * (size(avg_ints, 1) + size(avg_ints, 2)) / 6;
% 
%             set(image, 'CData', slice);
% %             set(image, 'ZData', avg_ints);
%         end

