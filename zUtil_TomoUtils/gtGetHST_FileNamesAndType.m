function [fnames, fpath, datatype] = gtGetHST_FileNamesAndType(full_fname)
% GTGETHST_FILENAMESANDTYPE  Gets all the names that match the given filename,
%                            and the HST datatype.
%     -------------------------------------------------------------------------
%     [fnames, fpath, datatype] = gtGetHST_FileNamesAndType(full_fname)
%
%     INPUT:
%       full_fame  = <string> path to the input file
%
%     OUTPUT:
%       fnames     = <struct> files related to the volume
%       fpath      = <string> path to the related files
%       datatype   = <string> detected data type 
%
%     Version 002 02-03-2012 by NVigano, nicola.vigano@esrf.fr
%      Enables file extension identification in gtHSTVolReader macro
%
%     Version 001 10-02-2012 by NVigano, nicola.vigano@esrf.fr
%       Imported from pfGetHSTFilenames.m (id19)

    errorID = 'HST:wrong_file_name';

    [fpath, filename, fext] = fileparts(full_fname);

    % check file extension
    if ~any(strcmp(fext, {'.xml','.raw','.vol'}))
        % No valid extension (vol,raw,xml) supplied - append a pseudo extension
        full_fname = [full_fname '.'];
        [fpath, filename, fext] = fileparts(full_fname);
    end

    % if NO path supplied
    if isempty(fpath)
        fpath = pwd;
    end

    switch (fext)
        case '.xml'  % user has specified the .xml file
            % is it a vol.xml or a raw.xml?
            [~, ~, subExt] = fileparts(filename);
            switch subExt
                case '.vol'
                    fnames.vol = fullfile(fpath, filename);
                    fnames.xml = [fnames.vol fext];
                    [~, fnames.prefix, ~] = fileparts(fnames.vol);
                case '.raw'
                    fnames.raw = fullfile(fpath, filename);
                    fnames.info = [fnames.raw '.info'];
                    [~, fnames.prefix, ~] = fileparts(fnames.raw);
            end
        case '.vol'  % user has specified the .vol file
            fnames.vol = fullfile(fpath, [filename fext]);
            fnames.xml = [fnames.vol '.xml'];
            [~, fnames.prefix, ~] = fileparts(fnames.vol);
        case '.raw'
            fnames.raw  = fullfile(fpath, [filename fext]);
            fnames.info = [fnames.raw '.info'];
            [~, fnames.prefix, ~] = fileparts(fnames.raw);
        case '.'  % user supplied without extension
            %    disp('No extension supplied');
            vol_file = fullfile(fpath, [filename '.vol']);
            raw_file = fullfile(fpath, [filename '.raw']);
            if exist(vol_file, 'file')
                % 32 bit
                fnames.vol = vol_file;
                fnames.xml = [vol_file '.xml'];
            elseif exist(raw_file,'file')
                fnames.raw = raw_file;
                fnames.info = fullfile(fpath, [filename '.info']);
            else
                message = ['No file found that started with: ' full_fname];
                Mexc = MException(errorID, message);
                throw(Mexc);
            end
            [~, fnames.prefix, ~] = fileparts(full_fname);
        otherwise
            message = ['Sorry, could''t determine HST file: ' full_fname];
            Mexc = MException(errorID, message);
            throw(Mexc);
    end

    % Check that all necessary files exist
    if isfield(fnames,'raw') && ~exist(fnames.raw,'file')
        Mexc = MException(errorID, '.raw file does not exist');
        throw(Mexc);
    end
    if isfield(fnames,'info') && ~exist(fnames.info,'file')
        Mexc = MException(errorID, '.info file does not exist');
        throw(Mexc);
    end
    if isfield(fnames,'xml') && ~exist(fnames.xml,'file')
        Mexc = MException(errorID, '.xml file does not exist');
        throw(Mexc);
    end
    if isfield(fnames,'vol') && ~exist(fnames.vol,'file')
        Mexc = MException(errorID, '.vol file does not exist');
        throw(Mexc);
    end

    % Guessing most likely Filetype
    if isfield(fnames, 'raw')
        % going with the 8 bit file
        datatype = 'uint8';
    elseif isfield(fnames, 'xml')
        % going with the 32 bit file - should really check this!
        datatype = 'float32';
    else
        message = 'File does not really appear to exist!';
        Mexc = MException(errorID, message);
        throw(Mexc);
    end
end
