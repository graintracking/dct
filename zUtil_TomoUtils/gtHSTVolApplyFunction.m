function gtHSTVolApplyFunction(full_fname, varargin)
% GTHSTVOLAPPLYFUNCTION  Applies functions to an HST volume.
%     -------------------------------------------------------------------------
%     gtHSTVolApplyFunction(full_fname, varargin)
%
%     INPUT:
%       full_fname        = <string>  prefix path to the volume file to work on
%
%     OPTIONAL INPUT (varargin as a list of pairs, see parse_pv_pairs.m):
%       zrange            = <range>  index range of output in Z direction
%       functionhandle    = <handle> handle to the funtion to apply 
%       functionarguments = <args>   arguments for the function to apply
%
%     EXAMPLES:
%
%       gtHSTVolApplyFunction('/data/file','functionhandle',@remove_rings,'functionarguments',[13.8])
%       This reads the volume and calls the ring removal function with a
%       threshold value (to segment sample from background) of 13.8. Each
%       corrected slice is written as an EDF file to the CURRENT directory.
%       This shall be condored soon to permit much faster work.
%
%       The easier function to call is
%       sequence_remove_rings('/data/id19/myvolume',13.8)
%
%       To guess the threshold, load a single slice from the volume (see above)
%       and run:
%       autothresh(im)
%
%     Version 002 10-02-2012 by NVigano,  nicola.vigano@esrf.fr
%       Imported and modified
%       Changed gtVolReadSingle calling interface, and fixed misc indentation
%
%     Version 001 XX-01-2007 by ?
%       Now does ring correction! Documentation needs completion.
%
% Required files from the directory:
%        /data/id19/archive/matlab/util/
%
% Individual files:
%        parse_pv_pairs.m
%        inputwdefaultnumeric.m

    params.zrange = 'all';
    params.functionhandle = '';
    params.functionarguments = [];
    % Not used right now
    % params.outputfile = '';

    params = parse_pv_pairs(params, varargin);

    if ~exist('full_fname','var')  % filename not specified on command line
        [tmp1, tmp2] = uigetfile('*','Select a .vol or .raw volume file');
        full_fname = fullfile(tmp2,tmp1);
    end

    if ~isa(params.functionhandle,'function_handle')
        Mexc = MException(  'HST:missing_parameter', ...
                            'I need a valid functionhandle!');
        throw(Mexc)
    end

    try
        HST_header = gtHST_info(full_fname);
    catch Mexc
        disp('Unable to read either the volume or the associated XML file.  Please')
        disp('verify that the files exist in the current directory')
        disp(' ')
        Mexc1 = MException('HST:wrong_input', 'Unable to read files');
        Mexc1 = addCause(Mexc1, Mexc);
        throw(Mexc1);
    end

    if ~exist(HST_header.fname_final, 'file')
        Mexc = MException('HST:wrong_file_name', ...
                          ['File ''' HST_header.fname_final ''' does not exist!']);
        throw(Mexc)
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%
    % common for both filetypes


    % user has specified x y and z ranges
    % might have used 'all' to indicate all elements
    if strcmp(params.zrange, 'all')
        rangeZ = 1:HST_header.volSizeZ;
    else
        rangeZ = params.zrange;
    end

    if any(find(rangeZ > HST_header.volSizeZ)) % if any part of the range is bigger than the volume
        Mexc = MException('HST:wrong_parameter', ...
                          'Z range is too big for specified volume file');
        throw(Mexc);
    end

    % read slice by slice, process, and save
    for slicenum = 1:(rangeZ(end)-rangeZ(1)+1)
        fprintf('Working on slice %d/%d...\n', rangeZ(slicenum), rangeZ(end));
        % always read as singles, even if 8 bit - calculations might need the
        % precision...

        skip_voxels = (HST_header.volSizeX)*(HST_header.volSizeY)*(rangeZ(slicenum)-1);
        skip_bytes = skip_voxels*HST_header.bytespervoxel;

        infoVOL = HST_header;
        infoVOL.volSizeZ = 1;
        img = gtVolReadSingle(HST_header.fname_final, infoVOL, skip_bytes);

        % process slice
        if ~isempty(params.functionarguments)
            img2 = params.functionhandle(img, params.functionarguments);
        else
            img2 = params.functionhandle(img);
        end

        % save slice
        info = [];
        info.datatype = HST_header.ftype;
        outputname = sprintf('%s_RC_%04d.edf', HST_header.fname_prefix, rangeZ(slicenum));

        edf_write(single(img2), outputname, info);
    end
end

