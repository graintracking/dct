classdef GtGuiSample < GtGuiJoinableElem
    properties
        sample;
    end

    methods (Access = public)
        function obj = GtGuiSample(sample, varargin)
            obj = obj@GtGuiJoinableElem();
            if (ischar(sample))
                obj.sample = GtSample.loadFromFile(sample);
            else
                obj.sample = sample;
%                 obj.sample = sample.getCopy();
            end

            obj.conf.f_title = 'Sample GUI';
            obj.initGtBaseGuiElem(varargin);
        end
    end

    methods (Access = protected)
        function initGui(obj)
            initGui@GtBaseGuiElem(obj)

            phasesNum = length(obj.sample.phases);

            sample_boxes = [];
            sample_boxes.main = uiextras.VBox('Parent', obj.conf.currentParent);

            sample_boxes.upper_sum = uicontrol('Parent', sample_boxes.main, ...
                                                'Style', 'text', ...
                                                'HorizontalAlignment', 'left', ...
                                                'String', sprintf('Sample with: %d phases. ', phasesNum));
            sample_boxes.grid = uiextras.Grid('Parent', sample_boxes.main);
            sample_boxes.labels(1) = uicontrol('Parent', sample_boxes.grid, ...
                                                'Style', 'text', ...
                                                'String', 'Volume File:', ...
                                                'HorizontalAlignment', 'left');
            sample_boxes.labels(2) = uicontrol('Parent', sample_boxes.grid, ...
                                                'Style', 'text', ...
                                                'String', 'Dilated Volume File:', ...
                                                'HorizontalAlignment', 'left');         
            sample_boxes.labels(3) = uicontrol('Parent', sample_boxes.grid, ...
                                                'Style', 'text', ...
                                                'String', 'Absorption Volume File:', ...
                                                'HorizontalAlignment', 'left');
            sample_boxes.edits(1) = uicontrol('Parent', sample_boxes.grid, ...
                                                'Style', 'edit', ...
                                                'HorizontalAlignment', 'left');
            sample_boxes.edits(2) = uicontrol('Parent', sample_boxes.grid, ...
                                                'Style', 'edit', ...
                                                'HorizontalAlignment', 'left');
            sample_boxes.edits(3) = uicontrol('Parent', sample_boxes.grid, ...
                                                'Style', 'edit', ...
                                                'HorizontalAlignment', 'left');
            sample_boxes.editsButt(1) = uicontrol('Parent', sample_boxes.grid, ...
                'Style', 'pushbutton', 'String', 'Path', ...
                'Callback', @(src, evt)openPathForEdit(obj, sample_boxes.edits(1), ...
                                                        '5_reconstruction', 'volume.mat'));
            sample_boxes.editsButt(2) = uicontrol('Parent', sample_boxes.grid, ...
                'Style', 'pushbutton', 'String', 'Path', ...
                'Callback', @(src, evt)openPathForEdit(obj, sample_boxes.edits(2), ...
                                                        '5_reconstruction', 'volume_dilated.mat'));
            sample_boxes.editsButt(3) = uicontrol('Parent', sample_boxes.grid, ...
                'Style', 'pushbutton', 'String', 'Path', ...
                'Callback', @(src, evt)openPathForEdit(obj, sample_boxes.edits(3), ...
                                                        '5_reconstruction', 'volume_absorption.mat'));
            set(sample_boxes.grid, 'ColumnSizes', [180 -1 50], 'RowSizes', [25 25 25]);

            sample_boxes.phases = [];
            for ii = 1:phasesNum
                phaseBox = [];
                phaseBox.panel = uiextras.Panel('Parent', sample_boxes.main, ...
                                                'Title', sprintf('Phase %d:', ii));
                phaseBox.grid = uiextras.Grid('Parent', phaseBox.panel);
                phaseBox.labels(1) = uicontrol('Parent', phaseBox.grid, ...
                                                'Style', 'text', ...
                                                'String', 'Phase Name:', ...
                                                'HorizontalAlignment', 'left');
                phaseBox.labels(2) = uicontrol('Parent', phaseBox.grid, ...
                                                'Style', 'text', ...
                                                'String', 'Is active:', ...
                                                'HorizontalAlignment', 'left');
                phaseBox.labels(3) = uicontrol('Parent', phaseBox.grid, ...
                                                'Style', 'text', ...
                                                'String', 'Volume File:', ...
                                                'HorizontalAlignment', 'left');
                phaseBox.labels(4) = uicontrol('Parent', phaseBox.grid, ...
                                                'Style', 'text', ...
                                                'String', 'Volume Field:', ...
                                                'HorizontalAlignment', 'left');
                phaseBox.edits(1) = uicontrol('Parent', phaseBox.grid, ...
                                                'Style', 'edit', ...
                                                'HorizontalAlignment', 'left');
                phaseBox.edits(2) = uipanel('Parent', phaseBox.grid, 'BorderType', 'none');
                phaseBox.edits(3) = uicontrol('Parent', phaseBox.grid, ...
                                                'Style', 'edit', ...
                                                'HorizontalAlignment', 'left');
                phaseBox.edits(4) = uicontrol('Parent', phaseBox.grid, ...
                                                'Style', 'edit', ...
                                                'HorizontalAlignment', 'left');
                phaseBox.editsButt(1) = uipanel('Parent', phaseBox.grid, 'BorderType', 'none');
                phaseBox.editsButt(2) = uicontrol('Parent', phaseBox.grid, ...
                    'Style', 'togglebutton', ...
                    'Callback', @(src, evt)toggleSelected(obj, src));
                phaseBox.editsButt(3) = uicontrol('Parent', phaseBox.grid, ...
                    'Style', 'pushbutton', 'String', 'Path', ...
                    'Callback', @(src, evt)openPathForEdit(obj, phaseBox.edits(3), ...
                                                        '5_reconstruction', sprintf('phase_%02d_vol.mat', ii)));
                phaseBox.editsButt(4) = uicontrol('Parent', phaseBox.grid, ...
                    'Style', 'pushbutton', 'String', 'Find', ...
                    'Callback', @(src, evt)findFieldInMat(obj, phaseBox.edits(3), phaseBox.edits(4)));
                set(phaseBox.grid, 'ColumnSizes', [120 -1 70], 'RowSizes', [25 25 25 25]);

                sample_boxes.phases = [sample_boxes.phases phaseBox];
            end

            sample_boxes.butts = uiextras.HButtonBox('Parent', sample_boxes.main);

            uicontrol('Parent', sample_boxes.butts, 'Style', 'pushbutton', ...
                      'String', 'Ok', 'Callback', @(src, evt)callbackButtOk(obj));
            uicontrol('Parent', sample_boxes.butts, 'Style', 'pushbutton', ...
                      'String', 'Cancel', 'Callback', @(src, evt)callbackButtCancel(obj));
            uicontrol('Parent', sample_boxes.butts, 'Style', 'pushbutton', ...
                      'String', 'Reset', 'Callback', @(src, evt)callbackButtReset(obj));

            set(sample_boxes.main, 'Sizes', [25, 100, -ones(1, phasesNum), 35]);
            obj.conf.sample_boxes = sample_boxes;
        end

        function resetUiComponents(obj)
            set(obj.conf.sample_boxes.edits(1), 'String', obj.sample.volumeFile);
            set(obj.conf.sample_boxes.edits(2), 'String', obj.sample.dilVolFile);
            set(obj.conf.sample_boxes.edits(3), 'String', obj.sample.absVolFile);

            phasesNum = length(obj.sample.phases);
            for ii = 1:phasesNum
                phase = obj.sample.phases{ii};
                phaseBox = obj.conf.sample_boxes.phases(ii);

                set(phaseBox.edits(1), 'String', phase.phaseName);

                phaseVolFile = regexp(phase.volumeFile, ':', 'split');
                if (numel(phaseVolFile) >= 1)
                    set(phaseBox.edits(3), 'String', phaseVolFile{1});
                end
                if (numel(phaseVolFile) >= 2)
                    set(phaseBox.edits(4), 'String', phaseVolFile{2});
                end

                % Activeness
                if (phase.active)
                    set(phaseBox.editsButt(2), 'String', 'Active');
                    set(phaseBox.editsButt(2), 'Value', get(phaseBox.editsButt(2), 'Max'))
                else
                    set(phaseBox.editsButt(2), 'String', 'Not Active');
                    set(phaseBox.editsButt(2), 'Value', get(phaseBox.editsButt(2), 'Min'))
                end
            end
        end

        function doUpdateDisplay(obj)
        end

        function addUICallbacks(obj)
        end

        function openPathForEdit(~, edit, defaultPath, defaultName)
            fileCompletePath = get(edit, 'String');
            if (isempty(fileCompletePath))
                fileCompletePath = fullfile(defaultPath, defaultName);
            end

            filter = {'*.mat', 'MATLAB Files (*.mat)'};
            [fileName, filePath] = uigetfile(filter, 'Select file...', fileCompletePath);
            if (filePath(end) == filesep())
                filePath(end) = [];
            end
            [~, containingDir] = fileparts(filePath);
            if (strcmpi(containingDir, '5_reconstruction'))
                filePath = containingDir;
            end

            if (ischar(fileName))
                set(edit, 'String', fullfile(filePath, fileName));
            end
        end

        function findFieldInMat(obj, fileEdit, fieldEdit)
            filePath = get(fileEdit, 'String');
            if (isempty(filePath))
                error('GUI:GtGuiSample:wrong_argument', 'No file specified')
            end
            matFile = matfile(filePath);
            % If it's writable, it means it doesn't exist
            if (matFile.Properties.Writable)
                error('GUI:GtGuiSample:wrong_argument', ...
                        'File %s doesn''t exist!', filePath)
            end

            fieldNames = fieldnames(matFile);
            selector = GtGuiSelectInList(fieldNames(2:end));
            index = selector.join();

            if (~isempty(index))
                set(fieldEdit, 'String', fieldNames{index+1});
            end
        end

        function toggleSelected(~, src)
        % GTGUISAMPLE/TOGGLESELECTED Toggles the selected status of a phase

            isSelected = (get(src, 'Value') == get(src, 'Max'));
            if (isSelected)
                set(src, 'String', 'Active');
            else
                set(src, 'String', 'Not Active');
            end
        end

        function sample = collectSampleInfo(obj)
            sample = obj.sample;
            sample.volumeFile = get(obj.conf.sample_boxes.edits(1), 'String');
            sample.dilVolFile = get(obj.conf.sample_boxes.edits(2), 'String');
            sample.absVolFile = get(obj.conf.sample_boxes.edits(3), 'String');

            phasesNum = length(obj.sample.phases);
            for ii = 1:phasesNum
                phaseBox = obj.conf.sample_boxes.phases(ii);

                obj.sample.phases{ii}.phaseName = get(phaseBox.edits(1), 'String');

                obj.sample.phases{ii}.volumeFile = ...
                    [get(phaseBox.edits(3), 'String') ':' get(phaseBox.edits(4), 'String')];

                obj.sample.phases{ii}.active = ...
                    (get(phaseBox.editsButt(2), 'Value') == get(phaseBox.editsButt(2), 'Max'));
            end
        end
    end

    methods (Access = public)
        function callbackButtOk(obj)
            retSample = obj.collectSampleInfo();
            if (obj.isJoint)
                obj.setJointReturnVal(retSample);
            else
                filter = {'*.mat', 'MATLAB Files (*.mat)'};
                [fileName, filePath] = uiputfile(filter, 'Select file...', '4_grains/sample.mat');
                if (ischar(fileName))
                    retSample.saveToFile(fullfile(filePath, fileName));
                else
                    error('GUI:GtGuiSample:wrong_argument', 'No file specified!');
                end
                delete(obj);
            end
        end

        function callbackButtCancel(obj)
            if (obj.isJoint)
                obj.setJointReturnVal([]);
            else
                delete(obj);
            end
        end

        function callbackButtReset(obj)
            obj.resetUiComponents();
        end
    end
end
