function hst_header = gtHST_info(full_fname)
% GTHST_INFO  Loads the information about the HST volume.
%     -------------------------------------------------------------------------
%     hst_header = gtHST_info(full_fname)
%
%     INPUT:
%       full_fname = <string>  path to the input file 
%
%     OUTPUT:
%       hst_header = <struct>  HST volume info
% 
%     Version 001 10-02-2012 by NVigano,  nicola.vigano@esrf.fr
%       Imported and modified from HST_info.m (id19)

    if strcmp(full_fname(1:2),'..')
        Mexc = MException('HST:wrong_file_path', ...
                'Not able to parse relative paths - please specify completely');
        throw(Mexc);
    end

    [fnames, fpath, filetype] = gtGetHST_FileNamesAndType(full_fname);

    if strcmp(filetype, 'float32')
        fname_final = fnames.vol;

        xmlHSTinfo = xml_read(fnames.xml);
        xmlHSTinfo = xmlHSTinfo.reconstruction.listSubVolume.subVolume;

        if isfield(xmlHSTinfo, 'RAW_NAME')
            true_ftype = 'uint8';
            bytespervoxel = 1;
        else
            % 32 bit float volume
            true_ftype = 'float32';
            bytespervoxel = 4;
        end

        volSizeX = xmlHSTinfo.('SIZEX');
        volSizeY = xmlHSTinfo.('SIZEY');
        volSizeZ = xmlHSTinfo.('SIZEZ');

        if strcmp(xmlHSTinfo.('BYTE_ORDER'), 'LOWBYTEFIRST')
            fbyteorder = 'l';
        else
            fbyteorder = 'b';
        end
    elseif strcmp(filetype, 'uint8')
        % read the .info file here
        fname_final = fnames.raw;
        fid = fopen(fnames.info, 'rt');

        % skip the first line
        fgetl(fid);

        % Read sizes
        volSizeX = sscanf(fgetl(fid), 'NUM_X = %d');
        volSizeY = sscanf(fgetl(fid), 'NUM_Y = %d');
        volSizeZ = sscanf(fgetl(fid), 'NUM_Z = %d');

        fbyteorder = 'l';
        true_ftype = filetype;
        bytespervoxel = 1;
    end

    hst_header = gtCreateInfoVol([volSizeX volSizeY volSizeZ], true_ftype, fbyteorder);
    hst_header.bytespervoxel = bytespervoxel;
    hst_header.fpath = fpath;
    hst_header.fname_final = fname_final;
    hst_header.fname_prefix = fnames.prefix;
end
