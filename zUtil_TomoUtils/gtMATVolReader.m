function [vol, info] = gtMATVolReader(vol)
% GTMATVOLREADER  Loads a volume from a .mat file. It needs to know also the
%                 field that holds the volume, and in case (optional) the info 
%                 about the volume.
%     -------------------------------------------------------------------------
%     [vol, info] = gtMATVolReader(vol)
%
%     INPUT:
%       vol  = <string>    All info about volume file with following syntax:
%                          'filename:volume_field[:info_field]'
%
%     OUTPUT:
%       vol  = <3Dimage>  Output 3D volume 
%       info = <struct>   HST parameter structure 
%
%     NOTE:
%       As a side application, it can load every field in a matlab file, even 
%       if it is not a volume.
%
%     Version 001 12-03-2012 by NVigano,  nicola.vigano@esrf.fr

    [~, ~, ext] = fileparts(vol);
    ext = regexp(ext, ':', 'split');
    volName = regexp(vol, ':', 'split');

    if (length(ext) < 2)
        error('MAT_VOL:wrong_argument', ...
              'You didn''t specify the fields where to read the data.')
    end

    vol = load(volName{1}, ext{2});
    vol = vol.(ext{2});

    if (length(ext) == 3)
        try
            info = load(volName{1}, ext{3});
            info = info.(ext{3});
        catch mexc
            gtPrintException(mexc, 'Failed loading Volume Info');
            info = [];
        end
    else
        info = [];
    end
end % end of function
