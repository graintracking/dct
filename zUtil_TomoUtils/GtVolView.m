classdef GtVolView < GtBaseGuiElem
% GTVOLVIEW  A simple viewer of 3D volume.
%     -------------------------------------------------------------------------
%     GtVolView(vol, varargin)
%
%     INPUT:
%       vol          = <3Dimage>       Input image to view, can be the volume
%                      <string>        variable or the name of the file.
%
%     OPTIONAL INPUT (varargin as a list of pairs, see parse_pv_pairs.m):
%       'plane'      = 'xy','yz','zx', Set the viewing plane
%                      'yx','zy','xz'
%       'slicendx'   = <int>           Start viewing at slice index supplied
%                                      default is 1
%       'cmap'       = <string>        Set the colour map ('gray','jet', etc).
%                                      see 'help graph3d'
%                                      The cmap_saturation colormap is quite
%                                      useful for determining which pixels
%                                      would be saturated for certain min/max
%                                      default is 'gray'
%       'cbar'       = <bool>          Show colourbar
%       'clims'      = perc,[min max]  Set the colour limits by giving either
%                                      a single value or two values
%                                      see 'help gtSetColourLimits'
%       'hordisplay' = [min max]       Set limits for windows horizontal axis
%       'verdisplay' = [min max]                 "             vertical   "
%       'parent'     = <ui_handle>     You can supply a graphic object where
%                                      the volume viewer will install into
%       'overlay'    = <vol>           If you supply a single image with the
%                                      dimensions as the slice you are looking
%                                      at, it is overlaid on top.  Watch your
%                                      colormap!
%       'linkname'   = <string>        If you run GtVolView multiple times,
%                                      using the same linkname, the sliders
%                                      are linked together in the different
%                                      viewers.  What fun!
%
%     OUTPUT:
%       h           = <ui_handle>      Graphical user interface handle
%
%     EXAMPLES:
%       GtVolView(vol,'cbar',true,'plane','yz','slicendx',43,'cmap',jet,'clims',3)
%
%       You can use vol = gtHSTVolReader to load your ESRF HST volumes.
%
%     SUB-FUNCTIONS:
%[sub]- displayVolumeInfo
%[sub]- changeVol
%[sub]- changePerspective
%[sub]- changeOverlay
%[sub]- changeSlice
%[sub]- getVolValue
%[sub]- initParams
%[sub]- setConfigInvariants
%[sub]- initGui
%[sub]- toggleOverlayVisibility
%[sub]- addUICallbacks
%[sub]- resetUiComponents
%[sub]- createAxesMenu
%[sub]- get3DPixelInfo
%[sub]- copy3DPixInfoToClipboard
%[sub]- guiQuit
%[sub]- loadSlice
%[sub]- setVisualisedData
%[sub]- doUpdateDisplay
%[sub]- scrollVolume
%[sub]- updateBC
%[sub]- updateOverlay
%[sub]- setOverlayButton
%[sub]- updateZoom
%[sub]- loadVolume
%
%     Version 009 16-01-2013 by YGuilhem, yoann.guilhem@esrf.fr
%       Added buttons to flip left/right and up/down
%
%     Version 008 30-05-2012 by YGuilhem, yoann.guilhem@esrf.fr
%       Added a changeSlice interactive function
%
%     Version 007 15-05-2012 by NVigano,  nicola.vigano@esrf.fr
%       Added single phase loading
%
%     Version 006 26-04-2012 by AKing,    andrew.king@esrf.fr
%       Chooses which renderer to use
%
%     Version 005 21-03-2012 by YGuilhem, yoann.guilhem@esrf.fr
%       Now handles every perspective possibility
%       Fix overlay and resizing window feature
%
%     Version 004 20-03-2012 by NVigano,  nicola.vigano@esrf.fr
%       Added absorption overlay visualization for GtGrainsManager
%       Fixed no context menu when using an overlay
%       Moved overlay checking to initParams, to just do it when needed
%
%     Version 003 13-03-2012 by YGuilhem, yoann.guilhem@esrf.fr
%       Add the mouse scrolling feature
%       Can now load tif volumes directly
%       Display window for non-square slices
%
%     Version 002 05-03-2012 by NVigano,  nicola.vigano@esrf.fr
%       Changed axes conventions to the natural convention from ID19's
%       tomographic conventions
%       Changed access permissions
%       Added extensible context menu
%       Update display every 10ms
%       Added Transparency toggle button (for the overlay)
%
%     Version 001 21-02-2012 by NVigano,  nicola.vigano@esrf.fr
%       Made a OO class from gtVolView.m
%       fixed errors with overlay and closure of the figure

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Public object API - Interaction with outside
    methods (Access = public)
        function obj = GtVolView(vol, varargin)
        % GTVOLVIEW/GTVOLVIEW Constructor
            obj = obj@GtBaseGuiElem();

            [obj.conf.vol, obj.conf.volInfo] = GtVolView.loadVolume(vol);
            if (isempty(vol))
                gtError('GtVolView:wrong_argument', 'Incoming volume is empty!');
            end

            obj.conf.f_title = 'Volume Viewer';

            obj.conf.slicendx = 1;
            obj.conf.clims = 0;
            obj.conf.savedclims = [0 0];
            obj.conf.autoclims = [false false];
            obj.conf.cbar = false;
            obj.conf.cmap = 'gray';
            obj.conf.plane = 'xy';

            obj.conf.linkname = [];
            obj.conf.overlay = [];

            [dim(1), dim(2), dim(3)] = size(obj.conf.vol);
            obj.conf.dim = dim;
            obj.conf.hordisplay = [1 obj.conf.dim(1)];
            obj.conf.verdisplay = [1 obj.conf.dim(2)];
            obj.conf.horflip = false;
            obj.conf.verflip = true;

            obj.initGtBaseGuiElem(varargin);
        end

        function displayVolumeInfo(obj)
        % GTVOLVIEW/DISPLAYVOLUMEINFO Displays volume and overlay information,
        % if loaded.
            display_str = '';
            if (~isempty(obj.conf.vol) && ~isempty(obj.conf.volInfo))
                display_str = [display_str sprintf('Volume:\n')];
                display_str = [display_str evalc('disp(obj.conf.volInfo)')];
            end
            if (~isempty(obj.conf.overlay) && ~isempty(obj.conf.overlayInfo))
                display_str = [display_str sprintf('Overlay:\n')];
                display_str = [display_str evalc('disp(obj.conf.overlayInfo)')];
            end
            if isempty(display_str)
                display_str = 'No info loaded.';
            end
            helpdlg(display_str, 'Volume Info');
        end

        function changeVol(obj, newvol, varargin)
        % GTVOLVIEW/CHANGEVOL Allows to change the loaded/visualised volume,
        % after launching the viewer
            [obj.conf.vol, obj.conf.volInfo] = GtVolView.loadVolume(newvol);
            if (isempty(obj.conf.vol))
                gtError('GtVolView:wrong_argument', 'Incoming volume is empty!');
            end

            [dim(1), dim(2), dim(3)] = size(obj.conf.vol);
            obj.conf.dim = dim;
            obj.conf.hordisplay = [1 obj.conf.dim(obj.conf.horDim)];
            obj.conf.verdisplay = [1 obj.conf.dim(obj.conf.verDim)];
            obj.conf.clims = 0;

            obj.initParams(varargin);
            obj.setConfigInvariants();
            obj.resetUiComponents();
            obj.toggleOverlayVisibility();
            obj.updateDisplay();
        end

        function autoCLims(obj, ind, newState)
        % GTVOLVIEW/AUTOCLIM Allows to switch to/from auto color/contrast limits
            if obj.conf.autoclims(ind) ~= newState
                obj.conf.autoclims(ind) = newState;
                if obj.conf.autoclims(ind)
                    obj.conf.savedclims(ind) = obj.conf.clims(ind);
                    autoLims = gtAutolim(obj.conf.vol);
                    obj.conf.clims(ind) = autoLims(ind);
                else
                    obj.conf.clims(ind) = obj.conf.savedclims(ind);
                end

                obj.resetUiComponents();
                %obj.updateBC();
                %updateBC(obj);
                obj.doUpdateDisplay();
            end
        end

        function horFlip(obj, newState)
        % GTVOLVIEW/HORFLIP Allows to switch to/from horizontal flip mode
            if (obj.conf.horflip ~= newState)
                obj.conf.horflip = newState;
                obj.doUpdateDisplay();
            end
        end

        function verFlip(obj, newState)
        % GTVOLVIEW/VERFLIP Allows to switch to/from vertical flip mode
            if (obj.conf.verflip ~= newState)
                obj.conf.verflip = newState;
                obj.doUpdateDisplay();
            end
        end

        function changePerspective(obj, new_perspective)
        % GTVOLVIEW/CHANGEPERSPECTIVE Allows to change the perspective of the
        % visualised data. In other words, it allows to change the axes between
        % XY, YZ, ZX, YX, ZY and XZ.
            if ( ~ischar(new_perspective) ...
                    || ~ismember(new_perspective, {'xy', 'yz', 'zx', 'yx', 'zy', 'xz' }) )
                mexc = MException('GUI:wrong_argument', ...
                                  'Perspectives need to be one of "xy", "yz", "zx", "yx", "zy" or "xz"');
                throw(mexc);
            end
            obj.conf.plane = new_perspective;

            obj.setConfigInvariants();
            obj.resetUiComponents();
            obj.updateDisplay();
        end

        function changeOverlay(obj, newoverlay, varargin)
        % GTVOLVIEW/CHANGEOVERLAY Allows to change the loaded/visualised overlay,
        % after launching the viewer
            [obj.conf.overlay, obj.conf.overlayInfo] = GtVolView.loadVolume(newoverlay);

            obj.initParams(varargin);
            obj.setConfigInvariants();
            obj.resetUiComponents();
            obj.toggleOverlayVisibility();
            obj.updateDisplay();
        end

        function changeSlice(obj, newslicendx)
        % GTVOLVIEW/CHANGESLICE Allows to change the loaded slice index
            obj.conf.slicendx = round(newslicendx);
            set(obj.conf.h_slider, 'Value', obj.conf.slicendx);
            obj.updateDisplay();
        end

        function setZoom(obj, xy_lims)
            set(obj.conf.h_ax, 'XLim', xy_lims{1})
            set(obj.conf.h_ax, 'YLim', xy_lims{2})
        end

        function value = getVolValue(obj, point)
        % GTVOLVIEW/GETVOLVALUE Returns the value for the given point, from the
        % volume stored in the object
            roundedPoint = round(point);
            value = obj.conf.vol(roundedPoint(1), roundedPoint(2), roundedPoint(3));
        end

        function [point, value] = getComplete3DPixelInfo(obj, point)
        % GTVOLVIEW/GET3DPIXELINFO Returns the value and the point where the
        % pointer is now over in respect to the volume stored in the object
            if (~exist('point', 'var'))
                point = round(obj.get3DPixelInfo());
            else
                point = round(point);
            end
            % At the border, rounding gives a wrong pixel position
            [vol_size(1), vol_size(2), vol_size(3)] = size(obj.conf.vol);
            point = min(point, vol_size);
            value = obj.getVolValue(point);
        end
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Internal methods - Addressable from derived classes
    methods (Access = protected)
        function initParams(obj, arguments)
            initParams@GtBaseGuiElem(obj, arguments)

            % Let's consider Overlay / Checking its dimensions
            if ~isempty(obj.conf.overlay)
                [obj.conf.overlay, obj.conf.overlayInfo] = GtVolView.loadVolume(obj.conf.overlay);
                % Now checking dimensions
                [dimOvrl(1), dimOvrl(2), dimOvrl(3)] = size(obj.conf.overlay);
                if (~isequal(obj.conf.dim, dimOvrl))
                    mexc = MException('GUI:wrong_overlay_dimensions', ...
                                      'Overlay dimensions should be equal to volume dimensions');
                    throw(mexc);
                end
            end
        end

        function setConfigInvariants(obj)
            % Fixed parameters
            setConfigInvariants@GtBaseGuiElem(obj);

            % Internal parameters (not exposed to the user)
            obj.conf.firsttime = true;
            obj.conf.transparency = 0.66;

            % modify the colour limits if a percentile is requested
            obj.conf.clims = gtSetColourLimits(obj.conf.vol, obj.conf.clims, true);

            % Process the color map
            obj.conf.cmap = gtSetColourMap(obj.conf.vol, obj.conf.cmap, true);

            % Horizontal, vertical and orthogonal dimensions indexes (real axes)
            switch obj.conf.plane
                case 'xy'
                    obj.conf.horDim = 1;
                    obj.conf.verDim = 2;
                    obj.conf.ortDim = 3;
                case 'yz'
                    obj.conf.horDim = 2;
                    obj.conf.verDim = 3;
                    obj.conf.ortDim = 1;
                case 'zx'
                    obj.conf.horDim = 3;
                    obj.conf.verDim = 1;
                    obj.conf.ortDim = 2;
                case 'yx'
                    obj.conf.horDim = 2;
                    obj.conf.verDim = 1;
                    obj.conf.ortDim = 3;
                case 'zy'
                    obj.conf.horDim = 3;
                    obj.conf.verDim = 2;
                    obj.conf.ortDim = 1;
                case 'xz'
                    obj.conf.horDim = 1;
                    obj.conf.verDim = 3;
                    obj.conf.ortDim = 2;
                otherwise
                    % ensure slice is possible for a given orientation
                    help(mfilename)
                    error('GUI:wrong_argument', ...
                        'Plane must be one of "xy", "yz", "zx", "yx", "zy" or "xz"');
            end
        end

        function initGui(obj)
            initGui@GtBaseGuiElem(obj);
            % Let's disable zoom, while configuring
            % must disable zoom before setting impixelinfoval (R2008A)
            zoomOp = zoom(obj.conf.h_figure);
            zoomState = get(zoomOp, 'Enable');
            zoom(obj.conf.h_figure, 'off');

            obj.conf.main_boxes = uiextras.VBox('Parent', obj.conf.currentParent);
            obj.conf.upper_boxes = uiextras.HBox('Parent', obj.conf.main_boxes);
            obj.conf.center_boxes = uiextras.HBox('Parent', obj.conf.main_boxes);

            obj.conf.h_sliceslabel = uicontrol('Style', 'text', ...
                                           'Parent', obj.conf.main_boxes, ...
                                           'String', 'Slide through the slices');
            set(obj.conf.h_sliceslabel, 'units', 'normalized');

            obj.conf.h_slider = uicontrol('Style', 'slider', 'Parent', obj.conf.main_boxes);
            set(obj.conf.h_slider, 'units', 'normalized');
            set(obj.conf.h_slider, 'Tag', 'slider');

            set(obj.conf.main_boxes, 'Sizes', [30 -1 15 20]);

            if (obj.conf.dim(3) == 1)
                set(obj.conf.h_slider, 'Visible', 'off');
            end

            %%% Brightness / Contrast
            obj.conf.h_min = uicontrol('Style', 'slider', 'Parent', obj.conf.center_boxes);
            set(obj.conf.h_min, 'Units', 'normalized');

            obj.conf.h_max = uicontrol('Style', 'slider', 'Parent', obj.conf.center_boxes);
            set(obj.conf.h_max, 'Units', 'normalized');

            obj.conf.h_minmaxlabel = uicontrol('Style', 'text', ...
                                           'Parent', obj.conf.upper_boxes, ...
                                           'HorizontalAlignment', 'left');
            set(obj.conf.h_minmaxlabel, 'units', 'normalized');
            %%% Done Brightness - Contrast

            obj.conf.display_boxes = uiextras.VBox('Parent', obj.conf.center_boxes);
            set(obj.conf.center_boxes, 'Sizes', [20 20 -1]);

            obj.conf.ax_boxes = uiextras.HBox('Parent', obj.conf.display_boxes);

            obj.conf.h_ax = axes('Parent', obj.conf.ax_boxes, ...
                             'ActivePositionProperty', 'OuterPosition');
            set(obj.conf.ax_boxes, 'Sizes', -1);

            obj.conf.h_im = imagesc('Parent', obj.conf.h_ax);

            % Set up the overlay
            hold(obj.conf.h_ax, 'on');
            obj.conf.h_overlay = imagesc('Parent', obj.conf.h_ax);
            hold(obj.conf.h_ax, 'off');

            obj.conf.h_translabel = uicontrol('Style', 'text', ...
                                          'Parent', obj.conf.display_boxes, ...
                                          'String', 'Overlay transparency');
            obj.conf.trans_boxes = uiextras.HBox('Parent', obj.conf.display_boxes);
            obj.conf.h_trans = uicontrol('Style', 'slider', ...
                                     'Parent', obj.conf.trans_boxes, ...
                                     'Min', 0, 'Max', 1);
            set(obj.conf.h_trans, 'Units', 'normalized');

            obj.conf.h_trans_butt = uicontrol('Style', 'togglebutton', ...
                                              'Parent', obj.conf.trans_boxes, ...
                                              'String', 'Overlay');
            set(obj.conf.trans_boxes, 'Sizes', [-1 70]);
            obj.toggleOverlayVisibility();
            if ~isempty(obj.conf.overlay)
                % only use OpenGL if we need an overlay
                set(obj.conf.h_figure, 'Renderer', 'OpenGL');
            end
                % End setup of the overlay

            obj.conf.pixelinfo_panel = uipanel('Parent', obj.conf.upper_boxes, ...
                                           'BorderType', 'none');
            try
                %%% Leaving old code for image toolbox nostalgics, in case just
                %%% uncomment and use it :-P
%                 obj.conf.h_pixelinfo = impixelinfoval(obj.conf.pixelinfo_panel, obj.conf.h_im);
%                 set(obj.conf.h_pixelinfo, 'Unit', 'normalized');
%                 set(obj.conf.h_pixelinfo, 'Position', [0 0 1 1]);
                obj.conf.h_pixelinfo = GtPixelInfoVal(obj.conf.pixelinfo_panel, ...
                                                    obj.conf.h_im, ...
                                                    @(x, y)obj.getComplete3DPixelInfo());
            catch mexc
                gtPrintException(mexc, 'Disabling pixel info value!');
            end

            % Let's create the Axes Menu
            obj.createAxesMenu();

            % Auto clims buttons
            obj.conf.h_autoclimslabel = uicontrol('Style', 'text', ...
                                           'Parent', obj.conf.upper_boxes, ...
                                           'HorizontalAlignment', 'right');
            set(obj.conf.h_autoclimslabel, 'string', 'Auto clims');

            obj.conf.autoclims_boxes = uiextras.VBox('Parent', obj.conf.upper_boxes);
            obj.conf.h_autominclim_button = uicontrol('Parent', obj.conf.autoclims_boxes, ...
                                            'Style', 'togglebutton', ...
                                            'String', 'min', ...
                                            'Callback', @(src, evt)autoCLims(obj, 1, ~obj.conf.autoclims(1)));
            obj.conf.h_automaxclim_button = uicontrol('Parent', obj.conf.autoclims_boxes, ...
                                            'Style', 'togglebutton', ...
                                            'String', 'max', ...
                                            'Callback', @(src, evt)autoCLims(obj, 2, ~obj.conf.autoclims(2)));

            % Zoom button
            obj.conf.h_zoom_button = uicontrol('Parent', obj.conf.upper_boxes, ...
                                           'Style', 'togglebutton', ...
                                           'String', 'Zoom');
            if strcmp(zoomState, 'on')
                set(obj.conf.h_zoom_button, 'Value', get(obj.conf.h_zoom_button, 'Max'));
            else
                set(obj.conf.h_zoom_button, 'Value', get(obj.conf.h_zoom_button, 'Min'));
            end


            % Flip L/R U/D toggle buttons
            obj.conf.h_horflip_button = uicontrol('Parent', obj.conf.upper_boxes, ...
                                            'Style', 'togglebutton', ...
                                            'String', 'Flip L/R', ...
                                            'Callback', @(src, evt)horFlip(obj, ~obj.conf.horflip));

            obj.conf.h_verflip_button = uicontrol('Parent', obj.conf.upper_boxes, ...
                                            'Style', 'togglebutton', ...
                                            'String', 'Flip U/D', ...
                                            'Callback', @(src, evt)verFlip(obj, ~obj.conf.verflip));

            % Perspective radio buttons
            obj.conf.radio_boxes = uiextras.VBox('Parent', obj.conf.upper_boxes);
            obj.conf.radio_boxes1 = uiextras.HBox('Parent', obj.conf.radio_boxes);
            obj.conf.radio_boxes2 = uiextras.HBox('Parent', obj.conf.radio_boxes);
            obj.conf.h_radio_xy = uicontrol('Style', 'Radio', ...
                                            'String', 'XY',...
                                            'Parent', obj.conf.radio_boxes1, ...
                                            'Callback', @(src, evt)changePerspective(obj, 'xy'));
            obj.conf.h_radio_yz = uicontrol('Style', 'Radio', ...
                                            'String', 'YZ',...
                                            'Parent', obj.conf.radio_boxes1, ...
                                            'Callback', @(src, evt)changePerspective(obj, 'yz'));
            obj.conf.h_radio_zx = uicontrol('Style', 'Radio', ...
                                            'String', 'ZX',...
                                            'Parent', obj.conf.radio_boxes1, ...
                                            'Callback', @(src, evt)changePerspective(obj, 'zx'));
            obj.conf.h_radio_yx = uicontrol('Style', 'Radio', ...
                                            'String', 'YX',...
                                            'Parent', obj.conf.radio_boxes2, ...
                                            'Callback', @(src, evt)changePerspective(obj, 'yx'));
            obj.conf.h_radio_zy = uicontrol('Style', 'Radio', ...
                                            'String', 'ZY',...
                                            'Parent', obj.conf.radio_boxes2, ...
                                            'Callback', @(src, evt)changePerspective(obj, 'zy'));
            obj.conf.h_radio_xz = uicontrol('Style', 'Radio', ...
                                            'String', 'XZ',...
                                            'Parent', obj.conf.radio_boxes2, ...
                                            'Callback', @(src, evt)changePerspective(obj, 'xz'));

            set(obj.conf.radio_boxes, 'Sizes', [-1 -1], 'Spacing', 0, 'Padding', 0);
            set(obj.conf.radio_boxes1, 'Sizes', [-1 -1 -1], 'Spacing', 0, 'Padding', 0);
            set(obj.conf.radio_boxes2, 'Sizes', [-1 -1 -1], 'Spacing', 0, 'Padding', 0);
            % End perpective

            obj.conf.h_info_button = uicontrol('Parent', obj.conf.upper_boxes, ...
                                               'Style', 'pushbutton', ...
                                               'String', 'Info');

            set(obj.conf.upper_boxes, 'Sizes', [-1.5 -1 40 40 45 60 60 120 40]);

            % Reset back the zoom to the previous state (before the inclusion)
            set(zoomOp, 'Enable', zoomState);

            % write the app variable to a shared location (appending if necessary)
            if ~isempty(obj.conf.linkname)
                if (isappdata(0, 'GtVolView'))
                    appdata = getappdata(0, 'GtVolView');
                    if (isfield(appdata, obj.conf.linkname))
                        % check that sliders have same maximum values
                        try
                            disp(['Adding instance to previously existing ' ...
                                    'linkname: ' obj.conf.linkname]);
                            appdata.(obj.conf.linkname).attach(obj, obj.conf.dim);
                        catch mexc
                            gtPrintException(mexc, 'Linkname creation failed!');
                            obj.conf.linkname = [];
                        end
                    else
                        disp(['First instance of GtVolView with linkname: ' ...
                                obj.conf.linkname]);
                        appdata.(obj.conf.linkname) = ...
                            GtEventLinkerVolView(obj.conf.dim, obj.conf.slicendx, ...
                                                 obj.conf.plane, obj.conf.horflip, ...
                                                 obj.conf.verflip, ...
                                                 get(obj.conf.h_ax, {'XLim', 'YLim'}), ...
                                                 obj);
                    end
                else
                    disp('First instance of GtVolView linkname');
                    appdata = [];
                    appdata.(obj.conf.linkname) = ...
                        GtEventLinkerVolView(obj.conf.dim, obj.conf.slicendx, ...
                                             obj.conf.plane, obj.conf.horflip, ...
                                             obj.conf.verflip, ...
                                             get(obj.conf.h_ax, {'XLim', 'YLim'}), ...
                                             obj);
                end
                setappdata(0, 'GtVolView', appdata);
            end

            % Finally add the Callback to update the volume (not a GUI Callback)
            obj.conf.changeVolume_Callback = @changeVol;
        end

        function toggleOverlayVisibility(obj)
        % GTVOLVIEW/TOGGLEOVERLAYVISIBILITY If there's an overlay, triggers its
        % visualization, otherwise shuts it down

            if ~isempty(obj.conf.overlay)
                set(obj.conf.h_overlay, 'Visible', 'on');
                set(obj.conf.h_translabel, 'Enable',  'on');
                set(obj.conf.h_translabel, 'Visible', 'on');
                set(obj.conf.trans_boxes,  'Enable',  'on');
                set(obj.conf.trans_boxes,  'Visible', 'on');
                set(obj.conf.display_boxes, 'Sizes', [-1 15 20]);
                obj.conf.minOverlayVol = min(obj.conf.overlay(:));
                obj.conf.maxOverlayVol = max(obj.conf.overlay(:));
            else
                set(obj.conf.h_overlay, 'Visible', 'off');
                set(obj.conf.h_translabel, 'Enable',  'off');
                set(obj.conf.h_translabel, 'Visible', 'off');
                set(obj.conf.trans_boxes,  'Enable',  'off');
                set(obj.conf.trans_boxes,  'Visible', 'off');
                set(obj.conf.display_boxes, 'Sizes', [-1 0 0]);
            end
        end

        function addUICallbacks(obj)
        % GTVOLVIEW/ADDUICALLBACKS Adds most of the callbacks to the related
        % objects.

            %%% GUI Callbacks need to be added later on because the addition of
            %%% them triggers their call, and if the application data is not
            %%% saved yet, matlab is going to throw errors in the callbacks.
            %%% Thankfully these callbacks just need the objects but not the
            %%% other callbacks
            slider_cbk = @(src, evt)changeSlice(obj, round(get(obj.conf.h_slider, 'Value')));
            obj.addUICallback(obj.conf.h_slider, ...
                              'AdjustmentValueChangedCallback', ...
                              slider_cbk, true);

            % Mouse scroll action to move slider
            obj.addUICallback(obj.conf.h_figure, 'WindowScrollWheelFcn', ...
                              @(src, evt)scrollVolume(obj, evt), false);

            obj.addUICallback(obj.conf.h_min, ...
                              'AdjustmentValueChangedCallback', ...
                              @(src, evt)updateBC(obj), true);
            obj.addUICallback(obj.conf.h_max, ...
                              'AdjustmentValueChangedCallback', ...
                              @(src, evt)updateBC(obj), true);

            obj.addUICallback(obj.conf.h_zoom_button, 'Callback', ...
                              @(src, evt)updateZoom(obj), false);
            obj.addUICallback(obj.conf.h_info_button, 'Callback', ...
                              @(src, evt)displayVolumeInfo(obj), false);

            obj.addUICallback(obj.conf.h_trans, ...
                              'AdjustmentValueChangedCallback', ...
                              @(src, evt)updateOverlay(obj), true);
            obj.addUICallback(obj.conf.h_trans_butt, ...
                              'Callback', ...
                              @(src, evt)setOverlayButton(obj), false);
            obj.addUICallback(obj.conf.h_figure, ...
                              'WindowButtonMotionFcn', ...
                              @(src, evt)obj.conf.h_pixelinfo.onMouseMoveCbk(), ...
                              false);
            %%% End callbacks
        end

        function resetUiComponents(obj)
        % GTVOLVIEW/RESETUICOMPONENTS Resets the gui elements to the default
        % values. Useful in case of Volume or Perspective changes.

            minRadio = get(obj.conf.h_radio_xy, 'Min');
            maxRadio = get(obj.conf.h_radio_xy, 'Max');

            ortDim = obj.conf.ortDim;
            set(obj.conf.h_slider, 'Min', 1, 'Max', obj.conf.dim(ortDim), ...
                    'sliderstep', [1/obj.conf.dim(ortDim) 1/obj.conf.dim(ortDim)]);
            if (obj.conf.slicendx > obj.conf.dim(ortDim))
                obj.conf.slicendx = obj.conf.dim(ortDim);
            end

            set(obj.conf.h_radio_xy, 'Value', minRadio);
            set(obj.conf.h_radio_yz, 'Value', minRadio);
            set(obj.conf.h_radio_zx, 'Value', minRadio);
            set(obj.conf.h_radio_yx, 'Value', minRadio);
            set(obj.conf.h_radio_zy, 'Value', minRadio);
            set(obj.conf.h_radio_xz, 'Value', minRadio);
            switch (obj.conf.plane)
                case 'xy'
                    set(obj.conf.h_radio_xy, 'Value', maxRadio);
                case 'yz'
                    set(obj.conf.h_radio_yz, 'Value', maxRadio);
                case 'zx'
                    set(obj.conf.h_radio_zx, 'Value', maxRadio);
                case 'yx'
                    set(obj.conf.h_radio_yx, 'Value', maxRadio);
                case 'zy'
                    set(obj.conf.h_radio_zy, 'Value', maxRadio);
                case 'xz'
                    set(obj.conf.h_radio_xz, 'Value', maxRadio);
            end

            set(obj.conf.h_slider, 'Value', obj.conf.slicendx);

            set(obj.conf.h_min, 'Min', obj.conf.clims(1), 'Max', obj.conf.clims(2), ...
                'Value', obj.conf.clims(1));
            set(obj.conf.h_max, 'Min', obj.conf.clims(1), 'Max', obj.conf.clims(2), ...
                'Value', obj.conf.clims(2));

            obj.conf.hordisplay = [1 obj.conf.dim(obj.conf.horDim)];
            obj.conf.verdisplay = [1 obj.conf.dim(obj.conf.verDim)];

            if (~isempty(obj.conf.overlay))
                set(obj.conf.h_trans, 'Value', obj.conf.transparency);
            end
        end

        function createAxesMenu(obj)
            obj.conf.h_context_menu = uicontextmenu(...
                'Parent', obj.conf.h_figure, ...
                'Callback', @(src, evt)saveClickedPoint(obj));
            set(obj.conf.h_im, 'UIContextMenu', obj.conf.h_context_menu);
            set(obj.conf.h_overlay, 'UIContextMenu', obj.conf.h_context_menu);

            % Copy pixel info in context menu
            cbk = @(src, evt)copy3DPixInfoToClipboard(obj);
            obj.conf.h_menu_items = {};
            obj.conf.h_menu_items{end+1} = uimenu(obj.conf.h_context_menu, ...
                                           'Label', 'Copy pixel 3D info', ...
                                           'Callback', cbk);
            % Save figure in context menu
            obj.conf.h_menu_items{end+1} = uimenu(obj.conf.h_context_menu, ...
                                           'Label', 'Save figure', ...
                                           'Callback', @(src, evt)saveFigure(obj));
        end

        function point = get3DPixelInfo(obj)
            point2D = get(obj.conf.h_ax, 'CurrentPoint');
            point = zeros(1, 3);
            point(obj.conf.horDim) = point2D(1, 1);
            point(obj.conf.verDim) = point2D(1, 2);
            point(obj.conf.ortDim) = obj.conf.slicendx;
        end

        function copy3DPixInfoToClipboard(obj, verbose)
            if ~exist('verbose','var') || isempty(verbose)
                verbose = true;
            end
            clipString = obj.conf.h_pixelinfo.getString();
            if (verbose)
                disp(clipString)
            end
            clipboard('copy', clipString);
        end

        function guiQuit(obj)
            if (isfield(obj.conf, 'linkname') && ~isempty(obj.conf.linkname))
                try
                    if (isappdata(0, 'GtVolView'))
                        appdata = getappdata(0, 'GtVolView');
                        if (isfield(appdata, obj.conf.linkname))
                            % check that sliders have same maximum values
                            disp(['Removing instance to previously existing ' ...
                                    'linkname: ' obj.conf.linkname]);
                            emptied = appdata.(obj.conf.linkname).detach(obj);
                            if (emptied)
                                disp(['Removing linkname: ' obj.conf.linkname]);
                                appdata = rmfield(appdata, obj.conf.linkname);
                            end
                        else
                            gtError('GtVolView:linkname_inconsistency', ...
                                'Linkname "%s" doesn''t exist!', ...
                                obj.conf.linkname);
                        end
                    else
                        gtError('GtVolView:linkname_inconsistency', ...
                            'Linkname field for GtVolView doesn''t exist!');
                    end
                    setappdata(0, 'GtVolView', appdata);
                catch mexc
                    gtPrintException(mexc, 'Linkname detaching failed!');
                end
            end

            guiQuit@GtBaseGuiElem(obj);
        end

        function slice = loadSlice(obj, vol)
        % GTVOLVIEW/LOADSLICE Loads a slice from the volume (given the plane and
        % slice index in obj.conf.*) and permutes it to be visualised correctly
        % by matlab, which always flips X and Y dimensions in visualisation.

            switch (obj.conf.ortDim)
                case 3 % equivalent to plane 'xy' or 'yx'
                    slice = squeeze(vol(:, :, obj.conf.slicendx, :));
                case 1 % equivalent to plane 'yz' or 'zy'
                    slice = squeeze(vol(obj.conf.slicendx, :, :, :));
                case 2 % equivalent to plane 'zx' or 'xz'
                    slice = squeeze(vol(:, obj.conf.slicendx, :, :));
            end
            % Cope with matlab showing issue
            if (obj.conf.verDim > obj.conf.horDim)
                slice = permute(slice, [2, 1, 3]);
            end
        end

        function setVisualisedData(obj, image, vol, is_overlay)
        % GTVOLVIEW/SETVISUALISEDDATA Loads a slice and then operates
        % transformations on them, to adapt to the requested visualization needs

            slice = obj.loadSlice(vol);

            if (is_overlay && ~isa(vol,'integer'))
                sliceMin = min(slice(:));
                sliceMax = max(slice(:));
                sliceRange = sliceMax - sliceMin;
                slice = repmat(slice-sliceMin, [1,1,3])/sliceRange;
            end

            set(image, 'cdata', slice);
        end

        function doUpdateDisplay(obj)
        % GTVOLVIEW/DOUPDATEDISPLAY Function that actually updates all the
        % visualized GUI components of the GUI element

            set(obj.conf.h_slider, 'Value', obj.conf.slicendx);

            if (obj.conf.firsttime)
                sliceDims = [obj.conf.dim(obj.conf.horDim) obj.conf.dim(obj.conf.verDim)];
                xlim = [0.5 sliceDims(1)+0.5];
                ylim = [0.5 sliceDims(2)+0.5];
            else
                xlim = get(obj.conf.h_ax, 'xlim');
                ylim = get(obj.conf.h_ax, 'ylim');
            end

            % Now reset the image showed by the viewer
            obj.setVisualisedData(obj.conf.h_im, obj.conf.vol, false);
            if (~isempty(obj.conf.overlay))
                obj.setVisualisedData(obj.conf.h_overlay, obj.conf.overlay, true);
            end

            axis(obj.conf.h_ax, 'on');
            axis(obj.conf.h_ax, 'equal');
            if obj.conf.horflip
                set(obj.conf.h_ax, 'XDir', 'reverse');
            else
                set(obj.conf.h_ax, 'XDir', 'normal');
            end
            if obj.conf.verflip
                set(obj.conf.h_ax, 'YDir', 'reverse');
            else
                set(obj.conf.h_ax, 'YDir', 'normal');
            end

            frame = 'XYZ';
            xlabel(obj.conf.h_ax, frame(obj.conf.horDim));
            ylabel(obj.conf.h_ax, frame(obj.conf.verDim));

            set(obj.conf.h_im, 'xdata', linspace(obj.conf.hordisplay(1), obj.conf.hordisplay(2), obj.conf.dim(obj.conf.horDim)));
            set(obj.conf.h_im, 'ydata', linspace(obj.conf.verdisplay(1), obj.conf.verdisplay(2), obj.conf.dim(obj.conf.verDim)));

            set(obj.conf.h_ax, 'xlim', xlim); % reset the axis limits to those of the previous image (possibly zoomed in)
            set(obj.conf.h_ax, 'ylim', ylim);

            colormap(obj.conf.h_ax, obj.conf.cmap);
            %if (obj.conf.cbar), colorbar('peer', obj.conf.h_ax); end
            title(obj.conf.h_ax, sprintf('Slice: %3d', obj.conf.slicendx));

            % Discard First Time behavior
            obj.conf.firsttime = false;

            % Cope with links to other visualization tools
            if (~isempty(obj.conf.linkname))
                try
                    if (isappdata(0, 'GtVolView'))
                        appdata = getappdata(0, 'GtVolView');
                        if (isfield(appdata, obj.conf.linkname))
                            appdata.(obj.conf.linkname).notify(...
                                            obj.conf.slicendx, obj.conf.plane, ...
                                            obj.conf.horflip, obj.conf.verflip, ...
                                            {xlim, ylim});
                        else
                            gtError('GtVolView:linkname_inconsistency', ...
                                'Linkname "%s" doesn''t exist!', ...
                                obj.conf.linkname);
                        end
                    else
                        gtError('GtVolView:linkname_inconsistency', ...
                            'Linkname field for GtVolView doesn''t exist!');
                    end
                catch mexc
                    gtPrintException(mexc, 'Problem in linkname');
                end
            end

            % Let's trigger also the others
            updateBC(obj);
            if (~isempty(obj.conf.overlay)), updateOverlay(obj); end
        end

        function scrollVolume(obj, evt)
        % GTVOLVIEW/SCROLLVOLUME Function that moves the slider when using
        % mouse scroll
            if (evt.VerticalScrollCount ~= 0)
                newSlicendx = round(get(obj.conf.h_slider, 'Value')) + evt.VerticalScrollCount;
                newSlicendx = max(newSlicendx, get(obj.conf.h_slider, 'Min'));
                newSlicendx = min(newSlicendx, get(obj.conf.h_slider, 'Max'));
                obj.changeSlice(newSlicendx);
                obj.conf.h_pixelinfo.onMouseMoveCbk();
            end
        end
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Public Callbacks - Interaction with GUI elements
    methods (Access = public)
        function updateBC(obj)
        % GTVOLVIEW/UPDATEBC Function to update Brightness and Contrast
            clims = [get(obj.conf.h_min, 'Value') get(obj.conf.h_max, 'Value')];

            if clims(1) >= clims(2)
                if gco == obj.conf.h_max
                    set(obj.conf.h_min, 'Value', get(obj.conf.h_max, 'Value'));
                else
                    set(obj.conf.h_max, 'Value', get(obj.conf.h_min, 'Value'));
                end
                clims(2) = clims(1)+0.00001;
            end

            set(obj.conf.h_ax, 'clim', clims);
            set(obj.conf.h_minmaxlabel, 'string', sprintf('Limits: [%.6g, %.6g]', clims));
            drawnow('expose', 'update');
        end

        function updateOverlay(obj)
        % GTVOLVIEW/UPDATEOVERLAY Function to update the Overlay Transparency
            obj.conf.transparency = get(obj.conf.h_trans, 'Value');
            set(obj.conf.h_overlay, 'alphadata', obj.conf.transparency);

            butt_value = get(obj.conf.h_trans_butt, 'Value');
            if (obj.conf.transparency > 0)
                if (butt_value == get(obj.conf.h_trans_butt, 'Min'))
                    new_value = get(obj.conf.h_trans_butt, 'Max');
                    set(obj.conf.h_trans_butt, 'Value', new_value);
                end
            else
                if (butt_value == get(obj.conf.h_trans_butt, 'Max'))
                    new_value = get(obj.conf.h_trans_butt, 'Min');
                    set(obj.conf.h_trans_butt, 'Value', new_value);
                end
            end
            drawnow;
        end

        function setOverlayButton(obj)
        % GTVOLVIEW/UPDATEOVERLAYBUTTON Function to update the Overlay
        % Transparency on the action of the toggle button
            value = get(obj.conf.h_trans_butt, 'Value');
            tempTransp = obj.conf.transparency;

            if (value == get(obj.conf.h_trans_butt, 'Min'))
                set(obj.conf.h_trans, 'Value', 0);
            else
                set(obj.conf.h_trans, 'Value', obj.conf.transparency);
            end
            drawnow;
            obj.updateOverlay();

            obj.conf.transparency = tempTransp;
        end

        function updateZoom(obj)
        % GTVOLVIEW/UPDATEZOOM Function to update the Zoom state
            zoomOp = zoom(obj.conf.h_figure);
            value = get(obj.conf.h_zoom_button, 'Value');

            if (value == get(obj.conf.h_zoom_button, 'Min'))
                set(zoomOp, 'Enable', 'off');
            else
                set(zoomOp, 'Enable', 'on');
            end
            drawnow;
        end

        function saveFigure(obj, fname, ftype)
        % GTVOLVIEW/SAVEFIGURE Save the content of axis to a file
            cancel = false;
            if (~exist('fname', 'var') || isempty(fname) || ...
                ~exist('ftype', 'var') || isempty(ftype))
                [fname, ftype, cancel] = imputfile();
            else
                if (exist(fname, 'file'))
                    answer = inputwdefault(['''' fname ''' already exists.\n  Do you want to replace it? [y/n]'], 'n');
                    if (~strcmp(answer, 'y'))
                        cancel = true;
                    end
                end
            end
            if (cancel)
                disp('User canceled image saving, so nothing has been written.');
            else
                im = frame2im(getframe(obj.conf.h_ax));
                imwrite(im, fname, ftype);
            end
        end

        function saveClickedPoint(obj)
        % GTVOLVIEW/SAVECLICKEDPOINT Saves the point position so that we
        % don't lose it, by moving the mouse by mistake
            point = get3DPixelInfo(obj);
            obj.conf.clicked_point = point;
        end
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Utility Methods independent of the visualisation object
    methods (Static)
        function [vol, info] = loadVolume(vol)
        % GTVOLVIEW/LOADVOLUME Loads a volume, trying also to detect if it is
        % already a volume or a filename. In case of filename, tries to guess
        % also the format
            if isa(vol, 'char') % user has specified a filename
                if (isempty(vol))
                    error('GtVolView:loadVolume:wrong_argument', 'Empty volume filename');
                elseif (strfind(vol, ':') == 1)
                    error('GtVolView:loadVolume:wrong_argument', 'Invalid volume filename: %s', vol);
                end
                [~, ~, ext] = fileparts(vol);
                ext = regexp(ext, ':', 'split');
                switch (ext{1})
                    case '.edf'
                        [vol, info] = edf_read(vol);
                    case '.tif'
                        [vol, info] = gtTIFVolReader(vol);
                    case '.mat'
                        [vol, info] = gtMATVolReader(vol);
                    otherwise
                        [vol, info] = gtHSTVolReader(vol);
                end
            else
                if islogical(vol)
                    disp('Converting logical volume to uint8 for display');
                    vol = uint8(vol);
                elseif (iscell(vol))
                    disp('Summing all cells into one volume');
                    vol_fin = vol{1};
                    for n = 2:numel(vol)
                        vol_fin = vol_fin + vol{n};
                    end
                    vol = vol_fin;
                end
                info = [];
            end
        end

        function handles = compareVolumes(vols, varargin)
            handles = GtVolView.classCompareVolumes(vols, varargin{:});
        end
    end

    methods (Static, Access = protected)
        function handles = classCompareVolumes(vols, varargin)
        % GTVOLVIEW/COMPAREVOLUMES Utility function to compare multiple volumes
        % with the same parameters. Linkname is determined automatically.

            try
                throw(MException('phony:error',''));
            catch ME
                caller_class = regexp(ME.stack(2).name, '\.', 'split');
                caller_class = caller_class{1};
                constructor = str2func(caller_class);
            end

            autoLinkPattern = 'autolink%04d';
            autoLinkNumber = 0;
            if (isappdata(0, 'GtVolView'))
                app = getappdata(0, 'GtVolView');
                while (isfield(app, sprintf(autoLinkPattern, autoLinkNumber)))
                    autoLinkNumber = autoLinkNumber +1;
                end
            end

            totVols = numel(vols);
            linkname_args = [ ...
                {'linkname', sprintf(autoLinkPattern, autoLinkNumber)} varargin];
            for ii = 1:totVols
                final_args = [{'f_number', ii} linkname_args];
                handles(ii) = constructor(vols{ii}, final_args{:}); %#ok<AGROW>
            end
        end
    end
end
