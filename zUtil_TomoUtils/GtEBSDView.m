classdef GtEBSDView < GtBaseGuiElem
    properties
        ebsd = [];
    end

    methods
        function self = GtEBSDView(ebsd_str, varargin)
        % GTVOLVIEW/GTVOLVIEW Constructor
            self = self@GtBaseGuiElem();

            self.conf.f_title = 'EBSD Map Viewer';

            self.conf.cmap = 'gray';

            if (~isstruct(ebsd_str))
                error('GtEBSDView:wrong_argument', ...
                    'Only an EBSD data structure is accepted')
            end
            self.ebsd = ebsd_str;
            self.conf.ipf_axis = [];

            self.conf.horDim = 1;
            self.conf.verDim = 2;
            [dim(1), dim(2), dim(3)] = size(self.ebsd.map_r);
            self.conf.dim = dim(1:2);
            self.conf.hordisplay = [1 self.conf.dim(1)];
            self.conf.verdisplay = [1 self.conf.dim(2)];
            self.conf.horflip = false;
            self.conf.verflip = true;
            self.conf.firsttime = true;

            self.initGtBaseGuiElem(varargin);
        end

        function horFlip(obj, newState)
        % GTVOLVIEW/HORFLIP Allows to switch to/from horizontal flip mode
            if (obj.conf.horflip ~= newState)
                obj.conf.horflip = newState;
                obj.doUpdateDisplay();
            end
        end

        function verFlip(obj, newState)
        % GTVOLVIEW/VERFLIP Allows to switch to/from vertical flip mode
            if (obj.conf.verflip ~= newState)
                obj.conf.verflip = newState;
                obj.doUpdateDisplay();
            end
        end

        function r_vec = getRvector(obj, point)
        % GTVOLVIEW/GETVOLVALUE Returns the value for the given point, from the
        % volume stored in the object
            roundedPoint = round(point);
            r_vec = obj.ebsd.map_r(roundedPoint(1), roundedPoint(2), :);
            r_vec = reshape(r_vec, 1, 3);
        end

        function [point, r_vec] = getCompletePixelInfo(obj, point)
        % GTVOLVIEW/GET3DPIXELINFO Returns the value and the point where the
        % pointer is now over in respect to the volume stored in the object
            if (~exist('point', 'var'))
                point = round(obj.getPixel());
            else
                point = round(point);
            end
            % At the border, rounding gives a wrong pixel position
            point = min(point, obj.conf.dim);

            r_vec = obj.getRvector(point);
        end
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Internal methods - Addressable from derived classes
    methods (Access = protected)
        function initParams(self, arguments)
            initParams@GtBaseGuiElem(self, arguments)

            bad_voxels_mask = any(abs(self.ebsd.map_r) > eps('single'), 3);
            if (isempty(self.ebsd.mask))
                self.ebsd.mask = bad_voxels_mask;
            else
                self.ebsd.mask = self.ebsd.mask & bad_voxels_mask;
            end
        end

        function initGui(obj)
            initGui@GtBaseGuiElem(obj);
            % Let's disable zoom, while configuring
            % must disable zoom before setting impixelinfoval (R2008A)
            zoomOp = zoom(obj.conf.h_figure);
            zoomState = get(zoomOp, 'Enable');
            zoom(obj.conf.h_figure, 'off');

            obj.conf.main_boxes = uiextras.VBox('Parent', obj.conf.currentParent);
            obj.conf.upper_boxes = uiextras.HBox('Parent', obj.conf.main_boxes);
            obj.conf.center_boxes = uiextras.HBox('Parent', obj.conf.main_boxes);

            set(obj.conf.main_boxes, 'Sizes', [30 -1]);
            %%% Done Brightness - Contrast

            obj.conf.display_boxes = uiextras.VBox('Parent', obj.conf.center_boxes);
            set(obj.conf.center_boxes, 'Sizes', -1);

            obj.conf.h_ax = axes('Parent', obj.conf.display_boxes, ...
                             'ActivePositionProperty', 'OuterPosition');
            obj.conf.h_im = imagesc('Parent', obj.conf.h_ax);

            obj.conf.pixelinfo_panel = uipanel('Parent', obj.conf.upper_boxes, ...
                                           'BorderType', 'none');
            try
                %%% Leaving old code for image toolbox nostalgics, in case just
                %%% uncomment and use it :-P
%                 obj.conf.h_pixelinfo = impixelinfoval(obj.conf.pixelinfo_panel, obj.conf.h_im);
%                 set(obj.conf.h_pixelinfo, 'Unit', 'normalized');
%                 set(obj.conf.h_pixelinfo, 'Position', [0 0 1 1]);
                obj.conf.h_pixelinfo = GtPixelInfoVal(obj.conf.pixelinfo_panel, ...
                    obj.conf.h_im, @(x, y)obj.getCompletePixelInfo(), '(%d, %d) [%g, %g, %g]');
            catch mexc
                gtPrintException(mexc, 'Disabling pixel info value!');
            end

            % Let's create the Axes Menu
            obj.createAxesMenu();

            % Zoom button
            obj.conf.h_zoom_button = uicontrol('Parent', obj.conf.upper_boxes, ...
                                           'Style', 'togglebutton', ...
                                           'String', 'Zoom');
            if strcmp(zoomState, 'on')
                set(obj.conf.h_zoom_button, 'Value', get(obj.conf.h_zoom_button, 'Max'));
            else
                set(obj.conf.h_zoom_button, 'Value', get(obj.conf.h_zoom_button, 'Min'));
            end


            % Flip L/R U/D toggle buttons
            obj.conf.h_horflip_button = uicontrol('Parent', obj.conf.upper_boxes, ...
                                            'Style', 'togglebutton', ...
                                            'String', 'Flip L/R', ...
                                            'Callback', @(src, evt)horFlip(obj, ~obj.conf.horflip));

            obj.conf.h_verflip_button = uicontrol('Parent', obj.conf.upper_boxes, ...
                                            'Style', 'togglebutton', ...
                                            'String', 'Flip U/D', ...
                                            'Callback', @(src, evt)verFlip(obj, ~obj.conf.verflip));

            set(obj.conf.upper_boxes, 'Sizes', [-1 45 60 60]);

            % Reset back the zoom to the previous state (before the inclusion)
            set(zoomOp, 'Enable', zoomState);
        end

        function addUICallbacks(obj)
        % GTVOLVIEW/ADDUICALLBACKS Adds most of the callbacks to the related
        % objects.

            %%% GUI Callbacks need to be added later on because the addition of
            %%% them triggers their call, and if the application data is not
            %%% saved yet, matlab is going to throw errors in the callbacks.
            %%% Thankfully these callbacks just need the objects but not the
            %%% other callbacks

            obj.addUICallback(obj.conf.h_zoom_button, 'Callback', ...
                              @(src, evt)updateZoom(obj), false);

            obj.addUICallback(obj.conf.h_figure, ...
                              'WindowButtonMotionFcn', ...
                              @(src, evt)obj.conf.h_pixelinfo.onMouseMoveCbk(), ...
                              false);
            %%% End callbacks
        end

        function createAxesMenu(obj)
            obj.conf.h_context_menu = uicontextmenu(...
                'Parent', obj.conf.h_figure, ...
                'Callback', @(src, evt)saveClickedPoint(obj));
            set(obj.conf.h_im, 'UIContextMenu', obj.conf.h_context_menu);

            % Copy pixel info in context menu
            obj.conf.h_menu_items = {};
            cbk = @(src, evt)copy3DPixInfoToClipboard(obj);
            obj.conf.h_menu_items{end+1} = uimenu(obj.conf.h_context_menu, ...
                                           'Label', 'Copy pixel 3D info', ...
                                           'Callback', cbk);
            cbk = @(src, evt)copyGrainAvgRvecInfoToClipboard(obj);
            obj.conf.h_menu_items{end+1} = uimenu(obj.conf.h_context_menu, ...
                                           'Label', 'Copy grain Average R-vector info', ...
                                           'Callback', cbk);
            cbk = @(src, evt)segmentGrain(obj);
            obj.conf.h_menu_items{end+1} = uimenu(obj.conf.h_context_menu, ...
                                           'Label', 'Segment grain to file...', ...
                                           'Callback', cbk);
            cbk = @(src, evt)extractBandContrastOfGrain(obj);
            obj.conf.h_menu_items{end+1} = uimenu(obj.conf.h_context_menu, ...
                                           'Label', 'Extract grain Band Contrast to file...', ...
                                           'Callback', cbk);
        end

        function resetUiComponents(obj)
        % GTVOLVIEW/RESETUICOMPONENTS Resets the gui elements to the default
        % values. Useful in case of Volume or Perspective changes.

            obj.conf.hordisplay = [1 obj.conf.dim(obj.conf.horDim)];
            obj.conf.verdisplay = [1 obj.conf.dim(obj.conf.verDim)];
        end

        function point = getPixel(obj)
            point2D = get(obj.conf.h_ax, 'CurrentPoint');
            point([obj.conf.horDim obj.conf.verDim]) = point2D(1, 1:2);
        end

        function copy3DPixInfoToClipboard(obj, verbose)
            if ~exist('verbose','var') || isempty(verbose)
                verbose = true;
            end
            clipString = obj.conf.h_pixelinfo.getString();
            if (verbose)
                disp(clipString)
            end
            clipboard('copy', clipString);
        end

        function copyGrainAvgRvecInfoToClipboard(self, verbose)
            if ~exist('verbose','var') || isempty(verbose)
                verbose = true;
            end
            if (isfield(self.conf, 'clicked_point') && ~isempty(self.conf.clicked_point))
                point = round(self.conf.clicked_point);
            else
                point = round(self.getPixel());
            end
            [grain_r_vecs, grain_mask] = gtDefEBSDSegmentGrainFromSeed(self.ebsd.map_r, self.ebsd.mask, point);
            grain_r_vecs = gtDefDmvol2Gvdm(permute(grain_r_vecs, [1 2 4 3]));
            avg_r_vec = sum(grain_r_vecs(:, grain_mask(:)), 2) ./ sum(grain_mask(:));
            clipString = sprintf('(%d, %d) [%g, %g, %g]', point, avg_r_vec');
            if (verbose)
                disp(clipString)
            end
            clipboard('copy', clipString);
        end

        function segmentGrain(self)
            if (isfield(self.conf, 'clicked_point') && ~isempty(self.conf.clicked_point))
                point = round(self.conf.clicked_point);
            else
                point = round(self.getPixel());
            end
            [grain_r_vecs, grain_mask, grain_bb] = gtDefEBSDSegmentGrainFromSeed(self.ebsd.map_r, self.ebsd.mask, point);

            filter = {'*.mat', 'MATLAB Files (*.mat)'};
            [filename, pathname] = uiputfile( ...
                filter, 'Select file...', ...
                fullfile('EBSD', 'grain_seg.mat'));

            grain_struct = struct( ...
                'R_vectors', grain_r_vecs, ...
                'mask', grain_mask, ...
                'bb', grain_bb); %#ok<NASGU>

            if (filename)
                save(fullfile(pathname, filename), '-struct', 'grain_struct', '-v7.3');
            end
        end

        function extractBandContrastOfGrain(self)
            if (isfield(self.conf, 'clicked_point') && ~isempty(self.conf.clicked_point))
                point = round(self.conf.clicked_point);
            else
                point = round(self.getPixel());
            end
            [~, grain_mask, grain_bb] = gtDefEBSDSegmentGrainFromSeed(self.ebsd.map_r, self.ebsd.mask, point);

            grain_lims = grain_bb([1 2 1 2]) + [1 1 grain_bb(3:4)];
            grain_bc = self.ebsd.bc(grain_lims(1):grain_lims(3), grain_lims(2):grain_lims(4));

            grain_struct = struct( ...
                'band_contrast', grain_bc, ...
                'mask', grain_mask, ...
                'bb', grain_bb); %#ok<NASGU>

            filter = {'*.mat', 'MATLAB Files (*.mat)'};
            [filename, pathname] = uiputfile( ...
                filter, 'Select file...', ...
                fullfile('EBSD', 'grain_bc.mat'));

            if (filename)
                save(fullfile(pathname, filename), '-struct', 'grain_struct', '-v7.3');
            end
        end

        function doUpdateDisplay(obj)
        % GTVOLVIEW/DOUPDATEDISPLAY Function that actually updates all the
        % visualized GUI components of the GUI element

            if (obj.conf.firsttime)
                sliceDims = [obj.conf.dim(obj.conf.horDim) obj.conf.dim(obj.conf.verDim)];
                xlim = [0.5 sliceDims(1)+0.5];
                ylim = [0.5 sliceDims(2)+0.5];
            else
                xlim = get(obj.conf.h_ax, 'xlim');
                ylim = get(obj.conf.h_ax, 'ylim');
            end

            % Now reset the image showed by the viewer
            obj.setVisualisedData();

            axis(obj.conf.h_ax, 'on');
            axis(obj.conf.h_ax, 'equal');
            if (obj.conf.horflip)
                set(obj.conf.h_ax, 'XDir', 'reverse');
            else
                set(obj.conf.h_ax, 'XDir', 'normal');
            end
            if (obj.conf.verflip)
                set(obj.conf.h_ax, 'YDir', 'reverse');
            else
                set(obj.conf.h_ax, 'YDir', 'normal');
            end

            frame = 'XY';
            xlabel(obj.conf.h_ax, frame(obj.conf.horDim));
            ylabel(obj.conf.h_ax, frame(obj.conf.verDim));

            set(obj.conf.h_im, 'xdata', linspace(obj.conf.hordisplay(1), obj.conf.hordisplay(2), obj.conf.dim(obj.conf.horDim)));
            set(obj.conf.h_im, 'ydata', linspace(obj.conf.verdisplay(1), obj.conf.verdisplay(2), obj.conf.dim(obj.conf.verDim)));

            set(obj.conf.h_ax, 'xlim', xlim); % reset the axis limits to those of the previous image (possibly zoomed in)
            set(obj.conf.h_ax, 'ylim', ylim);

            % Discard First Time behavior
            obj.conf.firsttime = false;
        end

        function setVisualisedData(self)
        % GTVOLVIEW/SETVISUALISEDDATA Loads a slice and then operates
        % transformations on them, to adapt to the requested visualization needs

            p = gtLoadParameters();
            cryst_system = p.cryst(self.ebsd.phase_id).crystal_system;
            cryst_spacegroup = p.cryst(self.ebsd.phase_id).spacegroup;
            symm = gtCrystGetSymmetryOperators(cryst_system, cryst_spacegroup);

            indx = find(self.ebsd.mask);

            r_vecs = reshape(self.ebsd.map_r, [], 3);
            r_vecs = r_vecs(indx, :);

            [cmap, ~, ~] = gtIPFCmap( ...
                self.ebsd.phase_id, self.conf.ipf_axis, ...
                'r_vectors', r_vecs, ...
                'crystal_system', cryst_system, ...
                'background', false, ...
                'symm', symm);

            slice_R = zeros(self.conf.dim);
            slice_G = zeros(self.conf.dim);
            slice_B = zeros(self.conf.dim);

            slice_R(indx) = cmap(:, 1);
            slice_G(indx) = cmap(:, 2);
            slice_B(indx) = cmap(:, 3);

            set(self.conf.h_im, 'cdata', cat(3, slice_R', slice_G', slice_B'));
        end
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Public Callbacks - Interaction with GUI elements
    methods (Access = public)
        function updateZoom(obj)
        % GTVOLVIEW/UPDATEZOOM Function to update the Zoom state
            zoomOp = zoom(obj.conf.h_figure);
            value = get(obj.conf.h_zoom_button, 'Value');

            if (value == get(obj.conf.h_zoom_button, 'Min'))
                set(zoomOp, 'Enable', 'off');
            else
                set(zoomOp, 'Enable', 'on');
            end
            drawnow();
        end

        function saveClickedPoint(self)
        % GTVOLVIEW/SAVECLICKEDPOINT Saves the point position so that we
        % don't lose it, by moving the mouse by mistake
            point = self.getPixel();
            self.conf.clicked_point = point;
        end
    end
end
