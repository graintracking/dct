classdef GtGrainsAnalysis < GtGrainsManager

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% PROPERTIES
properties
    name = '';                  % Name of the current dataset
    parameters = struct();      % parameters.mat

    p_name = {};                % Name for each phase
    p_crystalsystem = {};       % Crystal system for each phase
    p_latticepar = {};          % Crystal lattice parameters for each phase
    p_symm = {};                % Symmetry operators for each phase
    p_grainIDs = {};            % List of grain IDs for each phase
    p_activeIDs = {};           % List of grain IDs active for each phase
    p_slipFamilies = {};        % List of slip system families for each phase
    p_slipSystems = {};         % List of slip systems for each phase

    Ngrains = [];               % Number of grains
    maxGrainID = [];            % Maximal grain ID for each phase
    grainIDType = '';           % Data type used for grain IDs

    grainIDs = [];              % List of grain IDs
    phaseIDs = [];              % phaseID for each grain
    orientations = [];          % Orientation matrix g for each grain
    r_vectors = [];             % Rodrigues vector for each grain
    completeness = [];          % Completeness value for each grain
    centers = [];               % Center for each grain

    sampleDir = [0 0 1];
    crystDir = [0 0 1];         % crystallographic direction to plot in sample CS
    loadDir = -[0 0 1];         % loading direction in sample CS, negative because it is intending compression
    schmidFactors = struct();   % Schmid factors for each grain
    slipTransfer = {};          % Slip transfer parameter btw grains

    spotTable = '';             % Database information for difspot table
    pairTable = '';             % Database NAN entries for difspot table
    g_difspots = {};            % Diffraction spot list for each grain
    g_mosaicity = {};           % Mosaicity for each grain
    g_allblobs = {};            % Fsim grain results

    selectedFlag = [];          % Switch from 'index' to 'fsim' difspot list for grains
    g_flags = {};               % Fsim flag for spots ondet
    g_table = {};               % grain table for fsim show (difspotID, mosaicity(end), mosaicity(ext), flag, ...)
    g_table_key = {};           % grain table header for fsim show

    grainIDColorMap = [];       % Color map for grain ID/label
    grainRandColorMap = [];     % Random color map
    grainVolumeColorMap = [];   % Color map for grain volume / radius
    grainCompletenessColorMap = []; % Color map for grain completeness
    grainCaxisColorMap = [];    % Color map for grain c axis direction
    grainIPFColorMap = [];      % Color map for grain inverse pole figure
    grainRvecColorMap = [];     % Color map for grain Rvector
    grainSchmidFColorMap = {};  % Color map for schmid factor
    grainMosaicityColorMap = [];% Color map for omega spread for difspots
    grainAnnealingColorMap = [];% Color map for annealing twin clusters

    extras = struct( ...
        'twinInfo', [], ...
        'displayedFlag', '', ...
        'vars_pars', [], ...
        'rej_pars', [] );          % Extra information
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% PUBLIC METHODS
methods (Access = public)
    function obj = GtGrainsAnalysis(varargin)
    % GTTWINANALYSIS/GTTWINANALYSIS  Contructor
    %
    % obj = GtTwinAnalysis(varargin)

        viewer_args = varargin;
        if isempty(varargin) || ~ismember('f_title', varargin(1:2:end))
            viewer_args(end+1:end+2) = {'f_title', ['GtGrainsAnalysis : ' gtGetLastDirName(pwd())]};
        end
        obj = obj@GtGrainsManager([], viewer_args{:});
        obj.doUpdateDisplay();
        obj.updateSidePanel();
        drawnow('expose');

        % assemble parameters for twin analysis
        obj.assembler_params = struct('use_parent_mask', true);

        params = obj.getParams();
        obj.name = params.acq(1).name;

        % And now let's initialize colormap, crystallographic phase info, grain
        % and phase IDs
        % prepare the cmap structure
        if (~isempty(obj.cmap))
            if (~isfield(obj.conf, 'h_cmap') || ~isa(obj.conf.h_cmap, 'GtColormap'))
                tmp = GtColormap('h_fig', [], 'defname', 'ID', ...
                    'name', 'cool', 'nsteps', 50, ...
                    'hasBkg', true, 'hasConflict', true);
                obj.conf.h_cmap = tmp;
                obj.conf.h_cmap.h_fig = [];
            else
                tmp = obj.conf.h_cmap;
                if (isempty(tmp.origCmap))
                    tmp.origCmap = tmp.colormap;
                end
            end
            obj.cmap = tmp.exportProperties();
            obj.cmap.name = 'bgr';
            if (obj.cmap.origLength ~= obj.Ngrains)
                obj.cmap.origLength = obj.Ngrains;
            end
        end

        % set up the phase/grain info
        obj.setPhasesCrystalInfo();
        obj.loadGrainsInfo();
        obj.loadGrainsVolume();
        obj.setPhasesGrainID();

        % get db info for difspot table
        obj.loadDifspotTable();
        % get db info for spotpairs table
        obj.loadPairTable();

        obj.changeGrainCmap('Rand');
    end

    function setPhasesCrystalInfo(obj)
    % GTTWINANALYSIS/SETPHASESCRYSTALINFO  Sets the crystal info of each phase
    %
    % setPhasesCrystalInfo(obj)

        params = obj.getParams();
        ph_num = numel(params.cryst);

        disp(['  Number of phases            : ' num2str(ph_num)])
        for ii_ph = 1:ph_num
            % Store phase crystal system and symmetry operators
            syst = params.cryst(ii_ph).crystal_system;
            obj.p_crystalsystem{ii_ph} = syst;
            obj.p_latticepar{ii_ph} = params.cryst(ii_ph).latticepar;
            % Get symmetry operators
            obj.p_symm{ii_ph} = gtCrystGetSymmetryOperators(syst, [], ii_ph);
            % phase name
            obj.p_name{ii_ph} = params.cryst(ii_ph).name;
            % slip systems
            switch (syst)
                case 'hexagonal'
                    obj.p_slipFamilies{ii_ph} = {...
                        'hcp001', ...
                        'hcp100', ...
                        'hcp101', ...
                        'hcp102', ...
                        'hcp112', ...
                        'hcp100ac', ...
                        'hcp101ac', ...
                        'hcp111ac', ...
                        'hcp112ac', ...
                        'hcp102twin', ...
                        'hcp101twin', ...
                        'hcp111twin', ...
                        'hcp112twin', ...
                        'hcp113twin', ...
                        'hcp114twin'};
                case 'cubic'
                    obj.p_slipFamilies{ii_ph} = {...
                        'fcc', ...
                        'bcc', ...
                        'bcc112', ...
                        'bcc123'};
                otherwise
                    obj.p_slipFamilies{ii_ph} = {};
                    warning('GtGrainsAnalysis:wrong_crystal_system', ...
                        'Crystal system "%s" not recognized', syst)
            end
            % compute slip systems
            obj.p_slipSystems{ii_ph} = gtGetSlipSystems(obj.p_slipFamilies{ii_ph});
        end
    end

    function loadGrainsInfo(obj)
    % GTTWINANALYSIS/LOADGRAINSINFO  Loads the list of all the R vectors,
    % compute orientation matrices and load completeness values
    %
    % loadGrainsInfo(obj)

        params = obj.getParams();
        phases_num = numel(params.cryst);

        sample = obj.getSample();
        obj.r_vectors = [];
        for iphase = 1:phases_num
            if (sample.phases{iphase}.active)
                rvec = sample.phases{iphase}.getR_vector()';
                obj.r_vectors = horzcat(obj.r_vectors, rvec);
            end
        end

        % Compute all orientation matrices g
        obj.orientations = gtMathsRod2OriMat(obj.r_vectors);

        obj.completeness = [];
        for iphase = 1:phases_num
            if (sample.phases{iphase}.active)
                complt = sample.phases{iphase}.getCompleteness()';
                obj.completeness = horzcat(obj.completeness, complt);
            end
        end

        obj.centers = [];
        for iphase = 1:phases_num
            if (sample.phases{iphase}.active)
                cent = sample.phases{iphase}.getCenter()';
                obj.centers = horzcat(obj.centers, cent);
            end
        end

        % grainID for all the phases
        obj.grainIDs = 1:size(obj.r_vectors, 2);
        obj.Ngrains  = length(obj.grainIDs);

        disp(['  Total number of grains      : ' num2str(obj.Ngrains)]);

        % slip transfer
        obj.slipTransfer = cell(obj.Ngrains, obj.Ngrains);
    end

    function loadGrainsVolume(obj, volume)
    % GTTWINANALYSIS/LOADGRAINSVOLUME  Loads the grains volume containing the grains ID
    %
    % loadGrainsVolume(obj, volume, voxelSize)

        params = obj.getParams();

        vox_size_um = 1e3 * mean(params.recgeo(1).voxsize); %to um
        disp(['    DCT volume voxel size     : ' num2str(vox_size_um) 'um']);

        if (~exist('volume', 'var') || isempty(volume))
            volume = 'complete';
        end
        if ischar(volume) && ~strcmpi(volume, 'complete')
            obj.setVolumeToLoad(volume);
        else
            volume = obj.conf.load_vol;
        end
        disp(['    Loaded volume type        : ' volume])

        maxID = max(max(max(obj.conf.vol)));
        obj.grainIDType = gtFindSmallestDataType(maxID, 'uint');

        disp(['    Grains volume size        : ' num2str(size(obj.conf.vol))]);
        disp(['    Data type                 : ' obj.grainIDType]);
        disp(' ');
    end

    function setPhasesGrainID(obj)
    % GTTWINANALYSIS/SETPHASESGRAINID  Sets grain IDs of each phases
    %
    % setPhasesGrainID(obj)

        params = obj.getParams();
        phases_num = numel(params.cryst);

        obj.p_grainIDs = cell(1, phases_num);
        obj.phaseIDs = zeros(1, obj.Ngrains);
        sample = obj.getSample();
        for ii_ph = 1:phases_num
            maxID = sample.phases{ii_ph}.getNumberOfGrains();
            IDs = 1:maxID;
            if (ii_ph > 1)
                maxIDold = sample.phases{ii_ph-1}.getNumberOfGrains();
            else
                maxIDold = maxID;
            end
            IDs = IDs + maxIDold * (ii_ph - 1);
            obj.p_activeIDs{ii_ph} = sample.phases{ii_ph}.getSelectedGrains()';
            obj.p_grainIDs{ii_ph} = IDs;
            obj.phaseIDs(IDs) = ii_ph;
            fprintf('    Ngrains for phase %d       : %d\n', ii_ph, maxID)
            fprintf('    Max grainID for phase %d   : %d\n', ii_ph, max(IDs - maxIDold*(ii_ph-1)))

            info = GtAssembleVol3D.loadPhaseTwinOutput(obj.dataDir, ii_ph);
            if (~isempty(info))
                % Is this supposed to work with multi-phase materials?
                % I guess no
                obj.extras.twinInfo = info;

                if isfield(obj.extras.twinInfo, 'twin_list')
                    twinInfo = obj.extras.twinInfo.twin_list;
                    if (~isempty(twinInfo))
                        twinInfo(twinInfo(:, 1) == 0, :) = [];
                    end
                    obj.extras.twinInfo.twin_list = twinInfo;
                end
                if isfield(obj.extras.twinInfo, 'check_list')
                    twinInfo = obj.extras.twinInfo.check_list;
                    if (~isempty(twinInfo))
                        twinInfo(twinInfo(:, 1) == 0, :) = [];
                    end
                    obj.extras.twinInfo.check_list = twinInfo;
                end
                if isfield(obj.extras.twinInfo, 'merge_list')
                    twinInfo = obj.extras.twinInfo.merge_list;
                    if (~isempty(twinInfo))
                        twinInfo(twinInfo(:, 1) == 0, :) = [];
                    end
                    obj.extras.twinInfo.merge_list = twinInfo;
                end
                if isfield(obj.extras.twinInfo, 'fail_list')
                    twinInfo = obj.extras.twinInfo.fail_list;
                    if (~isempty(twinInfo))
                        twinInfo(twinInfo(:, 1) == 0, :) = [];
                    end
                    obj.extras.twinInfo.fail_list = twinInfo;
                end
            end

            obj.maxGrainID(ii_ph) = max([0, obj.p_activeIDs{:}]);
            fprintf('    Max active ID for phase %d : %d\n', ii_ph, obj.maxGrainID(ii_ph));
        end
        fprintf('  Setting phase(s) IDs        : done!\n');

        obj.extras.displayedFlag = 'all'; % to be handles correctly
    end

    function loadDifspotTable(obj)
    % GTTWINANALYSIS/LOADDIFSPOTTABLE  Loads difspot table data
    %
    % loadDifspotTable(obj)

        [obj.spotTable, ~] = gtDBLoadTable([obj.name 'difspot']);
        disp(' ')
        disp('  Loading difspot table       : done!')
    end

    function loadPairTable(obj)
    % GTTWINANALYSIS/LOADPAIRTABLE  Loads pairs table data
    %
    % loadPairTable(obj)

        params = obj.getParams();

        [obj.pairTable, ~] = gtDBLoadTable(params.acq(1).pair_tablename);
        disp(' ')
        disp('  Loading pairs table         : done!')
    end

    function resetAllGrainVars(obj)
    % GTTWINANALYSIS/RESETALLGRAINVARS  Resets the variables
    % 'flag', 'difspot', 'allblobs' to the original content saved into
    % grain_####.mat files
    %
    % resetAllGrainVars(obj)

        disp(' ')
        fprintf('  Set all grain variables     : ');
        for gID = 1:obj.Ngrains
            obj.getGrainVars(gID);
        end
        fprintf('done!\n')
    end

    function updateGrainsInfoIndexes(obj, selectedFlag, fields)
    % GTTWINANALYSIS/UPDATEGRAINSINFOINDEXES  Gets the same indexes for all
    % the grain variables, depending on the 'selectedFlag' value
    %
    % updateGrainsInfoIndexes(obj, selectedFlag, fields)

        params = obj.getParams();

        if (~exist('fields', 'var') || isempty(fields))
            fields = {'g_difspots', 'g_flags', 'g_mosaicity'};
        elseif any(~isfield(obj, fields))
            fields(~isfield(obj, fields)) = [];
        end
        if (~exist('selectedFlag', 'var') || isempty(selectedFlag))
            selectedFlag = 3; % it was: params.fsim.included, which means 3
        end
        if (isempty(obj.selectedFlag))
            obj.selectedFlag = 3; % it was: params.fsim.included
        end
        if (selectedFlag >= obj.selectedFlag)
            disp(' ')
            text_str = '  Updating indexes for variable  : ';
            disp([text_str fields{1}])
            cellfun(@(x) fprintf('%s%s\n', repmat(' ', 1, length(text_str)), x), [fields(2:end) {'g_allblobs'}])
            fprintf('    current flag from fsim       : %d\n', obj.selectedFlag)
            fprintf('    new selected flag from fsim  : %d\n', selectedFlag)

            included = 3; % reflections included in fsim
            indexes = obj.getIndexesFromFlag(included);
            % updates flags first !
            obj.setGrainFieldIndexes('g_flags', indexes);
            for gID = 1:obj.Ngrains
                obj.setAllblobsFieldIndexes(gID, indexes{gID});
            end
            obj.selectedFlag = selectedFlag;
            % only indexed reflections
            keepidx  = obj.getIndexesFromFlag(obj.selectedFlag);
            cellfun(@(x) obj.setGrainFieldIndexes(x, keepidx), fields);
            for gID = 1:obj.Ngrains
                obj.setAllblobsFieldIndexes(gID, keepidx{gID});
            end
            fprintf('%s-> done!\n', repmat(' ', 1, length(text_str)-3))
        else
            disp(['  Selected flag must be greater (or equal) than (/to) ' num2str(3)])
        end
    end

    function setGrainInfoTable(obj, varargin)
    % GTTWINANALYSIS/SETGRAININFOTABLE  Prepares a table for gtShowFsim
    % using difspotID, mosaicity, flag and varargin
    %
    % setGrainInfoTable(obj, varargin)

        overwrite = false;
        if (isempty(obj.g_difspots) || isempty(obj.g_allblobs) || isempty(obj.g_flags))
            obj.resetAllGrainVars();
            overwrite = true;
        end
        if isempty(obj.g_mosaicity)
            obj.computeGrainsMosaicity();
            overwrite = true;
        end
        if length(obj.g_difspots{1}) ~= length(obj.g_flags{1})
            obj.updateGrainsInfoIndexes();
            overwrite = true;
        end
        if isempty(obj.g_table)
            overwrite = true;
            obj.g_table = cell(1, obj.Ngrains);
        end
        disp(' ')
        fprintf('  Create fsim grain table     : ')
        % varargin check
        varargin = varargin( ismember(varargin, fieldnames(obj.g_allblobs{1})) );

        for gID = 1:obj.Ngrains
            % table header : difspotID mosaicity flag varargin{:}
            if (isempty(obj.g_table{gID}) || (overwrite))
                obj.g_table{gID} = [obj.g_difspots{gID} obj.g_mosaicity{3, gID} obj.g_flags{gID}];
            end
            if (~isempty(varargin) && ~any(ismember(varargin, obj.g_table_key)))
                for ii=1:length(varargin)
                    size2 = size(obj.g_allblobs{gID}.(varargin{ii}), 2);
                    obj.g_table{gID}(:, end+1:end+size2) = obj.g_allblobs{gID}.(varargin{ii});
                end
            end
        end
        fprintf('done!\n')
        % set table_key
        if (isempty(obj.g_table_key) || (overwrite))
            obj.g_table_key = [{'difspotID'} {'mosaicity'} {'flag'} varargin];
            text_str = '    Used following variables  : ';
        elseif (~isempty(varargin) && ~any(ismember(varargin, obj.g_table_key)))
            obj.g_table_key = [obj.g_table_key varargin];
            text_str = '    Added following variables : ';
        else
            text_str = '    Used following variables  : ';
        end
        disp([text_str obj.g_table_key{1}])
        cellfun(@(x) fprintf('%s%s\n', repmat(' ', 1, length(text_str)), x), obj.g_table_key(2:end))
    end

    function updateMosaicity(obj, varargin)
    % GTTWINANALYSIS/UPDATEMOSAICITY
    %
    % updateMosaicity(obj, varargin)

        if (isempty(varargin))
            vars_pars.extend  = false;
            vars_pars.etacorr = true;
            vars_list = cell(0, 4);
            vars_list(end+1, :) = {'extend', 'Use extstartimage and extendimage from difspot table instead of startimage and endimage', 'logical', 1};
            vars_list(end+1, :) = {'etacorr', 'Use eta correction', 'logical', 1};
            varargin = struct2pv(vars_pars);
        else
            vars_pars = struct(varargin{:});
        end

        check = questdlg('Do you want to compute again the Mosaicity?', 'Mosaicity computation:', 'Yes', 'No', 'Yes');
        if (strcmp(check, 'Yes')) || isempty(obj.g_mosaicity)
            vars_pars = gtModifyStructure(vars_pars, vars_list, 1, 'Setting options for computing mosaicity:');
            varargin = struct2pv(vars_pars);
            obj.computeGrainsMosaicity(varargin{:});
        end
    end

    function computeGrainsMosaicity(obj, varargin)
    % GTTWINANALYSIS/COMPUTEGRAINSMOSAICITY  Computes omega spread for each
    % segmented spot
    % extend  : compute also ext-extend mosaicity {false}
    % etacorr : eta correction {true} (if true, using indexed pairs)
    %           (ExtEndImage - ExtStartImage)
    %
    % computeGrainsMosaicity(obj, varargin)

        par.extend = false;
        par.etacorr = [];
        [par, ~] = parse_pv_pairs(par, varargin);

        if isempty(obj.spotTable)
            obj.loadDifspotTable();
        end
        if isempty(obj.pairTable)
            obj.loadPairTable();
        end
        if isempty(obj.g_difspots) || isempty(obj.g_flags) || isempty(obj.g_allblobs)
            obj.resetAllGrainVars();
        end

        obj.spotTable.maxImage = max(obj.spotTable.EndImage);
        obj.spotTable.maxExtImage = max(obj.spotTable.ExtEndImage);
        % image spread
        obj.spotTable.ImSpread = obj.spotTable.EndImage - obj.spotTable.StartImage;
        if any(obj.spotTable.ImSpread < 0)
            obj.spotTable.ImSpread(obj.spotTable.ImSpread < 0) = obj.spotTable.ImSpread(obj.spotTable.ImSpread < 0) + obj.spotTable.maxImage;
        end
        % ext image spread
        obj.spotTable.ExtImSpread = obj.spotTable.ExtEndImage - obj.spotTable.ExtStartImage;
        if any(obj.spotTable.ExtImSpread < 0)
            obj.spotTable.ExtImSpread(obj.spotTable.ExtImSpread < 0) = obj.spotTable.ExtImSpread(obj.spotTable.ExtImSpread < 0) + obj.spotTable.maxExtImage;
        end

        % reset mosaicity
        obj.g_mosaicity = cell(3, obj.Ngrains);

        disp(' ')
        fprintf('  Mosaicity for grains        : ')
        if (isempty(par.etacorr))
            maps = gtMosaicityCmaps([], obj.Ngrains, obj.spotTable, obj.pairTable, false);
            maps2 = gtMosaicityCmaps([], obj.Ngrains, obj.spotTable, obj.pairTable, true);
            if (~par.extend)
                obj.g_mosaicity(1, :) = maps.imsp;
                obj.g_mosaicity(2, :) = maps2.imsp;
            else
                obj.g_mosaicity(1, :) = maps.extimsp;
                obj.g_mosaicity(2, :) = maps2.extimsp;
            end
        else
            maps = gtMosaicityCmaps([], obj.Ngrains, obj.spotTable, obj.pairTable, par.etacorr);
            if (~par.extend)
                if (par.etacorr)
                    obj.g_mosaicity(2, :) = maps.imsp;
                else
                    obj.g_mosaicity(1, :) = maps.imsp;
                end
            else
                if (par.etacorr)
                    obj.g_mosaicity(2, :) = maps.extimsp;
                else
                    obj.g_mosaicity(1, :) = maps.extimsp;
                end
            end
        end
        for gID = 1:obj.Ngrains
            % get values for each grain
            if (~par.extend)
                obj.g_mosaicity{3, gID} = obj.spotTable.ImSpread(obj.g_difspots{gID});
            else
                obj.g_mosaicity{3, gID} = obj.spotTable.ExtImSpread(obj.g_difspots{gID});
            end
        end
        fprintf('done!\n')
        disp('    ImSpread calculation      : done!')
        disp('    ExtImSpread calculation   : done!')
        if isempty(par.etacorr) || (~isempty(par.etacorr) && par.etacorr)
            disp('    Eta correction            : yes')
        else
            disp('    Eta correction            : no')
        end
    end

    function updateSchmidFactors(obj, varargin)
    % GTTWINANALYSIS/UPDATESCHMIDFACTORS
    %
    % updateSchmidFactors(obj, varargin)
        vars_pars.useMTex    = true;
        vars_pars.loadDir    = obj.loadDir;
        vars_pars.convention = 'X';

        vars_list = get_structure(vars_pars, [], false, [], []);
        vars_list(:, 2) = {'Use MTex toolbox calculation for Schmid factors if available?', ...
            'Loading direction for computing Schmid factors: use sign ''-'' to indicate compression (1x3)', ...
            'Hcp crystal axes convention {''X''} | ''Y'''};

        if isempty(varargin)
            varargin = struct2pv(vars_pars);
        else
            vars_pars = struct(varargin{:});
        end

        check = questdlg('Do you want to compute again the Schmid factors?', 'Schmid factors computation:', 'Yes', 'No', 'Yes');
        if (strcmp(check, 'Yes')) || isempty(obj.schmidFactors) || ...
            ~isfield(obj.schmidFactors, 'taugrain') || ~isfield(obj.schmidFactors, 'schmids')
            vars_pars = gtModifyStructure(vars_pars, vars_list, 1, 'Options for computing Schmid factors:');
            varargin = struct2pv(rmfield(vars_pars, {'useMTex'}));
            obj.computeSchmidFactors(varargin{:});
            if (vars_pars.useMTex)
                obj.computeMTexSchmidF(varargin{:});
            end
        end
    end

    function computeSchmidFactors(obj, varargin)
    % GTTWINANALYSIS/COMPUTESCHMIDFACTORS  Compute Schmid factors depending on the given loading direction.
    %
    % loadDir    : sample CS loading direction (1x3) {[0 0 1]}
    % orimat     : orientation matrices as gtMathsRod2OriMat output (3x3xN)
    % g_phase    : phaseID for each grain {[]}
    % convention : hcp crystal axes convention {'X'}
    %   HexaSchmid software:
    %   https://www.tu-berlin.de/metallischewerkstoffe/menue/forschung/parameter/maxhilfe/
    %
    % computeSchmidFactors(obj, varargin)

        par.loadDir    = [];
        par.orimat     = [];
        par.g_phase    = [];
        par.convention = 'X';
        [par, rej_pars] = parse_pv_pairs(par, varargin);

        if isempty(par.loadDir)
            par.loadDir = obj.loadDir;
        end
        if ~isempty(par.orimat)
            nof_grains = size(par.orimat, 1)/3;
        else
            par.orimat = obj.orientations;
            nof_grains = obj.Ngrains;
        end
        if isempty(par.orimat)
            g_phase = ones(1, nof_grains);
        else
            g_phase = obj.phaseIDs;
        end

        % reset schmid factors
        obj.schmidFactors = [];

        % normalize loading direction
        obj.loadDir = par.loadDir / norm(par.loadDir);

        disp(' ')
        fprintf('  Loading direction Sample CS : [%d %d %d]\n', obj.loadDir)
        Bmat = @(iphase) gtCrystHKL2CartesianMatrix(obj.p_latticepar{iphase});
        % convert loadDir to crystal CS
        loadCryst = gtVectorLab2Cryst(obj.loadDir, par.orimat);
        if strcmpi(par.convention, 'Y')
            % rotates counterclockwise the sample CS
            loadCryst = rotateVectors(loadCryst, 'angle', 30, 'axis', [0 0 1]);
        end
        obj.schmidFactors.loadCryst = loadCryst';

        % get list of slip systems
        getSlipSystems = @(x) struct2cell( obj.p_slipSystems{x} );

        fprintf('  Computing Schmid factors    : ')

        for gID = 1:nof_grains
            iphase = g_phase(gID);
            slipSystems = getSlipSystems(iphase);

            for ifam = 1:length(slipSystems)
                syst = slipSystems{ifam};
                schmid = zeros(length(syst), 1);
                c_phi = zeros(length(syst), 1);
                c_lambda = zeros(length(syst), 1);

                for isyst = 1:length(syst)
                    syst(isyst).num = isyst;
                    syst(isyst).fam = obj.p_slipFamilies{iphase}{ifam};
                    % slip plane normal and slip direction in crystal CS
                    syst(isyst).hkl = gtCrystHKL2Cartesian(syst(isyst).n', Bmat(iphase))';
                    syst(isyst).uvw = gtCrystHKL2Cartesian(syst(isyst).l', Bmat(iphase))';
                    % angle between loading direction and slip plane
                    % normal
%                     cosine = @(x, y) dot(x, y) / (norm(x)*norm(y));
%                     cosine2 = @(x, y) (x*G*y') / (sqrt(x*G*x')*sqrt(y*G*y'));
                    cosine3 = @(x, y) cosd(rad2deg(vectorAngle3d(x, y)));
                    cosphi  = cosine3(loadCryst(gID, :), syst(isyst).hkl);
                    % angle between loading direction and slip
                    % direction
                    coslambda = cosine3(loadCryst(gID, :), syst(isyst).uvw);
                    % schmid factor is the product of the two cosines
                    schmid(isyst, 1) = cosphi * coslambda;
                    c_phi(isyst, 1) = cosphi;
                    c_lambda(isyst, 1) = coslambda;
                end

                % take unique
                u_schmid = unique(abs(single(schmid)), 'stable');
                % store results
                obj.schmidFactors.schmids{ifam, iphase}(:, gID) = schmid;
                obj.schmidFactors.cosphi{ifam, iphase}(:, gID) = c_phi;
                obj.schmidFactors.coslambda{ifam, iphase}(:, gID) = c_lambda;

                obj.schmidFactors.u_schmids{ifam, iphase}(:, gID) = u_schmid;
                obj.schmidFactors.max_schmids{ifam, iphase}(:, gID) = u_schmid(ismember(abs(u_schmid), max(abs(u_schmid))));

                obj.schmidFactors.slipSystems{ifam, iphase} = syst;
            end
        end

        fprintf('done!\n')
        fprintf('    Slip families generated   : %d\n', length(vertcat(obj.p_slipFamilies{:})))
        fprintf('    Slip systems computed     : %d\n', length([obj.schmidFactors.slipSystems{:}]))
        disp(' ')
    end

    function computeMTexSchmidF(obj, varargin)
    % GTTWINANALYSIS/COMPUTEMTEXSCHMIDF  Uses the MTex toolbox to compute
    % the Schmid factor for all the grains, given an applied stress tensor
    %
    % M : stress (3x3)
    %
    % computeMTexSchmidF(obj, varargin)

        params = obj.getParams();
        phases_num = numel(params.cryst);

        par.convention = 'X';
        par.M          = [];
        par = parse_pv_pairs(par, varargin);

        if (isempty(par.M))
            par.M = zeros(3);
            par.M(1, 1) = obj.loadDir(1);
            par.M(2, 2) = obj.loadDir(2);
            par.M(3, 3) = obj.loadDir(3);
        end
        if (strcmp(par.convention, 'X'))
            Cref = 'X||a';
        elseif (strcmp(par.convention, 'Y'))
            Cref = 'X||a*';
        end
         % Define applied stress in sample reference
        sigmaS = tensor(par.M, 'name', 'stress'); % stress parallel to Z specimen axis

        obj.schmidFactors.taugrain = [];
        obj.schmidFactors.tauMaxgrain = [];

        % get list of slip systems
        getSlipSystems = @(x) struct2cell( obj.p_slipSystems{x} );
        g_grains = obj.orientations;

        fprintf('  MTex calculation of SchmidF : ')
        for ii_ph = 1:phases_num
            gID = obj.p_grainIDs{ii_ph};

            % CS = symmetry('spacegroup', params.cryst(iphase).spacegroup, 'X||a');
            CS = symmetry(obj.p_crystalsystem{ii_ph}, obj.p_latticepar{ii_ph}(1:3), obj.p_latticepar{ii_ph}(4:6)/degree, Cref);
            origrain = orientation('matrix', g_grains(:, :, gID), CS);

            % Calculate applied stress in crystal coordinate system for each grain
            sigmaCSgrain = rotate(sigmaS, inverse(origrain));
            obj.schmidFactors.sigmaCryst = sigmaCSgrain;

            % Define slip system for analysis
            isyst = 1;
            slipSystems = getSlipSystems(ii_ph);

            for ifam = 1:length(slipSystems)
                syst = slipSystems{ifam};

                hkl = syst(isyst).n;
                uvw = syst(isyst).l;
                m = Miller(hkl(1), hkl(2), hkl(3), hkl(4), CS, 'hkl'); % normal to the slip plane
                n = Miller(uvw(1), uvw(2), uvw(3), uvw(4), CS, 'uvw'); % slip direction in the slip plane

                % calculate Schmid factors for each grain
                [~, tauMaxgrain, ~, ~, taugrain] = gtCalcShearStress(sigmaCSgrain, m, n, 'symmetrise');

                % store results
                obj.schmidFactors.taugrain{ifam, ii_ph}    = taugrain;
                obj.schmidFactors.tauMaxgrain{ifam, ii_ph} = tauMaxgrain;
            end
        end
        fprintf('done!\n')

        % update loadDir
        obj.loadDir = par.M([1 5 9]);
    end

    function loadOutput(obj, filename)
    % GTTWINANALYSIS/LOADOUTPUT  Loads the object on disk
    %
    % loadOutput(obj, filename)

        if (~exist('filename', 'var') || isempty(filename))
            filename = fullfile(obj.dataDir, 'twinAnalysis.mat');
        end
        if (exist(filename, 'file'))
            tmp = load(filename);
            fields = fieldnames(tmp);
            for ii=1:length(fields)
                if isprop(obj, fields{ii})
                    obj.(fields{ii}) = tmp.(fields{ii});
                end
            end
            disp(['  Loaded GtTwinAnalysis output from ' filename])
        end
    end

    function saveOutput(obj)
    % GTTWINANALYSIS/SAVEOUTPUT  Saves the object on disk
    %
    % saveOutput(obj)

        fields = fieldnames(obj);
        fields([40 41 42 43 45:end]) = [];
        tmp = [];
        for ii = 1:length(fields)
            tmp.(fields{ii}) = obj.(fields{ii});
        end
        save('twinAnalysis.mat', '-struct', 'tmp')
    end

    function changeGrainCmap(obj, typemap, overwrite, varargin)
    % GTTWINANALYSIS/CHANGEGRAINCMAP  Changes the color map for the current
    % volume for all the grains
    %
    % changeGrainCmap(obj, typemap, overwrite, varargin)

        min_radio = get(obj.conf.h_cmap_butt(1), 'Min');
        max_radio = get(obj.conf.h_cmap_butt(1), 'Max');

        if (isempty(typemap))
            [typemap, current_selected] = obj.getCurrentCmap();
        else
            current_selected = find(strcmpi(arrayfun(@(x){get(x, 'String')}, obj.conf.h_cmap_butt), typemap), 1, 'first');
            if (isempty(current_selected))
                error('changeGrainCmap:wrong_parameter', ...
                    'Unknown colormap "%s"', typemap)
            end
        end

        if (~exist('overwrite', 'var') || isempty(overwrite))
            overwrite = true;
        end

        if (~isempty(typemap))
            obj.cmap.defname = typemap;

            switch (typemap)
                case 'ID'
                    if (isempty(obj.grainIDColorMap) || overwrite || ~isempty(varargin))
                        obj.createGrainIDColorMap(varargin{:});
                    end
                    obj.reloadAssebledVolume('CMap', obj.grainIDColorMap, 'default', true);

                case 'Rand'
                    fprintf('Changing to Random colormap\n')
                    if (isempty(obj.grainRandColorMap) || overwrite)
                        obj.createGrainRandColorMap();
                    end
                    obj.reloadAssebledVolume('CMap', obj.grainRandColorMap, 'default', true);

                case 'Volume'
                    fprintf('Changing to Volume colormap\n')
                    if (isempty(obj.grainVolumeColorMap) || overwrite || ~isempty(varargin))
                        obj.createGrainVolumeColorMap(varargin{:});
                    end
                    obj.reloadAssebledVolume('CMap', obj.grainVolumeColorMap, 'default', true);

                case 'Completeness'
                    fprintf('Changing to Completeness colormap\n')
                    if (isempty(obj.grainCompletenessColorMap) || overwrite || ~isempty(varargin))
                        obj.createGrainCompletenessColorMap(varargin{:});
                    end
                    obj.reloadAssebledVolume('CMap', obj.grainCompletenessColorMap, 'default', true);

                case 'Caxis'
                    fprintf('Changing to Caxis colormap\n')
                    if (isempty(obj.grainCaxisColorMap) || overwrite || ~isempty(varargin))
                        obj.createGrainCaxisColorMap(varargin{:})
                    end
                    obj.reloadAssebledVolume('CMap', obj.grainCaxisColorMap, 'default', true);

                case 'IPF'
                    fprintf('Changing to IPF colormap\n')
                    if (isempty(obj.grainIPFColorMap) || overwrite || ~isempty(varargin))

                        obj.createGrainIPFColorMap(varargin{:})
                    end
                    obj.reloadAssebledVolume('CMap', obj.grainIPFColorMap, 'default', true);

                case 'Rvec'
                    fprintf('Changing to Rvectors colormap\n')
                    if (isempty(obj.grainRvecColorMap) || overwrite)
                        obj.createGrainRvecColorMap();
                    end
                    obj.reloadAssebledVolume('CMap', obj.grainRvecColorMap, 'default', true);

                case 'SchmidF'
                    fprintf('Changing to Schmid factor colormap\n')
                    if (isempty(obj.grainSchmidFColorMap) || overwrite || ~isempty(varargin))
                        obj.updateSchmidFactors(varargin{:})
                        obj.createGrainSchmidFColorMap(varargin{:})
                    end
                    obj.reloadAssebledVolume('CMap', obj.grainSchmidFColorMap, 'default', true);

                case 'Mosaicity'
                    fprintf('Changing to mosaicity colormap\n')
                    if (isempty(obj.grainMosaicityColorMap) || overwrite || ~isempty(varargin))
                        obj.updateMosaicity(varargin{:})
                        obj.createGrainMosaicityColorMap(varargin{:})
                    end
                    obj.reloadAssebledVolume('CMap', obj.grainMosaicityColorMap, 'default', true);
                case 'Annealing'
                    fprintf('Changing to annealing twin cluster colormap\n')
                    if (isempty(obj.grainAnnealingColorMap) || overwrite || ~isempty(varargin))
                        obj.createGrainAnnealingColorMap(varargin{:})
                    end
                    obj.reloadAssebledVolume('CMap', obj.grainAnnealingColorMap, 'default', true);

                otherwise
                    disp('Color map name is not known...')
                    typemap = '';
                    return
            end

            % updates the multiple selection of colormaps on GUI
            for ii = [1:current_selected-1, current_selected+1:numel(obj.conf.h_cmap_butt)]
                set(obj.conf.h_cmap_butt(ii), 'Value', min_radio);
            end
            set(obj.conf.h_cmap_butt(current_selected), 'Value', max_radio);

            % updates info into colormap structure
            obj.cmap.defname = typemap;
            if (ismember(obj.cmap.defname, {'Caxis', 'Rvec', 'IPF'}))
                obj.cmap.par_values = obj.cmap.values;
            end
        end
    end

    function createGrainIDColorMap(obj, varargin)
    % GTTWINANALYSIS/CREATEGRAINIDCOLORMAP  Creates a color map for grain labels
    %
    % createGrainIDColorMap(obj, varargin)

        fprintf('  grain ID color map          : ')
        maxV = max(obj.grainIDs(:));
        map = obj.makeColorMap('values', obj.grainIDs, 'maxV', maxV, varargin{:});
        obj.grainIDColorMap = map;
        fprintf('done!\n')
    end

    function createGrainRandColorMap(obj)
    % GTTWINANALYSIS/CREATEGRAINRANDCOLORMAP  Creates a random color map
    % for grain labels
    %
    % createGrainRandColorMap(obj)

        fprintf('  Random color map            : ')
        obj.grainRandColorMap = gtRandCmap(obj.Ngrains, obj.cmap.bkg);
        fprintf('done!\n')
    end

    function createGrainVolumeColorMap(obj, varargin)
    % GTTWINANALYSIS/CREATEGRAINVOLUMECOLORMAP  Creates a color map for grain volume
    %
    % createGrainVolumeColorMap(obj, varargin)

        fprintf('  Grain volume color map      : ')
        g_volumes = arrayfun(@(x) sum(obj.conf.vol(:)==x), 1:obj.Ngrains)';
        maxV = max(g_volumes(:));
        map = obj.makeColorMap('values', g_volumes, 'maxV', maxV, varargin{:});
        obj.grainVolumeColorMap = map;
        fprintf('done!\n')
    end

    function createGrainCompletenessColorMap(obj, varargin)
    % GTTWINANALYSIS/CREATEGRAINCOMPLETENESSCOLORMAP  Creates a color map for grain completeness
    %
    % createGrainCompletenessColorMap(obj, varargin)

        fprintf('  Grain completeness color map      : ')
        maxV = 1;
        map = obj.makeColorMap('values', obj.completeness, 'maxV', maxV, varargin{:});
        obj.grainCompletenessColorMap = map;
        fprintf('done!\n')
    end

    function createGrainCaxisColorMap(obj, varargin)
    % GTTWINANALYSIS/CREATEGRAINCAXISCOLORMAP  Creates a color map for
    % grains given a crystallographic direction
    %
    % createGrainCaxisColorMap(obj, varargin)

        vars_pars.crystDir       = obj.crystDir;
        vars_pars.phaseid        = max(1, obj.selectedPhase);
        vars_pars.axesRot        = [1 2 3];
        vars_pars.background     = obj.cmap.hasBkg;
        vars_pars.save           = false;
        vars_pars.convention     = 'X'; % to double-check with assemble parameter
        rej_pars = [];
        if (~isempty(obj.extras.vars_pars))
            [vars_pars, rej_pars] = parse_pv_pairs(vars_pars, struct2pv(obj.extras.vars_pars));
        end

        if (isempty(varargin))
            vars_list = get_structure(vars_pars, [], false, [], []);
            vars_list(:, 2) = {'Sample direction in the sample CS for CrystAxis calculation (1x3)', ...
                'Current phase', ...
                'Sample axes permutation (1x3)', ...
                'Save colormap on disk?', ...
                'Do we want the background color in the cmap?', ...
                'Hcp crystal axes convention {''X''} | ''Y'''};
            ind = findValueIntoCell(fieldnames(vars_pars), {'background', 'convention'});
            vars_list(ind(:, 1), 4) = {2};

            vars_pars = gtModifyStructure(vars_pars, vars_list, 1, 'Options for Cryst axis colormap:');
        else
            [vars_pars, rej_pars] = parse_pv_pairs(vars_pars, varargin);
        end
        varargin = struct2pv(rmfield(vars_pars, {'crystDir', 'phaseid'}));

        fprintf('  Crystallographic direction  : [%s]\n', strrep(num2str(vars_pars.crystDir), '  ', ' '))
        obj.updatePhaseInfo(vars_pars.crystDir, 'crystDir', vars_pars.phaseid);
        obj.setVolumeToLoad('phase', 'reload', false);

        [obj.cmap.values, obj.grainCaxisColorMap] = gtCrystAxisPoles(obj.selectedPhase, obj.crystDir, ...
            'crystal_system', obj.p_crystalsystem{obj.selectedPhase}, ...
            'latticepar', obj.p_latticepar{obj.selectedPhase}, ...
            'r_vectors', obj.r_vectors.', varargin{:});
        fprintf('    Cryst axis color map      : done!\n')

        obj.extras.vars_pars = vars_pars;
        if (~isempty(rej_pars))
            rej_pars = cell2struct(rej_pars(2:2:end), rej_pars(1:2:end), 2);
        end
        obj.extras.rej_pars = rej_pars;
    end

    function createGrainIPFColorMap(obj, varargin)
    % GTTWINANALYSIS/CREATEGRAINIPFCOLORMAP  Creates a color map for grain
    % inverse pole figure for LD direction
    %
    % createGrainIPFColorMap(obj, varargin)

        vars_pars = struct( ...
            'sampleDir', obj.sampleDir, ...
            'phaseid', max(1, obj.selectedPhase), ...
            'saturate', true, ...
            'save', false, ...
            'background', obj.cmap.hasBkg );
        rej_pars = [];
        if (~isempty(obj.extras.vars_pars))
            [vars_pars, rej_pars] = parse_pv_pairs(vars_pars, struct2pv(obj.extras.vars_pars));
        end

        if (isempty(varargin))
            vars_list = get_structure(vars_pars, [], false, [], []);
            vars_list(:, 2) = {'Sample direction in the sample CS for IPF calculation', ...
                'Current phase', ...
                'Saturate colors in the IPF map', ...
                'Save colormap on disk?', ...
                'Do we want the background color in the cmap?'};

            vars_pars = gtModifyStructure(vars_pars, vars_list, 1, 'Options for IPF colormap:');
        else
            [vars_pars, rej_pars] = parse_pv_pairs(vars_pars, varargin);
        end
        varargin = struct2pv(rmfield(vars_pars, {'sampleDir', 'phaseid'}));

        fprintf('  Sample direction            : [%d %d %d]\n', vars_pars.sampleDir)
        obj.updatePhaseInfo(vars_pars.sampleDir, 'sampleDir', vars_pars.phaseid);
        obj.setVolumeToLoad('phase', 'reload', false);

        [obj.grainIPFColorMap, obj.cmap.values] = gtIPFCmap(vars_pars.phaseid, ...
            vars_pars.sampleDir, ...
            'r_vectors', obj.r_vectors.', ...
            'crystal_system', obj.p_crystalsystem{vars_pars.phaseid}, ...
            varargin{:});
        fprintf('    IPF color map             : done!\n')

        obj.extras.vars_pars = vars_pars;
        if (~isempty(rej_pars))
            rej_pars = cell2struct(rej_pars(2:2:end), rej_pars(1:2:end), 2);
        end
        obj.extras.rej_pars = rej_pars;
    end

    function createGrainRvecColorMap(obj)
    % GTTWINANALYSIS/CREATEGRAINRVECCOLORMAP  Creates a color map for
    % rvectors
    %
    % createGrainRvecColorMap(obj)

        fprintf('  Rvectors color map          : ')
        obj.grainRvecColorMap = gtRvectorCmap([], obj.r_vectors.');
        obj.cmap.values = obj.r_vectors.';
        fprintf('done!\n')
    end

    function createGrainSchmidFColorMap(obj, varargin)
    % GTTWINANALYSIS/CREATEGRAINSCHMIDFCOLORMAP  Creates a color map for grain schmid factor for LD direction.
    % By default use MTex computation if possible and find the maximum Schmid factor
    % value for each slip family (among the symmetry equivalents)
    %
    % createGrainSchmidFColorMap(obj, varargin)

        vars_pars.fam       = 1;
        vars_pars.syst      = 1;
        vars_pars.phaseid   = max(1, obj.selectedPhase);
        vars_pars.useMTex   = true;
        vars_pars.maxSystem = true;
        rej_pars = [];
        if (~isempty(obj.extras.vars_pars))
            [vars_pars, rej_pars] = parse_pv_pairs(vars_pars, struct2pv(obj.extras.vars_pars));
        end

        if (isempty(varargin))
            vars_list = get_structure(vars_pars, [], false, [], []);
            vars_list(:, 2) = {'Slip family number for this crystal', ...
                'Slip system number over the equivalent', ...
                'Current phase', ...
                'Use MTex toolbox calculation for Schmid factors if available?', ...
                'Consider only maximum values of Schmid factors for each slip family?'};

            vars_pars = gtModifyStructure(vars_pars, vars_list, 1, 'Options for Schmid factors colormap:');
            if (~vars_pars.maxSystem)
                vars_pars = gtModifyStructure(vars_pars, vars_list, 2, 'Options for Schmid factors colormap:');
            end
        else
            [vars_pars, rej_pars] = parse_pv_pairs(vars_pars, varargin);
            varargin = cell2struct(obj.extras.rej_pars(2:2:end), obj.extras.rej_pars(1:2:end), 2);
        end
        if (~isfield(obj.schmidFactors, 'taugrain') || isempty(obj.schmidFactors.taugrain))
            vars_pars.useMTex = false;
        end

        disp(' ')
        fprintf('  Schmid factor color map     : ')
        if (vars_pars.useMTex)
            f_schmids = obj.schmidFactors.taugrain;
        else
            f_schmids = obj.schmidFactors.schmids;
        end
        schmids = f_schmids{vars_pars.fam, vars_pars.phaseid};
        maxV = 0.5;
        map = obj.makeColorMap('values', schmids, ...
              'maxV', maxV, 'maxSystem', vars_pars.maxSystem, varargin{:});
        obj.grainSchmidFColorMap = map;
        totSlip = size(schmids, 1);
        fprintf('done!\n')

        if (vars_pars.useMTex)
            fprintf('    MTex calculation          : yes\n')
        else
            fprintf('    MTex calculation          : no\n')
        end
        fprintf('  Slip systems used           : %d\n', totSlip)
        fprintf('  Slip family name            : %s\n', obj.schmidFactors.slipSystems{vars_pars.fam, vars_pars.phaseid}(vars_pars.syst).fam)
        if (vars_pars.maxSystem)
            fprintf('  Slip system mode {hkl}<uvw> : {%s}<%s>\n', num2str(obj.schmidFactors.slipSystems{vars_pars.fam, vars_pars.phaseid}(vars_pars.syst).n), ...
              num2str(obj.schmidFactors.slipSystems{vars_pars.fam, vars_pars.phaseid}(vars_pars.syst).l) )
            fprintf('  Maximum Schmid factor values for each family considered\n')
        else
            fprintf('  Slip system mode (hkl)[uvw] : (%s)[%s]\n', num2str(obj.schmidFactors.slipSystems{vars_pars.fam, vars_pars.phaseid}(vars_pars.syst).n), ...
              num2str(obj.schmidFactors.slipSystems{vars_pars.fam, vars_pars.phaseid}(vars_pars.syst).l) )
            fprintf('  Slip system number          : %d / %d\n', vars_pars.syst, size(obj.schmidFactors.schmids{vars_pars.fam, vars_pars.phaseid}, 1))
        end

        obj.extras.vars_pars = vars_pars;
        if (~isempty(rej_pars))
            rej_pars = cell2struct(rej_pars(2:2:end), rej_pars(1:2:end), 2);
        end
        obj.extras.rej_pars  = rej_pars;
    end

    function createGrainMosaicityColorMap(obj, varargin)
    % GTTWINANALYSIS/CREATEGRAINMOSAICITYCOLORMAP  Create a color map for
    % each grain omega spread as average of each difspot assigned to it
    %
    % createGrainMosaicityColorMap(obj, varargin)

        vars_pars.func    = 'median';
        vars_pars.etacorr = true;
        vars_pars.pairs   = true;
        rej_pars = [];
        if ~isempty(obj.extras.vars_pars)
            [vars_pars, rej_pars] = parse_pv_pairs(vars_pars, struct2pv(obj.extras.vars_pars));
        end

        if isempty(varargin)
            vars_list = get_structure(vars_pars, [], false, [], []);
            vars_list(:, 2) = {'Function handle to compute mosaicity for each grain {''median''}', ...
                'Use computed values using the eta correction', ...
                'Consider only the spot pairs list, instead of adding single spots'};

            vars_pars = gtModifyStructure(vars_pars, vars_list, 1, 'Options for Mosaicity colormap:');
        else
            [vars_pars, rej_pars] = parse_pv_pairs(vars_pars, varargin);
            varargin = rej_pars;
        end
        func = obj.cmap.func(vars_pars.func);

        disp(' ')
        fprintf('  Grain omega spread cmap     : ')
        mosaicity = cellfun(@(x) func(x), obj.g_mosaicity)';
        if (vars_pars.etacorr)
            mosaicity = mosaicity(:, 2);
        else
            if (vars_pars.pairs)
                mosaicity = mosaicity(:, 1);
            else
                mosaicity = mosaicity(:, 3);
            end
        end
        maxV = max(mosaicity(:));
        map = obj.makeColorMap('values', mosaicity, 'maxV', maxV, varargin{:});
        obj.grainMosaicityColorMap = map;
        fprintf('done!\n')

        if (vars_pars.etacorr)
            fprintf('    Eta correction            : yes\n')
            fprintf('    Indexed pairs used only   : yes\n')
        elseif (vars_pars.pairs)
            fprintf('    Eta correction            : no\n')
            fprintf('    Indexed pairs used only   : yes\n')
        else
            fprintf('    Eta correction            : no\n')
            fprintf('    Indexed pairs used only   : no\n')
        end
        fprintf('    Function used             : %s\n', char(func))

        obj.extras.vars_pars = vars_pars;
        if ~isempty(rej_pars), rej_pars = cell2struct(rej_pars(2:2:end), rej_pars(1:2:end), 2); end
        obj.extras.rej_pars  = rej_pars;
    end

    function createGrainAnnealingColorMap(obj, varargin)
    % GTTWINANALYSIS/CREATEGRAINANNEALINGCOLORMAP  Create a color map for
    % annealing twin clusters
    %
    % createGrainAnnealingColorMap(obj, varargin)

        disp('  ')
        fprintf('  Annealing twin clusters cmap: ')
        vars_pars = struct( ...
            'phaseid', max(1, obj.selectedPhase));
        map = obj.grainRandColorMap;%obj.grainRandColorMap = gtRandCmap(obj.Ngrains, obj.cmap.bkg);
        clusters_annealing = GtSample.loadFromFile;
        clusters_annealing = clusters_annealing.phases{vars_pars.phaseid}.clusters;
        for i_tmp = 1:length(clusters_annealing)
            if clusters_annealing(i_tmp).cluster_type == 3
                tmp_nof_twins = size(clusters_annealing(i_tmp).included_ids, 2);
                map(clusters_annealing(i_tmp).included_ids(1, 2:tmp_nof_twins) + 1, :, :) = ...
                    map((clusters_annealing(i_tmp).included_ids(1, 1) + 1) * ones(tmp_nof_twins - 1, 1), :, :);
            end
        end
        obj.grainAnnealingColorMap = map;
        fprintf('done!\n')
    end

    function createAllColorMaps(obj, overwrite)
    % GTTWINANALYSIS/CREATEALLCOLORMAPS  Generate all the color maps need for the study
    %
    % createAllColorMaps(obj, overwrite)

        if ~exist('overwrite', 'var') || isempty(overwrite)
            overwrite = false;
        end

        if isempty(obj.grainIDColorMap) || (overwrite)
            obj.createGrainIDColorMap();
        end
        if isempty(obj.grainRandColorMap) || (overwrite)
            obj.createGrainRandColorMap();
        end
        if isempty(obj.grainVolumeColorMap) || (overwrite)
            obj.createGrainVolumeColorMap();
        end
        if isempty(obj.grainCompletenessColorMap) || (overwrite)
            obj.createGrainCompletenessColorMap();
        end
        if isempty(obj.grainCaxisColorMap) || (overwrite)
            obj.createGrainCaxisColorMap();
        end
        if isempty(obj.grainIPFColorMap) || (overwrite)
            obj.createGrainIPFColorMap();
        end
        if isempty(obj.grainRvecColorMap) || (overwrite)
            obj.createGrainRvecColorMap();
        end
        if isempty(obj.grainSchmidFColorMap) || (overwrite)
            obj.createGrainSchmidFColorMap();
        end
        if isempty(obj.grainMosaicityColorMap) || (overwrite)
            obj.createGrainMosaicityColorMap();
        end
        if isempty(obj.grainAnnealingColorMap) || (overwrite)
            obj.createGrainAnnealingColorMap();
        end
    end

end % end of public methods

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% PROTECTED METHODS
methods (Access = protected)

    function initGui(obj)
    % GTTWINANALYSIS/INITGUI  Inherited method that builds the interface
    %
    % initGui(obj)

        initGui@GtGrainsManager(obj)

        % Let's disable zoom, while configuring
        zoomOp = zoom(obj.conf.h_figure);
        zoomState = get(zoomOp, 'Enable');
        zoom(obj.conf.h_figure, 'off');

        % add stuff

        % Select grains button
        obj.conf.h_select_butt = uicontrol('Style', 'pushbutton', ...
                                          'String', 'Select/Deselect', ...
                                          'Parent', obj.conf.side_g_boxes, ...
                                          'Callback', ...
                                          @(src, evt)selectGrainsToVisualize(src, evt, obj));

        obj.conf.main_t_boxes = uiextras.HBox('Parent', obj.conf.currentParent);
        obj.conf.side_t_boxes = uiextras.VBox('Parent', obj.conf.main_t_boxes);
        obj.conf.center_t_boxes = uipanel('Parent', obj.conf.main_t_boxes, 'BorderType', 'line', 'BorderWidth', 3);
        set(obj.conf.main_t_boxes, 'Sizes', [160 -1]);

        % add axis for colorbar
        obj.conf.h_cbar_box   = uipanel('Parent', obj.conf.center_boxes, ....
            'Tag', 'h_cbar_box', 'BorderType', 'line');
        obj.conf.h_cbar_ax = axes('Parent', obj.conf.h_cbar_box, ...
            'ActivePositionProperty', 'OuterPosition', ...
            'Tag', 'h_cbar_ax', 'Visible', 'off');
        obj.conf.h_cbar_im = copyobj(obj.conf.h_im, obj.conf.h_cbar_ax);
        set(obj.conf.h_cbar_im, 'Visible', 'off', 'Tag', 'h_cbar_im');
        set(obj.conf.center_boxes, 'Sizes', [20 20 -1 0]);

        % change parents
        set(obj.conf.main_g_boxes, 'Parent', obj.conf.center_t_boxes);
        set(obj.conf.main_boxes, 'Parent', obj.conf.h_viewer_panel);
        set(obj.conf.h_ax, 'Tag', 'h_ax');
        set(obj.conf.h_im, 'Tag', 'h_im');

        % add spacer
        uicontrol('Parent', obj.conf.side_t_boxes, 'Enable', 'off', ...
            'Visible', 'off', 'Tag', 'spacer');

        % side panel : buttons + panel + query
        obj.conf.cmap_t_buttons = uiextras.VButtonBox( ...
            'Parent', obj.conf.side_t_boxes);

        % Change cmap options
        obj.conf.h_cmap_set_butt = uicontrol('Style', 'pushbutton', ...
                                          'String', 'Set Cmap Options', ...
                                          'Parent', obj.conf.cmap_t_buttons);
        % Show supported cmaps
        obj.conf.h_cmap_show_butt = uicontrol('Style', 'pushbutton', ...
                                          'String', 'Supported Cmap', ...
                                          'Parent', obj.conf.cmap_t_buttons);
        % Advanced Colormap Editor
        obj.conf.h_cmap_edit_butt = uicontrol('Style', 'pushbutton', ...
                                          'String', 'Open Cmap Editor', ...
                                          'Parent', obj.conf.cmap_t_buttons);
        % Save cmap figure
        obj.conf.h_cmap_save_butt = uicontrol('Style', 'pushbutton', ...
                                          'String', 'Save Cmap Figure', ...
                                          'Parent', obj.conf.cmap_t_buttons);
        % Update current cmap
        obj.conf.h_cmap_update_butt = uicontrol('Style', 'pushbutton', ...
                                          'String', 'Update Cmap', ...
                                          'Parent', obj.conf.cmap_t_buttons);
        buttonSize = get(obj.conf.side_g_boxes, 'ButtonSize');
        buttonSize(1) = 140;
        set(obj.conf.cmap_t_buttons, 'ButtonSize', buttonSize);
        set(obj.conf.cmap_t_buttons, 'VerticalAlignment', 'bottom');

        uicontrol('Parent', obj.conf.side_t_boxes, 'Enable', 'off', 'Visible', 'off', 'Tag', 'spacer');

        obj.conf.h_cmap_butts_vbox = uiextras.VButtonBox( ...
            'Parent', obj.conf.side_t_boxes);

        obj.conf.h_cmap_butt(1) = uicontrol( ...
            'Parent', obj.conf.h_cmap_butts_vbox, ...
            'Style', 'radiobutton', 'String', 'ID', ...
            'Callback', @(src, evt)obj.changeGrainCmap('ID', true));
        obj.conf.h_cmap_butt(2) = uicontrol( ...
            'Parent', obj.conf.h_cmap_butts_vbox, ...
            'Style', 'radiobutton', 'String', 'Rand', ...
            'Callback', @(src, evt)obj.changeGrainCmap('Rand', true));
        obj.conf.h_cmap_butt(3) = uicontrol( ...
            'Parent', obj.conf.h_cmap_butts_vbox, ...
            'Style', 'radiobutton', 'String', 'Volume', ...
            'Callback', @(src, evt)obj.changeGrainCmap('Volume', true));
        obj.conf.h_cmap_butt(4) = uicontrol(...
            'Parent', obj.conf.h_cmap_butts_vbox, ...
            'Style', 'radiobutton', 'String', 'Completeness', ...
            'Callback', @(src, evt)obj.changeGrainCmap('Completeness', true));
        obj.conf.h_cmap_butt(5) = uicontrol( ...
            'Parent', obj.conf.h_cmap_butts_vbox, ...
            'Style', 'radiobutton', 'String', 'Caxis', ...
            'Callback', @(src, evt)obj.changeGrainCmap('Caxis', true));
        obj.conf.h_cmap_butt(6) = uicontrol( ...
            'Parent', obj.conf.h_cmap_butts_vbox, ...
            'Style', 'radiobutton', 'String', 'IPF', ...
            'Callback', @(src, evt)obj.changeGrainCmap('IPF', true));
        obj.conf.h_cmap_butt(7) = uicontrol( ...
            'Parent', obj.conf.h_cmap_butts_vbox, ...
            'Style', 'radiobutton', 'String', 'Rvec', ...
            'Callback', @(src, evt)obj.changeGrainCmap('Rvec', true));
        obj.conf.h_cmap_butt(8) = uicontrol( ...
            'Parent', obj.conf.h_cmap_butts_vbox, ...
            'Style', 'radiobutton', 'String', 'SchmidF', ...
            'Callback', @(src, evt)obj.changeGrainCmap('SchmidF', true));
        obj.conf.h_cmap_butt(9) = uicontrol( ...
            'Parent', obj.conf.h_cmap_butts_vbox, ...
            'Style', 'radiobutton', 'String', 'Mosaicity', ...
            'Callback', @(src, evt)obj.changeGrainCmap('Mosaicity', true));
        obj.conf.h_cmap_butt(10) = uicontrol( ...
            'Parent', obj.conf.h_cmap_butts_vbox, ...
            'Style', 'radiobutton', 'String', 'Annealing', ...
            'Callback', @(src, evt)obj.changeGrainCmap('Annealing', true));

        uicontrol('Parent', obj.conf.side_t_boxes, 'Enable', 'off', 'Visible', 'off', 'Tag', 'spacer');

        % Computation
        obj.conf.compute_t_buttons = uiextras.VButtonBox('Parent', obj.conf.side_t_boxes);
        obj.conf.h_schmid_butt = uicontrol('Style', 'pushbutton', ...
            'String', 'Calculate SchmidF', ...
            'Parent', obj.conf.compute_t_buttons);
        obj.conf.h_mosaicity_butt = uicontrol('Style', 'pushbutton', ...
            'String', 'Calculate Mosaicity', ...
            'Parent', obj.conf.compute_t_buttons);
        obj.conf.h_pf_butt = uicontrol('Style', 'pushbutton', ...
            'String', 'Pole Figure', ...
            'Parent', obj.conf.compute_t_buttons);
        obj.conf.h_ipf_butt = uicontrol('Style', 'pushbutton', ...
            'String', 'Inverse Pole Figure', ...
            'Parent', obj.conf.compute_t_buttons);

        set(obj.conf.compute_t_buttons, 'ButtonSize', buttonSize);
        set(obj.conf.compute_t_buttons, 'VerticalAlignment', 'bottom');

        uicontrol('Parent', obj.conf.side_t_boxes, 'Enable', 'off', 'Visible', 'off', 'Tag', 'spacer');

        obj.conf.pixe_value_panel = uipanel('Parent', obj.conf.side_t_boxes, ...
            'BorderType', 'none');
        obj.conf.h_value_label = GtPixelInfoVal(obj.conf.pixe_value_panel, ...
            obj.conf.h_im, @(x, y)obj.getComplete3DPixelInfo());

        set(obj.conf.side_t_boxes, 'Sizes', [20 120 20 200 20 100 20 -1]);

        % Reset back the zoom to the previous state (before the inclusion)
        set(zoomOp, 'Enable', zoomState);
    end

    function setVisualisedData(obj, image, vol, is_overlay)
    % GTGRAINSMANAGER/SETVISUALISEDDATA Prepares the slices to be visualised
    % in the axes, and then sets them

        try
            params = obj.getParams();
            is_6D = strcmpi(params.rec.grains.algorithm(1:2), '6D');
        catch
            is_6D = false;
        end
        try
            is_IPF = strcmpi(obj.cmap.defname, 'IPF');
        catch
            is_IPF = false;
        end
        if (is_6D && is_IPF && ~is_overlay)
            load_vol = lower(obj.conf.load_vol);
            switch (load_vol)
                case 'phase'
                    dmvol = obj.loadSlice(obj.cache.get('phases', {obj.selectedPhase}, 'dmvol'));
                    gr_ids = obj.loadSlice(obj.cache.get('phases', {obj.selectedPhase}, 'vol'));
                    gr_mask = gr_ids > 0;
                    phases = gr_mask * obj.selectedPhase;
                case {'volume', 'volume_dilated'}
                    dmvol = obj.loadSlice(obj.cache.get(load_vol, {}, 'dmvol'));
                    phases = obj.loadSlice(obj.cache.get(load_vol, {}, 'phases'));
                    gr_ids = obj.loadSlice(obj.cache.get(load_vol, {}, 'grains'));

                    if (obj.selectedPhase > 0)
                        phases(phases ~= obj.selectedPhase) = 0;
                        if (obj.selectedGrain > 0)
                            gr_ids(gr_ids ~= obj.selectedGrain) = 0;
                        end
                    end

                    gr_mask = gr_ids > 0;
                otherwise
            end
            dmvol = permute(dmvol, [1 2 4 3]);
            phase_ids = unique(phases(phases > 0));

            img_slice_R = zeros(size(gr_mask));
            img_slice_G = zeros(size(gr_mask));
            img_slice_B = zeros(size(gr_mask));

            conflict_inds = find((gr_ids == -1) | (phases == -1));
            img_slice_R(conflict_inds) = 1;
            img_slice_G(conflict_inds) = 1;
            img_slice_B(conflict_inds) = 1;

            gvdm = gtDefDmvol2Gvdm(dmvol);

            for ii = 1:numel(phase_ids)
                mask = gr_mask & (phases == phase_ids(ii));
                indx = find(mask(:));
                avg_R_vecs = gvdm(:, indx)';

                cs = params.cryst(phase_ids(ii)).crystal_system;
                sp = params.cryst(phase_ids(ii)).spacegroup;

                symm = gtCrystGetSymmetryOperators(cs, sp);

                if (~isempty(avg_R_vecs))
                    [cmap, ~, ~] = gtIPFCmap(phase_ids(ii), obj.sampleDir, ...
                        'r_vectors', avg_R_vecs, 'crystal_system', cs, ...
                        'symm', symm, 'background', false);

                    img_slice_R(indx) = cmap(:, 1);
                    img_slice_G(indx) = cmap(:, 2);
                    img_slice_B(indx) = cmap(:, 3);
                end
            end

            set(image, 'CData', cat(3, img_slice_R, img_slice_G, img_slice_B));
        else
            setVisualisedData@GtGrainsManager(obj, image, vol, is_overlay);
        end
    end

    function updateSidePanel(obj)
    % GTTWINANALYSIS/UPDATESIDEPANEL  Updates the side panel from
    % GrainsManager and the new left panel. Updates also the figure size
    %
    % updateSidePanel(obj)

        buttonSize = get(obj.conf.cmap_t_buttons, 'ButtonSize');

        set(obj.conf.side_g_boxes, 'ButtonSize', buttonSize);
        set(obj.conf.main_g_boxes, 'Sizes', [-1 160]);
        set(obj.conf.h_viewer_panel, 'BorderWidth', 0)
        set(obj.conf.side_t_boxes, 'Sizes', [5 150 5 240 5 120 5 -1]);
        drawnow()

        fig_pos = GtBaseGuiElem.getPixels(obj.conf.h_figure, 'Position');
        fig_pos(3:4) = max(fig_pos(3:4), [1040 680]);
        GtBaseGuiElem.setPixels(obj.conf.h_figure, 'Position', fig_pos);
        drawnow('expose')
    end

    function doUpdateDisplay(obj)
    % GTTWINANALYSIS/DOUPDATEDISPLAY  Updates labels on main axes
    %
    % doUpdateDisplay(obj)

        doUpdateDisplay@GtVolView(obj)

        frame = 'XYZ';
        xlabel(obj.conf.h_ax, frame(obj.conf.horDim));
        ylabel(obj.conf.h_ax, frame(obj.conf.verDim));
    end

    function toggleCbarVisibility(obj)
    % GTTWINANALYSIS/TOGGLECBARVISIBILITY  Triggers the colorbar
    % visualization, otherwise shuts it down
    %
    % toggleCbarVisibility(obj)

        if (~obj.conf.cbar)
            set(obj.conf.h_ax_cbar, 'Visible', 'on');
            obj.conf.cbar = true;
%             set(obj.conf.h_translabel, 'Enable',  'on');
%             set(obj.conf.h_translabel, 'Visible', 'on');
%             set(obj.conf.trans_boxes,  'Enable',  'on');
%             set(obj.conf.trans_boxes,  'Visible', 'on');
%             set(obj.conf.display_boxes, 'Sizes', [-1 15 20]);
%             obj.conf.minOverlayVol = min(obj.conf.overlay(:));
%             obj.conf.maxOverlayVol = max(obj.conf.overlay(:));
        else
            set(obj.conf.h_ax_cbar, 'Visible', 'off');
            obj.conf.cbar = false;
%             set(obj.conf.h_translabel, 'Enable',  'off');
%             set(obj.conf.h_translabel, 'Visible', 'off');
%             set(obj.conf.trans_boxes,  'Enable',  'off');
%             set(obj.conf.trans_boxes,  'Visible', 'off');
%             set(obj.conf.display_boxes, 'Sizes', [-1 0 0]);
        end
    end

    function addUICallbacks(obj)
    % GTTWINANALYSIS/ADDUICALLBACKS  Adds the callbacks to the GUI elements
    % of the left panel
    %
    % addUICallbacks(obj)

        addUICallbacks@GtGrainsManager(obj)

        obj.addUICallback(obj.conf.h_cmap_set_butt, ...
                          'Callback', ...
                          @(src, evt)obj.setMakemapOptions(), false);
        obj.addUICallback(obj.conf.h_cmap_show_butt, ...
                          'Callback', ...
                          @(src, evt)GtColormap.showSupportedCmaps(src, evt, false), false);
        obj.addUICallback(obj.conf.h_cmap_edit_butt, ...
                          'Callback', ...
                          @(src, evt)obj.displayCmapEditor(obj.get3DPixelInfo(), false, []), false);
        obj.addUICallback(obj.conf.h_cmap_save_butt, ...
                          'Callback', ...
                          @(src, evt)obj.saveCmapFigure([]), false);
        obj.addUICallback(obj.conf.h_cmap_update_butt, ...
                          'Callback', ...
                          @(src, evt)obj.updateCmap(), false);

        obj.addUICallback(obj.conf.h_schmid_butt, ...
                          'Callback', ...
                          @(src, evt)obj.updateSchmidFactors(), false)
        obj.addUICallback(obj.conf.h_mosaicity_butt, ...
                          'Callback', ...
                          @(src, evt)obj.updateMosaicity(), false);
        obj.addUICallback(obj.conf.h_pf_butt, ...
                          'Callback', ...
                          @(src, evt)obj.displayPoleFigure(), false);
        obj.addUICallback(obj.conf.h_ipf_butt, ...
                          'Callback', ...
                          @(src, evt)obj.displayInversePoleFigure(), false);

        % Overriding previous Callback!
        obj.addUICallback(obj.conf.h_figure, ...
                          'WindowButtonMotionFcn', ...
                          @(src, evt)obj.updatePixelInfoVals(), false);
        drawnow('expose');
    end

    function resetUiComponents(obj)
    % GTTWINANALYSIS/RESETUICOMPONENTS  Inherited method to reset the ui
    % components
    %
    % resetUiComponents(obj)

        obj.updateStatus('Reset UI components...');

        drawnow('expose');

        resetUiComponents@GtGrainsManager(obj);
        obj.updateStatus('Done.');
    end

    function updateValue(obj, infoPattern)
    % GTTWINANALYSIS/UPDATEVALUE  Updates the selected cmap value for the
    % current grain
    %
    % updateValue(obj, infoPattern)

        set(obj.conf.h_value_label, 'String', infoPattern);
    end

    function createAxesMenu(obj)
    % GTTWINANALYSIS/CREATEAXESMENU  Updates the axes UIContextMenu
    %
    % createAxesMenu(obj)

        createAxesMenu@GtGrainsManager(obj);

        obj.conf.h_menu_items{end+1} = uimenu(obj.conf.h_context_menu, ...
                                       'Label', 'Grain Neighborhood', ...
                                       'Tag', 'h_neighborhood', ...
                                       'Separator', 'on', ...
                                       'Callback', @(src, evt)copy3DPixInfoToClipboard(obj, false));
        obj.conf.h_menu_items{end+1} = uimenu(findobj(obj.conf.h_figure, 'Tag', 'h_neighborhood'), ...
                                       'Label', 'Print IDs', ...
                                       'Callback', @(src, evt)displayNeighborhood(obj, obj.get3DPixInfoFromClipboard(), false));
        obj.conf.h_menu_items{end+1} = uimenu(findobj(obj.conf.h_figure, 'Tag', 'h_neighborhood'), ...
                                       'Label', 'Print Cmap values', ...
                                       'Callback', @(src, evt)displayNeighborhood(obj, obj.get3DPixInfoFromClipboard(), true));
        obj.conf.h_menu_items{end+1} = uimenu(findobj(obj.conf.h_figure, 'Tag', 'h_neighborhood'), ...
                                       'Label', 'Schmid Factor', ...
                                       'Callback', @(src, evt)displaySchmidFNeighborhood(obj, obj.get3DPixInfoFromClipboard()));
        obj.conf.h_menu_items{end+1} = uimenu(findobj(obj.conf.h_figure, 'Tag', 'h_neighborhood'), ...
                                       'Label', 'Slip Transfer', ...
                                       'Callback', @(src, evt)displaySlipTransferNeighborhood(obj, obj.get3DPixInfoFromClipboard()));

        obj.conf.h_menu_items{end+1} = uimenu(obj.conf.h_context_menu, ...
                                       'Label', 'Show Fsim', ...
                                       'Tag', 'h_figmenu_showfsim', ...
                                       'Callback', @(src, evt)displayShowFsim(obj, obj.get3DPixelInfo()));

        obj.conf.h_menu_items{end+1} = uimenu(obj.conf.h_context_menu, ...
                                       'Label', 'Render Grains', ...
                                       'Tag', 'h_figmenu_rendering', ...
                                       'Callback', @(src, evt)copy3DPixInfoToClipboard(obj, false));
        obj.conf.h_menu_items{end+1} = uimenu(findobj(obj.conf.h_figure, 'Tag', 'h_figmenu_rendering'), ...
                                       'Label', 'Slice', ...
                                       'Callback', @(src, evt)displayRenderGrains(obj, obj.get3DPixInfoFromClipboard(), ...
                                           setdiff(unique(get(obj.conf.h_im, 'CData')), [-1 0]), true, false, []));
        obj.conf.h_menu_items{end+1} = uimenu(findobj(obj.conf.h_figure, 'Tag', 'h_figmenu_rendering'), ...
                                       'Label', 'Grain list', ...
                                       'Callback', @(src, evt)displayRenderGrains(obj, obj.get3DPixInfoFromClipboard(), [], true, false, []));
        obj.conf.h_menu_items{end+1} = uimenu(findobj(obj.conf.h_figure, 'Tag', 'h_figmenu_rendering'), ...
                                       'Label', 'Neighborhood', ...
                                       'Callback', @(src, evt)displayRenderGrains(obj, obj.get3DPixInfoFromClipboard(), [], true, true, 1.1));
        obj.conf.h_menu_items{end+1} = uimenu(findobj(obj.conf.h_figure, 'Tag', 'h_figmenu_rendering'), ...
                                       'Label', 'Parents/Twins', ...
                                       'Callback', @(src, evt)displayRenderGrains(obj, obj.get3DPixInfoFromClipboard(), ...
                                           obj.extras.twinInfo.twin_list, ...
                                           true, false, []));
        obj.conf.h_menu_items{end+1} = uimenu(findobj(obj.conf.h_figure, 'Tag', 'h_figmenu_rendering'), ...
                                       'Label', 'Only Twinned grains', ...
                                       'Callback', @(src, evt)displayRenderGrains(obj, obj.get3DPixInfoFromClipboard(), ...
                                           obj.extras.twinInfo.twin_list(:, 2), ...
                                           true, false, []));
        obj.conf.h_menu_items{end+1} = uimenu(findobj(obj.conf.h_figure, 'Tag', 'h_figmenu_rendering'), ...
                                       'Label', 'Only Twins', ...
                                       'Callback', @(src, evt)displayRenderGrains(obj, obj.get3DPixInfoFromClipboard(), ...
                                           obj.extras.twinInfo.twin_list(:, 1), ...
                                           true, false, []));

        obj.conf.h_menu_items{end+1} = uimenu(obj.conf.h_context_menu, ...
                                       'Label', 'Display Unit Cells', ...
                                       'Tag', 'h_figmenu_unitcells', ...
                                       'Callback', @(src, evt)copy3DPixInfoToClipboard(obj, false));
        obj.conf.h_menu_items{end+1} = uimenu(findobj(obj.conf.h_figure, 'Tag', 'h_figmenu_unitcells'), ...
                                       'Label', 'Slice', ...
                                       'Callback', @(src, evt)displayRenderGrains(obj, obj.get3DPixInfoFromClipboard(), ...
                                           setdiff(unique(get(obj.conf.h_im, 'CData')), [-1 0]), false, false, []));
        obj.conf.h_menu_items{end+1} = uimenu(findobj(obj.conf.h_figure, 'Tag', 'h_figmenu_unitcells'), ...
                                       'Label', 'Grain list', ...
                                       'Callback', @(src, evt)displayRenderGrains(obj, obj.get3DPixInfoFromClipboard(), [], false, false, []));
        obj.conf.h_menu_items{end+1} = uimenu(findobj(obj.conf.h_figure, 'Tag', 'h_figmenu_unitcells'), ...
                                       'Label', 'Neighborhood', ...
                                       'Callback', @(src, evt)displayRenderGrains(obj, obj.get3DPixInfoFromClipboard(), [], false, true, 1.1));
        obj.conf.h_menu_items{end+1} = uimenu(findobj(obj.conf.h_figure, 'Tag', 'h_figmenu_unitcells'), ...
                                       'Label', 'Parents/Twins', ...
                                       'Callback', @(src, evt)displayRenderGrains(obj, obj.get3DPixInfoFromClipboard(), ...
                                           obj.extras.twinInfo.twin_list, ...
                                           false, false, []));
        obj.conf.h_menu_items{end+1} = uimenu(findobj(obj.conf.h_figure, 'Tag', 'h_figmenu_unitcells'), ...
                                       'Label', 'Only Twinned grains', ...
                                       'Callback', @(src, evt)displayRenderGrains(obj, obj.get3DPixInfoFromClipboard(), ...
                                           obj.extras.twinInfo.twin_list(:, 2), ...
                                           false, false, []));
        obj.conf.h_menu_items{end+1} = uimenu(findobj(obj.conf.h_figure, 'Tag', 'h_figmenu_unitcells'), ...
                                       'Label', 'Only Twins', ...
                                       'Callback', @(src, evt)displayRenderGrains(obj, obj.get3DPixInfoFromClipboard(), ...
                                           obj.extras.twinInfo.twin_list(:, 1), ...
                                           false, false, []));
    end

    function [point, pID, gID] = get3DPixInfoFromClipboard(obj)
    % GTTWINANALYSIS/GET3DPIXINFOFROMCLIPBOARD
    %
    % [point, pID, gID] = get3DPixInfoFromClipboard(obj)

        point = [];
        str = clipboard('paste');
        if ~isempty(str)
            numbers = str2double(regexp(str, '\d*', 'match'));
            if ~isempty(numbers)
                point = numbers(1:3);
                if nargout > 1
                    pID = numbers(4);
                    if nargout > 2
                        gID = numbers(5);
                    end
                end
                obj.extras.point = point;
            end
        end
    end

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% PUBLIC METHODS
methods (Access = public)
    function [cmap_name, current_selected] = getCurrentCmap(obj)
        max_radio = get(obj.conf.h_cmap_butt(1), 'Max');
        radio_values = arrayfun(@(x)get(x, 'Value'), obj.conf.h_cmap_butt);
        current_selected = find(radio_values == max_radio, 1, 'first');
        if (isempty(current_selected))
            current_selected = 1;
        end
        cmap_name = get(obj.conf.h_cmap_butt(current_selected), 'String');
    end

    function updatePixelInfoVals(obj)
        try
            obj.conf.h_pixelinfo.onMouseMoveCbk();
        catch mexc
            gtPrintException(mexc)
        end
        try
            cmap_name = obj.getCurrentCmap();
            pixel_val_text = cmap_name;
            switch (cmap_name)
                case {'Caxis', 'IPF', 'Rvec'}
                    pixel_val_text = [pixel_val_text ':\n%0.3f %0.3f %0.3f'];
                case 'Rand'
                    pixel_val_text = [pixel_val_text ':\n%0.3f'];
                otherwise
                    pixel_val_text = [pixel_val_text ':\n%0.3f'];
            end
            if (~strcmpi(obj.conf.h_value_label.getPattern(), pixel_val_text))
                obj.conf.h_value_label.changePatternAndCbk(pixel_val_text, @(x, y)obj.getPhaseAndGrainInfoValue())
            end

            obj.conf.h_value_label.onMouseMoveCbk();
        catch mexc
            gtPrintException(mexc)
        end
    end

    function [point, values] = getPhaseAndGrainInfoValue(obj)
    % GTTWINANALYSIS/GETPHASEANDGRAININFOVALUE  Sets the text relative to
    % the current colormap value of the selected grain
    %
    % [point, values] = getPhaseAndGrainInfoValue(obj, ~, ~)

        cmap_name = obj.getCurrentCmap();

        [~, values] = obj.getPhaseAndGrainOfCurrentPointer();
        % build string to display
        if (values(1) > 0 && values(2) > 0 && ~isempty(obj.cmap.par_values))
            switch (cmap_name)
                case 'Caxis'
                    values = double(obj.cmap.par_values(values(2), end-2:end));
                case {'IPF', 'Rvec'}
                    values = double(obj.cmap.par_values(values(2), :));
                case 'Rand'
                    values = 0;
                otherwise
                    values = double(obj.cmap.par_values(values(2)));
            end
        else
            switch (cmap_name)
                case {'Caxis', 'IPF', 'Rvec'}
                    values = [0 0 0];
                otherwise
                    values = 0;
            end
        end
        point = [];
    end

    function updateCmap(obj)
    % GTTWINANALYSIS/UPDATECMAP  Updates the current colormap letting us
    % changing the parameters for that colormap, together with
    %
    % updateCmap(obj)

        cmap_name = obj.getCurrentCmap();
        if ~ismember(cmap_name, {'Caxis', 'IPF', 'Rvec', 'Rand'})
            suppCmaps = obj.cmap.suppCmaps;
            if ~isempty(obj.cmap.name)
                [~, ind] = ismember(obj.cmap.name, suppCmaps);
                suppCmaps(ind) = [];
                suppCmaps = vertcat(obj.cmap.name, suppCmaps);
            end
            vars_pars.nsteps = obj.cmap.nsteps;
            vars_pars.name   = suppCmaps;

            vars_list = get_structure(vars_pars, [], false, [], []);
            vars_list(:, 2) = {'Number of steps for building the colormap', ...
                'Standard colormap name in Matlab'};

            vars_pars = gtModifyStructure(vars_pars, vars_list, 1, 'Options for building the colormap:');
            obj.cmap = gtAddMatFile(obj.cmap, vars_pars, true);
        end

        obj.changeGrainCmap([], true);
    end

    function updatePhaseInfo(obj, newdir, dirtype, phaseID)
    % GTGRAINSMANAGER/UPDATEPHASEINFO  Updates phase info as phaseID,
    % direction for a specific direction type (sampleDir, crystDir,
    % loadDir)
    %
    % updatePhaseInfo(obj, newdir, dirtype, phaseID)

        try
            if ~exist('newdir', 'var')
                newdir = [];
            end
            if ~exist('dirtype', 'var')
                dirtype = 'sampleDir';
            end
            if ~exist('phaseID', 'var') || isempty(phaseID)
                phaseID = 1;
            end

            if isempty(newdir)
                newdir = [0 0 1];
                obj.(dirtype) = newdir;
                    newdir = [0 0 1];
            elseif ischar(newdir) && ismember(dirtype, {'sampleDir', 'loadDir'})
                switch (newdir)
                    case {'x', 'X'}
                        obj.(dirtype) = [1 0 0];
                    case {'y', 'Y'}
                        obj.(dirtype) = [0 1 0];
                    case {'z', 'Z'}
                        obj.(dirtype) = [0 0 1];
                end
                if strcmp(dirtype, 'loadDir')
                    obj.(dirtype) = -obj.(dirtype);
                end
            elseif isnumeric(newdir)
                if all(isequal(newdir, [1 0 0]))
                    obj.(dirtype) = [1 0 0];
                    newdir = 'X';
                elseif all(isequal(newdir, [0 1 0]))
                    obj.(dirtype) = [0 1 0];
                    newdir = 'Y';
                elseif all(isequal(newdir, [0 0 1]))
                    obj.(dirtype) = [0 0 1];
                    newdir = 'Z';
                else
                    obj.(dirtype) = newdir;
                    newdir = strrep(num2str(newdir), '  ', '');
                end
            end
            obj.selectedPhase = phaseID;

        catch mexc
            gtPrintException(mexc, 'Couldn''t update phase info');
        end
    end

    function setMakemapOptions(obj)
    % GTTWINANALYSIS/SETMAKEMAPOPTIONS  Sets some general colormap options
    % used in makemap method
    %
    % setMakemapOptions(obj)

        vars_pars.bkg         = obj.cmap.bkg;
        vars_pars.conflict    = obj.cmap.conflict;
        vars_pars.hasBkg      = obj.cmap.hasBkg;
        vars_pars.hasConflict = obj.cmap.hasConflict;
        vars_pars.hideColor   = obj.cmap.hideColor;
        vars_pars.maxSystem   = obj.cmap.maxSystem;
        vars_pars.scale       = obj.cmap.scale;

        vars_list = get_structure(vars_pars, [], false, [], []);
        vars_list(:, 2) = {sprintf('The colormap backgroud color (currently [%0.1f %0.1f %0.1f])', obj.cmap.bkg), ...
            sprintf('The colormap conflict color (currently [%0.1f %0.1f %0.1f])', obj.cmap.conflict), ...
            'Does the colormap contain the background color?', ...
            'Does the colormap contain the conflict color?', ...
            sprintf('RGB color for hidden grains (currently [%0.1f %0.1f %0.1f])', obj.cmap.hideColor), ...
            'Consider only maximum values for each row?', ...
            'Scale values from 0 to 1?'};
        ind = findValueIntoCell(fieldnames(vars_pars), {'bkg'});
        vars_list(ind(:, 1), 4) = {2};
        ind = findValueIntoCell(fieldnames(vars_pars), {'conflict'});
        vars_list(ind(:, 1), 4) = {3};

        vars_pars = gtModifyStructure(vars_pars, vars_list, 1, 'Options for building the colormap:');
        if (vars_pars.hasBkg)
            vars_pars = gtModifyStructure(vars_pars, vars_list, 2, 'Options for building the colormap:');
        end
        if (vars_pars.hasConflict)
            vars_pars = gtModifyStructure(vars_pars, vars_list, 3, 'Options for building the colormap:');
        end
        obj.cmap = gtAddMatFile(obj.cmap, vars_pars, true);
    end

    function [list, figtitle, pID] = getIDlist(obj, point, cluster, oversize)
    % GTTWINANALYSIS/GETIDLIST  Gets the ID list basing on a point into the
    % volume of its neighborhood or, if deselected, gets the active grains
    % list. It gets also a figure title.
    %
    % [list, figtitle, pID] = getIDlist(obj, point, cluster, oversize)

        % neighbourhood display
        if ~exist('cluster', 'var') || isempty(cluster)
            cluster = false;
        end
        if ~exist('oversize', 'var') || isempty(oversize)
            oversize = 1;
        end
        if (cluster)
            % current mouse position on axis
            [pID, gID] = obj.getPhaseAndGrainOfPoint(point);
            if gID ~= 0
                [list, figtitle] = obj.getNeighborhood(gID, oversize);
            else
                list = [];
                figtitle = [strrep(obj.name, '_', ' ') ' - ' obj.cmap.defname ' map'];
                pID = obj.selectedPhase;
            end
        else
            list = cat(2, obj.p_activeIDs{:});
            figtitle = [strrep(obj.name, '_', ' ') ' - ' obj.cmap.defname ' map'];
            pID = obj.selectedPhase;
        end
    end

    function list = getVolumeSlice(obj, list, figtitle, varargin)
    % GTTWINANALYSIS/GETVOLUMESLICE  Gets the slice volume and copies it
    % into a new figure, adding control buttons and the colormap
    %
    % list = getVolumeSlice(obj, list, figtitle, varargin)
    % OPTIONAL INPUT:
    %   'hide'
    %   'cbar'
    %   'draw'

        % open new figure and copy the axis
        h_cmap_fig = figure('Name', figtitle, 'Tag', 'h_cmap_fig', ...
            'Position', get(obj.conf.h_figure, 'Position')+[100 100 0 0]);

        % HBox for buttons, axes, cbar
        h_panel = uiextras.HBox('Parent', h_cmap_fig, 'Tag', 'h_panel');
        h_button_box = uiextras.VButtonBox('Parent', h_panel, ...
            'Tag', 'h_button_box', 'Spacing', 1);
        h_axes_box   = uipanel('Parent', h_panel, 'Tag', 'h_axes_box', 'BorderType', 'none');
        h_cbar_box   = uipanel('Parent', h_panel, 'Tag', 'h_cbar_box', 'BorderType', 'none');
        set(h_panel, 'Sizes', [140 -1 100]);
        set(h_cbar_box, 'Position', [0 0 1 1]);

        buttonSize = get(h_button_box, 'ButtonSize');
        buttonSize(1) = 120;
        set(h_button_box, 'ButtonSize', buttonSize);

        h_cmap_ax = copyobj(obj.conf.h_ax, h_axes_box);
        set(h_cmap_ax, 'Position', get(obj.conf.h_ax, 'Position'), 'Tag', 'h_cmap_ax');
        h_cmap_im = get(h_cmap_ax, 'Children');
        delete( h_cmap_im( findStringIntoCell(get(h_cmap_im, 'Visible'), {'off'}, 1) ) );
        h_cmap_im = get(h_cmap_ax, 'Children');
        set(h_cmap_im, 'Tag', 'h_cmap_im');

        colormap(obj.conf.cmap)
        set(h_cmap_ax, 'CLim', obj.conf.clims)

        h_cbar_ax = copyobj(obj.conf.h_cbar_ax, h_cbar_box);
        h_cbar_im = get(h_cbar_ax, 'Children');

        if ~isfield(obj.conf, 'h_cmap') || ~isa(obj.conf.h_cmap, 'GtColormap') || isempty(obj.conf.h_cmap)
            obj.conf.h_cmap = GtColormap('h_fig', h_cmap_fig);
            obj.conf.h_cmap.getCurrentFigCmap();
            obj.conf.h_cmap.getCmapExtra();
            obj.conf.h_cmap.getCurrentCmapName();
        end

        % figure title
        %set(obj.conf.h_cmap.h_fig, 'Name', figtitle)
        obj.cmap.h_fig = h_cmap_fig;
        % update properties
        obj.conf.h_cmap.importProperties(obj.cmap);

        % draw figure
        [hasP, ind] = ismember('draw', varargin(1:2:end));
        if (hasP && varargin{ind*2}==false)
            close(h_cmap_fig)
            obj.conf.h_cmap.h_fig = [];
            obj.cmap.h_fig = [];
            return
        end

        % get current IDs
        data = get(h_cmap_im, 'CData');
        IDs = setdiff(unique(data), [-1 0]);
        if ~isempty(list)
            obj.conf.h_cmap.hideIDs = setdiff(IDs, list);
        end
        % if some IDs are not active, update cmap
        if ~isempty(obj.conf.h_cmap.hideIDs)
%             obj.conf.h_cmap.getCmapExtra()
%             obj.conf.h_cmap.updateCmap(obj.conf.h_cmap.colormap, obj.conf.h_cmap.cbar)
        end

        % draw buttons
        [hasP, ind] = ismember('hide', varargin(1:2:end));
        if (~hasP)
            if ismember(obj.conf.h_cmap.defname, {'Caxis', 'IPF', 'Rvec', 'Rand'})
                hide = [1:12 17:19 24 25];
            else
                hide = 1;
            end
            if strcmpi(obj.conf.h_cmap.name, 'gray')
                hide = unique([hide, [6:12 19]]);
            end
            if ~isempty(obj.conf.h_cmap.hideIDs)
                hide = unique([hide, [4:12 19]]);
            end
            if ismember(obj.conf.h_cmap.name, GtColormap.getMatlabCmaps())
                hide = unique([hide, 19]);
            end
        else
            hide = varargin{ind*2};
        end
        obj.conf.h_cmap.setControls('hide', hide, 'parent', h_button_box);
        obj.selectedPhase = max(1, obj.selectedPhase);

        % colorbar
        if ~isempty(varargin)
            [hasP, ind]=ismember('cbar', varargin(1:2:end));
            if (hasP && varargin{ind*2}==true) || (~hasP)
                obj.conf.h_cmap.displayColorbar(obj.conf.cmap);
            end
        elseif ~ismember(obj.conf.h_cmap.defname, {'Caxis', 'IPF', 'Rvec', 'Rand'})
            obj.conf.h_cmap.displayColorbar(obj.conf.cmap);
        else
            obj.conf.h_cmap.plotCbar(...
                h_cbar_ax, ...
                obj.p_crystalsystem{obj.selectedPhase}, ....
                obj.conf.h_cmap.cbar, obj.conf.h_cmap.name, ...
                obj.conf.h_cmap.defname, obj.conf.h_cmap.nsteps, ...
                obj.conf.h_cmap.maxV);
            set(h_panel, 'Sizes', [140 -1 250]);
            set(h_cbar_ax, 'Position', [0.01 0.01 0.95 0.95]);
            set(h_cbar_ax, 'DataAspectRatio', [1 1 1])
        end

        obj.conf.h_cbar = findobj(obj.conf.h_cmap.h_fig, 'Tag', 'Colorbar');
        if ~isempty(obj.conf.h_cbar)
            set(obj.conf.h_cbar, 'Position', [0 0.1 0.3 0.82]);
        end
        obj.cmap = obj.conf.h_cmap.exportProperties();
    end

    function displayCmapEditor(obj, point, cluster, oversize, varargin)
    % GTTWINANALYSIS/DISPLAYCMAPEDITOR  Plays with colormap and save
    % snapshot
    %
    % displayCmapEditor(obj, point, cluster, oversize, varargin)

        if ~exist('cluster', 'var') || isempty(cluster)
            cluster = false;
        end
        if ~exist('oversize', 'var') || isempty(oversize)
            oversize = 1;
        end
        [list, figtitle] = obj.getIDlist(point, cluster, oversize);

        obj.getVolumeSlice(list, figtitle, varargin{:});
        set(obj.conf.h_cmap.h_fig, 'CloseRequestFcn', @(src, evt)closeCmapFigure(src, evt, obj, false))
        if ~isempty(list) && cluster
            disp('Displaying grainIDS:')
            disp(list)
        end

        % add menu item
        hm = uimenu(obj.conf.h_cmap.h_fig, 'Label', 'Colormap', 'ForegroundColor', 'red');
        uimenu(hm, 'Label', 'Save Figure', ...
            'Callback', @(src, evt)saveCmapFigure(obj, obj.conf.h_cmap.h_fig))
        uimenu(hm, 'Label', 'Render Grains', ...
            'Separator', 'on', ...
            'Callback', @(src, evt)displayRenderGrains(obj, point, list, true))
        uimenu(hm, 'Label', 'Draw Unit Cells', ...
            'Callback', @(src, evt)displayRenderGrains(obj, point, list, false))
    end

    function closeCmapFigure(src, ~, obj, reload)
    % GTTWINANALYSIS/CLOSECMAPFIGURE  Closes the colormap figure, exporting
    % the cmap properties and reloading the volume using this cmap if
    % reload is true
    %
    % closeCmapFigure(src, ~, obj, reload)

        if ~exist('reload', 'var') || isempty(reload)
            reload = false;
        end
        if isfield(obj.conf, 'h_cmap') && ~isempty(obj.conf.h_cmap) && isa(obj.conf.h_cmap, 'GtColormap')
            CMap = obj.cmap.colormap;
            CMap = obj.conf.h_cmap.setCmapExtra(CMap);

            obj.cmap = obj.conf.h_cmap.exportProperties();
            if src ~= obj.conf.h_figure
                delete(src)
            end
            obj.conf = rmfield(obj.conf, 'h_cmap');

            if (reload)
                obj.reloadAssebledVolume('CMap', CMap)
            end
        end
        obj.cmap.h_fig = [];
        obj.cmap.controls.handles = [];
        obj.cmap.hideIDs = [];
    end

    function saveCmapFigure(obj, h_fig, varargin)
    % GTTWINANALYSIS/SAVECMAPFIGURE  Saves the figures using dataset name and slice
    % number into the [home]/workspace/[datasetname] folder by default
    %
    % saveCmapFigure(obj, h_fig, varargin)

        GMgui = false;
        % get figure handle and uicontrol if existing
        if ~exist('h_fig', 'var') || isempty(h_fig)
            if isfield(obj.conf, 'h_cmap') && ishandle(obj.conf.h_cmap.h_fig)
                h_fig = obj.conf.h_cmap.h_fig;
                if all(ishandle(obj.conf.h_cmap.controls.handles))
                    hh = obj.conf.h_cmap.controls.handles;
                else
                    hh = [];
                end
            else
                obj.getVolumeSlice([], [], 'hide', [1:12, 19], varargin{:});
                h_fig = obj.conf.h_cmap.h_fig;
                hh = obj.conf.h_cmap.controls.handles;
                GMgui = true;
            end
        else
            hh = findobj(h_fig, 'type', 'uicontrol');
        end

        vars_pars = obj.setFigureName('GMgui', GMgui, varargin{:});

        if (~vars_pars.rendering)
            set(findobj(h_fig, 'Tag', 'h_left_panel'), 'Visible', 'off')
        end

        % exporting, hiding uicontrol first
        if ~isempty(hh)
            set(hh, 'Visible', 'off')
            export(vars_pars.fname, h_fig, vars_pars.paper_fig, vars_pars.ws_path, false)
            set(hh, 'Visible', 'on')
        else
            export(vars_pars.fname, h_fig, vars_pars.paper_fig, vars_pars.ws_path, false)
        end
        if (vars_pars.GMgui)
            close(h_fig)
        end
    end

    function vars_pars = setFigureName(obj, varargin)
    % GTTWINANALYSIS/SETFIGURENAME  Creates a figure name based on varargin
    % arguments
    %
    % vars_pars = setFigureName(obj, varargin)
    % OPTIONAL INPUT:
    %   paper_fig : true if paper figure format (.tif 600dpi)
    %   ws_path   : worksace directory
    %   ids       : ID list
    %   neighbors : true if considering only neighbors
    %   rendering : true if it's a render plot
    %   GMgui     : GrainsManager GUI
    %   fname     : figure name

        vars_pars.paper_fig = false;
        vars_pars.ws_path   = true;
        vars_pars.GMgui     = false;
        vars_pars.ids       = [];
        vars_pars.neighbors = false;
        vars_pars.rendering = false;
        vars_pars.ipf       = false;

        % remove cells from varargin
        ind = find(cellfun(@(x) iscell(x), varargin));
        varargin([ind, ind-1]) = [];

        if isempty(varargin)
            vars_list = get_structure(vars_pars, [], false, [], []);
            vars_list(:, 2) = {'Use paper figure saving settings?', ...
                'Use workspace path? (/users/<USERNAME>/workspace/)', ...
                'Does it have a GtGrainsManager GUI as parent?', ...
                'Specific ids list', ...
                'Use only neighborhood?', ...
                'Is a rendered plot? If not, it is supposed to be a unit cell plot', ...
                'Is a IPF figure plot?'};
            ind = findValueIntoCell(fieldnames(vars_pars), {'ids', 'neighbors', 'rendering', 'ipf'});
            vars_list(ind(:, 1), 4) = {2};

            vars_pars = gtModifyStructure(vars_pars, vars_list, 1, 'Building figure filename:');
            if (~vars_pars.GMgui)
                vars_pars = gtModifyStructure(vars_pars, vars_list, 2, 'Building figure filename:');
            end
        else
            [vars_pars, rej_pars] = parse_pv_pairs(vars_pars, varargin);
            varargin = rej_pars;
        end

        % transform logical and numerical into strings
        varargin(end+1:end+length(fieldnames(obj.extras.vars_pars))*2) = struct2pv(obj.extras.vars_pars);

        % remove cells from varargin
        ind = find(cellfun(@(x) iscell(x), varargin));
        varargin([ind, ind-1]) = [];
        ind = find(cellfun(@(x) isempty(x), varargin));
        varargin([ind, ind-1]) = [];
        ind = find(cellfun(@(x) isvector(x) & length(x) > 1 & isnumeric(x), varargin));
        %varargin(ind) = cellfun(@(x) strrep(num2str(x), '  ', ' '), varargin(ind), 'UniformOutput', false);
        varargin([ind, ind-1]) = [];

        ind = find(cellfun(@(x) isnumeric(x), varargin));
        varargin(ind) = cellfun(@(x) num2str(x), varargin(ind), 'UniformOutput', false);
        ind = find(cellfun(@(x) islogical(x), varargin));
        varargin(ind) = cellfun(@(x) num2str(x), varargin(ind), 'UniformOutput', false);


        % filename building
        [hasP, ind]=ismember('fname', varargin(1:2:end));
        if (hasP && ischar(varargin{ind*2}))
            fname = varargin{ind*2};
            varargin(ind*2-1:ind*2) = [];
        else
            fname = {obj.name 'colormap' obj.cmap.defname 'type' obj.cmap.name};

            if (~isempty(vars_pars.ids))
                ids = vars_pars.ids;
                if length(ids) <= 5
                    fname = [fname, {strrep(num2str(ids), '  ', '_')}];
                else
                    fname = [fname, {['slice' upper(obj.conf.plane)] num2str(obj.conf.slicendx)}];
                end
            elseif (~vars_pars.rendering)
                fname = [fname, {['slice' upper(obj.conf.plane)] num2str(obj.conf.slicendx)}];
            end
            fname = [fname, {obj.extras.displayedFlag}];

            if (vars_pars.neighbors)
                fname = [fname, {'neighbors'}];
            end
            if (vars_pars.rendering)
                fname = [fname, {'rendering'}];
            elseif (~vars_pars.GMgui && ~vars_pars.ipf)
                fname = [fname, {'unitcells'}];
            end
            if ~isempty(varargin)
                fname = [fname, varargin{:}];
            end
            fname(2, :) = repmat({'_'}, 1, length(fname));
            fname{2, end} = '';
            fname = [fname{:}];
        end

        vars_pars.fname = fname;
    end

    function h = displayRenderGrains(obj, point, ids, rendering, cluster, oversize, varargin)
    % GTTWINANALYSIS/DISPLAYRENDERGRAINS
    %
    % h = displayRenderGrains(obj, point, ids, rendering, cluster, oversize, varargin)

        if ~exist('ids', 'var')
            ids = [];
        end
        if ~exist('rendering', 'var') || isempty(rendering)
            rendering = true;
        end
        if ~exist('cluster', 'var') || isempty(cluster)
            cluster = false;
        end
        if ~exist('oversize', 'var') || isempty(oversize)
            oversize = 1;
        end
        [list, figtitle, pID] = obj.getIDlist(point, cluster, oversize);

        if isempty(ids)
            if (~cluster)
                % create IDs list
                prompt = {'Enter parent/twin(s) ID(s) space-separated:'};
                dlg_title = 'Input';
                num_lines = [1 50];
                if ~isempty(list)
                    def = {num2str(list(1))};
                else
                    def = {''};
                end
                x = inputdlg(prompt, dlg_title, num_lines, def);
                if ~isempty(x)
                    ids = str2num(x{:});
                else
                    disp('No grains to render...Quitting')
                    return
                end
            else
                ids = list;
            end
        end

        params = obj.getParams();
        phases_num = numel(params.cryst);

        % load grain from index
        [hasP, ind]=ismember('grain', varargin(1:2:end));
        if (hasP)
            grain = varargin{ind*2};
            varargin(ind*2-1:ind*2) = [];
        else
            grain = [];
            for iphase = 1:phases_num
                grain = [grain; load(fullfile(obj.dataDir, '4_grains', sprintf('phase_%02d', iphase), 'index.mat'), 'grain')];
            end
        end
        % pixelsize
        [hasP, ind] = ismember('pxsize', varargin(1:2:end));
        if (hasP)
            pxsize = varargin{ind*2};
            varargin(ind*2-1:ind*2) = [];
        else
            pxsize = mean(params.recgeo(1).voxsize);%mm
        end
        % colormap
        [hasP, ind]=ismember('cmap', varargin(1:2:end));
        if (hasP)
            colorMap = varargin{ind*2};
            varargin(ind*2-1:ind*2) = [];
        else
            colorMap = obj.conf.cmap(2:end, :);
        end
        [hasP, ind]=ismember('vol', varargin(1:2:end));
        if (hasP)
            vol = varargin{ind*2};
            varargin(ind*2-1:ind*2) = [];
        else
            vol = obj.conf.vol;
            if strcmpi(obj.conf.load_vol, 'dilated') && any(obj.conf.vol(:) == -1) && all(colorMap(1, :) == obj.cmap.conflict)
                colorMap(1, :) = [];
            end
        end
        [vars_pars, vars_list] = gtAppDrawGrainPars({grain});
        ind = findValueIntoCell(vars_list(:, 3), {'cell'});
        vars_list(ind(:, 1), 4) = {-1};
        ind = findValueIntoCell(vars_list(:, 1), {'figpos', 'figcolor'});
        vars_list(ind(:, 1), 4) = {2};
        ind = findValueIntoCell(vars_list(:, 1), {'secpos', 'plane'});
        vars_list(ind(:, 1), 4) = {3};
        ind = findValueIntoCell(vars_list(:, 1), {'legendpos'});
        vars_list(ind(:, 1), 4) = {4};

        vars_pars = gtModifyStructure(vars_pars, vars_list, 1, 'gtDrawGrainUnitCells options:');
        if (vars_pars.figure)
            vars_pars = gtModifyStructure(vars_pars, vars_list, 2, 'Figure options:');
        end
        if (vars_pars.section)
            vars_pars = gtModifyStructure(vars_pars, vars_list, 3, 'Section options:');
        end
        if (vars_pars.legend)
            vars_pars = gtModifyStructure(vars_pars, vars_list, 4, 'Legend options:');
        end

        varargin = [varargin, struct2pv(rmfield(vars_pars, vars_list(ind(:, 1), 1)))];

        % gtDrawGrainUnitCells default pars
        vargs_in = {'pixels', true, 'sampleaxes', false, 'sampleenv', true, 'showaxes', 'on', 'draw', true, 'figure', true};

        % add part where to prepare input for pymicro movie

        h.axes = [];
        if (rendering)
            try
                [h, ~]=gtRenderGrains(vol, 'grainids', ids, 'cmap', colorMap, ...
                    'lighting', 'phong', 'grid', false, 'showaxes', true, 'showlabel', false, 'box', 'off', ...
                    'pixels', true, 'sampleenv', true, 'sampleaxes', false, 'pxsize', pxsize);
                h.view = get(h.axes(1), 'View');
                view(0, -90)
                camlight
                set(h.axes(end), 'View', h.view, ...
                    'Tag', 'h_rendering', ...
                    'Position', [0.0592 0.1100 0.8458 0.8150])
                % shift grains in the center of the sample envelope (hard
                % coded)
                h.vertices = get(h.patch, 'Vertices');
                h.shift = h.volsize/2;
                set(h.patch, 'Vertices', h.vertices-repmat(h.shift + [-30 10 0], size(h.vertices, 1), 1));
                h.vertices = get(h.patch, 'Vertices');
                axes(h.axes(1))
                zoom(0.7)
            catch Mexc
                gtPrintException(Mexc)
            end
            % show unit cells only if it is a parent/twin plot, supposing
            % less than 5 grains
            if (cluster) || (length(ids) <= 5)
                h.axes(end+1) = copyobj(h.axes(1), h.fig);
                delete(findobj(h.axes(end), 'type', 'patch'))
                delete(findobj(h.axes(end), 'type', 'text'))
                [hh, p_h] = gtDrawGrainUnitCells({grain, grain}, ...
                    'cmap', {colorMap, colorMap}, 'pxsize', {pxsize, pxsize}, ...
                    'ids', {ids(1), ids(2:end)}, 'alpha', {1, 1}, 'patch', {false, true}, ...
                    vargs_in{:}, varargin{:});
                p_h = cat(2, p_h{:});
                copyobj(cat(2, p_h{:}), h.axes(end))
                axes(h.axes(end))
                set(h.axes(end), 'Position', [0.6761 0.5911 0.2740 0.2500], 'Tag', 'h_unitcells')
                h.appdata = getappdata(hh.fig, 'AppData');
                close(hh.fig)
            elseif vars_pars.paired || (size(ids, 2) == 2 && size(ids, 1) > 0)
                h.axes(end+1) = copyobj(h.axes(1), h.fig);
                delete(findobj(h.axes(end), 'type', 'patch'))
                delete(findobj(h.axes(end), 'type', 'text'))
                [hh, p_h] = gtDrawGrainUnitCells({grain, grain}, ...
                    'cmap', {colorMap, colorMap}, 'pxsize', {pxsize, pxsize}, ...
                    'ids', {ids(:, 2), ids(:, 1)}, 'alpha', {1, 1}, 'patch', {false, true}, ...
                    vargs_in{:}, varargin{:});
                p_h = cat(2, p_h{:});
                copyobj(cat(2, p_h{:}), h.axes(end))
                axes(h.axes(end))
                zoom(1.3)
                set(h.axes(end), 'Position', [0.6761 0.5911 0.2740 0.2500], 'Tag', 'h_unitcells')
                h.appdata = getappdata(hh.fig, 'AppData');
                close(hh.fig)
            elseif length(ids) == 1
                [~, handles]=gtPlotHexagon(grain, 'ids', ids, 'cmap', colorMap, ...
                    'reference', false, 'overlap', false, 'caxis', true, 'alpha', 0.4);
                p_h = handles.hp;
                h.axes(end+1) = copyobj(h.axes(1), h.fig);
                delete(findobj(h.axes(end), 'type', 'patch'))
                delete(findobj(h.axes(end), 'type', 'text'))
                copyobj(vertcat(p_h{:}), h.axes(end))
                axes(h.axes(end))
                set(h.axes(end), 'Position', [0.6761 0.5911 0.2740 0.2500])
                set(h.axes(end), 'Visible', 'off', 'Tag', 'h_plothexagon')
                close(handles.hf)
            end

            set(h.fig, 'Units', 'normalized', 'Position', [0.3 0.3 0.55 0.5])
        else
            if length(ids) > 1
                [hasP, ind]=ismember('cmap', varargin(1:2:end));
                if (hasP)
                    varargin(ind*2-1:ind*2) = [];
                end
                [hasP, ind]=ismember('ids', varargin(1:2:end));
                if (hasP)
                    varargin(ind*2-1:ind*2) = [];
                end
                [hasP, ind]=ismember('pxsize', varargin(1:2:end));
                if (hasP)
                    varargin(ind*2-1:ind*2) = [];
                end
                [hasP, ind]=ismember('alpha', varargin(1:2:end));
                if (hasP)
                    varargin(ind*2-1:ind*2) = [];
                end
                if (cluster)
                    [hasP, ind]=ismember('patch', varargin(1:2:end));
                    if (hasP)
                        varargin(ind*2-1:ind*2) = [];
                    end
                    [h, ~] = gtDrawGrainUnitCells({grain, grain}, ...
                        'cmap', {colorMap, colorMap}, 'pxsize', {pxsize, pxsize}, ...
                        'ids', {ids(1), ids(2:end)}, 'alpha', {1, 1}, 'patch', {true, false}, ...
                        vargs_in{:}, varargin{:});
                elseif vars_pars.paired || (size(ids, 2) == 2 && size(ids, 1) > 0)
                    [hasP, ind]=ismember('patch', varargin(1:2:end));
                    if (hasP)
                        varargin(ind*2-1:ind*2) = [];
                    end
                    [h, ~] = gtDrawGrainUnitCells({grain, grain}, ...
                        'cmap', {colorMap, colorMap}, 'pxsize', {pxsize, pxsize}, ...
                        'ids', {ids(:, 2), ids(:, 1)}, 'alpha', {1, 1}, 'patch', {false, true}, ...
                        vargs_in{:}, varargin{:});
                else
                    [h, ~] = gtDrawGrainUnitCells({grain}, ...
                        'cmap', {colorMap}, 'pxsize', {pxsize}, ...
                        'ids', {ids}, 'alpha', {1}, ...
                        vargs_in{:}, varargin{:});
                end
                h.axes = get(h.fig, 'CurrentAxes');
                h.view = [30 40];
                set(h.axes(end), 'View', h.view, ...
                    'Tag', 'h_unitcells')
                h.appdata = getappdata(h.fig, 'AppData');
                set(h.fig, 'Units', 'normalized', 'Position', [0.2 0.3 0.6 0.5])
            end
        end

        set(h.fig, 'Name', figtitle);
        % sample axes
        if ~isempty(findobj(h.fig, 'tag', 'sampleaxes'))
            saxes = findobj(h.fig, 'tag', 'sampleaxes');
            h.xaxis = saxes(3);
            h.yaxis = saxes(2);
            h.zaxis = saxes(1);
        else
            h.axes(end+1) = copyobj(h.axes(1), h.fig);
            delete(findobj(h.axes(end), 'type', 'patch'))
            delete(findobj(h.axes(end), 'type', 'line'))
            axes(h.axes(end))
            hold(h.axes(end), 'on')
            h.xaxis = gtDrawArrow3([0 0 0], [150 0 0], 'color', 'red', 'stemWidth', 3);
            h.yaxis = gtDrawArrow3([0 0 0], [0 150 0], 'color', 'green', 'stemWidth', 3);
            h.zaxis = gtDrawArrow3([0 0 0], [0 0 150], 'color', 'blue', 'stemWidth', 3);
            set(h.axes(end), 'Position', [0.07 0.0763 0.3000 0.3000])
            hold(h.axes(end), 'on');
            xlim(h.axes(end), [-150 150])
            ylim(h.axes(end), [-150 150])
            zlim(h.axes(end), [-150 150])
            h.dummy = gtDrawArrow3([-150 -150 -150], [150 150 150], 'color', 'white', 'stemWidth', 3);
            set(h.dummy, 'Visible', 'off')
            set(h.axes(end), 'Visible', 'off')
            set(h.axes(end), 'View', h.view, 'Tag', 'h_sampleref')
            %set([h.xaxis h.yaxis h.zaxis], 'FaceLighting', 'none')
            zoom(1.2)
        end
        % sample axes labels
        if ~isempty(findobj(h.fig, 'tag', 'sampletext'))
            stext = findobj(h.fig, 'tag', 'sampletext');
            h.xtext = stext(3);
            h.ytext = stext(2);
            h.ztext = stext(1);
            set(h.xtext, 'String', 'TD');
            set(h.ytext, 'String', 'ND');
            set(h.ztext, 'String', 'LD');
        else
            h.xtext = text(160, -10, -10, 'TD', 'color', 'red', 'fontsize', 12);
            h.ytext = text(-10, 160, -10, 'ND', 'color', 'green', 'fontsize', 12);
            h.ztext = text(-10, -10, 160, 'LD', 'color', 'blue', 'fontsize', 12);
        end

        ids = reshape(ids, [1 numel(ids)]);

        set(h.fig, 'Name', ['Phase: ' num2str(pID) ' Grains: [' num2str(ids) ']'])
        set(h.fig, 'Color', [1 1 1])
        set(h.fig, 'Toolbar', 'none')

        axis(h.axes, 'equal')
        axis(h.axes, 'vis3d')
        h.link = linkprop(h.axes, {'View'});
        setappdata(h.fig, 'linkprop', h.link)

        % colorbar / colormap key
        if ismember(obj.cmap.defname, {'IPF'})
            % axes 4
            obj.selectedPhase = max(1, obj.selectedPhase);
            handles = gtIPFCmapKey('crystal_system', obj.p_crystalsystem{obj.selectedPhase}, 'fontsize', 10);
            h.axes(end+1) = copyobj(get(handles.h_figure, 'CurrentAxes'), h.fig);
            set(h.axes(end), 'Position', [0.85 0.04 0.1489 0.1557])
            set(h.axes(end), 'Visible', 'off')
            delete(handles.h_figure)
            clear handles
        elseif ismember(obj.cmap.defname, {'Caxis'})
            % axes 4
            h_figure = gtCrystAxisCmap_key([], {'X', 'Y', 'Z'}, false, 12);
            h.axes(end+1) = copyobj(get(h_figure, 'CurrentAxes'), h.fig);
            set(h.axes(end), 'Position', [0.84 0.04 0.1589 0.1857])
            set(h.axes(end), 'Visible', 'off', 'Tag', 'h_cbar')
            axis(h.axes(end), 'square')
            delete(h_figure)
            clear h_figure
        elseif ismember(obj.cmap.defname, {'Rvec'})
            [h_figure, hc] = gtColorspace(1, {'Rx', 'Ry', 'Rz'}, 'FontSize', 12, 'LineWidth', 1);
            h.axes(end+1) = copyobj(get(h_figure, 'CurrentAxes'), h.fig);
            set(h.axes(end), 'Position', [0.74 0.04 0.2589 0.2857])
            set(h.axes(end), 'Visible', 'off', 'Tag', 'h_cbar')
            axis(h.axes(end), 'equal')
            %axis(h.axes(end), 'square')
            delete(h_figure)
            clear h_figure
        elseif ~ismember(obj.cmap.defname, {'Rand'})
            % colorbar
            if isfield(obj.conf, 'h_cmap') && isa(obj.conf.h_cmap, 'GtColormap') && ishandle(obj.conf.h_cmap.h_fig) && ~isempty(findobj(obj.conf.h_cmap.h_fig, 'tag', 'Colorbar'))
                h_cb = copyobj(findobj(obj.conf.h_cmap.h_fig, 'tag', 'Colorbar'), h.fig);
            elseif ~isempty(findobj(h.fig, 'Tag', 'Colorbar'))
                h_cb = findobj(h.fig, 'Tag', 'Colorbar');
            else
                obj.getVolumeSlice([], '', 'hide', [1:12, 19], varargin{:});
                h_cb = copyobj(findobj(obj.conf.h_cmap.h_fig, 'tag', 'Colorbar'), h.fig);
                close(obj.conf.h_cmap.h_fig);
            end
            obj.conf.h_cbar = h_cb;
            set(h_cb, 'Position', [0.9081 0.1143 0.0280 0.8198]);
        end

        axis(h.axes(1), 'tight')
        % add menu item
        hm = uimenu(h.fig, 'Label', 'Rendering', 'ForegroundColor', 'red');
        uimenu(hm, 'Label', 'Save Figure', 'Callback', @(src, evt)saveCmapFigure(obj, h.fig, 'ids', ids, 'rendering', rendering, 'neighbors', cluster, varargin{:}))
        uimenu(hm, 'Label', 'Create Movie', 'Callback', @(src, evt)createMovie(obj, h.fig, 'ids', ids, 'rendering', rendering, 'neighbors', cluster, varargin{:}))

        h.ids = ids;
        h.colorMap = colorMap;
        h.pxsize = pxsize;
        set(h.fig, 'Tag', 'h_fig_rendering')
        obj.conf.h_rendering = h.fig;

        setappdata(obj.conf.h_figure, 'renderdata', h)
    end

    function h = createMovie(obj, h_fig, varargin)
    % GTTWINANALYSIS/CREATEMOVIE  Creates a movie of the current figure by
    % rotating the current axes
    %
    % h = createMovie(obj, h_fig, varargin)

        vars_pars = obj.setFigureName(varargin{:});
        h.movname = [vars_pars.fname '.avi'];

        if (vars_pars.ws_path)
            h.movname = fullfile(getenv('HOME'), 'workspace', h.movname);
        else
            h.movname = fullfile('8_analysis', 'figures', h.movname);
        end

        tic
        [h.mov, h.wobj] = gtRenderMovie(h_fig, h.movname, ...
            'viewaz', [-30 150 330 150 -30], 'viewel', [30 0 -30 0 30], ...
            'nstep', 10, 'framerate', 5, 'showaxes', true, 'showmov', false, varargin{:});
        toc

        h.fig = h_fig;
        obj.conf.h_rendering = h.fig;
        setappdata(obj.conf.h_figure, 'moviedata', h)
    end

    function [parent, twins, list] = getTwinParentNeighbors(obj, gID, oversize, includeCheckList)
    % GTTWINANALYSIS/GETTWINPARENTNEIGHBORS  Gets the twinned grains and
    % relative neighbors for a grain and saves the list into
    % obj.extras.selected.id (.twins) (.list)
    %
    % [parent, twins, list] = getTwinParentNeighbors(obj, gID, oversize, includeCheckList)

        if ~exist('oversize', 'var') || isempty(oversize)
            oversize = 1;
        end
        if ~exist('includeCheckList', 'var') || isempty(includeCheckList)
            includeCheckList = true;
        end

        [list, figtitle] = obj.getNeighborhood(gID, oversize);
        list(find(list == gID)) = [];
        twins = [];
        parent = gID;
        obj.extras.selected.id = gID;
        twin_list = zeros(0, 2);
        if isfield(obj.extras.twinInfo, 'twin_list') && ~isempty(obj.extras.twinInfo.twin_list)
            twin_list = obj.extras.twinInfo.twin_list;
        end
        if isfield(obj.extras.twinInfo, 'check_list') && ~isempty(obj.extras.twinInfo.check_list) && includeCheckList
            twin_list = cat(1, twin_list, obj.extras.twinInfo.check_list);
        end
        if ~isempty(twin_list)
            ind = find(twin_list(:, 2) == gID);
            twins = twin_list(ind, 1);
            list(ismember(list, twins)) = [];
            obj.extras.selected.twins = reshape(twins, [1 numel(twins)]);
        end
        obj.extras.selected.list = list;
    end

    function displayIPFgrainList(obj, ids, varargin)
    % GTTWINANALYSIS/DISPLAYIPFGRAINLIST
    %
    % displayIPFgrainList(obj, ids, varargin)

        hf = [];

        [hasP, ind] = ismember('figure_grid', varargin(1:2:end));
        if (hasP)
            figure_grid = varargin{ind*2};
            varargin(ind*2-1:ind*2) = [];
        else
            figure_grid = true;
        end
        [hasP, ind] = ismember('twins', varargin(1:2:end));
        if (hasP)
            do_twins = varargin{ind*2};
            varargin(ind*2-1:ind*2) = [];
        else
            do_twins = true;
        end
        [hasP, ind] = ismember('similar', varargin(1:2:end));
        if (hasP)
            similar = varargin{ind*2};
            varargin(ind*2-1:ind*2) = [];
        else
            similar = false;
        end
        [hasP, ind] = ismember('id', varargin(1:2:end));
        if (hasP && similar)
            id = varargin{ind*2};
            varargin(ind*2-1:ind*2) = [];
        else
            id = [];
        end
        if length(id) ~= length(ids)
            id = [];
        end

        varargin = cat(2, varargin, {'linecolor', [1 1 1], 'figcolor', [0 0 0], ...
            'fontsize', 10});
        if (~figure_grid)
            for ii=1:length(ids)
                [parent, twins, list] = obj.getTwinParentNeighbors(ids(ii));
                if (~do_twins)
                    twins = [];
                end
                if (similar && ~isempty(id(ii)))
                    twins = id(ii);
                    list(ismember(list, twins)) = [];
                    label_t = ['similar id ' num2str(twins)];
                    label_p = ['grain ' num2str(parent)];
                else
                    label_p = ['grain ' num2str(parent)];
                    label_t = 'twins';
                end

                hf(ii) = obj.displayInversePoleFigure('grainids', parent, 'cmap', [0 1 0], 'label', label_p, ...
                    'markertype', 's', 'markersize', 20, varargin{:});
                if ~isempty(twins)
                    obj.displayInversePoleFigure('grainids', twins, 'cmap', [1 0 0], 'label', label_t, ...
                        'markertype', '^', 'markersize', 30, 'hf', hf(ii), varargin{:});
                end
                obj.displayInversePoleFigure('grainids', list, 'cmap', [1 1 0], 'label', 'neighbors', ...
                    'markertype', 'd', 'markersize', 20, 'hf', hf(ii), varargin{:});
            end
        else
            n_fig = length(ids);
            n_rows = ceil(n_fig/3);
            n_cols = 3;
            for ii=1:length(ids)
                [parent, twins, list] = obj.getTwinParentNeighbors(ids(ii));
                hf(ii) = gtSubFigure(n_rows, n_cols, ii);
                if (~do_twins)
                    twins = [];
                end
                if (similar && ~isempty(id(ii)))
                    twins = id(ii);
                    list(ismember(list, twins)) = [];
                    label_t = ['similar id ' num2str(twins)];
                    label_p = ['grain ' num2str(parent)];
                else
                    label_p = ['grain ' num2str(parent)];
                    label_t = 'twins';
                end

                obj.displayInversePoleFigure('grainids', parent, 'cmap', [0 1 0], 'label', label_p, ...
                    'markertype', 's', 'markersize', 10, 'hf', hf(ii), varargin{:});
                if ~isempty(twins)
                    obj.displayInversePoleFigure('grainids', twins, 'cmap', [1 0 0], 'label', label_t, ...
                        'markertype', '^', 'markersize', 15, 'hf', hf(ii), 'ha', get(hf(ii), 'CurrentAxes'), varargin{:});
                end
                obj.displayInversePoleFigure('grainids', list, 'cmap', [1 1 0], 'label', 'neighbors', ...
                    'markertype', 'd', 'markersize', 10, 'hf', hf(ii), 'ha', get(hf(ii), 'CurrentAxes'), varargin{:});
            end
        end
    end

    function [list, figtitle] = getNeighborhood(obj, gID, oversize)
    % GTTWINANALYSIS/GETNEIGHBORHOOD  Gets the neighborhood list for a
    % grain and creates the figure title
    %
    % [list, figtitle] = getNeighborhood(obj, gID, oversize)

        if ~exist('oversize', 'var') || isempty(oversize)
            oversize = 1;
        end

        pID = obj.phaseIDs(gID);
        sample = obj.getSample();
        phase = sample.phases{pID};
        list = [gID; phase.getNeighbours(gID, oversize)]';
        figtitle = [strrep(obj.name, '_', ' ') ' - ' obj.cmap.defname ' map - ' sprintf('Neighbors grain %d phase %d', gID, pID)];
    end

    function out = getNeighborsMisorientation(obj, ids, varargin)
    % GTTWINANALYSIS/GETNEIGHBORSMISORIENTATION  Gets the misorientation
    % between a grain and its neoghborhood
    %
    % out = getNeighborsMisorientation(obj, ids, varargin)

        out = [];
        for ii = 1:length(ids)
            list = obj.getNeighborhood(ids(ii));
            for jj = 2:length(list)
                out{ii, jj} = obj.calculateMis(list(1), list(jj), varargin{:});
            end
        end
    end

    function mis = getMisorientation(obj, list, varargin)
    % GTTWINANALYSIS/GETMISORIENTATION  Calculates the misorientation
    % between a list of grains with respect to all the grains, using the
    % symmetry operators relative to the first phase.
    %
    % mis = getMisorientation(obj, list, varargin)

        mis = gtMathsMisorientation(obj.r_vectors(:, list)', obj.r_vectors.', obj.p_symm{1}, false, true);
    end

    function out = calculateMis(obj, mis1, mis2, varargin)
    % GTTWINANALYSIS/CALCULATEMIS  Calculates the misorientation between
    % two grains, giving the Hcp axes convention and the merging angle,
    % within the rotation mode (See Rollet lectures)
    %
    % out = calculateMis(obj, mis1, mis2, varargin)

        par.convention  = 'X';
        par.merge_angle = 5;
        par.mode        = 'passive';
        [par, rej_pars] = parse_pv_pairs(par, varargin);

        params = obj.getParams();
        vox_size_um = mean(params.recgeo(1).voxsize) * 1e3;
        size_vol = arrayfun(@(x) (sum(obj.conf.vol(:)==x))^(1/3), 1:obj.Ngrains)' * vox_size_um; %voxels^3 -> um

        pID       = obj.phaseIDs(mis1);
        if (obj.phaseIDs(mis2) ~= pID)
            disp('Phase number must be the same between the two compared grains...Quitting')
            return
        end
        twinModel = fullfile('4_grains', 'phase_%02d', 'twinOut.mat');

        % Get symmetry operators
        symm      = obj.p_symm{pID};
        rvectors  = obj.r_vectors.';
        centroids = obj.centers.' * vox_size_um; %pixels -> um

        out = [];
        if exist(sprintf(twinModel, pID), 'file')
            twinOut = [];
            load(sprintf(twinModel, pID));
            IDs_1 = gtGetAllCellValues(twinOut, 'grainID_1');
            IDs_2 = gtGetAllCellValues(twinOut, 'grainID_2');
            ind = find((IDs_1 == mis1 & IDs_2 == mis2) | (IDs_1 == mis2 & IDs_2 == mis1));
            if ~isempty(ind)
                out = twinOut{ind};
            end
        end

        if isempty(out)
            sigmas = gtCrystGetSigmas(obj.p_crystalsystem{pID}, ...
                obj.p_latticepar{pID}, ...
                par.convention);

            [out, ~] = gtCrystTwinTest(rvectors(mis1, :), rvectors(mis2, :), ...
                pID, 'symm', symm, 'sigmas', sigmas, ...
                'merge_angle', par.merge_angle, ...
                'convention', par.convention, 'mode', par.mode);

            % it goes now from -90 to 90
            out.mis_angle( find( out.mis_angle > 90 | out.mis_angle < -90 ) ) = out.mis_angle( find( out.mis_angle > 90 | out.mis_angle < -90 ) ) - 180;

            out.grainID_1 = mis1;%(out.id1)';
            out.grainID_2 = mis2;%(out.id2)';
        end

        out.xyz       = (centroids(out.grainID_2, :) - centroids(out.grainID_1, :)).'; %um
        out.dist      = sqrt(sum(out.xyz .* out.xyz, 1)); %um
        out.Rvec      = (rvectors(out.grainID_2, :) - rvectors(out.grainID_1, :)).';
        out.Rdist     = sqrt(sum(out.Rvec .* out.Rvec, 1));
        out.size1     = size_vol(out.grainID_1, :).'; %um
        out.size2     = size_vol(out.grainID_2, :).'; %um
        out.scale     = out.size2 ./ out.size1;
        out.Sdev1     = (out.size2 - out.size1)./out.size1;
        out.Sdev2     = (out.size1 - out.size2)./out.size2;

        % deviation in rotation
        out.g1        = obj.orientations(:, :, out.grainID_1);
        out.g2        = obj.orientations(:, :, out.grainID_2);
        out.rot       = permute(out.g1, [2 1 3]) - out.g2; % transpose g1 - g2
        out.ii        = 1:length(mis1);

    end

    function slipTr = displaySlipTransferNeighborhood(obj, point, varargin)
    % GTTWINANALYSIS/DISPLAYSLIPTRANSFERNEIGHBORHOOD
    % slipTr = displaySlipTransferNeighborhood(obj, point, varargin)

        [list, ~, ~] = obj.getIDlist(point, true, 1);
        if isempty(list)
            disp('No grain selected... Quitting')
            return
        end
        grainID = list(1);
        IDs = list(2:end)';

        vars_pars.maxSystem = true;
        vars_pars.verbose   = false;
        vars_pars.fam       = [];
        vars_pars.thr_value = 0.6;
        vars_pars.write_to_file = true;
        rej_pars = [];
        if ~isempty(obj.extras.vars_pars)
            [vars_pars, rej_pars] = parse_pv_pairs(vars_pars, struct2pv(obj.extras.vars_pars));
        end

        if isempty(varargin)
            vars_list = get_structure(vars_pars, [], false, [], []);
            vars_list(:, 2) = {'Display the max value among the slip systems', ...
                'Verbosity on/off', ...
                'Slip family to be considered', ...
                'Threshold value for slip transfer', ...
                'Flag to write to text file the resutls'};
            vars_pars = gtModifyStructure(vars_pars, vars_list, 1, 'Options for calculating the Slip Transfer:');
        else
            [vars_pars, rej_pars] = parse_pv_pairs(vars_pars, varargin);
        end
        varargin = struct2pv(rmfield(vars_pars, {'thr_value', 'write_to_file'}));

        maxSlip = {};
        nFam    = {};

        for ii = 1:length(IDs)
            [maxSlip{ii}, nFam{ii}] = obj.calculateSlipTransfer(grainID, IDs(ii), varargin{:});
        end

        if (vars_pars.write_to_file)
            filename = sprintf('slipTransfer_neighborhood_grain%04d.txt', grainID);
            diary(filename)
        end
        disp(' ')
        disp('Slip Transfer')
        fprintf('Neighborhood of Grain : %d\n', grainID)
        if (vars_pars.maxSystem)
            disp('maximum slip transfer considered')
        end
        if all(size(maxSlip{1}) == [1, 1])
            fprintf('Family: %d - %d\n', nFam{1})
            values = [maxSlip{:}]';
            disp(['   grainID   maxSlip ' sprintf('   >= %0.2f', vars_pars.thr_value)])
            arrayfun(@(x, y, z) fprintf(' %9d %9f %7d\n', x, y, z), IDs, values, (values >= vars_pars.thr_value))
        elseif size(maxSlip{1}, 1) == size(maxSlip{1}, 2)
            nLength = cellfun(@(x) length(x), maxSlip);

            for jj = 1:length(IDs)
                values = maxSlip{jj};
                if ~isempty(vars_pars.fam)
                    fprintf('Family: %d\n', vars_pars.fam)
                    for ii = 1:max(nLength(:))
                        fprintf('    fam %2d', ii)
                    end
                    fprintf('\n')
                    disp(values(vars_pars.fam, :))
                elseif (vars_pars.maxSystem)
                    fprintf(' %9d %9d %9d %9f\n', IDs(jj), nFam{jj}(1), nFam{jj}(2), values(nFam{jj}(1), nFam{jj}(2)))
                else
                    fprintf('  gID %4d', IDs(jj))
                    for ii = 1:max(nLength(:))
                        fprintf('    fam %2d', ii)
                    end
                    fprintf('\n')
                    values = [(1:size(maxSlip{jj}, 1))' values];
                    disp(values)
                end
                disp(' ')
            end

        else
            nLength = cellfun(@(x) length(x), maxSlip);

            output = '';
            fprintf('   grainID')
            if length(nFam{1}) == 2
                fprintf('      Max1      Max2')
            else
                fprintf('    Family')
            end
            if ~isempty(vars_pars.fam)
                for ii = 1:max(nLength(:))
                    fprintf('   slip %2d', ii)
                    output = [output ' %9f'];
                end
            elseif (~vars_pars.maxSystem)
                for ii = 1:max(nLength(:))
                    fprintf(' system %2d', ii)
                    output = [output ' %9f'];
                end
            end
            fprintf('\n')
            if (~vars_pars.maxSystem)
                for jj = 1:length(IDs)
                    values = maxSlip{jj};
                    output_current = sprintf(output, values, NaN(1, max(nLength(:)) - nLength(jj)));
                    if length(nFam{jj}) == 2
                        fprintf(' %9d %9d %9d%s\n', IDs(jj), nFam{jj}(1), nFam{jj}(2), output_current)
                    else
                        fprintf(' %9d %9d%s\n', IDs(jj), nFam{jj}, output_current)
                    end
                end
            else
                if isempty(vars_pars.fam)
                    for jj = 1:length(IDs)
                        values = maxSlip{jj}(nFam{jj}(2));
                        fprintf(' %9d %9d %9d %9f\n', IDs(jj), nFam{jj}(1), nFam{jj}(2), values)
                    end
                else
                    for jj = 1:length(IDs)
                        values = maxSlip{jj};
                        output_current = sprintf(output, values, NaN(1, max(nLength(:)) - nLength(jj)));
                        fprintf(' %9d %9d %s\n', IDs(jj), vars_pars.fam, output_current)
                    end
                end
            end
        end
        disp(' ')

        if (vars_pars.write_to_file)
            diary('off')
            disp(['Results are written in file: ' filename])
        end

        if nargout > 0
            slipTr.maxSlip = maxSlip;
            slipTr.nFam    = nFam;
        end

        obj.extras.vars_pars = vars_pars;
        if ~isempty(rej_pars), rej_pars = cell2struct(rej_pars(2:2:end), rej_pars(1:2:end), 2); end
        obj.extras.rej_pars  = rej_pars;
    end

    function slipTr = displaySlipTransferGrainList(obj, ids, varargin)
    % GTTWINANALYSIS/DISPLAYSLIPTRANSFERGRAINLIST
    % slipTr = displaySlipTransferGrainList(obj, ids, varargin)

        vars_pars.maxSystem = true;
        vars_pars.verbose   = false;
        vars_pars.fam       = [];
        vars_pars.thr_value = 0.6;
        vars_pars.write_to_file = true;
        rej_pars = [];
        if ~isempty(obj.extras.vars_pars)
            [vars_pars, rej_pars] = parse_pv_pairs(vars_pars, struct2pv(obj.extras.vars_pars));
        end

        if isempty(varargin)
            vars_list = get_structure(vars_pars, [], false, [], []);
            vars_list(:, 2) = {'Display the max value among the slip systems', ...
                'Verbosity on/off', ...
                'Slip family to be considered', ...
                'Threshold value for slip transfer', ...
                'Flag to write to text file the resutls'};
            vars_pars = gtModifyStructure(vars_pars, vars_list, 1, 'Options for calculating the Slip Transfer:');
        else
            [vars_pars, rej_pars] = parse_pv_pairs(vars_pars, varargin);
        end
        varargin = struct2pv(rmfield(vars_pars, {'thr_value', 'write_to_file'}));


        if (vars_pars.write_to_file)
            filename = gtLastFileName('slipTransfer_idlist_', 'new', 'log');
            diary(filename)
        end

        maxSlip = {};
        nFam = {};
        list = [];
        for jj = 1:length(ids)
            kk = ids(jj);
            list{jj} = obj.getNeighborhood(kk);
            maxSlip{jj, 1} = list{jj}';
            for ii = 2:length(list{jj})
                [maxSlip{jj, ii}, nFam{jj, ii}] = obj.calculateSlipTransfer(list{jj}(1), list{jj}(ii), varargin{:});
            end
            fprintf('Grain : %d\n', kk)
            if length(maxSlip{jj, 2}) == 1
                fprintf('Family: %d\n', nFam{jj, ii})
                values = [maxSlip{jj, 2:end}]';
                IDs = list{jj}(2:end)';
                disp(['   grainID   maxSlip ' sprintf('   >= %0.2f', vars_pars.thr_value)])
                disp([IDs values (values >= vars_pars.thr_value)])
            else
                values = vertcat(maxSlip{jj, 2:end});
                IDs = list{jj}(2:end)';
                fprintf('   grainID    Family')
                for ii=1:length(obj.p_slipFamilies{obj.phaseIDs(kk)})
                    fprintf('   slip %2d', ii)
                end
                fprintf('   >= %0.2f', vars_pars.thr_value)
                fprintf('\n')
                disp([IDs vertcat(nFam{jj, 2:end}) values (values >= vars_pars.thr_value)])
            end
            disp(' ')
        end
        if (vars_pars.write_to_file)
            diary('off')
            disp(['Results are written in file: ' filename])
        end
        if nargout > 0
            slipTr.maxSlip = maxSlip;
            slipTr.nFam    = nFam;
        end

        obj.extras.vars_pars = vars_pars;
        if ~isempty(rej_pars), rej_pars = cell2struct(rej_pars(2:2:end), rej_pars(1:2:end), 2); end
        obj.extras.rej_pars  = rej_pars;
    end

    function [maxSlip, nfam] = calculateSlipTransfer(obj, mis1, mis2, varargin)
    % GTTWINANALYSIS/CALCULATESLIPTRANSFER  Calculates slip transfer parameter
    % between two grains
    %
    % [maxSlip, nfam] = calculateSlipTransfer(obj, mis1, mis2, varargin)

        vars_pars.maxSystem = true;
        vars_pars.verbose   = true;
        vars_pars.fam       = [];
        rej_pars = [];
        if ~isempty(obj.extras.vars_pars)
            [vars_pars, rej_pars] = parse_pv_pairs(vars_pars, struct2pv(obj.extras.vars_pars));
        end
        if isempty(varargin)
            vars_list = get_structure(vars_pars, [], false, [], []);
            vars_list(:, 2) = {'Display the max value among the slip systems', ...
                'Verbosity on/off', ...
                'Slip family to be considered'};
            vars_pars = gtModifyStructure(vars_pars, vars_list, 1, 'Options for calculating the Slip Transfer:');
        else
            [vars_pars, rej_pars] = parse_pv_pairs(vars_pars, varargin);
        end

        output = GtConditionalOutput(vars_pars.verbose);

        pID       = obj.phaseIDs(mis1);
        if (obj.phaseIDs(mis2) ~= pID)
            disp('Phase number must be the same between the two compared grains...Quitting')
            return
        end

        output.odisp(' ')
        output.fprintf(' Grains: %4d %4d\n', mis1, mis2)

        Bmat = @(iphase) gtCrystHKL2CartesianMatrix(obj.p_latticepar{iphase});
        % get list of slip systems
        getSlipSystems = @(x) struct2cell( obj.p_slipSystems{x} );

        % cosine between two vectors
        cosine3 = @(x, y) cosd(rad2deg(vectorAngle3d(x, y)));

        slipSystems = getSlipSystems(pID);
        orimat  = obj.orientations(:, :, [mis1 mis2]);

        obj.slipTransfer{mis1, mis2} = struct();

%             lp = obj.p_latticepar{pID};
%             % metric tensor
%             G = [lp(1)^2 lp(1)*lp(2)*cosd(lp(6)) lp(1)*lp(3)*cosd(lp(5)); ...
%                  lp(2)*lp(1)*cosd(lp(6)) lp(2)^2 lp(2)*lp(3)*cosd(lp(4)); ...
%                  lp(3)*lp(1)*cosd(lp(5)) lp(3)*lp(2)*cosd(lp(4)) lp(3)^2];

        for ifam = 1:length(slipSystems)
            syst = slipSystems{ifam};
            for isyst = 1:length(syst)
                if strcmpi(obj.p_crystalsystem{pID}, 'hexagonal')
                    syst(isyst).hkl = gtCrystHKL2Cartesian(syst(isyst).n', Bmat(pID))';
                    syst(isyst).uvw = gtCrystHKL2Cartesian(syst(isyst).l', Bmat(pID))';
                else
                    syst(isyst).hkl = syst(isyst).n/norm(syst(isyst).n);
                    syst(isyst).uvw = syst(isyst).l/norm(syst(isyst).l);
                end
                syst(isyst).num = isyst;
                syst(isyst).fam = obj.p_slipFamilies{pID}{ifam};
            end
            slipSystems{ifam} = syst;
        end

        % computation
        for ifam = 1:length(slipSystems)
            syst1 = slipSystems{ifam};
            % convert slipDir from crystal CS to Lab CS
            slip1 = gtVectorCryst2Lab(vertcat(syst1.uvw), orimat(:, :, 1));
            norm1 = gtVectorCryst2Lab(vertcat(syst1.hkl), orimat(:, :, 1));

            for jfam = 1:length(slipSystems)
                syst2 = slipSystems{jfam};
                % convert slipDir from crystal CS to Lab CS
                slip2 = gtVectorCryst2Lab(vertcat(syst2.uvw), orimat(:, :, 2));
                norm2 = gtVectorCryst2Lab(vertcat(syst2.hkl), orimat(:, :, 2));

                % angle between loading direction and slip plane normal
                cospsi  = arrayfun(@(num) cosine3(norm1, norm2(num, :)), 1:length(syst2), 'UniformOutput', false);
                cospsi  = [cospsi{:}];
                % angle between loading direction and slip direction
                coska   = arrayfun(@(num) cosine3(slip1, slip2(num, :)), 1:length(syst2), 'UniformOutput', false);
                coska = [coska{:}];
                % slip transfer parameter is the product of the two cosines
                slipTr  = cospsi .* coska;

                % take unique
                u_slipTr = unique(abs(single(slipTr)), 'stable');

                % store results
                obj.slipTransfer{mis1, mis2}.slipTr{ifam, jfam} = slipTr;
                obj.slipTransfer{mis1, mis2}.cospsi{ifam, jfam} = cospsi;
                obj.slipTransfer{mis1, mis2}.coska{ifam, jfam}  = coska;

                obj.slipTransfer{mis1, mis2}.u_slipTr{ifam, jfam}   = u_slipTr';
                obj.slipTransfer{mis1, mis2}.max_slipTr{ifam, jfam} = max(abs(u_slipTr));

                obj.slipTransfer{mis1, mis2}.slipSystems{ifam, jfam} = [syst1(1).fam '-' syst2(1).fam];
            end % end jfam loop
        end % for ifam loop

        % look for the maximum value
        if isempty(vars_pars.fam)
            maxSlip = cell2mat(obj.slipTransfer{mis1, mis2}.max_slipTr);
            [rFam, cFam] = find(maxSlip == max(maxSlip(:)), 1);
            vars_pars.fam = rFam;
            nfam = [rFam, cFam];
            maxSlip = maxSlip(vars_pars.fam, :);
        else
            maxSlip = [obj.slipTransfer{mis1, mis2}.max_slipTr{vars_pars.fam, :}];
            nfam = vars_pars.fam;
        end

        output.fprintf('    num: %d\n', vars_pars.fam)
        output.fprintf(' system: %s\n', obj.p_slipFamilies{pID}{vars_pars.fam})
        slipSystems = obj.slipTransfer{mis1, mis2}.slipSystems(vars_pars.fam, :);
        output.fprintf('  modes:\n')
        cellfun(@(x) output.fprintf('         %s\n', x), slipSystems)
        output.odisp('  Slip transfer values for the slip mode with the highest slip transfer:')
        output.odisp(maxSlip)
        if (vars_pars.maxSystem)
            output.fprintf('    --> Maximum slip transfer : %f\n', max(maxSlip(:)))
        end
        output.odisp(' ')

        obj.extras.vars_pars = vars_pars;
        if ~isempty(rej_pars), rej_pars = cell2struct(rej_pars(2:2:end), rej_pars(1:2:end), 2); end
        obj.extras.rej_pars  = rej_pars;
    end

    function schmidF = displaySchmidFNeighborhood(obj, point, varargin)
    % GTTWINANALYSIS/DISPLAYSCHMIDFNEIGHBORHOOD
    % schmidF = displaySchmidFNeighborhood(obj, point, varargin)

        [list, ~, ~] = obj.getIDlist(point, true, 1);
        if isempty(list)
            disp('No point selected... Quitting')
            return
        end
        grainID = list(1);
        IDs = list';

        vars_pars.maxSystem = true;
        vars_pars.fam       = [];
        vars_pars.thr_value = 0.4;
        rej_pars = [];
        if ~isempty(obj.extras.vars_pars)
            [vars_pars, rej_pars] = parse_pv_pairs(vars_pars, struct2pv(obj.extras.vars_pars));
        end

        if isempty(varargin)
            vars_list = get_structure(vars_pars, [], false, [], []);
            vars_list(:, 2) = {'Display the max value among the slip systems', ...
                'Slip family to be considered', ...
                'Threshold value for slip transfer'};
            vars_pars = gtModifyStructure(vars_pars, vars_list, 1, 'Options for calculating the Schmid Factor:');
        else
            [vars_pars, rej_pars] = parse_pv_pairs(vars_pars, varargin);
        end
        varargin = struct2pv(rmfield(vars_pars, 'thr_value'));

        obj.updateSchmidFactors(varargin{:});

        if (vars_pars.maxSystem)
            if isfield(obj.schmidFactors, 'tauMaxgrain') && ~isempty(obj.schmidFactors.tauMaxgrain)
                schmids = obj.schmidFactors.tauMaxgrain;
            else
                schmids = obj.schmidFactors.max_schmids;
            end

            if ~isempty(vars_pars.fam)
                schmids = schmids{vars_pars.fam, :};
                values = schmids(:, list);
                for ii=1:length(IDs)
                    maxSlip{ii} = values(ii);
                    nFam(ii) = vars_pars.fam;
                end
            else
                values = cellfun(@(x) x(:, list), schmids, 'UniformOutput', false);
                for ii=1:length(IDs)
                    uSlip{ii} = cellfun(@(x) abs(x(:, ii)), values, 'UniformOutput', false);
                end
                for ii=1:length(IDs)
                    tmp = uSlip{ii};
                    nSlipFam(ii) = find(cellfun(@(x) ~isempty(find(x == max(vertcat(tmp{:})), 1)), tmp), 1);
                    maxSlip{ii} = [uSlip{ii}{:}];
                end
                nFam = nSlipFam;
            end
        else
            if isfield(obj.schmidFactors, 'taugrain') && ~isempty(obj.schmidFactors.taugrain)
                schmids = obj.schmidFactors.taugrain;
            else
                schmids = obj.schmidFactors.u_schmids;
            end

            if ~isempty(vars_pars.fam)
                schmids = schmids{vars_pars.fam};
                values = schmids(:, list);
                for ii=1:length(IDs)
                    maxSlip{ii} = values(:, ii);
                    nFam(ii) = vars_pars.fam;
                end
            else
                values = cellfun(@(x) x(:, list), schmids, 'UniformOutput', false);
                for ii=1:length(IDs)
                    uSlip{ii} = cellfun(@(x) abs(x(:, ii)), values, 'UniformOutput', false);
                end
                for ii=1:length(IDs)
                    tmp = uSlip{ii};
                    nSlipFam(ii) = find(cellfun(@(x) ~isempty(find(x == max(vertcat(tmp{:})), 1)), tmp), 1);
                    maxSlip(ii) = uSlip{ii}(nSlipFam(ii));
                end
                nFam = nSlipFam;
            end
        end

        disp(' ')
        disp('Schmid Factor')
        fprintf('Neighborhood of Grain : %d\n', grainID)
        if length(maxSlip{1}) == 1
            fprintf('Family: %d\n', nFam(1))
            values = [maxSlip{:}]';
            disp(['   grainID   maxSlip ' sprintf('   >= %0.2f', vars_pars.thr_value)])
            arrayfun(@(x, y, z) fprintf(' %9d %9f %7d\n', x, y, z), IDs, values, (values >= vars_pars.thr_value))
        else
            maxSlip = cellfun(@(x) reshape(x, 1, []), maxSlip, 'UniformOutput', false);
            nLength = cellfun(@(x) length(x), maxSlip);

            output = '';
            fprintf('   grainID    Family')
            if ~isempty(vars_pars.fam)
                for ii = 1:max(nLength(:))
                    fprintf('   slip %2d', ii)
                    output = [output ' %9f'];
                end
            elseif (vars_pars.maxSystem)
                for ii = 1:max(nLength(:))
                    fprintf('    fam %2d', ii)
                    output = [output ' %9f'];
                end
            else
                for ii = 1:max(nLength(:))
                    fprintf(' system %2d', ii)
                    output = [output ' %9f'];
                end
            end
            fprintf('\n')
            for jj = 1:length(IDs)
                values = maxSlip{jj};
                output_current = sprintf(output, values, NaN(1, max(nLength(:)) - nLength(jj)));
                fprintf(' %9d %9d%s\n', IDs(jj), nFam(jj), output_current)
            end
        end

        if nargout > 0
            schmidF.maxSlip = maxSlip;
            schmidF.nFam    = nFam;
        end

        obj.extras.vars_pars = vars_pars;
        if ~isempty(rej_pars), rej_pars = cell2struct(rej_pars(2:2:end), rej_pars(1:2:end), 2); end
        obj.extras.rej_pars  = rej_pars;
    end

    function displayNeighborhood(obj, point, printValues)
    % GTTWINANALYSIS/DISPLAYNEIGHBORHOOD  Displays the neighbors of a grain
    %
    % displayNeighborhood(obj, point, printValues)

        [list, ~, ~] = obj.getIDlist(point, true, 1);
        grainID = list(1);
        pID = obj.phaseIDs(grainID);
        if (~printValues)
            fprintf('Grain neighborhood for grain %d, phase %d: \n', grainID, pID)
            disp(list(2:end)')
        else
            disp(' ')
            disp(['Colormap: ' obj.cmap.defname])
            fprintf('Value for grain %d, phase %d: \n', grainID, pID)
            par_values = obj.cmap.par_values;

            if strcmpi(obj.cmap.defname, 'SchmidF')
                par_values = max(abs(par_values));
            end

            if (size(par_values, 1) == 3)
                par_values = par_values';

                arrayfun(@(x, y, w, z) fprintf(' %4d %9f %9f %9f\n', x, y, w, z), ...
                    grainID, par_values(grainID, 1), par_values(grainID, 2), par_values(grainID, 3))
                fprintf('Values for neighbors for grain %d, phase %d: \n', grainID, pID)
                arrayfun(@(x, y, w, z) fprintf(' %4d %9f %9f %9f\n', x, y, w, z), ...
                    list(2:end)', par_values(list(2:end)', 1), par_values(list(2:end)', 2), par_values(list(2:end)', 3))
            else
                par_values = reshape(par_values, obj.Ngrains, []);

                arrayfun(@(x, y) fprintf(' %4d %9f\n', x, y), ...
                    grainID, par_values(grainID, :))
                fprintf('Values for neighbors for grain %d, phase %d: \n', grainID, pID)
                arrayfun(@(x, y) fprintf(' %4d %9f\n', x, y), ...
                    list(2:end)', par_values(list(2:end)', :))
            end
        end
    end

    function displayShowFsim(obj, point)
    % GTTWINANALYSIS/DISPLAYSHOWFSIM  Displays the forward simulation
    %
    % displayShowFsim(obj, point)

        [list, ~, phaseID] = obj.getIDlist(point, true, 1);
        grainID = list(1);
        obj.displayForwardSim(phaseID, grainID);
    end

    function closeSelectGrains(src, ~, obj, h)
    % GTTWINANALYSIS/CLOSESELECTGRAINS  Close the figure relative to the
    % grain selection and save the IDs in the sample.mat; update the figure
    % title and also save the flag into obj.extras
    %
    % closeSelectGrains(src, eventdata, obj, h)

        current_list = get(h, 'Value');
        current_list = cat(1, current_list{:})';
        activeIDs = find(current_list);
        not_activeIDs = find(~current_list);

        sample = obj.getSample();
        params = obj.getParams();
        phases_num = numel(params.cryst);

        for ii = 1:phases_num
            gIDs = find(obj.phaseIDs == ii);
            obj.p_activeIDs{ii} = gIDs(ismember(gIDs, activeIDs));
            obj.maxGrainID(ii) = max(activeIDs);
            arrayfun(@(x) sample.phases{ii}.setSelected(x, true), activeIDs);
            arrayfun(@(x) sample.phases{ii}.setSelected(x, false), not_activeIDs);
        end

        fprintf('Saving to file..');
        obj.cache.set('sample', {}, sample, 'sample');
        obj.cache.flushObjectField('sample', {}, 'sample');
        sample.saveToFile(fullfile(obj.dataDir, '4_grains', 'sample.mat'));
        fprintf(' Done.\n');

        % obj.conf.clims(2) = max(obj.grainIDs); % necessary ?

        % update displayed grains
        obj.extras.displayedFlag = get(obj.conf.h_select_butt, 'Tag');

        % change figure title
        curr_title = get(obj.conf.h_figure, 'Name');
        str_match = regexp(curr_title, 'activeIDs: \w*', 'match');
        if ~isempty(str_match)
            new_title = strrep(curr_title, str_match{1}, ['activeIDs: ' obj.extras.displayedFlag]);
            set(obj.conf.h_figure, 'Name', new_title);
        else
            set(obj.conf.h_figure, 'Name', [curr_title ' - activeIDs: ' obj.extras.displayedFlag]);
        end
        if src ~= obj.conf.h_figure
            delete(src)
        end
        obj.updateStatus('Run Asemble now ?!')
    end

    function selectGrainsToVisualize(hObj, ~, obj)
    % GTTWINANALYSIS/SELECTGRAINSTOVISUALIZE  Grain selection based aon
    % some criteria : parent which twinned, not twinned grains, only twins,
    % parents and twins, none, all
    %
    % selectGrainsToVisualize(hObj, eventdata, obj)

        % active
        sample = obj.getSample();
        params = obj.getParams();
        phases_num = numel(params.cryst);

        activeIDs = [];
        for ii = 1:phases_num
            activeIDs = cat(1, activeIDs, find(sample.phases{ii}.selectedGrains));
        end

        f = figure('ToolBar', 'none', 'Name', 'Select Grains', 'Units', 'normalized', 'Position', [0.2 0.3 0.4 0.4]);
        set(f, 'UserData', activeIDs);

        p = uipanel('Parent', f);
        pinfo = uiextras.VBox('Parent', p);
        pboxes = uiextras.HBox('Parent', pinfo);
        h_all        = uicontrol('Parent', pboxes, 'Style', 'togglebutton', ...
            'String', 'All', 'Value', false, 'Tag', 'all');
        h_none       = uicontrol('Parent', pboxes, 'Style', 'togglebutton', ...
            'String', 'None', 'Value', false, 'Tag', 'none');
        h_parents    = uicontrol('Parent', pboxes, 'Style', 'togglebutton', ...
            'String', 'Only Parents', 'Value', false, 'Tag', 'parents');
        h_twins      = uicontrol('Parent', pboxes, 'Style', 'togglebutton', ...
            'String', 'Only Twins', 'Value', false, 'Tag', 'twins');
        h_pairs      = uicontrol('Parent', pboxes, 'Style', 'togglebutton', ...
            'String', 'Parents/Twins', 'Value', false, 'Tag', 'parents_twins');
        h_notwinned  = uicontrol('Parent', pboxes, 'Style', 'togglebutton', ...
            'String', 'Not Twinned', 'Value', false, 'Tag', 'notwinned');
        h_notwins    = uicontrol('Parent', pboxes, 'Style', 'togglebutton', ...
            'String', 'No Twins!', 'Value', false, 'Tag', 'notwins');
        h_reset      = uicontrol('Parent', pboxes, 'Style', 'pushbutton', ...
            'String', 'Reset', 'Tag', 'reset');

        g = uiextras.Grid('Parent', pinfo, 'Spacing', 2);
        for ii = 1:max(obj.grainIDs)
            h(ii) = uicontrol('Parent', g, 'Style', 'togglebutton', ...
                'BackgroundColor', 'g' , 'String', sprintf('%d', ii), ...
                'Callback', @(src, evt)GtTwinAnalysis.toggleGrain(src), ...
                'Value', true);
        end

        set(h(setdiff(1:max(obj.grainIDs), activeIDs)), 'Value', false, 'BackgroundColor', 'r');
        set(f, 'CloseRequestFcn', @(src, evt)closeSelectGrains(src, evt, obj, h));

        set(pinfo, 'Sizes', [40 -1])
        set(g, 'RowSizes', [-1 -1 -1 -1 -1 -1 -1 -1 -1 -1]);


        twin_list = [];
        check_list = [];
        fail_list = [];
        merge_list = [];
        for ii = 1:phases_num
            foutput{ii} = GtAssembleVol3D.loadPhaseTwinOutput(obj.dataDir, ii, false);
            twin_list   = cat(1, twin_list, foutput{ii}.twin_list);
            check_list  = cat(1, check_list, foutput{ii}.check_list);
            merge_list  = cat(1, merge_list, foutput{ii}.merge_list);
            fail_list   = cat(1, fail_list, foutput{ii}.fail_list);
        end
        if ~isempty(check_list)
            twin_list   = cat(1, twin_list, check_list);
        end

        % separate parent/twin
        parents = [];
        twins = [];
        nottwinnes = [];
        if ~isempty(twin_list)
            parents = twin_list(:, 2);
            parents = reshape(parents, [1 numel(parents)]);

            twins = twin_list; twins(:, 2) = []; twins = setdiff(twins, 0);
            twins = reshape(twins, [1 numel(twins)]);
            twins( ismember(twins, parents) ) = [];

            notwinned = setdiff(1:max(obj.grainIDs), reshape(twin_list, [1 numel(twin_list)]));
        else
            notwinned = 1:max(obj.grainIDs);
        end

        % Callbacks
        set(h_all,       'Callback', @(src, evt)GtTwinAnalysis.toggleGrainList(src, evt, h, 1:max(obj.grainIDs), hObj, true));
        set(h_none,      'Callback', @(src, evt)GtTwinAnalysis.toggleGrainList(src, evt, h, 1:max(obj.grainIDs), hObj, false));
        set(h_parents,   'Callback', @(src, evt)GtTwinAnalysis.toggleGrainList(src, evt, h, parents, hObj));
        set(h_twins,     'Callback', @(src, evt)GtTwinAnalysis.toggleGrainList(src, evt, h, twins, hObj));
        set(h_pairs,     'Callback', @(src, evt)GtTwinAnalysis.toggleGrainList(src, evt, h, [parents twins], hObj));
        set(h_notwinned, 'Callback', @(src, evt)GtTwinAnalysis.toggleGrainList(src, evt, h, notwinned, hObj));
        set(h_notwins,   'Callback', @(src, evt)GtTwinAnalysis.toggleGrainList(src, evt, h, setdiff(1:max(obj.grainIDs), twins), hObj));
        set(h_reset,     'Callback', @(src, evt)GtTwinAnalysis.toggleGrainList(src, evt, h, activeIDs, hObj));
    end

    function displayPoleFigure(obj, varargin)
    % GTTWINANALYSIS/DISPLAYPOLEFIGURE  Displays the pole figure for the
    % selected grains and changes the colormap to Caxis color map
    %
    % displayPoleFigure(obj, varargin)

        obj.setVolumeToLoad('phase', 'reload', false);

        % if app.poles is empty, run gtCrystAxisPoles
        vars_pars.phaseid     = obj.selectedPhase;
        vars_pars.crystDir    = obj.crystDir;
        vars_pars.axesRot     = [1 2 3]; % trasform axes
        vars_pars.background  = obj.cmap.hasBkg;
        vars_pars.convention  = 'X';
        % read in the specs
        vars_pars.range       = 'phi360'; % 'phi360' | 'phi90' | 'hex' | 'sst'
        vars_pars.N           = 100;
        vars_pars.searchR     = 0.1;
        % drawing
        vars_pars.colormap    = unique(vertcat(obj.cmap.name, GtColormap.getMatlabCmaps), 'stable');    %
        vars_pars.surf        = false;    % true | false
        vars_pars.scatter     = false;    % true | false
        vars_pars.rings       = true;     % true | false
        vars_pars.maxring     = 90;       % degrees for psi angle
        vars_pars.levels      = 10;       % number of levels for contour and surf
        vars_pars.clims       = [];       % colour limits

        vars_pars.label_poles = 'none';   % 'all' | 'main' | 'none' | 'bcc' ( only for cubic )
        vars_pars.mark_poles  = 'main';   % 'all' | 'main' | 'none' | 'bcc' ( only for cubic )
        vars_pars.linecolor   = [1 1 1];  % color for rings, labels
        vars_pars.fontsize    = 10;       % text size

        vars_pars.colorbar    = true;     % true | false
        vars_pars.logscale    = false;    % true | false

        vars_pars.export      = false;    % true | false
        vars_pars.paper_fig   = false;
        vars_pars.ws_path     = true;
        vars_pars.dotFig      = false;

        vars_list = get_structure(vars_pars, [], false, [], []);
        ind = findValueIntoCell(fieldnames(vars_pars), {'paper_fig', 'ws_path', 'dotFig'});
        vars_list(ind(:, 1), 4) = {2};

        vars_pars = gtModifyStructure(vars_pars, vars_list, 1, 'Options for calculating the Pole Figure:');
        if (vars_pars.export)
            vars_pars = gtModifyStructure(vars_pars, vars_list, 2, 'Options for exporting the figure:');
        end

        vars_pars.crystal_system = obj.p_crystalsystem{obj.selectedPhase};
        vars_pars.latticepar     = obj.p_latticepar{obj.selectedPhase};
        vars_pars.r_vectors      = obj.r_vectors.';
        vars_pars.title       = '';
        vars_pars.cmap        = obj.cmap.colormap;       % for scatter plots
        vars_pars.grainids    = obj.cmap.activeIDs;       % for scatter plots
        vars_pars.tag         = 'h_pf_fig';       % tag for axes and cb
        vars_pars.figname     = 'Pole Figure calculation';
        vars_pars.plot_figure = true;     % true | false
        vars_pars.save        = false;
        vars_pars.debug       = false;    % true | false
        vars_pars.contourline = 'none';   % can be RGB color or 'none'

        varargin = struct2pv(vars_pars);
        h_pf_fig = gtMakePoleFigure(varargin{:});

        vargs_out = getappdata(h_pf_fig, 'AppData');
        obj.conf.h_pf_fig = h_pf_fig;

        fprintf('  Crystallographic direction  : [%s]\n', strrep(num2str(vargs_out.crystDir), '  ', ' '))
        obj.updatePhaseInfo(vargs_out.crystDir, 'crystDir', vargs_out.phaseid);
        fprintf('    Cryst axis color map      : done!\n')
        obj.grainCaxisColorMap = vargs_out.grainCaxisColorMap;
        obj.changeGrainCmap('Caxis', false)
    end

    function hf = displayInversePoleFigure(obj, varargin)
    % GTTWINANALYSIS/DISPLAYINVERSEPOLEFIGURE  Displays the Inverse Pole
    % Figure for the selected grains and changes the colormap to the IPF
    % color map
    %
    % hf = displayInversePoleFigure(obj, varargin)

        obj.setVolumeToLoad('phase', 'reload', false);

        vars_pars.phaseid    = max(1, obj.selectedPhase);
        vars_pars.sampleDir  = obj.sampleDir;
        vars_pars.poles      = [];
        vars_pars.surface    = false;    % true | false
        vars_pars.N          = 100;
        vars_pars.searchR    = 0.15;
        vars_pars.levels     = 10;       % number of levels for contour and surf
        vars_pars.colorbar   = true;     % true | false
        suppCmaps = unique(vertcat(obj.cmap.name, obj.cmap.suppCmaps), 'stable');
        vars_pars.colormap    = suppCmaps;
        vars_pars.grainids    = [];

        vars_pars.fontsize    = 10;       % text size
        vars_pars.markersize  = 10;       % marker size for scatter plot
        vars_pars.markertype  = {'d', '.', 's', 'o', 'h', 'p', 'x', '*', '^', 'v', '>', '<', '+'};
        vars_pars.label       = obj.extras.displayedFlag;
        vars_pars.cmap        = [];
        vars_pars.linecolor   = [1 1 1];  % color for rings, labels
        vars_pars.figcolor    = [0 0 0];  % figure color

        vars_pars.hf          = [];
        vars_pars.ha          = [];

        vars_pars.select_ids  = false;
        vars_pars.activeIDs   = true;

        vars_pars.export      = false;    % true | false
        vars_pars.paper_fig   = true;
        vars_pars.ws_path     = true;
        vars_pars.dotFig      = false;

        vars_pars.loadIPFcmap = false;

        if isempty(varargin)

            vars_list = get_structure(vars_pars, [], false, [], []);
            ind = findValueIntoCell(fieldnames(vars_pars), {'N', 'searchR', 'colorbar', 'colormap', 'levels'});
            vars_list(ind(:, 1), 4) = {2};
            ind = findValueIntoCell(fieldnames(vars_pars), {'paper_fig', 'ws_path', 'dotFig'});
            vars_list(ind(:, 1), 4) = {3};

            vars_pars = gtModifyStructure(vars_pars, vars_list, 1, 'Options for calculating the Inverse Pole Figure:');
            if (vars_pars.surface)
                vars_pars = gtModifyStructure(vars_pars, vars_list, 2, 'Options for density plot (surface/contour):');
            end
            if (vars_pars.export)
                vars_pars = gtModifyStructure(vars_pars, vars_list, 3, 'Options for exporting the figure:');
            end

            if ~isempty(vars_pars.label) && isempty(vars_pars.cmap)
                switch (vars_pars.label)
                    case 'parents'
                        vars_pars.cmap = [0 1 0];
                    case 'twins'
                        vars_pars.cmap = [1 0 0];
                    case 'parents_twins'
                        vars_pars.cmap = [1 0 1];
                    case 'all'
                        vars_pars.cmap = [1 1 1];
                    case 'notwinned'
                        vars_pars.cmap = [1 1 0];
                end
            end

            if (~vars_pars.activeIDs)
                if (vars_pars.select_ids)
                    prompt = {'Enter parent/twin(s) ID(s) space-separated:'};
                    dlg_title = 'Input';
                    num_lines = [1 50];
                    def = {''};
                    x = inputdlg(prompt, dlg_title, num_lines, def);
                    if ~isempty(x)
                        ids = str2num(x{:});
                    else
                        disp('No input given: all the grains are taken')
                        ids = [];
                    end
                else
                    ids = [];
                end
            else
                ids = cat(2, obj.p_activeIDs{:});
            end
            if isempty(vars_pars.grainids)
                vars_pars.grainids    = ids;
            end

            vars_pars.label_poles = 'main';   % 'all' | 'main' | 'none' | 'bcc' ( only for cubic )
            vars_pars.mark_poles  = 'main';   % 'all' | 'main' | 'none' | 'bcc' ( only for cubic )
            vars_pars.logscale    = false;    % true | false
            vars_pars.r_vectors   = obj.r_vectors.';
            vars_pars.view        = [0 90];   % view of the XY plane RD right and TD top -> 0 90;
            vars_pars.tag         = 'h_ipf_fig';       % tag for axes and cb
            vars_pars.figname     = 'Inverse Pole Figure calculation';
            vars_pars.plot_figure = true;     % true | false
            vars_pars.debug       = false;    % true | false
            vars_pars.contourline = 'none';   % can be RGB color or 'none'
            if ~isempty(vars_pars.hf) && isempty(vars_pars.ha)
                vars_pars.ha          = get(vars_pars.hf, 'CurrentAxes');
            end
            varargin = struct2pv(rmfield(vars_pars, {'select_ids', 'activeIDs', 'loadIPFcmap'}));

        else
            [vars_pars, rej_pars] = parse_pv_pairs(vars_pars, varargin);
            if isempty(vars_pars.label)
                vars_pars.label = 'all';
            end
            if ~isempty(vars_pars.label) && isempty(vars_pars.cmap)
                switch (vars_pars.label)
                    case 'parents'
                        vars_pars.cmap = [0 1 0];
                    case 'twins'
                        vars_pars.cmap = [1 0 0];
                    case 'parents_twins'
                        vars_pars.cmap = [1 0 1];
                    case 'all'
                        vars_pars.cmap = [1 1 1];
                    case 'notwinned'
                        vars_pars.cmap = [1 1 0];
                end
            end
        end

        h_ipf_fig = gtMakeInversePoleFigure(varargin{:});

        obj.conf.h_ipf_fig = h_ipf_fig;

        if (vars_pars.loadIPFcmap && ~vars_pars.figure_grid && ~vars_pars.surface)
            vargs_out = getappdata(h_ipf_fig, 'AppData');

            fprintf('  Sample direction  : [%s]\n', strrep(num2str(vargs_out.sampleDir), '  ', ' '))
            obj.updatePhaseInfo(vargs_out.sampleDir, 'sampleDir', vargs_out.phaseid);
            fprintf('    IPF color map      : done!\n')
            if isempty(vars_pars.cmap)
                obj.grainIPFColorMap = vargs_out.grainIPFColorMap;
            end
            obj.changeGrainCmap('IPF', false)
        end

        % add menu item
        if isempty(vars_pars.hf) || isempty(findobj(h_ipf_fig, 'type', 'uimenu'))
            hm = uimenu(h_ipf_fig, 'Label', 'Inverse Pole Figure', 'ForegroundColor', 'red');
            uimenu(hm, 'Label', 'Save Figure', ...
                'Callback', @(src, evt)saveCmapFigure(obj, h_ipf_fig, 'rendering', false, 'ipf', true, 'surface', vars_pars.surface))
        end

        if nargout > 0
            hf = h_ipf_fig;
        end
    end

end % end of public methods

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% PRIVATE METHODS
methods(Access = private)

    function getGrainVars(obj, igrain)
    % GTTWINANALYSIS/GETGRAINVARS  Get variables for model 'grain'
    % if Id > Ngrains{iphase}, it is calculated from
    % Id - Ngrains{iphase} = newId
    %
    % getGrainVars(obj, igrain)

        params = obj.getParams();
        phases_num = numel(params.cryst);

        id = [];
        ip = [];
        for ii = 1:phases_num
            if ismember(igrain, obj.p_grainIDs{ii})
                if igrain > max(obj.p_grainIDs{ii})
                    id = igrain - max(obj.p_grainIDs{ii});
                else
                    id = igrain;
                end
                ip = ii;
            end
        end
        if isempty(ip) || isempty(id)
            disp(['Wrong grain ID "' num2str(igrain) '"...Quitting'])
            return
        end
        % included = ondet(flag>=included)
        obj.g_difspots{igrain} = obj.cache.get('grain', {ip, id}, 'difspotID');
        % all fsim > ondet
        obj.g_allblobs{igrain} = obj.cache.get('grain', {ip, id}, 'allblobs');
        % ondet
        obj.g_flags{igrain}    = obj.cache.get('grain', {ip, id}, 'flag');

        % keep ondet reflections ONLY
        proj = obj.cache.get('grain', {ip, id}, 'proj');
        obj.setAllblobsFieldIndexes(id, proj.ondet);
    end

    function setAllblobsFieldIndexes(obj, igrain, indexes)
    % GTTWINANALYSIS/SETALLBLOBSFIELDINDEXES  Updates 'allblobs' structure
    % fields taking only some indexes for igrain
    %
    % setAllblobsFieldIndexes(obj, igrain, indexes)

        % keep indexes for each field of allblobs
        allblobs = obj.g_allblobs{igrain};
        size_1 = struct2array(structfun(@(x) size(x, 1), allblobs, 'UniformOutput', false));
        size_3 = struct2array(structfun(@(x) size(x, 3), allblobs, 'UniformOutput', false));
        % sort and unique size values
        u_size1 = unique(size_1);
        u_size3 = unique(size_3);
        [varfreq, vartype]=histc(size_1, u_size1);
        fields = fieldnames(allblobs);
        tmp2 = struct();
        for ii=1:length(varfreq)
            tmp = cell2struct(cellfun(@(x) allblobs.(x), fields(vartype==ii), 'UniformOutput', false), fields(vartype==ii), 1);
            if ii>1 && mod(u_size1(ii), u_size1(ii-1))==0 && ii~=length(varfreq)
                sizev = (u_size1(ii)/u_size1(ii-1));
                newind = [];
                for jj = 1:length(indexes)
                    newind = [newind, (indexes(jj)-1)*sizev+1 : indexes(jj)*sizev];
                end
                tmp2 = catstruct(tmp2, structfun(@(x) vertcat(x(newind, :)), tmp, 'UniformOutput', false));
            else
                if all(size_3(vartype==ii) == 1)
                    tmp2 = catstruct(tmp2, structfun(@(x) x(indexes, :), tmp, 'UniformOutput', false));
                elseif all(size_3(vartype==ii) > 1)
                    tmp2 = catstruct(tmp2, structfun(@(x) x(:, :, indexes), tmp, 'UniformOutput', false));
                end
            end
        end
        obj.g_allblobs{igrain} = tmp2;
    end

    function indexes = getIndexesFromFlag(obj, flag)
    % GTTWINANALYSIS/GETINDEXESFROMFLAG  Uses fsim flag to keep only some
    % indexes over the reflection indexes on the detector
    %
    % indexes = getIndexesFromFlag(obj, flag)

        indexes = cell(1, obj.Ngrains);
        for gID = 1:obj.Ngrains
            indexes{gID} = find(obj.g_flags{gID} >= flag);
        end
    end

    function setGrainFieldIndexes(obj, variable, indexes)
    % GTTWINANALYSIS/SETGRAINFIELDINDEXES  Keeps 'indexes' for the
    % given property 'variable'
    %
    % setGrainFieldIndexes(obj, variable, indexes)

        if ~isempty(indexes) && isprop(obj, variable)
            var = obj.(variable);
            if strcmpi(variable, 'g_mosaicity')
                var(1:2, :)=[];
            end
            for gID = 1:obj.Ngrains
                for irow = 1:size(var, 1)
                    var{irow, gID} = var{irow, gID}(indexes{gID});
                end
            end
            if strcmpi(variable, 'g_mosaicity')
                tmp =  obj.(variable);
                tmp(3, :) = var;
                var = tmp;
            end
            obj.(variable) = var;
        end
    end

    function map = makeColorMap(obj, varargin)
    % GTTWINANALYSIS/MAKECOLORMAP  Compute color map given a list of values
    %
    % map = makeColorMap(obj, varargin)

        par.maxSystem  = obj.cmap.maxSystem; % take max absolute value for each column
        par.iphase     = obj.selectedPhase;
        par.values     = []; % row vector for each grain; multiple rows is possible
        par.maxV       = [];
        par.nsteps     = obj.cmap.nsteps;
        par.name       = obj.cmap.name;
        par.bkg        = obj.cmap.hasBkg;
        par.conflict   = obj.cmap.hasConflict;
        par.scale      = obj.cmap.scale; % scaling from 0 to 1 instead of having X to 1
        [par, rej_pars] = parse_pv_pairs(par, varargin);

        if isempty(par.name)
            par.name = 'cool';
        elseif ischar(par.name)
            par.name = validatestring(par.name, ...
                obj.cmap.suppCmaps, 'makeColorMap@GtTwinAnalysis', 'name', 1);
        else
            gtError('GtTwinAnalysis:makeColorMap:wrong_input_type', 'Wrong type for colormap')
        end
        if (par.iphase == 0)
            par.iphase = 1;
        end
        if ~isempty(par.values)
            values = par.values;
            values = reshape(values, [], obj.Ngrains);
        else
            gtError('GtTwinAnalysis:makeColorMap:missing_argument', 'Missing values for building the colormap')
        end

        values = abs(values);
        par.values = values;

        if (par.maxSystem && size(values, 1) > 1 && size(values, 2) == obj.Ngrains)
            % take unique of abs along rows
            values = unique(abs(single(values)), 'rows');
            hasMax = ismember(abs(values), max(abs(values)));
            values = values( hasMax )';
        end

        % scale from 0 to 1
        if ~isempty(par.maxV) && (par.maxV ~= 0)
            maxV = par.maxV;
            if (par.scale)
                values = abs(values) - min(abs(values(:)));
                maxV   = max(abs(values(:)));
                values = values / maxV;
            else
                values = abs(values) / maxV;
            end
        else
            maxV = max(abs(values(:)));
            values = abs(values) / maxV;
        end

        if ismember(par.name, [obj.cmap.data_cmaps(:, 1); obj.cmap.autogen_cmaps])
            par.cmapdef = GtColormap.setNewCmap(obj.Ngrains, par.name, par.nsteps+1);
        elseif ismember(par.name, obj.cmap.matlab_cmaps)
            par.cmapdef = GtColormap.getCmapDef(par.name, par.nsteps);
        else
            par.name = 'cool';
            par.cmapdef = GtColormap.getCmapDef(par.name, par.nsteps);
        end
        % rgb = @(x) par.cmapdef(round(abs(x)*par.nsteps+1), :);
        % loop over rows
        map = zeros(size(values, 2)+1, 3, size(values, 1));
        for jj = 1:size(values, 1)
            map(:, :, jj) = [obj.cmap.bkg; GtColormap.getRGBValues(values(jj, :), par.cmapdef, par.nsteps)];
        end
        if (~par.bkg)
            map(1, :, :) = [];
        end

        if isfield(obj.conf, 'h_cmap') && isa(obj.conf.h_cmap, 'GtColormap')
            if ishandle(obj.conf.h_cmap.h_fig)
                obj.conf.h_cmap.changeParams('name', par.name, ...
                    'hasBkg', par.bkg, 'hasConflict', par.conflict, ...
                    'maxV', maxV, 'nsteps', par.nsteps, ...
                    'maxSystem', par.maxSystem, 'scale', par.scale, ...
                    'values', values, 'par_values', par.values, 'cbar', par.cmapdef, 'cmapdef', par.cmapdef);
                obj.cmap = obj.conf.h_cmap.exportProperties();
            else
                obj.conf = rmfield(obj.conf, 'h_cmap');
                obj.cmap.h_fig       = [];
                obj.cmap.name        = par.name;
                obj.cmap.hasBkg      = par.bkg;
                obj.cmap.hasConflict = par.conflict;
                obj.cmap.maxV        = maxV;
                obj.cmap.nsteps      = par.nsteps;
                obj.cmap.values      = values;
                obj.cmap.par_values  = par.values;
                obj.cmap.cmapdef     = par.cmapdef;
                obj.cmap.cbar        = par.cmapdef;
                obj.cmap.scale       = par.scale;
                obj.cmap.maxSystem   = par.maxSystem;
            end
        else
            obj.cmap.h_fig       = [];
            obj.cmap.name        = par.name;
            obj.cmap.hasBkg      = par.bkg;
            obj.cmap.hasConflict = par.conflict;
            obj.cmap.maxV        = maxV;
            obj.cmap.nsteps      = par.nsteps;
            obj.cmap.values      = values;
            obj.cmap.par_values  = par.values;
            obj.cmap.cmapdef     = par.cmapdef;
            obj.cmap.cbar        = par.cmapdef;
            obj.cmap.scale       = par.scale;
            obj.cmap.maxSystem   = par.maxSystem;
        end
    end

end

methods(Static, Access = public)

    function toggleGrainList(src, ~, h, list, hObj, val)
    % GTTWINANALYSIS/TOGGLEGRAINLIST  Select only a list of grain
    %
    % toggleGrainList(src, eventdata, h, list, hObj, val)

        current_list = get(h, 'Value');
        current_list = cat(1, current_list{:})';
        current_list = find(current_list);

        if ~isempty(list)
            if ~exist('val', 'var') || isempty(val)
                for ii=1:length(h)
                    set(h(ii), 'BackgroundColor', 'r');
                    set(h(ii), 'Value', get(h(ii), 'Min'))
                end

                for ii=1:length(list)
                    set(h(list(ii)), 'BackgroundColor', 'g')
                    set(h(list(ii)), 'Value', get(h(list(ii)), 'Max'))
                end
            else
                for ii=1:length(list)
                    set(h(list(ii)), 'Value', val)
                    if val == get(h(list(ii)), 'Max')
                        set(h(list(ii)), 'BackgroundColor', 'g')
                    else
                        set(h(list(ii)), 'BackgroundColor', 'r')
                    end
                end
            end
        end
        set(hObj, 'Tag', get(src, 'Tag'));
        set(src, 'UserData', current_list);
        set(src, 'Callback', @(src, evt)GtTwinAnalysis.toggleGrainList(src, evt, h, current_list, hObj));
    end

    function toggleGrain(src, ~)
    % GTTWINANALYSIS/TOGGLEGRAIN Toggles the grain status
    %
    % toggleGrain(src, eventdata)

        isSelected = (get(src, 'Value') == get(src, 'Max'));
        if (isSelected)
            set(src, 'BackgroundColor', 'g');
            set(src, 'Value', get(src, 'Max'))
        else
            set(src, 'BackgroundColor', 'r');
            set(src, 'Value', get(src, 'Min'))
        end
    end

end

end % end of class
