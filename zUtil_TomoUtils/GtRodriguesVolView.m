classdef GtRodriguesVolView < GtVolView
    properties
        r_vecs = [];

        mask = [];
        threshold = 0;

        p = gtLoadParameters();
        phase_id = 1;
        symm;
    end

    methods (Access = public)
        function self = GtRodriguesVolView(r_vecs_in, intensities, varargin)
            self = self@GtVolView(intensities, varargin{:});
            [self, ~] = parse_pv_pairs(self, varargin);

            % Should be in initGui, bu some parameters will not be ready by
            % then
            self.conf.h_overlay = [];

            cryst_system = self.p.cryst(self.phase_id).crystal_system;
            cryst_spacegroup = self.p.cryst(self.phase_id).spacegroup;
            self.symm = gtCrystGetSymmetryOperators(cryst_system, cryst_spacegroup);

            if (isempty(self.mask))
                self.mask = intensities > self.threshold;
            end
            self.r_vecs = r_vecs_in;
        end
    end

    methods (Access = protected)
        function initGui(self)
            self.initGui@GtVolView();

            self.conf.h_overlay = [];
        end

        function slice = loadSlice(self, vol)
        % GTVOLVIEW/LOADSLICE Loads a slice from the volume (given the plane and
        % slice index in obj.conf.*) and permutes it to be visualised correctly
        % by matlab, which always flips X and Y dimensions in visualisation.

            if (isempty(self.mask))
                slice(:, :, 4) = self.loadSlice@GtVolView(zeros(size(vol)));
            else
                slice(:, :, 4) = double(self.loadSlice@GtVolView(self.mask));
            end

            if (~isempty(self.r_vecs))
                slice(:, :, 1) = self.loadSlice@GtVolView(self.r_vecs(:, :, :, 1));
                slice(:, :, 2) = self.loadSlice@GtVolView(self.r_vecs(:, :, :, 2));
                slice(:, :, 3) = self.loadSlice@GtVolView(self.r_vecs(:, :, :, 3));
            end
        end

        function setVisualisedData(self, image, vol, is_overlay)
        % GTVOLVIEW/SETVISUALISEDDATA Loads a slice and then operates
        % transformations on them, to adapt to the requested visualization needs

            slice = self.loadSlice(vol);

            if (~is_overlay)
                avg_R_vecs = slice(:, :, 1:3);
                mask_slice =  slice(:, :, 4);
                indx = find(mask_slice(:));
                avg_R_vecs = permute(avg_R_vecs, [3 1 2]);
                avg_R_vecs = reshape(avg_R_vecs, 3, [])';
                avg_R_vecs = avg_R_vecs(indx, :);

                if (~isempty(avg_R_vecs))
                    [cmap, ~, ~] = gtIPFCmap(self.phase_id, [], ...
                        'r_vectors', avg_R_vecs, ...
                        'crystal_system', self.p.cryst(self.phase_id).crystal_system, ...
                        'symm', self.symm);
                    cmap = cmap(2:end, :);

                    img_slice_R = zeros(size(mask_slice));
                    img_slice_G = zeros(size(mask_slice));
                    img_slice_B = zeros(size(mask_slice));

                    img_slice_R(indx) = cmap(:, 1);
                    img_slice_G(indx) = cmap(:, 2);
                    img_slice_B(indx) = cmap(:, 3);

                    set(image, 'CData', cat(3, img_slice_R, img_slice_G, img_slice_B));
                end
            end
        end
    end
end