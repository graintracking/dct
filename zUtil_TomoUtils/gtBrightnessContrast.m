function h_bc = gtBrightnessContrast(h_axis, clims)
% GTBRIGHTNESSCONTRAST  adds brightness and contrast sliders to the figure 
%                       containing the specified axis.
%                       Substitute of pfBrightnessContrast
%     h_bc = gtBrightnessContrast(h_axis, [clims])
%     --------------------------------------------
%     INPUT:
%       h_axis = <handle>  handle of the given axis 
%
%     OPTIONAL INPUT:
%       clims  = [min max] Set the colour limits for the sliders
%                          by default, it takes the ones of the given axis
%
%     OUTPUT:
%       h_bc   = <handles> Sliders and text label handles
%
%[sub]- sfUpdateBC 
%
%     Version 001 24-02-2012 by NVigano, nicola.vigano@esrf.fr
%       Imported and modifed from pfBrightnessContrast.m (id19)

    if (~exist('clims', 'var'))
        clims = get(h_axis, 'clim');
    end

    h_figure = gtGetParentFigure(h_axis);

    % Save zoom state, and then turn it of!
    zoomOp = zoom(h_figure);
    zoomState = get(zoomOp, 'Enable');
    set(zoomOp, 'Enable', 'off');

    % Adding controls
    h_min = uicontrol('Style', 'slider', 'Min', clims(1), 'Max', clims(2), ...
                      'Value', clims(1));
    h_max = uicontrol('Style', 'slider', 'Min', clims(1), 'Max', clims(2), ...
                      'Value', clims(2));

    h_minmaxlabel = uicontrol('Style', 'text');

    set(h_min, 'Units', 'normalized', 'Position', [0.03 0.2 0.025 0.75]);
    set(h_max, 'Units', 'normalized', 'Position', [0.07 0.2 0.025 0.75]);

    set(h_min, 'Callback', @(src, evt)sfUpdateBC(h_axis, h_min, h_max, h_minmaxlabel));
    set(h_max, 'Callback', @(src, evt)sfUpdateBC(h_axis, h_min, h_max, h_minmaxlabel));

    set(h_minmaxlabel, 'Units', 'normalized', 'Position', [0.01 0.11 0.10 0.07]);

    % Update the label
    sfUpdateBC(h_axis, h_min, h_max, h_minmaxlabel);

    % Reset back the zoom to the previous state (before the inclusion)
    set(zoomOp, 'Enable', zoomState);

    if nargout == 1
        h_bc = [h_min h_max h_minmaxlabel];
    end

    function sfUpdateBC(h_axis, h_min, h_max, h_minmaxlabel)
    % GTBRIGHTNESSCONTRAST/UPDATEBC Callback function to update the contrast

        newClims = [get(h_min, 'Value'), get(h_max, 'Value')];

        if (newClims(1) >= newClims(2))
            if (gco == app.h_max)
                set(h_max, 'Value', newClims(1));
            else
                set(h_min, 'Value', newClims(2));
            end
            newClims(2) = newClims(1) + 1;
        end

        set(h_axis, 'clim', newClims)
        set(h_minmaxlabel, 'String', sprintf('%.6g\n%.6g', newClims(2), newClims(1)));

        drawnow();
    end

end % end of function
