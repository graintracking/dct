classdef GtSlideShow < GtVolView
    properties
    end
    
    methods (Access = public)
        function obj = GtSlideShow(filePattern, varargin)
        % GTSLIDESHOW/GTSLIDESHOW Constructor
            files = dir(filePattern);
            if (isempty(files))
                error('GtSlideShow:wrong_argument', ...
                      'the given file pattern: "%s", didn''t produce any result', ...
                      filePattern)
            end

            params = {'CMap', gray(2^16), 'CLims', [0 2^16], ...
                      'filesRecord', files, 'filesDir', fileparts(filePattern)};
            params = [params  varargin{:}];
            params(end+1:end+2) = {'f_title', 'GtSlideShow'};

            vol = ones(2, 2, length(files));
            vol(1:2, 1, 1) = 0;

            obj = obj@GtVolView(vol, params{:});

            % We need to get the parameters seen - It's a dirty trick but it
            % works
            obj.initParams(params);
            obj.setConfigInvariants();
            obj.resetUiComponents();
            obj.updateDisplay();
        end
    end

    methods (Access = protected)
        function initParams(obj, arguments)
            obj.conf.filesRecord = [];
            obj.conf.filesDir = '';
            initParams@GtVolView(obj, arguments)
        end

        function initGui(obj)
        % GTGRAINSMANAGER/INITGUI Inherited method that builds the interface

            initGui@GtVolView(obj)
            % Let's disable zoom, while configuring
            zoomOp = zoom(obj.conf.h_figure);
            zoomState = get(zoomOp, 'Enable');
            zoom(obj.conf.h_figure, 'off');

            obj.conf.main_s_boxes = uiextras.VBox('Parent', obj.conf.currentParent);
            obj.conf.upper_s_boxes = uiextras.HBox('Parent', obj.conf.main_s_boxes);

            obj.conf.h_viewer_panel = uipanel('BorderWidth', 4, ...
                                              'BorderType', 'line', ...
                                              'Parent', obj.conf.main_s_boxes);
            set(obj.conf.main_boxes, 'Parent', obj.conf.h_viewer_panel);
            set(obj.conf.main_s_boxes, 'Sizes', [25 -1]);

            % Setting up image selection bar
            obj.conf.upper_s_bar(1) = uicontrol('Parent', obj.conf.upper_s_boxes, ...
                'Style', 'text', 'HorizontalAlignment', 'left');
            obj.conf.upper_s_bar(2) = uicontrol('Parent', obj.conf.upper_s_boxes, ...
                'Style', 'text', 'String', 'Index:', 'HorizontalAlignment', 'left');
            obj.conf.upper_s_bar(3) = uicontrol('Parent', obj.conf.upper_s_boxes, ...
                'Style', 'edit', 'HorizontalAlignment', 'right', ...
                'Callback', @(src, evt)onChangeEdit(obj));
            obj.conf.upper_s_bar(4) = uicontrol('Parent', obj.conf.upper_s_boxes, ...
                'Style', 'text', 'HorizontalAlignment', 'left', ...
                'String', sprintf('/%d', length(obj.conf.filesRecord)));
            obj.conf.upper_s_bar(5) = uicontrol('Parent', obj.conf.upper_s_boxes, ...
                'Style', 'pushbutton', 'String', 'List', ...
                'Callback', @(src, evt)onSelectButton(obj));
            set(obj.conf.upper_s_boxes, 'Sizes', [-1 45 40 50 60]);

            cbk = @(x, y)getImageInfo(obj, x, y);
            obj.conf.h_pixelinfo.changePatternAndCbk([], cbk);

            % Reset back the zoom to the previous state (before the inclusion)
            set(zoomOp, 'Enable', zoomState);
        end

        function slice = loadSlice(obj, vol)
            if (~isempty(obj.conf.filesRecord))
                file_name = obj.conf.filesRecord(obj.conf.slicendx).name;
                file_path = fullfile(obj.conf.filesDir, file_name);

                set(obj.conf.upper_s_bar(1), 'String', sprintf('Image: %s', file_path));
                set(obj.conf.upper_s_bar(3), 'String', sprintf('%d', obj.conf.slicendx));
                drawnow;

                [slice, obj.conf.volInfo] = GtVolView.loadVolume(file_path);

                [dim(1), dim(2)] = size(slice);
                obj.conf.dim = [dim, length(obj.conf.filesRecord)];
                obj.conf.hordisplay = [1 dim(obj.conf.horDim)];
                obj.conf.verdisplay = [1 dim(obj.conf.verDim)];
                if (obj.conf.verDim > obj.conf.horDim)
                    slice = permute(slice, [2, 1]);
                end

                obj.conf.clims = gtSetColourLimits(slice, 0, true);

                minVal = get(obj.conf.h_min, 'Value');
                maxVal = get(obj.conf.h_max, 'Value');
                if (minVal < obj.conf.clims(1))
                    minVal = obj.conf.clims(1);
                elseif (minVal > obj.conf.clims(2))
                    minVal = obj.conf.clims(2);
                end
                if (maxVal < obj.conf.clims(1))
                    maxVal = obj.conf.clims(1);
                elseif (maxVal > obj.conf.clims(2))
                    maxVal = obj.conf.clims(2);
                end

                set(obj.conf.h_min, 'Min', obj.conf.clims(1), 'Max', obj.conf.clims(2), 'Value', minVal);
                set(obj.conf.h_max, 'Min', obj.conf.clims(1), 'Max', obj.conf.clims(2), 'Value', maxVal);

                pixelInfoPattern = sprintf('(%%d, %%d, %s) Val: %%f', file_name);
                obj.conf.h_pixelinfo.changePatternAndCbk(pixelInfoPattern, []);
            else
                slice = loadSlice@GtVolView(obj, vol);
            end
        end
    end

    methods (Access = public)
        function onSelectButton(obj)
            names = {obj.conf.filesRecord.name};
            names = cellfun(@(x){fullfile(obj.conf.filesDir, x)}, names);

            selector = GtGuiSelectInList(names);
            index = selector.join();

            if (~isempty(index))
                obj.changeSlice(index);
            end
        end

        function onChangeEdit(obj)
            value = get(obj.conf.upper_s_bar(3), 'String');
            try
                value = str2double(value);
                if (value > 0 && value <= length(obj.conf.filesRecord))
                    obj.changeSlice(value);
                else
                    error('GUI:GtSlideShow:wrong_argument', ...
                            'Index %d out of bounds', value);
                end
            catch mexc
                gtPrintException(mexc)
                d = errordlg(['Error: ' mexc.message], mexc.identifier, 'modal');
                uiwait(d);
            end
        end

        function [point, value] = getImageInfo(obj, x, y)
            point = round([x, y]);
            data = get(obj.conf.h_im, 'CData');
            value = data(point(2), point(1));
        end
    end
end
