function cmap = gtSetColourMap(vol, cmap, verbose)
% GTSETCOLOURMAP  Handles the colour map.
%     -------------------------------------------------------------------------
%     cmap = gtSetColourMap(vol, cmap, verbose)
%
%     INPUT:
%       vol     = Input volume, for which the colour map needs to be created
%       cmap    = Type of Colour Map, which can be one of the following:
%                 - 'gray': The default, gray scale colour map.
%                 - 'gray(num)': Gray scale colour map, with 'num' levels.
%                 - 'random': A random colour map.
%                 - 'random(num)': A random colour map, with 'num' levels.
%
%     OPTIONAL INPUT:
%       verbose = Boolean that tells if we want messages to be printed
%
%     OUTPUT:
%       cmap    = Newly created colour map
%
%     Version 001 16-02-2012 by NVigano,  nicola.vigano@esrf.fr

    pattern_gray = '^gray\((?<level>\d*?)\)$';
    pattern_rand = '^random\((?<level>\d*?)\)$';
    pattern_cons = '^constant\((?<level>\d*?)\)$';

    if ( ~exist('verbose', 'var') )
        verbose = false;
    end

    if (ischar(cmap))
        switch (cmap)
            case {'gray'}
                if (verbose), disp('Using Gray colour map (default).'); end
                cmap = gray(256);
            case {'viridis', 'parula'}
                if (verbose), disp('Using parula (viridis) colour map.'); end
                cmap = parula(256);
            case {'jet'}
                if (verbose), disp('Using jet colour map.'); end
                cmap = jet(256);
            case {'random'}
                if (verbose), disp('Using Random colour map.'); end
                maxval = max(vol(:));
                cmap = gtRandCmap(maxval);
            otherwise
                graylev = regexp(cmap, pattern_gray, 'tokens');
                randlev = regexp(cmap, pattern_rand, 'tokens');
                conslev = regexp(cmap, pattern_cons, 'tokens');

                if (~isempty(graylev))
                    graylev = char(graylev{1});
                    if (verbose), disp(['Using Gray colour map with ' graylev ' levels.']); end
                    graylev = str2double(graylev);
                    cmap = gray(graylev);
                elseif (~isempty(randlev))
                    randlev = char(randlev{1});
                    if (verbose), disp(['Using Random colour map with ' randlev ' levels.']); end
                    randlev = uint64(str2double(randlev));
                    cmap = gtRandCmap(randlev);
                elseif (~isempty(conslev))
                    conslev = char(conslev{1});
                    if (verbose), disp(['Using Constant red colour map with ' conslev ' levels.']); end
                    conslev = uint64(str2double(conslev));
                    cmap = [0 0 0 ; repmat([1 0 0], [conslev 1])];
                else
                    if (verbose), disp(['Didn''t understand "' cmap ...
                                        '". Using the default Gray colour map.']); end
                    cmap = gray(256);
                end
        end
    end
end
