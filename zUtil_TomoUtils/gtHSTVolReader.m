function [vol, HST_header] = gtHSTVolReader(full_fname, varargin)
% GTHSTVOLREADER  Reads volumes got from pyHST reconstruction tool.
%     -------------------------------------------------------------------------
%     [vol, HST_header] = gtHSTVolReader(full_fname, varargin)
%
%     INPUT:
%       full_fname = <string> Name of the volume file. Can be 'vname.vol', 
%                             'vname.vol.xml' or just 'vname'
%
%     OPTIONAL INPUT (varargin as a list of pairs, see parse_pv_pairs.m):
%       'xrange'   = <range>  Range of values to read along x axis  
%       'yrange'   = <range>  Range of values to read along y axis
%       'zrange'   = <range>  Range of values to read along z axis
%                             If a range is not specified along an axis, it is
%                             asked to the user
%
%     OUTPUT:
%       vol        = <3Dimage>  Output 3D volume
%       HST_header = <struct>   HST volume header fields stored in a structure
%
%     EXAMPLES:
%
%       vol = gtHSTVolReader('myfile','xrange',200:300,'yrange',400:5:500,'zrange',1:10)
%         reads from myvolume.vol the every voxel in X between 200 and
%         300, every 5th voxel in Y between 400 and 500, and the first 10
%         slices in Z.
%
%       vol = gtHSTVolReader('yourfile','xrange','all','zrange',50:60,'yrange','all');
%          reads from yourvolume.vol the complete Z slices between 50 and
%          60.  NB that the parameters can be in ANY order
%
%
% NB:  Reading 32 bit files (.vol) gives output as 'single's - 32 bit
%      floats. Reading 8 bit files (.raw) gives output as 'uint8's - 8 bit
%      unsigned integers.  These take up much less space, but not all
%      calculations can be done with them.  If you want them as floating point
%      numbers, use:
%
%        vol8  = gtHSTVolReader(my8bitfile...);
%        vol32 = single(vol8);   % for 32 bit variable OR
%        vol64 = double(vol8);   % for 64 bit (double) variable - the matlab
%                                default type.
%
% PS:  If you copy this function, look at the internal comments (too long for the help) to
%      know what else you need.
% 
% Required files from the directory:
%        /data/id19/archive/matlab/util/
%
% Individual files:
%        parse_pv_pairs.m
%        inputwdefaultnumeric.m
%
%     Version 004 10-02-2012 by NVigano,  nicola.vigano@esrf.fr
%       Imported and modified from HSTVolReader.m (id19)
%
%     Version 003 XX-01-2007 by GJohnson
%       Was able to do ring correction, but now use gtHSTVolApplyFunction, instead.
%
%     Version 002 XX-12-2006 by GJohnson
%       Now works with 8 bit files also (.raw)!  Outputs uint8 variables
%
%     Version 001 XX-02-2005 by GJohnson
%       HSTVolReader.m

    if (nargin < 4)
        disp('You could also run this command directly - see help for details:')
    end

    params = struct('data_type_out', [], ...
        'xrange', 'all', 'yrange', 'all', 'zrange', 'all');
    params = parse_pv_pairs(params, varargin);

    if (~exist('full_fname', 'var'))
        % filename not specified on command line
        [tmp1, tmp2] = uigetfile('*','Select a .vol or .raw volume file');
        full_fname = fullfile(tmp2,tmp1);
    end

    try
        HST_header = gtHST_info(full_fname);
    catch mexc
        disp('Unable to read either the volume or the associated XML file.  Please')
        disp('verify that the files exist in the current directory')
        disp(' ')
        mexc1 = MException('HST:wrong_input', 'Unable to read files');
        mexc1 = addCause(mexc1, mexc);
        throw(mexc1);
    end

    if (~exist(HST_header.fname_final, 'file'))
        error('HST:wrong_file_name', ...
            ['File ''' HST_header.fname_final ''' does not exist!']);
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%
    % common for both filetypes

    if (nargin <= 1) % no parameters except the filename
        if (HST_header.volSizeZ == 1) % if there is only one slice, don't bother asking the user and just read the whole thing
            rangeX = 1:HST_header.volSizeX;
            rangeY = 1:HST_header.volSizeY;
            rangeZ = 1:HST_header.volSizeZ;
        else
            disp('Interactive choice of ROI')
            % interactively choose what to read
            fprintf('Volume size: X %d Y %d Z %d\n', ...
                HST_header.volSizeX, HST_header.volSizeY, HST_header.volSizeZ);
            disp('Specify ROI to read');

            rangeX = inputwdefaultnumeric('X', ['1:' num2str(HST_header.volSizeX)]);
            rangeY = inputwdefaultnumeric('Y', ['1:' num2str(HST_header.volSizeY)]);
            rangeZ = inputwdefaultnumeric('Z', ['1:' num2str(HST_header.volSizeZ)]);
        end
    else
        % user has specified x y and z ranges
        % might have used 'all' to indicate all elements
        if (strcmp(params.xrange, 'all'))
            rangeX = 1:HST_header.volSizeX;
        else
            rangeX = params.xrange;
        end
        if (strcmp(params.yrange, 'all'))
            rangeY = 1:HST_header.volSizeY;
        else
            rangeY = params.yrange;
        end
        if (strcmp(params.zrange, 'all'))
            rangeZ = 1:HST_header.volSizeZ;
        else
            rangeZ = params.zrange;
        end
    end

    if (any(find(rangeZ > HST_header.volSizeZ)))
        % if any part of the range is bigger than the volume
        error('HST:wrong_parameter', ...
            'Z range is too big for specified volume file');
    end

    % this is not very efficient for non-contiguous slices.  One day, this
    % should be improved...
    % *** should include memory mapped code from volviewer - useful
    % for larger volumes

    % if reading an 8 bit file, give the output as 8 bits, too.  Saves A LOT of
    % memory...
    infoVOL = HST_header;
    infoVOL.volSizeZ = length(rangeZ);

    vol_size = [HST_header.volSizeX, HST_header.volSizeY, HST_header.volSizeZ];
    roi(2, :) = [...
        rangeX(end) - rangeX(1) + 1, ...
        rangeY(end) - rangeY(1) + 1, ...
        rangeZ(end) - rangeZ(1) + 1, ...
        ];
    roi(1, :) = [rangeX(1), rangeY(1), rangeZ(1)];
    roi = roi(:)';

    vol = gtVolRead(HST_header.fname_final, 'vol_size', vol_size, 'roi', roi, ...
        'byteorder', HST_header.fbyteorder, 'data_type_raw', HST_header.ftype, ...
        'data_type_out', params.data_type_out );

    % Applying slicing
    vol = vol(rangeX - rangeX(1) + 1, rangeY - rangeY(1) + 1, rangeZ - rangeZ(1) + 1);
end

