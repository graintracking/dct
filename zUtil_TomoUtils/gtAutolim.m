function cmaprange = gtAutolim(img, percentile)
% GTAUTOLIM  Uses stretchlim (from Image Processing Toolbox) but rescales to 
%            limits of data first. Returns a range pair suitable for use in 
%            imshow or imagesc.
%     -------------------------------------------------------------------------
%     cmaprange = gtAutolim(img, percentile)
%
%     INPUT:
%       img        = <image>       Input image
%
%     OPTIONAL INPUT:
%       percentile = <float>     Fraction of the image to saturate
%                                default is 0.02 
%
%     OUTPUT:
%       cmaprange  = [min ; max] New data limits computed by the macro
%
%     Version 002 XX-02-2012 by NVigano,  nicola.vigano@esrf.fr
%       Imported and modified from autolim.m (id19)
%    
%     Version 001 XX-05-2006 by GJohnson
%       autolim.m

    img = double(img);  % data must be cast to double before manipulation
    if ~exist('percentile', 'var')
        percentile = 0.02;  % 2 percent by default
    end
    img = img(:);
    % find range of data
    minim = min(img);
    maxim = max(img);

    % scale image to range [0..1]
    img = (img-minim)./(maxim-minim);

    slim = stretchlim(img, percentile)';
    cmaprange = (slim.*(maxim-minim))+minim;
end
