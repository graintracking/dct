function volume = gtVolRead(filename, varargin)
% GTVOLREAD  Reads a volume in raw format into the matrix image.
%     -------------------------------------------------------------------------
%     volume = gtVolRead(filename, varargin)
%
%     INPUT:
%       filename = <string>  Name of the input file
%       infoVOL  = <struct>  Data structure with the following fields:
%                            - ftype: datatype, see fread for possibilities
%                            - volSizeX: first dimension (rows in Matlab)
%                            - volSizeY: second dimension (columns in Matlab)
%                            - volSizeZ: third dimension (default is 1)
%                            - fbyteorder: 'b' or 'l' (default); big endian or
%                              little endian
%
%     OPTIONAL INPUT:
%       skip     = <int>     Number of bytes skipped at file start
%                            default is 0
%       outFtype = <string>  Can be used to force the output datatype
%
%     OUTPUT:
%       volume   = <3Dimage> Output volume with the original type, if not 
%                            enforced by the user in the field ftype
%
%     Version 003 10-02-2012 by NVigano,   nicola.vigano@esrf.fr   
%       Imported and changed API to use structure, added better error
%       checking/reporting
%
%     Version 002 XX-03-2006 by GJohnson
%       Output singles instead of doubles (requires half the memory, useful 
%       for large volumes).
%
%     Version 001            by WLudwig,   wolfgang.ludwig@esrf.fr 
%                               PReischig, peter.reischig@esrf.fr 

    [~, ~, default_byteorder] = computer();
    conf = struct( ...
        'vol_size', [], ...
        'roi', [], ...
        'skip', 0, ...
        'byteorder', default_byteorder, ...
        'data_type_raw', 'single', ...
        'data_type_out', []);
    conf = parse_pv_pairs(conf, varargin);

    if (isempty(conf.vol_size))
        error('gtVolRead:missing_parameter', ...
            'The volume size is required')
    end

    conf.data_type_raw = convert_to_matlab_datatype(conf.data_type_raw);
    if (isempty(conf.data_type_out))
        conf.data_type_out = conf.data_type_raw;
    end
    data_type = [conf.data_type_raw '=>' conf.data_type_out];

    fid = fopen(filename, 'r', conf.byteorder);
    if (fid == -1)
        error('gtReadVol:wrong_file_name', ...
            'File "%s" does not exist!', filename);
    end

    switch (conf.data_type_raw)
        case {'uint8', 'int8'}
            nbytes = 1;
        case {'uint16', 'int16'}
            nbytes = 2;
        case {'uint32', 'int32', 'float32', 'single'}
            nbytes = 4;
        case {'uint64', 'int64', 'float64', 'double'}
            nbytes = 8;
    end

    if ((conf.skip > 0) && fseek(fid, nbytes * conf.skip, 'bof') == -1)
        fclose(fid);
        error('gtReadVol:wrong_argument', ...
            'Problems reading file, file size ???');
    end

    if (~isempty(conf.roi))
        roi_offets = conf.roi(1:2:end);
        out_vol_size = conf.roi(2:2:end);

        base_initial_skip = sum((roi_offets - 1) .* cumprod([1, conf.vol_size(1:end-1)])) + conf.skip;
        if (fseek(fid, nbytes * base_initial_skip, 'bof') == -1)
            fclose(fid);
            error('gtReadVol:wrong_argument', ...
                'Problems reading file, file size ???');
        end

        if (numel(out_vol_size) == 3)
            num_slices = out_vol_size(end);
            volume = zeros([prod(out_vol_size(1:end-1)), num_slices], conf.data_type_out);
            expected_counts = prod(out_vol_size(1:end-1));
            slice_count = zeros(num_slices, 1);
            base_slice_skip = prod(conf.vol_size(1:end-1));
            for ii = 1:num_slices
                [volume(:, ii), slice_count(ii)] = fread(fid, expected_counts, data_type);

                if (ii < num_slices)
                    slice_skip = base_slice_skip * ii + base_initial_skip;
                    if (fseek(fid, nbytes * slice_skip, 'bof') == -1)
                        fclose(fid);
                        error('gtReadVol:wrong_argument', ...
                            'Problems reading file, file size ???');
                    end
                end
            end
            count = sum(slice_count);
            expected_counts = expected_counts * num_slices;
        else
            expected_counts = prod(out_vol_size);
            [volume, count] = fread(fid, expected_counts, data_type);
        end
    else
        out_vol_size = conf.vol_size;
        expected_counts = prod(conf.vol_size);
        [volume, count] = fread(fid, expected_counts, data_type);
    end
    fclose(fid);

    if (count ~= expected_counts)
        if (exist('slice_count', 'var'))
            fprintf('Slice counts: ')
            disp(slice_count)
        end
        error('gtReadVol:wrong_parameters', ...
            'The volume contains less data (%d) than expected (%d)', ...
            count, expected_counts);
    end

    volume = reshape(volume, out_vol_size);
end

function dtype = convert_to_matlab_datatype(dtype)
    switch (dtype)
        case 'float32'
            dtype = 'single';
        case 'double'
            dtype = 'double';
    end
end

