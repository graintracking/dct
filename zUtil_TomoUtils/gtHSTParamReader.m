function par = gtHSTParamReader(filename)
% GTHSTPARAMREADER  Parses HST parameter file to HST parameter structure.
%     -------------------------------------------------------------------------
%     par = gtHSTParamReader(filename)
% 
%     INPUT:
%       filename = <string> 
%
%     OUTPUT:
%       par      = <struct> HST parameter structure 
%
%     SUB-FUNCTIONS:
%[sub]- sfCleanField
%
%     Version 002 14-03-2012 by YGuilhem, yoann.guilhem@esrf.fr
%       Import par_read.m in DCT with some modifications
%
%     Version 001 01-11-2006 by HProudhon
%       par_read.m

par = [];
par.parfilename = filename;
pattern = '(?<parameter>.*)=(?<value>.*)';
if ~exist(filename, 'file')
    gtError('gtHSTParamReader', ['Could not find ' filename]);
end

fid = fopen(filename, 'rt');
if (fid == -1)
    gtError('gtHSTParamReader', ['Could not open ' filename]);
end

% Initialize correct_rings_nb
par.correct_rings_nb = -1;

while ~feof(fid)
    txt = fgetl(fid);
    hashndx    = find(txt=='#', 1, 'first') - 1;
    exclamndx  = find(txt=='!', 1, 'first') - 1;
    commentndx = min(union(hashndx, exclamndx));

    if ~isempty(commentndx)
        txt = txt(1:commentndx);
    end

    tmp = regexp(txt, pattern, 'names');

    if ~isempty(tmp)
        field = sfCleanField(tmp.parameter);
        tmp.value = strtrim(tmp.value);

        if regexp(field, '^ar2(\d+)')
            value = regexprep(field, 'ar2(\d+)', '$1');
            par.correct_rings_nb = str2double(value)/2 - 1;
            continue;
        end

        nb = str2double(tmp.value);
        if ~isempty(regexp(tmp.value, '[0-9]', 'once')) && ~isnan(nb)
            par.(field) = nb;
        else
            par.(field) = tmp.value;
        end
    else
        disp(txt);
    end
end

% Now handle special cases...
ndx = find(par.options==':') + 2;
tmp.padding = par.options(ndx(1));
tmp.axis    = par.options(ndx(2));
par.options_padding = tmp.padding;
par.options_axis = tmp.axis;
if length(ndx) > 2
    tmp.avoid   = par.options(ndx(3));
    par.options_avoidhalftomo = tmp.avoid;
end

par.ccd_filter_para = sscanf(par.ccd_filter_para, '{"threshold":%f}');

fclose(fid);

end % end of function

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SUB-FUNCTIONS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function field = sfCleanField(str)
% SFCLEANFIELD  Removes weird characters (spaces, parentheses) from field.
    str = lower(strtrim(str));
    str(str==' ') = '_';
    str(str=='(') = [];
    str(str==')') = [];
    str(str=='-') = '_';
    str(str=='[') = [];
    str(str==']') = [];
    str(str==':') = [];
    field = str;
end % end of sfCleanField
