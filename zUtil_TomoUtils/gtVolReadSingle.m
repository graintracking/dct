function volume = gtVolReadSingle(filename, infoVOL, skip)
% GTVOLREADSINGLE  Reads a volume in raw format into the matrix image.
%     -------------------------------------------------------------------------
%     volume = gtVolReadSingle(filename, infoVOL, skip)
%
%     INPUT:
%       filename = <string> Name of the input file
%       infoVOL  = <struct> Data structure with the following fields:
%                           - ftype: datatype, see fread for possibilities
%                           - volSizeX: first dimension (rows in Matlab)
%                           - volSizeY: second dimension (columns in Matlab)
%                           - volSizeZ: third dimension (default is 1)
%                           - fbyteorder: 'b' or 'l' (default); big endian or
%                             little endian
%       skip     = <int>    Number of bytes skipped at file start (default is 0)
%
%     OUTPUT:
%       volume   = <float>  Output volume with single precision 
%
%     Version 003 10-02-2012 by NVigano,   nicola.vigano@esrf.fr   
%       Imported and changed API to use structure, added better error
%       checking/reporting
%
%     Version 002 XX-03-2006 by GJohnson
%       Output singles instead of doubles (requires half the memory, useful 
%       for large volumes).
%
%     Version 001            by WLudwig,   wolfgang.ludwig@esrf.fr 
%                               PReischig, peter.reischig@esrf.fr 

    volume = gtVolRead(filename, infoVOL, skip, 'float32');
end
