classdef GtMultiVolView < GtVolView
    properties
        volumes;
    end

    methods (Access = public)
        function obj = GtMultiVolView(vols, varargin)
            viewer_args = varargin;
            if ~ismember('f_title', varargin(1:2:end))
                viewer_args(end+1:end+2) = {'f_title', 'Multi-Volume Viewer'};
            end
            tempVol = GtMultiVolView.getMultiVol(vols, 1);
            obj = obj@GtVolView(tempVol, viewer_args{:});
            obj.volumes = vols;
            numVols = GtMultiVolView.getNumVols(obj.volumes);
            set(obj.conf.upper_s_bar(3), 'String', sprintf('%d', 1));
            set(obj.conf.upper_s_bar(4), 'String', sprintf('/%d', numVols));
        end
    end

    methods (Access = protected)
        function initParams(obj, arguments)
            obj.conf.volumeIndex = 1;
            initParams@GtVolView(obj, arguments)
        end

        function initGui(obj)
        % GTGRAINSMANAGER/INITGUI Inherited method that builds the interface

            initGui@GtVolView(obj)
            % Let's disable zoom, while configuring
            zoomOp = zoom(obj.conf.h_figure);
            zoomState = get(zoomOp, 'Enable');
            zoom(obj.conf.h_figure, 'off');

            obj.conf.main_s_boxes = uiextras.VBox('Parent', obj.conf.currentParent);
            obj.conf.upper_s_boxes = uiextras.HBox('Parent', obj.conf.main_s_boxes);

            obj.conf.h_viewer_panel = uipanel('BorderWidth', 4, ...
                                              'BorderType', 'line', ...
                                              'Parent', obj.conf.main_s_boxes);
            set(obj.conf.main_boxes, 'Parent', obj.conf.h_viewer_panel);
            set(obj.conf.main_s_boxes, 'Sizes', [25 -1]);

            % Setting up image selection bar
            obj.conf.upper_s_bar(1) = uicontrol('Parent', obj.conf.upper_s_boxes, ...
                'Style', 'text', 'HorizontalAlignment', 'left');
            obj.conf.upper_s_bar(2) = uicontrol('Parent', obj.conf.upper_s_boxes, ...
                'Style', 'text', 'String', 'Index:', 'HorizontalAlignment', 'left');
            obj.conf.upper_s_bar(3) = uicontrol('Parent', obj.conf.upper_s_boxes, ...
                'Style', 'edit', 'HorizontalAlignment', 'right', ...
                'Callback', @(src, evt)onChangeEdit(obj));
            obj.conf.upper_s_bar(4) = uicontrol('Parent', obj.conf.upper_s_boxes, ...
                'Style', 'text', 'HorizontalAlignment', 'left', ...
                'String', sprintf('/%d', 1));
            obj.conf.upper_s_bar(5) = uicontrol('Parent', obj.conf.upper_s_boxes, ...
                'Style', 'pushbutton', 'String', 'List', ...
                'Callback', @(src, evt)onSelectButton(obj));
            set(obj.conf.upper_s_boxes, 'Sizes', [-1 45 40 50 60]);

            % Reset back the zoom to the previous state (before the inclusion)
            set(zoomOp, 'Enable', zoomState);
        end
    end

    methods (Access = public)
        function onSelectButton(obj)
            numVols = GtMultiVolView.getNumVols(obj.volumes);
            names = arrayfun(@(x){sprintf('%d', x)}, 1:numVols);

            selector = GtGuiSelectInList(names);
            index = selector.join();

            if (~isempty(index))
                try
                    obj.selectMultiVol(index);
                catch mexc
                    gtPrintException(mexc)
                    d = errordlg(['Error: ' mexc.message], mexc.identifier, 'modal');
                    uiwait(d);
                end
            end
        end

        function onChangeEdit(obj)
            index = get(obj.conf.upper_s_bar(3), 'String');
            try
                index = str2double(index);
                obj.selectMultiVol(index);
            catch mexc
                gtPrintException(mexc)
                d = errordlg(['Error: ' mexc.message], mexc.identifier, 'modal');
                uiwait(d);
            end
        end

        function selectMultiVol(obj, index)
            tempVol = GtMultiVolView.getMultiVol(obj.volumes, index);
            obj.changeVol(tempVol);
            obj.conf.volumeIndex = index;
            set(obj.conf.upper_s_bar(3), 'String', sprintf('%d', index));
        end
    end

    methods (Access = public, Static)
        function vol = getMultiVol(vols, index)
            if (iscell(vols))
                if (index == 0)
                    vol = vols{1};
                    for n = 2:numel(vols)
                        vol = vol + vols{n};
                    end
                else
                    vol = vols{index};
                end
            elseif (isnumeric(vols))
                if (index == 0)
                    vol = sum(vols, 4);
                else
                    vol = vols(:, :, :, index);
                end
            else
                gtError('GtMultiVolView:wrong_argument', ...
                    'Unsupported multiple array type: %s', class(vols))
            end
        end

        function numVols = getNumVols(vols)
            if (iscell(vols))
                numVols = length(vols);
            elseif (isnumeric(vols))
                numVols = size(vols, 4);
            else
                gtError('GtMultiVolView:wrong_argument', ...
                    'Unsupported multiple array type: %s', class(vols))
            end
        end

        function handles = compareVolumes(vols, varargin)
            handles = GtMultiVolView.classCompareVolumes(vols, varargin{:});
        end
    end
end
