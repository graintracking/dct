function infoVol = gtCreateInfoVol(volSize, dtype, dbyteorder)
    if (~exist('dtype', 'var'))
        dtype = 'float32';
    end
    if (~exist('dbyteorder', 'var'))
        [~, ~, dbyteorder] = computer();
    end

    infoVol.volSizeX = volSize(1);
    infoVol.volSizeY = volSize(2);
    infoVol.volSizeZ = volSize(3);

    infoVol.ftype = dtype;
    infoVol.fbyteorder = dbyteorder;
end
