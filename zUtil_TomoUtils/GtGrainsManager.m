classdef GtGrainsManager < GtVolView
% GTGRAINSMANAGER
%
% You can pass extra behaviour options to the right click menu in the axes,
% using the argument syntax:
%   - 'plugins', {'menu_entry1', @callback1 [, 'menu_entry2', @callback2, ...]}
    properties
        selectedPhase = 0;
        selectedGrain = 0;

        plugins = {};

        separateGrainIDs = 0;
        cmap = struct();

        dataDir = [];

        assembler_params = [];
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Public object API - Interaction with outside
    methods (Access = public)
        function obj = GtGrainsManager(dataDir, varargin)
        % GTGRAINSMANAGER/GTGRAINSMANAGER  Constructor
        %
        % obj = GtGrainsManager(dataDir, varargin)

            if (~exist('dataDir', 'var') || isempty(dataDir))
                dataDir = pwd();
            end

            viewer_args = varargin;
            if isempty(varargin) || ~ismember('f_title',varargin(1:2:end))
                viewer_args(end+1:end+2) = {'f_title', ['Grains Manager : ' gtGetLastDirName(dataDir)]};
            end
            vol = ones(2, 2, 2);
            vol(1:2, 1, 1) = 0;
            obj = obj@GtVolView(vol, viewer_args{:});
            obj.dataDir = dataDir;

            % When passing 'view_args' to GtVolView directly, the
            % 'slicendx' parameter is not taken into acocunt because the
            % dummy volume size is initially 2x2x2 only !
            % Here below is re-passed to GtVolView
            [hasP,ind] = ismember('slicendx',varargin(1:2:end));
            if (hasP)
                obj.initParams(varargin(ind*2-1:ind*2))
            end

            % Initialise cache and metadata
            try
                grainsDir = fullfile(obj.dataDir, '4_grains');
                reconDir = fullfile(obj.dataDir, '5_reconstruction');

                sampleFileModel = fullfile(grainsDir, 'sample.mat');
                obj.cache.createModel(sampleFileModel, 'sample', 'error', 'always');

                paramsFileModel = fullfile(obj.dataDir, 'parameters.mat');
                obj.cache.createModel(paramsFileModel, 'parameters', 'error', 'always');

                grainDirsModel = fullfile(grainsDir, 'phase_%02d', 'grain_%04d.mat');
                obj.cache.createModel(grainDirsModel, 'grain', 'error', 'always');

                grainDetDirsModel = fullfile(grainsDir, 'phase_%02d', 'grain_details_%04d.mat');
                obj.cache.createModel(grainDetDirsModel, 'grain_det');

                grainExtDetDirsModel = fullfile(grainsDir, 'phase_%02d', 'grain_extended_details_%04d.mat');
                obj.cache.createModel(grainExtDetDirsModel, 'grain_ext_det');

                grainClusterDirsModel = fullfile(grainsDir, 'phase_%02d', 'grain_cluster%s.mat');
                obj.cache.createModel(grainClusterDirsModel, 'grain_cluster');

                grainDetClusterDirsModel = fullfile(grainsDir, 'phase_%02d', 'grain_cluster_details%s.mat');
                obj.cache.createModel(grainDetClusterDirsModel, 'grain_cluster_det');

                phaseVolumeModel = fullfile(reconDir, 'phase_%02d_vol.mat');
                obj.cache.createModel(phaseVolumeModel, 'phases');

                sample = obj.getSample();

                staticAbsVolumeModel = fullfile(obj.dataDir, sample.absVolFile);
                obj.cache.createModel(staticAbsVolumeModel, 'volume_absorption');

                staticVolumeModel = fullfile(obj.dataDir, sample.volumeFile);
                obj.cache.createModel(staticVolumeModel, 'volume');

                staticDilVolumeModel = fullfile(obj.dataDir, sample.dilVolFile);
                obj.cache.createModel(staticDilVolumeModel, 'volume_dilated');

                obj.fillSidePanel();

                obj.reloadAssebledVolume();

                obj.updateStatus('Loaded.');
            catch mexc
                gtPrintException(mexc)
                d = errordlg(['Error: ' mexc.message], 'Fatal error', 'modal');
                uiwait(d, 10);
                obj.delete();
            end
        end

        function [infoStr, phaseID, grainID] = getGrainInfo(obj, point)
        % GTGRAINSMANAGER/GETGRAININFO  Returns a string which contains
        % information about a specific grain, which resides in a specific voxel
        %
        % [infoStr, phaseID, grainID] = getGrainInfo(obj, point)
        %
        %
        % The difference with the 'voxel details' action, is that this looks
        % into the assembled volume, while the other looks into the single grain
        % volumes.
        % So this give the iformation about the result of the assembling.

            [phaseID, grainID] = obj.getPhaseAndGrainOfPoint(point);

            if ((phaseID == 0) || (grainID == 0))
                infoStr = 'No grain selected';
            elseif ((phaseID < 0) || (grainID < 0))
                infoStr = 'Conflict region selected';
            else
                infoStr = obj.produceGrainInfo(phaseID, grainID);
            end
        end

        function selectDiffSpots(obj, phaseID, grainID)
        % GTGRAINSMANAGER/SELECTDIFFSPOTS Opens the gtGuiMontage facility
        % function to select the diffraction spots in the given grain of the
        % selected phase

            if ((phaseID == 0) || (grainID == 0))
                infoStr = 'No grain selected';

                disp(infoStr);
                helpdlg(infoStr, 'Grain Diffspots');
            elseif ((phaseID < 0) || (grainID < 0))
                infoStr = 'Conflict region selected';

                disp(infoStr);
                helpdlg(infoStr, 'Grain Diffspots');
            else
                fields_to_load = ...
                    {'proj', 'allblobs', 'spotid', 'intensity'};

                % Loading the grain structure as needed
                params = obj.getParams();
                show_blobs = ~strcmpi(params.rec.grains.algorithm, 'SIRT');

                gr = obj.cache.get('grain', {phaseID, grainID}, fields_to_load);
                if (~isfield(gr.proj, 'ondet'))
                    gr_extra = obj.cache.get('grain', {phaseID, grainID}, ...
                        {'included', 'ondet', 'selected'});
                    gr.proj.ondet = gr_extra.ondet;
                    gr.proj.included = gr_extra.included;
                    gr.proj.selected = gr_extra.selected;
                end
                gr.id = grainID;
                gr.phaseid = phaseID;
                if (show_blobs && ~isfield(gr.proj, 'bl'))
                    % Let's handle the old location for .bl
                    gr.proj.bl = obj.cache.get('grain', {phaseID, grainID}, 'bl');
                end

                % Launch Montage
                gr.proj(1).selected = gtGuiGrainMontage(gr, [], show_blobs);

                % Save the selected
                obj.cache.set('grain', {phaseID, grainID}, gr.proj, 'proj');
                obj.cache.flushObjectField('grain', {phaseID, grainID}, 'proj');
            end
        end

        function reloadAssebledVolume(obj, varargin)
        % GTGRAINSMANAGER/RELOADASSEBLEDVOLUME  Reloads the assembled volumes and
        % displays it
        %
        % reloadAssebledVolume(obj, varargin)

            try
                switch (obj.conf.load_vol)
                    case 'phase'
                        % selected phase
                        phaseid = get(obj.conf.h_phase_edit, 'String');
                        phaseid = str2double(phaseid);
                        if (~phaseid)
                            phaseid = 1;
                            set(obj.conf.h_phase_edit, 'String', '1');
                            disp('Set default phaseid to 1')
                            obj.selectedPhase = phaseid;
                        end
                        grainIDs = obj.cache.get('phases', {phaseid}, 'vol');
                    case 'complete'
                        set(obj.conf.h_phase_edit, 'String', '0');
                        obj.selectedPhase = 0;
                        phaseIDs = obj.cache.get('volume', {}, 'phases');
                        grainIDs = obj.cache.get('volume', {}, 'grains');
                    case 'dilated'
                        set(obj.conf.h_phase_edit, 'String', '0');
                        obj.selectedPhase = 0;
                        phaseIDs = obj.cache.get('volume_dilated', {}, 'phases');
                        grainIDs = obj.cache.get('volume_dilated', {}, 'grains');
                    otherwise
                        error('GUI:wrong_argument', ...
                              'Unknown kind of volume: "%s"', obj.conf.load_vol)
                end

                if (strcmpi(obj.conf.load_vol, 'phase'))
                    finalVol = grainIDs;
                else
                    obj.separateGrainIDs = max(grainIDs(:));

                    finalVol = zeros(size(phaseIDs), class(phaseIDs));
                    for ii = 1:max(phaseIDs(:))
                        indexes = find(phaseIDs == ii);
                        finalVol(indexes) = grainIDs(indexes) + obj.separateGrainIDs * (ii-1);
                    end
                    % Treat conflicts
                    finalVol((phaseIDs == -1) | (grainIDs == -1)) = -1;
                end

                % change cmap for the current volume
                vargs_out = obj.updateCmapVolume(finalVol, varargin{:});

                newArgs = [vargs_out {'CMap', obj.conf.cmap}];
                obj.changeVol(finalVol, newArgs{:});

            catch mexc
                gtPrintException(mexc, 'Couldn''t load Volume file');
                uiwait(errordlg(mexc.message, 'Couldn''t load Volume file'), 10);
            end
        end

        function vargs_out = updateCmapVolume(obj, finalVol, varargin)
        % GTGRAINSMANAGER/UPDATECMAPVOLUME  Handling colormap
        %
        % varargout = updateCmapVolume(obj, finalVol, varargin)

            if isfield(obj.conf, 'h_cmap') && isa(obj.conf.h_cmap, 'GtColormap')
                CMap = obj.conf.h_cmap;
            elseif isprop(obj, 'cmap')
                CMap = obj.cmap;
            else
                CMap = struct();
            end

            maxGrainID = max(finalVol(:));
            % change cmap
            [hasP, ind] = ismember('CMap', varargin(1:2:end));
            if (hasP)
                obj.conf.cmap = varargin{ind*2};
                varargin(ind*2-1:ind*2) = [];
            elseif isfield(CMap, 'colormap') && ~isempty(CMap.colormap)
                obj.conf.cmap(end-size(CMap.colormap, 1)+1:end, :) = CMap.colormap;
            else
                obj.conf.cmap = gtRandCmap(maxGrainID);
                disp('Using random cmap')
                CMap.defname = 'Rand';
                CMap.type = 'default';
            end

            % get cmap data
            [hasP, ind] = ismember('default', varargin(1:2:end));
            if (hasP)
                default = varargin{ind*2};
                varargin(ind*2-1:ind*2) = [];
            else
                default = false;
            end
            vargs_out = varargin;


            if (default || ~isfield(CMap, 'colormap'))
                CMap.origCmap = obj.conf.cmap(2:end,:);
            elseif (~default && isfield(CMap, 'colormap'))
                CMap.origCmap = CMap.colormap;
            else
                CMap.origCmap = [];
            end
            CMap.colormap = obj.conf.cmap(2:end,:);

            if isfield(CMap, 'defname') && ismember(CMap.defname,{'Rand','IPF','Rvec','Caxis'})
                CMap.maxV = [];
                if ismember(CMap.defname,{'Rand'})
                    CMap.values = [];
                end
            end
            CMap.hideIDs = [];
            CMap.maskCmap = [];
            CMap.clims = obj.conf.clims;
            CMap.IDs = setdiff(obj.conf.clims(1):obj.conf.clims(2),[-1 0]);
            CMap.activeIDs = ismember(CMap.IDs, setdiff(unique(obj.conf.vol),[-1 0]))';

            % change figure title
            if isfield(CMap, 'defname')
                curr_title = get(obj.conf.h_figure,'Name');
                tmp = regexp(curr_title,'colormap: \w*','match');
                if ~isempty(tmp)
                    new_title = strrep(curr_title,tmp{1},['colormap: ' CMap.defname]);
                else
                    new_title = [curr_title ' - colormap: ' CMap.defname];
                end
                set(obj.conf.h_figure,'Name',new_title);
            end

            % check conflict/conflict color existence
            if (any(finalVol(:) == -1)) && size(obj.conf.cmap,1) == maxGrainID+1
                disp('Set conflict color to white...');
                obj.conf.cmap = [1 1 1; obj.conf.cmap];
                CMap.hasConflict = true;
                CMap.conflict = [1 1 1];
            elseif all(finalVol(:) ~= -1) && size(obj.conf.cmap,1) == maxGrainID+2
                disp('Remove conflict color...');
                obj.conf.cmap(1,:) = [];
                CMap.hasConflict = false;
            end
            % update cmap
            if isfield(obj.conf, 'h_cmap') && isa(obj.conf.h_cmap, 'GtColormap')
                obj.conf.h_cmap.importProperties(CMap);
            end
            if isprop(obj, 'cmap') && ~isempty(obj.cmap)
                obj.cmap = gtAddMatFile(obj.cmap, CMap, true, true);
            else
                obj.cmap = CMap;
            end
        end

        function [point, values] = getPhaseAndGrainOfCurrentPointer(obj)
        % GTGRAINSMANAGER/GETPHASEANDGRAINOFCURRENTPOINTER  Returns the
        % Phase ID and Grain ID of the current pointer
        %
        % [point, values] = getPhaseAndGrainOfCurrentPointer(obj)

            point = round(obj.get3DPixelInfo());
            [phaseNum, grainNum] = obj.getPhaseAndGrainOfPoint(point);
            values = [phaseNum, grainNum];
        end

        function [phaseNum, grainNum] = getPhaseAndGrainOfPoint(obj, point)
        % GTGRAINSMANAGER/GETPHASEANDGRAINOFPOINT  Returns the Phase ID and Grain
        % ID in the selected voxel
        %
        % [phaseNum, grainNum] = getPhaseAndGrainOfPoint(obj, point)

            if isempty(point)
                disp('Pointer is outside the volume!')
                phaseNum = 0;
                grainNum = 0;
                return
            end

            voxel = round(point);

            switch (obj.conf.load_vol)
                case 'phase'
                    % selected phase
                    phaseNum = get(obj.conf.h_phase_edit, 'String');
                    phaseNum = str2double(phaseNum);

                    grainIDs = obj.cache.get('phases', {phaseNum}, 'vol');
                    grainNum = grainIDs(voxel(1), voxel(2), voxel(3));
                case {'complete', 'dilated'}
                    if (strcmpi(obj.conf.load_vol, 'complete'))
                        phaseIDs = obj.cache.get('volume', {}, 'phases');
                        grainIDs = obj.cache.get('volume', {}, 'grains');
                    else
                        phaseIDs = obj.cache.get('volume_dilated', {}, 'phases');
                        grainIDs = obj.cache.get('volume_dilated', {}, 'grains');
                    end
                    phaseNum = phaseIDs(voxel(1), voxel(2), voxel(3));

                    if (phaseNum == 0)
                        grainNum = 0;
                    else
                        grainNum = grainIDs(voxel(1), voxel(2), voxel(3));
                    end
                otherwise
                    error('GUI:wrong_argument', ...
                          'Unknown kind of volume: "%s"', obj.conf.load_vol)
            end
        end

        function dist = getVoxelMisorientation(obj, point)
            params = obj.getParams();

            is_6D = strcmpi(params.rec.grains.algorithm(1:2), '6D');
            if (~is_6D)
                error('GtGrainsManager:wrong_parameters', ...
                    'Misorientation is only available with 6D reconstructions')
            end

            [phaseID, grainID] = obj.getPhaseAndGrainOfPoint(point);

            avg_r_vector = obj.cache.get('grain', {phaseID, grainID}, 'R_vector');

            sample = obj.getSample();
            is_extended = sample.phases{phaseID}.getUseExtended(grainID);
            if (is_extended)
                gr_odf6d = obj.cache.get('grain_ext_det', {phaseID, grainID}, 'ODF6D');
            else
                gr_odf6d = obj.cache.get('grain_det', {phaseID, grainID}, 'ODF6D');
            end

            try
                rot_angles = obj.cache.get('phases', {phaseID}, 'rotation_angles');
                rot_axes = obj.cache.get('phases', {phaseID}, 'rotation_axes');

                if (~isempty(rot_angles))
                    fprintf('Rotating grain volumes! (this might slow down the operation)\n')
                    proj = obj.cache.get('grain', {phaseID, grainID}, 'proj');
                    gr_odf6d = gtGrainRotateStructure(gr_odf6d, proj, 'ODF6D', params, rot_angles, rot_axes);
                end
            catch mexc
                if (strcmpi(mexc.identifier, 'MATLAB:MatFile:VariableNotInFile'))
                    fprintf('\nNo rotation information for phase %02d, skipping rotation\n', phaseID)
                else
                    rethrow(mexc)
                end
            end

            vol_point = point - gr_odf6d.shift;
            try
                point_r_vector = gr_odf6d.voxels_avg_R_vectors(vol_point(1), vol_point(2), vol_point(3), :);
            catch mexc
                fprintf('Assembled volume point: (%d, %d, %d), grain shift: (%d, %d, %d), grain point (%d, %d, %d)', ...
                    point, gr_odf6d.shift, vol_point);
                rethrow(mexc)
            end
            point_r_vector = reshape(point_r_vector, 1, 3);

            dist = 2 * atand(avg_r_vector - point_r_vector);
        end

        function grains = getVoxelDetails(obj, point)
        % GTGRAINSMANAGER/GETVOXELDETAILS  Returns the grains (organized by
        % phase) that claim the voxel
        %
        % grains = getVoxelDetails(obj, point)

            voxel = round(point);
            sample = obj.getSample();
            phasesNum = length(sample.phases);
            grains = cell(1, phasesNum);
            % Check per each phase, which grains claim the voxel
            for phaseID = 1:phasesNum
                bboxList = sample.phases{phaseID}.simpleSearchBBtouched(voxel);
                for jj = 1:length(bboxList)
                    grainID = bboxList(jj);
                    try
                        gr_seg = obj.cache.get('grain_det', {phaseID, grainID}, 'SEG');
                    catch
                        gr_seg = obj.cache.get('grain', {phaseID, grainID}, {'seg'});
                    end
                    if (isempty(gr_seg.seg) || numel(gr_seg.seg) == 1)
                        fprintf('Segmentation volume for grain %d is empty. Please run Reconstruction and 3d Threshold again!\n', grainID)
                        continue
                    end
                    shift = sample.phases{phaseID}.getBoundingBoxShift(grainID);
                    pos = (voxel - shift) + 1;
                    if (gr_seg.seg(pos(1), pos(2), pos(3)) ~= 0)
                        grains{phaseID} = [grains{phaseID} grainID];
                    end
                end
            end
        end

        function goToGrain(obj, phaseID, grainID)
        % GTGRAINSMANAGER/GOTTOGRAIN  Goes to the selected grain, into the volume
        %
        % goToGrain(obj, phaseID, grainID)

            sample = obj.getSample();
            bbox = sample.phases{phaseID}.getBoundingBoxExtremes(grainID);
            extremes = [bbox(obj.conf.ortDim) bbox(obj.conf.ortDim + 3)];
            if (obj.conf.slicendx < extremes(1) || obj.conf.slicendx > extremes(2))
                center = round(sum(extremes) / length(extremes));
                center = max(center, get(obj.conf.h_slider, 'Min'));
                center = min(center, get(obj.conf.h_slider, 'Max'));
                obj.conf.slicendx = center;
                set(obj.conf.h_slider, 'Value', obj.conf.slicendx);
            end
        end

        function segmentGrain(obj, phaseID, grainID, threshold, center, border_mask)
        % GTGRAINSMANAGER/SETTHRESHOLD  Sets the threshold of a specific grain
        %
        % segmentGrain(obj, phaseID, grainID, threshold, center)

            if (~exist('border_mask' ,'var'))
                border_mask = [];
            end
            if (~exist('center', 'var'))
                center = [];
            end

            params = obj.getParams();
            segmenter = GtThreshold(params);
            gr = segmenter.singleGrainThresholdInFile(phaseID, grainID, threshold, center, border_mask);

            sample = obj.getSample();
            sample.phases{phaseID}.setBoundingBox(grainID, gr.segbb);

            % Should be kinda automatic, but we want to avoid troubles
            obj.setSample(sample);
        end

        function segmentCluster(obj, phaseID, clusterID, cluster_rec, threshold, seeds)
        % GTGRAINSMANAGER/SETTHRESHOLD  Sets the threshold of a specific grain
        %
        % segmentGrain(obj, phaseID, grainID, threshold, center)

            params = obj.getParams();
            segmenter = GtThreshold(params);
            gr = segmenter.singleGrainInClusterAutoThreshold(phaseID, clusterID, cluster_rec, threshold, seeds);

            sample = obj.getSample();
            sample.phases{phaseID}.setBoundingBox(grainID, gr.segbb);

            % Should be kinda automatic, but we want to avoid troubles
            obj.setSample(sample);
        end
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Public Callbacks - Interaction with GUI elements
    methods (Access = public)
        function displayDiffspots(obj, point)
        % GTGRAINSMANAGER/DISPLAYDIFFSPOTS  Callback which displays the selection
        % gui for the diffspots of a specific grain, which was selected for the
        % spcific voxel
        %
        % displayDiffspots(obj, point)

            if (~exist('point', 'var'))
                if (~isempty(obj.conf.clicked_point))
                    point = obj.conf.clicked_point;
                else
                    point = round(obj.get3DPixelInfo());
                end
            end

            [phaseNum, grainNum] = obj.getPhaseAndGrainOfPoint(point);
            obj.selectDiffSpots(phaseNum, grainNum);
        end

        function updateVisualisedPhase(obj, phaseid)
        % GTGRAINSMANAGER/UPDATEVISUALISEDPHASE  Updates the visualised picture,
        % depending on the selected phase
        %
        % updateVisualisedPhase(obj, phaseid)

            try
                if (~exist('phaseid', 'var'))
                    phaseid = get(obj.conf.h_phase_edit, 'String');
                    phaseid = str2double(phaseid);
                else
                    set(obj.conf.h_phase_edit, 'String', num2str(phaseid));
                end
                obj.updateStatus(sprintf('Visualised phase: %d', phaseid));
                obj.selectedPhase = phaseid;
                obj.updateVisualisedGrain(0);

            catch mexc
                gtPrintException(mexc, 'Couldn''t change Phase ID');
            end
        end

        function updateVisualisedGrain(obj, grainid)
        % GTGRAINSMANAGER/UPDATEVISUALISEDGRAIN  Updates the visualised picture,
        % depending on the selected phase and grain IDs
        %
        % updateVisualisedGrain(obj, grainid)

            try
                if (~exist('grainid', 'var'))
                    grainid = get(obj.conf.h_grain_edit, 'String');
                    grainid = str2double(grainid);
                else
                    set(obj.conf.h_grain_edit, 'String', num2str(grainid));
                end
                if (obj.selectedPhase <= 0)
                    set(obj.conf.h_grain_edit, 'Enable', 'off');
                    set(obj.conf.h_grain_butt, 'Enable', 'off');
                    set(obj.conf.h_grain_open, 'Enable', 'off');
                else
                    obj.updateStatus(sprintf('Visualised grain: %d', grainid));
                    set(obj.conf.h_grain_edit, 'Enable', 'on');
                    set(obj.conf.h_grain_butt, 'Enable', 'on');
                    if (grainid > 0)
                        obj.goToGrain(obj.selectedPhase, grainid);
                        % Enable open button
                        set(obj.conf.h_grain_open, 'Enable', 'on');
                    else
                        % Disable open button
                        set(obj.conf.h_grain_open, 'Enable', 'off');
                    end
                end
                obj.selectedGrain = grainid;
                obj.updateDisplay();
            catch mexc
                gtPrintException(mexc, 'Couldn''t change Grain ID');
            end
        end

        function displayVolumeInfo(obj)
        % GTGRAINSMANAGER/DISPLAYVOLUMEINFO  Displays Sample's and phases'
        % volume info
        %
        % displayVolumeInfo(obj)

            sample = obj.getSample();
            guiSample = GtGuiSample(sample);
            newSample = guiSample.join();
            if (~isempty(newSample))
                obj.setSample(newSample);
                obj.updateStatus('Updated sample information.');
            end
        end

        function displayGrainInfo(obj, phaseID, grainID)
        % GTGRAINSMANAGER/DISPLAYGRAININFO  Displays a gui about the selected
        % grain, with the possibility to inspect its properties and interact
        % with it
        %
        % displayGrainInfo(obj, phaseID, grainID)

            d = dialog('Name', 'Grain Info', 'Resize', 'on', ...
                       'WindowStyle', 'normal');
            obj.makeGrainPanel(d, phaseID, grainID);

            dlg_size = obj.getPixels(d, 'Position');
            dlg_size(3:4) = round(dlg_size(3:4) * 1.5);
            dlg_size(1:2) = round(dlg_size(1:2) * 0.6);
            obj.setPixels(d, 'Position', dlg_size);
        end

        function displaySelectedGrainInfo(obj)
        % GTGRAINSMANAGER/DISPLAYSELECTEDGRAININFO  Displays the selected
        % grain info if available
        %
        % displaySelectedGrainInfo(obj)

            try
                phaseID = get(obj.conf.h_phase_edit, 'String');
                phaseID = str2double(phaseID);
                grainID = get(obj.conf.h_grain_edit, 'String');
                grainID = str2double(grainID);

                obj.displayGrainInfo(phaseID, grainID);
            catch mexc
                gtPrintException(mexc, 'Not able to fetch grain information')
            end
        end

        function displayGrainInfoFromPoint(obj, point)
        % GTGRAINSMANAGER/DISPLAYGRAININFOFROMPOINT  Displays a gui about the
        % selected grain, with the possibility to inspect its properties and
        % interact with it
        %
        % displayGrainInfoFromPoint(obj, point)

            if (~exist('point', 'var'))
                point = obj.conf.clicked_point;
            end

            [infoStr, phaseID, grainID] = obj.getGrainInfo(point);
            disp(infoStr);

            if ((phaseID < 1) || (grainID < 1))
                d = helpdlg(infoStr, 'Grain Info');
                uiwait(d);
            else
                obj.displayGrainInfo(phaseID, grainID);
            end
        end

        function displayVoxelDetailsForGrains(obj, grainids)
        % GTGRAINSMANAGER/DISPLAYVOXELDETAILSFORGRAINS  Displays a gui which gives a
        % perspective of the given voxel: grains which claim it, et cetera
        %
        % displayVoxelDetailsForGrains(obj, grainids)

            obj.updateStatus(sprintf('Display details for grains %d', grainids{:}));
            drawnow;

            dlgBlock = [];
            dlgBlock.d = dialog('Name', 'Voxel Details', ...
                'Resize', 'on', 'WindowStyle', 'normal');
            dlg_size = obj.getPixels(dlgBlock.d, 'Position');
            dlg_size(3:4) = round(dlg_size(3:4) * 1.5);
            dlg_size(1:2) = round(dlg_size(1:2) * 0.6);
            obj.setPixels(dlgBlock.d, 'Position', dlg_size);

            dlgBlock.vert_boxes = uiextras.VBox('Parent', dlgBlock.d);
            dlgBlock.phases_boxes = {};

            visualize = false;

            sample = obj.getSample();
            % Check per each phase, which grains claim the voxel
            for phaseID = 1:numel(sample.phases)
                finalGrains{phaseID} = grainids{phaseID};
                if (~isempty(finalGrains{phaseID}))
                    dlgBlock.phases_boxes{phaseID} = obj.makePhasePanel(...
                        dlgBlock.vert_boxes, phaseID, [finalGrains{phaseID}]);
                    visualize = true;
                end
            end

            if (~visualize)
                delete(dlgBlock.d);
                dlgBlock.d = helpdlg('No grains found!', 'Voxel Details');
            end
            obj.updateStatus('Done.');
            drawnow;
        end

        function displayVoxelDetails(obj, point)
        % GTGRAINSMANAGER/DISPLAYVOXELDETAILS  Displays a gui which gives a
        % perspective of the given voxel: grains which claim it, et cetera
        %
        % displayVoxelDetails(obj, point)

            if (~exist('point', 'var'))
                point = obj.conf.clicked_point;
            end

            obj.updateStatus(sprintf('Display details of voxel (%d, %d, %d)..', ...
                                     round(point)));

            finalGrains = obj.getVoxelDetails(point);
            obj.displayVoxelDetailsForGrains(finalGrains);
            drawnow;
        end

        function toggleSelected(obj, src, phaseID, grainID)
        % GTGRAINSMANAGER/TOGGLESELECTED  Toggles the selected status of a grain
        %
        % toggleSelected(obj, src, phaseID, grainID)

            isSelected = (get(src, 'Value') == get(src, 'Max'));
            if (isSelected)
                set(src, 'String', 'Active');
            else
                set(src, 'String', 'Not Active');
            end
            sample = obj.getSample();
            sample.phases{phaseID}.setSelected(grainID, isSelected);
            obj.setSample(sample);
        end

        function changeGrainThreshold(obj, phaseID, grainID)
        % GTGRAINSMANAGER/CHANGEGRAINTHRESHOLD  Launches a gui object to adjust
        % the threshold of a specific grain
        %
        % changeGrainThreshold(obj, phaseID, grainID)

            params = obj.getParams();
            sample = obj.getSample();

            % well - I did not try to create / modify a cache model for
            % clusters - so for the moment we have to load them from
            % disk...
            clusterID = sample.phases{phaseID}.getClusterID(grainID);
            if ~isempty(clusterID)
                is_cluster = true;
                grain_ids = sample.phases{phaseID}.clusters(clusterID).included_ids;
                grain_str = sprintf('_%04d', grain_ids);
                cluster_rec = gtLoadClusterRec(phaseID, grain_ids);
            else
                is_cluster = false;
            end

            is_6D = strcmpi(params.rec.grains.algorithm(1:2), '6D');
            is_extended = sample.phases{phaseID}.getUseExtended(grainID);

            try
                if (is_cluster)
                    gr_seg = cluster_rec.SEG;
                elseif (is_6D && is_extended)
                    gr_seg = obj.cache.get('grain_ext_det', {phaseID, grainID}, 'SEG');
                else
                    gr_seg = obj.cache.get('grain_det', {phaseID, grainID}, 'SEG');
                end
                gr_thr = gr_seg.threshold;
            catch
                gr_thr = 0;
            end

            if (~is_6D)
                try
                    grain_vol3d = obj.cache.get('grain_det', {phaseID, grainID}, 'VOL3D');
                    grain_vol = grain_vol3d.intensity;
                catch
                    grain_vol = obj.cache.get('grain', {phaseID, grainID}, 'vol');
                end
            else
                if (is_extended)
                    grain_odf6d = obj.cache.get('grain_ext_det', {phaseID, grainID}, 'ODF6D');
                    grain_vol = grain_odf6d.intensity;
                elseif (is_cluster)
                    grain_odf6d = cluster_rec.ODF6D;
                    grain_vol = {grain_odf6d(:).intensity};
                else
                    grain_odf6d = obj.cache.get('grain_det', {phaseID, grainID}, 'ODF6D');
                    grain_vol = grain_odf6d.intensity;
                end
            end

            if (is_cluster)
                sum_vol = sum(cat(4, grain_vol),4);
                thr = GtGuiThresholdGrain.compareVolumes({sum_vol,grain_vol});
                if (isfield(params.rec.thresholding, 'mask_border_voxels') ...
                    && ~isempty(params.rec.thresholding.mask_border_voxels))
                    thr.changeBorderMask(params.rec.thresholding.mask_border_voxels);
                end
                for ii = 1 : numel(thr)
                    thr(ii).changeThreshold(gr_thr(ii));
                    thr(ii).setSegmentFunction(@(thresh, center, border)segmentCluster(obj, phaseID, clusterID, grain_vol{ii}, ii, thresh, center, border));
                end
            else
                thr = GtGuiThresholdGrain(grain_vol);
                thr.changeThreshold(gr_thr);
                if (isfield(params.rec.thresholding, 'mask_border_voxels') ...
                    && ~isempty(params.rec.thresholding.mask_border_voxels))
                    thr.changeBorderMask(params.rec.thresholding.mask_border_voxels);
                end
                thr.setSegmentFunction(@(thresh, center, border)segmentGrain(obj, phaseID, grainID, thresh, center, border));
            end

        end

        function forwardSimulateGrain(obj, phaseID, grainID)
        % GTGRAINSMANAGER/FORWARDSIMULATEGRAIN  Allows the forward
        % simulation, letting changng the fsim parameters first for the
        % current grain
        %
        % forwardSimulateGrain(obj, phaseID, grainID)

            % Fsim extra parameters
%             [vars_pars, vars_list] = gtAppFsimExtraPars();
%             vars_pars = gtModifyStructure(vars_pars, vars_list, 1, 'Extra options for Forward Simulation:');
%             if (vars_pars.variants)
%                 vars_pars = gtModifyStructure(vars_pars, vars_list, 2, 'Options for twin variant calculation:');
%             end

            parameters = obj.getParams();
            list = build_list_v2();
            fsim = gtModifyStructure(parameters.fsim, list.fsim, [], ...
                'Modify Forward Simulation parameters:');
            extra_params = struct2pv(fsim);

%             extra_params = [extra_params, struct2pv(vars_pars)];
            gtForwardSimulate_v2(grainID, grainID, obj.dataDir, phaseID, extra_params{:});
        end

        function viewDeselectedGrains(obj)
        % GTGRAINSMANAGER/VIEWDESELECTEDGRAINS  Launches a gui which let's you
        % inspect and interact with the deselected grains
        %
        % viewDeselectedGrains(obj)

            obj.updateStatus('Visualizing Deselected Grains..');
            drawnow;

            dlgBlock = [];
            dlgBlock.d = dialog('Name', 'Deselected Grains', 'Resize', 'on', ...
                                'WindowStyle', 'normal');
            dlgBlock.vert_boxes = uiextras.VBox('Parent', dlgBlock.d);
            dlgBlock.phases_boxes = {};

            visualize = false;
            sample = obj.getSample();
            % Check per each phase, which grains claim the voxel
            for phaseID = 1:length(sample.phases)
                deselected = sample.phases{phaseID}.getDeselectedGrains();
                if (~isempty(deselected))
                    dlgBlock.phases_boxes{phaseID} = obj.makePhasePanel(...
                        dlgBlock.vert_boxes, phaseID, deselected);
                    visualize = true;
                end
            end

            if (~visualize)
                delete(dlgBlock.d);
                dlgBlock.d = helpdlg('No deselected grains found!', ...
                                     'Deselected Grains');
            end
            obj.updateStatus('Done.');
            drawnow;
        end

        function launchReconSegGUI(obj)
        % GTGRAINSMANAGER/LAUNCHASSEMBLEGUI  Launches the assembling interface
        %
        % launchReconSegGUI(obj)

            obj.updateStatus('Launching Reconstructor..');
            GtGuiReconstructAndSegment();
            obj.updateStatus('Done.');
        end

        function launchAssembleGUI(obj, varargin)
        % GTGRAINSMANAGER/LAUNCHASSEMBLEGUI  Launches the assembling interface
        %
        % launchAssembleGUI(obj, varargin)

            obj.updateStatus('Launching Assembler..');
            varargin(end+1:end+2) = {'cache', obj.cache};
            varargin(end+1:end+2) = {'f_title',['Assemble GUI : ' gtGetLastDirName(obj.dataDir)]};
            varargin(end+1:end+2) = {'GMgui', obj};
            % get back the assembler parameters if already set
            if ~isempty(obj.assembler_params)
                assembler_params_list = struct2pv(obj.assembler_params);
                varargin(end+1:end+length(assembler_params_list)) = assembler_params_list;
            end
            GtGuiAssembleVol3D(varargin{:});
            obj.updateStatus('Done.');
        end

        function autoAssemble(obj)
        % GTGRAINSMANAGER/AUTOASSEMBLE  Does automatically the assembling
        %
        % autoAssemble(obj)

            assembler_params_list = struct2pv(obj.assembler_params);
            ass = GtAssembleVol3D(assembler_params_list{:});
            ass.assembleAllPhases();
            ass.assembleCompleteVolume();
            obj.assembler_params = ass.localPar;
            obj.setVolumeToLoad('complete');
            drawnow('expose')
        end

        function checkVolumesUpdate(obj)
        % GTGRAINSMANAGER/CHECKVOLUMESUPDATE  Checks wether the volumes are up to
        % date with the grain segmented volumes
        %
        % checkVolumesUpdate(obj)

            try
                sample = obj.getSample();
                % complete volume
                volumeFile = fullfile(obj.dataDir, sample.volumeFile);
                if (exist(volumeFile, 'file'))
                    volumeUpToDate = true;

                    fileInfo = dir(volumeFile);
                    volumeDateNum = fileInfo(1).datenum;
                else
                    volumeUpToDate = false;
                    volumeDateNum = 0;
                end
                % phase volumes
                phasesNum = length(sample.phases);
                for phaseID = 1:phasesNum
                    [upToDate, phaseDateNum] = obj.isPhaseUptodate(phaseID);
                    if (upToDate)
                        set(obj.conf.h_phases_labels{phaseID}, 'ForegroundColor', 'g');
                        if (volumeUpToDate && (phaseDateNum > volumeDateNum))
                            volumeUpToDate = false;
                        end
                    else
                        set(obj.conf.h_phases_labels{phaseID}, 'ForegroundColor', 'r');
                        volumeUpToDate = false;
                    end
                end
                % dilated volume
                if (volumeUpToDate)
                    set(obj.conf.h_volume_label, 'ForegroundColor', 'g');
                    dilVolFile = fullfile(obj.dataDir, sample.dilVolFile);
                    if (exist(dilVolFile, 'file'))
                        fileInfo = dir(dilVolFile);
                        dilvolDateNum = fileInfo(1).datenum;
                    else
                        dilvolDateNum = 0;
                    end
                    if (~isempty(fileInfo) && dilvolDateNum > volumeDateNum)
                        set(obj.conf.h_dil_volume_label, 'ForegroundColor', 'g');
                    else
                        set(obj.conf.h_dil_volume_label, 'ForegroundColor', 'r');
                    end
                else
                    set(obj.conf.h_volume_label, 'ForegroundColor', 'r');
                    set(obj.conf.h_dil_volume_label, 'ForegroundColor', 'r');
                end
            catch mexc
                gtPrintException(mexc, 'Failed to check volume updates.');
            end
        end

        function toggleAbsorptionVolume(obj)
        % GTGRAINSMANAGER/TOGGLEABSORPTIONVOLUME  Show/Hide the absorption
        %  volume as overlay
        %
        % toggleAbsorptionVolume(obj)

            try
                sample = obj.getSample();
                if (isempty(obj.conf.overlay))
                    if (isempty(sample.absVolFile))
                        error('GUI:GtGrainsManager:wrong_argument', ...
                                'Absorption volume file not specified!')
                    end
                    abs = obj.cache.get('volume_absorption', {}, 'abs');
                    obj.changeOverlay(abs);
                    newValueAbsVol = get(obj.conf.h_abs_butt, 'Max');
                else
                    obj.changeOverlay([]);
                    newValueAbsVol = get(obj.conf.h_abs_butt, 'Min');
                end
                set(obj.conf.h_abs_butt, 'Value', newValueAbsVol);
            catch mexc
                gtPrintException(mexc)
                d = errordlg(['Error: ' mexc.message], mexc.identifier, 'modal');
                uiwait(d);
            end
        end

        function setVolumeToLoad(obj, volume_str, varargin)
        % GTGRAINSMANAGER/SETVOLUMETOLOAD  Sets the volume to load and
        % update GUI controls for cmap and volume type
        %
        % setVolumeToLoad(obj, volume_str, varargin)

            if (ismember(volume_str, {'phase', 'complete', 'dilated'}))
                obj.conf.load_vol = volume_str;
            else
                error('GUI:wrong_argument', ...
                      'No such kind of volume "%s"', volume_str);
            end
            if isempty(varargin)
                reloadNow = true;
            else
                [~,ind]=ismember('reload',varargin(1:2:end));
                reloadNow = varargin{ind*2};
                varargin(ind*2-1:ind*2) = [];
            end
            maxRadio = get(obj.conf.h_c_vol_butt, 'Max');
            minRadio = get(obj.conf.h_c_vol_butt, 'Min');
            % reset volume type buttons
            set(obj.conf.h_d_vol_butt, 'Value', minRadio);
            set(obj.conf.h_c_vol_butt, 'Value', minRadio);
            set(obj.conf.h_p_vol_butt, 'Value', minRadio);
            switch (obj.conf.load_vol)
                case 'phase'
                    if strcmp(get(obj.conf.h_phase_edit, 'String'), '0')
                        set(obj.conf.h_phase_edit, 'String', '1')
                        obj.selectedPhase = 1;
                        drawnow();
                    end
                    set(obj.conf.h_p_vol_butt, 'Value', maxRadio);
                case 'complete'
                    set(obj.conf.h_c_vol_butt, 'Value', maxRadio);
                case 'dilated'
                    set(obj.conf.h_d_vol_butt, 'Value', maxRadio);
            end
            if (reloadNow)
                obj.reloadAssebledVolume(varargin{:});
            end
        end

        function showQueryDialog(obj)
        % GTGRAINSMANAGER/SHOWQUERYDIALOG
        %
        % showQueryDialog(obj)

            dlgBlock = [];
            dlgBlock.d = dialog('Name', 'Query Grains', 'Resize', 'on', ...
                                'WindowStyle', 'normal');
            dlgBlock.p = obj.produceQueryPanel(dlgBlock.d);
        end

        function launchQuery(obj, panelBlk, queryType)
        % GTGRAINSMANAGER/LAUNCHQUERY
        %
        % launchQuery(obj, panelBlk, queryType)

            % Let's delete what's already in the panel
            if (isstruct(panelBlk.result_panel) ...
                    && isfield(panelBlk.result_panel, 'Children'))
                prevRes = panelBlk.result_panel.Children;
                delete(prevRes);
                panelBlk.result_panel.Children = [];
                drawnow;
            end

            sample = obj.getSample();
            switch (queryType)
                case 'completeness'
                    value = str2double(get(panelBlk.h_query.completeness.edit, 'String'));
                    grainIDs = cell(0,length(sample.phases));
                    for phaseID = 1:length(sample.phases)
                        grainIDs{phaseID} = sample.phases{phaseID}.getGrainsWithCompletenessLessThan(value);
                    end
                case 'difspots'
                    value = str2double(get(panelBlk.h_query.difspots.edit, 'String'));
                    grainIDs = cell(0,length(sample.phases));
                    for phaseID = 1:length(sample.phases)
                        grainIDs = sample.phases{phaseID}.getGrainsWithDiffspotsNumLessThan(value);
                    end
            end

            panelBlk.res_vert_boxes = uiextras.VBox('Parent', panelBlk.result_panel);
            for phaseID = 1:length(sample.phases)
                if (~isempty(grainIDs{phaseID}))
                    obj.makePhasePanel(panelBlk.res_vert_boxes, phaseID, grainIDs{phaseID});
                end
            end
            drawnow;
        end

        function reconstructGrain(obj, phaseID, grainID)
        % GTGRAINSMANAGER/RECONSTRUCTGRAIN  Reconstructs a grain basing on
        % the selected diffspots
        %
        % reconstructGrain(obj, phaseID, grainID)

            parameters = obj.getParams();
            gtReconstructGrains(grainID, grainID, obj.dataDir, phaseID, parameters);
        end

        function viewReconstructedGrain(obj, phaseID, grainID)
        % GTGRAINSMANAGER/VIEWRECONSTRUCTEDGRAIN  Displays the grain.vol volume
        %
        % viewReconstructedGrain(obj, phaseID, grainID)

            params = obj.getParams();
            switch (upper(params.rec.grains.algorithm))
                case {'SIRT', '3DTV'}
                    grain_vol3d = obj.cache.get('grain_det', {phaseID, grainID}, 'VOL3D');
                    grain_vol = grain_vol3d.intensity;
                otherwise
                    grain_odf6d = obj.cache.get('grain_det', {phaseID, grainID}, 'ODF6D');
                    grain_vol = grain_odf6d.intensity;
            end
            GtVolView(grain_vol);
        end

        function viewSegmentedGrain(obj, phaseID, grainID)
        % GTGRAINSMANAGER/VIEWSEGMENTEDGRAIN  Displays the grain.seg volume
        %
        % viewSegmentedGrain(obj, phaseID, grainID)

            try
                gr_seg = obj.cache.get('grain_det', {phaseID, grainID}, 'SEG');
            catch
                gr_seg = obj.cache.get('grain', {phaseID, grainID}, {'seg'});
            end
            GtVolView(gr_seg.seg);
        end

        function displayForwardSim(obj, phaseID, grainID, varargin)
        % GTGRAINSMANAGER/DISPLAYFORWARDSIM  Displays forward simulation
        % resuts for the given grainID for phaseID
        %
        % displayForwardSim(obj, phaseID, grainID, varargin)

            try
                fields = {'id', 'phaseid', 'full', 'allblobs', 'proj', ...
                    'cands', 'flag', 'spotid', 'difspotID', ...
                    'bb_exp', 'om_exp', 'uv_exp'};
                    %'ondet', 'selected', 'included', ...
                grain =  obj.cache.get('grain', {phaseID, grainID}, fields);

                if isempty(varargin)
                    [vars_pars, vars_list] = gtAppFsimDefaultPars();
                    % parameters not to modify - they can be changed/removed here
                    ind = findValueIntoCell(fieldnames(vars_pars), {'fsimtype', 'f_title'});
                    vars_list(ind(:, 1), 4) = {2};

                    vars_pars = gtModifyStructure(vars_pars, vars_list, 1, 'Options for displaying the Forward Simulation:');
                    varargin = struct2pv(vars_pars);
                end
                gtShowFsim(grain, phaseID, varargin{:});
            catch mexc
                gtPrintException(mexc, 'Failed to show forward simulation results.');
            end
        end

        function copyVoxelMisorientationToClipboard(obj, point, verbose)
            if (~exist('verbose','var') || isempty(verbose))
                verbose = true;
            end
            if (~exist('point','var') || isempty(point))
                if (~isempty(obj.conf.clicked_point))
                    point = round(obj.conf.clicked_point);
                else
                    point = round(obj.get3DPixelInfo());
                end
            end
            dist = obj.getVoxelMisorientation(point);

            clipString = sprintf('(%d, %d, %d) [%g, %g, %g] deg', point, dist);
            if (verbose)
                disp(clipString)
            end
            clipboard('copy', clipString);
        end
    end % end methods public

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Internal methods - Addressable from derived classes
    methods (Access = protected)
        function initParams(obj, arguments)
        % GTGRAINSMANAGER/INITPARAMS  Preprocessing the arguments, before sending
        % them to GtVolView. It extracts the plugins from the arguments

            % Let's catch plugins
            pluginsCatcher = [];
            pluginsCatcher.plugins = {};
            [pluginsCatcher, viewer_args] = parse_pv_pairs(pluginsCatcher, arguments);

            obj.plugins = GtGrainsManager.checkMenuParams(pluginsCatcher.plugins);
            if ~isempty(obj.plugins)
                disp('Additional Plugins, specified by the user');
                disp(obj.plugins);
            end

            initParams@GtVolView(obj, viewer_args);
        end

        function initGui(obj)
        % GTGRAINSMANAGER/INITGUI Inherited method that builds the interface

            initGui@GtVolView(obj)
            % Let's disable zoom, while configuring
            zoomOp = zoom(obj.conf.h_figure);
            zoomState = get(zoomOp, 'Enable');
            zoom(obj.conf.h_figure, 'off');

            obj.conf.main_g_boxes = uiextras.HBox('Parent', obj.conf.currentParent);

            obj.conf.center_g_boxes = uiextras.VBox('Parent', obj.conf.main_g_boxes);
            obj.conf.h_viewer_panel = uipanel('BorderWidth', 4, ...
                                              'BorderType', 'line', ...
                                              'Parent', obj.conf.center_g_boxes);
            obj.conf.lower_g_boxes = uiextras.HBox('Parent', obj.conf.center_g_boxes);

            set(obj.conf.center_g_boxes, 'Sizes', [-1 20]);

            obj.conf.side_g_boxes = uiextras.VButtonBox('Parent', obj.conf.main_g_boxes);

            set(obj.conf.main_g_boxes, 'Sizes', [-1 140]);

            obj.conf.h_grain_status = uicontrol('Style', 'text', ...
                                             'Parent', obj.conf.lower_g_boxes, ...
                                             'HorizontalAlignment', 'left');

            % Phase selection
            obj.conf.h_phase_label = uicontrol('Style', 'text', ...
                                             'Parent', obj.conf.lower_g_boxes, ...
                                             'HorizontalAlignment', 'left');
            obj.conf.h_phase_edit = uicontrol('Style', 'edit', ...
                                              'Parent', obj.conf.lower_g_boxes);
            obj.conf.h_phase_butt = uicontrol('Style', 'pushbutton', ...
                                              'String', 'Apply', ...
                                              'Parent', obj.conf.lower_g_boxes);
            % End Phase selection

            % GrainSelection
            obj.conf.h_grain_label = uicontrol('Style', 'text', ...
                                             'Parent', obj.conf.lower_g_boxes, ...
                                             'HorizontalAlignment', 'left');
            obj.conf.h_grain_edit = uicontrol('Style', 'edit', ...
                                              'Parent', obj.conf.lower_g_boxes);
            obj.conf.h_grain_butt = uicontrol('Style', 'pushbutton', ...
                                              'String', 'Apply', ...
                                              'Parent', obj.conf.lower_g_boxes);
            obj.conf.h_grain_open = uicontrol('Style', 'pushbutton', ...
                                              'String', 'Open', ...
                                              'Parent', obj.conf.lower_g_boxes);
            % End Grain selection

            set(obj.conf.lower_g_boxes, 'Sizes', [-1, 120, 40, 50, 120, 40, 50, 50]);
            set(obj.conf.main_boxes, 'Parent', obj.conf.h_viewer_panel);

            % Reset back the zoom to the previous state (before the inclusion)
            set(zoomOp, 'Enable', zoomState);

            obj.conf.load_vol = 'complete';

            pixelInfoPattern = '(%d, %d, %d) Phase: %d, Grain %d';
            cbk = @(x, y)getPhaseAndGrainOfCurrentPointer(obj);
            obj.conf.h_pixelinfo.changePatternAndCbk(pixelInfoPattern, cbk);
        end

        function fillSidePanel(obj)
        % GTGRAINSMANAGER/FILLSIDEPANEL Fills the side panel with GUI elements
        % to interact with the data (visualisation, assembling, reloading...)

            % Reconstruct/segment grains button
            obj.conf.h_rec_t_butt = uicontrol('Style', 'pushbutton', ...
                                              'String', 'Reconstruct & Segment', ...
                                              'Parent', obj.conf.side_g_boxes);
            % Deselected grains button
            obj.conf.h_desel_butt = uicontrol('Style', 'pushbutton', ...
                                              'String', 'Deselected Grains', ...
                                              'Parent', obj.conf.side_g_boxes);

            % Spacer
            uicontrol('Parent', obj.conf.side_g_boxes, 'Enable', 'off', 'Visible', 'off', 'Tag', 'spacer');

            obj.conf.h_phases_labels = {};
            sample = obj.getSample();
            for phaseID = 1:length(sample.phases)
                obj.conf.h_phases_labels{phaseID} = ...
                            uicontrol('Style', 'text', ...
                                      'String', sprintf('Phase %02d', phaseID), ...
                                      'Parent', obj.conf.side_g_boxes, ...
                                      'HorizontalAlignment', 'left');
            end
            obj.conf.h_volume_label = uicontrol('Style', 'text', ...
                                              'HorizontalAlignment', 'left', ...
                                              'String', 'Complete Volume', ...
                                              'Parent', obj.conf.side_g_boxes);
            obj.conf.h_dil_volume_label = uicontrol('Style', 'text', ...
                                              'HorizontalAlignment', 'left', ...
                                              'String', 'Dilated Volume', ...
                                              'Parent', obj.conf.side_g_boxes);

            % Assemble grains button
            obj.conf.h_assem_butt = uicontrol('Style', 'pushbutton', ...
                                              'String', 'Assemble & D.', ...
                                              'Parent', obj.conf.side_g_boxes);
            obj.conf.h_assemauto_butt = uicontrol('Style', 'pushbutton', ...
                                              'String', 'Auto Assemble', ...
                                              'Parent', obj.conf.side_g_boxes);
            % Spacer
            uicontrol('Parent', obj.conf.side_g_boxes, 'Enable', 'off', 'Visible', 'off', 'Tag', 'spacer');

            obj.conf.h_p_vol_butt = uicontrol('Style', 'radiobutton', ...
                                              'String', 'Phase Volume', ...
                                              'Parent', obj.conf.side_g_boxes);

            obj.conf.h_c_vol_butt = uicontrol('Style', 'radiobutton', ...
                                              'String', 'Complete Volume', ...
                                              'Parent', obj.conf.side_g_boxes);

            obj.conf.h_d_vol_butt = uicontrol('Style', 'radiobutton', ...
                                              'String', 'Dilated Volume', ...
                                              'Parent', obj.conf.side_g_boxes);
            % Spacer
            uicontrol('Parent', obj.conf.side_g_boxes, 'Enable', 'off', 'Visible', 'off', 'Tag', 'spacer');

            obj.conf.h_reload_butt = uicontrol('Style', 'pushbutton', ...
                                              'String', 'Reload Volume', ...
                                              'Parent', obj.conf.side_g_boxes);

            obj.conf.h_abs_butt = uicontrol('Style', 'togglebutton', ...
                                              'String', 'Absorption Vol', ...
                                              'Parent', obj.conf.side_g_boxes);

            % Spacer
            uicontrol('Parent', obj.conf.side_g_boxes, 'Enable', 'off', 'Visible', 'off', 'Tag', 'spacer');

            obj.conf.h_query_butt = uicontrol('Style', 'pushbutton', ...
                                              'String', 'Query Metadata', ...
                                              'Parent', obj.conf.side_g_boxes);

            if (isempty(obj.conf.overlay))
                valueAbsVol = get(obj.conf.h_abs_butt, 'Min');
            else
                valueAbsVol = get(obj.conf.h_abs_butt, 'Max');
            end
            set(obj.conf.h_abs_butt, 'Value', valueAbsVol);

            buttonSize = get(obj.conf.side_g_boxes, 'ButtonSize');
            buttonSize(1) = 120;
            set(obj.conf.side_g_boxes, 'ButtonSize', buttonSize);
            set(obj.conf.side_g_boxes, 'VerticalAlignment', 'top');

            obj.checkVolumesUpdate();

            obj.addUICallback(obj.conf.h_rec_t_butt, ...
                              'Callback', ...
                              @(src, evt)launchReconSegGUI(obj), false);

            obj.addUICallback(obj.conf.h_desel_butt, ...
                              'Callback', ...
                              @(src, evt)viewDeselectedGrains(obj), false);

            obj.addUICallback(obj.conf.h_assem_butt, ...
                              'Callback', ...
                              @(src, evt)launchAssembleGUI(obj), false);
            obj.addUICallback(obj.conf.h_assemauto_butt, ...
                              'Callback', ...
                              @(src, evt)autoAssemble(obj), false);

            obj.addUICallback(obj.conf.h_p_vol_butt, ...
                              'Callback', ...
                              @(src, evt)setVolumeToLoad(obj, 'phase'), false);

            obj.addUICallback(obj.conf.h_c_vol_butt, ...
                              'Callback', ...
                              @(src, evt)setVolumeToLoad(obj, 'complete'), false);

            obj.addUICallback(obj.conf.h_d_vol_butt, ...
                              'Callback', ...
                              @(src, evt)setVolumeToLoad(obj, 'dilated'), false);
            obj.addUICallback(obj.conf.h_reload_butt, ...
                              'Callback', ...
                              @(src, evt)reloadAssebledVolume(obj), false);

            obj.addUICallback(obj.conf.h_abs_butt, ...
                              'Callback', ...
                              @(src, evt)toggleAbsorptionVolume(obj), false);

            obj.addUICallback(obj.conf.h_query_butt, ...
                              'Callback', ...
                              @(src, evt)showQueryDialog(obj), false);

            obj.addTimer('TimerFcn', @(src, evt)checkVolumesUpdate(obj), ...
                         'Period', 2, 'StartDelay', 1, ...
                         'ExecutionMode', 'fixedSpacing');

            % Bad bodge, but it also creates the internal field 'load_vol'
            obj.setVolumeToLoad('complete','reload',false);

            drawnow('expose');
        end

        function resetUiComponents(obj)
        % GTGRAINSMANAGER/RESETUICOMPONENTS Inherited method to reset the ui
        % components
            obj.updateStatus('Reset UI components...');

            set(obj.conf.h_phase_label, 'String', 'Phase ID (0 = All):');
            set(obj.conf.h_phase_edit, 'String', obj.selectedPhase);
            set(obj.conf.h_grain_label, 'String', 'Grain ID (0 = All):');
            set(obj.conf.h_grain_edit, 'String', obj.selectedGrain);
            if (obj.selectedPhase <= 0)
                set(obj.conf.h_grain_edit, 'Enable', 'off');
                set(obj.conf.h_grain_butt, 'Enable', 'off');
                set(obj.conf.h_grain_open, 'Enable', 'off');
            else
                set(obj.conf.h_grain_edit, 'Enable', 'on');
                set(obj.conf.h_grain_butt, 'Enable', 'on');
                if (obj.selectedGrain <= 0)
                    set(obj.conf.h_grain_open, 'Enable', 'off');
                else
                    set(obj.conf.h_grain_open, 'Enable', 'on');
                end
            end

            drawnow('expose');

            resetUiComponents@GtVolView(obj);
            obj.updateStatus('Done.');

        end

        function setVisualisedData(obj, image, vol, is_overlay)
        % GTGRAINSMANAGER/SETVISUALISEDDATA Prepares the slices to be visualised
        % in the axes, and then sets them

            slice = obj.loadSlice(vol);

            if (~is_overlay)
                if (~strcmpi(obj.conf.load_vol, 'phase'))
                    % If loading the full volume, let's manage the conflicts and
                    % selections
                    if (obj.selectedPhase > 0)
                        phaseIDs = obj.loadSlice(obj.cache.get('volume', {}, 'phases'));

                        slice(~(phaseIDs == obj.selectedPhase)) = 0;

                        if (obj.selectedGrain > 0)
                            selectedPhaseOffset = ( obj.separateGrainIDs ...
                                                    * (obj.selectedPhase - 1));
                            slice = obj.selectedGrain ...
                                    * (slice == (obj.selectedGrain + selectedPhaseOffset));

                            if (isempty(find(slice ~= 0, 1)))
                                % Empty slice! it means the grain has no volume
                                % or it was not selected! Let's show a contour!
                                slice = obj.getContourOfGrain(obj.selectedPhase, obj.selectedGrain);
                            end
                        elseif (obj.selectedGrain < 0)
                            grainIDs = obj.loadSlice(obj.cache.get('volume', {}, 'grains'));
                            slice(~(grainIDs == -1)) = 0;
                        end
                    elseif (obj.selectedPhase < 0)
                        phaseIDs = obj.loadSlice(obj.cache.get('volume', {}, 'phases'));
                        slice(~(phaseIDs == -1)) = 0;
                    end
                end
            else
                slice = (repmat(slice, [1, 1, 3]) - obj.conf.minOverlayVol) ...
                        / (obj.conf.maxOverlayVol - obj.conf.minOverlayVol);
            end
            set(image, 'cdata', slice);
        end

        function slice = getContourOfGrain(obj, phaseID, grainID)
        % GTGRAINSMANAGER/GETCONTOUROFGRAIN Function that gets border of grains,
        % when grain is not into assembled volume
            sample = obj.getSample();
            try
                gr_seg = obj.cache.get('grain_det', {phaseID, grainID}, 'SEG');
            catch
                gr_seg = obj.cache.get('grain', {phaseID, grainID}, {'seg'});
            end
            bbox = sample.phases{phaseID}.getBoundingBox(grainID);

            indx = obj.conf.slicendx - bbox(obj.conf.ortDim);
            if (indx >= 1 && indx <= bbox(obj.conf.ortDim + 3))
                switch (obj.conf.ortDim)
                    case 3 % equivalent to plane 'xy' or 'yx'
                        slice = zeros(obj.conf.dim(1), obj.conf.dim(2));
                        sliceVol = gr_seg.seg(:, :, indx);
                        slice = gtPlaceSubVolume(slice, sliceVol, ([bbox(1) bbox(2) 1]-1));
                    case 1 % equivalent to plane 'yz' or 'zy'
                        slice = zeros(obj.conf.dim(2), obj.conf.dim(3));
                        sliceVol = squeeze(gr_seg.seg(indx, :, :));
                        slice = gtPlaceSubVolume(slice, sliceVol, ([1 bbox(2) bbox(3)]-1));
                    case 2 % equivalent to plane 'zx' or 'xz'
                        slice = zeros(obj.conf.dim(1), obj.conf.dim(3));
                        sliceVol = squeeze(gr_seg.seg(:, indx, :));
                        slice = gtPlaceSubVolume(slice, sliceVol, ([bbox(1) 1 bbox(3)]-1));
                end

                padding1 = zeros(size(slice, 1), 1);
                zer1 = zeros(size(slice, 1)-1, 1);
                D1 = diag(ones(size(slice, 1)-1, 1));
                D1 = [D1, zer1] - [zer1, D1];
                padding2 = zeros(size(slice, 2), 1);
                zer2 = zeros(size(slice, 2)-1, 1);
                D2 = diag(ones(size(slice, 2)-1, 1));
                D2 = [D2, zer2] - [zer2, D2];

                slice = grainID * (([abs(D1 * slice); padding2'] + [abs(D2 * slice')', padding1]) ~= 0);
            else
                switch (obj.conf.ortDim)
                    case 3 % equivalent to plane 'xy' or 'yx'
                        slice = zeros(obj.conf.dim(1), obj.conf.dim(2));
                    case 1 % equivalent to plane 'yz' or 'zy'
                        slice = zeros(obj.conf.dim(2), obj.conf.dim(3));
                    case 2 % equivalent to plane 'zx' or 'xz'
                        slice = zeros(obj.conf.dim(1), obj.conf.dim(3));
                end
            end

            % Cope with matlab showing issue
            if (obj.conf.verDim > obj.conf.horDim)
                slice = permute(slice, [2, 1]);
            end
        end

        function addUICallbacks(obj)
        % GTGRAINSMANAGER/ADDUICALLBACKS Adds the callbacks to the GUI elements

            addUICallbacks@GtVolView(obj)

            obj.addUICallback(obj.conf.h_phase_butt, ...
                              'Callback', ...
                              @(src, evt)updateVisualisedPhase(obj), false);
            obj.addUICallback(obj.conf.h_phase_edit, ...
                              'Callback', ...
                              @(src, evt)updateVisualisedPhase(obj), false);

            obj.addUICallback(obj.conf.h_grain_butt, ...
                              'Callback', ...
                              @(src, evt)updateVisualisedGrain(obj), false);
            obj.addUICallback(obj.conf.h_grain_edit, ...
                              'Callback', ...
                              @(src, evt)updateVisualisedGrain(obj), false);
            obj.addUICallback(obj.conf.h_grain_open, ...
                              'Callback', ...
                              @(src, evt)displaySelectedGrainInfo(obj), false);
        end

        function updateStatus(obj, statusString)
        % GTGRAINSMANAGER/UPDATESTATUS Changes the displayed string in the
        % status bar

            set(obj.conf.h_grain_status, 'String', ['Status: ' statusString]);
        end

        function createAxesMenu(obj)
        % GTGRAINSMANAGER/CREATEAXESMENU Creates the context menu in the axes

            createAxesMenu@GtVolView(obj)
            % Built-in Items
            obj.conf.h_menu_items{end+1} = uimenu(obj.conf.h_context_menu, ...
                                           'Label', 'Voxel Details', ...
                                           'Callback', @(src,evt)displayVoxelDetails(obj));
            obj.conf.h_menu_items{end+1} = uimenu(obj.conf.h_context_menu, ...
                                           'Label', 'Grain Info', ...
                                           'Callback', @(src,evt)displayGrainInfoFromPoint(obj));
            obj.conf.h_menu_items{end+1} = uimenu(obj.conf.h_context_menu, ...
                                           'Label', 'Select Diffraction Spots', ...
                                           'Callback', @(src,evt)displayDiffspots(obj));
            obj.conf.h_menu_items{end+1} = uimenu(obj.conf.h_context_menu, ...
                                           'Label', 'Get Voxel Misorientation', ...
                                           'Callback', @(src,evt)copyVoxelMisorientationToClipboard(obj));
            % Extended Items (user plugins)
            for ii = 1:size(obj.plugins, 2)
                disp('adding menuitem');
                obj.conf.h_menu_items{end+1} = uimenu(obj.conf.h_context_menu, ...
                                               'Label', obj.plugins{1,ii}, ...
                                               'Callback', @(src,evt)obj.plugins{2,ii}(obj));
            end
        end

        function guiQuit(obj)
        % GTGRAINSMANAGER/GUIQUIT Inherited method to close the manager
            obj.updateStatus('Closing...');

            drawnow('expose');

            sample = obj.getSample();
            if (~isempty(sample))
                try
                    % Redundant but ensures we save it, in case of previous
                    % errors (if we are at least in the correct directory)
                    obj.setSample(sample);
                catch mexc
                    gtPrintException(mexc)
                    d = warndlg(mexc.message);
                    uiwait(d, 10);
                end
            end

            guiQuit@GtVolView(obj);
        end

        function setSample(obj, sample)
            fprintf('Saving to file..');
            old_sample = obj.getSample();
            if (~strcmpi(old_sample.volumeFile, sample.volumeFile))
                staticVolumeModel = fullfile(obj.dataDir, sample.volumeFile);
                obj.cache.modifyModelPath('volume', staticVolumeModel);
            end
            if (~strcmpi(old_sample.absVolFile, sample.absVolFile))
                staticAbsVolumeModel = fullfile(obj.dataDir, sample.volumeFile);
                obj.cache.modifyModelPath('volume_absorption', staticAbsVolumeModel);
            end
            if (~strcmpi(old_sample.dilVolFile, sample.dilVolFile))
                staticDilVolumeModel = fullfile(obj.dataDir, sample.dilVolFile);
                obj.cache.modifyModelPath('volume_dilated', staticDilVolumeModel);
            end
            obj.cache.set('sample', {}, sample, 'sample');
            obj.cache.flushObjectField('sample', {}, 'sample');
            fprintf(' Done.\n');
        end

        function sample = getSample(obj)
            sample = obj.cache.get('sample', {}, 'sample');
        end

        function params = getParams(obj)
            params = obj.cache.get('parameters', {}, 'parameters');
        end

        function phasePanel = makePhasePanel(obj, parent, phaseID, grainIDs, varargin)
        % GTGRAINSMANAGER/MAKEPHASEPANEL Creates the phase panel, which contains
        % all the grain panels of the selected grain for the calling action.
        %
        % phasePanel = makePhasePanel(obj, parent, phaseID, grainIDs, varargin)

            phasePanel = [];
            phasePanel.panel = uipanel('Parent', parent, varargin{:});
            phasePanel.scroll_boxes = uiextras.HBox('Parent', phasePanel.panel);

            phasePanel.phase_boxes = uiextras.VBox('Parent', phasePanel.scroll_boxes);
            phasePanel.scroll_bar = uicontrol('Parent', phasePanel.scroll_boxes, ...
                                              'Style', 'slider');

            set(phasePanel.scroll_boxes, 'Sizes', [-1 20]);

            numGrainsFound = length(grainIDs);
            phaseString = sprintf('Phase %02d: %04d grains', phaseID, numGrainsFound);
            phasePanel.phase_label = uicontrol('Parent', phasePanel.phase_boxes, ...
                                              'Style', 'text', ...
                                              'String', phaseString);
            fprintf('%s\n', phaseString);
            disp(grainIDs);

            phasePanel.grainPanels = {};
            for ii = 1:numGrainsFound
                grainID = grainIDs(ii);
                phasePanel.grainPanels{end+1} = obj.makeGrainPanel(...
                                    phasePanel.phase_boxes, phaseID, grainID);
            end

            sizesVec = ones(1, numGrainsFound+1) * (-1);
            sizesVec(1) = 20;
            set(phasePanel.phase_boxes, 'Sizes', sizesVec);

            % Record the size of a grain panel, for scrolling
            pixels = GtBaseGuiElem.getPixels(phasePanel.grainPanels{1}.descr, 'Extent');
            phasePanel.panelsSize = 20 + pixels(4);

            drawnow('expose');

            % Add resize callback
            set(phasePanel.panel, 'ResizeFcn', @(src, evt)resizePhasePanel(obj, phasePanel));
            set(phasePanel.scroll_bar, 'Callback', @(src, evt)updatePhasePanelSlider(obj, phasePanel));

            obj.resizePhasePanel(phasePanel);
        end

        function resizePhasePanel(~, phasePanel)
        % GTGRAINSMANAGER/RESIZEPHASEPANEL Manages the visibility of the grain
        % panels into a phase panel, in order to make them appear completely
        % even if the phase panel is not big enough to containe them all
        %
        % resizePhasePanel(~, phasePanel)

            pixels = GtBaseGuiElem.getPixels(phasePanel.panel, 'Position');
            numOfPanels = length(phasePanel.grainPanels);
            if (pixels(4) < (phasePanel.panelsSize * numOfPanels + 20))
                set(phasePanel.scroll_bar, 'Enable', 'on');
                set(phasePanel.scroll_bar, 'Visible', 'on');

                numVisible = max(int32(floor((pixels(4) - 20)/phasePanel.panelsSize)), 1);

                sizesVec = zeros(1, numOfPanels+1);
                sizesVec(1) = 20;
                sizesVec(2:numVisible+1) = -1;
                set(phasePanel.phase_boxes, 'Sizes', sizesVec);

                set(phasePanel.scroll_bar, 'Min', 1);
                set(phasePanel.scroll_bar, 'Max', numOfPanels);
                set(phasePanel.scroll_bar, 'Value', numOfPanels);
                sliderStep = 1/double(max(numOfPanels-1, 1));
                set(phasePanel.scroll_bar, 'SliderStep', [sliderStep sliderStep]);
            else
                set(phasePanel.scroll_bar, 'Enable', 'off');
                set(phasePanel.scroll_bar, 'Visible', 'off');

                sizesVec = ones(1, numOfPanels+1) * (-1);
                sizesVec(1) = 20;
                set(phasePanel.phase_boxes, 'Sizes', sizesVec);
            end

            drawnow('expose');
        end

        function updatePhasePanelSlider(~, phasePanel)
        % GTGRAINSMANAGER/UPDATEPHASEPANELSLIDER If triggered updates the
        % visibility of the grain panels, accordin to the position of the slider
        %
        % updatePhasePanelSlider(~, phasePanel)

            pixels = GtBaseGuiElem.getPixels(phasePanel.panel, 'Position');
            numOfPanels = length(phasePanel.grainPanels);
            numVisible = max(int32(floor((pixels(4) - 20)/phasePanel.panelsSize)), 1);
            base = int32(numOfPanels - get(phasePanel.scroll_bar, 'Value')+1);
            last = min(numVisible+base, numOfPanels+1);

            sizesVec = zeros(1, numOfPanels+1);
            sizesVec(1) = 20;
            sizesVec(1+base:last) = -1;
            set(phasePanel.phase_boxes, 'Sizes', sizesVec);
        end

        function grainPanel = makeGrainPanel(obj, parent, phaseID, grainID, varargin)
        % GTGRAINSMANAGER/MAKEGRAINPANEL Generates a panel which will fit into a
        % GUI and contain the information about a grain, and all the buttons to
        % interact with it.
        %
        % grainPanel = makeGrainPanel(obj, parent, phaseID, grainID, varargin)

            grainPanel = [];
            grainPanel.panel = uipanel('Parent', parent, varargin{:});
            grainPanel.binfo = uiextras.VBox('Parent', grainPanel.panel);
            grainPanel.boxes = uiextras.HBox('Parent', grainPanel.binfo);
            grainPanel.label = uicontrol('Parent', grainPanel.boxes, ...
                                         'Style', 'text', ...
                                         'HorizontalAlignment', 'left', ...
                                         'String', sprintf('Grain: %04d', grainID));
            grainPanel.select = uicontrol('Parent', grainPanel.boxes, ...
                                         'Style', 'togglebutton');
            grainPanel.difspt = uicontrol('Parent', grainPanel.boxes, ...
                                         'Style', 'pushbutton', ...
                                         'String', 'Diffspots');
            grainPanel.fwd2 = uicontrol('Parent', grainPanel.boxes, ...
                                         'Style', 'pushbutton', ...
                                         'String', 'ForwardSim');
            grainPanel.reconstr = uicontrol('Parent', grainPanel.boxes, ...
                                         'Style', 'pushbutton', ...
                                         'String', 'Reconstruct');
            grainPanel.thresh = uicontrol('Parent', grainPanel.boxes, ...
                                         'Style', 'pushbutton', ...
                                         'String', 'Threshold');
            grainPanel.seevol = uicontrol('Parent', grainPanel.boxes, ...
                                         'Style', 'pushbutton', ...
                                         'String', 'Vol View');
            grainPanel.see = uicontrol('Parent', grainPanel.boxes, ...
                                         'Style', 'pushbutton', ...
                                         'String', 'Seg View');
            grainPanel.fsim = uicontrol('Parent', grainPanel.boxes, ...
                                         'Style', 'pushbutton', ...
                                         'String', 'Show FSim');
            grainPanel.pdesc = uipanel('Parent', grainPanel.binfo);
            grainPanel.descr = uicontrol('Parent', grainPanel.pdesc, ...
                                         'Style', 'text', ...
                                         'Units', 'normalized', ...
                                         'Position', [0 0 1 1], ...
                                         'HorizontalAlignment', 'left', ...
                                         'String', obj.produceGrainInfo(phaseID, grainID));
            set(grainPanel.binfo, 'Sizes', [25 -1]);
            set(grainPanel.boxes, 'Sizes', [-1 80 80 95 95 95 80 100 85]);

            sample = obj.getSample();
            % Activeness
            isSelected = sample.phases{phaseID}.getSelected(grainID);
            if (isSelected)
                set(grainPanel.select, 'String', 'Active');
                set(grainPanel.select, 'Value', get(grainPanel.select, 'Max'))
            else
                set(grainPanel.select, 'String', 'Not Active');
                set(grainPanel.select, 'Value', get(grainPanel.select, 'Min'))
            end

            drawnow('expose');

            % Adding callbacks to buttons
            set(grainPanel.select,   'Callback', @(src,evt)toggleSelected(obj, src, phaseID, grainID));
            set(grainPanel.difspt,   'Callback', @(src,evt)selectDiffSpots(obj, phaseID, grainID));
            set(grainPanel.fwd2,     'Callback', @(src,evt)forwardSimulateGrain(obj, phaseID, grainID));
            set(grainPanel.reconstr, 'Callback', @(src,evt)reconstructGrain(obj, phaseID, grainID));
            set(grainPanel.thresh,   'Callback', @(src,evt)changeGrainThreshold(obj, phaseID, grainID));
            set(grainPanel.seevol,   'Callback', @(src,evt)viewReconstructedGrain(obj, phaseID, grainID));
            set(grainPanel.see,      'Callback', @(src,evt)viewSegmentedGrain(obj, phaseID, grainID));
            set(grainPanel.fsim,     'Callback', @(src,evt)displayForwardSim(obj, phaseID, grainID));
        end

        function infoStr = produceGrainInfo(obj, phaseNum, grainNum)
        % GTGRAINSMANAGER/PRODUCEGRAININFO Produces a string which contains the
        % info about a specific grain in a specific phase.

            sample = obj.getSample();
            phase = sample.phases{phaseNum};
            infoStr = [];
            infoStr = [infoStr sprintf('Phase ID:      %d\n', phaseNum)];
            infoStr = [infoStr sprintf('Grain ID:      %d\n', grainNum)];
            infoStr = [infoStr sprintf('Is selected:   %d\n', phase.getSelected(grainNum))];
            infoStr = [infoStr sprintf('Center:        (%f, %f, %f)\n', phase.getCenter(grainNum))];
            infoStr = [infoStr sprintf('Bounding box:  pos (%d, %d, %d), dims (%d, %d, %d)\n', phase.getBoundingBox(grainNum))];
            infoStr = [infoStr sprintf('Completeness:  %f\n', phase.getCompleteness(grainNum))];
            infoStr = [infoStr sprintf('R vector:      (%f, %f, %f)\n', phase.getR_vector(grainNum))];
            try
                gr = obj.cache.get('grain', {phaseNum, grainNum}, {'proj'});
                if (~isfield(gr.proj, 'included'))
                    gr_extra = obj.cache.get('grain', {phaseNum, grainNum}, {'included', 'selected'});
                    gr.proj.included = gr_extra.included;
                    gr.proj.selected = gr_extra.selected;
                end
                try
                    gr_seg = obj.cache.get('grain_det', {phaseNum, grainNum}, 'SEG');
                catch
                    gr_seg = obj.cache.get('grain', {phaseNum, grainNum}, {'threshold'});
                end
                infoStr = [infoStr sprintf('Included difspots:   %d\n', numel(gr.proj.included))];
                infoStr = [infoStr sprintf('Selected difspots:   %d\n', numel(find(gr.proj.selected)))];
                infoStr = [infoStr sprintf('Threshold:     %f\n', gr_seg.threshold)];
                % check for conflicts
                grain = gtCheckGrainConflicts(grainNum, phaseNum, [], [], false, false);
                if (isfield(grain, 'conf_table') && ~isempty(grain.conf_table))
                    numConflictDiffspots = numel(grain.conf_table.x);
                    uIDs = unique([grain.conf_table.data{:, 2}]);
                    [a, b] = histc([grain.conf_table.data{:, 2}], uIDs);
                    infoStr = [infoStr sprintf('Found %d difspots in common with other %d grains\n', numConflictDiffspots, numel(a))];
                    for jj = 1:numel(a)
                        ind = find(b == jj, 1);
                        gID = grain.conf_table.data{ind, 2};
                        infoStr = [infoStr sprintf('Conflict ID:   %d     Difspots in common:   %d\n', gID, a(jj))]; %#ok<AGROW>
                    end
                else
                    infoStr = [infoStr sprintf('No conflict information available\n')];
                end
            end
            infoStr = [infoStr sprintf('\n')];

            drawnow('expose');
        end

        function [upToDate, phaseDateNum] = isPhaseUptodate(obj, phaseID)
        % GTGRAINSMANAGER/ISPHASEUPTODATE Checks if the phase volume denoted by
        % phaseID is up to date in respect to the grains that compose it.
        %
        % [upToDate, phaseDateNum] = isPhaseUptodate(obj, phaseID)

            sample = obj.getSample();
            phase = sample.phases{phaseID};
            % Phase datetime
            volumeFile = regexp(phase.volumeFile, ':', 'split');
            volumeFile = fullfile(obj.dataDir, volumeFile{1});
            if (isempty(volumeFile) || ~exist(volumeFile, 'file'))
                upToDate = false;
                phaseDateNum = 0;
                return
            else
                upToDate = true;
                fileInfo = dir(volumeFile);
                phaseDateNum = fileInfo(1).datenum;
            end

            filePath = fullfile(obj.dataDir, '4_grains', ...
                                sprintf('phase_%02d', phaseID), 'grain_*.mat');
            fileInfo = dir(filePath);
            numGrains = phase.getNumberOfGrains();
            if (length(fileInfo) < numGrains)
                upToDate = false;
                return
            else
                grainDateNums = [fileInfo.datenum];
                if ( any(grainDateNums > phaseDateNum) )
                    upToDate = false;
                    return
                end
            end
        end

        function panelBlk = produceQueryPanel(obj, parent)
            panelBlk = [];
            panelBlk.d = uiextras.VBox('Parent', parent);
            panelBlk.query_boxes = uiextras.VBox('Parent', panelBlk.d);
            panelBlk.result_panel = uipanel('Parent', panelBlk.d);

            set(panelBlk.d, 'Sizes', [50, -1]);

            panelBlk.h_query = [];
            panelBlk.h_query.completeness = [];
            panelBlk.h_query.completeness.b = uiextras.HBox('Parent', panelBlk.query_boxes);
            panelBlk.h_query.difspots = [];
            panelBlk.h_query.difspots.b = uiextras.HBox('Parent', panelBlk.query_boxes);

            panelBlk.h_query.completeness.label = uicontrol('Parent', panelBlk.h_query.completeness.b, ...
                                            'Style', 'text', ...
                                            'HorizontalAlignment', 'left', ...
                                            'String', 'List the grains that have completeness lower than:');
            panelBlk.h_query.completeness.edit = uicontrol('Parent', panelBlk.h_query.completeness.b, ...
                                            'Style', 'edit');
            panelBlk.h_query.completeness.butt = uicontrol('Parent', panelBlk.h_query.completeness.b, ...
                                            'Style', 'pushbutton', ...
                                            'String', 'Query');
            set(panelBlk.h_query.completeness.b, 'Sizes', [-1 50 50]);

            panelBlk.h_query.difspots.label = uicontrol('Parent', panelBlk.h_query.difspots.b, ...
                                            'Style', 'text', ...
                                            'HorizontalAlignment', 'left', ...
                                            'String', 'List the grains that have fewer difspots than:');
            panelBlk.h_query.difspots.edit = uicontrol('Parent', panelBlk.h_query.difspots.b, ...
                                            'Style', 'edit');
            panelBlk.h_query.difspots.butt = uicontrol('Parent', panelBlk.h_query.difspots.b, ...
                                            'Style', 'pushbutton', ...
                                            'String', 'Query');
            set(panelBlk.h_query.difspots.b, 'Sizes', [-1 50 50]);

            set(panelBlk.h_query.completeness.butt, 'Callback', ...
                @(src, evt)launchQuery(obj, panelBlk, 'completeness'))
            set(panelBlk.h_query.completeness.edit, 'Callback', ...
                @(src, evt)launchQuery(obj, panelBlk, 'completeness'))
            set(panelBlk.h_query.difspots.butt, 'Callback', ...
                @(src, evt)launchQuery(obj, panelBlk, 'difspots'))
            set(panelBlk.h_query.difspots.edit, 'Callback', ...
                @(src, evt)launchQuery(obj, panelBlk, 'difspots'))

            drawnow('expose');
        end
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Utility Methods independent of the visualisation object
    methods (Static, Access = public)
        function [paramsOut, itemsNum] = checkMenuParams(params)
        % GTGRAINSMANAGER/CHECKMENUPARAMS Checks wether the menu plugins are
        % correctly passed or not
        %
        % [paramsOut, itemsNum] = checkMenuParams(params)

            if (~iscell(params))
                error('PARAMS:no_cell_array', ...
                      [ 'The context menu additional parametes are supposed' ...
                        ' to be in a cell array']);
            end
            if (mod(length(params), 2) == 1)
                error('PARAMS:wrong_parameters_number', ...
                      [ 'The context menu additional parametes are supposed' ...
                        ' to be in pairs: "menu string", @function_handle']);
            end

            itemsNum = uint64(length(params)/2);
            paramsOut = cell(2, itemsNum);
            paramsOut(1,1:end) = params(1:2:end);
            paramsOut(2,1:end) = params(2:2:end);

            for ii = 1:length(paramsOut(1,1:end))
                if (~ischar(paramsOut{1,ii}))
                    error('PARAMS:wrong_parameters_type', ...
                          [ 'The context menu additional parametes are ' ...
                            'supposed to have strings as menu item labes']);
                end
            end
            for ii = 1:length(paramsOut(2,1:end))
                if (~isa(paramsOut{2,ii}, 'function_handle'))
                    error('PARAMS:wrong_parameters_type', ...
                          [ 'The context menu additional parametes are ' ...
                            'supposed to function handles as callbacks']);
                end
            end
        end
    end
end
