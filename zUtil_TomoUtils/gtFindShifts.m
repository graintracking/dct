function shifts = gtFindShifts(im0, im1, varargin)
% GTFINDSHIFTS  Determines visually the relative shift between two 2D images.
%     -------------------------------------------------------------------------
%     shifts = gtFindShifts(im0, im1, varargin)
%
%     INPUT:
%       im0  = <2Dimage>  Reference image
%       im1  = <2Dimage>  Shifted image
%
%     OPTIONAL INPUT (varargin as a list of pairs, see parse_pv_pairs.m):
%       'roix'         = [min max]   sets the horizontal region of interest
%       'roiy'         = [min max]        "    vertical         "
%       'climt'        = [min max]   preset colour limits for toggle mode
%       'clims'        = [min max]              "             substraction mode
%       'clima'        = [min max]              "             addition mode
%       'precorrelate' = 'on'|'off'  correlate images before interaction
%       'block'        = 'on'|'off'  makes the window return the shifts on exit
%
%     OUTPUT:
%       shifts = [hs vs]  Horizontal / vertical shift to correlate the images
%       par    = <struct> HST parameter structure 
%
%     KEYS:
%       t      - toggle between the two images
%       a      - switch to image addition mode
%       s      - switch to image subtraction mode
%       z      - select graphically a region of interest (4x zoom by default)
%       r      - return to full image
%       Arrows - 1 pixel shift (10 pixels by pressing Shift)
%       1 / 2  - increase / decrease image contrast
%       3 / 4  - increase / decrease image brightness
%       Enter  - accept current value and return it
% 
%     SUB-FUNCTIONS:
%[sub]- initialiseShiftParams(im0, im1, varargin)
%[sub]- sfKeyPress
%[sub]- sfZoom
%[sub]- sfUpdateFigure
%[sub]- closeFigure()
%
%     Version 001 XX-12-2006 by WLudwig,  wolfgang.ludwig@esrf.fr

    % Initialization
    app = initialiseShiftParams(im0, im1, varargin);

    help(mfilename);
    sfUpdateFigure();

    % Main loop for console-blocking mode
    if strcmp(app.block,'on')
        % if user wants nothing to return until findshift is finished
        while 1
            if (app.quit), break; end
            pause(0.001);
            drawnow;
        end
    end

    % Copy and report results
    shifts = [app.vshift+app.voffset app.hshift+app.hoffset];
    fprintf('Shift is: [%.2fv %.2fh]\n', shifts(1), shifts(2));
    
    if strcmp(app.block,'on')
        set(app.window, 'KeyPressFcn', '');
        delete(app.window);
    end

    % End of function

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Private functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    function conf = initialiseShiftParams(im0, im1, varargin)
    % Let's initialise the configuration and create the gui
        
        % Global Parameters for the Imaging tool
        iptsetpref('ImtoolInitialMagnification', 'fit');

        conf = [];
        conf.mode = 'subtract';
        conf.togglestate = true;
        conf.vshift  = 0;
        conf.hshift  = 0;
        conf.voffset = 0;
        conf.hoffset = 0;
        conf.zoom  = 1;
        conf.clims = [-0.1 0.1];
        conf.climt = [0 1];
        conf.clima = [0 2];
        conf.orig0 = im0;
        conf.orig1 = im1;
        conf.roix  = [1 size(conf.orig0, 2)];
        conf.roiy  = [1 size(conf.orig0, 1)];
        conf.precorrelate = 'off';
        conf.block = 'off';
        conf.quit  = false;

        if nargin > 2
            if iscell(varargin{1})
                conf = parse_pv_pairs(conf, varargin{1});
            else
                conf = parse_pv_pairs(conf, varargin);
            end
        end

        % precrop the image 
        conf.im0 = conf.orig0(conf.roiy(1):conf.roiy(2), conf.roix(1):conf.roix(2));
        conf.im1 = conf.orig1(conf.roiy(1):conf.roiy(2), conf.roix(1):conf.roix(2));

        % then pre-correlate if requested
        if strcmpi(conf.precorrelate,'yes') ...
            || strcmpi(conf.precorrelate,'on') ...
            || (islogical(conf.precorrelate) && conf.precorrelate)

            fprintf('Pre-correlating images\n');

            tmp = correlate(conf.im0, conf.im1);
            conf.voffset = tmp(1);
            conf.hoffset = tmp(2);
        end

        % Let's create the figure
        conf.window = figure( ...
                'Name', 'Manual correlation to find rotation axis...', ...
                'NumberTitle', 'off', ...
                'MenuBar', 'none', ...
                'Toolbar', 'none'); % create a new figure

        % Setting axes to display the image
        conf.axes1 = axes( 'Parent', conf.window, ...
                           'ActivePositionProperty', 'OuterPosition');
        colormap(conf.axes1, 'gray');

        % Callbacks
        set(conf.window, 'KeyPressFcn', @sfKeyPress);
        set(conf.window, 'CloseRequestFcn', @closeFigure);
    end

    function sfKeyPress(~, evt)
    % Usage guide, from matlab doc: 
    %
    % -- KeyPressFcn Event Structure.
    % When you specify the callback as a function handle, MATLAB passes to it a
    % structure to containing the following fields.
    %
    % Field     | Contents
    % ----------|------------------
    % Character | The character displayed as a result of the pressing the
    %           | key(s), which can be empty or unprintable
    % Key       | The key being pressed, identified by the lowercase label on
    %           | key or a descriptive string
    % Modifier  | A cell array containing the names of one or more modifier
    %           | keys being pressed (i.e., control, alt, shift).
    %           | On Macintosh computers, it contains 'command' when pressing
    %           | the command modifier key.
        switch (evt.Key)
            case 'uparrow'
                if strcmp(evt.Modifier, 'shift')
                    app.vshift = app.vshift+10/app.zoom;
                else
                    app.vshift = app.vshift+1/app.zoom;
                end
                sfUpdateFigure();

            case 'downarrow'
                if strcmp(evt.Modifier, 'shift')
                    app.vshift = app.vshift-10/app.zoom;
                else
                    app.vshift = app.vshift-1/app.zoom;
                end
                sfUpdateFigure();

            case 'rightarrow'
                if strcmp(evt.Modifier, 'shift')
                    app.hshift = app.hshift+10/app.zoom;
                else
                    app.hshift = app.hshift+1/app.zoom;
                end
                sfUpdateFigure();

            case 'leftarrow'
                if strcmp(evt.Modifier, 'shift')
                    app.hshift = app.hshift-10/app.zoom;
                else
                    app.hshift = app.hshift-1/app.zoom;
                end
                sfUpdateFigure();

            case 'return'
                %exit interactive mode and return current shift
                closeFigure(0,0);
                
                if (~strcmp(app.block,'on'))
                    shifts = [app.vshift+app.voffset app.hshift+app.hoffset];
                    % Nasty global variable set - used in console-non-blocking mode
                    assignin('base', 'shift', shifts);
                    %delete(app.window);
                end

            case 'a'
                disp('Switching to addition mode');
                app.mode = 'add';
                sfUpdateFigure();

            case '1'
                fprintf('Increasing contrast: ');
                switch app.mode
                    case 'subtract'
                      app.clims = app.clims*0.9;
                      fprintf('clims is [%f %f]\n', app.clims(1), app.clims(2));
                    case 'toggle'
                      app.climt = app.climt*0.95;
                      fprintf('climt is [%f %f]\n', app.climt(1), app.climt(2));
                    case 'add'
                      app.clima = app.clima*0.9;
                      fprintf('clima is [%f %f]\n', app.clima(1), app.clima(2));
                end
                sfUpdateFigure();
            case '2'
                fprintf('Decreasing contrast: ');
                switch app.mode
                    case 'subtract'
                        app.clims = app.clims*1.1;
                        fprintf('clims is [%f %f]\n', app.clims(1), app.clims(2));
                    case 'toggle'
                        app.climt = app.climt*1.05;
                        fprintf('climt is [%f %f]\n', app.climt(1), app.climt(2));
                    case 'add'
                        app.clima = app.clima*1.1;
                        fprintf('clima is [%f %f]\n', app.clima(1), app.clima(2));
                end
                sfUpdateFigure();
            case '3'
                fprintf('Colormap brighter: ');
                switch app.mode
                    case 'subtract'
                      app.clims = app.clims-max(app.clims)*0.1;
                      fprintf('clims is [%f %f]\n', app.clims(1), app.clims(2));
                    case 'toggle'
                      app.climt = app.climt-mean(app.climt)*0.05;
                      fprintf('climt is [%f %f]\n', app.climt(1), app.climt(2));
                    case 'add'
                      app.clima = app.clima-mean(app.clima)*0.1;
                      fprintf('clima is [%f %f]\n', app.clima(1), app.clima(2));
                end
                sfUpdateFigure();
            case '4'
                fprintf('Colormap darker: ');
                switch app.mode
                    case 'subtract'
                      app.clims = app.clims+max(app.clims)*0.1;
                      fprintf('clims is [%f %f]\n', app.clims(1), app.clims(2));
                    case 'toggle'
                      app.climt = app.climt+mean(app.climt)*0.05;
                      fprintf('climt is [%f %f]\n', app.climt(1), app.climt(2));
                    case 'add'
                      app.clima = app.clima+mean(app.clima)*0.1;
                      fprintf('clima is [%f %f]\n', app.clima(1), app.clima(2));
                end
                sfUpdateFigure();
            case 's'
                disp('Switching to subtraction mode');
                app.mode = 'subtract';
                app.clim = app.clims;
                sfUpdateFigure();
            case 'z'
                disp('Select region of interest');
                title('Select region of interest');
                [~, r]=imcrop;
                disp('Zooming...');
                title('Zooming...');
                r=round(r);
                sfZoom(r);
                sfUpdateFigure();
            case 'r'
                disp('Reset full image');
                app.zoom = 1;
                app.im0  = app.orig0;
                app.im1  = app.orig1;
                app.vshift  = round(app.vshift+app.voffset);
                app.hshift  = round(app.hshift+app.hoffset);
                app.voffset = 0;
                app.hoffset = 0;
                sfUpdateFigure();
            case 't'
                app.mode = 'toggle';
                app.togglestate = not(app.togglestate);
                app.clim = app.climt;
                sfUpdateFigure();
            case 'h'
                help(mfilename);
        end
    end

    function sfZoom(roi)
        app.orig0 = app.im0;
        app.orig1 = app.im1;
        % ak comment - I've fixed this at zero for big ROIs
        app.zoom = 1;
        app.im0 = imresize(imcrop(app.im0,roi),app.zoom,'bicubic');
        app.im1 = imresize(imcrop(app.im1,[roi(1)-app.hshift, roi(2)-app.vshift, roi(3), roi(4)]),app.zoom,'bicubic');
        app.voffset = app.vshift;
        app.hoffset = app.hshift;
        app.vshift = 0;
        app.hshift = 0;
    end

    function sfUpdateFigure()
        im_tmp = circshift(app.im1, [app.vshift*app.zoom app.hshift*app.zoom]);
        l_shifts = [app.vshift+app.voffset app.hshift+app.hoffset];
        switch app.mode
            case 'add'
                img = app.im0+im_tmp;
                lims = app.clima;
                title_str = sprintf('vertical shift: %.2f\t horizontal shift: %.2f', l_shifts(1), l_shifts(2));
            case 'subtract'
                img = app.im0-im_tmp;
                lims = app.clims;
                title_str = sprintf('vertical shift: %.2f\t horizontal shift: %.2f', l_shifts(1), l_shifts(2));
            case 'toggle'
                if (app.togglestate)
                    img = app.im0;
                    lims = app.climt;
                    title_str = 'first image (fixed position)';
                else
                    img = im_tmp;
                    lims = app.climt;
                    title_str = sprintf('second image: vertical shift: %.2f\t horizontal shift: %.2f', l_shifts(1), l_shifts(2));
                end
        end
        imagesc(img, 'Parent', app.axes1, lims);
        axis(app.axes1, 'image');
        title(app.axes1, title_str);
    end

    function closeFigure(~, ~)
        if strcmp(app.block,'on')
            app.quit = true;
        else
            delete(app.window);
        end
    end
end
