function clims = gtSetColourLimits(vol, clims, verbose)
% GTSETCOLOURLIMITS  Histogram based colour limits calculator for 2D/3D images.
%     clims = gtSetColourLimits(vol, clims, [verbose])
%     ------------------------------------------------
%     INPUT:
%       vol    = <2Dimage>  2D/3D images volume data to work on
%                <3Dimage>
%       clims  = <float>    percentile used to set the color limits as follows:
%                           clims=2      set limits to 2nd and 98th percentile
%                           clims=[5 80] set limits to gray levels of 5 and 80
%                           default is 2, i.e. 2nd and 98th percentile
%
%     OPTIONAL INPUT:
%       verbose = <bool>    verbose mode, default is false
%
%     OUTPUT:
%       clims   = <float>   color limits determined by the macro
% 
%     Version 003 10-02-2012 by NVigano,  nicola.vigano@esrf.fr
%       Imported and modified
%
%     Version 002 XX-05-2007 by GJohnson
%       Changed from [x y] being percentiles (not useful) to exact graylevels
%       which is slightly more intuitive.
%
%     Version 001 XX-11-2006 by GJohnson

    if ( ~exist('verbose', 'var') )
        verbose = false;
    end

    switch length(clims)
        case 0
            if (verbose), disp('Using 2% colour limits.'); end
            clims = gtAutolim(vol, 0.02);
        case 1
            if (clims == 0)
                clims = [min(vol(:)) max(vol(:))];
                if (verbose), fprintf('Using full range of data [%2.1f, %2.1f].\n', clims); end
            else
                if (verbose), fprintf('Calculating %2.1f%% colour limits.\n', clims); end
                clims = gtAutolim(vol, clims/100);
            end
        otherwise
            if (verbose), fprintf('Using [%2.1f, %2.1f] as colour limits.\n', clims); end
    end

end % end of function
