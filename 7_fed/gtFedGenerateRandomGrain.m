function grain = gtFedGenerateRandomGrain(grain,vert,pars,varargin)
% GTFEDGENERATERANDOMGRAIN  Forward simulate diffraction spots
%     grain = GTFEDGENERATERANDOMGRAIN(grain,vert,pars,varargin)
%     ----------------------------------------------------------
%       Input and output arguments description, variable type (in <>) and default value (in {}) are written below.
%
%
%     INPUT:
%
%            grain = grain of interest (<struct>)
%             vert = difspots image output from "gtPlaceDifspotinFull" (<double MxM>)
%                    M is the size of the image (typically 2048)
%             pars = parameters file (<struct> {parameters.mat})
%         varargin = optional arguments list given by pairs
%
%
%     OPTIONAL INPUT (as a list of pairs):
%
%       Reflections = number N of first reflections to consider (<double Nx1> {[]})
%       Beamchroma = beam chromaticity (<char 1x4 > {'mono'}, 'poly')
%       Beamenergymin = minimum beam energy (in keV) (<double> {5})
%       Beamenergymax = maximum beam energy (in keV) (<double> {25})
%       showfigure = show or not the figure (<logical> {0})
%            color = show or not the prediction spot positions coloured as
%                    omega (<logical> {0})
%           Dlimit = maximum strain value (<double> {0})
%             Gref = reference full deformation gradient tensor (<double 3x3 {zeros(3)})
%            debug = debug mode on/off (<logical> {0})
%             axis = plot axis, image reference and beam direction in the
%                    image (<logical> {0})
%             save = save or not the grain (<logical> {0})
%       Markersize = marker size (<double> {15})
%        Grayscale = scale of grays (<double 1x2> {[-300 500]})
%
%
%     OUTPUT:
%
%       grain = grain of interest (<struct>) 
%              added fields:
%                - randomID <int32>
%                - pllab <double Nx3>
%                - hklsp <double Nx3>
%                - dvec <double Nx3>
%                - allblobs <struct>
%
%              updated fields:
%                - thetatype <double Nx1>
%                - hkl <double Nx3>
%                - pl <double Nx3>
%                - theta <double Nx1> 
%                - eta <double Nx1>   
%                - omega <double Nx1> 
%
%
% Version 004 23-04-2012 by PReischig
%     Adapted to use CrystHKL2Cartesian
% Version 003 29-11-2011 by LNervo
%
% Modified by LNervo on May 13, 2011
%     Add comments
% Modified by LNervo on May 17, 2011
%     Add Color flag
% Modified by LNervo on June 15, 2011
%     Remove unneeded staff
%


%% read optional arguments

app.color=0;
app.showfigure=0;
app.Reflections=[];
app.Beamchroma='mono';
app.Beamenergymin=0;
app.Beamenergymax=0;
app.Dlimit=0;
app.Gref=zeros(3);
app.debug=0;
app.axis=0;
app.save=0;
app.Markersize=15;
app.Grayscale=[-300 500];

app=parse_pv_pairs(app,varargin);

disp('Initial settings')
disp(app)

if nargin < 1
    disp('Usage:  grain = gtFedGenerateRandomGrain(grain,vert,pars,varargin)')
    disp('                grain = grain{grainid}')
    disp('                 vert = image with spots from db')
    disp('                 pars = parameters file')
    return
end

if ~exist('vert','var') || isempty(vert)
    vert=zeros(2048);
end

if ~exist('pars','var') || isempty(pars)
    pars=load('parameters.mat');
end
parameters=pars;

% Assign a random grain ID, so that grain can be identified later:
grain.randomID = int64(rand*1e10);

if app.Dlimit~=0 
    if app.Gref==zeros(3)
        app.Gref=(ones(3)-rand(3)*2)*app.Dlimit;
    end
    disp('Random strain tensor:')
    Eref=(app.Gref+app.Gref')/2;
    disp(Eref)

    disp('Random rotation tensor:')
    Rref=(app.Gref-app.Gref')/2;
    disp(Rref)
end

if app.debug
    dbstop if error
end

% Rodrigues vector (grain orientation)
Rvec=grain.R_vector;
disp(['grain.center: ' num2str(grain.center)]);


acq   = parameters.acq;
cryst = parameters.cryst;
geo   = parameters.geo;
labgeo = parameters.labgeo;

beamdir    = geo.beamdir0';
rotdir     = geo.rotdir0';
spacegroup = cryst.spacegroup;
crystal    = cryst.crystal_system;% string

if strcmp(app.Beamchroma,'mono')
    lambda = gtConvEnergyToWavelength(acq.energy);
    if rotdir == [0; 0; 0]
        error('Rot. axis direction set incorrect in parameters.')
    end
elseif strcmp(app.Beamchroma,'poly')
    lambdamax = gtConvEnergyToWavelength(app.Beamenergymin);
    lambdamin = gtConvEnergyToWavelength(app.Beamenergymax);
    lambda = 0;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Define grain
% Load all unique reflections on the detector from parameters file
% info = parameters.hkldet;
% hkltypes = info.reflections;
% 
% %fprintf('Number of families found: %d\n',length(hkltypes))
% %disp(['hkltypes: ' num2str(size(hkltypes)) ])
% 
% if isempty(app.Reflections)
%     hkltypes_used = hkltypes;
% else
%     hkltypes_used = hkltypes(app.Reflections,:);
% end
% 
% reflections = parameters.cryst;

hklsp  = parameters.cryst.hklsp;
d0     = parameters.cryst.dsp;
thtype = parameters.cryst.thtype; 
hklt   = parameters.cryst.hkl;

Bmat = gtCrystHKL2CartesianMatrix(parameters.cryst.latticepar);

disp('Getting normalized cartesian coordinates from lattice coordinates...');
for ii = 1:size(hklsp,2)  % changed from size(hklsp, 1)
    pl0(ii,:) = gtCrystHKL2Cartesian(hklsp(:, ii), Bmat);
end
disp('...done!');

% Compute orientation matrix g
g = gtMathsRod2OriMat(Rvec);

% Plane normals in SAMPLE coordinate system
pl_g = gtVectorCryst2Lab(pl0, g);

%disp(['pl_g: ' num2str(size(pl_g))])

% add lines to save results

%% new added variables
% What are these variables?

grain.pl    = [];
grain.pllab = [];
grain.hkl   = [];
grain.hklsp = [];
grain.thetatype = [];
grain.theta = [];
grain.eta   = [];
grain.omega = [];
grain.dvec  = [];
grain.srot  = [];

grain.allblobs.pl    = [];
grain.allblobs.pllab = [];
grain.allblobs.hkl   = [];
grain.allblobs.hklsp = [];
grain.allblobs.thetatype = [];
grain.allblobs.theta = [];
grain.allblobs.eta   = [];
grain.allblobs.omega = [];
grain.allblobs.dvec  = [];
grain.allblobs.uv    = [];
grain.allblobs.detector.uvw   = [];
grain.allblobs.srot  = [];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  Impose strain on grain

for ii = 1:size(pl_g, 1)

    if isfield(app,'Gref')
        plorig = pl_g(ii, :)';
        [pldef,drel] = gtFedStrainedPlaneNormal(plorig,app.Gref); % unit vector
        % new d-spacing
        dnew = drel*d0(ii);
        % deformed plane normals
        pl_gd(ii,:) = pldef;
        % d-spacings in deformed state
        d0_gd(ii,:) = dnew;
        % thetas in deformed state
        th_gd(ii,:) = asind(lambda/2/d0_gd(ii,:));
    else
        pl_gd(ii,:) = pl_g(ii,:)';
        th_gd(ii,:) = asind(lambda/2/d0(ii)); %this is theta, not twotheta
    end

end
%disp(['pl_gd: ' num2str(size(pl_gd))])
%disp(['th_gd: ' num2str(size(th_gd))])


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Detector geometry
disp('Getting the detector geometry...')

detdiru = geo.detdiru0';
detdirv = geo.detdirv0';
Qdet = gtFedDetectorProjectionTensor(detdiru,detdirv,1,1);
tn = cross(detdiru,detdirv);
detpos = (geo.detpos0./acq.pixelsize)';
%uvorig = [acq.rotx; acq.ydet/2];
uvorig = [geo.detucen0, geo.detvcen0]';
csam = grain.center';
omstep = 180/acq.nproj;

disp('...done!')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Setup figure

if app.showfigure

    figure
    imshow(vert,app.Grayscale);
    hold on

      if app.axis
          detusize=acq.xdet;
          detvsize=acq.ydet;
    
          % Image frame
          plot([0 detusize+1],[0 0],'k')
          plot([0 detusize+1],[detvsize+1 detvsize+1],'k')
          plot([0 0],[0 detvsize+1],'k')
          plot([detusize+1 detusize+1],[0 detvsize+1],'k')
    
          % Midlines
          plot([0 detusize+1],[detvsize/2+0.5 detvsize/2+0.5],'-.k')
          plot([detusize/2+0.5 detusize/2+0.5],[0 detvsize+1],'-.k')
    
          % Set figure propeties
          axis equal
          set(gca,'YDir','reverse')
          set(gca,'Position',[0.05 0.05 0.95 0.9])
          xlim([-detusize*0.1 detusize*1.1])
          ylim([-detvsize*0.1 detvsize*1.1])
          xlabel('U direction')
          ylabel('V direction')
    
%           % Colormap for indicating omega values
%           cmap = jet(1001);
%           colormap(cmap)
%           colorbar
%           caxis([0 360])
%     
%           % Function handle to associate RGB value to omega
%           omtoRGB = @(om) cmap(round((om)/(360)*1000+1),:);
    
          % Beam direction in image
          beamuv = Qdet*beamdir;
          % Arrow in figure indicating beam direction
          quiver(uvorig(1),uvorig(2),beamuv(1),beamuv(2),1000,'-k','Linewidth',3)
    
          % Rotation axis direction in image
          rotuv = Qdet*rotdir;
          % Arrow in figure indicating rotation axis direction
          quiver(uvorig(1),uvorig(2),rotuv(1),rotuv(2),1000,'-.k','Linewidth',3)
      end
    impixelinfo
end % showfigure



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Compute omega and eta

rotcomp = gtFedRotationMatrixComp(rotdir);

if strcmp(app.Beamchroma,'mono')  % monochromatic beam

    for ii = 1:size(pl_gd,1)
        hkl(ii, :) = hklt(:,thtype(ii))'; %changed here
        pl    = pl_gd(ii, :)';
        sinth = sind(th_gd(ii));
        % Four omegas of the plane normal (1-3 and 2-4 are the two Friedel
        % pairs):
        % pls is plus or minus pl that's needed for the derivatives later
        [om,pllab,pls] = gtFedPredictOmega(pl,sinth,beamdir,rotdir,rotcomp);

        % If reflection occurs
        if ~isempty(om)

            % Rotation tensors from omegas (one for each of the two Friedel pairs)
            S1 = gtFedRotationTensor(om(1),rotcomp);
            S2 = gtFedRotationTensor(om(2),rotcomp);
            S3 = gtFedRotationTensor(om(3),rotcomp);
            S4 = gtFedRotationTensor(om(4),rotcomp);

            % Diffraction vectors
            d1 = gtFedPredictDiffVec(pllab(:,1),sinth,beamdir); % takes coloumn vectors
            d2 = gtFedPredictDiffVec(pllab(:,2),sinth,beamdir);
            d3 = gtFedPredictDiffVec(pllab(:,3),sinth,beamdir);
            d4 = gtFedPredictDiffVec(pllab(:,4),sinth,beamdir);

            
            
            % Normalize vectors
            d1 = d1/norm(d1);
            d2 = d2/norm(d2);
            d3 = d3/norm(d3);
            d4 = d4/norm(d4);

            % Following the convention of the mathcing output, the omega value
            % smaller than 180deg (spot "a") is used in the pair data.
            % The two Friedel pairs are 1a-1b and 2a-2b.

            if om(1) < om(3)
                om1a  = om(1);
                s1a   = S1;
                s1b   = S3;
                om1b  = om(3);
                pl1a  = pls(:,1)';
                pl1b  = pls(:,3)';
                pllab1a = pllab(:,1)';
                pllab1b = pllab(:,3)';
                d1a   = d1';
                d1b   = d3';
            else
                om1a  = om(3);
                s1a   = S3;
                s1b   = S1;
                om1b  = om(1);
                pl1a  = pls(:,3)';
                pl1b  = pls(:,1)';
                pllab1a = pllab(:,3)';
                pllab1b = pllab(:,1)';
                d1a   = d3';
                d1b   = d1';
            end

            if om(2) < om(4)
                om2a  = om(2);
                om2b  = om(4);
                s2a   = S2;
                s2b   = S4;
                pl2a  = pls(:,2)';
                pl2b  = pls(:,4)';
                pllab2a = pllab(:,2)';
                pllab2b = pllab(:,4)';
                d2a   = d2';
                d2b   = d4';
            else
                om2a  = om(4);
                om2b  = om(2);
                s2a   = S4;
                s2b   = S2;
                pl2a  = pls(:,4)';
                pl2b  = pls(:,2)';
                pllab2a = pllab(:,4)';
                pllab2b = pllab(:,2)';
                d2a   = d4';
                d2b   = d2';
            end

            % Eta angles
            eta1a = gtGeoEtaFromDiffVec(d1a, labgeo); % takes row vector
            eta1b = gtGeoEtaFromDiffVec(d1b, labgeo);
            eta2a = gtGeoEtaFromDiffVec(d2a, labgeo);
            eta2b = gtGeoEtaFromDiffVec(d2b, labgeo);

            % u,v coordinates on the detector
            uv1a=gtFedPredictUVW(s1a,d1a',csam,detpos,tn,Qdet,uvorig,om1a,omstep);
            uv1b=gtFedPredictUVW(s1b,d1b',csam,detpos,tn,Qdet,uvorig,om1b,omstep);
            uv2a=gtFedPredictUVW(s2a,d2a',csam,detpos,tn,Qdet,uvorig,om2a,omstep);
            uv2b=gtFedPredictUVW(s2b,d2b',csam,detpos,tn,Qdet,uvorig,om2b,omstep);
            
            
            % fill the structure grain with one pair only
            grain.pl    = [grain.pl; pl1a; pl2a]; % signed plane normals
            grain.pllab = [grain.pllab; pllab1a; pllab2a];
            grain.hkl   = [grain.hkl; hkl(ii,:); hkl(ii,:)];
            grain.hklsp = [grain.hklsp; hklsp(:,ii); hklsp(:,ii)];
            grain.theta = [grain.theta; th_gd(ii,:); th_gd(ii,:)];
            grain.thetatype = [grain.thetatype; thtype(ii); thtype(ii)];
            grain.eta   = [grain.eta; eta1a; eta2a];
            grain.omega = [grain.omega; om1a; om2a];
            grain.dvec  = [grain.dvec; d1a; d2a];
            % added by LNervo
            grain.srot  = [grain.srot; s1a; s2a];

            % fill with the two pairs
            grain.allblobs.pl    = [grain.allblobs.pl; pl1a; pl1b; pl2a; pl2b];
            grain.allblobs.pllab = [grain.allblobs.pllab; pllab1a; pllab1b; pllab2a; pllab2b];
            grain.allblobs.hkl   = [grain.allblobs.hkl; hkl(ii,:); hkl(ii,:); hkl(ii,:); hkl(ii,:)];
            grain.allblobs.hklsp = [grain.allblobs.hklsp; hklsp(:,ii); -hklsp(:,ii); hklsp(:,ii); -hklsp(:,ii)];
            grain.allblobs.theta = [grain.allblobs.theta; th_gd(ii,:); th_gd(ii,:); th_gd(ii,:); th_gd(ii,:)];
            grain.allblobs.thetatype = [grain.allblobs.thetatype; thtype(ii); thtype(ii); thtype(ii); thtype(ii)];
            grain.allblobs.eta   = [grain.allblobs.eta; eta1a; eta1b; eta2a; eta2b];
            grain.allblobs.omega = [grain.allblobs.omega; om1a; om1b; om2a; om2b];
            grain.allblobs.dvec  = [grain.allblobs.dvec; d1a; d1b; d2a; d2b];
            % added by LNervo
            grain.allblobs.srot  = [grain.allblobs.srot; s1a; s1b; s2a; s2b];
            
            cmap = jet(1001);
            omtoRGB = @(om) cmap(round((om)/(360)*1000+1),:);
            if app.color
                %cmap = jet(1001);

                % Function handle to associate RGB value to omega
                %omtoRGB = @(om) cmap(round((om)/(360)*1000+1),:);

                colormap(cmap)
                colorbar
                caxis([0 360])

                % modified on August, 29
                if isempty(uv1a)
                    uv1a=[NaN; NaN; NaN];
                else
                   if app.showfigure
                        plot(uv1a(1),uv1a(2),'or','MarkerSize',app.Markersize,'MarkerEdgeColor',omtoRGB(om1a));%20
                    end  
                end
                if isempty(uv1b)
                    uv1b=[NaN; NaN; NaN];
                else
                     if app.showfigure
                        plot(uv1b(1),uv1b(2),'ob','MarkerSize',app.Markersize,'MarkerEdgeColor',omtoRGB(om1b));
                    end
                end
                
                if isempty(uv2a)
                    uv2a=[NaN; NaN; NaN];
                else
                    if app.showfigure
                        plot(uv2a(1),uv2a(2),'oy','MarkerSize',app.Markersize,'MarkerEdgeColor',omtoRGB(om2a));
                    end
                end
                if isempty(uv2b)
                    uv2b=[NaN; NaN; NaN];
                else
                    if app.showfigure
                        plot(uv2b(1),uv2b(2),'og','MarkerSize',app.Markersize,'MarkerEdgeColor',omtoRGB(om2b));
                    end
                end

                colormap(gray)
            else
                
                 % modified on August, 29
                if isempty(uv1a)
                    uv1a=[NaN; NaN; NaN];
                else
                   if app.showfigure
                        plot(uv1a(1),uv1a(2),'ob','MarkerSize',app.Markersize);%20
                    end  
                end
                if isempty(uv1b)
                    uv1b=[NaN; NaN; NaN];
                else
                     if app.showfigure
                        plot(uv1b(1),uv1b(2),'ob','MarkerSize',app.Markersize);
                    end
                end
                
                if isempty(uv2a)
                    uv2a=[NaN; NaN; NaN];
                else
                    if app.showfigure
                        plot(uv2a(1),uv2a(2),'ob','MarkerSize',app.Markersize);
                    end
                end
                if isempty(uv2b)
                    uv2b=[NaN; NaN; NaN];
                else
                    if app.showfigure
                        plot(uv2b(1),uv2b(2),'ob','MarkerSize',app.Markersize);
                    end
                end
                
                
            end % end if app.color

            %uv1a
            %uv1b
            %uv2a
            %uv2b
            
            grain.allblobs.uv     =  [grain.allblobs.uv;uv1a(1),uv1a(2);uv1b(1),uv1b(2);uv2a(1),uv2a(2);uv2b(1),uv2b(2)];
            grain.allblobs.detector.uvw    =  [grain.allblobs.detector(1).uvw;uv1a(1),uv1a(2),uv1a(3);uv1b(1),uv1b(2),uv1b(3);uv2a(1),uv2a(2),uv2a(3);uv2b(1),uv2b(2),uv2b(3)];

        end % end if ~empty(om)
    end % end for pl_gd


elseif strcmp(app.Beamchroma,'poly')

    % Loop through plane normals

    for ii = 1:size(pl_gd,1)

        pl    = pl_gd(ii,:)';
        % Calculate plane normal angle with beam
        sinth = abs(pl'*beamdir) ;
        lambda = sinth*2*d0_gd(ii) ;

        % Check if reflection occurs in the given lambda range
        if (lambdamin <= lambda) && (lambda <= lambdamax)

            pls   = -pl*sign(pl'*beamdir) ; % signed plane normal
            theta = asind(sinth) ;
            dvec  = gtFedPredictDiffVec(pls,sinth,beamdir); % takes coloumn vectors

            grain.pl    = [grain.pl; pls'];
            grain.pllab = [grain.pllab; pls'];
            grain.hkl   = [grain.hkl; hkl(ii,:)];
            grain.hklsp = [grain.hklsp; hklsp(ii,:)];
            grain.theta = [grain.theta; theta];
            grain.thetatype = [grain.thetatype; thtype(ii)];
            grain.omega = [grain.omega; 0];
            grain.eta   = [grain.eta; NaN];
            grain.dvec  = [grain.dvec; dvec'];
        end

    end

    grain.allblobs.pl        = grain.pl;
    grain.allblobs.pllab     = grain.pllab;
    grain.allblobs.hkl       = grain.hkl;
    grain.allblobs.hklsp     = grain.hklsp;
    grain.allblobs.theta     = grain.theta;
    grain.allblobs.thetatype = grain.thetatype;
    grain.allblobs.eta       = grain.eta;
    grain.allblobs.omega     = grain.omega;
    grain.allblobs.dvec      = grain.dvec;
    % end Beamchroma=poly
else
    error(' Beamchroma not recognised')

end % end Beamchroma


if app.save
   name=sprintf('gr%d_fed.mat',grain.id);
   save(name, 'grain')
end

end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Create output

%grain.hkl   = int32(grain.hkl);
%grain.hklsp = int32(grain.hklsp);
% grain.thetatype = int32(grain.thetatype);
%
% grain.R_vector = Rvec';
%
% if strcmp(app.Beamchroma,'mono')
% 	disp('Total number of reflecting plane normals:')
% 	disp(length(grain.pl)/2)
% 	disp('Total number of pairs:')
% 	disp(length(grain.pl))
% elseif strcmp(app.Beamchroma,'poly')
% 	disp('Total number of reflecting plane normals:')
% 	disp(length(grain.pl))
% end
% disp('Generated {hkl}-s:')
% disp(grain.hklsp)
% disp('Generated plane normals, theta, eta and omega angles:')
% disp([grain.pl grain.theta grain.eta grain.omega])
%
%
% % Create the unique plane normals:
% ACM = gtIndexAngularConsMatrix(hkltypes_used,parameters);
%
% grain = gtIndexUniquePlaneNormals(grain,ACM);
%
% if strcmp(app.Beamchroma,'mono')
% 	if length(grain.pl) == 2*length(hklsp)
% 		grain.uni.hklsp = hklsp;
% 	else
% 		warning('Field uni.hklsp -unique signed hkl- could not be set properly. This happens by chance. Try to run the whole macro again with new random values.')
% 	end
% end
%
% % Add "measured" d-spacings and lattice parameters
% %grain=gtSTRAINAddLatticePar(grain);
% grain = gtAddGrainLatticePar(grain);
%
% % Set some artificial errors (needed for strain fitting)
% grain.uni.ddy(:) = 0.0001;
% grain.uni.ddz(:) = 0.0001;
% grain.uni.dom(:) = 0.01;
%
% disp('Standard deviations set to a small value in grain.uni.')
%

