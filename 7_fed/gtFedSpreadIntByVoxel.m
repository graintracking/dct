function [gv,bl,bluvol]=gtFedSpreadIntByVoxel(gv,bl,fedpars,flag)
% spreads the intensities of the volume elements over the blob volumes

if ~exist('flag','var')
	flag = 0;
end

nbl = length(bl);
nv  = length(gv);

disp(' Spreading intensities ...  ')
tic

ngv   = fedpars.ngv;
grsize = fedpars.grainsize;
gvsize = fedpars.gvsize;

dint  = zeros(2,2,2); % volume size for interpolating intensity


% Variables for intepolation of u for each voxel

% Grid to store element first voxel indices (locations) in grain volume:
% uvol1   = zeros(ngv); 
% uvol2   = zeros(ngv);
% uvol3   = zeros(ngv);
uvol1   = zeros(ngv+2); 
uvol2   = zeros(ngv+2);
uvol3   = zeros(ngv+2);
for j = 1:nv
	% flip coordinates 1 and 2 for meshgrid !!!
% 	uvol1(gv(j).ind(1),gv(j).ind(2),gv(j).ind(3)) = gv(j).cs(2);
% 	uvol2(gv(j).ind(1),gv(j).ind(2),gv(j).ind(3)) = gv(j).cs(1);
% 	uvol3(gv(j).ind(1),gv(j).ind(2),gv(j).ind(3)) = gv(j).cs(3);
	uvol1(gv(j).ind(1)+1,gv(j).ind(2)+1,gv(j).ind(3)+1) = gv(j).cs(2);
	uvol2(gv(j).ind(1)+1,gv(j).ind(2)+1,gv(j).ind(3)+1) = gv(j).cs(1);
	uvol3(gv(j).ind(1)+1,gv(j).ind(2)+1,gv(j).ind(3)+1) = gv(j).cs(3);
end

% Grid to store actual element [u,v,w] values: 
uvolval = zeros([ngv+2, 3]);
% uvolval = zeros([ngv+2, 3]);

% Interpolated values [u,v,w] values of each voxel will be stored in: 
uvoli   = zeros(grsize(1),grsize(2),grsize(3),3); 
bluvol  = zeros(grsize(1),grsize(2),grsize(3),3,nbl); 

% Grid on which u values will be calculated (representing each voxel of grain
% volume):
[uvox1,uvox2,uvox3] = meshgrid(1:grsize(1),1:grsize(2),1:grsize(3));

	
% Loop through blobs
for i=1:nbl

	blint = zeros(bl(i).bbsize);
	
% 	% Loop through all elements
% 	for j=1:nv
% 	
% 		% Actual u is the previous u plus the change according to strain state.
%     % u0 is the precise value of each element, linear approximation plays 
% 		% first here when applying the strain gv(j).D.
% 
% 		% !!!!
% 		if iniflag==1
% 			ubl = gv(j).u0bl(:,i);  % u in blob of the first voxel of gv(j)
% 		elseif iniflag==2
% 			ubl = gv(j).umbl(:,i);  % u in blob of the first voxel of gv(j)
% 		else
% 			u  = gv(j).U0(:,:,i)*gv(j).d;  % change compared to original u0 due to strain
% 			gv(j).u(:,i)   = u ;
% 			ubl            = gv(j).u0bl(:,i) + u ; % u in blob of the first voxel of gv(j)
% 			gv(j).ubl(:,i) = ubl ;
% 		end
% 	end
	

	% Interpolation of u for each voxel
	% load u values of elements in uvolval
	for j = 1:nv
		
		if flag==1 % at solution
			newu = gv(j).ucm(:,i);
		else
			% New u:
			newu  = gv(j).U0(:,:,i)*gv(j).d;  % change compared to original u0bl due to strain
		end
			
		gv(j).uc(:,i) = newu ;
		
		% u error:
		gv(j).ucerr(:,i) = gv(j).uc(:,i) - gv(j).ucm(:,i);

		% New u in blob:
		%ubl = gv(j).u0bl(:,i) + newu ; % new u of the first voxel of gv(j)in blob 
		%gv(j).ubl(:,i) = ubl ;
		%uvolval(gv(j).ind(1)+1,gv(j).ind(2)+1,gv(j).ind(3)+1,:) = ubl;

		%ubl = gv(j).u0bl(:,i) + gv(j).udc(:,i) + newu ; % new u of element centre gv(j) in blob 
		%gv(j).ubl(:,i) = gv(j).u0bl(:,i) + newu ;
		%uvolval(gv(j).ind(1),gv(j).ind(2),gv(j).ind(3),:) = ubl;
	
		ubl = gv(j).uc0bl(:,i) + newu ; % new u of element centre gv(j) in blob 
		gv(j).ucbl(:,i) = ubl ;
		uvolval(gv(j).ind(1)+1,gv(j).ind(2)+1,gv(j).ind(3)+1,:) = ubl;
	end

	% Extend values around cube boundaries to avoid NaN-s in interpolation
	uvolval(1,:,:,:)   = uvolval(2,:,:,:);
	uvolval(end,:,:,:) = uvolval(end-1,:,:,:);
	uvolval(:,1,:,:)   = uvolval(:,2,:,:);
	uvolval(:,end,:,:) = uvolval(:,end-1,:,:);
	uvolval(:,:,1,:)   = uvolval(:,:,2,:);
	uvolval(:,:,end,:) = uvolval(:,:,end-1,:);
	
	uvol1(1,:,:,:)   = uvol1(2,:,:,:);     
	uvol1(end,:,:,:) = uvol1(end-1,:,:,:); 
	uvol1(:,1,:,:)   = uvol1(:,2,:,:) - gvsize(2);
	uvol1(:,end,:,:) = uvol1(:,end-1,:,:) + gvsize(2);
	uvol1(:,:,1,:)   = uvol1(:,:,2,:);
	uvol1(:,:,end,:) = uvol1(:,:,end-1,:);
	
	uvol2(1,:,:,:)   = uvol2(2,:,:,:) - gvsize(1);
	uvol2(end,:,:,:) = uvol2(end-1,:,:,:) + gvsize(1);
	uvol2(:,1,:,:)   = uvol2(:,2,:,:);     
	uvol2(:,end,:,:) = uvol2(:,end-1,:,:); 
	uvol2(:,:,1,:)   = uvol2(:,:,2,:);
	uvol2(:,:,end,:) = uvol2(:,:,end-1,:);

	uvol3(1,:,:,:)   = uvol3(2,:,:,:);
	uvol3(end,:,:,:) = uvol3(end-1,:,:,:);
	uvol3(:,1,:,:)   = uvol3(:,2,:,:);
	uvol3(:,end,:,:) = uvol3(:,end-1,:,:);
	uvol3(:,:,1,:)   = uvol3(:,:,2,:)     - gvsize(3);
	uvol3(:,:,end,:) = uvol3(:,:,end-1,:) + gvsize(3);
	
	
	% interpolate u,v,w for each voxel
	for k = 1:3
		%uvoli(:,:,:,k) = interp3(uvol1,uvol2,uvol3,uvolval(:,:,:,k),uvox1,uvox2,uvox3,'*spline');
		uvoli(:,:,:,k) = interp3(uvol1,uvol2,uvol3,uvolval(:,:,:,k),uvox1,uvox2,uvox3,'*linear');
	end

	bluvol(:,:,:,:,i) = uvoli;
	
	% Loop through each voxel and project intensity:
	for iv = 1:grsize(1)
		for jv = 1:grsize(2)
			for kv = 1:grsize(3)
% 			
% 				if isnan(uvoli(iv,jv,kv,1))
% 					continue
% 				end
% 				
% 				% Distribute value over 8 voxels:
				ublvox(1) = uvoli(iv,jv,kv,1);
				ublvox(2) = uvoli(iv,jv,kv,2);
				ublvox(3) = uvoli(iv,jv,kv,3);
				
				mu = mod(ublvox,1);

% 				flu = floor(ublvox);
% 				clu = ceil(ublvox)	
							
				% This way of rounding is correct also for round values when mu=mod...
				% is used above:
				flu = floor(ublvox);
				clu = flu + 1;
				
% 
% 				dint = dint0;
				
% 				% Distribute value over 8 voxels:
% 				mu = mod(uvoli(iv,jv,kv,:),1);
% 
% 				flu = floor(uvoli(iv,jv,kv,:));
% 				clu = ceil(uvoli(iv,jv,kv,:));

				dint(1,1,1) = (1-mu(1))*(1-mu(2))*(1-mu(3));
				dint(1,1,2) = (1-mu(1))*(1-mu(2))*mu(3);
				dint(1,2,1) = (1-mu(1))*mu(2)*(1-mu(3));
				dint(1,2,2) = (1-mu(1))*mu(2)*mu(3);
				dint(2,1,1) = mu(1)*(1-mu(2))*(1-mu(3));
				dint(2,1,2) = mu(1)*(1-mu(2))*mu(3);
				dint(2,2,1) = mu(1)*mu(2)*(1-mu(3));
				dint(2,2,2) = mu(1)*mu(2)*mu(3);
 
% 				dint(2,:,:) = dint(2,:,:)*mu(1);
% 				dint(1,:,:) = dint(1,:,:)*(1-mu(1));
% 				dint(:,2,:) = dint(:,2,:)*mu(2);
% 				dint(:,1,:) = dint(:,1,:)*(1-mu(2));
% 				dint(:,:,2) = dint(:,:,2)*mu(3);
% 				dint(:,:,1) = dint(:,:,1)*(1-mu(3));

				blint(flu(1):clu(1),flu(2):clu(2),flu(3):clu(3)) = ...
					blint(flu(1):clu(1),flu(2):clu(2),flu(3):clu(3)) + dint;
			end
		end
	end
	
	% Determine centroid of blob:
	totint   = sum(blint(:));  % total intensity
	cu       = sum(sum(sum(blint,3),2).*(1:size(blint,1))')/totint; % centre u
	cv       = sum(sum(sum(blint,3),1).*(1:size(blint,2)))/totint;  % centre v
	sum12    = zeros(1,size(blint,3));
	sum12(:) = sum(sum(blint,1),2);
	cw = sum(sum12.*(1:size(blint,3)))/totint; % centre w

	
	% Store results
% 	if iniflag==1
% 		bl(i).int0(:,:,:) = blint;
% 		bl(i).int(:,:,:)  = blint;
% 		bl(i).com0bb = [cu cv cw];
% 		bl(i).com0im = [cu cv cw] + bl(i).bbor;
% 		bl(i).intd(:,:,:) = blint - bl(i).intm(:,:,:);
% 	elseif iniflag==2
% 		bl(i).intm(:,:,:) = blint;  
% 		bl(i).commbb = [cu cv cw];
% 		bl(i).commim = [cu cv cw] + bl(i).bbor;
% 	else
% 		bl(i).int(:,:,:)  = blint;
% 		bl(i).combb = [cu cv cw];
% 		bl(i).comim = [cu cv cw] + bl(i).bbor;
% 		bl(i).intd(:,:,:) = blint - bl(i).intm(:,:,:);
% 	end

  % Store results
  bl(i).int(:,:,:)  = blint;
	bl(i).combl = [cu cv cw];
	bl(i).comim = [cu cv cw] + bl(i).bbor;
	bl(i).intd(:,:,:) = blint - bl(i).intm(:,:,:);

	%intd = bl(i).intd(:,:,:);
	%dev = intd.*intd;
	
	%dev = bl(i).intd.*bl(i).intd;
	bl(i).err = std(bl(i).intd(:),1);
		
end

toc


