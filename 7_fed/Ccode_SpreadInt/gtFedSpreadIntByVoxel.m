%function [gv,bl,bluvol,uvolval,uvol1]=gtFedSpreadIntByVoxel(gv,bl,fedpars,flag)
function [gv,bl,uvoli]=gtFedSpreadIntByVoxel(gv,bl,fedpars,flag)
% spreads the intensities of the volume elements over the blob volumes
% if flag ==1, use gv.ucm  --  "measured distortion for sim data'


%if ~exist('flag','var')
%	flag = 1;
%else flag=1;    
%end

%nbl = length(bl);
nbl = length(fedpars.usebl);
nv  = length(gv);
%nbl=1;

disp(' Spreading intensities ...  ')
tic

ngv   = fedpars.ngv;
grsize = fedpars.grainsize;
gvsize = fedpars.gvsize;
usebl = fedpars.usebl;

%if isfield(fedpars, 'crude_acceleration') && fedpars.crude_acceleration
%    disp('doing acceleration')
%    crude_acc=2;
    % need to dilate by odd number, approx 2x crude_acc
%    crude_dil=(2*crude_acc)-1;
    
%     tmp=zeros(71, 71, 71);
%     % make a 6.2 diameter sphere in a 7 cubed cube
%     for i=1:71
%         for j=1:71
%             for k=1:71
%                 r=sqrt((i-36)^2 + (j-36)^2 + (k-36)^2);
%                 if r<31.02
%                     tmp(i,j,k)=1;
%                 end
%             end
%         end
%     end
%     crude_im=sum(tmp, 3);
%     crude_im=imresize(crude_im, [7 7]);
%     % imresize gives some negative values
%     crude_im(find(crude_im<0))=0;
%     % total imtensity should be 27
%     crude_im=crude_im*(crude_acc^3)/sum(crude_im(:));
%else
%    crude_acc=1;
%end
crude_acc=1;
crude_dil=(2*crude_acc)-1;
dint  = zeros(2,2,2); % volume size for interpolating intensity


% Variables for intepolation of u for each voxel
% Grid to store element first voxel indices (locations) in grain volume:

%uvol1   = zeros(ngv+2); 
%uvol2   = zeros(ngv+2);
%uvol3   = zeros(ngv+2);

% first element centre, offset by one element position
centre1 = (gvsize/2) + 0.5 - gvsize;

% last element centre, offset by one element position
centre2 = centre1 + (ngv+1).*gvsize;

%centre1
%centre2
% use mesh grid to produce uvol1, uvol2, uvol3
[uvol1,uvol2,uvol3] = meshgrid(centre1(2):gvsize(2):centre2(2), centre1(1):gvsize(1):centre2(1), centre1(3):gvsize(3):centre2(3));
%uvol1
%size(uvol1)

% for j = 1:nv
%    ! not sure if this is right to flip indices
%     ! don't need toloop through like this for elemnt positions
%    uvol1(gv(j).ind(1)+1,gv(j).ind(2)+1,gv(j).ind(3)+1) = gv(j).cs(2);
% 	uvol2(gv(j).ind(1)+1,gv(j).ind(2)+1,gv(j).ind(3)+1) = gv(j).cs(1);
% 	uvol3(gv(j).ind(1)+1,gv(j).ind(2)+1,gv(j).ind(3)+1) = gv(j).cs(3);
% end


% Grid to store actual element [u,v,w] values: 
% use NaNs to avoid misleading interpolations at the edge of a real grain
uvolval = nan([ngv+2, 3]);
% uvolval = zeros([ngv+2, 3]);

% Interpolated values [u,v,w] values of each voxel will be stored in: 
uvoli   = zeros(grsize(1),grsize(2),grsize(3),3); 

% don't need this for basic operation
%bluvol  = zeros(grsize(1),grsize(2),grsize(3),3); 
%bluvol=[];

% Grid on which u values will be calculated (representing each voxel of grain
% volume):

% flip coordinates 1 and 2 for meshgrid !!!
%[uvox1,uvox2,uvox3] = meshgrid(1:grsize(1),1:grsize(2),1:grsize(3));
[uvox1,uvox2,uvox3] = meshgrid(1:grsize(2),1:grsize(1),1:grsize(3));

	
% Loop through chosen blobs
for i=2:2
    b = fedpars.usebl(i); % which blob out of the possible list are we using?
	%bluvol  = zeros(grsize(1),grsize(2),grsize(3),3); 

	
% 	% Loop through all elements
% 	for j=1:nv
% 	
% 		% Actual u is the previous u plus the change according to strain state.
%     % u0 is the precise value of each element, linear approximation plays 
% 		% first here when applying the strain gv(j).D.
% 
% 		% !!!!
% 		if iniflag==1
% 			ubl = gv(j).u0bl(:,i);  % u in blob of the first voxel of gv(j)
% 		elseif iniflag==2
% 			ubl = gv(j).umbl(:,i);  % u in blob of the first voxel of gv(j)
% 		else
% 			u  = gv(j).U0(:,:,i)*gv(j).d;  % change compared to original u0 due to strain
% 			gv(j).u(:,i)   = u ;
% 			ubl            = gv(j).u0bl(:,i) + u ; % u in blob of the first voxel of gv(j)
% 			gv(j).ubl(:,i) = ubl ;
% 		end
% 	end
	

	% Interpolation of u for each voxel
	% load u values of elements in uvolval
	for j = 1:nv
		
		if flag==1 % at solution
			newu = gv(j).ucm(:,i);
		else
			% New u:
			newu  = gv(j).U0(:,:,i)*gv(j).d;  % change compared to original u0bl due to strain
		end
			
		gv(j).uc(:,i) = newu ;
		
		% u error:
		gv(j).ucerr(:,i) = gv(j).uc(:,i) - gv(j).ucm(:,i);

		% New u in blob:
		%ubl = gv(j).u0bl(:,i) + newu ; % new u of the first voxel of gv(j)in blob 
		%gv(j).ubl(:,i) = ubl ;
		%uvolval(gv(j).ind(1)+1,gv(j).ind(2)+1,gv(j).ind(3)+1,:) = ubl;

		%ubl = gv(j).u0bl(:,i) + gv(j).udc(:,i) + newu ; % new u of element centre gv(j) in blob 
		%gv(j).ubl(:,i) = gv(j).u0bl(:,i) + newu ;
		%uvolval(gv(j).ind(1),gv(j).ind(2),gv(j).ind(3),:) = ubl;
	
		ubl = gv(j).uc0bl(:,i) + newu ; % new u of element centre gv(j) in blob 
		gv(j).ucbl(:,i) = ubl ;
		uvolval(gv(j).ind(1)+1,gv(j).ind(2)+1,gv(j).ind(3)+1,:) = ubl;
    end
    %disp(sprintf('%f %f %f %f %f %f %f
    %%f',uvolval(13,9,2,1),uvolval(14,9,2,1),uvolval(13,10,2,1),uvolval(13,
    %%9,3,1),uvolval(13,10,3,1),uvolval(14,10,2,1),uvolval(14,9,3,1),uvolva
    %%l(14,10,3,1)))
    
% new problem is that the voxels at the edges of the grain are interpolated
% towards zero (outside the grain)...  this isn't right because if an edge
% voxel projects to 
    
    
	
	
	% interpolate u,v,w for each voxel
	for k = 1:3
		%uvoli(:,:,:,k) = interp3(uvol1,uvol2,uvol3,uvolval(:,:,:,k),uvox1,uvox2,uvox3,'*spline');
%		uvoli(:,:,:,k) = interp3(uvol1,uvol2,uvol3,uvolval(:,:,:,k),uvox1,uvox2,uvox3,'*linear');
	uvoli(:,:,:,k) = interp3(uvol1,uvol2,uvol3,uvolval(:,:,:,k),uvox1,uvox2,uvox3,'*linear');
    %bluvol(:,:,:,k) = uvoli(:,:,:,k);
    end

    % don't need this for basic operation
	%bluvol(:,:,:,:,i) = uvoli;
    
	blint = zeros(bl(b).bbsize);
	
    % Loop through each voxel and project intensity:
    % for crude acceleration, treat 3x3x3 cubes as spheres
	for iv = 1:crude_acc:grsize(1)
		for jv = 1:crude_acc:grsize(2)
			for kv = 1:crude_acc:grsize(3)
% 			
				if isnan(uvoli(iv,jv,kv,1))
					continue
                end

                % deal with grain shape properly
                if fedpars.grvol(iv,jv,kv)==0
                    continue
                end


% 				% Distribute value over 8 voxels:
				ublvox(1) = uvoli(iv,jv,kv,1);
				ublvox(2) = uvoli(iv,jv,kv,2);
				ublvox(3) = uvoli(iv,jv,kv,3);
                
	% some voxels project outside teh blobs we have at the moment.  Skip these
% for ease of thing for the moment

if any(ublvox<1) | any(ublvox>bl(b).bbsize) 			
			continue
end
    
    mu = mod(ublvox,1);

				flu = floor(ublvox);
				%clu = ceil(ublvox);
                clu=flu+1; % if any clu=exactly an integer
                
% 				% Distribute value over 8 voxels:
				dint(1,1,1) = (1-mu(1))*(1-mu(2))*(1-mu(3));
				dint(1,1,2) = (1-mu(1))*(1-mu(2))*mu(3);
				dint(1,2,1) = (1-mu(1))*mu(2)*(1-mu(3));
				dint(1,2,2) = (1-mu(1))*mu(2)*mu(3);
				dint(2,1,1) = mu(1)*(1-mu(2))*(1-mu(3));
				dint(2,1,2) = mu(1)*(1-mu(2))*mu(3);
				dint(2,2,1) = mu(1)*mu(2)*(1-mu(3));
				dint(2,2,2) = mu(1)*mu(2)*mu(3);
             
                if crude_acc==1 % if doing this properly
                    
				blint(flu(1):clu(1),flu(2):clu(2),flu(3):clu(3)) = ...
					blint(flu(1):clu(1),flu(2):clu(2),flu(3):clu(3)) + dint;

                else % treating only 1 voxel in 27
                    % 3x3x3 cube equivalent to 3.7 diameter sphere
                    % at each point of dint place a 5x5x1 image of
                    % projected sphere
%                     
%                     crude_dint=zeros(8,8,2);
%                     crude_dint(1:7, 1:7, 1)= dint(1,1,1)*crude_im;
%                     crude_dint(1:7, 1:7, 2)= dint(1,1,2)*crude_im;
%                     crude_dint(1:7, 2:8, 1)= dint(1,2,1)*crude_im;
%                     crude_dint(1:7, 2:8, 2)= dint(1,2,2)*crude_im;
%                     crude_dint(2:8, 1:7, 1)= dint(2,1,1)*crude_im;
%                     crude_dint(2:8, 1:7, 2)= dint(2,1,2)*crude_im;
%                     crude_dint(2:8, 2:8, 1)= dint(2,2,1)*crude_im;
%                     crude_dint(2:8, 2:8, 2)= dint(2,2,2)*crude_im;
%                     
%                     flu(1)=flu(1)-3; flu(2)=flu(2)-3;
%                     clu(1)=clu(1)+3; clu(2)=clu(2)+3;
%                     
%                     blint(flu(1):clu(1),flu(2):clu(2),flu(3):clu(3)) = ...
% 					blint(flu(1):clu(1),flu(2):clu(2),flu(3):clu(3)) + crude_dint;
                
                				blint(flu(1):clu(1),flu(2):clu(2),flu(3):clu(3)) = ...
					blint(flu(1):clu(1),flu(2):clu(2),flu(3):clu(3)) + dint;

                end
            end
		end
    end
    
    % if decimating, dilate to ~fix, rescale blob
    if crude_acc~=1

        blint=imdilate(blint, ones(crude_dil,crude_dil,1));
        % scale to correct intensity
        blint=blint*fedpars.grainvol/sum(blint(:));
        
        
    end
    
	% Determine centroid of blob:
	totint   = sum(blint(:));  % total intensity
	cu       = sum(sum(sum(blint,3),2).*(1:size(blint,1))')/totint; % centre u
	cv       = sum(sum(sum(blint,3),1).*(1:size(blint,2)))/totint;  % centre v
	sum12    = zeros(1,size(blint,3));
	sum12(:) = sum(sum(blint,1),2);
	cw = sum(sum12.*(1:size(blint,3)))/totint; % centre w

	
	% Store results
% 	if iniflag==1
% 		bl(i).int0(:,:,:) = blint;
% 		bl(i).int(:,:,:)  = blint;
% 		bl(i).com0bb = [cu cv cw];
% 		bl(i).com0im = [cu cv cw] + bl(i).bbor;
% 		bl(i).intd(:,:,:) = blint - bl(i).intm(:,:,:);
% 	elseif iniflag==2
% 		bl(i).intm(:,:,:) = blint;  
% 		bl(i).commbb = [cu cv cw];
% 		bl(i).commim = [cu cv cw] + bl(i).bbor;
% 	else
% 		bl(i).int(:,:,:)  = blint;
% 		bl(i).combb = [cu cv cw];
% 		bl(i).comim = [cu cv cw] + bl(i).bbor;
% 		bl(i).intd(:,:,:) = blint - bl(i).intm(:,:,:);
% 	end

  % Store results
    bl(b).int  = blint;
	bl(b).combb = [cu cv cw];
	bl(b).comim = [cu cv cw] + bl(b).bbor;
	bl(b).intd(:,:,:) = blint - bl(b).intm(:,:,:);

	%intd = bl(i).intd(:,:,:);
	%dev = intd.*intd;
	
	%dev = bl(i).intd.*bl(i).intd;
	bl(b).err = std(bl(b).intd(:),1);

   disp(sprintf('... done blob %d', b))

end

toc


