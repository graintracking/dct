#!/bin/bash

export MATLAB=matlab_2011b

#source /scisoft/ESRF_sw/opteron2/set_environment 

g++44 -c -I/mntdirect/_sware/com/${MATLAB}/extern/include \
-I/mntdirect/_sware/com/${MATLAB}/simulink/include -D_GNU_SOURCE -DMATLAB_MEX_FILE -ansi -fPIC \
-fomit-frame-pointer -DMX_COMPAT_32 -O3 -msse2 -DNDEBUG mexFedSpreadIntByVoxel.cc

g++44 -O3 -msse2 -fomit-frame-pointer -shared \
-Wl,--version-script,/mntdirect/_sware/com/${MATLAB}/extern/lib/glnxa64/mexFunction.map -Wl,--no-undefined \
-o mexFedSpreadIntByVoxel.mexa64  mexFedSpreadIntByVoxel.o  \
-Wl,-rpath-link,/mntdirect/_sware/com/${MATLAB}/bin/glnxa64 \
-L/mntdirect/_sware/com/${MATLAB}/bin/glnxa64 -lmx -lmex -lmat -lm

g++44 -c  -fopenmp -lgomp -I/mntdirect/_sware/com/${MATLAB}/extern/include \
-I/mntdirect/_sware/com/${MATLAB}/simulink/include -D_GNU_SOURCE -DMATLAB_MEX_FILE -ansi  -D__OPENMP -fPIC \
-fomit-frame-pointer -DMX_COMPAT_32 -O3 -msse2 -DNDEBUG mexFedSpreadIntByVoxel_omp.cc

g++44 -O3 -msse2 -fomit-frame-pointer -fopenmp -shared \
-Wl,--version-script,/mntdirect/_sware/com/${MATLAB}/extern/lib/glnxa64/mexFunction.map -Wl,--no-undefined \
-o mexFedSpreadIntByVoxel_omp.mexa64  mexFedSpreadIntByVoxel_omp.o  \
-Wl,-rpath-link,/mntdirect/_sware/com/${MATLAB}/bin/glnxa64 \
-L/mntdirect/_sware/com/${MATLAB}/bin/glnxa64 -lmx -lmex -lmat -lm -lgomp
