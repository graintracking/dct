
/*Usage: [{bluvol}]=mexFedSpreadIntByVoxel(gv,bl,fedpars,0,flag)
  gv, bl are updated internally. bluvol is optional and when set it returns the
  interpolation matrix of the LAST blob.
 */
/**
 * \file
 * \brief  MEX C library for FEDSpreadIntensity
 *
 * Replacement to the gtFedSpreadIntByVoxel matlab function
 */

/*
 *   Project: MEX C library for FEDSpreadIntensity
 *
 *   Copyright (C) 2010-2012 European Synchrotron Radiation Facility
 *                           Grenoble, France
 *
 *   Principal authors: D. Karkoulis (karkouli@esrf.fr)
 *   Last revision: 16/04/2012
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published
 *   by the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   and the GNU Lesser General Public License  along with this program.
 *   If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <mex.h>  //Mex header file for the MATLAP API
#include <matrix.h> //mx

#include <emmintrin.h> //SSE header.

//SSE2 intrinsic, converts one double to one 32bit integer by rounding. Check "cvtsd2si" SSE2 command.
#define sd2int(in_d)\
  _mm_cvtsd_si32(_mm_load_sd(&(in_d))) 

//SSE2 intrinsic, converts one double to one 32bit integer by truncation. Check "cvttsd2si" SSE2 command.
#define tsd2int(in_d)\
  _mm_cvttsd_si32(_mm_load_sd(&(in_d)))
  
#ifdef _PROFILE
//Profiler
#include "timer.hc"
#else
#define profiler(...)
#endif

#ifdef _WIN32
  #include <float.h>
  #define isnan(x) _isnan(x)
  #include <ymath.h>
  #if _MSC_VER < 1300
    #define NAN _Nan._D
  #else
    #define NAN _Nan._Double
  #endif
#endif
/*
+NAN float: 7fc00000
-NAN float: 7fc00000
+NAN double: 7ff8000000000000 
-NAN double: 7ff8000000000000

+INF float: 7f800000
-INF float: 7f800000
+INF double: 7ff0000000000000
-INF double: 7ff0000000000000
*/


//gv size {1..nv}
struct gv{

  double *uc; //(3,nb)
  double *ucm; //(3,nb)
  double *U0; //(3,9,nb)
  double *d; //(9,1)
  double *ucerr; //(3,nb)
  double *uc0bl; //(3,nb)
  double *ucbl; //(3,nb)
  double *ind; //(1,3)
  int    indt[3];
  double *csgr;

};

//bl size {1..nb}
struct bl{

  double *bbsize;//(1,3)
  double *inte;  //(bbsize(1),bbsize(2),bbsize(3))
  double *intd;  //(bbsize(1),bbsize(2),bbsize(3))
  double *intm;  //(bbsize(1),bbsize(2),bbsize(3))
  double *combb; //?
  double *comim; //?
  double *bbor;  //?
  double *err;   //?
};

struct fedpars{

  double *grainsize; //(1,3)
  double *gvsize;    //(1,3)
  double *grainvol;  //(1)
  double *ngv;       //(1,3)
  double *usebl;     //(1,nb)
  //mxLogical is 1byte length!
  mxLogical *grvol;          //(grainsize(1),grainsize(2),grainsize(3))
  //int    *grvol;     //(grainsize(1),grainsize(2),grainsize(3))
  double *crude_acceleration; //1/0
  int    len_usebl;
};

//Mex function to call MATLAB commands (does not return results).
//int mexEvalString(const char *command);

inline double stddev(int *ibbsize,bl *c_bl){

  double mean,dev,estimator;
  double den;
  
  mean = 0.0f; den = 1.0/(ibbsize[0]*ibbsize[1]*ibbsize[2]);
  for(int i=0;i<ibbsize[0]*ibbsize[1]*ibbsize[2];i++) mean += c_bl->intd[i];
  mean *= den;

  dev = 0.0f;
  for(int i=0;i<ibbsize[0]*ibbsize[1]*ibbsize[2];i++) dev += (c_bl->intd[i] - mean)*(c_bl->intd[i] - mean);
  estimator = sqrt(den * dev);
  
  return estimator;
}

inline double trilinear_interpolation(int *ngv,double *xd,double *yd,double *zd,int *uvx,
                                      int *uvy,int *uvz,double *uvk){

  //Notice that: NaN*0=NaN etc.
  double v[8];
  int cur_idx;

  //V000
  cur_idx=(uvx[0]) + ngv[0]*((uvy[0]) + ngv[1]*(uvz[0]));
  v[0]=(uvk[cur_idx]) * (xd[0]) * (yd[0]) * (zd[0]);

  //V100
  cur_idx=(uvx[1]) + ngv[0]*((uvy[0]) + ngv[1]*(uvz[0]));
  v[1]=(uvk[cur_idx]) * (xd[1]) * (yd[1]) * (zd[1]);

  //V010
  cur_idx=(uvx[0]) + ngv[0]*((uvy[1]) + ngv[1]*(uvz[0]));
  v[2]=(uvk[cur_idx]) * (xd[2]) * (yd[2]) * (zd[2]);

  //V001
  cur_idx=(uvx[0]) + ngv[0]*((uvy[0]) + ngv[1]*(uvz[1]));
  v[3]=(uvk[cur_idx]) * (xd[4]) * (yd[4]) * (zd[4]);

  //V110
  cur_idx=(uvx[1]) + ngv[0]*((uvy[1]) + ngv[1]*(uvz[0]));
  v[4]=(uvk[cur_idx]) * (xd[3]) * (yd[3]) * (zd[3]);

  //V101
  cur_idx=(uvx[1]) + ngv[0]*((uvy[0]) + ngv[1]*(uvz[1]));
  v[5]=(uvk[cur_idx]) * (xd[5]) * (yd[5]) * (zd[5]);

  //V011
  cur_idx=(uvx[0]) + ngv[0]*((uvy[1]) + ngv[1]*(uvz[1]));
  v[6]=(uvk[cur_idx]) * (xd[6]) * (yd[6]) * (zd[6]);

  //V111
  cur_idx=(uvx[1]) + ngv[0]*((uvy[1]) + ngv[1]*(uvz[1]));
	v[7]=(uvk[cur_idx]) * (xd[7]) * (yd[7]) * (zd[7]);

  return (v[0]+v[1]+v[2]+v[3]+v[4]+v[5]+v[6]+v[7]);

}
int interpolate_extrapolate(gv *c_gv,int *grsize,int *ngv,int *gvsize, double *uvoli,
                            double *uvolvalk,double *uvol1,double *uvol2,double *uvol3){

  int uv1,uv2,uv3;
	int loc_v; //voxel in coordinates local to the element. 
	int gvcenter[3]; //INTEGER (truncated) center of an element (local)
	int isoncenter[3]; //Flag to mark absense of interpolation on a dimension.
	int isodd[3]; //Element size is not even
	int uv[3]; //are the current elements, vx,vy,vz
	int ijkv[3]; //are the current voxels, x,y,z
  int uvx[2],uvy[2],uvz[2]; //are the nearest elements to a voxel

  double da[8],db[8],dc[8];
	double xd[8],yd[8],zd[8]; // are the differences x - [x], y - [y], z - [z]				
  double signa,signb,signc;
	double d_gvcenter[3];

	gvcenter[0] = gvsize[0]/2;
  gvcenter[1] = gvsize[1]/2;
	gvcenter[2] = gvsize[2]/2;
	
	for(int i=0;i<3;i++){
		if(gvsize[i]%2)isodd[i]=1;
		else isodd[i]=0;
	}
	
#ifndef _OLD_INTEROP
  for (int kv = 0;kv<grsize[2];kv++){
    for (int jv = 0;jv<grsize[1];jv++){
      for (int iv = 0;iv<grsize[0];iv++){
        //if the voxel belongs to an inactive element skip this iteration step and fetch a new voxel:
        //-This indexing (iv/gvsize[]) gives the element number of a voxel. Since each element containts i.e. for
        //-the x-axis gvsize[0] voxels then iv/gvsize[0]  is moving one x-element for every gvsize[0] x-voxels.
        // 				if(!c_gv[iv/gvsize[0] + ngv[0]*(jv/gvsize[1] + ngv[1]*(kv/gvsize[2]))].active){
        // 					printf("Skipping voxel {%d,%d,%d} as part of inactive element {%d,%d,%d} (in Matlab indexing)\n",
        // 						iv+1,jv+1,kv+1,iv/gvsize[0]+1,jv/gvsize[1]+1,kv/gvsize[2]+1);
        // 					continue;
        // 				}

        //start of vertices for trilinear interpolation:
        //Notice that all voxels that are on the edges, in fact over half element_size size towards the borded are discarded cause
        // they are not meant to be interpolated!
        if(iv>=gvcenter[0] && iv < grsize[0]-gvcenter[0] && jv>=gvcenter[1] && jv<grsize[1]-gvcenter[1] && kv>=gvcenter[2] && kv<grsize[2]-gvcenter[2]){
				//if(iv==88 && jv==160 && kv==22){
          //In which element this voxel belongs to? Is it on an odd or even position (affects the direction of the interpolation)
          uv1 = iv / gvsize[0];
          uv2 = jv / gvsize[1];
          uv3 = kv / gvsize[2];
          //handle the case where an element is inactive
          if( isnan(uvolvalk[uv1 + ngv[0]*(uv2 + ngv[1]*uv3)]) )continue;
					//Find the local coordinate and use it to find which elements to interpolate with
//					uv1 == 0 ? loc_v = iv : loc_v = iv % uv1;
					uv1 == 0 ? loc_v = iv : loc_v = iv - (uv1*gvsize[0]);
					if(loc_v/gvcenter[0]){ //if >=1 its right of center or center
						if(isodd[0] && loc_v == gvcenter[0]){ //is the central point -> do not interpolate
							isoncenter[0]=1;
							uvx[0]=uv1; uvx[1]=uv1+1; //No interpolation, get all the value of parent element
              //uvoli[iv + grsize[0]*(jv + grsize[1]*kv)] = uvolvalk;
						}else{
							isoncenter[0]=0;
							uvx[0]=uv1; uvx[1]=uv1+1;
						}
						
					}else{ //if ==0 its left of center
            uvx[0]=uv1-1; uvx[1]=uv1; //V000 is one element to the left, V100 is this element												
            isoncenter[0]=0;
					}


//          uv2 == 0 ? loc_v = jv : loc_v = jv % uv2;
          uv2 == 0 ? loc_v = jv : loc_v = jv - (uv2*gvsize[1]);
					if(loc_v/gvcenter[1]){ //if >=1 its up of center or center
						if(isodd[1] && loc_v == gvcenter[1]){ //is the central point -> do not interpolate
							isoncenter[1]=1;
							uvy[0]=uv2; uvy[1]=uv2+1;             
						}else{
							isoncenter[1]=0;
							uvy[0]=uv2; uvy[1]=uv2+1;
						}
						
					}else{ //if ==0 its down of center
            uvy[0]=uv2-1; uvy[1]=uv2;
						isoncenter[1]=0;
					}
					

//					uv3 == 0 ? loc_v = kv : loc_v = kv % uv3;
					uv3 == 0 ? loc_v = kv : loc_v = kv - (uv3*gvsize[2]);
					if(loc_v/gvcenter[2]){ //if >=1 its up of center or center
						if(isodd[2] && loc_v == gvcenter[2]){ //is the central point -> do not interpolate
							isoncenter[2]=1;
              uvz[0]=uv3; uvz[1]=uv3+1;
						}else{
							isoncenter[2]=0;
							uvz[0]=uv3; uvz[1]=uv3+1;
						}
						
					}else{ //if ==0 its down of center
            uvz[0]=uv3-1; uvz[1]=uv3;
						isoncenter[2]=0;
					}					

        }else continue;

        for(int kd=0;kd<2;kd++){ //Vxx0|1
					for(int jd=0;jd<2;jd++){ //Vx0|1x
				    for(int id=0;id<2;id++){ //V0|1xx
#ifdef _DEBUG
							if((uvx[id])>=ngv[0] || (uvy[jd])>=ngv[1] || (uvz[kd])>=ngv[2]){
								printf("uv1 %d id %d iv %d \n"
											 "uv2 %d jd %d jv %d \n"
											 "uv3 %d kd %d,kv %d \n",uv1,id,iv,uv2,jd,jv,uv3,kd,kv);
								printf("uvxyz[id] = {%d,%d,%d}\n",uvx[id],uvy[jd],uvz[kd]);
								printf("ngv[xyz] = {%d,%d,%d}\n",ngv[0],ngv[1],ngv[2]);
								printf("isodd [%d %d %d]\n",isodd[0],isodd[1],isodd[2]);
								printf("isoncenter [%d %d %d]\n",isoncenter[0],isoncenter[1],isoncenter[2]);
								mexErrMsgTxt("Interpolation - Extrapolation: uvols out of bounds");
							}
#endif
							//Get the position of element in voxels
              da[id + 2*(jd + 2*kd)]=uvol1[(uvx[1-id]) + ngv[0]*((uvy[1-jd]) + ngv[1]*(uvz[1-kd]))];
              db[id + 2*(jd + 2*kd)]=uvol2[(uvx[1-id]) + ngv[0]*((uvy[1-jd]) + ngv[1]*(uvz[1-kd]))];
              dc[id + 2*(jd + 2*kd)]=uvol3[(uvx[1-id]) + ngv[0]*((uvy[1-jd]) + ngv[1]*(uvz[1-kd]))];
							
							//Calculate the distance of current voxel from center of element and normalize to 1
//               xd[id + 2*(jd + 2*kd)]  =fabs((da[id + 2*(jd + 2*kd)] - (iv+0.5))/gvsize[0]);
//               yd[id + 2*(jd + 2*kd)]  =fabs((db[id + 2*(jd + 2*kd)] - (jv+0.5))/gvsize[1]);
//               zd[id + 2*(jd + 2*kd)]  =fabs((dc[id + 2*(jd + 2*kd)] - (kv+0.5))/gvsize[2]);
              xd[id + 2*(jd + 2*kd)]  =fabs( (da[id + 2*(jd + 2*kd)] - iv - 0.5) /gvsize[0] );
              yd[id + 2*(jd + 2*kd)]  =fabs( (db[id + 2*(jd + 2*kd)] - jv - 0.5) /gvsize[1] );
              zd[id + 2*(jd + 2*kd)]  =fabs( (dc[id + 2*(jd + 2*kd)] - kv - 0.5) /gvsize[2] );
              signa-=2; //V0xx -> V1xx
						}
            signb-=2;signa=1;//Vx0x -> Vx1x, V1xx->V0xx
					}
          signc-=2;signb=1;signa=1;//Vxx0 -> Vxx1, Vx1x->Vx0x, V1xx->V0xx
				}

        double value=trilinear_interpolation(ngv,xd,yd,zd,uvx,uvy,uvz,uvolvalk);
        uvoli[iv + grsize[0]*(jv + grsize[1]*kv)] = value;

      }
    }
  }
#else
//--------- Old code path
  for (int kv = 0;kv<grsize[2];kv++){
    for (int jv = 0;jv<grsize[1];jv++){
      for (int iv = 0;iv<grsize[0];iv++){
        //if the voxel belongs to an inactive element skip this iteration step and fetch a new voxel:
        //-This indexing (iv/gvsize[]) gives the element number of a voxel. Since each element containts i.e. for
        //-the x-axis gvsize[0] voxels then iv/gvsize[0]  is moving one x-element for every gvsize[0] x-voxels.
        // 				if(!c_gv[iv/gvsize[0] + ngv[0]*(jv/gvsize[1] + ngv[1]*(kv/gvsize[2]))].active){
        // 					printf("Skipping voxel {%d,%d,%d} as part of inactive element {%d,%d,%d} (in Matlab indexing)\n",
        // 						iv+1,jv+1,kv+1,iv/gvsize[0]+1,jv/gvsize[1]+1,kv/gvsize[2]+1);
        // 					continue;
        // 				}

        //start of vertices for trilinear interpolation:
        if(iv>0 && iv < grsize[0]-1 && jv>0 && jv<grsize[1]-1 && kv>0 && kv<grsize[2]-1){
          //In which element this voxel belongs to? Is it on an odd or even position (affects the direction of the interpolation)

          uv1 = iv / gvsize[0];
          uv2 = jv / gvsize[1];
          uv3 = kv / gvsize[2];
          //handle the case where an element is inactive
          if( isnan(uvolvalk[uv1 + ngv[0]*(uv2 + ngv[1]*uv3)]) )continue;

          if(iv % gvsize[0] == 0)	{ //if is left
            uvx[0]=uv1-1; uvx[1]=uv1; //V000 is one element to the left, V100 is this element
          }
          else { //else is right
            uvx[0]=uv1; uvx[1]=uv1+1;
          }
          if(jv % gvsize[1] == 0){ //if is down
            uvy[0]=uv2-1; uvy[1]=uv2;
          }
          else { //else is up
            uvy[0]=uv2; uvy[1]=uv2+1;
          }
          if(kv % gvsize[2] == 0) { //if is front
            uvz[0]=uv3-1; uvz[1]=uv3;
          }
          else { //else is back
            uvz[0]=uv3; uvz[1]=uv3+1;
          }
        }
        //start of vertices for trilinear extrapolation:
        else{

          if(iv/gvsize[0] == ngv[0]-1) uv1 = iv / gvsize[0] - 1;
          else uv1 = iv / gvsize[0];
          if(jv/gvsize[1] == ngv[1]-1) uv2 = jv / gvsize[1] - 1;
          else uv2 = jv / gvsize[1];
          if(kv/gvsize[2] == ngv[2]-1) uv3 = kv / gvsize[2] - 1;
          else uv3 = kv / gvsize[2];

          //uncomment continue to let border voxels 0
          continue;
        }
//         double da[8],db[8],dc[8];
//         double signa,signb,signc;
        for(int kd=0;kd<2;kd++){ //Vxx0|1
					for(int jd=0;jd<2;jd++){ //Vx0|1x
				    for(int id=0;id<2;id++){ //V0|1xx
							if((uvx[id])>=ngv[0] || (uvy[jd])>=ngv[1] || (uvz[kd])>=ngv[2]){
								printf("uv1 %d id %d uv2 %d jd %d uv3 %d kd %d,iv jv kv %d %d %d\n",uv1,id,uv2,jd,uv3,kd,iv,jv,kv);
								mexErrMsgTxt("Intrapolation - Extrapolation: uvols out of bounds");
							}
              da[id + 2*(jd + 2*kd)]=uvol1[(uvx[1-id]) + ngv[0]*((uvy[1-jd]) + ngv[1]*(uvz[1-kd]))];
              db[id + 2*(jd + 2*kd)]=uvol2[(uvx[1-id]) + ngv[0]*((uvy[1-jd]) + ngv[1]*(uvz[1-kd]))];
              dc[id + 2*(jd + 2*kd)]=uvol3[(uvx[1-id]) + ngv[0]*((uvy[1-jd]) + ngv[1]*(uvz[1-kd]))];
							xd[id + 2*(jd + 2*kd)]  =fabs((da[id + 2*(jd + 2*kd)] - (iv+0.5))/2);
							yd[id + 2*(jd + 2*kd)]  =fabs((db[id + 2*(jd + 2*kd)] - (jv+0.5))/2);
							zd[id + 2*(jd + 2*kd)]  =fabs((dc[id + 2*(jd + 2*kd)] - (kv+0.5))/2);
              signa-=2; //V0xx -> V1xx
						}
            signb-=2;signa=1;//Vx0x -> Vx1x, V1xx->V0xx
					}
          signc-=2;signb=1;signa=1;//Vxx0 -> Vxx1, Vx1x->Vx0x, V1xx->V0xx
				}

        double value=trilinear_interpolation(ngv,xd,yd,zd,uvx,uvy,uvz,uvolvalk);
        uvoli[iv + grsize[0]*(jv + grsize[1]*kv)] = value;

      }
    }
  }
#endif
  return 0;

}


double *c_FedSpreadIntByVoxel(int len_usebl,int len_nbl,int len_nv,int crude_acceleration,int flag,
                              gv *c_gv,bl *c_bl,fedpars *c_fedpars){

#ifdef _PROFILE	
  proftype T;
#endif	
	int iv,jv,kv,v,blob,rblob;
  int i,j,k;
  int useMATLABfunctions=0;
  int crude_dil;
  int ind1dint,ind2dint,ind3dint;
  int indt[3];
  double totalintensity;
  int bbindex;
  int ngv[3],grsize[3],gvsize[3];
  int nngv=1;

	int *vindt;
  double *uvoli;
  double *uvol1,*uvol2,*uvol3;
  double *uvolval;
  double *bluvol;
  double *intesum3;
  double *intesumu;
  double *intesumv;
  double *intesumw;
  double *intesum1;
  double cu,cv,cw;

  double ublvox[3]={0,0,0};
  double mu[3]={0,0,0};
  int flu[3]={0,0,0};
  int clu[3]={0,0,0};
  int ibbsize[3];
  double fluclu_convert;
  double dint[2][2][2];

//Ingore crude acceleration
//   if(crude_acceleration){
//     crude_acceleration=2;
//     crude_dil=(2*crude_acceleration)-1;
//     //bypass
//     crude_acceleration=1;
//   }else crude_acceleration=1;

  for(int i=0;i<len_nv;i++) memset(c_gv[i].uc,0,3*len_nbl*sizeof(double));

  printf(" Spreading intensities...\n");
	printf(" Reminder, ucerr calculation ommited!\n");

  for(int i=0;i<3;i++) {
    ngv[i] =sd2int( c_fedpars->ngv[i]);
    nngv*=ngv[i];
    grsize[i]=sd2int(c_fedpars->grainsize[i]);
    gvsize[i]=sd2int(c_fedpars->gvsize[i]);

  }
//   printf("gvsize %d %d %d\n",gvsize[0],gvsize[1],gvsize[2]);  
//   return NULL;
  //Calloc allocates memory and initializes it to 0!. You should never try to free this memory, Matlab will do that for you.
	profiler(T,"START");
	vindt = (int    *)mxCalloc(len_nv*3,sizeof(int));
  uvol1 = (double *)mxCalloc(nngv,sizeof(double)); //Warning not all elements physically exist in gv.
  uvol2 = (double *)mxCalloc(nngv,sizeof(double)); //Warning not all elements physically exist in gv.
  uvol3 = (double *)mxCalloc(nngv,sizeof(double)); //Warning not all elements physically exist in gv.
  //bluvol= (double *)mxCalloc(grsize[0] * grsize[1] * grsize[2] * 3 * len_nbl,sizeof(double)); //Removed due to memory issues!
  bluvol= (double *)mxCalloc(grsize[0] * grsize[1] * grsize[2] * 3 * 1,sizeof(double));	
  uvolval = (double *)mxCalloc(nngv*3,sizeof(double)); //Warning not all elements physically exist in gv.
	profiler(T,"STOP");
	profiler(T,"PRINT","mxCalloc");

#ifdef _DEBUG	
  size_t countalloc=0;	
	countalloc = (nngv * 6 + grsize[0] * grsize[1] * grsize[2] * 3 * 1)*sizeof(double);
	printf(" Allocated C memory: %lu (MB)\n",countalloc/(1024*1024));
#endif	
	
	if(!vindt) mexErrMsgTxt("Critical memory allocation failure for vindt");
  if(!uvol1 || !uvol2 || !uvol3) mexErrMsgTxt("Critical memory allocation failure for uvols");
  if(!bluvol) mexErrMsgTxt("Critical memory allocation failure for bluvol");
  if(!uvolval) mexErrMsgTxt("Critical memory allocation failure for uvolval");

	profiler(T,"START");
  for(int i=0;i<nngv*3;i++)uvolval[i] = NAN;
  for(int i=0;i<grsize[0] * grsize[1] * grsize[2] * 3 * 1;i++)bluvol[i]=NAN;
  for(v=0;v<len_nv;v++){
    vindt[0 + 3*v]=sd2int(c_gv[v].ind[0])-1;
    vindt[1 + 3*v]=sd2int(c_gv[v].ind[1])-1;
    vindt[2 + 3*v]=sd2int(c_gv[v].ind[2])-1;
		indt[0]=vindt[0 + 3*v];
		indt[1]=vindt[1 + 3*v];
		indt[2]=vindt[2 + 3*v];
    //calculate the element positions in voxels (not centered!)
//     uvol1[indt[0] + ngv[0]*(indt[1] + ngv[1]*indt[2])] = indt[0]*gvsize[0] + gvsize[0]/2;
//     uvol2[indt[0] + ngv[0]*(indt[1] + ngv[1]*indt[2])] = indt[1]*gvsize[1] + gvsize[1]/2;
//     uvol3[indt[0] + ngv[0]*(indt[1] + ngv[1]*indt[2])] = indt[2]*gvsize[2] + gvsize[2]/2;
    uvol1[indt[0] + ngv[0]*(indt[1] + ngv[1]*indt[2])] = (double)(indt[0]*gvsize[0]) + (double)gvsize[0]/2.0;
    uvol2[indt[0] + ngv[0]*(indt[1] + ngv[1]*indt[2])] = (double)(indt[1]*gvsize[1]) + (double)gvsize[1]/2.0;
    uvol3[indt[0] + ngv[0]*(indt[1] + ngv[1]*indt[2])] = (double)(indt[2]*gvsize[2]) + (double)gvsize[2]/2.0;

  }
  profiler(T,"STOP");
	profiler(T,"PRINT","Init Latency");

  for(rblob=0;rblob<len_usebl;rblob++){

    blob = sd2int(c_fedpars->usebl[rblob]) - 1; //Convert matlab index to C
    printf("blob %d\n",blob+1);
    for(i=0;i<3;i++) ibbsize[i]=sd2int(c_bl[blob].bbsize[i]);
    memset(c_bl[blob].inte,0,ibbsize[0]*ibbsize[1]*ibbsize[2]*sizeof(double));

    for(v=0;v<len_nv;v++){
      //Calculate the peak shifts relative to the previous iteration step for all elements in all blobs:
      //  v.uc = v.U0 * v.d
      //The new absolut position of a peak in a blob is the reference position plus the change:
      //	v.ucbl = v.uc0bl + v.uc

			indt[0]=vindt[0 + 3*v];
			indt[1]=vindt[1 + 3*v];
			indt[2]=vindt[2 + 3*v];
      for(i=0;i<3;i++){
        for(k=0;k<9;k++){

          if(flag==0)c_gv[v].uc[i + 3*blob] += c_gv[v].U0[i + 3*(k + 9*blob)] * c_gv[v].d[k];
          else       c_gv[v].uc[i + 3*blob]  = c_gv[v].ucm[i + 3*blob];
        }
        //c_gv[v].ucerr[i +3*blob]= c_gv[v].uc[i + 3*blob] - c_gv[v].ucm[i + 3*blob];
        c_gv[v].ucbl[i +3*blob] = c_gv[v].uc0bl[i +3*blob] + c_gv[v].uc[i +3*blob];

        uvolval[indt[0] + (ngv[0]*(indt[1] + ngv[1]*(indt[2] +ngv[2]*i)))] = c_gv[v].ucbl[i +3*blob];
      }
    }//end nv loop
    //Interpolate the v.ucbl results over all the voxels in the grain volume,
    //so that each voxel can then be projected seperately to get a smooth intensity distribution in the blobs
    //interpolated values will be stored in bluvol  = zeros(grsize(1),grsize(2),grsize(3),3,nbl)
  profiler(T,"START");    
    if(!useMATLABfunctions){
      for(k=0;k<3;k++){
        //instead of using uvoli and then copy bluvol, pass bluvol directly, in the position
        //of memory that is to store this blob's xdta
        uvoli = &bluvol[grsize[0] * grsize[1] * grsize[2] * (k + 3*0)];
        interpolate_extrapolate(c_gv,grsize,ngv,gvsize,uvoli,&uvolval[nngv*k],uvol1,uvol2,uvol3);
      }
    } else {
      /*    int err = mexCallMATLAB();*/
    }
  profiler(T,"STOP");
	profiler(T,"PRINT","Interp Time");
    //Project the intensities represented by the grain voxels into all blobs.
    //Distribute the intensity (intensity of a voxel >=0, but may not be binary!)
    //of each grain voxel into 8 neighbouring blob voxels, and them all up.

	profiler(T,"START");
    //-TODO- FIX FAST INDEX ROTATION -TODO-
    for (iv = 0;iv<grsize[0]; iv++){
      for (jv = 0;jv<grsize[1]; jv++){
        for (kv = 0;kv<grsize[2]; kv++){
  
          if(c_fedpars->grvol[iv + grsize[0]*(jv + kv*grsize[1])] == 0) continue;
          // Distribute value over 8 voxels:
          for(i=0;i<3;i++) ublvox[i] = bluvol[iv + grsize[0]*(jv + grsize[1]*(kv + grsize[2]*(i + 3*0)))];
  
          if(isnan(ublvox[0]) || isnan(ublvox[1]) || isnan(ublvox[2])) continue;
          if(ublvox[0]<=0 || ublvox[1]<=0 || ublvox[2]<=0)continue;
          if(ublvox[0]>=ibbsize[0]-1 || ublvox[1]>=ibbsize[1]-1 || ublvox[2]>=ibbsize[2]-1)continue;

#ifdef _DEBUG					
          if((ublvox[0]>ibbsize[0]-1 || ublvox[1]>ibbsize[1]-1 || ublvox[2]>ibbsize[2]-1) || \
            (ublvox[0]<0 || ublvox[1]<0 || ublvox[2]<0)){
              printf("ublvox {%f,%f,%f}\n",ublvox[0],ublvox[1],ublvox[2]);
              printf("and array bounxdries are: {%d,%d,%d}\n",ibbsize[0],ibbsize[1],ibbsize[2]);
              mexErrMsgTxt("ublvox exceeds dimensions. Interpolation problem???");
          }
#endif          
  
           //for(i=0;i<3;i++){
           //  mu[i]  = fmod(ublvox[i],1);
           //  fluclu_convert = floor(ublvox[i]);
           //  flu[i] = sd2int(fluclu_convert);
           //  clu[i] = flu[i] + 1;
           //}
          
          for(i=0;i<3;i++){
            mu[i]  = fmod(ublvox[i],1);
            flu[i] = tsd2int(ublvox[i]);
            clu[i] = flu[i] + 1;
          }

          dint[0][0][0] = (1-mu[0])*(1-mu[1])*(1-mu[2]);
          dint[0][0][1] = (1-mu[0])*(1-mu[1])*mu[2];
          dint[0][1][0] = (1-mu[0])*mu[1]*(1-mu[2]);
          dint[0][1][1] = (1-mu[0])*mu[1]*mu[2];
          dint[1][0][0] = mu[0]*(1-mu[1])*(1-mu[2]);
          dint[1][0][1] = mu[0]*(1-mu[1])*mu[2];
          dint[1][1][0] = mu[0]*mu[1]*(1-mu[2]);
          dint[1][1][1] = mu[0]*mu[1]*mu[2];

          for(i=(flu[0]-1);i<clu[0];i++){
            for(j=(flu[1]-1);j<clu[1];j++){
              for(k=(flu[2]-1);k<clu[2];k++){
                ind1dint = i - (flu[0]-1); //is 0 or 1
                ind2dint = j - (flu[1]-1); //-1 to make it a C index
                ind3dint = k - (flu[2]-1);

#ifdef _DEBUG								
                //check to determine if access to xdta is within legal limits and print debug info if a violation is detected:
                if(ind1dint>=2 || ind2dint>=2 ||ind3dint>=2 || ind1dint<0 || ind2dint<0 || ind3dint<0){
                  printf("ibbsizes %d %d %d\n",ibbsize[0],ibbsize[1],ibbsize[2]);
                  printf("dint {%d,%d,%d},ijk {%d,%d,%d}",ind1dint,ind2dint,ind3dint,i,j,k);
                  mexErrMsgTxt("dint array out of bounds");
                }
#endif                
                if(i<0 || j <0 || k<0){
                  //Warning-C version does not need to expand the elements. As such it is possible that ijk are -1 in which case we ignore them
                  // Much like the Matlab version, which instead has expanded elements and thus element -1 is the first element, but is set to NAN
                  // and ignored.
                  //printf("Warning ivjvkv {%d,%d,%d}\n",iv,jv,kv);
                  //printf("ubl %f %f %f\n",ublvox[0],ublvox[1],ublvox[2]);
                  //printf("%f %f %f %f %f %f %f %f\n",dint[0][0][1],dint[0][1][0],dint[0][1][1],dint[1][0][0],dint[1][0][1],dint[1][1][0],dint[1][1][1],dint[0][0][0]);
                  //printf("ijk {%d,%d,%d} ,flu[] {%d,%d,%d},clu[] {%d,%d,%d} , ind123 {%d,%d,%d}\n",i,j,k,flu[0],flu[1],flu[2],clu[0],clu[1],clu[2],ind1dint,ind2dint,ind3dint);
                  continue;
                  //mexErrMsgTxt("intensity array out of bounds.i or j or k < 0");
                }
#ifdef _DEBUG                
                if( (i>=ibbsize[0]) || (j>=ibbsize[1]) || (k>=ibbsize[2]) ) {
                  printf("blob %d i,j,k %d %d %d, ibb %d,%d,%d\n",blob,i,j,k,ibbsize[0],ibbsize[1],ibbsize[2]);
                  mexErrMsgTxt("intensity array out of bounds");
                }
                if( isnan(c_bl[blob].inte[i + ibbsize[0]*(j + ibbsize[1]*k)]) ) {
                  printf("blob %d i,j,k %d %d %d\n",blob,i,j,k);
                  mexErrMsgTxt("intensity is NaN");
                }
#endif                
                //end of sanity checks
  
                //end of checks. If xdta is ok proceed:
                c_bl[blob].inte[i + ibbsize[0]*(j + ibbsize[1]*k)] += dint[ind1dint][ind2dint][ind3dint];
              }//end k
            }//end j
          }//end i
        }//end kv
      }//end jv
    }//end iv
  profiler(T,"STOP");
	profiler(T,"PRINT","ijvk loop");
  
  
  
    cu=0;cv=0;cw=0;
    totalintensity=0;
    //Calloc allocates memory and initializes it to 0!. You should never try to free this memory, Matlab will do that for you.
    {
      intesum3 = (double *)calloc(ibbsize[0]*ibbsize[1],sizeof(double)) ;
      intesum1 = (double *)calloc(ibbsize[1]*ibbsize[2],sizeof(double)) ;
      intesumu = (double *)calloc(ibbsize[0],sizeof(double)) ;
      intesumv = (double *)calloc(ibbsize[1],sizeof(double)) ;
      intesumw = (double *)calloc(ibbsize[2],sizeof(double)) ;
      if(!intesum3 || !intesum1 || !intesumu || !intesumv || !intesumw) mexErrMsgTxt("Critical memory allocation failure in sum arrays");
    }

    for(i=0;i<ibbsize[0];i++){
      for(j=0;j<ibbsize[1];j++){
        for(k=0;k<ibbsize[2];k++){
          bbindex=i + ibbsize[0]*(j + ibbsize[1]*k);
          c_bl[blob].intd[bbindex] = c_bl[blob].inte[bbindex] - c_bl[blob].intm[bbindex];
          intesum3[i + ibbsize[0]*j] += c_bl[blob].inte[bbindex];
          intesum1[j + ibbsize[1]*k] += c_bl[blob].inte[bbindex];
          totalintensity += c_bl[blob].inte[bbindex];
          if(i==ibbsize[0]-1) intesumw[k] += intesum1[j + ibbsize[1]*k];
        }
        intesumu[i] += intesum3[i + ibbsize[0]*j];
        intesumv[j] += intesum3[i + ibbsize[0]*j];
      }
    }
    //for the centroid we apply the weights +1 since in C we start from 0 not 1 in memory mapping
    //these 3 loops represent the element by element product and summation of 1:size(blint,1) with the previously found sum of
    //sum(sum(blint,3),2)
    for(i=0;i<ibbsize[0];i++) cu += (intesumu[i] * (i+1));
    for(j=0;j<ibbsize[1];j++) cv += (intesumv[j] * (j+1));
    for(k=0;k<ibbsize[2];k++) cw += (intesumw[k] * (k+1));
  
    free(intesum3);
    free(intesum1);
    free(intesumu);
    free(intesumv);
    free(intesumw);
  
    cu = cu/totalintensity;
    cv = cv/totalintensity;
    cw = cw/totalintensity;
  
    c_bl[blob].combb[0] = cu;
    c_bl[blob].combb[1] = cv;
    c_bl[blob].combb[2] = cw;
    c_bl[blob].comim[0] = cu + c_bl[blob].bbor[0];
    c_bl[blob].comim[1] = cv + c_bl[blob].bbor[1];
    c_bl[blob].comim[2] = cw + c_bl[blob].bbor[2];
    *c_bl[blob].err = stddev(ibbsize,&c_bl[blob]);

    //     c_bl[blob].combl[0] = cu/totalintensity;
    //     c_bl[blob].combl[1] = cv/totalintensity;
    //     c_bl[blob].combl[2] = cw/totalintensity;
#ifdef _DEBUG
    printf("... done blob %d\n",blob+1);
#endif		
  
  }//end blobs loop
  //Return to mexfunction to copy results to the output arrays for use in Matlab
  return bluvol;
}

/* -- mexFedSpreadIntByVoxel() spreads the intensities of the volume elements over the blob volumes --*/
/* wrapper for the C version of Peter's matlab function [gv,bl,bluvol]=gtFedSpreadIntByVoxel(gv,bl,fedpars,flag)*/
/* nlhs is num of arguments on the left (output), nrhs is number of arguments on the right (input) */
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]){

  //mxArray is a pointer to MATLAB xdta as defined by the MATLAB API:
  mxArray *mxgv,*mxbl,*mxfedpars;  
  mxLogical *mxgrvol;
  //mxArray *mxflag;
  const char *mxGetField_error="\
                               Invalid argument or no value assigned to the specified field. Common causes of failure include:\
                               *Specifying an array pointer that does not point to a structure mxArray. To determine whether it points to a structure\
                               mxArray, call mxIsStruct. \
                               *Specifying an index to an element outside the bounds of the mxArray. For example, given a structure mxArray that contains\
                               ten elements, you cannot specify an index greater than 9 in C (10 in Fortran).\
                               *Specifying a nonexistent fieldname. Call mxGetFieldNameByNumber or mxGetFieldNumber to get existing field names.\
                               *Insufficient heap space to hold the returned mxArray.";

  double *cdflag;
  int flag,crude_acceleration;
  int len_nbl,len_nv,len_usebl;
  mwIndex mxind;

  gv *c_gv;
  bl *c_bl;
  fedpars *c_fedpars;
#ifdef _PROFILE	
  proftype T;
#endif	


  if(nrhs!=5) mexErrMsgTxt ("Expects 5 input arguments (gv,bl,fedpars,0,flag).\n"\
    " usage: '[{bluvol}]=mexFedSpreadIntByVoxel(gv,bl,fedpars,0,flag)'\n"\
    " fields in {} are optional.[{bluvol}] is for debugging. \n");

  if(nlhs > 1) mexErrMsgTxt ("(gv,bl) are automatically updated. Please remove them from the output arguments\n"\
    " usage: '[{bluvol}]=mexFedSpreadIntByVoxel(gv,bl,fedpars,0,flag)'\n"\
    " fields in {} are optional.[{bluvol}] is for debugging.\n");

  mexEvalString("tic");
	profiler(T,"START");
  //the following two are handled outside C in a special m file that calls the mex function
  crude_acceleration = (int)mxGetScalar(prhs[3]);
  flag = (int)mxGetScalar(prhs[4]);                         

  //Check that the input arguments are structures. If they are not there will be segmentation fault
  //- mxErrMsgTxt is a Matlab function that prints an error message in Matlab console and kills the mex-file
  if(!mxIsStruct(prhs[0]) || !mxIsStruct(prhs[1]) || !mxIsStruct(prhs[2]))
    mexErrMsgTxt("Input arguments 0,1 and 2 must be of type struct");
  if(mxIsStruct(prhs[3]) || mxIsStruct(prhs[4]))
    mexErrMsgTxt("Input arguments 3 and 4 must be scalars not structs");

  len_nbl = mxGetN(prhs[1]) * mxGetM(prhs[1]);
  len_nv  = mxGetN(prhs[0]) * mxGetM(prhs[0]);

  mxfedpars=mxGetField(prhs[2],0,"usebl");
  len_usebl = mxGetN(mxfedpars) * mxGetM(mxfedpars);
  //len_nbl=1;len_usebl=1;

  if(len_nv<1 || len_usebl<1){
    mexPrintf("len_nv %d, N %d,M %d\n",len_nbl,mxGetN(prhs[3]),mxGetM(prhs[3]));
    mexPrintf("len_nv %d, N %d,M %d\n",len_nv,mxGetN(prhs[0]),mxGetM(prhs[0]));
    mexErrMsgTxt("Usebl is 0 or negative. v length is 0 or negative");
  } 

  //allocate the memory to hold the pointers to the Matlab xdta:
  c_gv = new gv [len_nv];
  c_bl = new bl [len_nbl];
  c_fedpars = new fedpars;

  //These are the keys to access in the input structures:
  const char *gvkeys[] = { "ucm", "U0","d","uc","ucerr","uc0bl","ucbl","ind","csgr"};
  int gvkeyslen=9;
  //loop over all the keys, Get a pointer to the field of interest in input Matlab struct and assign it to a C pointer.
  for(int i=0;i<gvkeyslen;i++){
    for(mxind=0;mxind<len_nv;mxind++){
      mxgv=mxGetField(prhs[0],mxind,gvkeys[i]);
      if(!mxgv) {
        printf("->%s",gvkeys[i]);
        mexErrMsgTxt(mxGetField_error);
      }
      switch (i){
        case 0:
          c_gv[mxind].ucm    = mxGetPr(mxgv); break;
        case 1:
          c_gv[mxind].U0     = mxGetPr(mxgv); break;        
        case 2:
          c_gv[mxind].d      = mxGetPr(mxgv); break;          
        case 3:
          c_gv[mxind].uc     = mxGetPr(mxgv); break;
        case 4:
          c_gv[mxind].ucerr  = mxGetPr(mxgv); break;          
        case 5:
          c_gv[mxind].uc0bl  = mxGetPr(mxgv); break;
        case 6:
          c_gv[mxind].ucbl   = mxGetPr(mxgv); break;          
        case 7:
          c_gv[mxind].ind    = mxGetPr(mxgv); break;  
        case 8:
          c_gv[mxind].csgr   = mxGetPr(mxgv); break;
        default:
          mexErrMsgTxt("switch exception");         
      }
    }
  } 
  const char *blkeys[] = { "bbsize","int","intd","intm","combb","comim","bbor","err"};
  int blkeyslen=8;

  for(int i=0;i<blkeyslen;i++){
    for(mxind=0;mxind<len_nbl;mxind++){
      mxbl=mxGetField(prhs[1],mxind,blkeys[i]);
      if(!mxbl) {
        printf("->%s",blkeys[i]);
        mexErrMsgTxt(mxGetField_error);
      }
      switch (i){
        case 0:
          c_bl[mxind].bbsize  = mxGetPr(mxbl); break;
        case 1:
          c_bl[mxind].inte    = mxGetPr(mxbl); break;          
        case 2:
          c_bl[mxind].intd    = mxGetPr(mxbl); break;          
        case 3:
          c_bl[mxind].intm    = mxGetPr(mxbl); break;          
        case 4:
          c_bl[mxind].combb   = mxGetPr(mxbl); break;          
        case 5:
          c_bl[mxind].comim   = mxGetPr(mxbl); break;                    
        case 6:
          c_bl[mxind].bbor    = mxGetPr(mxbl); break; 
        case 7:
          c_bl[mxind].err     = mxGetPr(mxbl); break; 
        default:
          mexErrMsgTxt("switch exception");         
      }
    }
  }

  const char *fedparskeys[] = { "grainsize", "gvsize" , "grainvol","ngv","usebl","grvol","crude_acceleration","len_usebl"};
  int fedparskeyslen=7;
  for(int i=0;i<fedparskeyslen;i++){
    mxfedpars=mxGetField(prhs[2],0,fedparskeys[i]);
    if(!mxfedpars) {
      printf("->%s",fedparskeys[i]);
      mexErrMsgTxt(mxGetField_error);
    }
    switch (i){
        case 0:
          c_fedpars->grainsize = mxGetPr(mxfedpars); break;
        case 1:
          c_fedpars->gvsize    = mxGetPr(mxfedpars); break;
        case 2:
          c_fedpars->grainvol  = mxGetPr(mxfedpars); break;
        case 3:
          c_fedpars->ngv       = mxGetPr(mxfedpars); break;
        case 4:
          c_fedpars->usebl     = mxGetPr(mxfedpars); break;
        case 5:
          c_fedpars->grvol    = mxGetLogicals(mxfedpars); break;
        case 6:
          //c_fedpars->crude_acceleration = mxGetPr(mxfedpars); 
          break;
        case 7:
          //c_fedpars->len_usebl  = (int) mxGetScalar(mxfedpars);
          break;          
        default:
          mexErrMsgTxt("switch exception");         
    }
  }

  //call main function
#ifdef _DEBUG
  printf("calling main\n");
#endif	
#ifndef _OLD_INTEROP
  printf(" Using new interpolation method\n");
#else
  printf(" Using old 2x2x2 limited interpolation method \n");
#endif
  double *out=NULL; double *help;
	profiler(T,"STOP");
	profiler(T,"PRINT","Interface latency");	
	profiler(T,"START");	
  out = c_FedSpreadIntByVoxel(len_usebl,len_nbl,len_nv,crude_acceleration,flag,c_gv,c_bl,c_fedpars);
	profiler(T,"STOP");
	profiler(T,"PRINT","Exec Time");	
#ifdef _DEBUG	
  printf("calling main ok\n");
#endif	


  //debug bluvol/uvolval. We allocate and copy the memory in Matlab to examine this test variable thoroughly
  if(1){
    if(nlhs==1){
      int dims[5];
      dims[0]= (int)c_fedpars->grainsize[0];
      dims[1]= (int)c_fedpars->grainsize[1];
      dims[2]= (int)c_fedpars->grainsize[2];
      dims[3]= 3;
      dims[4]= 1;
      plhs[0] = mxCreateNumericArray(5,dims,mxDOUBLE_CLASS,mxREAL);
      help = mxGetPr(plhs[0]);
      for(int i=0;i<dims[0]*dims[1]*dims[2]*dims[3]*dims[4];i++) help[i] = out[i];
    }
  }else{
    if(nlhs==1){
      int dims[4];
      dims[0]= sd2int( c_fedpars->ngv[0]);
      dims[1]= sd2int( c_fedpars->ngv[1]);
      dims[2]= sd2int( c_fedpars->ngv[2]);
      dims[3]= 3;
      plhs[0] = mxCreateNumericArray(4,dims,mxDOUBLE_CLASS,mxREAL);
      help = mxGetPr(plhs[0]);
      for(int i=0;i<dims[0]*dims[1]*dims[2]*dims[3];i++) help[i] = out[i];
    }
  }

  mexEvalString("toc");

  return;
}

