#!/bin/bash

export MATLAB=/sware/com/matlab_2011b
CC=g++-4.4
#source /scisoft/ESRF_sw/opteron2/set_environment 

${CC} -c  -fopenmp -lgomp -I${MATLAB}/extern/include \
-I${MATLAB}/simulink/include -D_GNU_SOURCE -DMATLAB_MEX_FILE -ansi  -D__OPENMP -fPIC \
-fomit-frame-pointer -DMX_COMPAT_32 -O3 -msse2 -DNDEBUG mexFedSpreadIntByVoxel.cc

${CC} -O3 -msse2 -fomit-frame-pointer -fopenmp -shared \
-Wl,--version-script,${MATLAB}/extern/lib/glnxa64/mexFunction.map -Wl,--no-undefined \
-o mexFedSpreadIntByVoxel.mexa64  mexFedSpreadIntByVoxel.o  \
-Wl,-rpath-link,${MATLAB}/bin/glnxa64 \
-L${MATLAB}/bin/glnxa64 -lmx -lmex -lmat -lm -lgomp

${CC} -c  -fopenmp -lgomp -I${MATLAB}/extern/include \
-I${MATLAB}/simulink/include -D_GNU_SOURCE -DMATLAB_MEX_FILE -ansi  -D__OPENMP -fPIC \
-fomit-frame-pointer -DMX_COMPAT_32 -O3 -msse2 -DNDEBUG mexFedSpreadIntByVoxel_omp.cc

${CC} -O3 -msse2 -fomit-frame-pointer -fopenmp -shared \
-Wl,--version-script,${MATLAB}/extern/lib/glnxa64/mexFunction.map -Wl,--no-undefined \
-o mexFedSpreadIntByVoxel_omp.mexa64  mexFedSpreadIntByVoxel_omp.o  \
-Wl,-rpath-link,${MATLAB}/bin/glnxa64 \
-L${MATLAB}/bin/glnxa64 -lmx -lmex -lmat -lm -lgomp
