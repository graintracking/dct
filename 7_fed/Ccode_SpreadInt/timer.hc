
#include <stdio.h>
#include <string.h>

#ifdef __linux
#include <sys/time.h>
#endif
#ifdef _WIN32
#pragma comment(lib, "winmm.lib")
#include <windows.h>
//#define WIN32_LEAN_AND_MEAN
#define _CRT_SECURE_NO_WARNINGS
#endif

typedef struct{

#ifdef _WIN32
  double start;
  double end;
#elif defined(__linux)
  timeval s,e;
  double start;
  double end;
#endif

}proftype;

int profiler(proftype &T, const char *mode, const char *msg=NULL){

  //Windows timeGetTime resolution is about 5ms but can be adjusted by timeBeginPeriod/timeEndPeriod
  //Linux gettimeofday resolution is in millisec scale. 
 if(!strcmp(mode,"START")){
#ifdef _WIN32
  T.start=(double)timeGetTime();
  T.start*=1000.; //convert to usec
#elif defined(__linux)
   gettimeofday(&(T.s), 0);
   T.start = (double)(T.s.tv_sec)*1e6 + (double)(T.s.tv_usec);
#endif
 }else if(!strcmp(mode,"STOP")){
#ifdef _WIN32
   T.end=(double)timeGetTime();
   T.end*=1000.; //convert to usec
#elif defined(__linux)
   gettimeofday(&(T.e),0);
   T.end = (double)(T.e.tv_sec)*1e6 + (double)(T.e.tv_usec);
#endif
 }else if(!strcmp(mode,"PRINT")){
   if(msg)printf("%s: %f (ms) %f (s)\n",msg,((T.end - T.start))*1e-3,((T.end - T.start))*1e-6);
   else printf("%f (ms) %f (s)\n",((T.end - T.start))*1e-3,((T.end - T.start))*1e-6);
 }
   return 0;
 }
