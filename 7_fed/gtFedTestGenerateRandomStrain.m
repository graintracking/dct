function [gv,dvol] = gtFedGenerateRandomStrain(gv,fedpars)

% Interpolates a function in 3D between random values defined at 3x3x3
% nodes.
	
grsize = fedpars.grainsize;
ngv = fedpars.ngv;

dvol=NaN([grsize 9]);

for i=1:9
	% 3x3x3 grid points on which random strain components will be generated
	mgvec            = [0, 0.5, 1];
	[mg1,mg2,mg3]    = meshgrid(mgvec,mgvec,mgvec);

	% Flip real coordinates 1-2 due to meshgrid !!!!
  [mg1i,mg2i,mg3i] = meshgrid((0.5:grsize(2))/grsize(2),...
		                          (0.5:grsize(1))/grsize(1),...
                          		(0.5:grsize(3))/grsize(3));
	% Generate random strain components (+-fedpars.dapplim) on the 3x3x3 grid
	mgvals           = (1-2*rand(3,3,3))*fedpars.dapplim(i);
	
	% For polychromatic beam, the first normal strain component is set 0.
	%   Hydrostatic strain cannot be inferred, i.e. max. 8 components only.
	if (i==4) && strcmp(fedpars.beamchroma,'poly')
		mgvals(:) = 0;
	end
	
	% ('*cubic' - cubic interpolation where input data is regularly spaced)
	dvol(:,:,:,i) = interp3(mg1,mg2,mg3,mgvals,mg1i,mg2i,mg3i,'*cubic');
	
 	% Interpolate d values for each voxel and gv centre
	%if strcmp(fedpars.dappinterp,'cubic')

	for j=1:length(gv)
		% Flip coordinates 1-2 due to meshgrid !!!!
		gv(j).dmcub(i) = interp3(mg1,mg2,mg3,mgvals,(gv(j).ind(2)-0.5)/ngv(2),...
			(gv(j).ind(1)-0.5)/ngv(1),(gv(j).ind(3)-0.5)/ngv(3),...
			'*cubic');
		%gv(j).d(i)  = 0; % initial d values
	end

	%elseif strcmp(fedpars.dappinterp,'linear');

% 		for j=1:length(gv)
% 			% Flip coordinates 1-2 due to meshgrid !!!!
% 			gv(j).dm(i) = interp3(mg1,mg2,mg3,mgvals,(gv(j).ind(2)-0.5)/ngv(2),...
% 				            (gv(j).ind(1)-0.5)/ngv(1),(gv(j).ind(3)-0.5)/ngv(3),...
% 										'*linear');	
% 			gv(j).d(i)  = 0; % initial d values
% 		end

  for j=1:length(gv)
		for k = 1:size(gv(j).voxindrel,2)
			voxind  = gv(j).cs1+gv(j).voxindrel(:,k)';
			dvox(k) = dvol(voxind(1),voxind(2),voxind(3),i);
		end
		gv(j).dm(i) = mean(dvox);
	end
			
	%else
	%	error('Interpolation type for strain not recognized.')
	%end
end


