function gv = gtFedDefCompSmoothing(ngdvol,gv,fedpars)


% no. of elements in x,y,z
ngv  = fedpars.ngv;


if fedpars.dsmoothing > 0
	
	% Smoothing of solution by taking the mean values in the neighbourhood of
	% a voxel

	% Loop through elements by index
	
	for i = 1:ngv(1)
		for j = 1:ngv(2)
			for k = 1:ngv(3)

				lim1l = max(i-1,1);
				lim1h = min(i+1,ngv(1));
				lim2l = max(j-1,1);
				lim2h = min(j+1,ngv(2));
				lim3l = max(k-1,1);
				lim3h = min(k+1,ngv(3));

				% linear index of actual element
				gvactind = sub2ind(ngv,i,j,k);
				
				% Loop through d components
				for id = 1:9
					
					% Determine d values in subvolumes
					dvals(1) = ngdvol(lim1l,j,k,id);
					dvals(2) = ngdvol(lim1h,j,k,id);
					dvals(3) = ngdvol(i,lim2l,k,id);
					dvals(4) = ngdvol(i,lim2h,k,id);
					dvals(5) = ngdvol(i,j,lim3l,id);
					dvals(6) = ngdvol(i,j,lim3h,id);
					
					% The extent of smoothing is a linear comb. of the actual value
					% and the mean of the neighbours.
					gv(gvactind).d(id) = fedpars.dsmoothing*mean(dvals(:)) + ...
						               (1-fedpars.dsmoothing)*gv(gvactind).d(id) ;
												 
				end		
				
			end
		end
	end

end





