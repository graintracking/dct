function gtFedShowDifSpots(whichids,bl,mflag)
%function gtFedShowDifSpots(whichids,bl,mflag)
% whichids - which ID-s to show
% bl - blob structure
% mflag -  0-initial, 1-measured, 2-actual, 3-measured+actual


if ~exist('mflag','var')
	mflag=[];
end


if strcmp(mflag,'ini') || strcmp(mflag,'mes') || strcmp(mflag,'act')
	if isempty(whichids)
		whichids = 1:length(bl);
	end
	for i=1:length(whichids)
		gtFedShowDifSpot(whichids(i),bl,mflag)
	end
else
	
	close all

	if isempty(whichids)

		for i=1:8
			gtFedShowDifSpot(i,bl,'mes')
		end
		for i=1:8
			gtFedShowDifSpot(i,bl,'act')
		end

		for i=9:16
			gtFedShowDifSpot(i,bl,'mes')
		end
		for i=9:16
			gtFedShowDifSpot(i,bl,'act')
		end

		for i=17:18
			gtFedShowDifSpot(i,bl,'mes')
		end
		for i=1:6
			figure
		end
		for i=17:18
			gtFedShowDifSpot(i,bl,'act')
		end

	else
		for i=1:length(whichids)
			gtFedShowDifSpot(whichids(i),bl,'ini')
			gtFedShowDifSpot(whichids(i),bl,'act')
		end
	end
	
end
	
tilefig

end


