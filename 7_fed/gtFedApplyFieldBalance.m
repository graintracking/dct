function [gv,bl]=gtFedApplyFieldBalance(gv,bl,fedpars)



nbl = length(bl);
nv  = length(gv);

dlim = fedpars.dlim;
ngv = fedpars.ngv;

% allfu = zeros(nv,nbl);
% allfv = zeros(nv,nbl);
% allfw = zeros(nv,nbl);

%fedpars.dstep = 0.00005*ones(9,1);
%fedpars.dstepmode = 'discr';


if strcmp(fedpars.dstepmode,'discr')
	dstepdiscrete = true;
else
	dstepdiscrete = false;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Start with measured mean omegas of the blobs

disp(' Applying gradient field...  ')
tic


for i = 1:nbl

	% Gradient of intensity difference (first and second coord. are flipped):
	[gradv,gradu,gradw] = gradient(bl(i).intd);
	
	gvcent = zeros(3,nv);

	% Absolut u of element center in blob
	for j=1:nv		
		gvcent(:,j) = gv(j).u(:,i) + gv(j).udc(:,i) + gv(j).u0bl(:,i);
	end
	
	% Interpolated gradient values at element [u,v,w] (flip first 2 coords!):
	gradui = interp3(gradu,gvcent(2,:),gvcent(1,:),gvcent(3,:),'*linear');
	gradvi = interp3(gradv,gvcent(2,:),gvcent(1,:),gvcent(3,:),'*linear');
	gradwi = interp3(gradw,gvcent(2,:),gvcent(1,:),gvcent(3,:),'*linear');

	for j=1:nv		
		gv(j).fu(:,i) = -[gradui(j); gradvi(j); gradwi(j)];
	end
	
end


dfvol = zeros(ngv(1),ngv(2),ngv(3),9);

% Loop through elements
for i = 1:nv
	
	% Loop through Friedel pairs (every second plane normal)
	for j = 1:2:nbl

		fAu = gv(i).fu(1,j);
		fBu = gv(i).fu(1,j+1);
		if fAu*fBu > 0
  		gv(i).fu2(1,(j+1)/2) = (fAu + fBu)/2;
		else  % contradictory guess
  		gv(i).fu2(1,(j+1)/2) = 0;
		end
		
		fAv = gv(i).fu(2,j);
		fBv = gv(i).fu(2,j+1);
		if fAv*fBv < 0
  		gv(i).fu2(2,(j+1)/2) = (fAv - fBv)/2;
		else  % contradictory guess
  		gv(i).fu2(2,(j+1)/2) = 0;
		end
		
		fAw = gv(i).fu(3,j);
		fBw = gv(i).fu(3,j+1);
		if fAw*fBw > 0
  		gv(i).fu2(3,(j+1)/2) = (fAw + fBw)/2;
		else  % contradictory guess
  		gv(i).fu2(3,(j+1)/2) = 0;
		end		
		
	end

	
	% Speed factor for optimum solution:
	fu2s = gv(i).fu2;
	fu2s(fu2s==0) = NaN;
	gv(i).fac2m = (gv(i).um(:,1:2:end) - gv(i).su2)./fu2s;

	%gv(i).su2 = min(gv(i).fu2.*fedpars.sufac, fedpars.sumax);
	%gv(i).su2 = max(gv(i).su2, -fedpars.sumax);
	gv(i).su2 = gv(i).fu2.*fedpars.sufac;

	gv(i).solsr = gv(i).su2./(gv(i).um(:,1:2:end) - gv(i).su2);
	
	% Change to be applied to d:
	ddact = gv(i).T02*gv(i).su2(:);

	% Discretize d change
	if dstepdiscrete
		ddact = sign(ddact).*fedpars.dsteplim;
	else
		ddact = min(ddact,fedpars.dsteplim);
		ddact = max(ddact,-fedpars.dsteplim);
	end
	
	dnew = gv(i).d + ddact;
	dnew = min(dnew,dlim);
	dnew = max(dnew,-dlim);

	gv(i).do = gv(i).d; % old d value
	gv(i).df = dnew;
	gv(i).d  = dnew;

	dfvol(gv(i).ind(1),gv(i).ind(2),gv(i).ind(3),:) = dnew;

end


if fedpars.dsmoothing
	% Smoothing of solution by taking the mean values in the neighbourhood of
	% a voxel
	disp(' Smoothing d components over elements...')
	
	for i = 1:ngv(1)
		for j = 1:ngv(2)
			for k = 1:ngv(3)

								lim1l = max(i-1,1);
								lim1h = min(i+1,ngv(1));
								lim2l = max(j-1,1);
								lim2h = min(j+1,ngv(2));
								lim3l = max(k-1,1);
								lim3h = min(k+1,ngv(3));
				
								for id = 1:9
									dfvals = dfvol(lim1l:lim1h,lim2l:lim2h,lim3l:lim3h,id);
									%gv(sub2ind(ngv,i,j,k)).d(id) = (mean(dfvals(:)) + gv(sub2ind(ngv,i,j,k)).d(id))/2;
									gv(sub2ind(ngv,i,j,k)).d(id) = mean(dfvals(:));
								end

% 				for id = 1:9
% 					
% 					dfvals(1) = dfvol(i-1,j,k,id);
% 					dfvals(2) = dfvol(i+1,j,k,id);
% 					dfvals(3) = dfvol(i,j-1,k,id);
% 					dfvals(4) = dfvol(i,j+1,k,id);
% 					dfvals(5) = dfvol(i,j,k-1,id);
% 					dfvals(6) = dfvol(i,j,k+1,id);
% 					
% 					% New value is mean of neighbouring values and its own value:
% 					gv(sub2ind(ngv,i,j,k)).d(id) = mean([mean(dfvals(:)) dfvol(i,j,k,id)]);
% 					
% 				end

			end
		end
	end

else
	disp(' No smoothing on d components applied...')
end


% Save d difference from current step:
for i=1:nv
	gv(i).dd = gv(i).d - gv(i).do;
end


toc




