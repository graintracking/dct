function [dfac, clab] = gtFedPredictDiffFac(Rottens, dlab, csam, detrefpos, detnorm)
% [dfac,clab] = gtFedPredictDiffFac(Rottens, dlab, csam, detrefpos, detnorm)
%
% Gives the diffraction vector multiplicator.
% Diffraction vector dlab doesn't have to be normalised.
% csam,detrefpos must be in identical units.
%

    % Position of centre after rotation (Sample -> Lab coord.)
    if isempty(Rottens)
        clab = csam;
    else
        warning('gtFedPredictDiffFac:deprecated_behavior', ...
            'csam should always be given directly in Lab coordinates for future compatibility')

        clab = Rottens * csam; % column 3x1
    end

    % the scalar multiplicator of the diffraction vector
    dfac = detnorm'*(detrefpos-clab)/(detnorm'*dlab);

    % beam won't hit the detector if dfac is negative
    if (dfac <= 0)
        dfac = [];
    end
end % end of function
