function DerPlnDef = gtFedDerPlaneNormDeformation(n)

% FUNCTION 
%
% Gives the derivative of the unit plane normal components wrt. the deformation components.
%
%
%


% SAMPLE coordinate system is orthogonal, right handed [X,Y,Z].
% Deformation components are defined as: 
%   d1 = r32, right-handed rotation around X axis
%   d2 = r13, right-handed rotation around Y axis
%   d3 = r21, right-handed rotation around Z axis
%   d4 = e11, normal strain component along X axis
%   d5 = e22, normal strain component along Y axis
%   d6 = e33, normal strain component along Z axis
%   d7 = e23, shear strain component in YZ plane
%   d8 = e13, shear strain component in XZ plane
%   d9 = e12, shear strain component in XY plane
%



DerPlnDef = [    0,  n(3), -n(2), (n(1)*n(1)-1)*n(1),     n(2)*n(2)*n(1),     n(3)*n(3)*n(1),      2*n(2)*n(3)*n(1), (2*n(1)*n(1)-1)*n(3),  (2*n(1)*n(1)-1)*n(2); ...
	           -n(3),     0,  n(1),     n(1)*n(1)*n(2), (n(2)*n(2)-1)*n(2),     n(3)*n(3)*n(2),  (2*n(2)*n(2)-1)*n(3),     2*n(1)*n(3)*n(2),  (2*n(2)*n(2)-1)*n(1); ...
	            n(2), -n(1),     0,     n(1)*n(1)*n(3),     n(2)*n(2)*n(3), (n(3)*n(3)-1)*n(3),  (2*n(3)*n(3)-1)*n(2), (2*n(3)*n(3)-1)*n(1),      2*n(1)*n(2)*n(3)];
