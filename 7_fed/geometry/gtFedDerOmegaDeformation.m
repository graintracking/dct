function DerOmDef = gtFedDerOmegaDeformation(pln,Rottens,DerRottensOm,...
               DerPlnDef,beamdir,DerEloDef,sinth)

% FUNCTION 
%
% Gives the derivative of omega wrt. the deformation components.
%
% pln - plane normal in SAMPLE coordinates
%
%
%

tmp = (beamdir'*DerRottensOm*pln);

if tmp==0
	DerOmDef = [0 0 0 0 0 0 0 0 0];
else
	DerOmDef = 1/tmp*(sinth*DerEloDef - beamdir'*Rottens*DerPlnDef) ;
end