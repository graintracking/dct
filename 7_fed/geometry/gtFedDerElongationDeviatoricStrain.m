function out = gtFedDerElongationDeviatoricStrain(n)

% FUNCTION 
%
% Gives the derivative of the elongation along a plane normal wrt. the 
% deviatoric strain components.
%
%
%

out = [n(1)*n(1)-n(3)*n(3), n(2)*n(2)-n(3)*n(3), 2*n(2)*n(3), 2*n(1)*n(3), 2*n(1)*n(2)];
