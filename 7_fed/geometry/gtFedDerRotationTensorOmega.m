function DerRottensOm = gtFedDerRotationTensorOmega(om,rotcomp)

% FUNCTION 
%
% Gives the derivative of the rotation tensor wrt. the rotation angle omega.
%
%
%
%
%

DerRottensOm = -rotcomp.cos*sind(om) + rotcomp.sin*cosd(om) ; % in [1/radian]

DerRottensOm = DerRottensOm*pi/180;  % in [1/degree]
