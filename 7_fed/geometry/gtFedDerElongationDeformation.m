function DerEloDef = gtFedDerElongationDeformation(n)

% FUNCTION 
%
% Gives the derivative of the elongation along a plane normal wrt. the deformation components.
%
%
%

DerEloDef = [0, 0, 0, n(1)*n(1), n(2)*n(2), n(3)*n(3), 2*n(2)*n(3), 2*n(1)*n(3), 2*n(1)*n(2)];

