function Srot = gtFedRotationTensor(om,rotcomp)
% FUNCTION Srot = gtFedRotationTensor(om,rotcomp)
%
% Computes the rotation tensor given the 3 matrices in Rodrigues' 
% rotation formula. Can be used for computing rotation around an arbitrary
% axis.
%
% Rodrigues's rotation formula describes a linear combination of
% matrices with cos(om) only and sin(om) only dependent terms:
%   Srot = Sconst + Scos*cos(om) + Ssin*sin(om)
%     Srot: is the rotation tensor: vec_rotated = Srot*vec
%     om  : rotation angle
%
% INPUT
%   om            - rotation angle (in degrees)
%   rotcomp.const - Sconst matrix
%   rotcomp.cos   - Scos matrix
%   rotcomp.sin   - Ssin matrix
%
% OUTPUT
%   Srot          - rotation matrix
%

Srot = rotcomp.const + rotcomp.cos*cosd(om) + rotcomp.sin*sind(om);

end