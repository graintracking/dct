function out = gtFedDerDfacPosition(dlab,Rottens,tn)
						 
%function out = gtFedDerDfacStrain(pln,om,Rottens,rotcomp,sinth,csam,dlab,...
%	             DerCenStrain,DerDvecStrain,pdet,tn,beamdir)
% FUNCTION 
%
% Gives the derivative of the diffraction vector multiplicator wrt. the strain components.
%
%
%
%
%

%DerRottensOm  = gtFedDerRotationTensorOmega(om,rotcomp) ;

%DerEloStrain  = gtFedDerElongationStrain(pln) ;

%DerPlnStrain  = gtFedDerPlaneNormStrain(pln) ;

%DerOmStrain   = gtFedDerOmegaStrain(pln,Rottens,DerRottensOm,...
% 			          DerPlnStrain,beamdir,DerEloStrain,sinth) ;
		
%DerCenStrain  = DerRottensOm*csam*DerOmStrain ;

%DerDvecStrain = gtFedDerDiffVectorStrain(pln,om,Rottens,rotcomp,sinth) ;

out = (-tn'./(tn'*dlab))*Rottens ;
