function DerUVDef = gtFedDerUVDeformation(pln,Rottens,csam,dlab,dfac,...
	  DerRottensOm,DerPlnDef,pdet,tn,Qdet,beamdir,DerEloDef,sinth,beamchroma)

% FUNCTION 
%
% Gives the derivative of the detector coordinates u,v wrt. the deformation components.
%
% csam, pdet is in voxels.
% Qdet is in unit that scales grain voxels into detector pixels.
%
%

% if ~exist('beamchroma','var')
% 	beamchroma = 'mono';
% end

% input pln in SAMPLE coord
DerOmDef   = gtFedDerOmegaDeformation(pln,Rottens,DerRottensOm,...
		          	DerPlnDef,beamdir,DerEloDef,sinth) ;
		
DerCenDef  = DerRottensOm*csam*DerOmDef ;

DerDvecDef = gtFedDerDiffVectorDeformation(pln,Rottens,DerRottensOm,...
	           DerPlnDef,beamdir,DerEloDef,sinth,beamchroma) ;

DerDfacDef = gtFedDerDfacDeformation(Rottens,csam,dlab,...
	              DerCenDef,DerDvecDef,pdet,tn) ;
						 
DerUVDef = Qdet*(DerCenDef + dlab*DerDfacDef + dfac*DerDvecDef) ;

