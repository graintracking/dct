function rotmatcomp = gtFedRotationMatrixComp(rotdir)
% rotmatcomp = gtFedRotationMatrixComp(rotdir)
%
% Given an arbitrary rotation axis in 3D space, returns the three matrices
% in Rodrigues's formula. The matrices can be used to compose the rotation 
% tensor. Considers right-handed rotation for a positive angle.
%
% To be used with COLUMN VECTORS, MULTIPLY FROM THE LEFT side! 
% Multiplication of row vectors from the right gives the wrong result!
%
% Rodrigues's rotation formula describes a linear combination of
% matrices with only cos(om) and only sin(om) dependent terms:
%   S = Sconst + Scos*cos(om) + Ssin*sin(om)
%     S:  rotation matrix: columnvector_rotated = S*columnvector
%     om: right-handed rotation angle
%
% INPUT
%   rotdir - rotation axis direction in LAB coordinates (3,1)
%
% OUTPUT
%   rotmatcomp.const = matrix Sconst (3,3)
%   rotmatcomp.cos   = matrix Scos   (3,3)
%   rotmatcomp.sin   = matrix Ssin   (3,3)
%

if size(rotdir) ~= [3,1]
	error('Input rotdir has to be a 3x1 column vector.')
end


if rotdir == [0; 0; 0] % no rotation (e.g. polychromatic case)
	
	rotmatcomp.const = [1 0 0; 0 1 0; 0 0 1];
	rotmatcomp.cos   = [0 0 0; 0 0 0; 0 0 0];
	rotmatcomp.sin   = [0 0 0; 0 0 0; 0 0 0];
	
else  % monochromatic case
	
	rotmatcomp.const = rotdir*rotdir';
	rotmatcomp.cos   = eye(3) - rotmatcomp.const;
	rotmatcomp.sin   = [ 0         -rotdir(3)  rotdir(2);...
		                 rotdir(3)          0 -rotdir(1);...
		                -rotdir(2)  rotdir(1)          0];
	
end

end % end of function