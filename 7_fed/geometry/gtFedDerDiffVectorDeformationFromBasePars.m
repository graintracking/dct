function DerDiffVecDef = gtFedDerDiffVectorDeformationFromBasePars(pln,om,rotcomp,...
	             beamdir,sinth,beamchroma)

% FUNCTION 
%
% Gives the derivative of the diffraction vector wrt. the deformation components.
%
% pln - plane normal in SAMPLE coord.
%
%
%

% if ~exist('beamchroma','var')
% 	beamchroma = 'mono';
% end


DerPlnDef    = gtFedDerPlaneNormDeformation(pln) ;


if strcmp(beamchroma,'poly')
	
	DerDiffVecDef = gtFedDerDiffVectorDeformation(pln,[],[],...
	             DerPlnDef,beamdir,[],[],'poly') ;
	
else
	
	DerEloDef    = gtFedDerElongationDeformation(pln) ;
	
	Rottens      = gtFedRotationTensor(om,rotcomp) ;
	
	DerRottensOm = gtFedDerRotationTensorOmega(om,rotcomp) ;
	
	DerDiffVecDef = gtFedDerDiffVectorDeformation(pln,Rottens,DerRottensOm,...
		              DerPlnDef,beamdir,DerEloDef,sinth);

end