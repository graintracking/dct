function uvw = gtFedPredictUVW(Rottens,dlab,csam,detrefpos,detnorm,Qdet,detrefuv,om,omstep)
% uvw = gtFedPredictUVW(Rottens,dlab,csam,detrefpos,detnorm,Qdet,detrefuv,om,omstep)
%
% Predicts a single spot location [u,v,w] on the detector plane.
% Diffraction vector dlab doesn't have to be normalised.
% csam,detrefuv,detrefpos must be in identical units. Qdet scales this unit
% into detector pixels.
%

uv = gtFedPredictUV(Rottens,dlab,csam,detrefpos,detnorm,Qdet,detrefuv);

if isempty(uv)
	uvw = [];
	return
end

uvw = [ uv; mod(om,360)/omstep ];

end % end of function