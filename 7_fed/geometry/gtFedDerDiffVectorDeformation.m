function DerDvecDef = gtFedDerDiffVectorDeformation(pln,Rottens,DerRottensOm,...
	             DerPlnDef,beamdir,DerEloDef,sinth,beamchroma)

% FUNCTION 
%
% Gives the derivative of the diffraction vector wrt. the deformation components.
%
%
%
%
%
%DerRottensOm = gtFedDerRotationTensorOmega(om,rotcomp) ;
%DerEloDef    = gtFedDerElongationDeformation(pln) ;
%DerPlnDef    = gtFedDerPlaneNormDeformation(pln) ;


if exist('beamchroma','var') && strcmp(beamchroma,'poly')
	
	DerDvecDef = -2*(pln*(beamdir'*DerPlnDef)+(beamdir'*pln)*DerPlnDef);
	
else

  DerDvecDef = 2*sinth*(-Rottens*pln*DerEloDef +...
	  DerRottensOm*pln*gtFedDerOmegaDeformation(pln,Rottens,DerRottensOm,...
  	DerPlnDef,beamdir,DerEloDef,sinth) +...
		Rottens*DerPlnDef) ;

end