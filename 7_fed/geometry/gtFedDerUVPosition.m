function out = gtFedDerUVPosition(Rottens,dlab,tn,Qdet)

% FUNCTION 
%
% Gives the derivative of the detector coordinates u,v wrt. the strain components.
%
%
%
%
%

out = Qdet*(Rottens + dlab*gtFedDerDfacPosition(dlab,Rottens,tn)) ;
