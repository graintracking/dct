function DerOmDef = gtFedDerOmegaDeformationFromBasePars(pln,om, ...
                    rotcomp,beamdir,sinth)

% FUNCTION 
%
% Gives the derivative of omega wrt. the deformation components.
%
% pln - plane normal in SAMPLE coordinates
%
%
%

Rottens = gtFedRotationTensor(om,rotcomp);

DerRottensOm = gtFedDerRotationTensorOmega(om,rotcomp);

DerPlnDef = gtFedDerPlaneNormDeformation(pln);

DerEloDef = gtFedDerElongationDeformation(pln);


tmp = (beamdir'*DerRottensOm*pln);

if tmp==0
	DerOmDef = [0 0 0 0 0 0 0 0 0];
else
	DerOmDef = 1/tmp*(sinth*DerEloDef - beamdir'*Rottens*DerPlnDef) ;
end