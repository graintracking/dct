function [om, pllab, plsigned, Srot, omind] = gtFedPredictOmega(pl, sinth, beamdir, rotdir, rotcomp)
%    FUNCTION  [om,pllab,plsigned, Srot] = gtFedPredictOmega(pl,sinth,beamdir,rotdir,rotcomp)
%
% Predicts the four omega angles where diffraction occurs for a plane normal pl
% given in SAMPLE coordinates (omega=0).
% In the output:
%   - om1-om3 is one Friedel pair, and om2-om4 is the other Friedel pair in an orthogonal setup
%   - om1 is always smaller than om2
% It takes the sin of theta as argument!
%
% INPUT
%    pl      - plane normal (3x1)
%    sinth   - corresponding sinus theta (Bragg angle) of plane normal
%    beamdir - beam direction LAB coordinates (3x1)
%    rotdir  - rotation axis direction LAB coordinates (3x1)
%    rotcomp - rotational matrix components
%
% OUTPUT
%    om       - the four omegas where reflections appear; contains NaN when
%               partial solution, and empty if no solution at all. (1x4)
%    pllab    - the plane normal in LAB coordinates in diffraction positions (3x4);
%               No. 3 and 4 are inverted after rotation (pointing oppposite to beam)
%               for handy further calculation.
%    plsigned - the original plane normal in SAMPLE coordinates; (3x4)
%               No. 3 and 4 are inverted.
%    Srot     - the rotation tensor specific to each corresponging omega
%

% % Polychromatic case
% if rotdir == [0 0 0]
%
%     om = 0;
%     pllab = -pl*sign(pl'*beamdir);
%     plsigned = pllab;
%
%     return
% end

om       = NaN(1,4);
plsigned = NaN(3,4);

% Diffraction condition 1
% Pl_lab and beamdir dotproduct equals -sin(th)
A = beamdir' * rotcomp.cos * pl;
B = beamdir' * rotcomp.sin * pl;
C = beamdir' * rotcomp.const * pl + sinth;

D = A^2 + B^2 - C^2;

if (D > 0)
    E = sqrt(D);
    om(1) = 2 * atand((-B+E) / (C-A));
    om(2) = 2 * atand((-B-E) / (C-A));
    plsigned(:, 1:2) = [pl pl];
end

% Diffraction condition 2
% Pl_lab and beamdir dotproduct equals +sin(th)
A = beamdir' * rotcomp.cos * pl;
B = beamdir' * rotcomp.sin * pl;
C = beamdir' * rotcomp.const * pl - sinth;

D = A^2 + B^2 - C^2;

if (D > 0)
    E = sqrt(D);
    om(3) = 2 * atand((-B+E) / (C-A));
    om(4) = 2 * atand((-B-E) / (C-A));
    plsigned(1:3, 3:4) = -[pl pl];
end

% Shape output
if all(isnan(om))
    om = [];
    pllab = [];
    plsigned = [];
    Srot = [];
    omind = [];
    return
end

om = mod(om, 360);
omind = [1; 2; 3; 4];

% Exchange om1 and om2 if om1 > om2
if (om(1) > om(2))
    om(1:2) = [om(2) om(1)];
    omind(1:2) = [omind(2) omind(1)];
end

% Rotation matrix for each omega
Srot = gtMathsRotationTensor(om, rotcomp);

% Plane normal in diffraction position
pllab = [ ...
    Srot(:, :, 1) * pl, Srot(:, :, 2) * pl, ...
    Srot(:, :, 3) * pl, Srot(:, :, 4) * pl ];

% Exchange om3 and om4 if needed, so that om1-om3 and om2-om4 are the
% opposite reflections (Friedel pairs in case of an orthogonal setup).
% Midplane of setup is defined by beam direction and rotation axis.

tempCrossProd = gtMathsCross(beamdir', rotdir');
or1 = tempCrossProd * pllab(:, 1);
or3 = tempCrossProd * pllab(:, 3);

if ((or1 * or3) > 0) % orientation is not opposite, exchange om3 to om4
    om(3:4) = [om(4) om(3)];

    omind(3:4) = [omind(4) omind(3)];

    Srot(:, :, [3 4]) = Srot(:, :, [4 3]);

    pllab(:, 3:4) = [pllab(:, 4) pllab(:, 3)];
end

pllab(:, 3:4) = -1 * pllab(:, 3:4);
end








