function [DefT,RotT,StrainT] = gtFedDeformationTensorFromComps(dc)

% Gives deformation, rotation and strain tensors from the def. components.
% This function contains the definition (including sign) of the deformation
% components. 
%
% SAMPLE coordinate system is orthogonal, right handed [X,Y,Z].
% Deformation components are defined as: 
%   d1 = r32, right-handed rotation around X axis
%   d2 = r13, right-handed rotation around Y axis
%   d3 = r21, right-handed rotation around Z axis
%   d4 = e11, normal strain component along X axis
%   d5 = e22, normal strain component along Y axis
%   d6 = e33, normal strain component along Z axis
%   d7 = e23, shear strain component in YZ plane
%   d8 = e13, shear strain component in XZ plane
%   d9 = e12, shear strain component in XY plane
%


DefT = [dc(4)        dc(9)-dc(3)  dc(8)+dc(2); ...
        dc(9)+dc(3)  dc(5)        dc(7)-dc(1); ...
        dc(8)-dc(2)  dc(7)+dc(1)  dc(6)      ];

			
if nargout == 1
	return
end
			

RotT = [ 0     -dc(3)  dc(2); ...
         dc(3)  0     -dc(1); ...
        -dc(2)  dc(1)  0    ];

			
StrainT = [dc(4)  dc(9)  dc(8); ...
           dc(9)  dc(5)  dc(7); ...
           dc(8)  dc(7)  dc(6)];


	 


	 
	 

			 