function [Udef,Upos] = gtFedDerUmatrix(pln,om,Rottens,rotcomp,csam,sinth,...
	                     dlab,dfac,pdet,tn,Qdet,beamdir,omstep,beamchroma)
% [Udef,Upos] = gtFedDerUmatrix(pln,om,Rottens,rotcomp,csam,sinth,...
%   	                        dlab,dfac,pdet,tn,Qdet,beamdir,omstep,[beamchroma])
%
% pln input is original plane normal in SAMPLE coordinates


if ~exist('beamchroma','var') || isempty(beamchroma)
	beamchroma = 'mono';
end


% Derivatives wrt. deformation
DerPlnDef    = gtFedDerPlaneNormDeformation(pln) ;
DerEloDef    = gtFedDerElongationDeformation(pln) ;

% Other derivatives
DerRottensOm = gtFedDerRotationTensorOmega(om,rotcomp) ;

DerUVDef = gtFedDerUVDeformation(pln,Rottens,csam,dlab,dfac,...
	         DerRottensOm,DerPlnDef,pdet,tn,Qdet,beamdir,DerEloDef,sinth,beamchroma);			 		 

% input pln is signed original in SAMPLE coordinates
DerOmDef = gtFedDerOmegaDeformation(pln,Rottens,DerRottensOm,...
           DerPlnDef,beamdir,DerEloDef,sinth) ;
				 
Udef = [DerUVDef; DerOmDef/omstep] ;


% Derivatove wrt. position

DerUVPos = gtFedDerUVPosition(Rottens,dlab,tn,Qdet) ;

Upos = [DerUVPos; [0 0 0]] ;

end % end of function
						