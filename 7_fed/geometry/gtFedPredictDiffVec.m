function dlab = gtFedPredictDiffVec(pllab, sinth, beamdir)
% dlab = gtFedPredictDiffVec(pllab, sinth, beamdir)
%
% Gives the diffraction vector in LAB coordinates for a plane normal also 
% given in LAB (!) coordinates (i.e. turned to the omega position).
%

% Needs to consider the plane normal pointing opposite to the beam:
pllab = -sign(beamdir' * pllab) * pllab;

% Diffraction vector:
dlab = 2 * sinth * pllab + beamdir ;

end % end of function