function Qdet = gtFedDetectorProjectionTensor(detdiru, detdirv, ...
                                           detscaleu, detscalev)
% 
% Qdet = gtFedDetectorProjectionTensor(detdiru, detdirv, detscaleu, detscalev)
%
% INPUT  
%    detdiru, detdirv    - detector u,v directions in LAB coordinates
%                          (3x1 unit vectors)
%    detscaleu,detscalev - detector u,v scaling
%                          (grain voxel / det. pixel size) or (lab units /
%                          det. pixel size) or other units
%                          
% OUTPUT
%    Qdet - projection matrix that transforms LAB into DETECTOR coordinates
%

% Make sure it's a column vector
detdiru = detdiru(:);
detdirv = detdirv(:);

Qdet = [(detdiru/sqrt(sum(detdiru.^2)))'*detscaleu ; ...
        (detdirv/sqrt(sum(detdirv.^2)))'*detscalev];

end % end of function