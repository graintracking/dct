function uv = gtFedPredictUV(Rottens,dlab,csam,detrefpos,detnorm,Qdet,detrefuv)
% FUNCTION uv = gtFedPredictUV(Rottens,dlab,csam,detrefpos,detnorm,Qdet,detrefuv)
%
% Predicts a single peak location [u,v,w] on the detector plane.
% Diffraction vector dlab doesn't have to be normalised.
% csam,detrefuv,detrefpos must be in identical units. Qdet scales their 
% units into detector pixels.
%

    % multiplicator for dlab
    [dfac, clab] = gtFedPredictDiffFac(Rottens, dlab, csam, detrefpos, detnorm);

    % dfac is empty if the beam is diffracted away from detector plane -> spot is not
    %  detected
    if isempty(dfac)
        uv = [];
        return
    end

    % u,v position on detector plane
    uv = detrefuv + Qdet*(clab + dfac*dlab - detrefpos);
end
