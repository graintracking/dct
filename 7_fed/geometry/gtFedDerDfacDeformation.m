function DerDfacDef = gtFedDerDfacDeformation(Rottens,csam,dlab,...
	             DerCenDef,DerDvecDef,pdet,tn)
						 
% FUNCTION 
%
% Gives the derivative of the diffraction vector multiplicator wrt. the deformation components.
%
%
%
%
%

%DerRottensOm  = gtFedDerRotationTensorOmega(om,rotcomp) ;

%DerEloStrain  = gtFedDerElongationStrain(pln) ;

%DerPlnStrain  = gtFedDerPlaneNormStrain(pln) ;

%DerOmStrain   = gtFedDerOmegaStrain(pln,Rottens,DerRottensOm,...
% 			          DerPlnStrain,beamdir,DerEloStrain,sinth) ;
		
%DerCenStrain  = DerRottensOm*csam*DerOmStrain ;

%DerDvecStrain = gtFedDerDiffVectorStrain(pln,om,Rottens,rotcomp,sinth) ;

DerDfacDef = (-tn'./(tn'*dlab)^2)*((tn'*dlab)*DerCenDef + (tn'*(pdet-Rottens*csam))*DerDvecDef) ;