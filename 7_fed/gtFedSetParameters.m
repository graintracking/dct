function fedpars = gtFedSetParameters(fedpars, defaults)
%     fedpars = gtFedSetParameters(fedpars, defaults)
%     ----------------------------------------------------
%     Set FED parameters in fedpars structure
%     Optional third input 'defaults' returns default parameters
%
%     Otherwise, use funky GUI gtModifyStructure to get parameters from the
%     user (based on default values)
%

% if default values demanded when calling function
if ~exist('defaults','var') || isempty(defaults)
  defaults=false;
end


% does the user want the default options? - how to handle running compiled?
if ~defaults
  check = inputwdefault('Use default FED parameters? [y/n]', 'n');
  if strcmpi(check, 'y')
    defaults=true;
  end
end


list = cell(0,4);
% static FED values
list(end+1,:) = [{'real_data'},{'Real data (true/false)'},{'logical'},{1}];
list(end+1,:) = [{'detscaleu'},{'voxelsize/pixelsize, u'},{'double'},{1}];
list(end+1,:) = [{'detscalev'},{'voxelsize/pixelsize, v'},{'double'},{1}];
list(end+1,:) = [{'beamchroma'},{'beam chomaticity (poly/mono)'},{'char'},{1}];
list(end+1,:) = [{'auto_gvsize'},{'auto element size / voxels'},{'double'},{1}];
list(end+1,:) = [{'gvsize'},{'element size'},{'double'},{1}];
list(end+1,:) = [{'max_elements_grain'},{'if auto, max elements per grain'},{'double'},{1}];
list(end+1,:) = [{'ngvsub'},{'mystery thingy'},{'double'},{1}];
list(end+1,:) = [{'blext'},{'pad when reading blobs'},{'double'},{1}];
list(end+1,:) = [{'solver'},{'which solver? (Flow/etc/etc)'},{'char'},{1}];
list(end+1,:) = [{'filename'},{'FED output filename'},{'char'},{1}];
list(end+1,:) = [{'wonly'},{'use Omega variations only (true/false)'},{'logical'},{1}];
list(end+1,:) = [{'dlim'},{'maximum allowed deformations [Rx Ry Rz e11 e22 e33 e23 e13 e12]'},{'double'},{1}];
list(end+1,:) = [{'sufac'},{'factor in calculating deformation steps'},{'double'},{1}];
list(end+1,:) = [{'T0strategy'},{'how to calculate T02 matrix (A/B)'},{'char'},{1}];
list(end+1,:) = [{'nphases'},{'How many phases (sets of parameters) in solver?'},{'double'},{1}];

% dynamic FED values
list(end+1,:) = [{'solver'},{'which solver? (Flow/etc/etc)'},{'char'},{2}];
list(end+1,:) = [{'dindex'},{'Which deformation to solve? [Rx Ry Rz e11 e22 e33 e23 e13 e12]'},{'double'},{2}];
list(end+1,:) = [{'loadbl'},{'List of which blobs to load'},{'double'},{2}];
list(end+1,:) = [{'niter'},{'Number of iterations in this phase'},{'double'},{2}];
list(end+1,:) = [{'crude_acceleration'},{'Crude acceleration of forward projector? true/false'},{'logical'},{2}];
list(end+1,:) = [{'usebl'},{'Which blobs to use?'},{'double'},{2}];
list(end+1,:) = [{'dsteplim'},{'Maximum change in deformation in a step'},{'double'},{2}];
list(end+1,:) = [{'dstepmode'},{'dstepmode (discr / continuous)'},{'char'},{2}];
list(end+1,:) = [{'dmeancorr'},{'Do dmean correction (true/false)'},{'logical'},{2}];
list(end+1,:) = [{'dsmoothing'},{'Maximum change in deformation in a step'},{'double'},{2}];
list(end+1,:) = [{'fbbsize'},{'Factor for calculating flow bounding box size'},{'double'},{2}];
list(end+1,:) = [{'fbbminsize'},{'Minimum flow bounding box size'},{'double'},{2}];


if ~defaults
    % get values from the user
 
    disp('Set the static FED values')
    fedpars = gtModifyStructure(fedpars, list, 1, 'Static FED parameters:');

    if fedpars.nphases < 1
        fedpars.nphases = 1;
    end

    % get rid of "excess" default phases
    if fedpars.nphases < 3
        for ii = fedpars.nphases+1 : 3
            fedpars.strategy = rmfield(fedpars.strategy, sprintf('s%d', ii));
        end
    end
    
    for ii = 1 : fedpars.nphases
        fprintf('Now set the dynamic FED values for phase %d', ii)
        fedpars.strategy.(sprintf('s%d', ii)) = gtModifyStructure(fedpars.strategy.(sprintf('s%d', ii)), list, 2, 'Dynamic FED parameters:');
    end
end

end % end of function
