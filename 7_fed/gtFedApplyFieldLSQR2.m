function [gv,bl]=gtFedApplyFieldLSQR2(gv,bl,bluvol,bluvolsol,fedpars)



nbl = length(bl);
nv  = length(gv);

dlim = fedpars.dlim;
ngv = fedpars.ngv;
gvsize = fedpars.gvsize;
grsize = fedpars.grainsize;


if strcmp(fedpars.dstepmode,'discr')
	dstepdiscrete = true;
else
	dstepdiscrete = false;
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Start with measured mean omegas of the blobs

disp(' Applying LSQR field...  ')

dnewvol = zeros(ngv(1),ngv(2),ngv(3),9);


% Use the 4 best blobs
% for i = 1:nbl
% 	blerr(i) = bl(i).err;
% end
% [minvals,mininds] = sort(blerr);
% wbl = mininds(1:4);

%ngvsub = subsize./gvsize;


ngvsub = fedpars.ngvsub;
nvsub = ngvsub(1)*ngvsub(2)*ngvsub(3);

voxsubsize = ngvsub.*gvsize;
nvoxsub = voxsubsize(1)*voxsubsize(2)*voxsubsize(3);

vox2gvind = fedpars.vox2gvind;
vox2gvindne = fedpars.vox2gvindne;
vox2gvindp = fedpars.vox2gvindp;


if mod(ngvsub,1)~= 0
	error('Subsampling')
end

for is = 1:ngv(1)/ngvsub(1)
	for js = 1:ngv(2)/ngvsub(2)
		for ks = 1:ngv(3)/ngvsub(3)
			
			Id = [];
			intdvec = [];

			voxsubindl(1) = (is-1)*voxsubsize(1)+1;
			voxsubindh(1) = is*voxsubsize(1);
			voxsubindl(2) = (js-1)*voxsubsize(2)+1;
			voxsubindh(2) = js*voxsubsize(2);
			voxsubindl(3) = (ks-1)*voxsubsize(3)+1;
			voxsubindh(3) = ks*voxsubsize(3);

			
			% gv elements in the subset:
			vox2gvindsub = vox2gvind(voxsubindl(1):voxsubindh(1),voxsubindl(2):voxsubindh(2),voxsubindl(3):voxsubindh(3));
			vox2gvindnesub = vox2gvindne(voxsubindl(1):voxsubindh(1),voxsubindl(2):voxsubindh(2),voxsubindl(3):voxsubindh(3),:);
			vox2gvindpsub = vox2gvindp(voxsubindl(1):voxsubindh(1),voxsubindl(2):voxsubindh(2),voxsubindl(3):voxsubindh(3),:);

			% active gv indices
			%gvactind = unique(vox2gvindnesub(:));
			gvactind = unique(vox2gvindsub(:));

			% original to active gv indices
			gvorig2actind = zeros(nv,1);
			for i = 1:length(gvactind)
				gvorig2actind(gvactind(i)) = i;
			end		
			
			
			% Loop through blobs
			for ib = fedpars.usebl
			
				sprintf('Blob #%d',ib)
				tic
				
				ublib = bluvol(voxsubindl(1):voxsubindh(1),voxsubindl(2):voxsubindh(2),voxsubindl(3):voxsubindh(3),1,ib);
				vblib = bluvol(voxsubindl(1):voxsubindh(1),voxsubindl(2):voxsubindh(2),voxsubindl(3):voxsubindh(3),2,ib);
				wblib = bluvol(voxsubindl(1):voxsubindh(1),voxsubindl(2):voxsubindh(2),voxsubindl(3):voxsubindh(3),3,ib);

	      % gvind = floor((gvox-1)./gvsize)+1;
			
				% Neighbouring voxel coordinates
				mu  = mod([ublib(:) vblib(:) wblib(:)],1);
				flu = floor([ublib(:) vblib(:) wblib(:)]);
				clu = ceil([ublib(:) vblib(:) wblib(:)]);

				blibactive = false(bl(ib).bbsize);
			
				fcorig(:,1) = sub2ind(bl(ib).bbsize,flu(:,1),flu(:,2),flu(:,3));
				fcorig(:,2) = sub2ind(bl(ib).bbsize,flu(:,1),flu(:,2),clu(:,3));
				fcorig(:,3) = sub2ind(bl(ib).bbsize,flu(:,1),clu(:,2),flu(:,3));
				fcorig(:,4) = sub2ind(bl(ib).bbsize,flu(:,1),clu(:,2),clu(:,3));
				fcorig(:,5) = sub2ind(bl(ib).bbsize,clu(:,1),flu(:,2),flu(:,3));
				fcorig(:,6) = sub2ind(bl(ib).bbsize,clu(:,1),flu(:,2),clu(:,3));
				fcorig(:,7) = sub2ind(bl(ib).bbsize,clu(:,1),clu(:,2),flu(:,3));
				fcorig(:,8) = sub2ind(bl(ib).bbsize,clu(:,1),clu(:,2),clu(:,3));

				% Acti
				blibactive(fcorig(:)) = true;
				
				
				% Single indices of active blob voxels:
				actind = (1:length(blibactive(:)))';
				actind(~blibactive(:)) = [];

				intdbl = bl(ib).intd(actind) ;
				intdvec = [intdvec; intdbl];

				orig2actind = zeros(length(blibactive(:)),1);

				for i = 1:length(actind)
					orig2actind(actind(i)) = i;
				end


				% Contribution of each element for each active blob voxel

				
				% U0blibsub for interpolation
				%U0blibsub = zeros([ngvsub 3*9]);
				

				% Active indices of affected blob voxels:
				fcactind = zeros(size(fcorig));
				fcactind(:) = orig2actind(fcorig(:));

				
				% Intensity of the 8 blob voxels after distributing the intensities, depending on u position of the grain
				% voxel:
				% 			Iubl(vind1) = (1-mu(1))*(1-mu(2))*(1-mu(3));
				% 			Iubl(vind2) = (1-mu(1))*(1-mu(2))*mu(3);
				% 			Iubl(vind3) = (1-mu(1))*mu(2)*(1-mu(3));
				% 			Iubl(vind4) = (1-mu(1))*mu(2)*mu(3);
				% 			Iubl(vind5) = mu(1)*(1-mu(2))*(1-mu(3));
				% 			Iubl(vind6) = mu(1)*(1-mu(2))*mu(3);
				% 			Iubl(vind7) = mu(1)*mu(2)*(1-mu(3));
				% 			Iubl(vind8) = mu(1)*mu(2)*mu(3);


				% Derivatives of the 8 blob voxel intensities wrt. [u,v,w] position:

				diperdu = zeros(3,nvoxsub,8);
				
				diperdu(1,:,1) = -(1-mu(:,2)).*(1-mu(:,3));
				diperdu(1,:,2) = -(1-mu(:,2)).*mu(:,3);
				diperdu(1,:,3) = -mu(:,2).*(1-mu(:,3));
				diperdu(1,:,4) = -mu(:,2).*mu(:,3);
				diperdu(1,:,5) = (1-mu(:,2)).*(1-mu(:,3));
				diperdu(1,:,6) = (1-mu(:,2)).*mu(:,3);
				diperdu(1,:,7) = mu(:,2).*(1-mu(:,3));
				diperdu(1,:,8) = mu(:,2).*mu(:,3);

				diperdu(2,:,1) = -(1-mu(:,1)).*(1-mu(:,3));
				diperdu(2,:,2) = -(1-mu(:,1)).*mu(:,3);
				diperdu(2,:,3) = (1-mu(:,1)).*(1-mu(:,3));
				diperdu(2,:,4) = (1-mu(:,1)).*mu(:,3);
				diperdu(2,:,5) = -mu(:,1).*(1-mu(:,3));
				diperdu(2,:,6) = -mu(:,1).*mu(:,3);
				diperdu(2,:,7) = mu(:,1).*(1-mu(:,3));
				diperdu(2,:,8) = mu(:,1).*mu(:,3);

				diperdu(3,:,1) = -(1-mu(:,1)).*(1-mu(:,2));
				diperdu(3,:,2) = (1-mu(:,1)).*(1-mu(:,2));
				diperdu(3,:,3) = -(1-mu(:,1)).*mu(:,2);
				diperdu(3,:,4) = (1-mu(:,1)).*mu(:,2);
				diperdu(3,:,5) = -mu(:,1).*(1-mu(:,2));
				diperdu(3,:,6) = mu(:,1).*(1-mu(:,2));
				diperdu(3,:,7) = -mu(:,1).*mu(:,2);
				diperdu(3,:,8) = mu(:,1).*mu(:,2);

				% Blob voxel intensity derivatives wrt. u positions of grain
				% voxels in the given blob:
				Iubl = zeros(length(actind),nvoxsub*3);

				Iu1stind = repmat(fcactind(:)',3,1);
				Iu1stind = Iu1stind(:);
				Iu2ndind = repmat((1:nvoxsub*3)',8,1);
				Iusingleind = sub2ind(size(Iubl),Iu1stind,Iu2ndind);
				
				%Iubl(Iusingleind) = Iubl(Iusingleind) + diperdu(:);
				Iubl(:) = accumarray(Iusingleind,diperdu(:),[length(Iubl(:)) 1]);
		
				% u derivatives of grain voxels wrt. all active d components:
				U0bl = zeros(nvoxsub*3,length(gvactind)*9);
				for i = 1:nvoxsub
					
					[v1,v2,v3] = ind2sub(voxsubsize,i);
					rows = 3*i-2:3*i;
					
% 					gvorig = vox2gvindsub(i);        % original gv element index
% 					gvact  = gvorig2actind(gvorig);  % active gv element index
% 					U0bl(rows, 9*gvact-8:9*gvact) = gv(gvorig).U0(:,:,ib);

					for k = 1:8
						gvorig = vox2gvindnesub(v1,v2,v3,k); % original gv element index
						gvact  = gvorig2actind(gvorig);  % active gv element index
						
						if gvact~=0
							cols = 9*gvact-8:9*gvact;
							U0bl(rows,cols) = U0bl(rows,cols) + gv(gvorig).U0(:,:,ib)*vox2gvindpsub(v1,v2,v3,k);
						end
					end
					
				end

				% Intensity derivative wrt. d components:
				Idbl = Iubl*U0bl;

				Id = [Id; Idbl];

				
				%%%%%%%%%%%%%%%%%
				ddm = zeros(length(gvactind)*9,1);
				for j = 1:length(gvactind)
					gvact = gvactind(j);
					ddm(9*j-8:9*j) =  gv(gvact).dm-gv(gvact).d;
				end
				diblm = Idbl*ddm;
				iblm = intdbl + diblm;

				disp('error:')
				disp([sum(intdbl(:).*intdbl(:)) sum(iblm(:).*iblm(:))])

				%%%%%%%%%%%%%%%%%%%
				
				
				
				
				% 				% Neighbouring elements around the grain voxel:
				% 				gvnbslx = max(iv-1,1);
				% 				gvnbsly = max(jv-1,1);
				% 				gvnbslz = max(kv-1,1);
				%
				% 				gvnbshx = min(iv+1,1);
				% 				gvnbshy = min(jv+1,1);
				% 				gvnbshz = min(kv+1,1);
				%
				% 				gvnbsx = gvnbslx:gvnbshx;
				% 				gvnbsy = gvnbsly:gvnbshy;
				% 				gvnbsz = gvnbslz:gvnbshz;
				%
				% 				ngvnbs = gvnbsx*gvnbsy*gvnbsz;
				%
				% 				for in = gvnbsx
				% 					for jn = gvnbsy
				% 						for kn = gvnbsz
				% 							Uvbl(3*j-2:3*j, 9*j-8:9*j) = gv(j).U0(:,:,i);
				%
				% 							Uvbl(3*j-2:3*j, 9*j-8:9*j) = gv(j).U0(:,:,i)/ngvnbs;
				%
				% 						end
				% 					end
				% 				end
				
				toc
				
			end % of blob
	

			% Least squares fit of all active d components:
			sprintf(' Fitting d components ...')
			tic

			disp('No. of rows and colomns in Id, and their ratio:')
			disp([size(Id) size(Id,1)/size(Id,2)])

			%bluvoldif = 0;
			
% 			bluvoldif = floor(bluvol)-floor(bluvolsol);
% 			disp('Total no. of [u,v,w] coords:')
% 			length(bluvol(:))
% 			disp('Error in floor position [0 1 2 3 >3]:')
% 			disp([sum(bluvoldif(:)==0) sum(abs(bluvoldif(:))==1) sum(abs(bluvoldif(:))==2) ...
% 			sum(abs(bluvoldif(:))==3) sum(abs(bluvoldif(:))>3)])
			
			%dsollsq = lsqr(Id,-intdvec,[],100);
			%dsoldiv = mldivide(Id,-intdvec);
			
			%dlowb = -repmat(fedpars.dsteplim,nvsub,1);
			%dupb  = repmat(fedpars.dsteplim,nvsub,1);
			%d0    = zeros(size(Id,2),1);
			%dsol = lsqlin(Id,-intdvec,[],[],[],[],dlowb,dupb,d0);

			   %intdlim = 10*[ones(length(intdvec),1); -ones(length(intdvec),1)];
			   %dsol2 = lsqlin(Id,-intdvec,[Id; -Id],intdlim,[],[],dlowb,dupb,d0);

			dsol = Id\(-intdvec);
			
			
			%%%%%
			%dsol = ddm;
			%%%%
			
% 			Isol  = Id*dsol+intdvec;
% 			%Isol2 = Id*dsol2+intdvec;
% 			Isolm = Id*ddm+intdvec;
% 
% 			close all
% 			
% 			bins = -0.001:0.00001:0.001;
% 			%figure, hist(dsollsq,bins)
% 			figure, hist(ddm,bins)
% 			figure, hist(dsol,bins)
% 			figure, hist(dsol-ddm,bins)
% 			%figure, hist(dsol2-ddm,bins)
% 
% 			bins = -20:0.1:20;
% 			figure, hist(intdvec,bins)
% 			figure, hist(Isol,bins)
% 			%figure, hist(Isol2,bins)
% 			figure, hist(Isolm,bins)
% 
% 			sum(Isol(:).*Isol(:))
% 			%sum(Isol2(:).*Isol2(:))
% 			sum(Isolm(:).*Isolm(:))
			
			
			
			% Store new d components
			
			for i = 1:length(gvactind)
				gvorig = gvactind(i);
				
				%gvorig = vox2gvindsub(1); % !!!!
				
				% active gv index
				%gvact = gvorig2actind(gvorig);
								
				
				% Change to be applied to d:
				ddact = dsol(9*i-8:9*i);

				% Discretize d change
				if dstepdiscrete
					ddact = sign(ddact).*fedpars.dsteplim;
				else
					ddact = min(ddact,fedpars.dsteplim);
					ddact = max(ddact,-fedpars.dsteplim);
				end

				dnew = gv(gvorig).d + ddact;
				dnew = min(dnew,dlim);
				dnew = max(dnew,-dlim);

				gv(gvorig).do = gv(gvorig).d; % old d value
				gv(gvorig).d  = dnew;

				dnewvol(gv(gvorig).ind(1),gv(gvorig).ind(2),gv(gvorig).ind(3),:) = dnew;

				
				gv(gvorig).solsr = zeros(3,nbl);

				
			end

			toc
			
		end % of subset ks
	end % of subset js
end % of subset is


	

	
						
			
			
			
			



if fedpars.dsmoothing
	% Smoothing of solution by taking the mean values in the neighbourhood of
	% a voxel
	disp(' Smoothing d components over elements...')
	
	for i = 1:ngv(1)
		for j = 1:ngv(2)
			for k = 1:ngv(3)

				lim1l = max(i-1,1);
				lim1h = min(i+1,ngv(1));
				lim2l = max(j-1,1);
				lim2h = min(j+1,ngv(2));
				lim3l = max(k-1,1);
				lim3h = min(k+1,ngv(3));

% 				for id = 1:9
% 					dfvals = dnewvol(lim1l:lim1h,lim2l:lim2h,lim3l:lim3h,id);
% 					gv(sub2ind(ngv,i,j,k)).d(id) = mean(dfvals(:));
% 				end
				
				for id = 1:9
					dvals(1) = dnewvol(lim1l,j,k,id);
					dvals(2) = dnewvol(lim1h,j,k,id);
					dvals(3) = dnewvol(i,lim2l,k,id);
					dvals(4) = dnewvol(i,lim2h,k,id);
					dvals(5) = dnewvol(i,j,lim3l,id);
					dvals(6) = dnewvol(i,j,lim3h,id);
					dvals(7) = dnewvol(i,j,k,id);
					
					gv(sub2ind(ngv,i,j,k)).d(id) = mean(dvals(:));
				end		
				

			end
		end
	end

else
	disp(' No smoothing on d components applied...')
end


% Save d difference from current step:
for i=1:nv
	gv(i).dd = gv(i).d - gv(i).do;
end


toc




