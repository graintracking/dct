function b = gtFedBlobDefinition()

% Crystallographic info:
b.IDfed     = NaN;      
b.IDgrain   = NaN;
b.difspotID = NaN;      % difspot ID
b.hkl       = NaN(1,3); % {hkl}
b.hklsp     = NaN(1,3); % specific{hkl}
b.thetatype = NaN(1,1); % thetatype
b.pl0       = NaN(1,3); % initial plane normal in diffracting position
b.plsam     = NaN(1,3); % initial plane normal in SAMPLE coordinates
b.dfac0     = NaN     ; % initial diffraction vector multiplicator
b.dvec0     = NaN(1,3); % initial diffraction vector

% Diffraction angles:
b.eta0    = NaN;        % initial eta
b.om0     = NaN;        % initial omega
b.th0     = NaN;        % initial theta
b.sinth0  = NaN;        % initial sin(theta)

% Blob position
b.bbsize  = NaN(1,3);   % bounding box size
b.bbu     = NaN(1,2);   % boundaries in u in image coordinates
b.bbv     = NaN(1,2);   % boundaries in v in image coordinates
b.bbw     = NaN(1,2);   % boundaries in w in image coordinates
b.mbbsize = NaN(1,3);   % measured bounding box size
b.mbbu    = NaN(1,2);   % measured boundaries in u in image coordinates
b.mbbv    = NaN(1,2);   % measured boundaries in v in image coordinates
b.mbbw    = NaN(1,2);   % measured boundaries in w in image coordinates
b.bbor    = NaN(1,3);   % blob origin [u=0,v=0,w=0] in image coordinates
%b.bbcent=NaN(1,3);      % blob center (integer) [u,v,w] in image coordinates
b.addbl   = NaN(1,3);   % blob volume extension beyond reference blob (is usually ulim)
b.ulim    = NaN(1,3);   % +-uc limit for all elements (at extreme strain combination)
b.u0im    = NaN(1,3);   % initial centroid [u,v,w] in image coordinates
b.u0bl    = NaN(1,3);   % initial centroid [u,v,w] in blob coordinates

% Transformation tensors:
b.S0     = NaN(3,3);    % initial blob S rotation tensor
b.Q0     = NaN(2,3);    % initial blob Q
b.U0     = NaN(3,9);    % initial blob U mean
b.Umax   = NaN(3,9);    % initial blob U maximum

% Centre of mass position [u,v,w]
b.com0bl = NaN(1,3);    % initial in blob coordinates
b.com0im = NaN(1,3);    % initial in image coordinates
b.combl  = NaN(1,3);    % actual in blob coordinates
b.comim  = NaN(1,3);    % actual in image coordinates
b.commbl = NaN(1,3);    % measured in blob coordinates
b.commim = NaN(1,3);    % measured in image coordinates

% Intensity distribution (volumes)
b.int0   = NaN;         % initial
b.int    = NaN;         % actual
b.intm   = NaN;         % measured
b.intd   = NaN;         % difference between measured and actual

b.err = NaN(1,1);       % error figure based on b.intd
b.su  = NaN(1,3);       % 

end % end of function
