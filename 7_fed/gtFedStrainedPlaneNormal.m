function [nn,drel]=gtFedStrainedPlaneNormal(n,G)
% FUNCTION [nn,drel]=gtFedStrainedPlaneNormal(n,G)
%
% Gives the new exact orientation (nn) of a plane normal n due to a deformation 
% tensor G (9 distinct components), and also the relative d-spacing after 
% the deformation (drel).
% The results are exact, since no linear approximation is used. 
% Both n and G must be given in the same reference.
%

% Get an arbitrary vector e1 perpendicular to n:
[maxval,maxloc]=max(abs(n));

if     maxloc==1
	e1=cross(n,[0; 1; 0]);  % close to Z axis 
elseif maxloc==2
	e1=cross(n,[0; 0; 1]);  % close to X axis 
elseif maxloc==3
	e1=cross(n,[1; 0; 0]);  % close to Y axis 
end

% e2 perpendicular to n and e1, i.e. e1 and e2 define the plane n
e2=cross(e1,n);

% flip e2 if needed to get them right handed:
if cross(e1,e2)'*n<0
  e2=-e2;
end

% e-s in deformed state:
e1n=G*e1+e1;
e2n=G*e2+e2;

% new deformed n
nn=cross(e1n,e2n);

% flip it if needed
if nn'*n<0
	nn=-nn;
end

% normalise
nn=nn/norm(nn);

% new relative d-spacing (exact calculation, non-linear in the G components)
fvec = n + G*n;   % change along original plane normal

% new normalised d-spacing
drel = dot(nn,fvec);


