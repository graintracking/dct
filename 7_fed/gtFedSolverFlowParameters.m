function stagepars = gtFedSolverFlowParameters

% STRATEGY for the iterations
%   Parameters for each stage are defined this way: 
%     fedpars.strategy.s1.solver = 'Flow'; fedpars.strategy.s1.dindex = ...
%     fedpars.strategy.s2.solver = 'LSQR'; fedpars.strategy.s2.dindex = ...
%     etc.
%   fedpars.strategy.

stagepars.solver     = 'Flow';       % which solver to use
stagepars.niter      = 10;           % number of steps (iterations) in this stage
stagepars.dindex     = [1 1 1 1 1 1 1 1 1]; % which def. comps. are sought in solution
stagepars.usebl      = 1:nbl;        % which blobs to use in solution
stagepars.dsteplim   = 0.0002*ones(9,1); % maximum absolute change in def. comps. in each step
stagepars.crude_acceleration = true; % use of rough but fast forward projection
stagepars.T0strategy = 'B';          % update T matrix in each step
stagepars.dsmoothing = 1;            % smoothing of def. comps. (between 0 and 1; 0=no; 1=mean of neighbouring the elements)
stagepars.gaugemult  = 0.7;          % size of gauge volume
stagepars.gaugemax   = [10 10 10];          % size of gauge volume
stagepars.gaugemin   = [4 4 4];          % size of gauge volume
%stagepars.blob       = true ;        % ????
stagepars.sufac      = ones(3,nbl/2);% flow speed factor of volume elements
stagepars.sumax      = Inf(3,nbl/2); % maximum flow speed of volume elements
stagepars.dstepmode  = 'cont';       % d component update mode continuous ('cont') or discrete ('discr')