function gtFedShowDifBlob(nbl,bl,wint,cmap)

% vol=bl(nbl).int(:,:,:);
% vol=permute(vol,[2,1,3]);
% 
% 
% GtVolView(vol,'plane','zx','slicendx',bl(nbl).cent(3)
% xlabel('u - horizontal')  % GtVolView will change it back
% ylabel('v - vertical')
% 
% fprintf('vol=bl(%d).int(:,:,:);',nbl)
% disp('vol=permute(vol,[2,1,3]);');
% %fprintf('GtVolView(vol,''plane'',''zx'',''slicendx'',%d)',bl(nbl).cent(3)-bl(nbl).bbw(1))
% fprintf('GtVolView(vol,''slicendx'',%d)',bl(nbl).cent(3)-bl(nbl).bbw(1))
% %disp('xlabel(''u - horizontal'')')
% %disp('ylabel(''v - vertical'')')

if ~exist('wint','var')
	wint = 'act';
end
if ~exist('cmap','var')
	cmap = 0;
end

if strcmp(wint,'ini')
	vol = zeros(size(bl(nbl).int0)+[1 1 1]);
	vol(2:end,2:end,2:end) = bl(nbl).int0;
elseif strcmp(wint,'mes')
	vol = zeros(size(bl(nbl).intm)+[1 1 1]);
	vol(2:end,2:end,2:end) = bl(nbl).intm;
elseif strcmp(wint,'act')
	vol = zeros(size(bl(nbl).int)+[1 1 1]);
	vol(2:end,2:end,2:end) = bl(nbl).int;
elseif strcmp(wint,'dif')
	vol = zeros(size(bl(nbl).intd)+[1 1 1]);
	vol(2:end,2:end,2:end) = bl(nbl).intd;
end
	
vol = permute(vol,[2,1,3]);

s = warning('query', 'all');
warning off

if cmap == 0
	h = dipshow(vol,'lin','name',sprintf('Blob #%d ',nbl));
else
	h = dipshow(vol,[0 max(vol(:))],'name',sprintf('Blob #%d  ',nbl));
end	

warning(s)

set(h,'numbertitle','off')
dipshow(h,'ch_slice',round(bl(nbl).commbl(3)))


