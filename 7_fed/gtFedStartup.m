

addpath /users/reischig/matlabDCT/zUtil_Cryst/
addpath /users/reischig/matlabDCT/zUtil_FED2/
addpath /users/reischig/matlabDCT/zUtil_FED2/geometry/
addpath /users/reischig/matlabDCT/zUtil_FED2/Ccode_SpreadInt/
addpath /users/reischig/matlabDCT/zUtil_FED2/FedTest/

cd /users/reischig/matlabDCT/zUtil_FED2/Ccode_SpreadInt/

gtFedInitialize

cd /users/reischig/matlabDCT/zUtil_FED2/FedTest/

dbstop if error

