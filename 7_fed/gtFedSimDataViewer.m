function [vol, RGBmap]=gtFedSimDataViewer(gv, fedpars, d_components)

% function vol=gtFedSimDataViewer(gv, fedpars, d_components)
%
% crude function to:
% reads the intial and calculated distortion data out of the gv variable
% for display in GtVolView
% GtVolView shows: [initial calcaulated difference]
% for the d components specified (ie 9 = Rz)

% fedpars.vox2gvind relates gv to voxels in the grain volume

volin=zeros(fedpars.grainsize);
volout=zeros(fedpars.grainsize);
RGBmap=[];RGBmap2=[];RGBmap3=[];

if ~all(d_components==[7 8 9]) % normal way to do things
    
for j=d_components

for x=1:fedpars.grainsize(1)
    for y=1:fedpars.grainsize(2)
        for z=1:fedpars.grainsize(3)
    
            i=fedpars.vox2gvind(x,y,z);
            
            if i~=0
                
            if ~fedpars.real_data
                volin(x,y,z)=gv(i).dm(j);
            end
            
            volout(x,y,z)=gv(i).d(j);
            end
            
    
        end
    end
end


if ~fedpars.real_data 
    vol=[volin volout volin-volout];
else
    vol=volout;
end

GtVolView(vol, 'cmap', jet);
set(gcf, 'name', sprintf('distortion component %d', j))

end

    
    

else %  special case with RGB colormap
    
    
for x=1:fedpars.grainsize(1)
    for y=1:fedpars.grainsize(2)
        for z=1:fedpars.grainsize(3)
   
            i=fedpars.vox2gvind(x,y,z);
            
            if i~=0
            volout(x,y,z)=i;
            
            d=gv(i).d(7:9);
            RGBmap(i+1, 1:3)=d;
            
            if ~fedpars.real_data 
            dm=gv(i).dm(7:9);
            RGBmap2(i+1, 1:3)=dm;
            
            ddif=gv(i).dm(7:9)-gv(i).d(7:9);
            RGBmap3(i+1, 1:3)=ddif;
            end
            
            end             
        
        end
    end
end 
 
% scale RGB map
normalise=max(abs(RGBmap(:)));
RGBmap=RGBmap+normalise;
RGBmap=RGBmap/(2*normalise);
RGBmap(1,:)=[0 0 0];
vol=volout;
GtVolView(volout, 'cmap', RGBmap)

% make a key
figure

keyx=repmat(1:30, 30, 1)/30;
keyx=[keyx; zeros(5,30); keyx; zeros(5, 30); keyx];
keyy=repmat([1:30 0 0 0 0 0 1:30 0 0 0 0 0 1:30]', 1, 30)/30;
keyz=[ones(30); zeros(5, 30); ones(30)*15; zeros(5, 30); ones(30)*30]/30;

key=cat(3, keyx, keyy, keyz);



end








