function gtFedPlotErrors(gv)

for i = 1:length(gv)
	for k = 1:9 
		err(i,k) = gv(i).d(k) - gv(i).dm(k);
		ds(i,k)  = gv(i).d(k);
		dms(i,k) = gv(i).dm(k);
	end
end

for k = 1:9
	figure('name',sprintf('Error in d%d',k))
	hist(err(:,k),100)
end

for k = 1:9
	figure('name',sprintf('Correlation of d%d',k))
	plot(dms(:,k),ds(:,k),'.')
	axis equal
end