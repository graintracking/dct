function [gv,bl]=gtFedApplyFieldFlow(gv,bl,fedpars)

% Andy comment - modified to handle case where number of elements in gv < ngv(1)*ngv(2)*ngv(3)
% - ie that only filled elements are recorded
%
% further comment - try median filtering for real data? might work
% better...  edge preserving...
%
% note that the smoothing applied is not a simple mean filter any more, but
% the mean of the local value and the mean of the neighbourhood.
 
% modified for case where we have some blobs disabled - ie fedpars.usebl is
% not all blobs
%... or does this not work with C projector?  might have to always project
% all blobs.  Should still be able to apply solver to subset of blobs.




nbl = length(bl);
nv  = length(gv);

dlim = fedpars.dlim;
ngv = fedpars.ngv;

allfu = zeros(nv,length(fedpars.usebl));
allfv = zeros(nv,length(fedpars.usebl));
allfw = zeros(nv,length(fedpars.usebl));

%fedpars.dstep = 0.00005*ones(9,1);
%fedpars.dstepmode = 'discr';


if strcmp(fedpars.dstepmode,'discr')
    dstepdiscrete = true;
else
    dstepdiscrete = false;
end

if fedpars.wonly
    wonly = true;
else
    wonly = false;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Start with measured mean omegas of the blobs

disp(' Applying flow field...  ')
tic

if isempty(fedpars.fbbsize)
    fedpars.fbbsize = 0.1;
end

% note - we may not be using all the blobs.  Therefore, 

for i = 1:length(fedpars.usebl)

    b=fedpars.usebl(i); % get the right blob.
    
    % i is the index into variable with the length of usebl (allfu/v/w/, gv: T0, fu, su )
    % b is the index into variables with full length (all bl fields, gv: uc, ucm, U0, ucerr, uc0bl, ucbl) 
    
    % Force depends on 1) intensity difference between measured and simulated
    % and 2) distance (linearly decreasing with distance from actual u).
    % Size of sampled volume is a fraction of the whole blob.

    % bbsize is odd
    %fbbsize = bl(i).ulim + mod(bl(i).ulim,2);



    % bbsize is odd
    fbbsizebase = max(bl(b).ulim*fedpars.fbbsize,fedpars.fbbminsize);
    fbbsize =  fbbsizebase - mod(fbbsizebase,2);

    %fbbsize = [2 2 2];

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % 	fforceu = zeros(fbbsize);
    % 	fforcev = zeros(fbbsize);
    % 	fforcew = zeros(fbbsize);
    %
    % 	%freg = zeros([bl(i).ulim 3]);
    % 	%uregcent = [size(ureg,1); size(ureg,2); size(ureg,3)]/2 + 0.5;
    %
    % 	for j=1:fbbsize(1)/2
    % 		fforceu(j,:,:) = j;
    %   	fforceu(fbbsize(1)+1-j,:,:) = -j;
    % 	end
    % 	fforceu = fforceu/sum(abs(fforceu(:)));
    %
    % 	for j=1:fbbsize(2)/2
    % 		fforcev(:,j,:) = j;
    %   	fforcev(:,fbbsize(2)+1-j,:) = -j;
    % 	end
    % 	fforcev = fforcev/sum(abs(fforcev(:)));
    %
    % 	for j=1:fbbsize(3)/2
    % 		fforcew(:,:,j) = j;
    %   	fforcew(:,:,fbbsize(3)+1-j) = -j;
    % 	end
    % 	fforcew = fforcew/sum(abs(fforcew(:)));
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


    % fforce0 is the multiplicator factor as a function of distance in u,v,w.

    fforceu0 = zeros(fbbsize);
    fforcev0 = zeros(fbbsize);
    fforcew0 = zeros(fbbsize);

    %freg = zeros([bl(i).ulim 3]);
    %uregcent = [size(ureg,1); size(ureg,2); size(ureg,3)]/2 + 0.5;

    if ~wonly
        for j=1:fbbsize(1)/2
            fforceu0(j,:,:) = j; %j^2; %1; %j;
            fforceu0(fbbsize(1)+1-j,:,:) = -j; %-j^2; %-1; %-j;
            fforceu0 = fforceu0/sum(abs(fforceu0(:)));
        end

        for j=1:fbbsize(2)/2
            fforcev0(:,j,:) = j; %j^2; %1; %j;
            fforcev0(:,fbbsize(2)+1-j,:) = -j; %-j^2; %-1; %-j;
            fforcev0 = fforcev0/sum(abs(fforcev0(:)));
        end
    end

    for j=1:fbbsize(3)/2
        fforcew0(:,:,j) = j; %j^2; %1; %j;
        fforcew0(:,:,fbbsize(3)+1-j) = -j; %-j^2; %-1; %-j;
        fforcew0 = fforcew0/sum(abs(fforcew0(:)));
    end

    for j=1:nv

        % absolut u of element center in blob
        gvcentexact = gv(j).uc(:,b) + gv(j).uc0bl(:,b);
        % the same rounded:
        gvcentround = floor(gvcentexact) + 0.5;
        % offset from rounded value:
        %gvcentoffs  = gvcentexact - gvcentround;  !!! don't use it

        blbbu = gvcentround(1)+0.5-fbbsize(1)/2 : gvcentround(1)-0.5+fbbsize(1)/2 ;
        blbbv = gvcentround(2)+0.5-fbbsize(2)/2 : gvcentround(2)-0.5+fbbsize(2)/2 ;
        blbbw = gvcentround(3)+0.5-fbbsize(3)/2 : gvcentround(3)-0.5+fbbsize(3)/2 ;

        % if this exceeds the size of bl(i).intd, crop blbbu/v/w, and crop
        % fforceu0/v0/w0
        croplowu=find(blbbu<1);
        crophighu=find(blbbu>bl(b).bbsize(1));        
        croplowv=find(blbbv<1);
        crophighv=find(blbbv>bl(b).bbsize(2));
        croploww=find(blbbw<1);
        crophighw=find(blbbw>bl(b).bbsize(3));
        
        % remove out of range indices
        blbbu([croplowu crophighu])=[];
        blbbv([croplowv crophighv])=[];
        blbbw([croploww crophighw])=[];
                        
        % deal with the force fields
        fforceu0crop = fforceu0;
        fforcev0crop = fforcev0;
        fforcew0crop = fforcew0;
        if croplowu
        fforceu0crop(1:length(croplowu), :, :)=[];
        fforcev0crop(1:length(croplowu), :, :)=[];
        fforcew0crop(1:length(croplowu), :, :)=[];
        end
        if crophighu
        fforceu0crop((end-length(crophighu)+1):end, :, :)=[];
        fforcev0crop((end-length(crophighu)+1):end, :, :)=[];
        fforcew0crop((end-length(crophighu)+1):end, :, :)=[];
        end
        if croplowv
        fforceu0crop(:, 1:length(croplowv), :)=[];
        fforcev0crop(:, 1:length(croplowv), :)=[];
        fforcew0crop(:, 1:length(croplowv), :)=[];
        end
        if crophighv
        fforceu0crop(:, (end-length(crophighv)+1):end, :)=[];
        fforcev0crop(:, (end-length(crophighv)+1):end, :)=[];
        fforcew0crop(:, (end-length(crophighv)+1):end, :)=[];
        end
        if croploww
        fforceu0crop(:, :, 1:length(croploww))=[];
        fforcev0crop(:, :, 1:length(croploww))=[];
        fforcew0crop(:, :, 1:length(croploww))=[];
        end
        if crophighw
        fforceu0crop(:, :, (end-length(crophighw)+1):end)=[];
        fforcev0crop(:, :, (end-length(crophighw)+1):end)=[];
        fforcew0crop(:, :, (end-length(crophighw)+1):end)=[];
        end
        % Intensity difference in the sampled volume around u
        fintd = bl(b).intd(blbbu,blbbv,blbbw);

        if ~wonly

            % 			fforceu = fforceu0 - gvcentoffs(1);
            % 			fforceu = fforceu/sum(abs(fforceu(:)));
            %
            % 			fforcev = fforcev0 - gvcentoffs(2);
            % 			fforcev = fforcev/sum(abs(fforcev(:)));

            fu           = fintd.*fforceu0crop;
            allfu(j,i)   = sum(fu(:));

            fv           = fintd.*fforcev0crop;
            allfv(j,i)   = sum(fv(:));
        end

        % 		fforcew = fforcew0 - gvcentoffs(3);
        % 		fforcew = fforcew/sum(abs(fforcew(:)));

        fw           = fintd.*fforcew0crop;
        allfw(j,i)   = sum(fw(:));

    end

    % 	hist(allfu(:,2))
    % 	hist(allfv(:))
    % 	hist(allfw(:))

    %   bl(i).su(1) = min( std(allfu(:,i)),  bl(i).ulim(1)/4 );
    %   bl(i).su(1) = max( bl(i).su(1)    , -bl(i).ulim(1)/4 );
    %
    %   bl(i).su(2) = min( std(allfv(:,i)),  bl(i).ulim(2)/4 );
    %   bl(i).su(2) = max( bl(i).su(2)    , -bl(i).ulim(2)/4 );
    %
    % 	bl(i).su(3) = min( std(allfw(:,i)),  bl(i).ulim(3)/4 );
    %   bl(i).su(3) = max( bl(i).su(3)    , -bl(i).ulim(3)/4 );
    %
    % 	blsu(:,i) = bl(i).su';

    %	blsu = [];

end


% if isempty(blsuin)
% 	blsuin = blsu;
% end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

dnewvol = zeros(ngv(1),ngv(2),ngv(3),9);

% Loop through elements
for i = 1:nv

    gv(i).fu(:,:) = [allfu(i,:); allfv(i,:); allfw(i,:)];

    % Loop through Friedel pairs (every second plane normal)
    for j = 1:2:length(fedpars.usebl)

        % 		% force from blob A and B (v is opposite in the two blobs)
        % 		fA = [allfu(i,j);    allfv(i,j);   allfw(i,j)  ]; % force from blob A
        % 		fB = [allfu(i,j+1); -allfv(i,j+1); allfw(i,j+1)]; % force from blob B
        %
        % 		% multiplicator factor is their dot product; if fA and fB point to the
        % 		% same direction, it's high:
        % 		%mfac = fA'*fB/norm(fA)/norm(fB);
        %
        % 		if mfac > 0
        %   		gv(i).fu2(:,(j+1)/2) = (fA+fB)/2;
        % 		else % contradictory guess, the Friedel pair should be ignored
        %   		gv(i).fu2(:,(j+1)/2) = 0;
        % 		end

        if wonly
            gv(i).fu2(1:2,(j+1)/2) = 0;
        else

            fAu = allfu(i,j);
            fBu = allfu(i,j+1);
            if fAu*fBu > 0
                gv(i).fu2(1,(j+1)/2) = (fAu + fBu)/2;
            else  % contradictory guess
                gv(i).fu2(1,(j+1)/2) = 0;
            end

            fAv = allfv(i,j);
            fBv = allfv(i,j+1);
            if fAv*fBv < 0
                gv(i).fu2(2,(j+1)/2) = (fAv - fBv)/2;
            else  % contradictory guess
                gv(i).fu2(2,(j+1)/2) = 0;
            end

        end

        fAw = allfw(i,j);
        fBw = allfw(i,j+1);
        if fAw*fBw > 0
            gv(i).fu2(3,(j+1)/2) = (fAw + fBw)/2;
        else  % contradictory guess
            gv(i).fu2(3,(j+1)/2) = 0;
        end

    end

    if ~fedpars.real_data
        % Speed factor for optimum solution:
        fu2s = gv(i).fu2;
        fu2s(fu2s==0) = NaN;
        gv(i).fac2m = (gv(i).ucm(:,1:2:end) - gv(i).su2)./fu2s;
    end

    %gv(i).su2 = min(gv(i).fu2.*fedpars.sufac, fedpars.sumax);
    %gv(i).su2 = max(gv(i).su2, -fedpars.sumax);
    gv(i).su2 = gv(i).fu2.*fedpars.sufac;

    if ~fedpars.real_data
        gv(i).solsr = gv(i).su2./(gv(i).ucm(:,1:2:end) - gv(i).uc(:,1:2:end));
    end

    % Change to be applied to d:
    ddact = gv(i).T02*gv(i).su2(:);

    % Discretize d change
    if dstepdiscrete
        ddact = sign(ddact).*fedpars.dsteplim;
    else
        ddact = min(ddact,fedpars.dsteplim);
        ddact = max(ddact,-fedpars.dsteplim);
    end

    if wonly
        disp('possible error for w only case in flow solver')
        ddact(3) = 0;
    end

    %%%%%
    %ddact = (gv(i).dm - gv(i).d);
    %%%%%


    dnew = gv(i).d + ddact;
    dnew = min(dnew,dlim);
    dnew = max(dnew,-dlim);

    gv(i).do = gv(i).d;
    gv(i).df = dnew;
    gv(i).d  = dnew;

    dnewvol(gv(i).ind(1),gv(i).ind(2),gv(i).ind(3),:) = dnew;

end


% Loop through elements

% else  % 3 wbl-s
% In case of 3 blobs
% 	for i = 1:nv
%
% 		gv(i).f(:) = [allfu(i,:); allfv(i,:); allfw(i,:)];
% 		gv(i).su  = gv(i).f.*blsuin;
%
% 		U0_allwbl =[];
% 		su_allwbl = [];
%
% 		for j=wbl
% 			U0_allwbl = [U0_allwbl; gv(i).U0(:,:,j)];
% 			su_allwbl = [su_allwbl; gv(i).su(:,j)];
% 		end
%
%
% 		invU0 = inv(U0_allwbl);
%
%
% 		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 		%gv(i).su = (gv(i).um -gv(i).u);%/2;
% 		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% 		dnew = gv(i).d + invU0*su_allwbl;
% 		dnew = min(dnew,dlim);
% 		dnew = max(dnew,-dlim);
%
% 		gv(i).do = gv(i).d;
% 		gv(i).df = dnew;
% 		gv(i).d = dnew;
%
% 		dfvol(gv(i).ind(1),gv(i).ind(2),gv(i).ind(3),:) = dnew;
%
% 	end



if fedpars.dsmoothing

    % Smoothing of solution by taking the mean values in the neighbourhood of
    % a voxel

    disp(' Smoothing d components over elements...')

%     for i = 1:ngv(1)
%         for j = 1:ngv(2)
%             for k = 1:ngv(3)

   for n=1:nv  % replace with a loop through all elements, and read out i/j/k

        i=gv(n).ind(1);
        j=gv(n).ind(2);
        k=gv(n).ind(3);

                lim1l = max(i-1,1);
                lim1h = min(i+1,ngv(1));
                lim2l = max(j-1,1);
                lim2h = min(j+1,ngv(2));
                lim3l = max(k-1,1);
                lim3h = min(k+1,ngv(3));
                %
                % 				for id = 1:9
                % 					dfvals = dfvol(lim1l:lim1h,lim2l:lim2h,lim3l:lim3h,id);
                % 					gv(sub2ind(ngv,i,j,k)).d(id) = mean(dfvals(:));
                % 				end

                for id = 1:9 % loop through the nine components
                    dvals(1) = dnewvol(lim1l,j,k,id);
                    dvals(2) = dnewvol(lim1h,j,k,id);
                    dvals(3) = dnewvol(i,lim2l,k,id);
                    dvals(4) = dnewvol(i,lim2h,k,id);
                    dvals(5) = dnewvol(i,j,lim3l,id);
                    dvals(6) = dnewvol(i,j,lim3h,id);
                    dvals(7) = dnewvol(i,j,k,id);

                    %gv(sub2ind(ngv,i,j,k)).d(id) = mean(dvals(:));
                    %gv(sub2ind(ngv,i,j,k)).d(id) = (mean(dvals(:)) + gv(sub2ind(ngv,i,j,k)).d(id))/2;
                    gv(n).d(id) = (mean(dvals(:)) + gv(n).d(id))/2;
                    % NOTE - this is not a simple mean filter, but a
                    % weighted mean in which the current value is weighted
                    % equally with the mean value.
                end

                % 				for id = 1:9
                %
                % 					dfvals(1) = dfvol(i-1,j,k,id);
                % 					dfvals(2) = dfvol(i+1,j,k,id);
                % 					dfvals(3) = dfvol(i,j-1,k,id);
                % 					dfvals(4) = dfvol(i,j+1,k,id);
                % 					dfvals(5) = dfvol(i,j,k-1,id);
                % 					dfvals(6) = dfvol(i,j,k+1,id);
                %
                % 					% New value is mean of neighbouring values and its own value:
                % 					gv(sub2ind(ngv,i,j,k)).d(id) = mean([mean(dfvals(:)) dfvol(i,j,k,id)]);
                %
                % 				end
% 
%             end
%         end
%     end
   end

else
    disp(' No smoothing on d components applied...')
end



% d mean correction
if fedpars.dmeancorr
    disp('Applying mean correction ... ')
    if isfield(fedpars,'U0')
        U0 = fedpars.U0;
    else
        U0tmp = zeros(3,9,nbl);
        for j = 1:nv
            U0tmp = U0tmp + gv(j).U0;
        end

        U0 = zeros(3*nbl,9);
        for j = 1:nbl
            U0(3*j-2:3*j,:) = U0tmp(:,:,j);
        end

        U0 = U0/nv;
    end

    % d difference in blobs
    dubl = zeros(3*nbl,1);
    for i = 1:nbl
        dubl(3*i-2:3*i,1) = bl(i).commbb - bl(i).combb;
    end

    dd0 = U0\dubl;

    ddgvmean = zeros(9,1);
    for k = 1:9
        tmp = dnewvol2(:,:,:,k);
        ddgvmean(k,1) = mean(tmp(:));
    end

    ddcorr = dd0 - ddgvmean;

else
    ddcorr = zeros(9,1);
end


% Save d difference from current step:
for i=1:nv
    gv(i).d = gv(i).d + ddcorr;
    gv(i).dd = gv(i).d - gv(i).do;
end



% Interpolation of d for each voxel:

% % behaviour of meshgrid and interp3 tested OK
% if 1
%
% 	dvol1 = zeros(ngv);
% 	dvol2 = zeros(ngv);
% 	dvol3 = zeros(ngv);
% 	dvold = zeros(ngv(1),ngv(2),ngv(3),9);
% 	dvoli = zeros(grsize(1),grsize(2),grsize(3),9);
%
% 	[dvox1,dvox2,dvox3] = meshgrid(1:grsize(1),1:grsize(2),1:grsize(3));
%
% 	for j = 1:nv
% 		% input coordinates for interpolation on a grid
% 		% flip coordinates 1 and 2 for meshgrid !!!
% 		dvol1(gv(j).ind(1),gv(j).ind(2),gv(j).ind(3)) = gv(j).cs1(2);
% 		dvol2(gv(j).ind(1),gv(j).ind(2),gv(j).ind(3)) = gv(j).cs1(1);
% 		dvol3(gv(j).ind(1),gv(j).ind(2),gv(j).ind(3)) = gv(j).cs1(3);
% 		% input values for interpolation on a grid
% 		for i = 1:9
% 			dvold(gv(j).ind(1),gv(j).ind(2),gv(j).ind(3),i) = gv(j).dm(i);
% 		end
% 	end
%
% 	for i = 1:9
% 		dvoli(:,:,:,i) = interp3(dvol1,dvol2,dvol3,dvold(:,:,:,i),dvox1,dvox2,dvox3,'*linear');
% 	end
%
% end






toc




