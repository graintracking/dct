function [gv,bl,bluvol] = gtFedSpreadIntExact(gv,bl,dmvol,beamdir,rotdir,...
	                        detpos,detnorm,Qdet,detuvorig,omstep,fedpars,projmode)


% Select the way of calculating "measured" simulated intensities
%   projmode is 'exact' or 'interp'

												
nbl = length(bl);
nv  = length(gv);

grsize = size(dmvol);

dint0  = ones(2,2,2);
bluvol = zeros(grsize(1),grsize(2),grsize(3),3,nbl); 

% rotation matrix components:
rotcomp = gtFedRotationMatrixComp(rotdir);


% Calculate new spot location after straining for each volume element 
% (one element may represent many voxels)

disp('  Computing measured strain and diffraction for elements...')
tic

for i=1:nbl
	
	
	for j=1:nv
		
		% deformation tensor from the components			 
		G = gtFedDeformationTensorFromComps(gv(j).dm) ;
			 
		% New properties in the deformed state:
		
		% deformed plane normal and relative d-spacing from initial			
		[pl,drel] = gtFedStrainedPlaneNormal(bl(i).plsam',G); % !!! plsam 
	
		if strcmp(fedpars.beamchroma,'poly')
			
			om = bl(i).om0;
			
			%pllab = -pl*sign(pl'*beamdir);
			
			sinth = abs(pl'*beamdir);
		
		elseif strcmp(fedpars.beamchroma,'mono')
		
			% sin(theta) after deformation
			sinth = bl(i).sinth0 / drel;
			
			% the two possible omegas
			oms = gtFedPredictOmega(pl,sinth,beamdir,rotdir,rotcomp);
			oms = oms(1:2);
			
			% the omega closest to the expected new one
			imdiffs = oms/omstep - gv(j).uc0im(3,i) - gv(j).U0(3,:,i)*gv(j).dm ;
			[minval,minloc] = min(abs(imdiffs));
			om = oms(minloc);
		
		end
		
		% rotation tensor
		Srot = gtFedRotationTensor(om,rotcomp);
		
		% diffraction vector
		dvec = gtFedPredictDiffVec(Srot*pl,sinth,beamdir);

		% spot location from gv centre after deformation
		%uim = gtFedPredictSpotLoc(dvec, om, S, gv(j).cs', omstep, rotu, orv, l);
		uim = gtFedPredictUVW(Srot,dvec,gv(j).cs',detpos,detnorm,Qdet,...
   		                 detuvorig,om,omstep);
		
		ubl = uim - bl(i).bbor';
	
		
		% store the new value for the element
		gv(j).ucmim(:,i) = uim ;
		gv(j).ucmbl(:,i) = ubl ;
		gv(j).ucm(:,i)   = ubl - gv(j).uc0bl(:,i) ;
	
	end
end
	
toc

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Forward simulate patterns

disp('  Forward projecting "measured" blobs...')
tic

if strcmp(projmode,'exact')

	for i = 1:nbl

		blint = zeros(bl(i).bbsize);
	  ind   = zeros(3,1);
		dtmp  = zeros(9,1);
		
		% Loop through grain volume voxel by voxel (dmvol contains the measured
		% for each voxel ) and project its intensity in the blob
		
		for j=1:(size(dmvol,1)*size(dmvol,2)*size(dmvol,3))
			
			[ind(1) ind(2) ind(3)] = ind2sub(size(dmvol),j);
			
			% 			E = [dmvol(ind(1),ind(2),ind(3),4) dmvol(ind(1),ind(2),ind(3),9) dmvol(ind(1),ind(2),ind(3),8);...
			% 				dmvol(ind(1),ind(2),ind(3),9) dmvol(ind(1),ind(2),ind(3),5) dmvol(ind(1),ind(2),ind(3),7);...
			% 				dmvol(ind(1),ind(2),ind(3),8) dmvol(ind(1),ind(2),ind(3),7) dmvol(ind(1),ind(2),ind(3),6)];
			%
			% 			R = [                            0   dmvol(ind(1),ind(2),ind(3),3) dmvol(ind(1),ind(2),ind(3),2);...
			% 				-dmvol(ind(1),ind(2),ind(3),3)                              0 dmvol(ind(1),ind(2),ind(3),1);...
			% 				-dmvol(ind(1),ind(2),ind(3),2) -dmvol(ind(1),ind(2),ind(3),1)                             0];
			%
			% 			G = E + R;
			
			G = gtFedDeformationTensorFromComps(dmvol(ind(1),ind(2),ind(3),:));
			
			
			% New properties in the deformed state
			
			% plane normal and relative d-spacing
			[pl,drel] = gtFedStrainedPlaneNormal(bl(i).plsam',G);
	
			
			if strcmp(fedpars.beamchroma,'poly')
				
				om = bl(i).om0;
				
				%pllab = -pl*sign(pl'*beamdir);
				
				sinth = abs(pl'*beamdir);
				
			elseif strcmp(fedpars.beamchroma,'mono')
				
				% sin(theta)
				sinth = bl(i).sinth0 / drel;
				
				% possible new omegas
				oms = gtFedPredictOmega(pl,sinth,beamdir,rotdir,rotcomp);
				oms = oms(1:2);
				
				% the omega closest to the expected new one
				dtmp(:) = dmvol(ind(1),ind(2),ind(3),:);
				imdiffs = oms/omstep - bl(i).om0/omstep - bl(i).U0(3,:)*dtmp ;
				[minval,minloc] = min(abs(imdiffs));
				om = oms(minloc);
				
			end
			
			% rotation tensor
			Srot = gtFedRotationTensor(om,rotcomp);
			
			% diffraction vector
			dvec = gtFedPredictDiffVec(Srot*pl,sinth,beamdir);
			
			% spot location from gv centre after deformation
			%uim = gtFedPredictSpotLoc(dvec, om, S, gv(j).cs', omstep, rotu, orv, l);
			uim = gtFedPredictUVW(Srot,dvec,gv(1).cs1'+ind-[1; 1; 1],detpos,detnorm,Qdet,...
				detuvorig,om,omstep);
			
			
			ublvox = uim - bl(i).bbor';
			
			bluvol(ind(1),ind(2),ind(3),:,i) = ublvox;
			
			% Distribute value over 8 voxels:
			mu = mod(ublvox,1);
			
			% This way of rounding is correct also for round values when mu=mod...
			% is used above:
			flu = floor(ublvox);
			clu = flu + 1;
			
			dint = dint0;
			
			dint(2,:,:) = dint(2,:,:)*mu(1);
			dint(1,:,:) = dint(1,:,:)*(1-mu(1));
			dint(:,2,:) = dint(:,2,:)*mu(2);
			dint(:,1,:) = dint(:,1,:)*(1-mu(2));
			dint(:,:,2) = dint(:,:,2)*mu(3);
			dint(:,:,1) = dint(:,:,1)*(1-mu(3));
			
			blint(flu(1):clu(1),flu(2):clu(2),flu(3):clu(3)) = ...
				blint(flu(1):clu(1),flu(2):clu(2),flu(3):clu(3)) + dint;
			
		end
		
		% Store results:
		bl(i).intm(:,:,:) = blint;

	end
	
	
elseif strcmp(projmode,'interp')	
	
	% Load measured de.f comp. dm into actual d temporarily for mexFed.
	for j = 1:nv
		gv(j).d = gv(j).dm ;
	end
	
	% Forward simulation for all blobs, using linear approximation
	%   gv and bl are going to be updated
	bluvol = mexFedSpreadIntByVoxel(gv,bl,fedpars);

	% Set back original zero actual strain
	for i = 1:nbl
		bl(i).intm = bl(i).int;
		bl(i).int(:)  = 0;
		bl(i).intd(:) = 0;
		bl(i).combl = NaN(1,3);
	end
	
	for j = 1:nv
		gv(j).d(:)  = 0 ;
		gv(j).dd(:) = 0 ;
	end
	
end
	


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Determine centroid and extense of blobs:

for i = 1:nbl
	
	blint = bl(i).intm;
	
	totint   = sum(blint(:));
	cu       = sum(sum(sum(blint,3),2).*(1:size(blint,1))')/totint;
	cv       = sum(sum(sum(blint,3),1).*(1:size(blint,2)))/totint;
	sum12    = zeros(1,size(blint,3));
  sum12(:) = sum(sum(blint,1),2);
	cw       = sum(sum12.*(1:size(blint,3)))/totint;

  bl(i).commbl = [cu cv cw];
	bl(i).commim = [cu cv cw] + bl(i).bbor;
	
	
	% Calculate boundaries of "measured" blob inside blob bbox
	
	uproj = sum(sum(bl(i).intm,2),3);
	bl(i).mbbu = [find(uproj,1,'first'), find(uproj,1,'last')];
	
	vproj = sum(sum(bl(i).intm,1),3);
	bl(i).mbbv = [find(vproj,1,'first'), find(vproj,1,'last')];

	wproj = sum(sum(bl(i).intm,1),2);
	bl(i).mbbw = [find(wproj,1,'first'), find(wproj,1,'last')];
	
	bl(i).mbbsize = [bl(i).mbbu(2)-bl(i).mbbu(1)+1, ... 
                   bl(i).mbbv(2)-bl(i).mbbv(1)+1, ... 
									 bl(i).mbbw(2)-bl(i).mbbw(1)+1];	
end


toc


