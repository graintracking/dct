function [gv,bl]=gtFedApplyFieldFlowPolyChr(gv,bl,fedpars)


nbl = length(bl);
nv  = length(gv);

dlim = fedpars.dlim;
ngv = fedpars.ngv;

allfu = zeros(nv,nbl);
allfv = zeros(nv,nbl);


if strcmp(fedpars.dstepmode,'discr')
	dstepdiscrete = true;
else
	dstepdiscrete = false;
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Start with measured mean omegas of the blobs

disp(' Applying flow field...  ')
tic

if isempty(fedpars.fbbsize)
	fedpars.fbbsize = 0.1;
end

for i = 1:nbl

	% Force depends on 1) intensity difference between measured and simulated
	% and 2) distance (linearly decreasing with distance from actual u).
	% Size of sampled volume is a fraction of the whole blob.
			
	% bbsize is odd
	fbbsizebase = max(bl(i).ulim(1:2)*fedpars.fbbsizemult,fedpars.fbbsizemax); % !!!
	fbbsize =  fbbsizebase - mod(fbbsizebase,2);

	
  % fforce0 is the multiplicator factor as a function of distance in u,v,w.
	fforceu0 = zeros(fbbsize);
	fforcev0 = zeros(fbbsize);
	
	
	for j=1:fbbsize(1)/2
		fforceu0(j,:,:) = j; 
		fforceu0(fbbsize(1)+1-j,:,:) = -j;
		fforceu0 = fforceu0/sum(abs(fforceu0(:)));
	end
	
	for j=1:fbbsize(2)/2
		fforcev0(:,j,:) = j; 
		fforcev0(:,fbbsize(2)+1-j,:) = -j; 
		fforcev0 = fforcev0/sum(abs(fforcev0(:)));
	end
	

	for j=1:nv		
		
		% absolut u of element center in blob
		gvcentexact = gv(j).uc(:,i) + gv(j).uc0bl(:,i);
		
		% the same rounded:
		gvcentround = floor(gvcentexact) + 0.5;
				
		blbbu = gvcentround(1)+0.5-fbbsize(1)/2 : gvcentround(1)-0.5+fbbsize(1)/2 ;
		blbbv = gvcentround(2)+0.5-fbbsize(2)/2 : gvcentround(2)-0.5+fbbsize(2)/2 ;
		blbbw = gvcentexact(3);
		
		% Intensity difference in the sampled volume around u
		fintd = bl(i).intd(blbbu,blbbv,blbbw);
		
		%	The actual force applied to the element	
		fu           = fintd.*fforceu0; % multiply pixel by pixel
		allfu(j,i)   = sum(fu(:));      % sum contribution of pixels
		
		fv           = fintd.*fforcev0;
		allfv(j,i)   = sum(fv(:));
				
	end
	

% 	hist(allfu(:,2))
% 	hist(allfv(:))
% 	hist(allfw(:))

	
end

% omega force is zero
allfw = zeros(nv,nbl);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

dnewvol = zeros(ngv(1),ngv(2),ngv(3),9);

% Loop through elements
for i = 1:nv
	
	gv(i).fu(:,:) = [allfu(i,:); allfv(i,:); allfw(i,:)];
	
	% For debugging
	% Speed factor for optimum solution:
	% 	fu2s = gv(i).fu2;
	% 	fu2s(fu2s==0) = NaN;
	% 	gv(i).fac2m = (gv(i).ucm(:,1:2:end) - gv(i).su2)./fu2s;
	%   gv(i).solsr = gv(i).su2./(gv(i).ucm(:,1:2:end) - gv(i).uc(:,1:2:end));

	
	% actualu speed is force times a factor
	gv(i).su = gv(i).fu.*fedpars.sufac;

	
	% Change to be applied to d; use su as a vector
	ddact = gv(i).T0*gv(i).su(:);

	% Discretize d change
	if dstepdiscrete
		ddact = sign(ddact).*fedpars.dsteplim;
	else
		ddact = min(ddact,fedpars.dsteplim);
		ddact = max(ddact,-fedpars.dsteplim);
	end
	
	
	dnew = gv(i).d + ddact;
	dnew = min(dnew,dlim);
	dnew = max(dnew,-dlim);

	gv(i).do = gv(i).d;
	gv(i).df = dnew;
	gv(i).d  = dnew;

	dnewvol(gv(i).ind(1),gv(i).ind(2),gv(i).ind(3),:) = dnew;

end


toc


% Smooth def. components
gv = gtFedDefCompSmoothing(dnewvol,gv,fedpars) ;




% d mean correction
if fedpars.dmeancorr
	disp('Applying mean correction ... ')
	if isfield(fedpars,'U0')
		U0 = fedpars.U0;
	else
		U0tmp = zeros(3,9,nbl);
		for j = 1:nv
			U0tmp = U0tmp + gv(j).U0;
		end

		U0 = zeros(3*nbl,9);
		for j = 1:nbl
			U0(3*j-2:3*j,:) = U0tmp(:,:,j);
		end

		U0 = U0/nv;
	end

	% d difference in blobs
	dubl = zeros(3*nbl,1);
	for i = 1:nbl
		dubl(3*i-2:3*i,1) = bl(i).commbb - bl(i).combb;
	end

	dd0 = U0\dubl;

	ddgvmean = zeros(9,1);
	for k = 1:9
		tmp = dnewvol2(:,:,:,k);
		ddgvmean(k,1) = mean(tmp(:));
	end

	ddcorr = dd0 - ddgvmean;

else
	ddcorr = zeros(9,1);
end


% Save d difference from current step:
for i=1:nv
	gv(i).d = gv(i).d + ddcorr;
	gv(i).dd = gv(i).d - gv(i).do;
end







