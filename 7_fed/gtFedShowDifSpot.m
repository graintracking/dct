function gtFedShowDifSpot(nbl,bl,flag)

if ~exist('flag','var')
	flag=2;
end

if strcmp(flag,'ini')
	im=sum(bl(nbl).int0,3);
	figure('name',sprintf('Initial #%d difblob',nbl),'numbertitle','off')
elseif strcmp(flag,'mes')
	im=sum(bl(nbl).intm,3);
	figure('name',sprintf('Measured #%d difblob',nbl),'numbertitle','off')
elseif strcmp(flag,'act')
	im=sum(bl(nbl).int,3);
	figure('name',sprintf('Actual #%d difblob',nbl),'numbertitle','off')
end

im=im';

imshow(im,[])
set(gca,'position',[0 0 1 1])


end
