function out=gtFedCalculateUVWfromGeo(geometry,grainFed)
%% Compute uv coordinates and w(omega) values from grain given an arbitrary
%% geometry

detdiru = geometry.detdiru0';
detdirv = geometry.detdirv0';
Qdet = gtFedDetectorProjectionTensor(detdiru, detdirv, 1, 1);
tn = cross(detdiru,detdirv);
detpos = geometry.detpos0';
uvorig = [geometry.detucen0, geometry.detvcen0]';
omstep = 180 / geometry.nproj;

csam = grainFed.center';

fed.uv = [];
fed.uvw = [];

n = size(grainFed.allblobs.omega,1)/4;
for jj = 1:n % must be from 0 to n-1
    
    % from grainFed=gtFedGenerateRandomGRain2(grain{grainid},full)
%     S = grainFed.allblobs.srot(12*jj-11:12*jj, :);
    reflections = 4*jj-3:4*jj;

    S = grainFed.allblobs.srot(:, :, reflections);
    D = grainFed.allblobs.dvec(reflections, :);
    O = grainFed.allblobs.omega(reflections); % omega values ordered like small, small+180, big, big+180 for the 4

    uv1a = gtFedPredictUVW(S(:, :, 1), D(1, :)', csam, detpos, tn, Qdet, uvorig, O(1), omstep);
    uv1b = gtFedPredictUVW(S(:, :, 2), D(2, :)', csam, detpos, tn, Qdet, uvorig, O(2), omstep);
    uv2a = gtFedPredictUVW(S(:, :, 3), D(3, :)', csam, detpos, tn, Qdet, uvorig, O(3), omstep);
    uv2b = gtFedPredictUVW(S(:, :, 4), D(4, :)', csam, detpos, tn, Qdet, uvorig, O(4), omstep);
    
    fed.uv = [fed.uv; ...
        uv1a(1), uv1a(2); ...
        uv1b(1), uv1b(2); ...
        uv2a(1), uv2a(2); ...
        uv2b(1), uv2b(2) ];
    fed.uvw = [fed.uvw; ...
        uv1a(1), uv1a(2), uv1a(3); ...
        uv1b(1), uv1b(2), uv1b(3); ...
        uv2a(1), uv2a(2), uv2a(3); ...
        uv2b(1), uv2b(2), uv2b(3) ];

end % size /4

out=fed;

end % end function
