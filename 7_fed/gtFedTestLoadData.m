function [gv,bl,dmvol,bluvol,bluvolsol,fedpars]=gtFedLoadData(fedpars,grain)

% Sets up the framework, variables, random strain distribution for simulated data.
%  In the grain volume everything is defined in voxels, no dimensioning with pixel size.
%  In the projections and diff. blobs, everything is defined in detector pixels (here comes the pixel size and setup geometry into play). 
%    Thus the dimensioning/scaling happens in the forward projection
%    functions and their derivative functions, since they relate the
%    grain voxels to the detector pixels.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Load acquisition, grain and FED parameters

%load(fedparsfile)

load(fedpars.parametersfile)

% Number of blobs to consider
nbl = length(fedpars.loadbl(:));

% FED parameters
%fedpars = gtFedParameters(size(grain.pairset.pl,1));
%fedpars = gtFedParameters(nbl);

% fedpars.loadbl = loadbl;
% 
% % Grain volume [voxels]:
% fedpars.grainsize = [40 40 40];       % size of grain volume 
% fedpars.grainoff  = grain.voloffset;  % coordinates of voxel (0,0,0) in the SAMPLE ref.
% 
% % Size of a volume element (sampling)
% fedpars.gvsize = [2 2 2];
% fedpars.ngvsub = [1 1 1];
% fedpars.solver = 'Flow';
% fedpars.filename = 'fedout2.mat';
% 
% % Maximum strain values:
% fedpars.dlim    = 0.01*ones(9,1);    % assumed in solution
% fedpars.dapplim = 0.005*ones(9,1);   % applied in random strain distribution
% %fedpars.dappinterp = 'linear';        % interpolation for generating random strain field
% fedpars.dsteplim  = 0.0002*ones(9,1);
% fedpars.sufac(:)  = 10;
% fedpars.fbbsize   = 0.1;
% fedpars.dmeancorr = 0;
% 
% fedpars.dstepmode = 'discr';



% Blob size extension:
blext = fedpars.blobsizeadd;

grsize = fedpars.grainsize;
gvsize = fedpars.gvsize;
%rlab   = fedpars.rlab;
grvoloffset = fedpars.grainoff;


% Load parameters from parameters file
%   Using general geometry.


%l      = parameters.acq.dist/parameters.acq.pixelsize;
omstep = 180/parameters.acq.nproj;
%rotu   = parameters.acq.rotx;

beamdir  = parameters.geo.beamdir0';

detuvorig= [parameters.geo.detucen0; parameters.geo.detvcen0]; % in detector pixels
detdiru  = parameters.geo.detdiru0'; % unit vector
detdirv  = parameters.geo.detdirv0'; % unit vector

detnorm  = cross(detdiru,detdirv);   % unit vector

% Detector position
%   originally in detector pixels !!!
%   change to grain voxels !!!
%   pixel sizes in mm-s !!!
detpos   = parameters.geo.detpos0'*parameters.acq.pixelsize/fedpars.voxelsize;
rotdir   = parameters.geo.rotdir0';  % unit vector

detscaleu = fedpars.voxelsize/parameters.acq.pixelsize; % detector scaling along u (grain voxel size / det. pixel size)
detscalev = fedpars.voxelsize/parameters.acq.pixelsize; % detector scaling along v (grain voxel size / det. pixel size)


%latpar=parameters.cryst.latticepar(1);
%lambda=gtConvEnergyToWavelength(parameters.acq.energy);

% Sample geometry and origin of SAMPLE coord. system:
%sam = gtSampleGeoInSampleSystem;
%orv = sam.sorZim;

% Get function handles:
%[Lop,Lopd,Aop,Aopd,Pop,Nop,Dvec,Srot,dS_dom,dom_dD,dsinth_dD,...
% dd_dD,Qpro,du_dD,om2im]=gtFedFunctionHandles;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Load grain parameters

% Blobs to load and use
lbl = fedpars.loadbl;

% Load data from the specified blobs
pl0     = grain.allblobs.pllab(lbl,:);
plsam   = grain.allblobs.pl(lbl,:);
hklsp   = grain.allblobs.hklsp(lbl,:);
hkl     = grain.allblobs.hkl(lbl,:);
thetatype = grain.allblobs.thetatype(lbl,:);
th0     = grain.allblobs.theta(lbl,:);
eta0    = grain.allblobs.eta(lbl,:);
om0     = grain.allblobs.omega(lbl,:);
dvec0   = grain.allblobs.dvec(lbl,:);
sinth0  = sind(th0);


% Grain center
gc = grsize/2+[0.5 0.5 0.5] + grvoloffset;

% Grain volume 
grvol  = gtFedGenerateGrainVolume(grsize);
fedpars.grainvol = grvol;

% Determine number of volume elements along X,Y,Z, based on grain volume
% and element size:
ngv(1) = ceil(size(grvol,1)/gvsize(1));
ngv(2) = ceil(size(grvol,2)/gvsize(2));
ngv(3) = ceil(size(grvol,3)/gvsize(3));

fedpars.ngv = ngv;

% Size of grain envelope
grenv(1) = ngv(1)*gvsize(1);
grenv(2) = ngv(2)*gvsize(2);
grenv(3) = ngv(3)*gvsize(3);

fedpars.grainenv = grenv;

% Padded grain volume
grvol_pad = false(grenv);
grvol_pad(1:size(grvol,1),1:size(grvol,2),1:size(grvol,3)) = grvol; 
grvol     = grvol_pad;



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
%% Set up volume sampling and volume elements

disp('Allocating memory for elements ...')
tic

nv    = ngv(1)*ngv(2)*ngv(3);          % total number of vol elements
svvol = gvsize(1)*gvsize(2)*gvsize(3); % size of sampling volume

% Load element parameters as defined in 
tmp      = gtFedVolumeStructure(nbl,gvsize);
gv(1:nv) = tmp;


% Loop through elements to assign indices and positions

for ii = 1:nv
	gv(ii) = gtFedVolumeStructure(nbl,gvsize);
	gv(ii).active = true;
	
	% Element XYZ index 
	[gv(ii).ind(1) gv(ii).ind(2) gv(ii).ind(3)] = ind2sub(ngv, ii);
	
	% Elements' positions in the sample volume (in voxels):
	gv(ii).cs1   = grvoloffset + 1 + (gv(ii).ind-1).*gvsize; % position of first voxel of gv in sample coords.
	gv(ii).cs    = gv(ii).cs1 + 0.5*gvsize - 0.5;            % centre in sample coords.
	gv(ii).csgr  = (gv(ii).ind - 0.5).*gvsize + 0.5;         % centre in grain volume in voxels
end


% Loop through elements in X-Y-Z, find the voxels and intensities they
% represent in the grain volume

for ii = 1:ngv(1)
	irangel = gvsize(1)*(ii-1)+1;
	irangeh = min(gvsize(1)*ii,size(grvol,1));
	
  for jj = 1:ngv(2)
		jrangel = gvsize(2)*(jj-1)+1;
		jrangeh = min(gvsize(2)*jj,size(grvol,2));
		
    for k= 1:ngv(3)
			krangel = gvsize(3)*(k-1)+1;
			krangeh = min(gvsize(3)*k,size(grvol,3));

			% The sub-volume represented by the given element in the grain
		  subvol(:,:,:) = grvol(irangel:irangeh, jrangel:jrangeh, krangel:krangeh);

			ind = sub2ind(ngv,ii,jj,k);
			
			% the sub-volume
			gv(ind).vol(:,:,:) = subvol;         % the subvolume
			gv(ind).totint     = sum(subvol(:)); % the summed volume (no. of voxels or total intensity)

			% check whether sub-volume is complete
			if gv(ind).totint==svvol
				gv(ind).complete   = true;
			else
				gv(ind).complete   = false;
			end
			
		end
	end
end

toc


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Assign blob's diffraction and projection parameters

disp('Generating blob data...')
tic

% Create a blank blob structure
tmp       = gtFedBlobStructure;
bl(1:nbl) = tmp;

% Rotation tensor components
rotcomp = gtFedRotationMatrixComp(rotdir);
Qdet    = gtFedDetectorProjectionTensor(detdiru,detdirv,detscaleu,detscalev);


% Loop through blobs

blobsdiffaway = [];
blobondet =[];

for i=1:nbl

	% Load diffraction data of the actual blob
  bl(i).hkl    = hkl(i,:);
  bl(i).hklsp  = hklsp(i,:);
	bl(i).thetatype = thetatype(i);
	
	bl(i).pl0    = pl0(i,:);        % initial signed (!) plane normal (under initial strain)
	                                %   in diffraction position
	bl(i).th0    = th0(i);          % initial theta
	bl(i).sinth0 = sinth0(i);       % initial sin(theta)
	bl(i).eta0   = eta0(i);         % initial eta
	bl(i).om0    = om0(i);          % initial omega
	
	bl(i).S0     = gtFedRotationTensor(bl(i).om0,rotcomp); % initial rotation tensor

	% Diffraction vector:
	%d0           = dvec0(i,:);
	%bl(i).dvec0  = d0/norm(d0);
	bl(i).dvec0  = dvec0(i,:);
	bl(i).plsam  = plsam(i,:); % in SAMPLE coordinates
	
	
	% Calculate initial mean diffraction parameters of blob based on grain center
	
	% Initial centroid u in image
	bl(i).u0im   = gtFedPredictUVW(bl(i).S0,bl(i).dvec0',gc',detpos,detnorm,Qdet,...
		             detuvorig,bl(i).om0,omstep)';
							 
	if isempty(bl(i).u0im)
		blobsdiffaway = [blobsdiffaway, i];
	else
				
		bl(i).dfac0  = gtFedPredictDiffFac(bl(i).S0,bl(i).dvec0',gc',detpos,detnorm);
		
		% Mean U tensor of blob is calculated from mean parameters
		bl(i).U0   = gtFedDerUmatrix(bl(i).plsam',bl(i).om0,bl(i).S0,rotcomp,gc',...
			bl(i).sinth0,bl(i).dvec0',bl(i).dfac0,detpos,detnorm,Qdet,beamdir,omstep,fedpars.beamchroma);
		
		% For the moment, use Umean instead of Umax to estimate extreme peak
		% shifts:
		
		% u limit for all elements (u displacement at extreme strain combination);
		bl(i).ulim   = ceil(abs(bl(i).U0)*fedpars.dlim)';
		
		% Padding around blob volume according to ulim
		bl(i).addbl  = bl(i).ulim + blext;
		
		% Check if blob hits the detector area
		uok = (bl(i).u0im(1) < parameters.acq.xdet) & (bl(i).u0im(1) > 1);
		vok = (bl(i).u0im(2) < parameters.acq.ydet) & (bl(i).u0im(2) > 1);
		if uok && vok
		  blobondet = [blobondet, i]; 
		end
		
	end
	
end

disp('Blobs inside the detector area are:')
disp(blobondet)

if ~isempty(blobsdiffaway)
	disp('The following projections (blobs) are directed away from the detector plane.')
	disp(blobsdiffaway)
	error('Remove projections (blobs) that do not hit the detector.')
end




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Preallocate blob volumes

loadvol = false;  % -> handle simulated blobs 

if loadvol   % this section is ignored (it's to handle real blobs)

	% !!! to be revised
	
	for i=1:nbl 
		
		gtDBConnect

		[vol,bb] = gtDBBrowseDiffractionVolume(parameters.acq.name,difspotID(i));

		% Blobs are stored with the coordinate order [u,v,w] !
		%  I.e. difspot images made from such a blob have to be transposed !

		% Bounding box sizes and boundaries of the measured blob
		bl(i).mbbsize = bb(4:6); 
		bl(i).mbbu = [bb(1), bb(1)+bb(4)-1];      % boundaries in u
		bl(i).mbbv = [bb(2), bb(2)+bb(5)-1];      % boundaries in v
		bl(i).mbbw = [bb(3), bb(3)+bb(6)-1];      % boundaries in w
		
		% Bounding box sizes and boundaries for processing
		bl(i).bbsize = [bb(4)+2*bl(i).addbl(1),bb(5)+2*bl(i).addbl(2),bb(6)+2*bl(i).addbl(3)];
		bl(i).bbu = bl(i).mbbu + [-bl(i).addbl(1), bl(i).addbl(1)];
		bl(i).bbv = bl(i).mbbv + [-bl(i).addbl(2), bl(i).addbl(2)];
		bl(i).bbw = bl(i).mbbw + [-bl(i).addbl(3), bl(i).addbl(3)];
	
		% Blob origin in image [u,v,w]:
		bl(i).bbor=[bl(i).bbu(1)-1,bl(i).bbv(1)-1,bl(i).bbw-1]; 

		% Blob intensities:
		bl(i).intm = zeros(bl(i).bbsize);
		bl(i).intm(bl(i).addbl(1)+1:bl(i).addbl(1)+bb(4),...
			         bl(i).addbl(2)+1:bl(i).addbl(2)+bb(5),...
			         bl(i).addbl(3)+1:bl(i).addbl(3)+bb(6)) = vol;

		bl(i).int0 = zeros(bl(i).bbsize);
		bl(i).int  = zeros(bl(i).bbsize);
		bl(i).intd = zeros(bl(i).bbsize);
		bl(i).intm = zeros(bl(i).bbsize);

		% To save memory vs. simlicity
		% bl(i).int0 = single(zeros(bl(i).bbsize));
		% bl(i).int  = single(zeros(bl(i).bbsize));
		% bl(i).intd = single(zeros(bl(i).bbsize));
		% bl(i).intm = single(zeros(bl(i).bbsize));
					 
	end

else  % work with simulated data
	
	maxdiag=ceil(sqrt(grenv(1)^2+grenv(2)^2+grenv(3)^2));

	for i=1:nbl

		% Bounding box sizes and boundaries for processing

		bl(i).bbsize = [maxdiag+2*bl(i).addbl(1),maxdiag+2*bl(i).addbl(2),2+2*bl(i).addbl(3)];

		tmp       = round(bl(i).u0im(1)-bl(i).bbsize(1)/2);
		bl(i).bbu = [tmp, tmp+bl(i).bbsize(1)-1];
		
		tmp       = round(bl(i).u0im(2)-bl(i).bbsize(2)/2);
		bl(i).bbv = [tmp, tmp+bl(i).bbsize(2)-1];
		
		tmp       = round(bl(i).u0im(3)-bl(i).bbsize(3)/2);
		bl(i).bbw = [tmp, tmp+bl(i).bbsize(3)-1];

		% Blob origin and center [u,v,w]:
		bl(i).bbor   = [bl(i).bbu(1)-1, bl(i).bbv(1)-1, bl(i).bbw(1)-1]; 
		bl(i).bbcent = [round((bl(i).bbu(1)+bl(i).bbu(2))/2),...
			            round((bl(i).bbv(1)+bl(i).bbv(2))/2),...
								  round((bl(i).bbw(1)+bl(i).bbw(2))/2)];
		bl(i).int0 = zeros(bl(i).bbsize);
		bl(i).int  = zeros(bl(i).bbsize);
		bl(i).intd = zeros(bl(i).bbsize);
		bl(i).intm = zeros(bl(i).bbsize);
		
		% To save memory vs. simlicity			
		%	bl(i).int0 = single(zeros(bl(i).bbsize));
		%	bl(i).int  = single(zeros(bl(i).bbsize));
		%	bl(i).intd = single(zeros(bl(i).bbsize));
		%	bl(i).intm = single(zeros(bl(i).bbsize));
		
	end
	
end

toc

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Load blob data in structure

disp('Calculating elements initial projections and displacement matrices ...')
tic


% Voxel indices in an element relative to virtual voxel (0,0,0):
for ii=1:svvol  % svvol is the size of sampling volume
	[vindrel0(ii,1),vindrel0(ii,2),vindrel0(ii,3)] = ind2sub(gvsize,ii);
end

% Voxel indices in element relative to real voxel (1,1,1)
vindrel = (vindrel0 - 1)';
		

for i=1:nbl

	% u in blob = u in image minus u at blob origin
	bl(i).u0bl = bl(i).u0im - bl(i).bbor;

  % u differential wrt. voxel position in the given blob (same for each voxel):
	%ud0 = gtFedPredictSpotLocDiff(bl(i).dvec0', bl(i).S0);

	% u differential times the difference in voxel location
	%uds_complete = ud0*vindrel;
	
	
	% Loop through elements to determine U matrix and initial u-s.
	
	for j=1:nv
		
		% Initial U of the first voxel		
		gv(j).U0(:,:,i)  = gtFedDerUmatrix(bl(i).plsam',bl(i).om0,bl(i).S0,rotcomp,gv(j).cs1',...
		                   bl(i).sinth0,bl(i).dvec0',bl(i).dfac0,detpos,detnorm,Qdet,beamdir,omstep,fedpars.beamchroma);											
							 
		% Initial absolut u of gv center
 		gv(j).uc0im(:,i) = gtFedPredictUVW(bl(i).S0,bl(i).dvec0',gv(j).cs',detpos,detnorm,Qdet,...
   		                 detuvorig,bl(i).om0,omstep);
    
		% Initial u of gv centre relative to blob origin
		gv(j).uc0bl(:,i) = gv(j).uc0im(:,i) - bl(i).bbor';

    % Actual peak shift of gv centre
	  gv(j).uc(:,i)  = [0; 0; 0];

		
		if strcmp(fedpars.beamchroma,'poly')
			gv(j).d(:) = 0 ;
			
	
			
		else	
			% Actual def. comps. of gv centre
			gv(j).d(:)  = 0;
			
		end
		
		% if gv(j).complete
		%	  gv(j).udvoxs(:,:,i) = uds_complete;
		% else
		%	  vindrel_short    = vindrel;
		%	  vindrel_short(:,~gv(j).vol(:)') = [];
	  %	  gv(j).udvoxs(:,:,i) = ud0*vindrel_short;
		% end
		%
		%gv(j).udvoxs(:,:,i) = uds_complete;
		%gv(j).udc(:,i) = mean(gv(j).udvoxs(:,:,i),2);
	
	end
	
end


for j=1:nv
	gv(j).voxindrel(:,:) = vindrel;
	
	%if ~gv(j).complete
	%	gv(j).udvoxs(:,~gv(j).vol(:)',:) = [];
	%	gv(j).voxind(:,~gv(j).vol(:)') = [];
	%end
	
end

toc

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Compute the pseudo-inverse of U0 for each volume element

% The pseudo-inverse is like the inverse matrix but instead of the exact solution,
%   gives a least square fitting solution of the inversion.
% SVD is one way of calculating it.
% In our case: u = U*d
% We're looking for T, so that: d = T*u 
%
%
% Equations for Singular Value Decomposition (SVD)
%
% System to be solved: b=A*x  , where x is to be fitted
%	Decompose the A matrix: A=U*S*V'
%   [U,S,V]=svd(A,0) ;
% The inverse of A is given by:
%   omega=diag(S) ;
%   inv_A=V*diag(1./diag(S))*U' ;
% So the maximum likelihood fit of x is:
%   x=V*diag(1./diag(S))*U'*b ;


disp('Inverting elements displacement matrices ...')
tic


% Loop through elements

for i=1:nv

	% This will contain all U matrix values of the element from all blobs
	U0_allpl = zeros(3*nbl,9);
	
	% Loop through blobs
	for j=1:nbl
		U0_allpl((j-1)*3+1:j*3,:) = gv(i).U0(:,:,j);
	end
	
	% Delete coloumn of 4th component for polychromatic beam
	% (only deviatoric strain, thus 8 unknown components)
	if strcmp(fedpars.beamchroma,'poly')
		U0_allpl(:,4) = [];
	end
	
	% SVD solution for the inversion:
	[Usvd,Ssvd,Vsvd] = svd(U0_allpl,0);
	gv(i).T0 = Vsvd*diag(1./diag(Ssvd))*Usvd';
	
	if strcmp(fedpars.beamchroma,'poly')
		
		% Set omega components to zero.
		gv(i).T0(:,3:3:end) = 0;
		
		% For polychromatic beam, the first normal strain component is set 0.
		%   Hydrostatic strain cannot be inferred, i.e. max. 8 components only.
		gv(i).T0 = [gv(i).T0(1:3,:); zeros(1,3*nbl); gv(i).T0(4:8,:)];
		
	else
		
		% Assume that subsequent blobs are Friedel pairs, and compute TO2
		
		Tau = gv(i).T0(:,1:6:end);
		Tav = gv(i).T0(:,2:6:end);
		Taw = gv(i).T0(:,3:6:end);
		
		Tbu = gv(i).T0(:,4:6:end);
		Tbv = gv(i).T0(:,5:6:end);
		Tbw = gv(i).T0(:,6:6:end);
		
		
		gv(i).T02(:,1:3:end) = (Tau+Tbu)/2;
		gv(i).T02(:,2:3:end) = (Tav-Tbv)/2;
		gv(i).T02(:,3:3:end) = (Taw+Tbw)/2; % !!! this should be revised for some components (as i remember for rotations and u)

	end
	
end

toc


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Apply strain distribution to grain

disp('Applying random strain distribution ...')
tic

% Measured (real) strain distribution:
[gv,dmvol] = gtFedGenerateRandomStrain(gv,fedpars);

% if mod(grsize(1),2)==0 && mod(grsize(2),2)==0 && mod(grsize(3),2)==0
% 	grsize_even = true;
% else
% 	grsize_even = false;
% end
% 
% zerods = zeros(9,1);
% 
% for i=1:nv
% 
% 	if grsize_even
% 		gv(i).dm(:) = dmvol(gv(i).cs(1)-0.5,gv(i).cs(2)-0.5,gv(i).cs(3)-0.5,:);
% 	  % for k=1:9
%     % 	gv(i).dm(k)    = interp3(dmvol(:,:,:,k),gv(i).cs(1),gv(i).cs(2),gv(i).cs(3));
%     % end
% 	else
% 		gv(i).dm(:) = dmvol(gv(i).cs(1),gv(i).cs(2),gv(i).cs(3),:);
% 	end
% 
% 	% Actual deformation state in processing
% 	gv(i).d(:) = zerods;
% end


% figure('name','Epsilon33')
% slice(dmvol(:,:,:,3),grsize(1)/2,grsize(2)/2,grsize(3)/2);
% colorbar

disp('Largest strain component:')
disp(max(dmvol(:)))
disp('Smallest strain component:')
disp(min(dmvol(:)))
toc



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Simulate "measured" diffraction patterns

if ~loadvol
	disp('Simulating "measured" blob intensities...')
	[gv,bl,bluvolsol] = gtFedSpreadIntExact3(gv,bl,dmvol,beamdir,rotdir,...
		detpos,detnorm,Qdet,detuvorig,omstep,fedpars,'interp');
end


% Compute intensities at solution
%disp('Computing blob intensities at solution...')
%[gvsol,blsol,bluvollinsol] = gtFedSpreadIntByVoxel(gv,bl,fedpars,1);

% Compute initial intensities
disp('Computing initial blob intensities...')
tic
bluvol = mexFedSpreadIntByVoxel(gv,bl,fedpars);
%[gv,bl,bluvol] = gtFedSpreadIntByVoxel(gv,bl,fedpars); % !!!


for i = 1:nbl
	bl(i).int0 = bl(i).int;
	bl(i).combl = [0 0 0];	
end
toc


disp('Computing maximum errors in linear approximation...')
% Error of linear approximation at the solution:
maxulinerr = zeros(3,1);
%maxdlinerr = zeros(9,1);

for j = 1:nv
	for i = 1:nbl
		gv(j).ulinerr(:,i) = gv(j).U0(:,:,i)*gv(j).dm - gv(j).ucm(:,i) ;
	end
	
	maxulinerr_j = max(gv(j).ulinerr,[],2) ;
	maxulinerr = max(maxulinerr,maxulinerr_j) ;
	
	gv(j).dlinerr = gv(j).T0*gv(j).ulinerr(:) ; % error in d components	
	%maxdlinerr = max(maxdlinerr,gv(j).dlinerr) ;
end

disp('Maximum error of [u,v,w] of all the elements and blobs from linear approximation [pixel,pixel,image]:')
disp(maxulinerr)


% Voxel indices into gv elements:
% fedpars.vox2gvind = zeros(grsize);
% for i = 1:nv
% 	for j = 1:size(gv(i).voxindrel,2)
% 		ind = gv(i).cs1+gv(i).voxindrel(:,j)';
% 		fedpars.vox2gvind(ind(1),ind(2),ind(3)) = i;
% 	end
% end


% Voxel indices into neighbouring gv elements
fedpars.vox2gvind   = zeros(grsize);
fedpars.vox2gvindne = zeros([grsize 8]);
fedpars.vox2gvindp  = zeros([grsize 8]);

for i = 1:nv

	gvind = gv(i).ind;

  for j = 1:size(gv(i).voxindrel,2)
		ind = gv(i).cs1+gv(i).voxindrel(:,j)';
		fedpars.vox2gvind(ind(1),ind(2),ind(3)) = i;

		mu = (ind - gv(i).cs)./gvsize;
		
		gvindo(1) = gvind(1)+sign(mu(1));
		gvindo(2) = gvind(2)+sign(mu(2));
		gvindo(3) = gvind(3)+sign(mu(3));

		gvindne(:,1) = [gvind(1), gvind(2),  gvind(3) ];
		gvindne(:,2) = [gvind(1), gvind(2),  gvindo(3)];
		gvindne(:,3) = [gvind(1), gvindo(2), gvind(3) ];
		gvindne(:,4) = [gvind(1), gvindo(2), gvindo(3)];
		gvindne(:,5) = [gvindo(1),gvind(2),  gvind(3) ];
		gvindne(:,6) = [gvindo(1),gvind(2),  gvindo(3)];
		gvindne(:,7) = [gvindo(1),gvindo(2), gvind(3) ];
		gvindne(:,8) = [gvindo(1),gvindo(2), gvindo(3)];
		
		gvindne = max(gvindne,1);
		gvindne = min(gvindne,repmat(ngv',1,8));
		
		absmu = abs(mu);

		pgv(1) = (1-absmu(1))*(1-absmu(2))*(1-absmu(3));
		pgv(2) = (1-absmu(1))*(1-absmu(2))*absmu(3);
		pgv(3) = (1-absmu(1))*absmu(2)*(1-absmu(3));
		pgv(4) = (1-absmu(1))*absmu(2)*absmu(3);
		pgv(5) = absmu(1)*(1-absmu(2))*(1-absmu(3));
		pgv(6) = absmu(1)*(1-absmu(2))*absmu(3);
		pgv(7) = absmu(1)*absmu(2)*(1-absmu(3));
		pgv(8) = absmu(1)*absmu(2)*absmu(3);

		for k = 1:8
			vind = gvindne(:,k);
			fedpars.vox2gvindne(ind(1),ind(2),ind(3),k) = sub2ind(ngv,vind(1),vind(2),vind(3));
		end
		
		fedpars.vox2gvindp(ind(1),ind(2),ind(3),:) = pgv;
		
	end
end


% Mean U matrix of blobs
U0tmp = zeros(3,9,nbl);
for j = 1:nv
	U0tmp = U0tmp + gv(j).U0;
end
U0 = zeros(3*nbl,9);
for j = 1:nbl
	U0(3*j-2:3*j,:) = U0tmp(:,:,j);
end
fedpars.U0 = U0/nv;

% dubl = zeros(3*nbl,1);
% for i = 1:nbl
% 	dubl(3*i-2:3*i,1) = bl(i).commbb - bl(i).combb;
% end



%fedpars.usebl = 1:nbl;



beep, pause(1), beep, pause(1), beep


end % of main function 



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% SUB-FUNCTIONS

% function [gv,dvol]=sfApplyRandomStrain(gv,fedpars)
% % Interpolates a function in 3D between random values defined at 3x3x3
% % nodes.
% 	
% grsize = fedpars.grainsize;
% ngv = fedpars.ngv;
% 
% dvol=NaN([grsize 9]);
% 
% for i=1:9
% 	% 3x3x3 grid points on which random strain components will be generated
% 	mgvec            = [0, 0.5, 1];
% 	[mg1,mg2,mg3]    = meshgrid(mgvec,mgvec,mgvec);
% 
% 	% Flip real coordinates 1-2 due to meshgrid !!!!
%   [mg1i,mg2i,mg3i] = meshgrid((0.5:grsize(2))/grsize(2),...
% 		                          (0.5:grsize(1))/grsize(1),...
%                           		(0.5:grsize(3))/grsize(3));
% 	% Generate random strain components (+-fedpars.dapplim) on the 3x3x3 grid
% 	mgvals           = (1-2*rand(3,3,3))*fedpars.dapplim(i);
% 	
% 	% ('*cubic' - cubic interpolation where input data is regularly spaced)
% 	dvol(:,:,:,i) = interp3(mg1,mg2,mg3,mgvals,mg1i,mg2i,mg3i,'*cubic');
% 	
%  	% Interpolate d values for each voxel and gv centre
% 	%if strcmp(fedpars.dappinterp,'cubic')
% 
% 	for j=1:length(gv)
% 		% Flip coordinates 1-2 due to meshgrid !!!!
% 		gv(j).dmcub(i) = interp3(mg1,mg2,mg3,mgvals,(gv(j).ind(2)-0.5)/ngv(2),...
% 			(gv(j).ind(1)-0.5)/ngv(1),(gv(j).ind(3)-0.5)/ngv(3),...
% 			'*cubic');
% 		gv(j).d(i)  = 0; % initial d values
% 	end
% 
% 	%elseif strcmp(fedpars.dappinterp,'linear');
% 
% % 		for j=1:length(gv)
% % 			% Flip coordinates 1-2 due to meshgrid !!!!
% % 			gv(j).dm(i) = interp3(mg1,mg2,mg3,mgvals,(gv(j).ind(2)-0.5)/ngv(2),...
% % 				            (gv(j).ind(1)-0.5)/ngv(1),(gv(j).ind(3)-0.5)/ngv(3),...
% % 										'*linear');	
% % 			gv(j).d(i)  = 0; % initial d values
% % 		end
% 
%   for j=1:length(gv)
% 		for k = 1:size(gv(j).voxindrel,2)
% 			voxind  = gv(j).cs1+gv(j).voxindrel(:,k)';
% 			dvox(k) = dvol(voxind(1),voxind(2),voxind(3),i);
% 		end
% 		gv(j).dm(i) = mean(dvox);
% 	end
% 			
% 	%else
% 	%	error('Interpolation type for strain not recognized.')
% 	%end
% end
% 
% 
% end
% 
% 
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% function grvol = sfGenerateGrainVolume(grsize)
% % binary grain volume 
% %grvol    = true(grsize);
% 
% % grain volume as double:
% grvol    = ones(grsize);
% 
% end



				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
