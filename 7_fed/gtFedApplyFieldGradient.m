function [gv,bl]=gtFedApplyFieldGradient(gv,bl,bluvol,fedpars)



nbl = length(bl);
nv  = length(gv);

dlim = fedpars.dlim;
ngv = fedpars.ngv;
grsize = fedpars.grainsize;
gvsize = fedpars.gvsize;

% allfu = zeros(nv,nbl);
% allfv = zeros(nv,nbl);
% allfw = zeros(nv,nbl);

%fedpars.dstep = 0.00005*ones(9,1);
%fedpars.dstepmode = 'discr';


if strcmp(fedpars.dstepmode,'discr')
	dstepdiscrete = true;
else
	dstepdiscrete = false;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Start with measured mean omegas of the blobs

disp(' Applying gradient field...  ')
tic


for j=1:nv		
	gv(j).fu = zeros(3,nbl);
end

for i = 1:nbl

	% Gradient of intensity difference (first and second coord. are flipped):
	[gradv,gradu,gradw] = gradient(bl(i).intd);
	
	%gvcent = zeros(3,nv);

	% Absolut u of element center in blob
% 	for j=1:nv		
% 		%gvcent(:,j) = gv(j).uc(:,i) + gv(j).uc0bl(:,i);
% 		gvcent(:,j) = gv(j).uc(:,i) + gv(j).uc0bl(:,i);
% 	end
	
	% Interpolated gradient values at element [u,v,w] (flip first 2 coords!):
% 	gradui = interp3(gradu,gvcent(2,:),gvcent(1,:),gvcent(3,:),'*linear');
% 	gradvi = interp3(gradv,gvcent(2,:),gvcent(1,:),gvcent(3,:),'*linear');
% 	gradwi = interp3(gradw,gvcent(2,:),gvcent(1,:),gvcent(3,:),'*linear');

  bluvolui = bluvol(:,:,:,1,i);
  bluvolvi = bluvol(:,:,:,2,i);
  bluvolwi = bluvol(:,:,:,3,i);

	gradui = zeros(grsize);
	gradvi = zeros(grsize);
	gradwi = zeros(grsize);
	
	gradui(:) = interp3(gradu,bluvolvi(:),bluvolui(:),bluvolwi(:),'*linear');
	gradvi(:) = interp3(gradv,bluvolvi(:),bluvolui(:),bluvolwi(:),'*linear');
	gradwi(:) = interp3(gradw,bluvolvi(:),bluvolui(:),bluvolwi(:),'*linear');


	% Mean gradient from blob voxels belonging to gv
% 	for j=1:length(gradui(:))
% 		gv(fedpars.vox2gvind(j)).fu(:,i) = gv(fedpars.vox2gvind(j)).fu(:,i) - [gradui(j); gradvi(j); gradwi(j)];
% 	end
	
  % Gradient at gv center location in blob
	for j=1:nv		
		gv(j).fu(:,i) = -[gradui(j); gradvi(j); gradwi(j)];
	end
	
end

% for j=1:nv		
% 	gv(j).fu = gv(j).fu/gvsize(1)/gvsize(2)/gvsize(3);
% end


dnewvol = zeros(ngv(1),ngv(2),ngv(3),9);

% Loop through elements
for i = 1:nv

	%U0act = [];
	%fuact = [];

	% Loop through Friedel pairs (every second plane normal)
	for j = 1:2:nbl

		fAu = gv(i).fu(1,j);
		fBu = gv(i).fu(1,j+1);
		if fAu*fBu > 0
  		funew = (fAu + fBu)/2;
			gv(i).fu2(1,(j+1)/2) = funew;
			%U0act = [U0act; mean(gv(i).U0(1,:,j:j+1),3)];
			%fuact = [fuact; funew];
		else  % contradictory guess
  		gv(i).fu2(1,(j+1)/2) = 0;
		end
		
		fAv = gv(i).fu(2,j);
		fBv = gv(i).fu(2,j+1);
		if fAv*fBv < 0
			funew = (fAv - fBv)/2;
  		gv(i).fu2(2,(j+1)/2) = funew;
			%U0act = [U0act; mean(gv(i).U0(2,:,j:j+1),3)];
			%fuact = [fuact; funew];
		else  % contradictory guess
  		gv(i).fu2(2,(j+1)/2) = 0;
		end
		
		fAw = gv(i).fu(3,j);
		fBw = gv(i).fu(3,j+1);
		if fAw*fBw > 0
			funew = (fAw + fBw)/2;
  		gv(i).fu2(3,(j+1)/2) = funew; 
			%U0act = [U0act; mean(gv(i).U0(3,:,j:j+1),3)];
			%fuact = [fuact; funew];
		else  % contradictory guess
  		gv(i).fu2(3,(j+1)/2) = 0;
		end		
		
	end


	
	% Speed factor for optimum solution:
	fu2s = gv(i).fu2;
	fu2s(fu2s==0) = NaN;
	gv(i).fac2m = (gv(i).ucm(:,1:2:end) - gv(i).su2)./fu2s;

	%gv(i).su2 = min(gv(i).fu2.*fedpars.sufac, fedpars.sumax);
	%gv(i).su2 = max(gv(i).su2, -fedpars.sumax);
	gv(i).su2 = gv(i).fu2.*fedpars.sufac;

	gv(i).solsr = gv(i).su2./(gv(i).ucm(:,1:2:end) - gv(i).uc(:,1:2:end));
	
	% Change to be applied to d:
	ddact = gv(i).T02*gv(i).su2(:);
	%ddact = U0act\fuact;
	
	% Create inverse of U0 from the active blob pairs: 
	% 	[Usvd,Ssvd,Vsvd] = svd(U0act,0);
	% 	T0act = Vsvd*diag(1./diag(Ssvd))*Usvd';

	% Discretize d change
	if dstepdiscrete
		ddact = sign(ddact).*fedpars.dsteplim;
	else
		ddact = min(ddact,fedpars.dsteplim);
		ddact = max(ddact,-fedpars.dsteplim);
	end
	
	dnew = gv(i).d + ddact;
	dnew = min(dnew,dlim);
	dnew = max(dnew,-dlim);

	gv(i).do = gv(i).d; % old d value
	gv(i).df = dnew;
	gv(i).d  = dnew;

	dnewvol(gv(i).ind(1),gv(i).ind(2),gv(i).ind(3),:) = dnew;
	
	
	%%%%%%%%%%%%%%%%%
	%gv(i).d = gv(i).dm;
	%%%%%%%%%%%%%%%%%%%%%%%%%%
	
end


if fedpars.dsmoothing==1
	% Smoothing of solution by taking the mean values in the neighbourhood of
	% a voxel
	disp(' Smoothing d components over elements by mean values...')
	
	for i = 1:ngv(1)
		for j = 1:ngv(2)
			for k = 1:ngv(3)

				lim1l = max(i-1,1);
				lim1h = min(i+1,ngv(1));
				lim2l = max(j-1,1);
				lim2h = min(j+1,ngv(2));
				lim3l = max(k-1,1);
				lim3h = min(k+1,ngv(3));
% 
% 				for id = 1:9
% 					dfvals = dfvol(lim1l:lim1h,lim2l:lim2h,lim3l:lim3h,id);
% 					%gv(sub2ind(ngv,i,j,k)).d(id) = (mean(dfvals(:)) + gv(sub2ind(ngv,i,j,k)).d(id))/2;
% 					gv(sub2ind(ngv,i,j,k)).d(id) = mean(dfvals(:));
% 				end
								
        for id = 1:9
					dvals(1) = dnewvol(lim1l,j,k,id);
					dvals(2) = dnewvol(lim1h,j,k,id);
					dvals(3) = dnewvol(i,lim2l,k,id);
					dvals(4) = dnewvol(i,lim2h,k,id);
					dvals(5) = dnewvol(i,j,lim3l,id);
					dvals(6) = dnewvol(i,j,lim3h,id);
					dvals(7) = dnewvol(i,j,k,id);

					gv(sub2ind(ngv,i,j,k)).d(id) = mean(dvals(:));
				end

								% 				for id = 1:9
								%
								% 					dfvals(1) = dfvol(i-1,j,k,id);
								% 					dfvals(2) = dfvol(i+1,j,k,id);
								% 					dfvals(3) = dfvol(i,j-1,k,id);
								% 					dfvals(4) = dfvol(i,j+1,k,id);
								% 					dfvals(5) = dfvol(i,j,k-1,id);
								% 					dfvals(6) = dfvol(i,j,k+1,id);
								%
								% 					% New value is mean of neighbouring values and its own value:
								% 					gv(sub2ind(ngv,i,j,k)).d(id) = mean([mean(dfvals(:)) dfvol(i,j,k,id)]);
								%
								% 				end

			end
		end
	end
	
elseif fedpars.dsmoothing==2

	disp(' Smoothing d components over elements by splines...')
	
	dvol1   = zeros(ngv);
	dvol2   = zeros(ngv);
	dvol3   = zeros(ngv);

	dvolval = zeros([ngv, 9]);

	for j = 1:nv
		% flip coordinates 1 and 2 for meshgrid !!!
		dvol1(gv(j).ind(1),gv(j).ind(2),gv(j).ind(3)) = gv(j).cs(1);
		dvol2(gv(j).ind(1),gv(j).ind(2),gv(j).ind(3)) = gv(j).cs(2);
		dvol3(gv(j).ind(1),gv(j).ind(2),gv(j).ind(3)) = gv(j).cs(3);
		%dvolval(gv(j).ind(1),gv(j).ind(2),gv(j).ind(3),:) = gv(j).d;
	end

	% Grid on which u values will be calculated (representing each voxel of grain
	% volume):
	[vox1,vox2,vox3] = ndgrid(1:grsize(1),1:grsize(2),1:grsize(3));

	for k=1:9
		dspline(k) = spaps({1:ngv(1),1:ngv(2),1:ngv(3)},dvolval(:,:,:,k),fedpars.dsteplim(k)/2);
		for j=1:nv
			gv(j).d(k) = fnval(dspline(k),gv(j).ind');
		end
	end


	
	
else
	disp(' No smoothing on d components applied...')
end


% Save d difference from current step:
for i=1:nv
	gv(i).dd = gv(i).d - gv(i).do;
end


toc




