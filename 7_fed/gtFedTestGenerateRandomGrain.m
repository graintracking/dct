function [grain,Eref,Rref] = gtFedTestGenerateRandomGrain(reflections,beamchroma,...
	                           beamenergymin,beamenergymax,Dlimit,Gref,Rvec)
			 


disp('Warning, macro only works for cubic lattices!')

% Assign a random grain ID, so that grain can be identified later:
grain.randomID = int64(rand*1e10);


% Beam chromaticity
if ~exist('beamchroma','var') || isempty(beamchroma)
	beamchroma = 'mono' ; % 'mono' or ''poly'
	beamenergymin = 0 ;  % in keV
	beamenergymax = 0 ; % in keV
end

% Minimum beam energy
if ~exist('beamenergymin','var') || isempty(beamenergymin)
	beamenergymin = 5 ;  % in keV
end

% Maximum beam energy
if ~exist('beamenergymax','var') || isempty(beamenergymax)
	beamenergymax = 25 ;  % in keV
end

% Number of first reflections to consider
if ~exist('reflections','var') || isempty(reflections)
	reflections = [];
end

% Maximum strain values:
if ~exist('Dlimit','var') || isempty(Dlimit)
	Dlimit=0.01;
end
	
% Reference full deformation gradient tensor:
if ~exist('Gref','var') || isempty(Gref)
	Gref=(ones(3)-rand(3)*2)*Dlimit;
end

% Rodrigues vector (grain orientation)
if ~exist('Rvec','var') || isempty(Rvec)
    disp('Generated random Rodrigues vector:');
	Rvec=1-2*rand(3,1);
else
	disp('Using input Rodrigues vector:')
  %Rvec = [-0.1768 -0.2438 0.0810]';
end
disp(Rvec);


grain.center    = [0 0 0];
grain.voloffset = [0 0 0];

disp('Random strain tensor:')
Eref=(Gref+Gref')/2;
disp(Eref)

disp('Random rotation tensor:')
Rref=(Gref-Gref')/2;
disp(Rref)


if exist('parameters.mat','file')
	load('parameters.mat');
else
	parameters.cryst.latticepar=1;
	parameters.acq.energy=40;
end

latpar = parameters.cryst.latticepar(1);
% Beam direction in LAB coord.
beamdir = parameters.geo.beamdir0'
rotdir  = parameters.geo.rotdir0'
spacegroup = parameters.cryst.spacegroup

if strcmp(beamchroma,'mono')
	lambda = gtConvEnergyToWavelength(parameters.acq.energy);
	if rotdir == [0; 0; 0]
		error('Rot. axis direction set incorrect in parameters.')
	end
elseif strcmp(beamchroma,'poly')
	lambdamax = gtConvEnergyToWavelength(beamenergymin)
	lambdamin = gtConvEnergyToWavelength(beamenergymax)
	lambda = 0 ;
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Define grain properties
	
hklsp = parameters.cryst.hklsp;
d0    = parameters.cryst.dsp;

% Define all unique reflections
% hkltypes = gtCrystSpacegroupReflections(parameters);
% if isempty(reflections)
% 	hkltypes_used = hkltypes;
% else
% 	hkltypes_used = hkltypes(reflections,:);	
% end
% 
% disp('Simulated HKL reflections:')
% disp(hkltypes_used)
% 
% hklsp  = [];
% hkl    = [];
% thtype = []; 
% 
% symm = gtCrystGetSymmetryOperators([], spacegroup);
% 
% for i = 1:length(hkltypes_used)
% 	allhkls_i = gtCrystSignedHKLs(hkltypes_used(i,:),symm);
% 	nhkls   = length(allhkls_i)/2;
% 	hklsp   = [hklsp; allhkls_i(nhkls+1:end,:)];
% 	hkl     = [hkl; repmat(hkltypes_used(i,:),nhkls,1)];
% 	thtype  = [thtype; repmat(i,nhkls,1)];
% end

% plane normals 
for ii = 1:length(hklsp)
    pl0(ii,:) = hklsp(ii,:)/sqrt(hklsp(ii,:)*hklsp(ii,:)');
end

% Compute orientation matrix g
g = gtMathsRod2OriMat(Rvec);

% Plane normals in SAMPLE coord.
pl_g = gtVectorCryst2Lab(pl0, g);


grain.pl    = [];
grain.pllab = [];
grain.hkl   = [];
grain.hklsp = [];
grain.thetatype = [];
grain.theta = [];
grain.eta   = [];
grain.omega = [];
grain.dvec  = [];

grain.allblobs.pl    = [];
grain.allblobs.pllab = [];
grain.allblobs.hkl   = [];
grain.allblobs.hklsp = [];
grain.allblobs.thetatype = [];
grain.allblobs.theta = [];
grain.allblobs.eta   = [];
grain.allblobs.omega = [];
grain.allblobs.dvec  = [];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  Impose strain on grain

for ii = 1:size(pl_g,1)

	plorig = pl_g(ii, :)';
	
	[pldef,drel] = gtFedStrainedPlaneNormal(plorig,Gref); % unit vector

	% new d-spacing
	dnew = drel*d0(ii);

	% deformed plane normals
	pl_gd(ii, :) = pldef;
	
	% d-spacings in deformed state
	d0_gd(ii, :) = dnew;
	
	% thetas in deformed state
	th_gd(ii, :) = asind(lambda/2/dnew);
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Compute omega and eta

rotcomp = gtFedRotationMatrixComp(rotdir);


if strcmp(beamchroma,'mono')  % monochromatic beam
	
	for ii = 1:size(pl_gd,1)

		pl    = pl_gd(ii, :)';
	
		sinth = sind(th_gd(ii));
		
		% Four omegas of the plane normal (1-3 and 2-4 are the two Friedel
		% pairs):
		% pls is plus or minus pl that's needed for the derivatives later
		[om,pllab,pls] = gtFedPredictOmega(pl,sinth,beamdir,rotdir,rotcomp);
		
		% If reflection occurs
		if ~isempty(om)
			
			% Rotation tensors from omegas (one for each of the two Friedel pairs)
			S1 = gtFedRotationTensor(om(1),rotcomp);
			S2 = gtFedRotationTensor(om(2),rotcomp);
			S3 = gtFedRotationTensor(om(3),rotcomp);
			S4 = gtFedRotationTensor(om(4),rotcomp);
			
			% Diffraction vectors
			d1 = gtFedPredictDiffVec(pllab(:,1),sinth,beamdir); % takes coloumn vectors
			d2 = gtFedPredictDiffVec(pllab(:,2),sinth,beamdir);
			d3 = gtFedPredictDiffVec(pllab(:,3),sinth,beamdir);
			d4 = gtFedPredictDiffVec(pllab(:,4),sinth,beamdir);
			
			% Normalize vectors
			d1 = d1/norm(d1);
			d2 = d2/norm(d2);
			d3 = d3/norm(d3);
			d4 = d4/norm(d4);
			
			% Following the convention of the mathcing output, the omega value
			% smaller than 180deg (spot "a") is used in the pair data.
			% The two Friedel pairs are 1a-1b and 2a-2b.
			
			if om(1) < om(3)
				om1a  = om(1);
				om1b  = om(3);
				pl1a  = pls(:,1)';
				pl1b  = pls(:,3)';
				pllab1a = pllab(:,1)';
				pllab1b = pllab(:,3)';
				d1a   = d1';
				d1b   = d3';
			else
				om1a  = om(3);
				om1b  = om(1);
				pl1a  = pls(:,3)';
				pl1b  = pls(:,1)';
				pllab1a = pllab(:,3)';
				pllab1b = pllab(:,1)';
				d1a   = d3';
				d1b   = d1';
			end
			
			if om(2) < om(4)
				om2a  = om(2);
				om2b  = om(4);
				pl2a  = pls(:,2)';
				pl2b  = pls(:,4)';
				pllab2a = pllab(:,2)';
				pllab2b = pllab(:,4)';
				d2a   = d2';
				d2b   = d4';
			else
				om2a  = om(4);
				om2b  = om(2);
				pl2a  = pls(:,4)';
				pl2b  = pls(:,2)';
				pllab2a = pllab(:,4)';
				pllab2b = pllab(:,2)';
				d2a   = d4';
				d2b   = d2';
			end
			
			% Eta angles
			eta1a = gtMatchEtaOfPoint(d1a); % takes row vector
			eta1b = gtMatchEtaOfPoint(d1b);
			eta2a = gtMatchEtaOfPoint(d2a);
			eta2b = gtMatchEtaOfPoint(d2b);
			
			
			grain.pl    = [grain.pl; pl1a; pl2a]; % signed plane normals
			grain.pllab = [grain.pllab; pllab1a; pllab2a];
			grain.hkl   = [grain.hkl; hkl(ii,:); hkl(ii,:)];
			grain.hklsp = [grain.hklsp; hklsp(ii,:); hklsp(ii,:)];
			grain.theta = [grain.theta; th_gd(ii,:); th_gd(ii,:)];
			grain.thetatype = [grain.thetatype; thtype(ii); thtype(ii)];
			grain.eta   = [grain.eta; eta1a; eta2a];
			grain.omega = [grain.omega; om1a; om2a];
			grain.dvec  = [grain.dvec; d1a; d2a];
			
			grain.allblobs.pl    = [grain.allblobs.pl; pl1a; pl1b; pl2a; pl2b];
			grain.allblobs.pllab = [grain.allblobs.pllab; pllab1a; pllab1b; pllab2a; pllab2b];
			grain.allblobs.hkl   = [grain.allblobs.hkl; hkl(ii,:); hkl(ii,:); hkl(ii,:); hkl(ii,:)];
			grain.allblobs.hklsp = [grain.allblobs.hklsp; hklsp(ii,:); -hklsp(ii,:); hklsp(ii,:); -hklsp(ii,:)];
			grain.allblobs.theta = [grain.allblobs.theta; th_gd(ii,:); th_gd(ii,:); th_gd(ii,:); th_gd(ii,:)];
			grain.allblobs.thetatype = [grain.allblobs.thetatype; thtype(ii); thtype(ii); thtype(ii); thtype(ii)];
			grain.allblobs.eta   = [grain.allblobs.eta; eta1a; eta1b; eta2a; eta2b];
			grain.allblobs.omega = [grain.allblobs.omega; om1a; om1b; om2a; om2b];
			grain.allblobs.dvec  = [grain.allblobs.dvec; d1a; d1b; d2a; d2b];
			
		end
	end
	
	
elseif strcmp(beamchroma,'poly')
	
	% Loop through plane normals
	
	for ii = 1:size(pl_gd,1)
		
		pl    = pl_gd(ii,:)';
		
		% Calculate plane normal angle with beam
		sinth = abs(pl'*beamdir) ;
		
		lambda = sinth*2*d0_gd(ii) ;
		
		% Check if reflection occurs in the given lambda range
		if (lambdamin <= lambda) && (lambda <= lambdamax)
			
			pls   = -pl*sign(pl'*beamdir) ; % signed plane normal
			
			theta = asind(sinth) ;
			
			dvec  = gtFedPredictDiffVec(pls,sinth,beamdir); % takes coloumn vectors
			
			grain.pl    = [grain.pl; pls'];
			grain.pllab = [grain.pllab; pls'];
			grain.hkl   = [grain.hkl; hkl(ii,:)];
			grain.hklsp = [grain.hklsp; hklsp(ii,:)];
			grain.theta = [grain.theta; theta];
			grain.thetatype = [grain.thetatype; thtype(ii)];
			grain.omega = [grain.omega; 0];
			grain.eta   = [grain.eta; NaN];
			grain.dvec  = [grain.dvec; dvec'];
						
			
		end
		
	end
	
	grain.allblobs.pl        = grain.pl;
	grain.allblobs.pllab     = grain.pllab;
	grain.allblobs.hkl       = grain.hkl;
	grain.allblobs.hklsp     = grain.hklsp;
	grain.allblobs.theta     = grain.theta;
	grain.allblobs.thetatype = grain.thetatype;
	grain.allblobs.eta       = grain.eta;
	grain.allblobs.omega     = grain.omega;
	grain.allblobs.dvec      = grain.dvec;
	

else
	error(' beamchroma not recognised')
	
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Create output

%grain.hkl   = int32(grain.hkl);
%grain.hklsp = int32(grain.hklsp);
grain.thetatype = int32(grain.thetatype);

grain.R_vector = Rvec';

if strcmp(beamchroma,'mono')
	disp('Total number of reflecting plane normals:')
	disp(length(grain.pl)/2)
	disp('Total number of pairs:')
	disp(length(grain.pl))
elseif strcmp(beamchroma,'poly')
	disp('Total number of reflecting plane normals:')
	disp(length(grain.pl))
end
disp('Generated {hkl}-s:')
disp(grain.hklsp)
disp('Generated plane normals, theta, eta and omega angles:')
disp([grain.pl grain.theta grain.eta grain.omega])

				
% Create the unique plane normals:
ACM = gtIndexAngularConsMatrix(hkltypes_used,parameters);

grain = gtIndexUniquePlaneNormals(grain,ACM);

if strcmp(beamchroma,'mono')
	if length(grain.pl) == 2*length(hklsp)
		grain.uni.hklsp = hklsp;
	else
		warning('Field uni.hklsp -unique signed hkl- could not be set properly. This happens by chance. Try to run the whole macro again with new random values.')
	end
end

% Add "measured" d-spacings and lattice parameters
%grain=gtSTRAINAddLatticePar(grain);
grain = gtAddGrainLatticePar(grain);

% Set some artificial errors (needed for strain fitting)
grain.uni.ddy(:) = 0.0001;
grain.uni.ddz(:) = 0.0001;
grain.uni.dom(:) = 0.01;

disp('Standard deviations set to a small value in grain.uni.')


				
