function [gv,bl]=gtFedSpreadInt(gv,bl,fedpars,iniflag)

% spreads the intensities of the volume elements over the blob volumes

if ~exist('iniflag','var')
	iniflag = false;
end


nbl = length(bl);
nv  = length(gv);

dint0 = ones(fedpars.gvsize);

disp(' Spreading intensities ...  ')
tic

% Loop through blobs
for i=1:nbl

	blint = zeros(bl(i).bbsize);
	
	% Loop through all elements
	for j=1:nv
		
		% Actual u is the previous u plus the change according to strain state.
    % u0 is the precise value of each element, linear approximation plays 
		% first here when applying the strain gv(j).D.

		if iniflag==1
			ubl = gv(j).u0bl(:,i);  % u in blob of the first voxel of gv(j)
		elseif iniflag==2
			ubl = gv(j).umbl(:,i);  % u in blob of the first voxel of gv(j)
		else
			u  = gv(j).U0(:,:,i)*gv(j).d;  % change compared to original u0 due to strain 
			gv(j).u(:,i)   = u ;
			ubl            = gv(j).u0bl(:,i) + u ; % u in blob of the first voxel of gv(j)
			gv(j).ubl(:,i) = ubl ;
		end
		
		% Loop through voxels in the element
		for k=1:size(gv(j).udvoxs,2)
		
			ublvox = ubl + gv(j).udvoxs(:,k,i); % add the u difference of the given voxel 
			
			% Distribute value over 8 voxels:
			mu = mod(ublvox,1); 

			flu = floor(ublvox);
			clu = ceil(ublvox);

			dint = dint0;

			dint(2,:,:) = dint(2,:,:)*mu(1);
			dint(1,:,:) = dint(1,:,:)*(1-mu(1));
			dint(:,2,:) = dint(:,2,:)*mu(2);
			dint(:,1,:) = dint(:,1,:)*(1-mu(2));
			dint(:,:,2) = dint(:,:,2)*mu(3);
			dint(:,:,1) = dint(:,:,1)*(1-mu(3));

			blint(flu(1):clu(1),flu(2):clu(2),flu(3):clu(3)) = ...
				blint(flu(1):clu(1),flu(2):clu(2),flu(3):clu(3)) + dint;
		
		end

	end
	
	% Determine centroid of blob:
	totint   = sum(blint(:));
	cu = sum(sum(sum(blint,3),2).*(1:size(blint,1))')/totint;
	cv = sum(sum(sum(blint,3),1).*(1:size(blint,2)))/totint;
	sum12    = zeros(1,size(blint,3));
	sum12(:) = sum(sum(blint,1),2);
	cw = sum(sum12.*(1:size(blint,3)))/totint;

	
	% Store results
	if iniflag==1
		bl(i).int0(:,:,:) = blint;
		bl(i).int(:,:,:)  = blint;
		bl(i).com0bb = [cu cv cw];
		bl(i).com0im = [cu cv cw] + bl(i).bbor;
		bl(i).intd(:,:,:) = blint - bl(i).intm(:,:,:);
	elseif iniflag==2
		bl(i).intm(:,:,:) = blint;  
		bl(i).commbb = [cu cv cw];
		bl(i).commim = [cu cv cw] + bl(i).bbor;
	else
		bl(i).int(:,:,:)  = blint;
		bl(i).combb = [cu cv cw];
		bl(i).comim = [cu cv cw] + bl(i).bbor;
		bl(i).intd(:,:,:) = blint - bl(i).intm(:,:,:);
	end

	intd = bl(i).intd(:,:,:);
	
	dev = intd.*intd;
	bl(i).err = sqrt(sum(dev(:)));
		
end

toc


