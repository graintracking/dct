function [gv,bl]=gtFedApplyFieldNeighbour(gv,bl,bluvol,bluvolsol,fedpars)



nbl = length(bl);
nv  = length(gv);

dlim = fedpars.dlim;
ngv = fedpars.ngv;
gvsize = fedpars.gvsize;
grsize = fedpars.grainsize;
nvox = grsize(1)*grsize(2)*grsize(3);


if strcmp(fedpars.dstepmode,'discr')
	dstepdiscrete = true;
else
	dstepdiscrete = false;
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Start with measured mean omegas of the blobs

disp(' Applying divergence field through neighbouring elements ...  ')

dnewvol = zeros(ngv(1),ngv(2),ngv(3),9);


% Use the 4 best blobs
% for i = 1:nbl
% 	blerr(i) = bl(i).err;
% end
% [minvals,mininds] = sort(blerr);
% wbl = mininds(1:4);

%ngvsub = subsize./gvsize;


ngvsub = fedpars.ngvsub;
nvsub = ngvsub(1)*ngvsub(2)*ngvsub(3);

voxsubsize = ngvsub.*gvsize;
nvoxsub = voxsubsize(1)*voxsubsize(2)*voxsubsize(3);

vox2gvind = fedpars.vox2gvind;
vox2gvindne = fedpars.vox2gvindne;
vox2gvindp = fedpars.vox2gvindp;


if mod(ngvsub,1)~= 0
	error('Subsampling')
end


Id = [];			
digv = [];



% Loop through blobs
for ib = fedpars.usebl

	sprintf('Blob #%d',ib)
	tic

% 	ublib = bluvol(:,:,:,1,ib);
% 	vblib = bluvol(:,:,:,2,ib);
% 	wblib = bluvol(:,:,:,3,ib);
% 
% 	% Neighbouring voxel coordinates
% 	%mu  = mod([ublib(:) vblib(:) wblib(:)],1);
% 	flu = floor([ublib(:) vblib(:) wblib(:)]);
% 	clu = ceil([ublib(:) vblib(:) wblib(:)]);
% 
% 	fcvox(:,1) = sub2ind(bl(ib).bbsize,flu(:,1),flu(:,2),flu(:,3));
% 	fcvox(:,2) = sub2ind(bl(ib).bbsize,flu(:,1),flu(:,2),clu(:,3));
% 	fcvox(:,3) = sub2ind(bl(ib).bbsize,flu(:,1),clu(:,2),flu(:,3));
% 	fcvox(:,4) = sub2ind(bl(ib).bbsize,flu(:,1),clu(:,2),clu(:,3));
% 	fcvox(:,5) = sub2ind(bl(ib).bbsize,clu(:,1),flu(:,2),flu(:,3));
% 	fcvox(:,6) = sub2ind(bl(ib).bbsize,clu(:,1),flu(:,2),clu(:,3));
% 	fcvox(:,7) = sub2ind(bl(ib).bbsize,clu(:,1),clu(:,2),flu(:,3));
% 	fcvox(:,8) = sub2ind(bl(ib).bbsize,clu(:,1),clu(:,2),clu(:,3));
% 
% 
% 	gvinds = repmat(vox2gvind(1:nvox)',1,8);
% 
% 	digvbl = accumarray(gvinds(:),bl(ib).intd(fcvox(:)),[nv 1]);

  digvbl = zeros(nv,1);

	% Intensity Derivative
	digvperddgv = zeros(ngv(1),ngv(2),ngv(3),9,ngv(1),ngv(2),ngv(3));

	%igvtotint = gvsize(1)*gvsize(2)*gvsize(3);

	igvderu = gvsize(2)*gvsize(3); % = igvtotint/gvsize(1)
	igvderv = gvsize(1)*gvsize(3);
	igvderw = gvsize(1)*gvsize(2);

	igvder = [igvderu, igvderv, igvderw];
	
	for i = 1:ngv(1)
		for j = 1:ngv(2)
			for k = 1:ngv(3)

 				gvijk = sub2ind(ngv,i,j,k);

				uc = gv(gvijk).ucbl(:,ib);
				
				flu = floor(uc);
				clu = ceil(uc);

				tmp = bl(ib).intd(flu(1):clu(1),flu(2):clu(2),flu(3):clu(3));
				digvbl(gvijk) = mean(tmp(:));


				if i > 1
					digvperddgv(i-1,j,k,:,i,j,k) = (igvder.*sign(gv(gvijk-1).uc(:,ib)-gv(gvijk).uc(:,ib))')*gv(gvijk).U0(:,:,ib);
				end
				if i < ngv(1)
					digvperddgv(i+1,j,k,:,i,j,k) = (igvder.*sign(gv(gvijk+1).uc(:,ib)-gv(gvijk).uc(:,ib))')*gv(gvijk).U0(:,:,ib);
				end
				
				if j > 1
					digvperddgv(i,j-1,k,:,i,j,k) = (igvder.*sign(gv(gvijk-ngv(2)).uc(:,ib)-gv(gvijk).uc(:,ib))')*gv(gvijk).U0(:,:,ib);
				end
				if j < ngv(2)
					digvperddgv(i,j+1,k,:,i,j,k) = (igvder.*sign(gv(gvijk+ngv(2)).uc(:,ib)-gv(gvijk).uc(:,ib))')*gv(gvijk).U0(:,:,ib);
				end
				
				if k > 1
					digvperddgv(i,j,k-1,:,i,j,k) = (igvder.*sign(gv(gvijk-ngv(1)*ngv(2)).uc(:,ib)-gv(gvijk).uc(:,ib))')*gv(gvijk).U0(:,:,ib);
				end
				if k < ngv(3)
					digvperddgv(i,j,k+1,:,i,j,k) = (igvder.*sign(gv(gvijk+ngv(1)*ngv(2)).uc(:,ib)-gv(gvijk).uc(:,ib))')*gv(gvijk).U0(:,:,ib);
				end
				
			end
		end
	end

	Id = [Id; reshape(digvperddgv,nv,9*nv)];
  digv = [digv; digvbl];
	
end
	

%%%%%%%%%%%%%%%%%
% ddm = zeros(length(gvactind)*9,1);
% for j = 1:length(gvactind)
% 	gvact = gvactind(j);
% 	ddm(9*j-8:9*j) =  gv(gvact).dm-gv(gvact).d;
% end
% diblm = Idbl*ddm;
% iblm = intdbl + diblm;
% 
% disp('error:')
% disp([sum(intdbl(:).*intdbl(:)) sum(iblm(:).*iblm(:))])
% 
%%%%%%%%%%%%%%%%%%%

				
% Least squares fit of all active d components:
sprintf(' Fitting d components ...')
tic

disp('No. of rows and colomns in Id, and their ratio:')
disp([size(Id) size(Id,1)/size(Id,2)])

dsol = Id\(-digv);

			
			%bluvoldif = 0;
			
% 			bluvoldif = floor(bluvol)-floor(bluvolsol);
% 			disp('Total no. of [u,v,w] coords:')
% 			length(bluvol(:))
% 			disp('Error in floor position [0 1 2 3 >3]:')
% 			disp([sum(bluvoldif(:)==0) sum(abs(bluvoldif(:))==1) sum(abs(bluvoldif(:))==2) ...
% 			sum(abs(bluvoldif(:))==3) sum(abs(bluvoldif(:))>3)])
			
			%dsollsq = lsqr(Id,-intdvec,[],100);
			%dsoldiv = mldivide(Id,-intdvec);
			
			%dlowb = -repmat(fedpars.dsteplim,nvsub,1);
			%dupb  = repmat(fedpars.dsteplim,nvsub,1);
			%d0    = zeros(size(Id,2),1);
			%dsol = lsqlin(Id,-intdvec,[],[],[],[],dlowb,dupb,d0);

			   %intdlim = 10*[ones(length(intdvec),1); -ones(length(intdvec),1)];
			   %dsol2 = lsqlin(Id,-intdvec,[Id; -Id],intdlim,[],[],dlowb,dupb,d0);

			
			
			%%%%%
			%dsol = ddm;
			%%%%
			
% 			Isol  = Id*dsol+intdvec;
% 			%Isol2 = Id*dsol2+intdvec;
% 			Isolm = Id*ddm+intdvec;
% 
% 			close all
% 			
% 			bins = -0.001:0.00001:0.001;
% 			%figure, hist(dsollsq,bins)
% 			figure, hist(ddm,bins)
% 			figure, hist(dsol,bins)
% 			figure, hist(dsol-ddm,bins)
% 			%figure, hist(dsol2-ddm,bins)
% 
% 			bins = -20:0.1:20;
% 			figure, hist(intdvec,bins)
% 			figure, hist(Isol,bins)
% 			%figure, hist(Isol2,bins)
% 			figure, hist(Isolm,bins)
% 
% 			sum(Isol(:).*Isol(:))
% 			%sum(Isol2(:).*Isol2(:))
% 			sum(Isolm(:).*Isolm(:))
			
			
		







% Store new d components

for i = 1:nv

	% Change to be applied to d:
	ddact = dsol(9*i-8:9*i);

	% Discretize d change
	if dstepdiscrete
		ddact = sign(ddact).*fedpars.dsteplim;
	else
		ddact = min(ddact,fedpars.dsteplim);
		ddact = max(ddact,-fedpars.dsteplim);
	end

	dnew = gv(i).d + ddact;
	dnew = min(dnew,dlim);
	dnew = max(dnew,-dlim);

	gv(i).do = gv(i).d; % old d value
	gv(i).d  = dnew;

	dnewvol(gv(i).ind(1),gv(i).ind(2),gv(i).ind(3),:) = dnew;


	gv(i).solsr = zeros(3,nbl);

end



% Mean correction
% U0tmp = zeros(3,9,nbl);
% for j = 1:nv
% 	U0tmp = U0tmp + gv(j).U0;
% end
% 
% U0 = zeros(3*nbl,9);
% for j = 1:nbl
% 	U0(3*j-2:3*j,:) = U0tmp(:,:,j);
% end
% 
% U0 = U0/nv;
% 
% dubl = zeros(3*nbl,1);
% for i = 1:nbl
% 	dubl(3*i-2:3*i,1) = bl(i).commbb - bl(i).combb;
% end
% 
% dd0 = U0\dubl;
% 
% 
% for k = 1:9
% 	tmp = dnewvol(:,:,:,k);
% 	ddgvmean(k,1) = mean(tmp(:));
% end
% 
% ddcorr = dd0 - ddgvmean;
% 

% !!!!
ddcorr(:) = 0;


			



if fedpars.dsmoothing
	% Smoothing of solution by taking the mean values in the neighbourhood of
	% a voxel
	disp(' Smoothing d components over elements...')
	
	for i = 1:ngv(1)
		for j = 1:ngv(2)
			for k = 1:ngv(3)

				lim1l = max(i-1,1);
				lim1h = min(i+1,ngv(1));
				lim2l = max(j-1,1);
				lim2h = min(j+1,ngv(2));
				lim3l = max(k-1,1);
				lim3h = min(k+1,ngv(3));

% 				for id = 1:9
% 					dfvals = dnewvol(lim1l:lim1h,lim2l:lim2h,lim3l:lim3h,id);
% 					gv(sub2ind(ngv,i,j,k)).d(id) = mean(dfvals(:));
% 				end
				
				for id = 1:9
					dvals(1) = dnewvol(lim1l,j,k,id);
					dvals(2) = dnewvol(lim1h,j,k,id);
					dvals(3) = dnewvol(i,lim2l,k,id);
					dvals(4) = dnewvol(i,lim2h,k,id);
					dvals(5) = dnewvol(i,j,lim3l,id);
					dvals(6) = dnewvol(i,j,lim3h,id);
					dvals(7) = dnewvol(i,j,k,id);
					
					gv(sub2ind(ngv,i,j,k)).d(id) = mean(dvals(:)) + ddcorr(id);
				end		
				

			end
		end
	end

else
	disp(' No smoothing on d components applied...')
end


% Save d difference from current step:
for i=1:nv
	gv(i).dd = gv(i).d - gv(i).do;
end


toc




