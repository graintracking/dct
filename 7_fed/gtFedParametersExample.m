function fedpars = gtFedParametersExample(grain)

nbl = length(grain.pl);

fedpars = gtFedParameters(nbl);


fedpars.grainfile = 'grain_randomID_9063081506.mat';
fedpars.parametersfile = 'parameters.mat';

fedpars.niter    = 1000;       % number of iterations
fedpars.dsmoothing = true;       % smoothing of d components over elements


%fedpars.loadbl = 1:nbl;
fedpars.loadbl = [1 2 4 8 11 13];

% Grain volume [voxels]:
fedpars.grainsize = [20 20 20];       % size of grain volume 
fedpars.grainoff  = grain.voloffset;  % coordinates of voxel (0,0,0) in the SAMPLE ref.

% Size of a volume element (sampling)
fedpars.gvsize = [2 2 2];
fedpars.ngvsub = [1 1 1];
fedpars.solver = 'Flow';
fedpars.filename = 'fedout.mat';

% Maximum strain values:
fedpars.dlim    = 0.01*ones(9,1);    % assumed in solution
fedpars.dapplim = 0.005*ones(9,1);   % applied in random strain distribution
%fedpars.dappinterp = 'linear';        % interpolation for generating random strain field
fedpars.dsteplim  = 0.0002*ones(9,1);
fedpars.sufac(:)  = 10;
fedpars.fbbsize   = 0.1;
fedpars.dmeancorr = 0;

fedpars.dstepmode = 'discr';

fedpars.beamchroma    = 'poly';
fedpars.beamenergymin = 5;
fedpars.beamenergymax = 25;
fedpars.beamenergyprof = NaN;

fedpars.voxelsize = 0.001;  % in mm-s !!!

fedpars.detscaleu = 1/80;
fedpars.detscalev = 1/80;

fedpars.blobsizeadd = [2 2 2];

fedpars.sumax= 10*ones(3,length(fedpars.loadbl)); % !!!
