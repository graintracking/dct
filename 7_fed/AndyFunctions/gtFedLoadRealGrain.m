function [fedpars, gv]=gtFedLoadRealGrain(grainid, dct_vol, fedgrain, varargin)
% GTFEDLOADREALGRAIN  Load all data related to the "grain"
%     [fedpars, gv] = gtFedLoadRealGrain(grainid, dct_vol, fedgrain, varargin)
%     --------------------------------------------------------------
%     mesh grain, create gv
%     for smoothness - this function is always launched in the folder of the
%     dataset (parameters.acq.dir) and all paths to files are handled nicely.
%
%     will automatically select the gvsize (element size) to give a reasonable number of elements (<~2000)
%     this can be forced by setting parameters.fed.auto_gvsize=0, in which case
%     gvsize will default to whatever is set in fedpars.
%
%     need to apply some renumbering if there is a sequence of datasets.  Add
%     this to parameters.fed.renumber_list
%
%     INPUT:
%       grainid = grain id
%       dct_vol = reconstructed volume for the grain shape adnd position
%       fedgrain = fed grain built with gtFedLoadGrainFrom(Wo)Indexter.m
%
%     OPTIONAL INPUT:
%       save_flag = save fedgrain.mat, gv.mat
%
%     OUTPUT:
%       fedpars = fed parameters inside fedgrain.mat updated
%       gv      = ??
%

app.save_flag = false;
app=parse_pv_pairs(app,varargin);
if ~app.save_flag
    disp('gtFedLoadRealGrain is not saving fedgrain.mat and gv.mat')
end

parameters=[];
load('parameters.mat');

% naming
grainid_str=num2str(grainid);
fedgrain_name = [parameters.fed.dir '/7_fed/grain' grainid_str '_/fedgrain.mat'];
gv_name       = [parameters.fed.dir '/7_fed/grain' grainid_str '_/gv.mat'];

% unpack fedpars for convenience
fedpars=fedgrain.fedpars;

% simplify variables
dct_vol=dct_vol==fedgrain.reference_grainid;
fedpars.grainvol=sum(dct_vol(:));

% use pairs only
nbl=2*fedgrain.nof_pairs;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% find extent of the grain
profilex=sum(sum(dct_vol, 3), 2)>0;
x1=find(profilex, 1, 'first');
x2=find(profilex, 1, 'last');
sizex=1+x2-x1;

profiley=sum(sum(dct_vol, 3), 1)>0;
y1=find(profiley, 1, 'first');
y2=find(profiley, 1, 'last');
sizey=1+y2-y1;

profilez=squeeze(sum(sum(dct_vol, 1), 2))>0;
z1=find(profilez, 1, 'first');
z2=find(profilez, 1, 'last');
sizez=1+z2-z1;

% get the Grain volume
grvol  = dct_vol(x1:x2, y1:y2, z1:z2);
fedpars.grvol = grvol;

% offset from centre of sample
% Sample geometry and origin of SAMPLE coord. system:
sam=gtSampleGeoInSampleSystem();
z1=z1+sam.bot;
z2=z2+sam.bot;
x1=x1-sam.rad;
x2=x2-sam.rad;
y1=y1-sam.rad;
y2=y2-sam.rad;

fedpars.grainsize = [sizex sizey sizez];  % size of grain volume
fedpars.grainoff  = [x1 y1 z1];           % coordinates of voxel (0,0,0) in the SAMPLE ref.
grainoff=fedpars.grainoff;

% Grain center
gc = fedpars.grainsize/2+[0.5 0.5 0.5] + fedpars.grainoff;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% mesh the grain

% Determine size of a volume element (sampling)
if fedpars.auto_gvsize
    disp('determining a sensible gvsize')
    totalvol=sum(dct_vol(:));
    elementvol=(totalvol/fedpars.max_elements_grain); % max 2000 elements - PR - 10000 here ?
    elementsize=ceil(elementvol^(1/3));
    if elementsize<2
        elementsize=2; % minimum gvsize
    end
    fedpars.gvsize=[elementsize elementsize elementsize];
    gvsize=fedpars.gvsize;
    fprintf('using gvsize = %d\n', elementsize);
else
    disp('using gvsize specified in fedgrain.fedpars')
    gvsize=fedpars.gvsize;
end

% Number of volume elements
ngv(1) = ceil(size(grvol,1)/gvsize(1));
ngv(2) = ceil(size(grvol,2)/gvsize(2));
ngv(3) = ceil(size(grvol,3)/gvsize(3));
fedpars.ngv = ngv;

% Size of grain envelope
grenv(1) = ngv(1)*gvsize(1);
grenv(2) = ngv(2)*gvsize(2);
grenv(3) = ngv(3)*gvsize(3);
fedpars.grainenv = grenv;

% pad grain volume to multiple of element size
grvol_pad = false(grenv);
grvol_pad(1:size(grvol,1),1:size(grvol,2),1:size(grvol,3)) = grvol;
grvol     = grvol_pad;

% save padded grainsize into fedpars variable - required?
fedpars.grvol=grvol;
fedpars.grainsize=size(grvol);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% calculate the true required number of elements (non-empty...)
% label the volume, then prepare gv structure

grvol_labelled=zeros(size(grvol));
% label this volume - this will have complete elements,
% which must then be masked with grvol to get the correct grain shape

disp('Dividing grain volume into elements...')
tic
count=0;
for i = 1:ngv(1)
    irangel = gvsize(1)*(i-1)+1;
    irangeh = min(gvsize(1)*i,size(grvol,1));
    
    for j = 1:ngv(2)
        jrangel = gvsize(2)*(j-1)+1;
        jrangeh = min(gvsize(2)*j,size(grvol,2));
        
        for k= 1:ngv(3)
            krangel = gvsize(3)*(k-1)+1;
            krangeh = min(gvsize(3)*k,size(grvol,3));
            
            subvol(:,:,:) = grvol(irangel:irangeh, jrangel:jrangeh, krangel:krangeh);
            if any(subvol(:))
                count=count+1;
                grvol_labelled(irangel:irangeh, jrangel:jrangeh, krangel:krangeh)=count;
            end
            
        end
    end
end
toc
% voxel to element map comes straight from this
vox2gvind = grvol_labelled.*grvol;
fedpars.vox2gvind = vox2gvind;
nv=count;
fprintf('...found %d elements in this grain\n', nv)

%nv    = ngv(1)*ngv(2)*ngv(3); % total number of vol elements
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% now set up gv structure for the required number of elements
disp('Allocating memory for elements ...')
%tic
svvol = gvsize(1)*gvsize(2)*gvsize(3); % size of sampling volume
tmp      = gtFedElementDefinition(nbl,gvsize);
gv(1:nv) = tmp;


% Fill in gv structure
for i=1:nv
    % voxel indices in element i
    voxindices=find(grvol_labelled==i, 1, 'first');
    % position of the first voxel of element
    [gv(i).cs1(1), gv(i).cs1(2), gv(i).cs1(3)] = ind2sub(size(grvol_labelled), voxindices(1));
    cs1_ingrain=gv(i).cs1;
    % element index
    gv(i).ind = ((gv(i).cs1-1)./gvsize)+1;
    % position allowing for grain offset
    gv(i).cs1=gv(i).cs1+grainoff;
    % element centre
    gv(i).cs  = gv(i).cs1 + 0.5*gvsize - 0.5;
    
    % complete element?
    voxindices=find(vox2gvind==i);
    voxsub=[];
    [voxsub(:,1), voxsub(:,2), voxsub(:,3)] = ind2sub(fedpars.grainsize, voxindices);
    voxsubrel = voxsub-repmat(cs1_ingrain, length(voxindices), 1);
    
    % filled voxels relative to cs1
    gv(i).voxindrel = voxsubrel'; % subscript / index terminology somewhat vague ;-)
    
    if length(voxindices)==svvol
        gv(i).complete   = true;
        % complete element subvolume
        gv(i).vol=true(gvsize);
    else
        gv(i).complete   = false;
        % get the element subvolume
        voxindrel=sub2ind(gvsize, voxsubrel(:,1)+1, voxsubrel(:,2)+1, voxsubrel(:,3)+1);
        gv(i).vol(voxindrel)=true;
    end
    % set d to zeros to start with
    gv(i).d(:)=0;
end
toc
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% repack fedpars into fedgrain.fedpars
fedgrain.fedpars=fedpars;

% save new and updated data
if ~app.save_flag
    check=inputwdefault('save gv.mat and updated fedgrain.mat? [y/n]', 'y');
    if strcmpi(check, 'y')
        save(fedgrain_name, 'fedgrain');
        save(gv_name, 'gv');
    else
        disp('not saving...  you should do it by yourself')
    end
else
    disp('auto-saving fedgrain.mat and gv.mat')
    save(fedgrain_name, 'fedgrain');
    save(gv_name, 'gv');
end


end % end of function
