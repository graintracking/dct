function [fedpars, gv] = gtFedSetupT0(grainid, fedgrain, gv, bl, phase)
% GTFEDSETUPT0  Setup the equations for solving the thingy
%               choose which components are active, and how to calculate T0
%     [fedpars, gv] = gtFedSetupT0(grainid, fedgrain, gv, bl, [phase])
%     ----------------------------------------------------------------
%
%     select which blobs to use - this means selecting the relevant data from
%     gv(i).uc0bl_All -->  gv(i).uc0bl
%
%     if a second arguement is supplied, try to do everything automatically
%     based on fedpars.strategy
%
%     for smoothness - this function is always launched in the folder of the
%     dataste (parameters.acq.dir) and all paths to files are handled nicely.
%
%     make sure we get everything from the strategy fields...
%
%     INPUT:
%       grainid  = grain id
%       fedgrain = fed grain built with gtFedLoadGrainFrom(Wo)Indexter.m
%       gv       = ??
%       bl       = ??
%       phase    = ?? <double> {0}
%
%     OUTPUT:
%       fedpars  = ??
%       gv       = ??


if ~exist('phase','var') || isempty(phase)
    phase=0;
end

parameters=[];
load('parameters.mat');

% naming
grainid_str = sprintf('%04d',grainid);
fedgrain_name = fullfile(parameters.fed.dir,'7_fed',['grain' grainid_str '_'],'fedgrain.mat');
gv_name       = fullfile(parameters.fed.dir,'7_fed',['grain' grainid_str '_'],'gv.mat');


fedpars=fedgrain.fedpars;

if phase==0 || ~isfield(fedpars, 'strategy')
    % get the relevent fedpars parameters from user
    list = cell(0,4);
    list(end+1,:) = [{'niter'},{'Number of iterations in this phase'},{'double'},{1}];
    list(end+1,:) = [{'crude_acceleration'},{'Crude acceleration of forward projector? true/false'},{'logical'},{1}];
    list(end+1,:) = [{'usebl'},{'Which blobs to use?'},{'double'},{1}];
    list(end+1,:) = [{'dindex'},{'Which deformation to solve? [Rx Ry Rz e11 e22 e33 e23 e13 e12]'},{'double'},{1}];
    list(end+1,:) = [{'dsteplim'},{'Maximum change in deformation in a step'},{'double'},{1}];
    list(end+1,:) = [{'dstepmode'},{'dstepmode (discr / continuous)'},{'char'},{1}];
    list(end+1,:) = [{'dmeancorr'},{'Do dmean correction (true/false)'},{'logical'},{1}];
    list(end+1,:) = [{'dsmoothing'},{'Maximum change in deformation in a step'},{'double'},{1}];
    list(end+1,:) = [{'fbbsize'},{'Factor for calculating flow bounding box size'},{'double'},{1}];
    list(end+1,:) = [{'fbbminsize'},{'Minimum flow bounding box size'},{'double'},{1}];
    
    fedpars = gtModifyStructure(fedpars, list, [], 'Fed parameters:');
else
    % read these options from fedpars
    disp(['LOAD VALUES FOR PHASE ' num2str(phase)])
    
    tmp=fedpars.strategy.(sprintf('s%d', phase));
    tmpfnames=fieldnames(tmp);
    fnames=fieldnames(fedpars);
    
    for i=1:length(tmpfnames)
        if any(strcmp(fnames, tmpfnames{i}))
            fedpars.(tmpfnames{i}) = tmp.(tmpfnames{i});
        end
    end
end


% Now deal with the data input

% blobs should be paired - make sure that if one blob is removed, so is the
% other
r=setdiff(fedpars.loadbl, fedpars.usebl);
for i=1:length(r)
    pair=floor((r(i)+1)/2);
    pairA=(2*(pair-1))+1;
    pairB=(2*(pair-1))+2;
    fedpars.usebl(find(fedpars.usebl==pairA))=[];
    fedpars.usebl(find(fedpars.usebl==pairB))=[];
end

nbl_active=length(fedpars.usebl);
nbl_total=length(bl);

% other fedpars fields that depend on the number of blobs used
fedpars.sufac = 10*ones(3, nbl_active/2);
fedpars.sumax = inf(3, nbl_active/2);



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% calculate the T matrices
for i=1:length(gv)

    % some fields of gv need to be pre-allocated to the correct size
    gv(i).T0=NaN(9,3*nbl_active);
    gv(i).T02=NaN(9,3*nbl_active/2);
    
    gv(i).fu  = NaN(3,nbl_active);    % actual force
    gv(i).fu2 = NaN(3,nbl_active/2);  % actual force, paired
    gv(i).su  = NaN(3,nbl_active);    % actual speed
    gv(i).su2 = NaN(3,nbl_active/2);  % actual speed, paired
    
    % these fields need to be full size for the c projector
    gv(i).uc0im  = NaN(3,nbl_total);  % initial (reference state) absolut [u,v,w] vector in image
    gv(i).uc0bl  = NaN(3,nbl_total);  % initial (reference state) [u,v,w] vector in blob (relative to blob origin, i.e. not the blob center)
    gv(i).uc     = NaN(3,nbl_total);

    
    U0_allpl = zeros(3*nbl_active,9);

    % collect all gv blob information
    for j=1:nbl_active
        % which blob are we interested in?
        b=fedpars.usebl(j);

        % collect the U0 terms
        U0_allpl((j-1)*3+1:j*3,:) = gv(i).U0(:,:,b);

        % collect the uc0 terms - these are full length - could condense
        % this to gv(i).uc0im = gv(i).uc0im_All
        gv(i).uc0im(:,b) = gv(i).uc0im_All(:,b);
        gv(i).uc0bl(:,b) = gv(i).uc0bl_All(:,b);
        gv(i).uc(:,b) = gv(i).uc_All(:,b);
    end

    % Calculate the T0 matrices
    if strcmpi(fedpars.T0strategy, 'A')
        % solve for T0 as normal
        [Usvd,Ssvd,Vsvd] = svd(U0_allpl,0);
        gv(i).T0 = Vsvd*diag(1./diag(Ssvd))*Usvd';
        % force lines of T0 to zeros
        gv(i).T0(find(~fedpars.dindex), :)=0;

    elseif strcmpi(fedpars.T0strategy, 'B')
        % remove terms from U0_allpl
        U0_allpl(:, find(~fedpars.dindex))=[];
        [Usvd,Ssvd,Vsvd] = svd(U0_allpl,0);
        tmpT0 = Vsvd*diag(1./diag(Ssvd))*Usvd';
        % restore to standard dimensions
        gv(i).T0=zeros(9, 3*nbl_active);
        gv(i).T0(find(fedpars.dindex), :)=tmpT0;

    else
        error('fedpars.T0strategy not recognised! problem...')
    end

    % Calculate the T02 matrices
    Tau = gv(i).T0(:,1:6:end);
    Tav = gv(i).T0(:,2:6:end);
    Taw = gv(i).T0(:,3:6:end);

    Tbu = gv(i).T0(:,4:6:end);
    Tbv = gv(i).T0(:,5:6:end);
    Tbw = gv(i).T0(:,6:6:end);

    gv(i).T02(:,1:3:end) = (Tau+Tbu)/2;
    gv(i).T02(:,2:3:end) = (Tav-Tbv)/2;
    gv(i).T02(:,3:3:end) = (Taw+Tbw)/2;

end

% repack fedpars into fedgrain
fedgrain.fedpars=fedpars;

% save updated structures
disp('saving updated fedgrain.mat and gv.mat')
save(fedgrain_name, 'fedgrain');
save(gv_name, 'gv');


end % end of function
