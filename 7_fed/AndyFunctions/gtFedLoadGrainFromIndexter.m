function [fedpars, fedgrain] = gtFedLoadGrainFromIndexter(grainid, indexter_grain, varargin)
% GTFEDLOADGRAINFROMINDEXTER  Setup fedgrain.mat and fedpars.mat
%     [fedpars, fedgrain] = gtFedLoadGrainFromIndexter(grainid, indexter_grain, varargin)
%     -----------------------------------------------------------------------------------
%     equivilent to the random grain in the
%     simulated data code.
%     nothing more than the indexter data "unpacked" into single blobs rather
%     than pairs.
%
%     general comment - at the moment fedgrain and fedpars are saved together
%     in one variable (fedgrain).  This can perhaps be cleaned.
%
%     improve, automate and clean so that it can be run form condor without
%     fucking around...
%
%     fedpars.mat is grain specific - therefore it should go in the grain
%     folder.  For the moment, let's introduce a folder structure called
%     7_fed/grain%d_/ and put fedpars / bl etc in there
%
%     add a path to the dct volume in parameters.fed.final_dct_volume
%     also add parameters.fed.volume_offset to allow for sample movements
%     (see fed_real_data_scheme script)
%
%     deal with renumbering in a sequence of datasets - ie in this scan it
%     might be grain X, but in the reference dataset and the dct volume it
%     might be grain Y.
%
%     INPUT:
%       grainid        = grain id
%       indexter_grain = output of indexter 
%                        (usually grain.mat/indexter_lastrun##.mat)
%   
%     OPTIONAL INPUT:
%       save_flag = save fedgrain.mat, fedpars.mat
%       max_el    = number of maximum elements
% 
%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% are we saving?
app.save_flag = false;
app.max_el    = 2000;
app=parse_pv_pairs(app,varargin);
if ~app.save_flag
    disp('gtFedLoadGrainFromIndexter is not saving fedgrain.mat')
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% load DCT parameters
parameters=[];
load('parameters.mat');
% indexter output is passed in as indexter_grain
grainid_str = num2str(grainid);
fedgrain_name = [parameters.fed.dir '/7_fed/grain' grainid_str '_/fedgrain.mat'];

%%%%%%%%%%%%%%%%%%%%%%%% INDEXTER OUTPUT INTO FEDGRAIN %%%%%%%%%%%%%%%%%%%%%%%%%%%%
% simplify/unpack blob data from indexter
a=indexter_grain{grainid};
fedgrain.nof_pairs = a.nof_pairs;
fedgrain.R_vector  = a.R_vector;


%%%%%%%%%%%%%%%%%%% GRAIN ID RENUMBERING IF PART OF SEQUENCE %%%%%%%%%%%%%%%%%%%%%%
% may need to consider renumbering
fedgrain.grainID   = grainid;
if ~isempty(parameters.fed.renumber_list)
    list=parameters.fed.renumber_list;
    % list: [referenceID, this dataset ID]
    ndx=find(list(:,2)==grainid);
    if isempty(ndx)
        disp('problem - this grain is not linked to a grain in the reference dataset')
        return
    end
    fedgrain.reference_grainid = list(ndx, 1);
else
    fedgrain.reference_grainid = grainid;
end

%%%%%%%%%%%%%%%%%%%%%%%% SETUP FEDPARS %%%%%%%%%%%%%%%%%%%%%%%%%%%%
% How many blobs?
nbl = 2*fedgrain.nof_pairs;

%.fed parameters definition
fedpars = gtFedParameters(nbl);

% add parameters / could be interactive or use defaults
fedpars = gtFedSetParameters(fedpars, 'defaults');

% add GrainID
fedpars.grainID = grainid;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% load general info from parameters.acq into fedpars
disp('copy experiment geometry from parameters to fedpars')
% fedpars = gtAddMatFile(fedpars,gtGetGeometry(parameters,'geo',true),true,true,true);
fedpars = gtAddMatFile(fedpars,gtGetGeometry(parameters,'opt',true),true,true,true);

%%%%%%%%%%%%%%%%%%%%%%%% INDEXTER OUTPUT INTO FEDGRAIN %%%%%%%%%%%%%%%%%%%%%%%%%%%%
% unpack variables, expanding pairs
fedgrain.pairid    = zeros(1, nbl);
fedgrain.difspots  = zeros(1, nbl);
fedgrain.thetatype = zeros(1, nbl);
fedgrain.hkl       = zeros(nbl, size(a.hkl, 2));
fedgrain.pl0       = zeros(nbl, 3);
fedgrain.theta     = zeros(1, nbl);
fedgrain.eta       = zeros(1, nbl);
fedgrain.omega     = zeros(1, nbl);

% expand pairs
fedgrain.pairid(1:2:end) = a.pairid;
fedgrain.pairid(2:2:end) = a.pairid;

fedgrain.difspots(1:2:end) = a.difspots(1:fedgrain.nof_pairs);
fedgrain.difspots(2:2:end) = a.difspots((fedgrain.nof_pairs+1):end);

fedgrain.thetatype(1:2:end) = a.thetatype;
fedgrain.thetatype(2:2:end) = a.thetatype;

fedgrain.hkl(1:2:end, :) = a.hkl;
fedgrain.hkl(2:2:end, :) = a.hkl;

fedgrain.pl0(1:2:end, :) = a.pl;
fedgrain.pl0(2:2:end, :) = -a.pl;

fedgrain.theta(1:2:end) = a.theta;
fedgrain.theta(2:2:end) = a.theta;

fedgrain.sinth0 = sind(fedgrain.theta);

fedgrain.eta(1:2:end) = a.eta;
fedgrain.eta(2:2:end) = mod(180-a.eta, 360);

fedgrain.omega(1:2:end) = a.omega;
fedgrain.omega(2:2:end) = a.omega+180;

warning('USING MEASURED DIFF ANGLES, SHOULD BE THEORETICAL!!!')
% here we should determine the specific hkls, and predict theta, eta and
% omega and pl0 based on these and the grain R-vector

fedpars.max_elements_grain=app.max_el;
% combine fedgrain and fedpars - as fedpars can be grain specific
fedgrain.fedpars=fedpars;

if ~app.save_flag
    check=inputwdefault('save fedgrain.mat (which includes fedgrain.fedpars)? [y/n]', 'y');
    if strcmpi(check, 'y')
        save(fedgrain_name, 'fedgrain');
    else
        disp('not saving...  you should do it by yourself')
    end
else
    % new directory structure
    save(fedgrain_name, 'fedgrain');
end

end % end function
