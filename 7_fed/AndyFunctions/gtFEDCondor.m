function gtFEDCondor(first, last, workingdirectory, varargin)
% GTFEDCONDOR  Run the FED code 
%     gtFEDCondor(first, last, [workingdirectory],[varargin])
%     -------------------------------------------------------
%       cleaned FED "do all" function for OAR
%
%     INPUT:
%       first            = first grain to be processed
%       last             = last grain to be processed
%       workingdirectory = current directory <string> {pwd}
%
%     OPTIONAL INPUT:
%       vertical = set up the vertical detector case <logical> {0}
%       gv_name  = grain volume name <string> {'5_reconstruction/dct.edf'}
%       saveflag = save data on disk <logical> {1}
% 
%


app.vertical  = false;
app.gv_name   = '5_reconstruction/dct.edf';
app.saveflag  = true; % want to save data
app.projector = 'matlab';
app=parse_pv_pairs(app,varargin);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% get parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% special case for running interactively in current directory
if ~exist('workingdirectory','var')
    workingdirectory=pwd;
end
if isdeployed
    global GT_DB
    global GT_MATLAB_HOME
    load('workspaceGlobal.mat');
    first=str2double(first);
    last=str2double(last);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% load data files
cd(workingdirectory);
parameters=[];
grain=[];
if exist('parameters.mat','file')
    load('parameters.mat'); % parameters
else 
    disp('Attention! the parameters file does not exist. Check the current directory')
    return
end
if exist('grain.mat','file')
    load('grain.mat') % grain
else
    disp('grain.mat not found')
    disp('Copy the grain.mat from the forward scan if it''s the case and restart the function')
    return
end
if exist(app.gv_name,'file')
    parameters.fed.dct_vol=app.gv_name;
    save('parameters.mat','parameters');
end

% load volume
if ~exist(parameters.fed.dct_vol,'file')
    file=inputwdefault('Insert the path of the grain volume .edf file:',parameters.fed.dct_vol);
    dct_vol=edf_read(file);
    parameters.fed.dct_vol=file;
    save('parameters.mat','parameters');
else
    dct_vol=edf_read(parameters.fed.dct_vol);
end
% apply dataset specific shift
dct_vol=gtPlaceSubVolume(zeros(size(dct_vol)), dct_vol, parameters.fed.dct_offset);
size(dct_vol)

    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% loop through grains
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for grainid = first:last
       
    % preparing strings for creating dir and saving files
    grainid_str=num2str(grainid);
    grain_dir     = [parameters.fed.dir '/7_fed/grain' grainid_str '_'];
    grainFed_name = [parameters.fed.dir '/gr' grainid_str '_fed.mat'];
    gv_name       = [parameters.fed.dir '/7_fed/grain' grainid_str '_/gv.mat'];
    bl_name       = [parameters.fed.dir '/7_fed/grain' grainid_str '_/bl.mat'];
    fedgrain_name = [parameters.fed.dir '/7_fed/grain' grainid_str '_/fedgrain.mat'];
    
    % create grain folder if needed
    [s,msg]=mkdir(grain_dir);disp(msg)
    
    if app.vertical==true
                
        % loading grainFed from gtFedGenerateRandomGrain.m
        grainFed=load(grainFed_name,'gr');
        grainFed=grainFed.gr;
        % load database information from findSpotsWoIndexter.m, special
        % case for vertical detector experiments
        bbox=load('bbox.mat');
        bbox=bbox.bbox;
        
        % setup fedgrain with grainFed; update grainFed
        [fedpars, fedgrain, grainFed] = gtFedLoadGrainWoIndexter(grainFed,bbox,'save_flag',app.saveflag);

    elseif app.vertical==false
       
        % setup fedgrain from indexter grain output
        % save into the above grain folder
        [fedpars, fedgrain] = gtFedLoadGrainFromIndexter(grainid,grain,'save_flag',app.saveflag);
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % setup FED grain
    % may need to select the right grain in the original volume
    [fedpars, gv] = gtFedLoadRealGrain(grainid,dct_vol,fedgrain,'save_flag',app.saveflag);
    fedgrain.fedpars = fedpars;
    fedgrain.projector = app.projector;
    save(fedgrain_name,'fedgrain');
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % load blobs
    [gv, bl, bluvol] = gtFedLoadRealBlobs(grainid,fedgrain,gv,'save_flag',app.saveflag);
    keyboard
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % run the solver
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    % do the 3 steps
    for fedstep=1:3
        step_str=num2str(fedstep);
        err_name = [parameters.fed.dir '/7_fed/grain' grainid_str '_/err' step_str '.mat'];
        
        % setup the TO matrices
        [fedpars, gv] = gtFedSetupT0(grainid,fedgrain,gv,bl,fedstep);
        fedgrain.fedpars = fedpars;
        
        % run the solver for this step
        [gv,bl,err] = gtFedSolve(gv,bl,[],[],[],fedgrain);
        save(err_name,'err');

        % make and save omega images
        bl=gtFedMakeOmegaImages(bl, 0);
        
        for i=1:length(bl)
            bl(i).imfed_omega_save(:,:,fedstep)=bl(i).imfed_omega;
        end
        
        save(bl_name, 'bl');

        % save d field of gv
        for i=1:length(gv)
            gv(i).d_save(:,fedstep)=gv(i).d;
        end
        
        save(gv_name, 'gv');
        disp(['@@@@@@@@@@@@@@@@@@@@@  done step ' step_str ' for grain ' grainid_str '  @@@@@@@@@@@@@@@@@@@@@@'])
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % compress bl for space saving...
    bl=gtFedCompressBl(bl);
    
    save(bl_name,'bl');
    disp(['grain ' grainid_str ' : bl variable compressed to save diskspace'])
    disp(['%%%%%%%%%%%%   grain ' grainid_str ' finished :-)   %%%%%%%%%%%'])


end % loop through grains


end % end of function
