function [gv, bl, bluvol] = gtFedLoadRealBlobs(grainid, fedgrain, gv, varargin)
% GTFEDLOADREALBLOBS  Load in a real set of blobs, and set up bl structure
%                     also add the U0 etc fields to the gv structure
%     [gv, bl, bluvol] = gtFedLoadRealBlobs(grainid, fedgrain, gv, varargin)
%     ----------------------------------------------------------------------
%     change to read full images, not blobs from database (might need to
%     process a little, and scale)
%
%     _geo version.  Try to get to work with Peter's new arbitrary geometry
%     code; all function replaced, and arguments checked as a few inconsistancies
%     found.
%
%     for smoothness - this function is always launched in the folder of the
%     dataste (parameters.acq.dir) and all paths to files are handled nicely.
%
%     this function - formerly gtFedLoadRealBlobs_geo, for arbitary geometry -
%     is now the standard function and as such is renamed gtFedLoadRealBlobs
%
%     INPUT:
%       grainid  = grain id
%       fedgrain = fed grain built with gtFedLoadGrainFrom(Wo)Indexter.m
%       gv       = ??
%
%     OPTIONAL INPUT:
%       save_flag = save fedgrain.mat, gv.mat
%       projector = projector to be used <string> [{'matlab'} / 'mex']
%      
%     OUTPUT:
%       gv     = ??
%       bl     = ??
%       bluvol = ??
%
%

app.save_flag = false;
app.projector = 'matlab';
app=parse_pv_pairs(app,varargin);
if ~app.save_flag
    disp('gtFedLoadRealBlobs is not saving bl.mat and gv.mat')
end

fedgrain.projector = app.projector;
% unpack from fedpars
fedpars = fedgrain.fedpars;

parameters=[];
load('parameters.mat');

% naming
grainid_str = num2str(grainid);
fedgrain_name = [parameters.fed.dir '/7_fed/grain' grainid_str '_/fedgrain.mat'];
gv_name       = [parameters.fed.dir '/7_fed/grain' grainid_str '_/gv.mat'];
bl_name       = [parameters.fed.dir '/7_fed/grain' grainid_str '_/bl.mat'];

gtDBConnect();

bluvol=[]; % if C projector is used, this might never appear, and will cause an error when the functon returns

blext = fedpars.blext;
gc    = fedpars.grainsize/2+[0.5 0.5 0.5] + fedpars.grainoff;
nbl=length(fedpars.loadbl);

% unpack from parameters
omstep=180/parameters.acq.nproj;

% dataset must be 360 degree
lastimage=(parameters.acq.nproj*2)-1;

% Sample geometry and origin of SAMPLE coord. system:
sam=gtSampleGeoInSampleSystem();

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% matrices for computing rotation tensors / arbitary geometry unpacking
% arbitary geometry information will all come from fedpars.
rotcomp    = gtFedRotationMatrixComp(fedpars.rotdir');
beamdir    = fedpars.beamdir';
% detector geometry
det.uvcen  = [fedpars.detucen, fedpars.detvcen]';
det.Q      = gtFedDetectorProjectionTensor(fedpars.detdiru', fedpars.detdirv', fedpars.detscaleu, fedpars.detscalev);
det.p      = fedpars.detpos'/parameters.acq.pixelsize; % mm / (mm/pixel) = pixel
if isfield(fedpars,'detdir')
    det.n  = fedpars.detdir';
else
    det.n  = cross(fedpars.detdiru, fedpars.detdirv)';
end
beamchroma = fedpars.beamchroma;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Load measured blob data into extended blobs
% note bl structure is blobA blobApair blobB blobBpair...
disp('setting up blob structure...')
tic
tmp       = gtFedBlobDefinition();
bl(1:nbl) = tmp;

% work though the blobs

for i=1:nbl

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % read data from indexter

    bl(i).hkl       = fedgrain.hkl(i,:);
    %bl(i).hklsp     = hklsp(i,:); % needed? fix gtAbsoluteIndexing if needed
    bl(i).thetatype = fedgrain.thetatype(i);
    bl(i).pl0       = fedgrain.pl0(i,:);        % initial plane normal (under initial strain)
    bl(i).th0       = fedgrain.theta(i);          % initial theta
    bl(i).sinth0    = fedgrain.sinth0(i);       % initial sin(theta)
    bl(i).eta0      = fedgrain.eta(i);         % initial eta
    bl(i).om0       = fedgrain.omega(i);          % initial omega

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % preallocate bl(i).combb - required for C projector
    bl(i).combb = [0 0 0];

    % determine diffraction data
    bl(i).S0 = gtFedRotationTensor(bl(i).om0,rotcomp);

    % Diffraction vector:
    % (gtFedPredictDiffVec takes the plane normal rotated to the lab
    % coordinate system)
    d0 = gtFedPredictDiffVec(bl(i).S0*bl(i).pl0', bl(i).sinth0, beamdir);
    bl(i).dvec0     = d0'/norm(d0);

    % Initial centroid u in image
    bl(i).u0im = gtFedPredictUVW(bl(i).S0, bl(i).dvec0', gc', det.p, det.n, det.Q, det.uvcen, bl(i).om0, omstep)';

    % previously, dfac calculated inside gtFedDerUmatrix...  now outside
    [bl(i).dfac0, bl(i).clab0]=gtFedPredictDiffFac(bl(i).S0, bl(i).dvec0', gc', det.p, det.n);

    % Maximum change due tfo strain is estimated for an element on an extreme
    % location, outside the sample, farthest from the detector.
    [Udef,Upos] = gtFedDerUmatrix(bl(i).pl0', bl(i).om0, bl(i).S0, rotcomp, ...
        [-sam.rad; sam.rad; 0], bl(i).sinth0, bl(i).dvec0', bl(i).dfac0', det.p, det.n, det.Q, beamdir, omstep, beamchroma);

    bl(i).Umax=Udef;

    % u limit for all elements (u displacement at extreme strain combination);
    %bl(i).ulim   = ceil(abs(bl(i).Umax)*(fedpars.dlim.*fedpars.dindex'))';
    bl(i).ulim   = ceil(abs(bl(i).Umax)*fedpars.dlim)';

    % pad to add to blobs for working
    if 0
        disp('use variable ulim + blext to extend blobs, (ulim*fbbsize)/2')
        bl(i).addbl  = bl(i).ulim + blext;
    elseif 0
        disp('use variable (ulim*fbbsize)/2 to extend blobs')
        bl(i).addbl  =  ceil(bl(i).ulim*(fedpars.fbbsize)/2);
    else
        disp('use fixed blext from fedpars to extend blobs')
        bl(i).addbl  = blext;
    end


    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % load in the blob volumes
    bl(i).difspotID=fedgrain.difspots(i);
    [vol,bb] = gtDBBrowseDiffractionVolume(parameters.acq.name, bl(i).difspotID);

    if strcmp(parameters.acq.name, 'al_plast_dct1_b_grid_') || ...
       strcmp(parameters.acq.name, 'al_plast_dct1_b_nogrid_763V_') || ...
       strcmp(parameters.acq.name, 'al_plast_dct1_b_nogrid_1000V_19N_')
   
        disp('... apply fix for renumbered datasets')
        [tmp1, tmp2]=mym(sprintf('select startimage, endimage from %sdifspot where difspotid=%d', parameters.acq.name, bl(i).difspotID));
        bb(3)=tmp1;
        bb(6)=tmp2-tmp1+1;

        % i have an example where bb(3)+bb(6)-1>nimages - add a fix to correct
        % for this
        if bb(3)+bb(6)-1>lastimage
            excess=bb(3)+bb(6)-1-lastimage;
            bb(6)=bb(6)-excess;
            vol(:,:,(bb(6)-excess+1):bb(6))=[];
            disp('applying a correction to the volume size from the datebase')
        end
        % another case is that the startimage is 0, but the blob starts
        % earlier, because of the 18 degree offset.
        % these need to be cropped from the front
        if bb(3)==0
            excess=size(vol, 3)-bb(6);
            vol(:,:,1:excess)=[];
        end
    else
        disp('assuming no renumbering problems!!!')
    end

    %  % transpose blob for u=first component, v=second
    vol = permute(vol, [2 1 3]);

    % Bounding box sizes and boundaries of the measured blob
    bl(i).mbbsize = bb(4:6);
    bl(i).mbbu = [bb(1), bb(1)+bb(4)-1];      % boundaries in u
    bl(i).mbbv = [bb(2), bb(2)+bb(5)-1];      % boundaries in v
    bl(i).mbbw = [bb(3), bb(3)+bb(6)-1];      % boundaries in w

    % Bounding box sizes and boundaries for processing
    bl(i).bbsize = [bb(4)+2*bl(i).addbl(1),bb(5)+2*bl(i).addbl(2),bb(6)+2*bl(i).addbl(3)];

    bl(i).bbu = bl(i).mbbu + [-bl(i).addbl(1), bl(i).addbl(1)];
    bl(i).bbv = bl(i).mbbv + [-bl(i).addbl(2), bl(i).addbl(2)];
    bl(i).bbw = bl(i).mbbw + [-bl(i).addbl(3), bl(i).addbl(3)];


    bl(i).bbcent = [round((bl(i).bbu(1)+bl(i).bbu(2))/2),...
        round((bl(i).bbv(1)+bl(i).bbv(2))/2),...
        round((bl(i).bbw(1)+bl(i).bbw(2))/2)];

    % check the limits of the blob bounding box should not exceed the data
    % also adjust bl(i).addbl because it is needed for the mask below
    if bl(i).bbw(1)<0
        bl(i).bbsize(3)=bl(i).bbsize(3)+bl(i).bbw(1);
        bl(i).bbw(1)=0;
        bl(i).addbl(3)=bl(i).addbl(3)+bl(i).bbw(1);
    end
    if bl(i).bbu(1)<1
        bl(i).bbsize(1)=bl(i).bbsize(1)+bl(i).bbu(1)-1;
        bl(i).bbu(1)=1;
        bl(i).addbl(1)=bl(i).addbl(1)+bl(i).bbu(1)-1;
    end
    if bl(i).bbv(1)<1
        bl(i).bbsize(2)=bl(i).bbsize(2)+bl(i).bbv(1)-1;
        bl(i).bbv(1)=1;
        bl(i).addbl(2)=bl(i).addbl(2)+bl(i).bbv(1)-1;
    end
    if bl(i).bbw(2)>lastimage
        bl(i).bbsize(3)=bl(i).bbsize(3)-bl(i).bbw(2)+lastimage;
        bl(i).bbw(2)=lastimage;
    end
    if bl(i).bbu(2)>parameters.acq.xdet
        bl(i).bbsize(1)=bl(i).bbsize(1)-bl(i).bbu(2)+parameters.acq.xdet;
        bl(i).bbu(2)=parameters.acq.xdet;
    end
    if bl(i).bbv(2)>parameters.acq.ydet
        bl(i).bbsize(2)=bl(i).bbsize(2)-bl(i).bbv(2)+parameters.acq.ydet;
        bl(i).bbv(2)=parameters.acq.ydet;
    end

    %keyboard
    % message to self - are the actual volume size and the bbsize
    % consistant?

    % Blob origin [u,v,w]: - compute after checking limits
    bl(i).bbor=[bl(i).bbu(1)-1,bl(i).bbv(1)-1,bl(i).bbw(1)-1];


    % Blob intensities:
    bl(i).intm = zeros(bl(i).bbsize);
    %  bl(i).intm = zeros(bl(i).bbsize([2, 1, 3]));


    if ~strcmp(parameters.acq.name, 'alumina_strain2_dct_')
        % read blob from full images
        for w=1:bl(i).bbsize(3)
            % which image
            ww=bl(i).bbw(1)+w-1;
            gtbb = [bl(i).bbu(1) bl(i).bbv(1) bl(i).bbu(2)-bl(i).bbu(1)+1 bl(i).bbv(2)-bl(i).bbv(1)+1];
            im=edf_read(sprintf('%s/1_preprocessing/full/full%04d.edf', parameters.acq.dir, ww), gtbb);
            bl(i).intm(:,:,w)=im'; % transpose for peter
        end
    else
        % if we don't have full images, use tge difblobs from database (vol)
        bl(i).intm(bl(i).addbl(1)+1:bl(i).addbl(1)+bb(4),...
            bl(i).addbl(2)+1:bl(i).addbl(2)+bb(5),...
            bl(i).addbl(3)+1:bl(i).addbl(3)+bb(6)) = vol;
    end

    % because of renumbering after segmentation, can have some bounding box
    % problems.  This is a bodge
    if strcmp(parameters.acq.name, 'al_plast_dct1_b_grid_')
        try
            % mask out unwanted stuff - hopefully gives better blob edges
            mask=zeros(size(bl(i).intm));
            mask(bl(i).addbl(1)+1:bl(i).addbl(1)+bb(4),...
                bl(i).addbl(2)+1:bl(i).addbl(2)+bb(5),...
                bl(i).addbl(3)+1:bl(i).addbl(3)+bb(6)) = vol;
            mask=imdilate(mask>0, ones(10,10,5));
        catch Mexc
            disp('problem with bounding box / mask for this blob')
            mask=ones(size(bl(i).intm));
        end
    else
        % mask out unwanted stuff - hopefully gives better blob edges
        mask=zeros(size(bl(i).intm));
        mask(bl(i).addbl(1)+1:bl(i).addbl(1)+bb(4),...
            bl(i).addbl(2)+1:bl(i).addbl(2)+bb(5),...
            bl(i).addbl(3)+1:bl(i).addbl(3)+bb(6)) = vol;
        mask=imdilate(mask>0, ones(10,10,5));
    end


    % apply mask
    bl(i).intm = bl(i).intm.*mask;

    % scale
    bl(i).intm = bl(i).intm * fedpars.grainvol/sum(bl(i).intm(:));

    % prepare other volumes
    bl(i).int0 = zeros(bl(i).bbsize);
    bl(i).int  = zeros(bl(i).bbsize);
    bl(i).intd = zeros(bl(i).bbsize);

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % deal with element - blob parameters

    % u in blob = u in image minus u at blob origin
    bl(i).u0bl = bl(i).u0im - bl(i).bbor;


    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % loop through the gv elements, adding U0 etc

    for j=1:length(gv)

        % Initial U of the first voxel
        [Udef,Upos] = gtFedDerUmatrix(bl(i).pl0', bl(i).om0, bl(i).S0, rotcomp, ...
            gv(j).cs1', bl(i).sinth0, bl(i).dvec0', bl(i).dfac0', ...
            det.p, det.n, det.Q, beamdir, omstep, beamchroma);

        gv(j).U0(:,:,i)=Udef;

        % Initial absolut u of gv center
        gv(j).uc0im(:,i) = gtFedPredictUVW(bl(i).S0, bl(i).dvec0', gv(j).cs', det.p, det.n, det.Q, det.uvcen, bl(i).om0, omstep);

        % Initial u of gv centre relative to blob origin
        gv(j).uc0bl(:,i) = gv(j).uc0im(:,i) - bl(i).bbor';

        % Actual u of gv centre
        gv(j).uc(:,i)  = [0; 0; 0];

    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    disp(['done blob ' num2str(i) '...'])
end

% repack fedpars into fedgrain
fedgrain.fedpars=fedpars;

% save updated gv and bl
if app.save_flag
    save(fedgrain_name,'fedgrain');
    save(bl_name, 'bl');
    save(gv_name, 'gv');
    disp('saving updated fedgrain.mat, gv.mat and bl.mat')
end


% Compute initial intensities
disp('Computing initial blob intensities...')

% make sure that we simulate all blobs at this initial stage
disp('we simulate all blobs at this initial stage')
fedpars.usebl=fedpars.loadbl;
if strcmpi(fedgrain.projector,'matlab')
    disp('using the (slow) matlab projector')
    [gv,bl,bluvol] = gtFedSpreadIntByVoxel(gv,bl,fedpars);
elseif strcmpi(fedgrain.projector,'mex')
    disp('using the fast C projector')
    mexFedSpreadIntByVoxel(gv,bl,fedpars,fedpars.crude_acceleration,0); % last argument is "flag" - at solution I think
else
    disp('Projector type not recognized. Please run by yourself the projector.')
    return
end

for i = 1:nbl
    bl(i).int0 = bl(i).int;
end

% also populate gv.uc_All, so that we can select only the interesting
% blobs
for i=1:length(gv)
    gv(i).uc0im_All=gv(i).uc0im;
    gv(i).uc0bl_All=gv(i).uc0bl;
    gv(i).uc_All=gv(i).uc;
end

% save updated gv and bl
if app.save_flag
    save(bl_name, 'bl');
    save(gv_name, 'gv');
    disp('saving updated gv.mat and bl.mat')
end

end % end of function