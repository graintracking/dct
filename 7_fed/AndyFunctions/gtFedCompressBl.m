function bl = gtFedCompressBl(bl)
% GTFEDCOMPRESSBL  Compress bl variable for storage
% bl = gtFedCompressBl(bl)

disp('does not save changes...  do this separately!')
disp('restore possible with gtFedDeCompressBl...')
pause(3)

for i=1:length(bl)
    
    % has bl previously been compressed? if it has, need to skip over int0,
    % which is not decompressed
    if all(size(bl(i).int)==size(bl(i).int0))
        do_int0=1;
    else
        do_int0=0;
    end
    
    if do_int0
        tmpvol=bl(i).intm+bl(i).int+bl(i).int0;
    else
        tmpvol=bl(i).intm+bl(i).int;
    end
    
    tmpvol=tmpvol>0;
    
    %    profile1
    profile1=sum(sum(tmpvol, 3), 2);
    %    profile2
    profile2=sum(sum(tmpvol, 3), 1);
    %    profile3
    profile3=squeeze(sum(sum(tmpvol, 1), 2));
    
    
    %   new bbu
    rangeu(1)=find(profile1, 1, 'first');
    rangeu(2)=find(profile1, 1, 'last');
    bl(i).newbbu(1)=bl(i).bbu(1)+rangeu(1)-1;
    bl(i).newbbu(2)=bl(i).newbbu(1)+rangeu(2)-rangeu(1);
    
    %   new bbv
    rangev(1)=find(profile2, 1, 'first');
    rangev(2)=find(profile2, 1, 'last');
    bl(i).newbbv(1)=bl(i).bbv(1)+rangev(1)-1;
    bl(i).newbbv(2)=bl(i).newbbv(1)+rangev(2)-rangev(1);
    
    %   new bbu
    rangew(1)=find(profile3, 1, 'first');
    rangew(2)=find(profile3, 1, 'last');
    bl(i).newbbw(1)=bl(i).bbw(1)+rangew(1)-1;
    bl(i).newbbw(2)=bl(i).newbbw(1)+rangew(2)-rangew(1);
    
    %   new volumes
    bl(i).intm=bl(i).intm(rangeu(1):rangeu(2), rangev(1):rangev(2), rangew(1):rangew(2));
    bl(i).int=bl(i).int(rangeu(1):rangeu(2), rangev(1):rangev(2), rangew(1):rangew(2));
    bl(i).intd=bl(i).intd(rangeu(1):rangeu(2), rangev(1):rangev(2), rangew(1):rangew(2));
    if do_int0
        bl(i).int0=bl(i).int0(rangeu(1):rangeu(2), rangev(1):rangev(2), rangew(1):rangew(2));
    end
    
    disp(['...blob ' num2str(i) ' compressed...'])
    
end

end % end of function

