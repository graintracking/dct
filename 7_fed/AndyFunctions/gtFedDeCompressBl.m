


function bl = gtFedDeCompressBl(bl, varargin)

% decompress bl variable for to continue analysis

save_flag=0;
if length(varargin)==1
    save_flag=varargin{1};
end

if all(size(bl(1).int)==bl(1).bbsize)
    disp('appears to have been decompressed already - skipping')
    return
end
    
if save_flag
    disp('WILL SAVE CHANGES TO BL!!!  ctrl-c now if you dont want this!')
    pause(3)
end


for i=1:length(bl)
    
    tmpvol=zeros(bl(i).bbsize);
    
    ustart=bl(i).newbbu(1)-bl(i).bbu(1)+1;
    vstart=bl(i).newbbv(1)-bl(i).bbv(1)+1;
    wstart=bl(i).newbbw(1)-bl(i).bbw(1)+1;
    
    uend=ustart + (bl(i).newbbu(2)-bl(i).newbbu(1));
    vend=vstart + (bl(i).newbbv(2)-bl(i).newbbv(1));
    wend=wstart + (bl(i).newbbw(2)-bl(i).newbbw(1));
    
    tmpvol(ustart:uend, vstart:vend, wstart:wend)=bl(i).int;
    bl(i).int=tmpvol;
    tmpvol(ustart:uend, vstart:vend, wstart:wend)=bl(i).intm;
    bl(i).intm=tmpvol;
    tmpvol(ustart:uend, vstart:vend, wstart:wend)=bl(i).intd;
    bl(i).intd=tmpvol;
    % don't need int0
    
    disp(sprintf('...blob %d decompressed...', i))
    
end

if save_flag
save bl bl -v7.3
end