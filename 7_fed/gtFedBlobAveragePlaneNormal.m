function grainout=gtFedBlobAveragePlaneNormal(bl,grvol,grain,dmvol,Eref)

npl=length(bl);


load('parameters.mat');

l=parameters.acq.dist/parameters.acq.pixelsize;
omstep=180/parameters.acq.nproj;
rotu=parameters.acq.rotx;

% Sample geometry and origin of SAMPLE coord. system:
sam=gtSampleGeoInSampleSystem;
orv=sam.sorZim;

% Get function handles:
[Lop,Lopd,Aop,Aopd,Pop,Nop,Dvec,Srot,dS_dom,dom_dD,dsinth_dD,...
 dd_dD,Qpro,du_dD,om2im]=gtFedFunctionHandles;




% grain centre of mass in SAMPLE ref.
grcom = gtFedVolumeCentroid(grvol) + grain.voloffset';


for i=1:npl

	% average omega
	om = bl(i).commim(3)*omstep;
	
	% grain center in LAB ref. (rotated with omega)
	gc = Srot(om)*grcom;

	% spot coordinates in LAB ref.
	spotlab = gtMatchSc2Lab(bl(i).commim(1),bl(i).commim(2),l,0,0,rotu,orv);
	
	% diffraction vector 
	dvec = spotlab - gc;
	dvec = dvec/norm(dvec);
	
	% plane normal in LAB ref.
	pllab = gtMatchPlaneNormInLab(dvec);
	
	% plane normal in SAMPLE ref.
	%   Srot maps a vector from SAMPLE to LAB ref.
	%   here we need the inverse
	pl = Srot(-om)*pllab;
	
	% theta
	th = gtMatchThetaOfPair(dvec);

	% eta
	eta = gtMatchEtaOfPoint(dvec);
	
	grainout.pl(i,:)      = pl;
	grainout.hkl(i,:)     = bl(i).hkl;
	grainout.hklsp(i,:)   = bl(i).hklsp;
	grainout.thetatype(i) = bl(i).thetatype;
	grainout.theta(i)     = th;
	grainout.eta(i)       = eta;
	grainout.omega(i)     = om;
	
end

grainout = gtIndexUniquePlaneNormals(grainout);

% to get the strain macros work, some constant errors are imposed
grainout.uni.ddy = grain.uni.ddy;
grainout.uni.ddz = grain.uni.ddz;
grainout.uni.dom = grain.uni.dom;


grainout = gtSTRAINAddLatticePar(grainout);
 
grainout = gtSTRAINGrains(grainout);




e11=dmvol(:,:,:,1);
e11=mean(e11(:));

e22=dmvol(:,:,:,2);
e22=mean(e22(:));

e33=dmvol(:,:,:,3);
e33=mean(e33(:));

e23=dmvol(:,:,:,4);
e23=mean(e23(:));

e13=dmvol(:,:,:,5);
e13=mean(e13(:));

e12=dmvol(:,:,:,6);
e12=mean(e12(:));



Eref = Eref

Eadd=[e11 e12 e13; e12 e22 e23; e13 e23 e33]

Etot=Eref+Eadd

Emes=grainout.strain.ST

Eerr= Emes-Etot


















