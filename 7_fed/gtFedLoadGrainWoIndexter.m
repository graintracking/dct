function [fedpars, fedgrain, gr] = gtFedLoadGrainWoIndexter(gr, bbox, varargin)
% GTFEDLOADGRAINWOINDEXTER  Prepare fedgrain,fedpars without indexter
%     [fedpars, fedgrain, gr] = gtFedLoadGrainWoIndexter(gr, bbox, varargin)
%     ----------------------------------------------------------------------
%     Usefull when there is not the output from indexter, only grain position
%     and orientation.
%
%     INPUT:
%       gr   = output of gtFedGenerateRandomGrain(grain,[vert],[pars],[varargin])
%       bbox = output of gtOptimizeFindSpotsWoIndexter(parameters,uvfed,omegafed,bbsizemean,areamin,areamax)
%   
%     OPTIONAL INPUT:
%       save_flag = save fedgrain.mat, fedpars.mat
%       debug     = print out comments
%       max_el    = number of maximum elements
% 
%     In order to have the proper input, run this before to obtain grain and bbox structures and the optimized geometry.
%     [newgeo,bbox,full,gr]=gtOptimizeVerticalDetector(grain,parameters,bbxsize,areamin,areamax,[color],[greys],[save])
%
%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% are we saving?
app.save_flag = false;
app.debug     = false;
app.max_el    = 2000;
app=parse_pv_pairs(app,varargin);
if ~app.save_flag && app.debug
    disp('gtFedLoadGrainWoIndexter is not saving fedgrain.mat')
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% load DCT parameters
parameters=[];
load('parameters.mat');
% indexter output is passed in as indexter_grain


gr.difspots = bbox.expected_id;
gr.indsorted = bbox.indsorted;

gr.table=[gr.allblobs.omega gr.allblobs.eta gr.allblobs.theta gr.allblobs.thetatype gr.allblobs.hkl gr.allblobs.pl];
gr.tabledb=gr.table(gr.indsorted,:); 
gr.tablebbox=[gr.tabledb bbox.expected_id gr.indsorted];
gr.tabletitle='omega eta theta thetatype hkl pl';
gr.tablebboxtitle=[gr.tabletitle ' difspotID indsorted'];

% check existence of spot pairs
% omega2 = omega1 + 180
% eta2 = mod(180 - eta1 , 360)
% pl2 = -pl1
% theta2 = theta1
% hkl2= hkl1
% thetatype2 = thetatype1

count = 0;
pairid = [];
pairinddb = [];
pairindgl = [];

for i=1:length(gr.indsorted)-1
    spot1=gr.tablebbox(i,:);
    
    for j=i+1:length(gr.indsorted) % search from i+1 to i+5
        spot2=gr.tablebbox(j,:);
        if app.debug
            fprintf('i = %d    j = %d\n',i,j)
            disp('omega check:')
            disp([spot1(1)+180 spot2(1)])
            disp('eta check:')
            disp([mod(180-spot1(2),360) spot2(2)])
            disp('theta check:')
            disp([spot2(3) spot1(3)])
            disp([spot2(4) spot1(4)])
            disp('hkl check:')
            disp(spot2(5:7))
            disp(spot1(5:7))
            disp('pl check:')
            disp(spot2(8:10))
            disp(-spot1(8:10))
        end
        if (spot2(1)-(spot1(1)+180))<0.0001 && ...
                (spot2(2)-(mod(180-spot1(2),360)))<0.0001 && ...
                spot2(3) == spot1(3) && ...
                spot2(4) == spot1(4) && ...
                isequal(spot2(5:7),spot1(5:7)) && ...
                isequal(spot2(8:10),-spot1(8:10))
            
            count=count+1;
            if app.debug 
                disp('pair found')
                fprintf('pairid = %d    i = %d    j = %d\n',count,i,j)
                disp('*******************************************************************')
            end
            pairid=[pairid;count;count];
            pairinddb=[pairinddb;i;j];
            pairindgl=[pairindgl;gr.indsorted(i);gr.indsorted(j)];
        else
            if app.debug
                disp('this is not a pair')
                disp('----------------------------------------')
            end
        end % end if

    end % end for j

end % end for i

% save pairs info into gr
gr.nof_pairs      = length(pairid)/2;
gr.pairid         = pairid;
gr.pairinddb      = pairinddb;
gr.pairindgl      = pairindgl;
gr.pairdifspotsID = gr.tablebbox(gr.pairinddb,end-1);

% create fedgrain
fedgrain.nof_pairs         = gr.nof_pairs;
fedgrain.R_vector          = gr.R_vector;
fedgrain.grainID           = gr.id;
fedgrain.reference_grainid = gr.id;
fedgrain.pairid            = gr.pairid;
fedgrain.difspots          = gr.pairdifspotsID;

fedgrain.thetatype = gr.allblobs.thetatype(gr.pairindgl);
fedgrain.hkl       = gr.allblobs.hkl(gr.pairindgl,:);
fedgrain.pl0       = gr.allblobs.pl(gr.pairindgl,:);
fedgrain.theta     = gr.allblobs.theta(gr.pairindgl);
fedgrain.eta       = gr.allblobs.eta(gr.pairindgl);
fedgrain.omega     = gr.allblobs.omega(gr.pairindgl);
fedgrain.sinth0    = sind(fedgrain.theta);


%%%%%%%%%%%%%%%%%%%%%%%% SETUP FEDPARS %%%%%%%%%%%%%%%%%%%%%%%%%%%%
% How many blobs?
nbl=2*fedgrain.nof_pairs;

% FED parameters definition
fedpars = gtFedParameters(nbl);

% add parameters / could be interactive or use defaults
fedpars = gtFedSetParameters(fedpars, 'defaults');

% add GrainID
fedpars.grainID = gr.id;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% load general info from parameters.acq into fedpars
disp('copy experiment geometry from parameters.acq to fedpars')
fedpars = gtAddMatFile(fedpars,gtGetGeometry(parameters,'opt',true),true,true,true);

fedpars.max_elements_grain = app.max_el;
% combine fedgrain and fedpars - as fedpars can be grain specific
fedgrain.fedpars = fedpars;

%%%%%%%%%%%%%%%%%%%%%%%%%%%
% saving...
if ~app.save_flag
    a=inputwdefault('save fedgrain.mat (which includes fedgrain.fedpars)? [y/n]', 'y');
    if strcmpi(a, 'y')
        save([parameters.fed.dir '/7_fed/grain' num2str(gr.id) '_/fedgrain.mat'], 'fedgrain');
    else
        disp('not saving...  you should do it by yourself')
    end
else
    % new directory structure
    save([parameters.fed.dir '/7_fed/grain' num2str(gr.id) '_/fedgrain.mat'], 'fedgrain');
end

if app.debug
    disp('### Exiting from gtFedLoadGrainFromIndexter... ###')
    disp(' ')
end

end % end function 
