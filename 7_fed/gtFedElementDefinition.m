%
% FUNCTION v=gtFedVolumeStructure(nbl,grsamp,nd)
%
% Defines and creates the data structure in which the volume element data 
% are stored (one for each element).
% 
% INPUT  nbl    - number of blobs (reflections)
%        grsamp - element size in voxels (sizeX,sizeY,sizeZ)
%        nd     - number of strain components to resolve
%
% OUTPUT structure containing NaN-s
%

function v = gtFedElementDefinition(nbl,grsamp,nd)

if ~exist('nd','var')
	nd = 9;
end


% Volume represented by the element: 
v.ind      = NaN(1,3);  % element indices in grain volume (ordered in X,Y,Z: v(1)-> [1,1,1]; v(2)-> [2,1,1] )
v.active   = NaN ;      % if false, it's currently not used (zero intensity)
v.totint   = NaN;       % total intensity (volume) represented by the element
v.complete = false;     % element completeness (has the maximum intensity)
v.vol      = false(grsamp); % 3D volume represented by the element
v.voxindrel= NaN(3,grsamp(1)*grsamp(2)*grsamp(3)); % indices of voxels in element relative to voxel [1,1,1]

% Position
v.cs1 = NaN(1,3);       % coordinates of voxel (1,1,1) of element in SAMPLE coord.
v.cs  = NaN(1,3);       % coordinates of element center in SAMPLE coord.
v.csgr= NaN(1,3);       % element geometric center in grain volume in voxels

% Displacement - deformation transformation matrices
v.U0  = NaN(3,nd,nbl);  % initial U matrix (deformation -> displacement)
v.T0  = NaN(nd,3*nbl);  % initial 'inverse' of U matrix (displacement -> deformation) (see Moore-Penrose pseudoinverse on wikipedia)
v.T02 = NaN(nd,3*nbl/2);% initial 'inverse' of U2 matrix

% Deformation state:
v.d   = NaN(nd,1);      % actual deformation state (nd deformation tensor components)
v.dd  = NaN(nd,1);      % last applied change to d
v.dm  = NaN(nd,1);      % measured (real) deformation state from linear interpolation
v.dmcub = NaN(nd,1);    % measured (real) deformation state from cubic interpolation

% Coordinates of the element center in blobs:
v.uc0im  = NaN(3,nbl);  % initial (reference state) absolut [u,v,w] vector in image coordinates
v.uc0bl  = NaN(3,nbl);  % initial (reference state) absolut [u,v,w] vector in blob coordinates (i.e. relative to blob origin, not to blob center)
v.uc     = NaN(3,nbl);  % actual [u,v,w] deviation from reference state (v.uc0bl) in blob coordinates;
v.ucbl   = NaN(3,nbl);  % actual absolut [u,v,w] in blob coordinates ( = v.uc0bl + v.uc)
v.ucerr  = NaN(3,nbl);  % actual error in uc, i.e. deviation from known solution (for simulation and development) *changed from nbl/2 to nbl because otherwise crashes the c projector!
v.ucmim  = NaN(3,nbl);  % ideal, measured, absolut uc in image coordinates (for simulation and development)
v.ucmbl  = NaN(3,nbl);  % ideal, measured, absolut uc in blob coordinates (for simulation and development)
v.ucm    = NaN(3,nbl);  % ideal, measured uc (for simulation and development)
v.uclim  = NaN(3,nbl);  % u limit applied in iteration

% Differentials of applied displacements in iteration:
v.fu  = NaN(3,nbl);     % actual force for displacement
v.fu2 = NaN(3,nbl/2);   % actual force, paired
v.su  = NaN(3,nbl);     % actual speed for displacement
v.su2 = NaN(3,nbl/2);   % actual speed, paired

% Extra info
v.ulinerr = NaN(3,nbl); % maximum error in [u,v,w] of the element in all the blobs from linear appr...
v.dlinerr = NaN(nd,1);  % the error it would cause in the fitted strain components

%v.D0  = NaN(nd,1);     % initial deformation state
%v.udvoxs= NaN(3,grsamp(1)*grsamp(2)*grsamp(3),nbl);  % u differential for additional voxels of v other than the first one ([u,v] difference only)
%v.udc   = NaN(3,nbl);  % u difference between element v center and its first voxel (constant) (uc = u + udc)
