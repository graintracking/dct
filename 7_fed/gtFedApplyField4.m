function [gv,bl]=gtFedApplyField4(gv,bl,fedpars)



nbl = length(bl);
nv  = length(gv);

dlim = fedpars.dlim;
ngv = fedpars.ngv;


%fedpars.dstep = 0.00005*ones(9,1);
%fedpars.dstepmode = 'discr';


if strcmp(fedpars.dstepmode,'discr')
	dstepdiscrete = true;
else
	dstepdiscrete = false;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Start with measured mean omegas of the blobs

disp(' Applying flow field...  ')
tic

if isempty(fedpars.fbbsize)
	fedpars.fbbsize = 0.5;
end

for i = 1:nbl

	% Force depends on 1) intensity difference between measured and simulated
	% and 2) distance (linearly decreasing with distance from actual u).
	% Size of sampled volume is a fraction of the whole blob.
	
	% bbsize is odd
	%fbbsize = bl(i).ulim + mod(bl(i).ulim,2);
	
		
		
	% bbsize is odd
	fbbsizebase = max(bl(i).ulim*fedpars.fbbsize,[4 4 4]);
	fbbsize =  fbbsizebase - mod(fbbsizebase,2);
	
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
% 	fforceu = zeros(fbbsize);
% 	fforcev = zeros(fbbsize);
% 	fforcew = zeros(fbbsize);
% 	
% 	%freg = zeros([bl(i).ulim 3]);
% 	%uregcent = [size(ureg,1); size(ureg,2); size(ureg,3)]/2 + 0.5;
% 	
% 	for j=1:fbbsize(1)/2
% 		fforceu(j,:,:) = j;
%   	fforceu(fbbsize(1)+1-j,:,:) = -j;
% 	end
% 	fforceu = fforceu/sum(abs(fforceu(:)));
% 	
% 	for j=1:fbbsize(2)/2
% 		fforcev(:,j,:) = j;
%   	fforcev(:,fbbsize(2)+1-j,:) = -j;
% 	end
% 	fforcev = fforcev/sum(abs(fforcev(:)));
% 
% 	for j=1:fbbsize(3)/2
% 		fforcew(:,:,j) = j;
%   	fforcew(:,:,fbbsize(3)+1-j) = -j;
% 	end
% 	fforcew = fforcew/sum(abs(fforcew(:)));
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


  % fforce0 is the multiplicator factor as a function of distance in u,v,w.

	fforceu0 = zeros(fbbsize);
	fforcev0 = zeros(fbbsize);
	fforcew0 = zeros(fbbsize);
	
	%freg = zeros([bl(i).ulim 3]);
	%uregcent = [size(ureg,1); size(ureg,2); size(ureg,3)]/2 + 0.5;
	
	for j=1:fbbsize(1)/2
		fforceu0(j,:,:) = j; %j^2; %1; %j;
  	fforceu0(fbbsize(1)+1-j,:,:) = -j; %-j^2; %-1; %-j;
	end
	
	for j=1:fbbsize(2)/2
		fforcev0(:,j,:) = j; %j^2; %1; %j;
  	fforcev0(:,fbbsize(2)+1-j,:) = -j; %-j^2; %-1; %-j;
	end

	for j=1:fbbsize(3)/2
		fforcew0(:,:,j) = j; %j^2; %1; %j;
  	fforcew0(:,:,fbbsize(3)+1-j) = -j; %-j^2; %-1; %-j;
	end

	for j=1:nv		
		
		% absolut u of element center in blob
		gvcentexact = gv(j).uc(:,i) + gv(j).uc0bl(:,i);
		% the same rounded:
		gvcentround = floor(gvcentexact) + 0.5;
		% offset from rounded value:
		gvcentoffs  = gvcentexact - gvcentround;
		
		fforceu = fforceu0 - gvcentoffs(1);
		fforceu = fforceu/sum(abs(fforceu(:)));
		
		fforcev = fforcev0 - gvcentoffs(2);
		fforcev = fforcev/sum(abs(fforcev(:)));
	
		fforcew = fforcew0 - gvcentoffs(3);
		fforcew = fforcew/sum(abs(fforcew(:)));

		
		blbbu = gvcentround(1)+0.5-fbbsize(1)/2 : gvcentround(1)-0.5+fbbsize(1)/2 ;
		blbbv = gvcentround(2)+0.5-fbbsize(2)/2 : gvcentround(2)-0.5+fbbsize(2)/2 ;
		blbbw = gvcentround(3)+0.5-fbbsize(3)/2 : gvcentround(3)-0.5+fbbsize(3)/2 ;

		% Intensity difference in the sampled volume around u
		fintd = bl(i).intd(blbbu,blbbv,blbbw);
		
		fu            = fintd.*fforceu;
		gv(j).fu(1,i) = sum(fu(:));
		
		fv            = fintd.*fforcev;
		gv(j).fu(2,i) = sum(fv(:));
	
		fw            = fintd.*fforcew;
		gv(j).fu(3,i) = sum(fw(:));
		
	end

% 	hist(allfu(:,2))
% 	hist(allfv(:))
% 	hist(allfw(:))
	
%   bl(i).su(1) = min( std(allfu(:,i)),  bl(i).ulim(1)/4 );
%   bl(i).su(1) = max( bl(i).su(1)    , -bl(i).ulim(1)/4 );
% 	
%   bl(i).su(2) = min( std(allfv(:,i)),  bl(i).ulim(2)/4 );
%   bl(i).su(2) = max( bl(i).su(2)    , -bl(i).ulim(2)/4 );
% 
% 	bl(i).su(3) = min( std(allfw(:,i)),  bl(i).ulim(3)/4 );
%   bl(i).su(3) = max( bl(i).su(3)    , -bl(i).ulim(3)/4 );
% 	
% 	blsu(:,i) = bl(i).su';

%	blsu = [];
	
end


% if isempty(blsuin)
% 	blsuin = blsu;
% end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

dnewvol = zeros(ngv(1),ngv(2),ngv(3),9);
dnewvol2 = zeros(ngv(1),ngv(2),ngv(3),9);

% Loop through elements
for i = 1:nv
	
	U0act = [];
	fuact = [];

	% Loop through Friedel pairs (every second plane normal)
	for j = 1:2:nbl

		fAu = gv(i).fu(1,j);
		fBu = gv(i).fu(1,j+1);
		if fAu*fBu > 0
  		funew = (fAu + fBu)/2;
			gv(i).fu2(1,(j+1)/2) = funew;
			U0act = [U0act; mean(gv(i).U0(1,:,j:j+1),3)];
			fuact = [fuact; funew];
		else  % contradictory guess
  		gv(i).fu2(1,(j+1)/2) = 0;
		end
		
		fAv = gv(i).fu(2,j);
		fBv = gv(i).fu(2,j+1);
		if fAv*fBv < 0
			funew = (fAv - fBv)/2;
  		gv(i).fu2(2,(j+1)/2) = funew;
			U0act = [U0act; mean(gv(i).U0(2,:,j:j+1),3)];
			fuact = [fuact; funew];
		else  % contradictory guess
  		gv(i).fu2(2,(j+1)/2) = 0;
		end
		
		fAw = gv(i).fu(3,j);
		fBw = gv(i).fu(3,j+1);
		if fAw*fBw > 0
			funew = (fAw + fBw)/2;
  		gv(i).fu2(3,(j+1)/2) = funew; 
			U0act = [U0act; mean(gv(i).U0(3,:,j:j+1),3)];
			fuact = [fuact; funew];
		else  % contradictory guess
  		gv(i).fu2(3,(j+1)/2) = 0;
		end		
		
	end

  
	% Speed factor for optimum solution:
	fu2s = gv(i).fu2;
	fu2s(fu2s==0) = NaN;
	gv(i).fac2m = (gv(i).ucm(:,1:2:end) - gv(i).su2)./fu2s;

	%gv(i).su2 = min(gv(i).fu2.*fedpars.sufac, fedpars.sumax);
	%gv(i).su2 = max(gv(i).su2, -fedpars.sumax);
	gv(i).su2 = gv(i).fu2.*fedpars.sufac;

	gv(i).solsr = gv(i).su2./(gv(i).ucm(:,1:2:end) - gv(i).uc(:,1:2:end));
	
	% Change to be applied to d:
	%ddact = gv(i).T02*gv(i).su2(:);
	ddact = U0act\fuact;

	% Discretize d change
	if dstepdiscrete
		ddact = sign(ddact).*fedpars.dsteplim;
	else
		ddact = min(ddact,fedpars.dsteplim);
		ddact = max(ddact,-fedpars.dsteplim);
	end
	
	
	%%%%%
	%ddact = (gv(i).dm - gv(i).d);
	%%%%%
	
	
	dnew = gv(i).d + ddact;
	dnew = min(dnew,dlim);
	dnew = max(dnew,-dlim);

	gv(i).do = gv(i).d;
	gv(i).df = dnew;
	gv(i).d  = dnew;

	dnewvol(gv(i).ind(1),gv(i).ind(2),gv(i).ind(3),:) = dnew;

end


% Loop through elements
	
% else  % 3 wbl-s
% In case of 3 blobs	
% 	for i = 1:nv
% 
% 		gv(i).f(:) = [allfu(i,:); allfv(i,:); allfw(i,:)];
% 		gv(i).su  = gv(i).f.*blsuin;
% 	
% 		U0_allwbl =[];
% 		su_allwbl = [];
% 
% 		for j=wbl
% 			U0_allwbl = [U0_allwbl; gv(i).U0(:,:,j)];
% 			su_allwbl = [su_allwbl; gv(i).su(:,j)];
% 		end
% 				
% 
% 		invU0 = inv(U0_allwbl);
% 
% 
% 		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 		%gv(i).su = (gv(i).um -gv(i).u);%/2;
% 		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% 		dnew = gv(i).d + invU0*su_allwbl;
% 		dnew = min(dnew,dlim);
% 		dnew = max(dnew,-dlim);
% 
% 		gv(i).do = gv(i).d;
% 		gv(i).df = dnew;
% 		gv(i).d = dnew;
% 
% 		dfvol(gv(i).ind(1),gv(i).ind(2),gv(i).ind(3),:) = dnew;
% 
% 	end



if fedpars.dsmoothing

	% Smoothing of solution by taking the mean values in the neighbourhood of
	% a voxel

	disp(' Smoothing d components over elements...')
	
	for i = 1:ngv(1)
		for j = 1:ngv(2)
			for k = 1:ngv(3)

				lim1l = max(i-1,1);
				lim1h = min(i+1,ngv(1));
				lim2l = max(j-1,1);
				lim2h = min(j+1,ngv(2));
				lim3l = max(k-1,1);
				lim3h = min(k+1,ngv(3));
% 
% 				for id = 1:9
% 					dfvals = dfvol(lim1l:lim1h,lim2l:lim2h,lim3l:lim3h,id);
% 					gv(sub2ind(ngv,i,j,k)).d(id) = mean(dfvals(:));
% 				end

				for id = 1:9
					dvals(1) = dnewvol(lim1l,j,k,id);
					dvals(2) = dnewvol(lim1h,j,k,id);
					dvals(3) = dnewvol(i,lim2l,k,id);
					dvals(4) = dnewvol(i,lim2h,k,id);
					dvals(5) = dnewvol(i,j,lim3l,id);
					dvals(6) = dnewvol(i,j,lim3h,id);
					dvals(7) = dnewvol(i,j,k,id);
					
					meand = mean(dvals(:));
					
					gv(sub2ind(ngv,i,j,k)).d(id) = meand;

					dnewvol2(i,j,k,id) = meand;
				end		
				
% 				for id = 1:9
% 					
% 					dfvals(1) = dfvol(i-1,j,k,id);
% 					dfvals(2) = dfvol(i+1,j,k,id);
% 					dfvals(3) = dfvol(i,j-1,k,id);
% 					dfvals(4) = dfvol(i,j+1,k,id);
% 					dfvals(5) = dfvol(i,j,k-1,id);
% 					dfvals(6) = dfvol(i,j,k+1,id);
% 					
% 					% New value is mean of neighbouring values and its own value:
% 					gv(sub2ind(ngv,i,j,k)).d(id) = mean([mean(dfvals(:)) dfvol(i,j,k,id)]);
% 					
% 				end
				
			end
		end
	end

else
	disp(' No smoothing on d components applied...')
end


% d mean correction
if fedpars.dmeancorr
	disp('Applying mean correction ... ')
	if isfield(fedpars,'U0')
		U0 = fedpars.U0;
	else
		U0tmp = zeros(3,9,nbl);
		for j = 1:nv
			U0tmp = U0tmp + gv(j).U0;
		end

		U0 = zeros(3*nbl,9);
		for j = 1:nbl
			U0(3*j-2:3*j,:) = U0tmp(:,:,j);
		end

		U0 = U0/nv;
	end

	% d difference in blobs
	dubl = zeros(3*nbl,1);
	for i = 1:nbl
		dubl(3*i-2:3*i,1) = bl(i).commbb - bl(i).combb;
	end

	dd0 = U0\dubl;

	ddgvmean = zeros(9,1);
	for k = 1:9
		tmp = dnewvol2(:,:,:,k);
		ddgvmean(k,1) = mean(tmp(:));
	end

	ddcorr = dd0 - ddgvmean;

else
	ddcorr = zeros(9,1);
end




% Save d difference from current step:
for i=1:nv
	gv(i).d = gv(i).d + ddcorr;
	gv(i).dd = gv(i).d - gv(i).do;
end



% Interpolation of d for each voxel:

% % behaviour of meshgrid and interp3 tested OK
% if 1
% 	
% 	dvol1 = zeros(ngv);
% 	dvol2 = zeros(ngv);
% 	dvol3 = zeros(ngv);
% 	dvold = zeros(ngv(1),ngv(2),ngv(3),9);
% 	dvoli = zeros(grsize(1),grsize(2),grsize(3),9);
% 
% 	[dvox1,dvox2,dvox3] = meshgrid(1:grsize(1),1:grsize(2),1:grsize(3));
% 
% 	for j = 1:nv
% 		% input coordinates for interpolation on a grid
% 		% flip coordinates 1 and 2 for meshgrid !!!
% 		dvol1(gv(j).ind(1),gv(j).ind(2),gv(j).ind(3)) = gv(j).cs1(2);
% 		dvol2(gv(j).ind(1),gv(j).ind(2),gv(j).ind(3)) = gv(j).cs1(1);
% 		dvol3(gv(j).ind(1),gv(j).ind(2),gv(j).ind(3)) = gv(j).cs1(3);
% 		% input values for interpolation on a grid
% 		for i = 1:9
% 			dvold(gv(j).ind(1),gv(j).ind(2),gv(j).ind(3),i) = gv(j).dm(i);
% 		end
% 	end
% 
% 	for i = 1:9
% 		dvoli(:,:,:,i) = interp3(dvol1,dvol2,dvol3,dvold(:,:,:,i),dvox1,dvox2,dvox3,'*linear');
% 	end
% 
% end






toc




