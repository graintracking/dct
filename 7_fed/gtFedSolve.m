function [gv,bl,err]=gtFedSolve(gv,bl,bluvol,bluvolsol,err,fedgrain)
%     [gv,bl,err]=gtFedSolve(gv,bl,bluvol,bluvolsol,err,fedgrain)
%
%

fedpars=fedgrain.fedpars;

nbl = length(bl);
nv = length(gv);

if isempty(err)
    for j=1:nbl
        bl(j).err = std(bl(j).intd(:),1);
        err.bl(1,j)  = bl(j).err;
    end
    
    errdi = zeros(9,nv);
    for i = 1:nv
        errdi(:,i) = gv(i).d-gv(i).dm;
    end
    err.d(1,:) = std(errdi,1,2)';
end

close all

if 0
    fhsolsru = figure('name','su sol rat');
    fhsolsrv = figure('name','sv sol rat');
    fhsolsrw = figure('name','sw sol rat');
    
    fhsoldn = figure('name','dn sol rat');
    fhsolds = figure('name','ds sol rat');
    fhsoldr = figure('name','dr sol rat');
    fherru = figure('name','u err');
    fherrv = figure('name','v err');
    fherrw = figure('name','w err');
    fhbluvoldif = figure('name','bluvoldif');
    fherrdn = figure('name','dn err');
    fherrds = figure('name','ds err');
    fherrdr = figure('name','dr err');
end

fherr = figure('name','Mean error in blobs');
fherrd = figure('name','Mean error in d comps.');
fdcorr = figure('name','d correlation');
axis equal

fhistd1mes = figure('name','Measured d1 comp histogram');
fhistd1act = figure('name','Actual d1 comp histogram');

dmes = [gv(:).dm];
dbins = -0.01:0.0005:0.01;

figure(fhistd1mes)
hist(dmes(1,:),dbins)

% fspot1m = figure('name','Spot 1 mes');
% fspot1a = figure('name','Spot 1 act');
% fspot2m = figure('name','Spot 2 mes');
% fspot2a = figure('name','Spot 2 act');

tilefig();

fedpars.iter=10;
disp('HARD-CODED iterations')

for i=1:fedpars.niter
    
    fprintf('ITERATION %d \n',i)
    
    switch fedpars.solver
        case 'PolyChrFlow'
            [gv,bl] = gtFedApplyFieldFlowPolyChr(gv,bl,fedpars);
        case 'Flow'
            [gv,bl] = gtFedApplyFieldFlow(gv,bl,fedpars);
        case 'Flow4'
            [gv,bl] = gtFedApplyField4(gv,bl,fedpars);
        case 'Gradient'
            [gv,bl] = gtFedApplyFieldGradient(gv,bl,bluvol,fedpars);
        case 'LSQRSing'
            [gv,bl] = gtFedApplyFieldLSQRSing(gv,bl,bluvol,bluvolsol,fedpars);
        case 'LSQR'
            [gv,bl] = gtFedApplyFieldLSQR2(gv,bl,bluvol,bluvolsol,fedpars);
        case 'Neighbour'
            [gv,bl] = gtFedApplyFieldNeighbour(gv,bl,bluvol,bluvolsol,fedpars);
        otherwise
            error('Specified solver no recognised.')
    end
    
    if strcmpi(fedgrain.projector,'matlab')
        disp('using the (slow) matlab projector')
        [gv,bl,bluvol] = gtFedSpreadIntByVoxel(gv,bl,fedpars);
    elseif strcmpi(fedgrain.projector,'mex')
        disp('using the fast C projector')
        mexFedSpreadIntByVoxel(gv,bl,fedpars,fedpars.crude_acceleration,0); % last argument is "flag" - at solution I think
    else
        disp('Projector type not recognized. Please run by yourself the projector.')
        return
    end
    
    err.bl(end+1,:) = 0;
    for j=1:nbl
        bl(j).err  = std(bl(j).intd(:),1);
        err.bl(end,j) = bl(j).err;
    end
    
    blxint(:,:,i) = sum(bl(1).int,3);
    
    erru = [];
    errv = [];
    errw = [];
    soldn = [];
    solds = [];
    soldr = [];
    solsru = [];
    solsrv = [];
    solsrw = [];
    errdi = [];
    errdn = [];
    errds = [];
    errdr = [];
    
    
    for j = 1:nv
        errdi(:,j) = gv(j).d-gv(j).dm;
    end
    
    err.d(end+1,:) = std(errdi,1,2)';
    
    figure(fherr)
    plot(err.bl,'.-')
    tmp = axis;
    axis([tmp(1:2) 0 max(err.bl(end,:))*2])
    
    figure(fherrd)
    plot(err.d,'.-')
    legend('r23','r13','r12','e11','e22','e33','e23','e13','e12')
    legend('location','westoutside')
    
    if mod(i,10)==0
        if 0
            for j = 1:nv
                
                % 		ugoodu = [ugoodu gv(j).fac2m(1,:)];
                % 		ugoodv = [ugoodv gv(j).fac2m(2,:)];
                % 		ugoodw = [ugoodw gv(j).fac2m(3,:)];
                % 		ugoodu = [ugoodu gv(j).dd(1:3)'];
                % 		ugoodv = [ugoodv gv(j).dd(4:6)'];
                % 		ugoodw = [ugoodw gv(j).dd(7:9)'];
                erru = [erru gv(j).uc(1,:)-gv(j).ucm(1,:)];
                errv = [errv gv(j).uc(2,:)-gv(j).ucm(2,:)];
                errw = [errw gv(j).uc(3,:)-gv(j).ucm(3,:)];
                
                soldn = [soldn gv(j).dd(1:3)'./(gv(j).dm(1:3)'-gv(j).do(1:3)')];
                solds = [solds gv(j).dd(4:6)'./(gv(j).dm(4:6)'-gv(j).do(4:6)')];
                soldr = [soldr gv(j).dd(7:9)'./(gv(j).dm(7:9)'-gv(j).do(7:9)')];
                
                % 		errdn = [errdn gv(j).d(1:3)'-gv(j).dm(1:3)'];
                % 		errds = [errds gv(j).d(4:6)'-gv(j).dm(4:6)'];
                % 		errdr = [errdr gv(j).d(7:9)'-gv(j).dm(7:9)'];
                
                solsru = [solsru gv(j).solsr(1,:)];
                solsrv = [solsrv gv(j).solsr(2,:)];
                solsrw = [solsrw gv(j).solsr(3,:)];
                
                % 		solsfu = [solsfu gv(j).su2(1,:)./(gv(j).u(1,:)-gv(j).um(1,:))];
                % 		solsfv = [solsfv gv(j).su2(2,:)./(gv(j).u(2,:)-gv(j).um(2,:))];
                % 		solsfw = [solsfw gv(j).su2(3,:)./(gv(j).u(3,:)-gv(j).um(3,:))];
                
            end
            
            errdn = errdi(1:3,:);
            errds = errdi(4:6,:);
            errdr = errdi(7:9,:);
            
            bluvoldif = floor(bluvol)-floor(bluvolsol);
            bins = -10:10;
            figure(fhbluvoldif)
            hist(bluvoldif(:),bins);
            xlim([-10 10])
            
            figure(fherru)
            hist(erru,100)
            
            figure(fherrv)
            hist(errv,100)
            
            figure(fherrw)
            hist(errw,100)
            
            bins = -10:0.02:10;
            
            figure(fhsolsru)
            hist(solsru,bins);
            xlim([-5 5])
            
            figure(fhsolsrv)
            hist(solsrv,bins);
            xlim([-5 5])
            
            figure(fhsolsrw)
            hist(solsrw,bins);
            xlim([-5 5])
            
            figure(fhsoldn)
            hist(soldn,bins)
            xlim([-5 5])
            
            figure(fhsolds)
            hist(solds,bins)
            xlim([-5 5])
            
            figure(fhsoldr)
            hist(soldr,bins)
            xlim([-5 5])
            
            bins = -10:0.02:10;
            
            figure(fherrdn)
            hist(errdn(:),100)
            
            figure(fherrds)
            hist(errds(:),100)
            
            figure(fherrdr)
            hist(errdr(:),100)
        end
        
        dact = [gv(:).d];
        
        figure(fhistd1act)
        hist(dact(1,:),dbins)
        
        % Plot d correlations
        % 		figure(fdcorr)
        % 		alldm = [gv(:).dm]';
        % 		alld  = [gv(:).d]';
        % 		plot(alldm,alld,'.')
        % 		axis equal
        
        % Show difpots
        %gtFedShowDifSpot(1,bl,'act')
        
        drawnow();
        save(fedpars.filename,'gv','bl','err','bluvol','bluvolsol','blxint','fedpars');
    end
    
    assignin('base','gv',gv);
    assignin('base','bl',bl);
    assignin('base','bluvol',bluvol);
    assignin('base','err',err);
    %assignin('base','errd',errd);
    assignin('base','blxint',blxint);
    
end



% % d smotting by splines
%
%
% dvol1   = zeros(ngv);
% dvol2   = zeros(ngv);
% dvol3   = zeros(ngv);
% % uvol1   = zeros(ngvs+2);
% % uvol2   = zeros(ngvs+2);
% % uvol3   = zeros(ngvs+2);
%
% dvolval = zeros([ngv, 9]);
% % uvolval = zeros([ngvs+2, 3]);
%
%
% for j = 1:nv
% 	% flip coordinates 1 and 2 for meshgrid !!!
% 	dvol1(gv(j).ind(1),gv(j).ind(2),gv(j).ind(3)) = gv(j).cs(1);
% 	dvol2(gv(j).ind(1),gv(j).ind(2),gv(j).ind(3)) = gv(j).cs(2);
% 	dvol3(gv(j).ind(1),gv(j).ind(2),gv(j).ind(3)) = gv(j).cs(3);
% % 	uvol1(gv(j).ind(1)+1,gv(j).ind(2)+1,gv(j).ind(3)+1) = gv(j).cs1(2);
% % 	uvol2(gv(j).ind(1)+1,gv(j).ind(2)+1,gv(j).ind(3)+1) = gv(j).cs1(1);
% % 	uvol3(gv(j).ind(1)+1,gv(j).ind(2)+1,gv(j).ind(3)+1) = gv(j).cs1(3);
%
%   dvolval(gv(j).ind(1),gv(j).ind(2),gv(j).ind(3),:) = gv(j).d;
%
% end
%
% % Grid on which u values will be calculated (representing each voxel of grain
% % volume):
% [vox1,vox2,vox3] = ndgrid(1:grsize(1),1:grsize(2),1:grsize(3));
%
% dpl = spaps({1:ngv(1),1:ngv(2),1:ngv(3)},dvolval(:,:,:,9),0.0001);
%

beep

end % end of function


