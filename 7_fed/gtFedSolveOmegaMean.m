function [gv,bl]=gtFedSolveOmegaMean(gv,bl)


npl = length(bl);
nv  = length(gv);

% Absolut search limit of d components
dlimit = 0.005; 
dlim = ones(9,1)*dlimit;



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Start with measured mean omegas of the blobs

tic

% Mean omega values in each blob ("centre of mass in omega"):
for i=1:npl

	% Weighting by position:
	indices = zeros(bl(i).bbsize);
	for j=1:bl(i).bbsize(3)
		indices(:,:,j) = j;
	end

	% "centre of mass"
	wmeanbl{i} = sum(bl(i).intm.*indices,3);
	wmeanbl{i} = wmeanbl{i}./sum(bl(i).intm,3);

	wmeanbl{i}(isnan(wmeanbl{i})) = bl(i).u0bl(3);
	
end


% Loop through elements
for iv = 1:nv

	wAB = zeros(npl/2,1);
	U0w = zeros(npl/2,9);
	
	for ib = 1:2:npl

		uvA = gv(iv).u0bl([1,2],ib) + gv(iv).udc([1,2],ib);
		wA = interp2(wmeanbl{ib},uvA(1),uvA(2)) - gv(iv).u0bl(3,ib);

		uvB = gv(iv).u0bl([1,2],ib+1) + gv(iv).udc([1,2],ib+1);
		wB = interp2(wmeanbl{ib+1},uvB(1),uvB(2)) - gv(iv).u0bl(3,ib+1);
		
		if wA*wB > 0
			wAB((ib+1)/2) = (wA + wB)/2;
		else  % contradictory guess
			wAB((ib+1)/2) = 0;
		end
		
		% using only the row corresponding to w from U0
		U0w((ib+1)/2,:) = gv(i).U0(3,:,ib);
	
	end
	
	% Inverse of U0w:
	[Usvd,Ssvd,Vsvd] = svd(U0w,0);
	gv(iv).T0w = Vsvd*diag(1./diag(Ssvd))*Usvd';

	
	gv(iv).d0 = gv(iv).d;    % save old value of d

	% apply inverse matrix and w guess values
	gv(iv).dd = gv(iv).T0w*wAB;       % change of d
	newd      = gv(iv).d + gv(iv).dd; % new d value
	newd      = min(newd, dlim);      % apply max limit of newd
	gv(iv).d  = max(newd,-dlim);      %
	gv(iv).dd = gv(iv).d - gv(iv).d0; % final change of d
	
end


toc

disp('Spreading intensities ...')
[gv,bl] = gtFedSpreadInt(gv,bl);




