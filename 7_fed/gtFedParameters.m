function fedpars=gtFedParameters(nbl)

fedpars.grainID   = NaN;
fedpars.filename  = 'fedout.mat';    % filename for saving all FED data (gv, bl, err, fedpars...)
fedpars.real_data = true;             % if real data, then the "right" answer is not known    

fedpars.grainvol    = NaN;        % grain volume as logical (or continuous) 3D array
fedpars.grainoff    = NaN(1,3);   % offset of grain volume center from sample origin (rot. axis)
fedpars.grainsize   = NaN(1,3);   % grain volume size (size of grainvol)
fedpars.grainenv    = NaN(1,3);   % grain envelope size (may be > grainsize)
fedpars.gvsize      = NaN(1,3);   % size of one volume element
fedpars.ngv         = NaN;        % total number of volume elements
fedpars.ngvsub      = [1 1 1];    % ? - ask Peter
fedpars.auto_gvsize = false;   % auto calculate gvsize for sensible number of elements
fedpars.max_elements_grain = NaN; % max number of elements when calculating gvsize

fedpars.loadbl      = NaN;        % list of which blobs to load
fedpars.usebl       = NaN;         % list of which blobs to use in solver
fedpars.pairedblobs = 1;       % every odd-even blob is a Friedel pair

fedpars.strategy.s1.solver = 'Flow'; % which solver to use ('Flow' | 'LSQR')

fedpars.detpos  = NaN(1,3);     % experiment geometry - detector center
fedpars.detdiru = NaN(1,3);     % experiment geometry - direction of u
fedpars.detdirv = NaN(1,3);     % experiment geometry - direction of v
fedpars.rotdir  = NaN(1,3);     % experiment geometry - rotation axis direction
fedpars.detucen = NaN(1,3);     % experiment geometry - center of detector in pixels, u
fedpars.detvcen = NaN(1,3);     % experiment geometry - center of detector in pixels, v
fedpars.beamdir = NaN(1,3);     % experiment geometry - beam direction

fedpars.beamprofile = NaN;      % profile of the incident beam (?)

fedpars.detscaleu = NaN;        % (reconstruction voxelsize/detector pixelsize), u direction
fedpars.detscalev = NaN;        % (reconstruction voxelsize/detector pixelsize), u direction

fedpars.beamchroma      = '';          % 'mono' or 'poly'
fedpars.beamenergymin   = NaN;         % for polychromatic beam
fedpars.beamenergymax   = NaN;         % for polychromatic beam

fedpars.blext           = NaN(1,3);    % extension of bounding box when loading blobs
fedpars.wonly           = false;       % solve using omega variations only (true/false)

fedpars.dlim         = NaN(9,1);       % assumed maximum +/- def. comps. in solution
fedpars.dindex       = NaN(1,9);       % which deformation components to solve
fedpars.sufac        = NaN(9,1);       % speed factor to calculate deformation changes
fedpars.fbbsize      = 0.1;            % factor multiplying ulim to calculate the ROI size in flow solver
fedpars.fbbminsize   = [4 4 4];        % minimum ROI size in solver
fedpars.dstepmode    = '';             % 'discr' for discrete steps (size = dsteplim) in deformation in solver
fedpars.dsteplim     = NaN;            % maximum allowed step in deformation in solver
fedpars.dmeancorr    = false;          % dmean correction in solver (true/false)
fedpars.solver       = 'Flow';         % which solver to use?
fedpars.crude_acceleration = false;    % can speed up forward proj in matlab by treating only one voxel in 2x2x2, and then dilating
fedpars.T0strategy   = 'B';            % how to calculate T0 - 'A': use all U0 matrices, then set unused values ot zero / 'B' use only active U0 matrices
fedpars.dsmoothing   = true;           % smooth the calculated deformations
fedpars.niter        = NaN;            % number of iterations in solver




% set default values:

%%%%%%%%%%%%%%%%% REAL DATA %%%%%%%%%%%%%%%%%
fedpars.real_data = true;
%%%%%%%%%%%%%%%%%% GEOMETRY %%%%%%%%%%%%%%%%%
% sample voxel / detector pixel size ratio
fedpars.detscaleu=1;
fedpars.detscalev=1;
% monochromatic beam
fedpars.beamchroma='mono';
%%%%%%%%%%%%%%%%%% LOADING GRAIN %%%%%%%%%%%%%%%%%
% Size of a volume element (sampling)
fedpars.auto_gvsize = true;
fedpars.gvsize = [NaN NaN NaN]; % will be set in gtFedLoadRealGrain
fedpars.max_elements_grain = 10000; % from PR
fedpars.ngvsub = [1 1 1];
%%%%%%%%%%%%%%%%%% LOADING BLOBS %%%%%%%%%%%%%%%%%
fedpars.blext = [15 15 10];
%%%%%%%%%%%%%%%%%% SOLVER %%%%%%%%%%%%%%%%%
fedpars.solver = 'Flow';
fedpars.filename = 'fedout.mat';
fedpars.wonly=false;
% Maximum strain values:
fedpars.dlim    = 0.009*ones(9,1);      % assumed in solution
fedpars.sufac(:) = 10;
fedpars.T0strategy = 'B';               % how to calculate TO2 matrix - keep this constant normally
fedpars.nphases = 3;



fedpars.loadbl=1:nbl;
fedpars.niter=100;                   % 100 iterations
fedpars.crude_acceleration = false;
fedpars.usebl=1:nbl;
fedpars.dindex = [1 1 1 0 0 0 0 0 0];
fedpars.dsteplim = 0.0002*ones(9,1);              % check with Peter
fedpars.dstepmode = 'continuous';
fedpars.dmeancorr = false;
fedpars.dsmoothing=1;
fedpars.fbbsize = 0.1;
fedpars.fbbminsize   = [4 4 4];         % minimum ROI size in solver




%%%%%%%%%%%%%%%%%% STRATEGY %%%%%%%%%%%%%%%%%
for ii = 1 : fedpars.nphases
    ferpars.strategy.(sprint('s%d',ii)) = gtFedSolverFlowParameters(nbl);
end

%%%%%%%%%%%%%%%%%%% STEP 1 %%%%%%%%%%%%%%%%%
fedpars.strategy.s1.solver='Flow';
fedpars.strategy.s1.niter=100;                   % 100 iterations
fedpars.strategy.s1.dindex=[1 1 1 0 0 0 0 0 0];  % solve only for rotation components
fedpars.strategy.s1.usebl=1:min(14, nbl);        % use only the first 14 blobs initially
fedpars.strategy.s1.dsteplim = 0.0002*ones(9,1); % maximum deformation step
fedpars.strategy.s1.crude_acceleration=false;    % not needed for C code projector
fedpars.strategy.s1.T0strategy = 'B';
fedpars.strategy.s1.dsmoothing=1;                % smooth deformation results
fedpars.strategy.s1.gaugemult  = 0.7;          % size of gauge volume
fedpars.strategy.s1.gaugemax   = [10 10 10];          % size of gauge volume
fedpars.strategy.s1.gaugemin   = [4 4 4];          % size of gauge volume
%fedpars.strategy.s1.blob       = true ;        % ????
fedpars.strategy.s1.sufac      = ones(3,nbl/2);% flow speed factor of volume elements
fedpars.strategy.s1.sumax      = Inf(3,nbl/2); % maximum flow speed of volume elements
fedpars.strategy.s1.dstepmode  = 'cont';       % d component update mode continuous ('cont') or discrete ('discr')


fedpars.strategy.s1.dmeancor = false;            % dmean correction
fedpars.strategy.s1.fbbsize=0.4;                 % factor for calculating flow bounding box size
fedpars.strategy.s1.fbbminsize=[4 4 4];          % minimum flow bounding box size


%%%%%%%%%%%%%%%%%%% STEP 2 %%%%%%%%%%%%%%%%%
fedpars.strategy.s2.niter=200;
fedpars.strategy.s2.crude_acceleration=false;
fedpars.strategy.s2.usebl=1:nbl;
fedpars.strategy.s2.dindex=[1 1 1 0 0 0 0 0 0];
fedpars.strategy.s2.dsteplim = 0.0001*ones(9,1);
fedpars.strategy.s2.dstepmode = 'cont';
fedpars.strategy.s2.dmeancor = false;
fedpars.strategy.s2.dsmoothing=1;
fedpars.strategy.s2.fbbsize=0.3;
fedpars.strategy.s2.fbbminsize=[4 4 4];


%%%%%%%%%%%%%%%%%%% STEP 3 %%%%%%%%%%%%%%%%%
fedpars.strategy.s3.loadbl=1:nbl;
fedpars.strategy.s3.niter=100;
fedpars.strategy.s3.crude_acceleration=false;
fedpars.strategy.s3.usebl=1:nbl;
fedpars.strategy.s3.dindex=[1 1 1 0 0 0 0 0 0];
fedpars.strategy.s3.dsteplim = 0.00005*ones(9,1);
fedpars.strategy.s3.dstepmode = 'continuous';
fedpars.strategy.s3.dmeancor = false;
fedpars.strategy.s3.dsmoothing=1;                % not a good idea to turn off smoothing, it seems...
fedpars.strategy.s3.fbbsize=0.2;
fedpars.strategy.s3.fbbminsize=[4 4 4];


% STRATEGY for the iterations
%   Parameters for each stage are defined this way: 
%     fedpars.strategy.s1.solver = 'Flow'; fedpars.strategy.s1.dindex = ...
%     fedpars.strategy.s2.solver = 'LSQR'; fedpars.strategy.s2.dindex = ...
%     etc.

end
