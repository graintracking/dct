function com=gtFedVolumeCentroid(vol)

% total intensity
totint   = sum(vol(:));

cu = sum(sum(sum(vol,3),2).*(1:size(vol,1))')/totint;
cv = sum(sum(sum(vol,3),1).*(1:size(vol,2)))/totint;

sum12    = zeros(1,size(vol,3));
sum12(:) = sum(sum(vol,1),2);

cw = sum(sum12.*(1:size(vol,3)))/totint;

com = [cu; cv; cw];