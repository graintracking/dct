
disp(' ')
disp('VALIDITY CHECK of linear approximations in the strain calculations.')
disp(' ')

%beamchroma = 'poly'
beamchroma = 'mono'


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Assumed acquisition parameters

disp('APPLIED PARAMETERS')
disp(' ')

maxd = 0.01

disp('Beam direction in LAB reference:')
beamdir = [1; 0; 0]

disp('Rotation axis orientation:')
if strcmp(beamchroma,'poly')
	rotdir = [0; 0; 0]
else
	rotdir = [0; 0; -1]
end

disp('Angular increment per image in [deg]:')
omstep = 0.05

disp('Detector position [in detector pixels]:')
detrefpos = [3000; 0; 0]

disp('Detector orientation:')
detdiru = [0; 1; 0]
detdirv = [0; 0; -1]

disp('Origin on detector plane (rotation axis position):')
detrefuv = [1024.5; 1024.5]

disp('Grain center in SAMPLE coordinates:')
csam = [0; 0; 0]

disp('Initial Bragg angle:')
theta = 10

disp('Sin of initial Bragg angle:')
sinth = sind(theta)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% COMPUTE REAL CHANGE IN PLANE NORMALS AND ITS LINEAR APPROXIMATION

disp(' ')
disp('RANDOM INPUT')
disp(' ')

% Vectors
n1=1-rand(3,1)*2;
n2=1-rand(3,1)*2;

disp('Two random unit plane normals:')
n1=n1/norm(n1)
n2=n2/norm(n2)

% Create n-s from vector product
e12=cross(n1,n2);
e1=cross(n1,e12);
e2=cross(n2,e12);

e12=e12/norm(e12);
e1=e1/norm(e1);
e2=e2/norm(e2);

% Random full gradient tensor:
G=(ones(3)-rand(3)*2)*maxd;
if strcmp(beamchroma,'poly')
  G(1,1) = 0;
end

disp('Random strain tensor:')
E=(G+G')/2

disp('Random rotation tensor:')
R=(G-G')/2

% Vector of strain components:
Ecomps = [E(1,1); E(2,2); E(3,3); E(2,3); E(1,3); E(1,2)];

% Deformation components as a vector:
dcomps = [R(3,2); R(1,3); R(2,1); E(1,1); E(2,2); E(3,3); E(2,3); E(1,3); E(1,2)];
disp('Deformation components:')
dcomps

% e-s deformed due to G
e12n=G*e12+e12;
e1n=G*e1+e1;
e2n=G*e2+e2;

e12n=e12n/norm(e12n);
e1n=e1n/norm(e1n);
e2n=e2n/norm(e2n);

% new deformed n-s
n1n=cross(e12n,e1n);
n1n=n1n/norm(n1n);

n2n=cross(e12n,e2n);
n2n=n2n/norm(n2n);

disp(' ')
disp('ANALYSIS OF THE DOT PRODUCT n1*n2')
disp(' ')

disp('Change of unit plane normals [real vs. linear appr.]:')
dn1 = [n1n-n1, gtFedDerPlaneNormDeformation(n1)*dcomps]
dn2 = [n2n-n2, gtFedDerPlaneNormDeformation(n2)*dcomps]

disp('ERROR of linear approximation in change of unit plane normals:')
errdn1 = [n1n-n1 - gtFedDerPlaneNormDeformation(n1)*dcomps]
errdn2 = [n2n-n2 - gtFedDerPlaneNormDeformation(n2)*dcomps]

% disp('Change in dot product n1*n2 [real vs. linear appr.]:')
% disp([(n1n'*n2n)-n1'*n2, Aop(n1,n2)*Ecomps])
% 
% disp('ERROR of linear approximation in the dot product:')
% disp((n1n'*n2n)-n1'*n2 - Aop(n1,n2)*Ecomps)

disp('Elongations [real, lin. appr.]: ')
relo1 = [norm(G*n1+n1)-1 gtFedDerElongationDeformation(n1)*dcomps]
relo2 = [norm(G*n2+n2)-1 gtFedDerElongationDeformation(n2)*dcomps]


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Differential operators - omega

disp(' ')
disp('ANALYSIS OF CHANGES IN PLANE NORMAL n1 DUE TO STRAIN')
disp(' ')

disp('New sin(theta) due to deformation of n1:')
% deformed relative length along n1:
fvec = G*n1 + n1;

% deformed relative d-spacing:
dnew = dot(n1n,fvec);

% sin(theta) in deformed state:
sinthnew = sinth/dnew


% Input data for FED functions

% Detector
Qdet = gtFedDetectorProjectionTensor(detdiru,detdirv,1,1);
detnorm = cross(detdiru,detdirv);

% Rotation tensor constants
rotcomp = gtFedRotationMatrixComp(rotdir);

if strcmp(beamchroma,'poly')
	
	om1(1:4) = 0;
	n1lab = repmat(-n1*sign(n1'*beamdir),1,4);
	n1signed = n1lab ;
	
	om1n(1:4) = 0;
	n1nlab = repmat(-n1n*sign(n1n'*beamdir),1,4);
	n1nsigned = n1nlab;
	
	sinth = abs(n1'*beamdir);
	sinthnew = abs(n1n'*beamdir);
	
else
	
	% New omegas due to deformation:
	[om1,n1lab,n1signed] = gtFedPredictOmega(n1,sinth,beamdir,rotdir,rotcomp);
	[om1ntmp,n1nlabtmp,n1nsignedtmp] = gtFedPredictOmega(n1n,sinthnew,beamdir,rotdir,rotcomp);
	
	
	
	disp('Predicted initial omegas (all four reflections from plane normal n1) in [deg]:')
	if isempty(om1ntmp)
		disp('No diffraction from this plane. Run the macro again!')
		return
	else
		disp(om1)
	end
	
	% Match omegas
	for i = 1:4
		[minval,minloc] = min(abs(om1(i)-om1ntmp));
		matchom(i) = minloc;
		om1n(i) = om1ntmp(minloc);
		n1nlab(:,i) = n1nlabtmp(:,minloc);
		n1nsigned(:,i) = n1nsignedtmp(:,minloc);
	end

end


disp('Predicted omegas after deformation')
om1n

disp('Initial plane normal in diffraction position:')
n1lab
disp('Deformed plane normal in diffraction position:')
n1nlab

disp('Real change of omegas due to deformation in [deg]:')
rdom1 = [om1n(1)-om1(1), om1n(2)-om1(2), ...
         om1n(3)-om1(3), om1n(4)-om1(4)]


DerOmDef1 = gtFedDerOmegaDeformationFromBasePars(n1signed(:,1),om1(1),rotcomp,beamdir,sinth);
DerOmDef2 = gtFedDerOmegaDeformationFromBasePars(n1signed(:,2),om1(2),rotcomp,beamdir,sinth);
DerOmDef3 = gtFedDerOmegaDeformationFromBasePars(n1signed(:,3),om1(3),rotcomp,beamdir,sinth);
DerOmDef4 = gtFedDerOmegaDeformationFromBasePars(n1signed(:,4),om1(4),rotcomp,beamdir,sinth);

disp('Change of omegas predicted by linear appr. in [deg]:')			 
adom1 = [DerOmDef1*dcomps, DerOmDef2*dcomps, DerOmDef3*dcomps, DerOmDef4*dcomps]
			 

disp('ERROR in changes of omegas in [deg]:')
errdom1 = adom1-rdom1

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Differential operators - diffraction vector



disp(' ')
disp(' DIFFRACTION VECTORS')
disp(' ')
disp('Initial diffraction vectors:')
d1 = [gtFedPredictDiffVec(n1lab(:,1),sinth,beamdir), ...
	    gtFedPredictDiffVec(n1lab(:,2),sinth,beamdir), ...
			gtFedPredictDiffVec(n1lab(:,3),sinth,beamdir), ...
			gtFedPredictDiffVec(n1lab(:,4),sinth,beamdir)]		

%d1 = d1./repmat(sqrt(sum(d1.*d1,1)),3,1)

disp('Real diffraction vectors after deformation:')
d1n = [gtFedPredictDiffVec(n1nlab(:,1),sinthnew,beamdir), ...
       gtFedPredictDiffVec(n1nlab(:,2),sinthnew,beamdir), ...
		   gtFedPredictDiffVec(n1nlab(:,3),sinthnew,beamdir), ...
		   gtFedPredictDiffVec(n1nlab(:,4),sinthnew,beamdir)]
		 
%d1n = d1n./repmat(sqrt(sum(d1n.*d1n,1)),3,1)

		 
disp('Real change of diffraction vectors:')
rdd = d1n-d1

disp('Change of diffraction vectors from linear appr.:')
add = [gtFedDerDiffVectorDeformationFromBasePars(n1signed(:,1),om1(1),rotcomp,beamdir,sinth,beamchroma)*dcomps, ...
       gtFedDerDiffVectorDeformationFromBasePars(n1signed(:,2),om1(2),rotcomp,beamdir,sinth,beamchroma)*dcomps, ...
       gtFedDerDiffVectorDeformationFromBasePars(n1signed(:,3),om1(3),rotcomp,beamdir,sinth,beamchroma)*dcomps, ...
       gtFedDerDiffVectorDeformationFromBasePars(n1signed(:,4),om1(4),rotcomp,beamdir,sinth,beamchroma)*dcomps]

	 
disp('ERROR in change of diffraction vectors:')
errd = rdd-add

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Differential operators - spot location on detector

disp(' ')
disp(' SPOT LOCATIONS')
disp(' ')

Rottens11 = gtFedRotationTensor(om1(1),rotcomp);
Rottens12 = gtFedRotationTensor(om1(2),rotcomp);
Rottens13 = gtFedRotationTensor(om1(3),rotcomp);
Rottens14 = gtFedRotationTensor(om1(4),rotcomp);

Rottens1n1 = gtFedRotationTensor(om1n(1),rotcomp);
Rottens1n2 = gtFedRotationTensor(om1n(2),rotcomp);
Rottens1n3 = gtFedRotationTensor(om1n(3),rotcomp);
Rottens1n4 = gtFedRotationTensor(om1n(4),rotcomp);

disp(' ')
disp('Real spot locations on detector [u;v;w] in [pix;pix;im]:')
u1 = [gtFedPredictUVW(Rottens11,d1(:,1),csam,detrefpos,detnorm,Qdet,detrefuv,om1(1),omstep), ...
	    gtFedPredictUVW(Rottens12,d1(:,2),csam,detrefpos,detnorm,Qdet,detrefuv,om1(2),omstep), ...
    	gtFedPredictUVW(Rottens13,d1(:,3),csam,detrefpos,detnorm,Qdet,detrefuv,om1(3),omstep), ...
	    gtFedPredictUVW(Rottens14,d1(:,4),csam,detrefpos,detnorm,Qdet,detrefuv,om1(4),omstep)]

disp('Real spot locations on detector after deformation [u;v;w] in [pix;pix;im]:')
u1n = [gtFedPredictUVW(Rottens1n1,d1n(:,1),csam,detrefpos,detnorm,Qdet,detrefuv,om1n(1),omstep), ...
	     gtFedPredictUVW(Rottens1n2,d1n(:,2),csam,detrefpos,detnorm,Qdet,detrefuv,om1n(2),omstep), ...
    	 gtFedPredictUVW(Rottens1n3,d1n(:,3),csam,detrefpos,detnorm,Qdet,detrefuv,om1n(3),omstep), ...
	     gtFedPredictUVW(Rottens1n4,d1n(:,4),csam,detrefpos,detnorm,Qdet,detrefuv,om1n(4),omstep)]

disp(' ')
disp('Real change of spot locations on detector [u;v;image] in [pix;pix;im]:')
rdu1 = u1n-u1

disp('Change of spot locations on detector from linear appr. [u;v;w] in [pix;pix;im]:')
		
dfac1 = [gtFedPredictDiffFac(Rottens11,d1(:,1),csam,detrefpos,detnorm), ...
	       gtFedPredictDiffFac(Rottens12,d1(:,2),csam,detrefpos,detnorm), ...
				 gtFedPredictDiffFac(Rottens13,d1(:,3),csam,detrefpos,detnorm), ...
				 gtFedPredictDiffFac(Rottens14,d1(:,4),csam,detrefpos,detnorm)];

if isempty(dfac1)
	disp('Projection won`t hit the detector.')
	return
end
			 
adu1 = [gtFedDerUmatrix(n1signed(:,1),om1(1),Rottens11,rotcomp,csam,sinth,d1(:,1),dfac1(:,1),detrefpos,detnorm,Qdet,beamdir,omstep,beamchroma)*dcomps, ...
	      gtFedDerUmatrix(n1signed(:,2),om1(2),Rottens12,rotcomp,csam,sinth,d1(:,2),dfac1(:,2),detrefpos,detnorm,Qdet,beamdir,omstep,beamchroma)*dcomps, ...
				gtFedDerUmatrix(n1signed(:,3),om1(3),Rottens13,rotcomp,csam,sinth,d1(:,3),dfac1(:,3),detrefpos,detnorm,Qdet,beamdir,omstep,beamchroma)*dcomps, ...
				gtFedDerUmatrix(n1signed(:,4),om1(4),Rottens14,rotcomp,csam,sinth,d1(:,4),dfac1(:,4),detrefpos,detnorm,Qdet,beamdir,omstep,beamchroma)*dcomps]

disp(' ')
disp('ERROR of linear appr.in change of spot locations [u,v,om] in [pix;pix;im]:')
rdu1-adu1

disp('RELATIVE ERROR of linear appr. in change of spot locations [u,v,om] in %:')
(rdu1-adu1)./rdu1*100


disp(' ')
disp('Deformation tensor:')
disp(G)






























