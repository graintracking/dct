function grvol = gtFedGenerateGrainVolume(grsize)

% Grain volume as double:
grvol = ones(grsize);

end