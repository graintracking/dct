function bl = gtFedMakeOmegaImages(bl, calculate_flag)
% bl = gtFedMakeOmegaImages(bl, [calculate_flag])
%
%     calculate_flag = 1 display, don't calculate 
%                      2 display and calculate
%
% tool for viewing blobs from FED
% add images to the bl structure

if ~exist('calculate_flag','var') || isempty(calculate_flag)
    calculate_flag=2;
end

sorting=1;
if sorting
for i=1:length(bl)
    om(i)=bl(i).om0;
end
[om, ndx]=sort(om);
else
    ndx=1:(length(bl));
end

% omega centre of mass images
if calculate_flag == 2
    for j=1:length(bl)
        i=ndx(j);
        
        imm=nan(size(bl(i).intm, 1), size(bl(i).intm, 2));
        imfed=nan(size(imm));
        om=1:size(bl(i).intm, 3);
       
        for x=1:size(bl(i).int0, 1)
            for y=1:size(bl(i).int0, 2)
                
                colm=squeeze(bl(i).intm(x,y,:));
                colfed=squeeze(bl(i).int(x,y,:));
                
                if any(colm)
                    colm(find(colm<0))=0;
                    imm(x,y)=om*colm/sum(colm);
                end
                if any(colfed)
                    imfed(x,y)=om*colfed/sum(colfed);
                end
                
            end
        end
        
%         parameters=[];
%         % mask edges of the measured image, to reduce it to approx the size of
%         % the thresholded spot, rather than the dilated size used in the FED
%         % not appropriation for alumina where blobs come from DB
%         %load('parameters.mat');
%         if strcmp(parameters.acq.name, 'alumina_strain2_dct_')
%             disp('blobs from DB, so no masking')
%         else
%             disp('applying (hard coded) mask to measured blob image')
%             mask=imm>0;
%             mask=imerode(mask, ones(10, 10));
%             imm=imm.*mask;
%         end
%         clear parameters
        
        offset_m=min(imm(find(imm))); % smallest non-zero value
        offset_fed=min(imfed(find(imfed)));
        offset=min(offset_m, offset_fed);
        if isempty(offset)
            offset=0;
        end
        
        imm(find(imm))=imm(find(imm))-offset;
        imfed(find(imfed))=imfed(find(imfed))-offset;
        
        bl(i).imm_omega=imm;
        bl(i).imfed_omega=imfed;
    end % end for
end

if calculate_flag
    grid=ceil(sqrt(length(bl)));
    fh=figure;
    fh595=figure;
    
    disp('opening a text file called blob_details.txt')
    fid=fopen('blob_details.txt', 'w');
    fprintf(fid, 'sub-image pl(1), pl(2), pl(3), theta, eta, omega, fullclims1 fullclims2 595clims1 595clims2 \n');
    
    for j=1:length(bl)
        i=ndx(j);
        
        %imshow([bl(i).imm_omega, bl(i).imfed_omega bl(i).imm_omega-bl(i).imfed_omega], [])
        if 0
            subplot(grid,grid,j)
            line=ones(size(bl(i).imm_omega, 1), 10)*max(bl(i).imm_omega(:));
            imshow([bl(i).imm_omega, line, bl(i).imfed_omega], [])
        else
            disp('set mean image values equal..., tweak colormaps')
            a=bl(i).imm_omega;
            b=bl(i).imfed_omega;
            
            % % shouldn't have nans here
            % a(find(isnan(a)))=0;
            % b(find(isnan(b)))=0;
            %
            %  a(find(~a))=NaN;
            %  b(find(~b))=NaN;
            
            a=a-mean(a(find(~isnan(a))))+1;
            b=b-mean(b(find(~isnan(b))))+1;
            
            line=ones(size(bl(i).imm_omega, 1), 10)*max([a(:); b(:)]);
            %     line=ones(10, size(bl(i).imm_omega, 2))*max([a(:); b(:)]);
            im=[a line b];
            im595=[a line b];
            
            %     im=[a; line; b];
            
            tmp=[a b];
            tmp=sort(tmp(:));
            tmp(find(isnan(tmp)))=[];
            l=length(tmp);
            
            
            % 5-95 percentile colorlimits
            clims(1)=tmp(ceil(0.05*l));
            clims(2)=tmp(floor(0.95*l));
            
            % full colorscale
            fullclims(1)=tmp(1);
            fullclims(2)=tmp(end);
            
            % saturate these pixels manually, and then use full range colormap
            % in image - better for the backgroud I think
            im595(find(im>clims(2)))=clims(2);
            im595(find(im<clims(1)))=clims(1);
            
            
            clims(1)=clims(1)-(clims(2)-clims(1))/250;
            fullclims(1)=fullclims(1)-(fullclims(2)-fullclims(1))/250;
            
            %         clims(1)=min(im(:))
            %         clims(2)=max(im(:))
            %         range=clims(2)-clims(1);
            %         clims=clims+0.1*[range -range];
            
            
            % set background to show as black
            % im(find(isnan(im)))=min(im(:))-0.1*range
            figure(fh)
            subplot(grid,grid,j)
            imshow(im, fullclims)
                    title(sprintf('om: %0.1f et: %01.f', bl(i).om0, bl(i).eta0))

            figure(fh595)
            subplot(grid,grid,j)
            imshow(im595, clims)
                    title(sprintf('om: %0.1f et: %01.f', bl(i).om0, bl(i).eta0))

            
            %title(sprintf('sub-im %d: scale %0.1f : %0.1f', i, clims))
        end
        
        fprintf(fid, '%d %0.3f %0.3f %0.3f %0.1f  %0.1f  %0.1f %0.1f %0.1f %0.1f %0.1f  \n', i, bl(i).pl0, bl(i).th0, bl(i).eta0, bl(i).om0, fullclims, clims);
        if 0 % for headings
            title(sprintf('sub-im %d', i))
        end
        
    end
    
    figure(fh)
    colormap([1 1 1; jet])
    figure(fh595)
    colormap([1 1 1; jet])
    
    disp('saving the figure as blob_omega_images.png, and saving text file')
    saveas(fh, 'blob_omega_images_full.png', 'png');
    saveas(fh595, 'blob_omega_images_595.png', 'png');
    
    fclose(fid);
end % end calculate_flag==true


end % end of function




