% function [y,f]=fwhm(cc,c)
% looks for the maximum around the position given by c
% y corresponds to position of  maximum
% f corresponds to fwhm (no background)
% m corresponds to the maximum (parabolic fit)
%
  
function [y,f,m]=fwhm(cc,varargin)

switch nargin
case 1
	[m,c]=max(cc);
case 2
	c=varargin{1};
end


w=3;
wb=floor((w-1)/2);
we=ceil((w-1)/2);
xt=quadfunc((1:w)');
xt2=xt'*xt;

a=xt2\(xt'*cc(c-wb:c+we));
y=c+(-a(2)/2/a(3))-wb-1;
m=(a(1)-a(2)^2/4/a(3))/2;

% looking for left crossing of half of maximum
index=floor(y);
while cc(index)>m
	index=index-1;
end
yl=index+(m-cc(index))/(cc(index+1)-cc(index));

% looking for right crossing of half of maximum
index=ceil(y);
while cc(index)>m
	index=index+1;
end
yr=index-(m-cc(index))/(cc(index-1)-cc(index));	

f=yr-yl;	
m=2*m;

