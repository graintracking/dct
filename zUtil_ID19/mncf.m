function mt = mncf(opt, varargin)
% function mt = mncf(opt, varargin)
%
% Input:
%   opt : camera-setup
%       1: july 97, 10um set-up
%       2: april 98, 1.93um pixel set-up: optique 10x + 25um YAG
%       3: august 98, 0.932um pixel set-up: optique 10x + eyepiece + 25um YAG
%       4: september 97, 0.7 um pixel set-up: 20* YAG 5um
%       5: november 98, 0.5 um pixel set-up: optique 20x + eyepiece + LAG 3um
%       6: november 99, 0.394 um pixe set-up: optique 20x + eyepiece + LAG 6mu
%       7: november 2001, 1 micron setup with 0.69um pixelsize (2048 ccd): 10x optics + eyepiece + 25um YAG
%       8: for testing
%       9: for fits
% Optional Input:
%   m : number of row samples
%   n : number of column samples
%   oversampling : samplerate is fixed by oversamp, sigmas expressed in pixels,
%       changing n changes the width of the window and sampling rate
%       in frequency space
%   dimmensions: 1 | {2} create 1-dim mtf plot or 2-dim mtf map

    conf = struct( ...
        'size', 31, ...
        'oversampling', 1, ...
        'dimensions', 2, ...
        'p', []);
    conf = parse_pv_pairs(conf, varargin);

    switch (opt)
        case 1
            % july 97, 10 mu set-up
            mi = 0.01;
            ma = 1;
            p = [0.5884, 0.9093, 0.3570, 2.8537, 0.0352, 49.3503, 748.8534];
        case 2
            % april 98, 1.93 um pixel set-up: optique 10x + 25um YAG
            mi = 0.0117;
            ma = 0.9862;
            p = [0.4327, 0.9147, 0.2296, 7.9874, 0.1683, 54.6962, 197.9522];
        case 3
            % august 98, 0.932 mu pixel set-up: optique 10x + eyepiece 2x + 25um YAG
            %mi = 0.0149;
            %ma = 0.996;
            %p = [0.4723 1.3329 0.2154 5.8018 0.1569 56.1836 268.3804];

            %best 100 columns:
            %mi = 0.0151;
            %ma = 0.994;
            %p = [0.4530 1.2271 0.2279 5.2286 0.1550 52.5938 255.9305];

            %measurement in february 99 - optimised uptilt of GaAs
            mi = 0.013;
            ma = 0.984;
            p = [0.5985, 0.9903, 0.1622, 8.4452, 0.1423, 41.2661, 375.8298];
        case 4
            % september 97 0.7 um pixel set-up: optique 20x + eyepiece 1.6x + YAG 5um
            mi = 0.0214;
            ma = 0.9841;
            p = [0.3396, 1.2313, 0.2130, 10.4607, 0.1801, 51.0301, 240.2380];
        case 5
            % november 98 0.5 um pixel set-up: optique 20x + eyepiece 2x + LAG 3um
            mi = 0.022;
            ma = 1.001;
            p = [0.4182, 1.6436, 0.2293, 12.4082, 0.1922, 288.6455, 67.6439];
        case 6
            % november 99 0.394 um pixel set-up: optique 20x + eyepiece 2x + LAG 6um
            mi = 0.005;
            ma = 0.996;
            p = [0.6023, 1.6800, 0.2545, 5.0455, 0.1051, 29.9829, 235.2297];
        case 7
            % november 2001, 1 micron setup with 0.69um pixelsize (2048 ccd): 10x optic
            % determined with jofit and a defocus series
            mi = 0;
            ma = 1;
            p = [0.3978, 1.0493, 0.4608, 3.4978, 0.0674, 17.8457, 679.3300];
        case 8
            mi=0.035;
            ma=0.9875;
            p = [0.7288    4.3464    0.1172   50.8488    0.4826  461.0864  493.5530];
        case 9
            % for fits
            mi = 0;
            ma = 1;
            p = conf.p;
        case 10
            mi = 0;
            ma = 1;
            p = [0.5305, 1.5617, 0.4765, 7.4535, -0.0676, 15.7088, 683.5288];
    end

    % a's are the widths in erfc  =  sqrt(2)*sigma
    a1 = p(2) * conf.oversampling;
    a2 = p(4) * conf.oversampling;
    a3 = p(6) * conf.oversampling;
    a4 = p(7) * conf.oversampling;

    % b's are weigths
    b1 = p(1);
    b2 = p(3);
    b3 = p(5);
    b4 = ma - mi - p(1) - p(3) - p(5);

    n = conf.size;
    m = conf.size;

    switch (conf.dimensions)
        case 1
            f = (0:1/n:0.5)';
            mt = b1 * exp(-(pi * a1 * f) .^ 2) + b2 * exp(-(pi * a2 * f) .^ 2) + b3 * exp(-(pi * a3 * f) .^2 ) + b4 * exp(-(pi * a4 * f) .^ 2);
            mt = [mt; flipud(mt(2:n/2))];
            mt(1) = 1;
        case 2
            [x, y] = meshgrid(0:1/n:0.5, 0:1/m:0.5);
            f2 = x .^ 2 + y .^ 2;
            mt = b1 * exp(-(pi * a1) ^ 2 * f2) + b2 * exp(-(pi * a2) ^ 2 * f2) + b3 * exp(-(pi * a3) ^ 2 * f2) + b4 * exp(-(pi * a4) ^ 2 * f2);
            mt = [ ...
                mt, fliplr(mt(:, 2:floor(n/2))); ...
                flipud(mt(2:floor(m/2), :)), rot90(mt(2:floor(m/2), 2:floor(n/2)),2)];
            mt(1, 1) = 1;
        otherwise
            error([mfilename ':wrong_argument'], 'Dimemsions must be 1 or 2');
    end
end


