% function x=quadfunc(arg)
% used by correlate1, ...
% for implementing parabolic fit
% returns x=[ones(length(arg),1) arg arg.^2];
% origin: peter

function x=quadfunc(arg)

x=[ones(length(arg),1) arg arg.^2];
