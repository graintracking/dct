% function t=translate(r,c,n,m)
% creates matrix for interpolation using Fourier-transformation
% r: translation in the direction of the rows, positive is down
% c: translation in the direction of the columns, positive is to the right
% n: number of rows in the resulting matrix
% m: number of columns in the resulting matrix
% if an image im(x1,x2) must be translated of r and c in direction x1 and x2
% one can use:
% fft2( im(x1-r,x2-c) ) = translate(r,c,n,m) .* fft2( im(x1,x2) )
% or
% im(x1-r,x2-c) = real( ifft2( translate(r,c,n,m) .* fft2( im(x1,x2) ) ) 
% real is to avoid a small imaginary part in the result (only for real images)
% origin: peter

function t=translate(r,c,n,m)


f=r*2*pi/n*(0:floor(n/2))';
f=cos(f)-i*sin(f);
%f=exp(-i*f);
f=repmat([f;flipud(conj(f(2:ceil(n/2))))],1,m);

g=c*2*pi/m*(0:floor(m/2));
g=cos(g)-i*sin(g);
%g=exp(-i*g);
g=repmat([g fliplr(conj(g(2:ceil(m/2))))],n,1);

t=f.*g;
