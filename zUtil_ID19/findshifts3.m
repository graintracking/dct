function findshifts2(varargin);
% function shift=findshifts(im1,im2)
% allows to to determine visually the relative shift between two images
% KEYS:
% arrows (cursor keys) - one pixel shift
% shift-arrows - ten pixel shift
% 1 (2)  - increase (decrease) image contrast
% 3 (4)  - increase (decrease) image brightness
%
% press    t       to toggle between images
% press    s       to return to to image subtraction mode
% press    z       to select graphically a region of interest (4 times oversampling)
% press    r       to return to full image')
% press  enter     to accept current value')
% 
% OTHER STARTUP OPTIONS
% findshifts2(im1,im1,parameter,value)
% where parameter and value pairs can be:
%   'roix',[minimum maximum]  - set the horizontal region of interest
%   'roiy',[minimum maximum]  - vertical ROI
%   'clims',[minimum maximum] - preset the colour limits
%   'precorrelate','yes'      - attempt to correlate the images before
%                               interaction

% origin: Wolfgang  12/05
  warning('off','Images:initSize:adjustingMag');
  %% startup
  app=[];
  app.view.h=figure; % create a new figure
  app.mode='subtract';
  app.togglestate=true;
  app.vshift=0;
  app.hshift=0;
  app.voffset=0;
  app.hoffset=0;
  app.rot=0;
  app.zoom=1;
  app.view.h=gcf;
  app.clims=[-0.1 0.1];
  app.climt=[0 1];
  app.clima=[0 2];
  app.im0=varargin{1};
 
  app.im1=varargin{2};
  app.im1nr=app.im1;
  app.roix=[1 size(app.im0,2)];
  app.roiy=[1 size(app.im0,1)];
  app.precorrelate='off';
  app.block='off';
  app.quit=0;
  app.firsttime=true;
  if nargin>2
    if iscell(varargin{3})
      app=parse_pv_pairs(app,varargin{3});
    else
      app=parse_pv_pairs(app,{varargin{3:end}});
    end
  end
  % precrop the image 
  app.im0=app.im0(app.roiy(1):app.roiy(2),app.roix(1):app.roix(2));
  app.im1=app.im1(app.roiy(1):app.roiy(2),app.roix(1):app.roix(2));
  
  % then pre-correlate if requested
  if strcmpi(app.precorrelate,'yes') || strcmpi(app.precorrelate,'on')
    fprintf('Pre-correlating images\n')

    tmp=correlate(app.im0,app.im1);
    app.voffset = tmp(1);app.hoffset=tmp(2);
  end
  
  set(app.view.h,'KeyPressFcn',@sfKeyPress)
  %  help(mfilename)
  iptsetpref('imtoolinitialmagnification','fit')
  sfUpdateFigure;
  if strcmp(app.block,'on')
    % if user wants nothing to return until findshift is finished
    while 1
      drawnow
      if app.quit==1
        break
      end
    end
  end

function sfKeyPress(varargin)
  c=varargin{2};
  switch c.Key
    case 'uparrow'    
      if strcmp(c.Modifier,'shift')
        app.vshift=app.vshift+10/app.zoom;
      else
        app.vshift=app.vshift+1/app.zoom;
      end
      sfUpdateFigure;

    case 'downarrow'
      if strcmp(c.Modifier,'shift')
        app.vshift=app.vshift-10/app.zoom;
      else
        app.vshift=app.vshift-1/app.zoom;
      end
      sfUpdateFigure;
    
    case 'rightarrow'
      if ~isempty(c.Modifier)
	  if strcmp(c.Modifier,'shift')
        app.hshift=app.hshift+10/app.zoom;
	  elseif strcmp(c.Modifier,'control')
		app.rot=app.rot+0.2;
		app.im1=imrotate(app.im1nr,app.rot,'bicubic','crop');
		
	  elseif strcmp(c.Modifier,{'shift' 'control'})
		app.rot=app.rot+0.02;
		app.im1=imrotate(app.im1nr,app.rot,'bicubic','crop');
	    
	  end	
      else
        app.hshift=app.hshift+1/app.zoom;
      end
      sfUpdateFigure;

    case 'leftarrow'
      if ~isempty(c.Modifier)
	  if strcmp(c.Modifier,'shift')
        app.hshift=app.hshift-10/app.zoom;
	  elseif strcmp(c.Modifier,'control')
		app.rot=app.rot-0.2;
		app.im1=imrotate(app.im1nr,app.rot,'bicubic','crop');
		
	  elseif strcmp(c.Modifier,{'shift' 'control'})
		app.rot=app.rot-0.02;
		app.im1=imrotate(app.im1nr,app.rot,'bicubic','crop');
		
	  end
	  else
        app.hshift=app.hshift-1/app.zoom;
      end
      sfUpdateFigure;
    
    case 'return'
      %exit interactive mode and return current shift
      set(app.view.h,'KeyPressFcn','');
      assignin('base','shift',[app.vshift+app.voffset app.hshift+app.hoffset app.rot]);
      fprintf('Shift is: [%.2fv %.2fh]\n',app.vshift+app.voffset,app.hshift+app.hoffset);
      app.quit=1;
      return;

    case 'a'
      disp('changing to adding mode');
      app.mode='add';
      sfUpdateFigure;

    case '1'
      disp('Increasing contrast')
      switch app.mode
        case 'subtract'
          app.clims=app.clims*0.9;
          disp(sprintf('clim is %f %f',app.clims(1),app.clims(2)));
        case 'toggle'
          app.climt=app.climt*0.95;
          disp(sprintf('clim is %f %f',app.climt(1),app.climt(2)));
        case 'add'
          app.clima=app.clima*0.9;
          disp(sprintf('clim is %f %f',app.clima(1),app.clima(2)));
      end
      sfUpdateFigure;
      
    case '2'
      disp('Decreasing contrast')
      switch app.mode
        case 'subtract'
          app.clims=app.clims*1.1;
          disp(sprintf('clim is %f %f',app.clims(1),app.clims(2)));
        case 'toggle'
          app.climt=app.climt*1.05;
          disp(sprintf('clim is %f %f',app.climt(1),app.climt(2)));
        case 'add'
          app.clima=app.clima*1.1;
          disp(sprintf('clim is %f %f',app.clima(1),app.clima(2)));
      end
      sfUpdateFigure;
    case '3'
      disp('Colormap brighter')
      switch app.mode
        case 'subtract'
          app.clims=app.clims-max(app.clims)*0.1;
          disp(sprintf('clim is %f %f',app.clims(1),app.clims(2)));
        case 'toggle'
          app.climt=app.climt-mean(app.climt)*0.05;
          disp(sprintf('clim is %f %f',app.climt(1),app.climt(2)));
        case 'add'
          app.clima=app.clima-mean(app.clima)*0.1;
          disp(sprintf('clim is %f %f',app.clima(1),app.clima(2)));
      end
      sfUpdateFigure;
    case '4'
      disp('Colormap darker')
      switch app.mode
        case 'subtract'
          app.clims=app.clims+max(app.clims)*0.1;
          disp(sprintf('clim is %f %f',app.clims(1),app.clims(2)));
        case 'toggle'
          app.climt=app.climt+mean(app.climt)*0.05;
          disp(sprintf('clim is %f %f',app.climt(1),app.climt(2)));
        case 'add'
          app.clima=app.clima+mean(app.clima)*0.1;
          disp(sprintf('clim is %f %f',app.clima(1),app.clima(2)));
      end
      sfUpdateFigure;
    case 's'
      disp('changing to subtraction mode');
      app.mode='subtract';
      app.clim=app.clims;
      sfUpdateFigure;
    case 'z'
      disp('Select region of interest');
      title('Select region of interest');
      [toto,r]=imcrop;
      disp('Zooming...')
      title('Zooming...')
      r=round(r);
      sfZoom(r)
      sfUpdateFigure;
    case 'r'
      disp('reset full image');
      app.zoom=1;
      app.im0=app.orig0;
      app.im1=app.orig1;
      app.vshift=round(app.vshift+app.voffset);
      app.hshift=round(app.hshift+app.hoffset);
      app.voffset=0;
      app.hoffset=0;
      sfUpdateFigure;
    case 't'
      app.mode='toggle';
      app.togglestate= not(app.togglestate);
      app.clim=app.climt;
      sfUpdateFigure;
    case 'h'
      help(mfilename)
  end

end



function sfZoom(roi)
  
  app.orig0=app.im0;
  app.orig1=app.im1;
  app.zoom=4;
  app.im0=imresize(imcrop(app.im0,roi),4,'bicubic');
  app.im1=imresize(imcrop(app.im1,[roi(1)-app.hshift,roi(2)-app.vshift,roi(3),roi(4)]),4,'bicubic');
  app.voffset=app.vshift;
  app.hoffset=app.hshift;
  app.vshift=0;
  app.hshift=0;
end

function sfUpdateFigure
  colormap gray;
  shiftVector = round([app.vshift app.hshift] * app.zoom);
  im_tmp = circshift(app.im1, shiftVector);
  switch app.mode
    case 'add'
      imshow(app.im0+im_tmp,app.clima);
      title(sprintf('vertical shift: %.2f\t horizontal shift: %.2f',app.vshift+app.voffset,app.hshift+app.hoffset));
    case 'subtract'
      imagesc(app.im0-im_tmp,app.clims);
      if app.firsttime
        app.h_im=imagesc(app.im0-im_tmp,app.clims);
        %app.firsttime=false;
      else
        set(app.h_im,'cdata',app.im0-im_tmp)
        set(get(app.h_im,'parent'),'clim',app.clims);
      end
%      keyboard
%      axis image
      title(sprintf('vertical shift: %.2f\t horizontal shift: %.2f',app.vshift+app.voffset,app.hshift+app.hoffset));
    case 'toggle'
      if app.togglestate == true
        imagesc(app.im0,app.climt);
        axis image
        title('first image (fixed position)');
      else
        imagesc(im_tmp,app.climt);
        axis image
        title(sprintf('second image: vertical shift: %.2f\t horizontal shift: %.2f, rotation: %.2f',app.vshift+app.voffset,app.hshift+app.hoffset,app.rot));
      end
  end
  drawnow
end



end

