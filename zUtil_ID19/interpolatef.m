% function r=interpolate1f(p,r,c)
% interpolates each column of p according to the translation-fault given by fault (column vector)
% a positive fault corresponds to translating down the column
% method for interpolation is Fourier-transformation
% supposes periodic signals for the edges

function r=interpolatef(p,r,c)

[n m]=size(p);

r=real(ifft2(fft2(p).*translate(r,c,n,m)));
