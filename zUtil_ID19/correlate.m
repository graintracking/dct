% function c=correlate(z1,z2,varargin)
% 
% looks for the maximum correlation between z1 and z2
% 
% c=correlate(z1,z2,flag)
% flag should be zero if z1 and z2 are in real (default)       space
%                one                      reciprocal
% 
% c=correlate(z1,z2,flag,size_part)
% limits de maximum displacement to a given value (default: no limit)
%                               
% uses fourier transformation to calculate the cross-correlation between z1 and z2
% searches the maximum trough interpolation between the pixels
% max is max of parabol through 9 points around the abs. max
% result is c=[row column]
% z2 should go column columns to the right (or z1 column columns to the left)
% z2 should go row rows down (or z1 row rows up)
% to compensate drift: interpolate(z2,row,column)
% 
% see also INTERPOLATE, TRANSLATE
% 
% origin: peter

function c=correlate(z1,z2,varargin)

switch nargin
case 2
	flag=0;
	part=0;
case 3
	flag=varargin{1};
	part=0;
case 5
	flag=varargin{1};
	if isempty(flag)
		flag=0;
	end
	part=1;
	part_pos = varargin{2};
	part_size = varargin{3};
end


[n m]=size(z1);

if flag
	% matrices are fft'ed
	cc=abs(ifft2(z1.*conj(z2)));
else
	% matrices are not fft'ed
	cc=abs(ifft2(fft2(z1).*conj(fft2(z2))));
end %if

if part
	part_pos=round(part_pos);
	if (length(part_size)==1)
		part_size(2) = part_size(1);
	end
	w=zeros(size(z1));
	
% 	w([n-part_size:n 1:1+part_size],[m-part_size:m 1:1+part_size])=1;
	w(mod(-part_size(1)+part_pos(1):part_size(1)+part_pos(1),n)+1,mod(-part_size(2)+part_pos(2):part_size(2)+part_pos(2),m)+1)=1;
	cc=cc.*w-1e20.*(1-w);
end


[a,b]=max(cc);
[d,c(2)]=max(a);
c(1)=b(c(2));

c=c+min2par(cc([mod(c(1)-2,n)+1 c(1) mod(c(1),n)+1],[mod(c(2)-2,m)+1 c(2) mod(c(2),m)+1]));
c=rem(c+ceil([n/2 m/2])-[2 2],[n m])-ceil([n/2 m/2])+[1 1];


% subfunctions
%-------------------------
function mi=min2par(arg)

[X,Y]=meshgrid(-1:1,-1:1);
r=Y(:);
c=X(:);
xt=quad2func(r,c);
a=xt\arg(:);
mi=[-a(2) -a(3)]/[2*a(5) a(4);a(4) 2*a(6)];

%-------------------------
% function xt=quad2func(x,y)
% quadratic function of 2 parameters x and y
% size(xt)=[n 6] with n the number of observations
% for functions linear in the parameters:
% z = xt*a with size(a)=[6 1]
% i.e. z=a(1)+a(2)*x+a(3)*y+a(4)*x.*y+a(5)*x.^2+a(6)*y.^2
% if one has n observations given by z (size(z)=[n 1])
% the least squares estimation of a is
% a=xt\z;


function xt=quad2func(x,y)

xt=[ones(size(x)) x y x.*y x.^2 y.^2];
