% function c=correlate1(z1,z2)
% looks for the shift giving maximum correlation between (column) vectors z1 and z2
% uses Fourier transformation to calculate the cross-correlation between z1 and z2
% searches the maximum trough interpolation between the pixels
% uses parabolic fit around the absolute maximum
% result is c
% z2 should go c rows down (or z1 c rows up)
% to correct use interpolate1(f)(z2,c)
% if z1 and z2 are matrices the correlation is determined between the corresponding columns
%
% may not work for very short vectors (length 3 and so on)
% length of the vectors should be even
%
% assumes that the shift is less then half the number of rows of z1
% returns NaN if not successful for a given row
%
% origin: peter
%
% see also INTERPOLATE1, CORRELATE

function c=correlate1(z1,z2)

[n,m]=size(z1);
% m is the number of columns in z1 (and z2)
% n is the number of rows in z1
if rem(n,2)
	disp('Please use an even number of rows in correlate1!')
	return
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%   Calculation of cross-correlation function  %%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

cc=abs(ifft(fft(z1).*conj(fft(z2))));


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%   Determination maximum of cc function       %%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% starting from absolute maximum
[a,c]=max(cc);

% translation of the zero displacement to the center of the vector because this is where the maximum will be
% to avoid to look at start and end of the vector

c=rem(c+n/2-1,n)+1;
cc = circshift(cc, [round(n/2) 0]);
%cc=roll(cc,n/2,0);

% parabolic fit using w points around the maximum to go sub-pixel
w=3;
wb=floor((w-1)/2);
we=ceil((w-1)/2);
xt=quadfunc((1:w)');
xt2=xt'*xt;
for k=1:m 
	if (c(k)-wb<1)|(c(k)+we>n)
		disp(sprintf('Correlation was not successful for row %d',m))
		c(k)=NaN;
	else
		a=xt2\(xt'*cc(c(k)-wb:c(k)+we,k));
		c(k)=c(k)+(-a(2)/2/a(3));
	end
end

% correction for translation made
c=c-n/2-2-wb;










