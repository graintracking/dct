% Test program for the ITERATIVECLOSESTPOINT algorithm.
%
% This program:
%      - reads the reference data from an ASCII file;
%      - creates the floating data by rotating and translating the
%   reference data;
%      - plots the two initial data-sets as point clouds in the same
%   figure;
%      - asks you to hit any key
%      - performs ICP;
%      - plots the two final data-sets as point clouds in the same
%   figure;
%
% The reference data file must be an ASCII file with the points
% coordinates written on three columns:
%
%        P1_x    P1_y   P1_z
%        P2_x    P2_y   P2_z
%        P3_x    P3_y   P3_z
%              ........
%        Pn_x    Pn_y   Pn_z
%
%
% Author:  Cristian Pirnog
%          Computer Vision Laboratory - ETH Zurich
%          pirnog@vision.ee.ethz.ch
%
% Created: 12 May 2004
%
%/***************************************************************************
% *                                                                         *
% *   This program is not free software. It is distributed under the        *
% *   GNU General Public License version 2 or later. See the included       *
% *   "gpl.txt" file or get it at http://www.gnu.org/licenses/gpl.html      *
% *                                                                         *
% *   This program is the property of ETH (Swiss Federal Institute of       * 
% *   Technology) and the author. For feedback and comments please contact  *
% *   pirnog@vision.ee.ethz.ch                                              *
% *                                                                         *
% ***************************************************************************/
%

clear all

%+++++++++++++++++++++++++++++++++++++++++++++++++++++++
%    
%+++++++++++++++++++++++++++++++++++++++++++++++++++++++

%The reference data file
referenceDataFileName = 'data2_isotropic.txt';

% Use the default matlab axis (if useDefaultAxis~=0).
% If you use the default matlab axis is very difficult
% to compare the initial and final plots.
useDefaultAxis=0;

% Translation
deltaT = [0 0 0]; % [1, -2, 1];

% Rotation angles (degrees)
alphaX=17;
alphaY=-8;
alphaZ=14;

% The maximum number of steps for ICP
Nsteps=250;

% The maximum CLOSEST_DISTANCE for which the two 
% data-sets are considered alligned (this depends
% strongly on the data). If you don't want to use 
% it, just comment it.
maxICPDist=1e-5;

% Plot the data (if doPlot~=0).
doPlot=1;

%+++++++++++++++++++++++++++++++++++++++++++++++++++++++


close all
clc

% Read the reference data
r=load(referenceDataFileName, 'ascii').';


%Transform the angles in radians
alphaX = alphaX*pi/180;
alphaY = alphaY*pi/180;
alphaZ = alphaZ*pi/180;


% Compute the rotation matrix
Rx=[1, 0, 0;
    0, cos(alphaX), sin(alphaX);
    0, -sin(alphaX), cos(alphaX)];

Ry=[cos(alphaY), 0, -sin(alphaY);
    0, 1, 0;
    sin(alphaY), 0, cos(alphaY)];

Rz=[cos(alphaZ), sin(alphaZ), 0;
    -sin(alphaZ), cos(alphaZ), 0;
    0, 0, 1];

R=Rx*Ry*Rz
clear Rx Ry Rz


% fprintf(' {');
% fprintf('Point3D(%f, %f, %f),\n', R(1, 1), R(1, 2), R(1, 3));
% fprintf('Point3D(%f, %f, %f),\n', R(2, 1), R(2, 2), R(2, 3));
% fprintf('Point3D(%f, %f, %f)', R(3, 1), R(3, 2), R(3, 3));
% fprintf('};');

% Apply the transformation
% Rotate the floating data...
f=r-repmat(mean(r, 2), 1, size(r, 2));
f= R*f;
f=f+repmat(mean(r, 2), 1, size(r, 2));

% ... and then translate it
f = f + repmat(deltaT.', 1, size(r, 2));


% Plot the two initial data-sets
mX=min([min(f(1, :)) min(r(1, :))]);
mY=min([min(f(2, :)) min(r(2, :))]);
mZ=min([min(f(3, :)) min(r(3, :))]);

MX=max([max(f(1, :)) max(r(1, :))]);
MY=max([max(f(2, :)) max(r(2, :))]);
MZ=max([max(f(3, :)) max(r(3, :))]);

axisSize=max([MX-mX, MY-mY, MZ-mZ]);
if doPlot
  figure(1)
  hold off
  plot3(r(1, :), r(2, :), r(3, :), 'bo');
  grid on, hold on
  plot3(f(1, :), f(2, :), f(3, :), 'ro');
  title('Initial')
  legend('Ref', 'Flt')
  xlabel('x'), ylabel('y'), zlabel('z')
  if ~useDefaultAxis
    axis([mX mX+axisSize mY mY+axisSize mZ mZ+axisSize])
  end
  fprintf('Hit any key...\n')
  pause
end


% Perform ICP  
fprintf('Performing ICP...\t\t')
if ~exist('maxICPDist')
  [f1, tr, rot] = iterativeClosestPoint(r, f, Nsteps);
else
  [f1, tr, rot] = iterativeClosestPoint(r, f, Nsteps, maxICPDist);
end
fprintf('[Done]')


% Plot the two final data-sets
if doPlot
  figure(2)
  hold off
  plot3(r(1, :), r(2, :), r(3, :), 'bo');
  hold on, grid on
  plot3(f1(1, :), f1(2, :), f1(3, :), 'k*');
  title('After ICP')
  legend('Ref', 'Flt')
  xlabel('x'), ylabel('y'), zlabel('z')
  if ~useDefaultAxis
    axis([mX mX+axisSize mY mY+axisSize mZ mZ+axisSize])  
  end
end
