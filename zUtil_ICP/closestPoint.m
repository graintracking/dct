% CLOSESTPOINT Compute one step of the Iterative Closest Point (ICP) algorithm.
%
% [TR, ROT] = closestPoint(REF, FLT) returns the translation vector
%   TR and the rotation matrix ROT that lead to the minimization of
%   the total distance from the floating points FLT to their
%   corresponding closest reference points REF. The vectors FLT and
%   REF must have size either Mx3 and Nx3 or 3xM and 3xN (where M
%   and N are the number of reference and floating points
%   respectively).
%
%   Important: THIS ALGORITHM WORKS ONLY FOR THREE DIMENSIONAL POINTS.
%
%   Each floating point has a CLOSEST_DISTANCE, which is the
%   distance from it to its closest reference point.
%
% [TR, ROT] = closestPoint(REF, FLT, THR) will use the supplied
%   threshold THR for leaving out possible outliers. A floating
%   point is considered outlier if its CLOSEST_DISTANCE is outside
%   the domain [0, AVG_DIST+THR], where AVG_DIST is the average
%   distance of all the floating points.
%
% [TR, ROT, AVG_DIST] = closestPoint(REF, FLT) will also return the
%   average CLOSEST_DISTANCE, which can be used to stop the ICP
%   algorithm if early convergence was achieved.
%
%   If no threshold is supplied, CLOSESTPOINT will use the
%   default threshold, which is 2*DIST_ST_DEV, where DIST_ST_DEV
%   is the standard deviation of the floating points
%   CLOSEST_DISTANCEs.
%
%   Example:
%
%   See also: ITERATIVECLOSESTPOINT
%
%
% Author:  Cristian Pirnog
%          Computer Vision Laboratory - ETH Zurich
%          pirnog@vision.ee.ethz.ch
%
% Created: 12 May 2004
%
%/***************************************************************************
% *                                                                         *
% *   This program is not free software. It is distributed under the        *
% *   GNU General Public License version 2 or later. See the included       *
% *   "gpl.txt" file or get it at http://www.gnu.org/licenses/gpl.html      *
% *                                                                         *
% *   This program is the property of ETH (Swiss Federal Institute of       * 
% *   Technology) and the author. For feedback and comments please contact  *
% *   pirnog@vision.ee.ethz.ch                                              *
% *                                                                         *
% ***************************************************************************/
%

function [translation, rotation, varargout] = closestPoint(referencePoints, ...
						  floatingPoints, varargin)


%===============================================================
% Check that the data is given in a 2D matrix
%===============================================================
if ndims(referencePoints)~=2
  error('The reference matrix must be two dimensional.')
end

if ndims(floatingPoints)~=2
  error('The floating matrix must be two dimensional.')
end


%===============================================================
% Check that the data is 3-dimensional
%===============================================================
noOfDims = 3;
if ~(size(referencePoints)==noOfDims) || ~sum(size(floatingPoints)==noOfDims)
  error('This algorithm works only for 3D points.')
end

%===============================================================
% Check for 3X3 matrices (where you cannot differenciate the 
% dimension from the number of points)
%===============================================================
if size(referencePoints)==[noOfDims, noOfDims]
  error( ['Te reference data matrix has size [', ...
	  int2str(noOfDims), ', ', int2str(noOfDims), ...
	  '].', ...
	  'Cannot differenciate between the coordinates and the', ...
	  ' actual points.']);
end



% Make the floating data points have the coordinates on columns
%
% floatingPoints = [ x1, y1, z1;
%                    x2, y2, z2;
%                    x3, y3, z3;
%                    x4, y4, z4;
%                       ...
%                    xN, yN, zN];
%
if size(floatingPoints, 1)==noOfDims
  floatingPoints=floatingPoints.';
end

% Same for the reference data points
if size(referencePoints, 1)==noOfDims
  referencePoints=referencePoints.';
end

% The number of original floating and reference points
floatingPointsNo=size(floatingPoints, 1);
referencePointsNo=size(referencePoints, 1);


%----------------------------------------------------------
%           Start closest point computation
%----------------------------------------------------------

% 1. Find the reference points the that are closest to each floating
% point
closestPoints=[];
minDistances=[];
for i=1:floatingPointsNo
  distances = sum((referencePoints - repmat(floatingPoints(i, :), ...
					    referencePointsNo, 1)).^2, 2);
  
  [dist, index]=min(distances);
  minDistances(i)=sqrt(dist);
  
  closestPoints(i, :)=referencePoints(index, :);
end


% 2. Elliminate the possible outliers
meanDist=mean(minDistances);
if nargin==3
  threshold=varargin{:, 1};
else
  stdDev=std(minDistances, 1);
  threshold=2*stdDev;
end


% 3. Keep the good floating points...
goodFloatingPoints = floatingPoints(minDistances-meanDist<threshold, :);
% ... and their corresponding closest points
closestPoints = closestPoints(minDistances-meanDist<threshold, :);

if isempty(closestPoints)
  error(['The closest points vector is empty. You probably set a' ...
	 ' too big threshold']);
end

drawnow
% 4. Compute the center of the floating and reference (closest) points
centerOfMass_flt = mean(goodFloatingPoints, 1);
centerOfMass_ref = mean(closestPoints, 1);

drawnow
% 5. Compute the cross-covariance matrix
goodPointsNo=size(closestPoints, 1);
C = closestPoints.'*goodFloatingPoints/goodPointsNo;
C = (C - centerOfMass_ref.'*centerOfMass_flt).';

drawnow
% 6. Compute "delta"
delta = [C(2, 3) - C(3, 2),
	 C(3, 1) - C(1, 3),
	 C(1, 2) - C(2, 1)].';

% ... and cross-covariance matrix trace
traceC=trace(C);

drawnow
% 7. Compute matrix "Q"
tmp = (C + C.') - diag(ones(noOfDims, 1))*traceC;
Q = [ traceC, delta;
      delta.', tmp];

    
drawnow
% 8. Compute the eigenvectors and eigenvalues
[V, D] = eig(Q);

drawnow
% 9. Compute the rotation matrix...
v=V(:, 4).';
v2=v.^2;
rotation(1, :) = [ v2(1) + v2(2) - v2(3) - v2(4), ...
		   2*(v(2)*v(3) - v(1)*v(4)), ...
		   2*(v(2)*v(4)+v(1)*v(3))];

rotation(2, :) = [ 2*(v(2)*v(3) + v(1)*v(4)), ...
		   v2(1) + v2(3) - v2(2) - v2(4), ...
		   2*(v(3)*v(4) - v(1)*v(2))];

rotation(3, :) = [ 2*(v(2)*v(4)-v(1)*v(3)), ...
		   2*(v(3)*v(4) + v(1)*v(2)), ...
		   v2(1) + v2(4) - v2(2) - v2(3)];

% ... and the translation vector
translation = centerOfMass_ref - centerOfMass_flt*rotation.';


%===============================================================
% Assign the output values
%===============================================================
if nargout==3
  varargout(1) = {meanDist};
end

