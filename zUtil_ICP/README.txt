

This Matlab implementation of the ICP (Iterative Closest Point) algorithm 
is licensed under the GNU General Public License version 2 or later. 

Before starting using and/or changing any of the files included here, please
take a moment and reade the included "gpl.txt" file or get it at:

http://www.gnu.org/licenses/gpl.html


All Matlab files included here must contain the following text, which should
be also visible from Matlab's help:
%
%/***************************************************************************
% *                                                                         *
% *   This program is not free software. It is distributed under the        *
% *   GNU General Public License version 2 or later. See the included       *
% *   "gpl.txt" file or get it at http://www.gnu.org/licenses/gpl.html      *
% *                                                                         *
% *   This program is the property of ETH (Swiss Federal Institute of       * 
% *   Technology) and the author. For feedback and comments please contact  *
% *   pirnog@vision.ee.ethz.ch                                              *
% *                                                                         *
% ***************************************************************************/
%


This directory should contain at least the following files:

  closestPoint.m           - implements one iteration of the ICP
  iterativeClosestPoint.m  - performs the actual ICP
  testICP.m                - test file for the algorithm
  gpl.txt                  - GNU General Public License version 2


If any of this files is missig, please contact Cristian Pirnog at:
pirnog@vision.ee.ethz.ch.


The directory may also contain any of these sample data files:

  data1.txt
  data1_isotropic.txt
  data2.txt
  data2_isotropic.txt

The sample data files can be used directly with "testICP.m", to give you
an ideea of how these programs work.

Best regards,
Cristian Pirnog



 ---------------------------------------------
| Cristian Pirnog - pirnog@vision.ee.ethz.ch  |
| ETZ F 71, Gloriastrasse 35                  |
| ETH Zentrum, 8092 Zurich,  Tel. 01 632 7948 |
| Web Page: www.vision.ee.ethz.ch/~pirnog     |
 ---------------------------------------------