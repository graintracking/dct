% ITERATIVECLOSESTPOINT Perform the Iterative Closest Point (ICP) algorithm.
%
% [TR_FLT] = iterativeClosestPoint(REF, FLT, STEPS) will return the
%   transformed floating points TR_FLT that have minimal distance
%   to the reference points REF. The number of iteration steps will
%   be STEPS.
%
%   Important: THIS ALGORITHM WORKS ONLY FOR THREE DIMENSIONAL POINTS.
%
% [TR, ROT] = iterativeClosestPoint(REF, FLT, STEPS) returns the
%   translation vector TR and the rotation matrix ROT that lead to
%   the minimization of the total distance from the floating points
%   FLT to their corresponding closest reference points REF.
%
% [TR, ROT] = iterativeClosestPoint(REF, FLT, STEPS, MIN_DIST) will
%   stop the optimization if the average CLOSEST_DISTANCE is smaller
%   than MIN_DIST. See CLOSESTPOINT, for details about
%   CLOSEST_DISTANCE. In this case, STEPS represents the maximum
%   number of iterations performed.
%
% [TR, ROT] = iterativeClosestPoint(REF, FLT, STEPS, MIN_DIST, THR)
%   will use the supplied threshold THR for leaving out possible
%   outliers during optimization. See CLOSESTPOINT, for more
%   details.
%
% [TR_FLT, TR, ROT] = iterativeClosestPoint(REF, FLT, STEPS, ...)
%   will return the transformed floating points and also the
%   translation vector and the rotation matrix.
%
%   If no threshold is supplied, ITERATIVECLOSESTPOINT will use the
%   default threshold from CLOSESTPOINT.
%
%   Example:
%
%   See also: CLOSESTPOINT
%
%
% Author:  Cristian Pirnog
%          Computer Vision Laboratory - ETH Zurich
%          pirnog@vision.ee.ethz.ch
%
% Created: 12 May 2004
%
%/***************************************************************************
% *                                                                         *
% *   This program is not free software. It is distributed under the        *
% *   GNU General Public License version 2 or later. See the included       *
% *   "gpl.txt" file or get it at http://www.gnu.org/licenses/gpl.html      *
% *                                                                         *
% *   This program is the property of ETH (Swiss Federal Institute of       * 
% *   Technology) and the author. For feedback and comments please contact  *
% *   pirnog@vision.ee.ethz.ch                                              *
% *                                                                         *
% ***************************************************************************/
%


function varargout = iterativeClosestPoint(referencePoints, ...
					   floatingPoints, steps, ...
					   varargin) 


%===============================================================
% Check that 'steps' is a scalar
%===============================================================
if ndims(steps)~=2 || min(size(steps))~=1
  error('The number of steps must be a scalar')
end


%===============================================================
% Check for 'threshold'
%===============================================================
minDist=0;
closestPointCall='closestPoint(referencePoints, floatingPoints)';
if nargin>3
  minDist=varargin{:, 1};
  if nargin==5
    threshold=varargin{:, 2};
    closestPointCall=['closestPoint(referencePoints, floatingPoints,' ...
		      ' threshold)'];
  end
end


%===============================================================
% Make the floating data points have the coordinates on columns
%===============================================================
% floatingPoints = [ x1, y1, z1;
%                    x2, y2, z2;
%                    x3, y3, z3;
%                    x4, y4, z4;
%                       ...
%                    xN, yN, zN];
%
noOfDims = 3;
if size(floatingPoints, 2)==noOfDims
  floatingPoints=floatingPoints.';
end

% Same for the reference data points
if size(referencePoints, 2)==noOfDims
  referencePoints=referencePoints.';
end


%===============================================================
% Initialize translation and rotation
%===============================================================
rotation=eye(3);
translation=zeros(1, 3);


%===============================================================
% Perform the ICP
%===============================================================
for i=1:steps
  % Compute the new translation and rotation
  [newTranslation, newRotation, d]=eval(closestPointCall);
  fprintf('Distance: %f\n',d);
  drawnow % let user trying to interact with figures get _some_ chance!  GJ
  
  % Transform the floating points
  floatingPoints = newRotation*floatingPoints + repmat(newTranslation.', 1, size(floatingPoints, 2));

  % Update the total rotation and translation
  rotation = newRotation*rotation;
  translation = translation + newTranslation*newRotation;
    
  % Check for early convergence
  if minDist>d
    fprintf('Converged to %f after %d steps\n',d,i)
    break
  end
end
if i==steps
  fprintf('Converged only to %f after %d steps \n',d,steps)
end
%===============================================================
% Assign the output values
%===============================================================
switch nargout
 case 1
  varargout(1) = {floatingPoints};
 case 2
  varargout(1) = {translation};
  varargout(2) = {rotation};
 otherwise
  varargout(1) = {floatingPoints};
  varargout(2) = {translation};
  varargout(3) = {rotation};
end