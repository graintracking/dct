function edf_check(filename)
    % Acquire file information
    edf_file = dir(filename);

    % File Error Checking
    if (isempty(edf_file))
        error('EDF:file_not_found', 'Could not find: "%s"', filename);
    end

    if (edf_file.isdir == true)
        error('EDF:file_is_directory', ...
            '"%s" is a directory instead of a file.', filename);
    end

    if (edf_file.bytes == 0)
        error('EDF:file_is_empty', '"%s" is an empty file.', filename);
    end
end