function [img, varargout] = edf_read(filename, GT_bb, dispmes, info, varargin)
% EDF_READ  Reads images and 3D volumes from .edf files.
%    --------------------------------------------------------------------------
%    [img[, info]] = edf_read(filename[, GT_bb, dispmes, info, varargin]);
%
%    INPUT:
%      filename = <string> Input edf filename
%
%    OPTIONAL INPUT:
%      GT_bb    = <int>    Region of interest [originx originy width height]
%                          (works only with 2D files)
%      dispmes  = <string> Verbose mode {'disp', 'nodisp'}
%      info     = <struct> EDF header info (as input) 
%
%    OUTPUT:
%      img      = <double> Output image (2D or 3D)
%
%    OPTIONAL OUTPUT:
%      info     = <struct> EDF header info (as output)
%
%    Version 002 XX-06-2006 by NVigano,  nicola.vigano@esrf.fr
%      Many changes, many versions
%      Modified by Nicola Vigano, 2011-2015, nicola.vigano@esrf.fr
%
%    Version 001 XX-06-2006 by GJohnson

    if (~exist('dispmes', 'var'))
        dispmes = true;
    else
        dispmes = ~strcmp(dispmes, 'nodisp');
    end

    conf = struct('verbose', dispmes, 'convert', true, 'permute', true);
    conf = parse_pv_pairs(conf, varargin);

    % Check file
    edf_check(filename);

    % Let's read the header
    if (~exist('info', 'var') || isempty(info))
        info = edf_info(filename);
    elseif (~isfield(info, 'headerlength') || info.headerlength < 0)
        % Make it sure the header length is found/computed!
        info.headerlength = findHeaderLength(filename, info.byteorder);
    end

    % Test and fix third dimension
    if (~isfield(info, 'dim_3'))
        info.dim_3 = 1;
    else
        if (info.dim_3 > 1 && (exist('GT_bb', 'var') && ~isempty(GT_bb)))
            error('EDF:roi_in_3d', 'Cannot use an ROI on 3D EDF file');
        end
    end

    fid = fopen(filename, 'r', info.byteorder);
    if (fid == -1)
        error('EDF:file_not_found', ['Could not open ' fname]);
    end
    fseek(fid, info.headerlength, 'bof');

    %%%%%%%%%%%%
    %ROI

    if (exist('GT_bb', 'var') && ~isempty(GT_bb))
        % GT_bb = [originx originy width height]
        kodak_midline = 1024 - GT_bb(2) + 1;

        switch info.datatype
            case {'uint8', 'int8'}
                nbytes = 1;
            case {'uint16', 'int16'}
                nbytes = 2;
            case {'uint32', 'int32', 'float32'}
                nbytes = 4;
            case {'uint64', 'int64', 'float64'}
                nbytes = 8;
        end

        % ROI was specified
        pr_str = sprintf('%d*%s=>%s', GT_bb(3), info.datatype, info.datatype);
        skip = nbytes * (info.dim_1 - GT_bb(3));
        fseek(fid, nbytes * (info.dim_1 * (GT_bb(2) - 1) + GT_bb(1) - 1), 0);
        [img, count] = fread(fid, [GT_bb(3), GT_bb(4)], pr_str, skip);
        dim_1 = GT_bb(3);
        dim_2 = GT_bb(4);
    else
        %%%%%%%%%%%%%
        kodak_midline = 1024;
        dim_1 = info.dim_1;
        dim_2 = info.dim_2;
        [img, count] = fread(fid, info.dim_1*info.dim_2*info.dim_3, [info.datatype '=>' info.datatype]);
    end
    fclose(fid);

    %%% Check if the file was corrupted
    if (count ~= (dim_1*dim_2*info.dim_3))
        error('EDF:corrupted_file', ...
            'File "%s" contains less data than expected:\n  Expected: %d bytes, Got: %d bytes', ...
            dim_1 * dim_2 * info.dim_3, count);
    end

    %%% Let's reshape to the real format of the matrix
    img = reshape(img, dim_1, dim_2, info.dim_3);
    if conf.permute
        if (info.dim_3 > 1) % 3D volume
            img = permute(img, [2 1 3]); % reorganise dimensions
        else % 2D image
            img = transpose(img);
        end
    end

    if (~isa(img, 'double') && conf.convert)
        img = double(img);
    end

    % Kodak4MV1: do any special processing required
    if (isfield(info, 'ccdmodel') && any(isfield(info, {'isccdcorrected', 'israw'})))
        % long test to keep compatible with 'israw' and 'isccdcorrected'
        % fields.  Should be able to remove 'israw' in due course
        if (strcmpi(info.ccdmodel, 'kodak4mv1') ...
                && ( (isfield(info, 'isccdcorrected') && strcmpi(info.isccdcorrected, 'false')) ...
                || (isfield(info, 'israw') && strcmpi(info.israw, 'true'))) )
            if (kodak_midline > 0 && info.dim_2 == 2048)
                if (conf.verbose)
                    disp('Adding missing row for Kodak4M sensor and applying 0.96 gamma')
                end
                imc = [ img(1:kodak_midline, :); ...
                    mean([img(kodak_midline, :); ...
                    img(kodak_midline+1, :)]); ...
                    img(kodak_midline+1:end-1, :) ];
                img = imc .^ 0.96;
            elseif (conf.verbose)
                disp('WARNING: Cropped image may be corrupted due to Kodak4M sensor.')
            end
        end
    end

    %%% If requested, let's output the header
    if (nargout == 2)
        varargout{1} = info;
    end
end

function header_length = findHeaderLength(filename, byteorder)
    fid_find = fopen(filename, 'r', byteorder);
    if (fid_find == -1)
        error('EDF:file_not_found', ['Could not open ' fname]);
    end

    for ii = 1:8
        fseek(fid_find, 512 * ii - 2, 'bof');
        if (fgets(fid_find, 1) == '}')
            fclose(fid_find);
            header_length = 512 * ii;
            return
        end
    end

    fprintf('Malformed header, searching manually.\n');
    fseek(fid_find, 1, 'bof');
    end_of_header = false;
    while (~end_of_header)
        htxt = fgetl(fid_find);
        end_of_header = find( htxt == '}' );
    end
    header_length = ftell(fid_find);
    fclose(fid_find);
end
