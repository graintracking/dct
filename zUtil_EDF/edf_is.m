function edf = edf_is(filename)
% EDF_IS  Check if a file contains a EDF header.
%    -------------------------------------------------------------------------
%    edf = edf_is(filename)
%
%    INPUTS:
%      filename = <string>  Name of the file to check%
%
%    OUTPUT:
%      edf      = <bool>    Result of the check

fid=fopen(filename,'rt');
fgetl(fid);  % first line
header=strfind(fgetl(fid),'HeaderID');
fclose(fid);

if header
    edf=true;
else
    edf=false;
end

end % end of function
