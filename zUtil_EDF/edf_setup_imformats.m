function edf_setup_imformats()
% adds the EDF file format to the register of image types, so that EDF files
% can be opened with imread, imwrite, imfinfo,etc

fmt.ext={'edf'};
fmt.isa=@edf_is;
fmt.info=@edf_info;
fmt.read=@edf_read;
fmt.write=@edf_write;
fmt.alpha=0;  % no alpha channel in EDFs
fmt.description='ESRF EDF image file format';

imformats('update','edf',fmt)
