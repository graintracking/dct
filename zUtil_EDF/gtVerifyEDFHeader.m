function correct = gtVerifyEDFHeader(directory, verbose)
% GTVERIFYEDFHEADER  Checks the validiy of the header of multiple EDF files.
%     -------------------------------------------------------------------------
%     correct = gtVerifyEDFHeader(directory, verbose)
%
%     INPUT:
%       directory = <string>  Directory where the EDF files are located
%       verbose   = <bool>    Verbose mode
%
%     OUTPUT:
%       correct   = <bool>    True if all headers are correct

if (~exist('verbose', 'var'))
        verbose = true;
    end
    out = GtConditionalOutput(verbose);
    correct = true;
    out.fprintf('Getting names: ');
    files = dir(fullfile(directory, '*.edf'));
    out.fprintf('Done.\n');

    gauge = GtGauge(length(files), 'Verifying headers: ', 'verbose', verbose);
    for ii = 1:length(files)
        gauge.incrementAndDisplay();
        info = edf_info(fullfile(directory, files(ii).name));
        if (mod(info.headerlength, 512) ~= 0)
            correct = false;
        end
    end
    gauge.delete();
end

end % end of function

