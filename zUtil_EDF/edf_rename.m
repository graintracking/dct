function newname = edf_rename(oldname, parameters)
% EDF_RENAME  Determines how files should be renamed
% Name convention, description of scheme:
%
% for 80 images collected over two turns
% note this would be nproj=40 in ftomosetup, so be careful!
% keep parameters.acq.nproj as images/180degrees in final data
%
% first turn:
% ref0000_0000.edf..., name_0_0000.edf, name_0_0001.edf... name_0_0039.edf,
% ref0000_0040.edf
%
% second turn: (This was probably true in the past)
% name_1_0000.edf, name_1_0001.edf... name_1_0039.edf, ref0000_1_0040.edf
% second turn: (This is TRUE now)
% name_1_0000.edf, name_1_0001.edf... name_1_0039.edf, ref0000_0040_1.edf

    [~, fname, ext] = fileparts(oldname);

    if (strcmp(ext, '.edf') && ~(strncmpi(fname, 'dark', 4)))
        % we want sequentially number filenames and references groups, without
        % the turn indication.
        if (strncmpi(fname, 'ref', 3) || strncmpi(fname, 'quali', 5))
            splitted_name = regexp(fname, '_', 'split');
            % bad idea to name scans ref* or quali*!
            switch (numel(splitted_name))
                case 3
                    chief_turn = str2double(splitted_name{end});
                    old_number = str2double(splitted_name{end-1});
                case 2
                    chief_turn = 0;
                    old_number = str2double(splitted_name{end});
                otherwise
                    % don't rename / renumber / overwrite if not sure!
                    gtError('gtCopyCorrectUndistortCondor:wrong_renaming', ...
                        'dont recognise the name of this edf!')
            end
            % keep the ref00xx_ and quali_ part of the name
            base_name = [splitted_name{1} '_'];

        elseif (strncmpi(fname, parameters.acq.name, length(parameters.acq.name)))
            % We remove the assumption of '_' at the end of the acquisition name
            numbers_name = strrep(fname, parameters.acq.name, '');
            splitted_name = regexp(numbers_name, '_', 'split');
            % if this is an image.
            if (parameters.acq.interlaced_turns > 0)
                chief_turn = str2double(splitted_name{end-1});
            else
                chief_turn = 0;
            end
            old_number = str2double(splitted_name{end});
            base_name = parameters.acq.name;
        else
            % don't rename / renumber / overwrite if not sure!
            gtError('gtCopyCorrectUndistortCondor:wrong_renaming', ...
                'dont recognise the name of this edf!')
        end
        images_per_turn = (2 * parameters.acq.nproj) ...
            / (parameters.acq.interlaced_turns + 1);
        new_number = old_number + (chief_turn * images_per_turn);
        % note that in some datasets we exceed 9999 images.
        newname = sprintf('%s%04d.edf', base_name, new_number);
    else
        % Either not an edf file or darks
        newname = oldname;
    end
end
