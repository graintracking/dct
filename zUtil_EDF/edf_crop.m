function edf_crop(fnametemplate, fnamerange, xrange, yrange, replace)
% EDF_CROP.M
% Crops sequences of EDF files (orignalXYZ.edf) and writes them to 'originalcrop_XYZ.edf'
% May 2006, Greg Johnson
%
%> Usage:
% edf_crop(filename_template,filename_range,xrange,yrange)
%> Uses the standard C conventions for the filename_template (file_%04d.edf
% will find all the images with a four digit number at the end).  Using the
% .edf extension is OPTIONAL - works either way
% filename_range is a vector of file number ([1:1500] or if want
% [0:5:1500])
% xrange and yrange are also vectors that can be simple ([400:600]) or more
% complex for skipping pixels ([400:10:600]).
%
% By default, EDF_CROP will not replace an existing file.  If you are sure
% you want to, use 'replace' as an additional argument at the end
%
% Examples:
% edf_crop('yourfiles_%05',0:5:1500,1:10,50:75)
% edf_crop('myfiles_%04d.edf',0:1502,100:900,200:5:500,'replace')
% edf_crop('dark',0,100:900,200:5:500);  % (works on single image, too)
% 
% See also:
% EDF_DOWNSAMPLE

% Not much error checking done for correct arguments :(

  if exist('replace') == 1
    replace = strcmpi(replace,'replace')
  else
    replace = false;
  end

  if replace
    disp('Replacing any files which already exist!!')
  else
    disp('Not replacing existing files')
  end

  [fname_root, fname_format_string] = fileparts(fnametemplate);
  fname_in_tmplt  = [fname_root fname_format_string '.edf'];
  fname_out_tmplt = [fname_root 'crop_' fname_format_string '.edf'];

  for n = fnamerange
    fnamein = sprintf(fname_in_tmplt, n);
    fnameout = sprintf(fname_out_tmplt, n);
    fprintf('Slice: %d/%d...', n, fnamerange(end));
    if replace == false && exist(fnameout,'file')
      disp('skipping because it already exists and we''re not replacing:\n');
      disp(['  ' fnameout])
    else
      [im, info] = edf_read(fnamein);
      % image is NOT transposed, so yrange,xrange (rather than vice-versa) IS correct
      im_crop = im(yrange, xrange);
      edf_write(im_crop, fnameout, info)
    end
    fprintf('\r');
  end
  fprintf('\n')
end

