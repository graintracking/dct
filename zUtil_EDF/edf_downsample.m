function edf_downsample(varargin)
% EDF_DOWNSAMPLE.M
% Downsamples (bins) a (series of) EDF files.
%
% Usage:
% edf_downsample(filename)
%   will downsample the EDF file (with or without extensions) specified with 
%   binning of 2 and write to filename_binned.edf 
% 
% Greg Johnson, May 2006
%

fnamein=varargin{1};
fparts=utilExtractFilenameParts(fnamein);

if isempty(fparts.extension) % user didn't supply filename with extension
    fparts.extension='edf';
end

fnameout=[fparts.prefix 'binned_' fparts.index '.' fparts.extension];
fnamein=[fparts.prefix fparts.index '.' fparts.extension];
% check file exists
if ~exist(fnamein)
    disp('Can''t see file - sorry!');
    return;
end

% read original file
im=edf_read(fnamein);

% bin 2x2
sizeim=size(im);
if mod(sizeim(1),2)==1  % odd number of rows - crop from end if so
    im=im(1:end-1,:);
end
if mod(sizeim(2),2)==1 % odd number of columesn - crop from end if so
    im=im(:,1:end-1);
end

im_d=(im(1:2:end,1:2:end)+im(1:2:end,2:2:end)+im(2:2:end,1:2:end)+im(2:2:end,2:2:end))/4;

% write result.  May not be the best choice to use floats, but it is flexible...
edf_write(im_d,fnameout,'float32');

end % end of function
