function edf_crop_hp(fnametemplate,fnamerange,xrange,yrange,varargin)
% UPDATE HELP
% EDF_CROP.M
% Crops sequences of EDF files (orignalXYZ.edf) and writes them to 'originalcrop_XYZ.edf'
% May 2006, Greg Johnson
% Modified by Henry Proudhon to estimate the correct motor positions to a
% new xml file if asked to...
%
% Usage:
% edf_crop(filename_template,filename_range,xrange,yrange)
% Uses the standard C conventions for the filename_template (file_%04d.edf
% will find all the images with a four digit number at the end).  Using the
% .edf extension is OPTIONAL - works either way
% filename_range is a vector of file number ([1:1500] or if want
% [0:5:1500])
% xrange and yrange are also vectors that can be simple ([400:600]) or more
% complex for skipping pixels ([400:10:600]).
%
% By default, EDF_CROP will not replace an existing file.  If you are sure
% you want to, use 'replace' as an additional argument at the end
%
% Examples:
% edf_crop('yourfiles_%05',0:5:1500,1:10,50:75)
% edf_crop('myfiles_%04d.edf',0:1502,100:900,200:5:500,'replace',true)
% edf_crop('dark',0,100:900,200:5:500);  % (works on single image, too)
% 
% See also:
% EDF_DOWNSAMPLE

params.replace=false;
params.string='crop_';
params.xml=false;
params=parse_pv_pairs(params,varargin);

[fnameroot,fnameformatstring]=utilStripExtensionandFormat(fnametemplate);
fnameintemplate=[fnameroot fnameformatstring '.edf'];
fnameouttemplate=[fnameroot params.string fnameformatstring '.edf'];

if params.replace
  disp('Replacing any files which already exist!!')
else
  disp('Not replacing existing files')
end

for n=fnamerange
    fnamein=sprintf(fnameintemplate,n);
    fnameout=sprintf(fnameouttemplate,n);
    fprintf('Slice: %d/%d... %s',n,fnamerange(end),fnameout);
    if params.replace==false
        if exist(fnameout,'file')
            fprintf('skipping\n');
        else
            sfReadandCrop
        end
    else
        sfReadandCrop
    end
    fprintf('\r');
end
fprintf('\n')

if params.xml
    xmlinfname=[fnameroot '.xml'];
    xmloutfname=[fnameroot params.string '.xml'];
    correctXml(xmlinfname,xmloutfname);
    fprintf('wrote xml file with updated motor position: %s\n',xmloutfname);
end

function correctXml(xmlinfname,xmloutfname)
    xml=xml_load(xmlinfname);
    % find out which motors are pmy and sz
    for i=1:size(xml.acquisition.listMotors,2)
        if strcmp(xml.acquisition.listMotors(i).motor.motorName,'pmy')
            i_pmy=i;
        elseif strcmp(xml.acquisition.listMotors(i).motor.motorName,'sz')
            i_sz=i;
        end
    end
    pmy_orig = str2double(xml.acquisition.listMotors(i_pmy).motor.motorPosition);
    sz_orig = str2double(xml.acquisition.listMotors(i_sz).motor.motorPosition);
    dim_1 = str2int(xml.acquisition.projectionSize.DIM_1);
    dim_2 = str2int(xml.acquisition.projectionSize.DIM_2);
    px_size = str2float(xml.acquisition.pixelSize);
    % compute new motor position from the crop range and the pixel size
    xml.acquisition.listMotors(i_pmy).motor.motorPosition =...
        pmy_orig + (xrange(length(xrange))+xrange(1)-1-dim_1)/2*px_size/1000;
    xml.acquisition.listMotors(i_sz).motor.motorPosition =...
        sz_orig + (yrange(length(yrange))+yrange(1)-1-dim_2)/2*px_size/1000;
    xml_write(xmloutfname,xml)
end

function sfReadandCrop
    [im,datatype]=edf_read(fnamein);
    % image is NOT transposed with edf_read. so yrange, xrange (rather than vice-versa) IS correct
    im_crop=im(yrange,xrange);
    edf_write(im_crop,fnameout,datatype);

end

end
