function [img, varargout] = edf_wait_read(fname, bb, waitfortranslated, info)
% EDF_WAIT_READ.M Reads edf if available, otherwise wait 10s and try again...
% Usage:
% data = edf_wait_read(filename[, bb, waitfortranslated]);
% [data, info] = edf_wait_read(filename[, bb, waitfortranslated]);
%
% bb (optional) - only reads the region from [originx % originy width height]
%   inclusive (only works for 2D EDF files)
%
% waitfortranslated (optional) - if this is true, pfWaitToRead waits until the
%   istranslated field in the edf header is X where X is the number of times
%   that the image has been translated (ie it increments)
%
% If a second output variable is given, returns the edf header info
%
% from greg's sfWaitToRead(fname)
% and then from peter's pfWaitToRead(fname, bb, waitfortranslated)
% Modified by Nicola Vigano, 2011-2012, nicola.vigano@esrf.fr

  %% handle input
  if ~exist('bb', 'var')
    bb=[];
  end

  if ~exist('waitfortranslated', 'var')
    waitfortranslated = false;
  %else
  %  disp(sprintf('waiting for images to have been translated %d times', waitfortranslated))
  end

  %% wait for file to appear
  if ~exist(fname,'file')
    seconds = 0;
    fprintf('Waiting for "%s"\n',fname);
    while ~exist(fname,'file')
      pause(2)
      seconds = seconds + 2;
    end

    minutes = floor(seconds/60);
    hours = floor(minutes/60);
    fprintf('Found it (in %dh:%dm:%ds)!\n', hours, minutes, seconds);
    pause(2)
  end

  %% check whether file has been translated 
  % (by gtApplyDrifts.m)
  firsttime = true;
  if waitfortranslated
    translated = false;
    while ~translated
      try
        if (~exist('info', 'var'))
          info = edf_info(fname);
        end
        if isfield(info, 'istranslated') && (str2double(info.istranslated) == waitfortranslated)
          translated = true;
          %disp('file has been translated!')
        else
          if firsttime == true
            disp('waiting for image to be translated....')
            firsttime = false;
          end
          pause(2)
        end
      catch mexc
        gtPrintException(mexc, 'EDF access failed.')
        % Just try again (we shouldn't rely on this)
%         gtCheckExceptionType(mexc, 'EDF:', @()rethrow(mexc));
        disp(' - trying again')
        pause(1)
      end  %try - sometimes file acess fails because of simultaneous read / write access...
    end
  end

  %% try to read file
  while (true)
    try
      if ~exist('info', 'var')
        [img, info] = edf_read(fname, bb ,'nodisp');
      else
        img = edf_read(fname, bb ,'nodisp', info);
      end
      break
    catch mexc
      gtPrintException(mexc, 'EDF Read failed.')
      % Just try again (we shouldn't rely on this)
%       gtCheckExceptionType(mexc, 'EDF:', @()rethrow(mexc));
      disp(' - trying again')
      pause(1)
    end
  end

  %% If requested, let's output the header
  if nargout > 1
    varargout{1} = info;
  end
end

