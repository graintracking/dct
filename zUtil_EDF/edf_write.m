function edf_write(data, filename, info, minimal)
% EDF_WRITE  An EDF writer than handles images and 3D volumes
%    --------------------------------------------------------------------------
%    edf_write(data, filename, info, minimal)
%
%    WARNING!  THESE IMAGES DO NOT NEED TRANSPOSING BEFORE WRITING!
%
%    Any data dimension provided in the fields 'dim_1','dim_2','dim_3' of
%    the header 'info' will be overwritten by the actual data sizes.   
%
%
%    INPUT:
%      data     = <anytype>  Image data to write in the EDF file. The datatype 
%                            of the EDF file will be the same as the Matlab 
%                            variable (usually double - 64 bits).
%      filename = <string>   Output EDF filename
%      
%    OPTIONAL INPUT:
%      info     = <struct>   Any information to put in the file header and 
%                            force any data type. See edf_info.m
%                            e.g header.datatype = 'uint16' for 16bit integers
%      minimal  = <any>      To speedup the writing process
%
%     NOTE:
%       If using edf header fields to store information, be aware that
%       capital letters get converted to lowercase.  Safer to use
%       non-capitialised field names
%       Andy King - 9/12/2008
%
%     Version 002 2011-2012 by NVigano,  nicola.vigano@esrf.fr
%       Added 'minimal' parameter to speedup the write.
%       Imported and modified.
%
%     Version 001 XX-06-2012 by GJohnson

    if (exist('info', 'var'))
        if isstruct(info)
            edfinfo = info;
        else
            edfinfo = [];
            edfinfo.datatype = info;
        end
    else
        edfinfo = [];
    end

    if (~exist('minimal', 'var'))
        minimal = false;
    end

    if (~exist('edfinfo', 'var') || ~isfield(edfinfo, 'datatype'))
        %disp('Datatype being determined from the variable')
        % sets datatype from type of data in matlab
        if isa(data, 'double')
            edfinfo.datatype = 'float64';
        elseif isa(data, 'single')
            edfinfo.datatype = 'float32';
        elseif isa(data, 'logical')
            data = uint8(data);
            edfinfo.datatype = 'uint8';
        elseif isa(data, 'uint8')
            edfinfo.datatype = 'uint8';
        elseif isa(data, 'uint16')
            edfinfo.datatype = 'uint16';
        elseif isa(data, 'uint32')
            edfinfo.datatype = 'uint32';
        else
            error('EDF:bad_data_format', 'Unsupported "%s" datatype', class(data));
        end
    end
    
    [edfinfo.dim_2, edfinfo.dim_1, edfinfo.dim_3] = size(data);
    edfinfo.size = edfinfo.dim_2 * edfinfo.dim_1 * edfinfo.dim_3;

    % Header
    header = sfGenerateHeader(edfinfo, minimal);
    % Data back in C format from Fortran
    data = permute(data, [2 1 3]);

    [fid, msg] = fopen(filename, 'w', 'l');
    if (fid == -1)
        error('EDF:open_error', 'Could not open %s: %s\n',filename, msg);
    end

    fwrite(fid, header);
    fwrite(fid, data, edfinfo.datatype);

    fclose(fid);
end % end of function

function header = sfGenerateHeader(edfinfo, minimal)
    info = edfinfo;
    % now setup ESRF datatype name
    switch info.datatype
        case 'uint8'
            esrfdatatype = 'UnsignedByte';
            nbytes = 1;
        case 'uint16'
            esrfdatatype = 'UnsignedShort';
            nbytes = 2;
        case 'uint32'
            esrfdatatype = 'UnsignedLong';
            nbytes = 4;
        case 'int32'
            esrfdatatype = 'SignedLong';
            nbytes = 4;
        case 'float32'
            esrfdatatype = 'FloatValue';
            nbytes = 4;
        case 'float64'
            esrfdatatype = 'DoubleValue';
            nbytes = 8;
    end

    % I think info.size should be in bytes, not pixels/voxels - AK 30/3/2012
    info.size = info.size * nbytes;

    stringFields = {'HeaderID', 'ByteOrder', 'DataType', 'Image'; ...
        'EH:000001:000000:000000', 'LowByteFirst', esrfdatatype, '1'};
    numericFields = {'Dim_1', 'Dim_2', 'Size'; ...
        info.dim_1, info.dim_2, info.size};

    % It looks like that date is a costy operation
    if (~isfield(info, 'date'))
        stringFields(1:2, end+1) = {'Date'; date};
    else
        stringFields(1:2, end+1) = {'Date'; info.date};
    end

    % test removing dim 3 for 2D images for PyHST - AK 30/3/2012
    if (info.dim_3 ~= 1)
        numericFields(1:2, end+1) = {'Dim_3'; info.dim_3};
    end

    header = [ sprintf('{\n') ...
                edf_headerstring_cell(stringFields) ...
                edf_headerstring_cell(numericFields), ...
              ];

    % Skip unneeded fields if requested
    if (~minimal)
        compositeFields = { 'motor', 'counter' };
        for k = 1:length(compositeFields)
            if isfield(info, compositeFields{k})
                info_field = info.( compositeFields{k} );
                field_names = fieldnames(info_field);
                field_mne = [];
                field_pos = [];
                for ii = 1:length(field_names)
                    field_mne = [field_mne ' ' field_names{ii}];
                    value = mat2str( info_field.( field_names{ii} ) );
                    field_pos = [field_pos ' ' value];
                end
                header = [header, edf_headerstring([compositeFields{k} '_mne'], field_mne)];
                header = [header, edf_headerstring([compositeFields{k} '_pos'], field_pos)];
            end
        end

        fixedFields = {
            stringFields{1, :}, numericFields{1, :}, 'headerlength' ...
            };
        fieldsAlreadyProcessed = lower([fixedFields compositeFields{:}]);
        %%% Write remaining fields
        fields_left = fieldnames(info);
        % Quite expensive
        fields_left = setdiff(fields_left, fieldsAlreadyProcessed);

        for k = 1:length(fields_left)
            value = info.( fields_left{k} );
            header = [header, edf_headerstring(fields_left{k}, value)]; %#ok<AGROW>
        end
    end

    %%% Compute header blank space, on real header bytes usage.
    header_remaining_length = mod(512 - mod(length(header)+2, 512), 512);
    header = [header blanks(header_remaining_length) sprintf('}\n')];
end


function str_header = edf_headerstring(what, value)
% function str_header = edf_headerstring(what, value)
% returns a string for incorporation in an edf-header
% what is the information item (e.g. 'Dim_1')
% value of the item
% kind is string, integer or float
% origin: peter
    switch class(value)
        case 'char'
            str_header = sprintf('% 15s = %s ;\n', what, value);
        case 'logical'
            if value, value = 'true'; else value = 'false'; end
            str_header = sprintf('% 15s = %s ;\n', what, value);
        case {'double', 'single'}
            str_header = sprintf('% 15s = %f ;\n', what, value);
        case {'int8', 'uint8', 'int16', 'uint16', 'int32', 'uint32', 'int64', 'uint64'}
            str_header = sprintf('% 15s = %d ;\n', what, value);
        otherwise
            str_header = [sprintf('% 15s = ', what) value sprintf(' ;\n')];
    end
end

function str_header = edf_headerstring_cell(data)
% function str_header = edf_headerstring_cell(data)
% returns a string for incorporation in an edf-header
% what is the information item (e.g. 'Dim_1')
% value of the item
% kind is string, integer or float
% origin: peter
    switch class(data{2, 1})
        case 'char'
            str_header = sprintf('% 15s = %s ;\n', data{:});
        case 'logical'
            data(2, :) = cellfun(@(x){strtrim(char((x == 1) * 'true ' + (x == 0) * 'false'))}, data(2, :));
            str_header = sprintf('% 15s = %s ;\n', data{:});
        case {'double', 'single'}
            str_header = sprintf('% 15s = %f ;\n', data{:});
        case {'int8', 'uint8', 'int16', 'uint16', 'int32', 'uint32', 'int64', 'uint64'}
            str_header = sprintf('% 15s = %d ;\n', data{:});
        otherwise
            str_header = [sprintf('% 15s = ', data) value sprintf(' ;\n')];
    end
end

