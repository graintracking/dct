function im = edf_read_clean(fname, maskfname)
  mask = edf_read(maskfname);
  im = edf_read(fname);
  im = roifill(im, mask);
end
