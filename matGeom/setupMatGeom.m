function setupMatGeom(verbose)
% SETUPMATGEOM Add the different directories of MatGeom to the path

% ------
% Author: David Legland
% e-mail: david.legland@grignon.inra.fr
% Created: 2011-01-11,    using Matlab 7.9.0.529 (R2009b)
% Copyright 2011 INRA - Cepia Software Platform.

% Imported by LNervo

if ~exist('verbose','var') || isempty(verbose)
    verbose = false;
end
% extract library path
fileName = mfilename('fullpath');
libDir = fileparts(fileName);

moduleNames = {...
    'geom2d', ...
    'polygons2d', ...
    'graphs', ...
    'polynomialCurves2d', ...
    'geom3d', ...
    'meshes3d'};

fprintf('Installing MatGeom Library...');
% adding matGeom directory
addpath(libDir);

% add all library modules
for i = 1:length(moduleNames)
    name = moduleNames{i};
    if (verbose), fprintf('Adding module: %-20s', name); end
    addpath(fullfile(libDir, name));
    if (verbose), disp(' (ok)'); end
end
disp('Finished installing.')

end
