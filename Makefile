CXX=g++
CXXFLAGS=-O2 -march=native -std=c++11 -g -fopenmp -fPIC -Wall
CXXOPTIMFLAGS="${CXXFLAGS}"
LDOPTIMFLAGS="${CXXFLAGS}"

DEFINES_PROFILE=-DENABLE_CALLGRIND

IDIR=-IzUtil_Cxx/include -I/sware/com/matlab_2016a/extern/include
DEPS=DctData.h

LIBS=-lm
LBIR=

#DctData.o: DctData.cpp
#	$(CXX) -c -o $@ $< $(CXXFLAGS) $(IDIR) $(DEFINES)
#DctMatlabData.o: DctMatlabData.cpp
#	$(CXX) -c -o $@ $< $(CXXFLAGS) $(IDIR) $(DEFINES)

profile_reduction:
	/sware/com/matlab_2016a/bin/mex -v -client engine $(LIBS) $(LDIR) $(IDIR) $(DEFINES) CXXOPTIMFLAGS="${CXXFLAGS}" zUtil_Cxx/test/gtTestCxxMathsSumCellVolumes.cpp -largeArrayDims -outdir bin/test/

profile_cell_plus:
	/sware/com/matlab_2016a/bin/mex -v -client engine $(LIBS) $(LDIR) $(IDIR) $(DEFINES) CXXOPTIMFLAGS="${CXXFLAGS}" zUtil_Cxx/test/gtTestCxxMathsCellPlus.cpp -largeArrayDims -outdir bin/test/

profile_b2s:
	/sware/com/matlab_2016a/bin/mex -v -client engine $(LIBS) $(LDIR) $(IDIR) $(DEFINES) CXXOPTIMFLAGS="${CXXFLAGS}" zUtil_Cxx/test/gtTestCxx6DBlobsToMultipleSinos.cpp -largeArrayDims -outdir bin/test/
	/sware/com/matlab_2016a/bin/mex -v -client engine $(LIBS) $(LDIR) $(IDIR) $(DEFINES) CXXOPTIMFLAGS="${CXXFLAGS}" zUtil_Cxx/test/gtTestCxx6DMultipleSinosToBlobs.cpp -largeArrayDims -outdir bin/test/

profile: profile_reduction profile_b2s profile_cell_plus

#all: DctData.o DctMatlabData.o
all: profile

clean:
	rm bin/test/*.o
