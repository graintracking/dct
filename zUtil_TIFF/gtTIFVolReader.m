function [vol, info] = gtTIFVolReader(filename, varargin)
% GTTIFVOLREADER  Read TIFF volume (stack) and store it into a matrix [X,Y,Z].
%
%     [vol, info] = gtTIFVolReader(filename, varargin)
%     -------------------------------------------------------------------------
%
%     INPUT:
%       filename     = <string>  Path to TIFF file to write
%                                it may be relative or absolute
%                                if extension is missing, '.tif' is append
%
%     OPTIONAL INPUT (varargin as a list of pairs, see parse_pv_pairs.m):
%       'xrange'     = <range>   Index range of output in X direction {'all'}
%       'yrange'     = <range>               "            Y     "     {'all'}
%       'zrange'     = <range>               "            Z     "     {'all'}
%       'bbox'       = <int>     Index range as bbox [Xi Yi Zi Xf Yf Zf]
%       'mode'       = <string>  Input type which is {'single'} or 'stack'
%       'digits'     = <int>     Number of digits in stack filenames {4}
%       'startindex' = <int>     Starting index for stack filenames {1}
%       'endindex'   = <int>     Ending             "               {1}
%       'filext'     = <string>  Input TIFF file extension {'tif'}
%       'newMethod'  = <bool>    Use new reading method (faster) {'true'}
%
%     OUTPUT:
%       vol          = <3Dimage> Output 3D volume
%       info         = <struct>  Information about volume
%
%     Version 003 30-10-2012 by YGuilhem, yoann.guilhem@esrf.fr
%       Use tifflib for fast reading
%       Some bugfixes
%
%     Version 002 04-09-2012 by YGuilhem, yoann.guilhem@esrf.fr
%       Add support for multi TIFF files stack reading
%
%     Version 001 15-02-2012 by YGuilhem, yoann.guilhem@esrf.fr

% Set default parameters and parse optional arguments
params.newMethod = false;
params.xrange = [];
params.yrange = [];
params.zrange = [];
params.bbox = [];
params.mode = 'single';
params.filext = 'tif';
params.digits = 4;
params.startindex = 1;
params.endindex = 1;
params = parse_pv_pairs(params, varargin);

% Check if tifflib is present, if not -> old reading method
if params.newMethod
    if ~verLessThan('matlab', '8.1')
        disp('Since matlab2013, the newMethod is slower, so won''t use it...');
        params.newMethod = false;
    elseif ~exist('tifflib', 'file')
        disp('Cannot use new TIFF reading method because ''tifflib'' not found!');
        params.newMethod = false;
    end
end

% Check if single file or stack of files
if strcmp(params.mode, 'single')
    params.singlemode = true;
elseif strcmp(params.mode, 'stack')
    params.singlemode = false;
else
    gtError('gtTIFVolReader:wrong_input_mode', ...
        ['Unkown input mode: ''' params.mode  '''']);
end

if ~exist('filename', 'var')
    if params.singlemode
        [fname, fpath] = uigetfile('*', 'Select a .tif volume file');
        filename = fullfile(fpath, fname);
    else
        gtError('gtTIFVolReader:missing_input_filename', ...
            'The name of the input file is missing!');
    end
end

% Get file parts and store the proper extension
[fpath, fname, fext] = fileparts(filename);
if any(strcmp(fext, {'.tif', '.tiff'}))
    params.filext = fext(2:end);
end

if params.singlemode
    filenameFormat = sprintf('%s.%s', fname, params.filext);
    fullFilenameFormat = fullfile(fpath, filenameFormat);

    % Check existence of file
    if ~exist(fullFilenameFormat, 'file')
        gtError('gtTIFVolReader:wrong_file_name', ...
            ['File ''' fullFilenameFormat ''' does not exist!']);
    end
else
    filenameFormat = sprintf('%s%%0%dd.%s', fname, params.digits, params.filext);
    fullFilenameFormat = fullfile(fpath, filenameFormat);

    % Check existence of input stack files
    for idx = params.startindex:params.endindex
        sfilename = sprintf(fullFilenameFormat, idx);
        if ~exist(sfilename, 'file')
            gtError('gtTIFVolReader:wrong_file_name', ...
                ['File ''' sfilename ''' does not exist!']);
        end
    end
end

% Get information about each slice
infoFilename = sprintf(fullFilenameFormat, params.startindex);
try
    tifInfo = imfinfo(infoFilename);
catch Mexc
    Mexc1 = MException('gtTIFVolReader:wrong_input', ...
        ['Unable to read file ''' infoFilename '''']);
    Mexc1 = addCause(Mexc1, Mexc);
    throw(Mexc1);
end

% Cannot read RGB TIFF files right now
if length(tifInfo(1).BitsPerSample) > 1
    gtError('gtTIFVolReader:unsupported_input', ...
        'Cannot handle RGB TIFF files yet!');
end

fullFilename = tifInfo(1).Filename;
if params.singlemode
    fileIndex = ones(1, numel(tifInfo));
else
    fullFilename = regexprep(fullFilename, ['[0-9]{' num2str(params.digits) '}.tif$'], '');
    fileIndex = params.startindex:params.endindex;
end
tifSizeX = tifInfo(1).Width;
tifSizeY = tifInfo(1).Height;
tifSizeZ = length(fileIndex);

% Update file parts and full file format
[fpath, fname, ~] = fileparts(fullFilename);
fullFilenameFormat = fullfile(fpath, filenameFormat);

% Check if we use ranges or bbox input
if isempty(params.bbox)
    if isempty(params.xrange), params.xrange = 'all'; end;
    if isempty(params.yrange), params.yrange = 'all'; end;
    if isempty(params.zrange), params.zrange = 'all'; end;
elseif isempty([params.xrange params.yrange params.zrange])
    params.xrange = params.bbox(1):params.bbox(4);
    params.yrange = params.bbox(2):params.bbox(5);
    params.zrange = params.bbox(3):params.bbox(6);
else
    gtError('gtTIFVolReader:wrong_range_input', ...
        'You cannot use both bbox and [xyz]range options!');
end

% Check each dimension range
fullRangeXY = true;
if strcmp(params.xrange, 'all')
    rangeX = 1:tifSizeX;
else
    rangeX = params.xrange;
    if rangeX(1) ~= 1 || rangeX(end) ~= tifSizeX || length(rangeX) ~= tifSizeX
        fullRangeXY = false;
    end
end
if strcmp(params.yrange, 'all')
    rangeY = 1:tifSizeY;
else
    rangeY = params.yrange;
    if rangeY(1) ~= 1 || rangeY(end) ~= tifSizeY || length(rangeY) ~= tifSizeY
        fullRangeXY = false;
    end
end
if strcmp(params.zrange, 'all')
    rangeZ = 1:tifSizeZ;
else
    rangeZ = params.zrange;
end
if ~params.singlemode
    rangeZ = ones(1, tifSizeZ);
end

volSizeX = length(rangeX);
volSizeY = length(rangeY);
volSizeZ = length(rangeZ);

switch tifInfo(1).BitDepth
    case 1
        ftype = 'logical';
    case 8
        ftype = 'uint8';
    case 12
        ftype = 'uint16';
    case 16
        ftype = 'uint16';
    otherwise
        ftype = '';
end

% Same info as HST volumes
info.volSizeX = volSizeX;
info.volSizeY = volSizeY;
info.volSizeZ = volSizeZ;
info.tifSizeX = tifSizeX;
info.tifSizeY = tifSizeY;
info.tifSizeZ = tifSizeZ;
info.bytespervoxel = tifInfo(1).SamplesPerPixel * tifInfo(1).BitsPerSample / 8;
info.fpath = fpath;
info.byteorder = tifInfo(1).ByteOrder(1);
info.ftype = ftype;
info.fname_prefix = fname;
info.fname_final = fullFilename;

% TIFF specific info
info.BitDepth     = tifInfo(1).BitDepth;
info.RowsPerStrip = tifInfo(1).RowsPerStrip;

% Set slice container
if isempty(ftype)
    vol = zeros(volSizeX, volSizeY, volSizeZ);
    tmp_slice = zeros(volSizeX, volSizeY);
elseif strcmp(ftype, 'logical')
    vol = false(volSizeX, volSizeY, volSizeZ);
    tmp_slice = false(volSizeX, volSizeY);
else
    vol = zeros(volSizeX, volSizeY, volSizeZ, ftype);
    tmp_slice = zeros(volSizeX, volSizeY, ftype);
end

% If new reading method and PhotometricInterpretation set to WhiteIsZero
% Can be complicated

metric = tifInfo(1).PhotometricInterpretation;
if params.newMethod && any(strcmp(metric, {'WhiteIsZero', 'MinIsWhite'}))
    disp(['PhotometricInterpretation set to ''' metric ''', so using old reading method...']);
    params.newMethod = false;
end

if params.newMethod
    inputFilename = sprintf(fullFilenameFormat);
    if fullRangeXY
        getSlice = @(fid, slice, z)sfReadTIFSlice(fid, slice, z, info.RowsPerStrip);
    else
        getSlice = @(fid, slice, z)sfReadTIFSliceROI(fid, slice, z, info.RowsPerStrip);
    end
    if params.singlemode
        fid = tifflib('open', inputFilename, 'r');
        for idx = 1:volSizeZ
            vol(:, :, idx) = getSlice(fid, tmp_slice, rangeZ(idx));
        end
        tifflib('close', fid);
    else
        for idx = 1:volSizeZ
            inputFilename = sprintf(fullFilenameFormat, fileIndex(idx));
            fid = tifflib('open', inputFilename, 'r');
            vol(:, :, idx) = getSlice(fid, tmp_slice, 1);
            tifflib('close', fid);
        end
    end
else
    rangeX_no_offset = rangeX - rangeX(1) + 1;
    rangeY_no_offset = rangeY - rangeY(1) + 1;
    for idx = 1:volSizeZ
        inputFilename = sprintf(fullFilenameFormat, fileIndex(idx));
        if fullRangeXY
            vol(:, :, idx) = imread(inputFilename, rangeZ(idx), 'Info', tifInfo).';
        else
            tmp = imread(inputFilename, rangeZ(idx), 'PixelRegion', {[rangeY(1) rangeY(end)], [rangeX(1) rangeX(end)]});
            % This second method should be faster but in fact...
            %tmp = imread(inputFilename, rangeZ(i), 'Info', info, 'PixelRegion', {[rangeY(1) rangeY(end)], [rangeX(1) rangeX(end)]});
            vol(:, :, idx) = tmp(rangeY_no_offset, rangeX_no_offset).';
        end
    end
end

function x = sfReadTIFSlice(fid, x, sliceIndex, rowPerStrip)
    tifflib('setDirectory', fid, sliceIndex);
    rowPerStrip = min(rowPerStrip, volSizeY);
    for irow = 1:rowPerStrip:volSizeY
        indY = irow:min(volSizeY, irow+rowPerStrip-1);
        stripNum = tifflib('computeStrip', fid, irow);
        x(:, indY) = tifflib('readEncodedStrip', fid, stripNum).';
    end
end

function x = sfReadTIFSliceROI(fid, x, sliceIndex, rowPerStrip)
    tifflib('setDirectory', fid, sliceIndex);
    iy = 1;
    while iy <= volSizeY
        rowStart = iy + rangeY(1) - 1;
        modRowStart = mod(rowStart, rowPerStrip);
        stripNum = tifflib('computeStrip', fid, rowStart);
        tifStrip = tifflib('readEncodedStrip', fid, stripNum);

        rowEnd = min(stripNum*rowPerStrip, rangeY(end));
        nRows = rowEnd - rowStart;

        if modRowStart == 0 && nRows == 0
            modRowStart = rowPerStrip;
        end
        stripIndicesY = modRowStart:(modRowStart + nRows);
        volIndicesY   = iy:(iy + nRows);
        x(:, volIndicesY) = tifStrip(stripIndicesY, rangeX).';
        iy = iy + nRows + 1;
    end
end

end % end of function

