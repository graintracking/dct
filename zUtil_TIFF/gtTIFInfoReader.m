function info = gtTIFInfoReader(filename, info)
% GTTIFINFOREADER  Read TIFF info file (BBox, voxelsize and origin).
%
%     info = gtTIFInfoReader(filename, info)
%     -------------------------------------------------------------------------
%
%     INPUT:
%       filename     = <string>  Path to info file
%
%     OPTIONAL INPUT
%       info         = <struct>  TIFF info structure to complete
%
%     OUTPUT:
%       info         = <struct>  Information about volume
%
%     Note:
%       info file should be formatted as the following:
%
%       [bboxMinX, bboxMinY, bboxMinZ] [bboxMaxX, bboxMaxY, bboxMaxZ]
%       voxelsize
%
%     Version 001 24-04-2013 by YGuilhem, yoann.guilhem@esrf.fr

% Check existence of file
if ~exist(filename, 'file')
    gtError('gtTIFInfoReader:wrong_file_name', ...
        ['File ''' filename ''' does not exist!']);
end

% Read info file
fid = fopen(filename, 'r');
bbox = fscanf(fid, '[%d, %d, %d] [%d, %d, %d]', 6).';
vSize = fscanf(fid, '%f', 1);
fclose(fid);

% Store bbox, volume size, voxel size and origin
info.boundingBoxMin = bbox(1:3) + 1;
info.boundingBoxMax = bbox(4:6);
info.boundingBox = [info.boundingBoxMin info.boundingBoxMax]; 
info.volSize = bbox(4:6) - bbox(1:3);
info.voxelSize = vSize;
info.origin = bbox(1:3) .* info.voxelSize;
info.infoFile = filename;

end % end of function

