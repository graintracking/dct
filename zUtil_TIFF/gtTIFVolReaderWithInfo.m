function [vol, info] = gtTIFVolReaderWithInfo(filename, varargin)
% GTTIFVOLREADERWITHINFO  Read TIFF volume with the info (BBox and voxelsize).
%
%     [vol, info] = gtTIFVolReaderWithInfo(filename, varargin)
%     -------------------------------------------------------------------------
%
%     INPUT:
%       filename     = <string>  Path to TIFF file to write
%                                it may be relative or absolute
%                                if extension is missing, '.tif' is append
%
%     OPTIONAL INPUT -> see gtTIFVolReader
%
%     OUTPUT:
%       vol        = <3Dimage> Output 3D volume
%       info       = <struct>  Information about volume
%
%     Note:
%       info file should be formatted as the following:
%
%       [bboxMinX, bboxMinY, bboxMinZ] [bboxMaxX, bboxMaxY, bboxMaxZ]
%       voxelsize
%
%     Version 001 24-04-2013 by YGuilhem, yoann.guilhem@esrf.fr

% Read TIFF file
if isempty(varargin)
    [vol, info] = gtTIFVolReader(filename);
else
    [vol, info] = gtTIFVolReader(filename, varargin{:});
end

% Extract path, basename and extension from file name
[fpath, fname, fext] = fileparts(info.fname_final);

% Read info from file
infoFile = fullfile(fpath, [fname '.info']);
info = gtTIFInfoReader(infoFile, info);

end % end of function

