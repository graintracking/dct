function uvw = gtFedPredictUVWMultiple(Rottens, dlab, csam, detrefpos, ...
                                       detnorm, Qdet, detrefuv, om, omstep)
% GTFEDPREDICTUVWMULTIPLE Predicts (u,v,w) peak positions on the detector
% plane. Vectorised for speed.
% 
% uv = gtFedPredictUVWMultiple(Rottens, dlab, csam, detrefpos, detnorm, ...
%      Qdet, detrefuv, om, omstep)
%
% -------------------------------------------------------------
%
% Predicts peak positions [u,v] on the detector plane.
% Note:
%   Diffraction vectors 'dlab' don't have to be normalised.
%   'csam','detrefpos','detrefuv' must be in identical units.
%   Qdet is in units that scales lab units into detector pixels.
%   'om' is in degrees; 'omstep' is degrees/image
%
% INPUT
%   Rottens - rotation matrices that transform the Sample coordinates 'csam'
%             into Lab coordinates (3x3xn or 3x(3xn); for column vectors);
%             if empty, no rotation will be applied
%   dlab    - diffraction vectors (diffracted beam direction) in Lab
%             reference (3xn)
%   csam    - positions in Sample reference (or in Lab if Rottens is empty)
%             to be projected along 'dlab'; i.e. usually the grain centers;
%             (3xn)
%   detrefpos - any point in the detector plane (3x1)
%   detnorm   - detector plane normal pointing towards beam (3x1; unit vector)
%   Qdet      - detector projection matrix (2x3)
%   detrefuv  - detector reference position (u,v) (2x1)
%   om        - omega positions in degrees (1xn)
%   omstep    - omega step size in degrees/image
%
% OUTPUT
%   uvw      - (u,v,w) positions on the detector plane;
%              [pixel, pixel, image no.] (3xn);

uv = gtFedPredictUVMultiple(Rottens, dlab, csam, detrefpos, detnorm, ...
                            Qdet, detrefuv);

uvw = [ uv; mod(om,360)/omstep ];

end 