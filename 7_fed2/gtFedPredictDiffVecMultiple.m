function dlab = gtFedPredictDiffVecMultiple(pllab, beamdir)
% GTFEDPREDICTDIFFVECMULTIPLE Diffraction vectors from plane normals and
% beam directions in the Lab reference. Vectorised for speed.
%
% dlab = gtFedPredictDiffVecMultiple(pllab, beamdir)
%
% --------------------------------------------------------------
%
% Returns the diffraction vectors for a set of plane normals and beam
% directions. All given in the Lab reference (i.e. turned into the omega
% rotational position). Output 'dlab' is NOT normalised!
%
% INPUT
%   pllab   - normalised plane normals (3xn)
%   beamdir - normalised beam direction(s) (3x1) or (3xn)
%
% OUTPUT
%   dlab    - diffraction vectors (diffracted beam directions);
%             NOT normalised! (3xn)

    if (all([size(beamdir, 1), size(beamdir, 2)] == [3, 1]))
        % Single beamdir vector
        dotpro = 2 * beamdir' * pllab;
        dlab(3,:) = beamdir(3) - dotpro .* pllab(3,:);
        dlab(2,:) = beamdir(2) - dotpro .* pllab(2,:);
        dlab(1,:) = beamdir(1) - dotpro .* pllab(1,:);        
    else
        % Multiple beamdir vectors
        dotpro = 2 * sum(beamdir .* pllab, 1);
        dlab   = beamdir - bsxfun(@times, dotpro, pllab);
    end
    
end
