function DefT = gtFedDefTensorFromComps(dcin, dcomps, method, eyemode)
% Gives deformation, rotation and strain tensors from the def. components.
% This function contains the definition (including sign) of the deformation
% components. Takes multiple input dc-s.
%
% SAMPLE coordinate system is orthogonal, right handed [X,Y,Z].
%
% INPUT
%  dcin    - deformation components; see below
%  dcomps  - true for indices of deformation components present in dcin
%            (logical 1x9)
%  method  - 'small' or 'rod_rightstretch'
%  eyemode - form of deformation gradient (see in output)
%
% OUTPUT
%  DefT   - deformation gradient tensor
%             if eyemode=0, it excludes the identity matrix
%             if eyemode=1, it includes the identity matrix
%
% SMALL DEFORMATIONS
%
% Deformation components are defined as:
%   dc(1) = r32, right-handed rotation around X axis
%   dc(2) = r13, right-handed rotation around Y axis
%   dc(3) = r21, right-handed rotation around Z axis
%   dc(4) = e11, normal strain component along X axis
%   dc(5) = e22, normal strain component along Y axis
%   dc(6) = e33, normal strain component along Z axis
%   dc(7) = e23, shear strain component in YZ plane
%   dc(8) = e13, shear strain component in XZ plane
%   dc(9) = e12, shear strain component in XY plane
%
%   Signs for a right-handed rotation around X,Y,Z:
%      +X -> +Y
%      +Y -> +Z
%      +Z -> +X
%
%      RotT = [ 0    -r21  +r13; ...
%              +r21   0    -r23; ...
%              -r31  +r32   0  ];
%
%   ROTATION MATRIX
%      RotT = [ 0      -dc(3)  +dc(2); ...
%              +dc(3)   0      -dc(1); ...
%              -dc(2)  +dc(1)   0    ];
%
%   STRAIN TENSOR
%      StrainT = [dc(4)  dc(9)  dc(8); ...
%                 dc(9)  dc(5)  dc(7); ...
%                 dc(8)  dc(7)  dc(6)];
%
% LARGE DEFORMATIONS (FINITE DEFORMATION THEORY)
%   dc(1:3) - orientation dviation expressed as a Rodrigues vector
%   dc(4:9) - right stretch tensor components (symmetric)
%
%    DefT = RotT * RightStretchT
%

    if (~islogical(dcomps))
        error('dcomps needs to be 1x9 vector of logicals.')
    end

    % Create dc with 9 components
    dc = zeros(9, size(dcin, 2));
    dc(dcomps, :) = dcin;

    if strcmp(method, 'small')
        if (eyemode)
            DefT = [dc(4,:) + 1     ;...
                    dc(9,:)+dc(3,:) ;...
                    dc(8,:)-dc(2,:) ;...
                    dc(9,:)-dc(3,:) ;...
                    dc(5,:) + 1     ;...
                    dc(7,:)+dc(1,:) ;...
                    dc(8,:)+dc(2,:) ;...
                    dc(7,:)-dc(1,:) ;...
                    dc(6,:) + 1     ];
        else
            DefT = [dc(4,:)         ;...
                    dc(9,:)+dc(3,:) ;...
                    dc(8,:)-dc(2,:) ;...
                    dc(9,:)-dc(3,:) ;...
                    dc(5,:)         ;...
                    dc(7,:)+dc(1,:) ;...
                    dc(8,:)+dc(2,:) ;...
                    dc(7,:)-dc(1,:) ;...
                    dc(6,:)         ];
        end
        DefT = reshape(DefT,3,3,[]);

    elseif strcmp(method, 'rod_rightstretch')
        DefT = zeros(size(dc));

        % Rotation tensors unfolded
        RotT = gtMathsRod2OriMat(-dc(1:3, :));
        RotT = reshape(RotT, 9, []);

        % Multiply rotation tensors and right-stretch tensors
        rr = RotT .* dc([4 4 4 9 9 9 8 8 8], :);
        DefT(1:3, :) = rr(1:3, :) + rr(4:6,:) + rr(7:9,:);

        rr = RotT .* dc([9 9 9 5 5 5 7 7 7], :);
        DefT(4:6, :) = rr(1:3, :) + rr(4:6,:) + rr(7:9,:);

        rr = RotT .* dc([8 8 8 7 7 7 6 6 6], :);
        DefT(7:9, :) = rr(1:3, :) + rr(4:6, :) + rr(7:9, :);

        % Add the identity component in the right-stretch tensor
        DefT = DefT + RotT;


        DefT = reshape(DefT, 3, 3, []);

        if (~eyemode)
            DefT(1, 1, :) = DefT(1, 1, :) - 1;
            DefT(2, 2, :) = DefT(2, 2, :) - 1;
            DefT(3, 3, :) = DefT(3, 3, :) - 1;
        end
    end
end
