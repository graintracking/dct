function active_vols = gtFedActiveVolumeVoxels(phase_vol, parameters_inline, varargin)
% GTFEDACTIVEVOLUMEVOXELS    Backprojects the absorption
% image from a mask on the sample volume, obtaining the diffracting
% voxels for a given diffraction geometry.
%
% NOTE: This function was written for the Mg monocrystal. If you need to
% change it, for another dataset, please make it ust more customizable!
%
% gtFedActiveVolumeVoxels(phase_vol)
% ---------------------------------------------------------------------
%
% INPUT
%   phase_vol         : assembled phase volume reconstructed with 6D
%                       algorithm
%   parameters_inline : parameters for an in-line acquisition
%
% OUTPUT
%   active_vol        : three volumes with modulated intensities
%                       according to scattering powers, one for each
%                       considered reflection
%
% OPTIONAL INPUT
%   mask              : {'pencil'} | 'line'
%   det_index         : {1} The index of the detector used for the
%                       back-projection of the mask
%   vert_slice        : {[]}, and empty field selects the central slice
%   pencil_spacing    : {0.05}, determines the spacing between each window
%                       and it is in microns

    if (~exist('parameters_inline', 'var') || isempty(parameters_inline))
        parameters_inline = gtLoadParameters();
    end

    conf = struct(...
        'mask', 'pencil', ...
        'oversampling', 3, ...
        'det_index', 1, ...
        'vert_slice', [], ...
        'pencil_shifts', [], ...
        'pencil_width', [], ...
        'pencil_spacing', 0.050 );
    conf = parse_pv_pairs(conf, varargin);

    detgeo = parameters_inline.detgeo(conf.det_index);
    labgeo = parameters_inline.labgeo;
    samgeo = parameters_inline.samgeo;
    recgeo = parameters_inline.recgeo(conf.det_index);
    acq = parameters_inline.acq(conf.det_index);

    vol_size_xyz = size(phase_vol.intvol);

    if (isempty(conf.vert_slice))
        conf.vert_slice = round(vol_size_xyz(2) / 2);
    end

    if (isempty(conf.pencil_shifts))
        conf.pencil_shifts = 1;
    end
    if (isempty(conf.pencil_width)) % UNUSED at the moment
        conf.pencil_width = recgeo.voxsize(3);
    end

    num_shifts = numel(conf.pencil_shifts);

    % Back-projection of absorption image into sample volume
    pmos = [-88.2 91.8 -26.2];
    omegas = pmos + 90;
    num_rots = numel(pmos);

    active_vols = cell(num_shifts, num_rots);

    % Manual correction of intensity volume
    phase_vol_corr = single(phase_vol.intvol);

    proj_geoms = gtGeoProjForReconstruction([], omegas, [], acq.bb, ...
        zeros(num_rots, 2), detgeo, labgeo, samgeo, recgeo, ...
        'ASTRA_absorption');

    for ii_s = 1:num_shifts
        fprintf('Creating a %s mask for shift %f..', conf.mask, conf.pencil_shifts(ii_s))
        c = tic();
        switch(conf.mask)
            case 'pencil'
                % Creation of the pencil mask: 7 um windows with 50 um period,
                % 1 vx vertical thickness
                vox_size = recgeo.voxsize(3);
                coords(2, :) = ( 0:conf.pencil_spacing:((vol_size_xyz(3)-1) * vox_size) ) / vox_size + conf.pencil_shifts(ii_s);
                coords(1, :) = conf.vert_slice;
                [inds, ints] = gtMathsGetInterpolationIndices(coords');

                good_inds = (ints > eps('single')) ...
                    & all(inds >= 1, 2) ...
                    & all(inds <= repmat(vol_size_xyz(2:3), [numel(ints), 1]), 2);
                inds = inds(good_inds, :);
                ints = ints(good_inds, :);

                det_mask = single(accumarray(inds, ints, vol_size_xyz(2:3)));
            case 'line'
                % Creation of the line mask: 1 vx vertical thickness
                [inds, ints] = gtMathsGetInterpolationIndices(conf.vert_slice);

                good_inds = (ints > eps('single')) ...
                    & (inds >= 1) ...
                    & (inds <= vol_size_xyz(3));
                inds = inds(good_inds);
                ints = ints(good_inds);

                det_mask = zeros(vol_size_xyz(2:3), 'single');
                for ii_i = 1:numel(inds)
                    det_mask(inds(ii_i), :) = ints(ii_i);
                end
        end
        fprintf('\b\b: Done in %g seconds.\n', toc(c))

        for ii_r = 1:num_rots
            fprintf('Backprojecting, angle = %g, pmo = %g..', ...
                omegas(ii_r), pmos(ii_r))
            c = tic();

            proj_stack(:, 1, :) = det_mask;
            vol = gtAstraBackproject(proj_stack, proj_geoms(ii_r, :), vol_size_xyz, conf.oversampling);

            active_vols{ii_s, ii_r} = vol .* phase_vol_corr;
            fprintf('\b\b: Done in %g seconds.\n', toc(c))
        end
    end
end

