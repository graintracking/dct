function [gvb, bl] = gtFedFwdProjExact(gv, gvb, bl, fedpars, parameters, det_num, verbose)

    if (~exist('det_num', 'var'))
        det_num = 1;
    end
    if (~exist('verbose', 'var'))
        verbose = true;
    end

    o = GtConditionalOutput(verbose);

    o.fprintf('Forward projection (%s):\n', mfilename)

    nbl = length(bl);
    uinds = gv.used_ind;
    nv = numel(uinds);
    ones_bl = ones(nbl, 1);

    detgeo = parameters.detgeo(det_num);
    labgeo = parameters.labgeo;
    samgeo = parameters.samgeo;

    dcomps = fedpars.dcomps;
    defmethod = fedpars.defmethod;

    plorig = vertcat(bl(:).plorig);
    bbor   = vertcat(bl(:).bbor);
    bbsize = vertcat(bl(:).bbsize);

    % The plane normals need to be brought in the Lab reference where the
    % beam direction and rotation axis are defined.
    % Use the Sample -> Lab orientation transformation assuming omega=0;
    % (vector length preserved for free vectors)
    plorig = gtGeoSam2Lab(plorig, eye(3), labgeo, samgeo, true);

    if (isfield(fedpars, 'detector') ...
            && isfield(fedpars.detector, 'psf') ...
            && ~isempty(fedpars.detector(det_num).psf))
        psf = fedpars.detector(det_num).psf;
        if (~iscell(psf))
            psf = { psf };
        end
        if (numel(psf) == 1)
            psf = psf(ones_bl);
        end
    else
        psf = {};
    end

    if (isfield(fedpars, 'detector') ...
            && isfield(fedpars.detector, 'apply_uv_shift') ...
            && ~isempty(fedpars.detector(det_num).apply_uv_shift) ...
            && fedpars.detector(det_num).apply_uv_shift)
        apply_uv_shifts = true;
    else
        apply_uv_shifts = false;
    end

    num_oversampling = size(gv.d, 3);
    gvpow = gv.pow(1, uinds);

    for ii_ss = 1:num_oversampling
        gvcs = gv.cs(:, uinds, ii_ss);

        gvd = gv.d(:, uinds, ii_ss);

        % Deformation tensor (relative to reference state) from its components
        defT = gtFedDefTensorFromComps(gvd, dcomps, defmethod, 0);
        uvw = cell(1, nbl);

        %%% Computation of indices
        o.fprintf(' - Super sampling %03d/%03d:\n   * Computing indices and bbsizes: ', ...
            ii_ss, num_oversampling)
        t = tic();

        for ii = 1:nbl
            num_chars = o.fprintf('%03d/%03d', ii, nbl);
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % Calculate new detector coordinates
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            % New deformed plane normals and relative elongations (relative to
            % reference state)
            plsam = plorig(ii, :)';  % ! use plorig and not plsam to keep omega order below!

            [ucbl, ~] = predict_uvw(plsam, gvcs, defT, bl(ii), labgeo, samgeo, detgeo);

            if (apply_uv_shifts)
                ucbl(1:2, :) = ucbl(1:2, :) - bl(ii).uv_shift(ones(1, nv), :)';
            end

            % Detector coordinates U,V in blob
            bbor_t = bbor(ii, :)';
            ucbl = ucbl - bbor_t(:, ones(1, nv));

            % U,V shift relative to reference state
            gvb(ii).ucd = ucbl - gvb(ii).uc0bl - gvb(ii).uc;
            gvb(ii).uc  = ucbl - gvb(ii).uc0bl;

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % Sum intensities
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % Distribute value over 8 pixels

            % Modulus of uvw position
            gvb(ii).ucblmod = mod(ucbl, 1);

            % This way of rounding is correct also for round values when mu=mod...
            % is used above:
            ul = floor(ucbl); % lower pixel indices
            uh = ul + 1;      % higher pixel indices

            % uvw indices of 8 nearest neighbours (3xnx8)
            ucbl8 = [ ul, ...
                     [ul([1,2],:); uh(3,:)], ...
                     [ul(1,:); uh(2,:); ul(3,:)], ...
                     [ul(1,:); uh([2,3],:)], ...
                     [uh(1,:); ul([2,3],:)], ...
                     [uh(1,:); ul(2,:); uh(3,:)], ...
                     [uh([1,2],:); ul(3,:)], ...
                     uh ];
            % uvw indices of 8 nearest neighbours (nx3)
            uvw{ii} = ucbl8';

            % Don't use sub2ind, it's slow
            gvb(ii).ucbl8 = ucbl8(1, :) ...
                + bbsize(ii, 1) .* (ucbl8(2, :)-1) ...
                + bbsize(ii, 1) .* bbsize(ii, 2) .* (ucbl8(3, :)-1);

            o.fprintf(repmat('\b', [1, num_chars]));
        end
        o.fprintf('Done in %g s\n', toc(t));

        %%% Verification of indices
        o.fprintf('   * Computing max BBox size and feasibilities:\n')
        max_bbox_proj_size = [inf, inf, inf; 0, 0, 0];
        max_bbsize_blob = [1, 1, 1; 0, 0, 0];
        psf_size = zeros(nbl, 2);
        for ii = 1:nbl
            bbsize_blob = bbsize(ii,:);
            min_ind_blob = min(uvw{ii}, [], 1);
            max_ind_blob = max(uvw{ii}, [], 1);

            max_bbsize_blob(2, :) = max(max_bbsize_blob(2, :), bbsize_blob);

            if (~isempty(psf))
                psf_size(ii, :) = (size(psf{ii}) - 1) / 2;
            end

            max_bbox_proj_size(1, :) = min(max_bbox_proj_size(1, :), min_ind_blob);
            max_bbox_proj_size(2, :) = max(max_bbox_proj_size(2, :), max_ind_blob);

            if (any(min_ind_blob < 1) || any(max_ind_blob > bbsize_blob))
                fprintf('     + Blob: %d, Blobsize: (%d, %d, %d), bbox projection [(%d, %d, %d), (%d, %d, %d)]\n', ...
                    ii, bbsize_blob, min_ind_blob, max_ind_blob);
            elseif (any(min_ind_blob(1:2) < psf_size(ii, :)) || any(max_ind_blob(1:2) > (bbsize_blob(1:2) - psf_size(ii, :))))
                fprintf('     + Blob: %d, Blobsize: (%d, %d), bbox projection [(%d, %d), (%d, %d)], psf (%d, %d)\n', ...
                    ii, bbsize_blob(1:2), min_ind_blob(1:2), max_ind_blob(1:2), psf_size(ii, :));
            end
        end
        o.fprintf('     Maximum BBox size: [(%d, %d, %d), (%d, %d, %d)]\n', max_bbox_proj_size(1, :), max_bbox_proj_size(2, :));
        o.fprintf('     Maximum Blob size: [(%d, %d, %d), (%d, %d, %d)]\n', max_bbsize_blob(1, :), max_bbsize_blob(2, :));

        if (isfield(fedpars, 'detector'))
            blob_size_add = fedpars.detector(det_num).blobsizeadd;
        else
            blob_size_add = fedpars.blobsizeadd(det_num, :);
        end

        psf_size(:, 3) = 0;
        minimum_blob_size_add = blob_size_add + max( [ ...
            max_bbsize_blob(1 * ones_bl, :) - max_bbox_proj_size(1 * ones_bl, :) + psf_size; ...
            max_bbox_proj_size(2 * ones_bl, :) - max_bbsize_blob(2 * ones_bl, :) + psf_size ...
            ], [], 1);
        o.fprintf('     Current Blob size add: (%d, %d, %d) -> minimum suggested: (%d, %d, %d)\n', ...
            blob_size_add, minimum_blob_size_add);

        %%% Blob projection
        o.fprintf('   * Projecting volumes: ')
        t = tic();
        for ii = 1:nbl
            num_chars = o.fprintf('%03d/%03d', ii, nbl);

            ints = compute_projected_intensities(gvb(ii), gvpow, bl(ii), nv);

            % Accumulate all intensities in the blob voxel-wise
            %   'uvw' needs to be nx3; 'ints' is now 1xnx8
            try
                bl(ii).int = bl(ii).int + accumarray(uvw{ii}, ints, bbsize(ii,:));
            catch mexc
                disp(' ')
                disp('ERROR')
                disp(['The error is probably caused by the projected intensities' ...
                    ' falling outside the blob volume. Try increasing the blob' ...
                    ' volume padding: fedpars.detector(det_ind).blobsizeadd'])
                disp('Blob ID:')
                disp(ii)
                disp('Blob size:')
                disp(bbsize(ii,:))
                disp('Min projected U,V,W coordinates:')
                disp(min(uvw{ii},[],1))
                disp('Max projected U,V,W coordinates:')
                disp(max(uvw{ii},[],1))
                new_exc = GtFedExceptionFwdProj(mexc, det_num, minimum_blob_size_add);
                throw(new_exc)
            end

            o.fprintf(repmat('\b', [1, num_chars]));
        end

        o.fprintf('Done in %g s\n', toc(t));
    end

    if (num_oversampling > 1)
        o.fprintf(' - Renormalizing to initial intensity:');
        t = tic();

        for ii = 1:nbl
            num_chars = o.fprintf('%03d/%03d', ii, nbl);

            bl(ii).int = bl(ii).int / num_oversampling;

            o.fprintf(repmat('\b', [1, num_chars]));
        end

        o.fprintf('Done in %g s\n', toc(t));
    end

    if (~isempty(psf))
        o.fprintf(' - Applying PSF:', toc(t));
        t = tic();

        for ii = 1:nbl
            num_chars = o.fprintf('%03d/%03d', ii, nbl);

            bl(ii).int = convn(bl(ii).int, psf{ii}, 'same');

            o.fprintf(repmat('\b', [1, num_chars]));
        end

        o.fprintf('Done in %g s\n', toc(t));
    end
end

function [uvw_bl, rot_s2l] = predict_uvw(plsam, gvcs, defT, bl, labgeo, samgeo, detgeo)
%
    [pl_samd, drel] = gtStrainPlaneNormals(plsam, defT); % unit column vectors

    % New sin(theta)
    sinth = bl.sinth0 ./ drel;

    % Predict omega angles: 4 for each plane normal
    [om, pllab, ~, rot_l2s] = gtFedPredictOmegaMultiple(pl_samd, ...
        sinth, labgeo.beamdir', labgeo.rotdir', labgeo.rotcomp, bl.omind);

    % Delete those where no reflection occurs
    if (any(isnan(om)))
        inds_bad = find(isnan(om));
        gvd(:, inds_bad(1:10))
        error('gtFedFwdProjExact:bad_R_vectors', ...
            'No diffraction from some elements after deformation (%d over %d).', ...
            numel(inds_bad), numel(om))
    end

    % Diffraction vector
    dvec_lab = gtFedPredictDiffVecMultiple(pllab, labgeo.beamdir');

    rot_s2l = permute(rot_l2s, [2 1 3]);
    gvcs_lab = gtGeoSam2Lab(gvcs', rot_s2l, labgeo, samgeo, false);

    uvw_bl = gtFedPredictUVWMultiple([], dvec_lab, gvcs_lab', ...
        detgeo.detrefpos', detgeo.detnorm', detgeo.Qdet, ...
        [detgeo.detrefu, detgeo.detrefv]', om, labgeo.omstep);
end

function ints = compute_projected_intensities(gvb, gvpow, bl, nv)
%
    mu = gvb.ucblmod;
    mu1 = mu(1, :);
    mu2 = mu(2, :);
    mu3 = mu(3, :);

    mu12  = mu1 .* mu2;
    mu13  = mu1 .* mu3;
    mu23  = mu2 .* mu3;
    mu123 = mu12 .* mu3;

    ints = zeros(1, numel(gvpow) * 8, 'like', bl.int);

    ints(     1 :   nv) = gvpow.*(-mu1 - mu2 - mu3 + mu12 + mu13 + mu23 - mu123 + 1);
    ints(  nv+1 : 2*nv) = gvpow.*(mu3 - mu13 - mu23 + mu123);
    ints(2*nv+1 : 3*nv) = gvpow.*(mu2 - mu12 - mu23 + mu123);
    ints(3*nv+1 : 4*nv) = gvpow.*(mu23 - mu123);
    ints(4*nv+1 : 5*nv) = gvpow.*(mu1 - mu12 - mu13 + mu123);
    ints(5*nv+1 : 6*nv) = gvpow.*(mu13 - mu123);
    ints(6*nv+1 : 7*nv) = gvpow.*(mu12 - mu123);
    ints(7*nv+1 : 8*nv) = gvpow.*mu123;
end
