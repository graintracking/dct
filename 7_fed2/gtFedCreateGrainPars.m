function [fedpars, dmvol, intvol] = gtFedCreateGrainPars(grain, grain_rec, parameters, varargin)
% [fedpars, dmvol, intvol] = gtFedCreateGrainPars(grain, grain_rec, parameters, varargin)

    conf = struct(...
        'is_real_data', [], ...
        'keep_ondet', true );
    conf = parse_pv_pairs(conf, varargin);
    
    %%%%%%%%%%%%%%%%%%%%%%%%%
    % Load inputs if needed
    %%%%%%%%%%%%%%%%%%%%%%%%%
    
    % Grain structure
    if (~exist('grain', 'var') || (isempty(grain)))
        disp('No grain structure found.');
        phase_id = input('Specify the phase ID: \n', 's');
        phase_id = str2double(phase_id);

        grain_id = input('Specify the grain ID: \n', 's');
        grain_id = str2double(grain_id);
        grain = gtLoadGrain(phase_id, grain_id);
    end

    if (~exist('grain_rec', 'var') || (isempty(grain_rec)))
        disp('No grain reconstruction details found.');
        phase_id = input('Specify the phase ID: \n', 's');
        phase_id = str2double(phase_id);

        grain_id = input('Specify the grain ID: \n', 's');
        grain_id = str2double(grain_id);
        grain_rec = gtLoadGrainRec(phase_id, grain_id);
    end

    % Parameters
    if (~exist('parameters', 'var') || isempty(parameters))
        fprintf('No parameters file specified. Loading it from %s', pwd)
        parameters = gtLoadParameters();
    end
    
    % Deformation volume
    dmvol = grain_rec.ODF6D.voxels_avg_R_vectors;

    % Intensities volume
    intvol = grain_rec.ODF6D.intensity;
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Crop intensities and deformation volumes to save memory
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    disp('- Cropping intensities and deformation volumes ...')
    c = tic();
    t = GtThreshold();
    if (isfield(grain_rec, 'SEG'))
        gr_seg = t.volumeThreshold(intvol, grain_rec.SEG.threshold, grain_rec.SEG.seed)
    else
        gr_seg = t.volumeThreshold(intvol, grain.threshold, grain.seed);
    end
    cropbb = gr_seg.segbb + [1 1 1 0 0 0];
    intvol = gtCrop(intvol, cropbb);

    intvol(~gr_seg.seg) = 0;

    dmvol_crop = zeros([size(intvol) 3]);
    dmvol_crop(:, :, :, 1) = gtCrop(dmvol(:, :, :, 1), cropbb); 
    dmvol_crop(:, :, :, 2) = gtCrop(dmvol(:, :, :, 2), cropbb);
    dmvol_crop(:, :, :, 3) = gtCrop(dmvol(:, :, :, 3), cropbb);
    dmvol = dmvol_crop;

    toc(c)

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Creation of actual fedpars structure
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    disp('- Creating fedpars structure ...')

    % x, y, z no. of voxels in the grain
    fedpars.grainsize                  = size(intvol);

    % x, y, z no. of voxels in a volume element
    fedpars.gvsize                     = fedpars.grainsize ./ size(intvol); 
%     gv_elem                        = numel(grain.vol);
%     sample_freq                    = 3e+6;                       % verify
%     gvsize                         = gv_elem / sample_freq;
%     fedpars.gvsize                 = [gvsize gvsize gvsize]; 

    % Active deformation components. Total number is 9.
    % In case of rotations only (should be our case):
    fedpars.dcomps                     = [true(1, 3) false(1, 6)];

    % Origin voxel in the SAM reference
    % Using original intensities volume size to retrieve the correct shift
    vc = gtGeoSam2Sam(size(grain_rec.ODF6D.intensity) / 2, parameters.recgeo, parameters.samgeo, false);
    vs = gtGeoSam2Sam(gr_seg.segbb(1:3), parameters.recgeo, parameters.samgeo, false);
    fedpars.vox000sam                  = grain.center - vc + vs;

    % Class of strain states
    fedpars.gvtype                     = class(intvol);

    % Real or simulated data?
    if (isempty(conf.is_real_data))
        is_real = inputwdefault('Are you working with real data? [y/n]', 'y');
        fedpars.is_real_data = ~strcmpi(is_real, 'n');
    else
        fedpars.is_real_data = conf.is_real_data;
    end

    % Generate random deformation components
    if (~exist('dmvol', 'var') || isempty(dmvol))

        % Type of deformation applied
        fedpars.deftype                = 'smooth';
        deftype = inputwdefault(['Specify the type of deformation ' ...
            'applied: smooth (s) or realistic (r):', 's']);

        if (strcomp(deftype, 'r'))
            fedpars.deftype            = 'realistic';
        end
        
        % Random strain definition mode
        fedpars.randdefmode            = 'smooth';
        randdefmode = input(['Specify the random strain definition mode: ' ...
            'smooth (s) or interpolated (i):'], 's');

        if (strcompi(randdefmode, 'i'))
            fedpars.randdefmode        = 'interp';
        end

        % Maximum absolute deformation gradient
        fedpars.dappgradlim            = 2e-4 * ones(size(fedpars.dcomps));    % guess: verify

        % Maximum absolute deformation value which can be randomly generated
        fedpars.dapplim                = 5e-3 * ones(size(fedpars.dcomps));    % guess: verify
    end

    % ID of blobs to load for figure
    fedpars.loadbl = [];

    % Detector parameters
    ndet = numel(parameters.detgeo);

    if (~conf.keep_ondet)
        grain_fwd = gtCalculateGrain(grain, parameters);
    end

    for ii_det = 1:ndet
        
        if (conf.keep_ondet)
            if (isfield(grain.proj, 'ondet') && (~isempty(grain.proj.ondet)))
                fedpars.detector(ii_det).ondet = grain.proj.ondet;
                fedpars.detector(ii_det).included = grain.proj.included;
                fedpars.detector(ii_det).selected = grain.proj.selected;
            else
                fedpars.detector(ii_det).ondet = grain.ondet;
                fedpars.detector(ii_det).included = grain.included;
                fedpars.detector(ii_det).selected = grain.selected;
            end
        else
            fedpars.detector(ii_det).ondet = find(grain_fwd.allblobs(ii_det).detector.ondet);
            fedpars.detector(ii_det).included = 1:numel(fedpars.detector(ii_det).ondet);
            fedpars.detector(ii_det).selected = true(size(fedpars.detector(ii_det).included));
        end

        % Increments in blobs bounding boxes accounting for deformation.
        % Let gtFedTestLoadData_v2 decide the minimum size
        fedpars.detector(ii_det).blobsizeadd = zeros(ndet, 3);
    end

    % Types for blobs intensities
    fedpars.bltype                     = 'single';

    % Deformation method for calculation of deformation tensor
    fedpars.defmethod                  = 'rod_rightstretch';
%     fedpars.defmethod                  = 'small';

    % Algorithm for blobs smoothing
    fedpars.intsmoothalg               = 'none';

    disp('Done')
end