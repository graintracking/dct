function intvol_out = gtFedCreatePencilMask(intvol, w, p, parameters)
% intvol_out = gtPencilMask(intvol, w, p, parameters)
%
% Apply a pencil beam mask with square holes width x width (in microns) 
% and pitch p (in microns) to an intensities volume reconstructed through
% a 6D algorithm.

    if (~exist('parameters', 'var') || isempty(parameters))
        fprintf('No parameters file specified. Loading it from %s\n', pwd)
        parameters = gtLoadParameters;
    end

    % Volume sizes in microns
    xyz = size(intvol);
    fprintf('Dimensions of intensities volume: %d %d %d\n', xyz(1), ...
        xyz(2), xyz(3))

    % Allocating space
    mask = zeros(size(intvol));

    w_vox = ceil((w / 1000) / parameters.recgeo.voxsize(2));
    p_vox = ceil((p / 1000) / parameters.recgeo.voxsize(2));
    fprintf(['%d central slices in the vertical direction are cut by '...
        'the mask\n'], w_vox)

    % Indices of vertical slices cut by the mask
    v_ind(2) = floor((median(1:xyz(2))) + ceil(w_vox / 2));
    v_ind(1) = v_ind(2) - w_vox + 1;

    % Transmission function of the mask. The mask is periodic along 
    % z-direction
    cycle = [ones(1, w_vox) zeros(1, p_vox-w_vox)];
    transmitted = repmat(cycle, [1, floor(xyz(3) / p_vox)]);
    left = xyz(3) - numel(transmitted);
    if (left <= w)
        transmitted(numel(transmitted) + 1:xyz(3)) = ...
               ones(1, xyz(3) - numel(transmitted));
    else
        transmitted(numel(transmitted) + 1:numel(transmitted) + w_vox) =...
               ones(1, w_vox);
        transmitted(numel(transmitted) + 1:xyz(3)) = ...
               zeros(1, xyz(3) - numel(transmitted));   
    end

    transmitted = transmitted(ones(1, w_vox), :, ones(1, xyz(1)));
    transmitted = permute(transmitted, [3 1 2]);
    mask(:, v_ind(1):v_ind(2), :) = transmitted;
    intvol_out = mask .* intvol;
    fprintf('Dimensions of masked intensities volume: %d %d %d\n', ...
        size(intvol_out, 1), size(intvol_out, 2), size(intvol_out, 3))
end