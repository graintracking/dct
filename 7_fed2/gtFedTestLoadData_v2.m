function [grain, gv, dmvol, fedpars, parameters] = gtFedTestLoadData_v2(fedpars, grain, parameters, dmvol, intvol)
% [grain, gv, dmvol, fedpars, parameters] = gtFedTestLoadData_v2(fedpars, grain, parameters, dmvol, intvol)
%
% Sets up the framework, variables, random strain distribution for simulated data.
%  In the grain volume everything is defined in voxels, no dimensioning with pixel size.
%  In the projections and diff. blobs, everything is defined in detector pixels
%  (here comes the pixel size and setup geometry into play).
%    Thus the dimensioning/scaling happens in the forward projection
%    functions and their derivative functions, since they relate the
%    grain voxels to the detector pixels.
%
% INPUT
%   fedpars    - FED parameters
%                Some important ones:
%                  grainsize   - no. of voxels in grain
%                  grainenv    = grainsize
%                  ngv         = grainsize
%                  gvsize      = [1 1 1]
%                  loadbl      - blobs ID-s in grain.allblobs to be generated
%                  dcomps      - for rotations only use [1 1 1 0 0 0 0 0 0]
%                  blobsizeadd - blob volume padding
%                  vox000sam   - (X, Y, Z) coordinates of voxel (0, 0, 0) in the Sample reference frame
%   grain      - grain data as output from Indexter
%   parameters - parameters as in DCT paramaters file
%   dmvol      - all deformation components per voxel for the entire volume
%                (nx, ny, nz, ndef); if empty, random distribution is applied
%                with max limits as in fedpars.dapplim and max gradients
%                as in fedpars.dappgradlim
%   intvol     - Volume instensities

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Load and update geometry parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fprintf('Setting voxels up..')
c = tic();

labgeo = parameters.labgeo;
labgeo.omstep = gtAcqGetOmegaStep(parameters);
% Rotation matrix components:
labgeo.rotcomp = gtMathsRotationMatrixComp(labgeo.rotdir', 'col');

samgeo = parameters.samgeo;
recgeo = parameters.recgeo;
if (~isfield(parameters, 'detgeo') || isempty(parameters.detgeo))
    [parameters.detgeo, labgeo] = gtGeoConvertLegacyLabgeo2Detgeo(labgeo);
end
detgeo = parameters.detgeo;
% Update labgeo
parameters.labgeo = labgeo;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Load grain parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

grsize = fedpars.grainsize; % x, y, z number of voxels in grain
gvsize = fedpars.gvsize;    % x, y, z number of voxels in an element

% Grain volume
grvol = gtFedTestGenerateGrainVolume(grsize);
fedpars.grainvol = grvol;

% Determine number of volume elements along X, Y, Z, based on grain volume
% and element size:
[grenv(1), grenv(2), grenv(3)] = size(grvol);
ngv = ceil(grenv ./ gvsize);
fedpars.ngv = ngv;

% Size of grain envelope
grenv = ngv .* gvsize;
fedpars.grainenv = grenv;

% Padded grain volume
grvol_pad = false(grenv);
grvol_pad(1:size(grvol, 1), 1:size(grvol, 2), 1:size(grvol, 3)) = grvol;
grvol     = grvol_pad;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Volume sampling and volume elements
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fprintf('\b\b: Done in %g seconds.\nAllocating memory for elements..', toc(c))
c = tic();

nv    = ngv(1)*ngv(2)*ngv(3);          % total number of vol elements
svvol = gvsize(1)*gvsize(2)*gvsize(3); % size of sampling volume

nd = sum(fedpars.dcomps);

% Element XYZ index
[gv.ind(:, 1), gv.ind(:, 2), gv.ind(:, 3)] = ind2sub(ngv, 1:nv);
gv.ind = gv.ind';

% Element diffracting power
if (~exist('intvol', 'var') || isempty(intvol))
    gv.pow = ones(1, nv, fedpars.gvtype);
else
    if (numel(intvol) ~= nv)
        error('gtFedTestLoadData_v2:wrong_argument', ...
            'The volume of voxel intensities should have the same size as dmvol''s first 3 coordinates')
    end
    gv.pow = reshape(intvol, 1, []);
    if (strcmp(fedpars.gvtype, 'single'))
        gv.pow = single(gv.pow);
    end
end

gv.used_ind = find(gv.pow > 0);
nuv = numel(gv.used_ind);
ones_nuv = ones(1, nuv);

% Element actual strain state
gv.d = zeros(nd, nv, fedpars.gvtype);

% Element change in strain state
gv.dd = zeros(nd, nv, fedpars.gvtype);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Apply strain distribution to grain
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fprintf('\b\b: Done in %g seconds.\n', toc(c))

disp('Active deformation components:')
disp(fedpars.dcomps)

disp('Applying random deformation..')
c = tic();

% Measured (true, real) strain distribution:
if (~exist('dmvol', 'var') || isempty(dmvol))
    [gvdm, dmvol, gradok] = gtFedTestGenerateRandomDef(fedpars, nd);
else
    [gvdm, dmvol_size] = gtDefDmvol2Gvdm(dmvol);
    if (isfield(fedpars, 'is_real_data') && fedpars.is_real_data)
        gvdm = gvdm - grain.R_vector(ones(1, size(gvdm, 2)), :)';
    else
        if (dmvol_size(4) == 3)
            mean_gvdm = mean(gvdm, 2);
            fprintf('Recentering GVDM and DMVOL to the mean orientation: (%f, %f, %f), from: (%f, %f, %f).\n', ...
                grain.R_vector, mean_gvdm)
            gvdm_shift = grain.R_vector' - mean_gvdm;
            gvdm = gvdm + gvdm_shift(:, ones(size(gvdm, 2), 1));
            dmvol = gtDefGvdm2Dmvol(gvdm, dmvol_size);
        end
    end
    gradok = [];
end

gv.dm = gvdm;
gv.d = gv.dm;
if (strcmp(fedpars.gvtype, 'single'))
    fprintf('\b\b: Done in %g seconds.\nConverting type to single..', toc(c))
    c = tic();
    fn = fieldnames(gv);
    for ii = 1:length(fn)
        gv.(fn{ii}) = single(gv.(fn{ii}));
    end
end

fprintf('\b\b: Done in %g seconds.\nComputing voxel centers..', toc(c))
c = tic();

% Elements' positions in the sample volume in mm!
[gv.cs, gv.d] = compute_voxel_centers(gv, dmvol, fedpars, recgeo(1));

if (~(isfield(fedpars, 'is_real_data') && fedpars.is_real_data))
    % Grain center position in sample in mm!
    gc = fedpars.vox000sam + (grsize/2 + [0.5, 0.5, 0.5]) .* recgeo(1).voxsize;
    grain.center = gc;
    fedpars.graincentsam = gc;
else
    gc = grain.center;
end

fprintf('\b\b: Done in %g seconds.\n', toc(c))

if ~all(gradok)
    disp('Gradients are higher than limit for components:')
    disp(find(~gradok)')
end

disp('Largest deformation component:')
disp(max(dmvol(:)))
disp('Smallest deformation component:')
disp(min(dmvol(:)))

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Computing allblobs info
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Cleaning grain struct
if (isfield(grain, 'id'))
    grain = struct('id', grain.id, 'phaseid', grain.phaseid, ...
        'center', grain.center, 'R_vector', grain.R_vector);
else
    grain = struct('id', 1, 'phaseid', grain.phaseid, ...
        'center', grain.center, 'R_vector', grain.R_vector);
end

grain = gtCalculateGrain(grain, parameters);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Iinitial blob parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

num_dets = numel(detgeo);
grain.proj(1:num_dets) = struct( ...
    'stack', [], 'num_cols', [], 'num_rows', [], 'bl', [], ...
    'vol_size_x', [], 'vol_size_y', [], 'vol_size_z', [], 'shift', [], ...
    'ondet', [], 'included', [], 'selected', [], 'centerpix', [], ...
    'gvb', []);

for n = 1:num_dets
    % Checking if we should just project all of them, or wether the user
    % has something to say abou it.
    if (isfield(fedpars, 'detector') && isfield(fedpars.detector(n), 'ondet'))
        ondet = fedpars.detector(n).ondet;
        included = fedpars.detector(n).included;
        selected = fedpars.detector(n).selected;
    else
        ondet = find(grain.allblobs(n).detector.ondet);
        included = (1:numel(ondet))';
        selected = true(size(included));
    end

    nbl = numel(included);

    is_nearfield = norm(detgeo(n).detrefpos) < 90;
    % If it is farfield, we should adjust the voxel size to be 1/4 of the
    % pixel size
%     if (~is_nearfield)
%         voxsize = mean([detgeo(n).pixelsizeu, detgeo(n).pixelsizev]) / 4;
%         recgeo(n).voxsize = voxsize([1 1 1]);
%         parameters.recgeo(n) = recgeo(n);
%     end
    fprintf( ['Generating blob data (for detector: %d, blobs on the' ...
        ' detector: %d):\n - Collecting blob info..'], n, nbl);
    c = tic();

    % Create a blank blob structure
    bl = repmat(gtFedBlobDefinition(), nbl, 1);
    gvb = [];

    for ii = 1:nbl
        % Load diffraction paramaters of blobs
        ii_b = ondet(included(ii));

        % Plane families
        bl(ii).hkl       = grain.allblobs(n).hkl(ii_b, :);
        bl(ii).hklsp     = grain.allblobs(n).hklsp(ii_b, :);
        bl(ii).thetatype = grain.allblobs(n).thetatype(ii_b);

        % Initial strained signed plane normal in SAMPLE reference
        bl(ii).plsam = grain.allblobs(n).pl(ii_b, :);

        % Initial strained non-signed plane normal in SAMPLE reference
        bl(ii).plorig = grain.allblobs(n).plorig(ii_b, :);

        % Initial strained signed plane normal in the diffraction position in
        % LAB reference
        bl(ii).pl0 = grain.allblobs(n).pllab(ii_b, :);

        % Initial strained diffraction angles and rotation matrix
        bl(ii).th0    = grain.allblobs(n).theta(ii_b);     % initial theta
        bl(ii).sinth0 = grain.allblobs(n).sintheta(ii_b);  % initial sin(theta)
        bl(ii).eta0   = grain.allblobs(n).eta(ii_b);       % initial eta
        bl(ii).om0    = grain.allblobs(n).omega(ii_b);     % initial omega
        bl(ii).omind  = grain.allblobs(n).omind(ii_b);     % initial omega index (1..4)
        bl(ii).S0     = grain.allblobs(n).srot(:, :, ii_b); % initial rotation tensor

        % Initial strained diffraction vector in LAB reference
        bl(ii).dvec0 = grain.allblobs(n).dvec(ii_b, :);

        % Initial strained diffraction vector in SAMPLE reference
        bl(ii).dvecsam = grain.allblobs(n).dvecsam(ii_b, :);

        % Initial centroid in image
        bl(ii).u0im = grain.allblobs(n).detector.uvw(ii_b, :);

        % Padding around blob volume
        if (isfield(fedpars, 'detector'))
            bl(ii).addbl = fedpars.detector(n).blobsizeadd;
        else
            bl(ii).addbl = fedpars.blobsizeadd(n, :);
        end
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%% Preallocate blob volumes
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    dim_u = ceil(mean(grenv(1:2) .* recgeo(1).voxsize(1:2) ./ recgeo(n).voxsize(1:2)));
    dim_v = ceil(grenv(3) .* recgeo(1).voxsize(3) ./ recgeo(n).voxsize(3));

    fprintf('\b\b, base blob uv sizes: [%d %d] (%f seconds).\n - Generating blob data..', ...
        dim_u, dim_v, toc(c))
    c = tic();

    for ii = 1:nbl
        % Bounding box sizes and boundaries for processing
        bl(ii).bbsize = [ dim_u, dim_v, 2 ] + 2 * bl(ii).addbl;

        im_low_lims = round( bl(ii).u0im - bl(ii).bbsize/2 );
        bl(ii).bbuim = [im_low_lims(1), im_low_lims(1) + bl(ii).bbsize(1) - 1];
        bl(ii).bbvim = [im_low_lims(2), im_low_lims(2) + bl(ii).bbsize(2) - 1];
        bl(ii).bbwim = [im_low_lims(3), im_low_lims(3) + bl(ii).bbsize(3) - 1];

        % Blob origin and center [u, v, w]:
        bl(ii).bbor = [bl(ii).bbuim(1), bl(ii).bbvim(1), bl(ii).bbwim(1)] - 1;

        bl(ii).bbcent = [ ...
            round( (bl(ii).bbuim(1) + bl(ii).bbuim(2))/2 ), ...
            round( (bl(ii).bbvim(1) + bl(ii).bbvim(2))/2 ), ...
            round( (bl(ii).bbwim(1) + bl(ii).bbwim(2))/2 ) ];

        % Projection parameters
        bl(ii).proj_geom = gtGeoProjForReconstruction( ...
            bl(ii).dvecsam, bl(ii).om0, gc, ...
            [bl(ii).bbuim(1) bl(ii).bbvim(1) bl(ii).bbsize(1:2)], ...
            [], detgeo(n), labgeo, samgeo, recgeo(n), ...
            'ASTRA_grain');

        bl(ii).int = zeros(bl(ii).bbsize, fedpars.bltype);
        bl(ii).mask = ones(bl(ii).bbsize, fedpars.bltype);

        % u in blob = u in image minus u at blob origin
        bl(ii).u0bl = bl(ii).u0im - bl(ii).bbor;
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%% Initial element parameters
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    fprintf('\b\b (%f seconds).\n - Generating voxel->blob data..', toc(c))
    c = tic();

    gvcs = gv.cs(:, gv.used_ind);

    for ii = nbl:-1:1
        gvb(ii).uc      = zeros(size(gvcs), fedpars.gvtype);
        gvb(ii).ucd     = zeros(size(gvcs), fedpars.gvtype);
        gvb(ii).ucblmod = zeros(size(gvcs), fedpars.gvtype);
        gvb(ii).ucbl8   = zeros(1, size(gvcs, 2)*8, fedpars.gvtype);
        gvb(ii).ucbl8ini= zeros(1, size(gvcs, 2)*8, fedpars.gvtype);
        gvb(ii).ucbl8ok = 0;

        % Expand diffraction vector
        dvec = bl(ii).dvec0';
        dvec = dvec(:, ones_nuv);

        % Expand omega
        om = bl(ii).om0(1, ones_nuv);

        % Change from sample to lab coordinates
        gvcs_lab = gtGeoSam2Lab(gvcs', bl(ii).S0', labgeo, samgeo, false);

        % Initial absolute u of gv center
        uc0im = gtFedPredictUVWMultiple([], dvec, gvcs_lab', ...
            detgeo(n).detrefpos', detgeo(n).detnorm', detgeo(n).Qdet, ...
            [detgeo(n).detrefu, detgeo(n).detrefv]', om, labgeo.omstep );

        % Initial u of gv centre relative to blob origin
        bbor = bl(ii).bbor';
        gvb(ii).uc0bl = cast(uc0im - bbor(:, ones_nuv), fedpars.gvtype);
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%% Simulate "measured" diffraction patterns
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    fprintf('\b\b (%f seconds)\n - Simulating true ("measured") blob intensities..', toc(c))
    c = tic();
    [gvb, bl] = gtFedFwdProjExact(gv, gvb, bl, fedpars, parameters, n);

    % Set back original zero actual strain
    for ii = 1:nbl
        bl(ii).intm    = bl(ii).int;
        bl(ii).int(:)  = [];

        bl(ii).comim   = NaN(1, 3);
        bl(ii).combl   = NaN(1, 3);

        gvb(ii).ucbl8ini = gvb(ii).ucbl8;
    end

    fprintf('\b\b (%f seconds)\n - Calculate boundaries of measured blobs inside blob bounding box..', toc(c))
    c = tic();
    for ii = 1:nbl
        uproj = sum(sum(abs(bl(ii).intm), 2), 3);
        bl(ii).mbbu = [find(uproj, 1, 'first'), find(uproj, 1, 'last')] + bl(ii).bbuim(1) - 1;

        vproj = sum(sum(abs(bl(ii).intm), 1), 3);
        bl(ii).mbbv = [find(vproj, 1, 'first'), find(vproj, 1, 'last')] + bl(ii).bbvim(1) - 1;

        wproj = sum(sum(abs(bl(ii).intm), 1), 2);
        bl(ii).mbbw = [find(wproj, 1, 'first'), find(wproj, 1, 'last')] + bl(ii).bbwim(1) - 1;

        bl(ii).mbbsize = [bl(ii).mbbu(2) - bl(ii).mbbu(1) + 1, ...
                          bl(ii).mbbv(2) - bl(ii).mbbv(1) + 1, ...
                          bl(ii).mbbw(2) - bl(ii).mbbw(1) + 1 ];
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%% Transform into single
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % Transform into single precision
    if strcmp(fedpars.gvtype, 'single')
        fprintf('\b\b (%f seconds)\n - Converting type to single..', toc(c))
        c = tic();
        fn = fieldnames(gvb);
        for ii = 1:length(fn)
            for jj = 1:length(gvb)
                gvb(jj).(fn{ii}) = single(gvb(jj).(fn{ii}));
            end
        end
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%% Assembling the final proj structure
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    fprintf('\b\b (%f seconds)\n - Producing..\n', toc(c))
    c = tic();

    stack = arrayfun(@(x){sum(x.intm, 3)}, bl);
    stack = cat(3, stack{:});

    vol_size = gtGeoSam2Sam(grsize, recgeo(1), recgeo(n), 1);
    vol_size = ceil(vol_size * 1.2);
    if (is_nearfield)
        vol_size(3) = max(vol_size(3), 8);
    end

    grain.proj(n).stack = permute(stack, [1 3 2]);
    grain.proj(n).num_cols = size(grain.proj(n).stack, 3);
    grain.proj(n).num_rows = size(grain.proj(n).stack, 1);
    grain.proj(n).bl = bl;
    grain.proj(n).vol_size_x = vol_size(2);
    grain.proj(n).vol_size_y = vol_size(1);
    grain.proj(n).vol_size_z = vol_size(3);
    grain.proj(n).centerpix = gtGeoSam2Sam(grain.center, samgeo, recgeo(n), 0);
    grain.proj(n).shift = gtFwdSimComputeVolumeShifts(grain.proj(n), parameters, vol_size);
    grain.proj(n).ondet = ondet;
    grain.proj(n).included = included;
    grain.proj(n).selected = selected;

    grain.proj(n).gvb = gvb;

    fprintf('\b\b(%f seconds)\n - True blob dimensions:\n', toc(c))
    fprintf(' %3d) %4d %4d %4d\n', [(1:nbl)', vertcat(bl(:).mbbsize)]')
end

end

function [gvcs, gvd] = compute_voxel_centers(gv, dmvol, fedpars, recgeo)
    nv = size(gv.ind, 2);
    nv_ones = ones(nv, 1);

    if (isfield(fedpars, 'volume_super_sampling') ...
            && (fedpars.volume_super_sampling > 1))

        vssampling = fedpars.volume_super_sampling;

        num_subvoxels = vssampling ^ 3;
        gvcs = zeros(3, nv, num_subvoxels, fedpars.gvtype);
        gvd = zeros(3, nv, num_subvoxels, fedpars.gvtype);

        dmvol_size = [size(dmvol, 1), size(dmvol, 2), size(dmvol, 3)]';
        dmvol_size = dmvol_size(:, nv_ones);

        orig_gvdm = gv.dm;

        counter = 0;
        corner = (1 - (1 / vssampling)) / 2 .* recgeo.voxsize;

        fprintf('\b\b: ')

        for ss_x = linspace(-corner(1), corner(1), vssampling)
            for ss_y = linspace(-corner(2), corner(2), vssampling)
                for ss_z = linspace(-corner(3), corner(3), vssampling)

                    num_chars = fprintf('%03d/%03d', counter, num_subvoxels);

                    offset_subvoxel = [ss_x, ss_y, ss_z];

                    gvcs(:, :, num_subvoxels-counter) = ...
                        fedpars.vox000sam(nv_ones, :)' ...
                        + gv.ind .* recgeo.voxsize(nv_ones, :)' ...
                        + offset_subvoxel(nv_ones, :)';

                    offset_pos = offset_subvoxel ./ recgeo.voxsize;
                    offset_pos = gv.ind + offset_pos(nv_ones, :)';

                    res_r_vecs = zeros(3, nv);
                    res_ints = zeros(3, nv);

                    for x_r = 0:1
                        for y_r = 0:1
                            for z_r = 0:1
                                rounding = [x_r, y_r, z_r]';
                                pos = floor(offset_pos) + rounding(:, nv_ones);
                                int_coeff = 1 - abs(offset_pos - pos);

                                valid = all(int_coeff > 0, 1) ...
                                    & all(pos > 0, 1) ...
                                    & all(pos <= dmvol_size, 1);

                                valid_x = pos(1, valid);
                                valid_y = pos(2, valid);
                                valid_z = pos(3, valid);

                                ind_valid_pos = valid_x ...
                                    + (valid_y - 1) * size(dmvol, 1) ...
                                    + (valid_z - 1) * size(dmvol, 1) * size(dmvol, 2);

                                % Filtering the positions that have no
                                % intensity
                                valid(valid) = valid(valid) & (gv.pow(ind_valid_pos) > 0);

                                valid_x = pos(1, valid);
                                valid_y = pos(2, valid);
                                valid_z = pos(3, valid);

                                ind_valid_pos = valid_x ...
                                    + (valid_y - 1) * size(dmvol, 1) ...
                                    + (valid_z - 1) * size(dmvol, 1) * size(dmvol, 2);

                                res_ints(:, valid) = res_ints(:, valid) + int_coeff(:, valid);

                                res_r_vecs(:, valid) = res_r_vecs(:, valid) ...
                                    + int_coeff(:, valid) .* orig_gvdm(:, ind_valid_pos);
                            end
                        end
                    end

                    % Renormalizing disabled because not correct!!
%                     gvpow(1, :, num_subvoxels-counter) = prod(res_ints, 1) .* gv.pow;

                    renorm_res_ints = res_ints + (res_ints == 0);
                    gvd(:, :, num_subvoxels-counter) = res_r_vecs ./ renorm_res_ints;

                    counter = counter + 1;

                    fprintf(repmat('\b', [1 num_chars]));
                end
            end
        end
    else
        gvcs = fedpars.vox000sam(nv_ones, :)' + gv.ind .* recgeo.voxsize(nv_ones, :)';
        gvd = gv.d;
    end
end
