function [gvdm, dmvol, gradok] = gtFedTestGenerateRandomDef(fedpars, nn)

    volsize = fedpars.grainsize;

    interp_method = 'spline';
%     interp_method = 'linear';

    if (~isfield(fedpars, 'deftype'))
        fedpars.deftype = 'domains3D';
    end

    if (~isfield(fedpars, 'generate'))
        fedpars.generate = struct(...
            'dappgradlim', 2e-04 * ones(1, 9), ...
            'dapplim', 5e-3 * ones(1, 9), ...
            'randdefmode', 'interp', ...
            'seeding', 'radial' );
    end

    fprintf('Creating dmvol: ')
    c = tic();
    switch (lower(fedpars.deftype))
        case 'smooth'
            [gvdm, dmvol, gradok] = make_smooth_def(fedpars.generate, volsize, nn, interp_method);
        case 'blocks3d'
            [gvdm, dmvol, gradok] = make_blocks3D_def(fedpars.generate, volsize, nn);
        case 'domains3d'
            [gvdm, dmvol, gradok] = make_domains3D_def(fedpars.generate, volsize, nn);
    end
    fprintf('Done in %g seconds.\n', toc(c))

    gtGetMaxDisorientation(gvdm, [], 'diameter_average');
    gtGetMaxDisorientation(gvdm, [], 'diameter_zero');
    gtGetMaxDisorientation(gvdm, [], 'minimal');

%     tiles = gt6DComputeOrientationTiles(dmvol, 11);
%     merged = gt6DTileDmvol(dmvol, tiles);
%     GtVolView(merged, 'cmap', jet);
    igm = gtDefComputeIntraGranularMisorientation(dmvol, [], 'R_vector', [0,0,0]);
    GtVolView(igm, 'cmap', 'parula')

    disp('Mean of deformation components:')
    disp(mean(gvdm, 2))
    disp('Std of deformation components:')
    disp(std(gvdm, 0, 2))
    disp('Bbox of deformation components:')
    disp([max(gvdm, [], 2)'; min(gvdm, [], 2)'])
end % of function

function [vol, gradok, maxgrad] = smooth_vol(vol, gausssize, gradlim)
    for jj = 1:500
        % Smooth until gradients are small enough
        diffx = diff(vol, 1, 1);
        diffy = diff(vol, 1, 2);
        diffz = diff(vol, 1, 3);

        maxgrad = max(abs([diffx(:); diffy(:); diffz(:)]));

        gradok = maxgrad <= gradlim;
        if (gradok), return; end

        vol = smooth3(vol, 'gaussian', gausssize);
    end
    fprintf('Failed to minimize gradient!\n');
end

function [gvdm, dmvol, gradok] = make_smooth_def(fedpars, volsize, nn, interp_method)

    gausssize = [3 3 3];

    gradok = false(nn, 1);

    gvdm  = zeros(nn, volsize(1)*volsize(2)*volsize(3));
    gradlim = fedpars.dappgradlim;

    for ii = 1:nn
        switch(fedpars.randdefmode)

            case 'smooth'
                % Random values for all voxels
                vol = randn(volsize) * fedpars.dapplim(ii);

            case 'interp'
                % 3x3x3 grid points on the corners of the grain volume;
                % random strain components will be generated on this grid
                mgvec           = [0, 0.5, 1];
                [mg1, mg2, mg3] = meshgrid(mgvec, mgvec, mgvec);

                % Generate random strain components (+-fedpars.dapplim) on the 3x3x3 grid
                mgvals = -randn(3, 3, 3) * fedpars.dapplim(ii);

                % Grid for all voxels in grain volume.
                % Flip real coordinates 1-2 due to meshgrid !!!!
                [mg1i, mg2i, mg3i] = meshgrid( ...
                    (0.5:volsize(2))/volsize(2), ...
                    (0.5:volsize(1))/volsize(1), ...
                    (0.5:volsize(3))/volsize(3) );

                % interpolation using 'interp_method' method.
                vol = interp3(mg1, mg2, mg3, mgvals, mg1i, mg2i, mg3i, interp_method);

            otherwise
                error('Mode for generating random deformation not recognized.')
        end

        % Shall we really smooth things out?
        [vol, gradok(ii), maxgrad] = smooth_vol(vol, gausssize, gradlim(ii));

        fprintf('Maximum gradient for component %d: %g (accepted: %g)\n', ii, maxgrad, gradlim(ii))

        gvdm(ii, :) = vol(:);
    end

    dmvol = gtDefGvdm2Dmvol(gvdm, [volsize, nn]);

    f = figure();
    ax = axes('parent', f);
    hold(ax, 'on')
    scatter3(ax, gvdm(1, 1:200:end)', gvdm(2, 1:200:end)', gvdm(3, 1:200:end)', 20, 'c')
    scatter3(ax, mgvals(1, :)', mgvals(2, :)', mgvals(3, :)', 20, 'r')
    hold(ax, 'off')
    drawnow();
end

function [gvdm, dmvol, gradok] = make_blocks3D_def(fedpars, volsize, nn)

    num_seeds = 4;
    % Find Seeds first
    seeds = 2 * rand(3, num_seeds - 1) - 1;
    seeds = seeds ./ sqrt(abs(seeds));
    seeds = [seeds, zeros(3, 1)];
    seeds = (seeds + 1) / 2;
    seeds = round((repmat(volsize', 1, num_seeds) - 1) .* seeds) + 1;
    radii = [];
    for ii_s = 1:num_seeds
        for ii_s2 = (ii_s+1):num_seeds
            radii(end+1) = norm(seeds(:, ii_s) - seeds(:, ii_s2));
        end
    end
    safe_dist = min(radii) / 2;

    prob = cell(3, num_seeds);
    [xx, yy, zz] = ndgrid(1:volsize(1), 1:volsize(2), 1:volsize(3));

    for ii_dim = 1:3
%         polariz_fac = rand() * 2 + 1;
        polariz_fac = rand() * 0.4 + 0.6;
        for ii_s = 1:num_seeds
            prob{ii_dim}{ii_s} =  exp( -( ...
                ( ((xx - seeds(1, ii_s)) .^ 2) ./ ((safe_dist * polariz_fac) .^ 2 * 2) ) + ...
                ( ((yy - seeds(2, ii_s)) .^ 2) ./ ((safe_dist * polariz_fac) .^ 2 * 2) ) + ...
                ( ((zz - seeds(3, ii_s)) .^ 2) ./ ((safe_dist * polariz_fac) .^ 2 * 2) ) ...
                ) );
        end
    end

    % Generate random def components (+-fedpars.dapplim) on the 3x3x3 grid
    mgvals = -randn(nn, num_seeds-1) .* fedpars.dapplim(ones(1, num_seeds-1), 1:nn)';
    mgvals = [mgvals, zeros(nn, 1)];

    gvdm = zeros(nn, volsize(1)*volsize(2)*volsize(3));

    for ii_dim = 1:3
        sum_weigths = sum(cat(4, prob{ii_dim}{:}), 4);

        vol = mgvals(ii_dim, 1) .* prob{ii_dim}{1};
        for ii_s = 2:num_seeds
            vol = vol + mgvals(ii_dim, ii_s) .* prob{ii_dim}{ii_s};
        end
        vol = vol./ sum_weigths;

        gvdm(ii_dim, :) = vol(:);
    end

    dmvol = gtDefGvdm2Dmvol(gvdm, [volsize, nn]);
    gradok = false(nn, 1);

    f = figure();
    ax = axes('parent', f);
    hold(ax, 'on')
    scatter3(ax, gvdm(1, 1:200:end)', gvdm(2, 1:200:end)', gvdm(3, 1:200:end)', 20, 'c')
    scatter3(ax, mgvals(1, :)', mgvals(2, :)', mgvals(3, :)', 20, 'r')
    hold(ax, 'off')
    drawnow();
end

function [gvdm, dmvol, gradok] = make_domains3D_def(fedpars, volsize, nn)

    num_extra_seeds = 8;
    base_seeds = zeros(3, 1);
%     base_seeds = [ones(3, 1), -ones(3, 1), [-1; 0.25; 0.25], zeros(3, 1)];
    num_seeds = num_extra_seeds + size(base_seeds, 2);
    % Find Seeds first
    seeds = 2 * rand(3, num_extra_seeds) - 1;
    seeds = seeds ./ (abs(seeds) .^ (1/6));
    seeds = [seeds, base_seeds];

    if (isfield(fedpars, 'generate') && strcmpi(fedpars.generate.seeding, 'radial'))
        mgvals = seeds .* fedpars.dapplim(ones(1, num_seeds), 1:nn)';
    else
        % Generate random def components (+-fedpars.dapplim) on the 3x3x3 grid
        mgvals = (2 * rand(nn, num_seeds-1) - 1) .* fedpars.dapplim(ones(1, num_seeds-1), 1:nn)';
        mgvals = [mgvals, zeros(nn, 1)];

        fprintf('\n- Ordering seeds..')
        c = tic();
        % ordering magnitude by distance to center
        dist_seeds = sqrt(sum(seeds .^ 2, 1));
        dist_defs = sqrt(sum(mgvals .^ 2, 1));
        [~, seeds_inds] = sort(dist_seeds, 'descend');
        [~, defs_inds] = sort(dist_defs, 'descend');
        seeds = seeds(:, seeds_inds);
        mgvals = mgvals(:, defs_inds);
        fprintf('\b\b: Done in %g seconds.\n', toc(c))
    end

    % re-scaling the seeds to positions
    seeds = (seeds + 1) / 2;
    seeds = round((repmat(volsize', 1, num_seeds) - 1) .* seeds) + 1;

    dmvol = zeros([volsize, nn]);

    smooth_edge = ones(1, 3) * (round(mean(volsize) / 20) * 2) + 1;
    xx = linspace(-16, 16, smooth_edge(1));
    yy = linspace(-16, 16, smooth_edge(2));
    zz = linspace(-16, 16, smooth_edge(3));
    [xx, yy, zz] = ndgrid(xx, yy, zz);
    smooth_filter = exp(-(xx .^ 2 + yy .^ 2 + zz .^ 2) .^ (1/2));
    smooth_filter = smooth_filter / sum(smooth_filter(:));

    coeff_rough = 0.025;
    strengths = (0.8 - coeff_rough) * rand(1, num_seeds) + 0.1;

    [xx, yy, zz] = ndgrid(1:volsize(1), 1:volsize(2), 1:volsize(3));

    fprintf('- Computing domains..')
    c = tic();
    for ii_c = 1:nn
        dists = cell(1, num_seeds);
        for ii_s = 1:num_seeds
            dist_comp = cat(4, xx - seeds(1, ii_s), yy - seeds(2, ii_s), zz - seeds(3, ii_s));
            dists{ii_s} = sqrt(sum(dist_comp .^ 2, 4)) * (1 - 0.1 * strengths(ii_s));
        end

        [~, ind_s] = min(cat(4, dists{:}), [], 4);
        dmvol_comp_sharp = reshape(mgvals(ii_c, ind_s(:)), volsize);

        coeffs_sharp = 1 - coeff_rough;

        dmvol(:, :, :, ii_c) = coeffs_sharp .* dmvol_comp_sharp;
    end
    fprintf('\b\b: Done in %g seconds.\n', toc(c))

    fprintf('- Adding smoothness..')
    c = tic();
    % This will probably be a large convolution, so we do it in Fourier
    dmvol_pad_smooth = (smooth_edge - 1) / 2;
    slice_dmvol = cell(1, 4);
    slice_dmvol(:) = {':'};
    for ii_s = 1:3
        pre_pad = slice_dmvol;
        pre_pad{ii_s} = ones(1, dmvol_pad_smooth(ii_s));
        post_pad = pre_pad;
        post_pad{ii_s} = post_pad{ii_s} * size(dmvol, ii_s);
        dmvol = cat(ii_s, dmvol(pre_pad{:}), dmvol, dmvol(post_pad{:}));
        dmvol = fft(dmvol, [], ii_s);

        size_filter = size(smooth_filter);
        size_filter(ii_s) = (size(dmvol, ii_s) - size_filter(ii_s)) / 2;
        smooth_filter = cat(ii_s, zeros(ceil(size_filter)), smooth_filter, zeros(floor(size_filter)));
        smooth_filter = ifftshift(smooth_filter, ii_s);
        smooth_filter = fft(smooth_filter, [], ii_s);
    end
    dmvol = bsxfun(@times, dmvol, smooth_filter);
    for ii_s = 1:3
        dmvol = ifft(dmvol, [], ii_s);
    end
    dmvol = real(dmvol);
    dmvol = dmvol( ...
        dmvol_pad_smooth(1)+1:end-dmvol_pad_smooth(1), ...
        dmvol_pad_smooth(2)+1:end-dmvol_pad_smooth(2), ...
        dmvol_pad_smooth(3)+1:end-dmvol_pad_smooth(3), :);
    fprintf('\b\b: Done in %g seconds.\n', toc(c))

    fprintf('- Adding roughness..')
    c = tic();
    for ii_r = 1:3
        rough_edge = [7, 7, 7] * (3 ^ (ii_r - 1));
        xx_r = linspace(1, volsize(1), rough_edge(1));
        yy_r = linspace(1, volsize(2), rough_edge(2));
        zz_r = linspace(1, volsize(3), rough_edge(3));
        dmvol_rough_ii = (2 .* rand(nn, prod(rough_edge)) - 1) .* fedpars.dapplim(ones(1, prod(rough_edge)), 1:nn)';
        dmvol_rough_ii = reshape(dmvol_rough_ii', [rough_edge, nn]) / ii_r;

        for ii_c = 1:nn
            dmvol_comp_rough = interp3(xx_r, yy_r, zz_r, dmvol_rough_ii(:, :, :, ii_c), xx, yy, zz, 'spline');
            dmvol(:, :, :, ii_c) = dmvol(:, :, :, ii_c) + coeff_rough .* dmvol_comp_rough;
        end
    end
    fprintf('\b\b: Done in %g seconds.\n', toc(c))

    gvdm = gtDefDmvol2Gvdm(dmvol);
    gradok = false(nn, 1);

%     f = figure();
%     ax = axes('parent', f);
%     hold(ax, 'on')
%     scatter3(ax, gvdm(1, 1:200:end)', gvdm(2, 1:200:end)', gvdm(3, 1:200:end)', 20, 'c')
%     scatter3(ax, mgvals(1, :)', mgvals(2, :)', mgvals(3, :)', 20, 'r')
%     hold(ax, 'off')
%     drawnow();
end



