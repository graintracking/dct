function rot = gtFedRod2RotTensor(r)
% GTFEDROD2ROTTENSOR  Rotation tensors from Rodrigues vectors. Vectorised
%                     for speed
% 
%     rot = gtFedRod2RotTensor(r)
%     ---------------------------
%
%     Transforms a set of Rodrigues vectors into rotation tensors. It uses the 
%     general mathematical definition, that is right-handed rotations 
%     around 'r' with alpha according to norm(r) = tan(alpha/2) . I.e. it does
%     NOT imply any crystallographic notation or definitions. 
%     
%     INPUT
%       r   = Rodrigues vectors (1x3 or 3x1 or 3xn)
%
%     OUTPUT
%       rot = rotation matrices (3x3xn)
%


% If one single entry
if (numel(r) == 3)
    
    rs1 = r(1)^2;
    rs2 = r(2)^2;
    rs3 = r(3)^2;
    
    r1r2 = r(1)*r(2);
    r1r3 = r(1)*r(3);
    r2r3 = r(2)*r(3);
    
    rot = [rs1 - rs2 - rs3 + 1, 2*(r1r2 - r(3)),     2*(r1r3 + r(2)); ...
           2*(r1r2 + r(3)),     rs2 - rs1 - rs3 + 1, 2*(r2r3 - r(1)); ...
           2*(r1r3 - r(2)),     2*(r2r3 + r(1)),     rs3 - rs1 - rs2 + 1];
    
    rot = rot/(rs1 + rs2 + rs3 + 1);
    
    return
end


% Otherwise vectorize computation
n   = size(r,2);
rot = zeros(9, n, class(r));


% Diagonals (1,1) (2,2) (3,3)
r2 = r.^2;
rot(1,:) = r2(1,:);
rot(5,:) = r2(2,:);
rot(9,:) = r2(3,:);

% Off diagonals (1,2) (2,1)
rr = r(1,:).*r(2,:);
rot(2,:) = rr + r(3,:);
rot(4,:) = rr - r(3,:);

% Off diagonals (1,3) (3,1)
rr = r(1,:).*r(3,:);
rot(3,:) = rr - r(2,:);
rot(7,:) = rr + r(2,:);

% Off diagonals (2,3) (3,2)
rr = r(2,:).*r(3,:);
rot(6,:) = rr + r(1,:);
rot(8,:) = rr - r(1,:);


% Multiply all by two
rot = rot*2;


% Add term to diagonals
r2s = sum(r2,1);
rot(1,:) = rot(1,:) - r2s + 1;
rot(5,:) = rot(5,:) - r2s + 1;
rot(9,:) = rot(9,:) - r2s + 1;


% Divide all
r2s = 1 + r2s;
rot = rot./r2s(ones(9,1),:);


% Reshape for output
rot = reshape(rot, 3, 3, []);


end % of function
