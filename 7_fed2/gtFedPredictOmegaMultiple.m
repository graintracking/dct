function [om, plrot, plsigned, rot_s2l_c, omind] = gtFedPredictOmegaMultiple(...
         pl, sinth, beamdir, rotdir, rotcomp, omind)
% GTFEDPREDICTOMEGAMULTIPLE Omega angles where reflection occur from the 
% the plane normals (Bragg's law is satisifed). Vectorised for speed.
% 
% [om, pllab, plsigned, rot, omind] = gtFedPredictOmegaMultiple(...
%     pl, sinth, beamdir, rotdir, rotcomp, omind)
%
% -------------------------------------------------------------------
%
% Predicts the four omega angles where diffraction occurs for multiple 
% plane normals 'pl '. Rotating 'pl' around 'rotdir' will bring them 
% in a position where the angle between 'pl' and 'beamdir' will be theta 
% (the Bragg angle). 
% All inputs have to be given in the same reference. 
%
% The output 'plsigned' is as follows: 
%   plsigned(:,:,1:2) =  pl
%   plsigned(:,:,3:4) = -pl
% These vectors result in rotated plane normals such that their dot products 
% with the beam direction is always negative, i.e they satisfy: 
%   plrot(:,i,j) = rot(:,:,i,j)*plsigned(:,i,j)  , 1<=j<=4          
%   beamdir'*plrot(:,i,j) = -sinth
%
% The omega angles are arranged in the output as follows:
%   - om(1,:)-om(3,:) are one Friedel pair, and om(2,:)-om(4,:) are the 
%     other Friedel pair in an orthogonal setup ('beamdir' perpendicular
%     to 'rotdir')
%   - om1 is always smaller than om2
% Therefore the omega indices are NOT necessarily ORDERED and are returned 
% in 'omind'. These indices can be re-used later as input for speed-up, so 
% that only the relevant omega angles are (re)computed.
%
%
% INPUT
%    pl      - normalised unit plane normals (3xn)
%    sinth   - sin(theta) corresponding to 'pl' (1x1 or 1xn)
%    beamdir - normalised beam direction (3x1 or 3x1)
%    rotdir  - normalised rotation axis direction (right-handed) (3x1)
%    rotcomp - rotational matrix components for COLUMN vectors !
%              (pl_rotated = Srot*pl)
%    omind   - omega index (1..4) to be computed; empty to get 
%              all four; (1x1 or 1xn)
%
% OUTPUT
%    om        - the four omegas in degrees where reflections appear;
%                contains NaN where no solution exists (1xn or 4xn)
%    plrot     - the plane normals rotated in the diffraction positions;
%                no. 3 and 4 are inverted after rotation (pointing oppposite
%                to beam) to ease calculation at a later stage;
%                size (3xn or 3xnx4) (in LAB coordinates)
%    plsigned  - the original input plane normals with no. 3 and 4 inverted;
%                in Sam coordinates
%                 plrot(:,i,j) = rot(:,:,i,j)*plsigned(:,i,j) the  is where (3xn or 3xnx4)               
%    rot_s2l_c - rotation matrices (3x3xn or 3x3xnx4)
%                Saml->Lab for column vectors, Lab->Sam for row vectors
%    omind     - omega indices (1xn or 4xn)
%


% % Polychromatic case
% if rotdir == [0 0 0]
% 	
% 	om = 0;
% 	plrot = -pl*sign(pl'*beamdir);
% 	plsigned = plrot;
% 	
% 	return
% end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Quadratic equation for omega angles
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

nn = size(pl, 2);

if ((size(beamdir, 1) == 3) && (size(beamdir, 2) == 1))
    A = beamdir' * (rotcomp.cos * pl);
    B = beamdir' * (rotcomp.sin * pl);
    C = beamdir' * (rotcomp.const * pl);
else
    A = sum(beamdir .* (rotcomp.cos * pl), 1);
    B = sum(beamdir .* (rotcomp.sin * pl), 1);
    C = sum(beamdir .* (rotcomp.const * pl), 1);
end

D = A .^ 2 + B .^ 2;

if (~isempty(omind))
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%% Single omega index
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    ssp = ((omind == 1) | (omind == 2));
    ss1 = ssp - ~ssp;

    CC = C + ss1 .* sinth;
    DD = D - CC .^ 2;
    E  = sqrt(DD);
    ok = DD > 0;

    ssp = ((omind == 1) | (omind == 3));
    ss  = ssp - ~ssp;
    ss  = ss .* ok;

    om = 2 * atand((-B + ss .* E) ./ (CC - A));
    om(~ok) = NaN;
  
    % Limit range
    om = mod(om, 360);

    % ROTATION MATRICES AND PLANE NORMALS IN LAB

    if (nargout > 1)
        % Get rotation matrix and multiply the input plane normals to get
        % them in the diffracting position
        rot_s2l_c = gtMathsRotationTensor(om, rotcomp);

        % expand for multiplication
        plt   = reshape(pl, 1, 3, []);
        plrot = bsxfun(@times, rot_s2l_c, plt);
        plrot = sum(plrot, 2);
        plrot = reshape(plrot, 3, []);

        plrot    = bsxfun(@times, ss1, plrot);
        plsigned = bsxfun(@times, ss1, pl);
    end
else
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%% All four omega indices
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    om = NaN(4, nn);

    % Diffraction condition 1
    % Pl_rot and beamdir dotproduct equals -sin(th)
    CC = C + sinth;
    DD = D - CC .^ 2;
    ok = DD > 0;
    E  = sqrt(DD(ok));

    om(1, ok) = 2 * atand((-B(ok) + E) ./ (CC(ok) - A(ok)));
    om(2, ok) = 2 * atand((-B(ok) - E) ./ (CC(ok) - A(ok)));

    % Diffraction condition 2
    % Pl_rot and beamdir dotproduct equals +sin(th)
    CC = C - sinth;
    DD = D - CC .^ 2;
    ok = DD > 0;
    E  = sqrt(DD(ok));

    om(3, ok) = 2 * atand((-B(ok) + E) ./ (CC(ok) - A(ok)));
    om(4, ok) = 2 * atand((-B(ok) - E) ./ (CC(ok) - A(ok)));

    % Limit range
    om = mod(om, 360);

    % Omegea indices
    omind = [1; 2; 3; 4];
    omind = omind(:, ones(1, nn));

    % For output conventions:
    % make sure om1 is smaller than om2
    chom = om(1, :) > om(2, :);

    om([1 2], chom)    = om([2 1], chom);
    omind([1 2], chom) = omind([2 1], chom);

    % ROTATION MATRICES AND PLANE NORMALS IN LAB (plsigned still in SAM)

    plsigned = zeros(3, nn, 4);
    plsigned(:, :, 1) = pl;
    plsigned(:, :, 2) = pl;
    plsigned(:, :, 3) = -pl;
    plsigned(:, :, 4) = -pl;

    % Get rotation matrices and multpily the input plane normals to get
    % them in the diffracting position (avoid looping)
    rot_s2l_c = gtMathsRotationTensor([om(1, :) om(2, :) om(3, :) om(4, :)], rotcomp);
    rot_s2l_c = reshape(rot_s2l_c, 3, 3, [], 4);

    plt = reshape(pl, 1, 3, []);
    plrot = bsxfun(@times, rot_s2l_c, plt);

    plrot = sum(plrot, 2);
    plrot = reshape(plrot, 3, nn, []);

    % Exchange om3 and om4 if needed, so that om1-om3 and om2-om4 are the
    % opposite reflections (Friedel pairs in case the rotation axis is
    % perpendicular to the beam).
    % Midplane of setup is defined by beam direction and rotation axis.
    % This is needed to consider consistently all possible setups where 
    % the beam and rotation axis are not perpendicular.

    % Cross product beamdir and rotdir
    br = [ beamdir(2, :) * rotdir(3) - beamdir(3, :) * rotdir(2); ...
           beamdir(3, :) * rotdir(1) - beamdir(1, :) * rotdir(3); ...
           beamdir(1, :) * rotdir(2) - beamdir(2, :) * rotdir(1) ];

    % Dot product of br and pllab
    if (size(beamdir, 1) == 3) && (size(beamdir, 2) == 1)
        dot1 = br' * plrot(:, :, 1);
        dot3 = br' * plrot(:, :, 3);
    else
        dot1 = sum(br .* plrot(:, :, 1), 1);
        dot3 = sum(br .* plrot(:, :, 3), 1);
    end

    % Indices to be swapped
    chom = (((dot1 .* dot3) > 0) & ~isnan(dot1) & ~isnan(dot3)) ;

    % Swap 3rd and 4th
    om([3 4], chom)        = om([4 3], chom);
    omind([3 4], chom)     = omind([4 3], chom);
    plrot(:, chom, [3 4])  = plrot(:, chom, [4 3]);
    rot_s2l_c(:, :, chom, [3 4]) = rot_s2l_c(:, :, chom, [4 3]);

    % Change all pllab to opposite direction
    plrot(:, :, [3 4]) = -plrot(:, :, [3 4]);
end

end % of function