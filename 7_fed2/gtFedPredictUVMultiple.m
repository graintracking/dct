function uv = gtFedPredictUVMultiple(Rottens, dlab, csam, detrefpos, ...
                                     detnorm, Qdet, detrefuv)
% GTFEDPREDICTUVMULTIPLE Predicts (u,v) peak positions on the detector
% plane. Vectorised for speed.
% 
% uv = gtFedPredictUVMultiple(Rottens, dlab, csam, detrefpos, detnorm, ...
%      Qdet, detrefuv)
%
% -------------------------------------------------------------
%
% Predicts peak positions [u,v] on the detector plane.
% Note:
%   Diffraction vectors 'dlab' don't have to be normalised.
%   'csam','detrefpos','detrefuv' must be in identical units.
%   Qdet is in units that scales lab units into detector pixels.
%
% INPUT
%   Rottens - rotation matrices that transform the Sample coordinates 'csam'
%             into Lab coordinates (3x3xn or 3x(3xn); for column vectors);
%             if empty, no rotation will be applied
%   dlab    - diffraction vectors (diffracted beam direction) in Lab
%             reference (3xn)
%   csam    - positions in Sample reference (or in Lab if Rottens is empty)
%             to be projected along 'dlab'; i.e. usually the grain centers;
%             (3xn)
%   detrefpos - any point in the detector plane (3x1)
%   detnorm   - detector plane normal pointing towards beam (3x1; unit vector)
%   Qdet      - detector projection matrix (2x3)
%   detrefuv  - detector reference position (u,v) (2x1)
%
% OUTPUT
%   uv      - (u,v) positions on the detector plane (2xn)

% Multiplicator for dlab
%  'dfac' is NaN where the beam is diffracted away from the detector plane,
%  i.e. where the spot is not detected
[dfac, clab] = gtFedPredictDiffFacMultiple(Rottens, dlab, csam, detrefpos, detnorm);

num_vecs = size(dlab, 2);
ones_num_vecs = ones(1, num_vecs);
% Expansion
detrefuv = detrefuv(:, ones_num_vecs);
detrefpos = detrefpos(:, ones_num_vecs);

% u,v position on detector plane
uv = detrefuv + Qdet * (clab + dfac([1 1 1], :) .* dlab - detrefpos);

end