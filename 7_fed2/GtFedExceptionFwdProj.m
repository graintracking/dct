classdef GtFedExceptionFwdProj < MException
    properties
        minimum_blob_size_add = zeros(1, 3);
        det_ind = 1;
    end

    methods
        function self = GtFedExceptionFwdProj(mexc, det_ind, minimum_blob_size_add)
            self = self@MException(mexc.identifier, mexc.message);
            self.addCause(mexc);
            self.det_ind = det_ind;
            self.minimum_blob_size_add = minimum_blob_size_add;
        end
    end
end
