function fedpars = gtFedCreateGrainParsMasked(grain, active_vol, dmvol, parameters, varargin)
% [fedpars, dmvol, active_vol] = gtFedCreateGrainParsMasked(grain, grain_rec, parameters, varargin)
% active_vol mask containing active voxels of dmvol
% dmvol 4D of R_vectors
    conf = struct( ...
        'keep_ondet', true, ...
        'volume_super_sampling', 1, ...
        'voxsize', [] );
    conf = parse_pv_pairs(conf, varargin);

    %%%%%%%%%%%%%%%%%%%%%%%%%
    % Load inputs if needed
    %%%%%%%%%%%%%%%%%%%%%%%%%

    % Grain structure
    if (~exist('grain', 'var') || (isempty(grain)))
        disp('No grain structure found.');
        phase_id = input('Specify the phase ID: \n', 's');
        phase_id = str2double(phase_id);

        grain_id = input('Specify the grain ID: \n', 's');
        grain_id = str2double(grain_id);
        grain = gtLoadGrain(phase_id, grain_id);
    end

    % Parameters
    if (~exist('parameters', 'var') || isempty(parameters))
        fprintf('No parameters file specified. Loading it from %s', pwd)
        parameters = gtLoadParameters();
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Creation of actual fedpars structure
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    fprintf('Creating fedpars structure..')
    c = tic();

    % x, y, z no. of voxels in the grain
    fedpars.grainsize = size(active_vol);

    % Active deformation components. Total number is 9.
    % In case of rotations only (should be our case):
    fedpars.dcomps = [true(1, 3), false(1, 6)];

    % Class of strain states
    fedpars.gvtype = class(active_vol);

    % Generate random deformation components
    if (~exist('dmvol', 'var') || isempty(dmvol))
        % Type of deformation applied
        deftype = inputwdefault(['Specify the type of deformation ' ...
            'applied: smooth (s) or realistic (r):', 's']);
        switch (lower(deftype))
            case 'r'
                fedpars.deftype = 'realistic';
            otherwise
                fedpars.deftype = 'smooth';
        end

        % Random strain definition mode
        randdefmode = input(['Specify the random strain definition mode: ' ...
            'smooth (s) or interpolated (i):'], 's');

        switch (lower(randdefmode))
            case 'i'
                fedpars.randdefmode = 'interp';
            otherwise
                fedpars.randdefmode = 'smooth';
        end

        % Maximum absolute deformation gradient
        fedpars.dappgradlim = 2e-4 * ones(size(fedpars.dcomps)); % guess: verify

        % Maximum absolute deformation value which can be randomly generated
        fedpars.dapplim = 5e-3 * ones(size(fedpars.dcomps)); % guess: verify
    end

    % Detector parameters
    ndet = numel(parameters.detgeo);

    if (~(conf.keep_ondet && isfield(grain, 'proj') && ndet <= numel(grain.proj)))
        grain_fwd = gtCalculateGrain(grain, parameters);
    end

    for ii_det = 1:ndet
        if (conf.keep_ondet && isfield(grain, 'proj') && (numel(grain.proj) >= ii_det))
            if (isfield(grain.proj, 'ondet') ...
                    && (~isempty(grain.proj(ii_det).ondet)))
                fedpars.detector(ii_det).ondet = grain.proj(ii_det).ondet;
                fedpars.detector(ii_det).included = grain.proj(ii_det).included;
                fedpars.detector(ii_det).selected = grain.proj(ii_det).selected;
            else
                fedpars.detector(ii_det).ondet = grain.ondet;
                fedpars.detector(ii_det).included = grain.included;
                fedpars.detector(ii_det).selected = grain.selected;
            end
        else
            fedpars.detector(ii_det).ondet = find(grain_fwd.allblobs(ii_det).detector.ondet);
            fedpars.detector(ii_det).included = 1:numel(fedpars.detector(ii_det).ondet);
            fedpars.detector(ii_det).selected = true(size(fedpars.detector(ii_det).included));
        end

        % Increments in blobs bounding boxes accounting for deformation.
        % Let gtFedTestLoadData_v2 decide the minimum size
        fedpars.detector(ii_det).blobsizeadd = zeros(1, 3);
    end

    % Types for blobs intensities
    fedpars.bltype = 'single';

    % Deformation method for calculation of deformation tensor
    fedpars.defmethod = 'rod_rightstretch';

    % Algorithm for blobs smoothing
    fedpars.intsmoothalg = 'none';

    fedpars.volume_super_sampling = conf.volume_super_sampling;

    fedpars.voxsize = conf.voxsize;

    fprintf('\b\b: Done in %g seconds.\n', toc(c))
end
