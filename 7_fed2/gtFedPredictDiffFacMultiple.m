function [dfac, clab] = gtFedPredictDiffFacMultiple(Rottens, dlab, csam, ...
                        detrefpos, detnorm)
% GTFEDPREDICTDIFFFACMULTIPLE Diffraction vector scalar multiplicator
% to calculate where the beam hits the detector. Vectorised for speed.
%
% [dfac, clab] = gtFedPredictDiffFac(Rottens, dlab, csam, detrefpos, detnorm)
%
% -------------------------------------------------------------------
%
% Diffraction vector dlab doesn't have to be normalised. 'csam' and 
% 'detrefpos' must be in identical units.
%
% INPUT
%   Rottens - rotation matrices that transform the Sample coordinates 'csam'
%             into Lab coordinates (3x3xn or 3x(3xn); for column vectors);
%             if empty, no rotation will be applied
%   dlab    - diffraction vectors (diffracted beam direction) in Lab
%             reference (3xn)
%   csam    - positions in Sample reference (or in Lab if Rottens is empty)
%             to be projected along 'dlab'; i.e. usually the grain centers;
%             (3xn)
%   detrefpos - any point in the detector plane (3x1)
%   detnorm - detector plane normal pointing towards beam (3x1; unit vector)
%
% OUTPUT
%   dfac    - scalar multiplicators; NaN where projection point away from detector
%             (1xn)
%   clab    - Lab coordinates of 'csam' (3xn)
%

% Position of centre after rotation (Sample -> Lab coord.)
if isempty(Rottens)
    clab = csam;
else
    %warning('gtFedPredictDiffFacMultiple:deprecated_behavior', ...
    %    'csam should always be given directly in Lab coordinates for future compatibility')

    csamt = reshape(csam, 1, 3, []);
    Rottens = reshape(Rottens, 3, 3, []);
    rc = Rottens .* csamt([1 1 1], :, :);

    clab = sum(rc, 2);
    clab = reshape(clab, 3, []);
end

% the scalar multiplicator of the diffraction vector
detrefpos = detrefpos(:, ones(1, size(clab, 2)));
dfac = detnorm' * (detrefpos - clab) ./ (detnorm' * dlab);

% beam won't hit the detector if dfac is negative
dfac(dfac <= 0) = NaN;

end % of function