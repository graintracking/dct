

function crack_out = gtColourCrack2(crack, boundaries, boundaries_structure)

%colour crack according to boundary labels
%use criteria of local boundary plane

%calculate crack plane from this size neighbourhood
crackR=3;
%consider boundaries within this neighborhood
boundsR=5;  % was 10
  
[crackx, cracky, crackz]=ind2sub(size(crack), find(crack));
[sizex, sizey, sizez]=size(boundaries);

%make a distance map for later
distmap=zeros((2*boundsR)+1, (2*boundsR)+1, (2*boundsR)+1);
distmap(boundsR+1, boundsR+1, boundsR+1)=1;
distmap=bwdist(distmap);

%crack_out is coloured by boundaries
crack_out=zeros(size(crack));

for i=1:length(crackx)
  
  if mod(i, 1000)==0
    i/length(crackx)
  end
  
  %get the crack voxel location
  x=crackx(i);  y=cracky(i);  z=crackz(i);
  
  %get the crack neighbourhood to calculate crack plane
  xc1=x-crackR;yc1=y-crackR;zc1=z-crackR;
  xc2=x+crackR;yc2=y+crackR;zc2=z+crackR;
  
  dum=find(crackx>xc1 & crackx<xc2 & cracky>yc1 & cracky<yc2 & crackz>zc1 & crackz<zc2);
  
  if length(dum)>3
  
  crackx_local=crackx(dum);
  cracky_local=cracky(dum);
  crackz_local=crackz(dum);
  
  %check that this is okay x y z
  
  [crack_origin, crack_normal]=lsplane([crackx_local cracky_local crackz_local]);
  crack_normal(3)=-crack_normal(3); %should match boundaries_structure.mat  
  
  %which boundaries are close?
  xb1=max(1,x-boundsR);yb1=max(1,y-boundsR);zb1=max(1,z-boundsR);
  xb2=min(sizex,x+boundsR);yb2=min(sizey,y+boundsR);zb2=min(sizez,z+boundsR);
  
  bounds_local=boundaries(xb1:xb2, yb1:yb2, zb1:zb2);
  bounds_list=unique(bounds_local);
  bounds_list(find(bounds_list==0))=[];
  
 dev=[]; dist=[];
  for j=1:length(bounds_list)
    %are these boundaries close in angle? read from boundaries_structure
    bound_normal=boundaries_structure(bounds_list(j)).gb_norm;
    if ~isempty(bound_normal)
      bound_dev=acosd(dot(bound_normal, crack_normal));
      dev(j)=90-abs(bound_dev-90);
    else
      dev(j)=11;%default for external boundaries
      
 %     nb external boundaries shouldn't really crack! only where there are internal gaps in the map
      
    end
  
    %are they close in distance? use distmap
    dist(j)=min(distmap(find(bounds_local==bounds_list(j))));
  end
  
  %choose best boundary
  
  %   can adjust things here
  
  rating=(dev/5)+(dist/boundsR);
%  rating=(dev/10)+(dist/boundsR);
  boundary=bounds_list(find(rating==min(rating)));
  
  if ~isempty(boundary)
  crack_out(x,y,z)=min(boundary);
  else
  crack_out(x,y,z)=0;
  end
  
    
  end
end
