%having aligned crack and boundaries by correlation, assign crack voxels to
%grain boundaries according to which boundaries are closest.  Hence allow a
%bit of flexibility to overcome distortions and misalignments.
%maxradius is the maximum distance between crack and boundary that will be
%considered

function crack = gtColourCrack(crack_in, boundaries_in, maxradius)

%crack - the crack
%boundaries - the labelled boundaries


crack=crack_in;
boundaries=boundaries_in;
% 
% for i=1:size(crack,1)
%     i
% for j=1:size(crack,2)
% for k=1:size(crack,3)
%   
%   if crack(i,j,k)==0
%     continue
%   else %a crack voxel
%     %find the closest boundary label, within a maximum distance
%     
%     
[yy, xx, zz]=ind2sub(size(crack), find(crack));

for a=1:length(xx)
    
    if mod(a, 1000)==0
        a/length(xx)
    end
    
    i=yy(a);
    j=xx(a);
    k=zz(a);
    
    if boundaries(i,j,k)~=0
      crack(i,j,k)=boundaries(i,j,k);
    else
      
    found=0;%found a boundary label voxel
    radius=1;
    while found==0
    
      %loop through a neighbourhood
      label=[];
      for x=i-radius:i+radius
        if x>size(crack,1) || x<1
          continue
        end
        for y=j-radius:j+radius
        if y>size(crack,2) || y<1
          continue
        end
          for z=k-radius:k+radius
            if z>size(crack,3) || z<1
              continue
            end
  
            %if distance greater than radius
            if sqrt(([x y z]-[i j k])*([x y z]-[i j k])')>radius
        continue
            end
            %likewise, if distance less than radius-1
            if sqrt(([x y z]-[i j k])*([x y z]-[i j k])')<radius-1
        continue
            end
            
            if boundaries(x,y,z)~=0%if a label is found
            label=[label boundaries(x,y,z)];%record all labels within radius
            end
            
          end
        end
      end
    
      if ~isempty(label)
        crack(i,j,k)=mode(label);%found a label
        found=1;
      elseif radius>maxradius;% used 6 for ss_crackedA_;
        crack(i,j,k)=0;%failed to find a label
        found=1;
      else
        radius=radius+1;%search wider
      end
      
    end%while searching
      
    end%if we need to search
    
  end%if this is a crack voxel

% end%loop through crack volume
% end
% i/size(crack,1);
% end

% %colour the output volume
% crack_in(y1:y2, x1:x2, z1:z2)=crack;
% 
% %loop through subvolumes
% 
% xx
% yy
% zz
%     end
%   end
% end
if 0
edf_write(crack, 'crack_coloured.edf', 'uint16')
end
