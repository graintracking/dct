// This program uses ITK and VTK libraries to:
// 1) get a grayscale TIFF image including a crack
// 2) apply a mask to the volume                        [optional]
// 3) extract a region of interest from this volume     [optional]
// 4) resample this region by a factor                  [optional]
// 5) proceed to the crack segmentation
// 6) write the binary volume in a TIF file             [optional]

// 7) compute the surface and convert it to an ITK mesh
// 8) convert it to a VTK mesh
// 9) smooth the mesh by a laplacian method             [optional]
// 10) decimate the mesh to reduce the number of cells  [optional]
// 11) write the mesh in a vtk file                     [optional]
// 12) display the resulting mesh in vtk                [optional]

// Personal options interface headers
#include "ProgOptions.h"

// ITK headers
#include "itkImage.h"
#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"
#include "itkTIFFImageIO.h"
#include "itkMaskImageFilter.h"
#include "itkRegionOfInterestImageFilter.h"
#include "itkResampleImageFilter.h"
#include "itkCastImageFilter.h"
#include "itkLinearInterpolateImageFunction.h"
#include "itkCurvatureFlowImageFilter.h"
#include "itkConnectedThresholdImageFilter.h"
#include "itkBinaryMask3DMeshSource.h"
#include "itkBinaryBallStructuringElement.h"
#include "itkBinaryDilateImageFilter.h"
#include "itkBinaryErodeImageFilter.h"
#include "itkMesh.h"

// ITK/VTK interface headers
#include "itkMeshTovtkPolyData.h"
#include "itkVTKImageExport.h"
#include "itkVTKImageImport.h"
#include "itkImportExportVTK.h"

// VTK headers
#include "vtkImageImport.h"
#include "vtkImageExport.h"
#include "vtkContourFilter.h"
#include "vtkSmartPointer.h"
#include "vtkSmoothPolyDataFilter.h"
#include "vtkDecimatePro.h"
//#include <vtkWindowedSincPolyDataFilter.h>

// Personal VTK mesh display headers 
#include "vtkShowWriteMesh.h"

class myProgOptions : public ProgOptions {
  public:
    myProgOptions() : ProgOptions() { }
    bool Check();
};

int main(int argc, char * argv[] )
{
  myProgOptions opt;
  opt.Parse(argc, argv);
  // Start the registration program
  cout << endl;
  cout << "**************************" << endl;
  cout << "*** Crack Segmentation ***" << endl;
  cout << "**************************" << endl;
  cout << endl;

  opt.Check();
  if (opt.verbose) opt.Summary();

  // Pixel types
  typedef unsigned char InputPixelType;
  typedef float         InternalPixelType;
  typedef unsigned char MaskPixelType;
  typedef unsigned char CrackPixelType;

  // Image types
  const unsigned int Dimension = 3;
  typedef itk::Image< InputPixelType,    Dimension > InputImageType;
  typedef itk::Image< InternalPixelType, Dimension > InternalImageType;
  typedef itk::Image< MaskPixelType,     Dimension > MaskImageType;
  typedef itk::Image< CrackPixelType,    Dimension > CrackImageType;

  // Reader
  typedef itk::ImageFileReader< InputImageType > ReaderType;
  ReaderType::Pointer reader = ReaderType::New();
  reader->SetFileName( opt.inputImage );
  try {
    std::cout << "Reading input volume ..." << std::endl; 
    std::cout << "  - from file: " << opt.inputImage << std::endl;
    reader->Update();
  }
  catch( itk::ExceptionObject & EO ) {
    std::cerr << "Exception thrown while reading input file" << std::endl;
    std::cerr << EO << std::endl;
    return EXIT_FAILURE;
  }
  InputImageType::Pointer readImage = reader->GetOutput();

  // Set input spacing (voxel size)
  double readSpacing[] = {opt.voxelSize, opt.voxelSize, opt.voxelSize};
  readImage->SetSpacing(readSpacing); 

  // Mask 
  InputImageType::Pointer toMaskImage = readImage;
  InputImageType::Pointer inputImage;
  if (opt.maskImage.size()) {
    std::cout << "Applying mask ..." << std::endl; 
    std::cout << "  - from file: " << opt.maskImage << std::endl;
    ReaderType::Pointer maskReader = ReaderType::New();
    maskReader->SetFileName( opt.maskImage );
    try {
      maskReader->Update();
    }
    catch( itk::ExceptionObject & EO ) {
      std::cerr << "Exception thrown while reading input file" << std::endl;
      std::cerr << EO << std::endl;
      return EXIT_FAILURE;
    }

    typedef itk::MaskImageFilter<InputImageType, InputImageType,
            InputImageType> MaskFilterType;
    MaskFilterType::Pointer maskFilter = MaskFilterType::New();
    maskFilter->SetInput1( readImage );
    maskFilter->SetInput2( maskReader->GetOutput() );
    maskFilter->SetOutsideValue( opt.maskReplaceValue );
    //maskFilter->SetOutsideValue( opt.segMaxThreshold+100 );
    maskFilter->Update();
    inputImage = maskFilter->GetOutput();
  }
  else inputImage = readImage;
  inputImage->Update();

  // Store input Image Data
  typedef InputImageType::SpacingType InputSpacingType;
  const InputSpacingType inputSpacing = inputImage->GetSpacing();
  typedef InputImageType::SizeType InputSizeType;
  const InputSizeType inputSize =
    inputImage->GetLargestPossibleRegion().GetSize();

  // Create ROI
  InputSizeType size;
  typedef InputImageType::IndexType IndexType;
  IndexType startIndex;
  typedef InputImageType::PointType PointType;
  PointType start;
  if (opt.ROI.size()) {
    start[0] = opt.ROI[0] * opt.voxelSize;
    start[1] = opt.ROI[1] * opt.voxelSize;
    start[2] = opt.ROI[2] * opt.voxelSize;
    if(!inputImage->TransformPhysicalPointToIndex(start, startIndex)) {
      std::cerr << "ROI start is not inside the image!" << std::endl;
      std::cerr << start << std::endl;
      std::cerr << startIndex << std::endl;
      return EXIT_FAILURE;
    }
    PointType end;
    end[0] = opt.ROI[3] * opt.voxelSize;
    end[1] = opt.ROI[4] * opt.voxelSize;
    end[2] = opt.ROI[5] * opt.voxelSize;
    IndexType endIndex;
    if(!inputImage->TransformPhysicalPointToIndex(end, endIndex)) {
      std::cerr << "ROI start is not inside the image!" << std::endl;
      std::cerr << end << std::endl;
      std::cerr << endIndex << std::endl;
      return EXIT_FAILURE;
    }
    size[0] = (unsigned long)round((end[0] - start[0]) / opt.voxelSize);
    size[1] = (unsigned long)round((end[1] - start[1]) / opt.voxelSize);
    size[2] = (unsigned long)round((end[2] - start[2]) / opt.voxelSize);
    std::cout << "Setting ROI in pixels ..." << std::endl;
    std::cout << "  - start: " << startIndex << std::endl;
    std::cout << "  - end:   " << endIndex << std::endl;
    std::cout << "  - size:  " << size << std::endl;
    std::cout << "Setting ROI in microns ..." << std::endl;
    std::cout << "  - start: " << start << std::endl;
    std::cout << "  - end:   " << end << std::endl;
    std::cout << "  - size:  " << end-start << std::endl;
  }
  else {
    start = inputImage->GetOrigin();
    inputImage->TransformPhysicalPointToIndex(start, startIndex);
    //startIndex.Fill(0);
    size = inputSize;
    std::cout << "Setting ROI as the full image ..." << std::endl;
  }
         
  const InputImageType::RegionType region(startIndex, size);

  // Oversample
  InternalImageType::Pointer resampledImage;
  if (opt.scaleFactor != 1.0) {
    typedef itk::IdentityTransform<double, Dimension> TransformType;
    TransformType::Pointer transform = TransformType::New();
    transform->SetIdentity();
    typedef itk::LinearInterpolateImageFunction<InputImageType, double>
      InterpolatorType;
    InterpolatorType::Pointer interpolator = InterpolatorType::New();

    InputSpacingType resampledSpacing = inputSpacing / opt.scaleFactor;

    const InputSizeType& regionSize = region.GetSize();
    InputSizeType resampledSize;
    resampledSize[0] = (unsigned long)round(regionSize[0] * opt.scaleFactor);
    resampledSize[1] = (unsigned long)round(regionSize[1] * opt.scaleFactor);
    resampledSize[2] = (unsigned long)round(regionSize[2] * opt.scaleFactor);

    // Resampling filter
    typedef itk::ResampleImageFilter<InputImageType, InternalImageType>
      ResampleFilterType;
    ResampleFilterType::Pointer resampler = ResampleFilterType::New();

    // Input of the resampler is the output of the reader
    resampler->SetInput(inputImage);
    resampler->SetTransform(transform);
    resampler->SetInterpolator(interpolator);
    resampler->SetDefaultPixelValue(0);
    resampler->SetSize(resampledSize);
    resampler->SetOutputSpacing(resampledSpacing);
    resampler->SetOutputOrigin(start);
    resampler->SetOutputDirection(inputImage->GetDirection());
    resampler->Update();
    std::cout << "Resampling image ..." << std::endl;
    std::cout << "  - factor: " << opt.scaleFactor << std::endl;
    resampledImage = resampler->GetOutput();
  }
  else {
    typedef itk::RegionOfInterestImageFilter<InputImageType, InputImageType>
      ROIFilterType;
    ROIFilterType::Pointer croper = ROIFilterType::New();
    croper->SetRegionOfInterest( region );
    croper->SetInput( inputImage );

    typedef itk::CastImageFilter<InputImageType, InternalImageType> FilterType;
    FilterType::Pointer caster = FilterType::New();
    caster->SetInput( croper->GetOutput() );
    caster->Update();
    resampledImage = caster->GetOutput();
  }
  resampledImage->Update();

  // Release reader and inputImage
  reader->Delete();
  inputImage->Delete();

  // Filter
  std::cout << "Filtering volume to prepare segmentation ..." << std::endl;
  typedef itk::CurvatureFlowImageFilter<InternalImageType, InternalImageType>
    CurvatureFlowImageFilterType;
  CurvatureFlowImageFilterType::Pointer smoothing =
    CurvatureFlowImageFilterType::New();
  smoothing->SetInput( resampledImage );
  smoothing->SetNumberOfIterations( opt.preNumberOfIterations );
  smoothing->SetTimeStep( opt.preTimeStep );
  smoothing->Update();

  // Release resampledImage
  resampledImage->Delete();

  // Threshold segmentation
  std::cout << "Setting crack segmentation ..." << std::endl;
  typedef itk::ConnectedThresholdImageFilter<InternalImageType, CrackImageType>
    ConnectedFilterType;
  ConnectedFilterType::Pointer connectedThreshold = ConnectedFilterType::New();
  connectedThreshold->SetInput( smoothing->GetOutput() );

  // Seed
  if (opt.segSeeds.size()) {
    std::cout << "  Seeds: " << std::endl;
    itk::Point< double, Dimension > seedVoxel;
    itk::Point< double, Dimension > seedCoord;
    InternalImageType::IndexType seedIndex;
    int iseed = 1;
    for(std::vector<std::vector<double> >::iterator it = opt.segSeeds.begin();
        it != opt.segSeeds.end(); ++it) {
      seedVoxel[0] = (*it)[0];
      seedVoxel[1] = (*it)[1];
      seedVoxel[2] = (*it)[2];
      seedCoord[0] = (*it)[0] * opt.voxelSize;
      seedCoord[1] = (*it)[1] * opt.voxelSize;
      seedCoord[2] = (*it)[2] * opt.voxelSize;
      bool isIn = resampledImage->TransformPhysicalPointToIndex( seedCoord,
          seedIndex );
      std::cout << "  - seed " << iseed++ << std::endl;
      std::cout << "    input seed voxel            " << seedVoxel << std::endl;
      std::cout << "    input seed coordinates      " << seedCoord << std::endl;
      std::cout << "    seed index in resampled ROI " << seedIndex << std::endl;

      if(!isIn) {
        std::cerr << "Seed is not inside the image!" << std::endl;
        std::cerr << seedVoxel << std::endl;
        std::cerr << seedCoord << std::endl;
        std::cerr << seedIndex << std::endl;
        return EXIT_FAILURE;
      }
      connectedThreshold->AddSeed( seedIndex );
    }
  }
  else {
    std::cerr << "No seeds have been given, so exiting ..." << std::endl;
    return EXIT_FAILURE;
  }

  std::cout << "Segmenting crack ..." << std::endl;
  connectedThreshold->SetLower( opt.segMinThreshold );
  connectedThreshold->SetUpper( opt.segMaxThreshold );
  CrackPixelType objectValue;
  objectValue = static_cast<CrackPixelType>( opt.segReplaceValue );
  //InternalPixelType objectValue;
  //objectValue = static_cast<InternalPixelType>( opt.segReplaceValue );
  connectedThreshold->SetReplaceValue( objectValue );

  CrackImageType::Pointer crack = connectedThreshold->GetOutput();
  //CrackImageType::Pointer crackOut = crack;
  CrackImageType::Pointer crackOut;

  // Structuring element
  typedef itk::BinaryBallStructuringElement<CrackPixelType, Dimension>
    StructuringElementType;
  StructuringElementType structuringElement;
  //structuringElement.CreateStructuringElement();

  // Dilate segmented crack volume
  if (opt.segDilationRadius.size()) {
    std::cout << "Dilating crack volume for output ..." << std::endl; 
    CrackImageType::SizeType ballSize;
    ballSize[0] = opt.segDilationRadius[0];
    ballSize[1] = opt.segDilationRadius[1];
    ballSize[2] = opt.segDilationRadius[2];
    structuringElement.SetRadius(ballSize);
    structuringElement.CreateStructuringElement();
    typedef itk::BinaryDilateImageFilter<CrackImageType, CrackImageType,
            StructuringElementType> DilationType;
    DilationType::Pointer dilateFilter = DilationType::New();
    dilateFilter->SetInput(crack);
    dilateFilter->SetDilateValue(objectValue);
    dilateFilter->SetKernel(structuringElement);
    crackOut = dilateFilter->GetOutput();
    dilateFilter->Update();
  }
  else crackOut = crack;
  crackOut->Update();

  // Crack binary volume writer
  if (opt.outBinFile.size()) {
    std::cout << "Writing crack binary volume ..." << std::endl; 
    std::cout << "  - to file: " << opt.outBinFile << std::endl;
    typedef itk::TIFFImageIO IOType;
    IOType::Pointer io = IOType::New();
    io->SetCompressionToLZW();
    typedef itk::ImageFileWriter< CrackImageType > WriterType;
    WriterType::Pointer binWriter = WriterType::New();
    binWriter->SetImageIO( io );
    binWriter->SetFileName( opt.outBinFile );
    binWriter->SetUseCompression( opt.compressOutBin );
    binWriter->SetInput( crackOut );
    try {
      binWriter->Update();
    }
    catch( itk::ExceptionObject & EO ) {
      std::cerr << "Exception thrown while writing crack binary volume file"
        << std::endl;
      std::cerr << EO << std::endl;
      return EXIT_FAILURE;
    }

    // Bounding box
    const double outVoxelSize = opt.voxelSize / opt.scaleFactor;

    const CrackImageType::PointType& crackOrigin = crack->GetOrigin();
    CrackImageType::IndexType crackStartIndex;
    crackStartIndex[0] = round(crackOrigin[0] / outVoxelSize);
    crackStartIndex[1] = round(crackOrigin[1] / outVoxelSize);
    crackStartIndex[2] = round(crackOrigin[2] / outVoxelSize);
    typedef CrackImageType::SizeType CrackSizeType;
    const CrackSizeType& crackSize = crack->GetLargestPossibleRegion().GetSize();
    const CrackImageType::IndexType crackEndIndex = crackStartIndex + crackSize;
    std::stringstream b;
    b << crackStartIndex << " " << crackEndIndex << std::endl;
    std::stringstream v;
    v << outVoxelSize << std::endl;

    // Info file
    size_t found = opt.outBinFile.find_last_of(".");
    std::string infoFile = opt.outBinFile.substr(0,found) + ".info";
    std::cout << "Writing crack binary volume info file ..." << std::endl;
    std::cout << "  - to file: " <<  infoFile << std::endl;
    std::cout << "  - BBox      : " << b.str();
    std::cout << "  - Voxel size: " << v.str();
    std::ofstream infoOut(infoFile.c_str());
    if (infoOut.is_open()) {
      infoOut << b.str();
      infoOut << v.str();
      infoOut.close();
    }
    else {
      std::cerr << "Couldn't write crack binary volume info in: '" << infoFile
        << "'!" << std::endl;
    }
  }

  // Stop program here if no mesh output is asked
  if (opt.outVTKFile.size() == 0) {
    cout << "No mesh output specified so exiting..." << endl;
    return EXIT_SUCCESS;
  }

  // Release CurvatureFlowImageFilter
  smoothing->Delete();

  // Dilate
  CrackImageType::Pointer toDilateVolume = crack;
  CrackImageType::Pointer dilatedVolume;
  if ( opt.postDilationRadius > 0) {
    std::cout << "Dilating volume with radius " << opt.postDilationRadius << " ..." << std::endl; 
    typedef itk::BinaryDilateImageFilter < CrackImageType, CrackImageType,
          StructuringElementType > DilateFilterType;
    DilateFilterType::Pointer dilate = DilateFilterType::New();

    structuringElement.SetRadius( opt.postDilationRadius );
    structuringElement.CreateStructuringElement();
    dilate->SetKernel( structuringElement );
    dilate->SetInput( crack );
    dilate->Update();

    dilatedVolume = dilate->GetOutput();
  }
  else dilatedVolume = toDilateVolume;
  dilatedVolume->Update();

  // Erode
  CrackImageType::Pointer toErodeVolume = dilatedVolume;
  CrackImageType::Pointer erodedVolume;
  if ( opt.postErosionRadius > 0) {
    std::cout << "Eroding volume with radius " << opt.postErosionRadius << " ..." << std::endl; 
    typedef itk::BinaryErodeImageFilter < CrackImageType, CrackImageType,
            StructuringElementType > ErodeFilterType;
    ErodeFilterType::Pointer erode = ErodeFilterType::New();

    structuringElement.SetRadius( opt.postErosionRadius );
    structuringElement.CreateStructuringElement();
    erode->SetKernel( structuringElement );
    erode->SetInput( dilatedVolume );
    erode->Update();

    erodedVolume = erode->GetOutput();
  }
  else erodedVolume = toErodeVolume;
  erodedVolume->Update();

  // Mesh
  CrackImageType::Pointer toMeshVolume = erodedVolume;
  std::cout << "Extracting mesh from the crack surface ..." << std::endl;
  typedef itk::DefaultDynamicMeshTraits< double, Dimension, Dimension, double,
          double > MeshTrait;
  typedef itk::Mesh<double, Dimension, MeshTrait> MeshType;

  typedef itk::BinaryMask3DMeshSource<CrackImageType, MeshType> MeshSourceType;
  MeshSourceType::Pointer meshSource = MeshSourceType::New();
  meshSource->SetObjectValue( opt.segReplaceValue );
  meshSource->SetInput( toMeshVolume );

  try {
    meshSource->Update();
  }
  catch( itk::ExceptionObject & EO ) {
    std::cerr << "Exception thrown during Update() " << std::endl;
    std::cerr << EO << std::endl;
    return EXIT_FAILURE;
  }

  MeshType::Pointer itkMesh = meshSource->GetOutput();
  std::cout << "  After extraction:" << std::endl;
  std::cout << "  - Nodes = " << meshSource->GetNumberOfNodes() << std::endl;
  std::cout << "  - Cells = " << meshSource->GetNumberOfCells() << std::endl;

  // Release crack volumes
  toDilateVolume->Delete();
  dilatedVolume->Delete();
  toErodeVolume->Delete();
  erodedVolume->Delete();
  toMeshVolume->Delete();
  crack->Delete();

  // Import ITK mesh in VTK
  std::cout << "Converting ITK mesh to VTK ..." << std::endl;
  itkMeshTovtkPolyData* meshConverter = new itkMeshTovtkPolyData;
  meshConverter->SetInput( itkMesh );
  vtkSmartPointer<vtkPolyData> vtkMesh = meshConverter->GetOutput(); 
  //meshConverter->ConvertitkTovtk();
  //meshConverter->Update();

  // Release meshSource and ConnectedThresholdImageFilter
  meshSource->Delete();
  connectedThreshold->Delete();

  // Smooth filter
  vtkSmartPointer<vtkPolyData> toSmoothMesh = vtkMesh;
  vtkSmartPointer<vtkPolyData> smoothMesh;
  if (opt.smoothNumberOfIterations > 0) {
    std::cout << "Smoothing mesh surface ..." << std::endl;
    vtkSmartPointer<vtkSmoothPolyDataFilter> smoothPolyFilter =
      vtkSmartPointer<vtkSmoothPolyDataFilter>::New();
#if (VTK_MAJOR_VERSION > 5)
    smoothPolyFilter->SetInputData( toSmoothMesh );
#else
    smoothPolyFilter->SetInput( toSmoothMesh );
#endif
    smoothPolyFilter->SetNumberOfIterations( opt.smoothNumberOfIterations );
    smoothPolyFilter->Update();

    smoothMesh = smoothPolyFilter->GetOutput();
  }
  else smoothMesh = toSmoothMesh;
  //smoothMesh->Update();

  // Free some memory
  //meshConverter->Delete();

  // Decimation
  vtkSmartPointer<vtkPolyData> toDecimateMesh = smoothMesh;
  vtkSmartPointer<vtkPolyData> decimatedMesh;
  if (opt.decimRatio > 0) {
    std::cout << "Decimating mesh surface ..." << std::endl;
    std::cout << "  - ratio:        " << opt.decimRatio << std::endl;
    std::cout << "  - featureAngle: " << opt.decimFeatureAngle << std::endl;
    vtkSmartPointer<vtkDecimatePro> decimate =
      vtkSmartPointer<vtkDecimatePro>::New();
    // TEST IN
    //decimate->PreSplitMeshOn();
    // TEST OUT
    decimate->SetFeatureAngle( opt.decimFeatureAngle );
    decimate->SetTargetReduction( opt.decimRatio );
#if (VTK_MAJOR_VERSION > 5)
    decimate->SetInputData( smoothMesh );
#else
    decimate->SetInputConnection( smoothMesh->GetProducerPort() );
#endif

    try {
     decimate->Update();
    }
    catch(itk::ExceptionObject & EO) {
      std::cerr << "Exception caught!" << std::endl;
      std::cerr << EO << std::endl;
    }
    std::cout << "  After decimation:" << std::endl;
    std::cout << "  - Nodes = " << decimate->GetOutput()->GetNumberOfPoints()
      << std::endl;
    std::cout << "  - Cells = " << decimate->GetOutput()->GetNumberOfCells()
      << std::endl;
    decimatedMesh = decimate->GetOutput();
  }
  else decimatedMesh = toDecimateMesh;
  //decimatedMesh->Update();

  // Write mesh in VTK file
  vtkSmartPointer<vtkPolyData> finalMesh = decimatedMesh;
  if (opt.outVTKFile.size()) {
    WriteMeshVTK(finalMesh, opt.outVTKFile, opt.binaryVTK);
  }

  // Show mesh in VTK
  if (!opt.noShow) {
    ShowMeshVTK(finalMesh); 
  }

  // Release all ITK components
  itkMesh->Delete();

  // Release all VTK components
  vtkMesh->Delete();
  toSmoothMesh->Delete();
  smoothMesh->Delete();
  toDecimateMesh->Delete();
  decimatedMesh->Delete();
  finalMesh->Delete();
  //meshConverter->Delete();

  std::cout << "Exiting program ..." << std::endl;
  std::cout.flush();

  return EXIT_SUCCESS;
}

bool myProgOptions::Check()
{
  if (!ProgOptions::Check()) {
    return false;
  }
  return true;
}
