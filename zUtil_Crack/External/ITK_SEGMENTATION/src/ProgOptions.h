#ifndef __ProgOptions_h__
#define __ProgOptions_h__

// Basic includings
#include <fstream>
#include <iostream>
#include <iterator>
#include <vector>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

// Boost includings
#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>

namespace po = boost::program_options;
namespace fsys = boost::filesystem;

class ProgOptions {
  public:
  std::string inputImage;
  std::string maskImage;
  std::string outBinFile;
  std::string outVTKFile;
  bool verbose;
  bool noShow;
  bool binaryVTK;
  bool compressOutBin;
  unsigned short maskReplaceValue;
  int preNumberOfIterations;
  int segReplaceValue;
  int smoothNumberOfIterations;
  int postDilationRadius;
  int postErosionRadius;
  double scaleFactor;
  double voxelSize;
  double preTimeStep;
  double decimRatio;
  double decimFeatureAngle;
  std::vector<unsigned> segDilationRadius;
  std::vector<double> ROI;
  double segMinThreshold;
  double segMaxThreshold;
  std::vector<std::vector<double> > segSeeds;

  ProgOptions();
  virtual ~ProgOptions();
  virtual void Parse(int argc, char *argv[]);
  virtual bool Check();
  virtual void Summary();
};

struct bbox {
  public:
    std::vector<double> v;
    bbox() { v.resize(6,0); }
    bbox(std::vector<double> i) { assert(i.size()==6); v=i; }
    size_t size() { return v.size(); }
    double& operator[](int i) { return v[i]; }
    const double& operator[](int i) const { return v[i]; }
};

struct indices {
  public:
    std::vector<unsigned> v;
    indices( ) { v.resize(3,0); }
    indices( std::vector<unsigned> i ) { assert(i.size()==3); v=i; }
    size_t size() { return v.size(); }
    unsigned& operator[](int i) { return v[i]; }
    const unsigned& operator[](int i) const { return v[i]; }
};

struct coordinate {
  public:
    std::vector<double> v;
    coordinate( ) { v.resize(3,0); }
    coordinate( std::vector<double> i ) { assert(i.size()==3); v=i; }
    size_t size() { return v.size(); }
    double& operator[](int i) { return v[i]; }
    const double& operator[](int i) const { return v[i]; }
};

struct coordinates {
  public:
    std::vector<coordinate> L;
    coordinates( ) { L.resize(0); }
    size_t size() { return L.size(); }
    coordinate& operator[](int i) { return L[i]; }
    const coordinate& operator[](int i) const { return L[i]; }
};

// A helper function to simplify the main part.
template<class T> std::ostream& operator<<(std::ostream& os,
    const std::vector<T>& v)
{
  copy(v.begin(), v.end(), std::ostream_iterator<T>(std::cout, " "));
  return os;
}

// A helper function to simplify the main part.
template<class T> std::ostream& operator<<(std::ostream& os, std::vector<T>& v)
{
  copy(v.begin(), v.end(), std::ostream_iterator<T>(std::cout, " "));
  return os;
}

#endif
