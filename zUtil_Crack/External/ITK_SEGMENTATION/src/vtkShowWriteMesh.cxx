#include "vtkShowWriteMesh.h"

void WriteMeshVTK(vtkSmartPointer<vtkPolyData> mesh, const std::string& filename, bool binary)
{
  if (filename.size()) {
    vtkSmartPointer<vtkPolyDataWriter> writer = vtkPolyDataWriter::New();
    writer->SetFileName( filename.c_str() );
    if (binary) writer->SetFileTypeToBinary();
#if (VTK_MAJOR_VERSION > 5)
    writer->SetInputData( mesh );
#else
    writer->SetInputConnection( mesh->GetProducerPort() );
#endif
    try {
      std::cout << "Writing mesh in vtk file: " << filename << std::endl;
      writer->Write();
    }
    catch( itk::ExceptionObject& EO ) {
      std::cerr << "Exception catched !! " << std::endl;
      std::cerr << EO << std::endl;
    }
  }
  else std::cout << "No filename for vtk output file, so not writing mesh to vtk file..." << std::endl;
}

void ShowMeshVTK(vtkSmartPointer<vtkPolyData> mesh)
{
  // Assign default props to the actor
  vtkSmartPointer<vtkProperty> property = vtkProperty::New();
  //property->SetAmbient(0.1);
  //property->SetDiffuse(0.1);
  //property->SetSpecular(0.5);
  property->SetColor(1.0,1.0,1.0);
  property->SetLineWidth(2.0);
  property->SetRepresentationToSurface();

  // Poly data mapper
  vtkSmartPointer<vtkPolyDataMapper> polyMapper = vtkPolyDataMapper::New();
#if (VTK_MAJOR_VERSION > 5)
  polyMapper->SetInputData( mesh );
#else
  polyMapper->SetInputConnection( mesh->GetProducerPort() );
#endif

  // Poly data actor
  vtkSmartPointer<vtkActor> polyActor = vtkSmartPointer<vtkActor>::New();
  polyActor->SetMapper(polyMapper);

  polyActor->SetProperty( property );

  // Renderer
  vtkSmartPointer<vtkRenderer> renderer = vtkRenderer::New();
  renderer->SetBackground(0., 0., 0.);
  renderer->AddActor( polyActor );
  renderer->ResetCamera();

  // Render Window
  vtkSmartPointer<vtkRenderWindow> renWin = vtkRenderWindow::New();
  renWin->SetSize(500, 500);
  renWin->SetWindowName("Final mesh");
  renWin->AddRenderer(renderer);

  // Render Window Interactor
  vtkSmartPointer<vtkRenderWindowInteractor> iren = vtkRenderWindowInteractor::New();
  iren->SetRenderWindow(renWin);

  /*
  // Keypress callback for custom closing of vtk window
  vtkSmartPointer<vtkCallbackCommand> keypressCallback = vtkSmartPointer<vtkCallbackCommand>::New();
  keypressCallback->SetCallback ( KeypressCallbackFunction );
  iren->AddObserver(vtkCommand::KeyPressEvent, keypressCallback);
  */

  // Axes widget
  vtkSmartPointer<vtkAxesActor> axes = vtkSmartPointer<vtkAxesActor>::New();
  vtkSmartPointer<vtkOrientationMarkerWidget> widget = vtkSmartPointer<vtkOrientationMarkerWidget>::New();
  widget->SetOrientationMarker( axes );
  widget->SetInteractor( iren );
  widget->SetViewport( -0.2, -0.2, 0.3, 0.3 );
  widget->SetEnabled( 1 );
  widget->InteractiveOn();

  renWin->Render();

  try {
    // Bring up the render window and begin interaction.
    std::cout << "Showing resulting mesh in vtk window..." << std::endl;
    std::cout << std::endl;
    std::cout << ">> Press 'q' or 'e' to close the vtk window and quit the program. <<" << std::endl;
    std::cout << std::endl;
    iren->Start();
  }
  catch( itk::ExceptionObject& EO ) {
    std::cerr << "Exception catched !! " << std::endl;
    std::cerr << EO << std::endl;
  }

  if(iren != NULL) {
    iren->GetRenderWindow()->Finalize();
    iren->TerminateApp();
  }
}

/*
// Custom interator to close vtk window
void KeypressCallbackFunction(vtkObject* caller, long unsigned int vtkNotUsed(eventId), void* vtkNotUsed(clientData), void* vtkNotUsed(callData))
{
  vtkRenderWindowInteractor *iren = static_cast<vtkRenderWindowInteractor*>(caller);
  std::string key = iren->GetKeySym();

  if (key == "q" || key == "Q") {
    // Close the window
    iren->GetRenderWindow()->Finalize();

    std::cout << "Closing vtk window..." << std::endl;
    std::cout.flush();
 
    // Stop the interactor
    iren->TerminateApp();
  }
}
*/

