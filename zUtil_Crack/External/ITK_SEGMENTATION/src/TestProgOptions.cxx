// Basic includings
#include "ProgOptions.h"

using namespace std;

class myProgOptions : public ProgOptions {
  public:
    myProgOptions();
    bool Check();
};

myProgOptions::myProgOptions() : ProgOptions() { }

int main(int argc, char *argv[])
{
  myProgOptions opt;
  opt.Parse(argc, argv);
  opt.Check();
  opt.Summary();
  return EXIT_FAILURE;
}

bool myProgOptions::Check()
{
  cout << "ROI: " << ROI << endl;
  cout << "seeds:" << endl;
  for (std::vector<std::vector<typeof(segSeeds[0][0])> >::iterator it =
      segSeeds.begin(); it != segSeeds.end(); ++it) {
    cout << " " << *it << endl;
  }
  return true;
}

