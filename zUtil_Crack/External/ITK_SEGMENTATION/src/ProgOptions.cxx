#include "ProgOptions.h"

namespace po = boost::program_options;
namespace fsys = boost::filesystem;

using namespace std;

ProgOptions::ProgOptions() : verbose(false) { }

ProgOptions::~ProgOptions() { }

void ProgOptions::Parse(int argc, char *argv[]) 
{
  try {
    po::options_description po_general("General options");
    po_general.add_options()
      ("help,h", "print help message")
      ("inputImage,i",
       po::value<string>(&inputImage)->required(),
       "path to the input image file")
      ("maskImage,m",
       po::value<string>(&maskImage),
       "path to the input mask file")
      ("outBinFile,b",
       po::value<string>(&outBinFile),
       "name of the output TIF file for the binary volume of the crack")
      ("outVTKFile,o",
       po::value<string>(&outVTKFile),
       "name of the output VTK file for the crack mesh")
      ("binaryVTK,B",
       po::bool_switch(&binaryVTK)->default_value(false),
       "write vtk file in binary mode")
      ("compressOutBin,C",
       po::bool_switch(&compressOutBin)->default_value(false),
       "write binary volume of the crack with compression")
      ("ROI,R",
       //po::value<vector<double> >(&ROI)->multitoken(),
       po::value<bbox>()->multitoken(),
       "pixel indexes defining the ROI starts and ends")
      ("maskReplaceValue",
       po::value<unsigned short>(&maskReplaceValue)->default_value(255),
       "value set to voxels out of the mask")
      ("voxelSize,V",
       po::value<double>(&voxelSize)->required(),
       "voxel size of the input image (in µm)")
      ("scaleFactor,f",
       po::value<double>(&scaleFactor)->default_value(1.0),
       "resampling factor before segmentation")
      ("verbose,v",
       po::bool_switch(&verbose)->default_value(false),
       "verbose mode")
      ("noShow",
       po::bool_switch(&noShow)->default_value(false),
       "don't show resulting mesh")
      ;

    po::options_description po_preprocessing("Pre-processing options");
    po_preprocessing.add_options()
      ("preprocessing.numberOfIterations",
       po::value<int>(&preNumberOfIterations)->default_value(5),
       "number of iterations in the smoothing filter (CurvatureFlowImageFilter)")
      ("preprocessing.timeStep",
       po::value<double>(&preTimeStep)->default_value(0.125),
       "time step in the smoothing filter (CurvatureFlowImageFilter)")
      ;

    po::options_description po_segmentation("Segmentation options");
    po_segmentation.add_options()
      ("segmentation.minThreshold,t",
       po::value<double>(&segMinThreshold)->default_value(0),
       "min thresholds for region growing segmentation")
      ("segmentation.maxThreshold,T",
       po::value<double>(&segMaxThreshold)->default_value(100),
       "max thresholds for region growing segmentation")
      ("segmentation.seed,s",
       po::value<coordinates>()->multitoken()->composing(),
       "region growing segmentation seeds")
      ("segmentation.replaceValue,r",
       po::value<int>(&segReplaceValue)->default_value(255),
       "replacement value for segmented voxels")
      ("segmentation.dilationRadius",
       po::value<indices>()->multitoken(),
       "radius used when dilating crack volume for output")
      ;

    po::options_description po_postprocessing("Post-processing options");
    po_postprocessing.add_options()
      ("postprocessing.dilationRadius",
       po::value<int>(&postDilationRadius)->default_value(0),
       "radius of structural element used for dilation (1st step)")
      ("postprocessing.erosionRadius",
       po::value<int>(&postErosionRadius)->default_value(0),
       "radius of structural element used for erosion (2nd step)")
      ;

    po::options_description po_mesh("Mesh options");
    po_mesh.add_options()
      ("mesh.smoothNumberOfIterations,n",
       po::value<int>(&smoothNumberOfIterations)->default_value(1000),
       "number of iterations in the mesh smoothing process")
      ("mesh.decimationRatio,d",
       po::value<double>(&decimRatio)->default_value(0.5),
       "ratio of merged cells in the mesh decimation process")
      ("mesh.decimationFeatureAngle,a",
       po::value<double>(&decimFeatureAngle)->default_value(15),
       "feature angle (in degree) in the mesh decimation process")
      ;

    po::options_description po_config("");
    po_config.add_options()
      ("configFileName,c",   po::value<string>()->default_value(""),
       "configuration file containing parameters values")
      ;

    // Gather options
    po_general.add(po_config);
    po::options_description all_options("");
    all_options.add(po_general).add(po_preprocessing).add(po_segmentation);
    all_options.add(po_postprocessing).add(po_mesh);

    // Parse config file command line option, then parse config file, then all
    // command line option
    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, all_options,
          po::command_line_style::unix_style), vm);
    if (!vm["configFileName"].defaulted()) {
      string configFilename = vm["configFileName"].as<string>();
      if (verbose) cout << "Config file: " << configFilename << endl;
      ifstream configFile(configFilename.c_str());
      po::store(po::parse_config_file<char>(configFile, all_options), vm);
      configFile.close();
    }

    if (argc == 1 || vm.count("help")) {
      cerr << "Usage: " << fsys::basename(argv[0]) << " [options]" << endl;
      cerr << all_options << endl;
      exit(EXIT_FAILURE);
    }

    po::notify(vm);
    if (vm.count("ROI")) {
      ROI = vm["ROI"].as<bbox>().v;
    }
    if (vm.count("segmentation.dilationRadius")) {
      segDilationRadius = vm["segmentation.dilationRadius"].as<indices>().v;
    }
    if (vm.count("segmentation.seed")) {
      coordinates tmp = vm["segmentation.seed"].as<coordinates>();
      segSeeds.resize(tmp.size());
      for (size_t i=0; i<tmp.size(); i++) {
        segSeeds[i].resize(3);
        segSeeds[i][0] = tmp[i][0];
        segSeeds[i][1] = tmp[i][1];
        segSeeds[i][2] = tmp[i][2];
      }
    }
  }
  catch (std::exception& e) {
    cout << e.what() << endl;
    exit(EXIT_FAILURE);
  }
}

bool ProgOptions::Check()
{
  if (voxelSize < 0) {
    cerr << "INPUT ERROR: voxel size should be positive!";
    return false;
  }
  if (postDilationRadius < postErosionRadius) {
    cout << "WARNING in post-processing options:" << endl;
    cout << "  The erosion radius is bigger than the dilation one!" << endl;
    cout << "  You might lose lots of the crack features..." << endl;
  }
  return true;
}

void ProgOptions::Summary() {
  cout << "Options summary" << endl;
  cout << "- inputImage:               " << inputImage << endl;
  cout << "- maskImage:                " << maskImage << endl;
  cout << "- outBinFile:               " << outBinFile
    << (compressOutBin ? " (COMPRESSED)" : "") << endl;
  cout << "- outVTKFile:               " << outVTKFile
    << (binaryVTK ? " (BINARY)" : " (ASCII)") << endl;
  cout << "- voxelSize                 " << voxelSize << endl;
  cout << "- scaleFactor:              " << scaleFactor << endl;
  cout << "- maskReplaceValue:         " << maskReplaceValue << endl;
  cout << "- ROI:                      " << ROI << endl;

  cout << "[preprocessing]" << endl;
  cout << "- numberOfIterations:       " << preNumberOfIterations << endl;
  cout << "- timeStep:                 " << preTimeStep << endl;

  cout << "[segmentation]" << endl;    
  cout << "- minThreshold:             " << segMinThreshold << endl;
  cout << "- maxThreshold:             " << segMaxThreshold << endl;
  cout << "- replaceValue:             " << segReplaceValue << endl;
  cout << "- dilationRadius:           " << segDilationRadius << endl;
  cout << "- seeds:                    ";
  for (vector<vector<typeof(segSeeds[0][0])> >::iterator it=segSeeds.begin();
      it!=segSeeds.end(); ++it)
    cout << (it!=segSeeds.begin() ? "                     " : "") << *it << endl;

  cout << "[postprocessing]" << endl;
  cout << "- dilationRadius:           " << postDilationRadius << endl;
  cout << "- erosionRadius:            " << postErosionRadius << endl;

  cout << "[mesh]" << endl;    
  cout << "- smoothNumberOfIterations: " << smoothNumberOfIterations << endl;
  cout << "- decimationRatio:          " << decimRatio << endl;
  cout << "- decimationFeatureAngle:   " << decimFeatureAngle << endl;
  cout << endl;
}

static void validate(boost::any& val, const vector<string>& s, bbox*, int) {
  po::validators::check_first_occurrence(val);
  vector<double> dval;
  for (vector<string>::const_iterator it = s.begin(); it != s.end(); ++it) {
    stringstream ss(*it);
    copy(istream_iterator<double>(ss), istream_iterator<double>(),
        back_inserter(dval));
    if (!ss.eof())
      throw po::validation_error(po::validation_error::invalid_option_value,
          "Invalid bbox specification");
  }
  if(dval.size() != 6)
    throw po::validation_error(po::validation_error::invalid_option_value,
        "Invalid bbox specification");
  bbox b(dval);
  val = b;
}

static void validate(boost::any& val, const vector<string>& s, coordinates*,
    int) {
  vector<double> dval;
  for (vector<string>::const_iterator it = s.begin(); it != s.end(); ++it) {
    stringstream ss(*it);
    copy(istream_iterator<double>(ss), istream_iterator<double>(),
        back_inserter(dval));
    if (!ss.eof())
      throw po::validation_error(po::validation_error::invalid_option_value,
          "Invalid coordinates specification");
  }
  if (dval.size() % 3 !=0)
    throw po::validation_error(po::validation_error::invalid_option_value,
        "Invalid coordinates specification");
  coordinate c(dval);
  if (val.empty()) val = boost::any(coordinates());
  coordinates *l = boost::any_cast<coordinates>(&val);
  BOOST_ASSERT(l);
  l->L.push_back(c);
}

static void validate(boost::any& val, const vector<string>& s, indices*, int) {
  po::validators::check_first_occurrence(val);
  vector<unsigned> uval;
  for (vector<string>::const_iterator it = s.begin(); it != s.end(); ++it) {
    stringstream ss(*it);
    copy(istream_iterator<unsigned>(ss), istream_iterator<unsigned>(),
        back_inserter(uval));
    if (!ss.eof())
      throw po::validation_error(po::validation_error::invalid_option_value,
          "Invalid coordinate specification");
  }
  if (uval.size() != 3)
    throw po::validation_error(po::validation_error::invalid_option_value,
        "Invalid coordinate specification");
  indices c(uval);
  val = c;
}

/*
static void validate(boost::any& v, const vector<string>& values, coordinate*,
    int) {
  vector<double> dvalues;
  for (vector<string>::const_iterator it = values.begin(); it != values.end();
      ++it) {
    stringstream ss(*it);
    copy(istream_iterator<double>(ss), istream_iterator<double>(),
        back_inserter(dvalues));
    if (!ss.eof())
      throw po::validation_error(po::validation_error::invalid_option_value,
          "Invalid coordinate specification");
  }
  if (dvalues.size() != 3)
    throw po::validation_error(po::validation_error::invalid_option_value,
        "Invalid coordinate specification");
  coordinate c(dvalues);
  v = c;
}
*/

/*
struct coordinates2 {
  public:
    vector<std::string> v;
};
*/

/*
static void validate(boost::any& v, const vector<string>& tokens, coordinates2*,
    int) {
  if (v.empty()) v = boost::any(coordinates2());

  coordinates2 *p = boost::any_cast<coordinates2>(&v);
  BOOST_ASSERT(p);

  boost::char_separator<char> sep(",");
  BOOST_FOREACH(string const& t, tokens) {
    if (t.find(",")) {
      boost::tokenizer<boost::char_separator<char> > tok(t, sep);
      copy(tok.begin(), tok.end(),  back_inserter(p->v));
    }
    else {
      p->v.push_back(t);
    }
  }
}
*/
