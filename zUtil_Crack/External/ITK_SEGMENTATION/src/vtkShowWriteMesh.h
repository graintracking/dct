#ifndef __vtkShowWriteMesh_h__
#define __vtkShowWriteMesh_h__

//#include "itkExceptionObject.h"
#include "itkMacro.h"

#include "vtkSmartPointer.h"
#include "vtkPolyData.h"
#include "vtkPolyDataMapper.h"
#include "vtkRenderer.h"
#include "vtkRenderWindow.h"
#include "vtkRenderWindowInteractor.h"
#include "vtkProperty.h"
#include "vtkCallbackCommand.h"
#include "vtkCommand.h"
#include "vtkAxesActor.h"
#include "vtkOrientationMarkerWidget.h"

#include "vtkPolyDataWriter.h"

void WriteMeshVTK(vtkSmartPointer<vtkPolyData> mesh, const std::string& filename, bool binary=false);

void ShowMeshVTK(vtkSmartPointer<vtkPolyData> mesh);

// Custom interator to close vtk window
//void KeypressCallbackFunction(vtkObject* caller, long unsigned int eventId, void* clientData, void* callData);

#endif
