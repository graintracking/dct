// This program uses ITK and VTK libraries to:
// 1) read the binary volume in a TIF file              [optional]
// 2) compute the surface and convert it to an ITK mesh
// 3) convert it to a VTK mesh
// 4) smooth the mesh by a laplacian method             [optional]
// 5) decimate the mesh to reduce the number of cells   [optional]
// 6) write the mesh in a vtk file                      [optional]
// 7) display the resulting mesh in vtk

// ITK headers
#include "itkImage.h"
#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"
#include "itkTIFFImageIO.h"
#include "itkMaskImageFilter.h"
#include "itkBinaryErodeImageFilter.h"
#include "itkBinaryDilateImageFilter.h"
#include "itkBinaryBallStructuringElement.h"
#include "itkRegionOfInterestImageFilter.h"
#include "itkResampleImageFilter.h"
#include "itkCastImageFilter.h"
#include "itkLinearInterpolateImageFunction.h"
#include "itkCurvatureFlowImageFilter.h"
#include "itkConnectedThresholdImageFilter.h"
#include "itkBinaryMask3DMeshSource.h"
#include "itkBinaryBallStructuringElement.h"
#include "itkBinaryDilateImageFilter.h"
#include "itkMesh.h"

// ITK/VTK interface headers
#include "itkMeshTovtkPolyData.h"
#include "itkVTKImageExport.h"
#include "itkVTKImageImport.h"
#include "itkImportExportVTK.h"

// VTK headers
#include "vtkImageImport.h"
#include "vtkImageExport.h"
#include "vtkContourFilter.h"
#include "vtkSmartPointer.h"
#include "vtkSmoothPolyDataFilter.h"
#include "vtkDecimatePro.h"
//#include <vtkWindowedSincPolyDataFilter.h>

// Personal VTK mesh display headers 
#include "vtkShowWriteMesh.h"

int main(int argc, char * argv[] )
{
  if (strcmp(argv[1], "-h") == 0) {
    cout << "Usage: " << argv[0]
      << " inputImage  outVTKFile  decimRatio decimFeatureAngle " << endl;
    cout << "  [ decimSplitAngle  smoothNumberOfIterations  preDilationRadius preErosionRadius ]" << endl;
    return EXIT_SUCCESS;
  }

  cout << endl;
  cout << "**************************" << endl;
  cout << "****** Crack Remesh ******" << endl;
  cout << "**************************" << endl;
  cout << endl;

  // Pixel types
  typedef unsigned char InputPixelType;

  // Image types
  const unsigned int Dimension = 3;
  typedef itk::Image< InputPixelType,    Dimension > InputImageType;

  struct ProgOptions {
    std::string inputImage;
    std::string outVTKFile;
    bool noShow;

    int preDilationRadius;
    int preErosionRadius;

    unsigned int smoothNumberOfIterations;
    unsigned int maskReplaceValue;

    double decimSplitAngle;
    double decimFeatureAngle;
    double decimRatio;

    unsigned int smooth2NumberOfIterations;
  };

  // Fill options
  ProgOptions opt;
  opt.inputImage = argv[1];
  opt.outVTKFile = argv[2];
  opt.noShow = false;

  opt.preDilationRadius = 0;
  opt.preErosionRadius = 0;

  opt.smoothNumberOfIterations = 1000;
  opt.smooth2NumberOfIterations = 0;
  opt.maskReplaceValue = 255;

  opt.decimRatio        = atof(argv[3]);
  opt.decimFeatureAngle = atof(argv[4]);
  opt.decimSplitAngle   = -1;

  opt.smoothNumberOfIterations = 100;

  if (argc > 5) {
    opt.decimSplitAngle = atof(argv[5]);
  }
  if (argc > 6) {
    opt.smoothNumberOfIterations = atoi(argv[6]);
  }
  if (argc > 7) {
    opt.preDilationRadius = atoi(argv[7]);
  }
  if (argc > 8) {
    opt.preErosionRadius = atoi(argv[8]);
  }
  if (argc > 9) {
    opt.smooth2NumberOfIterations = atoi(argv[9]);
  }

  cout << "Options summary" << endl;
  cout << "- inputImage:                " << opt.inputImage << endl;
  cout << "- outVTKFile:                " << opt.outVTKFile << endl;
  cout << "- maskReplaceValue:          " << opt.maskReplaceValue << endl;
  cout << "[preprocessing]" << endl;
  cout << "- dilationRadius:            " << opt.preDilationRadius << endl;
  cout << "- erosionRadius:             " << opt.preErosionRadius << endl;
  cout << "[decimation]" << endl;
  cout << "- smoothNumberOfIterations:  " << opt.smoothNumberOfIterations << endl;
  cout << "- decimationRatio:           " << opt.decimRatio << endl;
  cout << "- decimationFeatureAngle:    " << opt.decimFeatureAngle << endl;
  cout << "- decimationSplitAngle:      " << opt.decimSplitAngle << endl;
  cout << "- smooth2NumberOfIterations: " << opt.smooth2NumberOfIterations << endl;
  cout << endl;

  // Reader
  typedef itk::ImageFileReader< InputImageType > ReaderType;
  ReaderType::Pointer reader = ReaderType::New();
  reader->SetFileName( opt.inputImage );
  try {
    std::cout << "Reading input volume ..." << std::endl; 
    std::cout << "  - from file: " << opt.inputImage << std::endl;
    reader->Update();
  }
  catch( itk::ExceptionObject & EO ) {
    std::cerr << "Exception thrown while reading input file" << std::endl;
    std::cerr << EO << std::endl;
    return EXIT_FAILURE;
  }
  InputImageType::Pointer inputVolume = reader->GetOutput();

  // Binary dilation/erosion filters
  typedef itk::BinaryBallStructuringElement < InputPixelType, Dimension >
    StructuringElementType;
  StructuringElementType structuringElement;

  // Dilate
  InputImageType::Pointer dilatedVolume;
  if ( opt.preDilationRadius > 0) {
    std::cout << "Dilating volume with radius " << opt.preDilationRadius
      << " ..." << std::endl; 
    typedef itk::BinaryDilateImageFilter < InputImageType, InputImageType,
          StructuringElementType > DilateFilterType;
    DilateFilterType::Pointer dilate = DilateFilterType::New();

    structuringElement.SetRadius( opt.preDilationRadius );
    structuringElement.CreateStructuringElement();
    dilate->SetKernel( structuringElement );
    dilate->SetInput( inputVolume );
    dilate->Update();

    dilatedVolume = dilate->GetOutput();
  }
  else dilatedVolume = inputVolume;
  dilatedVolume->Update();

  // Erode
  InputImageType::Pointer erodedVolume;
  if ( opt.preErosionRadius > 0) {
    std::cout << "Eroding volume with radius " << opt.preErosionRadius
      << " ..." << std::endl; 
    typedef itk::BinaryErodeImageFilter < InputImageType, InputImageType,
            StructuringElementType > ErodeFilterType;
    ErodeFilterType::Pointer erode = ErodeFilterType::New();

    structuringElement.SetRadius( opt.preErosionRadius );
    erode->SetKernel( structuringElement );
    erode->SetInput( dilatedVolume );
    erode->Update();

    erodedVolume = erode->GetOutput();
  }
  else erodedVolume = dilatedVolume;
  erodedVolume->Update();

  // Mesh
  std::cout << "Extracting mesh from the crack surface ..." << std::endl;
  typedef itk::DefaultDynamicMeshTraits< double, Dimension, Dimension, double,
          double > MeshTrait;
  typedef itk::Mesh<double, Dimension, MeshTrait> MeshType;

  typedef itk::BinaryMask3DMeshSource<InputImageType, MeshType> MeshSourceType;
  MeshSourceType::Pointer meshSource = MeshSourceType::New();
  meshSource->SetObjectValue( opt.maskReplaceValue );
  meshSource->SetInput( erodedVolume );

  try {
    meshSource->Update();
  }
  catch( itk::ExceptionObject & EO ) {
    std::cerr << "Exception thrown during Update() " << std::endl;
    std::cerr << EO << std::endl;
    return EXIT_FAILURE;
  }

  MeshType::Pointer itkMesh = meshSource->GetOutput();
  std::cout << "  After extraction:" << std::endl;
  std::cout << "  - Nodes = " << meshSource->GetNumberOfNodes() << std::endl;
  std::cout << "  - Cells = " << meshSource->GetNumberOfCells() << std::endl;

  // Import ITK mesh in VTK
  std::cout << "Converting ITK mesh to VTK ..." << std::endl;
  itkMeshTovtkPolyData* meshConverter = new itkMeshTovtkPolyData;
  meshConverter->SetInput( itkMesh );
  vtkSmartPointer<vtkPolyData> vtkMesh = meshConverter->GetOutput(); 
  //meshConverter->ConvertitkTovtk();
  //meshConverter->Update();

  // Release meshSource
  meshSource->Delete();

  // Smooth filter
  vtkSmartPointer<vtkPolyData> smoothMesh;
  if (opt.smoothNumberOfIterations > 0) {
    std::cout << "Smoothing mesh surface ..." << std::endl;
    vtkSmartPointer<vtkSmoothPolyDataFilter> smoothPolyFilter =
      vtkSmartPointer<vtkSmoothPolyDataFilter>::New();
#if (VTK_MAJOR_VERSION > 5)
    smoothPolyFilter->SetInputData( vtkMesh );
#else
    smoothPolyFilter->SetInput( vtkMesh );
#endif
    smoothPolyFilter->SetNumberOfIterations( opt.smoothNumberOfIterations );
    smoothPolyFilter->Update();

    smoothMesh = smoothPolyFilter->GetOutput();
  }
  else smoothMesh = vtkMesh;

  // Free some memory
  //meshConverter->Delete();

  // Decimation
  vtkSmartPointer<vtkPolyData> decimMesh;
  if (opt.decimRatio > 0) {
    std::cout << "Decimating mesh surface ..." << std::endl;
    std::cout << "  - ratio:        " << opt.decimRatio << std::endl;
    std::cout << "  - featureAngle: " << opt.decimFeatureAngle << std::endl;
    vtkSmartPointer<vtkDecimatePro> decimate =
      vtkSmartPointer<vtkDecimatePro>::New();
    decimate->SetFeatureAngle( opt.decimFeatureAngle );
    decimate->SetTargetReduction( opt.decimRatio );
#if (VTK_MAJOR_VERSION > 5)
    decimate->SetInputData( smoothMesh );
#else
    decimate->SetInputConnection( smoothMesh->GetProducerPort() );
#endif
    // TEST IN
    decimate->PreSplitMeshOn();
    // TEST OUT
    if (opt.decimSplitAngle > 0) {
      std::cout << "  - splitAngle:   " << opt.decimSplitAngle << std::endl;
      decimate->SetSplitAngle( opt.decimSplitAngle );
      decimate->SplittingOn();
    }

    try {
     decimate->Update();
    }
    catch(itk::ExceptionObject & EO) {
      std::cerr << "Exception caught!" << std::endl;
      std::cerr << EO << std::endl;
    }
    std::cout << "  After decimation:" << std::endl;
    std::cout << "  - Nodes = " << decimate->GetOutput()->GetNumberOfPoints()
      << std::endl;
    std::cout << "  - Cells = " << decimate->GetOutput()->GetNumberOfCells()
      << std::endl;
    decimMesh = decimate->GetOutput();
  }
  else decimMesh = smoothMesh;

  // Final smooth filter
  vtkSmartPointer<vtkPolyData> finalMesh;
  if (opt.smooth2NumberOfIterations > 0) {
    std::cout << "Smoothing final mesh surface ..." << std::endl;
    vtkSmartPointer<vtkSmoothPolyDataFilter> smoothPolyFilter =
      vtkSmartPointer<vtkSmoothPolyDataFilter>::New();
#if (VTK_MAJOR_VERSION > 5)
    smoothPolyFilter->SetInputData( decimMesh );
#else
    smoothPolyFilter->SetInput( decimMesh );
#endif
    smoothPolyFilter->SetNumberOfIterations( opt.smooth2NumberOfIterations );
    smoothPolyFilter->Update();

    finalMesh = smoothPolyFilter->GetOutput();
  }
  else finalMesh = vtkMesh;

  // Write mesh in VTK file
  if (opt.outVTKFile.size()) {
    WriteMeshVTK(finalMesh, opt.outVTKFile, true);
  }

  // Show mesh in VTK
  if (!opt.noShow) {
    ShowMeshVTK(finalMesh); 
  }

  // Release all ITK components
  itkMesh->Delete();

  // Release all VTK components
  vtkMesh->Delete();
  smoothMesh->Delete();
  decimMesh->Delete();
  finalMesh->Delete();

  std::cout << "Exiting program ..." << std::endl;
  std::cout.flush();

  return EXIT_SUCCESS;
}

