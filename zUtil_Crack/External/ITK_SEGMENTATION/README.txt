This project build some libraries and executables to process to crack
segmentation using ITK and VTK libraries.

Prequisites:
- cmake          (>= 2.6)
- boost          (>= 1.42, program-options, system and filesystem modules,
                 libboost-program-options-dev, libboost-system-dev and 
                 libboost-filesystem-dev packages on Debian6)
- InsightToolKit (>= 3.18, libinsighttoolkit3-dev package on Debian6)
- vtk            (>= 5.4, libvtk5-dev package on Debian6)
- gdcm           (>=?,    libgdcm2-dev on Debian6)

To build the program, here are the following steps.

1) From the main directory (DIR), create a new directory called Build and enter
in it.

$ mkdir Build && cd Build

2) Configure the makefiles with cmake

$ cmake ..

3) Build the executables

$ make

4) You can also install the executable into your favorite bin directory.
Don't forget to set it in the ${DIR}/CMakeLists.txt file before running cmake.

$ make install

