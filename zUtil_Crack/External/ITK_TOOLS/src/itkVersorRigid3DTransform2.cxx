// Program to apply simple rigid 3D transform to volume stored in TIF file

#include "itkImage.h"
#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"
#include "itkTIFFImageIO.h"
#include "itkResampleImageFilter.h"
#include "itkVersorRigid3DTransform.h"
#include "itkCenteredTransformInitializer.h"
#include "itkLinearInterpolateImageFunction.h"

#define DEFAULT_PIXEL_VALUE 0

using namespace std;

int main( int argc, char * argv[] )
{
  if(argc < 9) {
    cerr << "Usage: " << endl;
    cerr << argv[0] << " inputImageFile outputImageFile "
      << "transX transY transZ versorX versorY versorZ "
      << "[defaultPixelValue]" << endl;
    return EXIT_FAILURE;
  }

  // Dimension and pixel types
  const   unsigned int  Dimension = 3;
  typedef unsigned char InputPixelType;
  typedef unsigned char OutputPixelType;

  // Input and output image types
  typedef itk::Image< InputPixelType,  Dimension > InputImageType;
  typedef itk::Image< OutputPixelType, Dimension > OutputImageType;

  // Reader
  typedef itk::ImageFileReader< InputImageType > ReaderType;
  ReaderType::Pointer reader = ReaderType::New();
  reader->SetFileName(argv[1]);

  // Read image
  try {
    reader->Update();
  }
  catch(itk::ExceptionObject& EO) {
    cerr << "Exception caught !" << endl;
    cerr << EO << endl;
  }
  InputImageType::ConstPointer inputImage = reader->GetOutput();

  // Setup transform
  typedef itk::VersorRigid3DTransform<double> TransformType;
  TransformType::Pointer transform = TransformType::New();

  // Initialize transform
  typedef itk::CenteredTransformInitializer<TransformType, InputImageType, 
          OutputImageType> TransformInitializerType;
  TransformInitializerType::Pointer initializer = TransformInitializerType::New();
  initializer->SetTransform(transform);
  initializer->SetFixedImage(inputImage);
  initializer->SetMovingImage(inputImage);
  //initializer->MomentsOn(); // Center of mass
  initializer->GeometryOn(); // Geometric center
  initializer->InitializeTransform();

  // Setup initial rotation and translation of the transform
  typedef TransformType::VersorType VersorType;
  typedef VersorType::VectorType    VectorType;
  VectorType rotAxis;   // Axis which will be rotated
  VectorType transAxis; // Initial translation
  VersorType rotation;  // Initial rotation
  for(unsigned int idim=0; idim<Dimension; idim++) {
    transAxis[idim] = atof(argv[3+idim]);
    rotAxis[idim]   = atof(argv[6+idim]);
  }
  rotation.Set(rotAxis);

  cout << "The volume will be transformed with:" << endl;
  cout << "- translation    : " << transAxis << endl;
  cout << "- rotation axis  : " << rotAxis << endl;

  // Setup the transform
  //transform->SetIdentity();
  transform->SetRotation(rotation);
  transform->SetTranslation(transAxis);
  cout << "Parameters:" << endl;
  cout << "- Versor      = " << transform->GetVersor() << endl;
  cout << "- Translation = " << transform->GetTranslation() << endl;
  cout << "- Offset      = " << transform->GetOffset() << endl;
  cout << "- Center      = " << transform->GetCenter() << endl;

  // Setup interpolator (linear)
  typedef itk::LinearInterpolateImageFunction<InputImageType, double>
    InterpolatorType;
  InterpolatorType::Pointer interpolator = InterpolatorType::New();

  // Default value for regions without source
  unsigned int defaultPixelValue = DEFAULT_PIXEL_VALUE;
  if(argc > 9) {
    defaultPixelValue = atoi(argv[9]);
    if(defaultPixelValue < 0) {
      cout << "WARNING: you have set default pixel value to "
        << defaultPixelValue << " which should be between [0;255]" << endl;
      defaultPixelValue = 0;
      cout << "WARNING: it has been set to " << defaultPixelValue << endl;
    }
    else if(defaultPixelValue > 255) {
      cout << "WARNING: you have set default pixel value to "
        << defaultPixelValue << " which should be between [0;255]"<< endl;
      defaultPixelValue = 255;
      cout << "WARNING: it has been set to " << defaultPixelValue << endl;
    }
  }

  // Setup resampler
  typedef itk::ResampleImageFilter<InputImageType, OutputImageType>
    ResampleFilterType;
  ResampleFilterType::Pointer resampler = ResampleFilterType::New();
  resampler->SetTransform(transform);
  resampler->SetInterpolator(interpolator);
  resampler->SetDefaultPixelValue(defaultPixelValue);
  resampler->SetInput(inputImage);
  resampler->SetSize(inputImage->GetLargestPossibleRegion().GetSize());

  // Setting writer
  typedef itk::TIFFImageIO IOType;
  IOType::Pointer io = IOType::New();
  io->SetCompressionToLZW();
  typedef itk::ImageFileWriter< OutputImageType > WriterType;
  WriterType::Pointer writer = WriterType::New();
  writer->SetImageIO(io);
  writer->SetInput(resampler->GetOutput());
  writer->SetUseCompression(true);
  writer->SetFileName(argv[2]);
  try { 
    writer->Update();
  }
  catch(itk::ExceptionObject& EO) {
    cout << "Exception caught!" << endl;
    cout << EO << endl;
  }
  cout << "Transform done!" << endl;

  return EXIT_SUCCESS;
}
