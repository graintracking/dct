#include "itkImage.h"
#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"
#include "itkTIFFImageIO.h"
#include "itkRescaleIntensityImageFilter.h"

using namespace std;

int main(int argc, char* argv[])
{
  if(argc < 5)
  {
    cerr << "Usage: " << endl;
    cerr << argv[0] << " inputImageFile outputImageFile min max" << endl;
    return EXIT_FAILURE;
  }

  // Dimension and pixel types
  const   unsigned int  Dimension = 3;
  typedef unsigned char InputPixelType;
  typedef unsigned char OutputPixelType;

  // Input and output image types
  typedef itk::Image< InputPixelType,  Dimension > InputImageType;
  typedef itk::Image< OutputPixelType, Dimension > OutputImageType;

  // Reader
  typedef itk::ImageFileReader<InputImageType> ReaderType;
  ReaderType::Pointer reader = ReaderType::New();
  reader->SetFileName(argv[1]);
 
  // Read image
  try { 
    reader->Update();
  }
  catch(itk::ExceptionObject& EO) {
    cerr << "Exception caught !" << endl;
    cerr << EO << endl;
  }
  InputImageType::ConstPointer inputImage = reader->GetOutput(); 
  
  typedef itk::RescaleIntensityImageFilter<InputImageType, OutputImageType> 
    FilterType;
  FilterType::Pointer filter = FilterType::New();
  filter->SetInput(inputImage);
  filter->SetOutputMinimum(atoi(argv[3]));
  filter->SetOutputMaximum(atoi(argv[4]));
  filter->Update();

  // Setting writer
  typedef itk::TIFFImageIO IOType;
  IOType::Pointer io = IOType::New();
  io->SetCompressionToLZW();
  typedef itk::ImageFileWriter< OutputImageType > WriterType;
  WriterType::Pointer writer = WriterType::New();
  writer->SetImageIO(io);
  writer->SetInput(filter->GetOutput());
  writer->SetUseCompression(true);
  writer->SetFileName(argv[2]);
  try { 
    writer->Update();
  }
  catch(itk::ExceptionObject& EO) {
    cout << "Exception caught!" << endl;
    cout << EO << endl;
  }

  try { 
    writer->Update();
  }
  catch(itk::ExceptionObject& EO) {
    cout << "Exception caught!" << endl;
    cout << EO << endl;
  }

  return EXIT_SUCCESS;
}
