// Program to apply simple rigid 3D transform to volume stored in TIF file

#include "itkImage.h"
#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"
#include "itkTIFFImageIO.h"
#include "itkResampleImageFilter.h"
#include "itkTransformFileReader.h"
#include "itkVersorRigid3DTransform.h"
#include "itkNearestNeighborInterpolateImageFunction.h"
#include "itkLinearInterpolateImageFunction.h"

#define DEFAULT_PIXEL_VALUE 0

using namespace std;

int main(int argc, char * argv[])
{
  if(argc < 4) {
    cerr << "Usage: " << endl;
    cerr << argv[0] << " inputImageFile outputImageFile "
      << "inputTransformFile [defaultPixelValue]" << endl;
    return EXIT_FAILURE;
  }

  // Dimension and pixel types
  const   unsigned int  Dimension = 3;
  typedef unsigned char InputPixelType;
  typedef unsigned char OutputPixelType;

  // Input and output image types
  typedef itk::Image< InputPixelType,  Dimension > InputImageType;
  typedef itk::Image< OutputPixelType, Dimension > OutputImageType;

  // Reader
  typedef itk::ImageFileReader< InputImageType > ReaderType;
  ReaderType::Pointer reader = ReaderType::New();
  reader->SetFileName(argv[1]);

  // Read image
  try {
    reader->Update();
  }
  catch(itk::ExceptionObject& EO) {
    cerr << "Exception caught !" << endl;
    cerr << EO << endl;
  }
  InputImageType::ConstPointer inputImage = reader->GetOutput();

  // Setup transform reader
  itk::TransformFileReader::Pointer transReader;
  transReader = itk::TransformFileReader::New();
  transReader->SetFileName( argv[3] );
  try {
    transReader->Update();
  }
  catch( itk::ExceptionObject& EO) {
    cerr << "Error while reading the transform file" << endl;
    cerr << EO << endl;
    return EXIT_FAILURE;
  }

  // Get list of transforms
  typedef itk::TransformFileReader::TransformListType TransformListType;
  TransformListType* transforms = transReader->GetTransformList();
  cout << "Number of transforms = " << transforms->size() << endl;

  typedef itk::VersorRigid3DTransform<double> TransformType;
  TransformType::Pointer transform = TransformType::New();
  for (TransformListType::const_iterator it = transforms->begin(); it!= transforms->end(); ++it) {
    if(!strcmp((*it)->GetNameOfClass(),"VersorRigid3DTransform")) {
      transform = static_cast<TransformType*>((*it).GetPointer());
      transform->Print(cout);
    }
  }

  // Setup interpolator (linear)
  //typedef itk::LinearInterpolateImageFunction<InputImageType, double>
  //  InterpolatorType;
  typedef itk::NearestNeighborInterpolateImageFunction<InputImageType, double>
    InterpolatorType;
  InterpolatorType::Pointer interpolator = InterpolatorType::New();

  // Default value for regions without source
  unsigned int defaultPixelValue = DEFAULT_PIXEL_VALUE;
  if(argc > 10) {
    defaultPixelValue = atoi(argv[10]);
    if(defaultPixelValue < 0) {
      cout << "WARNING: you have set default pixel value to "
        << defaultPixelValue << " which should be between [0;255]" << endl;
      defaultPixelValue = 0;
      cout << "WARNING: it has been set to " << defaultPixelValue << endl;
    }
    else if(defaultPixelValue > 255) {
      cout << "WARNING: you have set default pixel value to "
        << defaultPixelValue << " which should be between [0;255]"<< endl;
      defaultPixelValue = 255;
      cout << "WARNING: it has been set to " << defaultPixelValue << endl;
    }
  }

  // Setup resampler
  typedef itk::ResampleImageFilter<InputImageType, OutputImageType>
    ResampleFilterType;
  ResampleFilterType::Pointer resampler = ResampleFilterType::New();
  resampler->SetTransform(transform);
  resampler->SetInterpolator(interpolator);
  resampler->SetDefaultPixelValue(defaultPixelValue);
  resampler->SetInput(inputImage);
  resampler->SetSize(inputImage->GetLargestPossibleRegion().GetSize());

  // Setup writer
  typedef itk::TIFFImageIO IOType;
  IOType::Pointer io = IOType::New();
  io->SetCompressionToLZW();
  typedef itk::ImageFileWriter< OutputImageType > WriterType;
  WriterType::Pointer writer = WriterType::New();
  writer->SetImageIO(io);
  writer->SetInput(resampler->GetOutput());
  writer->SetUseCompression(true);
  writer->SetFileName(argv[2]);
  try {
    writer->Update();
  }
  catch(itk::ExceptionObject& EO) {
    cout << "Exception caught !" << endl;
    cout << EO << endl;
  }
  cout << "Transform done !" << endl;

  return EXIT_SUCCESS;
}
