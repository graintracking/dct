#include "itkImage.h"
#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"
#include "itkTIFFImageIO.h"
#include "itkNormalizeImageFilter.h"
#include "itkStatisticsImageFilter.h"

// #include "QuickView.h"

#include <iomanip>

using namespace std;

int main(int argc, char *argv[])
{
  if(argc < 3) {
    cerr << "Usage: " << argv[0] << " inputImageFile outputImageFile" << endl;
    return EXIT_FAILURE;
  }

  // Set the dimension and pixel types
  const unsigned int    Dimension = 3;
  typedef unsigned char InputPixelType;
  typedef unsigned char OutputPixelType;
  
  // Set the input, output and internal image types
  typedef itk::Image<InputPixelType,  Dimension> InputImageType;
  typedef itk::Image<OutputPixelType, Dimension> OutputImageType;

  typedef itk::ImageFileReader<InputImageType> ReaderType;
  ReaderType::Pointer reader = ReaderType::New();
  reader->SetFileName(argv[1]);

  typedef itk::NormalizeImageFilter<InputImageType, OutputImageType>
    NormalizeFilterType;
  NormalizeFilterType::Pointer normalizeFilter = NormalizeFilterType::New();
  normalizeFilter->SetInput(reader->GetOutput());

  // Set statistics on the input and output images
  typedef itk::StatisticsImageFilter< InputImageType > StatisticsFilterType;

  StatisticsFilterType::Pointer statistics1 = StatisticsFilterType::New();
  statistics1->SetInput(reader->GetOutput());
  statistics1->Update();

  StatisticsFilterType::Pointer statistics2 = StatisticsFilterType::New();
  statistics2->SetInput(normalizeFilter->GetOutput());
  statistics2->Update();

  // Write output image
  typedef itk::TIFFImageIO IOType;
  IOType::Pointer io = IOType::New();
  io->SetCompressionToLZW();
  typedef itk::ImageFileWriter< OutputImageType > WriterType;
  WriterType::Pointer writer = WriterType::New();
  writer->SetImageIO(io);
  writer->SetInput(normalizeFilter->GetOutput());
  writer->SetUseCompression(true);
  writer->SetFileName(argv[2]);
  try { 
    writer->Update();
  }
  catch(itk::ExceptionObject& EO) {
    cout << "Exception caught !" << endl;
    cout << EO << endl;
  }

  cout << itksys::SystemTools::GetFilenameName(argv[1])
    << "\nMean: " << statistics1->GetMean()
    << " Variance: " << statistics1->GetVariance() << endl;

  cout << itksys::SystemTools::GetFilenameName(argv[2])
    << "\nMean: " << fixed << setprecision (2) << statistics2->GetMean()
    << " Variance: " << statistics2->GetVariance() << endl;
  
  /*
  QuickView viewer;

  stringstream desc1;
  statistics1->Update();
  desc1 << itksys::SystemTools::GetFilenameName(argv[1])
    << "\nMean: " << statistics1->GetMean()
    << " Variance: " << statistics1->GetVariance();
  viewer.AddImage(reader->GetOutput(), true, desc1.str());  

  stringstream desc2;
  statistics2->Update();
  desc2 << "Normalize"
    << "\nMean: " << fixed << setprecision (2) << statistics2->GetMean()
    << " Variance: " << statistics2->GetVariance();
  viewer.AddImage(normalizeFilter->GetOutput(), true, desc2.str());  

  viewer.Visualize();
  */

  return EXIT_SUCCESS;
}
