// This is a little program to resize and crop a volume.
// The aim is to fit it to another volume with a different pixel size.
// Users have to set the scale factor and the final wanted volume size.

#include "itkImage.h"
#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"
#include "itkTIFFImageIO.h"
#include "itkResampleImageFilter.h"
#include "itkIdentityTransform.h"
#include "itkLinearInterpolateImageFunction.h"
#include "itkCastImageFilter.h"

#define DEFAULT_PIXEL_VALUE 0

using namespace std;

int main( int argc, char * argv[] )
{
  if(argc < 7) {
    cerr << "Usage: " << endl;
    cerr << argv[0] << " inputImageFile outputImageFile scaleFactor sizeX sizeY"
      << " sizeZ [defaultPixelValue]" << endl; 
    return EXIT_FAILURE;
  }

  // Dimension and pixel types
  const   unsigned int  Dimension = 3;
  typedef unsigned char InputPixelType;
  typedef unsigned char OutputPixelType;

  // Input and output image types
  typedef itk::Image< InputPixelType,  Dimension > InputImageType;
  typedef itk::Image< OutputPixelType, Dimension > OutputImageType;

  // Reader
  typedef itk::ImageFileReader< InputImageType > ReaderType;
  ReaderType::Pointer reader = ReaderType::New();
  reader->SetFileName(argv[1]);

  // Scaling factor
  const double factor = atof(argv[3]);

  // Read image
  try { 
    reader->Update();
  }
  catch(itk::ExceptionObject& EO) {
    cerr << "Exception caught !" << endl;
    cerr << EO << endl;
  }
  InputImageType::ConstPointer inputImage = reader->GetOutput();

  // Getting input image spacing
  const InputImageType::SpacingType& inputSpacing = inputImage->GetSpacing();
  cout << "Input spacing is :" << endl;
  cout << "- X : " << inputSpacing[0] << endl;
  cout << "- Y : " << inputSpacing[1] << endl;
  cout << "- Z : " << inputSpacing[2] << endl;

  // Set transformation to identity
  typedef itk::IdentityTransform<double, Dimension> TransformType;
  TransformType::Pointer transform = TransformType::New();
  transform->SetIdentity();

  // Set interpolator (linear)
  typedef itk::LinearInterpolateImageFunction<InputImageType, double>
    InterpolatorType;
  InterpolatorType::Pointer interpolator = InterpolatorType::New();

  // Default value for regions without source
  unsigned int defaultPixelValue = DEFAULT_PIXEL_VALUE;
  if(argc > 7) {
    defaultPixelValue = atoi(argv[7]);
    if(defaultPixelValue < 0) {
      cout << "WARNING: you have set default pixel value to "
        << defaultPixelValue << " which should be between [0;255]" << endl;
      defaultPixelValue = 0;
      cout << "WARNING: it has been set to " << defaultPixelValue << endl;
    }
    else if(defaultPixelValue > 255) {
      cout << "WARNING: you have set default pixel value to "
        << defaultPixelValue << " which should be between [0;255]"<< endl;
      defaultPixelValue = 255;
      cout << "WARNING: it has been set to " << defaultPixelValue << endl;
    }
  }

  // Set ouput image spacing
  OutputImageType::SpacingType spacing;
  for(unsigned int idim=0; idim<Dimension; idim++) 
    spacing[idim] = inputSpacing[idim] / factor;

  // Input image size
  typedef InputImageType::SizeType SizeType;
  SizeType inputSize = inputImage->GetLargestPossibleRegion().GetSize();

  // Set output image size
  SizeType size;
  double* newsize = new double[Dimension];
  for(unsigned int idim=0; idim<Dimension; idim++) {
    newsize[idim] = inputSize[idim] * factor;
    size[idim] = atoi(argv[4 + idim]);
  }
  cout << "Image will be resized from " << endl 
    << inputSize[0] << " x " 
    << inputSize[1] << " x "
    << inputSize[2] << " pixels" << endl;
  cout << "to " << endl
    << newsize[0] << " x "
    << newsize[1] << " x "
    << newsize[2] << " pixels" << endl;
  cout << endl;

  // Origin and direction
  InputImageType::PointType origin = inputImage->GetOrigin();

  int startPixel[Dimension];
  int endPixel[Dimension];
  for(unsigned int idim=0; idim<Dimension; idim++) {
    startPixel[idim] = int(newsize[idim] - size[idim])/2;
    origin[idim] = startPixel[idim] * spacing[idim];
    endPixel[idim] = startPixel[idim] + size[idim];
  }
  cout << "Image will be cropped from pixel " << endl 
    << startPixel[0] << " ; "
    << startPixel[1] << " ; "
    << startPixel[2] << endl;
  cout << "to" << endl 
    << endPixel[0] << " ; "
    << endPixel[1] << " ; "
    << endPixel[2] << endl;
  cout << endl;
  cout << "Default pixel value is : " << defaultPixelValue << endl;

  // Resampling filter
  typedef itk::ResampleImageFilter<InputImageType, OutputImageType>
    ResampleFilterType;
  ResampleFilterType::Pointer resampler = ResampleFilterType::New();

  // Input of the resampler is the output of the reader
  resampler->SetInput(reader->GetOutput());
  resampler->SetTransform(transform);
  resampler->SetInterpolator(interpolator);
  resampler->SetDefaultPixelValue(defaultPixelValue);
  resampler->SetOutputSpacing(spacing);
  resampler->SetSize(size);
  resampler->SetOutputOrigin(origin);
  resampler->SetOutputDirection(inputImage->GetDirection());

  // Setting writer
  typedef itk::TIFFImageIO IOType;
  IOType::Pointer io = IOType::New();
  io->SetCompressionToLZW();
  typedef itk::ImageFileWriter< OutputImageType > WriterType;
  WriterType::Pointer writer = WriterType::New();
  writer->SetImageIO(io);
  writer->SetInput(resampler->GetOutput());
  writer->SetUseCompression(true);
  writer->SetFileName(argv[2]);
  try { 
    writer->Update();
  }
  catch(itk::ExceptionObject& EO) {
    cout << "Exception caught!" << endl;
    cout << EO << endl;
  }
  cout << "Resampling done!" << endl;

  return EXIT_SUCCESS;
}

