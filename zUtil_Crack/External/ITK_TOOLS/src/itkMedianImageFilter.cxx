#include "itkImage.h"
#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"
#include "itkTIFFImageIO.h"
#include "itkCastImageFilter.h"
#include "itkMedianImageFilter.h"

using namespace std;

int main( int argc, char * argv[] )
{
  if(argc < 3)
  {
    cerr << "Usage: " << endl;
    cerr << argv[0] << " inputImageFile outputImageFile [radius]" << endl;
    return EXIT_FAILURE;
  }

  // Set the dimension and pixel types
  const unsigned int    Dimension = 3;
  typedef unsigned char InputPixelType;
  typedef unsigned char OutputPixelType;

  // Set the input, output and internal image types
  typedef itk::Image<InputPixelType,  Dimension> InputImageType;
  typedef itk::Image<OutputPixelType, Dimension> OutputImageType;

  typedef itk::ImageFileReader<InputImageType > ReaderType;
  ReaderType::Pointer reader = ReaderType::New();
  reader->SetFileName(argv[1]);

  InputImageType::SizeType indexRadius;

  long unsigned int radius = 1;
  if(argc > 3) radius = atoi(argv[3]);
  indexRadius.Fill(radius);

  OutputImageType::Pointer outImage;

  typedef itk::MedianImageFilter<InputImageType, OutputImageType> FilterType;
  typedef itk::CastImageFilter<InputImageType, OutputImageType> CasterType;
  FilterType::Pointer filter = FilterType::New();
  CasterType::Pointer caster = CasterType::New();
  if(radius > 0) {
    filter->SetRadius(indexRadius);
    filter->SetInput(reader->GetOutput());
    outImage = filter->GetOutput();
  }
  else {
    caster->SetInput(reader->GetOutput());
    outImage = caster->GetOutput();
  }

  // Setting writer
  typedef itk::TIFFImageIO IOType;
  IOType::Pointer io = IOType::New();
  io->SetCompressionToLZW();
  typedef itk::ImageFileWriter<OutputImageType> WriterType;
  WriterType::Pointer writer = WriterType::New();
  writer->SetImageIO(io);
  writer->SetFileName(argv[2]);
  writer->SetInput(outImage);
  writer->SetUseCompression(true);
  try { 
    writer->Update();
  }
  catch(itk::ExceptionObject& EO) {
    cout << "Exception caught !" << endl;
    cout << EO << endl;
  }

  return EXIT_SUCCESS;
}

