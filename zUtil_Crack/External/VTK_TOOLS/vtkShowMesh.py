#!/usr/bin/env python

import vtk
import sys

# Show vtk version
print "Using vtk version", vtk.vtkVersion.GetVTKVersion()

# Parameters
initAngle = -45.

# Set input filename
try :
    meshFile = sys.argv[1]
except :
    print 'Missing input file'

# Reader
meshExt = meshFile.rpartition('.')[2]
if meshExt == 'vtu' :
    reader = vtk.vtkXMLUnstructuredGridReader()
elif meshExt == 'vtp' :
    reader = vtk.vtkXMLPolyDataReader()
elif meshExt == 'vtk' :
    reader = vtk.vtkDataSetReader()
else :
    raise RuntimeError('Input file extension is not supported (%s)!' % meshExt)
readerType = reader.GetClassName()
dataType = readerType[0:readerType.rindex('Reader')]
print 'Reading a %s from a %s file...' % (dataType, meshExt.upper())
reader.SetFileName(meshFile)

# Number of scalar fields
if reader.IsA('vtkDataSetReader') :
    #W= reader.GetNumberOfScalarsInFile()
    #r = vtk.vtkInstantiator().CreateInstance(reader.GetClassName())
    r = vtk.vtkDataSetReader()
    r.SetFileName(reader.GetFileName())
    r.ReadAllScalarsOn()
    r.ReadAllColorScalarsOn()
    r.Update()
    d = r.GetOutput()
    nScalarFields = d.GetPointData().GetNumberOfArrays() + d.GetCellData().GetNumberOfArrays()
else :
    nScalarFields = reader.GetNumberOfPointArrays() + reader.GetNumberOfCellArrays()
if nScalarFields > 0 :
    reader.ReadAllScalarsOn()
    reader.ReadAllColorScalarsOn()
reader.Update()
outputType = reader.GetOutputDataObject(0).GetClassName()
print 'Getting a %s object as output' % outputType

# Mesh data
if reader.IsA('vtkPolyDataSetReader') \
        or reader.IsA('vtkXMLUnstructuredDataReader') \
        or reader.IsA('vtkXMLPolyDataReader') or reader.IsFilePolyData() :
    meshData = reader.GetOutput()
else :
    raise RuntimeError('Input mesh file does not contain a mesh (vtkPolyData)!')

# Print mesh information
nPoints = meshData.GetNumberOfPoints()
nCells  = meshData.GetNumberOfCells()
print "This %s contains:" % outputType
print " %9d points" % nPoints
print " %9d cells"  % nCells
if meshData.IsA('vtkPolyData') :
    nLines  = meshData.GetNumberOfLines()
    print " %9d lines"  % nLines

# Fields
if nScalarFields == 0 :
    iField = -1
    print 'This %s does not contains any data field' % outputType
else :
    # Point data fields
    pointData = meshData.GetPointData()
    nPointFields = pointData.GetNumberOfArrays()
    iField = -1
    print 'This %s contains %d data fields:' % (outputType, nScalarFields)
    if nPointFields == 0 :
        print '* no point data fields'
    else :
        print '* %d point data fields:' % nPointFields
        for i in range(nPointFields) :
            field     = pointData.GetArray(i)
            name      = field.GetName()
            nComp     = field.GetNumberOfComponents()
            dataType  = field.GetDataTypeAsString()
            dataRange = field.GetRange()
            print '  - % 3d %-25s (%d x %-15s) [ %g , %g ]' % (i+1, name, \
                    nComp, dataType, dataRange[0], dataRange[1])

    # Cell data fields
    cellData = meshData.GetCellData()
    nCellFields = cellData.GetNumberOfArrays()
    if nCellFields == 0 :
        print '* no cell data fields'
    else :
        print '* %d cell data fields:' % nCellFields
        for i in range(nCellFields) :
            field     = cellData.GetArray(i)
            name      = field.GetName()
            nComp     = field.GetNumberOfComponents()
            dataType  = field.GetDataTypeAsString()
            dataRange = field.GetRange()
            print '  - % 3d %-25s (%d x %-15s) [ %g , %g ]' % (i+1, name, \
                    nComp, dataType, dataRange[0], dataRange[1])

    # Chosen field
    if len(sys.argv) > 2 :
        iField = int(sys.argv[2]) - 1
    elif nScalarFields > 1 :
        iField = -2
        while iField < -1 or iField >= nScalarFields :
            print 'Which field do you want to display?'
            print '(enter field index or 0 if you want just material texture)?'
            a = raw_input()
            iField = int(a) - 1
            if iField == 0 :
                print 'You have chosen material texture.'
            else :
                print "You have chosen field %s (index %d)." % (a, iField)
    else :
        iField = 0

    if iField < 0 :
        print 'Displaying no field, using material texture'
    else :
        if iField < nPointFields :
            targetData = pointData
        else :
            targetData = cellData
            iField = iField - nPointFields
        displayDataName = cellData.GetArrayName(iField)
        print 'Displaying field number %d named "%s"' % (iField+1, \
                displayDataName)
        displayData = targetData.GetArray(iField)

# Mapper
if reader.IsA('vtkPolyDataSetReader') \
    or reader.IsA('vtkXMLPolyDataReader') :
    mapper = vtk.vtkPolyDataMapper()
elif reader.IsA('vtkXMLUnstructuredDataReader') \
    or reader.IsA('vtkXMLUnstructuredDataReader') \
    or reader.IsA('vtkDataSetReader') :
    mapper = vtk.vtkDataSetMapper()

if vtk.vtkVersion.GetVTKMajorVersion() <= 5 :
    mapper.SetInput(meshData)
else :
    mapper.SetInputData(meshData)

if iField < 0 :
    mapper.ScalarVisibilityOff()
elif nScalarFields > 0 :
    if iField < nPointFields :
        mapper.SetScalarModeToUsePointFieldData()
    else :
        mapper.SetScalarModeToUseCellFieldData()
    mapper.SetInputArrayToProcess(iField, meshData.GetInformation())
    mapper.SelectColorArray(iField)
    if displayData.GetNumberOfComponents() >= 3 :
        print "Using colors from RGB color field"
        mapper.SetColorModeToDefault()
    elif displayData.GetNumberOfComponents() == 1 :
        print "Using colors from lookup table"
        mapper.SetScalarRange(displayData.GetRange())
        mapper.SetLookupTable(displayData.GetLookupTable())
    else :
        print '!'

# Close vtk file
if reader.IsA('vtkDataSetReader') :
    reader.CloseVTKFile()

# Actor
actor = vtk.vtkActor()
actor.SetMapper(mapper)

# Renderer
renderer = vtk.vtkRenderer()

# Render window
renderWindow = vtk.vtkRenderWindow()
renderWindow.SetWindowName("vtkShowMesh.py " + sys.argv[1])
renderWindow.AddRenderer(renderer)
#renderWindow.FullScreenOn()

# Window interactor
renderWindowInteractor = vtk.vtkRenderWindowInteractor()
renderWindowInteractor.SetRenderWindow(renderWindow)
renderer.AddActor(actor)

# Start rendering
renderWindow.Render()

# Axes
axes = vtk.vtkAxesActor()
#axes = vtk.vtkAnnotatedCubeActor()
#axes.SetTotalLength(50,50,50)
#renderer.AddActor(axes)

# Axes widget
widget = vtk.vtkOrientationMarkerWidget()
widget.SetOutlineColor(0.9300, 0.5700, 0.1300)
widget.SetOrientationMarker(axes)
widget.SetInteractor(renderWindowInteractor)
widget.SetViewport(0.0, 0.0, 0.3, 0.3)
widget.SetEnabled(1)
widget.InteractiveOff()
#widget->InteractiveOn()

# Camera
camera = renderer.GetActiveCamera()
camera.Elevation(initAngle)
camera.SetViewUp(0, 0, 1)
renderWindow.Render()

# Trackball
style = vtk.vtkInteractorStyleSwitch()
style.SetCurrentRenderer(renderer)
renderWindowInteractor.SetInteractorStyle(style)
style.SetCurrentStyleToTrackballCamera()

# Event management
def PrintCallBack(obj, event) :
    # See http://www.vtk.org/Wiki/VTK/Examples/Cxx/Interaction/KeypressEvents
    # Don't know which one is the best for multi-platform
    key = renderWindowInteractor.GetKeySym()
    #key = renderWindowInteractor.GetKeyCode()
    if key != "p" :
        return
    w2if = vtk.vtkWindowToImageFilter()
    w2if.SetInput(renderWindow)
    w2if.SetInputBufferTypeToRGB()
    print 'Screenshot taken, please enter the name of the exported file :'
    print 'File:',
    pictureFilename = raw_input()
    if len(pictureFilename) == 0 :
        pictureFilename = 'picture.jpg'
    pictureExt = pictureFilename.rpartition('.')[2]
    if pictureExt == 'jpg' :
        picWriter = vtk.vtkJPEGWriter()
    elif pictureExt == 'png' :
        picWriter = vtk.vtkPNGWriter()
    elif pictureExt == 'tif' :
        picWriter = vtk.vtkTIFFWriter()
    elif pictureExt == 'ps' :
        picWriter = vtk.vtkPostScriptWriter()
    elif pictureExt == 'pnm' :
        picWriter = vtk.vtkPNMWriter()
    else :
        print 'Unsupported type of picture (%s), try again...' % pictureExt
        return
    picWriter.SetFileName(pictureFilename)
    picWriter.SetInputConnection(w2if.GetOutputPort())
    picWriter.Write()
    print 'Picture saved to:', pictureFilename
renderWindowInteractor.AddObserver("CharEvent", PrintCallBack)

# Reminder
print """
### Interactive commands reminder ###
- a / c : actor |a] or camera [c] mode (default is camera)
- j / t : joystick [j] or trackball [t] mode (default is trackball)
- s / w : surface [s] or wireframe [w] representation (default is surface)
-   3   : toggle stereo view mode
-   r   : reset the camera
-   p   : export a snapshot to /tmp/pic.jpg
- e / q : exit
"""

# Start rendering
renderWindow.Render()
renderWindowInteractor.Start()

# Before exiting
renderWindow.Finalize()
