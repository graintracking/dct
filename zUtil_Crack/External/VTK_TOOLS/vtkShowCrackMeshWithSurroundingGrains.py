#!/usr/bin/env python
'''
vtkShowCrackMeshWithSurroundingGrains.py
Program to show crack mesh and some surronding grains through vtk GUI.

Usage: vtkShowCrackMeshWithSurroundingGrains.py  <meshFile>  <volFile> 

Last update by Yoann Guilhem 2013-07-31
'''

import vtk
import sys

def sliderCallback(obj, event):
    global meshThreshold
    widgetValue = obj.GetValue()
    meshThreshold.ThresholdByLower(widgetValue)
    #glyph.SetScaleFactor( glyph.GetScaleFactor()*widgetValue)

initAngle = -45.

# Set mesh input filename
try :
    meshFile = sys.argv[1]
except :
    print 'Missing input mesh file'
    print 'Usage: vtkShowCrackMeshWithSurroundingGrains.py  <meshFile>  <volFile>'

# Set volume input filename
try :
    volFile = sys.argv[2]
except :
    print 'Missing volume file'
    print 'Usage: vtkShowCrackMeshWithSurroundingGrains.py  <meshFile>  <volFile>'

############### MESH ######################

# Mesh Reader
meshReader = vtk.vtkDataSetReader()
meshReader.SetFileName(meshFile)
meshReader.ReadAllScalarsOn() 
meshReader.Update()

# Polydata
if meshReader.IsFilePolyData():
    polydata = meshReader.GetOutput()
else:
    raise RuntimeError('Input mesh file does not contain a mesh (vtkPolyData)!')
#polydata.ReadAllScalarsOn()

# Range
meshRange = polydata.GetScalarRange()
print "mesh range:", meshRange

# Mesh threshold
meshThreshold = vtk.vtkThreshold()
#meshThreshold = vtk.vtkThresholdPoints()
meshThreshold.SetInputConnection(meshReader.GetOutputPort())
if vtk.vtkVersion.GetVTKMajorVersion() <= 5:
    meshThreshold.SetInput(polydata)
else:
    meshThreshold.SetInputData(polydata)
meshThreshold.ThresholdByLower(meshRange[1])
#meshThreshold.SetInputArrayToProcess(0, polydata.GetInformation())
meshThreshold.Update()
viewMesh = meshThreshold.GetOutput()

# Mesh mapper
#meshMapper = vtk.vtkPolyDataMapper()
meshMapper = vtk.vtkDataSetMapper()
if vtk.vtkVersion.GetVTKMajorVersion() <= 5:
    meshMapper.SetInput(viewMesh)
else:
    meshMapper.SetInputData(viewMesh)
meshMapper.SetScalarRange(meshRange)
meshMapper.SetColorModeToMapScalars()

# Mesh actor
meshActor = vtk.vtkActor()
meshActor.SetMapper(meshMapper)

############### VOLUME ######################

# Volume reader
if volFile.endswith('.vtk') :
    volReader = vtk.vtkDataSetReader()
    volReader.SetFileName(volFile)
    volReader.ReadAllScalarsOn()
    volReader.Update()
    # Volume input
    inputVol = volReader.GetOutput()
    # range
    labelRange = inputVol.GetScalarRange()

elif volFile.endswith('.tif') :
    infoFile = volFile.rstrip('.tif') + '.info'
    fid = open(infoFile, 'r')
    l = fid.readline()
    s = l.strip('[]\n').split('] [')
    originIndex = map(int, s[0].split(','))
    l = fid.readline()
    voxelSize = map(float, l.strip('\n'))
    import InsightToolkit as itk
    volReader = itk.itkImageFileReaderUC3.New()
    #volReader = vtk.vtkTIFFReader()
    #volReader.SetFilePrefix(volFile)
    #volReader.SetFilePattern(volFile)
    volReader.SetFileName(volFile)
    volReader.Update()
    # Volume input
    inputVol = volReader.GetOutput()
    inputVol.SetSpacing(voxelsize, voxelsize, voxelsize)
    inputVol.SetOrigin(originIndex[0] * voxelsize, \
                       originIndex[1] * voxelsize, \
                       originIndex[2] * voxelsize)
    itkExport = itk.itkVTKImageExportUC3.New()
    itkExport.SetInput(inputVol)
    vtkImport = vtk.vtkImageImport()
    ConnectVTKITKPython.ConnectITKUS2ToVTK(itkExport.GetPointer(), vtkImport)

    writer = vtk.vtkPNGWriter()
    writer.SetInput(vtkImport.GetOutput())
    writer.SetFileName(fileOut)
    #volReader.ReadAllScalarsOn()
    #volReader.SetDataSpacing(voxelsize, voxelsize, voxelsize)
    #volReader.SetDataOrigin(\
    #        originIndex[0] * voxelsize, \
    #        originIndex[1] * voxelsize, \
    #        originIndex[2] * voxelsize)
    #volReader.SetDataScalarTypeToUnsignedChar()

    print "Orientation:", volReader.GetOrientationType()
    print "Spacing:    ", volReader.GetDataSpacing()
    print "Origin:     ", volReader.GetDataOrigin()
    print "Extent:     ", volReader.GetDataExtent()
    print "ScalarType: ", volReader.GetDataScalarType()
    print "ByteOrder:  ", volReader.GetDataByteOrderAsString()
    print "Information:", volReader.GetInformation()

if len(sys.argv) > 3 :
    startLabel = int(sys.argv[3])
else :
    startLabel = labelRange[0]
if len(sys.argv) > 4 :
    endLabel = int(sys.argv[4])
else :
    endLabel = labelRange[1]

# Size
volSize = inputVol.GetDimensions()

# Uniform Grid
uGrid = vtk.vtkUniformGrid()
uGrid.SetExtent(0, volSize[0], \
                0, volSize[1], \
                0, volSize[2])
uGrid.SetOrigin(inputVol.GetOrigin())
uGrid.SetSpacing(inputVol.GetSpacing())
uGrid.SetScalarType(vtk.VTK_UNSIGNED_CHAR)

# Project point data to the cell data
array = inputVol.GetPointData().GetScalars()
uGrid.GetCellData().SetScalars(array)
viewGrid = uGrid.GetProducerPort()

# Volume threshold
threshold = vtk.vtkThreshold()
threshold.SetInputConnection(viewGrid)
threshold.ThresholdByUpper(1)
viewVol = threshold.GetOutputPort()

# Volume Mapper
volMapper = vtk.vtkDataSetMapper()
volMapper.SetInputConnection(viewVol)
volMapper.SetScalarRange(startLabel, endLabel)
#volMapper.SetScalarRange(1, 255)
volMapper.SetScalarModeToUseCellData()
volMapper.SetColorModeToMapScalars()

# Actor
volActor = vtk.vtkActor()
volActor.SetMapper(volMapper)
volActor.GetProperty().SetOpacity(0.25)

############# DISPLAY ######################

# Renderer
renderer = vtk.vtkRenderer()

# Render window
renderWindow = vtk.vtkRenderWindow()
renderWindow.AddRenderer(renderer)

# Window interactor
renderWindowInteractor = vtk.vtkRenderWindowInteractor()
renderWindowInteractor.SetRenderWindow(renderWindow)
renderer.AddActor(meshActor)
renderer.AddActor(volActor)

# Trackball
style = vtk.vtkInteractorStyleTrackballCamera()
renderWindowInteractor.SetInteractorStyle(style)

### SLIDER ON
# Slider representation
sliderRep = vtk.vtkSliderRepresentation2D()
sliderRep.SetMinimumValue(meshRange[0])
sliderRep.SetMaximumValue(meshRange[1])
sliderRep.SetValue(meshRange[1])
sliderRep.SetTitleText("Cycle number")
sliderRep.GetPoint1Coordinate().SetCoordinateSystemToNormalizedDisplay()
sliderRep.GetPoint2Coordinate().SetCoordinateSystemToNormalizedDisplay()
sliderRep.GetPoint1Coordinate().SetValue(0.2, 0.1)
sliderRep.GetPoint2Coordinate().SetValue(0.8, 0.1)
#sliderRep.SetSliderLength(0.02)
#sliderRep.SetSliderWidth(0.03)
#sliderRep.SetEndCapLength(0.03)
#sliderRep.SetEndCapWidth(0.03)
#sliderRep.SetTubeWidth(0.005)

# Slider
sliderWidget = vtk.vtkCenteredSliderWidget()
sliderWidget.SetInteractor(renderWindowInteractor)
sliderWidget.SetRepresentation(sliderRep)
#sliderWidget.SetAnimationModeToAnimate()
sliderWidget.EnabledOn()
sliderWidget.AddObserver("InteractionEvent", sliderCallback)

### SLIDER OFF

# First rendering
renderWindowInteractor.Initialize()
renderWindow.Render()

# Camera
camera = renderer.GetActiveCamera()
camera.Elevation(initAngle)
camera.SetViewUp(0, 0, 1)
renderWindow.Render()

# Recorder
recorder = vtk.vtkInteractorEventRecorder()
recorder.SetInteractor(renderWindowInteractor)
recorder.SetFileName("vtk.log")

renderWindowInteractor.Start()
