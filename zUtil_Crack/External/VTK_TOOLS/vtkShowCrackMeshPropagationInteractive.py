#!/usr/bin/env python
'''
vtkShowCrackMeshPropagationInteractive.py
Program to show interactively the propagation of a crack through vtk GUI.
The VTK input mesh file should contains the cycle number field as cell data.

Usage: vtkShowCrackMeshPropagationInteractive.py  <meshFile> <viewFieldNumber>  <thresholdFieldNumber>

Last update by Yoann Guilhem 2013-07-31
'''

import vtk
import sys

# Show vtk version
print "Using vtk version", vtk.vtkVersion.GetVTKVersion()

# Parameters
initAngle = -45.

############### FUNCTIONS ################

# When interaction starts, the requested frame rate is increased.
def StartInteraction(obj, event):
    global renderWindow
    renderWindow.SetDesiredUpdateRate(10)

# When interaction ends, the requested frame rate is decreased to
# normal levels. This causes a full resolution render to occur.
def EndInteraction(obj, event):
    global renderWindow
    renderWindow.SetDesiredUpdateRate(0.001)

# When moving slider, changing threshold
def sliderCallback(obj, event):
    global meshThreshold
    widgetValue = obj.GetRepresentation().GetValue()
    meshThreshold.ThresholdByLower(widgetValue)
    meshThreshold.Update()

############### MESH ######################

# Set mesh input filename
try :
    meshFile = sys.argv[1]
except :
    raise RuntimeError('Missing input mesh file\nUsage: vtkShowCrackMeshPropagationInteractive.py  <meshFile>  <viewFieldNumber>  <thresholdFieldNumber>')

# Set view field number
try :
    viewFieldNumber = int(sys.argv[2])
except :
    raise RuntimeError('Missing input view field number\nUsage: vtkShowCrackMeshPropagationInteractive.py  <meshFile>  <viewFieldNumber>  <thresholdFieldNumber>')

# Set threshold field number
if len(sys.argv) > 3 :
    thresholdFieldNumber = int(sys.argv[3])
else :
    print 'Using same field for threshold and mapping (%d)' % viewFieldNumber
    thresholdFieldNumber = viewFieldNumber

# Mesh Reader
meshReader = vtk.vtkDataSetReader()
meshReader.SetFileName(meshFile)
meshReader.ReadAllScalarsOn()
meshReader.ReadAllColorScalarsOn()
meshReader.Update()

# Polydata
if meshReader.IsFilePolyData():
    polydata = meshReader.GetOutput()
else:
    raise RuntimeError('Input mesh file does not contain a mesh (vtkPolyData)!')

# Print mesh information
nPoints = polydata.GetNumberOfPoints()
nCells  = polydata.GetNumberOfCells()
print "This file contains:"
print "  %9d points" % nPoints
print "  %9d cells"  % nCells

# Cell data fields
cellData = polydata.GetCellData()
nFields = cellData.GetNumberOfArrays()

# Extract display data
iField = viewFieldNumber - 1
if viewFieldNumber <= 0:
    print 'Displaying no field, using material texture'
elif viewFieldNumber <= nFields:
    displayDataName = cellData.GetArrayName(iField)
    displayData = cellData.GetArray(iField)
    print 'Displaying field number %d: %s' % (viewFieldNumber, displayDataName)
    # Display range
    displayRange = displayData.GetRange()
    print "display range:", displayRange
else:
    raise RuntimeError('Too high view field number!')

# Extract threshold data
jField = thresholdFieldNumber - 1
if thresholdFieldNumber <= 0:
    print 'No threshold...'
elif thresholdFieldNumber <= nFields:
    thresholdDataName = cellData.GetArrayName(jField)
    thresholdData = cellData.GetArray(jField)
    print 'Thresholding field number %d: %s' % (thresholdFieldNumber, thresholdDataName)
    # Threshold range
    thresholdRange = thresholdData.GetRange()
    print "threshold range:", thresholdRange
else:
    raise RuntimeError('Too high threshold field number!')

# Bounds
meshBounds = polydata.GetBounds()
meshCenter = polydata.GetCenter()
meshLength = polydata.GetLength()
print "mesh bounds:", meshBounds
print "mesh center:", meshCenter
print "mesh length:", meshLength

# Mesh threshold
meshThreshold = vtk.vtkThreshold()
if vtk.vtkVersion.GetVTKMajorVersion() <= 5:
    meshThreshold.SetInput(polydata)
else:
    meshThreshold.SetInputData(polydata)
if jField >=0:
    meshThreshold.SetInputArrayToProcess(0, 0, 0, vtk.vtkDataObject.FIELD_ASSOCIATION_CELLS, thresholdDataName)
    meshThreshold.ThresholdByLower(thresholdRange[1])
meshThreshold.Update()
viewMesh = meshThreshold.GetOutput()

# Mesh mapper
meshMapper = vtk.vtkDataSetMapper()
if vtk.vtkVersion.GetVTKMajorVersion() <= 5:
    meshMapper.SetInput(viewMesh)
else:
    meshMapper.SetInputData(viewMesh)
#meshMapper.SetColorModeToMapScalars()

if iField < 0:
    meshMapper.ScalarVisibilityOff()
else:
    meshMapper.SetInputArrayToProcess(iField, polydata.GetInformation())
    meshMapper.SelectColorArray(iField)
    meshMapper.SetScalarModeToUseCellFieldData()
    if displayData.GetNumberOfComponents() >= 3:
        print "Using colors from RGB color field"
        meshMapper.SetColorModeToDefault()
    elif displayData.GetNumberOfComponents() == 1:
        print "Using colors from lookup table"
        #meshMapper.SetScalarRange(displayData.GetRange())
        meshMapper.SetScalarRange(displayRange)
        meshMapper.SetLookupTable(displayData.GetLookupTable())
    else:
        print '!'

# Mesh actor
meshActor = vtk.vtkActor()
meshActor.SetMapper(meshMapper)

############# DISPLAY ######################

# Renderer
renderer = vtk.vtkRenderer()

# Render window
renderWindow = vtk.vtkRenderWindow()
renderWindow.AddRenderer(renderer)

# Window interactor
renderWindowInteractor = vtk.vtkRenderWindowInteractor()
renderWindowInteractor.SetRenderWindow(renderWindow)
renderer.AddActor(meshActor)

# Trackball
style = vtk.vtkInteractorStyleTrackballCamera()
renderWindowInteractor.SetInteractorStyle(style)

# First rendering
renderWindowInteractor.Initialize()
renderWindow.Render()

# Slider representation
sliderRep = vtk.vtkSliderRepresentation2D()
sliderRep.GetPoint1Coordinate().SetCoordinateSystemToNormalizedDisplay()
sliderRep.GetPoint2Coordinate().SetCoordinateSystemToNormalizedDisplay()
sliderRep.GetPoint1Coordinate().SetValue(0.1, 0.1)
sliderRep.GetPoint2Coordinate().SetValue(0.9, 0.1)
sliderRep.SetSliderLength(0.025)
sliderRep.SetSliderWidth(0.025)
sliderRep.SetEndCapLength(0.005)
sliderRep.SetEndCapWidth(0.05)
sliderRep.SetTubeWidth(0.005)
sliderRep.SetTitleText("Cycle number")
#sliderRep.SetTitleText(thresholdDataName)
sliderRep.GetSliderProperty().SetColor(0, 1, 0)    # knob in gree
sliderRep.GetSelectedProperty().SetColor(1, 0 ,0) # slected knob in red
if jField >= 0:
    sliderRep.SetMinimumValue(thresholdRange[0])
    sliderRep.SetMaximumValue(thresholdRange[1])
    sliderRep.SetValue(thresholdRange[1])
sliderRep.SetLabelFormat("%6.0f")

# Slider
#sliderWidget = vtk.vtkCenteredSliderWidget()
sliderWidget = vtk.vtkSliderWidget()
sliderWidget.SetInteractor(renderWindowInteractor)
sliderWidget.SetRepresentation(sliderRep)
#sliderWidget.SetNumberOfAnimationSteps(5)
#sliderWidget.SetAnimationModeToAnimate()
sliderWidget.SetAnimationModeToJump()
sliderWidget.EnabledOn()
sliderWidget.AddObserver("EndInteractionEvent", StartInteraction)
sliderWidget.AddObserver("InteractionEvent", sliderCallback)
sliderWidget.AddObserver("EndInteractionEvent", EndInteraction)

# Camera
camera = renderer.GetActiveCamera()
camera.Elevation(initAngle)
camera.SetViewUp(0, 0, 1)
renderWindow.Render()

# Recorder
recorder = vtk.vtkInteractorEventRecorder()
recorder.SetInteractor(renderWindowInteractor)
recorder.SetFileName("vtk.log")

renderWindowInteractor.Start()
