#!/usr/bin/env python

import vtk
import sys
import os
from math import *
import videoConversion as avc

# Parameters
initAngle = -45.
screenSizeX, screenSizeY = 800, 600

# Set input filename
try :    meshFile = sys.argv[1]
except : print 'Missing input file'

# Set movie filename
if len(sys.argv) > 2: movieName = sys.argv[2]
else:                 movieName = 'movie'

if len(sys.argv) > 3: N = int(sys.argv[3])
else:                 N = 150

if len(sys.argv) > 4: time = float(sys.argv[4])
else:                 time = 15

# Set number of frames per second
fps = int(N / time)

# Reader
reader = vtk.vtkDataSetReader()
reader.SetFileName(meshFile)
reader.ReadAllScalarsOn() 
reader.Update()

# Polydata
if reader.IsFilePolyData():
    polydata = reader.GetOutput()
else:
    raise RuntimeError('Input mesh file does not contain a mesh (vtkPolyData)!')

# Mapper
mapper = vtk.vtkPolyDataMapper()
if vtk.vtkVersion.GetVTKMajorVersion() <= 5:
    mapper.SetInput(polydata)
else :
    mapper.SetInputData(polydata)
mapper.SetScalarRange(polydata.GetScalarRange())

# Actor
actor = vtk.vtkActor()
actor.SetMapper(mapper)

# Renderer
#renderer = vtk.vtkRenderer()
renderer = vtk.vtkOpenGLRenderer()
renderer.AddActor(actor)

# Axes
#axes = vtk.vtkAxesActor()
#axes.SetTotalLength(50,50,50)
#renderer.AddActor(axes);

# Render window
renderWindow = vtk.vtkRenderWindow()
renderWindow.AddRenderer(renderer)
renderWindow.SetSize(screenSizeX, screenSizeY)
renderWindow.Start()
renderWindow.SetAlphaBitPlanes(1)
renderWindow.Render()

# Camera
camera = renderer.GetActiveCamera()
camera.Elevation(initAngle)
camera.SetViewUp(0, 0, 1)
#renderWindow.Render()
#renderer.ResetCamera(120, 400, 120, 150, 0, 180)
renderWindow.Render()

# Window to image 
w2if = vtk.vtkWindowToImageFilter()
w2if.SetInput(renderWindow)
w2if.SetInputBufferTypeToRGB()
#w2if.SetInputBufferTypeToRGBA()
#w2if.SetInputBufferTypeToZBuffer()

# TIFF Writer
tifWriter = vtk.vtkTIFFWriter()
tifWriter.SetInputConnection(w2if.GetOutputPort())
#tifWriter.SetCompressionToPackBits()
tifWriter.SetCompressionToDeflate()

# TIFF Settings
tifDir = 'ANIM'
tifPrefix = 'frame'
tifFullPrefix = os.path.join(tifDir, tifPrefix)

# Clean TIFF output directory
cmd = 'rm -rf %s' % tifDir
os.system(cmd)
os.mkdir(tifDir)

# AVI Writer
aviWriter = vtk.vtkFFMPEGWriter()
aviWriter.SetInputConnection(w2if.GetOutputPort())
aviWriter.SetFileName('%s.avi' % movieName)
aviWriter.SetRate(fps)
aviWriter.SetQuality(0)
#if vtk.vtkVersion.GetVTKMajorVersion() >= 5 and \
#        vtk.vtkVersion.GetVTKMinorVersion() >= 8:
#    aviWriter.SetBitRate(screenSizeX * screenSizeY * 24)
#    aviWriter.SetBitRateTolerance(screenSizeX * screenSizeY * fps/10)
#aviWriter.PRESERVES_GEOMETRY()
#aviWriter.PRESERVES_DATASET()
#aviWriter.PRESERVES_TOPOLOGY()
#aviWriter.Update()
aviWriter.Start()

# 1st loop
dAngle = 360./N
for i in range(N):
    renderWindow.Render()
    w2if.Modified()
    tifFullFile = '%s_%04d.tif' % (tifFullPrefix, i)
    tifWriter.SetFileName(tifFullFile)
    try: 
        tifWriter.Write()
        aviWriter.Write()
    except: print 'BAD'
    camera.Azimuth(dAngle)

# 2nd loop
for i in range(N):
    j = i + N
    renderWindow.Render()
    w2if.Modified()
    tifFullFile = '%s_%04d.tif' % (tifFullPrefix, j)
    tifWriter.SetFileName(tifFullFile)
    try:
        tifWriter.Write()
        aviWriter.Write()
    except: print 'BAD'
    camera.Elevation(dAngle)
    camera.OrthogonalizeViewUp()

# Last frame
renderWindow.Render()
w2if.Modified()
tifFullFile = '%s_%04d.tif' % (tifFullPrefix, 2*N)
tifWriter.SetFileName(tifFullFile)
try:
    tifWriter.Write()
    aviWriter.Write()
except: print 'BAD'

# Finish AVI Writer
aviWriter.End()

# Gather TIF files into a single one
print 'Gather all TIFF files into a single one'
cmd = 'tiffcp -s -x %s_*.tif %s.tif' % (tifFullPrefix, movieName)
os.system(cmd)

# Convert single TIFF file into PDF
print 'Convert single TIFF file into PDF file'
cmd = 'convert -units PixelsPerCentimeter -compress JPEG %s.tif %s.pdf' % (movieName, movieName)
#cmd = 'tiff2pdf -t "ti55531 Crack at 129,500 cycles" -u m -j -o %s.pdf %s.tif' % (movieName, movieName)
#cmd = 'tiff2pdf -t "ti55531 Crack at 129,500 cycles" -u m -z -o %s.pdf %s.tif' % (movieName, movieName)
os.system(cmd)

# Video converter
avconv = avc.videoConversion()
avconv.CheckExe()
avconv.SetVerbose()

# Convert tiff files to AVI
print 'Convert TIFF files into AVI file with %d frames per second' % fps
avconv.SetInput('%s_%%04d.tif' % tifFullPrefix)
avconv.SetOutput('%s_from-tiff.avi' % movieName)
avconv.SetInputFPS(fps)
avconv.SetVideoCodec('mpeg4')
avconv.Run()

# Convert tiff files to MP4
print 'Convert TIFF files into MP4 file with %d frames per second' % fps
avconv.SetOutput('%s_from-tiff.mp4' % movieName)
avconv.Run()

# Convert tiff files to FLV
print 'Convert TIFF files into FLV file with %d frames per second' % fps
avconv.SetOutput('%s_from-tiff.flv' % movieName)
avconv.ResetVideoCodec()
avconv.SetOutputQuality(1)
avconv.Run()

# Default parameters
avconv.ResetInputFPS()
avconv.ResetOutputQuality()

# Convert AVI to MP4
print 'Convert AVI file into MP4 file'
avconv.SetInput('%s.avi' % movieName)
avconv.SetOutput('%s.mp4' % movieName)
#avconv.SetVideoCodec('libx264')
avconv.SetVideoCodec('mpeg4')
avconv.Run()

# Convert AVI files to SWF
print 'Convert AVI file into FLV file'
avconv.SetInput('%s.avi' % movieName)
avconv.SetOutput('%s.flv' % movieName)
avconv.SetOutputQuality(1)
avconv.ResetVideoCodec()
avconv.Run(True)

# Clean TIFF directory
print 'Clean TIFF directory'
cmd = 'rm -rf %s' % tifDir
os.system(cmd)
