#!/usr/bin/env python
# -*- coding: utf-8 -*-

import vtk
import os

movie = True

initElevationAngle = -35.
initAzimuthAngle   = -60.
movieName = 'ti55531_crack-propagation_movie'
tifDir = 'ANIM'
tifPrefix = 'frame'
cmd = 'rm -rf %s' % tifDir
os.system(cmd)
os.mkdir(tifDir)
tifFullPrefix = os.path.join(tifDir, tifPrefix)

# Total time
totalTime = 20
if movie:
    nAngle = 20 # for movie
else:
    nAngle = 10 # for pdf

meshDir = '/data/id11/inhouse/guilhem/ti55531/seg_mesh'

meshFiles = ['ti55531_40k_crack-mesh_1-0.vtk', \
             'ti55531_50k_crack-mesh_1-0.vtk', \
             'ti55531_68k_crack-mesh_1-0.vtk', \
             'ti55531_80k_crack-mesh_1-0.vtk', \
             'ti55531_90k_crack-mesh_1-0.vtk', \
             'ti55531_100k_crack-mesh_1-0.vtk', \
             'ti55531_110k_crack-mesh_1-0.vtk', \
             'ti55531_116_5k_crack-mesh_1-0.vtk', \
             'ti55531_121k_crack-mesh_1-0.vtk', \
             'ti55531_125k_crack-mesh_1-0.vtk', \
             'ti55531_127k_crack-mesh_1-0.vtk', \
             'ti55531_129_5k_crack-mesh_1-0.vtk']

meshFiles = ['ti55531_36k_crack-mesh_1-0.vtk',\
             'ti55531_44k_crack-mesh_1-0.vtk',\
             'ti55531_65k_crack-mesh_1-0.vtk', \
             'ti55531_80k_crack-mesh_1-0.vtk', \
             'ti55531_112_5k_crack-mesh_1-0.vtk', \
             'ti55531_116_5k_crack-mesh_1-0.vtk', \
             'ti55531_121k_crack-mesh_1-0.vtk', \
             'ti55531_123k_crack-mesh_1-0.vtk', \
             'ti55531_125k_crack-mesh_1-0.vtk', \
             'ti55531_127_5k_crack-mesh_1-0.vtk', \
             'ti55531_129_5k_crack-mesh_1-0.vtk']

# Renderer
renderer = vtk.vtkRenderer()

# Render window
renderWindow = vtk.vtkRenderWindow()
renderWindow.AddRenderer(renderer)
#renderWindow.FullScreenOn()
renderWindow.SetSize(800,600)

# Actor
actor = vtk.vtkActor()
renderer.AddActor(actor)

# Axes
axes = vtk.vtkAxesActor()
axes.SetTotalLength(75, 75, 75)
#axes.SetOrigin(300.,300.,300.)
#axes.SetPosition(300.,300.,300.)
#renderer.AddActor(axes)

# Title
textActor = vtk.vtkTextActor()
textActor.GetTextProperty().SetFontSize( 40 )
textActor.SetDisplayPosition(0., 550.0)
textActor.GetTextProperty().SetColor( 1.0,1.0,1.0 )
textActor.GetTextProperty().SetColor( 1.0,0.0,0.0 )
renderer.AddActor2D(textActor)

# Window to image 
w2if = vtk.vtkWindowToImageFilter()
w2if.SetInput(renderWindow)

# Writer
writer = vtk.vtkTIFFWriter()
writer.SetInputConnection(w2if.GetOutputPort())
writer.SetCompressionToDeflate()

first = True

im = 0
for mesh in meshFiles :
    meshFile = os.path.join(meshDir, mesh)
    cycle = mesh[8:-19].replace('_', '.')
    textActor.SetInput ( "cycle " + cycle )

    # Reader
    reader = vtk.vtkDataSetReader()
    reader.SetFileName(meshFile)
    reader.ReadAllScalarsOn()
    reader.Update()

    # Polydata
    if reader.IsFilePolyData():
        polydata = reader.GetOutput()
    else:
        raise RuntimeError('Input mesh file does not contain a mesh (vtkPolyData)!')

    # Mapper
    mapper = vtk.vtkPolyDataMapper()
    if vtk.vtkVersion.GetVTKMajorVersion() <= 5:
        mapper.SetInput(polydata)
    else:
        mapper.SetInputData(polydata)
    mapper.SetScalarRange(polydata.GetScalarRange())

    actor.SetMapper(mapper)
    renderWindow.Render()

    # Camera
    camera = renderer.GetActiveCamera()
    if first :
        first = False
        #camera.SetDistance(500.)
        #camera.SetDistance(800.)
        camera.SetFocalPoint(220, 120, 129.27396583557129)
        camera.SetClippingRange(200, 600)
        camera.Zoom(0.65)
        camera.Elevation(initElevationAngle)
        print "Focal:", camera.GetFocalPoint()
        print "Position:", camera.GetPosition()
        print "Distance:", camera.GetDistance()
        print "Range   :", camera.GetClippingRange()
        #camera.SetPosition(400, -400, 900)
        #camera.SetDistance(600)
        camera.SetViewUp(0, 0, 1)
        camera.Azimuth(initAzimuthAngle)
        renderWindow.Render()
 
    dAngle = 15./nAngle
    for j in range(nAngle) :
        w2if.Modified()
        tifFullFile = '%s_%04d.tif' % (tifFullPrefix, im)
        writer.SetFileName(tifFullFile)
        try: writer.Write()
        except: print 'BAD'
        camera.Azimuth(dAngle)
        renderWindow.Render()
        im = im + 1
    dAngle = -15./nAngle
    for j in range(nAngle) :
        w2if.Modified()
        tifFullFile = '%s_%04d.tif' % (tifFullPrefix, im)
        writer.SetFileName(tifFullFile)
        try: writer.Write()
        except: print 'BAD'
        camera.Azimuth(dAngle)
        renderWindow.Render()
        im = im + 1

for j in range(24*nAngle) :
    w2if.Modified()
    tifFullFile = '%s_%04d.tif' % (tifFullPrefix, im)
    writer.SetFileName(tifFullFile)
    try: writer.Write()
    except: print 'BAD'
    camera.Azimuth(dAngle)
    renderWindow.Render()
    im = im + 1

nTransp = 24
dAngle = -15./nAngle
for j in range(nTransp, 0, -1) :
    tifFullFile = '%s_%04d.tif' % (tifFullPrefix, im)
    writer.SetFileName(tifFullFile)
    try: writer.Write()
    except: print 'BAD'
    actor.GetProperty().SetOpacity((j-1.)/nTransp)
    renderWindow.Render()
    im = im + 1
tifFullFile = '%s_%04d.tif' % (tifFullPrefix, im)
writer.SetFileName(tifFullFile)
try: writer.Write()
except: print 'BAD'

# Number of frames
nFrames = im

# Set number of frames per second
fps = int(nFrames / totalTime)

if movie:
    # Convert tiff files to AVI
    print 'Convert TIFF files into AVI file with %d frames per second' % fps
    cmd = 'avconv -y -v error -r %d -i %s_%%04d.tif -vcodec libx264 %s.avi' % (fps, tifFullPrefix, movieName)
    os.system(cmd)
else:
    # Gather TIF files into a single one
    print 'Gather all TIFF files into a single one'
    cmd = 'tiffcp -s -x %s_*.tif %s.tif' % (tifFullPrefix, movieName)
    os.system(cmd)

    # Convert single TIFF file into PDF
    print 'Convert single TIFF file into PDF file'
    cmd = 'convert -units PixelsPerCentimeter -compress JPEG %s.tif %s.pdf' % (movieName, movieName)
    os.system(cmd)

