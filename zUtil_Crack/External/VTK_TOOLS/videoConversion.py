'''
Class to facilitate system calls to video converter

Last update by Yoann Guilhem 27 Feb 2013
'''

import os
import subprocess

class videoConversion(object):

    def __init__(self, verbose = False, input = '', output = '', exe = '', \
            defaultArgs = '-y', inputArgs = '', outputArgs = '', \
            inputFPS = 0, outputFPS = 0, outputQuality = None, \
            videoCodec = '', pixelFormat= 'yuv420p'):
        self.verbose = verbose
        self.input = input
        self.output = output
        self.exe = exe
        self.defaultArgs = defaultArgs
        self.inputArgs = inputArgs
        self.outputArgs = outputArgs
        self.inputFPS = inputFPS
        self.outputFPS = outputFPS
        self.outputQuality = outputQuality
        self.videoCodec = videoCodec
        self.pixelFormat = pixelFormat

    def SetVerbose(self, verbose = True):
        self.verbose = verbose

    def SetInput(self, str):
        self.input = str

    def SetOutput(self, str):
        self.output = str

    def SetDefaultArgs(self, args):
        self.defaultArgs = args

    def SetInputArgs(self, args):
        self.inputArgs = args

    def ResetInputArgs(self, args):
        self.inputArgs = ''

    def SetOutputArgs(self, args):
        self.outputArgs = args

    def ResetOutputArgs(self, args):
        self.outputArgs = ''

    def SetInputFPS(self, fps):
        self.inputFPS = fps

    def ResetInputFPS(self):
        self.inputFPS = 0

    def SetOutputFPS(self, fps):
        self.outputFPS = fps

    def ResetOutputFPS(self):
        self.outputFPS = 0

    def SetOutputQuality(self, quality):
        # Set from worst to best: 0-100
        if quality < 0:
            print 'Quality should be set between 0 and 100, setting it to 0'
            quality = 0
        elif quality > 100:
            print 'Quality should be set between 0 and 100, setting it to 100'
            quality = 100
        self.outputQuality = quality

    def ResetOutputQuality(self):
        self.outputQuality = None

    def SetVideoCodec(self, codec):
        self.videoCodec = codec

    def ResetVideoCodec(self):
        self.videoCodec = ''

    def SetPixelFormat(self, pxFormat):
        self.pixelFormat = pxFormat

    def CheckExe(self, verbose = True):
        self.exe = 'avconv'
        self.defaultArgs = '-y -v error'
        try:
            subprocess.call([self.exe, '-version'], \
                    stdout = subprocess.PIPE, stderr = subprocess.PIPE)
        except OSError as e:
            if e.errno == os.errno.ENOENT:
                self.exe = 'ffmpeg'
                self.defaultArgs = '-y -v 2'
                try:
                    subprocess.call([self.exe, '-version'],\
                            stdout = subprocess.PIPE, stderr = subprocess.PIPE)
                except OSError as e:
                    if e.errno == os.errno.ENOENT:
                        raise SystemError('Neither avconv nor ffmpeg are installed!')
                        self.exe = None
                        self.defaultArgs = ''
                    else:
                        raise SystemError('Something went wrong testing ffmpeg!')
            else:
                raise SystemError('Something went wrong testing avconv!')
        if self.verbose or verbose:
            print 'Audio/video converter:', self.exe

    def Run(self, verbose = False):
        if not self.exe:
            self.CheckExe()
        cmd = [self.exe]
        if self.defaultArgs:
            cmd.extend(self.defaultArgs.split())
        if self.inputFPS:
            cmd.extend(['-r', '%d' % self.inputFPS])
        if self.inputArgs:
            cmd.extend(self.inputArgs.split())
        cmd.extend(['-i', self.input])
        if self.outputFPS:
            cmd.extend(['-r', '%d' % self.outputFPS])
        if self.videoCodec:
            cmd.extend(['-vcodec', self.videoCodec])
        if self.pixelFormat:
            cmd.extend(['-pix_fmt', self.pixelFormat])
        if self.outputQuality and self.exe == 'avconv':
            if self.output.endswith(('avi','flv')):# From best to worst: 1-31
                q = round((1. - (self.outputQuality/100.)) * 30) + 1
                cmd.extend(['-qscale', '%d' % q])
            elif self.output.endswith('mp4'):# From best to worst: 0-69
                q = round((1. - (self.outputQuality/100.)) * 69)
                cmd.extend(['-qp', '%d' % q])
        if self.outputArgs:
            cmd.extend(self.outputArgs.split())
        cmd.append(self.output)
        if verbose or self.verbose:
            print 'Launching command:', ' '.join(cmd)
            subprocess.call(cmd)
        else:
            subprocess.call(cmd, \
                    stdout = subprocess.PIPE, stderr = subprocess.PIPE)
