Here are some python scripts (using vtk python module) to visualize meshes 
(cracks) and volumes (grains). Also some interactive GUIs and scripts to produce
videos are also provided.

Prerequisites:
- python (>= 2.6)
- vtk (>=5.6, with python wrapping module, i.e. python-vtk packages on Debian6)
