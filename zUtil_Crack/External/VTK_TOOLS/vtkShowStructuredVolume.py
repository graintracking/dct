#!/usr/bin/env python
# -*- coding: utf-8 -*-

import vtk
import sys

# Parameters
drawOutline = False
drawAxes = True

# Show vtk version
print "Using vtk version", vtk.vtkVersion.GetVTKVersion()

# Set input filename
try :
    volFile = sys.argv[1]
except :
    print 'Missing input file'
print "Input file:", volFile

# Reader
reader = vtk.vtkDataSetReader()
reader.SetFileName(volFile)
reader.Update()

# Input volume

if reader.IsFileStructuredPoints():
    inputVol = reader.GetOutput()
else:
    raise RuntimeError('Input mesh file does not contain a structured volume (vtkStructuredPoints)!')

# Size
volSize = inputVol.GetDimensions()
print "size       :", volSize

# Range
reader.ReadAllScalarsOn()
labelRange = inputVol.GetScalarRange()
if len(sys.argv) > 2 :
    startLabel = int(sys.argv[2])
else :
    startLabel = labelRange[0]
if len(sys.argv) > 3 :
    endLabel = int(sys.argv[3])
else :
    endLabel = labelRange[1]
print "startLabel :", startLabel
print "endLabel   :", endLabel

# Uniform Grid
uGrid = vtk.vtkUniformGrid()
uGrid.SetExtent(0, volSize[0], 0, volSize[1], 0, volSize[2])
uGrid.SetOrigin(inputVol.GetOrigin())
uGrid.SetSpacing(inputVol.GetSpacing())

# Project point data to the cell data
array = inputVol.GetPointData().GetScalars()
uGrid.GetCellData().SetScalars(array)

# Crop
voi = vtk.vtkExtractVOI()
if vtk.vtkVersion.GetVTKMajorVersion() <= 5:
    voi.SetInputConnection(uGrid.GetProducerPort())
else:
    voi.SetInputData(uGrid)
if len(sys.argv) == 10 :
    voiXmin = int(sys.argv[4])
    voiXmax = int(sys.argv[7])
    voiYmin = int(sys.argv[5])
    voiYmax = int(sys.argv[8])
    voiZmin = int(sys.argv[6])
    voiZmax = int(sys.argv[9])
else :
    voiXmin = 0
    voiXmax = volSize[0] - 1
    voiYmin = 0
    voiYmax = volSize[1] - 1
    voiZmin = 0
    voiZmax = volSize[2] - 1
voi.SetVOI(voiXmin, voiXmax, voiYmin, voiYmax, voiZmin, voiZmax)
voi.SetSampleRate(1, 1, 1)
voi.Update()
print "voi        :", voi.GetVOI()
viewVol = voi.GetOutputPort()

if len(sys.argv) > 2 :
    # Threshold
    threshold = vtk.vtkThreshold()
    #if vtk.vtkVersion.GetVTKMajorVersion() <= 5:
    #    threshold.SetInputConnection(viewVol)
    #else:
    #    threshold.SetInputData(viewVol)
    threshold.SetInputConnection(viewVol)
    threshold.ThresholdBetween(startLabel, endLabel)
    viewVol2 = threshold.GetOutputPort()
else :
    viewVol2 = viewVol
    
# Mapper
mapper = vtk.vtkDataSetMapper()
mapper.SetInputConnection(viewVol2)
mapper.SetScalarRange(startLabel, endLabel)
mapper.SetLookupTable(array.GetLookupTable())
mapper.GetLookupTable().SetTableRange(labelRange[0], labelRange[1])
mapper.UseLookupTableScalarRangeOn()
mapper.SetScalarModeToUseCellData() # OK
#mapper.SetColorModeToMapScalars() # OK also

# Actor
actor = vtk.vtkActor()
actor.SetMapper(mapper)

if drawOutline :
    # use an outline instead of an opaque box
    out = vtk.vtkOutlineFilter()
    out.SetInput(uGrid)
    outlineMapper = vtk.vtkDataSetMapper()
    outlineMapper.SetInput(out.GetOutput())
    outline = vtk.vtkActor()
    outline.SetMapper(outlineMapper)
    #outline.GetProperty().SetColor(0, 0, 0)
    outline.GetProperty().SetColor(1, 1, 1)
    outline.GetProperty().SetLineWidth(1)

# Renderer
renderer = vtk.vtkRenderer()
renderer.AddActor(actor)
renderer.SetBackground(0.1, 0.1, 0.1)

if drawAxes :
    # Axes
    axes = vtk.vtkAxesActor()
    l = max(volSize)*0.75
    axes.SetTotalLength(l, l, l)
    axes.SetXAxisLabelText('x')
    axes.SetYAxisLabelText('y')
    axes.SetZAxisLabelText('z')
    axes.SetShaftTypeToCylinder()
    axes.SetCylinderRadius(0.02)
    axprop = vtk.vtkTextProperty()
    axprop.SetColor(1, 1, 1)
    axprop.SetFontSize(1)
    axprop.SetFontFamilyToArial()
    axes.GetXAxisCaptionActor2D().SetCaptionTextProperty(axprop)
    axes.GetYAxisCaptionActor2D().SetCaptionTextProperty(axprop)
    axes.GetZAxisCaptionActor2D().SetCaptionTextProperty(axprop)
    renderer.AddViewProp(axes);

# Render window
renderWindow = vtk.vtkRenderWindow()
renderWindow.AddRenderer(renderer)

# Interactor
renderWindowInteractor = vtk.vtkRenderWindowInteractor()
renderWindowInteractor.SetRenderWindow(renderWindow)
 
renderWindow.Render()
renderWindowInteractor.Initialize()

# Trackball
style = vtk.vtkInteractorStyleTrackballCamera()
renderWindowInteractor.SetInteractorStyle(style)
renderWindowInteractor.Start()

# Close vtk file
reader.CloseVTKFile()
