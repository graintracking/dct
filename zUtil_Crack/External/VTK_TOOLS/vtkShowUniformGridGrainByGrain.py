#!/usr/bin/env python
# -*- coding: utf-8 -*-

import vtk
import sys
import numpy as np
import time

# Parameters
drawAxes = True
rotate = False
initAngle = -50.
fadeTime = 2.
nFade = 15
nAngle = 150
#sampleCenter = [270.7, 301.7, 123.3]

# Show vtk version
print "Using vtk version", vtk.vtkVersion.GetVTKVersion()

# Set input filename
try :
    volFile = sys.argv[1]
except :
    print 'Missing input file'
print "Input file :", volFile

# Reader
reader = vtk.vtkDataSetReader()
reader.SetFileName(volFile)
reader.Update()

# Input volume
inputVol = reader.GetOutput()

# Size
volSize = inputVol.GetDimensions()
print "size       :", volSize

# Field
fieldName = reader.GetScalarsNameInFile(0)
print "fieldName  :", fieldName

# Range
reader.ReadAllScalarsOn()
labelRange = inputVol.GetScalarRange()
if len(sys.argv) > 2 :
    startLabel = int(sys.argv[2])
else :
    startLabel = labelRange[0]
if len(sys.argv) > 3 :
    endLabel = int(sys.argv[3])
else :
    endLabel = labelRange[1]
print "startLabel :", startLabel
print "endLabel   :", endLabel
difLabel = endLabel - startLabel + 1

# Uniform Grid
uGrid = vtk.vtkUniformGrid()
uGrid.SetExtent(0, volSize[0], 0, volSize[1], 0, volSize[2])
uGrid.SetOrigin(inputVol.GetOrigin())
uGrid.SetSpacing(inputVol.GetSpacing())
if vtk.vtkVersion.GetVTKMajorVersion() <= 5:
    uGrid.SetScalarType(inputVol.GetScalarType())
else:
    uGrid.SetScalarType(inputVol.GetScalarType(), inputVol.GetInformation())

# Project point data to the cell data
array = inputVol.GetPointData().GetScalars()
uGrid.GetCellData().SetScalars(array)

# Crop
voi = vtk.vtkExtractVOI()
if vtk.vtkVersion.GetVTKMajorVersion() <= 5:
    voi.SetInputConnection(uGrid.GetProducerPort())
else:
    voi.SetInputData(uGrid)
if len(sys.argv) == 10 :
    voiXmin = int(sys.argv[4])
    voiXmax = int(sys.argv[7])
    voiYmin = int(sys.argv[5])
    voiYmax = int(sys.argv[8])
    voiZmin = int(sys.argv[6])
    voiZmax = int(sys.argv[9])
else :
    voiXmin = 0
    voiXmax = volSize[0] - 1
    voiYmin = 0
    voiYmax = volSize[1] - 1
    voiZmin = 0
    voiZmax = volSize[2] - 1
voi.SetVOI(voiXmin, voiXmax, voiYmin, voiYmax, voiZmin, voiZmax)
voi.SetSampleRate(1, 1, 1)
voi.Update()
print "voi        :", voi.GetVOI()
viewVol = voi.GetOutputPort()

# Compute grains center
print "Computing grains centers:"
centers = np.zeros([difLabel, 3])
selector = vtk.vtkThreshold()
selector.SetInputConnection(viewVol)
for i in range(difLabel) :
    label = startLabel + i
    selector.ThresholdBetween(label, label)
    selector.Update()
    centers[i] = selector.GetOutput().GetCenter()
    print "- grain idx=%03d | label: %03d | center:" % (i, label), centers[i]
sortedIndices = centers[:,2].argsort().tolist()

# Remove centers of inexistent grains
print "Removing labels of inexistent grains:"
toDel = []
for i in sortedIndices :
    if np.linalg.norm(centers[i]) == 0 :
        toDel.append(i)
for i in toDel:
    print "- label %03d (idx=%d) center:" % (i+startLabel, i), centers[i]
    sortedIndices.remove(i)
Ngrain = len(sortedIndices)
 
# Merge grid
mergedGrid = vtk.vtkUnstructuredGrid()
merge = vtk.vtkMergeCells()
merge.SetUnstructuredGrid(mergedGrid)
merge.SetTotalNumberOfDataSets(Ngrain)
merge.SetTotalNumberOfCells(viewVol.GetProducer().GetOutput().GetNumberOfCells())
merge.SetTotalNumberOfPoints(viewVol.GetProducer().GetOutput().GetNumberOfPoints())

# Update threshold
selector.ThresholdBetween(startLabel, endLabel)
selector.Update()

# Mapper
mapper = vtk.vtkDataSetMapper()
mapper.SetInputConnection(selector.GetOutputPort())
mapper.SetScalarRange(startLabel, endLabel)
mapper.SetScalarModeToUseCellData()
mapper.SetColorModeToMapScalars()

# Actor
actor = vtk.vtkActor()
actor.SetMapper(mapper)

# Renderer
renderer = vtk.vtkRenderer()
renderer.AddActor(actor)
renderer.SetBackground(0.1, 0.1, 0.1)

if drawAxes :
    # Axes
    axes = vtk.vtkAxesActor()
    l = max(volSize)*0.75
    axes.SetTotalLength(l, l, l)
    axes.SetXAxisLabelText('x')
    axes.SetYAxisLabelText('y')
    axes.SetZAxisLabelText('z')    
    renderer.AddViewProp(axes)

# Render window
renderWindow = vtk.vtkRenderWindow()
renderWindow.AddRenderer(renderer)
renderWindow.SetSize(800, 600)
renderWindow.Render()

# Camera
camera = renderer.GetActiveCamera()
camera.Elevation(initAngle)
camera.SetViewUp(0, 0, 1)
camera.SetClippingRange(1000, 3000)
#camera.SetFocalPoint(sampleCenter)
camera.SetFocalPoint(mapper.GetInput().GetCenter())

renderWindow.Render()
renderWindow.Start()

# Fade grains
if fadeTime <= 0 :
    fadeTime = 2
fadeDTime = fadeTime/nFade
for i in range(nFade, 0, -1) :
    actor.GetProperty().SetOpacity((i-1)*1./nFade)
    #time.sleep(fadeDTime)
    renderWindow.Render()

# Show volume grain by grain
if vtk.vtkVersion.GetVTKMajorVersion() <= 5:
    mapper.SetInput(merge.GetUnstructuredGrid())
else:
    mapper.SetInputData(merge.GetUnstructuredGrid())
actor.GetProperty().SetOpacity(1)
thresh = vtk.vtkThreshold()
thresh.SetInputConnection(viewVol)
thresh.SetInputArrayToProcess(1, 0, 0, 0, "Label")
nViewPerGrain = (nAngle/Ngrain)
dAngle = 360./Ngrain/nViewPerGrain
for i in sortedIndices:
    label = startLabel + i
    thresh.ThresholdBetween(label, label)
    thresh.Update()
    merge.MergeDataSet(thresh.GetOutput())
    renderWindow.Render()
    if rotate :
        for i in range(nViewPerGrain):
            camera.Azimuth(dAngle)
            renderWindow.Render()
merge.Finish()

# Rotation around sample
dAngle = 360./nAngle
for i in range(nAngle):
    renderWindow.Render()
    camera.Azimuth(dAngle)

