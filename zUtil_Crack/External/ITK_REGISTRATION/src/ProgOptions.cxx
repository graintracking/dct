#include "ProgOptions.h"

// Basic includings
#include <fstream>
#include <math.h>

// Boost includings
#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>

namespace po = boost::program_options;
namespace fsys = boost::filesystem;

using namespace std;

ProgOptions::ProgOptions() : verbose(false) { }

ProgOptions::~ProgOptions() { }

void ProgOptions::Parse(int argc, char *argv[])
{
  try {
    po::options_description po_general("General options");
    po_general.add_options()
      ("help,h", "print help message")
      ("refFile,r",
       po::value<string>(&refFile)->required(),
       "path to the image file used as reference")
      ("movFile,m",
       po::value<string>(&movFile)->required(),
       "path to the image file to be registered")
      ("outFile,o",
       po::value<string>(&outFile)->required(),
       "path to the output registered image file")
      ("maskFile,M",
       po::value<string>(&maskFile)->default_value(""),
       "path to mask image used in the metric")
      ("difFile,d",
       po::value<string>(&difFile)->default_value(""),
       "path to the difference image file (refImage - outImage)")
      ("ROI,R",
       po::value<bbox>()->multitoken(),
       "ROI start & end voxel indices")
      //("ROIstart",
      // po::value<vector<int> >(&ROIstart)->multitoken(),
      // "pixel indexes where the ROI starts (optional)")
      //("ROIend",
      // po::value<vector<int> >(&ROIend)->multitoken(),
      // "pixel indexes where the ROI ends (optional)")
      ("filterRadius,f",
       po::value<unsigned int>(&filterRadius)->default_value(2),
       "radius of the median filter")
      ("defaultPixelValue,p",
       po::value<unsigned int>(&defaultPixelValue)->default_value(0),
       "default value for pixel with inexistent value")
      ("compressOutFiles,C",
       po::bool_switch(&compressOutFiles)->default_value(false),
       "compress output TIFF files")
      ("verbose,v",
       po::bool_switch(&verbose)->default_value(false),
       "verbose mode")
      ;

    po::options_description po_registration("Registration options");
    po_registration.add_options()
      ("registration.numberOfLevels",
       po::value<unsigned int>(&regNumberOfLevels)->required(),
       "number of levels in the pyramid process (3-6 seems ok)")
      ;

    po::options_description po_metric("Metric options");
    po_metric.add_options()
      ("metric.sampleFraction",
       po::value<double>(&metricSampleFraction)->default_value(0.01),
       "fraction of input image pixels used as samples")
      ("metric.numberOfBins",
       po::value<unsigned int>(&metricNumberOfBins)->default_value(0),
       "number of histogram boxes in the metric (overwrites sampleFraction)")
      ("metric.numberOfSamples",
       po::value<unsigned long int>(&metricNumberOfSamples)->default_value(0),
       "number of pixels used as samples in the metric")
      ;

    po::options_description po_initializer("Initializer options");
    po_initializer.add_options()
      ("initializer.translation",
       //po::value<vector<double> >(&initTranslation)->multitoken()->required(),
       po::value<coord>()->multitoken()->required(),
       "initial translation")
      ("initializer.rotAxis",
       //po::value<vector<double> >(&initRotAxis)->multitoken()->required(),
       po::value<coord>()->multitoken()->required(),
       "initial rotation axis")
      ("initializer.rotAngle",
       po::value<double>(&initRotAngle)->required(),
       "initial rotation angle")
      ("initializer.rotVersor",
       //po::value<vector<double> >(&initRotVersor)->multitoken(),
       po::value<coord>()->multitoken(),
       "initial rotation versor")
      ("initializer.rotCenter",
       //po::value<vector<double> >(&initRotCenter)->multitoken(),
       po::value<coord>()->multitoken(),
       "initial rotation center (centered if empty)")
      //("initializer.rotOffset",
      // //po::value<vector<double> >(&initRotOffset)->multitoken(),
      // po::value<coord>()->multitoken(),
      // "offset applied the initial rotation center found by the initializer")
      ;

    po::options_description po_optimizer("Optimizer options");
    po_optimizer.add_options()
      ("optimizer.minStepLength",
       po::value<double>(&optMinStepLength)->default_value(0.1, "0.1"),
       "initial minimum step length")
      ("optimizer.maxStepLength",
       po::value<double>(&optMaxStepLength)->default_value(2.0, "2.0"),
       "initial maximum step length")
      ("optimizer.rotScale",
       po::value<double>(&optRotScale)->default_value(1.0, "1.0"),
       "optimizer scale for rotation parameters")
      ("optimizer.transScale",
       po::value<double>(&optTransScale)->default_value(1.e-4, "1.e-4"),
       "optimizer scale for translation parameters")
      ("optimizer.maxIterations",
       po::value<unsigned long int>(&optMaxIterations)->default_value(300),
       "maximum number of optimizer iterations for each pyramid level")
      ("optimizer.relaxFactor",
       po::value<double>(&optRelaxFactor)->default_value(0.5, "0.5"),
       "relaxation factor in the gradient descent optimizer")
      ;

    po::options_description po_config;
    po_config.add_options()
      ("configFile,c",
       po::value<string>(&configFile)->default_value(""),
       "configuration file containing parameters values")
      ;

    // Gather options
    po_general.add(po_config);
    po::options_description all_options;
    all_options.add(po_general).add(po_registration).add(po_metric);
    all_options.add(po_initializer).add(po_optimizer);

    // Parse config file command line option, then parse config file, then all
    // command line option
    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, all_options,
          po::command_line_style::unix_style), vm);
    //po::store(po::basic_command_line_parser<char>(argc, argv)
    //.options(po_config).style(po::command_line_style::unix_style)
    //.allow_unregistered().run(), vm);

    if (!vm["configFile"].defaulted()) {
      string configFilename = vm["configFile"].as<string>();
      if (verbose) cout << "Config file: " << configFilename << endl;
      ifstream configFile(configFilename.c_str());
      po::store(po::parse_config_file<char>(configFile, all_options), vm);
      configFile.close();
    }
    //po::store(po::parse_command_line(argc, argv, all_options,
    //    po::command_line_style::unix_style), vm);
    if (argc == 1 || vm.count("help")) {
      cerr << "Usage: " << fsys::basename(argv[0]) << " [options]" << endl;
      cerr << all_options << endl;
      exit(EXIT_FAILURE);
    }
    po::notify(vm);
    if (vm.count("ROI")) {
      ROI = vm["ROI"].as<bbox>().v;
    }
    if (vm.count("initializer.translation")) {
      initTranslation = vm["initializer.translation"].as<coord>().v;
    }
    if (vm.count("initializer.rotAxis")) {
      initRotAxis = vm["initializer.rotAxis"].as<coord>().v;
    }
    if (vm.count("initializer.rotVersor")) {
      initRotCenter = vm["initializer.rotVersor"].as<coord>().v;
    }
    if (vm.count("initializer.rotCenter")) {
      initRotCenter = vm["initializer.rotCenter"].as<coord>().v;
    }
    //if (vm.count("initializer.rotOffset")) {
    //  initRotOffset = vm["initializer.rotOffset"].as<coord>().v;
    //}
  }
  catch (std::exception& e) {
    cout << e.what() << endl;
    exit(EXIT_FAILURE);
  }
}


bool ProgOptions::Check()
{
  // Check input and output image file names
  if (refFile==movFile) {
    cout << "Reference and moving files have the same name!" << endl;
    return false;
  }
  if (refFile==outFile) {
    cout << "Reference and output files have the same name!" << endl;
    return false;
  }
  if (refFile==difFile) {
    cout << "Reference and difference files have the same name!" << endl;
    return false;
  }
  if (movFile==outFile) {
    cout << "Moving and output files have the same name!" << endl;
    return false;
  }
  if (movFile==difFile) {
    cout << "Moving and difference files have the same name!" << endl;
    return false;
  }
  if (outFile==difFile) {
    cout << "Output and difference files have the same name!" << endl;
    return false;
  }

  // Check filter parameter
  if (filterRadius < 0) {
    cout << "Filter radius should be positive or zero !" << endl;
    return false;
  }

  // Check initializer parameters
  if (initRotAxis[2]==0) {
    cout << "You set the z rotation axis offset 0! " 
      << "Will be set to 1 due to a known bug in c++/itk. "
      << "Otherwise a segmentation fault will be thrown." << endl;
    initRotAxis[2] = 1.0;
  }
  if (fabs(initRotAngle) > 3.15) {
    cout << "Rotation angle is set to: " << initRotAngle << endl;
    cout << "Remember this angle is in radian so please give another value."
      << endl;
    return false;
  }

  // Check metric parameters
  if (metricSampleFraction <= 0 || metricSampleFraction > 1) {
    cout << "metric.sampleFraction should be set between 0 and 1." << endl;
    return false;
  }
  if (metricNumberOfBins <= 0) {
    cout << "metric.numberOfBins should be positive." << endl;
    return false;
  }

  // Check optimizer parameters
  if (optMaxIterations == 0) {
    cout << "You forgot to set optimizer.maxIterations for last pyramid step."
      << endl;
    return false;
  }
  if (optTransScale <= 0) {
    cout << "optimizer.transScale should be positive." << endl;
    return false;
  }
  if (optRotScale <= 0) {
    cout << "optimizer.rotScale should be positive." << endl;
    return false;
  }
  if (optRelaxFactor <= 0 || optRelaxFactor >= 1) {
    cout << "optmizer.relaxFactor should be set between 0 and 1." << endl;
    return false;
  }
  if (optMinStepLength <= 0) {
    cout << "optimizer.minStepLength should be positive." << endl;
    return false;
  }
  if (optMaxStepLength <= 0) {
    cout << "optmizer.maxStepLength should be positive." << endl;
    return false;
  }

  // Check registration parameters
  if (regNumberOfLevels < 1) {
    cout << "You set the number of pyramidlevels to smaller than 1!" << endl;
    regNumberOfLevels = 3;
    cout << "Will be set to " << regNumberOfLevels << " now." << endl;
  }

  //// Check ROI parameters
  //ROI = false;
  //if (ROIstart.size()>0 && ROIend.size()>0) {
  //  if (ROIstart.size()!=3 || ROIend.size()!=3) {
  //    cout << "ROIstart and ROIend must both have 3 components!" << endl;
  //    return false;
  //  }
  //  ROI = true;
  //}
  return true;
}

void ProgOptions::Summary()
{
  cout << "Options summary" << endl;
  cout << "- refFile:           " << refFile << endl;
  cout << "- movFile:           " << movFile << endl;
  cout << "- outFile:           " << outFile << 
    (compressOutFiles ? " (compressed)" : "" ) << endl;
  cout << "- difFile:           " << (difFile.size() ? difFile : "none" ) <<
      ((difFile.size() && compressOutFiles) ? " (compressed)" : "" ) << endl;
  cout << "- maskFile:          " << (maskFile.size() ? maskFile : "none" ) 
      << endl;
  cout << "- defaultPixelValue: " << defaultPixelValue << endl;
  cout << "- filterRadius:      " << filterRadius << endl;
  cout << "- ROI:               " << ROI << endl;
  //cout << "- ROIstart:          " << ROIstart << endl;
  //cout << "- ROIend:            " << ROIend << endl;
  cout << "- configFile:        " << configFile << endl;
  cout << "[registration]" << endl;
  cout << "- numberOfLevels:    " << regNumberOfLevels << endl;
  cout << "[metric]" << endl;
  cout << " - numberOfBins:     " << metricNumberOfBins << endl; 
  cout << " - numberOfSamples:  " << metricNumberOfSamples << endl; 
  cout << " - sampleFraction:   " << metricSampleFraction << endl; 
  cout << "[initializer]" << endl;
  cout << "- rotAngle:          " << initRotAngle << endl;
  cout << "- rotAxis:           " << initRotAxis << endl;
  cout << "- translation:       " << initTranslation << endl;
  cout << "- rotCenter:         " << initRotCenter << endl;
  //cout << "- rotOffset:         " << initRotOffset << endl;
  cout << "- centered:          " << (initCentered ? "true" : "false") << endl;
  cout << "[optimizer]" << endl;
  cout << "- maxIterations:     " << optMaxIterations << endl;
  cout << "- minStepLength:     " << optMinStepLength << endl;
  cout << "- maxStepLength:     " << optMaxStepLength << endl;
  cout << "- rotScale:          " << optRotScale << endl;
  cout << "- transScale:        " << optTransScale << endl;
  cout << "- relaxFactor:       " << optRelaxFactor << endl;
  //cout << "- learningRate:      " << optLearningRate << endl;
  return;
}

static void validate(boost::any& v, const vector<string>& val, bbox*, int) {
  po::validators::check_first_occurrence(v);
  vector<int> dvalues;
  for (vector<string>::const_iterator it=val.begin(); it!=val.end(); ++it) {
    stringstream ss(*it);
    copy(istream_iterator<int>(ss), istream_iterator<int>(),
        back_inserter(dvalues));
    if (!ss.eof())
      throw po::validation_error(po::validation_error::invalid_option_value,
          "Invalid bbox specification");
  }
  if (dvalues.size() != 6)
    throw po::validation_error(po::validation_error::invalid_option_value,
        "Invalid bbox specification");
  bbox b(dvalues);
  v = b;
}

static void validate(boost::any& v, const vector<string>& val, coord*, int) {
  po::validators::check_first_occurrence(v);
  vector<double> dvalues;
  for (vector<string>::const_iterator it = val.begin(); it != val.end(); ++it) {
    stringstream ss(*it);
    copy(istream_iterator<double>(ss), istream_iterator<double>(),
        back_inserter(dvalues));
    if (!ss.eof())
      throw po::validation_error(po::validation_error::invalid_option_value,
          "Invalid coordinate specification");
  }
  if(dvalues.size() != 3)
    throw po::validation_error(po::validation_error::invalid_option_value,
        "Invalid coordinate specification");
  coord b(dvalues);
  v = b;
}

