// A small registration program.

// Personal options interface headers
#include "ProgOptions.h"

// Basic includes
#include <iomanip>
#include <fstream>

// ITK includes
#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"
#include "itkTIFFImageIO.h"
#include "itkImage.h"

// Registration includes
#include "itkLinearInterpolateImageFunction.h"
#include "itkResampleImageFilter.h"
#include "itkMedianImageFilter.h"
#include "itkCastImageFilter.h"
#include "itkRescaleIntensityImageFilter.h"
#include "itkSubtractImageFilter.h"
#include "itkAffineTransform.h"
#include "itkVersorRigid3DTransform.h"
#include "itkCenteredTransformInitializer.h"
#include "itkRegularStepGradientDescentOptimizer.h"
#include "itkMultiResolutionImageRegistrationMethod.h"
#include "itkMultiResolutionPyramidImageFilter.h"
#include "itkMattesMutualInformationImageToImageMetric.h"
#include "itkImageMaskSpatialObject.h"
#include "itkTransformFileWriter.h"

using namespace std;

#define DIMENSION 3
#define SEED 666 // Seedpoint for the metric random generator, no need to set this from outside
#define DEFAULT_LEVEL_REFINE_MIN_STEPLENGTH 2.
#define DEFAULT_LEVEL_REFINE_MAX_STEPLENGTH 10.

class myProgOptions : public ProgOptions {
  public:
    myProgOptions() : ProgOptions() { }
};

template <typename TRegistration>
class RegistrationInterfaceCommand : public itk::Command {
  public:
    typedef RegistrationInterfaceCommand Self;
    typedef itk::Command                 Superclass;
    typedef itk::SmartPointer<Self>      Pointer;
    itkNewMacro(Self);

  protected:
    RegistrationInterfaceCommand() {};

    typedef TRegistration                        RegistrationType;
    typedef RegistrationType*                    RegistrationPointer;
    typedef itk::RegularStepGradientDescentOptimizer OptimizerType;
    typedef OptimizerType*                       OptimizerPointer;

    void Execute(itk::Object* object, const itk::EventObject& event) {

      if (!(itk::IterationEvent().CheckEvent(&event))) {
        return;
      }

      RegistrationPointer registration = dynamic_cast<RegistrationPointer>(object);
      OptimizerPointer    optimizer    = dynamic_cast<OptimizerPointer>(registration->GetOptimizer());

      cout << flush;
      cout << "=== Pyramid Level: " << registration->GetCurrentLevel() + 1
        << " ===" << endl;
      if (registration->GetCurrentLevel() > 0) {
        //optimizer->SetMaximumStepLength(optimizer->GetCurrentStepLength());
        optimizer->SetMaximumStepLength(optimizer->GetMaximumStepLength()/DEFAULT_LEVEL_REFINE_MAX_STEPLENGTH);
        optimizer->SetMinimumStepLength(optimizer->GetMinimumStepLength()/DEFAULT_LEVEL_REFINE_MIN_STEPLENGTH);
      }
    }

    void Execute(const itk::Object*, const itk::EventObject&) {
      return;
    }
};

// Just observing
class CommandIterationUpdate : public itk::Command {
  public:
    typedef CommandIterationUpdate  Self;
    typedef itk::Command            Superclass;
    typedef itk::SmartPointer<Self> Pointer;
    itkNewMacro(Self);

  protected:
    CommandIterationUpdate(): m_CumulativeIterationIndex(0) {};

  public:
    typedef itk::RegularStepGradientDescentOptimizer OptimizerType;
    typedef const OptimizerType*                 OptimizerPointer;

    void Execute(itk::Object* caller, const itk::EventObject& event) {
      Execute((const itk::Object*)caller, event);
    }
    void Execute(const itk::Object* object, const itk::EventObject& event) {
      OptimizerPointer optimizer = dynamic_cast<OptimizerPointer>(object);

      if (!(itk::IterationEvent().CheckEvent(&event))) {
        return;
      }
      cout << optimizer->GetCurrentIteration()+1 << " / ";
      cout << ++m_CumulativeIterationIndex << " = ";
      cout << optimizer->GetValue() << " : ";
      cout << optimizer->GetCurrentPosition() << " / ";
      cout << optimizer->GetCurrentStepLength() << endl;
      if (optimizer->GetCurrentIteration()%10==0) cout << flush;
    }

    unsigned int GetCumulativeIterationIndex() {
      return m_CumulativeIterationIndex;
    }

  private:
    unsigned int m_CumulativeIterationIndex;
};

time_t StartTimer(const string& taskName);
double StopTimer(const string& taskName, time_t startTaskTime);

int main(int argc, char *argv[]) {

  // Parse command line arguments
  myProgOptions par;
  par.Parse(argc, argv);
  if (!par.Check()) return EXIT_FAILURE;
  if (par.verbose) par.Summary();
 
  // Set the dimension and pixel types
  const unsigned int    Dimension = 3;
  typedef unsigned char InputPixelType;
  typedef unsigned char OutputPixelType;
  typedef float         InternalPixelType;

  // Set the type of the input, output and internal images
  typedef itk::Image<InputPixelType,    Dimension> FixedImageType;
  typedef itk::Image<InputPixelType,    Dimension> MovingImageType;
  typedef itk::Image<OutputPixelType,   Dimension> OutputImageType;
  typedef itk::Image<InternalPixelType, Dimension> InternalImageType; // Increasing the memory usage...

  // Set the type of the registration
  typedef itk::MultiResolutionImageRegistrationMethod<InternalImageType, InternalImageType> RegistrationType;

  // Set the type of all the components of the registration process:
  // transform, initializer, interpolator, metric, optimizer
  typedef itk::AffineTransform<double> TransformType;
  typedef itk::CenteredTransformInitializer<TransformType, InternalImageType, InternalImageType> TransformInitializerType;
  typedef itk::LinearInterpolateImageFunction<InternalImageType, double> InterpolatorType;
  typedef itk::MattesMutualInformationImageToImageMetric<InternalImageType, InternalImageType> MetricType;
  typedef itk::RegularStepGradientDescentOptimizer OptimizerType;

  // Start the registration program
  cout << endl;
  cout << "***********************" << endl;
  cout << "*** 3D registration ***" << endl;
  cout << "***********************" << endl;
  cout << endl;

  time_t startProgramTime = StartTimer("Program");

  time_t startReadingTime = StartTimer("Reading");

  // Instantiate and setup the input image readers
  typedef itk::ImageFileReader<FixedImageType>  FixReaderType;
  typedef itk::ImageFileReader<MovingImageType> MovReaderType;
  FixReaderType::Pointer refReader = FixReaderType::New();
  MovReaderType::Pointer movReader = MovReaderType::New();
  refReader->SetFileName(par.refFile);
  movReader->SetFileName(par.movFile);

  // Load images
  cout << "Read images" << endl;
  try {
    cout << "- Reference image: " << par.refFile << endl;
    refReader->Update();
  }
  catch(itk::ExceptionObject &EO) {
    cout << "Exception caught !" << endl;
    cout << EO << endl;
    return EXIT_FAILURE;
  }
  FixedImageType::Pointer refImage = refReader->GetOutput();
  cout << "  origin is " << refImage->GetOrigin() << endl;
  cout << "  spacing is " << refImage->GetSpacing() << endl;
  try {
    cout << "- Moving image: " << par.movFile << endl;
    movReader->Update();
  }
  catch(itk::ExceptionObject &EO) {
    cout << "Exception caught !" << endl;
    cout << EO << endl;
    return EXIT_FAILURE;
  }
  MovingImageType::Pointer movImage = movReader->GetOutput();
  cout << "  origin is " << movImage->GetOrigin() << endl;
  cout << "  spacing is " << movImage->GetSpacing() << endl;
  cout << "Reading finished!" << endl;
  cout << endl;

  double readingRunTime = StopTimer("Reading", startReadingTime);

  time_t startFilteringTime = StartTimer("Filtering");

  // Instantiate and setup the input image filters
  typedef itk::MedianImageFilter<FixedImageType,  InternalImageType> FixedFilterType;
  typedef itk::MedianImageFilter<MovingImageType, InternalImageType> MovingFilterType;
  FixedFilterType::Pointer  refFilter = FixedFilterType::New();
  MovingFilterType::Pointer movFilter = MovingFilterType::New();

  InternalImageType::SizeType radius;
  radius.Fill(par.filterRadius);
  refFilter->SetRadius(radius);
  refFilter->SetInput(refImage);
  movFilter->SetRadius(radius);
  movFilter->SetInput(movImage);

  // Apply filter on both images
  try {
    cout << "Filtering images (median filter with radius = " << par.filterRadius << ")" << endl;
    cout << "- Reference image" << endl;
    refFilter->Update();
    cout << "- Moving image"  << endl;
    movFilter->Update();
    cout << "Filtering finished!" << endl;
    cout << endl;
  }
  catch(itk::ExceptionObject &EO) {
    cout << "A failure occured !" << endl;
    cout << EO << endl;
    return EXIT_FAILURE;
  }
  InternalImageType::Pointer refFilteredImage = refFilter->GetOutput();
  InternalImageType::Pointer movFilteredImage = movFilter->GetOutput();
  double filteringRunTime = StopTimer("Filtering", startFilteringTime);

  time_t startRegistrationTime = StartTimer("Registration");

  // Instantiate and setup the registration
  cout << "Setting the registration" << endl;
  RegistrationType::Pointer registration = RegistrationType::New();
  registration->SetFixedImage(refFilteredImage);
  registration->SetMovingImage(movFilteredImage);

  // Setup the fixed image region (full image or ROI)
  if (par.ROI.size()) {
    // ROI variables
    InternalImageType::SizeType   size;
    InternalImageType::IndexType  start;
    InternalImageType::IndexType  end;
    InternalImageType::RegionType ROI;

    // Set ROI origin and size
    for (unsigned int idim=0; idim<Dimension; idim++) {
      start[idim] = par.ROI[idim];
      end[idim]   = par.ROI[idim+3];
      size[idim]  = end[idim] - start[idim];
    }

    // Set a region of interest to reduce computing time
    ROI.SetIndex(start);
    ROI.SetSize(size);
    registration->SetFixedImageRegion(ROI);

    cout << "- using region of interest with size " << size << endl;
    cout << "  from pixel : " << start << endl; 
    cout << "  to pixel   : " << end << endl;
  }
  else {
    cout << "- using the entire image" << endl;
    registration->SetFixedImageRegion(refFilteredImage->GetBufferedRegion());
  }
  cout << endl;

  // Instantiate the transform
  TransformType::Pointer transform = TransformType::New();

  // Instantiate and setup the transform initializer
  TransformInitializerType::Pointer initializer = TransformInitializerType::New();
  initializer->SetTransform(transform);
  initializer->SetFixedImage(refFilteredImage);
  initializer->SetMovingImage(movFilteredImage);
  //initializer->MomentsOn(); // Center of mass
  initializer->GeometryOn(); // Geometric center
  initializer->InitializeTransform();

  // Setup initial rotation and translation of the transform
  typedef itk::VersorRigid3DTransform<double> InitTransformType;
  InitTransformType::Pointer initTransform = InitTransformType::New();
  typedef InitTransformType::VersorType VersorType;
  typedef VersorType::VectorType    VectorType;
  VectorType translation; // Initial translation
  translation.Fill(0);
  if (par.initTranslation.size() == Dimension) {
    for (unsigned int idim=0; idim<Dimension; idim++)
      translation[idim] = par.initTranslation[idim];
    cout << "Initializing translation: " << translation << endl;
  }

  // Initial rotation
  VersorType rotation;
  if (par.initRotVersor.size() == Dimension) {
    VectorType rotVersor;
    for (unsigned int idim=0; idim<Dimension; idim++)
      rotVersor[idim] = par.initRotVersor[idim];
    cout << "Using versor: " << rotVersor << endl;
    rotation.Set(rotVersor);
  } else {
    VectorType rotAxis;
    rotAxis.Fill(0);
    rotAxis[2] = 1;
    if (par.initRotAxis.size() == Dimension) {
      for (unsigned int idim=0; idim<Dimension; idim++) 
        rotAxis[idim] = par.initRotAxis[idim];
      cout << "Initializing rotation axis: " << rotAxis << endl;
    }
    rotation.Set(rotAxis, par.initRotAngle);
  }
  initTransform->SetRotation(rotation);

  // Setup the transform
  transform->SetMatrix(initTransform->GetMatrix());
  transform->SetTranslation(translation);

  // Setup center
  if (par.initRotCenter.size() == Dimension) {
    TransformType::ParametersType fixedParameters;
    for (unsigned int idim=0; idim<Dimension; idim++)
      fixedParameters[idim] = par.initRotCenter[idim];
    cout << "Using center: " << par.initRotCenter << endl;
    transform->SetFixedParameters(fixedParameters);
  }

  // Instantiate and setup the interpolator
  InterpolatorType::Pointer interpolator = InterpolatorType::New();

  // Instantiate and setup the metric
  MetricType::Pointer metric = MetricType::New();
  metric->SetNumberOfHistogramBins(par.metricNumberOfBins);
  if (par.metricNumberOfSamples == 0) {
    par.metricNumberOfSamples = (unsigned long)(par.metricSampleFraction * registration->GetFixedImageRegion().GetNumberOfPixels());
  }
  metric->SetNumberOfSpatialSamples(par.metricNumberOfSamples);
  metric->ReinitializeSeed(SEED);

  // Instantiate mask as spatial object
  typedef itk::ImageMaskSpatialObject<Dimension> MaskType;
  MaskType::Pointer spatialObjectMask = MaskType::New();

  if (par.maskFile.size()) {
    // Set mask image type
    typedef itk::Image<InputPixelType, Dimension> ImageMaskType;

    // Instantiate mask reader
    typedef itk::ImageFileReader<ImageMaskType>   MaskReaderType;
    MaskReaderType::Pointer maskReader = MaskReaderType::New();
    maskReader->SetFileName(par.maskFile);

    // Read mask
    try {
      cout << "Reading mask image" << endl;
      cout << "- Mask image: " << par.maskFile << endl;
      maskReader->Update();
    }
    catch(itk::ExceptionObject& EO) {
      cout << "ExceptionObject caught !" << endl;
      cout << EO << endl;
      return EXIT_FAILURE;
    }
    cout << "Reading finished!" <<endl;
    cout << endl;
    // Apply the mask to the metric
    spatialObjectMask->SetImage(maskReader->GetOutput());
    metric->SetFixedImageMask(spatialObjectMask);
    metric->SetMovingImageMask(spatialObjectMask);
  }

  // Setup the optimizer
  OptimizerType::Pointer optimizer = OptimizerType::New();
  optimizer->SetMinimumStepLength(par.optMinStepLength);
  optimizer->SetMaximumStepLength(par.optMaxStepLength);
  optimizer->SetNumberOfIterations(par.optMaxIterations);

  // Setup optimizer scale
  typedef OptimizerType::ScalesType OptimizerScalesType;
  OptimizerScalesType optimizerScales(transform->GetNumberOfParameters());
  optimizerScales.Fill(par.optRotScale);
  optimizerScales[9] = par.optTransScale;
  optimizerScales[10] = par.optTransScale;
  optimizerScales[11] = par.optTransScale;

  optimizer->SetScales(optimizerScales);

  // Setup pyramid component
  typedef itk::MultiResolutionPyramidImageFilter<InternalImageType, InternalImageType> ReferenceImagePyramidType;
  typedef itk::MultiResolutionPyramidImageFilter<InternalImageType, InternalImageType> MovingImagePyramidType;
  ReferenceImagePyramidType::Pointer refPyramid = ReferenceImagePyramidType::New();
  MovingImagePyramidType::Pointer    movPyramid = MovingImagePyramidType::New();

  // Connect the components to the registration module
  registration->SetMetric(metric);
  registration->SetOptimizer(optimizer);
  registration->SetTransform(transform);
  registration->SetInterpolator(interpolator);
  registration->SetFixedImagePyramid(refPyramid);
  registration->SetMovingImagePyramid(movPyramid);
  registration->SetInitialTransformParameters(transform->GetParameters());

  // Set Number of pyramids
  registration->SetNumberOfLevels(par.regNumberOfLevels);

  // Observation of the process
  typedef RegistrationInterfaceCommand<RegistrationType> CommandType;
  CommandType::Pointer command  = CommandType::New();
  registration->AddObserver(itk::IterationEvent(), command);
  CommandIterationUpdate::Pointer observer = CommandIterationUpdate::New();
  optimizer->AddObserver(itk::IterationEvent(), observer);

  // Start the registration process
  try {
    cout << "Launching registration" << endl;
    cout << "0 / 0 =    N.A.   : " << registration->GetTransform()->GetParameters() << " / ";
    //cout << "0 / 0 = " << optimizer->GetValue() << " : ";
    //cout << optimizer->GetInitialPosition() << " / ";
    cout << optimizer->GetCurrentStepLength() << endl;
    //registration->StartRegistration(); // obsolete call
    registration->Update();
  }
  catch(itk::ExceptionObject &EO) {
    cout << "Exception caught !" << endl;
    cout << EO << endl;
    return EXIT_FAILURE;
  }
  cout << "Optimizer stop condition: " << endl;
  cout<< registration->GetOptimizer()->GetStopConditionDescription() << endl;
  cout << "Optimizer Stop Condition = " << optimizer->GetStopCondition() << endl;
  cout << "Registration finished!" << endl;
  cout << endl;

  // Get final parameters for the transformation
  typedef RegistrationType::ParametersType ParametersType;
  ParametersType finalParameters = registration->GetLastTransformParameters();
  ParametersType finalFixedParameters = transform->GetFixedParameters();

  const unsigned int numberOfIterations = optimizer->GetCurrentIteration();
  const double bestValue = optimizer->GetValue();
  const int lastPyramidLevel = registration->GetCurrentLevel();

  // Instantiate and setup the final transform
  TransformType::Pointer finalTransform = TransformType::New();
  finalTransform->SetParameters(finalParameters);
  finalTransform->SetFixedParameters(finalFixedParameters);
  //finalTransform->SetCenter(transform->GetCenter());

  cout << "==================================================" << endl;
  cout << "============== Optimization results ==============" << endl;
  //cout << endl;
  cout << "Optimizer:" << endl;
  cout << "- Pyramid level        = " << lastPyramidLevel << " / " << par.regNumberOfLevels << endl;
  cout << "- Last level iteration = " << numberOfIterations << endl;
  cout << "- Total iterations     = " << observer->GetCumulativeIterationIndex() << endl;
  cout << "- Metric value         = " << bestValue << endl;
  cout << "Parameters:" << endl;
  cout << "- Transformation matrix = " << endl << finalTransform->GetMatrix();
  cout << "- Translation = " << finalTransform->GetTranslation() << endl;
  cout << "- Offset      = " << finalTransform->GetOffset() << endl;
  cout << "- Center      = " << finalTransform->GetCenter() << endl;
  //cout << "- Scale     = " << finalTransform->GetScale() << endl;
  cout << "Fixed parameters:" << endl;
  cout << finalFixedParameters << endl;
  cout << endl;
  cout << "==================================================" << endl;
  cout << endl;

  double registrationRunTime = StopTimer("Registration", startRegistrationTime);

  time_t startWritingTime = StartTimer("Writing");

  // Instantiate and setup the final transform writer
  itk::TransformFileWriter::Pointer writer = itk::TransformFileWriter::New();
  writer->SetInput(finalTransform);
  writer->SetFileName(par.outFile + ".tfm");
  writer->Update();

  // Instantiate and setup the final image resampler
  typedef itk::ResampleImageFilter<MovingImageType, InternalImageType> ResampleFilterType;
  ResampleFilterType::Pointer resampler = ResampleFilterType::New();
  resampler->SetSize(refImage->GetLargestPossibleRegion().GetSize());
  resampler->SetOutputOrigin(refImage->GetOrigin());
  resampler->SetOutputSpacing(refImage->GetSpacing());
  resampler->SetDefaultPixelValue(par.defaultPixelValue);

  // Connect the final transform to the resampler filter
  resampler->SetInput(movImage);
  //resampler->SetTransform(registration->GetOutput()->Get());
  resampler->SetTransform(finalTransform);

  // Instantiante and setup the output image caster
  typedef itk::CastImageFilter<InternalImageType, OutputImageType> CastFilterType;
  CastFilterType::Pointer outCaster = CastFilterType::New();
  outCaster->SetInput(resampler->GetOutput());

  // Set TIFF IO compression mode
  typedef itk::TIFFImageIO IOType;
  IOType::Pointer io = IOType::New();
  io->SetCompressionToLZW();

  // Write output image
  typedef itk::ImageFileWriter<OutputImageType> WriterType;
  WriterType::Pointer outWriter = WriterType::New();
  outWriter->SetImageIO(io);
  outWriter->SetFileName(par.outFile);
  outWriter->SetInput(outCaster->GetOutput());
  outWriter->SetUseCompression(par.compressOutFiles);
  outWriter->Update();

  // Set reference image caster
  typedef itk::CastImageFilter<FixedImageType,  InternalImageType> ReferenceCasterType;
  ReferenceCasterType::Pointer refCaster = ReferenceCasterType::New();
  refCaster->SetInput(refImage);
  
  // Set difference image 
  typedef itk::SubtractImageFilter<InternalImageType, InternalImageType, InternalImageType> DifferenceFilterType;
  DifferenceFilterType::Pointer difference = DifferenceFilterType::New();
  difference->SetInput1(refCaster->GetOutput());
  difference->SetInput2(resampler->GetOutput());

  // The output is very dark. Colors set between <0,1> so we have to rescale the old intensity.
  typedef itk::RescaleIntensityImageFilter<InternalImageType, OutputImageType> RescalerType;
  RescalerType::Pointer intensityRescaler = RescalerType::New();
  intensityRescaler->SetInput(difference->GetOutput());
  intensityRescaler->SetOutputMinimum(0);
  intensityRescaler->SetOutputMaximum(255);

  // Write difference image
  if (par.difFile.size()) {
    WriterType::Pointer difWriter = WriterType::New();
    difWriter->SetImageIO(io);
    difWriter->SetInput(intensityRescaler->GetOutput());
    difWriter->SetFileName(par.difFile);
    difWriter->SetUseCompression(par.compressOutFiles);
    difWriter->Update();
  }

  double writingRunTime = StopTimer("Writing", startWritingTime);

  // Sum up process runtimes
  double programRunTime = StopTimer("Program", startProgramTime);
  int width = 12;
  int precision = 2;
  cout << setprecision(precision);
  cout << setiosflags(ios::left);
  cout << "*********************************************************" << endl;
  cout << "******************* Run time summary ********************" << endl;
  cout << "*********************************************************" << endl;
/*
  cout << "Reading      : " << sprintf("%12.2f", readingRunTime)      << endl;
  cout << "Casting      : " << sprintf("%12.2f", castingRunTime)      << endl;
  cout << "Registration : " << sprintf("%12.2f", registrationRunTime) << endl;
  cout << "Writing      : " << sprintf("%12.2f", writingRunTime)      << endl;
  cout << "Total [s]    : " << sprintf("%12.2f", programRunTime)      << endl;
*/
  cout << "Reading      : " << setw(width) << readingRunTime      << endl;
  cout << "Filtering    : " << setw(width) << filteringRunTime    << endl;
  cout << "Registration : " << setw(width) << registrationRunTime << endl;
  cout << "Writing      : " << setw(width) << writingRunTime      << endl;
  cout << "Total [s]    : " << setw(width) << programRunTime      << endl;
  cout << "*********************************************************" << endl;
  cout << resetiosflags(ios::left);

  return EXIT_SUCCESS;
}

time_t StartTimer(const string& task)
{
  time_t startTaskTime;
  time(&startTaskTime);
  //cout << "TIME " << task <<" process starting @ " <<
  //  asctime(localtime(&startTaskTime)) << endl;
  return startTaskTime; 
}

double StopTimer(const string& taskName, time_t startTaskTime)
{
  time_t stopTaskTime;
  time(&stopTaskTime);
  double taskRunTime = difftime(stopTaskTime, startTaskTime);
  //cout << "TIME " << taskName << " process stopping @ "
  //  << asctime(localtime(&stopTaskTime))
  //  << "TIME " << taskName << " process runtime: "
  //  << taskRunTime << "s" << endl;
  //cout << endl;
  return taskRunTime;
}
