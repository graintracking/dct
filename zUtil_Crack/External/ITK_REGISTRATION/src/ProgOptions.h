#ifndef __ProgOptions_h__
#define __ProgOptions_h__

// Basic includings
#include <cassert>
#include <iostream>
#include <iterator>
#include <vector>
#include <stdlib.h>
#include <string.h>

class ProgOptions {
  public:
    // General options
    std::string refFile;
    std::string movFile;
    std::string outFile;
    std::string difFile;
    std::string maskFile;
    std::string configFile;
    unsigned int defaultPixelValue;
    unsigned int filterRadius;
    bool compressOutFiles;
    bool verbose;

    // Registration options
    unsigned int regNumberOfLevels;    // OLD NumOLevels
    //bool ROI;
    //std::vector<int> ROIstart; // startROI
    //std::vector<int> ROIend;   // endROI
    std::vector<int> ROI;

    // Metric options
    unsigned int metricNumberOfBins; // OLD NumOBins
    unsigned long metricNumberOfSamples; // OLD NumOSamples
    double metricSampleFraction;     // OLD sampleFraction

    // Initializer options
    bool initCentered;                   // OLD centeredInitializer
    double initRotAngle;                 // OLD RotationAngle
    std::vector<double> initRotAxis;     // OLD RotationAxis
    std::vector<double> initTranslation; // OLD TranslationAxis
    std::vector<double> initRotVersor;
    std::vector<double> initRotCenter;
    //std::vector<double> initRotOffset;

    // Optimizer options
    unsigned long optMaxIterations; // OLD maxOptIter
    double optMinStepLength;        // OLD minOptStepL
    double optMaxStepLength;        // OLD maxOptStepL
    double optRotScale;             // OLD OptScale
    double optTransScale;           // OLD TransScale
    double optRelaxFactor;          // OLD relaxFactor
    //double optLearningRate;         // TO PUT IN OPTIONS > 0

    ProgOptions();
    virtual ~ProgOptions();
    virtual void Parse(int argc, char *argv[]);
    virtual bool Check();
    virtual void Summary();
};

struct bbox {
  public:
    std::vector<int> v;
    bbox() { v.resize(6, 0); }
    bbox(std::vector<int> i) { assert(i.size()==6); v=i; }
    size_t size() { return v.size(); }
    int& operator[](int i) { return v[i]; }
    const int& operator[](int i) const { return v[i]; }
};

struct coord {
  public:
    std::vector<double> v;
    coord() { v.resize(3,0); }
    coord(std::vector<double> i) { assert(i.size()==3); v=i; }
    size_t size() { return v.size(); }
    double& operator[](int i) { return v[i]; }
    const double& operator[](int i) const { return v[i]; }
};

// A helper function to simplify the main part.
template<class T> std::ostream& operator<<(std::ostream& os,
    const std::vector<T>& v)
{
  copy(v.begin(), v.end(), std::ostream_iterator<T>(std::cout, " "));
  return os;
}

// A helper function to simplify the main part.
template<class T> std::ostream& operator<<(std::ostream& os, std::vector<T>& v)
{
  copy(v.begin(), v.end(), std::ostream_iterator<T>(std::cout, " "));
  return os;
}

#endif

