classdef GtCrackAnalysis < handle

properties
    name;                 % Name of the study

    cycles;               % Parameters of each step
    cycleIndexType;       % Data type used for cycles index
    cycleNumberType;      % Data type used for cycles number
    finalCycleNumber;     % Final cycle number

    Nphases;              % Number of phases
    p_crystalSystem;      % Crystal system of each phase
    p_symm;               % Symmetry operators of each phase
    p_grainIDs;           % List of grain IDs for each phase
    p_faces;              % List of faces for each phase
    p_slipSystemsFamilies;% List of slip system families of each phase

    dctVol;               % DCT volume containing the grain IDs
    dctVoxelSize;         % Voxel size of DCT volume
    r_vectors;            % Rodrigues vectors

    mesh;                 % Mesh of the final crack
    v_cycleIndex;         % Cycle index when each vertex is created
    v_cycleNumber;        % Cycle number when each vertex is created

    f_grainID;            % Grain ID of each face
    f_GB;                 % Faces indices corresponding to Grain Boundaries
    f_cycleIndex;         % Cycle index when each face is created
    f_cycleNumber;        % Cycle number when each face is created
    f_cycleCrackFront;    % Cycle number of faces at the crack front
    f_innerCycleSteps;    % Cell of different propgation steps between measures for each face
    f_da;                 % Crack advance for each face
    f_dadN;               % Crack growth rate for each face

    Ngrains;              % Number of grains
    allGrainIDs;          % List of all grain IDs
    grainIDs;             % List of ROI grain IDs
    maxGrainID;           % Maximal grain ID
    %crackGrainIDs;        % List of cracked grainIDs
    grainIDType;          % Data type used for grain IDs
    g_phaseID;            % Phase related to grain ID
    g_orientation;        % Orientation matrix g for each grain ID
    g_schmidFactors;      % Schmid factors for each grains

    norm_samp;            % Normal vector for each face in sample CS
    norm_cryst;           % Normal vector for each face in crystal CS
    %norm_cryst_sst;       % Normal vector for each face in crystal SST

    facecol_phys;         % Face orientation color in sample CS
    facecol_cryst;        % Face orientation color in crystal CS
    facecol_cryst_sst;    % Face orientation color in crystal SST
    %facecol_cryst_sst_sat;% Face orientation color in crystal SST (saturated)
    %facecol_dadN;         % Face crack growth rate color

    grainIDColorMap;      % Color map for grain ID/label
    grainIPFColorMap;     % Color map for grain inverse pole figure
    %crackGrainIPFColorMap;% Color map for grain inverse pole figure
    cycleIndexColorMap;   % Color map for cycle indices
    cycleNumberColorMap;  % Color map for cycle number
    growthRateColorMap;   % Color map for crack growth rate
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% PUBLIC METHODS
methods (Access = public)
    function obj = GtCrackAnalysis()
    % GTCRACKANALYSIS/GTCRACKANALYSIS Contructor
        obj.name = '';

        obj.cycles = {};
        obj.cycleIndexType = '';
        obj.cycleNumberType = '';
        obj.finalCycleNumber = 0;

        obj.Nphases = 0;
        obj.p_crystalSystem = {};
        obj.p_symm = {};
        obj.p_grainIDs = {};
        obj.p_faces = {};
        obj.p_slipSystemsFamilies = {};

        obj.dctVol = [];
        obj.dctVoxelSize = 0;
        obj.r_vectors = [];

        obj.mesh = GtMesh();
        obj.v_cycleIndex = [];
        obj.v_cycleNumber = [];

        obj.f_grainID = [];
        obj.f_GB = [];
        obj.f_cycleIndex = [];
        obj.f_cycleNumber = [];
        obj.f_cycleCrackFront = [];
        obj.f_innerCycleSteps = {};
        obj.f_da = [];
        obj.f_dadN = [];

        obj.Ngrains = 0;
        obj.grainIDs = [];
        obj.allGrainIDs = [];
        %obj.crackGrainIDs = [];
        obj.maxGrainID = 0;
        obj.grainIDType = '';
        obj.g_phaseID = [];
        obj.g_orientation = [];
        obj.g_schmidFactors = [];

        obj.norm_samp = [];
        obj.norm_cryst = [];
        %obj.norm_cryst_sst = [];

        obj.facecol_phys = [];
        obj.facecol_cryst = [];
        obj.facecol_cryst_sst = [];

        obj.grainIDColorMap = [];
        obj.grainIPFColorMap = [];
        %obj.crackGrainIPFColorMap = [];
        obj.cycleIndexColorMap = [];
        obj.cycleNumberColorMap = [];
        obj.growthRateColorMap = [];
    end

    function delete(obj)
    % GTCRACKANALYSIS/DELETE Destructor
        delete@handle(obj);
        clear obj;
    end

    function loadCrackMesh(obj, filename)
    % GTCRACKANALYSIS/LOADCRACKMESH
    % Load the mesh of the final crack.
        if ~exist('filename', 'var') || isempty(filename)
            [f, p] = uigetfile('*', ['Select the file containing the mesh ' ...
                'of the crack']);
            filename = fullfile(f, p);
        end
        [~, ~, fext] = fileparts(filename);
        if strcmp(fext, '.vtk')
            disp(['  Loading crack mesh from ' filename ' ... ']);
            obj.mesh = gtVTKMeshReader(filename);
        elseif strcmp(fext, '.geof')
            disp(['  Loading crack mesh from ' filename ' ... ']);
            obj.mesh = gtZsetMeshReader(filename);
        else
            gtError('GtCrackAnalysis:wrong_filename', ...
                ['Cannot load mesh from ' fext ' file!']);
        end
        disp(['    number of vertices : ' num2str(obj.mesh.Nvertices) ]);
        disp(['    number of faces    : ' num2str(obj.mesh.Nfaces)    ]);
        disp(' ');
    end
    
    function loadCrackSteps(obj, filename)
    % GTCRACKANALYSIS/LOADCRACKSTEPS
    % Load each cycle parameters.
        if ~exist('filename', 'var') || isempty(filename)
            gtError('GtCrackAnalysis:missing_input_filename', ...
                'Missing crack steps file!');
        end
        if ~exist(filename, 'file')
            gtError('GtCrackAnalysis:wrong_filename', ...
                ['Cannot find file ''' filename '']);
        end
        disp(['  Reading cycles parameters from file: ' filename]);
        fid = fopen(filename, 'r');
        obj.cycles = textscan(fid, '%s %d %f', 'CommentStyle', '#', ...
            'MultipleDelimsAsOne', true);
        fclose(fid);

        % Set cycles index/number data types
        obj.cycleIndexType   = gtFindSmallestDataType(length(obj.cycles), 'uint');
        obj.finalCycleNumber = max(obj.cycles{2});
        obj.cycleNumberType  = gtFindSmallestDataType(obj.finalCycleNumber, 'uint');

        disp(' ');
    end

    function matchCrackFacesWithCycleNumbers(obj, filename, Nmin, Nmax, dilate)
    % GTCRACKANALYSIS/MATCHCRACKFACESWITHCYCLENUMBERS
    % Set the cycle number/index of creation for each faces
    % TODO: Maybe we can make this function protected...
        if ~exist('Nmin', 'var') || isempty(Nmin)
            Nmin = 0;
        end

        % Load crack steps file
        obj.loadCrackSteps(filename);
        cycleFiles      = obj.cycles{1};
        cycleNumbers    = obj.cycles{2};
        cycleVoxelSizes = obj.cycles{3};

        if ~exist('Nmax', 'var') || isempty(Nmax)
            Nmax = intmax(obj.cycleNumberType);
        end
        if ~exist('dilate', 'var') || isempty(dilate)
            dilate = 0;
        end

        obj.f_cycleIndex  = zeros(obj.mesh.Nfaces, 1, obj.cycleIndexType);
        obj.f_cycleNumber = zeros(obj.mesh.Nfaces, 1, obj.cycleNumberType);
        obj.v_cycleIndex  = zeros(obj.mesh.Nfaces, 1, obj.cycleIndexType);
        obj.v_cycleNumber = zeros(obj.mesh.Nfaces, 1, obj.cycleNumberType);
        voxelSize = 0;
        for iN = 1:length(cycleFiles)
            N = cycleNumbers(iN);
            if ~between(N, Nmin, Nmax)
                continue;
            end
            disp(['  Matching crack faces with crack volume at cycle: ' ...
                   num2str(N)]);
            [vol, info] = obj.loadCrackVolume(cycleFiles{iN});
            if info.voxelSize == 0
                info.voxelSize = cycleVoxelSizes(iN);
            end
            if dilate > 0
                se = strel('line', dilate, 0);
                %vol = imdilate(permute(vol, [1 3 2]), se);
                %vol = permute(vol, [1 3 2]);
                for iy = 1:size(vol, 2)
                    vol(:, iy, :) = imdilate(squeeze(vol(: , iy, :)), se);
                end
            end
            volSize = size(vol);
            if info.voxelSize ~= voxelSize
                voxelSize = info.voxelSize;
                % Turn each face vertices coordinates into indices
                [p1, p2, p3, f_ok] = obj.turnFacesCoordIntoIndices(...
                    voxelSize, volSize, info.boundingBox);
                [p, v_ok] = obj.turnVerticesCoordIntoIndices(...
                    voxelSize, volSize, info.boundingBox);
            end

            % Loop over vertices to determine if they were created at this step
            for ivert = 1:obj.mesh.Nvertices
                if ~v_ok(ivert)
                    continue;
                end
                % No need to continue if the vertex has been created at an
                % earlier cycle number
                jN = obj.v_cycleIndex(ivert);
                if jN > 0 && cycleNumbers(jN) < N
                    continue;
                end
                crack = vol(p(ivert, 1), p(ivert, 2), p(ivert, 3));
                if crack
                    obj.v_cycleIndex(ivert) = iN;
                    obj.v_cycleNumber(ivert) = N;
                end
            end

            % Loop over faces to determine if they were created at this step
            % - This is ok if every vertex is inside the crack volume
            for iface = 1:obj.mesh.Nfaces
                if ~f_ok(iface)
                    continue;
                end
                % No need to continue if the face has been created at an
                % earlier cycle number
                jN = obj.f_cycleIndex(iface);
                if jN > 0 && cycleNumbers(jN) < N
                    continue;
                end
                crack1 = vol(p1(iface, 1), p1(iface, 2), p1(iface, 3));
                crack2 = vol(p2(iface, 1), p2(iface, 2), p2(iface, 3));
                crack3 = vol(p3(iface, 1), p3(iface, 2), p3(iface, 3));
                if (crack1 && crack2 && crack3)
                    obj.f_cycleIndex(iface) = iN;
                    obj.f_cycleNumber(iface) = N;
                end
            end
        end
    end

    function computeCrackFrontLines(obj, force)
    % GTCRACKANALYSIS/COMPUTECRACKFRONTLINES
    % Compute the lines corresponding to the crack front advance.
        if (~exist('force', 'var') || isempty(force))
            force = false;
        end
        if (~isempty(obj.f_cycleCrackFront) && (~force))
            disp('Crack front lines are already computed, so skipping!');
            disp('(You can force it with the ''force'' argument).')
            return;
        end
        obj.f_cycleCrackFront = zeros([obj.mesh.Nfaces 1], obj.cycleNumberType);
        for iface = 1:obj.mesh.Nfaces
            f_iv = obj.mesh.faces(iface, :);
            c1 = obj.v_cycleNumber(f_iv(1));
            c2 = obj.v_cycleNumber(f_iv(2));
            c3 = obj.v_cycleNumber(f_iv(3));
            if (c1 == c2 && c1 == c3)
                obj.f_cycleCrackFront(iface) = 0;
            else
                obj.f_cycleCrackFront(iface) = min([c1 c2 c3]);
            end
        end
    end

    function v = buildCrackFrontVertexList(obj)
        v = unique(obj.mesh.faces(obj.f_cycleCrackFront > 0, :));
    end

    function computeInnerCycleSteps(obj)
    % GTCRACKANALYSIS/COMPUTEINNERCYCLESTEPS
    % Compute the crack propagation steps on each face between each measured cycle
        disp('  Computing crack propagation steps for each face...');
        fprintf('    ');
        obj.mesh.buildConnectivityFaces2FacesFull();
        if (isempty(obj.cycles))
            gtError('GtCrackAnalysis:no_cycles_info', 'Cycle info missing...');
        end
        obj.computeCrackFrontLines();
        cycleNumbers = obj.cycles{2};
        for icycle = 1:length(cycleNumbers) - 1
            N  = cycleNumbers(icycle);
            N1 = cycleNumbers(icycle +1);
            % Get list of faces defining crack front for cycle N
            selectCrackFrontFaces_N = obj.f_cycleCrackFront == N;
            % Get all the faces attached to these ones
            cfaces_N = unique([obj.mesh.connec_f2f{selectCrackFrontFaces_N}]); 
            % Get the list of faces created at cycle N1
            crackFaces_N1 = find(obj.f_cycleNumber == N1);
            % Intersection the connected faces with the faces of the next cycle N1
            steps = [];
            steps(:, 1) = intersect(cfaces_N, crackFaces_N1);

            % Iterate through the remaining faces of the next cycle N1
            k = 1;
            while (true)
                % Select faces previously added to steps
                stepFaces = steps(steps(:, k) > 0, k);
                % Get all the faces attached to these faces
                cfaces = unique([obj.mesh.connec_f2f{stepFaces}]); 
                % Intersect the connected faces with the faces of the next cycle N1
                temp = intersect(cfaces, crackFaces_N1);
                % Keep only the newly faces
                temp2 = setdiff(temp, unique(steps));
                % Stop workin on this cycle if no faces arre added
                if isempty(temp2)
                    break;
                end
                steps(1:length(temp2), k+1) = temp2;
                k = k + 1;
            end

            obj.f_innerCycleSteps{icycle} = steps;
        end
    end

    function computeCrackGrowth(obj)
    % GTCRACKANALYSIS/COMPUTECRACKGROWTH
    % Compute the crack growth rate for each face
        
        disp('  Computing crack growth for each face...');
        cycleNumbers = obj.cycles{2};

        % Compute crack advance for each face
        obj.f_da = zeros(obj.mesh.Nfaces, 1);
        for icycle = 1:length(cycleNumbers)-1
            cycleSteps = obj.f_innerCycleSteps{icycle};
            for istep = 1:size(cycleSteps, 2)
                step = cycleSteps(:, istep);
                % This formulae is really too simplistic (gotten from M. Herbig macros)
                % Have to find a better computation method
                obj.f_da(step(step ~= 0)) = (istep+1) * 1.41;
            end
        end
            
        dN = ones(size(cycleNumbers));
        for icycle = 1:length(dN) - 1
            dN(icycle) = cycleNumbers(icycle+1) - cycleNumbers(icycle);
        end
        disp(dN);
        f_cycleIndexOK = (obj.f_cycleIndex > 0);
        disp(size(f_cycleIndexOK))
        disp(size(dN(obj.f_cycleIndex(f_cycleIndexOK))))
        disp(size(obj.f_da(f_cycleIndexOK)))
        obj.f_dadN = zeros(obj.mesh.Nfaces, 1);
        obj.f_dadN(f_cycleIndexOK) = (obj.f_da(f_cycleIndexOK) ./ dN(obj.f_cycleIndex(f_cycleIndexOK)));  % (mum/N)
    end

    function loadGrainsVolume(obj, volume, voxelSize)
    % GTCRACKANALYSIS/LOADGRAINSVOLUME
    % Load the grains volume containing the grains ID.
        if ~exist('voxelSize', 'var') || isempty(voxelSize)
            voxelSize = 1.4;
            disp(['Warning: DCT volume voxel size not given, setting it to ' ...
                'default: ' num2str(voxelSize)]);
        end
        obj.dctVoxelSize = voxelSize;

        if isnumeric(volume) && ndims(volume == 3)
            dct = volume;
        else
            if ~exist('volume', 'var') || isempty(volume)
                [f, p] = uigetfile('*', ['Select the file containing the ' ...
                    'binary volume of the crack']);
                volume = fullfile(f, p);
            end
            [~, ~, fext] = fileparts(volume);
            disp(['  Loading grains volume from ' volume ' ... ']);
            switch fext
            case '.edf'
                dct = edf_read(volume);
            case '.tif'
                dct = gtTIFVolReader(volume);
            case {'.raw', '.vol'}
                dct = gtHSTVolReader(volume);
            otherwise
                gtError('Unsupported type of volume data!');
            end
        end
        maxID = max(max(max(dct)));
        obj.grainIDType = gtFindSmallestDataType(maxID, 'uint');
        obj.dctVol = cast(dct, obj.grainIDType);

        if isempty(obj.Nphases)
            disp('  Phases not set, please set it!');
            obj.setPhasesCrystalSystem();
        end

        obj.grainIDs = unique(obj.dctVol)';
        obj.grainIDs = obj.grainIDs(obj.grainIDs > 0);
        obj.maxGrainID = max(obj.grainIDs);
        obj.Ngrains = length(obj.grainIDs);
        disp(['    Grains volume size  : ' num2str(size(obj.dctVol))]);
        disp(['    Number of grains    : ' num2str(obj.Ngrains)       ]);
        disp(['    Number of grain IDs : ' num2str(obj.maxGrainID)    ]);
        disp(' ');

        % The multi-phase material is not completely managed right now
        obj.g_phaseID = ones(1, obj.maxGrainID);
    end

    function setPhaseGrainID(obj)
    % GTCRACKANALYSIS/SETPHASEGRAINID
    % Set grain IDs of each phases.
        disp('  Setting phase IDs ...');
        if isempty(obj.dctVol)
            obj.loadGrainsVolume();
        end
        if isempty(obj.Nphases)
            disp('  Phases not set, please set it!');
            obj.setPhasesCrystalSystem();
        end
        obj.p_grainIDs = cell(1, obj.Nphases);
        for iphase = 1:obj.Nphases
            obj.p_grainIDs{iphase} = cast(obj.grainIDs, obj.grainIDType);
        end
        disp(' ');
    end

    function matchCrackFacesWithGrains(obj)
    % GTCRACKANALYSIS/MATCHCRACKFACESWITHGRAINS
    % Match the crack mesh faces with the grains.
        if obj.mesh.Nvertices == 0 || obj.mesh.Nfaces == 0
            obj.loadCrackMesh();
        end
        if isempty(obj.dctVol)
            obj.loadGrainsVolume();
        end

        volSize = size(obj.dctVol);
        obj.f_grainID = zeros(obj.mesh.Nfaces, 1, obj.grainIDType);
        disp(['  Grain volume size is: ' num2str(volSize)]);

        p1 = round(obj.mesh.vertices(obj.mesh.faces(:, 1), :) / obj.dctVoxelSize);
        p2 = round(obj.mesh.vertices(obj.mesh.faces(:, 2), :) / obj.dctVoxelSize);
        p3 = round(obj.mesh.vertices(obj.mesh.faces(:, 3), :) / obj.dctVoxelSize);

        minIndex = ones(obj.mesh.Nfaces, 3);
        maxIndex = volSize(ones(1, obj.mesh.Nfaces), :);
        ok = between(p1, minIndex, maxIndex) & ...
             between(p2, minIndex, maxIndex) & ...
             between(p3, minIndex, maxIndex);

        if any(~ok)
            disp(['Problem with ' num2str(sum(~ok)) ' vertices:']);
        end

        for iface = 1:length(obj.mesh.faces)
            if ok(iface)
                igrain1 = obj.dctVol(p1(iface, 1), p1(iface, 2), p1(iface, 3));
                igrain2 = obj.dctVol(p2(iface, 1), p2(iface, 2), p2(iface, 3));
                igrain3 = obj.dctVol(p3(iface, 1), p3(iface, 2), p3(iface, 3));
                if igrain1 == igrain2 && igrain1 == igrain3
                    obj.f_grainID(iface) = igrain1;
                else
                    % GB labeled as maxGrainID+1
                    obj.f_grainID(iface) = obj.maxGrainID + 1;
                end
            end
        end
        %tmp = unique(obj.f_grainID);
        %obj.crackGrainIDs = tmp(tmp >= 1 & tmp <= obj.maxGrainID);
        obj.f_GB = find(obj.f_grainID == obj.maxGrainID+1);
        %obj.f_GB = obj.f_grainID == obj.maxGrainID+1;
        disp(' ');
    end

    function loadRVectors(obj, filename)
    % GTCRACKANALYSIS/LOADRVECTORS
    % Load the list of R vectors and compute orientation matrices.
        if ~exist('filename', 'var') || isempty(filename)
            if exist('r_vectors.mat', 'file')
                filename = fullfile(pwd, 'r_vectors.mat');
            else
                [f, p] = uigetfile('*.mat', ['Select the Matlab file ' ...
                    'containing the R vectors']);
                filename = fullfile(p, f);
            end
        end
        [fpath, fname, fext] = fileparts(filename);
        if isempty(fext)
            filename = fullfile(fpath, fname, '.mat');
        end
        tmp = load(filename, 'r_vectors');
        obj.r_vectors = tmp.r_vectors(:, 2:4);

        % Compute all orientation matrices g
        obj.g_orientation = gtMathsRod2OriMat(obj.r_vectors.');
    end

    function setPhasesCrystalSystem(obj, crystal_systems)
    % GTCRACKANALYSIS/SETPHASESCRYSTALSYSTEM
    % Set the crystal system of each phase.
        if ~exist('crystal_systems', 'var') || isempty(crystal_systems)
            N = inputwdefaultnumeric('Number of phases', '1');
            crystal_systems = cell(1, N);
            for iphase = 1:N
                crystal_systems{iphase} = inputwdefault([...
                    'Crystal systems for phase ' num2str(iphase) ''], 'cubic');
            end
        elseif ~iscell(crystal_systems) && ischar(crystal_systems)
            crystal_systems = {crystal_systems};
        end
        obj.Nphases = length(crystal_systems);

        for iphase = 1:obj.Nphases
            % Get crystal system
            try
                validatestring(crystal_systems{iphase}, {'cubic', ...
                    'hexagonal', 'trigonal'});
            catch Mexc
                throw(Mexc);
            end
            syst = crystal_systems{iphase};

            % Store phase crystal system and symmetry operators
            obj.p_crystalSystem{iphase} = syst;

            % Get symmetry operators
            obj.p_symm{iphase} = gtCrystGetSymmetryOperators(syst);
        end
    end

    function computeNormalSampleColor(obj)
    % GTCRACKANALYSIS/COMPUTENORMALSAMPLE
    % Compute the normal of all faces in the sample CS.
        if obj.mesh.Nvertices == 0 || obj.mesh.Nfaces == 0
            obj.loadCrackMesh();
        end
        p1 = obj.mesh.vertices(obj.mesh.faces(:, 1), :);
        p2 = obj.mesh.vertices(obj.mesh.faces(:, 2), :);
        p3 = obj.mesh.vertices(obj.mesh.faces(:, 3), :);
        %v1 = p1 - p2;
        %v2 = p3 - p2;
        %n = cross(  v1  ,   v2   );
        %n = cross(p3 - p2, p1 - p2);
        n = cross(p1 - p2, p3 - p2);
        n_norm = sqrt(sum(n.^2, 2));
        obj.norm_samp = n./n_norm(:, [1 1 1]);
        norm_zero = find(n_norm < 1.e-5);
        obj.norm_samp(norm_zero, :) = repmat([1 0 0], [size(norm_zero, 1) 1]);

        % Get the vector orientation color in sample
        rgb = gtVectorOrientationColor(obj.norm_samp);

        % Label GBs in black
        obj.facecol_phys = obj.colorGBFaces(rgb);
    end

    function computeNormalCrystalColor(obj)
    % GTCRACKANALYSIS/COMPUTENORMALCRYST
    % Compute the normal of all faces in the crystal CS.
        if isempty(obj.norm_samp)
            obj.computeNormalSampleColor();
        end
        if isempty(obj.r_vectors)
            obj.loadRVectors();
        end
        if isempty(obj.f_grainID)
            obj.matchCrackFacesWithGrains();
        end

        obj.norm_cryst = zeros(obj.mesh.Nfaces, 3);
        for iphase = 1:obj.Nphases
            for igrain = obj.p_grainIDs{iphase}
                g_facesID = find(obj.f_grainID == igrain);
                if g_facesID
                    g = obj.g_orientation(:, :, igrain);
                    obj.norm_cryst(g_facesID, :) = ...
                        gtVectorLab2Cryst(obj.norm_samp(g_facesID, :), g);
                end
            end
        end

        % Get the vector orientation color in crystal
        rgb = gtVectorOrientationColor(obj.norm_cryst);

        % Label GBs in black
        obj.facecol_cryst = obj.colorGBFaces(rgb);
    end

    function computeNormalCrystalSSTColor(obj)
    % GTCRACKANALYSIS/COMPUTENORMALCRYSTSST
    % Compute the normal of all faces in the crystal CS SST.
        if isempty(obj.norm_cryst)
            obj.computeNormalCrystalColor();
        end
        %obj.norm_cryst_sst = zeros(obj.mesh.Nfaces, 3);
        obj.facecol_cryst_sst = zeros(obj.mesh.Nfaces, 3);
        for iphase = 1:obj.Nphases
            for igrain = obj.p_grainIDs{iphase}
                g_facesID = find(obj.f_grainID == igrain);
                if ~isempty(g_facesID)
                    %[color, Vsst] = gtCrystVector2SST(...
                    color = gtCrystVector2SST(...
                        obj.norm_cryst(g_facesID, :), ...
                        obj.p_crystalSystem{iphase}, obj.p_symm{iphase}, false);
                    %    %obj.p_crystalSystem{iphase}, obj.p_symm{iphase});
                    obj.facecol_cryst_sst(g_facesID, :) = color;
                    %obj.norm_cryst_sst(g_facesID, :) = Vsst;
                end
            end
        end
    end

    function computeSchmidFactors(obj, load)
    % GTCRACKANALYSIS/COMPUTESCHMIDFACTORS
    % Compute Schmid factors depending on the given loading direction.
        if isempty(obj.r_vectors)
            obj.loadRVectors();
        end
        if ~exist('load', 'var') || isempty(load)
            load = [0 0 1];
        end
        load = load / norm(load);
        for iphase = 1:obj.Nphases
            g_IDs = obj.p_grainIDs{iphase};
            loadCryst = gtVectorLab2Cryst(load, obj.g_orientation(:, :, g_IDs));
            slipFamilies = obj.p_slipSystemsFamilies{iphase};
            slipSystems = gtGetSlipSystems(slipFamilies);
            for ifam = 1:length(slipFamilies)
                syst = slipSystems.slipFamilies{ifam};
                schmid = zeros(1, length(syst));
                for igrain = 1:obj.maxGrainID
                    for isyst = 1:length(syst)
                        theta = dot(loadCryst, syst(isyst).n');
                        kappa = dot(loadCryst, syst(isyst).l');
                        schmid(isyst) = abs(theta * kappa);
                    end
                end
                %setfield(obj.g_schmidFactors(iphase), slipFamilies{ifam}, schmid);
                obj.g_schmidFactors(iphase).slipFamilies{ifam} = schmid;
            end
        end
    end

    function x = colorGBFaces(obj, x, color)
    % GTCRACKANALYSIS/COLORGBFACES
    % Change the color of GB faces to black or white.
        if isempty(obj.f_grainID)
            obj.matchCrackFacesWithGrains();
        end
        Ncomp = size(x, 2);
        if ~exist('color', 'var') || isempty(color)
            color = zeros(1, Ncomp, class(x));
        elseif size(color, 1) ~= 1 || size(color, 2) ~= Ncomp
            gtError('GtCrackAnalysis:colorGBFaces', ...
                'Wrong RGB color vector size!');
        end
        if Ncomp == 1
            x(obj.f_GB) = color(ones(1, size(obj.f_GB, 1)));
        elseif Ncomp == 3
            x(obj.f_GB, :) = color(ones(1, size(obj.f_GB, 1)), :);
        end
    end

    function createAllColorMaps(obj)
    % GTCRACKANALYSIS/CREATEALLCOLORMAPS
    % Generate all the color maps need for the study
        if isempty(obj.grainIDColorMap)
            obj.createGrainIDColorMap();
        end
        if isempty(obj.grainIPFColorMap)
            obj.createGrainIPFColorMap();
        end
        %if isempty(obj.crackGrainIPFColorMap)
        %    obj.createCrackGrainIPFColorMap
        %end
        if isempty(obj.cycleIndexColorMap) || isempty(obj.cycleNumberColorMap)
            obj.createCycleColorMap();
        end
        if isempty(obj.growthRateColorMap)
            obj.createGrowthRateColorMap();
        end
    end

    function [hf, ha, hp] = drawCrackEdgesHeight(obj)
    % GTCRACKANALYSIS/DRAWCRACKEDGESHEIGHT
    % Draw the crack edges height.
        disp('  Drawing edges height map ...');
        disp(' ');
        if isempty(obj.f_grainID)
            obj.matchCrackFacesWithGrains();
        end
        heightColor = squeeze(obj.mesh.vertices(:, 3));
        [hf, ha, hp] = obj.drawCrackEdgesColor(heightColor, 'Name', ...
            'Height of the crack vertices');
    end

    function [hf, ha, hp] = drawCrackGrainLabel(obj)
    % GTCRACKANALYSIS/DRAWCRACKGRAINLABEL
    % Draw the crack faces grain ID/label.
        disp('  Drawing grain label map ...');
        disp(' ');
        if isempty(obj.f_grainID)
            obj.matchCrackFacesWithGrains();
        end
        if isempty(obj.grainIDColorMap)
            obj.createGrainIDColorMap();
        end
        [hf, ha, hp] = obj.drawCrackFacesColor(obj.f_grainID, false, ...
            'Name', 'Grain labels', 'Colormap', obj.grainIDColorMap);
    end

    function [hf, ha, hp] = drawCrackGrainIPF(obj, cycleNumber)
    % GTCRACKANALYSIS/DRAWCRACKGRAINIPF
    % Draw the crack faces grain inverse pole figure
        disp('  Drawing grain inverse pole figure map ...');
        disp(' ');
        if ~exist('cycleNumber', 'var')
            cycleNumber = [];
        end
        if isempty(obj.grainIPFColorMap)
            obj.createGrainIPFColorMap();
        end
        [hf, ha, hp] = obj.drawCrackFacesColor(obj.f_grainID, false, 'Name', ...
            'Grain inverse pole figure', 'Colormap', obj.grainIPFColorMap, ...
            'cycleNumber', cycleNumber);
    end

    function [hf, ha, hp] = drawCrackFacesHeight(obj)
    % GTCRACKANALYSIS/DRAWCRACKFACESHEIGHT
    % Draw the crack faces height.
        disp('  Drawing faces height map ...');
        disp(' ');
        if isempty(obj.f_grainID)
            obj.matchCrackFacesWithGrains();
        end
        f_height = squeeze(mean(reshape(obj.mesh.vertices(...
            obj.mesh.faces(:, :), 3), [obj.mesh.Nfaces 3 1]), 2));
        Ncolor = 50;
        colorGB = (1-1/Ncolor) * min(f_height) - max(f_height)/Ncolor;
        heightColor = obj.colorGBFaces(f_height, colorGB);
        cmap = [0 0 0 ; jet(Ncolor)];
        [hf, ha, hp] = obj.drawCrackFacesColor(heightColor, false, ...
            'ColorMap', cmap, ...
            'Name', 'Height of the crack faces');
        set(hp, 'CDataMapping', 'scaled');
    end

    function [hf, ha, hp] = drawCrackFacesOrientationSample(obj)
    % GTCRACKANALYSIS/DRAWCRACKFACESORIENTATIONSAMPLE
    % Draw the crack faces with the sample orientation color code.
        disp('  Drawing faces orientation in sample CS ...');
        disp(' ');
        if isempty(obj.facecol_phys) || isempty(obj.norm_samp)
            obj.computeNormalSampleColor();
        end
        [hf, ha, hp] = obj.drawCrackFacesColor(obj.facecol_phys, false, ...
            'Name', 'Orientation of the crack faces in the sample');
    end

    function [hf, ha, hp] = drawCrackFacesOrientationCrystal(obj)
    % GTCRACKANALYSIS/DRAWCRACKFACESORIENTATIONCRYSTAL
    % Draw the crack faces with the crystal orientation color code.
        disp('  Drawing faces orientation in crystal CS ...');
        disp(' ');
        if isempty(obj.facecol_cryst) || isempty(obj.norm_cryst)
            obj.computeNormalCrystalColor();
        end
        [hf, ha, hp] = obj.drawCrackFacesColor(obj.facecol_cryst, false, ...
            'Name', 'Orientation of the crack faces in the crystal');
    end

    function [hf, ha, hp] = drawCrackFacesOrientationCrystalSST(obj, sat, ...
        cycleNumber)
    % GTCRACKANALYSIS/DRAWCRACKFACESORIENTATIONCRYSTALSST
    % Draw the crack faces with the crystal SST orientation color code.
        if ~exist('sat', 'var') || isempty(sat)
            sat = true;
        end
        if ~exist('cycleNumber', 'var')
            cycleNumber = [];
        end
        disp('  Drawing faces orientation in crystal SST ...');
        disp(' ');
        if isempty(obj.facecol_cryst_sst) || isempty(obj.norm_cryst) || ...
                isempty(obj.norm_cryst)
            obj.computeNormalCrystalSSTColor();
        end
        [hf, ha, hp] = obj.drawCrackFacesColor(obj.facecol_cryst_sst, sat, ...
            'Name', 'Orientation of the crack faces in the crystal SST', ...
            'cycleNumber', cycleNumber);
    end

    function [hf, ha, hp] = drawCrackFacesCycleIndex(obj)
    % GTCRACKANALYSIS/DRAWCRACKFACESCYCLEINDEX
    % Draw the crack faces cycle index of creation.
        disp('  Drawing faces cycle index ...');
        disp(' ');
        if isempty(obj.f_cycleNumber)
            gtError('GtCrackAnalysis:data_not_ready', ...
                'Crack faces cycle indices have not been determined yet!');
        end
        %f_iN = obj.f_cycleIndex;
        %f_iN(obj.f_GB) = zeros(1, size(obj.f_GB, 1), cycleIndexType);
        f_iN = obj.colorGBFaces(obj.f_cycleIndex);
        if isempty(obj.cycleIndexColorMap)
            obj.createCycleColorMap();
        end
        [hf, ha, hp] = obj.drawCrackFacesColor(f_iN, false, ...
            'ColorMap', obj.cycleIndexColorMap, ...
            'Name', 'Cycle index of crack faces creation');
        set(hp, 'CDataMapping', 'scaled');
    end

    function [hf, ha, hp] = drawCrackFacesCycleNumber(obj)
    % GTCRACKANALYSIS/DRAWCRACKFACESCYCLENUMBER
    % Draw the crack faces cycle number of creation.
        disp('  Drawing faces cycle number ...');
        disp(' ');
        if isempty(obj.f_cycleNumber)
            gtError('GtCrackAnalysis:data_not_ready', ...
                'Crack faces cycle numbers have not been determined yet!');
        end
        %f_N = obj.f_cycleNumber;
        %f_N(obj.f_GB) = zeros(1, size(obj.f_GB, 1), obj.cycleNumberType);
        f_N = obj.colorGBFaces(obj.f_cycleNumber);
        if isempty(obj.cycleNumberColorMap)
            obj.createCycleColorMap();
        end
        [hf, ha, hp] = obj.drawCrackFacesColor(f_N, false, ...
            'ColorMap', obj.cycleNumberColorMap, ...
            'Name', 'Cycle number of crack faces creation');
        set(hp, 'CDataMapping', 'scaled');
    end

    function [hf, ha, hp] = drawCrackFacesGrowthRate(obj)
    % GTCRACKANALYSIS/DRAWCRACKFACESGROWTHRATE
    % Draw the crack faces growth rate.
        disp('  Drawing faces growth rate ...');
        disp(' ');
        if isempty(obj.f_dadN)
            gtError('GtCrackAnalysis:data_not_ready', ...
                'Crack faces growth rate have not been determined yet!');
        end
        %f_daDN = obj.colorGBFaces(obj.f_dadN);
        if isempty(obj.growthRateColorMap)
            obj.createGrowthRateColorMap();
        end
        [hf, ha, hp] = obj.drawCrackFacesColor(obj.f_dadN, false, ...
            'ColorMap', obj.growthRateColorMap, ...
            'Name', 'Crack faces growth rate');
        set(hp, 'CDataMapping', 'scaled');
    end
    
end % end of public methods

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% PUBLIC STATIC METHODS
methods (Static, Access = public)
    
    function [bin, info] = loadCrackVolume(filename)
    %function loadCrackVolume(obj, filename, voxelSize)
    % GTCRACKANALYSIS/LOADCRACKVOLUME
    % Load the binary volume of the crack.

        if ~exist('filename', 'var') || isempty(filename)
            [f, p] = uigetfile('*', ['Select the file containing the binary ' ...
                'volume of the crack']);
            filename = fullfile(f, p);
        end
        [fpath, fname, fext] = fileparts(filename);
        disp(['  Loading crack binary volume from ' filename ' ... ']);
        switch fext
        case '.edf'
            bin = logical(edf_read(filename));
        case {'.tif', '.tiff'}
            bin = logical(gtTIFVolReader(filename));
        case {'.raw', '.vol'}
            bin = logical(gtHSTVolReader(filename));
        otherwise
            gtError('GtCrackAnalysis', 'Unsupported type of volume data!');
        end
        disp(['    Size of crack binary volume: ' num2str(size(bin))]);

        infoFile = fullfile(fpath, [fname '.info']);
        if exist(infoFile, 'file')
            disp(['  Loading crack volume info from ' fname '.info ... ']);
            info = gtTIFInfoReader(infoFile);
            disp(['    BoundingBox: ' num2str(info.boundingBox) ...
                  '    Voxel Size:  ' num2str(info.voxelSize)]);
        else
            warning('GtCrackAnalysis:no_bbox_file', 'Could not find bbox file!');
        end
        disp(' ');
    end    
    
end % end of public static methods

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% PROTECTED METHODS
methods (Access = protected)

    function [indices, ok] = turnVerticesCoordIntoIndices(obj, voxelSize, ...
        volSize, bbox)
    % GTCRACKANALYSIS/TURNVERTICESCOORDINTOINDICES
    % Compute the corresponding indices of the crack face vertices for a
    % specific voxel size.
        if ~exist('bbox', 'var') || isempty(bbox)
            % Bbox index goes from 0 to N-1 (C++ style)
            bbox = [0 0 0 volSize-1];
        end
        A = bbox(ones(1, obj.mesh.Nvertices), 1:3);
        minIndex = ones(obj.mesh.Nvertices, 3);
        maxIndex = bbox(ones(1, obj.mesh.Nvertices), 4:6);
        indices = round(obj.mesh.vertices(:, :) / voxelSize) - A;
        ok = between(indices, minIndex, maxIndex);
        if any(~ok)
            disp(['!!! Problem with ' num2str(sum(~ok)) ' vertices:']);
        end
    end

    function [p1, p2, p3, ok] = turnFacesCoordIntoIndices(obj, voxelSize, ...
        volSize, bbox)
    % GTCRACKANALYSIS/TURNFACESCOORDINTOINDICES
    % Compute the corresponding indices of the crack face vertices for a
    % specific voxel size.
        if ~exist('bbox', 'var') || isempty(bbox)
            % Bbox index goes from 0 to N-1 (C++ style)
            bbox = [0 0 0 volSize-1];
        end
        A = bbox(ones(1, obj.mesh.Nfaces), 1:3);
        minIndex = ones(obj.mesh.Nfaces, 3);
        maxIndex = bbox(ones(1, obj.mesh.Nfaces), 4:6);
        p1 = round(obj.mesh.vertices(obj.mesh.faces(:, 1), :) / voxelSize) - A;
        p2 = round(obj.mesh.vertices(obj.mesh.faces(:, 2), :) / voxelSize) - A;
        p3 = round(obj.mesh.vertices(obj.mesh.faces(:, 3), :) / voxelSize) - A;
        ok = between(p1, minIndex, maxIndex) & ...
             between(p2, minIndex, maxIndex) & ...
             between(p3, minIndex, maxIndex);
        if any(~ok)
            disp(['!!! Problem with ' num2str(sum(~ok)) ' vertices:']);
            %disp(min(p1));
            %disp(min([minIndex; maxIndex]));
            %disp(min(A));
        end
    end

    function createGrainIDColorMap(obj)
    % GTCRACKANALYSIS/CREATEGRAINIDCOLORMAP
    % Creates a color map for grain labels.
        cmap = gtRandCmap(obj.maxGrainID, 'white');
        obj.grainIDColorMap = [ cmap ; 0 0 0 ];
    end

    function createGrainIPFColorMap(obj, LD)
    % GTCRACKANALYSIS/CREATEGRAINIPFCOLORMAP
    % Creates a color map for grain inverse pole figure for LD direction.
        if isempty(obj.f_grainID)
            obj.matchCrackFacesWithGrains();
        end
        if isempty(obj.r_vectors)
            obj.loadRVectors();
        end
        if ~exist('LD', 'var') || isempty(LD)
            LD = [ 0 0 1 ];
        end

        if isempty(obj.p_grainIDs)
            obj.setPhaseGrainID();
        end
        map = zeros(obj.maxGrainID, 3);
        %map = zeros(numel(obj.grainIDs), 3);
        for iphase = 1:obj.Nphases
            g_IDs = obj.p_grainIDs{iphase};
            map(g_IDs, :) = gtIPFCmap(iphase, LD, ...
                'crystal_system',  obj.p_crystalSystem{iphase}, ...
                'r_vectors', obj.r_vectors(g_IDs, :), ...
                'background', false);
            
            %LDc = gtVectorLab2Cryst(LD, obj.g_orientation(:, :, g_IDs));
            %map(g_IDs, :) = gtCrystVector2SST(LDc, ...
            %    obj.p_crystalSystem{iphase}, obj.p_symm{iphase});
            %    %obj.p_crystalSystem{iphase}, obj.p_symm{iphase}, false);
        end

        obj.grainIPFColorMap = [ 1 1 1 ; map ; 0 0 0 ];
        %obj.grainIPFColorMap = [ 1 1 1 ; map(obj.grainIDs, :) ; 0 0 0 ];
    end

%    function createCrackGrainIPFColorMap(obj, LD)
%    % GTCRACKANALYSIS/CREATECRACKGRAINSIPFCOLORMAP
%    % Creates a color map for cracked grains inverse pole figure for LD direction.
%        if isempty(obj.f_grainID)
%            obj.matchCrackFacesWithGrains();
%        end
%        if isempty(obj.r_vectors)
%            obj.loadRVectors();
%        end
%        if ~exist('LD', 'var') || isempty(LD)
%            LD = [ 0 0 1 ];
%        end

%        if isempty(obj.p_grainIDs)
%            obj.setPhaseGrainID();
%        end
%        map = zeros(obj.maxGrainID, 3);
%        for iphase = 1:obj.Nphases
%            g_IDs = obj.p_grainIDs{iphase};
%            LDc = gtVectorLab2Cryst(LD, obj.g_orientation(g_IDs));
%            map(g_IDs, :) = gtCrystVector2SST(LDc, ...
%                obj.p_crystalSystem{iphase}, obj.p_symm{iphase});
%                %obj.p_crystalSystem{iphase}, obj.p_symm{iphase}, false);
%        end
%        map = map(obj.crackGrainIDs, :);
%        obj.crackGrainIPFColorMap = [ 1 1 1 ; map ; 0 0 0 ];
%    end

    function createCycleColorMap(obj, N)
    % GTCRACKANALYSIS/CREATECYCLECOLORMAP
    % Creates a color map for cycle numbers
        if isempty(obj.cycles)
            warning('GtCrackAnalysis:no_cycles_info', ...
                'No cycles info in memory');
            return;
        end
        if ~exist('N', 'var') || isempty(N)
            N = 1000;
        end
        obj.cycleIndexColorMap  = [0 0 0; jet(length(obj.cycles{1}))];
        obj.cycleNumberColorMap = [0 0 0; jet(N)];
    end

    function createGrowthRateColorMap(obj, N)
    % GTCRACKANALYSIS/CREATEGROWTHRATECOLORMAP
    % Creates a color map for the growth rate.
        if isempty(obj.f_dadN)
            warning('GtCrackAnalysis:no_growth_rate', ...
                'No growth rate has been computed so far...');
        end
        if ~exist('N', 'var') || isempty(N)
            N = 1000;
        end
        obj.growthRateColorMap = jet(N);
    end

    function [hf, ha, hp] = drawCrackEdgesColor(obj, edgeColor, varargin)
    % GTCRACKANALYSIS/DRAWCRACKORIENTATION
    % Draw the crack faces orientation with given color.
        par.Color = 'w';
        par.ColorMap = [];
        par.Name = [];
        par = parse_pv_pairs(par, varargin);

        % Create figure
        hf = figure();
        ha = axes();

        % Create a patch from the crack mesh
        hp = patch(obj.mesh.getFV());
        set(hp, 'FaceVertexCData', edgeColor, ...
                'FaceColor', 'interp', ...
                'CDataMapping', 'scaled', ...
                'EdgeColor', 'interp');
                %'LineWidth', 5 ...)

        minColor = min(edgeColor);
        maxColor = max(edgeColor);

        shading(ha, 'flat');
        axis equal;
        axis vis3d;
        set(ha, 'XTickLabel', '', 'YTickLabel', '', 'ZTickLabel', '', ...
                'Xcolor', 'w', 'Ycolor', 'w', 'Zcolor', 'w', ...
                'GridLineStyle', 'none', ...
                'CLim', [minColor maxColor]);
        if exist('title', 'var') && ~isempty(title)
            set(hf, 'Name', title);
        end

        parFields = fieldnames(par);
        for fields = parFields
            field = fields{1};
            val = par.(field);
            if ~isempty(val)
                set(hf, field, val);
            end
        end
    end

    function [hf, ha, hp] = drawCrackFacesColor(obj, faceColor, sat, varargin)
    % GTCRACKANALYSIS/DRAWCRACKFACESCOLOR
    % Draw the crack faces orientation with given color.
        % Set and get figure parameters
        par.Color = 'w';
        par.ColorMap = [];
        par.Name = [];
        par.cycleNumber = [];
        par = parse_pv_pairs(par, varargin);

        if sat
            faceColor = gtCrystSaturateSSTColor(faceColor);
        end
        if ~isempty(par.cycleNumber)
            blankFaces = obj.f_cycleNumber > par.cycleNumber;
            nComp = size(faceColor, 2);
            nBlank = sum(blankFaces);
            faceColor(repmat(blankFaces, [1 nComp])) = ones([nBlank nComp]);
        end

        % Create figure / axis
        hf = figure();
        ha = axes();

        % Create a patch from the crack mesh
        hp = patch(obj.mesh.getFV());
        set(hp, 'FaceVertexCData', faceColor);

        shading(ha, 'flat');
        axis equal;
        axis vis3d;
        set(ha, 'XTickLabel', '', 'YTickLabel', '', 'ZTickLabel', '', ...
                'Xcolor', 'w', 'Ycolor', 'w', 'Zcolor', 'w', ...
                'GridLineStyle', 'none');
        if exist('title', 'var') && ~isempty(title)
            set(hf, 'Name', title);
        end

        parFields = fieldnames(par);
        for fields = parFields
            field = fields{1};
            if strcmp(field, 'cyclenumber')
                continue;
            end
            val = par.(field);
            if ~isempty(val)
                set(hf, field, val);
            end
        end
        shg;
    end

end % end of protected methods

end % end of class
