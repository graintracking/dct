%close a crack, allowing for a bifurcated crack
%after treatment, whole crack has an opening of 2 voxels
%Remove spurious "crack" voxels at the surface of the sample.

function volout = gtThinCrack(vol)


%vol is the crack volume (crack>0, material==0)
%close the crack along the z (3rd) direction
% pushes towards a (fixed) middle, not the middle of the
%crack


%%%%%%  Close the crack   %%%%%%%
volout=zeros(size(vol));
vol=uint8(vol>0); %crack=1, material=0



for i=1:size(vol,1)

  for j=1:size(vol,2)

 
    tmp=squeeze(vol(i,j,:));
    tmpout=zeros(size(tmp));
    
  %  if all(tmp~=0)
  %    tmpout=ones(size(tmp));
  %    disp('solid')
  %    continue
  %  end
    
    if max(tmp)~=0
    tmp=double(tmp);
    gradtmp=abs(gradient(tmp));
    centertmp=round(mean(find(tmp)));
    
    %deal with the two halves, and then combine
    
    %second half
    position=centertmp;
    for k=centertmp:length(tmp)
      if tmp(k)==0  &  &  gradtmp(k)==0
        %disp('1')
        tmpout(position)=0;
        position=position+1;
      elseif tmp(k)==0  &  &  gradtmp(k) ~=0
        %disp('2')
        tmpout(position)=1;
        position=position+1;
      end
    end
      
      %first half - reverse order
    position=centertmp;
    for k=centertmp:-1:1
      if tmp(k)==0  &  &  gradtmp(k)==0
        %disp('1')
        tmpout(position)=0;
        position=position-1;
      elseif tmp(k)==0  &  &  gradtmp(k) ~=0
        %disp('2')
        tmpout(position)=1;
        position=position-1;
      end      
    end

    
    end
    
    volout(i,j,1:length(tmpout))=tmpout;      
    
  end
end


if 0
%%%%%   Remove surface voxels  %%%%%

%estimate max crack opening displacement
tmp=sum(vol,3);
tmp=tmp(find(tmp<(size(vol,3)/5))); % discard apparently large crack openings
tmp(find(tmp<4))=[]; % discard zeros and very low values (noise)
[count, bin]=hist(tmp, ceil(size(vol,3)/10)); %histogram
a=max(count(end-9:end)); %max of the ten largest bins
max_opening=max(bin(find(count>a))); %find the edge of histogram peak

vol_dil = imerode(vol, ones(1,1,round(max_opening)));
vol_dil = imdilate(vol_dil, ones(1,1,round(max_opening)));
vol_dil = imdilate(vol_dil, ones(11,11,1));

%apply this mask
volout(find(vol_dil))=0;


%%%%%%%%%
end





