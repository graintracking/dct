%correlation of crack with boundaries
%ak january 2008
function [shift,data, crack_out] = correlate_crack(crack, boundaries, deg_freedom, starting_shift)
%degrees of freedom x/y/z/rot, eg [0 0 1 1]
%note rotation is around the centre of the volumes
%shift = [x,y,z,rot?] of maximum correlation
%data = correlation history
%starting_shift - for more iterative approachs - gives a starting positions
%other than [0 0 0 0]

%work in uint8
%cast if not already
crack=uint8(crack);
boundaries=uint8(boundaries);


[sizex,sizey,sizez]=size(crack);
data=[];

if ~exist('deg_freedom', 'var')
  deg_freedom=[1 1 1 1];
  disp('doing all degrees of freedom (x/y/z/rot) by default')
end
if ~exist('starting_shift', 'var')
  starting_shift=[0 0 0 0];
  disp('starting from reference position by default')
end


%initial variables - do a range of 10%
%in x
if deg_freedom(1)
  xhigh=10;%ceil(sizex/40);
  xlow=-xhigh;
  xinc=ceil((xhigh-xlow)/5);
  xhigh=max([xlow:xinc:xhigh]);
else
  xlow=0;
  xinc=1;
  xhigh=0;
end
%in y
if deg_freedom(2)
  yhigh=10;%ceil(sizey/40);
  ylow=-yhigh;
  yinc=ceil((yhigh-ylow)/5);
  yhigh=max([ylow:yinc:yhigh]);
else
  ylow=0;
  yinc=1;
  yhigh=0;
end
%in z
if deg_freedom(3)
  zhigh=10; %ceil(sizez/40);%changed here from 20 for small volumes
  zlow=-zhigh;
  zinc=ceil((zhigh-zlow)/5);
  zhigh=max([zlow:zinc:zhigh]);
else
  zlow=0;
  zinc=1;
  zhigh=0;
end
%in rotation
if deg_freedom(4)
  rotlow=-5; 
  rotinc=2.5;
  rothigh=5;
  rothigh=max([rotlow:rotinc:rothigh]);
else
  rotlow=0;
  rotinc=1;
  rothigh=0;
end

%apply intial shift
xlow=xlow+starting_shift(1);
xhigh=xhigh+starting_shift(1);
ylow=ylow+starting_shift(2);
yhigh=yhigh+starting_shift(2);
zlow=zlow+starting_shift(3);
zhigh=zhigh+starting_shift(3);
rotlow=rotlow+starting_shift(4);
rothigh=rothigh+starting_shift(4);

finished=0;
count=0;

while finished==0
  %tic;
  count=count+1;

  %offset for writing into data variable
  %if offset~=max(-[xlow ylow zlow rotlow])+20;
  %data=[]; %if changing offset erase data so far.
  %offset=max(-[xlow ylow zlow rotlow])+20;
  %end

  %display status
  disp(sprintf('correlation loop %d', count))
   disp('x range')
   xlow:xinc:xhigh
   disp('y range')
   ylow:yinc:yhigh
   disp('z range')
   zlow:zinc:zhigh
   disp('rot range')
   rotlow:rotinc:rothigh

  %correlation loop
  %rotation range
  for rot=rotlow:rotinc:rothigh
    disp('...')
    if rot
      tmpA=imrotate(crack, rot, 'nearest', 'crop');
    else
      tmpA=crack;
    end

    %xrange:
    for x=xlow:xinc:xhigh
      %yrange:
      for y=ylow:yinc:yhigh
        %zrange:
        for z=zlow:zinc:zhigh

          %has this point been done already?
          if isempty(data) || isempty(find(data(:,1)==x  &  &  data(:,2)==y  &  &  data(:,3)==z  &  &  data(:,4)==rot, 1));

            %calculate indexes to apply the shift
            xout1=max(x+1, 1);
            xout2=min(sizex, sizex+x);

            xin1=max(1-x, 1);
            xin2=min(sizex, sizex-x);

            yout1=max(y+1, 1);
            yout2=min(sizey, sizey+y);

            yin1=max(1-y, 1);
            yin2=min(sizey, sizey-y);

            zout1=max(z+1, 1);
            zout2=min(sizez, sizez+z);

            zin1=max(1-z, 1);
            zin2=min(sizez, sizez-z);

            %apply the shifts
            tmp=uint8(zeros(size(crack)));
            tmp(xout1:xout2,yout1:yout2,zout1:zout2)=tmpA(xin1:xin2,yin1:yin2,zin1:zin2);
            %do the correlation
            
            tmp=tmp.*boundaries;
            %record result
            %data(x+offset,y+offset,z+offset,rot+offset)=sum(tmp(:));
            data(end+1,:)=[x, y, z, rot, sum(tmp(:))];

          end
        end
      end
    end
  end

  % %read from data to find coorelation maximum
  % [val, ind]=max(data(:));
  % [x,y,z,rot]=ind2sub(size(data), ind);
  % x=x-offset;
  % y=y-offset;
  % z=z-offset;
  % rot=rot-offset;

  [val,ind]=max(data(:,5));
  shift=data(ind, 1:4);
  x=shift(1); y=shift(2); z=shift(3); rot=shift(4);

  finished=1;%test

  %can only be finshed we are using fine increments (==1)
  if xinc~=1 || yinc~=1 || zinc~=1 || rotinc~=1
    %  if zinc~=1 || rotinc~=1
    finished=0;
  end

  %adjust limits
  if deg_freedom(1)
    if x<xhigh  &  &  x>xlow
      disp('happy in x')
      if xinc>1
        xlow=x-2;%xinc+1;
        xhigh=x+2;%xinc-1;
        xinc=1;
      else
        xlow=x-1;
        xhigh=x+1;
      end
    elseif x==xlow
      disp('adjust x range-')
      finished=0;
      shift=round((xhigh-xlow)/2);
      xlow=xlow-shift;
      xhigh=xhigh-shift;
    elseif x==xhigh
      disp('adjust x range+')
      finished=0;
      shift=round((xhigh-xlow)/2);
      xlow=xlow+shift;
      xhigh=xhigh+shift;
    end
  end
  if deg_freedom(2)
    if y<yhigh  &  &  y>ylow
      disp('happy in y')
      if yinc>1
        ylow=y-2;%yinc+1;
        yhigh=y+2;%yinc-1;
        yinc=1;
      else
        ylow=y-1;
        yhigh=y+1;
      end
    elseif y==ylow
      disp('adjust y range-')
      finished=0;
      shift=round((yhigh-ylow)/2);
      ylow=ylow-shift;
      yhigh=yhigh-shift;
    elseif y==yhigh
      disp('adjust y range+')
      finished=0;
      shift=round((yhigh-ylow)/2);
      ylow=ylow+shift;
      yhigh=yhigh+shift;
    end
  end
  if deg_freedom(3)
    if z<zhigh  &  &  z>zlow
      disp('happy in z')
      if zinc>1
        zlow=z-2;%zinc+1;
        zhigh=z+2;%zinc-1;
        zinc=1;
      else
        zlow=z-1;
        zhigh=z+1;
      end
    elseif z==zlow
      disp('adjust z range-')
      finished=0;
      shift=round((zhigh-zlow)/2);
      zlow=zlow-shift;
      zhigh=zhigh-shift;
    elseif z==zhigh
      disp('adjust z range+')
      finished=0;
      shift=round((zhigh-zlow)/2);
      zlow=zlow+shift;
      zhigh=zhigh+shift;
    end
  end
  if deg_freedom(4)
    if rot<rothigh  &  &  rot>rotlow
      disp('happy in rotation')
      if rotinc>1
        rotlow=rot-rotinc+1;
        rothigh=rot+rotinc-1;
        rotinc=1;
      else
        rotlow=rot-1;
        rothigh=rot+1;
      end
    elseif rot==rotlow
      disp('adjust rot range-')
      finished=0;
      shift=round((rothigh-rotlow)/2);
      rotlow=rotlow-shift;
      rothigh=rothigh-shift;
    elseif rot==rothigh
      disp('adjust rot range+')
      finished=0;
      shift=round((rothigh-rotlow)/2);
      rotlow=rotlow+shift;
      rothigh=rothigh+shift;
    end
  end

%  toc
end

%apply the shift to give crack_out volume
%have already read x,y,z,rot from shift

%apply rotation
tmpA=imrotate(crack, rot, 'nearest', 'crop');
%calc indices
xout1=max(x+1, 1);
xout2=min(sizex, sizex+x);

xin1=max(1-x, 1);
xin2=min(sizex, sizex-x);

yout1=max(y+1, 1);
yout2=min(sizey, sizey+y);

yin1=max(1-y, 1);
yin2=min(sizey, sizey-y);

zout1=max(z+1, 1);
zout2=min(sizez, sizez+z);

zin1=max(1-z, 1);
zin2=min(sizez, sizez-z);

%apply the shifts
tmp=uint8(zeros(size(crack)));
tmp(xout1:xout2,yout1:yout2,zout1:zout2)=tmpA(xin1:xin2,yin1:yin2,zin1:zin2);
crack_out=tmp;

  [val,ind]=max(data(:,5));
  shift=data(ind, 1:4);
shift(end+1)=val;%add the correlation value




