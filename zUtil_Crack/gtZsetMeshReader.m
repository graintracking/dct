function mesh = gtZsetMeshReader(filename, varargin)

%% Get file parts and check extension
[fpath, fname, fext] = fileparts(filename);
if isempty(fext)
    fext = '.geof';
    filename = fullfile(fpath, [fname fext]);
elseif ~strcmp(fext, '.geof')
    gtError('gtZsetMeshReader:wrong_file_extension', ...
        'Output file extension should be ''.geof''!');
end

%% Open Zset mesh
fid = fopen(filename, 'r');
if fid ==-1
    gtError('gtZsetMeshWriter:bad_input_file', ...
        'Can''t open the file in read mode.');
end

%% Read header
str = fgetl(fid);
what = sscanf(strtrim(str), '%s', 1);
if strcmp(what, '***geometry')
    disp('Reading geometry ...');
else
    gtError('gtZsetMeshWriter:bad_input_header', ...
        'Couldn''t find ''***geometry'' header.');
end

%% Initialize mesh
mesh = GtMesh();

%% Read nodes
str = fgetl(fid);
what = sscanf(strtrim(str), '%s', 1);
if strcmp(what, '**node')
    fprintf('Reading nodes ... ');
else
    gtError('gtZsetMeshWriter:bad_node_header', ...
        'Couldn''t find ''**node'' header.');
end
str = fgetl(fid);
tmp = sscanf(strtrim(str), '%d %d');
nNodes = tmp(1);
dim = tmp(2);
nCoord = nNodes * dim;
nValues = nNodes * (1 + dim);
fprintf('%d defined in %dD\n', nNodes, dim);
[nodeId_Coord, sz] = fscanf(fid, '%d %f %f %f', nValues);
if sz ~= nValues
    warning('gtZsetMeshWriter:node_coord_warning', ...
        'Problem in reading node coordinates.');
end
nodeId_Coord = reshape(nodeId_Coord, 1+dim, nNodes).';
nodesIndex2Id = uint64(nodeId_Coord(:, 1));

% Convert Id to Index
maxNodeId = max(nodesIndex2Id);
nodesID2Index = zeros([maxNodeId 1], 'uint64');
nodesID2Index(nodesIndex2Id) = 1:nNodes;
mesh.setVertices(nodeId_Coord(:, 2:(dim+1)));

%% Read elements
str = fgetl(fid);
while isempty(str)
    str = fgetl(fid);
end
what = sscanf(strtrim(str), '%s', 1);
if strcmp(what, '**element')
    fprintf('Reading elements ... ');
else
    gtError('gtZsetMeshWriter:bad_element_header', ...
        'Couldn''t find ''**element'' header.');
end
str = fgetl(fid);
nElements = sscanf(strtrim(str), '%d');
elementType = sprintf('s3d%d', dim);
fprintf('%d %s elements\n', nElements, elementType);
nConnec = nElements*3;
[elem, sz] = fscanf(fid, ['%*d ' elementType ' %d %d %d'], nConnec);
if sz ~= nConnec 
    warning('gtZsetMeshWriter:element_connectivity_warning', ...
        'Problem in reading element connectivity.');
end
elem = reshape(uint64(elem), 3, nElements).';
mesh.setFaces([nodesID2Index(elem(:, :))]);

%% Finish reading
stop = false;
while (~stop)
    str = strtrim(fgetl(fid));
    while (isempty(str) || str(1) ~= '*')
        str = strtrim(fgetl(fid));
    end
    what = sscanf(str, '%s', 1);
    if strcmp(what, '***return')
        disp('Finished reading mesh');
        stop = true;
    elseif (strcmp(what, '***group'))
        % groups have to be added to mesh class to use it
        warning('gtZsetMeshWriter:set_header', ...
            'groups like nsets, elsets, lisets and fasets are not supported yet.');
    elseif (strcmp(what, '**nset') || strcmp(what, '**elset') || strcmp(what, '**liset') || strcmp(what, '**faset'))
        continue;
    end
end

%% Close the file end terminate
fclose(fid);

end % end of function
