classdef GtMesh < handle

properties
    Nvertices;  % Number of vertices
    vertices;   % List of vertices coordinates [Nvertices x 3]

    Nedges;     % Number of edges
    edges;      % List of edges vertices       [Nedges x 2]

    Nfaces;     % Number of faces
    faces;      % List of  faces vertices      [Nfaces x 2]

    connec_v2f; % Connection from vertex id to faces ids
    connec_f2f; % Connection from face id to faces ids

end % end of properties

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% PUBLIC METHODS
methods (Access = public)

    function obj = GtMesh()
    % GTMESH/GTMESH Contructor
        obj.Nvertices = 0;
        obj.vertices = [];

        obj.Nedges = 0;
        obj.edges = [];

        obj.Nfaces = 0;
        obj.faces = [];

        obj.connec_v2f = [];
        obj.connec_f2f = [];
    end

    function p = getFV(obj)
    % GTMESH/GETFV
        p.vertices = obj.vertices;
        %p.edges    = obj.edges;
        p.faces    = obj.faces;
    end

    function setEntity(obj, entity, x)
    % GTMESH/SETENTITY
        if ~ismember(entity, {'vertices', 'edges', 'faces'})
            gtError('GtMesh:wrong_entity', ['Unknown entity: ' entity]);
        end
        obj.(entity) = x;
        obj.(['N' entity]) = size(x, 1);
    end

    function setVertices(obj, v)
    % GTMESH/SETVERTICES
        obj.vertices = v;
        obj.Nvertices = size(v, 1);
    end

    function setLines(obj, e)
    % GTMESH/SETEDGES
        obj.edges = e;
        obj.Nedges = size(e, 1);
    end

    function setFaces(obj, f)
    % GTMESH/SETFACES
        obj.faces = f;
        obj.Nfaces = size(f, 1);
    end

    function buildEdges(obj)
    % GTMESH/BUILDEDGES
        nVerticesPerFace = size(obj.faces, 2);
        e = zeros(nVerticesPerFace * obj.Nfaces, 2);
        for iface = 1:nFaces
            f = obj.faces(iface, :);
            i0 = (iface - 1) * nVerticesPerFace + 1;
            i1 = iface * nVerticesPerFace;
            %edges(i0:i1, :) = [f' f([2:end 1])'];
            e(i0:i1, :) = [f f([2:end 1])]';
        end
        obj.edges = sortrows(unique(sort(e, 2), 'rows'));
    end

    function v2f = buildConnectivityVertices2Faces(obj, verticesIndex)
    % GTMESH/BUILDCONNECTIVITYVERTICES2FACES
    % Build connectivity vertices to faces.
        if ~exist('verticesIndex', 'var') || isempty(verticesIndex)
            verticesIndex = 1:obj.Nvertices;
        end
        t = triangulation(double(obj.faces), double(obj.vertices(:, 1:2)));
        %obj.connec_v2f = vertexAttachments(t, verticesIndex);
        v2f = vertexAttachments(t, double(verticesIndex(:)));
    end

    function buildConnectivityVertices2FacesFull(obj, force)
    % GTMESH/BUILDCONNECTIVITYVERTICES2FACESFULL
    % Build connectivity vertices to faces.
        if (~exist('force', 'var') || isempty(force))
            force = false;
        end
        if (~isempty(obj.connec_v2f) && (~force))
            disp('Vertices to faces connectivity is already computed, so skipping!');
            disp('(You can force it with the ''force'' argument).')
            return;
        end
        obj.connec_v2f = obj.buildConnectivityVertices2Faces();
    end

    function f2f = buildConnectivityFaces2Faces(obj, facesIndex)
    % GTMESH/BUILDCONNECTIVITYFACES2FACES
    % Build connectivity faces to faces.
        if ~exist('facesIndex', 'var') || isempty(facesIndex)
            facesIndex = 1:obj.Nfaces;
        end
        verticesIndex = unique(obj.faces(facesIndex, :));
        v2f = obj.buildConnectivityVertices2Faces(verticesIndex);
        f2f = cell(length(obj.Nfaces));

        gauge = GtGauge(length(facesIndex), 'Building faces to faces connectivity: ', 'style', 'percentage');
        for iface = facesIndex
            %% Get list of faces linked to the vertices belonging to the current face
            f2f{iface} = setdiff(unique([v2f{obj.faces(iface, :)}]), iface);
            gauge.incrementAndDisplay(100);
        end
        gauge.delete();
    end

    function buildConnectivityFaces2FacesFull(obj, force)
    % GTMESH/BUILDCONNECTIVITYFACES2FACESFULL
    % Build connectivity faces to faces for the complete mesh
        if (~exist('force', 'var') || isempty(force))
            force = false;
        end
        if (~isempty(obj.connec_f2f) && (~force))
            disp('Vertices to faces connectivity is already computed, so skipping!');
            disp('(You can force it with the ''force'' argument).')
            return;
        end
        obj.connec_f2f = obj.buildConnectivityFaces2Faces();
    end

    function boundaries = extractFacesBoundaries(obj, faces)
    % GTMESH/BEXTRACTFACESBOUNDARIES
    % Extract the boundary edges from a list of faces
        if (~exist('faces', 'var') || isempty(faces))
            faces = obj.faces;
        end
        if (size(faces, 2) ~= 3)
            gtError('GtMesh:extractFacesBoundaires:wrong_input', ...
                'Input faces array should be sized Nx3!');
        end
        % Get all edges from faces
        allEdges = [ faces(:, 1) faces(:, 2)  ; ...
                     faces(:, 2) faces(:, 3)  ; ...
                     faces(:, 3) faces(:, 1) ];
        % Sort rows so that edges are reorder in ascending order of indices
        sortedEdges = sort(allEdges, 2);
        % Determine uniqueness of edges, sortedEdges = c(ic, :);
        [c, ~, ic] = unique(sortedEdges, 'rows');
        % Determine counts for each unique edge
        counts = accumarray(ic(:), 1);
        % Extract edges that only occur once
        sortedBoundaryEdges = c((counts == 1), :);
        % Find in original edges so ordering of indices remains correct
        boundaries = allEdges(ismember(sortedEdges, sortedBoundaryEdges, 'rows'), :);
    end

end % end of public methods

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% PUBLIC STATIC METHODS
methods (Static, Access = public)

end % end of public static methods

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% PROTECTED METHODS
methods (Access = protected)

end % end of protected methods

end % end of class

