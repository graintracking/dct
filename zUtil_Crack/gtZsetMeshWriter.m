function gtZsetMeshWriter(mesh, filename)

%% Get file parts and check extension
[fpath, fname, fext] = fileparts(filename);
if isempty(fext)
    fext = '.geof';
    filename = fullfile(fpath, [fname fext]);
elseif ~strcmp(fext, '.geof')
    gtError('gtZsetMeshWriter:wrong_file_extension', ...
        'Output file extension should be ''.geof''!');
end

% Open Zset mesh
fid = fopen(filename, 'w');
if fid ==-1
    gtError('gtZsetMeshWriter:bad_output_file', ...
        'Can''t open the file in write mode.');
end
fprintf(fid, '***geometry\n');

% Get mesh dimension
dimension = size(mesh.vertices, 2);

% Writes nodes to geof
nodes = reshape(mesh.vertices', dimension, []);
N_nodes = size(nodes, 2);
fprintf(fid, '**node %d %d\n', N_nodes, dimension);
output = [(1:N_nodes); nodes];
fprintf(fid, '%d %f %f %f\n', output);

% Writes elements to geof
elements = reshape(mesh.faces', dimension, []);
N_elements = size(elements, 2);
fprintf(fid, '**element %d\n', N_elements);
output = [(1:N_elements); elements];
fprintf(fid,'%d s3d3 %d %d %d\n', output);

% Finish Zset mesh file
fprintf(fid, '***return\n');
fclose(fid);

end % end of function
