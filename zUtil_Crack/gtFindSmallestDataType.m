function dataType = gtFindSmallestDataType(val, classType)
% GTSMALLESTDATATYPE  Gives the smallest datatype that can handle the value.

switch classType
case 'uint'
    dataTypes = {'uint8', 'uint16', 'uint32', 'uint64'};
    for dType = dataTypes
        if (val >= intmin(dType{1})  && val <= intmax(dType{1}))
            dataType = dType{1};
            break;
        end
    end
case 'int'
    dataTypes = {'int8', 'int16', 'int32', 'int64'};
    for dType = dataTypes
        if (val >= intmin(dType{1})  && val <= intmax(dType{1}))
            dataType = dType{1};
            break;
        end
    end
case 'real'
    dataTypes = {'single', 'double'};
    for dType = dataTypes
        if (val >= realmin(dType{1})  && val <= realmax(dType{1}))
            dataType = dType{1};
            break;
        end
    end
otherwise
    gtError('gtFindSmallestDataType:wrong_class_type', ...
            ['Cannot manage class type: ' classType]);
end

if isempty(dataType)
    gtError('gtFindSmallestDataType:wrong_data_type', ...
        ['Cannot find relevant data type for value ''' val ...
        ''' for class type ''' classType]);
    end

end % end of function

